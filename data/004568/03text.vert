<s>
Paulo	Paula	k1gFnSc5	Paula
Coelho	Coelha	k1gFnSc5	Coelha
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgMnSc1d1	populární
brazilský	brazilský	k2eAgMnSc1d1	brazilský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Paulo	Paula	k1gFnSc5	Paula
Coelho	Coelha	k1gFnSc5	Coelha
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
brazilské	brazilský	k2eAgFnSc6d1	brazilská
středostavovské	středostavovský	k2eAgFnSc6d1	středostavovská
rodině	rodina	k1gFnSc6	rodina
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
jezuitské	jezuitský	k2eAgNnSc4d1	jezuitské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
touhu	touha	k1gFnSc4	touha
psát	psát	k5eAaImF	psát
rodiče	rodič	k1gMnSc4	rodič
neschvalovali	schvalovat	k5eNaImAgMnP	schvalovat
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
ho	on	k3xPp3gMnSc4	on
poslali	poslat	k5eAaPmAgMnP	poslat
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
podrobit	podrobit	k5eAaPmF	podrobit
elektrošokové	elektrošokový	k2eAgFnSc3d1	elektrošoková
terapii	terapie	k1gFnSc3	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
,	,	kIx,	,
koketoval	koketovat	k5eAaImAgInS	koketovat
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
magií	magie	k1gFnSc7	magie
Aleistera	Aleister	k1gMnSc2	Aleister
Crowleye	Crowley	k1gMnSc2	Crowley
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgInS	živit
se	se	k3xPyFc4	se
jako	jako	k9	jako
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgMnSc1d1	televizní
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
písňový	písňový	k2eAgMnSc1d1	písňový
textař	textař	k1gMnSc1	textař
brazilských	brazilský	k2eAgMnPc2d1	brazilský
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
zvláštním	zvláštní	k2eAgMnSc7d1	zvláštní
poradcem	poradce	k1gMnSc7	poradce
programu	program	k1gInSc2	program
UNESCO	UNESCO	kA	UNESCO
pro	pro	k7c4	pro
duchovní	duchovní	k2eAgNnSc4d1	duchovní
sbližování	sbližování	k1gNnSc4	sbližování
a	a	k8xC	a
dialog	dialog	k1gInSc4	dialog
mezi	mezi	k7c7	mezi
kulturami	kultura	k1gFnPc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
,	,	kIx,	,
vykonal	vykonat	k5eAaPmAgInS	vykonat
tisícikilometrovou	tisícikilometrový	k2eAgFnSc4d1	tisícikilometrová
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Santiaga	Santiago	k1gNnSc2	Santiago
de	de	k?	de
Compostela	Compostela	k1gFnSc1	Compostela
a	a	k8xC	a
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zužitkoval	zužitkovat	k5eAaPmAgMnS	zužitkovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
knize	kniha	k1gFnSc6	kniha
Poutník	poutník	k1gMnSc1	poutník
-	-	kIx~	-
Mágův	mágův	k2eAgInSc1d1	mágův
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
proslulost	proslulost	k1gFnSc4	proslulost
mu	on	k3xPp3gMnSc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
pohádkové	pohádkový	k2eAgNnSc4d1	pohádkové
podobenství	podobenství	k1gNnSc4	podobenství
Alchymista	alchymista	k1gMnSc1	alchymista
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
největším	veliký	k2eAgInSc7d3	veliký
brazilským	brazilský	k2eAgInSc7d1	brazilský
bestsellerem	bestseller	k1gInSc7	bestseller
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
hrdiny	hrdina	k1gMnPc4	hrdina
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
lidé	člověk	k1gMnPc1	člověk
hledající	hledající	k2eAgFnSc4d1	hledající
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
stereotypu	stereotyp	k1gInSc6	stereotyp
<g/>
,	,	kIx,	,
nabádá	nabádat	k5eAaBmIp3nS	nabádat
k	k	k7c3	k
duchovnímu	duchovní	k2eAgInSc3d1	duchovní
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
zodpovědnému	zodpovědný	k2eAgInSc3d1	zodpovědný
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
také	také	k9	také
fejetony	fejeton	k1gInPc4	fejeton
uveřejňované	uveřejňovaný	k2eAgInPc4d1	uveřejňovaný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
světových	světový	k2eAgFnPc6d1	světová
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
francouzský	francouzský	k2eAgMnSc1d1	francouzský
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Coelha	Coelha	k1gMnSc1	Coelha
rytířem	rytíř	k1gMnSc7	rytíř
Řádu	řád	k1gInSc2	řád
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
prestižní	prestižní	k2eAgFnSc7d1	prestižní
cenou	cena	k1gFnSc7	cena
Crystal	Crystal	k1gFnSc2	Crystal
Award	Award	k1gInSc1	Award
(	(	kIx(	(
<g/>
uděluje	udělovat	k5eAaImIp3nS	udělovat
ji	on	k3xPp3gFnSc4	on
Světové	světový	k2eAgNnSc1d1	světové
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
fórum	fórum	k1gNnSc1	fórum
<g/>
)	)	kIx)	)
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gMnSc3	on
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
udělila	udělit	k5eAaPmAgFnS	udělit
řád	řád	k1gInSc4	řád
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
cenami	cena	k1gFnPc7	cena
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
Coelho	Coel	k1gMnSc2	Coel
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
německá	německý	k2eAgFnSc1d1	německá
cena	cena	k1gFnSc1	cena
Bambi	Bambi	k1gNnSc1	Bambi
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
Club	club	k1gInSc1	club
of	of	k?	of
Budapest	Budapest	k1gFnSc1	Budapest
Planetary	Planetara	k1gFnSc2	Planetara
Arts	Arts	k1gInSc1	Arts
Award	Award	k1gInSc1	Award
2002	[number]	k4	2002
a	a	k8xC	a
Corine	Corin	k1gInSc5	Corin
Award	Award	k1gInSc4	Award
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
prestižní	prestižní	k2eAgFnSc2d1	prestižní
brazilské	brazilský	k2eAgFnSc2d1	brazilská
akademie	akademie	k1gFnSc2	akademie
ALB	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgNnPc2d1	další
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
jej	on	k3xPp3gNnSc4	on
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
pouť	pouť	k1gFnSc1	pouť
do	do	k7c2	do
Santiaga	Santiago	k1gNnSc2	Santiago
de	de	k?	de
Compostela	Compostela	k1gFnSc1	Compostela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Coelho	Coel	k1gMnSc2	Coel
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
přijel	přijet	k5eAaPmAgMnS	přijet
Coelho	Coel	k1gMnSc2	Coel
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
nemuslimským	muslimský	k2eNgMnSc7d1	nemuslimský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
oficiálně	oficiálně	k6eAd1	oficiálně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Procestoval	procestovat	k5eAaPmAgMnS	procestovat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
zavítal	zavítat	k5eAaPmAgInS	zavítat
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
setkal	setkat	k5eAaPmAgMnS	setkat
po	po	k7c6	po
34	[number]	k4	34
letech	let	k1gInPc6	let
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Ivanem	Ivan	k1gMnSc7	Ivan
Tomkem	Tomek	k1gMnSc7	Tomek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
ve	v	k7c6	v
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
uličce	ulička	k1gFnSc6	ulička
portrét	portrét	k1gInSc4	portrét
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Poutník	poutník	k1gMnSc1	poutník
-	-	kIx~	-
Mágův	mágův	k2eAgInSc1d1	mágův
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
O	o	k7c4	o
Diário	Diário	k1gNnSc4	Diário
de	de	k?	de
um	um	k1gInSc1	um
Mago	Maga	k1gFnSc5	Maga
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Alchymista	alchymista	k1gMnSc1	alchymista
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
;	;	kIx,	;
zvuková	zvukový	k2eAgFnSc1d1	zvuková
dramatizace	dramatizace	k1gFnSc1	dramatizace
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
O	o	k7c6	o
Alquimista	Alquimista	k1gMnSc1	Alquimista
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Brida	Brida	k1gMnSc1	Brida
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
Brida	Brida	k1gMnSc1	Brida
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
U	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Piedra	Piedr	k1gMnSc4	Piedr
jsem	být	k5eAaImIp1nS	být
usedla	usednout	k5eAaPmAgFnS	usednout
a	a	k8xC	a
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Na	na	k7c6	na
margem	marg	k1gInSc7	marg
do	do	k7c2	do
rio	rio	k?	rio
Piedra	Piedra	k1gFnSc1	Piedra
eu	eu	k?	eu
sentei	sentei	k6eAd1	sentei
e	e	k0	e
chorei	chorea	k1gFnSc6	chorea
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Pátá	pátý	k4xOgFnSc1	pátý
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
A	a	k8xC	a
Quinta	Quinta	k1gMnSc1	Quinta
Montanha	Montanha	k1gMnSc1	Montanha
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Veronika	Veronika	k1gFnSc1	Veronika
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Veronika	Veronika	k1gFnSc1	Veronika
decide	decid	k1gInSc5	decid
morrer	morrer	k1gMnSc1	morrer
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
slečna	slečna	k1gFnSc1	slečna
Chantal	Chantal	k1gMnSc1	Chantal
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
O	o	k7c4	o
Demônio	Demônio	k1gNnSc4	Demônio
e	e	k0	e
a	a	k8xC	a
srta	srta	k1gMnSc1	srta
Prym	Prym	k1gMnSc1	Prym
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Jedenáct	jedenáct	k4xCc1	jedenáct
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
Onze	Onz	k1gInSc2	Onz
Minutos	Minutos	k1gInSc1	Minutos
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Záhir	Záhir	k1gMnSc1	Záhir
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
Zahir	Zahir	k1gMnSc1	Zahir
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Čarodějka	čarodějka	k1gFnSc1	čarodějka
z	z	k7c2	z
Portobella	Portobello	k1gNnSc2	Portobello
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
A	a	k8xC	a
bruxa	bruxa	k1gFnSc1	bruxa
de	de	k?	de
Portobello	Portobello	k1gNnSc1	Portobello
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
O	o	k7c4	o
vencedor	vencedor	k1gInSc4	vencedor
está	estat	k5eAaBmIp3nS	estat
só	só	k?	só
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Valkýry	valkýra	k1gFnSc2	valkýra
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
As	as	k9	as
Valkírias	Valkírias	k1gInSc1	Valkírias
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Aleph	Aleph	k1gInSc1	Aleph
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Rukopis	rukopis	k1gInSc1	rukopis
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
Akkonu	Akkon	k1gInSc6	Akkon
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
Manuscrito	Manuscrita	k1gFnSc5	Manuscrita
Encontrado	Encontrada	k1gFnSc5	Encontrada
em	em	k?	em
Accra	Accra	k1gMnSc1	Accra
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Nevěra	nevěra	k1gFnSc1	nevěra
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
Adultério	Adultério	k1gNnSc1	Adultério
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Rukověť	rukověť	k1gFnSc4	rukověť
bojovníka	bojovník	k1gMnSc2	bojovník
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Manual	Manual	k1gMnSc1	Manual
do	do	k7c2	do
guerreiro	guerreiro	k6eAd1	guerreiro
da	da	k?	da
luz	luza	k1gFnPc2	luza
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Jako	jako	k8xC	jako
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
Ser	srát	k5eAaImRp2nS	srát
como	como	k1gNnSc4	como
um	um	k1gInSc1	um
rio	rio	k?	rio
que	que	k?	que
flui	flu	k1gFnSc2	flu
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Paulo	Paula	k1gFnSc5	Paula
Coelho	Coel	k1gMnSc4	Coel
<g/>
:	:	kIx,	:
Zpověď	zpověď	k1gFnSc4	zpověď
poutníka	poutník	k1gMnSc2	poutník
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
Las	laso	k1gNnPc2	laso
Confesiones	Confesionesa	k1gFnPc2	Confesionesa
del	del	k?	del
Peregrino	Peregrino	k1gNnSc1	Peregrino
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
Juan	Juan	k1gMnSc1	Juan
Arias	Arias	k1gMnSc1	Arias
Fernando	Fernanda	k1gFnSc5	Fernanda
Morais	Morais	k1gFnSc1	Morais
<g/>
:	:	kIx,	:
Mág	mág	k1gMnSc1	mág
-	-	kIx~	-
Paulo	Paula	k1gFnSc5	Paula
Coelho	Coel	k1gMnSc2	Coel
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
O	o	k7c4	o
mago	mago	k1gNnSc4	mago
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
citáty	citát	k1gInPc1	citát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
Vida	Vida	k?	Vida
<g/>
:	:	kIx,	:
Citaçõ	Citaçõ	k1gMnSc1	Citaçõ
selecionadas	selecionadas	k1gMnSc1	selecionadas
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Arquivos	Arquivos	k1gInSc1	Arquivos
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
O	o	k7c4	o
Manual	Manual	k1gInSc4	Manual
Prático	Prático	k6eAd1	Prático
do	do	k7c2	do
Vampirismo	Vampirisma	k1gFnSc5	Vampirisma
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
O	o	k7c6	o
Dom	Dom	k?	Dom
Supremo	Suprema	k1gFnSc5	Suprema
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Maktub	Maktuba	k1gFnPc2	Maktuba
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Letras	Letrasa	k1gFnPc2	Letrasa
do	do	k7c2	do
amor	amor	k1gMnSc1	amor
de	de	k?	de
um	um	k1gInSc1	um
prophet	propheta	k1gFnPc2	propheta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Palavras	Palavrasa	k1gFnPc2	Palavrasa
essenciais	essenciais	k1gFnPc2	essenciais
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Histórias	Histórias	k1gInSc1	Histórias
para	para	k2eAgFnPc2d1	para
pais	paisa	k1gFnPc2	paisa
<g/>
,	,	kIx,	,
filhos	filhosa	k1gFnPc2	filhosa
e	e	k0	e
netos	netos	k1gMnSc1	netos
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
O	o	k7c6	o
Gê	Gê	k1gFnSc6	Gê
e	e	k0	e
as	as	k1gNnPc1	as
Rosas	Rosas	k1gMnSc1	Rosas
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Caminhos	Caminhosa	k1gFnPc2	Caminhosa
Recolhidos	Recolhidosa	k1gFnPc2	Recolhidosa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
</s>
