<s>
Cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
(	(	kIx(	(
<g/>
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
humánní	humánní	k2eAgFnSc2d1	humánní
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
bakterie	bakterie	k1gFnSc1	bakterie
Vibrio	vibrio	k1gNnSc4	vibrio
cholerae	choleraat	k5eAaPmIp3nS	choleraat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
způsobované	způsobovaný	k2eAgFnSc2d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Pasteurella	Pasteurell	k1gMnSc2	Pasteurell
multocida	multocid	k1gMnSc2	multocid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
veterinární	veterinární	k2eAgFnSc4d1	veterinární
péči	péče	k1gFnSc4	péče
č.	č.	k?	č.
166	[number]	k4	166
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
představuje	představovat	k5eAaImIp3nS	představovat
cholera	cholera	k1gFnSc1	cholera
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
nákazu	nákaza	k1gFnSc4	nákaza
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
i	i	k8xC	i
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
akutní	akutní	k2eAgFnSc2d1	akutní
hemoragické	hemoragický	k2eAgFnSc2d1	hemoragická
septikémie	septikémie	k1gFnSc2	septikémie
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
morbiditou	morbidita	k1gFnSc7	morbidita
i	i	k8xC	i
mortalitou	mortalita	k1gFnSc7	mortalita
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
chronické	chronický	k2eAgNnSc1d1	chronické
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
lokalizovaná	lokalizovaný	k2eAgFnSc1d1	lokalizovaná
(	(	kIx(	(
<g/>
latentní	latentní	k2eAgFnSc1d1	latentní
<g/>
)	)	kIx)	)
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
i	i	k8xC	i
průběh	průběh	k1gInSc1	průběh
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňovat	k5eAaImNgInP	ovlivňovat
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
význam	význam	k1gInSc1	význam
sehrála	sehrát	k5eAaPmAgFnS	sehrát
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pasteurelóza	pasteurelóza	k1gFnSc1	pasteurelóza
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
označují	označovat	k5eAaImIp3nP	označovat
infekce	infekce	k1gFnPc1	infekce
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
i	i	k9	i
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
bakteriemi	bakterie	k1gFnPc7	bakterie
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Pasteurella	Pasteurello	k1gNnSc2	Pasteurello
<g/>
.	.	kIx.	.
</s>
<s>
Pasteurelóza	Pasteurelóza	k1gFnSc1	Pasteurelóza
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
čeledí	čeleď	k1gFnPc2	čeleď
bažantovitých	bažantovitý	k2eAgFnPc2d1	bažantovitý
(	(	kIx(	(
<g/>
Phasianidae	Phasianidae	k1gFnPc2	Phasianidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
kachnovitých	kachnovitý	k2eAgFnPc6d1	kachnovitý
(	(	kIx(	(
<g/>
Anatidae	Anatidae	k1gFnPc6	Anatidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
<g/>
,	,	kIx,	,
pštrosů	pštros	k1gMnPc2	pštros
<g/>
,	,	kIx,	,
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
,	,	kIx,	,
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
dravců	dravec	k1gMnPc2	dravec
(	(	kIx(	(
<g/>
Falconiformes	Falconiformes	k1gMnSc1	Falconiformes
<g/>
)	)	kIx)	)
i	i	k9	i
pěvců	pěvec	k1gMnPc2	pěvec
(	(	kIx(	(
<g/>
Passeriformes	Passeriformes	k1gMnSc1	Passeriformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc4d1	značný
druhové	druhový	k2eAgInPc4d1	druhový
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
vnímavosti	vnímavost	k1gFnSc6	vnímavost
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
i	i	k9	i
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnější	závažný	k2eAgFnSc7d3	nejzávažnější
pasteurelózou	pasteurelóza	k1gFnSc7	pasteurelóza
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Epizootie	Epizootie	k1gFnSc1	Epizootie
cholery	cholera	k1gFnSc2	cholera
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
jsou	být	k5eAaImIp3nP	být
popisovány	popisován	k2eAgInPc4d1	popisován
již	již	k6eAd1	již
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
"	"	kIx"	"
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
Mailet	Mailet	k1gInSc1	Mailet
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
prokázal	prokázat	k5eAaPmAgInS	prokázat
její	její	k3xOp3gFnSc4	její
kontagiozitu	kontagiozita	k1gFnSc4	kontagiozita
a	a	k8xC	a
formuloval	formulovat	k5eAaImAgMnS	formulovat
způsoby	způsob	k1gInPc7	způsob
její	její	k3xOp3gFnSc2	její
prevence	prevence	k1gFnSc2	prevence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
prokázal	prokázat	k5eAaPmAgInS	prokázat
Toussaint	Toussaint	k1gInSc1	Toussaint
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
etiologii	etiologie	k1gFnSc4	etiologie
a	a	k8xC	a
Pasteur	Pasteur	k1gMnSc1	Pasteur
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
izoloval	izolovat	k5eAaBmAgMnS	izolovat
bakterie	bakterie	k1gFnPc4	bakterie
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
očkovací	očkovací	k2eAgFnSc4d1	očkovací
látku	látka	k1gFnSc4	látka
záměrným	záměrný	k2eAgNnSc7d1	záměrné
oslabením	oslabení	k1gNnSc7	oslabení
patogenity	patogenita	k1gFnSc2	patogenita
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInPc4	první
popisy	popis	k1gInPc4	popis
cholery	cholera	k1gFnPc1	cholera
známy	znám	k2eAgFnPc1d1	známa
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
cholera	cholera	k1gFnSc1	cholera
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
hojně	hojně	k6eAd1	hojně
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
a	a	k8xC	a
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zlepšováním	zlepšování	k1gNnSc7	zlepšování
podmínek	podmínka	k1gFnPc2	podmínka
hygieny	hygiena	k1gFnSc2	hygiena
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
krmných	krmný	k2eAgFnPc2d1	krmná
směsí	směs	k1gFnPc2	směs
se	se	k3xPyFc4	se
frekvence	frekvence	k1gFnSc1	frekvence
výskytu	výskyt	k1gInSc2	výskyt
i	i	k9	i
průběh	průběh	k1gInSc4	průběh
onemocnění	onemocnění	k1gNnPc2	onemocnění
postupně	postupně	k6eAd1	postupně
snižovaly	snižovat	k5eAaImAgFnP	snižovat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
cholera	cholera	k1gFnSc1	cholera
objevuje	objevovat	k5eAaImIp3nS	objevovat
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
v	v	k7c6	v
chronické	chronický	k2eAgFnSc6d1	chronická
nebo	nebo	k8xC	nebo
lokalizované	lokalizovaný	k2eAgFnSc6d1	lokalizovaná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
zařazovaných	zařazovaný	k2eAgFnPc2d1	zařazovaná
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Pasteurella	Pasteurello	k1gNnSc2	Pasteurello
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
11-13	[number]	k4	11-13
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
prokázány	prokázán	k2eAgFnPc4d1	prokázána
kromě	kromě	k7c2	kromě
bakterie	bakterie	k1gFnSc2	bakterie
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc2	původce
cholery	cholera	k1gFnSc2	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
)	)	kIx)	)
také	také	k9	také
méně	málo	k6eAd2	málo
patogenní	patogenní	k2eAgFnSc4d1	patogenní
P.	P.	kA	P.
anatis	anatis	k1gFnSc4	anatis
<g/>
,	,	kIx,	,
P.	P.	kA	P.
avium	avium	k1gInSc1	avium
<g/>
,	,	kIx,	,
P.	P.	kA	P.
gallinarum	gallinarum	k1gInSc1	gallinarum
<g/>
,	,	kIx,	,
P.	P.	kA	P.
langaa	langa	k1gInSc2	langa
<g/>
,	,	kIx,	,
P.	P.	kA	P.
volantium	volantium	k1gNnSc1	volantium
a	a	k8xC	a
P.	P.	kA	P.
species	species	k1gFnSc2	species
A.	A.	kA	A.
P.	P.	kA	P.
gallinarum	gallinarum	k1gInSc1	gallinarum
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
nižší	nízký	k2eAgFnSc4d2	nižší
patogenitu	patogenita	k1gFnSc4	patogenita
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgNnSc1d1	stejné
hostitelské	hostitelský	k2eAgNnSc1d1	hostitelské
spektrum	spektrum	k1gNnSc1	spektrum
jako	jako	k8xC	jako
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
spíše	spíše	k9	spíše
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
sekundární	sekundární	k2eAgInSc4d1	sekundární
patogen	patogen	k1gInSc4	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
pěvců	pěvec	k1gMnPc2	pěvec
izolovány	izolován	k2eAgInPc1d1	izolován
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
pasteurely	pasteurel	k1gInPc1	pasteurel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
dosud	dosud	k6eAd1	dosud
taxonomicky	taxonomicky	k6eAd1	taxonomicky
zařazeny	zařadit	k5eAaPmNgInP	zařadit
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
;	;	kIx,	;
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
podmíněně	podmíněně	k6eAd1	podmíněně
patogenní	patogenní	k2eAgMnPc4d1	patogenní
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
je	být	k5eAaImIp3nS	být
G-	G-	k1gFnSc1	G-
<g/>
,	,	kIx,	,
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
<g/>
,	,	kIx,	,
nesporulující	sporulující	k2eNgFnSc1d1	nesporulující
<g/>
,	,	kIx,	,
opouzdřená	opouzdřený	k2eAgFnSc1d1	opouzdřená
nebo	nebo	k8xC	nebo
neopouzdřená	opouzdřený	k2eNgFnSc1d1	opouzdřený
krátká	krátký	k2eAgFnSc1d1	krátká
tyčinka	tyčinka	k1gFnSc1	tyčinka
(	(	kIx(	(
<g/>
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,4	[number]	k4	0,4
x	x	k?	x
0,6	[number]	k4	0,6
<g/>
-	-	kIx~	-
<g/>
2,5	[number]	k4	2,5
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
ovoidního	ovoidní	k2eAgInSc2d1	ovoidní
nebo	nebo	k8xC	nebo
kokobacilárního	kokobacilární	k2eAgInSc2d1	kokobacilární
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
jako	jako	k9	jako
řetízky	řetízek	k1gInPc1	řetízek
nebo	nebo	k8xC	nebo
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
podmínek	podmínka	k1gFnPc2	podmínka
nebo	nebo	k8xC	nebo
po	po	k7c6	po
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
kultivacích	kultivace	k1gFnPc6	kultivace
mají	mít	k5eAaImIp3nP	mít
buňky	buňka	k1gFnPc1	buňka
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
pleomorfismu	pleomorfismus	k1gInSc3	pleomorfismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otiscích	otisk	k1gInPc6	otisk
z	z	k7c2	z
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
exsudátů	exsudát	k1gInPc2	exsudát
a	a	k8xC	a
v	v	k7c6	v
čerstvě	čerstvě	k6eAd1	čerstvě
izolovaných	izolovaný	k2eAgFnPc6d1	izolovaná
kulturách	kultura	k1gFnPc6	kultura
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
bipolárně	bipolárně	k6eAd1	bipolárně
po	po	k7c6	po
obarvení	obarvení	k1gNnSc6	obarvení
podle	podle	k7c2	podle
Giemsy	Giemsa	k1gFnSc2	Giemsa
nebo	nebo	k8xC	nebo
Wrighta	Wrighto	k1gNnSc2	Wrighto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
opouzdřených	opouzdřený	k2eAgInPc2d1	opouzdřený
kmenů	kmen	k1gInPc2	kmen
je	být	k5eAaImIp3nS	být
pouzdro	pouzdro	k1gNnSc1	pouzdro
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
metodou	metoda	k1gFnSc7	metoda
negativního	negativní	k2eAgNnSc2d1	negativní
barvení	barvení	k1gNnSc2	barvení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
virulence	virulence	k1gFnSc2	virulence
a	a	k8xC	a
utilizace	utilizace	k1gFnSc2	utilizace
sorbitolu	sorbitol	k1gInSc2	sorbitol
a	a	k8xC	a
dulcitolu	dulcitol	k1gInSc2	dulcitol
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
3	[number]	k4	3
fenotypicky	fenotypicky	k6eAd1	fenotypicky
odlišné	odlišný	k2eAgInPc1d1	odlišný
poddruhy	poddruh	k1gInPc1	poddruh
-	-	kIx~	-
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
multocida	multocid	k1gMnSc2	multocid
<g/>
,	,	kIx,	,
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
septica	septica	k1gMnSc1	septica
a	a	k8xC	a
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
gallinarum	gallinarum	k1gInSc1	gallinarum
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
běžných	běžný	k2eAgFnPc6d1	běžná
půdách	půda	k1gFnPc6	půda
při	při	k7c6	při
optimální	optimální	k2eAgFnSc6d1	optimální
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
pH	ph	kA	ph
7,2	[number]	k4	7,2
<g/>
-	-	kIx~	-
<g/>
7,8	[number]	k4	7,8
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
izolaci	izolace	k1gFnSc4	izolace
pasteurel	pasteurlo	k1gNnPc2	pasteurlo
existují	existovat	k5eAaImIp3nP	existovat
různá	různý	k2eAgNnPc1d1	různé
selektivní	selektivní	k2eAgNnPc1d1	selektivní
i	i	k8xC	i
chemicky	chemicky	k6eAd1	chemicky
definovaná	definovaný	k2eAgNnPc1d1	definované
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
kyselinu	kyselina	k1gFnSc4	kyselina
pantotenovou	pantotenový	k2eAgFnSc4d1	pantotenová
a	a	k8xC	a
nikotinamid	nikotinamid	k1gInSc4	nikotinamid
jako	jako	k8xS	jako
růstový	růstový	k2eAgInSc1d1	růstový
faktor	faktor	k1gInSc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tekutých	tekutý	k2eAgFnPc6d1	tekutá
půdách	půda	k1gFnPc6	půda
obohacených	obohacený	k2eAgFnPc6d1	obohacená
peptonem	pepton	k1gInSc7	pepton
<g/>
,	,	kIx,	,
hydrolyzátem	hydrolyzát	k1gInSc7	hydrolyzát
kaseinu	kasein	k1gInSc2	kasein
nebo	nebo	k8xC	nebo
ptačím	ptačí	k2eAgNnSc7d1	ptačí
sérem	sérum	k1gNnSc7	sérum
naroste	narůst	k5eAaPmIp3nS	narůst
za	za	k7c4	za
16-24	[number]	k4	16-24
hodin	hodina	k1gFnPc2	hodina
s	s	k7c7	s
difuzním	difuzní	k2eAgInSc7d1	difuzní
zákalem	zákal	k1gInSc7	zákal
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
viskózním	viskózní	k2eAgInSc7d1	viskózní
sedimentem	sediment	k1gInSc7	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
agaru	agar	k1gInSc6	agar
i	i	k8xC	i
na	na	k7c6	na
agaru	agar	k1gInSc6	agar
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
nehemolyzuje	hemolyzovat	k5eNaPmIp3nS	hemolyzovat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
séra	sérum	k1gNnSc2	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Kultivace	kultivace	k1gFnSc1	kultivace
na	na	k7c6	na
Endově	Endův	k2eAgInSc6d1	Endův
agaru	agar	k1gInSc6	agar
je	být	k5eAaImIp3nS	být
vesměs	vesměs	k6eAd1	vesměs
negativní	negativní	k2eAgMnSc1d1	negativní
nebo	nebo	k8xC	nebo
značně	značně	k6eAd1	značně
opožděná	opožděný	k2eAgFnSc1d1	opožděná
<g/>
,	,	kIx,	,
s	s	k7c7	s
koloniemi	kolonie	k1gFnPc7	kolonie
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
nejvýše	vysoce	k6eAd3	vysoce
0,3	[number]	k4	0,3
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MacConkeyově	MacConkeyův	k2eAgFnSc6d1	MacConkeyův
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
na	na	k7c6	na
Simmonsově	Simmonsův	k2eAgInSc6d1	Simmonsův
citrátovém	citrátový	k2eAgInSc6d1	citrátový
agaru	agar	k1gInSc6	agar
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
neroste	růst	k5eNaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Pasteurely	Pasteurela	k1gFnPc1	Pasteurela
na	na	k7c6	na
pevných	pevný	k2eAgFnPc6d1	pevná
kultivačních	kultivační	k2eAgFnPc6d1	kultivační
půdách	půda	k1gFnPc6	půda
snadno	snadno	k6eAd1	snadno
disociují	disociovat	k5eAaBmIp3nP	disociovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
hladkou	hladký	k2eAgFnSc7d1	hladká
<g/>
,	,	kIx,	,
mukózní	mukózní	k2eAgFnSc7d1	mukózní
i	i	k8xC	i
drsnou	drsný	k2eAgFnSc7d1	drsná
fází	fáze	k1gFnSc7	fáze
růstu	růst	k1gInSc2	růst
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
virulentní	virulentní	k2eAgFnPc4d1	virulentní
a	a	k8xC	a
opouzdřené	opouzdřený	k2eAgFnPc4d1	opouzdřená
bakterie	bakterie	k1gFnPc4	bakterie
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
z	z	k7c2	z
akutních	akutní	k2eAgFnPc2d1	akutní
epizootií	epizootie	k1gFnPc2	epizootie
zpravidla	zpravidla	k6eAd1	zpravidla
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
S-fázi	Sáze	k1gFnSc6	S-fáze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hladkých	hladký	k2eAgInPc2d1	hladký
<g/>
,	,	kIx,	,
lesklých	lesklý	k2eAgInPc2d1	lesklý
<g/>
,	,	kIx,	,
konvexních	konvexní	k2eAgInPc2d1	konvexní
<g/>
,	,	kIx,	,
charakteristicky	charakteristicky	k6eAd1	charakteristicky
zapáchajících	zapáchající	k2eAgFnPc2d1	zapáchající
a	a	k8xC	a
světlolomných	světlolomný	k2eAgFnPc2d1	světlolomný
kolonií	kolonie	k1gFnPc2	kolonie
velikosti	velikost	k1gFnSc2	velikost
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
mm	mm	kA	mm
<g/>
,	,	kIx,	,
s	s	k7c7	s
tendencí	tendence	k1gFnSc7	tendence
splývat	splývat	k5eAaImF	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sporadických	sporadický	k2eAgInPc2d1	sporadický
případů	případ	k1gInPc2	případ
chronické	chronický	k2eAgFnSc2d1	chronická
cholery	cholera	k1gFnSc2	cholera
lze	lze	k6eAd1	lze
vykultivovat	vykultivovat	k5eAaPmF	vykultivovat
kolonie	kolonie	k1gFnSc1	kolonie
plošší	plochý	k2eAgFnSc1d2	plošší
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
namodralé	namodralý	k2eAgFnPc1d1	namodralá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
nesvětlolomné	světlolomný	k2eNgFnPc1d1	světlolomný
a	a	k8xC	a
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
izoláty	izolát	k1gInPc1	izolát
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
virulentní	virulentní	k2eAgFnPc1d1	virulentní
a	a	k8xC	a
neopouzdřené	opouzdřený	k2eNgFnPc1d1	opouzdřený
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
pasteurely	pasteurel	k1gInPc1	pasteurel
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mukózních	mukózní	k2eAgInPc2d1	mukózní
<g/>
,	,	kIx,	,
vlhkých	vlhký	k2eAgInPc2d1	vlhký
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
<g/>
,	,	kIx,	,
šedých	šedý	k2eAgInPc2d1	šedý
a	a	k8xC	a
splývajících	splývající	k2eAgFnPc2d1	splývající
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
M-fáze	Máze	k1gFnSc1	M-fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
o	o	k7c4	o
neopouzdřené	opouzdřený	k2eNgInPc4d1	neopouzdřený
avirulentní	avirulentní	k2eAgInPc4d1	avirulentní
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Virulentní	virulentní	k2eAgInPc1d1	virulentní
izoláty	izolát	k1gInPc1	izolát
mohou	moct	k5eAaImIp3nP	moct
během	během	k7c2	během
kultivací	kultivace	k1gFnPc2	kultivace
disociovat	disociovat	k5eAaBmF	disociovat
a	a	k8xC	a
růst	růst	k1gInSc4	růst
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
namodralých	namodralý	k2eAgFnPc2d1	namodralá
<g/>
,	,	kIx,	,
šedých	šedý	k2eAgFnPc2d1	šedá
i	i	k8xC	i
drsných	drsný	k2eAgFnPc2d1	drsná
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
růstové	růstový	k2eAgFnPc1d1	růstová
formy	forma	k1gFnPc1	forma
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
složením	složení	k1gNnSc7	složení
média	médium	k1gNnSc2	médium
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
imunogenních	imunogenní	k2eAgInPc2d1	imunogenní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
,	,	kIx,	,
fruktózy	fruktóza	k1gFnSc2	fruktóza
<g/>
,	,	kIx,	,
galaktózy	galaktóza	k1gFnSc2	galaktóza
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
tvorby	tvorba	k1gFnSc2	tvorba
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
a	a	k8xC	a
sacharózy	sacharóza	k1gFnPc4	sacharóza
zkvašuje	zkvašovat	k5eAaImIp3nS	zkvašovat
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
také	také	k9	také
manitol	manitol	k1gInSc1	manitol
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
laktóza	laktóza	k1gFnSc1	laktóza
a	a	k8xC	a
maltóza	maltóza	k1gFnSc1	maltóza
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
tvoří	tvořit	k5eAaImIp3nP	tvořit
indol	indol	k1gInSc4	indol
a	a	k8xC	a
sirovodík	sirovodík	k1gInSc4	sirovodík
<g/>
,	,	kIx,	,
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
ornitin	ornitin	k1gInSc4	ornitin
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
katalázu	kataláza	k1gFnSc4	kataláza
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
i	i	k9	i
na	na	k7c4	na
oxidázu	oxidáza	k1gFnSc4	oxidáza
<g/>
.	.	kIx.	.
</s>
<s>
Želatinu	želatina	k1gFnSc4	želatina
nezkapalňují	zkapalňovat	k5eNaImIp3nP	zkapalňovat
<g/>
,	,	kIx,	,
zkouška	zkouška	k1gFnSc1	zkouška
na	na	k7c4	na
ureázu	ureáza	k1gFnSc4	ureáza
je	být	k5eAaImIp3nS	být
negativní	negativní	k2eAgInSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
septica	septica	k1gMnSc1	septica
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
ze	z	k7c2	z
3	[number]	k4	3
poddruhů	poddruh	k1gInPc2	poddruh
nezkvašuje	zkvašovat	k5eNaImIp3nS	zkvašovat
sorbitol	sorbitol	k1gInSc1	sorbitol
<g/>
,	,	kIx,	,
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
gallinarum	gallinarum	k1gInSc1	gallinarum
naopak	naopak	k6eAd1	naopak
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
zkvašuje	zkvašovat	k5eAaImIp3nS	zkvašovat
dulcitol	dulcitol	k1gInSc4	dulcitol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
jsou	být	k5eAaImIp3nP	být
pasteurely	pasteurel	k1gInPc1	pasteurel
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
destruovány	destruovat	k5eAaImNgFnP	destruovat
slunečním	sluneční	k2eAgNnSc7d1	sluneční
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
sušením	sušení	k1gNnSc7	sušení
a	a	k8xC	a
teplem	teplo	k1gNnSc7	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysušení	vysušení	k1gNnSc6	vysušení
zanikají	zanikat	k5eAaImIp3nP	zanikat
za	za	k7c7	za
2-3	[number]	k4	2-3
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
trusu	trus	k1gInSc2	trus
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
až	až	k9	až
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
nad	nad	k7c7	nad
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
ničí	ničit	k5eAaImIp3nS	ničit
pasteurely	pasteurela	k1gFnPc4	pasteurela
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
je	on	k3xPp3gMnPc4	on
ničí	ničit	k5eAaImIp3nS	ničit
běžné	běžný	k2eAgInPc1d1	běžný
dezinfekční	dezinfekční	k2eAgInPc1d1	dezinfekční
prostředky	prostředek	k1gInPc1	prostředek
-	-	kIx~	-
formaldehyd	formaldehyd	k1gInSc1	formaldehyd
<g/>
,	,	kIx,	,
fenol	fenol	k1gInSc1	fenol
<g/>
,	,	kIx,	,
louh	louh	k1gInSc1	louh
sodný	sodný	k2eAgInSc1d1	sodný
i	i	k8xC	i
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
betapropiolakton	betapropiolakton	k1gInSc1	betapropiolakton
nebo	nebo	k8xC	nebo
glutaraldehyd	glutaraldehyd	k1gInSc1	glutaraldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Sérotypizace	Sérotypizace	k1gFnSc1	Sérotypizace
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
detekci	detekce	k1gFnSc4	detekce
specifických	specifický	k2eAgInPc2d1	specifický
pouzderných	pouzderný	k2eAgInPc2d1	pouzderný
a	a	k8xC	a
somatických	somatický	k2eAgInPc2d1	somatický
antigenů	antigen	k1gInPc2	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgInPc1d1	specifický
antigeny	antigen	k1gInPc1	antigen
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
pasivním	pasivní	k2eAgInSc7d1	pasivní
hemaglutinačním	hemaglutinační	k2eAgInSc7d1	hemaglutinační
testem	test	k1gInSc7	test
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rozlišit	rozlišit	k5eAaPmF	rozlišit
5	[number]	k4	5
sérologických	sérologický	k2eAgInPc2d1	sérologický
typů	typ	k1gInPc2	typ
označovaných	označovaný	k2eAgInPc2d1	označovaný
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
-	-	kIx~	-
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
byly	být	k5eAaImAgInP	být
zjištěny	zjištěn	k2eAgInPc1d1	zjištěn
sérotypy	sérotyp	k1gInPc1	sérotyp
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
D	D	kA	D
a	a	k8xC	a
F.	F.	kA	F.
Mukózní	Mukózní	k2eAgNnSc1d1	Mukózní
pouzdro	pouzdro	k1gNnSc1	pouzdro
(	(	kIx(	(
<g/>
u	u	k7c2	u
typu	typ	k1gInSc2	typ
A	a	k9	a
představované	představovaný	k2eAgInPc1d1	představovaný
z	z	k7c2	z
převážné	převážný	k2eAgFnSc2d1	převážná
části	část	k1gFnSc2	část
hyaluronovou	hyaluronův	k2eAgFnSc7d1	hyaluronův
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
faktorem	faktor	k1gInSc7	faktor
virulence	virulence	k1gFnSc2	virulence
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
protekčním	protekční	k2eAgInSc7d1	protekční
(	(	kIx(	(
<g/>
imunogenním	imunogenní	k2eAgInSc7d1	imunogenní
<g/>
)	)	kIx)	)
antigenem	antigen	k1gInSc7	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
fagocytózu	fagocytóza	k1gFnSc4	fagocytóza
a	a	k8xC	a
baktericidní	baktericidní	k2eAgFnSc4d1	baktericidní
aktivitu	aktivita	k1gFnSc4	aktivita
polymorfonukleárních	polymorfonukleární	k2eAgInPc2d1	polymorfonukleární
leukocytů	leukocyt	k1gInPc2	leukocyt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
termolabilní	termolabilní	k2eAgMnSc1d1	termolabilní
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
se	se	k3xPyFc4	se
teplotou	teplota	k1gFnSc7	teplota
56	[number]	k4	56
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
30	[number]	k4	30
min	mina	k1gFnPc2	mina
a	a	k8xC	a
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
24	[number]	k4	24
<g/>
hod	hod	k1gInSc1	hod
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
termostatu	termostat	k1gInSc6	termostat
při	při	k7c6	při
37	[number]	k4	37
°	°	k?	°
<g/>
C.	C.	kA	C.
Typizace	typizace	k1gFnSc1	typizace
somatických	somatický	k2eAgInPc2d1	somatický
lipopolysacharidových	lipopolysacharidový	k2eAgInPc2d1	lipopolysacharidový
antigenů	antigen	k1gInPc2	antigen
pasteurel	pasteurela	k1gFnPc2	pasteurela
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zkumavkovou	zkumavkův	k2eAgFnSc7d1	zkumavkův
aglutinací	aglutinace	k1gFnSc7	aglutinace
anebo	anebo	k8xC	anebo
častěji	často	k6eAd2	často
precipitací	precipitace	k1gFnPc2	precipitace
v	v	k7c6	v
gelu	gel	k1gInSc6	gel
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
16	[number]	k4	16
somatických	somatický	k2eAgInPc2d1	somatický
sérotypů	sérotyp	k1gInPc2	sérotyp
označovaných	označovaný	k2eAgInPc2d1	označovaný
arabskou	arabský	k2eAgFnSc7d1	arabská
číslicí	číslice	k1gFnSc7	číslice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
hostiteli	hostitel	k1gMnSc3	hostitel
i	i	k8xC	i
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
všech	všecek	k3xTgInPc2	všecek
sérotypů	sérotyp	k1gInPc2	sérotyp
byly	být	k5eAaImAgFnP	být
izolovány	izolovat	k5eAaBmNgFnP	izolovat
i	i	k8xC	i
z	z	k7c2	z
ptačích	ptačí	k2eAgMnPc2d1	ptačí
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
pouzderných	pouzderný	k2eAgInPc2d1	pouzderný
typů	typ	k1gInPc2	typ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
stejné	stejný	k2eAgInPc4d1	stejný
termostabilní	termostabilní	k2eAgInPc4d1	termostabilní
tělové	tělový	k2eAgInPc4d1	tělový
antigeny	antigen	k1gInPc4	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Antigeny	antigen	k1gInPc1	antigen
jednoho	jeden	k4xCgInSc2	jeden
kmene	kmen	k1gInSc2	kmen
reagují	reagovat	k5eAaBmIp3nP	reagovat
často	často	k6eAd1	často
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
monotypových	monotypový	k2eAgNnPc2d1	monotypový
sér	sérum	k1gNnPc2	sérum
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
např.	např.	kA	např.
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
sérotypy	sérotyp	k1gInPc1	sérotyp
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
3,4	[number]	k4	3,4
nebo	nebo	k8xC	nebo
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
3,4	[number]	k4	3,4
<g/>
,12	,12	k4	,12
apod.	apod.	kA	apod.
Cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
70-90	[number]	k4	70-90
%	%	kIx~	%
způsobována	způsobován	k2eAgFnSc1d1	způsobována
sérotypy	sérotyp	k1gInPc7	sérotyp
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
1,3	[number]	k4	1,3
anebo	anebo	k8xC	anebo
4	[number]	k4	4
<g/>
;	;	kIx,	;
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
sérotypy	sérotyp	k1gInPc7	sérotyp
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
D	D	kA	D
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
patogenní	patogenní	k2eAgMnPc4d1	patogenní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
a	a	k8xC	a
typizaci	typizace	k1gFnSc4	typizace
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
lze	lze	k6eAd1	lze
také	také	k6eAd1	také
využít	využít	k5eAaPmF	využít
její	její	k3xOp3gFnSc3	její
citlivosti	citlivost	k1gFnSc3	citlivost
k	k	k7c3	k
bakteriofágům	bakteriofág	k1gInPc3	bakteriofág
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
<g/>
,	,	kIx,	,
na	na	k7c6	na
geneticky	geneticky	k6eAd1	geneticky
determinované	determinovaný	k2eAgFnSc3d1	determinovaná
vnímavosti	vnímavost	k1gFnSc3	vnímavost
hostitele	hostitel	k1gMnSc2	hostitel
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
obranných	obranný	k2eAgFnPc6d1	obranná
schopnostech	schopnost	k1gFnPc6	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Pasážováním	Pasážování	k1gNnSc7	Pasážování
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
oslabených	oslabený	k2eAgNnPc6d1	oslabené
zvířatech	zvíře	k1gNnPc6	zvíře
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
virulence	virulence	k1gFnSc2	virulence
původce	původce	k1gMnSc2	původce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
špatné	špatný	k2eAgFnSc6d1	špatná
zoohygieně	zoohygiena	k1gFnSc6	zoohygiena
<g/>
,	,	kIx,	,
po	po	k7c6	po
transportu	transport	k1gInSc6	transport
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
silně	silně	k6eAd1	silně
virulentní	virulentní	k2eAgInPc4d1	virulentní
kmeny	kmen	k1gInPc4	kmen
není	být	k5eNaImIp3nS	být
oslabení	oslabení	k1gNnSc1	oslabení
organismu	organismus	k1gInSc2	organismus
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
infikuje	infikovat	k5eAaBmIp3nS	infikovat
ptáky	pták	k1gMnPc4	pták
přes	přes	k7c4	přes
sliznice	sliznice	k1gFnPc4	sliznice
dutiny	dutina	k1gFnSc2	dutina
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
,	,	kIx,	,
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
spojivku	spojivka	k1gFnSc4	spojivka
oční	oční	k2eAgFnSc4d1	oční
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
kožní	kožní	k2eAgNnSc4d1	kožní
poranění	poranění	k1gNnSc4	poranění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kousnutím	kousnutí	k1gNnSc7	kousnutí
kočky	kočka	k1gFnSc2	kočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pasteurely	Pasteurela	k1gFnPc1	Pasteurela
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
dýchacím	dýchací	k2eAgInSc6d1	dýchací
traktu	trakt	k1gInSc6	trakt
i	i	k9	i
u	u	k7c2	u
klinicky	klinicky	k6eAd1	klinicky
zdravých	zdravý	k2eAgMnPc2d1	zdravý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
nosiči	nosič	k1gMnPc7	nosič
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
porušena	porušen	k2eAgFnSc1d1	porušena
rovnováha	rovnováha	k1gFnSc1	rovnováha
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
hostitel	hostitel	k1gMnSc1	hostitel
-	-	kIx~	-
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pasteurela	pasteurela	k1gFnSc1	pasteurela
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nezvykle	zvykle	k6eNd1	zvykle
rychlá	rychlý	k2eAgFnSc1d1	rychlá
pasáž	pasáž	k1gFnSc1	pasáž
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc4	jeden
hostitele	hostitel	k1gMnSc4	hostitel
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
virulence	virulence	k1gFnSc1	virulence
původce	původce	k1gMnSc2	původce
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
snížená	snížený	k2eAgFnSc1d1	snížená
odolnost	odolnost	k1gFnSc1	odolnost
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Častou	častý	k2eAgFnSc7d1	častá
příčinou	příčina	k1gFnSc7	příčina
snížené	snížený	k2eAgFnSc2d1	snížená
odolnosti	odolnost	k1gFnSc2	odolnost
hostitele	hostitel	k1gMnSc2	hostitel
bývá	bývat	k5eAaImIp3nS	bývat
primární	primární	k2eAgFnSc1d1	primární
virová	virový	k2eAgFnSc1d1	virová
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
kmeny	kmen	k1gInPc4	kmen
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
<g/>
,	,	kIx,	,
produkují	produkovat	k5eAaImIp3nP	produkovat
endotoxin	endotoxin	k1gMnSc1	endotoxin
lipopolysacharidové	lipopolysacharidový	k2eAgFnSc2d1	lipopolysacharidový
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
pro	pro	k7c4	pro
myš	myš	k1gFnSc4	myš
a	a	k8xC	a
pyrogenní	pyrogenní	k2eAgNnSc4d1	pyrogenní
pro	pro	k7c4	pro
králíka	králík	k1gMnSc4	králík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
kulturách	kultura	k1gFnPc6	kultura
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
spontánní	spontánní	k2eAgFnSc7d1	spontánní
lýzou	lýza	k1gFnSc7	lýza
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
tkáních	tkáň	k1gFnPc6	tkáň
zvířat	zvíře	k1gNnPc2	zvíře
s	s	k7c7	s
hemoragickou	hemoragický	k2eAgFnSc7d1	hemoragická
septikémií	septikémie	k1gFnSc7	septikémie
jej	on	k3xPp3gInSc2	on
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
volný	volný	k2eAgInSc1d1	volný
<g/>
;	;	kIx,	;
indukuje	indukovat	k5eAaBmIp3nS	indukovat
aktivní	aktivní	k2eAgFnSc4d1	aktivní
imunitu	imunita	k1gFnSc4	imunita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
kmenů	kmen	k1gInPc2	kmen
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
sérotypu	sérotyp	k1gInSc2	sérotyp
A	A	kA	A
a	a	k8xC	a
D	D	kA	D
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
z	z	k7c2	z
krůt	krůta	k1gFnPc2	krůta
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
produkce	produkce	k1gFnSc1	produkce
termolabilního	termolabilní	k2eAgInSc2d1	termolabilní
proteinového	proteinový	k2eAgInSc2d1	proteinový
toxinu	toxin	k1gInSc2	toxin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
dermonekrotické	dermonekrotický	k2eAgInPc4d1	dermonekrotický
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
letální	letální	k2eAgMnSc1d1	letální
pro	pro	k7c4	pro
krůťata	krůtě	k1gNnPc4	krůtě
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
cholery	cholera	k1gFnSc2	cholera
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
určitou	určitý	k2eAgFnSc7d1	určitá
sezónností	sezónnost	k1gFnSc7	sezónnost
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
a	a	k8xC	a
teplým	teplý	k2eAgNnSc7d1	teplé
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
severskými	severský	k2eAgInPc7d1	severský
státy	stát	k1gInPc7	stát
či	či	k8xC	či
zeměmi	zem	k1gFnPc7	zem
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
predispoziční	predispoziční	k2eAgInPc4d1	predispoziční
faktory	faktor	k1gInPc4	faktor
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
terénních	terénní	k2eAgInPc2d1	terénní
výskytů	výskyt	k1gInPc2	výskyt
cholery	cholera	k1gFnSc2	cholera
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
postihovat	postihovat	k5eAaImF	postihovat
i	i	k9	i
pernatou	pernatý	k2eAgFnSc4d1	pernatá
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
chovy	chov	k1gInPc1	chov
exotů	exot	k1gMnPc2	exot
<g/>
,	,	kIx,	,
pštrosy	pštros	k1gMnPc4	pštros
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
i	i	k9	i
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgMnPc4d1	žijící
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Široký	široký	k2eAgInSc1d1	široký
hostitelský	hostitelský	k2eAgInSc1d1	hostitelský
rozsah	rozsah	k1gInSc1	rozsah
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
vnímavé	vnímavý	k2eAgFnPc1d1	vnímavá
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
postižených	postižený	k2eAgFnPc2d1	postižená
akutní	akutní	k2eAgFnSc7d1	akutní
cholerou	cholera	k1gFnSc7	cholera
obvykle	obvykle	k6eAd1	obvykle
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
králíky	králík	k1gMnPc4	králík
a	a	k8xC	a
myši	myš	k1gFnPc4	myš
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgMnPc1d1	ostatní
laboratorní	laboratorní	k2eAgMnPc1d1	laboratorní
savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
rezistentní	rezistentní	k2eAgFnSc3d1	rezistentní
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
jsou	být	k5eAaImIp3nP	být
refrakterní	refrakterní	k2eAgNnPc1d1	refrakterní
k	k	k7c3	k
orální	orální	k2eAgFnSc3d1	orální
inokulaci	inokulace	k1gFnSc3	inokulace
a	a	k8xC	a
podkožní	podkožní	k2eAgFnSc1d1	podkožní
(	(	kIx(	(
<g/>
sc	sc	k?	sc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
infekce	infekce	k1gFnSc1	infekce
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pouze	pouze	k6eAd1	pouze
lokální	lokální	k2eAgInPc4d1	lokální
abscesy	absces	k1gInPc4	absces
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
uhynout	uhynout	k5eAaPmF	uhynout
po	po	k7c6	po
nitrožilní	nitrožilní	k2eAgFnSc6d1	nitrožilní
(	(	kIx(	(
<g/>
iv	iv	k?	iv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
inokulaci	inokulace	k1gFnSc4	inokulace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
jsou	být	k5eAaImIp3nP	být
nejvnímavější	vnímavý	k2eAgFnPc4d3	nejvnímavější
krůty	krůta	k1gFnPc4	krůta
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc4	kachna
a	a	k8xC	a
husy	husa	k1gFnPc4	husa
<g/>
.	.	kIx.	.
</s>
<s>
Stresující	stresující	k2eAgInPc1d1	stresující
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
výživy	výživa	k1gFnSc2	výživa
<g/>
,	,	kIx,	,
zhoršené	zhoršený	k2eAgNnSc1d1	zhoršené
chovatelské	chovatelský	k2eAgNnSc1d1	chovatelské
prostředí	prostředí	k1gNnSc1	prostředí
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
negativně	negativně	k6eAd1	negativně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
výskyt	výskyt	k1gInSc4	výskyt
i	i	k8xC	i
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
krůtami	krůta	k1gFnPc7	krůta
je	být	k5eAaImIp3nS	být
kur	kur	k1gMnSc1	kur
domácí	domácí	k2eAgMnSc1d1	domácí
méně	málo	k6eAd2	málo
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dospělí	dospělý	k2eAgMnPc1d1	dospělý
kohouti	kohout	k1gMnPc1	kohout
a	a	k8xC	a
slepice	slepice	k1gFnPc1	slepice
jsou	být	k5eAaImIp3nP	být
vnímavější	vnímavý	k2eAgNnPc1d2	vnímavější
než	než	k8xS	než
kuřata	kuře	k1gNnPc1	kuře
a	a	k8xC	a
mladá	mladý	k2eAgFnSc1d1	mladá
drůbeže	drůbež	k1gFnPc1	drůbež
<g/>
;	;	kIx,	;
většinou	většinou	k6eAd1	většinou
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
až	až	k9	až
po	po	k7c4	po
16	[number]	k4	16
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
terénních	terénní	k2eAgInPc6d1	terénní
výskytech	výskyt	k1gInPc6	výskyt
cholery	cholera	k1gFnSc2	cholera
u	u	k7c2	u
slepic	slepice	k1gFnPc2	slepice
se	se	k3xPyFc4	se
mortalita	mortalita	k1gFnSc1	mortalita
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
0-20	[number]	k4	0-20
%	%	kIx~	%
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
pokles	pokles	k1gInSc1	pokles
snášky	snáška	k1gFnSc2	snáška
a	a	k8xC	a
perzistující	perzistující	k2eAgFnSc2d1	perzistující
lokalizované	lokalizovaný	k2eAgFnSc2d1	lokalizovaná
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
i	i	k9	i
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
působí	působit	k5eAaImIp3nP	působit
stresory	stresor	k1gInPc1	stresor
prostředí	prostředí	k1gNnSc2	prostředí
jako	jako	k8xC	jako
predispoziční	predispoziční	k2eAgInPc1d1	predispoziční
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Cholera	cholera	k1gFnSc1	cholera
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
pštrosů	pštros	k1gMnPc2	pštros
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Morishita	Morishita	k1gFnSc1	Morishita
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
vyšetřovali	vyšetřovat	k5eAaImAgMnP	vyšetřovat
kultivačně	kultivačně	k6eAd1	kultivačně
na	na	k7c6	na
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
253	[number]	k4	253
klinicky	klinicky	k6eAd1	klinicky
zdravých	zdravý	k2eAgMnPc2d1	zdravý
a	a	k8xC	a
75	[number]	k4	75
nemocných	nemocný	k2eAgMnPc2d1	nemocný
papoušků	papoušek	k1gMnPc2	papoušek
různých	různý	k2eAgInPc2d1	různý
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
5	[number]	k4	5
papoušků	papoušek	k1gMnPc2	papoušek
(	(	kIx(	(
<g/>
Pionites	Pionites	k1gInSc1	Pionites
leucogaster	leucogastra	k1gFnPc2	leucogastra
xanthomeria	xanthomerium	k1gNnSc2	xanthomerium
<g/>
,	,	kIx,	,
Coracopsis	Coracopsis	k1gFnSc2	Coracopsis
nigra	nigr	k1gMnSc4	nigr
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Psittacus	Psittacus	k1gMnSc1	Psittacus
erithacus	erithacus	k1gMnSc1	erithacus
a	a	k8xC	a
Nymphicus	Nymphicus	k1gMnSc1	Nymphicus
hollandicus	hollandicus	k1gMnSc1	hollandicus
<g/>
)	)	kIx)	)
vyizolovali	vyizolovat	k5eAaImAgMnP	vyizolovat
pasteurelu	pasteurel	k1gInSc2	pasteurel
sérotypu	sérotyp	k1gInSc2	sérotyp
3	[number]	k4	3
(	(	kIx(	(
<g/>
papoušci	papoušek	k1gMnPc1	papoušek
se	s	k7c7	s
septikémií	septikémie	k1gFnSc7	septikémie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sérotypu	sérotyp	k1gInSc2	sérotyp
4,7	[number]	k4	4,7
(	(	kIx(	(
<g/>
papoušci	papoušek	k1gMnPc1	papoušek
s	s	k7c7	s
pododermatitidou	pododermatitida	k1gFnSc7	pododermatitida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izolát	izolát	k1gInSc1	izolát
z	z	k7c2	z
korely	korela	k1gFnSc2	korela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ukořistěna	ukořistit	k5eAaPmNgFnS	ukořistit
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
sérotypu	sérotyp	k1gInSc3	sérotyp
3	[number]	k4	3
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
geneticky	geneticky	k6eAd1	geneticky
příbuzný	příbuzný	k1gMnSc1	příbuzný
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
2	[number]	k4	2
izoláty	izolát	k1gInPc4	izolát
stejného	stejný	k2eAgInSc2d1	stejný
sérotypu	sérotyp	k1gInSc2	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Stejní	stejný	k2eAgMnPc1d1	stejný
autoři	autor	k1gMnPc1	autor
zjistili	zjistit	k5eAaPmAgMnP	zjistit
choleru	cholera	k1gFnSc4	cholera
u	u	k7c2	u
8	[number]	k4	8
případů	případ	k1gInPc2	případ
z	z	k7c2	z
398	[number]	k4	398
vyšetřovaných	vyšetřovaný	k2eAgMnPc2d1	vyšetřovaný
dravců	dravec	k1gMnPc2	dravec
z	z	k7c2	z
řádů	řád	k1gInPc2	řád
Falconiformes	Falconiformesa	k1gFnPc2	Falconiformesa
a	a	k8xC	a
Strigiformes	Strigiformesa	k1gFnPc2	Strigiformesa
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
izolátů	izolát	k1gInPc2	izolát
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
patřilo	patřit	k5eAaImAgNnS	patřit
do	do	k7c2	do
sérotypu	sérotyp	k1gInSc2	sérotyp
1	[number]	k4	1
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgInPc4d1	zbývající
dva	dva	k4xCgInPc4	dva
do	do	k7c2	do
sérotypů	sérotyp	k1gInPc2	sérotyp
3	[number]	k4	3
a	a	k8xC	a
3,4	[number]	k4	3,4
<g/>
.	.	kIx.	.
</s>
<s>
Choleru	cholera	k1gFnSc4	cholera
také	také	k9	také
prokázali	prokázat	k5eAaPmAgMnP	prokázat
u	u	k7c2	u
káně	káně	k1gFnSc2	káně
rousné	rousný	k2eAgFnSc2d1	rousná
(	(	kIx(	(
<g/>
Buteo	Buteo	k6eAd1	Buteo
lagopus	lagopus	k1gInSc1	lagopus
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrečka	výreček	k1gMnSc2	výreček
plaménkového	plaménkový	k2eAgMnSc2d1	plaménkový
(	(	kIx(	(
<g/>
Otus	Otus	k1gInSc1	Otus
flammeolus	flammeolus	k1gInSc1	flammeolus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc1d1	projevující
se	se	k3xPyFc4	se
abscesy	absces	k1gInPc7	absces
v	v	k7c6	v
orofaryngu	orofaryng	k1gInSc6	orofaryng
a	a	k8xC	a
jícnu	jícen	k1gInSc6	jícen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
holubů	holub	k1gMnPc2	holub
se	se	k3xPyFc4	se
cholera	cholera	k1gFnSc1	cholera
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
drůbeže	drůbež	k1gFnSc2	drůbež
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
;	;	kIx,	;
holubi	holub	k1gMnPc1	holub
ani	ani	k8xC	ani
při	při	k7c6	při
úzkém	úzký	k2eAgInSc6d1	úzký
a	a	k8xC	a
intenzivním	intenzivní	k2eAgInSc6d1	intenzivní
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
postiženou	postižený	k2eAgFnSc7d1	postižená
drůbeží	drůbež	k1gFnSc7	drůbež
neonemocní	onemocnět	k5eNaPmIp3nS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
normální	normální	k2eAgFnSc3d1	normální
mikroflóře	mikroflóra	k1gFnSc3	mikroflóra
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přítomna	přítomen	k2eAgFnSc1d1	přítomna
u	u	k7c2	u
chronicky	chronicky	k6eAd1	chronicky
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
bakterionosiči	bakterionosič	k1gMnPc7	bakterionosič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
vykazovali	vykazovat	k5eAaImAgMnP	vykazovat
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
přežívající	přežívající	k2eAgFnSc4d1	přežívající
choleru	cholera	k1gFnSc4	cholera
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
rezervoárem	rezervoár	k1gInSc7	rezervoár
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
kadávery	kadáver	k1gInPc1	kadáver
ptáků	pták	k1gMnPc2	pták
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
na	na	k7c4	na
akutní	akutní	k2eAgFnSc4d1	akutní
choleru	cholera	k1gFnSc4	cholera
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
kanibalismu	kanibalismus	k1gInSc6	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálními	potenciální	k2eAgInPc7d1	potenciální
nosiči	nosič	k1gInPc7	nosič
nebo	nebo	k8xC	nebo
vektory	vektor	k1gInPc7	vektor
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
vrabci	vrabec	k1gMnPc1	vrabec
<g/>
,	,	kIx,	,
holubi	holub	k1gMnPc1	holub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnSc2	krysa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
bakterie	bakterie	k1gFnPc4	bakterie
pro	pro	k7c4	pro
drůbež	drůbež	k1gFnSc4	drůbež
avirulentní	avirulentní	k2eAgFnSc4d1	avirulentní
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
izolátů	izolát	k1gInPc2	izolát
od	od	k7c2	od
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
možná	možná	k9	možná
také	také	k9	také
od	od	k7c2	od
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
infekčního	infekční	k2eAgInSc2d1	infekční
řetězce	řetězec	k1gInSc2	řetězec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
mouchy	moucha	k1gFnPc4	moucha
<g/>
,	,	kIx,	,
klíšťáci	klíšťák	k1gMnPc1	klíšťák
<g/>
,	,	kIx,	,
čmelík	čmelík	k1gMnSc1	čmelík
kuří	kuří	k2eAgMnSc1d1	kuří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
nebo	nebo	k8xC	nebo
aerogenně	aerogenně	k6eAd1	aerogenně
<g/>
.	.	kIx.	.
</s>
<s>
Pasteurely	Pasteurel	k1gInPc1	Pasteurel
jsou	být	k5eAaImIp3nP	být
rozšiřovány	rozšiřován	k2eAgInPc1d1	rozšiřován
exkrety	exkret	k1gInPc1	exkret
z	z	k7c2	z
dutiny	dutina	k1gFnSc2	dutina
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
spojivky	spojivka	k1gFnSc2	spojivka
nemocných	mocný	k2eNgMnPc2d1	nemocný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
je	být	k5eAaImIp3nS	být
kontaminováno	kontaminován	k2eAgNnSc1d1	kontaminováno
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
krmivo	krmivo	k1gNnSc1	krmivo
a	a	k8xC	a
pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Trus	trus	k1gInSc1	trus
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
živé	živý	k2eAgFnSc2d1	živá
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Asymptomní	Asymptomný	k2eAgMnPc1d1	Asymptomný
nosiči	nosič	k1gMnPc1	nosič
přechovávají	přechovávat	k5eAaImIp3nP	přechovávat
bakterie	bakterie	k1gFnPc4	bakterie
v	v	k7c6	v
nosních	nosní	k2eAgFnPc6d1	nosní
dutinách	dutina	k1gFnPc6	dutina
<g/>
,	,	kIx,	,
sinusech	sinus	k1gInPc6	sinus
a	a	k8xC	a
choanách	choana	k1gFnPc6	choana
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
nepřímo	přímo	k6eNd1	přímo
živými	živý	k2eAgInPc7d1	živý
a	a	k8xC	a
neživými	živý	k2eNgInPc7d1	neživý
kontaminovanými	kontaminovaný	k2eAgInPc7d1	kontaminovaný
vektory	vektor	k1gInPc7	vektor
(	(	kIx(	(
<g/>
personál	personál	k1gInSc1	personál
<g/>
,	,	kIx,	,
pytle	pytel	k1gInPc1	pytel
od	od	k7c2	od
krmení	krmení	k1gNnSc2	krmení
<g/>
,	,	kIx,	,
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
,	,	kIx,	,
transportní	transportní	k2eAgInPc4d1	transportní
prostředky	prostředek	k1gInPc4	prostředek
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
vejcem	vejce	k1gNnSc7	vejce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
exaktně	exaktně	k6eAd1	exaktně
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
sliznice	sliznice	k1gFnPc1	sliznice
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
a	a	k8xC	a
hltanu	hltan	k1gInSc6	hltan
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
poranění	poranění	k1gNnSc1	poranění
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
virulentní	virulentní	k2eAgInPc4d1	virulentní
kmeny	kmen	k1gInPc4	kmen
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
akutní	akutní	k2eAgFnSc6d1	akutní
septikémii	septikémie	k1gFnSc6	septikémie
s	s	k7c7	s
následnými	následný	k2eAgFnPc7d1	následná
koagulopatiemi	koagulopatie	k1gFnPc7	koagulopatie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uvolňovaný	uvolňovaný	k2eAgInSc1d1	uvolňovaný
endotoxin	endotoxin	k1gInSc1	endotoxin
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
náhlý	náhlý	k2eAgInSc1d1	náhlý
úhyn	úhyn	k1gInSc1	úhyn
(	(	kIx(	(
<g/>
šok	šok	k1gInSc1	šok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
bakteriémii	bakteriémie	k1gFnSc4	bakteriémie
a	a	k8xC	a
kolonizaci	kolonizace	k1gFnSc4	kolonizace
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
sleziny	slezina	k1gFnSc2	slezina
a	a	k8xC	a
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Slabě	slabě	k6eAd1	slabě
virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
obecně	obecně	k6eAd1	obecně
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
chronické	chronický	k2eAgNnSc4d1	chronické
onemocnění	onemocnění	k1gNnSc4	onemocnění
s	s	k7c7	s
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Endotoxiny	Endotoxin	k1gInPc1	Endotoxin
produkované	produkovaný	k2eAgInPc1d1	produkovaný
pasteurelami	pasteurela	k1gFnPc7	pasteurela
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
krevní	krevní	k2eAgFnSc1d1	krevní
cévy	céva	k1gFnPc1	céva
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
edém	edém	k1gInSc4	edém
<g/>
,	,	kIx,	,
hemoragie	hemoragie	k1gFnPc4	hemoragie
a	a	k8xC	a
koagulační	koagulační	k2eAgFnSc4d1	koagulační
nekrózu	nekróza	k1gFnSc4	nekróza
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
méně	málo	k6eAd2	málo
virulentními	virulentní	k2eAgInPc7d1	virulentní
kmeny	kmen	k1gInPc7	kmen
obvykle	obvykle	k6eAd1	obvykle
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
po	po	k7c6	po
stresech	stres	k1gInPc6	stres
nebo	nebo	k8xC	nebo
u	u	k7c2	u
imunosupresovaného	imunosupresovaný	k2eAgMnSc2d1	imunosupresovaný
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
až	až	k6eAd1	až
9	[number]	k4	9
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Cholera	cholera	k1gFnSc1	cholera
drůbeže	drůbež	k1gFnSc2	drůbež
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
perakutně	perakutně	k6eAd1	perakutně
<g/>
,	,	kIx,	,
akutně	akutně	k6eAd1	akutně
nebo	nebo	k8xC	nebo
chronicky	chronicky	k6eAd1	chronicky
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
perakutním	perakutní	k2eAgInSc6d1	perakutní
průběhu	průběh	k1gInSc6	průběh
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
příznakem	příznak	k1gInSc7	příznak
náhlé	náhlý	k2eAgInPc4d1	náhlý
úhyny	úhyn	k1gInPc4	úhyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
mohou	moct	k5eAaImIp3nP	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
i	i	k9	i
50	[number]	k4	50
%	%	kIx~	%
v	v	k7c6	v
postiženém	postižený	k2eAgInSc6d1	postižený
chovu	chov	k1gInSc6	chov
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgInSc1d1	akutní
průběh	průběh	k1gInSc1	průběh
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
morbiditou	morbidita	k1gFnSc7	morbidita
i	i	k8xC	i
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
úhyn	úhyn	k1gInSc1	úhyn
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jediným	jediný	k2eAgInSc7d1	jediný
příznakem	příznak	k1gInSc7	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
ale	ale	k9	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úhynům	úhyn	k1gInPc3	úhyn
po	po	k7c4	po
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
<g/>
denním	denní	k2eAgNnSc6d1	denní
onemocnění	onemocnění	k1gNnSc6	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Možnými	možný	k2eAgInPc7d1	možný
příznaky	příznak	k1gInPc7	příznak
jsou	být	k5eAaImIp3nP	být
deprese	deprese	k1gFnSc2	deprese
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
anorexie	anorexie	k1gFnSc1	anorexie
<g/>
,	,	kIx,	,
načepýřené	načepýřený	k2eAgNnSc1d1	načepýřené
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
mukózní	mukózní	k2eAgInPc1d1	mukózní
výtoky	výtok	k1gInPc1	výtok
ze	z	k7c2	z
zobáku	zobák	k1gInSc2	zobák
a	a	k8xC	a
nozder	nozdra	k1gFnPc2	nozdra
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
zrychlené	zrychlený	k2eAgNnSc4d1	zrychlené
a	a	k8xC	a
ztížené	ztížený	k2eAgNnSc4d1	ztížené
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
cyanóza	cyanóza	k1gFnSc1	cyanóza
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
viditelná	viditelný	k2eAgFnSc1d1	viditelná
na	na	k7c6	na
neopeřených	opeřený	k2eNgFnPc6d1	neopeřená
částech	část	k1gFnPc6	část
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
hřeben	hřeben	k1gInSc1	hřeben
a	a	k8xC	a
lalůčky	lalůček	k1gInPc1	lalůček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průjmové	průjmový	k2eAgInPc1d1	průjmový
výkaly	výkal	k1gInPc1	výkal
jsou	být	k5eAaImIp3nP	být
zpočátku	zpočátku	k6eAd1	zpočátku
vodnaté	vodnatý	k2eAgFnPc1d1	vodnatá
a	a	k8xC	a
bělavé	bělavý	k2eAgFnPc1d1	bělavá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazelenalé	nazelenalý	k2eAgFnPc1d1	nazelenalá
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hlen	hlen	k1gInSc4	hlen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
plovací	plovací	k2eAgInPc1d1	plovací
pohyby	pohyb	k1gInPc1	pohyb
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přežijí	přežít	k5eAaPmIp3nP	přežít
počáteční	počáteční	k2eAgFnSc4d1	počáteční
akutní	akutní	k2eAgFnSc4d1	akutní
septikémii	septikémie	k1gFnSc4	septikémie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
respirační	respirační	k2eAgInPc4d1	respirační
šelesty	šelest	k1gInPc4	šelest
<g/>
,	,	kIx,	,
konjunktivitidu	konjunktivitida	k1gFnSc4	konjunktivitida
nebo	nebo	k8xC	nebo
zduření	zduření	k1gNnSc4	zduření
podočnicových	podočnicový	k2eAgFnPc2d1	podočnicový
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mohou	moct	k5eAaImIp3nP	moct
později	pozdě	k6eAd2	pozdě
uhynout	uhynout	k5eAaPmF	uhynout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zhoršeného	zhoršený	k2eAgInSc2d1	zhoršený
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
vyhublosti	vyhublost	k1gFnSc2	vyhublost
a	a	k8xC	a
dehydratace	dehydratace	k1gFnSc2	dehydratace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
chronicky	chronicky	k6eAd1	chronicky
nemocnými	nemocný	k2eAgInPc7d1	nemocný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
i	i	k9	i
uzdravit	uzdravit	k5eAaPmF	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
forma	forma	k1gFnSc1	forma
cholery	cholera	k1gFnSc2	cholera
obvykle	obvykle	k6eAd1	obvykle
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
akutním	akutní	k2eAgNnSc6d1	akutní
stadiu	stadion	k1gNnSc6	stadion
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
infekce	infekce	k1gFnSc2	infekce
P.	P.	kA	P.
multocida	multocid	k1gMnSc2	multocid
o	o	k7c6	o
nízké	nízký	k2eAgFnSc6d1	nízká
virulenci	virulence	k1gFnSc6	virulence
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
1-2	[number]	k4	1-2
týdny	týden	k1gInPc4	týden
i	i	k8xC	i
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
poruchami	porucha	k1gFnPc7	porucha
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
poklesem	pokles	k1gInSc7	pokles
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
i	i	k8xC	i
změny	změna	k1gFnPc1	změna
většinou	většina	k1gFnSc7	většina
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
lokalizaci	lokalizace	k1gFnSc4	lokalizace
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
se	se	k3xPyFc4	se
zduření	zduření	k1gNnSc1	zduření
lalůčků	lalůček	k1gInPc2	lalůček
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
lalůčková	lalůčkový	k2eAgFnSc1d1	lalůčkový
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sinusů	sinus	k1gInPc2	sinus
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
končetin	končetina	k1gFnPc2	končetina
nebo	nebo	k8xC	nebo
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
prstních	prstní	k2eAgInPc2d1	prstní
polštářků	polštářek	k1gInPc2	polštářek
nebo	nebo	k8xC	nebo
sternální	sternální	k2eAgFnSc2d1	sternální
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nahromadění	nahromadění	k1gNnSc2	nahromadění
fibrinózního	fibrinózní	k2eAgInSc2d1	fibrinózní
exsudátu	exsudát	k1gInSc2	exsudát
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
exsudativní	exsudativní	k2eAgFnSc1d1	exsudativní
konjunktivitida	konjunktivitida	k1gFnSc1	konjunktivitida
a	a	k8xC	a
faryngitida	faryngitida	k1gFnSc1	faryngitida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
infekce	infekce	k1gFnSc2	infekce
plen	plena	k1gFnPc2	plena
mozkových	mozkový	k2eAgFnPc2d1	mozková
<g/>
,	,	kIx,	,
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
nebo	nebo	k8xC	nebo
lebečních	lebeční	k2eAgFnPc2d1	lebeční
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
tortikolis	tortikolis	k1gFnSc1	tortikolis
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
ztíženým	ztížený	k2eAgNnSc7d1	ztížené
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
,	,	kIx,	,
tracheálními	tracheální	k2eAgInPc7d1	tracheální
šelesty	šelest	k1gInPc7	šelest
a	a	k8xC	a
hlenovitým	hlenovitý	k2eAgInSc7d1	hlenovitý
výtokem	výtok	k1gInSc7	výtok
ze	z	k7c2	z
zobáku	zobák	k1gInSc2	zobák
a	a	k8xC	a
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Chronicky	chronicky	k6eAd1	chronicky
nemocná	mocný	k2eNgFnSc1d1	mocný
drůbež	drůbež	k1gFnSc1	drůbež
může	moct	k5eAaImIp3nS	moct
uhynout	uhynout	k5eAaPmF	uhynout
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
asymptomním	asymptomní	k2eAgInSc7d1	asymptomní
nosičem	nosič	k1gInSc7	nosič
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
uzdravit	uzdravit	k5eAaPmF	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
sov	sova	k1gFnPc2	sova
a	a	k8xC	a
holubů	holub	k1gMnPc2	holub
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
granulomatózní	granulomatózní	k2eAgFnSc1d1	granulomatózní
dermatitida	dermatitida	k1gFnSc1	dermatitida
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopické	makroskopický	k2eAgFnPc1d1	makroskopická
změny	změna	k1gFnPc1	změna
při	při	k7c6	při
choleře	cholera	k1gFnSc6	cholera
nejsou	být	k5eNaImIp3nP	být
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
vnímavosti	vnímavost	k1gFnSc2	vnímavost
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
perakutním	perakutní	k2eAgInSc6d1	perakutní
průběhu	průběh	k1gInSc6	průběh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
i	i	k9	i
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgFnSc4d1	akutní
formu	forma	k1gFnSc4	forma
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
zejména	zejména	k9	zejména
četné	četný	k2eAgFnPc1d1	četná
krváceniny	krvácenina	k1gFnPc1	krvácenina
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
orgánech	orgán	k1gInPc6	orgán
a	a	k8xC	a
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
mnohočetné	mnohočetný	k2eAgFnSc2d1	mnohočetná
tečkovité	tečkovitý	k2eAgFnSc2d1	tečkovitá
nekrózy	nekróza	k1gFnSc2	nekróza
ve	v	k7c6	v
zduřelých	zduřelý	k2eAgNnPc6d1	zduřelé
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
hydroperikard	hydroperikard	k1gMnSc1	hydroperikard
<g/>
,	,	kIx,	,
ascites	ascites	k1gMnSc1	ascites
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
bývají	bývat	k5eAaImIp3nP	bývat
postižena	postižen	k2eAgNnPc1d1	postiženo
i	i	k8xC	i
ovaria	ovarium	k1gNnPc1	ovarium
<g/>
.	.	kIx.	.
</s>
<s>
Zralé	zralý	k2eAgInPc1d1	zralý
folikuly	folikul	k1gInPc1	folikul
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
svraštělé	svraštělý	k2eAgFnPc1d1	svraštělá
a	a	k8xC	a
ochablé	ochablý	k2eAgFnPc1d1	ochablá
<g/>
,	,	kIx,	,
thekální	thekální	k2eAgFnPc1d1	thekální
krevní	krevní	k2eAgFnPc1d1	krevní
cévy	céva	k1gFnPc1	céva
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
peritoneální	peritoneální	k2eAgFnSc6d1	peritoneální
dutině	dutina	k1gFnSc6	dutina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
žloutek	žloutek	k1gInSc1	žloutek
z	z	k7c2	z
prasklých	prasklý	k2eAgInPc2d1	prasklý
folikulů	folikul	k1gInPc2	folikul
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
forma	forma	k1gFnSc1	forma
cholery	cholera	k1gFnSc2	cholera
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
projevuje	projevovat	k5eAaImIp3nS	projevovat
lokalizovanými	lokalizovaný	k2eAgFnPc7d1	lokalizovaná
infekcemi	infekce	k1gFnPc7	infekce
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
septikemické	septikemický	k2eAgFnSc2d1	septikemický
povahy	povaha	k1gFnSc2	povaha
akutní	akutní	k2eAgFnSc2d1	akutní
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnPc1d1	lokální
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
katarálním	katarální	k2eAgInSc7d1	katarální
až	až	k6eAd1	až
fibrinózně	fibrinózně	k6eAd1	fibrinózně
purulentním	purulentní	k2eAgInSc7d1	purulentní
zánětem	zánět	k1gInSc7	zánět
a	a	k8xC	a
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
nekrózy	nekróza	k1gFnSc2	nekróza
a	a	k8xC	a
fibroplazie	fibroplazie	k1gFnSc2	fibroplazie
v	v	k7c6	v
infikovaných	infikovaný	k2eAgInPc6d1	infikovaný
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Postiženy	postižen	k2eAgInPc1d1	postižen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
lalůčky	lalůček	k1gInPc4	lalůček
(	(	kIx(	(
<g/>
lalůčková	lalůčkový	k2eAgFnSc1d1	lalůčkový
choroba	choroba	k1gFnSc1	choroba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
(	(	kIx(	(
<g/>
osteomyelitida	osteomyelitida	k1gFnSc1	osteomyelitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klouby	kloub	k1gInPc1	kloub
(	(	kIx(	(
<g/>
artritida	artritida	k1gFnSc1	artritida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochvy	pochva	k1gFnSc2	pochva
šlachové	šlachový	k2eAgInPc1d1	šlachový
<g/>
,	,	kIx,	,
prstní	prstní	k2eAgInPc1d1	prstní
polštářky	polštářek	k1gInPc1	polštářek
<g/>
,	,	kIx,	,
sternální	sternální	k2eAgFnSc1d1	sternální
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
granulomatózní	granulomatózní	k2eAgFnSc1d1	granulomatózní
dermatitida	dermatitida	k1gFnSc1	dermatitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
peritoneální	peritoneální	k2eAgFnSc4d1	peritoneální
dutinu	dutina	k1gFnSc4	dutina
(	(	kIx(	(
<g/>
exsudativní	exsudativní	k2eAgFnSc1d1	exsudativní
serozitida	serozitida	k1gFnSc1	serozitida
<g/>
,	,	kIx,	,
žloutková	žloutkový	k2eAgFnSc1d1	žloutková
peritonitida	peritonitida	k1gFnSc1	peritonitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vaječník	vaječník	k1gInSc1	vaječník
a	a	k8xC	a
vejcovod	vejcovod	k1gInSc1	vejcovod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
CNS	CNS	kA	CNS
byly	být	k5eAaImAgFnP	být
zjištěny	zjištěn	k2eAgFnPc4d1	zjištěna
infekce	infekce	k1gFnPc4	infekce
plen	plena	k1gFnPc2	plena
mozkových	mozkový	k2eAgFnPc2d1	mozková
<g/>
,	,	kIx,	,
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
(	(	kIx(	(
<g/>
otitis	otitis	k1gFnSc1	otitis
media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
lebeční	lebeční	k2eAgFnSc2d1	lebeční
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
slezině	slezina	k1gFnSc6	slezina
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
granulomy	granulom	k1gInPc1	granulom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
difteroidní	difteroidní	k2eAgFnSc1d1	difteroidní
enteritida	enteritida	k1gFnSc1	enteritida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
hostitele	hostitel	k1gMnSc2	hostitel
před	před	k7c7	před
pasteurelou	pasteurela	k1gFnSc7	pasteurela
<g/>
,	,	kIx,	,
typickým	typický	k2eAgMnSc7d1	typický
extracelulárním	extracelulární	k2eAgMnSc7d1	extracelulární
parazitem	parazit	k1gMnSc7	parazit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
především	především	k9	především
polymorfonukleární	polymorfonukleární	k2eAgInPc4d1	polymorfonukleární
leukocyty	leukocyt	k1gInPc4	leukocyt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
imunního	imunní	k2eAgNnSc2d1	imunní
zvířete	zvíře	k1gNnSc2	zvíře
je	být	k5eAaImIp3nS	být
inhibováno	inhibován	k2eAgNnSc4d1	inhibován
rychlé	rychlý	k2eAgNnSc4d1	rychlé
šíření	šíření	k1gNnSc4	šíření
pasteurel	pasteurela	k1gFnPc2	pasteurela
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
a	a	k8xC	a
monocyto-makrofágového	monocytoakrofágový	k2eAgInSc2d1	monocyto-makrofágový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Imunizace	imunizace	k1gFnSc1	imunizace
kuřat	kuře	k1gNnPc2	kuře
proti	proti	k7c3	proti
choleře	cholera	k1gFnSc3	cholera
navržená	navržený	k2eAgFnSc1d1	navržená
Pasteurem	Pasteur	k1gMnSc7	Pasteur
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvých	prvý	k4xOgInPc2	prvý
imunizačních	imunizační	k2eAgInPc2d1	imunizační
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
imunogenních	imunogenní	k2eAgFnPc2d1	imunogenní
avirulentních	avirulentní	k2eAgFnPc2d1	avirulentní
mutant	mutant	k1gMnSc1	mutant
<g/>
.	.	kIx.	.
</s>
<s>
Imunogenní	Imunogenní	k2eAgFnSc1d1	Imunogenní
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
bakteriální	bakteriální	k2eAgNnSc4d1	bakteriální
pouzdro	pouzdro	k1gNnSc4	pouzdro
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
teplem	teplo	k1gNnSc7	teplo
inaktivované	inaktivovaný	k2eAgFnSc2d1	inaktivovaná
bujónové	bujónový	k2eAgFnSc2d1	bujónová
vakcíny	vakcína	k1gFnSc2	vakcína
zcela	zcela	k6eAd1	zcela
neúčinné	účinný	k2eNgFnPc1d1	neúčinná
<g/>
.	.	kIx.	.
</s>
<s>
Předběžná	předběžný	k2eAgFnSc1d1	předběžná
diagnóza	diagnóza	k1gFnSc1	diagnóza
cholery	cholera	k1gFnSc2	cholera
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
anamnestických	anamnestický	k2eAgInPc2d1	anamnestický
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
pitevního	pitevní	k2eAgInSc2d1	pitevní
nálezu	nález	k1gInSc2	nález
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nálezem	nález	k1gInSc7	nález
bipolárních	bipolární	k2eAgFnPc2d1	bipolární
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
obarvených	obarvený	k2eAgInPc6d1	obarvený
preparátech	preparát	k1gInPc6	preparát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konečnou	konečný	k2eAgFnSc4d1	konečná
diagnózu	diagnóza	k1gFnSc4	diagnóza
je	být	k5eAaImIp3nS	být
však	však	k9	však
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
laboratorní	laboratorní	k2eAgNnSc1d1	laboratorní
vyšetření	vyšetření	k1gNnSc1	vyšetření
(	(	kIx(	(
<g/>
kultivační	kultivační	k2eAgInSc1d1	kultivační
průkaz	průkaz	k1gInSc1	průkaz
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
perakutním	perakutní	k2eAgInSc6d1	perakutní
až	až	k8xS	až
akutním	akutní	k2eAgInSc6d1	akutní
průběhu	průběh	k1gInSc6	průběh
cholery	cholera	k1gFnSc2	cholera
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
klinicky	klinicky	k6eAd1	klinicky
i	i	k9	i
pitevně	pitevně	k6eAd1	pitevně
odlišit	odlišit	k5eAaPmF	odlišit
především	především	k9	především
vysokopatogenní	vysokopatogenní	k2eAgFnSc4d1	vysokopatogenní
formu	forma	k1gFnSc4	forma
influenzy	influenza	k1gFnSc2	influenza
A	A	kA	A
<g/>
,	,	kIx,	,
Newcastleskou	Newcastleský	k2eAgFnSc4d1	Newcastleská
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
septikemicky	septikemicky	k6eAd1	septikemicky
probíhající	probíhající	k2eAgFnSc4d1	probíhající
pseudotuberkulózu	pseudotuberkulóza	k1gFnSc4	pseudotuberkulóza
a	a	k8xC	a
červenku	červenka	k1gFnSc4	červenka
<g/>
,	,	kIx,	,
salmonelózy	salmonelóza	k1gFnPc4	salmonelóza
<g/>
,	,	kIx,	,
streptokokózy	streptokokóza	k1gFnPc4	streptokokóza
a	a	k8xC	a
stafylokokózy	stafylokokóza	k1gFnPc4	stafylokokóza
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
další	další	k2eAgFnPc4d1	další
nemoci	nemoc	k1gFnPc4	nemoc
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
nebo	nebo	k8xC	nebo
nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
hemofilová	hemofilová	k1gFnSc1	hemofilová
rýma	rýma	k1gFnSc1	rýma
<g/>
,	,	kIx,	,
herpesvirová	herpesvirový	k2eAgFnSc1d1	herpesvirový
enteritida	enteritida	k1gFnSc1	enteritida
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
infekční	infekční	k2eAgFnSc1d1	infekční
synovitida	synovitida	k1gFnSc1	synovitida
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
cholery	cholera	k1gFnSc2	cholera
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Septikémií	septikémie	k1gFnSc7	septikémie
postižená	postižený	k2eAgNnPc1d1	postižené
zvířata	zvíře	k1gNnPc1	zvíře
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
záchranu	záchrana	k1gFnSc4	záchrana
injekční	injekční	k2eAgFnSc7d1	injekční
aplikací	aplikace	k1gFnSc7	aplikace
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působících	působící	k2eAgInPc2d1	působící
sulfonamidů	sulfonamid	k1gInPc2	sulfonamid
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnPc4	aplikace
sulfonamidů	sulfonamid	k1gInPc2	sulfonamid
nebo	nebo	k8xC	nebo
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
v	v	k7c6	v
krmivu	krmivo	k1gNnSc6	krmivo
<g/>
;	;	kIx,	;
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
léčby	léčba	k1gFnSc2	léčba
ale	ale	k8xC	ale
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
výskytu	výskyt	k1gInSc3	výskyt
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytná	zbytný	k2eNgFnSc1d1	zbytný
je	být	k5eAaImIp3nS	být
testace	testace	k1gFnSc1	testace
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kmeny	kmen	k1gInPc1	kmen
P.	P.	kA	P.
multocida	multocid	k1gMnSc4	multocid
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
citlivost	citlivost	k1gFnSc4	citlivost
na	na	k7c4	na
léky	lék	k1gInPc4	lék
a	a	k8xC	a
při	při	k7c6	při
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
terapii	terapie	k1gFnSc6	terapie
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
rezistence	rezistence	k1gFnSc1	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
také	také	k9	také
kombinace	kombinace	k1gFnSc1	kombinace
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
s	s	k7c7	s
hyperimunním	hyperimunní	k2eAgNnSc7d1	hyperimunní
sérem	sérum	k1gNnSc7	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
chronickou	chronický	k2eAgFnSc7d1	chronická
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ireversibilním	ireversibilní	k2eAgFnPc3d1	ireversibilní
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
parenchymatózních	parenchymatózní	k2eAgInPc6d1	parenchymatózní
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
případech	případ	k1gInPc6	případ
výskytu	výskyt	k1gInSc2	výskyt
cholery	cholera	k1gFnSc2	cholera
je	být	k5eAaImIp3nS	být
také	také	k9	také
nutné	nutný	k2eAgNnSc1d1	nutné
zvážit	zvážit	k5eAaPmF	zvážit
epizootologickou	epizootologický	k2eAgFnSc4d1	epizootologická
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
likvidace	likvidace	k1gFnSc1	likvidace
chovu	chov	k1gInSc2	chov
místo	místo	k7c2	místo
nejisté	jistý	k2eNgFnSc2d1	nejistá
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
problematické	problematický	k2eAgFnSc3d1	problematická
účinnosti	účinnost	k1gFnSc3	účinnost
vakcín	vakcína	k1gFnPc2	vakcína
představují	představovat	k5eAaImIp3nP	představovat
protinákazová	protinákazový	k2eAgNnPc1d1	protinákazový
a	a	k8xC	a
zoohygienická	zoohygienický	k2eAgNnPc1d1	zoohygienický
opatření	opatření	k1gNnPc1	opatření
základní	základní	k2eAgFnSc2d1	základní
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc1d1	hlavní
způsob	způsob	k1gInSc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
chovů	chov	k1gInPc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
přerušení	přerušení	k1gNnSc1	přerušení
infekčního	infekční	k2eAgInSc2d1	infekční
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgNnPc2	některý
jiných	jiný	k2eAgNnPc2d1	jiné
bakteriálních	bakteriální	k2eAgNnPc2d1	bakteriální
onemocnění	onemocnění	k1gNnPc2	onemocnění
není	být	k5eNaImIp3nS	být
cholera	cholera	k1gFnSc1	cholera
šířena	šířen	k2eAgFnSc1d1	šířena
z	z	k7c2	z
líhní	líheň	k1gFnPc2	líheň
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
chovu	chov	k1gInSc6	chov
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
primárním	primární	k2eAgInSc7d1	primární
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nemocná	nemocný	k2eAgFnSc1d1	nemocná
drůbež	drůbež	k1gFnSc1	drůbež
nebo	nebo	k8xC	nebo
rekonvalescentní	rekonvalescentní	k2eAgMnPc1d1	rekonvalescentní
nosiči	nosič	k1gMnPc1	nosič
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
taková	takový	k3xDgNnPc4	takový
zvířata	zvíře	k1gNnPc4	zvíře
eliminovat	eliminovat	k5eAaBmF	eliminovat
nebo	nebo	k8xC	nebo
zabránit	zabránit	k5eAaPmF	zabránit
jejich	jejich	k3xOp3gInSc2	jejich
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
hejna	hejno	k1gNnSc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jednorázovým	jednorázový	k2eAgNnSc7d1	jednorázové
naskladňováním	naskladňování	k1gNnSc7	naskladňování
a	a	k8xC	a
vyskladňováním	vyskladňování	k1gNnSc7	vyskladňování
<g/>
,	,	kIx,	,
omezením	omezení	k1gNnSc7	omezení
existence	existence	k1gFnSc2	existence
smíšených	smíšený	k2eAgInPc2d1	smíšený
chovů	chov	k1gInPc2	chov
různých	různý	k2eAgFnPc2d1	různá
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
i	i	k8xC	i
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zlepšenou	zlepšený	k2eAgFnSc7d1	zlepšená
zoohygienou	zoohygiena	k1gFnSc7	zoohygiena
a	a	k8xC	a
sanitací	sanitace	k1gFnSc7	sanitace
prostředí	prostředí	k1gNnSc2	prostředí
před	před	k7c7	před
naskladněním	naskladnění	k1gNnSc7	naskladnění
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
P.	P.	kA	P.
multocida	multocida	k1gFnSc1	multocida
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlodavců	hlodavec	k1gMnPc2	hlodavec
i	i	k8xC	i
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
zamezit	zamezit	k5eAaPmF	zamezit
jejich	jejich	k3xOp3gInSc2	jejich
vstupu	vstup	k1gInSc2	vstup
i	i	k8xC	i
vletu	vlet	k1gInSc2	vlet
do	do	k7c2	do
hal	hala	k1gFnPc2	hala
z	z	k7c2	z
chovanou	chovaný	k2eAgFnSc7d1	chovaná
drůbeží	drůbež	k1gFnSc7	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
pohyb	pohyb	k1gInSc4	pohyb
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
<g/>
.	.	kIx.	.
</s>
