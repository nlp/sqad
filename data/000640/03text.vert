<s>
Otrantský	Otrantský	k2eAgInSc1d1	Otrantský
průliv	průliv	k1gInSc1	průliv
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Canale	Canala	k1gFnSc3	Canala
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Otranto	Otranta	k1gFnSc5	Otranta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průliv	průliv	k1gInSc1	průliv
mezi	mezi	k7c7	mezi
Apeninským	apeninský	k2eAgInSc7d1	apeninský
a	a	k8xC	a
Balkánským	balkánský	k2eAgInSc7d1	balkánský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
a	a	k8xC	a
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
náleží	náležet	k5eAaImIp3nS	náležet
Itálii	Itálie	k1gFnSc3	Itálie
a	a	k8xC	a
východní	východní	k2eAgFnSc3d1	východní
Albánii	Albánie	k1gFnSc3	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Otranto	Otranta	k1gFnSc5	Otranta
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Vlora	Vlora	k1gFnSc1	Vlora
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
75	[number]	k4	75
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Maximálně	maximálně	k6eAd1	maximálně
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
850	[number]	k4	850
m.	m.	k?	m.
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Otrantské	Otrantský	k2eAgFnSc6d1	Otrantský
úžině	úžina	k1gFnSc6	úžina
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Spojenci	spojenec	k1gMnPc1	spojenec
zablokovali	zablokovat	k5eAaPmAgMnP	zablokovat
průliv	průliv	k1gInSc4	průliv
mezi	mezi	k7c7	mezi
Brindisi	Brindisi	k1gNnSc7	Brindisi
a	a	k8xC	a
ostrovem	ostrov	k1gInSc7	ostrov
Korfu	Korfu	k1gNnSc1	Korfu
<g/>
.	.	kIx.	.
</s>
<s>
Blokáda	blokáda	k1gFnSc1	blokáda
měla	mít	k5eAaImAgFnS	mít
zabránit	zabránit	k5eAaPmF	zabránit
Rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
v	v	k7c6	v
účasti	účast	k1gFnSc6	účast
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
operacích	operace	k1gFnPc6	operace
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
a	a	k8xC	a
2004	[number]	k4	2004
zahynula	zahynout	k5eAaPmAgFnS	zahynout
téměř	téměř	k6eAd1	téměř
stovka	stovka	k1gFnSc1	stovka
Albánců	Albánec	k1gMnPc2	Albánec
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
překonat	překonat	k5eAaPmF	překonat
průliv	průliv	k1gInSc4	průliv
a	a	k8xC	a
nezákonně	zákonně	k6eNd1	zákonně
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
О	О	k?	О
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
