<s>
Otrantský	Otrantský	k2eAgInSc1d1	Otrantský
průliv	průliv	k1gInSc1	průliv
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Canale	Canala	k1gFnSc3	Canala
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Otranto	Otranta	k1gFnSc5	Otranta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průliv	průliv	k1gInSc1	průliv
mezi	mezi	k7c7	mezi
Apeninským	apeninský	k2eAgInSc7d1	apeninský
a	a	k8xC	a
Balkánským	balkánský	k2eAgInSc7d1	balkánský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
a	a	k8xC	a
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
