<p>
<s>
Feng-jüan	Fengüan	k1gInSc1	Feng-jüan
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInPc1d1	tradiční
znaky	znak	k1gInPc1	znak
<g/>
:	:	kIx,	:
豐	豐	k?	豐
<g/>
;	;	kIx,	;
zjednodušené	zjednodušený	k2eAgInPc1d1	zjednodušený
znaky	znak	k1gInPc1	znak
丰	丰	k?	丰
<g/>
;	;	kIx,	;
tongyong	tongyong	k1gMnSc1	tongyong
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Fongyuán	Fongyuán	k1gMnSc1	Fongyuán
<g/>
;	;	kIx,	;
hanyu	hanyu	k5eAaPmIp1nS	hanyu
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Fē	Fē	k1gMnSc1	Fē
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
bohatá	bohatý	k2eAgFnSc1d1	bohatá
rovina	rovina	k1gFnSc1	rovina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
správním	správní	k2eAgInSc6d1	správní
systému	systém	k1gInSc6	systém
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
okresu	okres	k1gInSc2	okres
Tchaj-čung	Tchaj-čunga	k1gFnPc2	Tchaj-čunga
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
41,18	[number]	k4	41,18
km2	km2	k4	km2
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
164	[number]	k4	164
071	[number]	k4	071
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Feng-jüan	Fengüan	k1gMnSc1	Feng-jüan
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svými	svůj	k3xOyFgInPc7	svůj
potravinovými	potravinový	k2eAgInPc7d1	potravinový
obchůdky	obchůdek	k1gInPc7	obchůdek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
chrámu	chrám	k1gInSc2	chrám
Ma-cu	Maus	k1gInSc2	Ma-cus
(	(	kIx(	(
<g/>
媽	媽	k?	媽
<g/>
;	;	kIx,	;
známý	známý	k1gMnSc1	známý
také	také	k9	také
jako	jako	k9	jako
Východní	východní	k2eAgInSc1d1	východní
chrám	chrám	k1gInSc1	chrám
–	–	k?	–
廟	廟	k?	廟
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
spoustou	spousta	k1gFnSc7	spousta
pekáren	pekárna	k1gFnPc2	pekárna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
situovány	situovat	k5eAaBmNgFnP	situovat
podél	podél	k7c2	podél
hlavní	hlavní	k2eAgFnSc2d1	hlavní
ulice	ulice	k1gFnSc2	ulice
Čung-čeng	Čung-čenga	k1gFnPc2	Čung-čenga
(	(	kIx(	(
<g/>
中	中	k?	中
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
výrobkem	výrobek	k1gInSc7	výrobek
je	být	k5eAaImIp3nS	být
ananasový	ananasový	k2eAgInSc1d1	ananasový
koláč	koláč	k1gInSc1	koláč
Feng	Feng	k1gInSc1	Feng
<g/>
-li	i	k?	-li
<g/>
-su	u	k?	-su
(	(	kIx(	(
<g/>
鳳	鳳	k?	鳳
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Feng-jüan	Fengüana	k1gFnPc2	Feng-jüana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Feng-jüan	Fengüana	k1gFnPc2	Feng-jüana
</s>
</p>
