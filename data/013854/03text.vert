<s>
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
</s>
<s>
Prof.	prof.	kA
PhDr.	PhDr.	kA
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
Narození	narození	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1917	#num#	k4
<g/>
Rokycany	Rokycany	k1gInPc7
Úmrtí	úmrť	k1gFnSc7
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1995	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgNnSc2d1
Povolání	povolání	k1gNnSc2
</s>
<s>
literární	literární	k2eAgMnSc1d1
historik	historik	k1gMnSc1
a	a	k8xC
kritik	kritik	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1917	#num#	k4
Rokycany	Rokycany	k1gInPc7
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1995	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
literární	literární	k2eAgMnSc1d1
historik	historik	k1gMnSc1
a	a	k8xC
kritik	kritik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Rokycanech	Rokycany	k1gInPc6
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
reálném	reálný	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Klatovech	Klatovy	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	rok	k1gInPc6
1936	#num#	k4
<g/>
–	–	kIx~
<g/>
1939	#num#	k4
a	a	k8xC
1945	#num#	k4
<g/>
–	–	kIx~
<g/>
1946	#num#	k4
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
češtinu	čeština	k1gFnSc4
a	a	k8xC
francouzštinu	francouzština	k1gFnSc4
(	(	kIx(
<g/>
PhDr.	PhDr.	kA
v	v	k7c6
r.	r.	kA
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
byl	být	k5eAaImAgInS
vězněn	věznit	k5eAaImNgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
akce	akce	k1gFnSc2
„	„	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1939	#num#	k4
<g/>
“	“	k?
v	v	k7c6
koncentračním	koncentrační	k2eAgInSc6d1
táboře	tábor	k1gInSc6
Sachsenhausen-Oranienburg	Sachsenhausen-Oranienburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
účast	účast	k1gFnSc4
v	v	k7c6
národním	národní	k2eAgInSc6d1
odboji	odboj	k1gInSc6
byl	být	k5eAaImAgInS
vyznamenán	vyznamenat	k5eAaPmNgInS
Československým	československý	k2eAgInSc7d1
válečným	válečný	k2eAgInSc7d1
křížem	kříž	k1gInSc7
1939	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
válce	válka	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
nejdříve	dříve	k6eAd3
jako	jako	k8xC,k8xS
dělník	dělník	k1gMnSc1
<g/>
,	,	kIx,
po	po	k7c6
ukončení	ukončení	k1gNnSc6
studia	studio	k1gNnSc2
na	na	k7c6
knihovnické	knihovnický	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
jako	jako	k8xC,k8xS
knihovník	knihovník	k1gMnSc1
v	v	k7c6
Úřadu	úřad	k1gInSc6
předsednictva	předsednictvo	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
KSČ	KSČ	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
byl	být	k5eAaImAgInS
zaměstnán	zaměstnat	k5eAaPmNgMnS
jako	jako	k9
knihovník	knihovník	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
jako	jako	k9
vedoucí	vedoucí	k1gMnSc1
kulturního	kulturní	k2eAgInSc2d1
odboru	odbor	k1gInSc2
Kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
externě	externě	k6eAd1
vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
FF	ff	kA
UK	UK	kA
<g/>
,	,	kIx,
habilitoval	habilitovat	k5eAaBmAgMnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
prací	práce	k1gFnPc2
Bezruč	Bezruč	k1gFnPc2
–	–	k?
Toman	Toman	k1gMnSc1
–	–	k?
Gellner	Gellner	k1gMnSc1
–	–	k?
<g/>
Šrámek	Šrámek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
profesorem	profesor	k1gMnSc7
novodobé	novodobý	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
roku	rok	k1gInSc2
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
CSc.	CSc.	kA
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
monografií	monografie	k1gFnPc2
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
<g/>
,	,	kIx,
titul	titul	k1gInSc1
DrSc	DrSc	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
prací	práce	k1gFnPc2
Generace	generace	k1gFnSc2
buřičů	buřič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedru	katedra	k1gFnSc4
české	český	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
vedl	vést	k5eAaImAgInS
v	v	k7c6
období	období	k1gNnSc6
1956	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
,	,	kIx,
proděkanem	proděkan	k1gMnSc7
byl	být	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
tzv.	tzv.	kA
normalizace	normalizace	k1gFnSc2
byl	být	k5eAaImAgInS
vyškrtnut	vyškrtnout	k5eAaPmNgInS
z	z	k7c2
KSČ	KSČ	kA
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
setrval	setrvat	k5eAaPmAgMnS
jako	jako	k9
profesor	profesor	k1gMnSc1
na	na	k7c6
katedře	katedra	k1gFnSc6
české	český	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
literární	literární	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
FF	ff	kA
UK	UK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Literární	literární	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
publikoval	publikovat	k5eAaBmAgMnS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
novinách	novina	k1gFnPc6
a	a	k8xC
časopisech	časopis	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Rudém	rudý	k2eAgNnSc6d1
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Tvorbě	tvorba	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Literárních	literární	k2eAgFnPc6d1
novinách	novina	k1gFnPc6
<g/>
,	,	kIx,
odborné	odborný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
pak	pak	k6eAd1
zvláště	zvláště	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
<g/>
,	,	kIx,
Českém	český	k2eAgInSc6d1
jazyku	jazyk	k1gInSc6
a	a	k8xC
literatuře	literatura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redigoval	redigovat	k5eAaImAgMnS
měsíčník	měsíčník	k1gMnSc1
Impuls	impuls	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
editor	editor	k1gInSc1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
vydání	vydání	k1gNnSc6
několika	několik	k4yIc2
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Česká	český	k2eAgFnSc1d1
poesie	poesie	k1gFnSc1
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
</s>
<s>
J.	J.	kA
Hora	hora	k1gFnSc1
<g/>
:	:	kIx,
Výbor	výbor	k1gInSc1
z	z	k7c2
básní	báseň	k1gFnPc2
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
;	;	kIx,
F.	F.	kA
Gellner	Gellner	k1gInSc1
<g/>
:	:	kIx,
Má	mít	k5eAaImIp3nS
píseň	píseň	k1gFnSc4
ze	z	k7c2
sna	sen	k1gInSc2
budívá	budívat	k5eAaImIp3nS
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
V.	V.	kA
Dyk	Dyk	k?
<g/>
:	:	kIx,
Opustíš	opustit	k5eAaPmIp2nS
<g/>
-li	-li	k?
mne	já	k3xPp1nSc2
<g/>
,	,	kIx,
nezahynu	zahynout	k5eNaPmIp1nS
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
;	;	kIx,
Patnáct	patnáct	k4xCc1
májů	máj	k1gInPc2
(	(	kIx(
<g/>
sb	sb	kA
<g/>
.	.	kIx.
z	z	k7c2
české	český	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
1945	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Čítanka	čítanka	k1gFnSc1
českého	český	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
o	o	k7c6
literatuře	literatura	k1gFnSc6
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS
se	se	k3xPyFc4
českou	český	k2eAgFnSc7d1
literaturou	literatura	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
české	český	k2eAgFnSc3d1
poezii	poezie	k1gFnSc3
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Stanislav	Stanislav	k1gMnSc1
Kostka	Kostka	k1gMnSc1
Neumann	Neumann	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Gellner	Gellner	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Toman	Toman	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
<g/>
,	,	kIx,
Fráňa	Fráňa	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
soustředil	soustředit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
bádání	bádání	k1gNnSc4
na	na	k7c4
tvorbu	tvorba	k1gFnSc4
Karla	Karel	k1gMnSc2
Čapka	Čapek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímal	zajímat	k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
o	o	k7c6
literární	literární	k2eAgFnSc6d1
historii	historie	k1gFnSc6
klatovského	klatovský	k2eAgInSc2d1
regionu	region	k1gInSc2
(	(	kIx(
<g/>
Literární	literární	k2eAgInPc1d1
Klatovy	Klatovy	k1gInPc1
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Z	z	k7c2
poetického	poetický	k2eAgNnSc2d1
Pošumaví	Pošumaví	k1gNnSc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
pozornosti	pozornost	k1gFnSc2
stála	stát	k5eAaImAgFnS
historie	historie	k1gFnSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
několika	několik	k4yIc2
monografií	monografie	k1gFnPc2
(	(	kIx(
<g/>
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Fráňa	Fráňa	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Karel	Karel	k1gMnSc1
Toman	Toman	k1gMnSc1
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
14	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Jarmila	Jarmila	k1gFnSc1
Glazarová	Glazarová	k1gFnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Kritik	kritik	k1gMnSc1
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
aj.	aj.	kA
<g/>
)	)	kIx)
a	a	k8xC
syntetických	syntetický	k2eAgFnPc2d1
příruček	příručka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Výběr	výběr	k1gInSc1
</s>
<s>
Z	z	k7c2
dějin	dějiny	k1gFnPc2
české	český	k2eAgFnSc2d1
literární	literární	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
<g/>
,	,	kIx,
1965	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
1968	#num#	k4
</s>
<s>
Generace	generace	k1gFnSc1
buřičů	buřič	k1gMnPc2
<g/>
,	,	kIx,
1968	#num#	k4
</s>
<s>
O	o	k7c6
české	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
našeho	náš	k3xOp1gInSc2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
1972	#num#	k4
</s>
<s>
Z	z	k7c2
moderní	moderní	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
1980	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
,	,	kIx,
1981	#num#	k4
</s>
<s>
Čapkovské	čapkovský	k2eAgFnPc4d1
variace	variace	k1gFnPc4
<g/>
,	,	kIx,
1984	#num#	k4
</s>
<s>
Z	z	k7c2
literárněvědných	literárněvědný	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
1985	#num#	k4
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
Literatura	literatura	k1gFnSc1
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
–	–	k?
spoluautor	spoluautor	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BLAHYNKA	BLAHYNKA	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
spisovatelé	spisovatel	k1gMnPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
slovníková	slovníkový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
830	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
12396	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
61	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PETRÁŇ	Petráň	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
PETRÁŇOVÁ	Petráňová	k1gFnSc1
<g/>
,	,	kIx,
Lydia	Lydia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofové	filozof	k1gMnPc1
dělají	dělat	k5eAaImIp3nP
revoluci	revoluce	k1gFnSc4
<g/>
:	:	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
během	během	k7c2
komunistického	komunistický	k2eAgInSc2d1
experimentu	experiment	k1gInSc2
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc1
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
1132	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
2994	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
537	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezruc	Bezruc	k1gFnSc1
<g/>
̆	̆	k?
<g/>
,	,	kIx,
Toman	Toman	k1gMnSc1
<g/>
,	,	kIx,
Gellner	Gellner	k1gMnSc1
<g/>
,	,	kIx,
Šrámek	Šrámek	k1gMnSc1
<g/>
:	:	kIx,
studie	studie	k1gFnPc1
o	o	k7c6
básnících	básník	k1gMnPc6
počátku	počátek	k1gInSc2
našeho	náš	k3xOp1gInSc2
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
66292882	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
66292882	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PETRÁŇ	Petráň	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
PETRÁŇOVÁ	Petráňová	k1gFnSc1
<g/>
,	,	kIx,
Lydia	Lydia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofové	filozof	k1gMnPc1
dělají	dělat	k5eAaImIp3nP
revoluci	revoluce	k1gFnSc4
<g/>
:	:	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
během	během	k7c2
komunistického	komunistický	k2eAgInSc2d1
experimentu	experiment	k1gInSc2
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc1
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
1132	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
2994	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
478	#num#	k4
<g/>
–	–	k?
<g/>
479	#num#	k4
a	a	k8xC
537	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Impuls	impuls	k1gInSc1
<g/>
:	:	kIx,
měsičník	měsičník	k1gInSc1
pro	pro	k7c4
literární	literární	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
a	a	k8xC
teorii	teorie	k1gFnSc4
<g/>
..	..	k?
Impuls	impuls	k1gInSc1
:	:	kIx,
měsičník	měsičník	k1gInSc1
pro	pro	k7c4
literární	literární	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
a	a	k8xC
teorii	teorie	k1gFnSc4
<g/>
..	..	k?
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
<g/>
:	:	kIx,
440072290	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Czech	Czech	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
poesie	poesie	k1gFnSc1
:	:	kIx,
výbor	výbor	k1gInSc1
básní	básnit	k5eAaImIp3nS
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
GELLNER	GELLNER	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
píseň	píseň	k1gFnSc4
ze	z	k7c2
sna	sen	k1gInSc2
budívá	budívat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
85440386	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
85440386	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patnáct	patnáct	k4xCc1
májů	máj	k1gInPc2
<g/>
:	:	kIx,
česká	český	k2eAgFnSc1d1
a	a	k8xC
slovenska	slovensko	k1gNnSc2
poezie	poezie	k1gFnSc2
1945-1960	1945-1960	k4
=	=	kIx~
Pätnástʹ	Pätnástʹ	k1gFnPc2
májov	májovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Českoslov	Českoslov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spis	spis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
917697402	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
917697402	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Čítanka	čítanka	k1gFnSc1
českého	český	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
o	o	k7c6
literatuře	literatura	k1gFnSc6
/	/	kIx~
uspoř	uspořit	k5eAaPmRp2nS
<g/>
.	.	kIx.
a	a	k8xC
uvádí	uvádět	k5eAaImIp3nS
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
;	;	kIx,
text	text	k1gInSc1
ukázek	ukázka	k1gFnPc2
připr	připra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věra	Věra	k1gFnSc1
Pašková	Pašková	k1gFnSc1
-	-	kIx~
Katalog	katalog	k1gInSc1
Krajské	krajský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Františka	František	k1gMnSc2
Bartoše	Bartoš	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
.	.	kIx.
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
kfbz	kfbz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgInPc1d1
Klatovy	Klatovy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Krajské	krajský	k2eAgFnSc2d1
Nakl	Nakl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
947212428	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
947212428	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
poetického	poetický	k2eAgNnSc2d1
Pošumaví	Pošumaví	k1gNnSc2
<g/>
..	..	k?
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Západočes	Západočes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naklad	naklást	k5eAaPmDgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
246914672	#num#	k4
(	(	kIx(
<g/>
Undetermined	Undetermined	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
246914672	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Osvěta	osvěta	k1gFnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
730009913	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
730009913	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fráňa	Fráňa	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gInSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
603834876	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
603834876	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Toman	Toman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
439416380	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
439416380	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gInSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
263465959	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
263465959	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
GLAZAROVÁ	GLAZAROVÁ	kA
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jarmila	Jarmila	k1gFnSc1
Glazarová	Glazarová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Českoslov	Českoslov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisovatel	spisovatel	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
251718283	#num#	k4
(	(	kIx(
<g/>
Undetermined	Undetermined	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
251718283	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BURIÁNEK	Buriánek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritik	kritika	k1gFnPc2
F.	F.	kA
<g/>
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
Spisovatel	spisovatel	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
1015013720	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gMnSc1
<g/>
)	)	kIx)
OCLC	OCLC	kA
<g/>
:	:	kIx,
1015013720	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
A-	A-	k1gFnPc2
<g/>
J.	J.	kA
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
k	k	k7c3
tématu	téma	k1gNnSc3
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
na	na	k7c4
Obalkyknih	Obalkyknih	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
František	František	k1gMnSc1
Buriánek	Buriánek	k1gMnSc1
</s>
<s>
Slovník	slovník	k1gInSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
</s>
<s>
Soupis	soupis	k1gInSc1
děl	dělo	k1gNnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1020403	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1050376838	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1035	#num#	k4
2390	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80156055	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2483483	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80156055	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
