<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Rokycanech	Rokycany	k1gInPc6
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
reálném	reálný	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Klatovech	Klatovy	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	rok	k1gInPc6
1936	#num#	k4
<g/>
–	–	kIx~
<g/>
1939	#num#	k4
a	a	k8xC
1945	#num#	k4
<g/>
–	–	kIx~
<g/>
1946	#num#	k4
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
češtinu	čeština	k1gFnSc4
a	a	k8xC
francouzštinu	francouzština	k1gFnSc4
(	(	kIx(
<g/>
PhDr.	PhDr.	kA
v	v	k7c6
r.	r.	kA
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>