<s>
Irwin	Irwin	k1gMnSc1	Irwin
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
osobností	osobnost	k1gFnPc2	osobnost
beatnické	beatnický	k2eAgFnSc2d1	beatnická
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
