<s>
Kanadský	kanadský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
</s>
<s>
Kanadský	kanadský	k2eAgMnSc1d1
dolarZemě	dolarZemě	k6eAd1
</s>
<s>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
CAD	CAD	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
1,61	1,61	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
$	$	kIx~
nebo	nebo	k8xC
Can	Can	k1gFnSc1
<g/>
$	$	kIx~
nebo	nebo	k8xC
C	C	kA
<g/>
$	$	kIx~
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
cent	cent	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
¢	¢	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
¢	¢	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
¢	¢	k?
<g/>
,	,	kIx,
$	$	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
$	$	kIx~
<g/>
2	#num#	k4
<g/>
zřídka	zřídka	k6eAd1
užívaná	užívaný	k2eAgFnSc1d1
50	#num#	k4
<g/>
¢	¢	k?
Bankovky	bankovka	k1gFnPc1
</s>
<s>
$	$	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
$	$	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
$	$	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
$	$	kIx~
<g/>
50	#num#	k4
<g/>
,	,	kIx,
$	$	kIx~
<g/>
100	#num#	k4
</s>
<s>
Kanadský	kanadský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
canadian	canadian	k1gInSc1
dollar	dollar	k1gInSc1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
dollar	dollar	k1gInSc1
canadien	canadien	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
druhého	druhý	k4xOgInSc2
největšího	veliký	k2eAgInSc2d3
státu	stát	k1gInSc2
světa	svět	k1gInSc2
-	-	kIx~
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
CAD	CAD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
dolar	dolar	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
stem	sto	k4xCgNnSc7
centů	cent	k1gInPc2
(	(	kIx(
<g/>
¢	¢	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžném	běžný	k2eAgInSc6d1
mluveném	mluvený	k2eAgInSc6d1
projevu	projev	k1gInSc6
Kanaďanů	Kanaďan	k1gMnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
buck	buck	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
piastre	piastr	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1871	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Kanady	Kanada	k1gFnSc2
používalo	používat	k5eAaImAgNnS
více	hodně	k6eAd2
měn	měna	k1gFnPc2
(	(	kIx(
<g/>
dolar	dolar	k1gInSc1
Britské	britský	k2eAgFnSc2d1
Kolumbie	Kolumbie	k1gFnSc2
<g/>
,	,	kIx,
dolar	dolar	k1gInSc4
Ostrova	ostrov	k1gInSc2
prince	princ	k1gMnSc2
Edwarda	Edward	k1gMnSc2
<g/>
,	,	kIx,
dolar	dolar	k1gInSc1
Nového	Nového	k2eAgNnSc2d1
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
Uniform	Uniform	k1gInSc4
Currency	Currenc	k2eAgMnPc4d1
Act	Act	k1gMnPc4
/	/	kIx~
Loi	Loi	k1gFnSc1
sur	sur	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
uniformité	uniformitý	k2eAgNnSc1d1
de	de	k?
la	la	k1gNnSc1
monnaie	monnaie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
provinciích	provincie	k1gFnPc6
zavedla	zavést	k5eAaPmAgFnS
jednu	jeden	k4xCgFnSc4
společnou	společný	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
–	–	k?
kanadský	kanadský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnPc1
dolaru	dolar	k1gInSc2
jsou	být	k5eAaImIp3nP
raženy	razit	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
a	a	k8xC
50	#num#	k4
centů	cent	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
1	#num#	k4
a	a	k8xC
2	#num#	k4
dolary	dolar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
HodnotaLidový	HodnotaLidový	k2eAgInSc1d1
názevMateriálHmotnost	názevMateriálHmotnost	k1gFnSc1
[	[	kIx(
<g/>
g	g	kA
<g/>
]	]	kIx)
<g/>
Průměrmince	Průměrminec	k1gMnSc2
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
<g/>
Tloušťkamince	Tloušťkaminec	k1gMnSc2
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
<g/>
Rubovástrana	Rubovástran	k1gMnSc2
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
¢	¢	k?
</s>
<s>
Penny	penny	k1gFnSc1
</s>
<s>
ocel	ocel	k1gFnSc1
94	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
1.5	1.5	k4
<g/>
%	%	kIx~
-	-	kIx~
měď	měď	k1gFnSc1
4.5	4.5	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
měděný	měděný	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2.35	2.35	k4
</s>
<s>
19.05	19.05	k4
</s>
<s>
1.45	1.45	k4
</s>
<s>
list	list	k1gInSc1
javoru	javor	k1gInSc2
</s>
<s>
5	#num#	k4
<g/>
¢	¢	k?
</s>
<s>
Nickel	Nickel	k1gMnSc1
</s>
<s>
ocel	ocel	k1gFnSc1
94.5	94.5	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
2	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
měď	měď	k1gFnSc1
3.5	3.5	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
niklový	niklový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
3.95	3.95	k4
</s>
<s>
21.20	21.20	k4
</s>
<s>
1.76	1.76	k4
</s>
<s>
bobr	bobr	k1gMnSc1
kanadský	kanadský	k2eAgMnSc1d1
</s>
<s>
10	#num#	k4
<g/>
¢	¢	k?
</s>
<s>
Dime	Dime	k6eAd1
</s>
<s>
ocel	ocel	k1gFnSc1
92	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
2.5	2.5	k4
<g/>
%	%	kIx~
-	-	kIx~
měď	měď	k1gFnSc1
5.5	5.5	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
niklový	niklový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1.75	1.75	k4
</s>
<s>
18.03	18.03	k4
</s>
<s>
1.22	1.22	k4
</s>
<s>
kanadský	kanadský	k2eAgInSc1d1
škuner	škuner	k1gInSc1
Bluenose	Bluenosa	k1gFnSc3
</s>
<s>
25	#num#	k4
<g/>
¢	¢	k?
</s>
<s>
Quarter	quarter	k1gInSc1
</s>
<s>
ocel	ocel	k1gFnSc1
94	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
2.2	2.2	k4
<g/>
%	%	kIx~
-	-	kIx~
měď	měď	k1gFnSc1
3.8	3.8	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
niklový	niklový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
4.40	4.40	k4
</s>
<s>
23.88	23.88	k4
</s>
<s>
1.58	1.58	k4
</s>
<s>
sob	sob	k1gMnSc1
polární	polární	k2eAgFnSc2d1
</s>
<s>
50	#num#	k4
<g/>
¢	¢	k?
</s>
<s>
Stück	Stück	k6eAd1
</s>
<s>
ocel	ocel	k1gFnSc1
93.15	93.15	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
2.1	2.1	k4
<g/>
%	%	kIx~
-	-	kIx~
měď	měď	k1gFnSc1
4.75	4.75	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
niklový	niklový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
6.90	6.90	k4
</s>
<s>
27.13	27.13	k4
</s>
<s>
1.95	1.95	k4
</s>
<s>
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Kanady	Kanada	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
$	$	kIx~
</s>
<s>
Loonie	Loonie	k1gFnSc1
</s>
<s>
ocel	ocel	k1gFnSc1
91.5	91.5	k4
<g/>
%	%	kIx~
-	-	kIx~
bronz	bronz	k1gInSc4
8.5	8.5	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
bronzový	bronzový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
7.00	7.00	k4
</s>
<s>
26.5	26.5	k4
</s>
<s>
1.75	1.75	k4
</s>
<s>
potáplice	potáplice	k1gFnSc1
lední	lední	k2eAgFnSc2d1
</s>
<s>
2	#num#	k4
<g/>
$	$	kIx~
</s>
<s>
Twonie	Twonie	k1gFnSc1
</s>
<s>
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
nikl	nikl	k1gInSc1
99	#num#	k4
<g/>
%	%	kIx~
<g/>
Jádro	jádro	k1gNnSc1
<g/>
:	:	kIx,
měď	měď	k1gFnSc1
92	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
nikl	nikl	k1gInSc1
2	#num#	k4
<g/>
%	%	kIx~
-	-	kIx~
hliník	hliník	k1gInSc1
6	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
7.30	7.30	k4
</s>
<s>
28.00	28.00	k4
</s>
<s>
1.80	1.80	k4
</s>
<s>
medvěd	medvěd	k1gMnSc1
lední	lední	k2eAgFnSc2d1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
kanadského	kanadský	k2eAgInSc2d1
dolaru	dolar	k1gInSc2
mají	mít	k5eAaImIp3nP
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
dolarů	dolar	k1gInPc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
z	z	k7c2
polymeru	polymer	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vyobrazené	vyobrazený	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
bankovkách	bankovka	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
5	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
:	:	kIx,
Wilfrid	Wilfrida	k1gFnPc2
Laurier	Lauriero	k1gNnPc2
</s>
<s>
10	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
:	:	kIx,
John	John	k1gMnSc1
Macdonald	Macdonald	k1gMnSc1
</s>
<s>
20	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
:	:	kIx,
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
50	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
:	:	kIx,
William	William	k1gInSc1
Lyon	Lyon	k1gInSc1
Mackenzie	Mackenzie	k1gFnSc1
King	King	k1gMnSc1
</s>
<s>
100	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Borden	Bordna	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bank	bank	k1gInSc1
Note	Note	k1gFnSc1
Series	Series	k1gMnSc1
-	-	kIx~
Frontiers	Frontiers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bank	bank	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kanadský	kanadský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Historické	historický	k2eAgFnPc4d1
a	a	k8xC
současné	současný	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
Kanady	Kanada	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
