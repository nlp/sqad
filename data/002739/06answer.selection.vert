<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
ramenech	rameno	k1gNnPc6	rameno
(	(	kIx(	(
<g/>
Dziwna	Dziwna	k1gFnSc1	Dziwna
<g/>
,	,	kIx,	,
Svina	Svina	k1gFnSc1	Svina
a	a	k8xC	a
Peene	Peen	k1gMnSc5	Peen
<g/>
)	)	kIx)	)
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
ve	v	k7c6	v
Štětínské	štětínský	k2eAgFnSc6d1	Štětínská
deltě	delta	k1gFnSc6	delta
severně	severně	k6eAd1	severně
od	od	k7c2	od
polského	polský	k2eAgInSc2d1	polský
Štětína	Štětín	k1gInSc2	Štětín
<g/>
.	.	kIx.	.
</s>
