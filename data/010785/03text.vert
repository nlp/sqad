<p>
<s>
Gleisdreieck	Gleisdreieck	k6eAd1	Gleisdreieck
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnPc4	stanice
berlínského	berlínský	k2eAgNnSc2d1	berlínské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
znamenající	znamenající	k2eAgInSc1d1	znamenající
kolejištní	kolejištní	k2eAgInSc1d1	kolejištní
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Kreuzberg	Kreuzberg	k1gInSc1	Kreuzberg
ve	v	k7c6	v
správním	správní	k2eAgInSc6d1	správní
obvodě	obvod	k1gInSc6	obvod
Friedrichshain-Kreuzberg	Friedrichshain-Kreuzberg	k1gInSc1	Friedrichshain-Kreuzberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1908	[number]	k4	1908
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
stanice	stanice	k1gFnSc2	stanice
stala	stát	k5eAaPmAgFnS	stát
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
vlaky	vlak	k1gInPc1	vlak
jedoucí	jedoucí	k2eAgInPc1d1	jedoucí
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
se	se	k3xPyFc4	se
srazily	srazit	k5eAaPmAgFnP	srazit
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
A.	A.	kA	A.
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
zde	zde	k6eAd1	zde
staví	stavit	k5eAaPmIp3nP	stavit
dnešní	dnešní	k2eAgFnPc1d1	dnešní
linky	linka	k1gFnPc1	linka
U1	U1	k1gFnSc2	U1
a	a	k8xC	a
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
sanace	sanace	k1gFnSc1	sanace
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
oprava	oprava	k1gFnSc1	oprava
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
střechy	střecha	k1gFnSc2	střecha
za	za	k7c7	za
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Sanace	sanace	k1gFnSc1	sanace
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
až	až	k9	až
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
výtahy	výtah	k1gInPc4	výtah
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
otevřený	otevřený	k2eAgInSc4d1	otevřený
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
název	název	k1gInSc1	název
jako	jako	k8xC	jako
stanice	stanice	k1gFnSc1	stanice
nese	nést	k5eAaImIp3nS	nést
nedaleký	daleký	k2eNgMnSc1d1	nedaleký
60	[number]	k4	60
hektarový	hektarový	k2eAgInSc1d1	hektarový
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bahnhof	Bahnhof	k1gMnSc1	Bahnhof
Berlin	berlina	k1gFnPc2	berlina
Gleisdreieck	Gleisdreieck	k1gMnSc1	Gleisdreieck
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
