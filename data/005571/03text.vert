<s>
Orient	Orient	k1gInSc1	Orient
(	(	kIx(	(
<g/>
franc	franc	k1gInSc1	franc
<g/>
.	.	kIx.	.
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgNnSc1d1	tradiční
označení	označení	k1gNnSc1	označení
východních	východní	k2eAgFnPc2d1	východní
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
češtině	čeština	k1gFnSc6	čeština
znamená	znamenat	k5eAaImIp3nS	znamenat
zpravidla	zpravidla	k6eAd1	zpravidla
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
východní	východní	k2eAgNnPc4d1	východní
Středomoří	středomoří	k1gNnPc4	středomoří
<g/>
,	,	kIx,	,
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
i	i	k8xC	i
Střední	střední	k2eAgInSc4d1	střední
a	a	k8xC	a
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Orient	Orient	k1gInSc1	Orient
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
východ	východ	k1gInSc4	východ
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
oriens	oriens	k6eAd1	oriens
<g/>
,	,	kIx,	,
oriente	orient	k1gInSc5	orient
(	(	kIx(	(
<g/>
pocházející	pocházející	k2eAgFnPc1d1	pocházející
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnPc1d1	vycházející
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oriente	orient	k1gInSc5	orient
(	(	kIx(	(
<g/>
sole	sol	k1gInSc5	sol
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vychází	vycházet	k5eAaImIp3nS	vycházet
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
synonyma	synonymum	k1gNnSc2	synonymum
sole	sol	k1gInSc5	sol
levante	levant	k1gMnSc5	levant
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
označení	označení	k1gNnSc1	označení
levanta	levanta	k1gFnSc1	levanta
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
protiklad	protiklad	k1gInSc1	protiklad
<g/>
,	,	kIx,	,
okcident	okcident	k1gInSc1	okcident
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
occidens	occidens	k6eAd1	occidens
<g/>
,	,	kIx,	,
occidente	occident	k1gMnSc5	occident
(	(	kIx(	(
<g/>
sole	sol	k1gInSc5	sol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
západ	západ	k1gInSc1	západ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
označení	označení	k1gNnSc1	označení
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
obsah	obsah	k1gInSc4	obsah
pojmu	pojem	k1gInSc2	pojem
Orient	Orient	k1gInSc4	Orient
přirozeně	přirozeně	k6eAd1	přirozeně
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mluvčí	mluvčí	k1gMnSc1	mluvčí
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Zákoně	zákon	k1gInSc6	zákon
znamenal	znamenat	k5eAaImAgInS	znamenat
orient	orient	k1gInSc1	orient
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
anatolé	anatol	k1gMnPc1	anatol
<g/>
)	)	kIx)	)
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
východní	východní	k2eAgFnSc2d1	východní
Středomoří	středomoří	k1gNnSc3	středomoří
a	a	k8xC	a
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
mohl	moct	k5eAaImAgMnS	moct
znamenat	znamenat	k5eAaImF	znamenat
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
východní	východní	k2eAgInSc4d1	východní
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
odpovídajícího	odpovídající	k2eAgNnSc2d1	odpovídající
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
anatolé	anatolý	k1gMnPc4	anatolý
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
orientem	orient	k1gInSc7	orient
rozuměla	rozumět	k5eAaImAgFnS	rozumět
oblast	oblast	k1gFnSc1	oblast
východního	východní	k2eAgMnSc2d1	východní
<g/>
,	,	kIx,	,
pravoslavného	pravoslavný	k2eAgNnSc2d1	pravoslavné
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objevitelskými	objevitelský	k2eAgFnPc7d1	objevitelská
cestami	cesta	k1gFnPc7	cesta
a	a	k8xC	a
koloniálním	koloniální	k2eAgInSc7d1	koloniální
obchodem	obchod	k1gInSc7	obchod
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
pojmu	pojem	k1gInSc2	pojem
Orient	Orient	k1gInSc1	Orient
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
angličtině	angličtina	k1gFnSc6	angličtina
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
jižní	jižní	k2eAgFnSc4d1	jižní
polovinu	polovina	k1gFnSc4	polovina
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
po	po	k7c6	po
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Sundské	Sundský	k2eAgInPc1d1	Sundský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
:	:	kIx,	:
Orient	Orient	k1gInSc4	Orient
expres	expres	k2eAgInSc4d1	expres
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
končil	končit	k5eAaImAgInS	končit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nepřekročil	překročit	k5eNaPmAgMnS	překročit
hranice	hranice	k1gFnPc4	hranice
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
se	se	k3xPyFc4	se
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
Orient	Orient	k1gInSc4	Orient
spojovaly	spojovat	k5eAaImAgInP	spojovat
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
stereotypy	stereotyp	k1gInPc1	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
úsloví	úsloví	k1gNnSc1	úsloví
Ex	ex	k6eAd1	ex
oriente	orient	k1gInSc5	orient
lux	lux	k1gInSc1	lux
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc1	světlo
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
týkalo	týkat	k5eAaImAgNnS	týkat
východu	východ	k1gInSc3	východ
slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
starověku	starověk	k1gInSc6	starověk
jím	on	k3xPp3gInSc7	on
křesťané	křesťan	k1gMnPc1	křesťan
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgNnSc2	svůj
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
východní	východní	k2eAgFnSc2d1	východní
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
velké	velký	k2eAgFnSc2d1	velká
módy	móda	k1gFnSc2	móda
orientalismu	orientalismus	k1gInSc2	orientalismus
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dovážely	dovážet	k5eAaImAgInP	dovážet
a	a	k8xC	a
napodobovaly	napodobovat	k5eAaImAgInP	napodobovat
umělecké	umělecký	k2eAgInPc1d1	umělecký
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
stavěly	stavět	k5eAaImAgInP	stavět
"	"	kIx"	"
<g/>
čínské	čínský	k2eAgInPc1d1	čínský
pavilony	pavilon	k1gInPc1	pavilon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
parafrázovaly	parafrázovat	k5eAaBmAgInP	parafrázovat
a	a	k8xC	a
překládaly	překládat	k5eAaImAgInP	překládat
orientální	orientální	k2eAgInPc1d1	orientální
texty	text	k1gInPc1	text
a	a	k8xC	a
Goethe	Goethe	k1gInSc1	Goethe
napsal	napsat	k5eAaPmAgInS	napsat
"	"	kIx"	"
<g/>
Západovýchodní	západovýchodní	k2eAgInSc1d1	západovýchodní
díván	díván	k1gInSc1	díván
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
orientalismus	orientalismus	k1gInSc1	orientalismus
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
romantismem	romantismus	k1gInSc7	romantismus
a	a	k8xC	a
v	v	k7c6	v
orientálním	orientální	k2eAgInSc6d1	orientální
slohu	sloh	k1gInSc6	sloh
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
i	i	k9	i
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k9	ale
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
Orient	Orient	k1gInSc1	Orient
začaly	začít	k5eAaPmAgInP	začít
spojovat	spojovat	k5eAaImF	spojovat
záporné	záporný	k2eAgInPc1d1	záporný
stereotypy	stereotyp	k1gInPc1	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Balkán	Balkán	k1gInSc1	Balkán
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Orient	Orient	k1gInSc1	Orient
<g/>
"	"	kIx"	"
znamenaly	znamenat	k5eAaImAgFnP	znamenat
zanedbané	zanedbaný	k2eAgFnPc1d1	zanedbaná
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
špína	špína	k1gFnSc1	špína
a	a	k8xC	a
nepořádek	nepořádek	k1gInSc1	nepořádek
a	a	k8xC	a
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
korupce	korupce	k1gFnSc1	korupce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
spojení	spojení	k1gNnPc1	spojení
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
orientální	orientální	k2eAgNnSc1d1	orientální
bohatství	bohatství	k1gNnSc1	bohatství
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
orientální	orientální	k2eAgMnSc1d1	orientální
despota	despota	k1gMnSc1	despota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
těmto	tento	k3xDgInPc3	tento
stereotypům	stereotyp	k1gInPc3	stereotyp
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
americký	americký	k2eAgMnSc1d1	americký
kritik	kritik	k1gMnSc1	kritik
Edward	Edward	k1gMnSc1	Edward
Said	Said	k1gInSc4	Said
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
knihou	kniha	k1gFnSc7	kniha
Orientalismus	orientalismus	k1gInSc1	orientalismus
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
