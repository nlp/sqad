<s>
Orient	Orient	k1gInSc1	Orient
(	(	kIx(	(
<g/>
franc	franc	k1gInSc1	franc
<g/>
.	.	kIx.	.
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgNnSc1d1	tradiční
označení	označení	k1gNnSc1	označení
východních	východní	k2eAgFnPc2d1	východní
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
