<s>
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
Balaenoptera	Balaenopter	k1gMnSc2	Balaenopter
musculus	musculus	k1gMnSc1	musculus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
také	také	k9	také
'	'	kIx"	'
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
velryba	velryba	k1gFnSc1	velryba
<g/>
'	'	kIx"	'
anglicky	anglicky	k6eAd1	anglicky
Blue	Blue	k1gFnSc1	Blue
whale	whale	k1gFnSc2	whale
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Blauwal	Blauwal	k1gInSc1	Blauwal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořský	mořský	k2eAgMnSc1d1	mořský
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největšího	veliký	k2eAgMnSc2d3	veliký
současného	současný	k2eAgMnSc2d1	současný
žijícího	žijící	k2eAgMnSc2d1	žijící
živočicha	živočich	k1gMnSc2	živočich
a	a	k8xC	a
podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
informací	informace	k1gFnPc2	informace
i	i	k9	i
o	o	k7c4	o
největšího	veliký	k2eAgMnSc4d3	veliký
živočicha	živočich	k1gMnSc4	živočich
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
