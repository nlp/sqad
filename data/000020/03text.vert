<s>
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
vyrovnávací	vyrovnávací	k2eAgFnSc7d1	vyrovnávací
nádrží	nádrž	k1gFnSc7	nádrž
Vír	Vír	k1gInSc4	Vír
II	II	kA	II
pod	pod	k7c7	pod
Vírem	Vír	k1gInSc7	Vír
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
či	či	k8xC	či
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svědčí	svědčit	k5eAaImIp3nS	svědčit
jeho	jeho	k3xOp3gFnSc1	jeho
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zřícenina	zřícenina	k1gFnSc1	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
uváděný	uváděný	k2eAgInSc1d1	uváděný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nárůstu	nárůst	k1gInSc2	nárůst
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
nutnosti	nutnost	k1gFnSc2	nutnost
nových	nový	k2eAgNnPc2d1	nové
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
opodstatněný	opodstatněný	k2eAgMnSc1d1	opodstatněný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
není	být	k5eNaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
výrazný	výrazný	k2eAgInSc1d1	výrazný
nárůst	nárůst	k1gInSc1	nárůst
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsahu	rozsah	k1gInSc6	rozsah
jejich	jejich	k3xOp3gFnPc2	jejich
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
držav	država	k1gFnPc2	država
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
možno	možno	k6eAd1	možno
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
náleželo	náležet	k5eAaImAgNnS	náležet
území	území	k1gNnSc1	území
od	od	k7c2	od
Doubravníku	Doubravník	k1gInSc2	Doubravník
až	až	k9	až
k	k	k7c3	k
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
vrcholům	vrchol	k1gInPc3	vrchol
Žďárských	Žďárských	k2eAgInSc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
Pernštejnové	Pernštejnové	k2eAgInSc1d1	Pernštejnové
název	název	k1gInSc1	název
hradu	hrad	k1gInSc2	hrad
nikdy	nikdy	k6eAd1	nikdy
neužili	užít	k5eNaPmAgMnP	užít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
predikátu	predikát	k1gInSc6	predikát
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
přímý	přímý	k2eAgInSc1d1	přímý
vztah	vztah	k1gInSc1	vztah
jejich	jejich	k3xOp3gInPc2	jejich
členů	člen	k1gInPc2	člen
k	k	k7c3	k
lokalitě	lokalita	k1gFnSc3	lokalita
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Pyšolci	Pyšolec	k1gInSc6	Pyšolec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
Filipem	Filip	k1gMnSc7	Filip
mladším	mladý	k2eAgFnPc3d2	mladší
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
prodán	prodat	k5eAaPmNgInS	prodat
Ješkovi	Ješek	k1gMnSc3	Ješek
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
a	a	k8xC	a
Ješkovi	Ješkův	k2eAgMnPc1d1	Ješkův
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
a	a	k8xC	a
Ješek	Ješek	k1gMnSc1	Ješek
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
spolu	spolu	k6eAd1	spolu
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
připadne	připadnout	k5eAaPmIp3nS	připadnout
část	část	k1gFnSc4	část
panství	panství	k1gNnSc2	panství
tomu	ten	k3xDgNnSc3	ten
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
však	však	k9	však
Ješek	Ješek	k6eAd1	Ješek
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1358	[number]	k4	1358
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
územím	území	k1gNnSc7	území
až	až	k9	až
k	k	k7c3	k
říčce	říčka	k1gFnSc3	říčka
Bystřičce	bystřička	k1gFnSc3	bystřička
vyměněn	vyměněn	k2eAgMnSc1d1	vyměněn
za	za	k7c4	za
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
Dunajovicích	Dunajovice	k1gFnPc6	Dunajovice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
markrabě	markrabě	k1gMnSc1	markrabě
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
koupil	koupit	k5eAaPmAgMnS	koupit
i	i	k9	i
část	část	k1gFnSc4	část
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
patřilo	patřit	k5eAaImAgNnS	patřit
Ješkovi	Ješek	k1gMnSc3	Ješek
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zájem	zájem	k1gInSc1	zájem
markraběte	markrabě	k1gMnSc2	markrabě
bývá	bývat	k5eAaImIp3nS	bývat
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
jako	jako	k8xS	jako
snaha	snaha	k1gFnSc1	snaha
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
původní	původní	k2eAgFnSc4d1	původní
markraběcí	markraběcí	k2eAgFnSc4d1	markraběcí
doménu	doména	k1gFnSc4	doména
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
předchozího	předchozí	k2eAgNnSc2d1	předchozí
století	století	k1gNnSc2	století
rozchvácenou	rozchvácený	k2eAgFnSc7d1	rozchvácený
okolní	okolní	k2eAgFnSc7d1	okolní
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
patřil	patřit	k5eAaImAgInS	patřit
hrad	hrad	k1gInSc1	hrad
Joštovi	Jošt	k1gMnSc3	Jošt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
dal	dát	k5eAaPmAgInS	dát
jako	jako	k9	jako
léno	léno	k1gNnSc4	léno
Heraltovi	Heralt	k1gMnSc6	Heralt
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
projevili	projevit	k5eAaPmAgMnP	projevit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
snahu	snaha	k1gFnSc4	snaha
získat	získat	k5eAaPmF	získat
hrad	hrad	k1gInSc4	hrad
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hrad	hrad	k1gInSc1	hrad
příčinou	příčina	k1gFnSc7	příčina
sporu	spor	k1gInSc6	spor
mezi	mezi	k7c7	mezi
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
majitelem	majitel	k1gMnSc7	majitel
Bludem	blud	k1gInSc7	blud
z	z	k7c2	z
Kralic	Kralice	k1gFnPc2	Kralice
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1406	[number]	k4	1406
od	od	k7c2	od
Heralta	Heralt	k1gInSc2	Heralt
mladšího	mladý	k2eAgInSc2d2	mladší
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
v	v	k7c6	v
zástavu	zástav	k1gInSc6	zástav
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1417	[number]	k4	1417
musel	muset	k5eAaImAgInS	muset
Boček	boček	k1gInSc1	boček
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
exekuce	exekuce	k1gFnSc2	exekuce
přenechat	přenechat	k5eAaPmF	přenechat
Pyšolec	Pyšolec	k1gInSc4	Pyšolec
Janovi	Jan	k1gMnSc3	Jan
mladší	mladý	k2eAgFnSc2d2	mladší
z	z	k7c2	z
Bítova	Bítov	k1gInSc2	Bítov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
15	[number]	k4	15
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
markrabě	markrabě	k1gMnSc1	markrabě
Albrecht	Albrecht	k1gMnSc1	Albrecht
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
předal	předat	k5eAaPmAgMnS	předat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zubštejnem	Zubštejn	k1gInSc7	Zubštejn
Janovi	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1446	[number]	k4	1446
přešel	přejít	k5eAaPmAgInS	přejít
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
opět	opět	k6eAd1	opět
pod	pod	k7c7	pod
Pernštejny	Pernštejn	k1gInPc7	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
zbořený	zbořený	k2eAgMnSc1d1	zbořený
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nový	nový	k2eAgMnSc1d1	nový
majitel	majitel	k1gMnSc1	majitel
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
opravit	opravit	k5eAaPmF	opravit
a	a	k8xC	a
stavebně	stavebně	k6eAd1	stavebně
upravit	upravit	k5eAaPmF	upravit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prací	práce	k1gFnPc2	práce
svého	svůj	k3xOyFgInSc2	svůj
pokusu	pokus	k1gInSc2	pokus
zanechal	zanechat	k5eAaPmAgInS	zanechat
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
zůstal	zůstat	k5eAaPmAgInS	zůstat
již	již	k6eAd1	již
trvale	trvale	k6eAd1	trvale
opuštěn	opuštěn	k2eAgInSc1d1	opuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
stopy	stopa	k1gFnPc1	stopa
paláce	palác	k1gInSc2	palác
s	s	k7c7	s
obvodovými	obvodový	k2eAgInPc7d1	obvodový
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
válcové	válcový	k2eAgFnSc2d1	válcová
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
asi	asi	k9	asi
10	[number]	k4	10
m	m	kA	m
<g/>
)	)	kIx)	)
s	s	k7c7	s
patrnými	patrný	k2eAgInPc7d1	patrný
zbytky	zbytek	k1gInPc7	zbytek
středové	středový	k2eAgFnSc2d1	středová
šachty	šachta	k1gFnSc2	šachta
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
asi	asi	k9	asi
1,2	[number]	k4	1,2
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
vedla	vést	k5eAaImAgFnS	vést
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
až	až	k9	až
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
příkopu	příkop	k1gInSc2	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
měl	mít	k5eAaImAgInS	mít
ovšem	ovšem	k9	ovšem
3	[number]	k4	3
příkopy	příkop	k1gInPc1	příkop
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgFnPc1d1	oddělující
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
rozlehlého	rozlehlý	k2eAgInSc2d1	rozlehlý
hradu	hrad	k1gInSc2	hrad
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgInPc1d1	patrný
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
příkop	příkop	k1gInSc1	příkop
má	mít	k5eAaImIp3nS	mít
impozantní	impozantní	k2eAgFnSc4d1	impozantní
hloubku	hloubka	k1gFnSc4	hloubka
přes	přes	k7c4	přes
16	[number]	k4	16
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
archeologických	archeologický	k2eAgInPc2d1	archeologický
výzkumů	výzkum	k1gInPc2	výzkum
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
výrazné	výrazný	k2eAgFnSc2d1	výrazná
terasy	terasa	k1gFnSc2	terasa
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
výrazné	výrazný	k2eAgInPc1d1	výrazný
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
středověkého	středověký	k2eAgInSc2d1	středověký
výrobního	výrobní	k2eAgInSc2d1	výrobní
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
vévodí	vévodit	k5eAaImIp3nP	vévodit
především	především	k6eAd1	především
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
zdiva	zdivo	k1gNnSc2	zdivo
vápenické	vápenický	k2eAgFnSc2d1	vápenická
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
vápenka	vápenka	k1gFnSc1	vápenka
odborně	odborně	k6eAd1	odborně
archeologicky	archeologicky	k6eAd1	archeologicky
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
a	a	k8xC	a
zdokumentována	zdokumentován	k2eAgFnSc1d1	zdokumentována
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zakonzervování	zakonzervování	k1gNnSc2	zakonzervování
jejího	její	k3xOp3gNnSc2	její
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Poláček	Poláček	k1gMnSc1	Poláček
<g/>
,	,	kIx,	,
Feudální	feudální	k2eAgNnPc4d1	feudální
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Bystřice	Bystřice	k1gFnSc2	Bystřice
<g/>
,	,	kIx,	,
Nedvědičky	Nedvědička	k1gFnSc2	Nedvědička
a	a	k8xC	a
Bobrůvky	Bobrůvka	k1gFnSc2	Bobrůvka
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Pravěké	pravěký	k2eAgNnSc1d1	pravěké
a	a	k8xC	a
slovanské	slovanský	k2eAgNnSc1d1	slovanské
osídlení	osídlení	k1gNnSc1	osídlení
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
s.	s.	k?	s.
419	[number]	k4	419
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Endlicherová	Endlicherová	k1gFnSc1	Endlicherová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Korbička	korbička	k1gFnSc1	korbička
<g/>
,	,	kIx,	,
Středověká	středověký	k2eAgFnSc1d1	středověká
vápenická	vápenický	k2eAgFnSc1d1	vápenická
pec	pec	k1gFnSc1	pec
u	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
Pyšolce	Pyšolec	k1gInSc2	Pyšolec
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
sborník	sborník	k1gInSc1	sborník
Bystřicka	Bystřicka	k1gFnSc1	Bystřicka
I.	I.	kA	I.
Bystřice	Bystřice	k1gFnSc1	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
s.	s.	k?	s.
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
103	[number]	k4	103
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pyšolec	Pyšolec	k1gInSc4	Pyšolec
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Pyšolec	Pyšolec	k1gMnSc1	Pyšolec
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
