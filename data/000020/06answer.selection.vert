<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Pyšolci	Pyšolec	k1gInSc6	Pyšolec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
Filipem	Filip	k1gMnSc7	Filip
mladším	mladý	k2eAgFnPc3d2	mladší
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
prodán	prodat	k5eAaPmNgInS	prodat
Ješkovi	Ješek	k1gMnSc3	Ješek
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
a	a	k8xC	a
Ješkovi	Ješkův	k2eAgMnPc1d1	Ješkův
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
