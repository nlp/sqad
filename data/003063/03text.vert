<s>
V	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
je	být	k5eAaImIp3nS	být
foton	foton	k1gInSc1	foton
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
φ	φ	k?	φ
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
<g/>
)	)	kIx)	)
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
popisujeme	popisovat	k5eAaImIp1nP	popisovat
kvantum	kvantum	k1gNnSc1	kvantum
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
značen	značen	k2eAgInSc1d1	značen
řeckým	řecký	k2eAgNnSc7d1	řecké
písmenem	písmeno	k1gNnSc7	písmeno
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gama	gama	k1gNnSc1	gama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
zprostředkující	zprostředkující	k2eAgFnSc4d1	zprostředkující
elektromagnetickou	elektromagnetický	k2eAgFnSc4d1	elektromagnetická
interakci	interakce	k1gFnSc4	interakce
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
intermediální	intermediální	k2eAgFnSc1d1	intermediální
částice	částice	k1gFnSc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
studiem	studio	k1gNnSc7	studio
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Willis	Willis	k1gInSc1	Willis
Eugene	Eugen	k1gInSc5	Eugen
Lamb	Lamb	k1gInSc4	Lamb
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
foton	foton	k1gInSc1	foton
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zavedl	zavést	k5eAaPmAgInS	zavést
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
chemik	chemik	k1gMnSc1	chemik
Gilbert	Gilbert	k1gMnSc1	Gilbert
Newton	Newton	k1gMnSc1	Newton
Lewis	Lewis	k1gFnPc2	Lewis
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
až	až	k9	až
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
co	co	k9	co
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
flogiston	flogiston	k1gInSc4	flogiston
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc4	všechen
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
od	od	k7c2	od
radiových	radiový	k2eAgFnPc2d1	radiová
vln	vlna	k1gFnPc2	vlna
po	po	k7c6	po
záření	záření	k1gNnSc6	záření
gama	gama	k1gNnSc2	gama
je	být	k5eAaImIp3nS	být
kvantováno	kvantován	k2eAgNnSc1d1	kvantován
na	na	k7c4	na
fotony	foton	k1gInPc4	foton
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
popisuje	popisovat	k5eAaImIp3nS	popisovat
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
hybnost	hybnost	k1gFnSc1	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Životnost	životnost	k1gFnSc1	životnost
fotonu	foton	k1gInSc2	foton
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
nekonečného	konečný	k2eNgInSc2d1	nekonečný
poločasu	poločas	k1gInSc2	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
stabilní	stabilní	k2eAgFnSc7d1	stabilní
částicí	částice	k1gFnSc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Fotony	foton	k1gInPc1	foton
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
a	a	k8xC	a
zanikat	zanikat	k5eAaImF	zanikat
při	při	k7c6	při
interakcích	interakce	k1gFnPc6	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Částicové	částicový	k2eAgFnPc1d1	částicová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
především	především	k9	především
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
frekvencích	frekvence	k1gFnPc6	frekvence
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
energiích	energie	k1gFnPc6	energie
fotonů	foton	k1gInPc2	foton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
převažují	převažovat	k5eAaImIp3nP	převažovat
vlnové	vlnový	k2eAgFnPc4d1	vlnová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
fotonu	foton	k1gInSc2	foton
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
má	mít	k5eAaImIp3nS	mít
spin	spin	k1gInSc1	spin
roven	roven	k2eAgInSc1d1	roven
1	[number]	k4	1
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
boson	boson	k1gInSc4	boson
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nobelistů	nobelista	k1gMnPc2	nobelista
jako	jako	k8xC	jako
Willis	Willis	k1gFnPc2	Willis
Eugene	Eugen	k1gInSc5	Eugen
Lamb	Lamb	k1gMnSc1	Lamb
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Hard	Hard	k1gMnSc1	Hard
Townes	Townes	k1gMnSc1	Townes
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
foton	foton	k1gInSc1	foton
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
reálnou	reálný	k2eAgFnSc4d1	reálná
částici	částice	k1gFnSc4	částice
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
fonon	fonon	k1gInSc1	fonon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
postulátem	postulát	k1gInSc7	postulát
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
)	)	kIx)	)
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
nulovou	nulový	k2eAgFnSc4d1	nulová
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
jeho	jeho	k3xOp3gInSc2	jeho
neustálého	neustálý	k2eAgInSc2d1	neustálý
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
však	však	k9	však
nenulová	nulový	k2eNgFnSc1d1	nenulová
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
h	h	k?	h
f	f	k?	f
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
c	c	k0	c
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
hf	hf	k?	hf
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
hc	hc	k?	hc
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
relativistického	relativistický	k2eAgInSc2d1	relativistický
vztahu	vztah	k1gInSc2	vztah
ekvivalence	ekvivalence	k1gFnSc2	ekvivalence
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
m	m	kA	m
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
lze	lze	k6eAd1	lze
fotonu	foton	k1gInSc3	foton
přiřadit	přiřadit	k5eAaPmF	přiřadit
také	také	k9	také
určitou	určitý	k2eAgFnSc4d1	určitá
hmotnost	hmotnost	k1gFnSc4	hmotnost
(	(	kIx(	(
<g/>
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc1d1	projevující
se	s	k7c7	s
setrvačnými	setrvačný	k2eAgFnPc7d1	setrvačná
i	i	k8xC	i
gravitačními	gravitační	k2eAgFnPc7d1	gravitační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
foton	foton	k1gInSc4	foton
působí	působit	k5eAaImIp3nS	působit
gravitace	gravitace	k1gFnSc1	gravitace
dle	dle	k7c2	dle
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
gravitačně	gravitačně	k6eAd1	gravitačně
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
byly	být	k5eAaImAgInP	být
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
pozorováním	pozorování	k1gNnSc7	pozorování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pozorovaným	pozorovaný	k2eAgInSc7d1	pozorovaný
ohybem	ohyb	k1gInSc7	ohyb
záření	záření	k1gNnSc2	záření
kolem	kolem	k7c2	kolem
kosmických	kosmický	k2eAgNnPc2d1	kosmické
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
relativistického	relativistický	k2eAgInSc2d1	relativistický
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
částice	částice	k1gFnSc2	částice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
a	a	k8xC	a
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
fotonu	foton	k1gInSc2	foton
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
lze	lze	k6eAd1	lze
hybnost	hybnost	k1gFnSc1	hybnost
fotonu	foton	k1gInSc2	foton
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
f	f	k?	f
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
E	E	kA	E
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
hf	hf	k?	hf
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
fotonu	foton	k1gInSc2	foton
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
určit	určit	k5eAaPmF	určit
jeho	jeho	k3xOp3gFnSc4	jeho
relativistickou	relativistický	k2eAgFnSc4d1	relativistická
hmotnost	hmotnost	k1gFnSc4	hmotnost
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
uvážíme	uvážet	k5eAaImIp1nP	uvážet
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
m	m	kA	m
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
}	}	kIx)	}
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
c	c	k0	c
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
hf	hf	k?	hf
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
:	:	kIx,	:
Fotony	foton	k1gInPc1	foton
vznikají	vznikat	k5eAaImIp3nP	vznikat
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyzářením	vyzáření	k1gNnSc7	vyzáření
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
elektronu	elektron	k1gInSc2	elektron
mezi	mezi	k7c7	mezi
orbitálními	orbitální	k2eAgFnPc7d1	orbitální
hladinami	hladina	k1gFnPc7	hladina
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
anihilaci	anihilace	k1gFnSc6	anihilace
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
přístroje	přístroj	k1gInPc1	přístroj
jako	jako	k8xC	jako
maser	maser	k1gInSc1	maser
a	a	k8xC	a
laser	laser	k1gInSc1	laser
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
koherentní	koherentní	k2eAgInSc4d1	koherentní
svazek	svazek	k1gInSc4	svazek
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
interakce	interakce	k1gFnSc1	interakce
Elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
Intermediální	intermediální	k2eAgFnSc2d1	intermediální
částice	částice	k1gFnSc2	částice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
foton	foton	k1gInSc1	foton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
foton	foton	k1gInSc1	foton
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
