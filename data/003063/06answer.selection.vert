<s>
Foton	foton	k1gInSc1	foton
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
postulátem	postulát	k1gInSc7	postulát
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
)	)	kIx)	)
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
