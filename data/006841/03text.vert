<s>
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Acerra	Acerro	k1gNnSc2	Acerro
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Riccardo	Riccardo	k1gNnSc1	Riccardo
di	di	k?	di
Acerra	Acerr	k1gInSc2	Acerr
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1196	[number]	k4	1196
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Acerra	Acerro	k1gNnSc2	Acerro
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Rogera	Rogera	k1gFnSc1	Rogera
z	z	k7c2	z
Acerra	Acerr	k1gInSc2	Acerr
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
sicilské	sicilský	k2eAgFnSc2d1	sicilská
královny	královna	k1gFnSc2	královna
Sibyly	Sibyla	k1gFnSc2	Sibyla
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
svého	svůj	k3xOyFgMnSc4	svůj
švagra	švagr	k1gMnSc4	švagr
Tankreda	Tankred	k1gMnSc4	Tankred
během	během	k7c2	během
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
sicilské	sicilský	k2eAgFnSc2d1	sicilská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgInS	bojovat
se	se	k3xPyFc4	se
straníky	straník	k1gMnPc7	straník
štaufské	štaufský	k2eAgFnSc2d1	štaufská
kandidatury	kandidatura	k1gFnSc2	kandidatura
na	na	k7c4	na
sicilský	sicilský	k2eAgInSc4d1	sicilský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1190	[number]	k4	1190
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
podařilo	podařit	k5eAaPmAgNnS	podařit
zajmout	zajmout	k5eAaPmF	zajmout
Rogera	Roger	k1gMnSc4	Roger
z	z	k7c2	z
Andrie	Andrie	k1gFnSc2	Andrie
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
pretendenta	pretendent	k1gMnSc2	pretendent
sicilského	sicilský	k2eAgInSc2d1	sicilský
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
jako	jako	k9	jako
přeběhlíka	přeběhlík	k1gMnSc4	přeběhlík
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jej	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Tankredově	Tankredův	k2eAgFnSc6d1	Tankredův
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1194	[number]	k4	1194
nebyla	být	k5eNaImAgFnS	být
královna	královna	k1gFnSc1	královna
Sibyla	Sibyla	k1gFnSc1	Sibyla
jako	jako	k8xC	jako
regentka	regentka	k1gFnSc1	regentka
nezletilého	nezletilý	k1gMnSc2	nezletilý
syna	syn	k1gMnSc2	syn
Viléma	Vilém	k1gMnSc2	Vilém
schopná	schopný	k2eAgFnSc1d1	schopná
ubránit	ubránit	k5eAaPmF	ubránit
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Sicílie	Sicílie	k1gFnSc1	Sicílie
se	se	k3xPyFc4	se
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c4	na
vánoce	vánoce	k1gFnPc4	vánoce
1194	[number]	k4	1194
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
slavnostně	slavnostně	k6eAd1	slavnostně
korunován	korunován	k2eAgMnSc1d1	korunován
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
nechal	nechat	k5eAaPmAgMnS	nechat
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
spiknutí	spiknutí	k1gNnSc2	spiknutí
zajmout	zajmout	k5eAaPmF	zajmout
Sibylu	Sibyla	k1gFnSc4	Sibyla
i	i	k9	i
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
významnými	významný	k2eAgMnPc7d1	významný
muži	muž	k1gMnPc7	muž
země	zem	k1gFnSc2	zem
nechal	nechat	k5eAaPmAgMnS	nechat
odvézt	odvézt	k5eAaPmF	odvézt
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zrazen	zradit	k5eAaPmNgMnS	zradit
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
císařova	císařův	k2eAgMnSc2d1	císařův
muže	muž	k1gMnSc2	muž
Děpolta	Děpolt	k1gMnSc2	Děpolt
z	z	k7c2	z
Vohburgu	Vohburg	k1gInSc2	Vohburg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1196	[number]	k4	1196
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
rozsudku	rozsudek	k1gInSc2	rozsudek
od	od	k7c2	od
samotného	samotný	k2eAgMnSc2d1	samotný
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
tažen	táhnout	k5eAaImNgInS	táhnout
koňmi	kůň	k1gMnPc7	kůň
ulicemi	ulice	k1gFnPc7	ulice
Capuy	Capua	k1gFnPc4	Capua
a	a	k8xC	a
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
živý	živý	k1gMnSc1	živý
pověšen	pověsit	k5eAaPmNgMnS	pověsit
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
agónie	agónie	k1gFnSc2	agónie
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
stále	stále	k6eAd1	stále
naživu	naživu	k6eAd1	naživu
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Ansbert	Ansbert	k1gMnSc1	Ansbert
zase	zase	k9	zase
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
císař	císař	k1gMnSc1	císař
nechal	nechat	k5eAaPmAgMnS	nechat
nejdříve	dříve	k6eAd3	dříve
pověsit	pověsit	k5eAaPmF	pověsit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
stít	stít	k5eAaPmF	stít
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
hlavu	hlava	k1gFnSc4	hlava
vztyčit	vztyčit	k5eAaPmF	vztyčit
na	na	k7c6	na
bráně	brána	k1gFnSc6	brána
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
hrabství	hrabství	k1gNnSc4	hrabství
získal	získat	k5eAaPmAgMnS	získat
Děpolt	Děpolt	k1gInSc4	Děpolt
z	z	k7c2	z
Vohburgu	Vohburg	k1gInSc2	Vohburg
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Acerry	Acerra	k1gFnSc2	Acerra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
