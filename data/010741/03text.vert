<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
frontman	frontman	k1gMnSc1	frontman
valašskomeziříčské	valašskomeziříčský	k2eAgFnSc2d1	valašskomeziříčská
skupiny	skupina	k1gFnSc2	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
a	a	k8xC	a
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
skupiny	skupina	k1gFnSc2	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Ždorp	Ždorp	k1gInSc1	Ždorp
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
texty	text	k1gInPc4	text
a	a	k8xC	a
organizoval	organizovat	k5eAaBmAgMnS	organizovat
zvukové	zvukový	k2eAgFnPc4d1	zvuková
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
u	u	k7c2	u
Fialů	Fiala	k1gMnPc2	Fiala
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
depresivní	depresivní	k2eAgFnPc4d1	depresivní
nálady	nálada	k1gFnPc4	nálada
a	a	k8xC	a
neschopnosti	neschopnost	k1gFnPc4	neschopnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
se	se	k3xPyFc4	se
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
zvyklostem	zvyklost	k1gFnPc3	zvyklost
pobýval	pobývat	k5eAaImAgMnS	pobývat
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkal	potkat	k5eAaPmAgInS	potkat
zpěváka	zpěvák	k1gMnSc4	zpěvák
a	a	k8xC	a
textaře	textař	k1gMnSc4	textař
Jindru	Jindra	k1gMnSc4	Jindra
Spilku	Spilka	k1gMnSc4	Spilka
a	a	k8xC	a
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaBmAgMnS	napsat
řadu	řada	k1gFnSc4	řada
nyní	nyní	k6eAd1	nyní
velmi	velmi	k6eAd1	velmi
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
17	[number]	k4	17
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Mňágy	Mňág	k1gInPc4	Mňág
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
sólový	sólový	k2eAgInSc1d1	sólový
projekt	projekt	k1gInSc1	projekt
Jonáš	Jonáš	k1gMnSc1	Jonáš
Hvězda	Hvězda	k1gMnSc1	Hvězda
-	-	kIx~	-
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
pseudonymem	pseudonym	k1gInSc7	pseudonym
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
festivalech	festival	k1gInPc6	festival
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
i	i	k8xC	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
recitoval	recitovat	k5eAaImAgMnS	recitovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
písničky	písnička	k1gFnPc4	písnička
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
debutová	debutový	k2eAgFnSc1d1	debutová
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
deska	deska	k1gFnSc1	deska
Mňágy	Mňága	k1gFnSc2	Mňága
Made	Mad	k1gFnSc2	Mad
in	in	k?	in
Valmez	Valmez	k1gMnSc1	Valmez
<g/>
.	.	kIx.	.
</s>
<s>
Svého	své	k1gNnSc2	své
textařského	textařský	k2eAgNnSc2d1	textařské
a	a	k8xC	a
skladatelského	skladatelský	k2eAgNnSc2d1	skladatelské
nadání	nadání	k1gNnSc2	nadání
využil	využít	k5eAaPmAgInS	využít
při	při	k7c6	při
napsání	napsání	k1gNnSc6	napsání
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
Z	z	k7c2	z
nejhoršího	zlý	k2eAgMnSc2d3	nejhorší
jsme	být	k5eAaImIp1nP	být
uvnitř	uvnitř	k6eAd1	uvnitř
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
Nečum	čumět	k5eNaImRp2nS	čumět
a	a	k8xC	a
tleskej	tleskat	k5eAaImRp2nS	tleskat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2002	[number]	k4	2002
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
Mňágou	Mňága	k1gFnSc7	Mňága
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
především	především	k9	především
čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
slovenští	slovenský	k2eAgMnPc1d1	slovenský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
anglické	anglický	k2eAgFnSc2d1	anglická
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
klubu	klub	k1gInSc2	klub
Rotes	Rotes	k1gMnSc1	Rotes
Berlin	berlina	k1gFnPc2	berlina
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Svobodovou	Svobodová	k1gFnSc7	Svobodová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
až	až	k9	až
2011	[number]	k4	2011
moderoval	moderovat	k5eAaBmAgInS	moderovat
v	v	k7c6	v
ČT	ČT	kA	ČT
hudební	hudební	k2eAgInSc1d1	hudební
pořad	pořad	k1gInSc1	pořad
ladí	ladit	k5eAaImIp3nS	ladit
neladí	ladit	k5eNaImIp3nP	ladit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
napsal	napsat	k5eAaBmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Petr	Petr	k1gMnSc1	Petr
píše	psát	k5eAaImIp3nS	psát
:	:	kIx,	:
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Surikata	surikata	k1gFnSc1	surikata
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
vydal	vydat	k5eAaPmAgInS	vydat
dohromady	dohromady	k6eAd1	dohromady
13	[number]	k4	13
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zubří	zubří	k2eAgFnSc1d1	zubří
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
-	-	kIx~	-
Made	Mad	k1gInSc2	Mad
in	in	k?	in
Valmez	Valmez	k1gMnSc1	Valmez
(	(	kIx(	(
<g/>
N.	N.	kA	N.
<g/>
A.	A.	kA	A.
<g/>
R.	R.	kA	R.
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
;	;	kIx,	;
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Monitor	monitor	k1gInSc1	monitor
-	-	kIx~	-
reedice	reedice	k1gFnSc1	reedice
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
-	-	kIx~	-
Furt	Furt	k?	Furt
rovně	roveň	k1gFnSc2	roveň
(	(	kIx(	(
<g/>
BMG	BMG	kA	BMG
Ariola	Ariola	k1gFnSc1	Ariola
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
;	;	kIx,	;
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Monitor	monitor	k1gInSc1	monitor
-	-	kIx~	-
reedice	reedice	k1gFnSc1	reedice
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
-	-	kIx~	-
Radost	radost	k1gFnSc1	radost
až	až	k9	až
na	na	k7c4	na
kost	kost	k1gFnSc4	kost
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
-	-	kIx~	-
Valmez	Valmez	k1gInSc1	Valmez
rock	rock	k1gInSc1	rock
city	city	k1gFnSc1	city
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
-	-	kIx~	-
Ryzí	ryzí	k2eAgNnSc1d1	ryzí
zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
-	-	kIx~	-
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Chceš	chtít	k5eAaImIp2nS	chtít
mě	já	k3xPp1nSc4	já
<g/>
?	?	kIx.	?
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
Tě	ty	k3xPp2nSc4	ty
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Nic	nic	k6eAd1	nic
složitýho	složitýho	k?	složitýho
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Web	web	k1gInSc1	web
site	sit	k1gFnSc2	sit
story	story	k1gFnSc2	story
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Dutý	dutý	k2eAgInSc1d1	dutý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
free	free	k1gFnSc1	free
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
polární	polární	k2eAgFnPc1d1	polární
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Takže	takže	k8xS	takže
dobrý	dobrý	k2eAgInSc1d1	dobrý
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Made	Mad	k1gInSc2	Mad
in	in	k?	in
China	China	k1gFnSc1	China
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
-	-	kIx~	-
Třínohý	třínohý	k2eAgInSc1d1	třínohý
pessólová	pessólový	k2eAgNnPc1d1	pessólový
alba	album	k1gNnPc4	album
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
-	-	kIx~	-
Nečum	čumět	k5eNaImRp2nS	čumět
a	a	k8xC	a
tleskej	tleskat	k5eAaImRp2nS	tleskat
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
/	/	kIx~	/
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
<g/>
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Slepé	slepý	k2eAgNnSc4d1	slepé
střevo	střevo	k1gNnSc4	střevo
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
-	-	kIx~	-
Nečum	čumět	k5eNaImRp2nS	čumět
a	a	k8xC	a
tleskej	tleskat	k5eAaImRp2nS	tleskat
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ANNE	ANNE	kA	ANNE
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Mňága	Mňága	k1gFnSc1	Mňága
a	a	k8xC	a
Žďorp	Žďorp	k1gInSc1	Žďorp
</s>
</p>
<p>
<s>
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Fialou	Fiala	k1gMnSc7	Fiala
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
