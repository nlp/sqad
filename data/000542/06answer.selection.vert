<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Μ	Μ	k?	Μ
<g/>
,	,	kIx,	,
Methodios	Methodios	k1gInSc1	Methodios
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Methodius	Methodius	k1gInSc1	Methodius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
813	[number]	k4	813
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
885	[number]	k4	885
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
byzantským	byzantský	k2eAgMnSc7d1	byzantský
státním	státní	k2eAgMnSc7d1	státní
úředníkem	úředník	k1gMnSc7	úředník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
mnichem	mnich	k1gInSc7	mnich
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
