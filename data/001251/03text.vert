<s>
Franklin	Franklin	k1gInSc1	Franklin
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
32	[number]	k4	32
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgMnS	zvolit
byl	být	k5eAaImAgMnS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
osobnosti	osobnost	k1gFnPc4	osobnost
historie	historie	k1gFnSc2	historie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
bratranec	bratranec	k1gMnSc1	bratranec
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Harvard	Harvard	k1gInSc1	Harvard
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
senátorem	senátor	k1gMnSc7	senátor
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
náměstkem	náměstek	k1gMnSc7	náměstek
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
třiadvaceti	třiadvacet	k4xCc6	třiadvacet
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Eleonorou	Eleonora	k1gFnSc7	Eleonora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
obrnou	obrna	k1gFnSc7	obrna
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ochrnul	ochrnout	k5eAaPmAgInS	ochrnout
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
někdy	někdy	k6eAd1	někdy
používal	používat	k5eAaImAgMnS	používat
kolečkové	kolečkový	k2eAgNnSc4d1	kolečkové
křeslo	křeslo	k1gNnSc4	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
guvernérem	guvernér	k1gMnSc7	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zahájil	zahájit	k5eAaPmAgMnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
demokratickým	demokratický	k2eAgMnSc7d1	demokratický
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
plán	plán	k1gInSc4	plán
New	New	k1gFnSc3	New
Deal	Deal	k1gInSc1	Deal
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
úděl	úděl	k1gInSc1	úděl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c6	na
něj	on	k3xPp3gMnSc2	on
Giuseppe	Giusepp	k1gInSc5	Giusepp
Zangara	Zangar	k1gMnSc4	Zangar
spáchal	spáchat	k5eAaPmAgInS	spáchat
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
starosta	starosta	k1gMnSc1	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
přísahy	přísaha	k1gFnSc2	přísaha
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
pronesl	pronést	k5eAaPmAgInS	pronést
historický	historický	k2eAgInSc1d1	historický
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
požádal	požádat	k5eAaPmAgMnS	požádat
Američany	Američan	k1gMnPc4	Američan
o	o	k7c4	o
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jeho	jeho	k3xOp3gInSc1	jeho
projev	projev	k1gInSc1	projev
byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
dobrý	dobrý	k2eAgInSc1d1	dobrý
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
předznamenal	předznamenat	k5eAaPmAgInS	předznamenat
tak	tak	k9	tak
začátek	začátek	k1gInSc4	začátek
obratu	obrat	k1gInSc2	obrat
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
pronášel	pronášet	k5eAaImAgMnS	pronášet
podobné	podobný	k2eAgInPc4d1	podobný
projevy	projev	k1gInPc4	projev
pravidelně	pravidelně	k6eAd1	pravidelně
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
vžil	vžít	k5eAaPmAgMnS	vžít
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
název	název	k1gInSc4	název
rozhovory	rozhovor	k1gInPc1	rozhovor
od	od	k7c2	od
krbu	krb	k1gInSc2	krb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc3	jeho
administrativa	administrativa	k1gFnSc1	administrativa
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
,	,	kIx,	,
když	když	k8xS	když
naplánovala	naplánovat	k5eAaBmAgFnS	naplánovat
skupina	skupina	k1gFnSc1	skupina
bohatých	bohatý	k2eAgMnPc2d1	bohatý
průmyslníků	průmyslník	k1gMnPc2	průmyslník
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Business	business	k1gInSc1	business
Plot	plot	k1gInSc1	plot
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
svrhnout	svrhnout	k5eAaPmF	svrhnout
Rooseveltovu	Rooseveltův	k2eAgFnSc4d1	Rooseveltova
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
hledali	hledat	k5eAaImAgMnP	hledat
konspirátoři	konspirátor	k1gMnPc1	konspirátor
důvěru	důvěra	k1gFnSc4	důvěra
u	u	k7c2	u
generálmajora	generálmajor	k1gMnSc2	generálmajor
Smedleyho	Smedley	k1gMnSc2	Smedley
Butlera	Butler	k1gMnSc2	Butler
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
však	však	k8xC	však
jejich	jejich	k3xOp3gMnSc1	jejich
plán	plán	k1gInSc4	plán
zhatil	zhatit	k5eAaPmAgMnS	zhatit
a	a	k8xC	a
o	o	k7c6	o
převratu	převrat	k1gInSc6	převrat
svědčil	svědčit	k5eAaImAgMnS	svědčit
před	před	k7c7	před
Kongresem	kongres	k1gInSc7	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
/	/	kIx~	/
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
ve	v	k7c6	v
Flintu	flint	k1gInSc6	flint
státě	stát	k1gInSc6	stát
Michigan	Michigan	k1gInSc1	Michigan
stávka	stávka	k1gFnSc1	stávka
dělníků	dělník	k1gMnPc2	dělník
vyrábějících	vyrábějící	k2eAgInPc2d1	vyrábějící
automobily	automobil	k1gInPc7	automobil
<g/>
,	,	kIx,	,
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
John	John	k1gMnSc1	John
Garner	Garner	k1gMnSc1	Garner
po	po	k7c6	po
několika	několik	k4yIc6	několik
potyčkách	potyčka	k1gFnPc6	potyčka
dělníků	dělník	k1gMnPc2	dělník
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
připravoval	připravovat	k5eAaImAgInS	připravovat
intervenci	intervence	k1gFnSc3	intervence
<g/>
,	,	kIx,	,
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
tuto	tento	k3xDgFnSc4	tento
intervenci	intervence	k1gFnSc4	intervence
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
lokálního	lokální	k2eAgInSc2d1	lokální
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
přivedla	přivést	k5eAaPmAgFnS	přivést
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
dělníky	dělník	k1gMnPc4	dělník
tohoto	tento	k3xDgInSc2	tento
průmyslu	průmysl	k1gInSc2	průmysl
do	do	k7c2	do
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
výsledku	výsledek	k1gInSc6	výsledek
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
jejich	jejich	k3xOp3gFnSc1	jejich
úroveň	úroveň	k1gFnSc1	úroveň
a	a	k8xC	a
přihrála	přihrát	k5eAaPmAgFnS	přihrát
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
politické	politický	k2eAgFnPc1d1	politická
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgInS	začít
podporovat	podporovat	k5eAaImF	podporovat
západní	západní	k2eAgMnPc4d1	západní
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
především	především	k9	především
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
např.	např.	kA	např.
vyměnily	vyměnit	k5eAaPmAgInP	vyměnit
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
50	[number]	k4	50
zastaralých	zastaralý	k2eAgInPc2d1	zastaralý
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
za	za	k7c4	za
základny	základna	k1gFnPc4	základna
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgInS	prosadit
také	také	k9	také
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc6	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
USA	USA	kA	USA
Japonskem	Japonsko	k1gNnSc7	Japonsko
v	v	k7c4	v
Pearl	Pearl	k1gInSc4	Pearl
Harboru	Harbor	k1gInSc2	Harbor
začal	začít	k5eAaPmAgInS	začít
vést	vést	k5eAaImF	vést
aktivně	aktivně	k6eAd1	aktivně
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
zemím	zem	k1gFnPc3	zem
Osy	osa	k1gFnSc2	osa
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc7	jeho
prioritou	priorita	k1gFnSc7	priorita
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
konferencí	konference	k1gFnSc7	konference
Velké	velký	k2eAgFnSc2d1	velká
Trojky	trojka	k1gFnSc2	trojka
(	(	kIx(	(
<g/>
Winston	Winston	k1gInSc1	Winston
S.	S.	kA	S.
Churchill	Churchill	k1gInSc1	Churchill
<g/>
,	,	kIx,	,
Josif	Josif	k1gMnSc1	Josif
V.	V.	kA	V.
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
Franklin	Franklin	k1gInSc1	Franklin
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
)	)	kIx)	)
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
a	a	k8xC	a
na	na	k7c6	na
jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
dobrých	dobrý	k2eAgInPc6d1	dobrý
vztazích	vztah	k1gInPc6	vztah
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
s	s	k7c7	s
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
stalinistickým	stalinistický	k2eAgNnSc7d1	stalinistické
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
poslední	poslední	k2eAgFnSc2d1	poslední
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
velmi	velmi	k6eAd1	velmi
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
fyzické	fyzický	k2eAgNnSc1d1	fyzické
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
doprovázely	doprovázet	k5eAaImAgFnP	doprovázet
také	také	k6eAd1	také
psychické	psychický	k2eAgFnPc1d1	psychická
potíže	potíž	k1gFnPc1	potíž
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
depresí	deprese	k1gFnPc2	deprese
a	a	k8xC	a
projevy	projev	k1gInPc4	projev
arteriosklerózy	arterioskleróza	k1gFnSc2	arterioskleróza
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
dle	dle	k7c2	dle
očekávání	očekávání	k1gNnSc2	očekávání
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
již	již	k6eAd1	již
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
vnímal	vnímat	k5eAaImAgInS	vnímat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
projevoval	projevovat	k5eAaImAgMnS	projevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
nápadný	nápadný	k2eAgInSc1d1	nápadný
třes	třes	k1gInSc1	třes
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
duchem	duch	k1gMnSc7	duch
zcela	zcela	k6eAd1	zcela
nepřítomen	přítomen	k2eNgMnSc1d1	nepřítomen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
katastrofálním	katastrofální	k2eAgInSc6d1	katastrofální
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
jaltskou	jaltský	k2eAgFnSc4d1	Jaltská
konferenci	konference	k1gFnSc4	konference
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
neblahém	blahý	k2eNgInSc6d1	neblahý
výsledku	výsledek	k1gInSc6	výsledek
této	tento	k3xDgFnSc2	tento
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
podepsal	podepsat	k5eAaPmAgMnS	podepsat
právě	právě	k6eAd1	právě
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
a	a	k8xC	a
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
USA	USA	kA	USA
napadán	napadán	k2eAgInSc4d1	napadán
tiskem	tisk	k1gInSc7	tisk
i	i	k8xC	i
veřejností	veřejnost	k1gFnSc7	veřejnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgFnPc1d1	západní
mocnosti	mocnost	k1gFnPc1	mocnost
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
tolik	tolik	k6eAd1	tolik
ustupovaly	ustupovat	k5eAaImAgFnP	ustupovat
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
posledním	poslední	k2eAgInSc6d1	poslední
výročním	výroční	k2eAgInSc6d1	výroční
projevu	projev	k1gInSc6	projev
národu	národ	k1gInSc2	národ
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kvůli	kvůli	k7c3	kvůli
zhoršenému	zhoršený	k2eAgInSc3d1	zhoršený
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
-	-	kIx~	-
zotavoval	zotavovat	k5eAaImAgInS	zotavovat
se	se	k3xPyFc4	se
z	z	k7c2	z
rýmy	rýma	k1gFnSc2	rýma
-	-	kIx~	-
četl	číst	k5eAaImAgMnS	číst
pro	pro	k7c4	pro
rádio	rádio	k1gNnSc4	rádio
<g/>
,	,	kIx,	,
předložil	předložit	k5eAaPmAgInS	předložit
návrh	návrh	k1gInSc1	návrh
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
jako	jako	k8xC	jako
Druhá	druhý	k4xOgFnSc1	druhý
listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Second	Second	k1gMnSc1	Second
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
listina	listina	k1gFnSc1	listina
je	být	k5eAaImIp3nS	být
též	též	k9	též
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
listina	listina	k1gFnSc1	listina
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
práv	právo	k1gNnPc2	právo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
nad	nad	k7c4	nad
stávající	stávající	k2eAgNnPc4d1	stávající
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
právo	právo	k1gNnSc4	právo
každého	každý	k3xTgMnSc2	každý
Američana	Američan	k1gMnSc2	Američan
na	na	k7c4	na
<g/>
:	:	kIx,	:
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
vzdělání	vzdělání	k1gNnSc6	vzdělání
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
životaschopným	životaschopný	k2eAgNnSc7d1	životaschopné
platovým	platový	k2eAgNnSc7d1	platové
ohodnocením	ohodnocení	k1gNnSc7	ohodnocení
(	(	kIx(	(
<g/>
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
se	s	k7c7	s
mzdou	mzda	k1gFnSc7	mzda
dostačující	dostačující	k2eAgNnSc4d1	dostačující
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
adekvátních	adekvátní	k2eAgFnPc2d1	adekvátní
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
ošacení	ošacení	k1gNnSc2	ošacení
<g/>
,	,	kIx,	,
důstojného	důstojný	k2eAgNnSc2d1	důstojné
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
záchrannou	záchranný	k2eAgFnSc7d1	záchranná
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
ožebračením	ožebračení	k1gNnSc7	ožebračení
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
stářím	stáří	k1gNnSc7	stáří
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
nehodou	nehoda	k1gFnSc7	nehoda
nebo	nebo	k8xC	nebo
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
<g/>
:	:	kIx,	:
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
ochranu	ochrana	k1gFnSc4	ochrana
(	(	kIx(	(
<g/>
penzi	penze	k1gFnSc4	penze
<g/>
)	)	kIx)	)
během	během	k7c2	během
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
úrazu	úraz	k1gInSc2	úraz
<g/>
,	,	kIx,	,
nezaměstnání	nezaměstnání	k1gNnSc2	nezaměstnání
a	a	k8xC	a
užívání	užívání	k1gNnSc2	užívání
starobního	starobní	k2eAgInSc2d1	starobní
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
netrval	trvat	k5eNaImAgMnS	trvat
na	na	k7c4	na
zanesení	zanesení	k1gNnPc4	zanesení
těchto	tento	k3xDgNnPc2	tento
práv	právo	k1gNnPc2	právo
do	do	k7c2	do
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
postupné	postupný	k2eAgFnSc6d1	postupná
politické	politický	k2eAgFnSc6d1	politická
implementaci	implementace	k1gFnSc6	implementace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Druhé	druhý	k4xOgFnSc3	druhý
listině	listina	k1gFnSc3	listina
práv	právo	k1gNnPc2	právo
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
referoval	referovat	k5eAaBmAgInS	referovat
též	též	k9	též
český	český	k2eAgMnSc1d1	český
profesor	profesor	k1gMnSc1	profesor
Karel	Karel	k1gMnSc1	Karel
Vašák	Vašák	k1gMnSc1	Vašák
jako	jako	k8xS	jako
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
generací	generace	k1gFnPc2	generace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Rooseveltův	Rooseveltův	k2eAgInSc1d1	Rooseveltův
projev	projev	k1gInSc1	projev
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dokumentaristou	dokumentarista	k1gMnSc7	dokumentarista
Michaelem	Michael	k1gMnSc7	Michael
Moorem	Moor	k1gMnSc7	Moor
a	a	k8xC	a
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
filmu	film	k1gInSc6	film
O	o	k7c6	o
kapitalismu	kapitalismus	k1gInSc6	kapitalismus
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
když	když	k8xS	když
malířka	malířka	k1gFnSc1	malířka
kreslila	kreslit	k5eAaImAgFnS	kreslit
jeho	jeho	k3xOp3gInSc4	jeho
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
okamžiku	okamžik	k1gInSc6	okamžik
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zhroucení	zhroucení	k1gNnSc1	zhroucení
hitlerovského	hitlerovský	k2eAgNnSc2d1	hitlerovské
Německa	Německo	k1gNnSc2	Německo
očekávalo	očekávat	k5eAaImAgNnS	očekávat
každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
světových	světový	k2eAgMnPc2d1	světový
státníků	státník	k1gMnPc2	státník
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
