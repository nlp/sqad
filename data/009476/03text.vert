<p>
<s>
Kalafuna	kalafuna	k1gFnSc1	kalafuna
je	být	k5eAaImIp3nS	být
destilační	destilační	k2eAgInSc4d1	destilační
zbytek	zbytek	k1gInSc4	zbytek
z	z	k7c2	z
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
borovic	borovice	k1gFnPc2	borovice
nebo	nebo	k8xC	nebo
získávaný	získávaný	k2eAgInSc1d1	získávaný
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
buničiny	buničina	k1gFnSc2	buničina
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
ionského	ionský	k2eAgNnSc2d1	ionský
města	město	k1gNnSc2	město
Kolofónu	Kolofón	k1gInSc2	Kolofón
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
proslulo	proslout	k5eAaPmAgNnS	proslout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Chiem	Chi	k1gInSc7	Chi
především	především	k9	především
vývozem	vývoz	k1gInSc7	vývoz
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
slabých	slabý	k2eAgFnPc2d1	slabá
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
je	být	k5eAaImIp3nS	být
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
izolačním	izolační	k2eAgInSc7d1	izolační
odporem	odpor	k1gInSc7	odpor
a	a	k8xC	a
netečná	tečný	k2eNgFnSc1d1	netečná
vůči	vůči	k7c3	vůči
kovům	kov	k1gInPc3	kov
<g/>
.	.	kIx.	.
</s>
<s>
Taje	tát	k5eAaImIp3nS	tát
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
tekutá	tekutý	k2eAgFnSc1d1	tekutá
při	při	k7c6	při
120	[number]	k4	120
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
horkém	horký	k2eAgInSc6d1	horký
stavu	stav	k1gInSc6	stav
reaguje	reagovat	k5eAaBmIp3nS	reagovat
jako	jako	k9	jako
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
rozrušit	rozrušit	k5eAaPmF	rozrušit
tenké	tenký	k2eAgFnPc4d1	tenká
vrstvy	vrstva	k1gFnPc4	vrstva
oxidů	oxid	k1gInPc2	oxid
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
s.	s.	k?	s.
Reakční	reakční	k2eAgFnSc1d1	reakční
schopnost	schopnost	k1gFnSc1	schopnost
kalafuny	kalafuna	k1gFnSc2	kalafuna
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
moderním	moderní	k2eAgInPc3d1	moderní
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
aktivátory	aktivátor	k1gInPc1	aktivátor
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
přísady	přísada	k1gFnPc1	přísada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
čisticí	čisticí	k2eAgFnSc4d1	čisticí
schopnost	schopnost	k1gFnSc4	schopnost
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
teplotní	teplotní	k2eAgFnSc4d1	teplotní
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
lihu	líh	k1gInSc6	líh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
pájení	pájení	k1gNnSc6	pájení
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
–	–	k?	–
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
kalafuna	kalafuna	k1gFnSc1	kalafuna
nanáší	nanášet	k5eAaImIp3nS	nanášet
na	na	k7c4	na
žíně	žíně	k1gFnPc4	žíně
smyčců	smyčec	k1gInPc2	smyčec
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
třecího	třecí	k2eAgInSc2d1	třecí
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
depilaci	depilace	k1gFnSc6	depilace
prasečích	prasečí	k2eAgFnPc2d1	prasečí
štětin	štětina	k1gFnPc2	štětina
při	při	k7c6	při
zabijačce	zabijačka	k1gFnSc6	zabijačka
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
pryže	pryž	k1gFnSc2	pryž
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
konfekční	konfekční	k2eAgFnSc2d1	konfekční
lepivosti	lepivost	k1gFnSc2	lepivost
(	(	kIx(	(
<g/>
spojování	spojování	k1gNnSc1	spojování
pryžových	pryžový	k2eAgInPc2d1	pryžový
dílů	díl	k1gInPc2	díl
k	k	k7c3	k
sobě	se	k3xPyFc3	se
před	před	k7c7	před
vulkanizací	vulkanizace	k1gFnSc7	vulkanizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pasta	pasta	k1gFnSc1	pasta
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
smísená	smísený	k2eAgFnSc1d1	smísená
z	z	k7c2	z
přírodní	přírodní	k2eAgFnSc2d1	přírodní
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
,	,	kIx,	,
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
včelího	včelí	k2eAgInSc2d1	včelí
vosku	vosk	k1gInSc2	vosk
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
i	i	k8xC	i
terpentýnu	terpentýn	k1gInSc2	terpentýn
<g/>
)	)	kIx)	)
bývala	bývat	k5eAaImAgFnS	bývat
používána	používat	k5eAaImNgFnS	používat
k	k	k7c3	k
impregnaci	impregnace	k1gFnSc3	impregnace
a	a	k8xC	a
ochraně	ochrana	k1gFnSc3	ochrana
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
ochranou	ochrana	k1gFnSc7	ochrana
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
přírodním	přírodní	k2eAgFnPc3d1	přírodní
kombinacím	kombinace	k1gFnPc3	kombinace
a	a	k8xC	a
starým	starý	k2eAgFnPc3d1	stará
recepturám	receptura	k1gFnPc3	receptura
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kalafuna	kalafuna	k1gFnSc1	kalafuna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
