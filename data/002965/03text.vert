<s>
Valdická	valdický	k2eAgFnSc1d1	Valdická
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jičínském	jičínský	k2eAgNnSc6d1	Jičínské
Valdštejnově	Valdštejnův	k2eAgNnSc6d1	Valdštejnovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
symbol	symbol	k1gInSc4	symbol
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
52	[number]	k4	52
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ochoz	ochoz	k1gInSc1	ochoz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
32	[number]	k4	32
metrech	metr	k1gInPc6	metr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
výšky	výška	k1gFnSc2	výška
vede	vést	k5eAaImIp3nS	vést
156	[number]	k4	156
schodů	schod	k1gInPc2	schod
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
152	[number]	k4	152
schodů	schod	k1gInPc2	schod
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgNnSc4d1	poslední
čtyři	čtyři	k4xCgFnPc1	čtyři
jsou	být	k5eAaImIp3nP	být
slepé	slepý	k2eAgFnPc1d1	slepá
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
nad	nad	k7c4	nad
úroveň	úroveň	k1gFnSc4	úroveň
ochozu	ochoz	k1gInSc2	ochoz
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
často	často	k6eAd1	často
hostí	hostit	k5eAaImIp3nP	hostit
výstavy	výstava	k1gFnPc1	výstava
či	či	k8xC	či
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
opevnění	opevnění	k1gNnSc2	opevnění
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Trčků	Trčka	k1gMnPc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
roku	rok	k1gInSc2	rok
1568	[number]	k4	1568
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
kusy	kus	k1gInPc1	kus
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
byly	být	k5eAaImAgInP	být
využity	využít	k5eAaPmNgInP	využít
zbytky	zbytek	k1gInPc1	zbytek
starší	starý	k2eAgFnSc2d2	starší
brány	brána	k1gFnSc2	brána
z	z	k7c2	z
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
patro	patro	k1gNnSc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
Průjezd	průjezd	k1gInSc1	průjezd
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
pozdně	pozdně	k6eAd1	pozdně
gotickou	gotický	k2eAgFnSc7d1	gotická
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
s	s	k7c7	s
dvojitými	dvojitý	k2eAgInPc7d1	dvojitý
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
spojuje	spojovat	k5eAaImIp3nS	spojovat
Valdštejnovo	Valdštejnův	k2eAgNnSc1d1	Valdštejnovo
a	a	k8xC	a
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
italských	italský	k2eAgMnPc2d1	italský
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
do	do	k7c2	do
Jičína	Jičín	k1gInSc2	Jičín
pozval	pozvat	k5eAaPmAgMnS	pozvat
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
počítaly	počítat	k5eAaImAgFnP	počítat
se	s	k7c7	s
zbořením	zboření	k1gNnSc7	zboření
brány	brána	k1gFnSc2	brána
kvůli	kvůli	k7c3	kvůli
rozšíření	rozšíření	k1gNnSc3	rozšíření
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
však	však	k9	však
sešlo	sejít	k5eAaPmAgNnS	sejít
vojevůdcovou	vojevůdcův	k2eAgFnSc7d1	vojevůdcova
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1678	[number]	k4	1678
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc4d1	přestavěno
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
patro	patro	k1gNnSc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
bylo	být	k5eAaImAgNnS	být
dostavěno	dostavět	k5eAaPmNgNnS	dostavět
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
místního	místní	k2eAgMnSc2d1	místní
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc4	Josef
Oppolzera	Oppolzer	k1gMnSc4	Oppolzer
nové	nový	k2eAgNnSc1d1	nové
patro	patro	k1gNnSc1	patro
s	s	k7c7	s
ochozem	ochoz	k1gInSc7	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vybudována	vybudován	k2eAgFnSc1d1	vybudována
známá	známý	k2eAgFnSc1d1	známá
jehlanovitá	jehlanovitý	k2eAgFnSc1d1	jehlanovitá
střecha	střecha	k1gFnSc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
statici	statice	k1gFnSc4	statice
stav	stav	k1gInSc4	stav
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
studie	studie	k1gFnPc4	studie
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
trhlinám	trhlina	k1gFnPc3	trhlina
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
doporučila	doporučit	k5eAaPmAgFnS	doporučit
opravu	oprava	k1gFnSc4	oprava
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
závěry	závěr	k1gInPc7	závěr
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepožadovala	požadovat	k5eNaImAgFnS	požadovat
okamžité	okamžitý	k2eAgInPc4d1	okamžitý
zásahy	zásah	k1gInPc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
brány	brána	k1gFnSc2	brána
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Jičína	Jičín	k1gInSc2	Jičín
Článek	článek	k1gInSc1	článek
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Kulturních	kulturní	k2eAgFnPc6d1	kulturní
zařízení	zařízení	k1gNnSc2	zařízení
města	město	k1gNnSc2	město
Jičína	Jičín	k1gInSc2	Jičín
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
brány	brána	k1gFnSc2	brána
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Novin	novina	k1gFnPc2	novina
Jičínska	Jičínsko	k1gNnSc2	Jičínsko
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
sebevraždách	sebevražda	k1gFnPc6	sebevražda
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
a	a	k8xC	a
2009	[number]	k4	2009
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Novin	novina	k1gFnPc2	novina
Jičínska	Jičínsko	k1gNnPc4	Jičínsko
</s>
