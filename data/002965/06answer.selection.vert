<s>
Valdická	valdický	k2eAgFnSc1d1	Valdická
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jičínském	jičínský	k2eAgNnSc6d1	Jičínské
Valdštejnově	Valdštejnův	k2eAgNnSc6d1	Valdštejnovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
symbol	symbol	k1gInSc4	symbol
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
