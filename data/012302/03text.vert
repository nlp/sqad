<p>
<s>
Páčidlo	páčidlo	k1gNnSc1	páčidlo
-neboli	eboli	k6eAd1	-neboli
také	také	k9	také
pajsr	pajsr	k1gMnSc1	pajsr
či	či	k8xC	či
pajcr-	pajcr-	k?	pajcr-
je	být	k5eAaImIp3nS	být
pomocný	pomocný	k2eAgInSc1d1	pomocný
pracovní	pracovní	k2eAgInSc1d1	pracovní
nástroj	nástroj	k1gInSc1	nástroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
rozebírání	rozebírání	k1gNnSc4	rozebírání
navzájem	navzájem	k6eAd1	navzájem
spojených	spojený	k2eAgFnPc2d1	spojená
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
kovovou	kovový	k2eAgFnSc4d1	kovová
tyč	tyč	k1gFnSc4	tyč
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
zahnutým	zahnutý	k2eAgInSc7d1	zahnutý
a	a	k8xC	a
zploštělým	zploštělý	k2eAgInSc7d1	zploštělý
koncem	konec	k1gInSc7	konec
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
i	i	k9	i
oběma	dva	k4xCgInPc7	dva
ohnutými	ohnutý	k2eAgInPc7d1	ohnutý
konci	konec	k1gInPc7	konec
-	-	kIx~	-
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
es-hák	esák	k1gInSc1	es-hák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
opatřeným	opatřený	k2eAgInSc7d1	opatřený
štěrbinou	štěrbina	k1gFnSc7	štěrbina
-	-	kIx~	-
pro	pro	k7c4	pro
odstraňování	odstraňování	k1gNnSc4	odstraňování
hřebíků	hřebík	k1gInPc2	hřebík
<g/>
.	.	kIx.	.
</s>
<s>
Páčidla	páčidlo	k1gNnSc2	páčidlo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
páky	páka	k1gFnPc4	páka
k	k	k7c3	k
oddělování	oddělování	k1gNnSc3	oddělování
dvou	dva	k4xCgMnPc2	dva
navzájem	navzájem	k6eAd1	navzájem
spojených	spojený	k2eAgMnPc2d1	spojený
předmětů	předmět	k1gInPc2	předmět
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
hřebíků	hřebík	k1gInPc2	hřebík
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
<s>
Páčidla	páčidlo	k1gNnPc1	páčidlo
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
otevírání	otevírání	k1gNnSc3	otevírání
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
předmětů	předmět	k1gInPc2	předmět
spojených	spojený	k2eAgInPc2d1	spojený
navzájem	navzájem	k6eAd1	navzájem
hřebíky	hřebík	k1gInPc1	hřebík
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
bedny	bedna	k1gFnSc2	bedna
či	či	k8xC	či
krabice	krabice	k1gFnSc2	krabice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
páčidla	páčidlo	k1gNnPc1	páčidlo
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
také	také	k9	také
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
demoliční	demoliční	k2eAgFnPc4d1	demoliční
práce	práce	k1gFnPc4	práce
<g/>
:	:	kIx,	:
vytrhávání	vytrhávání	k1gNnSc4	vytrhávání
prken	prkno	k1gNnPc2	prkno
<g/>
,	,	kIx,	,
oddělování	oddělování	k1gNnSc1	oddělování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cihel	cihla	k1gFnPc2	cihla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
k	k	k7c3	k
rozbíjení	rozbíjení	k1gNnSc3	rozbíjení
měkčích	měkký	k2eAgInPc2d2	měkčí
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
nadzdvihování	nadzdvihování	k1gNnSc4	nadzdvihování
těžkých	těžký	k2eAgInPc2d1	těžký
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páčidlo	páčidlo	k1gNnSc1	páčidlo
obyčejně	obyčejně	k6eAd1	obyčejně
bývá	bývat	k5eAaImIp3nS	bývat
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
dražšího	drahý	k2eAgInSc2d2	dražší
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
<g/>
,	,	kIx,	,
nemagnetický	magnetický	k2eNgMnSc1d1	nemagnetický
a	a	k8xC	a
nedělá	dělat	k5eNaImIp3nS	dělat
jiskry	jiskra	k1gFnPc4	jiskra
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgNnSc1d1	jiné
fiktivní	fiktivní	k2eAgNnSc1d1	fiktivní
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
herní	herní	k2eAgFnSc6d1	herní
sérii	série	k1gFnSc6	série
Half-Life	Half-Lif	k1gInSc5	Half-Lif
je	být	k5eAaImIp3nS	být
páčidlo	páčidlo	k1gNnSc4	páčidlo
základní	základní	k2eAgFnSc7d1	základní
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
třímá	třímat	k5eAaImIp3nS	třímat
hráčem	hráč	k1gMnSc7	hráč
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
Gordon	Gordon	k1gMnSc1	Gordon
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
populárním	populární	k2eAgInSc7d1	populární
symbolem	symbol	k1gInSc7	symbol
v	v	k7c6	v
Half-life	Halfif	k1gInSc5	Half-lif
fanouškovské	fanouškovský	k2eAgFnSc3d1	fanouškovská
kultuře	kultura	k1gFnSc3	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
páčidlo	páčidlo	k1gNnSc4	páčidlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
