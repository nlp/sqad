<s>
Portland	Portland	k1gInSc1	Portland
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Oregon	Oregona	k1gFnPc2	Oregona
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Willamette	Willamett	k1gInSc5	Willamett
nedaleko	nedaleko	k7c2	nedaleko
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Columbií	Columbia	k1gFnPc2	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Pacifiku	Pacifik	k1gInSc2	Pacifik
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
110	[number]	k4	110
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
má	mít	k5eAaImIp3nS	mít
Portland	Portland	k1gInSc1	Portland
583	[number]	k4	583
776	[number]	k4	776
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
v	v	k7c6	v
regionu	region	k1gInSc6	region
je	být	k5eAaImIp3nS	být
budován	budovat	k5eAaImNgInS	budovat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
systém	systém	k1gInSc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
Portlandu	Portland	k1gInSc2	Portland
v	v	k7c6	v
Maine	Main	k1gMnSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Portland	Portland	k1gInSc1	Portland
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
měl	mít	k5eAaImAgInS	mít
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelé	zakladatel	k1gMnPc1	zakladatel
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vybrali	vybrat	k5eAaPmAgMnP	vybrat
pro	pro	k7c4	pro
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hluboký	hluboký	k2eAgInSc1d1	hluboký
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činily	činit	k5eAaImAgInP	činit
ideální	ideální	k2eAgInSc4d1	ideální
obchodní	obchodní	k2eAgInSc4d1	obchodní
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
domy	dům	k1gInPc1	dům
brzy	brzy	k6eAd1	brzy
nahradily	nahradit	k5eAaPmAgInP	nahradit
zděné	zděný	k2eAgInPc1d1	zděný
domy	dům	k1gInPc1	dům
s	s	k7c7	s
gotickými	gotický	k2eAgInPc7d1	gotický
štíty	štít	k1gInPc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
okolo	okolo	k7c2	okolo
18	[number]	k4	18
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
již	již	k9	již
46	[number]	k4	46
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Portland	Portland	k1gInSc1	Portland
neměl	mít	k5eNaImAgInS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
známý	známý	k1gMnSc1	známý
hazardem	hazard	k1gInSc7	hazard
a	a	k8xC	a
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
hostilo	hostit	k5eAaImAgNnS	hostit
město	město	k1gNnSc4	město
světový	světový	k2eAgInSc1d1	světový
veletrh	veletrh	k1gInSc1	veletrh
<g/>
,	,	kIx,	,
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
významné	významný	k2eAgFnSc6d1	významná
události	událost	k1gFnSc6	událost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobení	zdvojnásobení	k1gNnSc4	zdvojnásobení
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
90	[number]	k4	90
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
na	na	k7c4	na
207	[number]	k4	207
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
průmyslovému	průmyslový	k2eAgInSc3d1	průmyslový
rozvoji	rozvoj	k1gInSc3	rozvoj
Portlandu	Portland	k1gInSc2	Portland
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
dvě	dva	k4xCgFnPc1	dva
loděnice	loděnice	k1gFnPc1	loděnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zažilo	zažít	k5eAaPmAgNnS	zažít
město	město	k1gNnSc1	město
divokou	divoký	k2eAgFnSc4d1	divoká
výstavbu	výstavba	k1gFnSc4	výstavba
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
parkovišť	parkoviště	k1gNnPc2	parkoviště
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
historické	historický	k2eAgFnPc1d1	historická
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
centru	centr	k1gInSc6	centr
chátraly	chátrat	k5eAaImAgFnP	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
nekontrolovaný	kontrolovaný	k2eNgInSc4d1	nekontrolovaný
"	"	kIx"	"
<g/>
růst	růst	k1gInSc4	růst
<g/>
"	"	kIx"	"
města	město	k1gNnSc2	město
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
nahradily	nahradit	k5eAaPmAgFnP	nahradit
cihly	cihla	k1gFnPc1	cihla
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
silnice	silnice	k1gFnPc1	silnice
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
nová	nový	k2eAgFnSc1d1	nová
urbanistická	urbanistický	k2eAgFnSc1d1	urbanistická
koncepce	koncepce	k1gFnSc1	koncepce
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
přílivu	příliv	k1gInSc3	příliv
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1	okolní
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
levnější	levný	k2eAgFnPc1d2	levnější
nájemné	nájemný	k2eAgFnPc1d1	nájemná
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
příležitosti	příležitost	k1gFnPc1	příležitost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
grafického	grafický	k2eAgInSc2d1	grafický
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
internetového	internetový	k2eAgInSc2d1	internetový
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
firmách	firma	k1gFnPc6	firma
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Doc	doc	kA	doc
Martens	Martens	k1gInSc1	Martens
<g/>
,	,	kIx,	,
Nike	Nike	k1gFnSc1	Nike
<g/>
,	,	kIx,	,
Adidas	Adidas	k1gInSc1	Adidas
<g/>
,	,	kIx,	,
přilákali	přilákat	k5eAaPmAgMnP	přilákat
do	do	k7c2	do
města	město	k1gNnSc2	město
stovky	stovka	k1gFnSc2	stovka
až	až	k9	až
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
zejména	zejména	k9	zejména
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Portland	Portland	k1gInSc1	Portland
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
uchovat	uchovat	k5eAaPmF	uchovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
menšího	malý	k2eAgNnSc2d2	menší
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
neoklasicistní	neoklasicistní	k2eAgFnSc4d1	neoklasicistní
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
snadno	snadno	k6eAd1	snadno
pohybovat	pohybovat	k5eAaImF	pohybovat
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
krátké	krátký	k2eAgInPc4d1	krátký
bloky	blok	k1gInPc4	blok
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
poloviční	poloviční	k2eAgFnSc1d1	poloviční
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Willamette	Willamett	k1gMnSc5	Willamett
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c4	mezi
mosty	most	k1gInPc4	most
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
Bridge	Bridge	k1gFnSc4	Bridge
a	a	k8xC	a
Brodway	Brodway	k1gFnSc4	Brodway
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
především	především	k9	především
obytné	obytný	k2eAgFnPc4d1	obytná
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
náměstí	náměstí	k1gNnSc4	náměstí
Pioneer	Pioneer	kA	Pioneer
Courthouse	Courthouse	k1gFnSc1	Courthouse
Square	square	k1gInSc1	square
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Downtown	Downtowna	k1gFnPc2	Downtowna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
podle	podle	k7c2	podle
budovy	budova	k1gFnSc2	budova
Pioneer	Pioneer	kA	Pioneer
Courthouse	Courthouse	k1gFnSc2	Courthouse
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
především	především	k6eAd1	především
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ulic	ulice	k1gFnPc2	ulice
je	být	k5eAaImIp3nS	být
Broadway	Broadwaa	k1gFnPc4	Broadwaa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Nike	Nike	k1gFnPc2	Nike
Town	Town	k1gNnSc1	Town
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Paramount	Paramounta	k1gFnPc2	Paramounta
nebo	nebo	k8xC	nebo
Portland	Portland	k1gInSc4	Portland
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc4	museum
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
leží	ležet	k5eAaImIp3nS	ležet
podlouhlý	podlouhlý	k2eAgInSc1d1	podlouhlý
park	park	k1gInSc1	park
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
Blocks	Blocks	k1gInSc1	Blocks
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
stavbu	stavba	k1gFnSc4	stavba
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
vysoká	vysoká	k1gFnSc1	vysoká
Portland	Portland	k1gInSc1	Portland
Building	Building	k1gInSc1	Building
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
budova	budova	k1gFnSc1	budova
Wells	Wells	k1gInSc4	Wells
Fargo	Fargo	k6eAd1	Fargo
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
166	[number]	k4	166
m	m	kA	m
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
dokončení	dokončení	k1gNnSc2	dokončení
<g/>
,	,	kIx,	,
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Oregon	Oregon	k1gInSc1	Oregon
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
bloky	blok	k1gInPc4	blok
východně	východně	k6eAd1	východně
od	od	k7c2	od
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
řeky	řeka	k1gFnSc2	řeka
Willamette	Willamett	k1gMnSc5	Willamett
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Tom	Tom	k1gMnSc1	Tom
McCall	McCall	k1gMnSc1	McCall
Waterfront	Waterfront	k1gMnSc1	Waterfront
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
fontána	fontána	k1gFnSc1	fontána
Salmon	Salmon	k1gInSc4	Salmon
Street	Street	k1gInSc1	Street
Spring	Spring	k1gInSc1	Spring
<g/>
.	.	kIx.	.
</s>
<s>
Východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
zasněžený	zasněžený	k2eAgInSc4d1	zasněžený
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Hood	Hood	k1gInSc1	Hood
(	(	kIx(	(
<g/>
3	[number]	k4	3
426	[number]	k4	426
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
mosty	most	k1gInPc4	most
Burnside	Burnsid	k1gInSc5	Burnsid
a	a	k8xC	a
Steel	Steel	k1gInSc4	Steel
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
prvotních	prvotní	k2eAgNnPc2d1	prvotní
míst	místo	k1gNnPc2	místo
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
Old	Olda	k1gFnPc2	Olda
Town	Town	k1gInSc1	Town
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc4	místo
často	často	k6eAd1	často
zaplavováno	zaplavován	k2eAgNnSc4d1	zaplavováno
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc4	centrum
Portladnu	Portladnu	k1gFnPc2	Portladnu
se	se	k3xPyFc4	se
posunulo	posunout	k5eAaPmAgNnS	posunout
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
historické	historický	k2eAgFnPc1d1	historická
budovy	budova	k1gFnPc1	budova
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgFnP	přeměnit
na	na	k7c4	na
sklady	sklad	k1gInPc4	sklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Old	Olda	k1gFnPc2	Olda
Town	Towno	k1gNnPc2	Towno
především	především	k9	především
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
kavárny	kavárna	k1gFnPc1	kavárna
a	a	k8xC	a
butiky	butik	k1gInPc1	butik
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
kašna	kašna	k1gFnSc1	kašna
Skidmore	Skidmor	k1gInSc5	Skidmor
Fountain	Fountain	k2eAgInSc4d1	Fountain
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
na	na	k7c4	na
SW	SW	kA	SW
Ankeny	Ankena	k1gFnSc2	Ankena
St.	st.	kA	st.
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
je	být	k5eAaImIp3nS	být
kolonáda	kolonáda	k1gFnSc1	kolonáda
<g/>
,	,	kIx,	,
zrekonstruované	zrekonstruovaný	k2eAgNnSc1d1	zrekonstruované
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
zeleninové	zeleninový	k2eAgInPc1d1	zeleninový
trhy	trh	k1gInPc1	trh
New	New	k1gFnSc1	New
Market	market	k1gInSc1	market
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Burnside	Burnsid	k1gInSc5	Burnsid
St.	st.	kA	st.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
levnější	levný	k2eAgFnPc1d2	levnější
etnické	etnický	k2eAgFnPc1d1	etnická
restaurace	restaurace	k1gFnPc1	restaurace
<g/>
,	,	kIx,	,
bary	bar	k1gInPc1	bar
a	a	k8xC	a
rockové	rockový	k2eAgInPc1d1	rockový
a	a	k8xC	a
taneční	taneční	k2eAgInPc1d1	taneční
kluby	klub	k1gInPc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
a	a	k8xC	a
Čínské	čínský	k2eAgFnSc2d1	čínská
čtvrtí	čtvrt	k1gFnSc7	čtvrt
leží	ležet	k5eAaImIp3nS	ležet
Pearl	Pearl	k1gMnSc1	Pearl
District	District	k1gMnSc1	District
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
industriální	industriální	k2eAgFnSc1d1	industriální
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
čtvrť	čtvrť	k1gFnSc1	čtvrť
s	s	k7c7	s
elegantními	elegantní	k2eAgNnPc7d1	elegantní
podkrovími	podkroví	k1gNnPc7	podkroví
<g/>
,	,	kIx,	,
galeriemi	galerie	k1gFnPc7	galerie
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
a	a	k8xC	a
butiky	butik	k1gInPc7	butik
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Pearl	Pearla	k1gFnPc2	Pearla
Districtu	District	k1gInSc2	District
leží	ležet	k5eAaImIp3nS	ležet
Nob	Nob	k1gMnSc1	Nob
Hill	Hill	k1gMnSc1	Hill
známá	známý	k2eAgNnPc4d1	známé
svými	svůj	k3xOyFgFnPc7	svůj
restauracemi	restaurace	k1gFnPc7	restaurace
a	a	k8xC	a
bary	bar	k1gInPc7	bar
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Portland	Portland	k1gInSc1	Portland
na	na	k7c4	na
americké	americký	k2eAgInPc4d1	americký
poměry	poměr	k1gInPc4	poměr
celkem	celkem	k6eAd1	celkem
malé	malý	k2eAgNnSc4d1	malé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
systém	systém	k1gInSc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
autobusů	autobus	k1gInPc2	autobus
zde	zde	k6eAd1	zde
fungují	fungovat	k5eAaImIp3nP	fungovat
dvě	dva	k4xCgFnPc4	dva
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
sítě	síť	k1gFnPc4	síť
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc4d1	hlavní
dopravu	doprava	k1gFnSc4	doprava
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
rychlodrážní	rychlodrážní	k2eAgFnPc1d1	rychlodrážní
tramvaje	tramvaj	k1gFnPc1	tramvaj
MAX	max	kA	max
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
linkách	linka	k1gFnPc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
síť	síť	k1gFnSc4	síť
klasických	klasický	k2eAgFnPc2d1	klasická
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
tramvaje	tramvaj	k1gFnPc1	tramvaj
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Rychlodrážní	Rychlodrážní	k2eAgFnPc1d1	Rychlodrážní
tramvaje	tramvaj	k1gFnPc1	tramvaj
MAX	Max	k1gMnSc1	Max
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
dopravu	doprava	k1gFnSc4	doprava
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
klasické	klasický	k2eAgFnPc1d1	klasická
tramvaje	tramvaj	k1gFnPc1	tramvaj
jezdí	jezdit	k5eAaImIp3nP	jezdit
jen	jen	k9	jen
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
spíše	spíše	k9	spíše
jako	jako	k9	jako
turistická	turistický	k2eAgFnSc1d1	turistická
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgFnPc1	tento
tramvaje	tramvaj	k1gFnPc1	tramvaj
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
USA	USA	kA	USA
příliš	příliš	k6eAd1	příliš
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
využívanější	využívaný	k2eAgFnPc1d2	využívanější
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
rychlodrážní	rychlodrážní	k2eAgFnPc1d1	rychlodrážní
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
583	[number]	k4	583
776	[number]	k4	776
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
76,1	[number]	k4	76,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,0	[number]	k4	1,0
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
4,2	[number]	k4	4,2
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,7	[number]	k4	4,7
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
9,4	[number]	k4	9,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Aškelon	Aškelon	k1gInSc1	Aškelon
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Corinto	Corinto	k1gNnSc1	Corinto
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
Guadalajara	Guadalajara	k1gFnSc1	Guadalajara
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Kao-siung	Kaoiung	k1gInSc1	Kao-siung
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
Chabarovsk	Chabarovsk	k1gInSc1	Chabarovsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Mutare	Mutar	k1gMnSc5	Mutar
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwus	k1gMnSc5	Zimbabwus
San	San	k1gMnSc5	San
Pedro	Pedra	k1gMnSc5	Pedra
Sula	sout	k5eAaImAgNnP	sout
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
Sapporo	Sappora	k1gFnSc5	Sappora
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Su-čou	Su-ča	k1gFnSc7	Su-ča
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Ulsan	Ulsan	k1gInSc1	Ulsan
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
</s>
