<s>
Integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hasiči	hasič	k1gMnPc1
a	a	k8xC
záchranáři	záchranář	k1gMnPc1
zdravotnické	zdravotnický	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
spolupracují	spolupracovat	k5eAaImIp3nP
při	při	k7c6
záchranných	záchranný	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
</s>
<s>
Pojmem	pojem	k1gInSc7
integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
IZS	IZS	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
koordinovaný	koordinovaný	k2eAgInSc1d1
postup	postup	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
složek	složka	k1gFnPc2
při	při	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c6
mimořádné	mimořádný	k2eAgFnSc6d1
události	událost	k1gFnSc6
a	a	k8xC
při	při	k7c6
provádění	provádění	k1gNnSc6
záchranných	záchranný	k2eAgFnPc2d1
a	a	k8xC
likvidačních	likvidační	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
právním	právní	k2eAgInSc7d1
předpisem	předpis	k1gInSc7
je	být	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
239	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
integrovaném	integrovaný	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
IZS	IZS	kA
<g/>
)	)	kIx)
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jeho	jeho	k3xOp3gInPc1
základy	základ	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
koordinátorem	koordinátor	k1gMnSc7
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
na	na	k7c6
místě	místo	k1gNnSc6
zásahu	zásah	k1gInSc2
není	být	k5eNaImIp3nS
ustanoven	ustanovit	k5eAaPmNgInS
velitelem	velitel	k1gMnSc7
zásahu	zásah	k1gInSc6
velitel	velitel	k1gMnSc1
jednotky	jednotka	k1gFnSc2
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
nebo	nebo	k8xC
příslušný	příslušný	k2eAgMnSc1d1
funkcionář	funkcionář	k1gMnSc1
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
s	s	k7c7
právem	právo	k1gNnSc7
přednostního	přednostní	k2eAgNnSc2d1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
velitelem	velitel	k1gMnSc7
zásahu	zásah	k1gInSc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
vedoucí	vedoucí	k2eAgMnSc1d1
člen	člen	k1gMnSc1
složky	složka	k1gFnSc2
IZS	IZS	kA
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
činnost	činnost	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
převažující	převažující	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Složky	složka	k1gFnPc1
IZS	IZS	kA
</s>
<s>
Páteří	páteř	k1gFnSc7
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1
zdravotnické	zdravotnický	k2eAgNnSc1d1
operační	operační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Zdravotnické	zdravotnický	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
koordinuje	koordinovat	k5eAaBmIp3nS
činnost	činnost	k1gFnSc1
posádek	posádka	k1gFnPc2
zdravotnické	zdravotnický	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
</s>
<s>
Základní	základní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
</s>
<s>
Základní	základní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
IZS	IZS	kA
zajišťují	zajišťovat	k5eAaImIp3nP
nepřetržitou	přetržitý	k2eNgFnSc4d1
pohotovost	pohotovost	k1gFnSc4
pro	pro	k7c4
příjem	příjem	k1gInSc4
ohlášení	ohlášení	k1gNnSc4
vzniku	vznik	k1gInSc2
mimořádné	mimořádný	k2eAgFnSc2d1
události	událost	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gNnSc1
vyhodnocení	vyhodnocení	k1gNnSc1
a	a	k8xC
neodkladný	odkladný	k2eNgInSc1d1
zásah	zásah	k1gInSc1
v	v	k7c6
místě	místo	k1gNnSc6
mimořádné	mimořádný	k2eAgFnSc2d1
události	událost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
jej	on	k3xPp3gNnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
jednotky	jednotka	k1gFnPc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
zařazené	zařazený	k2eAgFnSc6d1
do	do	k7c2
plošného	plošný	k2eAgNnSc2d1
pokrytí	pokrytí	k1gNnSc2
kraje	kraj	k1gInSc2
jednotkami	jednotka	k1gFnPc7
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
</s>
<s>
poskytovatelé	poskytovatel	k1gMnPc1
zdravotnické	zdravotnický	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
</s>
<s>
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
složky	složka	k1gFnPc1
IZS	IZS	kA
poskytují	poskytovat	k5eAaImIp3nP
při	při	k7c6
záchranných	záchranný	k2eAgFnPc6d1
a	a	k8xC
likvidačních	likvidační	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
plánovanou	plánovaný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
na	na	k7c4
vyžádání	vyžádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
krizových	krizový	k2eAgInPc2d1
stavů	stav	k1gInPc2
se	se	k3xPyFc4
stávají	stávat	k5eAaImIp3nP
ostatními	ostatní	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
také	také	k9
odborná	odborný	k2eAgNnPc4d1
zdravotnická	zdravotnický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
na	na	k7c6
úrovni	úroveň	k1gFnSc6
fakultních	fakultní	k2eAgFnPc2d1
nemocnic	nemocnice	k1gFnPc2
pro	pro	k7c4
poskytování	poskytování	k1gNnSc4
specializované	specializovaný	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
jej	on	k3xPp3gNnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
obecní	obecnět	k5eAaImIp3nP
<g/>
/	/	kIx~
<g/>
městské	městský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
</s>
<s>
vyčleněné	vyčleněný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
a	a	k8xC
prostředky	prostředek	k1gInPc1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
</s>
<s>
ostatní	ostatní	k2eAgInPc1d1
ozbrojené	ozbrojený	k2eAgInPc1d1
bezpečnostní	bezpečnostní	k2eAgInPc1d1
sbory	sbor	k1gInPc1
</s>
<s>
ostatní	ostatní	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
</s>
<s>
orgány	orgán	k1gInPc1
ochrany	ochrana	k1gFnSc2
veřejného	veřejný	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
</s>
<s>
havarijní	havarijní	k2eAgFnPc1d1
<g/>
,	,	kIx,
pohotovostní	pohotovostní	k2eAgFnPc1d1
<g/>
,	,	kIx,
odborné	odborný	k2eAgFnPc1d1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1
tým	tým	k1gInSc1
Českého	český	k2eAgInSc2d1
červeného	červený	k2eAgInSc2d1
kříže	kříž	k1gInSc2
</s>
<s>
zařízení	zařízení	k1gNnSc1
civilní	civilní	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
</s>
<s>
neziskové	ziskový	k2eNgFnPc1d1
organizace	organizace	k1gFnPc1
a	a	k8xC
sdružení	sdružení	k1gNnPc1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
lze	lze	k6eAd1
využít	využít	k5eAaPmF
k	k	k7c3
záchranným	záchranný	k2eAgFnPc3d1
a	a	k8xC
likvidačním	likvidační	k2eAgFnPc3d1
pracím	práce	k1gFnPc3
<g/>
,	,	kIx,
např.	např.	kA
Asociace	asociace	k1gFnSc1
dobrovolných	dobrovolný	k2eAgMnPc2d1
záchranářů	záchranář	k1gMnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
z.	z.	k?
<g/>
s.	s.	k?
</s>
<s>
Horská	horský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
ČR	ČR	kA
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1
záchranná	záchranný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
ČČK	ČČK	kA
</s>
<s>
Skalní	skalní	k2eAgFnSc1d1
záchranná	záchranný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
ČČK	ČČK	kA
</s>
<s>
Tísňové	tísňový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
</s>
<s>
jednotné	jednotný	k2eAgNnSc1d1
evropské	evropský	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
tísňového	tísňový	k2eAgNnSc2d1
volání	volání	k1gNnSc2
<g/>
:	:	kIx,
112	#num#	k4
</s>
<s>
národní	národní	k2eAgFnSc1d1
tísňová	tísňový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
:	:	kIx,
150	#num#	k4
</s>
<s>
národní	národní	k2eAgFnSc1d1
tísňová	tísňový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
zdravotnické	zdravotnický	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
155	#num#	k4
</s>
<s>
národní	národní	k2eAgFnSc1d1
tísňová	tísňový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
městské	městský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
:	:	kIx,
156	#num#	k4
</s>
<s>
národní	národní	k2eAgFnSc1d1
tísňová	tísňový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
:	:	kIx,
158	#num#	k4
</s>
<s>
národní	národní	k2eAgFnSc1d1
tísňová	tísňový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
Horské	Horské	k2eAgFnSc2d1
služby	služba	k1gFnSc2
ČR	ČR	kA
<g/>
:	:	kIx,
1210	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
</s>
<s>
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupráce	spolupráce	k1gFnSc1
složek	složka	k1gFnPc2
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografia	k1gFnPc4
Hasičský	hasičský	k2eAgInSc4d1
záchranný	záchranný	k2eAgInSc4d1
sbor	sbor	k1gInSc4
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rescue	Rescu	k1gInSc2
Report	report	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
vydavatele	vydavatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
456	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
</s>
<s>
ČECH	Čech	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
IZS	IZS	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
POŽÁRY	požár	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2004-11-22	2004-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zák	Zák	k1gFnSc1
<g/>
.	.	kIx.
č.	č.	k?
239	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c6
integrovaném	integrovaný	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
,	,	kIx,
§	§	k?
19	#num#	k4
<g/>
,	,	kIx,
odst	odst	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
www.zakonyprolidi.cz	www.zakonyprolidi.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Báňský	báňský	k2eAgMnSc1d1
záchranář	záchranář	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
integrovaný	integrovaný	k2eAgInSc4d1
záchranný	záchranný	k2eAgInSc4d1
systém	systém	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7633552-5	7633552-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13060	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85008799	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85008799	#num#	k4
</s>
