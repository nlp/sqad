<s>
Pojmem	pojem	k1gInSc7
integrovaný	integrovaný	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
IZS	IZS	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
koordinovaný	koordinovaný	k2eAgInSc1d1
postup	postup	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
složek	složka	k1gFnPc2
při	při	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c6
mimořádné	mimořádný	k2eAgFnSc6d1
události	událost	k1gFnSc6
a	a	k8xC
při	při	k7c6
provádění	provádění	k1gNnSc6
záchranných	záchranný	k2eAgFnPc2d1
a	a	k8xC
likvidačních	likvidační	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>