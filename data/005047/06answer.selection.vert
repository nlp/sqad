<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
dílem	díl	k1gInSc7	díl
pocházejícím	pocházející	k2eAgInSc7d1	pocházející
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
je	být	k5eAaImIp3nS	být
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gInSc4	zákoník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvoumetrové	dvoumetrový	k2eAgFnSc3d1	dvoumetrová
čedičové	čedičový	k2eAgFnSc3d1	čedičová
stéle	stéla	k1gFnSc3	stéla
<g/>
.	.	kIx.	.
</s>
