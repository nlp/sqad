<p>
<s>
Elektronový	elektronový	k2eAgInSc4d1	elektronový
obal	obal	k1gInSc4	obal
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
elektronů	elektron	k1gInPc2	elektron
vázaných	vázaný	k2eAgInPc2d1	vázaný
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Elektronový	elektronový	k2eAgInSc1d1	elektronový
obal	obal	k1gInSc1	obal
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
atomové	atomový	k2eAgNnSc4d1	atomové
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
většinu	většina	k1gFnSc4	většina
prostoru	prostor	k1gInSc2	prostor
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
==	==	k?	==
</s>
</p>
<p>
<s>
Elektronový	elektronový	k2eAgInSc1d1	elektronový
obal	obal	k1gInSc1	obal
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pouze	pouze	k6eAd1	pouze
elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
záporný	záporný	k2eAgInSc4d1	záporný
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
neutralizovaný	neutralizovaný	k2eAgMnSc1d1	neutralizovaný
kladným	kladný	k2eAgInSc7d1	kladný
nábojem	náboj	k1gInSc7	náboj
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc4	atom
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Neutrální	neutrální	k2eAgInPc1d1	neutrální
atomy	atom	k1gInPc1	atom
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kladně	kladně	k6eAd1	kladně
nabitých	nabitý	k2eAgInPc2d1	nabitý
iontů	ion	k1gInPc2	ion
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
u	u	k7c2	u
záporně	záporně	k6eAd1	záporně
nabitých	nabitý	k2eAgInPc2d1	nabitý
iontů	ion	k1gInPc2	ion
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
celého	celý	k2eAgInSc2d1	celý
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
10	[number]	k4	10
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
tvoří	tvořit	k5eAaImIp3nS	tvořit
okolo	okolo	k7c2	okolo
0,01	[number]	k4	0,01
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
jsou	být	k5eAaImIp3nP	být
dány	dán	k2eAgFnPc1d1	dána
řešením	řešení	k1gNnSc7	řešení
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řešení	řešení	k1gNnSc2	řešení
pro	pro	k7c4	pro
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
atom	atom	k1gInSc4	atom
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastnosti	vlastnost	k1gFnPc1	vlastnost
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
čtyř	čtyři	k4xCgNnPc2	čtyři
kvantových	kvantový	k2eAgNnPc2d1	kvantové
čísel	číslo	k1gNnPc2	číslo
<g/>
:	:	kIx,	:
hlavního	hlavní	k2eAgNnSc2d1	hlavní
kvantového	kvantový	k2eAgNnSc2d1	kvantové
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
orbitálního	orbitální	k2eAgMnSc2d1	orbitální
<g/>
,	,	kIx,	,
magnetického	magnetický	k2eAgNnSc2d1	magnetické
a	a	k8xC	a
spinového	spinový	k2eAgNnSc2d1	spinové
kv	kv	k?	kv
<g/>
.	.	kIx.	.
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
kvantovém	kvantový	k2eAgNnSc6d1	kvantové
čísle	číslo	k1gNnSc6	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
orbitálním	orbitální	k2eAgNnSc6d1	orbitální
kvantovém	kvantový	k2eAgNnSc6d1	kvantové
čísle	číslo	k1gNnSc6	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
směr	směr	k1gInSc1	směr
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
kvantovým	kvantový	k2eAgNnSc7d1	kvantové
číslem	číslo	k1gNnSc7	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
složitějších	složitý	k2eAgInPc6d2	složitější
atomech	atom	k1gInPc6	atom
závislosti	závislost	k1gFnSc2	závislost
již	již	k9	již
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
např.	např.	kA	např.
energetické	energetický	k2eAgFnSc2d1	energetická
hladiny	hladina	k1gFnSc2	hladina
závisí	záviset	k5eAaImIp3nS	záviset
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
kvantovém	kvantový	k2eAgNnSc6d1	kvantové
čísle	číslo	k1gNnSc6	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
vedlejším	vedlejší	k2eAgNnSc6d1	vedlejší
kvantovém	kvantový	k2eAgNnSc6d1	kvantové
čísle	číslo	k1gNnSc6	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
V	v	k7c6	v
elektronových	elektronový	k2eAgInPc6d1	elektronový
obalech	obal	k1gInPc6	obal
složitějších	složitý	k2eAgInPc2d2	složitější
atomů	atom	k1gInPc2	atom
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
také	také	k9	také
tzv.	tzv.	kA	tzv.
spin	spin	k1gInSc1	spin
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
do	do	k7c2	do
orbitalů	orbital	k1gInPc2	orbital
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrony	elektron	k1gInPc1	elektron
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
slupce	slupka	k1gFnSc6	slupka
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
valenční	valenční	k2eAgFnPc1d1	valenční
<g/>
.	.	kIx.	.
</s>
<s>
Valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
pro	pro	k7c4	pro
chemické	chemický	k2eAgFnPc4d1	chemická
vazby	vazba	k1gFnPc4	vazba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnPc1	molekula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Elektron	elektron	k1gInSc1	elektron
</s>
</p>
<p>
<s>
Atomový	atomový	k2eAgInSc1d1	atomový
orbital	orbital	k1gInSc1	orbital
</s>
</p>
<p>
<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
</s>
</p>
