<s>
Niob	niob	k1gInSc1	niob
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Nb	Nb	k1gFnSc2	Nb
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
niobium	niobium	k1gNnSc1	niobium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kovovým	kovový	k2eAgInSc7d1	kovový
<g/>
,	,	kIx,	,
přechodným	přechodný	k2eAgInSc7d1	přechodný
prvkem	prvek	k1gInSc7	prvek
5	[number]	k4	5
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
a	a	k8xC	a
metalurgii	metalurgie	k1gFnSc6	metalurgie
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Niob	niob	k1gInSc1	niob
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
Charlesem	Charles	k1gMnSc7	Charles
Hatchttem	Hatchtt	k1gMnSc7	Hatchtt
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Niobé	Niobý	k2eAgFnSc2d1	Niobý
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bájného	bájný	k2eAgMnSc2d1	bájný
krále	král	k1gMnSc2	král
Tantala	Tantal	k1gMnSc2	Tantal
<g/>
.	.	kIx.	.
</s>
<s>
Hatchett	Hatchett	k1gMnSc1	Hatchett
objevil	objevit	k5eAaPmAgMnS	objevit
niob	niob	k1gInSc4	niob
v	v	k7c6	v
minerálu	minerál	k1gInSc6	minerál
columbitu	columbit	k1gInSc2	columbit
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
někdy	někdy	k6eAd1	někdy
také	také	k9	také
kolumbit	kolumbit	k5eAaPmF	kolumbit
<g/>
)	)	kIx)	)
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
columbium	columbium	k1gNnSc4	columbium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
niobu	niob	k1gInSc2	niob
a	a	k8xC	a
tantalu	tantal	k1gInSc2	tantal
panoval	panovat	k5eAaImAgInS	panovat
dlouho	dlouho	k6eAd1	dlouho
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
prokázal	prokázat	k5eAaPmAgMnS	prokázat
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rose	Rose	k1gMnSc1	Rose
<g/>
,	,	kIx,	,
že	že	k8xS	že
columbit	columbit	k5eAaPmF	columbit
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
tantal	tantal	k1gInSc4	tantal
a	a	k8xC	a
niob	niob	k1gInSc4	niob
<g/>
.	.	kIx.	.
</s>
<s>
Niob	niob	k1gInSc1	niob
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc1d1	kujný
<g/>
,	,	kIx,	,
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
chemicky	chemicky	k6eAd1	chemicky
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zbarvení	zbarvení	k1gNnSc1	zbarvení
se	se	k3xPyFc4	se
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
působení	působení	k1gNnSc6	působení
vzduchu	vzduch	k1gInSc2	vzduch
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
namodralé	namodralý	k2eAgNnSc4d1	namodralé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
musíme	muset	k5eAaImIp1nP	muset
chránit	chránit	k5eAaImF	chránit
v	v	k7c6	v
inertní	inertní	k2eAgFnSc6d1	inertní
atmosféře	atmosféra	k1gFnSc6	atmosféra
před	před	k7c7	před
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Oxidace	oxidace	k1gFnSc1	oxidace
vzduchem	vzduch	k1gInSc7	vzduch
začíná	začínat	k5eAaImIp3nS	začínat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
200	[number]	k4	200
°	°	k?	°
<g/>
C.	C.	kA	C.
Poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
(	(	kIx(	(
<g/>
HF	HF	kA	HF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyselých	kyselý	k2eAgInPc6d1	kyselý
roztocích	roztok	k1gInPc6	roztok
obsahujících	obsahující	k2eAgInPc2d1	obsahující
fluoridové	fluoridový	k2eAgInPc4d1	fluoridový
ionty	ion	k1gInPc4	ion
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
alkalickým	alkalický	k2eAgNnSc7d1	alkalické
tavením	tavení	k1gNnSc7	tavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Nb	Nb	k1gFnSc2	Nb
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Nb	Nb	k1gFnSc1	Nb
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
Nb	Nb	k1gMnSc1	Nb
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Niob	niob	k1gInSc1	niob
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
0,000	[number]	k4	0,000
01	[number]	k4	01
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
0,01	[number]	k4	0,01
ppb	ppb	k?	ppb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
niobu	niob	k1gInSc2	niob
na	na	k7c4	na
40	[number]	k4	40
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
v	v	k7c6	v
minerálech	minerál	k1gInPc6	minerál
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
tantalem	tantal	k1gInSc7	tantal
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
minerály	minerál	k1gInPc7	minerál
jsou	být	k5eAaImIp3nP	být
kolumbit	kolumbit	k5eAaImF	kolumbit
((	((	k?	((
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
coltan	coltan	k1gInSc1	coltan
((	((	k?	((
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Nb	Nb	k?	Nb
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
euxenit	euxenit	k5eAaImF	euxenit
((	((	k?	((
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Th	Th	k1gFnSc1	Th
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ložiska	ložisko	k1gNnPc1	ložisko
rud	rudo	k1gNnPc2	rudo
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
využitelným	využitelný	k2eAgInSc7d1	využitelný
obsahem	obsah	k1gInSc7	obsah
niobu	niob	k1gInSc2	niob
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rudách	ruda	k1gFnPc6	ruda
jej	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
tantal	tantal	k1gInSc1	tantal
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
chemické	chemický	k2eAgNnSc1d1	chemické
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
separaci	separace	k1gFnSc4	separace
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
používá	používat	k5eAaImIp3nS	používat
krystalizace	krystalizace	k1gFnSc1	krystalizace
jejich	jejich	k3xOp3gInPc2	jejich
fluorokoplexů	fluorokoplex	k1gInPc2	fluorokoplex
nebo	nebo	k8xC	nebo
frakční	frakční	k2eAgFnSc1d1	frakční
destilace	destilace	k1gFnSc1	destilace
pětimocných	pětimocný	k2eAgInPc2d1	pětimocný
chloridů	chlorid	k1gInPc2	chlorid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečištění	přečištění	k1gNnSc6	přečištění
sloučenin	sloučenina	k1gFnPc2	sloučenina
se	se	k3xPyFc4	se
elementární	elementární	k2eAgInSc1d1	elementární
kovový	kovový	k2eAgInSc1d1	kovový
niob	niob	k1gInSc1	niob
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Niob	niob	k1gInSc1	niob
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
využití	využití	k1gNnSc4	využití
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ušlechtilé	ušlechtilý	k2eAgFnSc2d1	ušlechtilá
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
slitin	slitina	k1gFnPc2	slitina
mnoha	mnoho	k4c2	mnoho
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slitiny	slitina	k1gFnPc1	slitina
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
potrubních	potrubní	k2eAgInPc2d1	potrubní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
<g/>
:	:	kIx,	:
Kov	kov	k1gInSc1	kov
má	mít	k5eAaImIp3nS	mít
malý	malý	k2eAgInSc1d1	malý
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
pro	pro	k7c4	pro
tepelné	tepelný	k2eAgInPc4d1	tepelný
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
svařování	svařování	k1gNnSc1	svařování
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
namodralé	namodralý	k2eAgFnPc1d1	namodralá
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
pro	pro	k7c4	pro
body-piercing	bodyiercing	k1gInSc4	body-piercing
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
niobu	niob	k1gInSc2	niob
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ferroniobia	ferroniobium	k1gNnSc2	ferroniobium
a	a	k8xC	a
niklniobia	niklniobium	k1gNnSc2	niklniobium
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
superslitinách	superslitina	k1gFnPc6	superslitina
pro	pro	k7c4	pro
součásti	součást	k1gFnPc4	součást
proudových	proudový	k2eAgInPc2d1	proudový
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
tantalu	tantal	k1gInSc2	tantal
v	v	k7c6	v
kondenzátorech	kondenzátor	k1gInPc6	kondenzátor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
fyziologicky	fyziologicky	k6eAd1	fyziologicky
inertní	inertní	k2eAgFnSc1d1	inertní
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
a	a	k8xC	a
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ochlazení	ochlazení	k1gNnSc6	ochlazení
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
přechází	přecházet	k5eAaImIp3nS	přecházet
niob	niob	k1gInSc1	niob
do	do	k7c2	do
supravodivého	supravodivý	k2eAgInSc2d1	supravodivý
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
kritická	kritický	k2eAgFnSc1d1	kritická
teplota	teplota	k1gFnSc1	teplota
elementárního	elementární	k2eAgInSc2d1	elementární
kovu	kov	k1gInSc2	kov
9,25	[number]	k4	9,25
K.	K.	kA	K.
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vanadem	vanad	k1gInSc7	vanad
a	a	k8xC	a
techneciem	technecium	k1gNnSc7	technecium
patří	patřit	k5eAaImIp3nP	patřit
niob	niob	k1gInSc4	niob
mezi	mezi	k7c4	mezi
jediné	jediný	k2eAgInPc4d1	jediný
tři	tři	k4xCgInPc4	tři
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
supravodiči	supravodič	k1gInPc7	supravodič
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
slitiny	slitina	k1gFnPc1	slitina
se	s	k7c7	s
zirkoniem	zirkonium	k1gNnSc7	zirkonium
<g/>
,	,	kIx,	,
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
titanem	titan	k1gInSc7	titan
aj.	aj.	kA	aj.
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
supravodiče	supravodič	k1gInPc4	supravodič
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
supravodivých	supravodivý	k2eAgInPc2d1	supravodivý
magnetů	magnet	k1gInPc2	magnet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
vyrobit	vyrobit	k5eAaPmF	vyrobit
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc1d1	silné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
niob	niob	k1gInSc1	niob
monoizotopický	monoizotopický	k2eAgInSc1d1	monoizotopický
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
Nb	Nb	k1gFnPc2	Nb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstabilnější	stabilní	k2eAgInPc1d3	nejstabilnější
radioizotopy	radioizotop	k1gInPc1	radioizotop
jsou	být	k5eAaImIp3nP	být
92	[number]	k4	92
<g/>
Nb	Nb	k1gFnPc1	Nb
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
34,7	[number]	k4	34,7
milionu	milion	k4xCgInSc2	milion
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
94	[number]	k4	94
<g/>
Nb	Nb	k1gFnPc2	Nb
(	(	kIx(	(
<g/>
20300	[number]	k4	20300
roků	rok	k1gInPc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
91	[number]	k4	91
<g/>
Nb	Nb	k1gFnPc2	Nb
(	(	kIx(	(
<g/>
680	[number]	k4	680
roků	rok	k1gInPc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
23	[number]	k4	23
dalších	další	k2eAgInPc2d1	další
isotopů	isotop	k1gInPc2	isotop
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
niob	niob	k1gInSc1	niob
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
niob	niob	k1gInSc1	niob
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
