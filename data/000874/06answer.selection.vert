<s>
Niob	niob	k1gInSc1	niob
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
Charlesem	Charles	k1gMnSc7	Charles
Hatchttem	Hatchtt	k1gMnSc7	Hatchtt
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
Niobé	Niobý	k2eAgFnSc2d1	Niobý
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bájného	bájný	k2eAgMnSc2d1	bájný
krále	král	k1gMnSc2	král
Tantala	Tantal	k1gMnSc2	Tantal
<g/>
.	.	kIx.	.
</s>
