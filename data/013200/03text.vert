<p>
<s>
You	You	k?	You
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Do	do	k7c2	do
That	Thata	k1gFnPc2	Thata
on	on	k3xPp3gMnSc1	on
Stage	Stagus	k1gMnSc5	Stagus
Anymore	Anymor	k1gMnSc5	Anymor
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
kolekce	kolekce	k1gFnPc1	kolekce
dvou	dva	k4xCgInPc2	dva
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nahrávky	nahrávka	k1gFnPc1	nahrávka
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
<g/>
,	,	kIx,	,
nahrané	nahraný	k2eAgInPc1d1	nahraný
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydané	vydaný	k2eAgNnSc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
napsal	napsat	k5eAaPmAgMnS	napsat
Frank	Frank	k1gMnSc1	Frank
Zappa	Zapp	k1gMnSc2	Zapp
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Disk	disk	k1gInSc1	disk
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sharleena	Sharleeno	k1gNnSc2	Sharleeno
<g/>
"	"	kIx"	"
–	–	k?	–
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bamboozled	Bamboozled	k1gInSc1	Bamboozled
by	by	kYmCp3nS	by
Love	lov	k1gInSc5	lov
<g/>
/	/	kIx~	/
<g/>
Owner	Ownra	k1gFnPc2	Ownra
of	of	k?	of
a	a	k8xC	a
Lonely	Lonela	k1gFnSc2	Lonela
Heart	Hearta	k1gFnPc2	Hearta
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lucille	Lucille	k1gFnSc1	Lucille
Has	hasit	k5eAaImRp2nS	hasit
Messed	Messed	k1gInSc1	Messed
My	my	k3xPp1nPc1	my
Mind	Mind	k1gInSc1	Mind
Up	Up	k1gFnSc1	Up
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Advance	Advance	k1gFnSc1	Advance
Romance	romance	k1gFnSc1	romance
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bobby	Bobb	k1gInPc4	Bobb
Brown	Browna	k1gFnPc2	Browna
Goes	Goesa	k1gFnPc2	Goesa
Down	Down	k1gInSc1	Down
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
It	It	k1gMnSc2	It
Greasey	Greasea	k1gMnSc2	Greasea
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Honey	Hone	k1gMnPc4	Hone
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
You	You	k1gMnSc1	You
Want	Want	k1gMnSc1	Want
a	a	k8xC	a
Man	Man	k1gMnSc1	Man
Like	Like	k1gFnPc2	Like
Me	Me	k1gMnSc1	Me
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
In	In	k1gMnSc2	In
France	Franc	k1gMnSc2	Franc
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Drowning	Drowning	k1gInSc1	Drowning
Witch	Witch	k1gInSc1	Witch
<g/>
"	"	kIx"	"
–	–	k?	–
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ride	Ridus	k1gInSc5	Ridus
My	my	k3xPp1nPc1	my
Face	Face	k1gFnSc4	Face
to	ten	k3xDgNnSc1	ten
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Carol	Carol	k1gInSc1	Carol
<g/>
,	,	kIx,	,
You	You	k1gMnSc1	You
Fool	Fool	k1gMnSc1	Fool
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Chana	Chano	k1gNnPc4	Chano
in	in	k?	in
de	de	k?	de
Bushwop	Bushwop	k1gInSc1	Bushwop
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Joe	Joe	k1gFnSc1	Joe
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Garage	Garage	k1gNnSc7	Garage
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Why	Why	k1gMnSc1	Why
Does	Doesa	k1gFnPc2	Doesa
It	It	k1gMnSc1	It
Hurt	Hurt	k1gMnSc1	Hurt
When	When	k1gMnSc1	When
I	i	k8xC	i
Pee	Pee	k1gMnSc1	Pee
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
===	===	k?	===
Disk	disk	k1gInSc1	disk
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Dickie	Dickie	k1gFnPc1	Dickie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Such	sucho	k1gNnPc2	sucho
an	an	k?	an
Asshole	Asshole	k1gFnSc2	Asshole
<g/>
"	"	kIx"	"
–	–	k?	–
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hands	Hands	k1gInSc4	Hands
With	Witha	k1gFnPc2	Witha
a	a	k8xC	a
Hammer	Hammra	k1gFnPc2	Hammra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Bozzio	Bozzio	k1gNnSc1	Bozzio
<g/>
)	)	kIx)	)
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zoot	Zoot	k2eAgInSc1d1	Zoot
Allures	Allures	k1gInSc1	Allures
<g/>
"	"	kIx"	"
–	–	k?	–
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Society	societa	k1gFnSc2	societa
Pages	Pages	k1gInSc1	Pages
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
Beautiful	Beautiful	k1gInSc1	Beautiful
Guy	Guy	k1gMnSc2	Guy
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Beauty	Beaut	k1gInPc1	Beaut
Knows	Knowsa	k1gFnPc2	Knowsa
No	no	k9	no
Pain	Pain	k1gNnSc1	Pain
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Enormous	Enormous	k1gMnSc1	Enormous
Mouth	Mouth	k1gMnSc1	Mouth
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Cocaine	Cocain	k1gInSc5	Cocain
Decisions	Decisionsa	k1gFnPc2	Decisionsa
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nig	Nig	k1gMnSc2	Nig
Biz	Biz	k1gMnSc2	Biz
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
King	King	k1gInSc1	King
Kong	Kongo	k1gNnPc2	Kongo
<g/>
"	"	kIx"	"
–	–	k?	–
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Cosmik	Cosmika	k1gFnPc2	Cosmika
Debris	Debris	k1gInSc1	Debris
<g/>
"	"	kIx"	"
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Zappa	Zapp	k1gMnSc2	Zapp
-	-	kIx~	-
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Volman	Volman	k1gMnSc1	Volman
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Howard	Howard	k1gMnSc1	Howard
Kaylan	Kaylan	k1gMnSc1	Kaylan
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Lowell	Lowell	k1gMnSc1	Lowell
George	Georg	k1gFnSc2	Georg
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Denny	Denen	k2eAgFnPc1d1	Denen
Walley	Wallea	k1gFnPc1	Wallea
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Vai	Vai	k1gMnSc1	Vai
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Dweezil	Dweezit	k5eAaPmAgInS	Dweezit
Zappa	Zapp	k1gMnSc4	Zapp
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gFnPc3	on
Sherwood	Sherwood	k1gInSc1	Sherwood
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ray	Ray	k?	Ray
Collins	Collins	k1gInSc1	Collins
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ike	Ike	k?	Ike
Willis	Willis	k1gInSc1	Willis
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ray	Ray	k?	Ray
White	Whit	k1gMnSc5	Whit
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ian	Ian	k?	Ian
Underwood	underwood	k1gInSc1	underwood
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
</s>
</p>
<p>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hearn	Hearn	k1gNnSc4	Hearn
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Roy	Roy	k1gMnSc1	Roy
Estrada	Estrada	k1gFnSc1	Estrada
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gFnPc3	on
Pons	Pons	k1gInSc1	Pons
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Scott	Scott	k1gMnSc1	Scott
Thunes	Thunes	k1gMnSc1	Thunes
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
syntezátor	syntezátor	k1gInSc1	syntezátor
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Fowler	Fowler	k1gMnSc1	Fowler
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
pozoun	pozoun	k1gInSc1	pozoun
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Wolf	Wolf	k1gMnSc1	Wolf
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Allan	Allan	k1gMnSc1	Allan
Zavod	Zavoda	k1gFnPc2	Zavoda
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Andre	Andr	k1gMnSc5	Andr
Lewis	Lewis	k1gFnSc7	Lewis
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Preston	Preston	k1gInSc1	Preston
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Duke	Duk	k1gFnSc2	Duk
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
</s>
</p>
<p>
<s>
Tommy	Tomma	k1gFnPc1	Tomma
Mars	Mars	k1gInSc1	Mars
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
</s>
</p>
<p>
<s>
Bobby	Bobba	k1gFnPc1	Bobba
Martin	Martin	k1gMnSc1	Martin
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
saxofon	saxofon	k1gInSc4	saxofon
</s>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Murphy	Murpha	k1gFnSc2	Murpha
Brock	Brock	k1gMnSc1	Brock
–	–	k?	–
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Fowler	Fowler	k1gMnSc1	Fowler
–	–	k?	–
pozoun	pozoun	k1gInSc1	pozoun
</s>
</p>
<p>
<s>
Bunk	Bunk	k1gMnSc1	Bunk
Gardner	Gardner	k1gMnSc1	Gardner
–	–	k?	–
roh	roh	k1gInSc1	roh
</s>
</p>
<p>
<s>
Ralph	Ralph	k1gMnSc1	Ralph
Humphrey	Humphrea	k1gFnSc2	Humphrea
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
Art	Art	k?	Art
Tripp	Tripp	k1gInSc1	Tripp
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
Chester	Chester	k1gMnSc1	Chester
Thompson	Thompson	k1gMnSc1	Thompson
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
</s>
</p>
<p>
<s>
Chad	Chad	k1gMnSc1	Chad
Wackerman	Wackerman	k1gMnSc1	Wackerman
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Jimmy	Jimma	k1gFnPc1	Jimma
Carl	Carl	k1gMnSc1	Carl
Black	Black	k1gMnSc1	Black
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
</s>
</p>
<p>
<s>
Aynsley	Aynsley	k1gInPc1	Aynsley
Dunbar	Dunbara	k1gFnPc2	Dunbara
–	–	k?	–
bicí	bicí	k2eAgFnSc2d1	bicí
</s>
</p>
<p>
<s>
Terry	Terra	k1gFnPc1	Terra
Bozzio	Bozzio	k1gNnSc1	Bozzio
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
</s>
</p>
<p>
<s>
Ruth	Ruth	k1gFnSc1	Ruth
Underwood	underwood	k1gInSc1	underwood
–	–	k?	–
perkuse	perkuse	k1gFnSc2	perkuse
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Ed	Ed	k?	Ed
Mann	Mann	k1gMnSc1	Mann
–	–	k?	–
perkuse	perkuse	k1gFnSc2	perkuse
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Texty	text	k1gInPc1	text
a	a	k8xC	a
informace	informace	k1gFnPc1	informace
</s>
</p>
