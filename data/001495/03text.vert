<s>
Philipp	Philipp	k1gMnSc1	Philipp
Eduard	Eduard	k1gMnSc1	Eduard
Anton	Anton	k1gMnSc1	Anton
von	von	k1gInSc4	von
Lenard	Lenarda	k1gFnPc2	Lenarda
<g/>
,	,	kIx,	,
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
Lénárd	Lénárd	k1gMnSc1	Lénárd
Fülöp	Fülöp	k1gMnSc1	Fülöp
Eduárd	Eduárd	k1gMnSc1	Eduárd
Antal	Antal	k1gMnSc1	Antal
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1862	[number]	k4	1862
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
katodových	katodový	k2eAgInPc2d1	katodový
paprsků	paprsek	k1gInPc2	paprsek
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
na	na	k7c4	na
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Philipp	Philipp	k1gInSc1	Philipp
Eduard	Eduard	k1gMnSc1	Eduard
Anton	Anton	k1gMnSc1	Anton
von	von	k1gInSc4	von
Lenard	Lenard	k1gInSc4	Lenard
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Philipp	Philipp	k1gInSc1	Philipp
Lenard	Lenard	k1gInSc1	Lenard
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Biografie	biografie	k1gFnSc1	biografie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Nobelprize	Nobelprize	k1gFnSc2	Nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
