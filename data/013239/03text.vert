<p>
<s>
Molybden	molybden	k1gInSc1	molybden
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mo	Mo	k1gFnSc1	Mo
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Molybdaenum	Molybdaenum	k1gInSc1	Molybdaenum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
6	[number]	k4	6
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
nalézá	nalézat	k5eAaImIp3nS	nalézat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
vysoce	vysoce	k6eAd1	vysoce
legovaných	legovaný	k2eAgFnPc2d1	legovaná
ocelí	ocel	k1gFnPc2	ocel
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
molybden	molybden	k1gInSc1	molybden
je	být	k5eAaImIp3nS	být
stříbřitý	stříbřitý	k2eAgInSc1d1	stříbřitý
až	až	k8xS	až
šedobílý	šedobílý	k2eAgInSc1d1	šedobílý
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
a	a	k8xC	a
křehký	křehký	k2eAgInSc1d1	křehký
kov	kov	k1gInSc1	kov
se	s	k7c7	s
značně	značně	k6eAd1	značně
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
0,915	[number]	k4	0,915
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
v	v	k7c6	v
těsně	těsně	k6eAd1	těsně
centrované	centrovaný	k2eAgFnSc6d1	centrovaná
kubické	kubický	k2eAgFnSc6d1	kubická
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
i	i	k9	i
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
a	a	k8xC	a
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
žádné	žádný	k3yNgInPc4	žádný
hydridy	hydrid	k1gInPc4	hydrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
především	především	k9	především
oxidačně	oxidačně	k6eAd1	oxidačně
působící	působící	k2eAgFnPc1d1	působící
kyseliny	kyselina	k1gFnPc1	kyselina
pasivují	pasivovat	k5eAaBmIp3nP	pasivovat
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
jej	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
před	před	k7c7	před
dalším	další	k2eAgNnSc7d1	další
napadením	napadení	k1gNnSc7	napadení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
roztokům	roztok	k1gInPc3	roztok
alkalických	alkalický	k2eAgMnPc2d1	alkalický
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
i	i	k8xC	i
lučavce	lučavka	k1gFnSc3	lučavka
královské	královský	k2eAgFnSc3d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Nejsnáze	snadno	k6eAd3	snadno
se	se	k3xPyFc4	se
kovový	kovový	k2eAgInSc1d1	kovový
molybden	molybden	k1gInSc1	molybden
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
alkalickým	alkalický	k2eAgNnSc7d1	alkalické
tavením	tavení	k1gNnSc7	tavení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	s	k7c7	s
směsí	směs	k1gFnSc7	směs
dusičnanu	dusičnan	k1gInSc2	dusičnan
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
+	+	kIx~	+
NaOH	NaOH	k1gFnSc2	NaOH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahřátí	zahřátí	k1gNnSc6	zahřátí
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
nekovy	nekov	k1gInPc7	nekov
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
převážně	převážně	k6eAd1	převážně
intersticiálních	intersticiální	k2eAgFnPc2d1	intersticiální
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
molybden	molybden	k1gInSc1	molybden
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
různých	různý	k2eAgNnPc2d1	různé
mocenství	mocenství	k1gNnPc2	mocenství
od	od	k7c2	od
Mo	Mo	k1gFnSc2	Mo
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
po	po	k7c6	po
Mo	Mo	k1gFnSc6	Mo
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
a	a	k8xC	a
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
škále	škála	k1gFnSc6	škála
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Carl	Carl	k1gMnSc1	Carl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Scheele	Scheel	k1gInPc4	Scheel
vyizoloval	vyizolovat	k5eAaImAgMnS	vyizolovat
z	z	k7c2	z
minerálu	minerál	k1gInSc2	minerál
molybdenitu	molybdenit	k1gInSc2	molybdenit
oxid	oxid	k1gInSc4	oxid
dosud	dosud	k6eAd1	dosud
neznámého	známý	k2eNgInSc2d1	neznámý
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
J.	J.	kA	J.
Hjelm	Hjelm	k1gMnSc1	Hjelm
připravil	připravit	k5eAaPmAgMnS	připravit
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
oxidu	oxid	k1gInSc2	oxid
kovový	kovový	k2eAgInSc1d1	kovový
molybden	molybden	k1gInSc1	molybden
redukcí	redukce	k1gFnPc2	redukce
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
molybden	molybden	k1gInSc1	molybden
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
pojmenování	pojmenování	k1gNnSc2	pojmenování
olova	olovo	k1gNnSc2	olovo
molybdos	molybdos	k1gMnSc1	molybdos
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
označovalo	označovat	k5eAaImAgNnS	označovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
měkký	měkký	k2eAgInSc4d1	měkký
černý	černý	k2eAgInSc4d1	černý
materiál	materiál	k1gInSc4	materiál
vhodný	vhodný	k2eAgInSc4d1	vhodný
ke	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Molybden	molybden	k1gInSc1	molybden
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
však	však	k9	však
molybden	molybden	k1gInSc1	molybden
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
až	až	k9	až
0,01	[number]	k4	0,01
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
molybdenu	molybden	k1gInSc2	molybden
na	na	k7c4	na
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
molybdenit	molybdenit	k1gInSc1	molybdenit
(	(	kIx(	(
<g/>
sulfid	sulfid	k1gInSc1	sulfid
molybdeničitý	molybdeničitý	k2eAgInSc1d1	molybdeničitý
<g/>
,	,	kIx,	,
MoS	MoS	k1gFnSc1	MoS
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
ložiska	ložisko	k1gNnPc1	ložisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
rudami	ruda	k1gFnPc7	ruda
jsou	být	k5eAaImIp3nP	být
wulfenit	wulfenit	k1gInSc4	wulfenit
<g/>
,	,	kIx,	,
molybdenan	molybdenan	k1gInSc4	molybdenan
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
PbMoO	PbMoO	k1gFnSc1	PbMoO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
powellit	powellit	k1gInSc1	powellit
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
Mo	Mo	k1gMnSc1	Mo
<g/>
,	,	kIx,	,
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Molybdenit	molybdenit	k1gInSc1	molybdenit
jako	jako	k8xC	jako
MoS	MoS	k1gFnSc1	MoS
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
buď	buď	k8xC	buď
samostatný	samostatný	k2eAgInSc1d1	samostatný
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečištění	přečištění	k1gNnSc6	přečištění
flotací	flotace	k1gFnSc7	flotace
se	s	k7c7	s
pražením	pražení	k1gNnSc7	pražení
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
molybdenový	molybdenový	k2eAgInSc4d1	molybdenový
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
MoS	MoS	k1gFnSc1	MoS
<g/>
2	[number]	k4	2
+	+	kIx~	+
7	[number]	k4	7
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
MoO	MoO	k1gFnSc1	MoO
<g/>
3	[number]	k4	3
+	+	kIx~	+
4	[number]	k4	4
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
využívá	využívat	k5eAaPmIp3nS	využívat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
aluminotermicky	aluminotermicky	k6eAd1	aluminotermicky
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
ferromolybden	ferromolybden	k1gInSc4	ferromolybden
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nachází	nacházet	k5eAaImIp3nS	nacházet
použití	použití	k1gNnSc4	použití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
molybden	molybden	k1gInSc1	molybden
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
molybdenu	molybden	k1gInSc2	molybden
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MoO	MoO	k?	MoO
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gMnSc1	H2
→	→	k?	→
Mo	Mo	k1gMnSc1	Mo
+	+	kIx~	+
3	[number]	k4	3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
OIonty	OIonta	k1gFnPc4	OIonta
molybdenu	molybden	k1gInSc2	molybden
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
proteinovém	proteinový	k2eAgInSc6d1	proteinový
komplexu	komplex	k1gInSc6	komplex
nitrogenáza	nitrogenáza	k1gFnSc1	nitrogenáza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
mutualistickými	mutualistický	k2eAgInPc7d1	mutualistický
fixátory	fixátor	k1gInPc7	fixátor
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
gramnegativní	gramnegativní	k2eAgFnSc2d1	gramnegativní
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
mikrosymbionti	mikrosymbionť	k1gFnSc2	mikrosymbionť
-	-	kIx~	-
zejména	zejména	k9	zejména
Rhizobium	Rhizobium	k1gNnSc4	Rhizobium
<g/>
)	)	kIx)	)
atmosferického	atmosferický	k2eAgInSc2d1	atmosferický
molekulárního	molekulární	k2eAgInSc2d1	molekulární
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
obohacování	obohacování	k1gNnSc3	obohacování
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
nalézá	nalézat	k5eAaImIp3nS	nalézat
molybden	molybden	k1gInSc4	molybden
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
molybdenu	molybden	k1gInSc2	molybden
ve	v	k7c6	v
slitině	slitina	k1gFnSc6	slitina
výrazně	výrazně	k6eAd1	výrazně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
její	její	k3xOp3gFnSc4	její
tvrdost	tvrdost	k1gFnSc4	tvrdost
<g/>
,	,	kIx,	,
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
a	a	k8xC	a
korozní	korozní	k2eAgFnSc4d1	korozní
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
z	z	k7c2	z
molybdenových	molybdenový	k2eAgFnPc2d1	molybdenová
ocelí	ocel	k1gFnPc2	ocel
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
silně	silně	k6eAd1	silně
mechanicky	mechanicky	k6eAd1	mechanicky
namáhané	namáhaný	k2eAgFnPc4d1	namáhaná
součásti	součást	k1gFnPc4	součást
strojů	stroj	k1gInPc2	stroj
jako	jako	k8xS	jako
například	například	k6eAd1	například
hlavně	hlavně	k9	hlavně
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgFnPc1d1	geologická
vrtné	vrtný	k2eAgFnPc1d1	vrtná
hlavice	hlavice	k1gFnPc1	hlavice
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
pro	pro	k7c4	pro
obrábění	obrábění	k1gNnSc4	obrábění
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
molybdenu	molybden	k1gInSc2	molybden
se	se	k3xPyFc4	se
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
povrchová	povrchový	k2eAgFnSc1d1	povrchová
vrstva	vrstva	k1gFnSc1	vrstva
pístních	pístní	k2eAgInPc2d1	pístní
kroužků	kroužek	k1gInPc2	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
reaktory	reaktor	k1gInPc4	reaktor
pracující	pracující	k2eAgInPc1d1	pracující
v	v	k7c6	v
silně	silně	k6eAd1	silně
korozivním	korozivní	k2eAgNnSc6d1	korozivní
prostředí	prostředí	k1gNnSc6	prostředí
za	za	k7c2	za
vysokých	vysoký	k2eAgInPc2d1	vysoký
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
<g/>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
petrochemických	petrochemický	k2eAgInPc2d1	petrochemický
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
sloužících	sloužící	k2eAgInPc2d1	sloužící
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
sirných	sirný	k2eAgFnPc2d1	sirná
sloučenin	sloučenina	k1gFnPc2	sloučenina
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
ropných	ropný	k2eAgInPc2d1	ropný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k9	jako
pro	pro	k7c4	pro
některá	některý	k3yIgNnPc4	některý
umělá	umělý	k2eAgNnPc4d1	umělé
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
brokolice	brokolice	k1gFnSc2	brokolice
nebo	nebo	k8xC	nebo
květáku	květák	k1gInSc2	květák
<g/>
.	.	kIx.	.
</s>
<s>
Potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
ho	on	k3xPp3gMnSc4	on
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
některých	některý	k3yIgInPc2	některý
potravinových	potravinový	k2eAgInPc2d1	potravinový
doplňků	doplněk	k1gInPc2	doplněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Chemie	chemie	k1gFnSc1	chemie
sloučenin	sloučenina	k1gFnPc2	sloučenina
molybdenu	molybden	k1gInSc2	molybden
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
a	a	k8xC	a
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
pouhý	pouhý	k2eAgInSc1d1	pouhý
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
molybden	molybden	k1gInSc1	molybden
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
různých	různý	k2eAgInPc6d1	různý
valenčních	valenční	k2eAgInPc6d1	valenční
stavech	stav	k1gInPc6	stav
od	od	k7c2	od
Mo	Mo	k1gFnPc2	Mo
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
po	po	k7c6	po
Mo	Mo	k1gFnSc6	Mo
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
přecházet	přecházet	k5eAaImF	přecházet
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemie	chemie	k1gFnSc1	chemie
molybdenu	molybden	k1gInSc2	molybden
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
předmětem	předmět	k1gInSc7	předmět
diplomových	diplomový	k2eAgFnPc2d1	Diplomová
prací	práce	k1gFnPc2	práce
než	než	k8xS	než
praktického	praktický	k2eAgNnSc2d1	praktické
uplatnění	uplatnění	k1gNnSc2	uplatnění
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
chemiků	chemik	k1gMnPc2	chemik
se	se	k3xPyFc4	se
již	již	k6eAd1	již
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
bohatého	bohatý	k2eAgNnSc2d1	bohaté
spektra	spektrum	k1gNnSc2	spektrum
jeho	jeho	k3xOp3gFnPc2	jeho
sloučenin	sloučenina	k1gFnPc2	sloučenina
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
nízkou	nízký	k2eAgFnSc4d1	nízká
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
udržet	udržet	k5eAaPmF	udržet
rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
molybden	molybden	k1gInSc4	molybden
kompletně	kompletně	k6eAd1	kompletně
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
obsahu	obsah	k1gInSc2	obsah
molybdenu	molybden	k1gInSc2	molybden
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
stává	stávat	k5eAaImIp3nS	stávat
soutěží	soutěž	k1gFnSc7	soutěž
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
operaci	operace	k1gFnSc4	operace
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
nějaká	nějaký	k3yIgFnSc1	nějaký
pestře	pestro	k6eAd1	pestro
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
sloučenina	sloučenina	k1gFnSc1	sloučenina
molybdenu	molybden	k1gInSc2	molybden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
molybden	molybden	k1gInSc4	molybden
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
typická	typický	k2eAgFnSc1d1	typická
tvorba	tvorba	k1gFnSc1	tvorba
heteropolykyselin	heteropolykyselina	k1gFnPc2	heteropolykyselina
<g/>
,	,	kIx,	,
polymerních	polymerní	k2eAgFnPc2d1	polymerní
sloučenin	sloučenina	k1gFnPc2	sloučenina
molybdenu	molybden	k1gInSc2	molybden
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
bez	bez	k7c2	bez
přesného	přesný	k2eAgInSc2d1	přesný
stechiometrického	stechiometrický	k2eAgInSc2d1	stechiometrický
vzorce	vzorec	k1gInSc2	vzorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
má	mít	k5eAaImIp3nS	mít
technologický	technologický	k2eAgInSc4d1	technologický
význam	význam	k1gInSc4	význam
například	například	k6eAd1	například
sulfid	sulfid	k1gInSc1	sulfid
molybdeničitý	molybdeničitý	k2eAgInSc1d1	molybdeničitý
<g/>
,	,	kIx,	,
MoS	MoS	k1gFnSc1	MoS
<g/>
2	[number]	k4	2
–	–	k?	–
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
lubrikant	lubrikant	k1gInSc1	lubrikant
(	(	kIx(	(
<g/>
mazadlo	mazadlo	k1gNnSc1	mazadlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
prostředích	prostředí	k1gNnPc6	prostředí
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
nebo	nebo	k8xC	nebo
s	s	k7c7	s
extrémním	extrémní	k2eAgNnSc7d1	extrémní
tlakovým	tlakový	k2eAgNnSc7d1	tlakové
namáháním	namáhání	k1gNnSc7	namáhání
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
prakticky	prakticky	k6eAd1	prakticky
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
solemi	sůl	k1gFnPc7	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
molybdenové	molybdenový	k2eAgFnSc2d1	molybdenová
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
MoO	MoO	k1gFnSc1	MoO
<g/>
4	[number]	k4	4
–	–	k?	–
molybdenany	molybdenan	k1gInPc1	molybdenan
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
složkou	složka	k1gFnSc7	složka
některých	některý	k3yIgInPc2	některý
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
nalézají	nalézat	k5eAaImIp3nP	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
molybden	molybden	k1gInSc1	molybden
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
živých	živý	k2eAgFnPc6d1	živá
tkáních	tkáň	k1gFnPc6	tkáň
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
běžných	běžný	k2eAgFnPc2d1	běžná
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
účastní	účastnit	k5eAaImIp3nS	účastnit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
enzymatických	enzymatický	k2eAgInPc2d1	enzymatický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
za	za	k7c4	za
metabolismus	metabolismus	k1gInSc4	metabolismus
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
detoxikaci	detoxikace	k1gFnSc4	detoxikace
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
molybden	molybden	k1gInSc1	molybden
i	i	k9	i
prevenci	prevence	k1gFnSc4	prevence
zubního	zubní	k2eAgInSc2d1	zubní
kazu	kaz	k1gInSc2	kaz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
přítomnost	přítomnost	k1gFnSc4	přítomnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
zubní	zubní	k2eAgFnSc2d1	zubní
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
.	.	kIx.	.
<g/>
Nedostatek	nedostatek	k1gInSc1	nedostatek
molybdenu	molybden	k1gInSc2	molybden
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
anémii	anémie	k1gFnSc3	anémie
<g/>
,	,	kIx,	,
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
výskytu	výskyt	k1gInSc3	výskyt
záchvatů	záchvat	k1gInPc2	záchvat
astmatu	astma	k1gNnSc2	astma
<g/>
,	,	kIx,	,
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
kazivosti	kazivost	k1gFnSc2	kazivost
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
zhoršení	zhoršení	k1gNnSc4	zhoršení
ochrany	ochrana	k1gFnSc2	ochrana
proti	proti	k7c3	proti
infekci	infekce	k1gFnSc3	infekce
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
molybdenu	molybden	k1gInSc2	molybden
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
příčinou	příčina	k1gFnSc7	příčina
depresivních	depresivní	k2eAgInPc2d1	depresivní
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
impotenci	impotence	k1gFnSc3	impotence
<g/>
.	.	kIx.	.
<g/>
Nedostatek	nedostatek	k1gInSc1	nedostatek
molybdenu	molybden	k1gInSc2	molybden
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
např.	např.	kA	např.
vyslepnutí	vyslepnutí	k1gNnSc1	vyslepnutí
květáku	květák	k1gInSc2	květák
nebo	nebo	k8xC	nebo
růstové	růstový	k2eAgFnSc2d1	růstová
poruchy	porucha	k1gFnSc2	porucha
dalších	další	k2eAgFnPc2d1	další
košťálových	košťálový	k2eAgFnPc2d1	košťálová
zelenin	zelenina	k1gFnPc2	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přirozeným	přirozený	k2eAgInSc7d1	přirozený
zdrojem	zdroj	k1gInSc7	zdroj
molybdenu	molybden	k1gInSc2	molybden
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
jsou	být	k5eAaImIp3nP	být
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
celozrnné	celozrnný	k2eAgNnSc1d1	celozrnné
pečivo	pečivo	k1gNnSc1	pečivo
a	a	k8xC	a
listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
molybden	molybden	k1gInSc1	molybden
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
molybden	molybden	k1gInSc1	molybden
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
