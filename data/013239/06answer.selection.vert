<s>
Molybden	molybden	k1gInSc1	molybden
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mo	Mo	k1gFnSc1	Mo
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Molybdaenum	Molybdaenum	k1gInSc1	Molybdaenum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
6	[number]	k4	6
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
