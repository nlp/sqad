<s>
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1924	[number]	k4	1924
Náchod	Náchod	k1gInSc1	Náchod
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
česko-kanadský	českoanadský	k2eAgInSc1d1	česko-kanadský
spisovatel-prozaik	spisovatelrozaik	k1gInSc1	spisovatel-prozaik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
exilový	exilový	k2eAgMnSc1d1	exilový
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Zdenou	Zdena	k1gFnSc7	Zdena
Salivarovou	Salivarová	k1gFnSc7	Salivarová
zakladatel	zakladatel	k1gMnSc1	zakladatel
exilového	exilový	k2eAgNnSc2d1	exilové
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
'	'	kIx"	'
<g/>
68	[number]	k4	68
Publishers	Publishersa	k1gFnPc2	Publishersa
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
