<s>
Downeaster	Downeaster	k1gInSc1	Downeaster
je	být	k5eAaImIp3nS	být
187	[number]	k4	187
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
linka	linka	k1gFnSc1	linka
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastní	k2eAgMnSc1d1	vlastní
Northern	Northern	k1gMnSc1	Northern
New	New	k1gMnSc1	New
England	Englanda	k1gFnPc2	Englanda
Passenger	Passenger	k1gMnSc1	Passenger
Rail	Rail	k1gMnSc1	Rail
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
NNEPRA	NNEPRA	kA	NNEPRA
<g/>
,	,	kIx,	,
Instituce	instituce	k1gFnSc1	instituce
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
Amtrak	Amtrak	k1gInSc4	Amtrak
a	a	k8xC	a
která	který	k3yQgNnPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
North	North	k1gInSc1	North
Station	station	k1gInSc1	station
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
s	s	k7c7	s
Portlandem	Portland	k1gInSc7	Portland
v	v	k7c6	v
Maine	Main	k1gMnSc5	Main
<g/>
.	.	kIx.	.
</s>
