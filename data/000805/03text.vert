<s>
Downeaster	Downeaster	k1gInSc1	Downeaster
je	být	k5eAaImIp3nS	být
187	[number]	k4	187
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
linka	linka	k1gFnSc1	linka
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastní	vlastní	k2eAgMnSc1d1	vlastní
Northern	Northern	k1gMnSc1	Northern
New	New	k1gMnSc1	New
England	Englanda	k1gFnPc2	Englanda
Passenger	Passenger	k1gMnSc1	Passenger
Rail	Rail	k1gMnSc1	Rail
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
NNEPRA	NNEPRA	kA	NNEPRA
<g/>
,	,	kIx,	,
Instituce	instituce	k1gFnSc1	instituce
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
Amtrak	Amtrak	k1gInSc4	Amtrak
a	a	k8xC	a
která	který	k3yQgNnPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
North	North	k1gInSc1	North
Station	station	k1gInSc1	station
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
s	s	k7c7	s
Portlandem	Portland	k1gInSc7	Portland
v	v	k7c6	v
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pěti	pět	k4xCc7	pět
spojeními	spojení	k1gNnPc7	spojení
denně	denně	k6eAd1	denně
a	a	k8xC	a
s	s	k7c7	s
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
cestujících	cestující	k1gMnPc6	cestující
ročně	ročně	k6eAd1	ročně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2008	[number]	k4	2008
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
linka	linka	k1gFnSc1	linka
Amtraku	Amtrak	k1gInSc2	Amtrak
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
trati	trať	k1gFnSc2	trať
je	být	k5eAaImIp3nS	být
128	[number]	k4	128
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
79	[number]	k4	79
mph	mph	k?	mph
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Downeaster	Downeaster	k1gInSc1	Downeaster
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
linek	linka	k1gFnPc2	linka
Amtraku	Amtrak	k1gInSc2	Amtrak
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
nabízet	nabízet	k5eAaImF	nabízet
Wifi	Wife	k1gFnSc4	Wife
připojení	připojení	k1gNnSc4	připojení
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
využívat	využívat	k5eAaPmF	využívat
systém	systém	k1gInSc4	systém
elektronických	elektronický	k2eAgFnPc2d1	elektronická
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
-	-	kIx~	-
Maine	Main	k1gInSc5	Main
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
obnovení	obnovení	k1gNnSc3	obnovení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
40	[number]	k4	40
km	km	kA	km
prodloužení	prodloužení	k1gNnSc4	prodloužení
trati	trať	k1gFnSc2	trať
až	až	k9	až
do	do	k7c2	do
Brunswicku	Brunswick	k1gInSc2	Brunswick
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
nová	nový	k2eAgFnSc1d1	nová
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
povozu	povoz	k1gInSc2	povoz
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Downeaster	Downeastra	k1gFnPc2	Downeastra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Downeaster	Downeastra	k1gFnPc2	Downeastra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Amtrak	Amtrak	k1gInSc1	Amtrak
-	-	kIx~	-
Tratě	trať	k1gFnPc1	trať
-	-	kIx~	-
Severovýchod	severovýchod	k1gInSc1	severovýchod
-	-	kIx~	-
Downeaster	Downeaster	k1gInSc1	Downeaster
The	The	k1gFnPc2	The
Downeaster	Downeastra	k1gFnPc2	Downeastra
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Downeastriders	Downeastridersa	k1gFnPc2	Downeastridersa
<g/>
.	.	kIx.	.
<g/>
us	us	k?	us
-	-	kIx~	-
Cestovní	cestovní	k2eAgMnSc1d1	cestovní
průvodce	průvodce	k1gMnSc1	průvodce
TrainRiders	TrainRidersa	k1gFnPc2	TrainRidersa
Northeast	Northeast	k1gMnSc1	Northeast
-	-	kIx~	-
Nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zkvalitňovat	zkvalitňovat	k5eAaImF	zkvalitňovat
osobní	osobní	k2eAgFnSc4d1	osobní
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Nové	Nové	k2eAgFnSc6d1	Nové
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
