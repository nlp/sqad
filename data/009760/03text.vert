<p>
<s>
Psychoaktivní	Psychoaktivní	k2eAgFnSc1d1	Psychoaktivní
droga	droga	k1gFnSc1	droga
(	(	kIx(	(
<g/>
též	též	k9	též
psychotropní	psychotropní	k2eAgFnSc1d1	psychotropní
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
omamná	omamný	k2eAgFnSc1d1	omamná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nepřesně	přesně	k6eNd1	přesně
droga	droga	k1gFnSc1	droga
nebo	nebo	k8xC	nebo
návyková	návykový	k2eAgFnSc1d1	návyková
látka	látka	k1gFnSc1	látka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
látka	látka	k1gFnSc1	látka
primárně	primárně	k6eAd1	primárně
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
centrálně	centrálně	k6eAd1	centrálně
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mění	měnit	k5eAaImIp3nS	měnit
mozkové	mozkový	k2eAgFnPc4d1	mozková
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
dočasné	dočasný	k2eAgFnPc4d1	dočasná
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
<g/>
,	,	kIx,	,
náladě	nálada	k1gFnSc6	nálada
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc6	vědomí
a	a	k8xC	a
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Záměrně	záměrně	k6eAd1	záměrně
bývá	bývat	k5eAaImIp3nS	bývat
využívána	využívat	k5eAaPmNgFnS	využívat
k	k	k7c3	k
rekreačním	rekreační	k2eAgInPc3d1	rekreační
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
entheogen	entheogen	k1gInSc1	entheogen
pro	pro	k7c4	pro
rituální	rituální	k2eAgInPc4d1	rituální
a	a	k8xC	a
duchovní	duchovní	k2eAgInPc4d1	duchovní
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
masturbace	masturbace	k1gFnSc1	masturbace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
mysli	mysl	k1gFnSc2	mysl
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
droga	droga	k1gFnSc1	droga
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
označuje	označovat	k5eAaImIp3nS	označovat
usušené	usušený	k2eAgFnPc4d1	usušená
části	část	k1gFnPc4	část
rostlin	rostlina	k1gFnPc2	rostlina
či	či	k8xC	či
živočichů	živočich	k1gMnPc2	živočich
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
droog	droog	k1gInSc1	droog
–	–	k?	–
"	"	kIx"	"
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
psychoaktivní	psychoaktivní	k2eAgFnPc4d1	psychoaktivní
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
droga	droga	k1gFnSc1	droga
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
situace	situace	k1gFnPc1	situace
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
závislosti	závislost	k1gFnSc3	závislost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
nebo	nebo	k8xC	nebo
hazardní	hazardní	k2eAgFnPc4d1	hazardní
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
psychoaktivní	psychoaktivní	k2eAgFnPc1d1	psychoaktivní
drogy	droga	k1gFnPc1	droga
působí	působit	k5eAaImIp3nP	působit
subjektivní	subjektivní	k2eAgFnPc1d1	subjektivní
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
náladě	nálada	k1gFnSc6	nálada
a	a	k8xC	a
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příjemné	příjemný	k2eAgFnPc1d1	příjemná
(	(	kIx(	(
<g/>
euforie	euforie	k1gFnPc1	euforie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
výhodné	výhodný	k2eAgNnSc1d1	výhodné
(	(	kIx(	(
<g/>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
ostražitost	ostražitost	k1gFnSc1	ostražitost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
návykových	návykový	k2eAgMnPc2d1	návykový
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
substance	substance	k1gFnPc1	substance
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
návykové	návykový	k2eAgFnPc1d1	návyková
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
užívání	užívání	k1gNnSc1	užívání
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
fyzické	fyzický	k2eAgFnSc2d1	fyzická
či	či	k8xC	či
psychické	psychický	k2eAgFnSc2d1	psychická
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
závislosti	závislost	k1gFnSc2	závislost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
kombinací	kombinace	k1gFnSc7	kombinace
psychoterapie	psychoterapie	k1gFnSc2	psychoterapie
<g/>
,	,	kIx,	,
skupinových	skupinový	k2eAgNnPc2d1	skupinové
sezení	sezení	k1gNnPc2	sezení
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zlomení	zlomení	k1gNnSc3	zlomení
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etické	etický	k2eAgInPc1d1	etický
aspekty	aspekt	k1gInPc1	aspekt
užívání	užívání	k1gNnSc2	užívání
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
návykovosti	návykovost	k1gFnSc3	návykovost
a	a	k8xC	a
jiným	jiný	k1gMnPc3	jiný
nebezpečím	bezpečit	k5eNaImIp1nS	bezpečit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojených	spojený	k2eAgFnPc2d1	spojená
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
zpravidla	zpravidla	k6eAd1	zpravidla
omezuje	omezovat	k5eAaImIp3nS	omezovat
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
obchodování	obchodování	k1gNnSc4	obchodování
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
substancemi	substance	k1gFnPc7	substance
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
restrikcí	restrikce	k1gFnPc2	restrikce
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
ilegálních	ilegální	k2eAgFnPc6d1	ilegální
a	a	k8xC	a
legálních	legální	k2eAgFnPc6d1	legální
drogách	droga	k1gFnPc6	droga
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
nikotin	nikotin	k1gInSc1	nikotin
a	a	k8xC	a
kofein	kofein	k1gInSc1	kofein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
psychoaktivní	psychoaktivní	k2eAgFnPc4d1	psychoaktivní
drogy	droga	k1gFnPc4	droga
pojem	pojem	k1gInSc1	pojem
návykové	návykový	k2eAgFnSc2d1	návyková
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psychoaktivní	Psychoaktivní	k2eAgFnPc1d1	Psychoaktivní
drogy	droga	k1gFnPc1	droga
neužívají	užívat	k5eNaImIp3nP	užívat
pouze	pouze	k6eAd1	pouze
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
různé	různý	k2eAgFnPc4d1	různá
omamné	omamný	k2eAgFnPc4d1	omamná
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
zvířata	zvíře	k1gNnPc4	zvíře
aby	aby	k9	aby
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
intoxikace	intoxikace	k1gFnSc1	intoxikace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kočky	kočka	k1gFnPc1	kočka
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
šanty	šanta	k1gFnSc2	šanta
kočičí	kočičí	k2eAgFnSc2d1	kočičí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
mýtů	mýtus	k1gInPc2	mýtus
naučila	naučit	k5eAaPmAgFnS	naučit
lidi	člověk	k1gMnPc4	člověk
užívat	užívat	k5eAaImF	užívat
drogy	droga	k1gFnSc2	droga
právě	právě	k9	právě
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
užívají	užívat	k5eAaImIp3nP	užívat
drogy	droga	k1gFnPc1	droga
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lemurové	lemurové	k?	lemurové
je	on	k3xPp3gMnPc4	on
získávají	získávat	k5eAaImIp3nP	získávat
z	z	k7c2	z
mnohonožek	mnohonožka	k1gFnPc2	mnohonožka
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
trávit	trávit	k5eAaImF	trávit
alkohol	alkohol	k1gInSc4	alkohol
již	již	k9	již
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
drog	droga	k1gFnPc2	droga
člověkem	člověk	k1gMnSc7	člověk
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
prehistorie	prehistorie	k1gFnSc2	prehistorie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jeskynní	jeskynní	k2eAgNnSc1d1	jeskynní
malířství	malířství	k1gNnSc1	malířství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
transu	trans	k1gInSc3	trans
a	a	k8xC	a
užívání	užívání	k1gNnSc3	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
jejich	jejich	k3xOp3gNnSc4	jejich
užívání	užívání	k1gNnSc4	užívání
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
deseti	deset	k4xCc7	deset
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
kulturním	kulturní	k2eAgNnSc6d1	kulturní
užívání	užívání	k1gNnSc6	užívání
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
pěti	pět	k4xCc7	pět
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
měly	mít	k5eAaImAgFnP	mít
především	především	k6eAd1	především
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
jako	jako	k9	jako
rekreace	rekreace	k1gFnSc1	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
izolováno	izolovat	k5eAaBmNgNnS	izolovat
mnoho	mnoho	k4c1	mnoho
aktivních	aktivní	k2eAgFnPc2d1	aktivní
složek	složka	k1gFnPc2	složka
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
morfin	morfin	k1gInSc4	morfin
<g/>
,	,	kIx,	,
kokain	kokain	k1gInSc4	kokain
nebo	nebo	k8xC	nebo
Meskalin	meskalin	k1gInSc4	meskalin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
rekreační	rekreační	k2eAgNnSc1d1	rekreační
užívání	užívání	k1gNnSc1	užívání
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
obchodování	obchodování	k1gNnSc1	obchodování
s	s	k7c7	s
psychoaktivními	psychoaktivní	k2eAgFnPc7d1	psychoaktivní
látkami	látka	k1gFnPc7	látka
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
kriminalizováno	kriminalizovat	k5eAaImNgNnS	kriminalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prohibice	prohibice	k1gFnSc1	prohibice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc2d1	americký
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
užívání	užívání	k1gNnSc4	užívání
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
nové	nový	k2eAgFnPc1d1	nová
drogy	droga	k1gFnPc1	droga
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
kriminalizovány	kriminalizován	k2eAgFnPc1d1	kriminalizován
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
amfetaminy	amfetamin	k1gInPc4	amfetamin
nebo	nebo	k8xC	nebo
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
výrobou	výroba	k1gFnSc7	výroba
"	"	kIx"	"
<g/>
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
<g/>
"	"	kIx"	"
drog	droga	k1gFnPc2	droga
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
zločin	zločin	k1gInSc1	zločin
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
narkomafie	narkomafie	k1gFnSc1	narkomafie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
náboženstvích	náboženství	k1gNnPc6	náboženství
jako	jako	k8xS	jako
Native	Natiev	k1gFnSc2	Natiev
American	American	k1gMnSc1	American
Church	Church	k1gMnSc1	Church
je	být	k5eAaImIp3nS	být
užívání	užívání	k1gNnSc4	užívání
ilegálních	ilegální	k2eAgFnPc2d1	ilegální
drog	droga	k1gFnPc2	droga
státem	stát	k1gInSc7	stát
povoleno	povolit	k5eAaPmNgNnS	povolit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užívání	užívání	k1gNnSc1	užívání
==	==	k?	==
</s>
</p>
<p>
<s>
Psychoaktivní	Psychoaktivní	k2eAgFnPc1d1	Psychoaktivní
drogy	droga	k1gFnPc1	droga
jsou	být	k5eAaImIp3nP	být
užívány	užívat	k5eAaImNgFnP	užívat
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgInPc3	ten
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
napomáhat	napomáhat	k5eAaImF	napomáhat
náboženským	náboženský	k2eAgFnPc3d1	náboženská
praktikám	praktika	k1gFnPc3	praktika
<g/>
,	,	kIx,	,
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
self	self	k1gInSc4	self
(	(	kIx(	(
<g/>
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
já	já	k3xPp1nSc1	já
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnit	změnit	k5eAaPmF	změnit
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
léčit	léčit	k5eAaImF	léčit
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
uniknout	uniknout	k5eAaPmF	uniknout
nudě	nuda	k1gFnSc3	nuda
a	a	k8xC	a
beznaději	beznaděj	k1gFnSc3	beznaděj
<g/>
,	,	kIx,	,
podpořit	podpořit	k5eAaPmF	podpořit
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
sociální	sociální	k2eAgFnSc4d1	sociální
interakci	interakce	k1gFnSc4	interakce
<g/>
,	,	kIx,	,
zlepšit	zlepšit	k5eAaPmF	zlepšit
smyslovou	smyslový	k2eAgFnSc4d1	smyslová
zkušenost	zkušenost	k1gFnSc4	zkušenost
a	a	k8xC	a
rozkoš	rozkoš	k1gFnSc4	rozkoš
<g/>
,	,	kIx,	,
stimulovat	stimulovat	k5eAaImF	stimulovat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
kreativitu	kreativita	k1gFnSc4	kreativita
a	a	k8xC	a
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
zlepšit	zlepšit	k5eAaPmF	zlepšit
fyzický	fyzický	k2eAgInSc4d1	fyzický
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
rebelovat	rebelovat	k5eAaImF	rebelovat
<g/>
,	,	kIx,	,
zařadit	zařadit	k5eAaPmF	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
vrstevníky	vrstevník	k1gMnPc7	vrstevník
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anestetika	anestetikum	k1gNnSc2	anestetikum
===	===	k?	===
</s>
</p>
<p>
<s>
Anestetika	anestetikum	k1gNnPc1	anestetikum
jsou	být	k5eAaImIp3nP	být
třídou	třída	k1gFnSc7	třída
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
užívaných	užívaný	k2eAgInPc2d1	užívaný
k	k	k7c3	k
blokování	blokování	k1gNnSc3	blokování
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
vjemů	vjem	k1gInPc2	vjem
u	u	k7c2	u
lékařských	lékařský	k2eAgMnPc2d1	lékařský
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
anestetik	anestetikum	k1gNnPc2	anestetikum
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bezvědomí	bezvědomí	k1gNnSc1	bezvědomí
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prodělat	prodělat	k5eAaPmF	prodělat
lékařské	lékařský	k2eAgInPc4d1	lékařský
zákroky	zákrok	k1gInPc4	zákrok
bez	bez	k7c2	bez
fyzické	fyzický	k2eAgFnSc2d1	fyzická
bolesti	bolest	k1gFnSc2	bolest
či	či	k8xC	či
emociálního	emociální	k2eAgNnSc2d1	emociální
traumatu	trauma	k1gNnSc2	trauma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
GABA	GABA	kA	GABA
a	a	k8xC	a
NMDA	NMDA	kA	NMDA
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
éter	éter	k1gInSc1	éter
<g/>
,	,	kIx,	,
ketamin	ketamin	k1gInSc1	ketamin
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
barbituráty	barbiturát	k1gInPc1	barbiturát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Analgetika	analgetikum	k1gNnSc2	analgetikum
===	===	k?	===
</s>
</p>
<p>
<s>
Analgetika	analgetikum	k1gNnPc1	analgetikum
jsou	být	k5eAaImIp3nP	být
třídou	třída	k1gFnSc7	třída
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
užívaných	užívaný	k2eAgInPc2d1	užívaný
k	k	k7c3	k
tlumení	tlumení	k1gNnSc3	tlumení
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
opioidy	opioida	k1gFnPc4	opioida
jako	jako	k8xS	jako
morfin	morfin	k1gInSc4	morfin
a	a	k8xC	a
kodein	kodein	k1gInSc4	kodein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Psychiatrická	psychiatrický	k2eAgNnPc4d1	psychiatrické
léčiva	léčivo	k1gNnPc4	léčivo
===	===	k?	===
</s>
</p>
<p>
<s>
Psychiatrická	psychiatrický	k2eAgNnPc1d1	psychiatrické
léčiva	léčivo	k1gNnPc1	léčivo
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
tlumení	tlumení	k1gNnSc3	tlumení
mentálních	mentální	k2eAgFnPc2d1	mentální
a	a	k8xC	a
citových	citový	k2eAgFnPc2d1	citová
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jich	on	k3xPp3gFnPc2	on
šest	šest	k4xCc1	šest
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
antidepresiva	antidepresivum	k1gNnPc1	antidepresivum
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgFnPc1d1	užívaná
na	na	k7c4	na
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
klinická	klinický	k2eAgFnSc1d1	klinická
deprese	deprese	k1gFnSc1	deprese
či	či	k8xC	či
dystymie	dystymie	k1gFnSc1	dystymie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
fluoxetin	fluoxetin	k2eAgInSc1d1	fluoxetin
(	(	kIx(	(
<g/>
Prozac	Prozac	k1gInSc1	Prozac
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
stimulanty	stimulant	k1gInPc1	stimulant
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
na	na	k7c4	na
hyperaktivitu	hyperaktivita	k1gFnSc4	hyperaktivita
či	či	k8xC	či
narkolepsii	narkolepsie	k1gFnSc4	narkolepsie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
pervitin	pervitin	k1gInSc1	pervitin
<g/>
,	,	kIx,	,
kokain	kokain	k1gInSc1	kokain
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
či	či	k8xC	či
nikotin	nikotin	k1gInSc1	nikotin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
antipsychotika	antipsychotika	k1gFnSc1	antipsychotika
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
na	na	k7c4	na
psychózy	psychóza	k1gFnPc4	psychóza
<g/>
,	,	kIx,	,
schizofrenii	schizofrenie	k1gFnSc6	schizofrenie
a	a	k8xC	a
mánie	mánie	k1gFnSc2	mánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgFnSc6d1	užívaná
na	na	k7c4	na
bipolární	bipolární	k2eAgFnPc4d1	bipolární
a	a	k8xC	a
schizoafektivní	schizoafektivní	k2eAgFnPc4d1	schizoafektivní
poruchy	porucha	k1gFnPc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
lithium	lithium	k1gNnSc1	lithium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
anxiolytika	anxiolytika	k1gFnSc1	anxiolytika
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
na	na	k7c4	na
úzkostné	úzkostný	k2eAgInPc4d1	úzkostný
poruchy	poruch	k1gInPc4	poruch
</s>
</p>
<p>
<s>
depresanty	depresant	k1gInPc1	depresant
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgInPc1d1	užívaný
jako	jako	k9	jako
hypnotika	hypnotikum	k1gNnPc1	hypnotikum
<g/>
,	,	kIx,	,
sedativa	sedativum	k1gNnPc1	sedativum
a	a	k8xC	a
anestetika	anestetikum	k1gNnPc1	anestetikum
</s>
</p>
<p>
<s>
===	===	k?	===
Rekreační	rekreační	k2eAgFnPc4d1	rekreační
drogy	droga	k1gFnPc4	droga
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
užíváno	užívat	k5eAaImNgNnS	užívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
příjemné	příjemný	k2eAgInPc4d1	příjemný
a	a	k8xC	a
užitečné	užitečný	k2eAgInPc4d1	užitečný
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
rozdělení	rozdělení	k1gNnSc1	rozdělení
drog	droga	k1gFnPc2	droga
užívaných	užívaný	k2eAgFnPc2d1	užívaná
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stimulanty	stimulant	k1gInPc1	stimulant
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
navyšují	navyšovat	k5eAaImIp3nP	navyšovat
funkci	funkce	k1gFnSc4	funkce
CNS	CNS	kA	CNS
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
pro	pro	k7c4	pro
euforické	euforický	k2eAgInPc4d1	euforický
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
pocit	pocit	k1gInSc4	pocit
dostatku	dostatek	k1gInSc2	dostatek
vitality	vitalita	k1gFnSc2	vitalita
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
pervitin	pervitin	k1gInSc1	pervitin
<g/>
,	,	kIx,	,
kokain	kokain	k1gInSc1	kokain
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
nebo	nebo	k8xC	nebo
nikotin	nikotin	k1gInSc1	nikotin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
halucinogeny	halucinogen	k1gInPc1	halucinogen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
stavy	stav	k1gInPc4	stav
podobné	podobný	k2eAgInPc4d1	podobný
transu	trans	k1gInSc3	trans
<g/>
,	,	kIx,	,
meditaci	meditace	k1gFnSc3	meditace
či	či	k8xC	či
snění	snění	k1gNnSc3	snění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hypnotika	hypnotikum	k1gNnPc1	hypnotikum
(	(	kIx(	(
<g/>
též	též	k9	též
narkotika	narkotikon	k1gNnSc2	narkotikon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
pocit	pocit	k1gInSc4	pocit
opilosti	opilost	k1gFnSc2	opilost
a	a	k8xC	a
euforie	euforie	k1gFnSc2	euforie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
opioidy	opioid	k1gInPc1	opioid
<g/>
,	,	kIx,	,
benzodiazepiny	benzodiazepin	k1gInPc1	benzodiazepin
<g/>
,	,	kIx,	,
barbituráty	barbiturát	k1gInPc1	barbiturát
a	a	k8xC	a
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
inhalanty	inhalant	k1gMnPc4	inhalant
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
různou	různý	k2eAgFnSc4d1	různá
škálu	škála	k1gFnSc4	škála
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
toluen	toluen	k1gInSc1	toluen
nebo	nebo	k8xC	nebo
benzín	benzín	k1gInSc1	benzín
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgMnPc4	některý
inhalanty	inhalant	k1gMnPc4	inhalant
spadají	spadat	k5eAaImIp3nP	spadat
účinky	účinek	k1gInPc7	účinek
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
dusný	dusný	k2eAgInSc1d1	dusný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kanabinoidy	kanabinoid	k1gInPc1	kanabinoid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
účinky	účinek	k1gInPc4	účinek
podobné	podobný	k2eAgInPc4d1	podobný
halucinogenům	halucinogen	k1gInPc3	halucinogen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
slabší	slabý	k2eAgFnSc7d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
produkty	produkt	k1gInPc1	produkt
konopí	konopí	k1gNnSc1	konopí
–	–	k?	–
marihuanu	marihuana	k1gFnSc4	marihuana
a	a	k8xC	a
hašiš	hašiš	k1gInSc4	hašiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
taneční	taneční	k2eAgFnPc1d1	taneční
drogy	droga	k1gFnPc1	droga
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látky	látka	k1gFnPc1	látka
užívané	užívaný	k2eAgFnPc1d1	užívaná
na	na	k7c6	na
tanečních	taneční	k2eAgFnPc6d1	taneční
party	party	k1gFnPc6	party
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
především	především	k9	především
MDMA	MDMA	kA	MDMA
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
<g/>
-methylendioxymethamfetamin	ethylendioxymethamfetamin	k1gInSc1	-methylendioxymethamfetamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
stimulanty	stimulant	k1gInPc7	stimulant
a	a	k8xC	a
empatogeny	empatogen	k1gInPc7	empatogen
<g/>
,	,	kIx,	,
a	a	k8xC	a
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
psychedelika	psychedelik	k1gMnSc4	psychedelik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Legalita	legalita	k1gFnSc1	legalita
a	a	k8xC	a
společenská	společenský	k2eAgFnSc1d1	společenská
akceptace	akceptace	k1gFnSc1	akceptace
===	===	k?	===
</s>
</p>
<p>
<s>
Držení	držení	k1gNnSc1	držení
a	a	k8xC	a
užívání	užívání	k1gNnSc1	užívání
některých	některý	k3yIgFnPc2	některý
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
předpisu	předpis	k1gInSc2	předpis
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
trestné	trestný	k2eAgFnPc1d1	trestná
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
perzekuce	perzekuce	k1gFnSc2	perzekuce
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
distributorů	distributor	k1gMnPc2	distributor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
uživatelů	uživatel	k1gMnPc2	uživatel
drog	droga	k1gFnPc2	droga
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
euroamerickém	euroamerický	k2eAgInSc6d1	euroamerický
světě	svět	k1gInSc6	svět
probíhají	probíhat	k5eAaImIp3nP	probíhat
zhruba	zhruba	k6eAd1	zhruba
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
veřejné	veřejný	k2eAgFnSc2d1	veřejná
diskuze	diskuze	k1gFnSc2	diskuze
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
odborníků	odborník	k1gMnPc2	odborník
kriminalizace	kriminalizace	k1gFnSc2	kriminalizace
a	a	k8xC	a
represe	represe	k1gFnPc4	represe
uživatelů	uživatel	k1gMnPc2	uživatel
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
především	především	k9	především
konopí	konopí	k1gNnSc4	konopí
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
prohlubování	prohlubování	k1gNnSc3	prohlubování
drogového	drogový	k2eAgInSc2d1	drogový
problému	problém	k1gInSc2	problém
a	a	k8xC	a
ne	ne	k9	ne
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
řešení	řešení	k1gNnSc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
psychedelik	psychedelika	k1gFnPc2	psychedelika
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
kriminalizace	kriminalizace	k1gFnSc1	kriminalizace
drog	droga	k1gFnPc2	droga
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
obavami	obava	k1gFnPc7	obava
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zastánců	zastánce	k1gMnPc2	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
vedoucích	vedoucí	k1gFnPc2	vedoucí
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
a	a	k8xC	a
myšlenkové	myšlenkový	k2eAgFnSc3d1	myšlenková
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
,	,	kIx,	,
neslučitelné	slučitelný	k2eNgInPc1d1	neslučitelný
se	s	k7c7	s
současným	současný	k2eAgInSc7d1	současný
konzumním	konzumní	k2eAgInSc7d1	konzumní
a	a	k8xC	a
konformním	konformní	k2eAgInSc7d1	konformní
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
mohou	moct	k5eAaImIp3nP	moct
určité	určitý	k2eAgFnSc2d1	určitá
náboženské	náboženský	k2eAgFnSc2d1	náboženská
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
Native	Natiev	k1gFnSc2	Natiev
American	American	k1gMnSc1	American
Church	Church	k1gMnSc1	Church
užívat	užívat	k5eAaImF	užívat
legálně	legálně	k6eAd1	legálně
zakázané	zakázaný	k2eAgFnPc4d1	zakázaná
psychoaktivní	psychoaktivní	k2eAgFnPc4d1	psychoaktivní
drogy	droga	k1gFnPc4	droga
na	na	k7c6	na
základě	základ	k1gInSc6	základ
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
naopak	naopak	k6eAd1	naopak
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zakázány	zakázán	k2eAgFnPc4d1	zakázána
drogy	droga	k1gFnPc4	droga
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
alkohol	alkohol	k1gInSc4	alkohol
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legální	legální	k2eAgFnPc1d1	legální
psychoaktivní	psychoaktivní	k2eAgFnPc1d1	psychoaktivní
drogy	droga	k1gFnPc1	droga
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
podléhají	podléhat	k5eAaImIp3nP	podléhat
mimořádnému	mimořádný	k2eAgInSc3d1	mimořádný
právnímu	právní	k2eAgInSc3d1	právní
<g/>
,	,	kIx,	,
celnímu	celní	k2eAgMnSc3d1	celní
a	a	k8xC	a
daňovému	daňový	k2eAgInSc3d1	daňový
režimu	režim	k1gInSc3	režim
–	–	k?	–
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
bývá	bývat	k5eAaImIp3nS	bývat
uvalena	uvalen	k2eAgFnSc1d1	uvalena
spotřební	spotřební	k2eAgFnSc1d1	spotřební
daň	daň	k1gFnSc1	daň
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
tlumících	tlumící	k2eAgFnPc2d1	tlumící
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pod	pod	k7c7	pod
lékařským	lékařský	k2eAgInSc7d1	lékařský
dohledem	dohled	k1gInSc7	dohled
jako	jako	k8xC	jako
léky	lék	k1gInPc7	lék
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
problém	problém	k1gInSc1	problém
se	s	k7c7	s
zneužíváním	zneužívání	k1gNnSc7	zneužívání
snadno	snadno	k6eAd1	snadno
dostupných	dostupný	k2eAgNnPc2d1	dostupné
hypnotik	hypnotikum	k1gNnPc2	hypnotikum
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
benzodiazepiny	benzodiazepin	k1gInPc4	benzodiazepin
<g/>
,	,	kIx,	,
a	a	k8xC	a
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sedativech	sedativum	k1gNnPc6	sedativum
a	a	k8xC	a
analgetikách	analgetikum	k1gNnPc6	analgetikum
závislých	závislý	k2eAgInPc2d1	závislý
4,7	[number]	k4	4,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
10	[number]	k4	10
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
prášky	prášek	k1gInPc4	prášek
na	na	k7c4	na
spaní	spaní	k1gNnSc4	spaní
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
i	i	k8xC	i
psychickou	psychický	k2eAgFnSc4d1	psychická
závislost	závislost	k1gFnSc4	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
studií	studio	k1gNnPc2	studio
má	mít	k5eAaImIp3nS	mít
tabák	tabák	k1gInSc4	tabák
a	a	k8xC	a
alkohol	alkohol	k1gInSc4	alkohol
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
99	[number]	k4	99
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
úmrtí	úmrť	k1gFnPc2	úmrť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nějak	nějak	k6eAd1	nějak
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
psychoaktivními	psychoaktivní	k2eAgFnPc7d1	psychoaktivní
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Psychoaktivní	Psychoaktivní	k2eAgFnPc1d1	Psychoaktivní
drogy	droga	k1gFnPc1	droga
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
dělené	dělený	k2eAgInPc1d1	dělený
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
měkké	měkký	k2eAgFnPc4d1	měkká
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
má	mít	k5eAaImIp3nS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
míru	míra	k1gFnSc4	míra
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
užívaní	užívaný	k2eAgMnPc1d1	užívaný
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
s	s	k7c7	s
akceptovatelným	akceptovatelný	k2eAgNnSc7d1	akceptovatelné
rizikem	riziko	k1gNnSc7	riziko
a	a	k8xC	a
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgNnPc2	který
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc4	riziko
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
užíváním	užívání	k1gNnSc7	užívání
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
neakceptovatelné	akceptovatelný	k2eNgFnPc1d1	neakceptovatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
měkkých	měkký	k2eAgFnPc2d1	měkká
drog	droga	k1gFnPc2	droga
bývá	bývat	k5eAaImIp3nS	bývat
řazena	řadit	k5eAaImNgFnS	řadit
především	především	k9	především
marihuana	marihuana	k1gFnSc1	marihuana
a	a	k8xC	a
kofein	kofein	k1gInSc1	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
bývají	bývat	k5eAaImIp3nP	bývat
řazeny	řazen	k2eAgFnPc1d1	řazena
i	i	k9	i
psychedelika	psychedelika	k1gFnSc1	psychedelika
jako	jako	k8xS	jako
LSD	LSD	kA	LSD
či	či	k8xC	či
peyotl	peyotnout	k5eAaPmAgInS	peyotnout
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
psilocybinové	psilocybinový	k2eAgFnPc4d1	psilocybinový
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
stojí	stát	k5eAaImIp3nS	stát
MDMA	MDMA	kA	MDMA
(	(	kIx(	(
<g/>
extáze	extáze	k1gFnSc1	extáze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
měkkou	měkký	k2eAgFnSc4d1	měkká
drogu	droga	k1gFnSc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
toxicita	toxicita	k1gFnSc1	toxicita
i	i	k8xC	i
návykovost	návykovost	k1gFnSc1	návykovost
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
společenská	společenský	k2eAgFnSc1d1	společenská
nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
toxicita	toxicita	k1gFnSc1	toxicita
a	a	k8xC	a
návykovost	návykovost	k1gFnSc1	návykovost
MDMA	MDMA	kA	MDMA
<g/>
.	.	kIx.	.
<g/>
Alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
drogu	droga	k1gFnSc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
závislost	závislost	k1gFnSc4	závislost
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
těžké	těžký	k2eAgFnSc2d1	těžká
závislosti	závislost	k1gFnSc2	závislost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
náhlá	náhlý	k2eAgFnSc1d1	náhlá
abstinence	abstinence	k1gFnSc1	abstinence
alkoholové	alkoholový	k2eAgNnSc4d1	alkoholové
delirium	delirium	k1gNnSc4	delirium
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rizikem	riziko	k1gNnSc7	riziko
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
alkohol	alkohol	k1gInSc1	alkohol
každoročně	každoročně	k6eAd1	každoročně
příčinou	příčina	k1gFnSc7	příčina
3,3	[number]	k4	3,3
milionu	milion	k4xCgInSc2	milion
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
velmi	velmi	k6eAd1	velmi
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
drogy	droga	k1gFnPc4	droga
patří	patřit	k5eAaImIp3nS	patřit
heroin	heroin	k1gInSc1	heroin
<g/>
,	,	kIx,	,
crack	crack	k1gInSc1	crack
a	a	k8xC	a
pervitin	pervitin	k1gInSc1	pervitin
<g/>
.	.	kIx.	.
<g/>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
velice	velice	k6eAd1	velice
matoucí	matoucí	k2eAgMnSc1d1	matoucí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
používá	používat	k5eAaImIp3nS	používat
více	hodně	k6eAd2	hodně
obtížně	obtížně	k6eAd1	obtížně
srovnatelných	srovnatelný	k2eAgInPc2d1	srovnatelný
faktorů	faktor	k1gInPc2	faktor
–	–	k?	–
hlavně	hlavně	k9	hlavně
míru	míra	k1gFnSc4	míra
návykovosti	návykovost	k1gFnSc2	návykovost
<g/>
,	,	kIx,	,
riziko	riziko	k1gNnSc4	riziko
poškození	poškození	k1gNnSc1	poškození
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc4d1	společenská
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
vyvážit	vyvážit	k5eAaPmF	vyvážit
je	on	k3xPp3gInPc4	on
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
žebříčku	žebříček	k1gInSc6	žebříček
je	být	k5eAaImIp3nS	být
nesnadná	snadný	k2eNgFnSc1d1	nesnadná
<g/>
.	.	kIx.	.
<g/>
Míra	Míra	k1gFnSc1	Míra
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c4	na
psychedelika	psychedelik	k1gMnSc4	psychedelik
či	či	k8xC	či
marihuanu	marihuana	k1gFnSc4	marihuana
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
na	na	k7c4	na
nikotin	nikotin	k1gInSc4	nikotin
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
silnější	silný	k2eAgMnSc1d2	silnější
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
opiodů	opiod	k1gInPc2	opiod
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
heroinu	heroina	k1gFnSc4	heroina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
rizika	riziko	k1gNnSc2	riziko
poškození	poškození	k1gNnSc2	poškození
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
psychedelik	psychedelika	k1gFnPc2	psychedelika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaných	označovaný	k2eAgFnPc2d1	označovaná
za	za	k7c4	za
"	"	kIx"	"
<g/>
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
"	"	kIx"	"
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
rizika	riziko	k1gNnPc1	riziko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
psychedelika	psychedelika	k1gFnSc1	psychedelika
přinášejí	přinášet	k5eAaImIp3nP	přinášet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
specifická	specifický	k2eAgNnPc1d1	specifické
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
příležitostní	příležitostní	k2eAgMnSc1d1	příležitostní
uživatelé	uživatel	k1gMnPc1	uživatel
neznají	neznat	k5eAaImIp3nP	neznat
nebo	nebo	k8xC	nebo
podceňují	podceňovat	k5eAaImIp3nP	podceňovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
jistých	jistý	k2eAgNnPc2d1	jisté
pravidel	pravidlo	k1gNnPc2	pravidlo
setu	set	k1gInSc2	set
a	a	k8xC	a
settingu	setting	k1gInSc2	setting
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejneškodnějších	škodní	k2eNgFnPc2d3	škodní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
nedodržení	nedodržení	k1gNnSc6	nedodržení
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
často	často	k6eAd1	často
u	u	k7c2	u
nezkušených	zkušený	k2eNgMnPc2d1	nezkušený
uživatelů	uživatel	k1gMnPc2	uživatel
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
již	již	k6eAd1	již
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
užití	užití	k1gNnSc6	užití
způsobit	způsobit	k5eAaPmF	způsobit
velké	velký	k2eAgFnPc4d1	velká
psychické	psychický	k2eAgFnPc4d1	psychická
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
psychózy	psychóza	k1gFnPc4	psychóza
<g/>
,	,	kIx,	,
týdny	týden	k1gInPc4	týden
trvající	trvající	k2eAgFnSc2d1	trvající
noční	noční	k2eAgFnSc2d1	noční
můry	můra	k1gFnSc2	můra
apod.	apod.	kA	apod.
Fyzicky	fyzicky	k6eAd1	fyzicky
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
psychedelik	psychedelika	k1gFnPc2	psychedelika
organismus	organismus	k1gInSc4	organismus
nepoškozuje	poškozovat	k5eNaImIp3nS	poškozovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
psychická	psychický	k2eAgFnSc1d1	psychická
závislost	závislost	k1gFnSc1	závislost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mnohem	mnohem	k6eAd1	mnohem
nebezpečnější	bezpečný	k2eNgFnSc1d2	nebezpečnější
než	než	k8xS	než
fyzická	fyzický	k2eAgFnSc1d1	fyzická
<g/>
.	.	kIx.	.
</s>
<s>
Stimulancia	stimulans	k1gNnPc1	stimulans
pervitin	pervitin	k1gInSc4	pervitin
<g/>
,	,	kIx,	,
kokain	kokain	k1gInSc4	kokain
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgFnPc2d3	nejnebezpečnější
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
nebezpečnosti	nebezpečnost	k1gFnSc2	nebezpečnost
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
i	i	k9	i
do	do	k7c2	do
zákonů	zákon	k1gInPc2	zákon
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
kategorie	kategorie	k1gFnPc4	kategorie
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
se	se	k3xPyFc4	se
zase	zase	k9	zase
používá	používat	k5eAaImIp3nS	používat
model	model	k1gInSc1	model
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
kategoriemi	kategorie	k1gFnPc7	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
měkké	měkký	k2eAgFnPc4d1	měkká
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
drogy	droga	k1gFnPc4	droga
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
nový	nový	k2eAgInSc1d1	nový
český	český	k2eAgInSc1d1	český
trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgMnSc1d1	účinný
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Psychoactive	Psychoactiv	k1gInSc5	Psychoactiv
drug	druga	k1gFnPc2	druga
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Doping	doping	k1gInSc1	doping
</s>
</p>
<p>
<s>
Jed	jed	k1gInSc1	jed
</s>
</p>
<p>
<s>
Droga	droga	k1gFnSc1	droga
(	(	kIx(	(
<g/>
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Léčivo	léčivo	k1gNnSc1	léčivo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
psychoaktivní	psychoaktivní	k2eAgFnSc1d1	psychoaktivní
droga	droga	k1gFnSc1	droga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Droga	droga	k1gFnSc1	droga
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
o	o	k7c6	o
ilegálních	ilegální	k2eAgFnPc6d1	ilegální
a	a	k8xC	a
legálních	legální	k2eAgFnPc6d1	legální
drogách	droga	k1gFnPc6	droga
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
ilegálních	ilegální	k2eAgFnPc6d1	ilegální
drogách	droga	k1gFnPc6	droga
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
užívání	užívání	k1gNnSc4	užívání
<g/>
,	,	kIx,	,
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
doporučení	doporučení	k1gNnPc4	doporučení
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
a	a	k8xC	a
závislostech	závislost	k1gFnPc6	závislost
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Podané	podaný	k2eAgFnPc1d1	podaná
ruce	ruka	k1gFnPc1	ruka
o.p.s.	o.p.s.	k?	o.p.s.
-	-	kIx~	-
nestátní	státní	k2eNgFnSc1d1	nestátní
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
prevenci	prevence	k1gFnSc3	prevence
<g/>
,	,	kIx,	,
léčbě	léčba	k1gFnSc3	léčba
a	a	k8xC	a
další	další	k2eAgFnSc3d1	další
pomoci	pomoc	k1gFnSc3	pomoc
lidem	člověk	k1gMnPc3	člověk
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
se	s	k7c7	s
závislostmi	závislost	k1gFnPc7	závislost
</s>
</p>
<p>
<s>
Internetová	internetový	k2eAgFnSc1d1	internetová
drogová	drogový	k2eAgFnSc1d1	drogová
poradna	poradna	k1gFnSc1	poradna
</s>
</p>
<p>
<s>
Drogy	droga	k1gFnPc1	droga
a	a	k8xC	a
návykové	návykový	k2eAgFnPc1d1	návyková
látky	látka	k1gFnPc1	látka
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
–	–	k?	–
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
</s>
</p>
<p>
<s>
www.legalizace.cz	www.legalizace.cz	k1gMnSc1	www.legalizace.cz
–	–	k?	–
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Legalizace	legalizace	k1gFnSc2	legalizace
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
www.biotox.cz	www.biotox.cz	k1gInSc1	www.biotox.cz
–	–	k?	–
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
psychotropních	psychotropní	k2eAgFnPc2d1	psychotropní
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
negativních	negativní	k2eAgInPc6d1	negativní
dopadech	dopad	k1gInPc6	dopad
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
drogám	droga	k1gFnPc3	droga
</s>
</p>
