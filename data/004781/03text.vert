<p>
<s>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Džugašvili	Džugašvili	k1gMnSc1	Džugašvili
(	(	kIx(	(
<g/>
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
ი	ი	k?	ი
ბ	ბ	k?	ბ
ძ	ძ	k?	ძ
ჯ	ჯ	k?	ჯ
[	[	kIx(	[
<g/>
Ioseb	Iosba	k1gFnPc2	Iosba
Besarionis	Besarionis	k1gFnSc2	Besarionis
dze	dze	k?	dze
Džugašvili	Džugašvili	k1gFnSc2	Džugašvili
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
И	И	k?	И
В	В	k?	В
Д	Д	k?	Д
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1878	[number]	k4	1878
Gori	Gori	k1gNnSc2	Gori
<g/>
,	,	kIx,	,
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Stalin	Stalin	k1gMnSc1	Stalin
(	(	kIx(	(
<g/>
С	С	k?	С
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
byl	být	k5eAaImAgMnS	být
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
rozporuplnou	rozporuplný	k2eAgFnSc7d1	rozporuplná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
při	při	k7c6	při
hladomoru	hladomor	k1gInSc6	hladomor
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
během	během	k7c2	během
násilné	násilný	k2eAgFnSc2d1	násilná
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
odhadu	odhad	k1gInSc2	odhad
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
7	[number]	k4	7
a	a	k8xC	a
10	[number]	k4	10
milióny	milión	k4xCgInPc1	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
mezi	mezi	k7c4	mezi
nejkrvavější	krvavý	k2eAgMnPc4d3	nejkrvavější
diktátory	diktátor	k1gMnPc4	diktátor
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc4	období
Stalinova	Stalinův	k2eAgMnSc2d1	Stalinův
SSSR	SSSR	kA	SSSR
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
represe	represe	k1gFnPc1	represe
vůči	vůči	k7c3	vůči
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
i	i	k8xC	i
vysokým	vysoký	k2eAgMnSc7d1	vysoký
stranickým	stranický	k2eAgMnSc7d1	stranický
funkcionářům	funkcionář	k1gMnPc3	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
příznivci	příznivec	k1gMnPc1	příznivec
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
pokrok	pokrok	k1gInSc4	pokrok
SSSR	SSSR	kA	SSSR
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
především	především	k9	především
transformaci	transformace	k1gFnSc4	transformace
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
státu	stát	k1gInSc2	stát
v	v	k7c4	v
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
velmoc	velmoc	k1gFnSc4	velmoc
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
růst	růst	k1gInSc4	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
gramotnosti	gramotnost	k1gFnSc2	gramotnost
nebo	nebo	k8xC	nebo
zlepšení	zlepšení	k1gNnSc4	zlepšení
úrovně	úroveň	k1gFnSc2	úroveň
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
matriky	matrika	k1gFnSc2	matrika
a	a	k8xC	a
úředních	úřední	k2eAgFnPc2d1	úřední
listin	listina	k1gFnPc2	listina
z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
gruzínské	gruzínský	k2eAgFnSc6d1	gruzínská
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
městě	město	k1gNnSc6	město
Gori	Gori	k1gNnPc2	Gori
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
juliánském	juliánský	k2eAgInSc6d1	juliánský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
)	)	kIx)	)
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
vytrvale	vytrvale	k6eAd1	vytrvale
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
juliánském	juliánský	k2eAgInSc6d1	juliánský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
)	)	kIx)	)
1879	[number]	k4	1879
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
nadále	nadále	k6eAd1	nadále
oslavováno	oslavován	k2eAgNnSc1d1	oslavováno
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
uvádějí	uvádět	k5eAaImIp3nP	uvádět
zdroje	zdroj	k1gInSc2	zdroj
různá	různý	k2eAgNnPc1d1	různé
data	datum	k1gNnPc1	datum
Stalinova	Stalinův	k2eAgNnSc2d1	Stalinovo
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Didi-Lilo	Didi-Lila	k1gFnSc5	Didi-Lila
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
tifliské	tifliský	k2eAgFnSc6d1	tifliský
gubernii	gubernie	k1gFnSc6	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
alkoholik	alkoholik	k1gMnSc1	alkoholik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ženu	hnát	k5eAaImIp1nS	hnát
i	i	k9	i
malého	malý	k2eAgMnSc4d1	malý
Josifa	Josif	k1gMnSc4	Josif
krutě	krutě	k6eAd1	krutě
bil	bít	k5eAaImAgMnS	bít
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
obuvníkem	obuvník	k1gMnSc7	obuvník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
živobytí	živobytí	k1gNnSc3	živobytí
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
do	do	k7c2	do
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
Tiflisu	Tiflis	k1gInSc2	Tiflis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
obuv	obuv	k1gFnSc4	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Jekatěrina	Jekatěrina	k1gFnSc1	Jekatěrina
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Geladze	Geladze	k1gFnSc1	Geladze
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
12	[number]	k4	12
let	léto	k1gNnPc2	léto
nevolnicí	nevolnice	k1gFnSc7	nevolnice
<g/>
.	.	kIx.	.
</s>
<s>
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
zbožná	zbožný	k2eAgFnSc1d1	zbožná
gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
křesťanka	křesťanka	k1gFnSc1	křesťanka
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
mít	mít	k5eAaImF	mít
ze	z	k7c2	z
syna	syn	k1gMnSc2	syn
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
carské	carský	k2eAgFnSc2d1	carská
vlády	vláda	k1gFnSc2	vláda
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
a	a	k8xC	a
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
chodily	chodit	k5eAaImAgInP	chodit
jen	jen	k6eAd1	jen
děti	dítě	k1gFnPc4	dítě
majetných	majetný	k2eAgMnPc2d1	majetný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zavedena	zaveden	k2eAgFnSc1d1	zavedena
přijímací	přijímací	k2eAgFnSc1d1	přijímací
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Stalin	Stalin	k1gMnSc1	Stalin
úspěšně	úspěšně	k6eAd1	úspěšně
složil	složit	k5eAaPmAgMnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
ho	on	k3xPp3gInSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
církevní	církevní	k2eAgFnSc2d1	církevní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Gori	Gori	k1gNnSc6	Gori
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
žákem	žák	k1gMnSc7	žák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
vyšel	vyjít	k5eAaPmAgInS	vyjít
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
a	a	k8xC	a
s	s	k7c7	s
doporučením	doporučení	k1gNnSc7	doporučení
do	do	k7c2	do
bohosloveckého	bohoslovecký	k2eAgInSc2d1	bohoslovecký
semináře	seminář	k1gInSc2	seminář
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
tak	tak	k9	tak
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
Stalin	Stalin	k1gMnSc1	Stalin
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
studenti	student	k1gMnPc1	student
nesměli	smět	k5eNaImAgMnP	smět
číst	číst	k5eAaImF	číst
světské	světský	k2eAgFnPc4d1	světská
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc1	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
přitom	přitom	k6eAd1	přitom
přistihli	přistihnout	k5eAaPmAgMnP	přistihnout
<g/>
,	,	kIx,	,
zavřeli	zavřít	k5eAaPmAgMnP	zavřít
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
školního	školní	k2eAgNnSc2d1	školní
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
sám	sám	k3xTgMnSc1	sám
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
protestu	protest	k1gInSc2	protest
proti	proti	k7c3	proti
ponižujícímu	ponižující	k2eAgInSc3d1	ponižující
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
jezuitským	jezuitský	k2eAgFnPc3d1	jezuitská
metodám	metoda	k1gFnPc3	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
panovaly	panovat	k5eAaImAgFnP	panovat
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
revolucionářem	revolucionář	k1gMnSc7	revolucionář
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Přes	přes	k7c4	přes
ubíjející	ubíjející	k2eAgNnSc4d1	ubíjející
ovzduší	ovzduší	k1gNnSc4	ovzduší
semináře	seminář	k1gInSc2	seminář
se	se	k3xPyFc4	se
v	v	k7c6	v
mladém	mladý	k2eAgMnSc6d1	mladý
Stalinovi	Stalin	k1gMnSc6	Stalin
ozývaly	ozývat	k5eAaImAgFnP	ozývat
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
snahy	snaha	k1gFnPc4	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gNnSc3	on
ani	ani	k9	ani
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
složil	složit	k5eAaPmAgMnS	složit
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
otištěna	otisknout	k5eAaPmNgFnS	otisknout
na	na	k7c6	na
první	první	k4xOgFnSc6	první
stránce	stránka	k1gFnSc6	stránka
novin	novina	k1gFnPc2	novina
"	"	kIx"	"
<g/>
Iveria	Iverium	k1gNnSc2	Iverium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
slavným	slavný	k2eAgInSc7d1	slavný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Čavčavadzem	Čavčavadz	k1gInSc7	Čavčavadz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A	a	k9	a
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdos	kdos	k3yInSc1	kdos
utlačen	utlačen	k2eAgMnSc1d1	utlačen
byl	být	k5eAaImAgInS	být
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
koho	kdo	k3yRnSc4	kdo
v	v	k7c4	v
prach	prach	k1gInSc4	prach
zemský	zemský	k2eAgMnSc1d1	zemský
tiskli	tisknout	k5eAaImAgMnP	tisknout
jen	jen	k6eAd1	jen
<g/>
,	,	kIx,	,
<g/>
nad	nad	k7c4	nad
štíty	štít	k1gInPc4	štít
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
vztyčíš	vztyčit	k5eAaPmIp2nS	vztyčit
hrdě	hrdě	k6eAd1	hrdě
<g/>
,	,	kIx,	,
nadějí	naděje	k1gFnSc7	naděje
jasnou	jasný	k2eAgFnSc7d1	jasná
okřídlen	okřídlen	k2eAgInSc4d1	okřídlen
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
V	v	k7c6	v
bohosloveckém	bohoslovecký	k2eAgInSc6d1	bohoslovecký
semináři	seminář	k1gInSc6	seminář
vydával	vydávat	k5eAaImAgMnS	vydávat
Stalin	Stalin	k1gMnSc1	Stalin
ilegální	ilegální	k2eAgFnSc2d1	ilegální
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
psané	psaný	k2eAgFnPc1d1	psaná
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
ke	k	k7c3	k
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
podzemní	podzemní	k2eAgFnSc2d1	podzemní
organizace	organizace	k1gFnSc2	organizace
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
Mesame	Mesam	k1gInSc5	Mesam
dasi	das	k1gMnSc3	das
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Revolucionář	revolucionář	k1gMnSc1	revolucionář
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
politického	politický	k2eAgNnSc2d1	politické
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Organizoval	organizovat	k5eAaBmAgMnS	organizovat
demonstrace	demonstrace	k1gFnPc4	demonstrace
a	a	k8xC	a
stávky	stávka	k1gFnPc4	stávka
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
centrech	centr	k1gInPc6	centr
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přílišnou	přílišný	k2eAgFnSc7d1	přílišná
horlivostí	horlivost	k1gFnSc7	horlivost
při	při	k7c6	při
vyvolávání	vyvolávání	k1gNnSc6	vyvolávání
krvavých	krvavý	k2eAgInPc2d1	krvavý
střetů	střet	k1gInPc2	střet
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
si	se	k3xPyFc3	se
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
své	svůj	k3xOyFgMnPc4	svůj
spolubojovníky	spolubojovník	k1gMnPc4	spolubojovník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1902	[number]	k4	1902
do	do	k7c2	do
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
byl	být	k5eAaImAgMnS	být
sedmkrát	sedmkrát	k6eAd1	sedmkrát
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
revoluční	revoluční	k2eAgFnSc4d1	revoluční
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
odesílán	odesílat	k5eAaImNgMnS	odesílat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mírnost	mírnost	k1gFnSc4	mírnost
rozsudků	rozsudek	k1gInPc2	rozsudek
a	a	k8xC	a
opakované	opakovaný	k2eAgInPc1d1	opakovaný
útěky	útěk	k1gInPc1	útěk
ho	on	k3xPp3gMnSc4	on
někteří	některý	k3yIgMnPc1	některý
podezírali	podezírat	k5eAaImAgMnP	podezírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tajným	tajný	k2eAgMnSc7d1	tajný
agentem	agent	k1gMnSc7	agent
carské	carský	k2eAgFnSc2d1	carská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
obvinění	obvinění	k1gNnPc4	obvinění
však	však	k9	však
neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
ruské	ruský	k2eAgFnSc2d1	ruská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
na	na	k7c4	na
bolševiky	bolševik	k1gMnPc4	bolševik
a	a	k8xC	a
menševiky	menševik	k1gMnPc4	menševik
se	se	k3xPyFc4	se
Stalin	Stalin	k1gMnSc1	Stalin
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
(	(	kIx(	(
<g/>
před	před	k7c7	před
přesunem	přesun	k1gInSc7	přesun
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Tammerforsu	Tammerfors	k1gInSc6	Tammerfors
-	-	kIx~	-
dnešním	dnešní	k2eAgInSc6d1	dnešní
Tampere	Tamper	k1gMnSc5	Tamper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Stalin	Stalin	k1gMnSc1	Stalin
zúčastňoval	zúčastňovat	k5eAaImAgMnS	zúčastňovat
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
setkání	setkání	k1gNnSc6	setkání
Ruské	ruský	k2eAgFnSc2d1	ruská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
delegátem	delegát	k1gMnSc7	delegát
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
delegátem	delegát	k1gMnSc7	delegát
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
sjezdech	sjezd	k1gInPc6	sjezd
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
Stockholm	Stockholm	k1gInSc1	Stockholm
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bolševici	bolševik	k1gMnPc1	bolševik
opakovaně	opakovaně	k6eAd1	opakovaně
střetávali	střetávat	k5eAaImAgMnP	střetávat
s	s	k7c7	s
menševiky	menševik	k1gMnPc7	menševik
<g/>
.13	.13	k4	.13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1907	[number]	k4	1907
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
rušné	rušný	k2eAgFnSc6d1	rušná
ulici	ulice	k1gFnSc6	ulice
k	k	k7c3	k
přepadení	přepadení	k1gNnSc3	přepadení
transportu	transport	k1gInSc2	transport
peněz	peníze	k1gInPc2	peníze
do	do	k7c2	do
Tbiliské	tbiliský	k2eAgFnSc2d1	tbiliská
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
přípravě	příprava	k1gFnSc6	příprava
se	se	k3xPyFc4	se
Stalin	Stalin	k1gMnSc1	Stalin
snad	snad	k9	snad
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
kořist	kořist	k1gFnSc1	kořist
a	a	k8xC	a
asi	asi	k9	asi
40	[number]	k4	40
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
menšina	menšina	k1gFnSc1	menšina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
ostraze	ostraha	k1gFnSc3	ostraha
transportu	transport	k1gInSc2	transport
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
byly	být	k5eAaImAgInP	být
předány	předat	k5eAaPmNgInP	předat
stranické	stranický	k2eAgInPc1d1	stranický
pokladně	pokladna	k1gFnSc6	pokladna
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
někteří	některý	k3yIgMnPc1	některý
gruzínští	gruzínský	k2eAgMnPc1d1	gruzínský
menševici	menševik	k1gMnPc1	menševik
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
pobouření	pobouření	k1gNnSc4	pobouření
nad	nad	k7c7	nad
těmito	tento	k3xDgFnPc7	tento
teroristickými	teroristický	k2eAgFnPc7d1	teroristická
a	a	k8xC	a
banditskými	banditský	k2eAgFnPc7d1	banditský
metodami	metoda	k1gFnPc7	metoda
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
Stalinovo	Stalinův	k2eAgNnSc4d1	Stalinovo
vyloučení	vyloučení	k1gNnSc4	vyloučení
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
však	však	k9	však
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
diskuse	diskuse	k1gFnSc2	diskuse
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
Lenin	Lenin	k1gMnSc1	Lenin
svolal	svolat	k5eAaPmAgMnS	svolat
stranickou	stranický	k2eAgFnSc4d1	stranická
konferenci	konference	k1gFnSc4	konference
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
formálně	formálně	k6eAd1	formálně
ustavena	ustaven	k2eAgFnSc1d1	ustavena
bolševická	bolševický	k2eAgFnSc1d1	bolševická
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
<g/>
;	;	kIx,	;
nepřítomného	přítomný	k2eNgMnSc4d1	nepřítomný
Stalina	Stalin	k1gMnSc4	Stalin
kooptoval	kooptovat	k5eAaBmAgMnS	kooptovat
do	do	k7c2	do
ÚV	ÚV	kA	ÚV
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
poté	poté	k6eAd1	poté
uposlechl	uposlechnout	k5eAaPmAgMnS	uposlechnout
Leninovy	Leninův	k2eAgFnSc2d1	Leninova
výzvy	výzva	k1gFnSc2	výzva
doručené	doručený	k2eAgFnSc2d1	doručená
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
Ordžonikidzem	Ordžonikidz	k1gInSc7	Ordžonikidz
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1912	[number]	k4	1912
opět	opět	k6eAd1	opět
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
a	a	k8xC	a
stanul	stanout	k5eAaPmAgMnS	stanout
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
zatčení	zatčení	k1gNnSc2	zatčení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
hledán	hledat	k5eAaImNgMnS	hledat
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
bez	bez	k7c2	bez
platných	platný	k2eAgInPc2d1	platný
dokladů	doklad	k1gInPc2	doklad
<g/>
)	)	kIx)	)
několikrát	několikrát	k6eAd1	několikrát
překročil	překročit	k5eAaPmAgMnS	překročit
rusko-rakouské	ruskoakouský	k2eAgFnPc4d1	rusko-rakouská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
ho	on	k3xPp3gNnSc4	on
totiž	totiž	k9	totiž
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
studovat	studovat	k5eAaImF	studovat
národnostní	národnostní	k2eAgFnSc4d1	národnostní
politiku	politika	k1gFnSc4	politika
rakouských	rakouský	k2eAgMnPc2d1	rakouský
socialistů	socialist	k1gMnPc2	socialist
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
mladého	mladý	k2eAgMnSc4d1	mladý
Stalina	Stalin	k1gMnSc4	Stalin
velké	velký	k2eAgNnSc1d1	velké
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
pobyt	pobyt	k1gInSc1	pobyt
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
znovu	znovu	k6eAd1	znovu
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
na	na	k7c6	na
odlehlé	odlehlý	k2eAgFnSc6d1	odlehlá
stanici	stanice	k1gFnSc6	stanice
Kurejka	Kurejka	k1gFnSc1	Kurejka
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgNnPc2d3	nejhorší
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
byl	být	k5eAaImAgInS	být
předvolán	předvolat	k5eAaPmNgInS	předvolat
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
a	a	k8xC	a
uznán	uznat	k5eAaPmNgInS	uznat
neschopným	schopný	k2eNgInSc7d1	neschopný
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
důležitější	důležitý	k2eAgMnSc1d2	důležitější
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
komise	komise	k1gFnSc1	komise
neposlala	poslat	k5eNaPmAgFnS	poslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Kurejky	Kurejka	k1gFnSc2	Kurejka
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
bydlet	bydlet	k5eAaImF	bydlet
na	na	k7c6	na
přístupnějším	přístupný	k2eAgNnSc6d2	přístupnější
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
brzy	brzy	k6eAd1	brzy
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
propagoval	propagovat	k5eAaImAgMnS	propagovat
spojenectví	spojenectví	k1gNnSc4	spojenectví
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
stranami	strana	k1gFnPc7	strana
levého	levý	k2eAgNnSc2d1	levé
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
Lenina	Lenin	k1gMnSc2	Lenin
přijal	přijmout	k5eAaPmAgMnS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
ideu	idea	k1gFnSc4	idea
nesmiřitelného	smiřitelný	k2eNgInSc2d1	nesmiřitelný
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
ostatním	ostatní	k2eAgFnPc3d1	ostatní
stranám	strana	k1gFnPc3	strana
a	a	k8xC	a
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
ÚV	ÚV	kA	ÚV
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgInS	získat
třetí	třetí	k4xOgInSc1	třetí
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
po	po	k7c6	po
Leninovi	Lenin	k1gMnSc6	Lenin
a	a	k8xC	a
Zinověvovi	Zinověva	k1gMnSc6	Zinověva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
Lenin	Lenin	k1gMnSc1	Lenin
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
čtyřčlenné	čtyřčlenný	k2eAgNnSc4d1	čtyřčlenné
byro	byro	k1gNnSc4	byro
ÚV	ÚV	kA	ÚV
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
Zinověv	Zinověv	k1gMnSc1	Zinověv
<g/>
,	,	kIx,	,
Kameněv	Kameněv	k1gMnSc1	Kameněv
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Lenin	Lenin	k1gMnSc1	Lenin
a	a	k8xC	a
Zinověv	Zinověv	k1gMnPc1	Zinověv
museli	muset	k5eAaImAgMnP	muset
uchýlit	uchýlit	k5eAaPmF	uchýlit
do	do	k7c2	do
ilegality	ilegalita	k1gFnSc2	ilegalita
a	a	k8xC	a
současně	současně	k6eAd1	současně
Trockij	Trockij	k1gFnSc1	Trockij
s	s	k7c7	s
Kameněvem	Kameněv	k1gInSc7	Kameněv
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
Stalin	Stalin	k1gMnSc1	Stalin
do	do	k7c2	do
září	září	k1gNnSc2	září
až	až	k9	až
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
nyní	nyní	k6eAd1	nyní
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
vypracovával	vypracovávat	k5eAaImAgMnS	vypracovávat
pozici	pozice	k1gFnSc4	pozice
nad	nad	k7c7	nad
svými	svůj	k3xOyFgMnPc7	svůj
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
a	a	k8xC	a
konkurenty	konkurent	k1gMnPc7	konkurent
-	-	kIx~	-
Zinověvem	Zinověv	k1gInSc7	Zinověv
<g/>
,	,	kIx,	,
Kameněvem	Kameněv	k1gInSc7	Kameněv
<g/>
,	,	kIx,	,
Sverdlovem	Sverdlov	k1gInSc7	Sverdlov
a	a	k8xC	a
Trockým	Trocký	k1gMnSc7	Trocký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
bolševiky	bolševik	k1gMnPc7	bolševik
Lenin	Lenin	k1gMnSc1	Lenin
sestavil	sestavit	k5eAaPmAgInS	sestavit
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Stalin	Stalin	k1gMnSc1	Stalin
stal	stát	k5eAaPmAgMnS	stát
komisařem	komisař	k1gMnSc7	komisař
pro	pro	k7c4	pro
národnostní	národnostní	k2eAgFnSc4d1	národnostní
problematiku	problematika	k1gFnSc4	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
okrajových	okrajový	k2eAgFnPc2d1	okrajová
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
obývaných	obývaný	k2eAgFnPc2d1	obývaná
jinými	jiný	k2eAgFnPc7d1	jiná
národnostmi	národnost	k1gFnPc7	národnost
<g/>
,	,	kIx,	,
však	však	k8xC	však
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
bílých	bílý	k1gMnPc2	bílý
–	–	k?	–
většinou	většina	k1gFnSc7	většina
procarských	procarský	k2eAgMnPc2d1	procarský
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hodlali	hodlat	k5eAaImAgMnP	hodlat
bolševický	bolševický	k2eAgInSc4d1	bolševický
puč	puč	k1gInSc4	puč
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
funkci	funkce	k1gFnSc6	funkce
Stalin	Stalin	k1gMnSc1	Stalin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Deklaraci	deklarace	k1gFnSc4	deklarace
práv	právo	k1gNnPc2	právo
národů	národ	k1gInPc2	národ
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
deklarace	deklarace	k1gFnSc1	deklarace
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
národům	národ	k1gInPc3	národ
Ruska	Rusko	k1gNnSc2	Rusko
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
sebeurčení	sebeurčení	k1gNnSc6	sebeurčení
<g/>
.	.	kIx.	.
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
navíc	navíc	k6eAd1	navíc
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
národy	národ	k1gInPc7	národ
bývalého	bývalý	k2eAgNnSc2d1	bývalé
carského	carský	k2eAgNnSc2d1	carské
impéria	impérium	k1gNnSc2	impérium
mají	mít	k5eAaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
se	se	k3xPyFc4	se
Finsko	Finsko	k1gNnSc1	Finsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
dále	daleko	k6eAd2	daleko
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
a	a	k8xC	a
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
ruská	ruský	k2eAgFnSc1d1	ruská
města	město	k1gNnSc2	město
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
a	a	k8xC	a
Moskva	Moskva	k1gFnSc1	Moskva
trpěla	trpět	k5eAaImAgFnS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
práce	práce	k1gFnSc1	práce
lidového	lidový	k2eAgMnSc2d1	lidový
komisaře	komisař	k1gMnSc2	komisař
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nedůležitou	důležitý	k2eNgFnSc7d1	nedůležitá
a	a	k8xC	a
Lenin	Lenin	k1gMnSc1	Lenin
ho	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
dodavatelem	dodavatel	k1gMnSc7	dodavatel
obilí	obilí	k1gNnSc2	obilí
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
neomezenými	omezený	k2eNgNnPc7d1	neomezené
právy	právo	k1gNnPc7	právo
a	a	k8xC	a
plnou	plný	k2eAgFnSc7d1	plná
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1918	[number]	k4	1918
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
Stalin	Stalin	k1gMnSc1	Stalin
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
Caricynu	Caricyn	k1gInSc2	Caricyn
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Volgograd	Volgograd	k1gInSc1	Volgograd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vykonání	vykonání	k1gNnSc6	vykonání
úkolu	úkol	k1gInSc2	úkol
mu	on	k3xPp3gMnSc3	on
bránily	bránit	k5eAaImAgInP	bránit
střety	střet	k1gInPc1	střet
s	s	k7c7	s
kozáckými	kozácký	k2eAgFnPc7d1	kozácká
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
a	a	k8xC	a
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Trockého	Trockého	k2eAgFnSc2d1	Trockého
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
severokavkazského	severokavkazský	k2eAgInSc2d1	severokavkazský
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
vážným	vážný	k2eAgInPc3d1	vážný
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
Trockým	Trocký	k2eAgInSc7d1	Trocký
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
z	z	k7c2	z
Caricynu	Caricyn	k1gInSc2	Caricyn
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
Permu	perm	k1gInSc2	perm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krutými	krutý	k2eAgFnPc7d1	krutá
metodami	metoda	k1gFnPc7	metoda
obnovil	obnovit	k5eAaPmAgMnS	obnovit
bojeschopnost	bojeschopnost	k1gFnSc4	bojeschopnost
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
proti	proti	k7c3	proti
vojsku	vojsko	k1gNnSc3	vojsko
generála	generál	k1gMnSc2	generál
Juděniče	Juděniče	k1gMnSc2	Juděniče
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ofenzivu	ofenziva	k1gFnSc4	ofenziva
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úkoly	úkol	k1gInPc1	úkol
zadané	zadaný	k2eAgInPc1d1	zadaný
Leninem	Lenin	k1gMnSc7	Lenin
splnil	splnit	k5eAaPmAgMnS	splnit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
zde	zde	k6eAd1	zde
naplno	naplno	k6eAd1	naplno
používal	používat	k5eAaImAgMnS	používat
metody	metoda	k1gFnPc4	metoda
rudého	rudý	k2eAgInSc2d1	rudý
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgInPc4d1	charakterizovaný
např.	např.	kA	např.
popravami	poprava	k1gFnPc7	poprava
bez	bez	k7c2	bez
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zlegalizovány	zlegalizovat	k5eAaPmNgInP	zlegalizovat
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Lenina	Lenin	k1gMnSc4	Lenin
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
aplikovány	aplikovat	k5eAaBmNgFnP	aplikovat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
úderu	úder	k1gInSc3	úder
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
Tuchačevskij	Tuchačevskij	k1gMnSc1	Tuchačevskij
i	i	k8xC	i
Stalin	Stalin	k1gMnSc1	Stalin
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zřejmě	zřejmě	k6eAd1	zřejmě
dokonce	dokonce	k9	dokonce
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
ovládnout	ovládnout	k5eAaPmF	ovládnout
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
smetení	smetení	k1gNnSc6	smetení
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
spojení	spojení	k1gNnSc2	spojení
se	se	k3xPyFc4	se
s	s	k7c7	s
maďarskými	maďarský	k2eAgMnPc7d1	maďarský
<g/>
,	,	kIx,	,
německými	německý	k2eAgMnPc7d1	německý
a	a	k8xC	a
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
soudruhy	soudruh	k1gMnPc7	soudruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
politický	politický	k2eAgMnSc1d1	politický
velitel	velitel	k1gMnSc1	velitel
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
jižního	jižní	k2eAgNnSc2d1	jižní
křídla	křídlo	k1gNnSc2	křídlo
armády	armáda	k1gFnSc2	armáda
útočícího	útočící	k2eAgInSc2d1	útočící
na	na	k7c4	na
Lvov	Lvov	k1gInSc4	Lvov
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
je	být	k5eAaImIp3nS	být
vedl	vést	k5eAaImAgMnS	vést
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
když	když	k8xS	když
Tuchačevského	Tuchačevský	k2eAgInSc2d1	Tuchačevský
armáda	armáda	k1gFnSc1	armáda
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
okraj	okraj	k1gInSc4	okraj
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
Stalin	Stalin	k1gMnSc1	Stalin
rozkaz	rozkaz	k1gInSc4	rozkaz
přesunout	přesunout	k5eAaPmF	přesunout
hlavní	hlavní	k2eAgFnPc4d1	hlavní
složky	složka	k1gFnPc4	složka
armády	armáda	k1gFnSc2	armáda
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Tuchačevskému	Tuchačevský	k2eAgInSc3d1	Tuchačevský
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
však	však	k9	však
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
Lvov	Lvov	k1gInSc4	Lvov
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
podnikla	podniknout	k5eAaPmAgFnS	podniknout
polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
protiútok	protiútok	k1gInSc4	protiútok
a	a	k8xC	a
zahnala	zahnat	k5eAaPmAgFnS	zahnat
obě	dva	k4xCgFnPc4	dva
armády	armáda	k1gFnPc4	armáda
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byla	být	k5eAaImAgFnS	být
územní	územní	k2eAgFnSc1d1	územní
ztráta	ztráta	k1gFnSc1	ztráta
části	část	k1gFnSc2	část
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
(	(	kIx(	(
<g/>
asi	asi	k9	asi
třetiny	třetina	k1gFnPc4	třetina
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Druhé	druhý	k4xOgFnSc2	druhý
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
politbyra	politbyro	k1gNnSc2	politbyro
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
z	z	k7c2	z
návrhu	návrh	k1gInSc2	návrh
L.	L.	kA	L.
B.	B.	kA	B.
Kameněva	Kameněva	k1gFnSc1	Kameněva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnPc4	funkce
využil	využít	k5eAaPmAgInS	využít
okamžitě	okamžitě	k6eAd1	okamžitě
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
se	s	k7c7	s
samotným	samotný	k2eAgMnSc7d1	samotný
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
,	,	kIx,	,
když	když	k8xS	když
Stalin	Stalin	k1gMnSc1	Stalin
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
přeměnit	přeměnit	k5eAaPmF	přeměnit
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
autonomní	autonomní	k2eAgFnSc6d1	autonomní
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbavit	zbavit	k5eAaPmF	zbavit
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
zbývajících	zbývající	k2eAgInPc2d1	zbývající
prvků	prvek	k1gInPc2	prvek
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
nakonec	nakonec	k6eAd1	nakonec
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
,	,	kIx,	,
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Leninův	Leninův	k2eAgInSc1d1	Leninův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
mrtvici	mrtvice	k1gFnSc6	mrtvice
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1922	[number]	k4	1922
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
(	(	kIx(	(
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Kameněv	Kameněv	k1gMnSc1	Kameněv
a	a	k8xC	a
Bucharin	Bucharin	k1gInSc1	Bucharin
<g/>
)	)	kIx)	)
vydali	vydat	k5eAaPmAgMnP	vydat
příkaz	příkaz	k1gInSc4	příkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenin	Lenin	k1gMnSc1	Lenin
smí	smět	k5eAaImIp3nS	smět
pracovat	pracovat	k5eAaImF	pracovat
jen	jen	k9	jen
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
přijímat	přijímat	k5eAaImF	přijímat
návštěvy	návštěva	k1gFnSc2	návštěva
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
ho	on	k3xPp3gMnSc4	on
nesmí	smět	k5eNaImIp3nS	smět
informovat	informovat	k5eAaBmF	informovat
o	o	k7c6	o
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
takové	takový	k3xDgFnSc3	takový
izolaci	izolace	k1gFnSc3	izolace
se	se	k3xPyFc4	se
Lenin	Lenin	k1gMnSc1	Lenin
vzepřel	vzepřít	k5eAaPmAgMnS	vzepřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
neměl	mít	k5eNaImAgMnS	mít
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
měl	mít	k5eAaImAgMnS	mít
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
Stalinovi	Stalin	k1gMnSc3	Stalin
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c6	v
nepřátelství	nepřátelství	k1gNnSc6	nepřátelství
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
ale	ale	k9	ale
už	už	k6eAd1	už
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
prosadit	prosadit	k5eAaPmF	prosadit
nemohl	moct	k5eNaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Stihl	stihnout	k5eAaPmAgMnS	stihnout
už	už	k9	už
jen	jen	k6eAd1	jen
sepsat	sepsat	k5eAaPmF	sepsat
tzv.	tzv.	kA	tzv.
Dopis	dopis	k1gInSc4	dopis
sjezdu	sjezd	k1gInSc2	sjezd
(	(	kIx(	(
<g/>
označovaný	označovaný	k2eAgMnSc1d1	označovaný
též	též	k9	též
jako	jako	k9	jako
Leninova	Leninův	k2eAgFnSc1d1	Leninova
závěť	závěť	k1gFnSc1	závěť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
napsal	napsat	k5eAaPmAgMnS	napsat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Soudruh	soudruh	k1gMnSc1	soudruh
Stalin	Stalin	k1gMnSc1	Stalin
jako	jako	k8xC	jako
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
soustředil	soustředit	k5eAaPmAgMnS	soustředit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
já	já	k3xPp1nSc1	já
si	se	k3xPyFc3	se
nejsem	být	k5eNaImIp1nS	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vždy	vždy	k6eAd1	vždy
dost	dost	k6eAd1	dost
obezřetně	obezřetně	k6eAd1	obezřetně
užívat	užívat	k5eAaImF	užívat
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
hrubý	hrubý	k2eAgInSc1d1	hrubý
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jakžtakž	jakžtakž	k6eAd1	jakžtakž
dá	dát	k5eAaPmIp3nS	dát
trpět	trpět	k5eAaImF	trpět
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
trpět	trpět	k5eAaImF	trpět
u	u	k7c2	u
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
soudruhům	soudruh	k1gMnPc3	soudruh
navrhuji	navrhovat	k5eAaImIp1nS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uvážili	uvážit	k5eAaPmAgMnP	uvážit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Stalina	Stalin	k1gMnSc2	Stalin
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
přemístit	přemístit	k5eAaPmF	přemístit
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
<g/>
...	...	k?	...
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
Leninovy	Leninův	k2eAgFnSc2d1	Leninova
závěti	závěť	k1gFnSc2	závěť
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc7	svůj
pozicí	pozice	k1gFnSc7	pozice
již	již	k6eAd1	již
natolik	natolik	k6eAd1	natolik
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
osobně	osobně	k6eAd1	osobně
přečetl	přečíst	k5eAaPmAgMnS	přečíst
na	na	k7c6	na
Ústředním	ústřední	k2eAgInSc6d1	ústřední
výboru	výbor	k1gInSc6	výbor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
maximálně	maximálně	k6eAd1	maximálně
využil	využít	k5eAaPmAgMnS	využít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
i	i	k9	i
jeho	jeho	k3xOp3gMnPc4	jeho
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
nato	nato	k6eAd1	nato
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
svoji	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ale	ale	k8xC	ale
výbor	výbor	k1gInSc4	výbor
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
ji	on	k3xPp3gFnSc4	on
dokonce	dokonce	k9	dokonce
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
největší	veliký	k2eAgMnPc1d3	veliký
oponenti	oponent	k1gMnPc1	oponent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tak	tak	k6eAd1	tak
nevyužili	využít	k5eNaPmAgMnP	využít
poslední	poslední	k2eAgFnSc4d1	poslední
vážnou	vážný	k2eAgFnSc4d1	vážná
šanci	šance	k1gFnSc4	šance
Stalina	Stalin	k1gMnSc2	Stalin
sesadit	sesadit	k5eAaPmF	sesadit
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
si	se	k3xPyFc3	se
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
aktéři	aktér	k1gMnPc1	aktér
boje	boj	k1gInSc2	boj
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Leninově	Leninův	k2eAgFnSc6d1	Leninova
smrti	smrt	k1gFnSc6	smrt
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
aktéry	aktér	k1gMnPc7	aktér
boje	boj	k1gInSc2	boj
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Trockij	Trockij	k1gMnSc1	Trockij
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
zavražděn	zavražděn	k2eAgInSc1d1	zavražděn
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grigorij	Grigorít	k5eAaPmRp2nS	Grigorít
Zinovjev	Zinovjev	k1gFnSc1	Zinovjev
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Kameněv	Kameněv	k1gMnSc1	Kameněv
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Rykov	Rykov	k1gInSc1	Rykov
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Bucharin	Bucharin	k1gInSc1	Bucharin
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michail	Michail	k1gMnSc1	Michail
Tomskij	Tomskij	k1gMnSc1	Tomskij
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgInSc2	první
moskevského	moskevský	k2eAgInSc2d1	moskevský
procesu	proces	k1gInSc2	proces
<g/>
)	)	kIx)	)
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923-1925	[number]	k4	1923-1925
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Grigorije	Grigorije	k1gMnSc2	Grigorije
Zinověva	Zinověv	k1gMnSc2	Zinověv
a	a	k8xC	a
Lva	Lev	k1gMnSc2	Lev
Kameněva	Kameněv	k1gMnSc2	Kameněv
odstavil	odstavit	k5eAaPmAgMnS	odstavit
od	od	k7c2	od
velení	velení	k1gNnSc2	velení
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Trockého	Trocký	k1gMnSc2	Trocký
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
za	za	k7c4	za
přihlížení	přihlížení	k1gNnSc4	přihlížení
Trockého	Trocký	k1gMnSc2	Trocký
a	a	k8xC	a
Kameněva	Kameněv	k1gMnSc2	Kameněv
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
Zinověva	Zinověv	k1gMnSc4	Zinověv
z	z	k7c2	z
politbyra	politbyro	k1gNnSc2	politbyro
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kameněvem	Kameněv	k1gInSc7	Kameněv
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
Trockého	Trockého	k2eAgInSc1d1	Trockého
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
je	být	k5eAaImIp3nS	být
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
prosadil	prosadit	k5eAaPmAgMnS	prosadit
vyloučení	vyloučení	k1gNnSc4	vyloučení
Zinověva	Zinověv	k1gMnSc2	Zinověv
a	a	k8xC	a
Trockého	Trocký	k1gMnSc2	Trocký
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Trockého	Trockého	k2eAgMnSc1d1	Trockého
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgMnS	nechat
vypovědět	vypovědět	k5eAaPmF	vypovědět
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
nemlčel	mlčet	k5eNaImAgMnS	mlčet
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
agentem	agent	k1gMnSc7	agent
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
veřejně	veřejně	k6eAd1	veřejně
vystupujícím	vystupující	k2eAgMnSc7d1	vystupující
Stalinovým	Stalinův	k2eAgMnSc7d1	Stalinův
odpůrcem	odpůrce	k1gMnSc7	odpůrce
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Bucharin	Bucharin	k1gInSc1	Bucharin
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
věrný	věrný	k2eAgMnSc1d1	věrný
spojenec	spojenec	k1gMnSc1	spojenec
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
kolektivizaci	kolektivizace	k1gFnSc3	kolektivizace
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
teroru	teror	k1gInSc2	teror
při	při	k7c6	při
vymáhání	vymáhání	k1gNnSc6	vymáhání
odvodů	odvod	k1gInPc2	odvod
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
odstaveni	odstavit	k5eAaPmNgMnP	odstavit
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
i	i	k9	i
Rykov	Rykov	k1gInSc4	Rykov
a	a	k8xC	a
Tomskij	Tomskij	k1gFnSc4	Tomskij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diktátor	diktátor	k1gMnSc1	diktátor
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
moc	moc	k6eAd1	moc
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
Stalin	Stalin	k1gMnSc1	Stalin
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgInS	být
neomezeným	omezený	k2eNgMnSc7d1	neomezený
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
jeho	jeho	k3xOp3gMnPc2	jeho
přívrženců	přívrženec	k1gMnPc2	přívrženec
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
politbyru	politbyro	k1gNnSc6	politbyro
i	i	k8xC	i
v	v	k7c6	v
ÚV	ÚV	kA	ÚV
VKS	VKS	kA	VKS
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
mírnou	mírný	k2eAgFnSc4d1	mírná
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Stalinových	Stalinových	k2eAgMnPc2d1	Stalinových
odpůrců	odpůrce	k1gMnPc2	odpůrce
bylo	být	k5eAaImAgNnS	být
hodně	hodně	k6eAd1	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prodělala	prodělat	k5eAaPmAgFnS	prodělat
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
hladomor	hladomor	k1gInSc4	hladomor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
únosnosti	únosnost	k1gFnSc2	únosnost
<g/>
.	.	kIx.	.
</s>
<s>
Leninova	Leninův	k2eAgFnSc1d1	Leninova
NEP	Nep	k1gInSc4	Nep
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zajistit	zajistit	k5eAaPmF	zajistit
základní	základní	k2eAgFnSc4d1	základní
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozvoj	rozvoj	k1gInSc1	rozvoj
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
západní	západní	k2eAgFnPc1d1	západní
velmoci	velmoc	k1gFnPc1	velmoc
hrozily	hrozit	k5eAaImAgFnP	hrozit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
intervencí	intervence	k1gFnSc7	intervence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolektivizace	kolektivizace	k1gFnPc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
hladomor	hladomor	k1gMnSc1	hladomor
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
Stalinovým	Stalinův	k2eAgInSc7d1	Stalinův
cílem	cíl	k1gInSc7	cíl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1927	[number]	k4	1927
sjezdem	sjezd	k1gInSc7	sjezd
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
nařídil	nařídit	k5eAaPmAgMnS	nařídit
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
likvidaci	likvidace	k1gFnSc4	likvidace
kulaků	kulak	k1gMnPc2	kulak
jako	jako	k8xS	jako
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
31	[number]	k4	31
nechal	nechat	k5eAaPmAgMnS	nechat
deportovat	deportovat	k5eAaBmF	deportovat
miliony	milion	k4xCgInPc1	milion
rolníků	rolník	k1gMnPc2	rolník
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
pomoci	pomoct	k5eAaPmF	pomoct
k	k	k7c3	k
podmanění	podmanění	k1gNnSc3	podmanění
vesnického	vesnický	k2eAgNnSc2d1	vesnické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
aparát	aparát	k1gInSc1	aparát
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
získával	získávat	k5eAaImAgMnS	získávat
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
produkty	produkt	k1gInPc4	produkt
od	od	k7c2	od
rolníků	rolník	k1gMnPc2	rolník
vykupováním	vykupování	k1gNnSc7	vykupování
přebytků	přebytek	k1gInPc2	přebytek
za	za	k7c4	za
základní	základní	k2eAgFnPc4d1	základní
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
nebyl	být	k5eNaImAgInS	být
efektivní	efektivní	k2eAgMnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
probíhala	probíhat	k5eAaImAgFnS	probíhat
násilnými	násilný	k2eAgFnPc7d1	násilná
metodami	metoda	k1gFnPc7	metoda
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
narušení	narušení	k1gNnSc1	narušení
výrobních	výrobní	k2eAgInPc2d1	výrobní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
pokles	pokles	k1gInSc1	pokles
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
Povolží	Povolží	k1gNnSc6	Povolží
<g/>
,	,	kIx,	,
severním	severní	k2eAgInSc6d1	severní
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
postihla	postihnout	k5eAaPmAgFnS	postihnout
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
špatná	špatný	k2eAgFnSc1d1	špatná
úroda	úroda	k1gFnSc1	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
nezaopatřila	zaopatřit	k5eNaPmAgFnS	zaopatřit
žádné	žádný	k3yNgFnPc4	žádný
zásoby	zásoba	k1gFnPc4	zásoba
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
nedostatku	nedostatek	k1gInSc2	nedostatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musela	muset	k5eAaImAgFnS	muset
splácet	splácet	k5eAaImF	splácet
úvěry	úvěr	k1gInPc4	úvěr
především	především	k6eAd1	především
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1932	[number]	k4	1932
<g/>
/	/	kIx~	/
<g/>
1933	[number]	k4	1933
postihl	postihnout	k5eAaPmAgMnS	postihnout
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
SSSR	SSSR	kA	SSSR
hladomor	hladomor	k1gMnSc1	hladomor
<g/>
,	,	kIx,	,
především	především	k9	především
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zejména	zejména	k9	zejména
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
zemřely	zemřít	k5eAaPmAgFnP	zemřít
hladem	hlad	k1gInSc7	hlad
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dnešního	dnešní	k2eAgNnSc2d1	dnešní
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
hodnocení	hodnocení	k1gNnSc2	hodnocení
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
uvědomělý	uvědomělý	k2eAgInSc1d1	uvědomělý
teroristický	teroristický	k2eAgInSc1d1	teroristický
akt	akt	k1gInSc1	akt
stalinského	stalinský	k2eAgInSc2d1	stalinský
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
proti	proti	k7c3	proti
pokojným	pokojný	k2eAgMnPc3d1	pokojný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Ukrajincům	Ukrajinec	k1gMnPc3	Ukrajinec
jako	jako	k8xC	jako
národu	národ	k1gInSc3	národ
a	a	k8xC	a
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
sedlákům	sedlák	k1gInPc3	sedlák
jako	jako	k8xC	jako
třídě	třída	k1gFnSc6	třída
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Důsledkem	důsledek	k1gInSc7	důsledek
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
byla	být	k5eAaImAgFnS	být
také	také	k9	také
migrace	migrace	k1gFnSc1	migrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
ve	v	k7c6	v
významných	významný	k2eAgNnPc6d1	významné
střediscích	středisko	k1gNnPc6	středisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pětileté	pětiletý	k2eAgNnSc1d1	pětileté
plánování	plánování	k1gNnSc1	plánování
===	===	k?	===
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
řídit	řídit	k5eAaImF	řídit
pomocí	pomocí	k7c2	pomocí
pětiletek	pětiletka	k1gFnPc2	pětiletka
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
na	na	k7c6	na
období	období	k1gNnSc6	období
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
napadení	napadení	k1gNnSc2	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
hitlerovským	hitlerovský	k2eAgNnSc7d1	hitlerovské
Německem	Německo	k1gNnSc7	Německo
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
ještě	ještě	k9	ještě
druhá	druhý	k4xOgFnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
pětiletka	pětiletka	k1gFnSc1	pětiletka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
již	již	k6eAd1	již
kvůli	kvůli	k7c3	kvůli
válce	válec	k1gInSc2	válec
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kolektivizací	kolektivizace	k1gFnSc7	kolektivizace
byla	být	k5eAaImAgFnS	být
vysokým	vysoký	k2eAgNnSc7d1	vysoké
tempem	tempo	k1gNnSc7	tempo
započata	započnout	k5eAaPmNgFnS	započnout
industrializace	industrializace	k1gFnSc1	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Prioritní	prioritní	k2eAgMnSc1d1	prioritní
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
pětiletce	pětiletka	k1gFnSc6	pětiletka
rozvoj	rozvoj	k1gInSc1	rozvoj
dosud	dosud	k6eAd1	dosud
slabého	slabý	k2eAgInSc2d1	slabý
kovozpracujícího	kovozpracující	k2eAgInSc2d1	kovozpracující
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc4	rozšíření
těžby	těžba	k1gFnSc2	těžba
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
pětiletce	pětiletka	k1gFnSc6	pětiletka
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
čistka	čistka	k1gFnSc1	čistka
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
spouštěčů	spouštěč	k1gInPc2	spouštěč
tzv.	tzv.	kA	tzv.
Velké	velká	k1gFnSc2	velká
čistky	čistka	k1gFnSc2	čistka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vražda	vražda	k1gFnSc1	vražda
Stalinova	Stalinův	k2eAgMnSc2d1	Stalinův
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
S.	S.	kA	S.
M.	M.	kA	M.
Kirova	Kirov	k1gInSc2	Kirov
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
si	se	k3xPyFc3	se
vraždu	vražda	k1gFnSc4	vražda
objednal	objednat	k5eAaPmAgMnS	objednat
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
neprokázaných	prokázaný	k2eNgFnPc2d1	neprokázaná
podezření	podezřeň	k1gFnPc2	podezřeň
na	na	k7c4	na
Stalina	Stalin	k1gMnSc4	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
atentát	atentát	k1gInSc1	atentát
nedopadl	dopadnout	k5eNaPmAgInS	dopadnout
i	i	k9	i
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Stalin	Stalin	k1gMnSc1	Stalin
dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mj.	mj.	kA	mj.
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
maximálně	maximálně	k6eAd1	maximálně
desetidenní	desetidenní	k2eAgNnSc4d1	desetidenní
projednání	projednání	k1gNnSc4	projednání
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
dalších	další	k2eAgFnPc2d1	další
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
odvolání	odvolání	k1gNnSc2	odvolání
a	a	k8xC	a
provedení	provedení	k1gNnSc2	provedení
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
zatýkání	zatýkání	k1gNnSc3	zatýkání
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
stranických	stranický	k2eAgMnPc2d1	stranický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
vrcholem	vrchol	k1gInSc7	vrchol
Velké	velký	k2eAgFnSc2d1	velká
čistky	čistka	k1gFnSc2	čistka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
tzv.	tzv.	kA	tzv.
Moskevské	moskevský	k2eAgInPc1d1	moskevský
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgInPc6	který
byli	být	k5eAaImAgMnP	být
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
bývalí	bývalý	k2eAgMnPc1d1	bývalý
Leninovi	Leninův	k2eAgMnPc1d1	Leninův
a	a	k8xC	a
Stalinovi	Stalinův	k2eAgMnPc1d1	Stalinův
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
Lev	Lev	k1gMnSc1	Lev
Kameněv	Kameněv	k1gMnSc1	Kameněv
<g/>
,	,	kIx,	,
Grigorij	Grigorij	k1gMnSc1	Grigorij
Zinověv	Zinověv	k1gMnSc1	Zinověv
a	a	k8xC	a
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Bucharin	Bucharin	k1gInSc1	Bucharin
<g/>
.	.	kIx.	.
</s>
<s>
Popraven	popravit	k5eAaPmNgInS	popravit
byl	být	k5eAaImAgInS	být
i	i	k9	i
bolševický	bolševický	k2eAgMnSc1d1	bolševický
vůdce	vůdce	k1gMnSc1	vůdce
Vladimir	Vladimir	k1gMnSc1	Vladimir
Antonov-Ovsejenko	Antonov-Ovsejenka	k1gFnSc5	Antonov-Ovsejenka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
během	během	k7c2	během
říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
zahájil	zahájit	k5eAaPmAgInS	zahájit
výstřelem	výstřel	k1gInSc7	výstřel
z	z	k7c2	z
křižníku	křižník	k1gInSc2	křižník
Aurora	Aurora	k1gFnSc1	Aurora
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Zimní	zimní	k2eAgInSc4d1	zimní
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
také	také	k9	také
maďarský	maďarský	k2eAgMnSc1d1	maďarský
bolševický	bolševický	k2eAgMnSc1d1	bolševický
politik	politik	k1gMnSc1	politik
Béla	Béla	k1gMnSc1	Béla
Kun	kuna	k1gFnPc2	kuna
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jako	jako	k8xC	jako
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k2eAgMnPc1d1	jiný
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
trockismu	trockismus	k1gInSc2	trockismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
popraven	popraven	k2eAgMnSc1d1	popraven
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bělogvardějský	bělogvardějský	k2eAgMnSc1d1	bělogvardějský
generál	generál	k1gMnSc1	generál
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Represe	represe	k1gFnPc1	represe
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Ježova	Ježův	k2eAgMnSc2d1	Ježův
<g/>
,	,	kIx,	,
šéfa	šéf	k1gMnSc2	šéf
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zatýkány	zatýkat	k5eAaImNgInP	zatýkat
a	a	k8xC	a
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popravovány	popravovat	k5eAaImNgInP	popravovat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
čistce	čistka	k1gFnSc3	čistka
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
většinu	většina	k1gFnSc4	většina
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
velitelů	velitel	k1gMnPc2	velitel
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
poprava	poprava	k1gFnSc1	poprava
maršála	maršál	k1gMnSc2	maršál
Tuchačevského	Tuchačevský	k2eAgMnSc2d1	Tuchačevský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Ježov	Ježov	k1gInSc1	Ježov
byl	být	k5eAaImAgInS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1938	[number]	k4	1938
zbaven	zbavit	k5eAaPmNgMnS	zbavit
předsednictví	předsednictví	k1gNnSc2	předsednictví
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Ježova	Ježův	k2eAgFnSc1d1	Ježova
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
NKVD	NKVD	kA	NKVD
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Lavrentij	Lavrentij	k1gMnSc1	Lavrentij
Berija	Berija	k1gMnSc1	Berija
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
NKVD	NKVD	kA	NKVD
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
1	[number]	k4	1
548	[number]	k4	548
367	[number]	k4	367
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
681	[number]	k4	681
692	[number]	k4	692
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
znamená	znamenat	k5eAaImIp3nS	znamenat
asi	asi	k9	asi
1000	[number]	k4	1000
poprav	poprava	k1gFnPc2	poprava
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k6eAd1	jenom
v	v	k7c6	v
Leningradu	Leningrad	k1gInSc2	Leningrad
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zatčenými	zatčený	k1gMnPc7	zatčený
byli	být	k5eAaImAgMnP	být
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Isaak	Isaak	k1gMnSc1	Isaak
Babel	Babel	k1gMnSc1	Babel
<g/>
,	,	kIx,	,
Varlam	Varlam	k1gInSc1	Varlam
Šalamov	Šalamov	k1gInSc1	Šalamov
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
Pilňak	Pilňak	k1gMnSc1	Pilňak
a	a	k8xC	a
Osip	Osip	k1gMnSc1	Osip
Mandelštam	Mandelštam	k1gInSc1	Mandelštam
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
sovětského	sovětský	k2eAgInSc2d1	sovětský
raketového	raketový	k2eAgInSc2d1	raketový
programu	program	k1gInSc2	program
Sergej	Sergej	k1gMnSc1	Sergej
Koroljov	Koroljovo	k1gNnPc2	Koroljovo
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Vladimir	Vladimir	k1gMnSc1	Vladimir
Varankin	Varankin	k1gMnSc1	Varankin
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Konstantin	Konstantin	k1gMnSc1	Konstantin
Rokossovskij	Rokossovskij	k1gMnSc1	Rokossovskij
<g/>
,	,	kIx,	,
prarodiče	prarodič	k1gMnPc4	prarodič
Michaila	Michail	k1gMnSc4	Michail
Gorbačova	Gorbačův	k2eAgMnSc4d1	Gorbačův
nebo	nebo	k8xC	nebo
otec	otec	k1gMnSc1	otec
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
Alexeje	Alexej	k1gMnSc2	Alexej
Leonova	Leonův	k2eAgMnSc2d1	Leonův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
expanze	expanze	k1gFnSc2	expanze
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
pod	pod	k7c7	pod
Stalinovým	Stalinův	k2eAgNnSc7d1	Stalinovo
vedením	vedení	k1gNnSc7	vedení
omezil	omezit	k5eAaPmAgMnS	omezit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
postupné	postupný	k2eAgNnSc1d1	postupné
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
aktivně	aktivně	k6eAd1	aktivně
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
krizí	krize	k1gFnPc2	krize
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podpořil	podpořit	k5eAaPmAgMnS	podpořit
dobrovolnickými	dobrovolnický	k2eAgFnPc7d1	dobrovolnická
oddíly	oddíl	k1gInPc1	oddíl
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
zabránily	zabránit	k5eAaPmAgFnP	zabránit
společné	společný	k2eAgFnPc1d1	společná
sovětsko-mongolské	sovětskoongolský	k2eAgFnPc1d1	sovětsko-mongolský
síly	síla	k1gFnPc1	síla
okupování	okupování	k1gNnSc2	okupování
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
Japonskem	Japonsko	k1gNnSc7	Japonsko
v	v	k7c6	v
japonsko-čínské	japonsko-čínský	k2eAgFnSc6d1	japonsko-čínská
válce	válka	k1gFnSc6	válka
</s>
</p>
<p>
<s>
Italskou	italský	k2eAgFnSc7d1	italská
agresí	agrese	k1gFnSc7	agrese
proti	proti	k7c3	proti
Habeši	Habeš	k1gFnSc3	Habeš
nejprve	nejprve	k6eAd1	nejprve
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
na	na	k7c6	na
Itálii	Itálie	k1gFnSc6	Itálie
uvalit	uvalit	k5eAaPmF	uvalit
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
i	i	k9	i
jen	jen	k9	jen
symbolické	symbolický	k2eAgFnPc1d1	symbolická
sankce	sankce	k1gFnPc1	sankce
<g/>
.	.	kIx.	.
<g/>
Maxim	maxim	k1gInSc1	maxim
Litvinov	Litvinov	k1gInSc1	Litvinov
<g/>
,	,	kIx,	,
Stalinův	Stalinův	k2eAgMnSc1d1	Stalinův
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1930	[number]	k4	1930
až	až	k6eAd1	až
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
hrozbu	hrozba	k1gFnSc4	hrozba
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
sblížení	sblížení	k1gNnSc6	sblížení
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
uznání	uznání	k1gNnSc4	uznání
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgInS	prosadit
také	také	k9	také
účast	účast	k1gFnSc4	účast
SSSR	SSSR	kA	SSSR
ve	v	k7c6	v
Společnosti	společnost	k1gFnSc6	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Litvinov	Litvinov	k1gInSc4	Litvinov
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
sbližování	sbližování	k1gNnSc2	sbližování
se	s	k7c7	s
západními	západní	k2eAgFnPc7d1	západní
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k9	také
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
podpisem	podpis	k1gInSc7	podpis
sovětsko-německého	sovětskoěmecký	k2eAgInSc2d1	sovětsko-německý
paktu	pakt	k1gInSc2	pakt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
na	na	k7c4	na
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
příkaz	příkaz	k1gInSc4	příkaz
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Molotovem	Molotovo	k1gNnSc7	Molotovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Expanze	expanze	k1gFnSc2	expanze
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
stál	stát	k5eAaImAgInS	stát
svět	svět	k1gInSc1	svět
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
další	další	k2eAgFnSc2d1	další
velké	velký	k2eAgFnSc2d1	velká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nacismus	nacismus	k1gInSc1	nacismus
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
Střední	střední	k2eAgFnSc7d1	střední
Evropou	Evropa	k1gFnSc7	Evropa
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
obsazením	obsazení	k1gNnSc7	obsazení
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
vojenské	vojenský	k2eAgNnSc4d1	vojenské
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
napadení	napadení	k1gNnSc2	napadení
Německem	Německo	k1gNnSc7	Německo
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
vojensky	vojensky	k6eAd1	vojensky
i	i	k9	i
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Československo-sovětská	československoovětský	k2eAgFnSc1d1	československo-sovětská
smlouva	smlouva	k1gFnSc1	smlouva
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
agresivitou	agresivita	k1gFnSc7	agresivita
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
snažil	snažit	k5eAaImAgMnS	snažit
Hitlera	Hitler	k1gMnSc4	Hitler
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svých	svůj	k3xOyFgMnPc2	svůj
diplomatů	diplomat	k1gMnPc2	diplomat
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1938	[number]	k4	1938
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
závazek	závazek	k1gInSc4	závazek
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pomoci	pomoc	k1gFnSc2	pomoc
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
francouzského	francouzský	k2eAgInSc2d1	francouzský
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
velmoc	velmoc	k1gFnSc1	velmoc
měl	mít	k5eAaImAgMnS	mít
nabídnout	nabídnout	k5eAaPmF	nabídnout
Československé	československý	k2eAgFnSc3d1	Československá
vládě	vláda	k1gFnSc3	vláda
pomoc	pomoc	k1gFnSc4	pomoc
i	i	k8xC	i
po	po	k7c6	po
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
údajný	údajný	k2eAgInSc4d1	údajný
rozhovor	rozhovor	k1gInSc4	rozhovor
Klementa	Klement	k1gMnSc2	Klement
Gottvalda	Gottvald	k1gMnSc2	Gottvald
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
Sergejem	Sergej	k1gMnSc7	Sergej
Alexandrovským	Alexandrovský	k2eAgMnSc7d1	Alexandrovský
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
též	též	k9	též
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
zapojil	zapojit	k5eAaPmAgMnS	zapojit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
na	na	k7c4	na
Společnost	společnost	k1gFnSc4	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
označila	označit	k5eAaPmAgFnS	označit
Německo	Německo	k1gNnSc4	Německo
za	za	k7c2	za
agresora	agresor	k1gMnSc2	agresor
<g/>
.	.	kIx.	.
<g/>
Problematická	problematický	k2eAgFnSc1d1	problematická
byla	být	k5eAaImAgFnS	být
možná	možná	k9	možná
účinnost	účinnost	k1gFnSc1	účinnost
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
přes	přes	k7c4	přes
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
byla	být	k5eAaImAgFnS	být
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
průjezdu	průjezd	k1gInSc2	průjezd
neúspěšná	úspěšný	k2eNgNnPc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
nebyla	být	k5eNaImAgFnS	být
do	do	k7c2	do
září	září	k1gNnSc2	září
1938	[number]	k4	1938
podepsána	podepsán	k2eAgFnSc1d1	podepsána
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
speciální	speciální	k2eAgFnSc2d1	speciální
železnice	železnice	k1gFnSc2	železnice
pro	pro	k7c4	pro
armádní	armádní	k2eAgInSc4d1	armádní
tranzit	tranzit	k1gInSc4	tranzit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pakt	pakt	k1gInSc1	pakt
Ribentropp-Molotov	Ribentropp-Molotov	k1gInSc1	Ribentropp-Molotov
a	a	k8xC	a
napadení	napadení	k1gNnSc1	napadení
Polska	Polsko	k1gNnSc2	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
Stalin	Stalin	k1gMnSc1	Stalin
od	od	k7c2	od
Západu	západ	k1gInSc2	západ
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
nalézt	nalézt	k5eAaBmF	nalézt
nemohl	moct	k5eNaImAgMnS	moct
<g/>
,	,	kIx,	,
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
německým	německý	k2eAgInSc7d1	německý
přepadení	přepadení	k1gNnSc4	přepadení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc3d1	známá
jako	jako	k8xC	jako
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajném	tajný	k2eAgInSc6d1	tajný
dodatku	dodatek	k1gInSc6	dodatek
k	k	k7c3	k
protokolu	protokol	k1gInSc3	protokol
byly	být	k5eAaImAgFnP	být
vymezeny	vymezen	k2eAgFnPc1d1	vymezena
sféry	sféra	k1gFnPc1	sféra
vlivu	vliv	k1gInSc2	vliv
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nejprve	nejprve	k6eAd1	nejprve
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
na	na	k7c6	na
mandžuských	mandžuský	k2eAgFnPc6d1	mandžuská
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
poté	poté	k6eAd1	poté
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
napadl	napadnout	k5eAaPmAgMnS	napadnout
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
prohrávající	prohrávající	k2eAgFnPc1d1	prohrávající
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ústy	ústa	k1gNnPc7	ústa
Molotova	Molotův	k2eAgNnSc2d1	Molotův
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
chránit	chránit	k5eAaImF	chránit
ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
a	a	k8xC	a
běloruské	běloruský	k2eAgNnSc1d1	běloruské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Rezervní	rezervní	k2eAgFnPc1d1	rezervní
jednotky	jednotka	k1gFnPc1	jednotka
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
na	na	k7c6	na
východních	východní	k2eAgFnPc6d1	východní
hranicích	hranice	k1gFnPc6	hranice
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
asi	asi	k9	asi
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
válečných	válečný	k2eAgMnPc2d1	válečný
a	a	k8xC	a
civilních	civilní	k2eAgMnPc2d1	civilní
zajatců	zajatec	k1gMnPc2	zajatec
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Katyňském	Katyňský	k2eAgInSc6d1	Katyňský
masakru	masakr	k1gInSc6	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vyjádření	vyjádření	k1gNnSc2	vyjádření
ruské	ruský	k2eAgFnSc2d1	ruská
Dumy	duma	k1gFnSc2	duma
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
hromadná	hromadný	k2eAgFnSc1d1	hromadná
vražda	vražda	k1gFnSc1	vražda
provedena	provést	k5eAaPmNgFnS	provést
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Stalina	Stalin	k1gMnSc2	Stalin
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
<g/>
Hrozbou	hrozba	k1gFnSc7	hrozba
násilím	násilí	k1gNnSc7	násilí
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgMnS	vynutit
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
rozmístění	rozmístění	k1gNnSc1	rozmístění
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
a	a	k8xC	a
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
a	a	k8xC	a
ještě	ještě	k9	ještě
dalšími	další	k2eAgInPc7d1	další
požadavky	požadavek	k1gInPc7	požadavek
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zimní	zimní	k2eAgFnSc1d1	zimní
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
SSSR	SSSR	kA	SSSR
Finsko	Finsko	k1gNnSc1	Finsko
napadl	napadnout	k5eAaPmAgMnS	napadnout
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Zimní	zimní	k2eAgFnSc1d1	zimní
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
dělnicko-rolnická	dělnickoolnický	k2eAgFnSc1d1	dělnicko-rolnická
vláda	vláda	k1gFnSc1	vláda
Finska	Finsko	k1gNnSc2	Finsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Otto	Otto	k1gMnSc1	Otto
Kuusinena	Kuusinena	k1gFnSc1	Kuusinena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Stalin	Stalin	k1gMnSc1	Stalin
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
legitimní	legitimní	k2eAgFnSc4d1	legitimní
vládu	vláda	k1gFnSc4	vláda
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Finové	Fin	k1gMnPc1	Fin
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
hůře	zle	k6eAd2	zle
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP	vyzbrojit
a	a	k8xC	a
několikanásobně	několikanásobně	k6eAd1	několikanásobně
přečísleni	přečíslit	k5eAaPmNgMnP	přečíslit
<g/>
,	,	kIx,	,
bojovali	bojovat	k5eAaImAgMnP	bojovat
s	s	k7c7	s
výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
urputností	urputnost	k1gFnSc7	urputnost
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
zdecimovaná	zdecimovaný	k2eAgFnSc1d1	zdecimovaná
čistkami	čistka	k1gFnPc7	čistka
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
lámáním	lámání	k1gNnSc7	lámání
jejich	jejich	k3xOp3gInSc2	jejich
odporu	odpor	k1gInSc2	odpor
obrovské	obrovský	k2eAgFnPc4d1	obrovská
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
potíže	potíž	k1gFnPc1	potíž
se	s	k7c7	s
zásobováním	zásobování	k1gNnSc7	zásobování
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
zapojením	zapojení	k1gNnSc7	zapojení
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
nevyprovokovanou	vyprovokovaný	k2eNgFnSc4d1	nevyprovokovaná
agresi	agrese	k1gFnSc4	agrese
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ze	z	k7c2	z
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
prozatím	prozatím	k6eAd1	prozatím
ustoupit	ustoupit	k5eAaPmF	ustoupit
od	od	k7c2	od
podmanění	podmanění	k1gNnSc2	podmanění
celého	celý	k2eAgNnSc2d1	celé
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
spokojil	spokojit	k5eAaPmAgMnS	spokojit
se	se	k3xPyFc4	se
s	s	k7c7	s
územními	územní	k2eAgInPc7d1	územní
zisky	zisk	k1gInPc7	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
Finska	Finsko	k1gNnSc2	Finsko
ze	z	k7c2	z
zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
činily	činit	k5eAaImAgFnP	činit
asi	asi	k9	asi
22	[number]	k4	22
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
50	[number]	k4	50
000	[number]	k4	000
těžce	těžce	k6eAd1	těžce
raněných	raněný	k2eAgMnPc2d1	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
ztratila	ztratit	k5eAaPmAgFnS	ztratit
asi	asi	k9	asi
120-130	[number]	k4	120-130
tisíc	tisíc	k4xCgInPc2	tisíc
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
180-190	[number]	k4	180-190
tisíc	tisíc	k4xCgInSc1	tisíc
zraněných	zraněný	k1gMnPc2	zraněný
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
6500	[number]	k4	6500
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expanze	expanze	k1gFnPc1	expanze
do	do	k7c2	do
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
a	a	k8xC	a
Besarábie	Besarábie	k1gFnSc2	Besarábie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
SSSR	SSSR	kA	SSSR
násilím	násilí	k1gNnSc7	násilí
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
paktem	pakt	k1gInSc7	pakt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Německo	Německo	k1gNnSc4	Německo
to	ten	k3xDgNnSc1	ten
akceptovalo	akceptovat	k5eAaBmAgNnS	akceptovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přinutil	přinutit	k5eAaPmAgInS	přinutit
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
k	k	k7c3	k
odevzdání	odevzdání	k1gNnSc3	odevzdání
Besarábie	Besarábie	k1gFnSc2	Besarábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
předválečných	předválečný	k2eAgFnPc2d1	předválečná
expanzí	expanze	k1gFnPc2	expanze
===	===	k?	===
</s>
</p>
<p>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
mezitím	mezitím	k6eAd1	mezitím
získalo	získat	k5eAaPmAgNnS	získat
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
záruky	záruka	k1gFnPc4	záruka
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Finsku	Finsko	k1gNnSc6	Finsko
objevily	objevit	k5eAaPmAgFnP	objevit
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Stalin	Stalin	k1gMnSc1	Stalin
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
obsazení	obsazení	k1gNnSc2	obsazení
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
svoji	svůj	k3xOyFgFnSc4	svůj
expanzi	expanze	k1gFnSc4	expanze
Evropou	Evropa	k1gFnSc7	Evropa
ukončil	ukončit	k5eAaPmAgInS	ukončit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
se	se	k3xPyFc4	se
necítil	cítit	k5eNaImAgMnS	cítit
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
potenciální	potenciální	k2eAgInPc1d1	potenciální
cíle	cíl	k1gInPc1	cíl
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
německou	německý	k2eAgFnSc7d1	německá
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
pacifikaci	pacifikace	k1gFnSc4	pacifikace
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velká	velký	k2eAgFnSc1d1	velká
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Napadení	napadení	k1gNnSc6	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Hitlerovské	hitlerovský	k2eAgNnSc1d1	hitlerovské
Německo	Německo	k1gNnSc1	Německo
SSSR	SSSR	kA	SSSR
přepadlo	přepadnout	k5eAaPmAgNnS	přepadnout
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
tomu	ten	k3xDgNnSc3	ten
zprvu	zprvu	k6eAd1	zprvu
nemohl	moct	k5eNaImAgMnS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
;	;	kIx,	;
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
provokaci	provokace	k1gFnSc4	provokace
německých	německý	k2eAgMnPc2d1	německý
generálů	generál	k1gMnPc2	generál
a	a	k8xC	a
že	že	k8xS	že
Hitler	Hitler	k1gMnSc1	Hitler
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nemůže	moct	k5eNaImIp3nS	moct
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Věrolomný	věrolomný	k2eAgInSc1d1	věrolomný
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
čistky	čistka	k1gFnPc4	čistka
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
z	z	k7c2	z
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
způsobil	způsobit	k5eAaPmAgMnS	způsobit
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
značné	značný	k2eAgFnSc2d1	značná
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
;	;	kIx,	;
její	její	k3xOp3gNnSc1	její
letectvo	letectvo	k1gNnSc1	letectvo
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
<g/>
%	%	kIx~	%
zničeno	zničen	k2eAgNnSc4d1	zničeno
<g/>
,	,	kIx,	,
tanková	tankový	k2eAgNnPc4d1	tankové
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
střetla	střetnout	k5eAaPmAgFnS	střetnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
navzdory	navzdory	k7c3	navzdory
kvantitativní	kvantitativní	k2eAgFnSc3d1	kvantitativní
převaze	převaha	k1gFnSc3	převaha
nad	nad	k7c7	nad
německými	německý	k2eAgInPc7d1	německý
pancéři	pancéř	k1gInPc7	pancéř
zlikvidována	zlikvidovat	k5eAaPmNgFnS	zlikvidovat
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
sám	sám	k3xTgMnSc1	sám
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
s	s	k7c7	s
emotivním	emotivní	k2eAgInSc7d1	emotivní
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
překvapil	překvapit	k5eAaPmAgMnS	překvapit
posluchače	posluchač	k1gMnSc4	posluchač
už	už	k6eAd1	už
oslovením	oslovení	k1gNnSc7	oslovení
"	"	kIx"	"
<g/>
Soudruzi	soudruh	k1gMnPc1	soudruh
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
a	a	k8xC	a
užil	užít	k5eAaPmAgMnS	užít
i	i	k8xC	i
spojení	spojení	k1gNnSc1	spojení
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
svatá	svatý	k2eAgFnSc1d1	svatá
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
územních	územní	k2eAgFnPc6d1	územní
ztrátách	ztráta	k1gFnPc6	ztráta
a	a	k8xC	a
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
lid	lid	k1gInSc4	lid
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
nepříteli	nepřítel	k1gMnSc3	nepřítel
i	i	k8xC	i
vlastním	vlastní	k2eAgMnPc3d1	vlastní
dezertérům	dezertér	k1gMnPc3	dezertér
a	a	k8xC	a
zbabělcům	zbabělec	k1gMnPc3	zbabělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
centralizace	centralizace	k1gFnSc1	centralizace
moci	moct	k5eAaImF	moct
za	za	k7c2	za
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
předsedou	předseda	k1gMnSc7	předseda
Rady	rada	k1gFnSc2	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zastával	zastávat	k5eAaImAgMnS	zastávat
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
Státního	státní	k2eAgInSc2d1	státní
výboru	výbor	k1gInSc2	výbor
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
lidového	lidový	k2eAgMnSc2d1	lidový
komisaře	komisař	k1gMnSc2	komisař
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
štáb	štáb	k1gInSc1	štáb
(	(	kIx(	(
<g/>
С	С	k?	С
Г	Г	k?	Г
К	К	k?	К
<g/>
)	)	kIx)	)
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
(	(	kIx(	(
<g/>
na	na	k7c4	na
С	С	k?	С
В	В	k?	В
К	К	k?	К
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
místo	místo	k7c2	místo
Timošenka	Timošenka	k1gFnSc1	Timošenka
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Stalin	Stalin	k1gMnSc1	Stalin
Timošenka	Timošenka	k1gFnSc1	Timošenka
nahradil	nahradit	k5eAaPmAgInS	nahradit
i	i	k9	i
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
soustředil	soustředit	k5eAaPmAgInS	soustředit
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
vládní	vládní	k2eAgFnSc4d1	vládní
<g/>
,	,	kIx,	,
stranickou	stranický	k2eAgFnSc4d1	stranická
i	i	k8xC	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
<g/>
.	.	kIx.	.
<g/>
Zemi	zem	k1gFnSc4	zem
řídil	řídit	k5eAaImAgMnS	řídit
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgNnSc2d3	veliký
ohrožení	ohrožení	k1gNnSc2	ohrožení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
v	v	k7c6	v
období	období	k1gNnSc6	období
bitvy	bitva	k1gFnSc2	bitva
před	před	k7c7	před
Moskvou	Moskva	k1gFnSc7	Moskva
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1941	[number]	k4	1941
-	-	kIx~	-
jaro	jaro	k1gNnSc4	jaro
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
přítomnost	přítomnost	k1gFnSc4	přítomnost
a	a	k8xC	a
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
morálky	morálka	k1gFnSc2	morálka
napadené	napadený	k2eAgFnSc2d1	napadená
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xC	jak
však	však	k9	však
uvedl	uvést	k5eAaPmAgMnS	uvést
N.	N.	kA	N.
S.	S.	kA	S.
Chruščov	Chruščov	k1gInSc1	Chruščov
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
v	v	k7c4	v
r.	r.	kA	r.
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
nechápal	chápat	k5eNaImAgMnS	chápat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
frontách	fronta	k1gFnPc6	fronta
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
válku	válka	k1gFnSc4	válka
nebyl	být	k5eNaImAgMnS	být
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
krátkou	krátký	k2eAgFnSc7d1	krátká
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
úseků	úsek	k1gInPc2	úsek
fronty	fronta	k1gFnSc2	fronta
či	či	k8xC	či
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
osvobozeném	osvobozený	k2eAgNnSc6d1	osvobozené
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
často	často	k6eAd1	často
vydával	vydávat	k5eAaPmAgMnS	vydávat
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
neodpovídaly	odpovídat	k5eNaImAgInP	odpovídat
skutečné	skutečný	k2eAgFnSc3d1	skutečná
situaci	situace	k1gFnSc3	situace
a	a	k8xC	a
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	Slabá	k1gFnSc1	Slabá
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnPc4	jeho
strategická	strategický	k2eAgNnPc4d1	strategické
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
září	září	k1gNnSc6	září
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
žádostí	žádost	k1gFnSc7	žádost
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
dopustil	dopustit	k5eAaPmAgInS	dopustit
obklíčení	obklíčení	k1gNnSc4	obklíčení
a	a	k8xC	a
zničení	zničení	k1gNnSc4	zničení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
seskupení	seskupení	k1gNnSc2	seskupení
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Kyjevský	kyjevský	k2eAgInSc4d1	kyjevský
kotel	kotel	k1gInSc4	kotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
jednání	jednání	k1gNnSc1	jednání
se	s	k7c7	s
Stalinovou	Stalinův	k2eAgFnSc7d1	Stalinova
účastí	účast	k1gFnSc7	účast
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
za	za	k7c2	za
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
účasti	účast	k1gFnSc2	účast
i	i	k8xC	i
část	část	k1gFnSc1	část
konferencí	konference	k1gFnPc2	konference
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
W.	W.	kA	W.
Averell	Averell	k1gMnSc1	Averell
Harriman	Harriman	k1gMnSc1	Harriman
<g/>
,	,	kIx,	,
Lord	lord	k1gMnSc1	lord
Beaverbrook	Beaverbrook	k1gInSc1	Beaverbrook
<g/>
,	,	kIx,	,
Molotov	Molotov	k1gInSc1	Molotov
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
pomoc	pomoc	k1gFnSc1	pomoc
Sovětům	Sovět	k1gMnPc3	Sovět
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Harriman	Harriman	k1gMnSc1	Harriman
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
důvodu	důvod	k1gInSc6	důvod
upřednostnění	upřednostnění	k1gNnSc2	upřednostnění
otevření	otevření	k1gNnSc2	otevření
severoafrické	severoafrický	k2eAgFnSc2d1	severoafrická
fronty	fronta	k1gFnSc2	fronta
před	před	k7c7	před
vyloděním	vylodění	k1gNnSc7	vylodění
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
anglicko	anglicko	k1gNnSc1	anglicko
-	-	kIx~	-
sovětská	sovětský	k2eAgFnSc1d1	sovětská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
výměně	výměna	k1gFnSc6	výměna
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Ministři	ministr	k1gMnPc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc1	Anthona
Eden	Eden	k1gInSc1	Eden
<g/>
,	,	kIx,	,
Molotov	Molotov	k1gInSc1	Molotov
<g/>
,	,	kIx,	,
Fu	fu	k0	fu
Bingchang	Bingchang	k1gMnSc1	Bingchang
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
deklarace	deklarace	k1gFnSc1	deklarace
-	-	kIx~	-
společné	společný	k2eAgNnSc1d1	společné
prohlášení	prohlášení	k1gNnSc1	prohlášení
vlád	vláda	k1gFnPc2	vláda
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
o	o	k7c6	o
osvobozování	osvobozování	k1gNnSc6	osvobozování
některých	některý	k3yIgInPc2	některý
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
Němců	Němec	k1gMnPc2	Němec
</s>
</p>
<p>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Molotov	Molotov	k1gInSc1	Molotov
<g/>
,	,	kIx,	,
Eden	Eden	k1gInSc1	Eden
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
Zřízení	zřízení	k1gNnSc1	zřízení
poválečných	poválečný	k2eAgFnPc2d1	poválečná
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
Balkánském	balkánský	k2eAgMnSc6d1	balkánský
poloostrověJako	poloostrověJako	k6eAd1	poloostrověJako
hlava	hlava	k1gFnSc1	hlava
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
těchto	tento	k3xDgFnPc2	tento
válečných	válečný	k2eAgFnPc2d1	válečná
konferencí	konference	k1gFnPc2	konference
mimo	mimo	k7c4	mimo
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Teheránská	teheránský	k2eAgFnSc1d1	Teheránská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Teherán	Teherán	k1gInSc1	Teherán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
První	první	k4xOgNnSc1	první
společné	společný	k2eAgNnSc1d1	společné
setkání	setkání	k1gNnSc1	setkání
Velké	velký	k2eAgFnSc2d1	velká
trojky	trojka	k1gFnSc2	trojka
<g/>
,	,	kIx,	,
plánování	plánování	k1gNnSc1	plánování
konečné	konečný	k2eAgFnSc2d1	konečná
strategie	strategie	k1gFnSc2	strategie
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc4	stanovení
data	datum	k1gNnSc2	datum
Dne	den	k1gInSc2	den
D	D	kA	D
</s>
</p>
<p>
<s>
Jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Jalta	Jalta	k1gFnSc1	Jalta
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
plány	plán	k1gInPc1	plán
porážky	porážka	k1gFnSc2	porážka
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
poválečné	poválečný	k2eAgInPc4d1	poválečný
plány	plán	k1gInPc4	plán
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc2	stanovení
data	datum	k1gNnSc2	datum
Konference	konference	k1gFnSc2	konference
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
podmínky	podmínka	k1gFnPc1	podmínka
vstupu	vstup	k1gInSc2	vstup
SSSR	SSSR	kA	SSSR
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Japonsku	Japonsko	k1gNnSc3	Japonsko
</s>
</p>
<p>
<s>
Postupimská	postupimský	k2eAgFnSc1d1	Postupimská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Postupim	Postupim	k1gFnSc1	Postupim
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
<g/>
:	:	kIx,	:
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Truman	Truman	k1gMnSc1	Truman
<g/>
,	,	kIx,	,
Attlee	Attlee	k1gFnSc1	Attlee
<g/>
,	,	kIx,	,
předmět	předmět	k1gInSc1	předmět
jednání	jednání	k1gNnSc2	jednání
<g/>
:	:	kIx,	:
Deklarace	deklarace	k1gFnSc1	deklarace
bezpodmínečné	bezpodmínečný	k2eAgFnSc2d1	bezpodmínečná
kapitulace	kapitulace	k1gFnSc2	kapitulace
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc2	stanovení
poválečné	poválečný	k2eAgFnSc2d1	poválečná
politiky	politika	k1gFnSc2	politika
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
hodnosti	hodnost	k1gFnSc2	hodnost
===	===	k?	===
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
hodnost	hodnost	k1gFnSc1	hodnost
maršál	maršál	k1gMnSc1	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
armádními	armádní	k2eAgNnPc7d1	armádní
špičkami	špička	k1gFnPc7	špička
navržen	navržen	k2eAgInSc1d1	navržen
Ústřednímu	ústřední	k2eAgInSc3d1	ústřední
výboru	výbor	k1gInSc3	výbor
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
udělení	udělení	k1gNnSc6	udělení
hodnosti	hodnost	k1gFnPc4	hodnost
generalissima	generalissimus	k1gMnSc2	generalissimus
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
tuto	tento	k3xDgFnSc4	tento
hodnost	hodnost	k1gFnSc4	hodnost
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jako	jako	k8xS	jako
ocenění	ocenění	k1gNnSc4	ocenění
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
zásluh	zásluha	k1gFnPc2	zásluha
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
vlastenecké	vlastenecký	k2eAgFnSc6d1	vlastenecká
válce	válka	k1gFnSc6	válka
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řády	řád	k1gInPc7	řád
Vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
Hrdina	Hrdina	k1gMnSc1	Hrdina
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
obdržel	obdržet	k5eAaPmAgInS	obdržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
na	na	k7c4	na
Stalina	Stalin	k1gMnSc4	Stalin
spáchán	spáchán	k2eAgInSc1d1	spáchán
atentát	atentát	k1gInSc1	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Atentátníkem	atentátník	k1gMnSc7	atentátník
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
sovětský	sovětský	k2eAgMnSc1d1	sovětský
nadporučík	nadporučík	k1gMnSc1	nadporučík
a	a	k8xC	a
poté	poté	k6eAd1	poté
špion	špion	k1gMnSc1	špion
Třetí	třetí	k4xOgFnSc2	třetí
Říše	říš	k1gFnSc2	říš
Pjotr	Pjotr	k1gMnSc1	Pjotr
Tavrin-Šilo	Tavrin-Šila	k1gFnSc5	Tavrin-Šila
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
zaútočit	zaútočit	k5eAaPmF	zaútočit
granátometem	granátomet	k1gInSc7	granátomet
na	na	k7c4	na
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
opancéřovaný	opancéřovaný	k2eAgInSc4d1	opancéřovaný
vůz	vůz	k1gInSc4	vůz
při	při	k7c6	při
výjezdu	výjezd	k1gInSc6	výjezd
z	z	k7c2	z
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
hodit	hodit	k5eAaImF	hodit
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
minu	mina	k1gFnSc4	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tavrin-Šilo	Tavrin-Šít	k5eAaPmAgNnS	Tavrin-Šít
byl	být	k5eAaImAgInS	být
vysazen	vysadit	k5eAaPmNgMnS	vysadit
na	na	k7c6	na
území	území	k1gNnSc6	území
SSSR	SSSR	kA	SSSR
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
pobyl	pobýt	k5eAaPmAgInS	pobýt
v	v	k7c4	v
Karmanovu	Karmanův	k2eAgFnSc4d1	Karmanův
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
odjel	odjet	k5eAaPmAgMnS	odjet
motocyklem	motocykl	k1gInSc7	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
ho	on	k3xPp3gInSc4	on
zastavil	zastavit	k5eAaPmAgMnS	zastavit
milicionář	milicionář	k1gMnSc1	milicionář
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jede	jet	k5eAaImIp3nS	jet
<g/>
.	.	kIx.	.
</s>
<s>
Tavrin-Šilo	Tavrin-Šít	k5eAaPmAgNnS	Tavrin-Šít
mu	on	k3xPp3gMnSc3	on
uvedl	uvést	k5eAaPmAgMnS	uvést
Ržev	Ržev	k1gInSc4	Ržev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
neměl	mít	k5eNaImAgMnS	mít
dělat	dělat	k5eAaImF	dělat
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
asi	asi	k9	asi
200	[number]	k4	200
km	km	kA	km
a	a	k8xC	a
Tavrin-Šilo	Tavrin-Šila	k1gFnSc5	Tavrin-Šila
měl	mít	k5eAaImAgInS	mít
téměř	téměř	k6eAd1	téměř
suché	suchý	k2eAgFnPc4d1	suchá
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
pršelo	pršet	k5eAaImAgNnS	pršet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
odveden	odvést	k5eAaPmNgInS	odvést
na	na	k7c4	na
úřadovnu	úřadovna	k1gFnSc4	úřadovna
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
nejen	nejen	k6eAd1	nejen
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
minulosti	minulost	k1gFnSc3	minulost
(	(	kIx(	(
<g/>
Tavrin-Šilo	Tavrin-Šila	k1gFnSc5	Tavrin-Šila
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
třikrát	třikrát	k6eAd1	třikrát
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Důsledky	důsledek	k1gInPc1	důsledek
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
20	[number]	k4	20
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
9	[number]	k4	9
milionu	milion	k4xCgInSc2	milion
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
země	země	k1gFnSc1	země
tak	tak	k6eAd1	tak
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
válčících	válčící	k2eAgInPc2d1	válčící
států	stát	k1gInPc2	stát
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stalinem	Stalin	k1gMnSc7	Stalin
vedený	vedený	k2eAgInSc1d1	vedený
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
dokázal	dokázat	k5eAaPmAgInS	dokázat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
dodávek	dodávka	k1gFnPc2	dodávka
a	a	k8xC	a
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
obrovských	obrovský	k2eAgFnPc2d1	obrovská
obětí	oběť	k1gFnPc2	oběť
německému	německý	k2eAgInSc3d1	německý
útoku	útok	k1gInSc2	útok
odolat	odolat	k5eAaPmF	odolat
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
převzal	převzít	k5eAaPmAgMnS	převzít
iniciativu	iniciativa	k1gFnSc4	iniciativa
a	a	k8xC	a
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
stanula	stanout	k5eAaPmAgFnS	stanout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
srovnán	srovnán	k2eAgMnSc1d1	srovnán
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
<g/>
,	,	kIx,	,
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
Besarábii	Besarábie	k1gFnSc4	Besarábie
a	a	k8xC	a
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
začlenil	začlenit	k5eAaPmAgMnS	začlenit
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xS	jako
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
stalo	stát	k5eAaPmAgNnS	stát
satelitem	satelit	k1gInSc7	satelit
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
si	se	k3xPyFc3	se
nezávislost	nezávislost	k1gFnSc4	nezávislost
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
státním	státní	k2eAgNnSc6d1	státní
zřízení	zřízení	k1gNnSc6	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
ve	v	k7c6	v
Stalinově	Stalinův	k2eAgFnSc6d1	Stalinova
sféře	sféra	k1gFnSc6	sféra
zájmů	zájem	k1gInPc2	zájem
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
a	a	k8xC	a
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
satelity	satelit	k1gInPc1	satelit
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
všudy	všudy	k6eAd1	všudy
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
režimy	režim	k1gInPc1	režim
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
nelze	lze	k6eNd1	lze
se	s	k7c7	s
Stalinovou	Stalinův	k2eAgFnSc7d1	Stalinova
diktaturou	diktatura	k1gFnSc7	diktatura
srovnávat	srovnávat	k5eAaImF	srovnávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
získal	získat	k5eAaPmAgMnS	získat
jakožto	jakožto	k8xS	jakožto
vítěz	vítěz	k1gMnSc1	vítěz
nad	nad	k7c7	nad
nacismem	nacismus	k1gInSc7	nacismus
gloriolu	gloriola	k1gFnSc4	gloriola
bojovníka	bojovník	k1gMnSc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
osvoboditele	osvoboditel	k1gMnPc4	osvoboditel
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
levicových	levicový	k2eAgMnPc2d1	levicový
intelektuálů	intelektuál	k1gMnPc2	intelektuál
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mělo	mít	k5eAaImAgNnS	mít
právě	právě	k6eAd1	právě
takový	takový	k3xDgInSc4	takový
názor	názor	k1gInSc4	názor
a	a	k8xC	a
nevěřili	věřit	k5eNaImAgMnP	věřit
zprávám	zpráva	k1gFnPc3	zpráva
o	o	k7c6	o
zločinech	zločin	k1gInPc6	zločin
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
páchat	páchat	k5eAaImF	páchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
<g/>
,	,	kIx,	,
Projev	projev	k1gInSc1	projev
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
pronesený	pronesený	k2eAgInSc1d1	pronesený
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
Stalinem	Stalin	k1gMnSc7	Stalin
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
názor	názor	k1gInSc1	názor
na	na	k7c4	na
Stalina	Stalin	k1gMnSc4	Stalin
měnil	měnit	k5eAaImAgMnS	měnit
od	od	k7c2	od
opatrné	opatrný	k2eAgFnSc2d1	opatrná
kritiky	kritika	k1gFnSc2	kritika
chyb	chyba	k1gFnPc2	chyba
po	po	k7c6	po
prohlášení	prohlášení	k1gNnSc6	prohlášení
Stalina	Stalin	k1gMnSc2	Stalin
za	za	k7c4	za
zločince	zločinec	k1gMnSc4	zločinec
ústy	ústa	k1gNnPc7	ústa
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ruského	ruský	k2eAgMnSc2d1	ruský
presidenta	president	k1gMnSc2	president
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
počet	počet	k1gInSc1	počet
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
Stalina	Stalin	k1gMnSc2	Stalin
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vrcholící	vrcholící	k2eAgInSc1d1	vrcholící
kult	kult	k1gInSc1	kult
osobnosti	osobnost	k1gFnSc2	osobnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
kult	kult	k1gInSc1	kult
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
osobnosti	osobnost	k1gFnSc2	osobnost
vstupoval	vstupovat	k5eAaImAgInS	vstupovat
do	do	k7c2	do
života	život	k1gInSc2	život
každého	každý	k3xTgMnSc2	každý
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Glorifikace	glorifikace	k1gFnSc1	glorifikace
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
gigantických	gigantický	k2eAgMnPc2d1	gigantický
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
během	během	k7c2	během
oslav	oslava	k1gFnPc2	oslava
vůdcových	vůdcův	k2eAgFnPc2d1	Vůdcova
sedmdesátých	sedmdesátý	k4xOgFnPc2	sedmdesátý
narozenin	narozeniny	k1gFnPc2	narozeniny
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
otevřena	otevřen	k2eAgFnSc1d1	otevřena
výstava	výstava	k1gFnSc1	výstava
darů	dar	k1gInPc2	dar
"	"	kIx"	"
<g/>
československého	československý	k2eAgInSc2d1	československý
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
J.	J.	kA	J.
V.	V.	kA	V.
Stalinovi	Stalin	k1gMnSc3	Stalin
<g/>
,	,	kIx,	,
obdobné	obdobný	k2eAgFnPc1d1	obdobná
výstavy	výstava	k1gFnPc1	výstava
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
socialistických	socialistický	k2eAgFnPc6d1	socialistická
metropolích	metropol	k1gFnPc6	metropol
a	a	k8xC	a
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
prosince	prosinec	k1gInSc2	prosinec
informoval	informovat	k5eAaBmAgMnS	informovat
tisk	tisk	k1gInSc4	tisk
o	o	k7c6	o
slavnostních	slavnostní	k2eAgNnPc6d1	slavnostní
shromážděních	shromáždění	k1gNnPc6	shromáždění
a	a	k8xC	a
závazcích	závazek	k1gInPc6	závazek
na	na	k7c4	na
počet	počet	k1gInSc4	počet
Stalinových	Stalinových	k2eAgFnPc2d1	Stalinových
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byly	být	k5eAaImAgFnP	být
Stalinovým	Stalinův	k2eAgNnSc7d1	Stalinovo
jménem	jméno	k1gNnSc7	jméno
pojmenovávány	pojmenovávat	k5eAaImNgFnP	pojmenovávat
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
ulice	ulice	k1gFnPc1	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnPc1	náměstí
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgNnPc6d1	slovenské
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Vinohradská	vinohradský	k2eAgFnSc1d1	Vinohradská
<g/>
)	)	kIx)	)
a	a	k8xC	a
důležité	důležitý	k2eAgInPc1d1	důležitý
výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dnešní	dnešní	k2eAgFnSc1d1	dnešní
chemička	chemička	k1gFnSc1	chemička
v	v	k7c6	v
Záluží	Záluží	k1gNnSc6	Záluží
a	a	k8xC	a
stavěny	stavěn	k2eAgInPc4d1	stavěn
pomníky	pomník	k1gInPc4	pomník
<g/>
.	.	kIx.	.
<g/>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
rozsah	rozsah	k1gInSc4	rozsah
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
uctívání	uctívání	k1gNnSc1	uctívání
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
pohřbu	pohřeb	k1gInSc2	pohřeb
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
===	===	k?	===
</s>
</p>
<p>
<s>
Poškození	poškození	k1gNnSc1	poškození
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
lékaři	lékař	k1gMnPc1	lékař
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Stalinův	Stalinův	k2eAgInSc1d1	Stalinův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
důsledek	důsledek	k1gInSc4	důsledek
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
přejídání	přejídání	k1gNnSc1	přejídání
se	se	k3xPyFc4	se
a	a	k8xC	a
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
pití	pití	k1gNnSc2	pití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
náročných	náročný	k2eAgNnPc2d1	náročné
jednání	jednání	k1gNnPc2	jednání
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
trojkou	trojka	k1gFnSc7	trojka
<g/>
,	,	kIx,	,
prodělal	prodělat	k5eAaPmAgMnS	prodělat
infarkt	infarkt	k1gInSc4	infarkt
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1953	[number]	k4	1953
mozkovou	mozkový	k2eAgFnSc7d1	mozková
mrtvicí	mrtvice	k1gFnSc7	mrtvice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neprobral	probrat	k5eNaPmAgMnS	probrat
<g/>
.	.	kIx.	.
<g/>
Vůdce	vůdce	k1gMnSc1	vůdce
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
síly	síla	k1gFnSc2	síla
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
každodennímu	každodenní	k2eAgNnSc3d1	každodenní
řízení	řízení	k1gNnSc3	řízení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1945	[number]	k4	1945
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
poválečné	poválečný	k2eAgFnSc2d1	poválečná
dovolené	dovolená	k1gFnSc2	dovolená
prodělal	prodělat	k5eAaPmAgMnS	prodělat
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
boj	boj	k1gInSc1	boj
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
muž	muž	k1gMnSc1	muž
číslo	číslo	k1gNnSc1	číslo
dvě	dva	k4xCgFnPc4	dva
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Michajlovič	Michajlovič	k1gInSc1	Michajlovič
Molotov	Molotov	k1gInSc1	Molotov
byl	být	k5eAaImAgMnS	být
odsunut	odsunout	k5eAaPmNgMnS	odsunout
a	a	k8xC	a
předstižen	předstihnout	k5eAaPmNgMnS	předstihnout
Andrejem	Andrej	k1gMnSc7	Andrej
A.	A.	kA	A.
Ždanovem	Ždanov	k1gInSc7	Ždanov
či	či	k8xC	či
Georgijem	Georgij	k1gInSc7	Georgij
M.	M.	kA	M.
Malenkovem	Malenkov	k1gInSc7	Malenkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berijovské	Berijovský	k2eAgFnPc4d1	Berijovský
represe	represe	k1gFnPc4	represe
===	===	k?	===
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
diktatury	diktatura	k1gFnSc2	diktatura
J.	J.	kA	J.
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc2	Stalin
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
stupňování	stupňování	k1gNnSc3	stupňování
represí	represe	k1gFnPc2	represe
uvnitř	uvnitř	k7c2	uvnitř
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Šéfem	šéf	k1gMnSc7	šéf
NKVD	NKVD	kA	NKVD
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
Lavrentij	Lavrentij	k1gMnSc1	Lavrentij
Berija	Berija	k1gMnSc1	Berija
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
teroru	teror	k1gInSc6	teror
podobném	podobný	k2eAgInSc6d1	podobný
třicátým	třicátý	k4xOgInSc7	třicátý
letem	let	k1gInSc7	let
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Ježova	Ježův	k2eAgMnSc2d1	Ježův
<g/>
.	.	kIx.	.
<g/>
Poválečnou	poválečný	k2eAgFnSc4d1	poválečná
éru	éra	k1gFnSc4	éra
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
i	i	k9	i
masivní	masivní	k2eAgFnPc1d1	masivní
propagační	propagační	k2eAgFnPc1d1	propagační
kampaně	kampaň	k1gFnPc1	kampaň
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
aféry	aféra	k1gFnPc1	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
propaganda	propaganda	k1gFnSc1	propaganda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
poklonkování	poklonkování	k1gNnSc3	poklonkování
před	před	k7c7	před
západem	západ	k1gInSc7	západ
(	(	kIx(	(
<g/>
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
kosmopolitismu	kosmopolitismus	k1gInSc3	kosmopolitismus
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
antisemitským	antisemitský	k2eAgNnSc7d1	antisemitské
pozadím	pozadí	k1gNnSc7	pozadí
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
represivní	represivní	k2eAgFnPc1d1	represivní
kampaně	kampaň	k1gFnPc1	kampaň
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
armádní	armádní	k2eAgFnSc4d1	armádní
elitu	elita	k1gFnSc4	elita
<g/>
,	,	kIx,	,
stranický	stranický	k2eAgInSc4d1	stranický
aparát	aparát	k1gInSc4	aparát
a	a	k8xC	a
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
a	a	k8xC	a
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
elitu	elita	k1gFnSc4	elita
(	(	kIx(	(
<g/>
leningradská	leningradský	k2eAgFnSc1d1	Leningradská
aféra	aféra	k1gFnSc1	aféra
<g/>
,	,	kIx,	,
mingrelské	mingrelský	k2eAgNnSc1d1	mingrelský
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
,	,	kIx,	,
spiknutí	spiknutí	k1gNnSc1	spiknutí
bílých	bílý	k2eAgInPc2d1	bílý
plášťů	plášť	k1gInPc2	plášť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
odhalování	odhalování	k1gNnSc2	odhalování
spiknutí	spiknutí	k1gNnSc2	spiknutí
se	se	k3xPyFc4	se
přenesla	přenést	k5eAaPmAgFnS	přenést
i	i	k9	i
do	do	k7c2	do
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
růst	růst	k1gInSc4	růst
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
konfliktu	konflikt	k1gInSc2	konflikt
nesmírně	smírně	k6eNd1	smírně
mocensky	mocensky	k6eAd1	mocensky
posílen	posílit	k5eAaPmNgInS	posílit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hospodářsky	hospodářsky	k6eAd1	hospodářsky
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
únosnosti	únosnost	k1gFnSc2	únosnost
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
žádal	žádat	k5eAaImAgInS	žádat
nové	nový	k2eAgFnPc4d1	nová
oběti	oběť	k1gFnPc4	oběť
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
moc	moc	k1gFnSc1	moc
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
zbrojilo	zbrojit	k5eAaImAgNnS	zbrojit
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
úkolem	úkol	k1gInSc7	úkol
pro	pro	k7c4	pro
sovětské	sovětský	k2eAgMnPc4d1	sovětský
vědce	vědec	k1gMnPc4	vědec
a	a	k8xC	a
rozvědku	rozvědka	k1gFnSc4	rozvědka
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc4	získání
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
preferoval	preferovat	k5eAaImAgMnS	preferovat
rozvoj	rozvoj	k1gInSc4	rozvoj
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
strojů	stroj	k1gInPc2	stroj
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spotřební	spotřební	k2eAgInSc1d1	spotřební
průmysl	průmysl	k1gInSc1	průmysl
byl	být	k5eAaImAgInS	být
minimalizován	minimalizovat	k5eAaBmNgInS	minimalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
reparacím	reparace	k1gFnPc3	reparace
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
vyvezena	vyvezen	k2eAgFnSc1d1	vyvezena
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zařízení	zařízení	k1gNnSc2	zařízení
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
továrny	továrna	k1gFnSc2	továrna
BMW	BMW	kA	BMW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Sovětům	Sověty	k1gInPc3	Sověty
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
kompenzovalo	kompenzovat	k5eAaBmAgNnS	kompenzovat
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
vyšší	vysoký	k2eAgFnSc2d2	vyšší
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
<g/>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nevzpamatovalo	vzpamatovat	k5eNaPmAgNnS	vzpamatovat
z	z	k7c2	z
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
<g/>
,	,	kIx,	,
válečných	válečný	k2eAgFnPc2d1	válečná
útrap	útrapa	k1gFnPc2	útrapa
a	a	k8xC	a
odvodů	odvod	k1gInPc2	odvod
práceschopných	práceschopný	k2eAgMnPc2d1	práceschopný
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
i	i	k8xC	i
poválečného	poválečný	k2eAgInSc2d1	poválečný
úprku	úprk	k1gInSc2	úprk
rolníků	rolník	k1gMnPc2	rolník
do	do	k7c2	do
měst	město	k1gNnPc2	město
za	za	k7c7	za
lepším	dobrý	k2eAgNnSc7d2	lepší
živobytím	živobytí	k1gNnSc7	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Kolchozní	kolchozní	k2eAgFnSc1d1	kolchozní
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měla	mít	k5eAaImAgFnS	mít
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
produkce	produkce	k1gFnSc1	produkce
za	za	k7c2	za
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
sice	sice	k8xC	sice
i	i	k9	i
nadále	nadále	k6eAd1	nadále
plánoval	plánovat	k5eAaImAgMnS	plánovat
expanzi	expanze	k1gFnSc3	expanze
a	a	k8xC	a
upevnění	upevnění	k1gNnSc3	upevnění
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc1	jeho
postupy	postup	k1gInPc1	postup
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
svou	svůj	k3xOyFgFnSc4	svůj
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
postupovat	postupovat	k5eAaImF	postupovat
méně	málo	k6eAd2	málo
nápadně	nápadně	k6eAd1	nápadně
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
obavy	obava	k1gFnPc4	obava
ze	z	k7c2	z
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
převaha	převaha	k1gFnSc1	převaha
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
zdařila	zdařit	k5eAaPmAgFnS	zdařit
sovětizace	sovětizace	k1gFnSc1	sovětizace
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Informační	informační	k2eAgNnSc4d1	informační
byro	byro	k1gNnSc4	byro
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
podřídit	podřídit	k5eAaPmF	podřídit
tamní	tamní	k2eAgFnSc2d1	tamní
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
sovětském	sovětský	k2eAgInSc6d1	sovětský
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
nečekané	čekaný	k2eNgFnPc1d1	nečekaná
komplikace	komplikace	k1gFnPc1	komplikace
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nepoddajnou	poddajný	k2eNgFnSc7d1	nepoddajná
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
J.	J.	kA	J.
B.	B.	kA	B.
Tita	Tita	k1gMnSc1	Tita
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgInPc1d1	komunistický
režimy	režim	k1gInPc1	režim
však	však	k9	však
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
Kim	Kim	k1gFnSc2	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
vliv	vliv	k1gInSc1	vliv
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
však	však	k9	však
rostl	růst	k5eAaImAgMnS	růst
i	i	k9	i
díky	díky	k7c3	díky
vlivné	vlivný	k2eAgFnSc3d1	vlivná
pozici	pozice	k1gFnSc3	pozice
v	v	k7c6	v
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
řečnickou	řečnický	k2eAgFnSc7d1	řečnická
arénou	aréna	k1gFnSc7	aréna
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
naplno	naplno	k6eAd1	naplno
během	během	k7c2	během
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
blokády	blokáda	k1gFnSc2	blokáda
(	(	kIx(	(
<g/>
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
doktrína	doktrína	k1gFnSc1	doktrína
zadržování	zadržování	k1gNnSc4	zadržování
komunismu	komunismus	k1gInSc2	komunismus
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
aktivizaci	aktivizace	k1gFnSc3	aktivizace
antikomunistických	antikomunistický	k2eAgFnPc2d1	antikomunistická
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
nároků	nárok	k1gInPc2	nárok
vůči	vůči	k7c3	vůči
Turecku	Turecko	k1gNnSc3	Turecko
i	i	k8xC	i
Íránu	Írán	k1gInSc3	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
i	i	k8xC	i
přes	přes	k7c4	přes
Stalinovu	Stalinův	k2eAgFnSc4d1	Stalinova
počáteční	počáteční	k2eAgFnSc4d1	počáteční
podporu	podpora	k1gFnSc4	podpora
nakonec	nakonec	k6eAd1	nakonec
obrátil	obrátit	k5eAaPmAgMnS	obrátit
zády	záda	k1gNnPc7	záda
a	a	k8xC	a
ani	ani	k8xC	ani
korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
nevyvíjela	vyvíjet	k5eNaImAgFnS	vyvíjet
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gFnPc2	jeho
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženat	ženat	k2eAgMnSc1d1	ženat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
Jekatěrina	Jekatěrina	k1gFnSc1	Jekatěrina
Semjonovna	Semjonovna	k1gFnSc1	Semjonovna
Svanidzeová	Svanidzeová	k1gFnSc1	Svanidzeová
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
zemřela	zemřít	k5eAaPmAgFnS	zemřít
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
Jakova	jakův	k2eAgInSc2d1	jakův
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Naděždou	Naděžda	k1gFnSc7	Naděžda
Alilujevovou	Alilujevová	k1gFnSc7	Alilujevová
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc2	syn
Vasilije	Vasilije	k1gMnSc1	Vasilije
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Světlanu	Světlana	k1gFnSc4	Světlana
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
dcery	dcera	k1gFnSc2	dcera
Světlany	Světlana	k1gFnSc2	Světlana
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
Stalin	Stalin	k1gMnSc1	Stalin
citově	citově	k6eAd1	citově
chladný	chladný	k2eAgMnSc1d1	chladný
a	a	k8xC	a
paranoidní	paranoidní	k2eAgMnSc1d1	paranoidní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kult	kult	k1gInSc1	kult
osobnosti	osobnost	k1gFnSc2	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
kultu	kult	k1gInSc2	kult
osobnosti	osobnost	k1gFnSc2	osobnost
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
oslavován	oslavován	k2eAgMnSc1d1	oslavován
jako	jako	k8xC	jako
veliký	veliký	k2eAgMnSc1d1	veliký
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
veliký	veliký	k2eAgMnSc1d1	veliký
vůdce	vůdce	k1gMnSc1	vůdce
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
národů	národ	k1gInPc2	národ
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
в	в	k?	в
в	в	k?	в
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
в	в	k?	в
в	в	k?	в
и	и	k?	и
у	у	k?	у
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
о	о	k?	о
н	н	k?	н
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
též	též	k9	též
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
gloriolu	gloriola	k1gFnSc4	gloriola
velkého	velký	k2eAgMnSc2d1	velký
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
maršála	maršál	k1gMnSc2	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
získal	získat	k5eAaPmAgMnS	získat
hodnost	hodnost	k1gFnSc4	hodnost
generalissima	generalissimus	k1gMnSc2	generalissimus
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hodnost	hodnost	k1gFnSc4	hodnost
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
pouze	pouze	k6eAd1	pouze
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
===	===	k?	===
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
paranoidní	paranoidní	k2eAgMnSc1d1	paranoidní
a	a	k8xC	a
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
letech	léto	k1gNnPc6	léto
trpěl	trpět	k5eAaImAgMnS	trpět
arterosklerózou	arteroskleróza	k1gFnSc7	arteroskleróza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
lékař	lékař	k1gMnSc1	lékař
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vinogradov	Vinogradov	k1gInSc4	Vinogradov
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
ve	v	k7c6	v
Stalinově	Stalinův	k2eAgInSc6d1	Stalinův
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Stalinovi	Stalin	k1gMnSc3	Stalin
navrhnul	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bral	brát	k5eAaImAgMnS	brát
věci	věc	k1gFnPc4	věc
na	na	k7c4	na
lehčí	lehký	k2eAgFnSc4d2	lehčí
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
rozzuřil	rozzuřit	k5eAaPmAgMnS	rozzuřit
se	se	k3xPyFc4	se
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
několik	několik	k4yIc1	několik
dalších	další	k2eAgMnPc2d1	další
lékařů	lékař	k1gMnPc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
židovského	židovský	k2eAgMnSc4d1	židovský
původu	původa	k1gMnSc4	původa
a	a	k8xC	a
tisk	tisk	k1gInSc1	tisk
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
pověsti	pověst	k1gFnPc4	pověst
o	o	k7c6	o
lékařském	lékařský	k2eAgNnSc6d1	lékařské
spiknutí	spiknutí	k1gNnSc6	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1953	[number]	k4	1953
TASS	TASS	kA	TASS
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
uvěznění	uvěznění	k1gNnSc6	uvěznění
devíti	devět	k4xCc2	devět
členů	člen	k1gMnPc2	člen
zločinné	zločinný	k2eAgFnSc2d1	zločinná
skupiny	skupina	k1gFnSc2	skupina
vraždících	vraždící	k2eAgMnPc2d1	vraždící
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
obviněných	obviněný	k1gMnPc2	obviněný
z	z	k7c2	z
likvidace	likvidace	k1gFnSc2	likvidace
předních	přední	k2eAgMnPc2d1	přední
sovětských	sovětský	k2eAgMnPc2d1	sovětský
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
1953	[number]	k4	1953
odjel	odjet	k5eAaPmAgMnS	odjet
Stalin	Stalin	k1gMnSc1	Stalin
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
daču	dača	k1gFnSc4	dača
v	v	k7c6	v
Kuncevu	Kuncev	k1gInSc6	Kuncev
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgFnPc2d1	další
událostí	událost	k1gFnPc2	událost
existují	existovat	k5eAaImIp3nP	existovat
rozporné	rozporný	k2eAgFnPc1d1	rozporná
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
Stalin	Stalin	k1gMnSc1	Stalin
strávil	strávit	k5eAaPmAgMnS	strávit
večeří	večeře	k1gFnSc7	večeře
a	a	k8xC	a
pitím	pití	k1gNnSc7	pití
do	do	k7c2	do
ranních	ranní	k2eAgFnPc2d1	ranní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
do	do	k7c2	do
pozdních	pozdní	k2eAgFnPc2d1	pozdní
večerních	večerní	k2eAgFnPc2d1	večerní
hodin	hodina	k1gFnPc2	hodina
neozývaly	ozývat	k5eNaImAgFnP	ozývat
z	z	k7c2	z
ložnice	ložnice	k1gFnSc2	ložnice
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
odvážil	odvážit	k5eAaPmAgMnS	odvážit
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
přítomných	přítomný	k1gMnPc2	přítomný
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
a	a	k8xC	a
nalezl	nalézt	k5eAaBmAgInS	nalézt
Stalina	Stalin	k1gMnSc4	Stalin
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Chruščevových	Chruščevův	k2eAgFnPc2d1	Chruščevova
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Malenkov	Malenkov	k1gInSc1	Malenkov
<g/>
,	,	kIx,	,
Berija	Berija	k1gMnSc1	Berija
a	a	k8xC	a
Bulganin	Bulganin	k2eAgMnSc1d1	Bulganin
do	do	k7c2	do
Kunceva	Kuncevo	k1gNnSc2	Kuncevo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
stráž	stráž	k1gFnSc1	stráž
telefonicky	telefonicky	k6eAd1	telefonicky
informovala	informovat	k5eAaBmAgFnS	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
uložen	uložit	k5eAaPmNgMnS	uložit
na	na	k7c4	na
pohovku	pohovka	k1gFnSc4	pohovka
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
jídelně	jídelna	k1gFnSc6	jídelna
a	a	k8xC	a
spí	spát	k5eAaImIp3nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
muži	muž	k1gMnPc1	muž
si	se	k3xPyFc3	se
neuvědomili	uvědomit	k5eNaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
něco	něco	k3yInSc1	něco
vážného	vážný	k2eAgNnSc2d1	vážné
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
když	když	k8xS	když
Stalin	Stalin	k1gMnSc1	Stalin
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
ochrnutý	ochrnutý	k2eAgMnSc1d1	ochrnutý
a	a	k8xC	a
nemluvil	mluvit	k5eNaImAgMnS	mluvit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
přivoláni	přivolán	k2eAgMnPc1d1	přivolán
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Bezradní	bezradný	k2eAgMnPc1d1	bezradný
členové	člen	k1gMnPc1	člen
politbyra	politbyro	k1gNnSc2	politbyro
dojížděli	dojíždět	k5eAaImAgMnP	dojíždět
na	na	k7c4	na
daču	dača	k1gFnSc4	dača
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Stalinově	Stalinův	k2eAgInSc6d1	Stalinův
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
v	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
v	v	k7c4	v
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
moskevského	moskevský	k2eAgInSc2d1	moskevský
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
bylo	být	k5eAaImAgNnS	být
tělo	tělo	k1gNnSc1	tělo
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
kremelské	kremelský	k2eAgFnSc6d1	kremelská
Sloupové	sloupový	k2eAgFnSc6d1	sloupová
síni	síň	k1gFnSc6	síň
a	a	k8xC	a
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Posmrtné	posmrtný	k2eAgNnSc4d1	posmrtné
uložení	uložení	k1gNnSc4	uložení
===	===	k?	===
</s>
</p>
<p>
<s>
Nabalzamované	nabalzamovaný	k2eAgNnSc1d1	nabalzamované
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
téhož	týž	k3xTgMnSc4	týž
roku	rok	k1gInSc6	rok
uloženo	uložit	k5eAaPmNgNnS	uložit
do	do	k7c2	do
Leninova	Leninův	k2eAgNnSc2d1	Leninovo
mauzolea	mauzoleum	k1gNnSc2	mauzoleum
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Mauzoleum	mauzoleum	k1gNnSc4	mauzoleum
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1961	[number]	k4	1961
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
sjezd	sjezd	k1gInSc1	sjezd
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalinovy	Stalinův	k2eAgInPc1d1	Stalinův
ostatky	ostatek	k1gInPc1	ostatek
nebudou	být	k5eNaImBp3nP	být
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
nadále	nadále	k6eAd1	nadále
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
vyneseno	vynesen	k2eAgNnSc1d1	vyneseno
a	a	k8xC	a
pohřbeno	pohřben	k2eAgNnSc1d1	pohřbeno
u	u	k7c2	u
Kremelské	kremelský	k2eAgFnSc2d1	kremelská
zdi	zeď	k1gFnSc2	zeď
v	v	k7c6	v
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
rakvi	rakev	k1gFnSc6	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Rakev	rakev	k1gFnSc1	rakev
byla	být	k5eAaImAgFnS	být
zalita	zalít	k5eAaPmNgFnS	zalít
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
dalšímu	další	k2eAgInSc3d1	další
možnému	možný	k2eAgInSc3d1	možný
přesunu	přesun	k1gInSc3	přesun
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stalinovi	Stalin	k1gMnSc3	Stalin
nástupci	nástupce	k1gMnSc3	nástupce
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
nástupci	nástupce	k1gMnPc1	nástupce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
moc	moc	k6eAd1	moc
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Georgij	Georgít	k5eAaPmRp2nS	Georgít
Maximilianovič	Maximilianovič	k1gInSc4	Maximilianovič
Malenkov	Malenkov	k1gInSc4	Malenkov
</s>
</p>
<p>
<s>
Lavrentij	Lavrentít	k5eAaPmRp2nS	Lavrentít
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Berija	Berija	k1gMnSc1	Berija
</s>
</p>
<p>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Molotov	Molotov	k1gInSc4	Molotov
</s>
</p>
<p>
<s>
Nikita	Nikita	k1gMnSc1	Nikita
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Chruščov	Chruščov	k1gInSc4	Chruščov
</s>
</p>
<p>
<s>
Kliment	Kliment	k1gMnSc1	Kliment
Jefremovič	Jefremovič	k1gMnSc1	Jefremovič
Vorošilov	Vorošilov	k1gInSc4	Vorošilov
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Bulganin	Bulganin	k2eAgMnSc1d1	Bulganin
</s>
</p>
<p>
<s>
Lazar	Lazar	k1gMnSc1	Lazar
Mojsejevič	Mojsejevič	k1gMnSc1	Mojsejevič
Kaganovič	Kaganovič	k1gMnSc1	Kaganovič
</s>
</p>
<p>
<s>
Anastáz	Anastáza	k1gFnPc2	Anastáza
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
MikojanJiž	MikojanJiž	k1gFnSc1	MikojanJiž
během	během	k7c2	během
zdlouhavého	zdlouhavý	k2eAgNnSc2d1	zdlouhavé
umírání	umírání	k1gNnSc2	umírání
diktátora	diktátor	k1gMnSc2	diktátor
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
dělení	dělení	k1gNnSc2	dělení
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
iniciativu	iniciativa	k1gFnSc4	iniciativa
převzal	převzít	k5eAaPmAgMnS	převzít
Stalinův	Stalinův	k2eAgMnSc1d1	Stalinův
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
krajan	krajan	k1gMnSc1	krajan
a	a	k8xC	a
úzký	úzký	k2eAgMnSc1d1	úzký
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Lavrentij	Lavrentij	k1gMnSc1	Lavrentij
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Berija	Berija	k1gMnSc1	Berija
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
aktivita	aktivita	k1gFnSc1	aktivita
ostatní	ostatní	k2eAgFnSc7d1	ostatní
překvapila	překvapit	k5eAaPmAgFnS	překvapit
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
paralyzovala	paralyzovat	k5eAaBmAgFnS	paralyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
Stalinovou	Stalinův	k2eAgFnSc7d1	Stalinova
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
zasedání	zasedání	k1gNnSc2	zasedání
ÚV	ÚV	kA	ÚV
a	a	k8xC	a
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
posvětit	posvětit	k5eAaPmF	posvětit
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
uvnitř	uvnitř	k7c2	uvnitř
"	"	kIx"	"
<g/>
věrchušky	věrchuška	k1gFnSc2	věrchuška
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
elita	elita	k1gFnSc1	elita
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
špička	špička	k1gFnSc1	špička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
slovo	slovo	k1gNnSc1	slovo
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
měli	mít	k5eAaImAgMnP	mít
Berija	Berija	k1gMnSc1	Berija
s	s	k7c7	s
Malenkovem	Malenkov	k1gInSc7	Malenkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
rozdělení	rozdělení	k1gNnSc1	rozdělení
funkcí	funkce	k1gFnPc2	funkce
anulovalo	anulovat	k5eAaBmAgNnS	anulovat
výsledky	výsledek	k1gInPc4	výsledek
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSSS	KSSS	kA	KSSS
a	a	k8xC	a
poslední	poslední	k2eAgInPc4d1	poslední
Stalinovy	Stalinův	k2eAgInPc4d1	Stalinův
zásahy	zásah	k1gInPc4	zásah
do	do	k7c2	do
mocenského	mocenský	k2eAgNnSc2d1	mocenské
postavení	postavení	k1gNnSc2	postavení
svých	svůj	k3xOyFgMnPc2	svůj
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
Rehabilitováno	rehabilitován	k2eAgNnSc1d1	rehabilitováno
bylo	být	k5eAaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
Molotova	Molotův	k2eAgNnSc2d1	Molotův
<g/>
,	,	kIx,	,
Mikojana	Mikojana	k1gFnSc1	Mikojana
a	a	k8xC	a
Vorošilova	Vorošilův	k2eAgFnSc1d1	Vorošilova
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nováčků	nováček	k1gMnPc2	nováček
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
strany	strana	k1gFnSc2	strana
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
ztratila	ztratit	k5eAaPmAgFnS	ztratit
(	(	kIx(	(
<g/>
Brežněv	Brežněv	k1gMnSc1	Brežněv
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
Stalinově	Stalinův	k2eAgFnSc6d1	Stalinova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Malenkov	Malenkov	k1gInSc1	Malenkov
jmenován	jmenovat	k5eAaImNgInS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
Rady	rada	k1gFnPc4	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
prvními	první	k4xOgMnPc7	první
náměstky	náměstek	k1gMnPc7	náměstek
Berija	Berijum	k1gNnSc2	Berijum
<g/>
,	,	kIx,	,
Molotov	Molotov	k1gInSc1	Molotov
<g/>
,	,	kIx,	,
Bulganin	Bulganin	k2eAgInSc1d1	Bulganin
a	a	k8xC	a
Kaganovič	Kaganovič	k1gInSc1	Kaganovič
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
triumvirát	triumvirát	k1gInSc1	triumvirát
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
Malenkova	Malenkův	k2eAgNnSc2d1	Malenkův
<g/>
,	,	kIx,	,
Beriji	Beriji	k1gFnSc1	Beriji
a	a	k8xC	a
Molotova	Molotův	k2eAgFnSc1d1	Molotova
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
také	také	k9	také
přednesli	přednést	k5eAaPmAgMnP	přednést
během	během	k7c2	během
pohřbu	pohřeb	k1gInSc2	pohřeb
diktátora	diktátor	k1gMnSc4	diktátor
projev	projev	k1gInSc4	projev
na	na	k7c6	na
Rudém	rudý	k2eAgNnSc6d1	Rudé
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
semknula	semknout	k5eAaPmAgFnS	semknout
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSSS	KSSS	kA	KSSS
proti	proti	k7c3	proti
L.	L.	kA	L.
P.	P.	kA	P.
Berijovi	Berij	k1gMnSc6	Berij
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1953	[number]	k4	1953
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1953	[number]	k4	1953
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1953	[number]	k4	1953
měli	mít	k5eAaImAgMnP	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
šéf	šéf	k1gMnSc1	šéf
vlády	vláda	k1gFnSc2	vláda
G.	G.	kA	G.
M.	M.	kA	M.
Malenkov	Malenkov	k1gInSc1	Malenkov
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
stranického	stranický	k2eAgInSc2d1	stranický
aparátu	aparát	k1gInSc2	aparát
N.	N.	kA	N.
S.	S.	kA	S.
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Udělení	udělení	k1gNnSc6	udělení
Československého	československý	k2eAgInSc2d1	československý
řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
J.	J.	kA	J.
V.	V.	kA	V.
Stalinovi	Stalin	k1gMnSc3	Stalin
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
schválila	schválit	k5eAaPmAgFnS	schválit
vláda	vláda	k1gFnSc1	vláda
propůjčení	propůjčení	k1gNnSc2	propůjčení
Československého	československý	k2eAgInSc2d1	československý
řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
I.	I.	kA	I.
třídy	třída	k1gFnPc4	třída
s	s	k7c7	s
řetězem	řetěz	k1gInSc7	řetěz
J.	J.	kA	J.
V.	V.	kA	V.
Stalinovi	Stalin	k1gMnSc3	Stalin
(	(	kIx(	(
<g/>
č.	č.	k?	č.
j.	j.	k?	j.
B	B	kA	B
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
vyznamenáni	vyznamenat	k5eAaPmNgMnP	vyznamenat
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
hodnocení	hodnocení	k1gNnSc1	hodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
obliba	obliba	k1gFnSc1	obliba
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
postsovětských	postsovětský	k2eAgInPc6d1	postsovětský
státech	stát	k1gInPc6	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
postsovětských	postsovětský	k2eAgInPc6d1	postsovětský
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
nebo	nebo	k8xC	nebo
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
)	)	kIx)	)
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
vlastenecké	vlastenecký	k2eAgFnSc6d1	vlastenecká
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obdivován	obdivován	k2eAgMnSc1d1	obdivován
jako	jako	k8xS	jako
válečný	válečný	k2eAgMnSc1d1	válečný
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ti	ten	k3xDgMnPc1	ten
samí	samý	k3xTgMnPc1	samý
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
odmítají	odmítat	k5eAaImIp3nP	odmítat
jeho	jeho	k3xOp3gFnSc4	jeho
represivní	represivní	k2eAgFnSc4d1	represivní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgFnSc4d3	veliký
Rus	Rus	k1gFnSc4	Rus
(	(	kIx(	(
<g/>
ruský	ruský	k2eAgInSc1d1	ruský
název	název	k1gInSc1	název
И	И	k?	И
Р	Р	k?	Р
<g/>
)	)	kIx)	)
umístil	umístit	k5eAaPmAgMnS	umístit
Stalin	Stalin	k1gMnSc1	Stalin
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
příznivci	příznivec	k1gMnPc1	příznivec
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
zachránil	zachránit	k5eAaPmAgInS	zachránit
před	před	k7c7	před
Leninovým	Leninův	k2eAgInSc7d1	Leninův
NEPem	Nep	k1gInSc7	Nep
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
rozdat	rozdat	k5eAaPmF	rozdat
<g/>
"	"	kIx"	"
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
Ruska	Rusko	k1gNnSc2	Rusko
Západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
že	že	k8xS	že
SSSR	SSSR	kA	SSSR
pozvedl	pozvednout	k5eAaPmAgInS	pozvednout
ze	z	k7c2	z
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
státu	stát	k1gInSc2	stát
na	na	k7c4	na
jadernou	jaderný	k2eAgFnSc4d1	jaderná
<g/>
,	,	kIx,	,
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
velmoc	velmoc	k1gFnSc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
kladně	kladně	k6eAd1	kladně
ohodnotilo	ohodnotit	k5eAaPmAgNnS	ohodnotit
Stalina	Stalin	k1gMnSc2	Stalin
28	[number]	k4	28
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
lidé	člověk	k1gMnPc1	člověk
starší	starý	k2eAgMnPc1d2	starší
55	[number]	k4	55
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
41	[number]	k4	41
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
až	až	k9	až
39	[number]	k4	39
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
výrazně	výrazně	k6eAd1	výrazně
narostl	narůst	k5eAaPmAgInS	narůst
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
kladně	kladně	k6eAd1	kladně
oceňujících	oceňující	k2eAgMnPc2d1	oceňující
roli	role	k1gFnSc4	role
Stalina	Stalin	k1gMnSc2	Stalin
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
kriticky	kriticky	k6eAd1	kriticky
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
již	již	k9	již
Stalin	Stalin	k1gMnSc1	Stalin
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
anktetě	ankteta	k1gFnSc6	ankteta
vyhodnocen	vyhodnocen	k2eAgInSc1d1	vyhodnocen
jako	jako	k8xC	jako
největší	veliký	k2eAgFnSc1d3	veliký
historická	historický	k2eAgFnSc1d1	historická
osobnost	osobnost	k1gFnSc1	osobnost
(	(	kIx(	(
<g/>
38	[number]	k4	38
%	%	kIx~	%
<g/>
)	)	kIx)	)
před	před	k7c7	před
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Putinem	Putin	k1gMnSc7	Putin
(	(	kIx(	(
<g/>
34	[number]	k4	34
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
Gruzii	Gruzie	k1gFnSc6	Gruzie
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
46	[number]	k4	46
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgNnPc2d1	dotazované
ke	k	k7c3	k
Stalinovi	Stalin	k1gMnSc3	Stalin
kladný	kladný	k2eAgInSc4d1	kladný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gori	Gori	k1gNnSc6	Gori
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
obří	obří	k2eAgFnSc1d1	obří
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
socha	socha	k1gFnSc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
označilo	označit	k5eAaPmAgNnS	označit
68	[number]	k4	68
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgMnPc2d1	dotazovaný
Gruzínců	Gruzínec	k1gMnPc2	Gruzínec
Stalina	Stalin	k1gMnSc2	Stalin
za	za	k7c7	za
"	"	kIx"	"
<g/>
moudrého	moudrý	k2eAgMnSc2d1	moudrý
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uvedlo	uvést	k5eAaPmAgNnS	uvést
38	[number]	k4	38
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
země	země	k1gFnSc1	země
by	by	kYmCp3nS	by
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
silného	silný	k2eAgMnSc4d1	silný
vůdce	vůdce	k1gMnSc4	vůdce
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
obnoven	obnoven	k2eAgInSc1d1	obnoven
pořádek	pořádek	k1gInSc1	pořádek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
zločinů	zločin	k1gInPc2	zločin
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
ruská	ruský	k2eAgFnSc1d1	ruská
organizace	organizace	k1gFnSc1	organizace
Memorial	Memorial	k1gMnSc1	Memorial
(	(	kIx(	(
<g/>
М	М	k?	М
<g/>
́	́	k?	́
<g/>
л	л	k?	л
<g/>
)	)	kIx)	)
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
jmenný	jmenný	k2eAgInSc4d1	jmenný
seznam	seznam	k1gInSc4	seznam
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
obětí	oběť	k1gFnPc2	oběť
stalinského	stalinský	k2eAgInSc2d1	stalinský
teroru	teror	k1gInSc2	teror
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
bude	být	k5eAaImBp3nS	být
postupně	postupně	k6eAd1	postupně
doplňován	doplňovat	k5eAaImNgInS	doplňovat
o	o	k7c4	o
zbývající	zbývající	k2eAgNnPc4d1	zbývající
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
jej	on	k3xPp3gMnSc4	on
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
soud	soud	k1gInSc1	soud
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vjačeslavem	Vjačeslav	k1gMnSc7	Vjačeslav
Molotovem	Molotov	k1gInSc7	Molotov
<g/>
,	,	kIx,	,
Lazarem	Lazar	k1gMnSc7	Lazar
Kaganovičem	Kaganovič	k1gMnSc7	Kaganovič
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Postyševem	Postyšev	k1gInSc7	Postyšev
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
předáky	předák	k1gInPc7	předák
ukrajinských	ukrajinský	k2eAgInPc2d1	ukrajinský
komunistů	komunista	k1gMnPc2	komunista
Vlasem	vlas	k1gInSc7	vlas
Čubarem	Čubar	k1gMnSc7	Čubar
<g/>
,	,	kIx,	,
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Kosiorem	Kosior	k1gMnSc7	Kosior
a	a	k8xC	a
Mendelem	Mendel	k1gMnSc7	Mendel
Chatajevičem	Chatajevič	k1gMnSc7	Chatajevič
shledal	shledat	k5eAaPmAgInS	shledat
vinným	vinný	k2eAgNnSc7d1	vinné
organizováním	organizování	k1gNnSc7	organizování
genocidy	genocida	k1gFnSc2	genocida
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
hladomor	hladomor	k1gInSc4	hladomor
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
otištěno	otištěn	k2eAgNnSc1d1	otištěno
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
"	"	kIx"	"
<g/>
Social-Demokrat	Social-Demokrat	k1gMnSc1	Social-Demokrat
<g/>
"	"	kIx"	"
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1913	[number]	k4	1913
<g/>
"	"	kIx"	"
<g/>
Těžko	těžko	k6eAd1	těžko
si	se	k3xPyFc3	se
umím	umět	k5eAaImIp1nS	umět
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
osobní	osobní	k2eAgFnSc4d1	osobní
svobodu	svoboda	k1gFnSc4	svoboda
nezaměstnaný	nezaměstnaný	k1gMnSc1	nezaměstnaný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chodí	chodit	k5eAaImIp3nS	chodit
hladový	hladový	k2eAgMnSc1d1	hladový
a	a	k8xC	a
nenachází	nacházet	k5eNaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
svoboda	svoboda	k1gFnSc1	svoboda
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zničené	zničený	k2eAgNnSc1d1	zničené
vykořisťování	vykořisťování	k1gNnSc1	vykořisťování
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neexistuje	existovat	k5eNaImIp3nS	existovat
útlak	útlak	k1gInSc4	útlak
jedněch	jeden	k4xCgMnPc2	jeden
lidí	člověk	k1gMnPc2	člověk
druhými	druhý	k4xOgFnPc7	druhý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
a	a	k8xC	a
žebráctví	žebráctví	k1gNnSc2	žebráctví
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
netřese	třást	k5eNaImIp3nS	třást
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zítra	zítra	k6eAd1	zítra
může	moct	k5eAaImIp3nS	moct
ztratit	ztratit	k5eAaPmF	ztratit
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
obydlí	obydlet	k5eAaPmIp3nS	obydlet
i	i	k9	i
chléb	chléb	k1gInSc1	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
skutečná	skutečný	k2eAgFnSc1d1	skutečná
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k9	jen
na	na	k7c6	na
papíru	papír	k1gInSc6	papír
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
–	–	k?	–
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
novinářské	novinářský	k2eAgFnSc2d1	novinářská
společnosti	společnost	k1gFnSc2	společnost
Royem	Roy	k1gMnSc7	Roy
Howardem	Howard	k1gMnSc7	Howard
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1936	[number]	k4	1936
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Projev	projev	k1gInSc1	projev
na	na	k7c4	na
II	II	kA	II
<g/>
.	.	kIx.	.
všesvazovém	všesvazový	k2eAgInSc6d1	všesvazový
sjezdu	sjezd	k1gInSc6	sjezd
sjezdu	sjezd	k1gInSc2	sjezd
sovětů	sovět	k1gInPc2	sovět
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1924	[number]	k4	1924
<g/>
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
nesprávné	správný	k2eNgNnSc1d1	nesprávné
si	se	k3xPyFc3	se
myslet	myslet	k5eAaImF	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
seriózní	seriózní	k2eAgInSc4d1	seriózní
kulturní	kulturní	k2eAgInSc4d1	kulturní
růst	růst	k1gInSc4	růst
členů	člen	k1gMnPc2	člen
společnosti	společnost	k1gFnSc2	společnost
bez	bez	k7c2	bez
změn	změna	k1gFnPc2	změna
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
pracovním	pracovní	k2eAgNnSc6d1	pracovní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
potřebné	potřebný	k2eAgNnSc1d1	potřebné
především	především	k6eAd1	především
zkrátit	zkrátit	k5eAaPmF	zkrátit
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
na	na	k7c4	na
minimálně	minimálně	k6eAd1	minimálně
6	[number]	k4	6
a	a	k8xC	a
potom	potom	k6eAd1	potom
na	na	k7c4	na
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
potřebné	potřebný	k2eAgNnSc4d1	potřebné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
členové	člen	k1gMnPc1	člen
společnosti	společnost	k1gFnSc2	společnost
dostali	dostat	k5eAaPmAgMnP	dostat
dostatek	dostatek	k1gInSc4	dostatek
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
potřebného	potřebné	k1gNnSc2	potřebné
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
všestranného	všestranný	k2eAgNnSc2d1	všestranné
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zavést	zavést	k5eAaPmF	zavést
povinnou	povinný	k2eAgFnSc4d1	povinná
výuku	výuka	k1gFnSc4	výuka
obecné	obecný	k2eAgFnSc2d1	obecná
politologie	politologie	k1gFnSc2	politologie
<g/>
,	,	kIx,	,
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
společnosti	společnost	k1gFnSc2	společnost
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
svobodně	svobodně	k6eAd1	svobodně
si	se	k3xPyFc3	se
vybrat	vybrat	k5eAaPmF	vybrat
profesi	profese	k1gFnSc4	profese
a	a	k8xC	a
nebýt	být	k5eNaImF	být
přikovanými	přikovaný	k2eAgInPc7d1	přikovaný
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
k	k	k7c3	k
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
jedné	jeden	k4xCgFnSc3	jeden
profesi	profes	k1gFnSc3	profes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
potřebné	potřebný	k2eAgNnSc1d1	potřebné
od	od	k7c2	od
základu	základ	k1gInSc2	základ
zlepšit	zlepšit	k5eAaPmF	zlepšit
bytové	bytový	k2eAgFnSc2d1	bytová
podmínky	podmínka	k1gFnSc2	podmínka
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
reálnou	reálný	k2eAgFnSc4d1	reálná
mzdu	mzda	k1gFnSc4	mzda
pracujícím	pracující	k1gFnPc3	pracující
a	a	k8xC	a
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
minimálně	minimálně	k6eAd1	minimálně
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
cestou	cesta	k1gFnSc7	cesta
přímého	přímý	k2eAgNnSc2d1	přímé
zvýšení	zvýšení	k1gNnSc2	zvýšení
mzdy	mzda	k1gFnSc2	mzda
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zvlášť	zvlášť	k6eAd1	zvlášť
cestou	cesta	k1gFnSc7	cesta
dalšího	další	k2eAgNnSc2d1	další
systematického	systematický	k2eAgNnSc2d1	systematické
snižování	snižování	k1gNnSc2	snižování
cen	cena	k1gFnPc2	cena
předmětů	předmět	k1gInPc2	předmět
masové	masový	k2eAgFnSc2d1	masová
spotřeby	spotřeba	k1gFnSc2	spotřeba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
ošacení	ošacení	k1gNnSc1	ošacení
<g/>
,	,	kIx,	,
bydlení	bydlení	k1gNnSc1	bydlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takové	takový	k3xDgFnPc1	takový
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc2d1	základní
podmínky	podmínka	k1gFnSc2	podmínka
přechodu	přechod	k1gInSc2	přechod
k	k	k7c3	k
opravdovému	opravdový	k2eAgInSc3d1	opravdový
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
Rusům	Rus	k1gMnPc3	Rus
nedůvěřujete	důvěřovat	k5eNaImIp2nP	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Beneši	Beneš	k1gMnSc3	Beneš
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
nedůvěřujete	důvěřovat	k5eNaImIp2nP	důvěřovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Avšak	avšak	k8xC	avšak
já	já	k3xPp1nSc1	já
vás	vy	k3xPp2nPc4	vy
ujišťuji	ujišťovat	k5eAaImIp1nS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodržíme	dodržet	k5eAaPmIp1nP	dodržet
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
–	–	k?	–
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
bude	být	k5eAaImBp3nS	být
svobodná	svobodný	k2eAgFnSc1d1	svobodná
a	a	k8xC	a
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
země	země	k1gFnSc1	země
a	a	k8xC	a
že	že	k8xS	že
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
nebudeme	být	k5eNaImBp1nP	být
vměšovat	vměšovat	k5eAaImF	vměšovat
do	do	k7c2	do
vašich	váš	k3xOp2gFnPc2	váš
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
budete	být	k5eAaImBp2nP	být
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
to	ten	k3xDgNnSc1	ten
budete	být	k5eAaImBp2nP	být
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
přípitek	přípitek	k1gInSc4	přípitek
na	na	k7c4	na
večeři	večeře	k1gFnSc4	večeře
na	na	k7c4	na
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
počest	počest	k1gFnSc4	počest
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
vracel	vracet	k5eAaImAgMnS	vracet
z	z	k7c2	z
londýnského	londýnský	k2eAgInSc2d1	londýnský
exilu	exil	k1gInSc2	exil
přes	přes	k7c4	přes
Moskvu	Moskva	k1gFnSc4	Moskva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
83	[number]	k4	83
s.	s.	k?	s.
</s>
</p>
<p>
<s>
O	o	k7c6	o
dialektickém	dialektický	k2eAgInSc6d1	dialektický
a	a	k8xC	a
historickém	historický	k2eAgInSc6d1	historický
materialismu	materialismus	k1gInSc6	materialismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
39	[number]	k4	39
s.	s.	k?	s.
</s>
</p>
<p>
<s>
O	o	k7c6	o
marxismu	marxismus	k1gInSc6	marxismus
v	v	k7c6	v
jazykovědě	jazykověda	k1gFnSc6	jazykověda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
a	a	k8xC	a
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
</s>
</p>
<p>
<s>
Korespondence	korespondence	k1gFnSc1	korespondence
předsedy	předseda	k1gMnSc2	předseda
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
SSSR	SSSR	kA	SSSR
s	s	k7c7	s
prezidenty	prezident	k1gMnPc7	prezident
USA	USA	kA	USA
a	a	k8xC	a
ministerskými	ministerský	k2eAgMnPc7d1	ministerský
předsedy	předseda	k1gMnPc7	předseda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
za	za	k7c2	za
Velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
717	[number]	k4	717
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bullock	Bullock	k1gMnSc1	Bullock
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
<g/>
:	:	kIx,	:
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Kma	Kma	k1gMnSc1	Kma
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
264	[number]	k4	264
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Německé	německý	k2eAgNnSc1d1	německé
vydání	vydání	k1gNnSc1	vydání
Hitler	Hitler	k1gMnSc1	Hitler
und	und	k?	und
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Fishman	Fishman	k1gMnSc1	Fishman
<g/>
,	,	kIx,	,
J.	J.	kA	J.
H.	H.	kA	H.
Hutton	Hutton	k1gInSc1	Hutton
<g/>
:	:	kIx,	:
Soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
Josefa	Josef	k1gMnSc2	Josef
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
286	[number]	k4	286
<g/>
-X	-X	k?	-X
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
:	:	kIx,	:
Tajný	tajný	k2eAgInSc4d1	tajný
projev	projev	k1gInSc4	projev
N.	N.	kA	N.
S.	S.	kA	S.
Chruščova	Chruščův	k2eAgMnSc4d1	Chruščův
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Moulis	Moulis	k1gFnSc2	Moulis
<g/>
:	:	kIx,	:
Běsové	běs	k1gMnPc1	běs
ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86569	[number]	k4	86569
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RYBAS	RYBAS	kA	RYBAS
<g/>
,	,	kIx,	,
Svjatoslav	Svjatoslav	k1gMnSc1	Svjatoslav
Jur	jura	k1gFnPc2	jura
<g/>
'	'	kIx"	'
<g/>
jevič	jevič	k1gInSc1	jevič
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
sláva	sláva	k1gFnSc1	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Miloš	Miloš	k1gMnSc1	Miloš
Hodač	Hodač	k1gMnSc1	Hodač
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7451	[number]	k4	7451
<g/>
-	-	kIx~	-
<g/>
438	[number]	k4	438
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Stalinovo	Stalinův	k2eAgNnSc1d1	Stalinovo
impérium	impérium	k1gNnSc1	impérium
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
391	[number]	k4	391
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Destalinizace	destalinizace	k1gFnSc1	destalinizace
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
</s>
</p>
<p>
<s>
Gulag	gulag	k1gInSc1	gulag
<g/>
,	,	kIx,	,
Komunismus	komunismus	k1gInSc1	komunismus
<g/>
,	,	kIx,	,
Totalita	totalita	k1gFnSc1	totalita
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Zločiny	zločin	k1gInPc1	zločin
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Moskevské	moskevský	k2eAgInPc1d1	moskevský
procesy	proces	k1gInPc1	proces
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
čistka	čistka	k1gFnSc1	čistka
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
</s>
</p>
<p>
<s>
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josif	Josif	k1gInSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Stalin	Stalin	k1gMnSc1	Stalin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Stalin	Stalin	k1gMnSc1	Stalin
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Joseph	Joseph	k1gMnSc1	Joseph
Stalin	Stalin	k1gMnSc1	Stalin
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
<g/>
:	:	kIx,	:
Beneš	Beneš	k1gMnSc1	Beneš
Stalinovi	Stalin	k1gMnSc3	Stalin
<g/>
,	,	kIx,	,
Panu	Pan	k1gMnSc3	Pan
maršálovi	maršál	k1gMnSc3	maršál
Svazu	svaz	k1gInSc3	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
J.	J.	kA	J.
V.	V.	kA	V.
Stalinovi	Stalinův	k2eAgMnPc1d1	Stalinův
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
vlády	vláda	k1gFnSc2	vláda
J.	J.	kA	J.
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc2	Stalin
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
dcera	dcera	k1gFnSc1	dcera
neměla	mít	k5eNaImAgFnS	mít
stání	stání	k1gNnSc4	stání
ani	ani	k8xC	ani
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
</s>
</p>
