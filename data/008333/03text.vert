<p>
<s>
Ghawar	Ghawar	k1gInSc1	Ghawar
je	být	k5eAaImIp3nS	být
ropné	ropný	k2eAgNnSc4d1	ropné
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
100	[number]	k4	100
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
Dhahran	Dhahrana	k1gFnPc2	Dhahrana
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
provincii	provincie	k1gFnSc6	provincie
či	či	k8xC	či
asi	asi	k9	asi
350	[number]	k4	350
km	km	kA	km
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
od	od	k7c2	od
íránského	íránský	k2eAgInSc2d1	íránský
ostrova	ostrov	k1gInSc2	ostrov
Kish	Kisha	k1gFnPc2	Kisha
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
ropná	ropný	k2eAgFnSc1d1	ropná
burza	burza	k1gFnSc1	burza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
280	[number]	k4	280
km	km	kA	km
a	a	k8xC	a
šířkou	šířka	k1gFnSc7	šířka
30	[number]	k4	30
km	km	kA	km
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgNnSc4d3	veliký
konvenční	konvenční	k2eAgNnSc4d1	konvenční
a	a	k8xC	a
suchozemské	suchozemský	k2eAgNnSc4d1	suchozemské
ropné	ropný	k2eAgNnSc4d1	ropné
pole	pole	k1gNnSc4	pole
na	na	k7c6	na
světě	svět	k1gInSc6	svět
co	co	k9	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
kapacity	kapacita	k1gFnSc2	kapacita
a	a	k8xC	a
kumulativní	kumulativní	k2eAgFnSc2d1	kumulativní
i	i	k8xC	i
současné	současný	k2eAgFnSc2d1	současná
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
též	též	k9	též
zůstane	zůstat	k5eAaPmIp3nS	zůstat
"	"	kIx"	"
<g/>
největším	veliký	k2eAgNnSc7d3	veliký
známým	známý	k2eAgNnSc7d1	známé
ložiskem	ložisko	k1gNnSc7	ložisko
ropy	ropa	k1gFnSc2	ropa
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Celé	celý	k2eAgNnSc1d1	celé
pole	pole	k1gNnSc1	pole
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
těží	těžet	k5eAaImIp3nS	těžet
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
znárodněná	znárodněný	k2eAgFnSc1d1	znárodněná
saúdská	saúdský	k2eAgFnSc1d1	Saúdská
těžební	těžební	k2eAgFnSc1d1	těžební
společnost	společnost	k1gFnSc1	společnost
Saudi	Saud	k1gMnPc1	Saud
Aramco	Aramco	k1gMnSc1	Aramco
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aramco	Aramco	k6eAd1	Aramco
nemá	mít	k5eNaImIp3nS	mít
vstřícnou	vstřícný	k2eAgFnSc4d1	vstřícná
politiku	politika	k1gFnSc4	politika
k	k	k7c3	k
poskytování	poskytování	k1gNnSc3	poskytování
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
Ghawar	Ghawar	k1gInSc1	Ghawar
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
těžebních	těžební	k2eAgFnPc2d1	těžební
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Ain	Ain	k1gFnSc1	Ain
Dar	dar	k1gInSc1	dar
</s>
</p>
<p>
<s>
Shedgum	Shedgum	k1gInSc1	Shedgum
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Uthmaniyah	Uthmaniyah	k1gInSc1	Uthmaniyah
</s>
</p>
<p>
<s>
Hawiyah	Hawiyah	k1gMnSc1	Hawiyah
</s>
</p>
<p>
<s>
Haradh	Haradh	k1gInSc1	Haradh
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
'	'	kIx"	'
<g/>
Uthmaniyah	Uthmaniyah	k1gInSc1	Uthmaniyah
spadá	spadat	k5eAaImIp3nS	spadat
ještě	ještě	k9	ještě
menší	malý	k2eAgFnSc1d2	menší
oáza	oáza	k1gFnSc1	oáza
Al-Ahsa	Al-Ahsa	k1gFnSc1	Al-Ahsa
<g/>
;	;	kIx,	;
v	v	k7c6	v
nejsevernějším	severní	k2eAgInSc6d3	nejsevernější
cípu	cíp	k1gInSc6	cíp
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblast	oblast	k1gFnSc1	oblast
Fazran	Fazrana	k1gFnPc2	Fazrana
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
další	další	k2eAgNnSc1d1	další
pole	pole	k1gNnSc1	pole
Abqaiq	Abqaiq	k1gFnSc2	Abqaiq
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ložiskové	ložiskový	k2eAgNnSc1d1	ložiskové
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
systémem	systém	k1gInSc7	systém
úzkých	úzké	k1gInPc2	úzké
<g/>
,	,	kIx,	,
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
antiklinál	antiklinála	k1gFnPc2	antiklinála
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
přesmyk	přesmyk	k1gInSc1	přesmyk
<g/>
,	,	kIx,	,
tvoříc	tvořit	k5eAaImSgNnS	tvořit
tak	tak	k6eAd1	tak
tři	tři	k4xCgFnPc4	tři
ropné	ropný	k2eAgFnPc4d1	ropná
kapsy	kapsa	k1gFnPc4	kapsa
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
spodních	spodní	k2eAgFnPc2d1	spodní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Antiklinály	antiklinála	k1gFnPc1	antiklinála
tvoří	tvořit	k5eAaImIp3nP	tvořit
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
cca	cca	kA	cca
20	[number]	k4	20
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
"	"	kIx"	"
<g/>
žlab	žlab	k1gInSc1	žlab
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnPc2	jehož
nejsvrchnějších	svrchní	k2eAgFnPc2d3	nejsvrchnější
pozic	pozice	k1gFnPc2	pozice
se	se	k3xPyFc4	se
odčerpává	odčerpávat	k5eAaImIp3nS	odčerpávat
směs	směs	k1gFnSc1	směs
ropy	ropa	k1gFnSc2	ropa
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
Pole	pole	k1gNnSc1	pole
Ghawar	Ghawara	k1gFnPc2	Ghawara
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
a	a	k8xC	a
produkovat	produkovat	k5eAaImF	produkovat
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
představovalo	představovat	k5eAaImAgNnS	představovat
60	[number]	k4	60
-	-	kIx~	-
65	[number]	k4	65
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
již	již	k6eAd1	již
denní	denní	k2eAgFnSc1d1	denní
produkce	produkce	k1gFnSc1	produkce
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
5,7	[number]	k4	5,7
milionů	milion	k4xCgInPc2	milion
barelů	barel	k1gInPc2	barel
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
asi	asi	k9	asi
56,6	[number]	k4	56,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
číslech	číslo	k1gNnPc6	číslo
ropné	ropný	k2eAgFnSc2d1	ropná
produkce	produkce	k1gFnSc2	produkce
Ghawar	Ghawar	k1gMnSc1	Ghawar
představoval	představovat	k5eAaImAgMnS	představovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
a	a	k8xC	a
6,25	[number]	k4	6,25
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
těžby	těžba	k1gFnSc2	těžba
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
zdaleka	zdaleka	k6eAd1	zdaleka
nejvíc	hodně	k6eAd3	hodně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
Ghawar	Ghawar	k1gMnSc1	Ghawar
doposud	doposud	k6eAd1	doposud
vydal	vydat	k5eAaPmAgMnS	vydat
60	[number]	k4	60
mld.	mld.	k?	mld.
barelů	barel	k1gInPc2	barel
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Aramco	Aramco	k6eAd1	Aramco
do	do	k7c2	do
ložisek	ložisko	k1gNnPc2	ložisko
Ghawaru	Ghawar	k1gInSc2	Ghawar
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
době	době	k6eAd1	době
injektuje	injektovat	k5eAaBmIp3nS	injektovat
7	[number]	k4	7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
denně	denně	k6eAd1	denně
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
těží	těžet	k5eAaImIp3nS	těžet
směs	směs	k1gFnSc1	směs
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
40-45	[number]	k4	40-45
%	%	kIx~	%
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zásoby	zásoba	k1gFnSc2	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Saudi	Saud	k1gMnPc1	Saud
Aramco	Aramco	k6eAd1	Aramco
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pole	pole	k1gNnSc1	pole
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
71	[number]	k4	71
mld.	mld.	k?	mld.
barelů	barel	k1gInPc2	barel
ověřených	ověřený	k2eAgFnPc2d1	ověřená
zásob	zásoba	k1gFnPc2	zásoba
"	"	kIx"	"
<g/>
snadné	snadný	k2eAgFnSc2d1	snadná
<g/>
"	"	kIx"	"
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Stuard	Stuard	k1gMnSc1	Stuard
Staniford	Staniford	k1gMnSc1	Staniford
a	a	k8xC	a
Matthew	Matthew	k1gMnSc1	Matthew
Simmons	Simmons	k1gInSc4	Simmons
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Twilight	Twilight	k1gMnSc1	Twilight
in	in	k?	in
the	the	k?	the
Desert	desert	k1gInSc1	desert
(	(	kIx(	(
<g/>
Soumrak	soumrak	k1gInSc1	soumrak
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ropná	ropný	k2eAgFnSc1d1	ropná
produkce	produkce	k1gFnSc1	produkce
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
dostává	dostávat	k5eAaImIp3nS	dostávat
přes	přes	k7c4	přes
ropný	ropný	k2eAgInSc4d1	ropný
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Simmonsova	Simmonsův	k2eAgFnSc1d1	Simmonsova
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
Aramcem	Aramec	k1gMnSc7	Aramec
striktně	striktně	k6eAd1	striktně
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zpráv	zpráva	k1gFnPc2	zpráva
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyčerpáno	vyčerpat	k5eAaPmNgNnS	vyčerpat
z	z	k7c2	z
48	[number]	k4	48
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podílu	podíl	k1gInSc3	podíl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Ghawar	Ghawar	k1gInSc1	Ghawar
(	(	kIx(	(
<g/>
z	z	k7c2	z
tisíců	tisíc	k4xCgInPc2	tisíc
současných	současný	k2eAgFnPc2d1	současná
ropných	ropný	k2eAgFnPc2d1	ropná
polí	pole	k1gFnPc2	pole
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc3d1	celosvětová
produkci	produkce	k1gFnSc3	produkce
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
celosvětové	celosvětový	k2eAgInPc4d1	celosvětový
objevy	objev	k1gInPc4	objev
ropy	ropa	k1gFnSc2	ropa
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
ropný	ropný	k2eAgInSc1d1	ropný
vrchol	vrchol	k1gInSc1	vrchol
Ghawaru	Ghawar	k1gInSc2	Ghawar
byl	být	k5eAaImAgInS	být
současně	současně	k6eAd1	současně
ropným	ropný	k2eAgInSc7d1	ropný
vrcholem	vrchol	k1gInSc7	vrchol
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
diskuze	diskuze	k1gFnPc1	diskuze
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
a	a	k8xC	a
dokumenty	dokument	k1gInPc1	dokument
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
o	o	k7c6	o
ropném	ropný	k2eAgInSc6d1	ropný
zlomu	zlom	k1gInSc6	zlom
Ghawaru	Ghawar	k1gInSc2	Ghawar
hovoří	hovořit	k5eAaImIp3nS	hovořit
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Ghawar	Ghawar	k1gMnSc1	Ghawar
Oil	Oil	k1gMnSc1	Oil
Field	Field	k1gMnSc1	Field
<g/>
,	,	kIx,	,
Saudi	Saud	k1gMnPc1	Saud
Arabia	Arabius	k1gMnSc2	Arabius
přehled	přehled	k1gInSc1	přehled
s	s	k7c7	s
mapou	mapa	k1gFnSc7	mapa
</s>
</p>
<p>
<s>
Ghawar	Ghawar	k1gInSc1	Ghawar
na	na	k7c4	na
Google	Google	k1gNnSc4	Google
Maps	Mapsa	k1gFnPc2	Mapsa
</s>
</p>
<p>
<s>
Hloubkový	hloubkový	k2eAgInSc1d1	hloubkový
ponor	ponor	k1gInSc1	ponor
Stuarta	Stuart	k1gMnSc2	Stuart
Staniforda	Staniford	k1gMnSc2	Staniford
do	do	k7c2	do
písků	písek	k1gInPc2	písek
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
</s>
</p>
<p>
<s>
Stanifordova	Stanifordův	k2eAgFnSc1d1	Stanifordův
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
analýza	analýza	k1gFnSc1	analýza
Ghawaru	Ghawar	k1gInSc2	Ghawar
</s>
</p>
<p>
<s>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Crude	Crud	k1gInSc5	Crud
Awakening	Awakening	k1gInSc1	Awakening
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Oil	Oil	k1gMnSc1	Oil
Crash	Crash	k1gMnSc1	Crash
(	(	kIx(	(
<g/>
zmínka	zmínka	k1gFnSc1	zmínka
kolem	kolem	k7c2	kolem
35	[number]	k4	35
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnSc2	minuta
dokumentu	dokument	k1gInSc2	dokument
<g/>
)	)	kIx)	)
</s>
</p>
