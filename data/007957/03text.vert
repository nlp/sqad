<s>
Lékořice	lékořice	k1gFnSc1	lékořice
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovité	bobovitý	k2eAgFnSc2d1	bobovitá
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
polokeře	polokeř	k1gInPc1	polokeř
se	s	k7c7	s
zpeřenými	zpeřený	k2eAgInPc7d1	zpeřený
listy	list	k1gInPc7	list
a	a	k8xC	a
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
,	,	kIx,	,
žlutými	žlutý	k2eAgInPc7d1	žlutý
nebo	nebo	k8xC	nebo
červenými	červený	k2eAgInPc7d1	červený
květy	květ	k1gInPc7	květ
v	v	k7c6	v
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
jako	jako	k9	jako
léčivo	léčivo	k1gNnSc1	léčivo
a	a	k8xC	a
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
lékořice	lékořice	k1gFnSc1	lékořice
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
květeně	květena	k1gFnSc6	květena
je	být	k5eAaImIp3nS	být
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
pouze	pouze	k6eAd1	pouze
zplanělý	zplanělý	k2eAgInSc1d1	zplanělý
druh	druh	k1gInSc1	druh
lékořice	lékořice	k1gFnSc2	lékořice
lysá	lysat	k5eAaImIp3nS	lysat
<g/>
.	.	kIx.	.
</s>
<s>
Lékořice	lékořice	k1gFnPc1	lékořice
jsou	být	k5eAaImIp3nP	být
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
polokeře	polokeř	k1gInPc1	polokeř
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
oddenky	oddenek	k1gInPc7	oddenek
a	a	k8xC	a
bohatým	bohatý	k2eAgInSc7d1	bohatý
kořenovým	kořenový	k2eAgInSc7d1	kořenový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
<g/>
,	,	kIx,	,
větvený	větvený	k2eAgInSc1d1	větvený
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
lichozpeřené	lichozpeřený	k2eAgFnPc1d1	lichozpeřený
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnPc1d1	složená
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
5	[number]	k4	5
až	až	k9	až
17	[number]	k4	17
celokrajných	celokrajný	k2eAgInPc2d1	celokrajný
nebo	nebo	k8xC	nebo
pilovitých	pilovitý	k2eAgInPc2d1	pilovitý
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc1d1	volný
<g/>
,	,	kIx,	,
vytrvalé	vytrvalý	k2eAgInPc1d1	vytrvalý
nebo	nebo	k8xC	nebo
opadavé	opadavý	k2eAgInPc1d1	opadavý
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
úžlabních	úžlabní	k2eAgInPc6d1	úžlabní
dlouze	dlouho	k6eAd1	dlouho
stopkatých	stopkatý	k2eAgInPc6d1	stopkatý
hroznech	hrozen	k1gInPc6	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
pětičetný	pětičetný	k2eAgMnSc1d1	pětičetný
<g/>
,	,	kIx,	,
zvonkovitý	zvonkovitý	k2eAgMnSc1d1	zvonkovitý
až	až	k6eAd1	až
válcovitý	válcovitý	k2eAgMnSc1d1	válcovitý
<g/>
,	,	kIx,	,
s	s	k7c7	s
5	[number]	k4	5
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
purpurová	purpurový	k2eAgFnSc1d1	purpurová
až	až	k6eAd1	až
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasických	klasický	k2eAgInPc2d1	klasický
květů	květ	k1gInPc2	květ
bobovitých	bobovitý	k2eAgMnPc2d1	bobovitý
jsou	být	k5eAaImIp3nP	být
člunek	člunek	k1gInSc1	člunek
a	a	k8xC	a
křídla	křídlo	k1gNnPc1	křídlo
zkráceny	zkrácen	k2eAgFnPc1d1	zkrácena
oproti	oproti	k7c3	oproti
pavéze	pavéza	k1gFnSc3	pavéza
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
9	[number]	k4	9
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
srostlých	srostlý	k2eAgFnPc2d1	srostlá
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
nebo	nebo	k8xC	nebo
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
přirostlá	přirostlý	k2eAgFnSc1d1	přirostlá
k	k	k7c3	k
ostatním	ostatní	k1gNnPc3	ostatní
(	(	kIx(	(
<g/>
jednobratré	jednobratrý	k2eAgInPc1d1	jednobratrý
nebo	nebo	k8xC	nebo
dvoubratré	dvoubratrý	k2eAgInPc1d1	dvoubratrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
přisedlý	přisedlý	k2eAgInSc1d1	přisedlý
<g/>
,	,	kIx,	,
lysý	lysý	k2eAgInSc1d1	lysý
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
až	až	k9	až
10	[number]	k4	10
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
pukavý	pukavý	k2eAgInSc1d1	pukavý
nebo	nebo	k8xC	nebo
nepukavý	pukavý	k2eNgInSc1d1	nepukavý
lusk	lusk	k1gInSc1	lusk
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
obvykle	obvykle	k6eAd1	obvykle
trnitý	trnitý	k2eAgMnSc1d1	trnitý
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
zúžený	zúžený	k2eAgMnSc1d1	zúžený
v	v	k7c4	v
zobánek	zobánek	k1gInSc4	zobánek
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
zploštělá	zploštělý	k2eAgNnPc1d1	zploštělé
<g/>
,	,	kIx,	,
okrouhlého	okrouhlý	k2eAgInSc2d1	okrouhlý
až	až	k8xS	až
ledvinovitého	ledvinovitý	k2eAgInSc2d1	ledvinovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
lékořice	lékořice	k1gFnSc2	lékořice
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
především	především	k9	především
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
i	i	k9	i
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
lepidota	lepidota	k1gFnSc1	lepidota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
G.	G.	kA	G.
astragalina	astragalina	k1gFnSc1	astragalina
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
G.	G.	kA	G.
acanthocarpa	acanthocarpa	k1gFnSc1	acanthocarpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
druh	druh	k1gInSc1	druh
původní	původní	k2eAgInSc1d1	původní
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
však	však	k9	však
místy	místy	k6eAd1	místy
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
lékořice	lékořice	k1gFnSc1	lékořice
lysá	lysý	k2eAgFnSc1d1	Lysá
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
glabra	glabra	k1gFnSc1	glabra
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
květeně	květena	k1gFnSc6	květena
je	být	k5eAaImIp3nS	být
lékořice	lékořice	k1gFnSc1	lékořice
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
původními	původní	k2eAgInPc7d1	původní
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Lékořice	lékořice	k1gFnSc1	lékořice
lysá	lysý	k2eAgFnSc1d1	Lysá
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
glabra	glabra	k1gFnSc1	glabra
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
lékořice	lékořice	k1gFnSc1	lékořice
ježatá	ježatý	k2eAgFnSc1d1	ježatá
(	(	kIx(	(
<g/>
G.	G.	kA	G.
echinata	echinata	k1gFnSc1	echinata
<g/>
)	)	kIx)	)
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
G.	G.	kA	G.
foetida	foetida	k1gFnSc1	foetida
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
:	:	kIx,	:
G.	G.	kA	G.
aspera	aspera	k1gFnSc1	aspera
a	a	k8xC	a
G.	G.	kA	G.
korshinskyi	korshinskyi	k1gNnSc1	korshinskyi
<g/>
.	.	kIx.	.
lékořice	lékořice	k1gFnSc1	lékořice
lysá	lysý	k2eAgFnSc1d1	Lysá
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
glabra	glabra	k1gFnSc1	glabra
<g/>
)	)	kIx)	)
lékořice	lékořice	k1gFnSc1	lékořice
uralská	uralský	k2eAgFnSc1d1	Uralská
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
uralensis	uralensis	k1gFnSc2	uralensis
<g/>
)	)	kIx)	)
Jako	jako	k8xC	jako
stará	starý	k2eAgFnSc1d1	stará
kulturní	kulturní	k2eAgFnSc1d1	kulturní
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
pěstována	pěstován	k2eAgFnSc1d1	pěstována
zejména	zejména	k9	zejména
lékořice	lékořice	k1gFnSc1	lékořice
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc1d1	pocházející
ze	z	k7c2	z
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
cukrářství	cukrářství	k1gNnSc6	cukrářství
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
tabákovém	tabákový	k2eAgInSc6d1	tabákový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
rostlinou	rostlina	k1gFnSc7	rostlina
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
medicíny	medicína	k1gFnSc2	medicína
je	být	k5eAaImIp3nS	být
lékořice	lékořice	k1gFnSc1	lékořice
uralská	uralský	k2eAgFnSc1d1	Uralská
(	(	kIx(	(
<g/>
Glycyrrhiza	Glycyrrhiza	k1gFnSc1	Glycyrrhiza
uralensis	uralensis	k1gFnSc2	uralensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Glycyrrhiza	Glycyrrhiz	k1gMnSc2	Glycyrrhiz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Glycyrrhiza	Glycyrrhiz	k1gMnSc4	Glycyrrhiz
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lékořice	lékořice	k1gFnSc2	lékořice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
