<s>
Ainština	Ainština	k1gFnSc1	Ainština
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Ainu	Aina	k1gFnSc4	Aina
(	(	kIx(	(
<g/>
ア	ア	k?	ア
イ	イ	k?	イ
<g/>
,	,	kIx,	,
Aynu	Aynus	k1gInSc2	Aynus
Itak	Itaka	k1gFnPc2	Itaka
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ア	ア	k?	ア
<g/>
,	,	kIx,	,
Ainu-go	Ainuo	k1gMnSc1	Ainu-go
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mluví	mluvit	k5eAaImIp3nS	mluvit
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
Ainu	Ainus	k1gInSc2	Ainus
na	na	k7c6	na
severu	sever	k1gInSc6	sever
japonského	japonský	k2eAgInSc2d1	japonský
ostrova	ostrov	k1gInSc2	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
.	.	kIx.	.
</s>
