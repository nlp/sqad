<p>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Franciszek	Franciszka	k1gFnPc2	Franciszka
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Frédéric	Frédéric	k1gMnSc1	Frédéric
François	François	k1gFnPc2	François
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
česká	český	k2eAgFnSc1d1	Česká
[	[	kIx(	[
<g/>
šopén	šopén	k1gInSc1	šopén
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
psáno	psát	k5eAaImNgNnS	psát
rovněž	rovněž	k6eAd1	rovněž
"	"	kIx"	"
<g/>
Szopen	Szopen	k2eAgMnSc1d1	Szopen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1810	[number]	k4	1810
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
skladatel	skladatel	k1gMnSc1	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
období	období	k1gNnSc2	období
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proslul	proslout	k5eAaPmAgInS	proslout
jako	jako	k9	jako
"	"	kIx"	"
<g/>
básník	básník	k1gMnSc1	básník
klavíru	klavír	k1gInSc2	klavír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
emigrant	emigrant	k1gMnSc1	emigrant
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
zchudlého	zchudlý	k2eAgInSc2d1	zchudlý
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
prožil	prožít	k5eAaPmAgMnS	prožít
dětství	dětství	k1gNnSc2	dětství
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
začal	začít	k5eAaPmAgMnS	začít
učit	učit	k5eAaImF	učit
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
u	u	k7c2	u
Čecha	Čech	k1gMnSc2	Čech
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Živného	živný	k2eAgMnSc2d1	živný
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
lycea	lyceum	k1gNnSc2	lyceum
a	a	k8xC	a
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1826	[number]	k4	1826
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
u	u	k7c2	u
Józefa	Józef	k1gMnSc2	Józef
Elsnera	Elsner	k1gMnSc2	Elsner
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
škole	škola	k1gFnSc6	škola
slavil	slavit	k5eAaImAgInS	slavit
úspěch	úspěch	k1gInSc4	úspěch
koncertováním	koncertování	k1gNnSc7	koncertování
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
měl	mít	k5eAaImAgInS	mít
malý	malý	k2eAgInSc1d1	malý
soukromý	soukromý	k2eAgInSc1d1	soukromý
recitál	recitál	k1gInSc1	recitál
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
opustil	opustit	k5eAaPmAgMnS	opustit
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navázal	navázat	k5eAaPmAgInS	navázat
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
George	George	k1gFnSc7	George
Sandovou	Sandová	k1gFnSc7	Sandová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
již	již	k9	již
nenavázal	navázat	k5eNaPmAgInS	navázat
další	další	k2eAgInSc1d1	další
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
cestoval	cestovat	k5eAaImAgMnS	cestovat
i	i	k9	i
po	po	k7c6	po
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
nemoci	nemoc	k1gFnPc4	nemoc
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
též	též	k9	též
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
srdce	srdce	k1gNnSc2	srdce
ve	v	k7c6	v
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
schránce	schránka	k1gFnSc6	schránka
je	být	k5eAaImIp3nS	být
však	však	k9	však
uloženo	uložit	k5eAaPmNgNnS	uložit
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
se	se	k3xPyFc4	se
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
konají	konat	k5eAaImIp3nP	konat
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
soutěže	soutěž	k1gFnPc1	soutěž
mladých	mladý	k2eAgMnPc2d1	mladý
klavíristů	klavírista	k1gMnPc2	klavírista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
psal	psát	k5eAaImAgMnS	psát
především	především	k6eAd1	především
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
ostatní	ostatní	k2eAgFnPc1d1	ostatní
skladby	skladba	k1gFnPc1	skladba
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
klavírní	klavírní	k2eAgInSc4d1	klavírní
part	part	k1gInSc4	part
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
skladby	skladba	k1gFnPc4	skladba
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
díla	dílo	k1gNnPc1	dílo
malého	malý	k2eAgInSc2d1	malý
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
nokturna	nokturno	k1gNnPc1	nokturno
<g/>
,	,	kIx,	,
valčíky	valčík	k1gInPc1	valčík
nebo	nebo	k8xC	nebo
cyklus	cyklus	k1gInSc1	cyklus
preludií	preludie	k1gFnPc2	preludie
<g/>
.	.	kIx.	.
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS	komponovat
také	také	k9	také
taneční	taneční	k2eAgFnPc4d1	taneční
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
polský	polský	k2eAgInSc1d1	polský
národní	národní	k2eAgInSc1d1	národní
prvek	prvek	k1gInSc1	prvek
vyniká	vynikat	k5eAaImIp3nS	vynikat
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
60	[number]	k4	60
mazurkách	mazurka	k1gFnPc6	mazurka
a	a	k8xC	a
ve	v	k7c6	v
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
polonézách	polonéza	k1gFnPc6	polonéza
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
skladby	skladba	k1gFnPc1	skladba
rozsáhlejšího	rozsáhlý	k2eAgInSc2d2	rozsáhlejší
charakteru	charakter	k1gInSc2	charakter
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
klavírní	klavírní	k2eAgFnPc1d1	klavírní
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
vynikají	vynikat	k5eAaImIp3nP	vynikat
především	především	k9	především
dva	dva	k4xCgInPc4	dva
klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
velmi	velmi	k6eAd1	velmi
radikálně	radikálně	k6eAd1	radikálně
změnil	změnit	k5eAaPmAgInS	změnit
vnímání	vnímání	k1gNnSc4	vnímání
některých	některý	k3yIgFnPc2	některý
hudebních	hudební	k2eAgFnPc2d1	hudební
forem	forma	k1gFnPc2	forma
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
romantismu	romantismus	k1gInSc6	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
a	a	k8xC	a
krystalizoval	krystalizovat	k5eAaImAgMnS	krystalizovat
jejich	jejich	k3xOp3gFnSc4	jejich
podobu	podoba	k1gFnSc4	podoba
(	(	kIx(	(
<g/>
nokturno	nokturno	k1gNnSc4	nokturno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
změnil	změnit	k5eAaPmAgInS	změnit
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
(	(	kIx(	(
<g/>
polonéza	polonéza	k1gFnSc1	polonéza
<g/>
,	,	kIx,	,
preludium	preludium	k1gNnSc1	preludium
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
nově	nově	k6eAd1	nově
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
(	(	kIx(	(
<g/>
balada	balada	k1gFnSc1	balada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pozdních	pozdní	k2eAgInPc6d1	pozdní
opusech	opus	k1gInPc6	opus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
klavírních	klavírní	k2eAgFnPc6d1	klavírní
sonátách	sonáta	k1gFnPc6	sonáta
nebo	nebo	k8xC	nebo
violoncellové	violoncellový	k2eAgFnSc6d1	violoncellová
sonátě	sonáta	k1gFnSc6	sonáta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
vracel	vracet	k5eAaImAgMnS	vracet
ke	k	k7c3	k
klasickým	klasický	k2eAgNnPc3d1	klasické
formálním	formální	k2eAgNnPc3d1	formální
řešením	řešení	k1gNnPc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
měl	mít	k5eAaImAgMnS	mít
Chopin	Chopin	k1gMnSc1	Chopin
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
Smetanu	smetan	k1gInSc6	smetan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
skladbám	skladba	k1gFnPc3	skladba
měl	mít	k5eAaImAgInS	mít
jsem	být	k5eAaImIp1nS	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
koncertech	koncert	k1gInPc6	koncert
co	co	k9	co
děkovat	děkovat	k5eAaImF	děkovat
za	za	k7c4	za
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jsem	být	k5eAaImIp1nS	být
skladby	skladba	k1gFnSc2	skladba
jeho	jeho	k3xOp3gNnSc4	jeho
byl	být	k5eAaImAgMnS	být
poznal	poznat	k5eAaPmAgMnS	poznat
a	a	k8xC	a
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
jsem	být	k5eAaImIp1nS	být
též	též	k9	též
<g/>
,	,	kIx,	,
co	co	k8xS	co
mojí	můj	k3xOp1gFnSc7	můj
úlohou	úloha	k1gFnSc7	úloha
jest	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Żelazowa	Żelazowum	k1gNnSc2	Żelazowum
Wola	Wol	k1gInSc2	Wol
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	s	k7c7	s
60	[number]	k4	60
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Varšavy	Varšava	k1gFnSc2	Varšava
v	v	k7c6	v
Sochaczewském	Sochaczewský	k2eAgInSc6d1	Sochaczewský
okrese	okres	k1gInSc6	okres
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Varšavském	varšavský	k2eAgNnSc6d1	Varšavské
knížectví	knížectví	k1gNnSc6	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Mikołaj	Mikołaj	k1gMnSc1	Mikołaj
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Nicolas	Nicolas	k1gMnSc1	Nicolas
<g/>
)	)	kIx)	)
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Francouz	Francouzy	k1gInPc2	Francouzy
z	z	k7c2	z
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
<g/>
,	,	kIx,	,
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
16	[number]	k4	16
let	léto	k1gNnPc2	léto
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
národní	národní	k2eAgFnSc6d1	národní
gardě	garda	k1gFnSc6	garda
během	během	k7c2	během
Kościuszkova	Kościuszkův	k2eAgNnSc2d1	Kościuszkův
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
ruské	ruský	k2eAgFnSc3d1	ruská
nadvládě	nadvláda	k1gFnSc3	nadvláda
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
děti	dítě	k1gFnPc4	dítě
polských	polský	k2eAgMnPc2d1	polský
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
hrabat	hrabě	k1gNnPc2	hrabě
Skarbků	Skarbek	k1gMnPc2	Skarbek
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
příbuznou	příbuzná	k1gFnSc7	příbuzná
<g/>
,	,	kIx,	,
Justynou	Justynou	k1gFnSc7	Justynou
Krzyżanowskou	Krzyżanowska	k1gFnSc7	Krzyżanowska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
<g/>
Justynin	Justynin	k2eAgMnSc1d1	Justynin
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
generála	generál	k1gMnSc2	generál
Unie	unie	k1gFnSc2	unie
za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Włodzimierza	Włodzimierz	k1gMnSc2	Włodzimierz
Krzyżanowského	Krzyżanowský	k1gMnSc2	Krzyżanowský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
Mikołaje	Mikołaj	k1gInSc2	Mikołaj
a	a	k8xC	a
Justyny	Justyna	k1gFnSc2	Justyna
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
v	v	k7c6	v
Brochówě	Brochówa	k1gFnSc6	Brochówa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
chrámu	chrám	k1gInSc6	chrám
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
pokřtěno	pokřtěn	k2eAgNnSc1d1	pokřtěno
jejich	jejich	k3xOp3gNnSc4	jejich
druhé	druhý	k4xOgNnSc4	druhý
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Franciszek	Franciszka	k1gFnPc2	Franciszka
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
nalezen	naleznout	k5eAaPmNgInS	naleznout
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
22	[number]	k4	22
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xC	jako
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
1	[number]	k4	1
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Ludwiku	Ludwika	k1gFnSc4	Ludwika
(	(	kIx(	(
<g/>
*	*	kIx~	*
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Isabellu	Isabella	k1gFnSc4	Isabella
(	(	kIx(	(
<g/>
*	*	kIx~	*
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
a	a	k8xC	a
Emilii	Emilie	k1gFnSc4	Emilie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Chopinovi	Chopin	k1gMnSc3	Chopin
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
od	od	k7c2	od
lexikografa	lexikograf	k1gMnSc2	lexikograf
Samuela	Samuel	k1gMnSc2	Samuel
Lindeho	Linde	k1gMnSc2	Linde
vyučovat	vyučovat	k5eAaImF	vyučovat
francouzštinu	francouzština	k1gFnSc4	francouzština
na	na	k7c6	na
varšavském	varšavský	k2eAgNnSc6d1	Varšavské
lyceu	lyceum	k1gNnSc6	lyceum
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
Saském	saský	k2eAgInSc6d1	saský
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
rodina	rodina	k1gFnSc1	rodina
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
velkokníže	velkokníže	k1gMnSc1	velkokníže
Konstantin	Konstantin	k1gMnSc1	Konstantin
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
změnil	změnit	k5eAaPmAgMnS	změnit
využití	využití	k1gNnSc2	využití
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
a	a	k8xC	a
lyceum	lyceum	k1gNnSc1	lyceum
se	se	k3xPyFc4	se
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Kazimierzowského	Kazimierzowský	k2eAgInSc2d1	Kazimierzowský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
university	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
prostorném	prostorný	k2eAgNnSc6d1	prostorné
apartmá	apartmá	k1gNnSc6	apartmá
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
sousední	sousední	k2eAgFnSc2d1	sousední
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
toto	tento	k3xDgNnSc4	tento
lyceum	lyceum	k1gNnSc4	lyceum
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1823	[number]	k4	1823
až	až	k9	až
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polský	polský	k2eAgMnSc1d1	polský
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
plně	plně	k6eAd1	plně
prostupovaly	prostupovat	k5eAaImAgFnP	prostupovat
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nebyla	být	k5eNaImAgFnS	být
Fryderykova	Fryderykův	k2eAgFnSc1d1	Fryderykův
francouzština	francouzština	k1gFnSc1	francouzština
úplně	úplně	k6eAd1	úplně
bezchybná	bezchybný	k2eAgFnSc1d1	bezchybná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
Enault	Enault	k1gMnSc1	Enault
<g/>
,	,	kIx,	,
životopisec	životopisec	k1gMnSc1	životopisec
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
citát	citát	k1gInSc4	citát
George	Georg	k1gMnSc2	Georg
Sandové	Sandová	k1gFnSc2	Sandová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Chopina	Chopin	k1gMnSc4	Chopin
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
polštějšího	polský	k2eAgMnSc2d2	polský
než	než	k8xS	než
Polsko	Polsko	k1gNnSc1	Polsko
samé	samý	k3xTgInPc4	samý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
muzikální	muzikální	k2eAgFnSc1d1	muzikální
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
také	také	k9	také
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rodičům	rodič	k1gMnPc3	rodič
se	se	k3xPyFc4	se
tak	tak	k9	tak
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
brzy	brzy	k6eAd1	brzy
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
v	v	k7c6	v
mnoha	mnoho	k4c3	mnoho
jejích	její	k3xOp3gFnPc6	její
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
<g/>
Józef	Józef	k1gInSc4	Józef
Sikorski	Sikorsk	k1gFnSc2	Sikorsk
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
Chopinův	Chopinův	k2eAgMnSc1d1	Chopinův
současník	současník	k1gMnSc1	současník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
Wspomnienie	Wspomnienie	k1gFnSc2	Wspomnienie
Chopina	Chopin	k1gMnSc4	Chopin
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc1	dítě
Chopin	Chopin	k1gMnSc1	Chopin
často	často	k6eAd1	často
plakal	plakat	k5eAaImAgMnS	plakat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
klavírní	klavírní	k2eAgFnSc1d1	klavírní
lekce	lekce	k1gFnSc1	lekce
mu	on	k3xPp3gMnSc3	on
dávala	dávat	k5eAaImAgFnS	dávat
jeho	jeho	k3xOp3gFnSc1	jeho
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
Ludwika	Ludwika	k1gFnSc1	Ludwika
<g/>
.	.	kIx.	.
<g/>
Chopinovým	Chopinův	k2eAgInSc7d1	Chopinův
prvním	první	k4xOgMnSc6	první
učitelem	učitel	k1gMnSc7	učitel
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1816	[number]	k4	1816
<g/>
–	–	k?	–
<g/>
1822	[number]	k4	1822
Čech	Čech	k1gMnSc1	Čech
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Živný	živný	k2eAgMnSc1d1	živný
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
psán	psán	k2eAgInSc1d1	psán
Wojciech	Wojciech	k1gInSc1	Wojciech
Żywny	Żywna	k1gFnSc2	Żywna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
již	již	k6eAd1	již
jako	jako	k9	jako
mladík	mladík	k1gInSc4	mladík
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
překonal	překonat	k5eAaPmAgMnS	překonat
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
vážil	vážit	k5eAaImAgMnS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
Szopenek	Szopenka	k1gFnPc2	Szopenka
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
srovnání	srovnání	k1gNnSc4	srovnání
s	s	k7c7	s
Mozartem	Mozart	k1gMnSc7	Mozart
a	a	k8xC	a
Beethovenem	Beethoven	k1gMnSc7	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
provedl	provést	k5eAaPmAgInS	provést
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Živného	živný	k2eAgMnSc2d1	živný
klavírní	klavírní	k2eAgInSc4d1	klavírní
koncert	koncert	k1gInSc4	koncert
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Jírovce	jírovec	k1gInSc2	jírovec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
též	též	k6eAd1	též
složil	složit	k5eAaPmAgMnS	složit
dvě	dva	k4xCgFnPc4	dva
polonézy	polonéza	k1gFnPc4	polonéza
(	(	kIx(	(
<g/>
g	g	kA	g
moll	moll	k1gNnSc1	moll
a	a	k8xC	a
B	B	kA	B
dur	dur	k1gNnSc4	dur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
Izydora	Izydor	k1gMnSc2	Izydor
Józefa	Józef	k1gMnSc2	Józef
Cybulského	Cybulský	k2eAgMnSc2d1	Cybulský
(	(	kIx(	(
<g/>
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
rytec	rytec	k1gMnSc1	rytec
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
varhanické	varhanický	k2eAgFnSc2d1	varhanická
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
vydavatelů	vydavatel	k1gMnPc2	vydavatel
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
jako	jako	k9	jako
rukopis	rukopis	k1gInSc1	rukopis
opsaný	opsaný	k2eAgInSc1d1	opsaný
Mikołajem	Mikołaj	k1gMnSc7	Mikołaj
Chopinem	Chopin	k1gMnSc7	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
malá	malá	k1gFnSc1	malá
díla	dílo	k1gNnSc2	dílo
snesla	snést	k5eAaPmAgFnS	snést
srovnání	srovnání	k1gNnSc4	srovnání
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
polonézami	polonéza	k1gFnPc7	polonéza
předních	přední	k2eAgMnPc2d1	přední
varšavských	varšavský	k2eAgMnPc2d1	varšavský
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
se	s	k7c7	s
slavnou	slavný	k2eAgFnSc7d1	slavná
polonézou	polonéza	k1gFnSc7	polonéza
Michała	Michał	k1gInSc2	Michał
Kleofase	Kleofasa	k1gFnSc3	Kleofasa
Ogińského	Ogińského	k2eAgMnPc1d1	Ogińského
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
rozvoj	rozvoj	k1gInSc1	rozvoj
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
melodické	melodický	k2eAgFnSc2d1	melodická
a	a	k8xC	a
harmonické	harmonický	k2eAgFnSc2d1	harmonická
invence	invence	k1gFnSc2	invence
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc2d1	klavírní
techniky	technika	k1gFnSc2	technika
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
Chopinově	Chopinův	k2eAgNnSc6d1	Chopinovo
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
Polonéze	polonéza	k1gFnSc6	polonéza
As	as	k1gNnSc2	as
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
věnoval	věnovat	k5eAaImAgMnS	věnovat
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
ke	k	k7c3	k
jmeninám	jmeniny	k1gFnPc3	jmeniny
svému	svůj	k3xOyFgInSc3	svůj
učiteli	učitel	k1gMnSc5	učitel
Živnému	živný	k2eAgInSc3d1	živný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
věku	věk	k1gInSc6	věk
11	[number]	k4	11
let	léto	k1gNnPc2	léto
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
před	před	k7c7	před
ruským	ruský	k2eAgMnSc7d1	ruský
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I.	I.	kA	I.
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
otevření	otevření	k1gNnSc2	otevření
varšavského	varšavský	k2eAgInSc2d1	varšavský
Sejmu	Sejm	k1gInSc2	Sejm
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
byl	být	k5eAaImAgMnS	být
Chopin	Chopin	k1gMnSc1	Chopin
schopen	schopen	k2eAgMnSc1d1	schopen
absorbovat	absorbovat	k5eAaBmF	absorbovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
je	on	k3xPp3gInPc4	on
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
projevil	projevit	k5eAaPmAgInS	projevit
výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
například	například	k6eAd1	například
překvapil	překvapit	k5eAaPmAgInS	překvapit
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
když	když	k8xS	když
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
jeho	jeho	k3xOp3gInSc4	jeho
portrét	portrét	k1gInSc4	portrét
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
zván	zvát	k5eAaImNgMnS	zvát
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
Belveder	Belveder	k1gMnSc1	Belveder
jako	jako	k8xC	jako
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
syna	syn	k1gMnSc2	syn
velkoknížete	velkokníže	k1gNnSc2wR	velkokníže
Konstantina	Konstantin	k1gMnSc2	Konstantin
Pavloviče	Pavlovič	k1gMnSc2	Pavlovič
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
často	často	k6eAd1	často
okouzloval	okouzlovat	k5eAaImAgMnS	okouzlovat
prchlivého	prchlivý	k2eAgMnSc4d1	prchlivý
velkoknížete	velkokníže	k1gMnSc4	velkokníže
svou	svůj	k3xOyFgFnSc7	svůj
klavírní	klavírní	k2eAgFnSc7d1	klavírní
hrou	hra	k1gFnSc7	hra
<g/>
.	.	kIx.	.
<g/>
Julian	Julian	k1gMnSc1	Julian
Ursyn	Ursyn	k1gMnSc1	Ursyn
Niemcewicz	Niemcewicz	k1gMnSc1	Niemcewicz
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
popularitu	popularita	k1gFnSc4	popularita
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dramatické	dramatický	k2eAgFnSc6d1	dramatická
ekloze	ekloga	k1gFnSc6	ekloga
Nasze	Nasze	k1gFnSc2	Nasze
Verkehry	Verkehra	k1gFnSc2	Verkehra
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
puberty	puberta	k1gFnSc2	puberta
<g/>
,	,	kIx,	,
během	během	k7c2	během
prázdninového	prázdninový	k2eAgInSc2d1	prázdninový
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Szafarnia	Szafarnium	k1gNnSc2	Szafarnium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
hostem	host	k1gMnSc7	host
A.	A.	kA	A.
Radziwiłła	Radziwiłł	k1gInSc2	Radziwiłł
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
motivy	motiv	k1gInPc4	motiv
často	často	k6eAd1	často
později	pozdě	k6eAd2	pozdě
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dopisech	dopis	k1gInPc6	dopis
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
prázdnin	prázdniny	k1gFnPc2	prázdniny
(	(	kIx(	(
<g/>
známé	známý	k2eAgFnSc2d1	známá
"	"	kIx"	"
<g/>
Szafarnia	Szafarnium	k1gNnPc1	Szafarnium
Courier	Courira	k1gFnPc2	Courira
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
jeho	jeho	k3xOp3gFnSc7	jeho
literární	literární	k2eAgInSc1d1	literární
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
vzdělávaný	vzdělávaný	k2eAgMnSc1d1	vzdělávaný
doma	doma	k6eAd1	doma
do	do	k7c2	do
věku	věk	k1gInSc2	věk
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
varšavské	varšavský	k2eAgNnSc4d1	Varšavské
lyceum	lyceum	k1gNnSc4	lyceum
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
u	u	k7c2	u
Živného	živný	k2eAgMnSc2d1	živný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
při	při	k7c6	při
interpretaci	interpretace	k1gFnSc6	interpretace
díla	dílo	k1gNnSc2	dílo
Ignaze	Ignaha	k1gFnSc3	Ignaha
Moschelese	Moschelesa	k1gFnSc3	Moschelesa
<g/>
,	,	kIx,	,
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
publikum	publikum	k1gNnSc4	publikum
svou	svůj	k3xOyFgFnSc7	svůj
improvizací	improvizace	k1gFnSc7	improvizace
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
"	"	kIx"	"
<g/>
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
pianistou	pianista	k1gMnSc7	pianista
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
Chopin	Chopin	k1gMnSc1	Chopin
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
tříletý	tříletý	k2eAgInSc4d1	tříletý
kurs	kurs	k1gInSc4	kurs
u	u	k7c2	u
polského	polský	k2eAgMnSc2d1	polský
skladatele	skladatel	k1gMnSc2	skladatel
Józefa	Józef	k1gMnSc2	Józef
Elsnera	Elsner	k1gMnSc2	Elsner
na	na	k7c6	na
varšavské	varšavský	k2eAgFnSc6d1	Varšavská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
součástí	součást	k1gFnSc7	součást
varšavské	varšavský	k2eAgFnSc2d1	Varšavská
university	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
Chopin	Chopin	k1gMnSc1	Chopin
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
absolvent	absolvent	k1gMnSc1	absolvent
této	tento	k3xDgFnSc2	tento
university	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
Elsnerem	Elsner	k1gMnSc7	Elsner
setkal	setkat	k5eAaPmAgMnS	setkat
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
Elsner	Elsner	k1gMnSc1	Elsner
dával	dávat	k5eAaImAgMnS	dávat
Chopinovi	Chopin	k1gMnSc3	Chopin
první	první	k4xOgFnSc2	první
rady	rada	k1gFnSc2	rada
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
Chopin	Chopin	k1gMnSc1	Chopin
oficiálně	oficiálně	k6eAd1	oficiálně
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
kursy	kurs	k1gInPc7	kurs
hudební	hudební	k2eAgFnSc2d1	hudební
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
continua	continuum	k1gNnSc2	continuum
a	a	k8xC	a
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ročním	roční	k2eAgNnSc6d1	roční
hodnocení	hodnocení	k1gNnSc6	hodnocení
Elsner	Elsnra	k1gFnPc2	Elsnra
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
značný	značný	k2eAgInSc4d1	značný
talent	talent	k1gInSc4	talent
a	a	k8xC	a
hudební	hudební	k2eAgFnSc4d1	hudební
genialitu	genialita	k1gFnSc4	genialita
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Živný	živný	k2eAgMnSc1d1	živný
i	i	k8xC	i
Elsner	Elsner	k1gMnSc1	Elsner
vypozoroval	vypozorovat	k5eAaPmAgMnS	vypozorovat
rozkvétající	rozkvétající	k2eAgInSc4d1	rozkvétající
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Elsnerův	Elsnerův	k2eAgInSc1d1	Elsnerův
styl	styl	k1gInSc1	styl
výuky	výuka	k1gFnSc2	výuka
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
neochotě	neochota	k1gFnSc6	neochota
omezovat	omezovat	k5eAaImF	omezovat
Chopina	Chopin	k1gMnSc2	Chopin
úzkoprsými	úzkoprsý	k2eAgFnPc7d1	úzkoprsá
<g/>
,	,	kIx,	,
akademickými	akademický	k2eAgInPc7d1	akademický
<g/>
,	,	kIx,	,
zastaralými	zastaralý	k2eAgNnPc7d1	zastaralé
pravidly	pravidlo	k1gNnPc7	pravidlo
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
umožnit	umožnit	k5eAaPmF	umožnit
mladému	mladý	k2eAgMnSc3d1	mladý
umělci	umělec	k1gMnSc3	umělec
dospět	dospět	k5eAaPmF	dospět
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
přirozeností	přirozenost	k1gFnSc7	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
Chopina	Chopin	k1gMnSc2	Chopin
i	i	k8xC	i
učil	učít	k5eAaPmAgMnS	učít
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
Josef	Josef	k1gMnSc1	Josef
Javůrek	Javůrek	k1gMnSc1	Javůrek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
podnájmu	podnájem	k1gInSc2	podnájem
naproti	naproti	k7c3	naproti
varšavské	varšavský	k2eAgFnSc3d1	Varšavská
universitě	universita	k1gFnSc3	universita
<g/>
,	,	kIx,	,
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
Krasińských	Krasińských	k2eAgInSc2d1	Krasińských
na	na	k7c6	na
Krakovském	krakovský	k2eAgNnSc6d1	Krakovské
předměstí	předměstí	k1gNnSc6	předměstí
č.	č.	k?	č.
5	[number]	k4	5
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
varšavská	varšavský	k2eAgFnSc1d1	Varšavská
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
opustil	opustit	k5eAaPmAgMnS	opustit
Varšavu	Varšava	k1gFnSc4	Varšava
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
zde	zde	k6eAd1	zde
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Cyprian	Cyprian	k1gMnSc1	Cyprian
Norwid	Norwid	k1gInSc4	Norwid
studoval	studovat	k5eAaImAgMnS	studovat
malířství	malířství	k1gNnSc4	malířství
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgMnS	napsat
poému	poéma	k1gFnSc4	poéma
"	"	kIx"	"
<g/>
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
klavír	klavír	k1gInSc4	klavír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c6	o
ruských	ruský	k2eAgMnPc6d1	ruský
vojácích	voják	k1gMnPc6	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
nástroj	nástroj	k1gInSc4	nástroj
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1828	[number]	k4	1828
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
rodinného	rodinný	k2eAgMnSc2d1	rodinný
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
zoologa	zoolog	k1gMnSc2	zoolog
Felikse	Feliks	k1gMnSc2	Feliks
Jarockého	Jarocký	k1gMnSc2	Jarocký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
vědecké	vědecký	k2eAgFnSc2d1	vědecká
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
představení	představení	k1gNnSc1	představení
několika	několik	k4yIc2	několik
oper	opera	k1gFnPc2	opera
režírovaných	režírovaný	k2eAgFnPc2d1	režírovaná
Gaspare	Gaspar	k1gMnSc5	Gaspar
Spontinim	Spontinim	k1gInSc4	Spontinim
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
Carla	Carl	k1gMnSc4	Carl
Friedricha	Friedrich	k1gMnSc4	Friedrich
Zeltera	Zelter	k1gMnSc4	Zelter
<g/>
,	,	kIx,	,
Felixe	Felix	k1gMnSc4	Felix
Mendelssohna	Mendelssohn	k1gInSc2	Mendelssohn
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
celebrity	celebrita	k1gFnSc2	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
byl	být	k5eAaImAgInS	být
hostem	host	k1gMnSc7	host
knížete	kníže	k1gMnSc2	kníže
Radziwiłła	Radziwiłłus	k1gMnSc2	Radziwiłłus
<g/>
,	,	kIx,	,
guvernéra	guvernér	k1gMnSc2	guvernér
Velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
poznaňského	poznaňský	k2eAgNnSc2d1	poznaňské
<g/>
,	,	kIx,	,
uznávaného	uznávaný	k2eAgMnSc2d1	uznávaný
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
snaživého	snaživý	k2eAgMnSc2d1	snaživý
violoncellisty	violoncellista	k1gMnSc2	violoncellista
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knížete	kníže	k1gMnSc4	kníže
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
Wandu	Wanda	k1gFnSc4	Wanda
složil	složit	k5eAaPmAgMnS	složit
Polonézu	polonéza	k1gFnSc4	polonéza
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
C	C	kA	C
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
slyšel	slyšet	k5eAaImAgMnS	slyšet
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
Paganiniho	Paganini	k1gMnSc2	Paganini
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
klavíristou	klavírista	k1gMnSc7	klavírista
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
Johannem	Johann	k1gMnSc7	Johann
Nepomukem	Nepomuk	k1gMnSc7	Nepomuk
Hummelem	Hummel	k1gMnSc7	Hummel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
varšavské	varšavský	k2eAgFnSc6d1	Varšavská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
debut	debut	k1gInSc1	debut
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
odehrál	odehrát	k5eAaPmAgInS	odehrát
dva	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
příznivých	příznivý	k2eAgFnPc2d1	příznivá
kritik	kritika	k1gFnPc2	kritika
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
některými	některý	k3yIgFnPc7	některý
vyčítán	vyčítán	k2eAgInSc4d1	vyčítán
slabý	slabý	k2eAgInSc4d1	slabý
úhoz	úhoz	k1gInSc4	úhoz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
houslistou	houslista	k1gMnSc7	houslista
Josefem	Josef	k1gMnSc7	Josef
Slavíkem	Slavík	k1gMnSc7	Slavík
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
koncert	koncert	k1gInSc1	koncert
konaný	konaný	k2eAgInSc1d1	konaný
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1829	[number]	k4	1829
ve	v	k7c6	v
varšavském	varšavský	k2eAgInSc6d1	varšavský
Kupecké	kupecký	k2eAgFnSc6d1	kupecká
besedě	beseda	k1gFnSc6	beseda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
pěvce	pěvec	k1gMnPc4	pěvec
a	a	k8xC	a
improvizoval	improvizovat	k5eAaImAgMnS	improvizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednesl	přednést	k5eAaPmAgMnS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
Klavírní	klavírní	k2eAgInSc4d1	klavírní
koncert	koncert	k1gInSc4	koncert
č.	č.	k?	č.
2	[number]	k4	2
f	f	k?	f
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
etudu	etuda	k1gFnSc4	etuda
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
polský	polský	k2eAgMnSc1d1	polský
malíř	malíř	k1gMnSc1	malíř
Ambroży	Ambroża	k1gMnSc2	Ambroża
Mieroszewski	Mieroszewski	k1gNnSc2	Mieroszewski
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sadu	sada	k1gFnSc4	sada
portrétů	portrét	k1gInPc2	portrét
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Emilie	Emilie	k1gFnSc2	Emilie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnPc1d2	starší
sestry	sestra	k1gFnPc1	sestra
Ludwiky	Ludwika	k1gFnSc2	Ludwika
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc2d2	mladší
sestry	sestra	k1gFnSc2	sestra
Izabely	Izabela	k1gFnSc2	Izabela
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Fryderyka	Fryderyka	k1gFnSc1	Fryderyka
<g/>
.	.	kIx.	.
</s>
<s>
Originály	originál	k1gInPc1	originál
však	však	k9	však
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zachovaly	zachovat	k5eAaPmAgInP	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
černobílé	černobílý	k2eAgFnPc1d1	černobílá
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Emilie	Emilie	k1gFnSc1	Emilie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
14	[number]	k4	14
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
otec	otec	k1gMnSc1	otec
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
studentky	studentka	k1gFnSc2	studentka
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
,	,	kIx,	,
Konstancie	Konstancie	k1gFnSc2	Konstancie
Gładkowské	Gładkowská	k1gFnSc2	Gładkowská
<g/>
.	.	kIx.	.
</s>
<s>
Bál	bát	k5eAaImAgMnS	bát
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
své	svůj	k3xOyFgInPc4	svůj
city	cit	k1gInPc4	cit
vyjevit	vyjevit	k5eAaPmF	vyjevit
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
však	však	k9	však
vtělil	vtělit	k5eAaPmAgInS	vtělit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
věty	věta	k1gFnSc2	věta
svého	svůj	k3xOyFgInSc2	svůj
druhého	druhý	k4xOgInSc2	druhý
klavírního	klavírní	k2eAgInSc2d1	klavírní
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
dočíst	dočíst	k5eAaPmF	dočíst
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
jeho	jeho	k3xOp3gMnSc3	jeho
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
Tytu	tyt	k2eAgFnSc4d1	Tyta
Woyciechowskému	Woyciechowské	k1gNnSc3	Woyciechowské
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
lásky	láska	k1gFnSc2	láska
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vyznal	vyznat	k5eAaPmAgMnS	vyznat
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Domluvili	domluvit	k5eAaPmAgMnP	domluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
psát	psát	k5eAaImF	psát
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
udržovat	udržovat	k5eAaImF	udržovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
asi	asi	k9	asi
rok	rok	k1gInSc4	rok
tato	tento	k3xDgFnSc1	tento
láska	láska	k1gFnSc1	láska
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
nevydržela	vydržet	k5eNaPmAgFnS	vydržet
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Konstancie	Konstancie	k1gFnSc1	Konstancie
provdala	provdat	k5eAaPmAgFnS	provdat
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
za	za	k7c4	za
jiného	jiný	k2eAgMnSc4d1	jiný
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
mnohým	mnohý	k2eAgFnPc3d1	mnohá
spekulacím	spekulace	k1gFnPc3	spekulace
však	však	k9	však
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
nikdy	nikdy	k6eAd1	nikdy
později	pozdě	k6eAd2	pozdě
nelitovala	litovat	k5eNaImAgFnS	litovat
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
jako	jako	k8xC	jako
interpreta	interpret	k1gMnSc2	interpret
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
mu	on	k3xPp3gMnSc3	on
otevřely	otevřít	k5eAaPmAgFnP	otevřít
dveře	dveře	k1gFnPc1	dveře
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
koncert	koncert	k1gInSc1	koncert
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgMnSc6	který
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
premiérován	premiérován	k2eAgInSc1d1	premiérován
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
1	[number]	k4	1
e	e	k0	e
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
za	za	k7c2	za
zraků	zrak	k1gInPc2	zrak
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
prstenem	prsten	k1gInSc7	prsten
od	od	k7c2	od
Konstancie	Konstancie	k1gFnSc2	Konstancie
Gładkowské	Gładkowská	k1gFnSc2	Gładkowská
na	na	k7c6	na
prstu	prst	k1gInSc6	prst
<g/>
,	,	kIx,	,
drže	držet	k5eAaImSgInS	držet
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
pohár	pohár	k1gInSc4	pohár
s	s	k7c7	s
prstí	prsť	k1gFnSc7	prsť
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Chopin	Chopin	k1gMnSc1	Chopin
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1830	[number]	k4	1830
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
Listopadové	listopadový	k2eAgNnSc1d1	listopadové
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
ruské	ruský	k2eAgFnSc3d1	ruská
nadvládě	nadvláda	k1gFnSc3	nadvláda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
spolucestující	spolucestující	k1gMnSc1	spolucestující
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
továrník	továrník	k1gMnSc1	továrník
a	a	k8xC	a
patron	patron	k1gMnSc1	patron
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
Tytus	Tytus	k1gMnSc1	Tytus
Woyciechowski	Woyciechowsk	k1gFnSc2	Woyciechowsk
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
se	se	k3xPyFc4	se
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
však	však	k9	však
již	již	k6eAd1	již
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
křehkost	křehkost	k1gFnSc4	křehkost
bojovat	bojovat	k5eAaImF	bojovat
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gNnSc4	on
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
jeho	jeho	k3xOp3gMnPc1	jeho
blízcí	blízký	k2eAgMnPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
Jachimecki	Jachimeck	k1gFnSc2	Jachimeck
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
zarmoucen	zarmoutit	k5eAaPmNgMnS	zarmoutit
nostalgií	nostalgie	k1gFnSc7	nostalgie
<g/>
,	,	kIx,	,
zklamán	zklamán	k2eAgMnSc1d1	zklamán
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
nadějích	naděje	k1gFnPc6	naděje
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
zralý	zralý	k2eAgInSc4d1	zralý
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
duchovní	duchovní	k2eAgFnPc1d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
romantického	romantický	k2eAgMnSc2d1	romantický
básníka	básník	k1gMnSc2	básník
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c4	v
národního	národní	k2eAgMnSc4d1	národní
barda	bard	k1gMnSc4	bard
<g/>
,	,	kIx,	,
uvědomujícího	uvědomující	k2eAgMnSc4d1	uvědomující
si	se	k3xPyFc3	se
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc4	přítomnost
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc4	budoucnost
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
<g/>
,	,	kIx,	,
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgMnS	vidět
Polsko	Polsko	k1gNnSc4	Polsko
z	z	k7c2	z
nadhledu	nadhled	k1gInSc2	nadhled
a	a	k8xC	a
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
velké	velký	k2eAgNnSc1d1	velké
a	a	k8xC	a
krásné	krásný	k2eAgNnSc1d1	krásné
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
jeho	jeho	k3xOp3gFnSc4	jeho
tragédii	tragédie	k1gFnSc4	tragédie
a	a	k8xC	a
heroismus	heroismus	k1gInSc4	heroismus
ve	v	k7c6	v
víru	vír	k1gInSc6	vír
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
září	září	k1gNnSc6	září
1831	[number]	k4	1831
poraženo	poražen	k2eAgNnSc1d1	poraženo
<g/>
,	,	kIx,	,
vylil	vylít	k5eAaPmAgMnS	vylít
si	se	k3xPyFc3	se
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
cestující	cestující	k1gMnSc1	cestující
právě	právě	k9	právě
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
zlost	zlost	k1gFnSc4	zlost
v	v	k7c6	v
mateřském	mateřský	k2eAgInSc6d1	mateřský
jazyce	jazyk	k1gInSc6	jazyk
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
skrýval	skrývat	k5eAaImAgInS	skrývat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výlevy	výlev	k1gInPc1	výlev
zkroušeného	zkroušený	k2eAgNnSc2d1	zkroušené
srdce	srdce	k1gNnSc2	srdce
nalezly	nalézt	k5eAaBmAgFnP	nalézt
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
svou	svůj	k3xOyFgFnSc4	svůj
odezvu	odezva	k1gFnSc4	odezva
v	v	k7c6	v
Scherzu	scherzo	k1gNnSc6	scherzo
č.	č.	k?	č.
1	[number]	k4	1
h	h	k?	h
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Revoluční	revoluční	k2eAgFnSc3d1	revoluční
etudě	etuda	k1gFnSc3	etuda
<g/>
"	"	kIx"	"
c	c	k0	c
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
č.	č.	k?	č.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paříž	Paříž	k1gFnSc1	Paříž
===	===	k?	===
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	zářit	k5eAaImIp3nS	zářit
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
usnadnil	usnadnit	k5eAaPmAgInS	usnadnit
svůj	svůj	k3xOyFgInSc4	svůj
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
hudební	hudební	k2eAgFnSc2d1	hudební
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bral	brát	k5eAaImAgMnS	brát
lekce	lekce	k1gFnPc4	lekce
u	u	k7c2	u
známého	známý	k1gMnSc2	známý
klavíristy	klavírista	k1gMnSc2	klavírista
Friedricha	Friedrich	k1gMnSc2	Friedrich
Kalkbrennera	Kalkbrenner	k1gMnSc2	Kalkbrenner
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
ale	ale	k8xC	ale
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
upustil	upustit	k5eAaPmAgMnS	upustit
pro	pro	k7c4	pro
neslučitelnost	neslučitelnost	k1gFnSc4	neslučitelnost
jejich	jejich	k3xOp3gInSc2	jejich
kompozičního	kompoziční	k2eAgInSc2d1	kompoziční
a	a	k8xC	a
hráčského	hráčský	k2eAgInSc2d1	hráčský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1832	[number]	k4	1832
Chopin	Chopin	k1gMnSc1	Chopin
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
všeobecným	všeobecný	k2eAgInSc7d1	všeobecný
obdivem	obdiv	k1gInSc7	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Vlivný	vlivný	k2eAgMnSc1d1	vlivný
muzikolog	muzikolog	k1gMnSc1	muzikolog
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
François-Joseph	François-Joseph	k1gMnSc1	François-Joseph
Fétis	Fétis	k1gInSc4	Fétis
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
Revue	revue	k1gFnSc6	revue
musicale	musical	k1gInSc5	musical
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
–	–	k?	–
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
kráčel	kráčet	k5eAaImAgMnS	kráčet
po	po	k7c6	po
vyšlapaných	vyšlapaný	k2eAgFnPc6d1	vyšlapaná
cestách	cesta	k1gFnPc6	cesta
–	–	k?	–
přinesl	přinést	k5eAaPmAgInS	přinést
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
naprostou	naprostý	k2eAgFnSc4d1	naprostá
obrodu	obroda	k1gFnSc4	obroda
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bezpochyby	bezpochyby	k6eAd1	bezpochyby
část	část	k1gFnSc4	část
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
o	o	k7c6	o
co	co	k9	co
jiní	jiný	k2eAgMnPc1d1	jiný
<g />
.	.	kIx.	.
</s>
<s>
dlouho	dlouho	k6eAd1	dlouho
marně	marně	k6eAd1	marně
usilovali	usilovat	k5eAaImAgMnP	usilovat
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
výstřednost	výstřednost	k1gFnSc1	výstřednost
originálních	originální	k2eAgFnPc2d1	originální
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nevídaných	vídaný	k2eNgMnPc2d1	nevídaný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
na	na	k7c4	na
Chopinovy	Chopinův	k2eAgFnPc4d1	Chopinova
Variace	variace	k1gFnPc4	variace
na	na	k7c4	na
"	"	kIx"	"
<g/>
La	la	k1gNnSc4	la
ci	ci	k0	ci
darem	dar	k1gInSc7	dar
la	la	k0	la
mano	mana	k1gFnSc5	mana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
z	z	k7c2	z
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
opery	opera	k1gFnSc2	opera
Don	Don	k1gMnSc1	Don
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Klobouk	klobouk	k1gInSc1	klobouk
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
pánové	pán	k1gMnPc1	pán
<g/>
!	!	kIx.	!
</s>
<s>
Génius	génius	k1gMnSc1	génius
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Po	po	k7c6	po
pařížském	pařížský	k2eAgInSc6d1	pařížský
koncertním	koncertní	k2eAgInSc6d1	koncertní
debutu	debut	k1gInSc6	debut
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1832	[number]	k4	1832
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Chopin	Chopin	k1gMnSc1	Chopin
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
hra	hra	k1gFnSc1	hra
lehkými	lehký	k2eAgInPc7d1	lehký
úhozy	úhoz	k1gInPc7	úhoz
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
velké	velký	k2eAgInPc4d1	velký
koncertní	koncertní	k2eAgInPc4d1	koncertní
sály	sál	k1gInPc4	sál
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Rothschildů	Rothschild	k1gMnPc2	Rothschild
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
patronát	patronát	k1gInSc4	patronát
mu	on	k3xPp3gMnSc3	on
otevřel	otevřít	k5eAaPmAgMnS	otevřít
dveře	dveře	k1gFnPc4	dveře
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
soukromých	soukromý	k2eAgInPc2d1	soukromý
salónů	salón	k1gInPc2	salón
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
našel	najít	k5eAaPmAgMnS	najít
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
společnost	společnost	k1gFnSc4	společnost
i	i	k8xC	i
příležitost	příležitost	k1gFnSc4	příležitost
uplatnit	uplatnit	k5eAaPmF	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
celebritou	celebrita	k1gFnSc7	celebrita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
slušně	slušně	k6eAd1	slušně
vydělávat	vydělávat	k5eAaImF	vydělávat
výukou	výuka	k1gFnSc7	výuka
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
plejádou	plejáda	k1gFnSc7	plejáda
slavných	slavný	k2eAgNnPc2d1	slavné
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
,	,	kIx,	,
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Hiller	Hiller	k1gMnSc1	Hiller
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gFnPc1	Eugè
Delacroix	Delacroix	k1gInSc1	Delacroix
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Adam	Adam	k1gMnSc1	Adam
Jerzy	Jerza	k1gFnSc2	Jerza
Czartoryski	Czartorysk	k1gFnSc2	Czartorysk
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
de	de	k?	de
Vigny	Vigna	k1gFnSc2	Vigna
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Valentin	Valentin	k1gMnSc1	Valentin
Alkan	Alkan	k1gMnSc1	Alkan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zřídka	zřídka	k6eAd1	zřídka
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
pořádal	pořádat	k5eAaImAgInS	pořádat
ojedinělé	ojedinělý	k2eAgInPc4d1	ojedinělý
výroční	výroční	k2eAgInPc4d1	výroční
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Salle	Sall	k1gInSc6	Sall
Pleyel	Pleyela	k1gFnPc2	Pleyela
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
asi	asi	k9	asi
300	[number]	k4	300
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
hrával	hrávat	k5eAaImAgMnS	hrávat
v	v	k7c6	v
salónech	salón	k1gInPc6	salón
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
elity	elita	k1gFnSc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
však	však	k9	však
preferoval	preferovat	k5eAaImAgInS	preferovat
hru	hra	k1gFnSc4	hra
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vlastním	vlastní	k2eAgNnSc6d1	vlastní
apartmá	apartmá	k1gNnSc6	apartmá
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
skupinky	skupinka	k1gFnPc4	skupinka
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
chatrné	chatrný	k2eAgNnSc1d1	chatrné
zdraví	zdraví	k1gNnSc1	zdraví
ho	on	k3xPp3gMnSc4	on
odrazovalo	odrazovat	k5eAaImAgNnS	odrazovat
od	od	k7c2	od
pořádání	pořádání	k1gNnSc2	pořádání
turné	turné	k1gNnSc2	turné
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimečně	výjimečně	k6eAd1	výjimečně
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
opouštěl	opouštět	k5eAaImAgMnS	opouštět
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vysoký	vysoký	k2eAgInSc1d1	vysoký
příjem	příjem	k1gInSc1	příjem
za	za	k7c4	za
vyučování	vyučování	k1gNnSc4	vyučování
a	a	k8xC	a
kompozice	kompozice	k1gFnSc2	kompozice
ho	on	k3xPp3gMnSc4	on
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
od	od	k7c2	od
nutnosti	nutnost	k1gFnSc2	nutnost
koncertního	koncertní	k2eAgNnSc2d1	koncertní
vystupování	vystupování	k1gNnSc2	vystupování
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
měl	mít	k5eAaImAgMnS	mít
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
nechuť	nechuť	k1gFnSc4	nechuť
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Hedley	Hedlea	k1gFnSc2	Hedlea
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
pianista	pianista	k1gMnSc1	pianista
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
reputaci	reputace	k1gFnSc4	reputace
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
na	na	k7c6	na
základě	základ	k1gInSc6	základ
minimálního	minimální	k2eAgInSc2d1	minimální
počtu	počet	k1gInSc2	počet
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vystoupení	vystoupení	k1gNnPc2	vystoupení
–	–	k?	–
asi	asi	k9	asi
30	[number]	k4	30
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
navštívil	navštívit	k5eAaPmAgInS	navštívit
Karlovy	Karlův	k2eAgInPc4d1	Karlův
Vary	Vary	k1gInPc4	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Sasko	Sasko	k1gNnSc4	Sasko
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
starým	starý	k1gMnSc7	starý
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
Wodzińskim	Wodzińskima	k1gFnPc2	Wodzińskima
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
potkal	potkat	k5eAaPmAgInS	potkat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
Marií	Maria	k1gFnPc2	Maria
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
šestnáctiletou	šestnáctiletý	k2eAgFnSc4d1	šestnáctiletá
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
jako	jako	k9	jako
do	do	k7c2	do
okouzlující	okouzlující	k2eAgFnSc2d1	okouzlující
<g/>
,	,	kIx,	,
umělecky	umělecky	k6eAd1	umělecky
nadané	nadaný	k2eAgFnPc4d1	nadaná
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgFnPc4d1	inteligentní
mladé	mladý	k2eAgFnPc4d1	mladá
dámy	dáma	k1gFnPc4	dáma
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
přes	přes	k7c4	přes
Drážďany	Drážďany	k1gInPc4	Drážďany
z	z	k7c2	z
dovolené	dovolená	k1gFnSc2	dovolená
strávené	strávený	k2eAgInPc1d1	strávený
s	s	k7c7	s
Wodzińskými	Wodzińská	k1gFnPc7	Wodzińská
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Marii	Maria	k1gFnSc3	Maria
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
přijala	přijmout	k5eAaPmAgFnS	přijmout
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Wodzińská	Wodzińská	k1gFnSc1	Wodzińská
<g/>
,	,	kIx,	,
svolila	svolit	k5eAaPmAgFnS	svolit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mariin	Mariin	k2eAgInSc4d1	Mariin
nízký	nízký	k2eAgInSc4d1	nízký
věk	věk	k1gInSc4	věk
a	a	k8xC	a
Chopinovo	Chopinův	k2eAgNnSc4d1	Chopinovo
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
(	(	kIx(	(
<g/>
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1835	[number]	k4	1835
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Varšavou	Varšava	k1gFnSc7	Varšava
šířila	šířit	k5eAaImAgFnS	šířit
fáma	fáma	k1gFnSc1	fáma
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
odsunuly	odsunout	k5eAaPmAgInP	odsunout
sňatek	sňatek	k1gInSc4	sňatek
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
Zasnoubení	zasnoubení	k1gNnSc1	zasnoubení
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neveřejným	veřejný	k2eNgInSc7d1	neveřejný
a	a	k8xC	a
sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vložil	vložit	k5eAaPmAgMnS	vložit
Chopin	Chopin	k1gMnSc1	Chopin
korespondenci	korespondence	k1gFnSc4	korespondence
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
obálky	obálka	k1gFnSc2	obálka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
polsky	polsky	k6eAd1	polsky
nadepsal	nadepsat	k5eAaPmAgInS	nadepsat
"	"	kIx"	"
<g/>
Moja	Moja	k?	Moja
bieda	bieda	k1gFnSc1	bieda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
žal	žal	k1gInSc1	žal
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
city	city	k1gFnSc1	city
k	k	k7c3	k
Marii	Maria	k1gFnSc3	Maria
zanechaly	zanechat	k5eAaPmAgInP	zanechat
stopy	stop	k1gInPc1	stop
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
Valčíku	valčík	k1gInSc6	valčík
As	as	k9	as
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
69	[number]	k4	69
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
zvaném	zvaný	k2eAgInSc6d1	zvaný
"	"	kIx"	"
<g/>
Valse	Vals	k1gInSc6	Vals
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
adieu	adieu	k0	adieu
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Valčík	valčík	k1gInSc1	valčík
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsaném	napsaný	k2eAgNnSc6d1	napsané
ráno	ráno	k6eAd1	ráno
v	v	k7c4	v
den	den	k1gInSc4	den
odjezdu	odjezd	k1gInSc2	odjezd
z	z	k7c2	z
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
složil	složit	k5eAaPmAgMnS	složit
Etudu	etuda	k1gFnSc4	etuda
f	f	k?	f
moll	moll	k1gNnSc2	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
25	[number]	k4	25
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
portrét	portrét	k1gInSc1	portrét
Mariiny	Mariin	k2eAgFnSc2d1	Mariina
duše	duše	k1gFnSc2	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
poslal	poslat	k5eAaPmAgMnS	poslat
Marii	Maria	k1gFnSc4	Maria
sedm	sedm	k4xCc1	sedm
písní	píseň	k1gFnPc2	píseň
na	na	k7c4	na
verše	verš	k1gInPc4	verš
polských	polský	k2eAgMnPc2d1	polský
romantických	romantický	k2eAgMnPc2d1	romantický
básníků	básník	k1gMnPc2	básník
Stefana	Stefan	k1gMnSc2	Stefan
Witwického	Witwický	k2eAgMnSc2d1	Witwický
<g/>
,	,	kIx,	,
Józefa	Józef	k1gMnSc2	Józef
Zaleského	Zaleský	k2eAgMnSc2d1	Zaleský
a	a	k8xC	a
Adama	Adam	k1gMnSc2	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
krachu	krach	k1gInSc6	krach
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
sňatek	sňatek	k1gInSc4	sňatek
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
polská	polský	k2eAgFnSc1d1	polská
hraběnka	hraběnka	k1gFnSc1	hraběnka
Delfina	Delfina	k1gFnSc1	Delfina
Potocka	Potocka	k1gFnSc1	Potocka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Chopinovou	Chopinův	k2eAgFnSc7d1	Chopinova
múzou	múza	k1gFnSc7	múza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
Valčík	valčík	k1gInSc1	valčík
Des	des	k1gNnSc2	des
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
64	[number]	k4	64
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
"	"	kIx"	"
<g/>
Minutový	minutový	k2eAgInSc1d1	minutový
valčík	valčík	k1gInSc1	valčík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
některých	některý	k3yIgInPc2	některý
společných	společný	k2eAgInPc2d1	společný
veřejných	veřejný	k2eAgInPc2d1	veřejný
koncertů	koncert	k1gInPc2	koncert
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
významnými	významný	k2eAgMnPc7d1	významný
hudebníky	hudebník	k1gMnPc7	hudebník
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
George	Georg	k1gMnSc2	Georg
Sandová	Sandová	k1gFnSc1	Sandová
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
na	na	k7c6	na
večírku	večírek	k1gInSc6	večírek
pořádaném	pořádaný	k2eAgInSc6d1	pořádaný
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Marie	Maria	k1gFnSc2	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoultovou	Agoultová	k1gFnSc7	Agoultová
<g/>
,	,	kIx,	,
milenkou	milenka	k1gFnSc7	milenka
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
Ference	Ferenc	k1gMnSc2	Ferenc
Liszta	Liszt	k1gMnSc2	Liszt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
a	a	k8xC	a
feministkou	feministka	k1gFnSc7	feministka
Amandine	Amandin	k1gInSc5	Amandin
Aurore	Auror	k1gInSc5	Auror
Lucille	Lucille	k1gNnSc1	Lucille
Dupinovou	Dupinová	k1gFnSc7	Dupinová
<g/>
,	,	kIx,	,
baronkou	baronka	k1gFnSc7	baronka
Dudevant	Dudevanta	k1gFnPc2	Dudevanta
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
George	Georg	k1gFnSc2	Georg
Sandová	Sandová	k1gFnSc1	Sandová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Sandové	Sandový	k2eAgMnPc4d1	Sandový
dřívější	dřívější	k2eAgMnPc4d1	dřívější
partnery	partner	k1gMnPc4	partner
patřili	patřit	k5eAaImAgMnP	patřit
Jules	Jules	k1gInSc4	Jules
Sandeau	Sandeaus	k1gInSc2	Sandeaus
<g/>
,	,	kIx,	,
Prosper	Prosper	k1gMnSc1	Prosper
Mérimée	Mérimé	k1gFnSc2	Mérimé
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
de	de	k?	de
Musset	Musset	k1gMnSc1	Musset
<g/>
,	,	kIx,	,
Louis-Chrystosome	Louis-Chrystosom	k1gInSc5	Louis-Chrystosom
Michel	Michel	k1gMnSc1	Michel
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Didier	Didier	k1gMnSc1	Didier
<g/>
,	,	kIx,	,
Pierre-François	Pierre-François	k1gFnSc1	Pierre-François
Bocage	Bocage	k1gFnSc1	Bocage
a	a	k8xC	a
Félicien	Félicien	k2eAgInSc1d1	Félicien
Mallefille	Mallefille	k1gInSc1	Mallefille
<g/>
.	.	kIx.	.
<g/>
Původně	původně	k6eAd1	původně
cítil	cítit	k5eAaImAgMnS	cítit
k	k	k7c3	k
Sandové	Sandový	k2eAgFnSc3d1	Sandový
averzi	averze	k1gFnSc3	averze
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
Hillerovi	Hiller	k1gMnSc3	Hiller
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sandová	Sandová	k1gFnSc1	Sandová
je	být	k5eAaImIp3nS	být
nesympatická	sympatický	k2eNgFnSc1d1	nesympatická
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vůbec	vůbec	k9	vůbec
žena	žena	k1gFnSc1	žena
<g/>
?	?	kIx.	?
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
jisté	jistý	k2eAgNnSc1d1	jisté
podezření	podezření	k1gNnSc1	podezření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sandová	Sandová	k1gFnSc1	Sandová
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
dvaatřicetistránkovém	dvaatřicetistránkový	k2eAgInSc6d1	dvaatřicetistránkový
dopise	dopis	k1gInSc6	dopis
hraběti	hrabě	k1gMnSc3	hrabě
Wojciechu	Wojciech	k1gMnSc3	Wojciech
Grzymałovi	Grzymała	k1gMnSc3	Grzymała
<g/>
,	,	kIx,	,
příteli	přítel	k1gMnSc3	přítel
jak	jak	k8xS	jak
svému	svůj	k1gMnSc3	svůj
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
<g/>
,	,	kIx,	,
vyznala	vyznat	k5eAaBmAgFnS	vyznat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
citů	cit	k1gInPc2	cit
ke	k	k7c3	k
skladateli	skladatel	k1gMnSc3	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
ukončit	ukončit	k5eAaPmF	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
současný	současný	k2eAgInSc4d1	současný
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vztahu	vztah	k1gInSc3	vztah
se	s	k7c7	s
Chopinem	Chopin	k1gMnSc7	Chopin
nestálo	stát	k5eNaImAgNnS	stát
nic	nic	k3yNnSc1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Sandovou	Sandová	k1gFnSc4	Sandová
stal	stát	k5eAaPmAgInS	stát
veřejným	veřejný	k2eAgNnSc7d1	veřejné
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
také	také	k9	také
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Eugè	Eugè	k1gFnSc3	Eugè
Delacroix	Delacroix	k1gInSc4	Delacroix
známý	známý	k2eAgInSc4d1	známý
společný	společný	k2eAgInSc4d1	společný
portrét	portrét	k1gInSc4	portrét
Chopina	Chopin	k1gMnSc2	Chopin
se	s	k7c7	s
Sandovou	Sandová	k1gFnSc7	Sandová
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
Chopinův	Chopinův	k2eAgMnSc1d1	Chopinův
historik	historik	k1gMnSc1	historik
Ganche	Ganch	k1gFnSc2	Ganch
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vzhled	vzhled	k1gInSc4	vzhled
křečovitý	křečovitý	k2eAgInSc4d1	křečovitý
bolestí	bolest	k1gFnSc7	bolest
nese	nést	k5eAaImIp3nS	nést
známky	známka	k1gFnPc4	známka
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Chopina	Chopin	k1gMnSc2	Chopin
podlomila	podlomit	k5eAaPmAgFnS	podlomit
<g/>
,	,	kIx,	,
a	a	k8xC	a
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
všechny	všechen	k3xTgInPc4	všechen
nepokoje	nepokoj	k1gInPc4	nepokoj
jeho	jeho	k3xOp3gFnSc2	jeho
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1838	[number]	k4	1838
a	a	k8xC	a
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Chopin	Chopin	k1gMnSc1	Chopin
se	s	k7c7	s
Sandovou	Sandová	k1gFnSc7	Sandová
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
dvěma	dva	k4xCgFnPc7	dva
děti	dítě	k1gFnPc4	dítě
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Mallorca	Mallorca	k1gMnSc1	Mallorca
<g/>
,	,	kIx,	,
doufaje	doufat	k5eAaImSgMnS	doufat
ve	v	k7c4	v
zlepšení	zlepšení	k1gNnSc4	zlepšení
svého	svůj	k3xOyFgNnSc2	svůj
zdraví	zdraví	k1gNnSc2	zdraví
díky	díky	k7c3	díky
zdejšímu	zdejší	k2eAgNnSc3d1	zdejší
podnebí	podnebí	k1gNnSc3	podnebí
(	(	kIx(	(
<g/>
Sandová	Sandová	k1gFnSc1	Sandová
tento	tento	k3xDgInSc4	tento
pobyt	pobyt	k1gInSc4	pobyt
líčí	líčit	k5eAaImIp3nS	líčit
v	v	k7c6	v
Zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
příznivý	příznivý	k2eAgInSc1d1	příznivý
<g/>
,	,	kIx,	,
zima	zima	k6eAd1	zima
byla	být	k5eAaImAgFnS	být
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
i	i	k9	i
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
odmítali	odmítat	k5eAaImAgMnP	odmítat
poskytnout	poskytnout	k5eAaPmF	poskytnout
ubytování	ubytování	k1gNnSc4	ubytování
Chopinovi	Chopin	k1gMnSc3	Chopin
<g/>
,	,	kIx,	,
sužovanému	sužovaný	k2eAgMnSc3d1	sužovaný
počínající	počínající	k2eAgInSc4d1	počínající
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
našli	najít	k5eAaPmAgMnP	najít
přístřeší	přístřeší	k1gNnSc4	přístřeší
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
kartuziánském	kartuziánský	k2eAgInSc6d1	kartuziánský
klášteře	klášter	k1gInSc6	klášter
Valldemossa	Valldemossa	k1gFnSc1	Valldemossa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navíc	navíc	k6eAd1	navíc
měl	mít	k5eAaImAgInS	mít
problém	problém	k1gInSc1	problém
se	s	k7c7	s
zasláním	zaslání	k1gNnSc7	zaslání
svého	svůj	k3xOyFgInSc2	svůj
klavíru	klavír	k1gInSc2	klavír
značky	značka	k1gFnSc2	značka
Pleyel	Pleyela	k1gFnPc2	Pleyela
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dorazil	dorazit	k5eAaPmAgMnS	dorazit
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
na	na	k7c4	na
celnici	celnice	k1gFnSc4	celnice
(	(	kIx(	(
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
klavír	klavír	k1gInSc1	klavír
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
celnici	celnice	k1gFnSc6	celnice
již	již	k6eAd1	již
osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
<g/>
...	...	k?	...
Požadují	požadovat	k5eAaImIp3nP	požadovat
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemám	mít	k5eNaImIp1nS	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Mezitím	mezitím	k6eAd1	mezitím
si	se	k3xPyFc3	se
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
starý	starý	k2eAgInSc4d1	starý
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
cvičil	cvičit	k5eAaImAgMnS	cvičit
a	a	k8xC	a
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
několik	několik	k4yIc4	několik
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
si	se	k3xPyFc3	se
postěžoval	postěžovat	k5eAaPmAgMnS	postěžovat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
mizerné	mizerný	k2eAgNnSc4d1	mizerné
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
na	na	k7c4	na
nekompetentnost	nekompetentnost	k1gFnSc4	nekompetentnost
místních	místní	k2eAgMnPc2d1	místní
lékařů	lékař	k1gMnPc2	lékař
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
nemocen	nemocen	k2eAgMnSc1d1	nemocen
jako	jako	k9	jako
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Navštívili	navštívit	k5eAaPmAgMnP	navštívit
mne	já	k3xPp1nSc4	já
tři	tři	k4xCgMnPc1	tři
lékaři	lékař	k1gMnPc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
umírám	umírat	k5eAaImIp1nS	umírat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
že	že	k8xS	že
dýchám	dýchat	k5eAaImIp1nS	dýchat
své	svůj	k3xOyFgInPc4	svůj
poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1839	[number]	k4	1839
George	George	k1gFnSc1	George
Sandová	Sandová	k1gFnSc1	Sandová
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
300	[number]	k4	300
franků	frank	k1gInPc2	frank
(	(	kIx(	(
<g/>
polovinu	polovina	k1gFnSc4	polovina
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
částky	částka	k1gFnSc2	částka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
klavír	klavír	k1gInSc4	klavír
z	z	k7c2	z
celního	celní	k2eAgInSc2d1	celní
skladu	sklad	k1gInSc2	sklad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Chopinovi	Chopin	k1gMnSc3	Chopin
klavír	klavír	k1gInSc4	klavír
dorazil	dorazit	k5eAaPmAgMnS	dorazit
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mohl	moct	k5eAaImAgInS	moct
využívat	využívat	k5eAaPmF	využívat
dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
nástroj	nástroj	k1gInSc4	nástroj
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
některých	některý	k3yIgFnPc2	některý
skladeb	skladba	k1gFnPc2	skladba
<g/>
:	:	kIx,	:
některá	některý	k3yIgNnPc1	některý
preludia	preludium	k1gNnPc1	preludium
z	z	k7c2	z
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
;	;	kIx,	;
revize	revize	k1gFnSc1	revize
balady	balada	k1gFnSc2	balada
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
38	[number]	k4	38
<g/>
;	;	kIx,	;
dvě	dva	k4xCgFnPc1	dva
polonézy	polonéza	k1gFnPc1	polonéza
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
;	;	kIx,	;
Scherzo	scherzo	k1gNnSc4	scherzo
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
39	[number]	k4	39
<g/>
;	;	kIx,	;
Mazurka	mazurka	k1gFnSc1	mazurka
e	e	k0	e
moll	moll	k1gNnSc6	moll
z	z	k7c2	z
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
41	[number]	k4	41
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
revize	revize	k1gFnSc1	revize
Sonáty	sonáta	k1gFnSc2	sonáta
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
nejplodnějších	plodný	k2eAgNnPc2d3	nejplodnější
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
zimy	zima	k1gFnSc2	zima
vážně	vážně	k6eAd1	vážně
narušilo	narušit	k5eAaPmAgNnS	narušit
jeho	jeho	k3xOp3gNnSc1	jeho
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rychlému	rychlý	k2eAgInSc3d1	rychlý
odjezdu	odjezd	k1gInSc3	odjezd
překážel	překážet	k5eAaImAgMnS	překážet
milovaný	milovaný	k2eAgInSc4d1	milovaný
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Sandová	Sandová	k1gFnSc1	Sandová
zařídila	zařídit	k5eAaPmAgFnS	zařídit
prodej	prodej	k1gFnSc4	prodej
francouzskému	francouzský	k2eAgInSc3d1	francouzský
páru	pár	k1gInSc3	pár
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
potomci	potomek	k1gMnPc1	potomek
dodnes	dodnes	k6eAd1	dodnes
spravují	spravovat	k5eAaImIp3nP	spravovat
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
odkaz	odkaz	k1gInSc4	odkaz
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
klášterní	klášterní	k2eAgFnSc6d1	klášterní
cele	cela	k1gFnSc6	cela
ve	v	k7c6	v
Valldemosse	Valldemossa	k1gFnSc6	Valldemossa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtveřice	čtveřice	k1gFnSc1	čtveřice
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vydala	vydat	k5eAaPmAgFnS	vydat
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c4	na
zotavenou	zotavená	k1gFnSc4	zotavená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1839	[number]	k4	1839
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
na	na	k7c4	na
usedlost	usedlost	k1gFnSc4	usedlost
G.	G.	kA	G.
Sandové	Sandové	k2eAgInSc2d1	Sandové
v	v	k7c6	v
Nohantu	Nohant	k1gInSc6	Nohant
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejprve	nejprve	k6eAd1	nejprve
žili	žít	k5eAaImAgMnP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
opustil	opustit	k5eAaPmAgInS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
byt	byt	k1gInSc4	byt
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
5	[number]	k4	5
v	v	k7c6	v
rue	rue	k?	rue
Tronchet	Tronchet	k1gMnSc1	Tronchet
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
Sandové	Sandové	k2eAgInSc2d1	Sandové
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
16	[number]	k4	16
na	na	k7c6	na
rue	rue	k?	rue
Pigalle	Pigall	k1gInSc6	Pigall
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
čtyři	čtyři	k4xCgMnPc1	čtyři
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
spolu	spolu	k6eAd1	spolu
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1839	[number]	k4	1839
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
č.	č.	k?	č.
80	[number]	k4	80
v	v	k7c6	v
rue	rue	k?	rue
Taitbout	Taitbout	k1gMnSc1	Taitbout
u	u	k7c2	u
place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orléans	Orléans	k1gInSc1	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
let	léto	k1gNnPc2	léto
trávili	trávit	k5eAaImAgMnP	trávit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
společně	společně	k6eAd1	společně
v	v	k7c6	v
Nohantu	Nohant	k1gInSc6	Nohant
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
existují	existovat	k5eAaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
nástroje	nástroj	k1gInPc4	nástroj
než	než	k8xS	než
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
tenoristy	tenorista	k1gMnSc2	tenorista
Adolphe	Adolph	k1gMnSc2	Adolph
Nourrita	Nourrita	k1gFnSc1	Nourrita
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jehož	jehož	k3xOyRp3gNnSc1	jehož
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
převezeno	převézt	k5eAaPmNgNnS	převézt
k	k	k7c3	k
pohřbu	pohřeb	k1gInSc3	pohřeb
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
transkripci	transkripce	k1gFnSc3	transkripce
Schubertovy	Schubertův	k2eAgFnSc2d1	Schubertova
skladby	skladba	k1gFnSc2	skladba
Die	Die	k1gFnSc2	Die
Gestirne	Gestirn	k1gInSc5	Gestirn
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1843	[number]	k4	1843
strávených	strávený	k2eAgMnPc2d1	strávený
na	na	k7c6	na
Nohantu	Nohant	k1gInSc6	Nohant
strávil	strávit	k5eAaPmAgMnS	strávit
klidné	klidný	k2eAgNnSc4d1	klidné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plodné	plodný	k2eAgInPc1d1	plodný
dny	den	k1gInPc1	den
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgInPc2	který
složil	složit	k5eAaPmAgInS	složit
množství	množství	k1gNnSc4	množství
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Polonéza	polonéza	k1gFnSc1	polonéza
As	as	k9	as
dur	dur	k1gNnSc4	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
53	[number]	k4	53
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Heroická	heroický	k2eAgFnSc1d1	heroická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Sandové	Sandová	k1gFnSc3	Sandová
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
popis	popis	k1gInSc1	popis
jeho	jeho	k3xOp3gInSc2	jeho
tvůrčího	tvůrčí	k2eAgInSc2d1	tvůrčí
procesu	proces	k1gInSc2	proces
<g/>
:	:	kIx,	:
počáteční	počáteční	k2eAgFnSc1d1	počáteční
inspirace	inspirace	k1gFnSc1	inspirace
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
usilovné	usilovný	k2eAgNnSc1d1	usilovné
využití	využití	k1gNnSc1	využití
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
změn	změna	k1gFnPc2	změna
původního	původní	k2eAgNnSc2d1	původní
tématu	téma	k1gNnSc2	téma
<g/>
,	,	kIx,	,
završené	završený	k2eAgFnPc1d1	završená
třeba	třeba	k6eAd1	třeba
i	i	k9	i
návratem	návrat	k1gInSc7	návrat
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
<g/>
Sandová	Sandová	k1gFnSc1	Sandová
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeden	jeden	k4xCgInSc1	jeden
večer	večer	k1gInSc1	večer
strávený	strávený	k2eAgInSc1d1	strávený
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
přítelem	přítel	k1gMnSc7	přítel
Delacroixem	Delacroix	k1gInSc7	Delacroix
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
nemoc	nemoc	k1gFnSc1	nemoc
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
,	,	kIx,	,
Sandová	Sandová	k1gFnSc1	Sandová
se	se	k3xPyFc4	se
z	z	k7c2	z
milenky	milenka	k1gFnSc2	milenka
měnila	měnit	k5eAaImAgFnS	měnit
v	v	k7c4	v
ošetřovatelku	ošetřovatelka	k1gFnSc4	ošetřovatelka
a	a	k8xC	a
často	často	k6eAd1	často
Chopina	Chopin	k1gMnSc4	Chopin
nazývala	nazývat	k5eAaImAgNnP	nazývat
svým	svůj	k3xOyFgInSc7	svůj
"	"	kIx"	"
<g/>
třetím	třetí	k4xOgNnSc7	třetí
dítětem	dítě	k1gNnSc7	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ošetřování	ošetřování	k1gNnSc2	ošetřování
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
pomalu	pomalu	k6eAd1	pomalu
začínalo	začínat	k5eAaImAgNnS	začínat
protivit	protivit	k5eAaImF	protivit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
nadále	nadále	k6eAd1	nadále
se	s	k7c7	s
Chopinem	Chopin	k1gMnSc7	Chopin
udržovala	udržovat	k5eAaImAgFnS	udržovat
přátelství	přátelství	k1gNnSc3	přátelství
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
dávala	dávat	k5eAaImAgFnS	dávat
průchod	průchod	k1gInSc4	průchod
netrpělivosti	netrpělivost	k1gFnSc2	netrpělivost
a	a	k8xC	a
odporu	odpor	k1gInSc2	odpor
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
zvláštnímu	zvláštní	k2eAgInSc3d1	zvláštní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
třetím	třetí	k4xOgFnPc3	třetí
osobám	osoba	k1gFnPc3	osoba
pak	pak	k8xC	pak
často	často	k6eAd1	často
Chopina	Chopin	k1gMnSc4	Chopin
nazývala	nazývat	k5eAaImAgFnS	nazývat
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
malým	malý	k2eAgMnSc7d1	malý
andílkem	andílek	k1gMnSc7	andílek
<g/>
,	,	kIx,	,
mučedníkem	mučedník	k1gMnSc7	mučedník
a	a	k8xC	a
milovaným	milovaný	k1gMnSc7	milovaný
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
zdraví	zdraví	k1gNnSc1	zdraví
opět	opět	k6eAd1	opět
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
<g/>
,	,	kIx,	,
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
Sandovou	Sandová	k1gFnSc7	Sandová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
roztržce	roztržka	k1gFnSc3	roztržka
došlo	dojít	k5eAaPmAgNnS	dojít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
dcery	dcera	k1gFnSc2	dcera
Sandové	Sandový	k2eAgFnSc2d1	Sandový
Solange	Solang	k1gFnSc2	Solang
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
vdát	vdát	k5eAaPmF	vdát
za	za	k7c2	za
sochaře	sochař	k1gMnSc2	sochař
Clésingera	Clésinger	k1gMnSc2	Clésinger
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
Sandová	Sandová	k1gFnSc1	Sandová
publikovala	publikovat	k5eAaBmAgFnS	publikovat
román	román	k1gInSc4	román
Lucrezia	Lucrezium	k1gNnSc2	Lucrezium
Floriani	Florian	k1gMnPc1	Florian
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
–	–	k?	–
bohatá	bohatý	k2eAgFnSc1d1	bohatá
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
chatrného	chatrný	k2eAgNnSc2d1	chatrné
zdraví	zdraví	k1gNnSc2	zdraví
–	–	k?	–
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
interpretování	interpretování	k1gNnSc4	interpretování
jako	jako	k8xC	jako
Sandová	Sandová	k1gFnSc1	Sandová
a	a	k8xC	a
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
byl	být	k5eAaImAgInS	být
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
nelichotivý	lichotivý	k2eNgInSc1d1	nelichotivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
již	již	k6eAd1	již
nejel	jet	k5eNaImAgMnS	jet
na	na	k7c4	na
Nohant	Nohant	k1gInSc4	Nohant
<g/>
.	.	kIx.	.
</s>
<s>
Společní	společní	k2eAgMnPc1d1	společní
přátelé	přítel	k1gMnPc1	přítel
páru	pár	k1gInSc2	pár
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skladatel	skladatel	k1gMnSc1	skladatel
byl	být	k5eAaImAgMnS	být
neústupný	ústupný	k2eNgInSc4d1	neústupný
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gMnPc4	jejich
společné	společný	k2eAgMnPc4d1	společný
přátele	přítel	k1gMnPc4	přítel
patřila	patřit	k5eAaImAgFnS	patřit
také	také	k6eAd1	také
mezzosopranistka	mezzosopranistka	k1gFnSc1	mezzosopranistka
Pauline	Paulin	k1gInSc5	Paulin
Viardotová	Viardotový	k2eAgNnPc5d1	Viardotový
<g/>
.	.	kIx.	.
</s>
<s>
Sandová	Sandová	k1gFnSc1	Sandová
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
postavě	postava	k1gFnSc6	postava
založila	založit	k5eAaPmAgFnS	založit
svůj	svůj	k3xOyFgInSc4	svůj
román	román	k1gInSc4	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
Consuela	Consuela	k1gFnSc1	Consuela
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgFnSc1d1	vynikající
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
byla	být	k5eAaImAgFnS	být
též	též	k6eAd1	též
výbornou	výborný	k2eAgFnSc7d1	výborná
pianistkou	pianistka	k1gFnSc7	pianistka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
zamýšlela	zamýšlet	k5eAaImAgFnS	zamýšlet
zabývat	zabývat	k5eAaImF	zabývat
se	s	k7c7	s
klavírní	klavírní	k2eAgFnSc7d1	klavírní
hrou	hra	k1gFnSc7	hra
profesionálně	profesionálně	k6eAd1	profesionálně
a	a	k8xC	a
brala	brát	k5eAaImAgFnS	brát
hodiny	hodina	k1gFnPc4	hodina
u	u	k7c2	u
Ference	Ferenc	k1gMnSc2	Ferenc
Liszta	Liszt	k1gMnSc2	Liszt
a	a	k8xC	a
Antonína	Antonín	k1gMnSc2	Antonín
Rejchy	Rejcha	k1gMnSc2	Rejcha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
přátelství	přátelství	k1gNnSc1	přátelství
se	s	k7c7	s
Chopinem	Chopin	k1gMnSc7	Chopin
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
umělecké	umělecký	k2eAgFnSc6d1	umělecká
úctě	úcta	k1gFnSc6	úcta
a	a	k8xC	a
podobném	podobný	k2eAgInSc6d1	podobný
temperamentu	temperament	k1gInSc6	temperament
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
hráli	hrát	k5eAaImAgMnP	hrát
čtyřručně	čtyřručně	k6eAd1	čtyřručně
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
jí	jíst	k5eAaImIp3nS	jíst
radil	radit	k5eAaImAgMnS	radit
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
klavírní	klavírní	k2eAgFnSc2d1	klavírní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
jí	on	k3xPp3gFnSc7	on
při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
písní	píseň	k1gFnPc2	píseň
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
mazurkách	mazurek	k1gInPc6	mazurek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Viardotové	Viardotový	k2eAgFnSc2d1	Viardotový
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgInPc4	první
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
španělské	španělský	k2eAgFnSc6d1	španělská
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
dramat	drama	k1gNnPc2	drama
či	či	k8xC	či
formalit	formalita	k1gFnPc2	formalita
dlouholeté	dlouholetý	k2eAgNnSc4d1	dlouholeté
přátelství	přátelství	k1gNnSc4	přátelství
mezi	mezi	k7c7	mezi
Sandovou	Sandová	k1gFnSc7	Sandová
a	a	k8xC	a
Chopinem	Chopin	k1gMnSc7	Chopin
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
<g/>
Kníže	kníže	k1gNnSc1wR	kníže
Wojciech	Wojciech	k1gInSc1	Wojciech
Grzymała	Grzymała	k1gFnSc1	Grzymała
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
romanci	romance	k1gFnSc4	romance
se	s	k7c7	s
Sandovou	Sandová	k1gFnSc7	Sandová
od	od	k7c2	od
začátků	začátek	k1gInPc2	začátek
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
později	pozdě	k6eAd2	pozdě
svůj	svůj	k3xOyFgInSc4	svůj
<g />
.	.	kIx.	.
</s>
<s>
názor	názor	k1gInSc1	názor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdyby	kdyby	k9	kdyby
Chopina	Chopin	k1gMnSc2	Chopin
nepotkalo	potkat	k5eNaPmAgNnS	potkat
to	ten	k3xDgNnSc1	ten
neštěstí	neštěstí	k1gNnSc1	neštěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
potkal	potkat	k5eAaPmAgMnS	potkat
G.	G.	kA	G.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
otrávila	otrávit	k5eAaPmAgFnS	otrávit
celé	celý	k2eAgInPc4d1	celý
jeho	jeho	k3xOp3gNnSc1	jeho
bytí	bytí	k1gNnSc1	bytí
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
jako	jako	k8xC	jako
Cherubini	Cherubin	k2eAgMnPc1d1	Cherubin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
39	[number]	k4	39
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
přítel	přítel	k1gMnSc1	přítel
Cherubini	Cherubin	k2eAgMnPc1d1	Cherubin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
81	[number]	k4	81
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
skladatelé	skladatel	k1gMnPc1	skladatel
leží	ležet	k5eAaImIp3nP	ležet
vzdáleni	vzdálen	k2eAgMnPc1d1	vzdálen
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Závěr	závěr	k1gInSc1	závěr
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
veřejná	veřejný	k2eAgFnSc1d1	veřejná
popularita	popularita	k1gFnSc1	popularita
jako	jako	k8xC	jako
virtuosa	virtuos	k1gMnSc2	virtuos
opadávala	opadávat	k5eAaImAgFnS	opadávat
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1848	[number]	k4	1848
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
jeho	jeho	k3xOp3gFnSc7	jeho
poslední	poslední	k2eAgInSc1d1	poslední
pařížský	pařížský	k2eAgInSc1d1	pařížský
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
večírcích	večírek	k1gInPc6	večírek
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
léta	léto	k1gNnSc2	léto
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Johnstone	Johnston	k1gInSc5	Johnston
v	v	k7c6	v
hrabství	hrabství	k1gNnPc1	hrabství
Renfrewshire	Renfrewshir	k1gInSc5	Renfrewshir
blízko	blízko	k6eAd1	blízko
Glasgow	Glasgow	k1gNnSc1	Glasgow
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
bývalé	bývalý	k2eAgFnSc2d1	bývalá
žákyně	žákyně	k1gFnSc2	žákyně
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
obdivovatelky	obdivovatelka	k1gFnSc2	obdivovatelka
Jane	Jan	k1gMnSc5	Jan
Wilhelminy	Wilhelmin	k1gInPc4	Wilhelmin
Stirlingové	Stirlingový	k2eAgFnPc1d1	Stirlingový
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
chtěla	chtít	k5eAaImAgFnS	chtít
za	za	k7c4	za
Chopina	Chopin	k1gMnSc4	Chopin
vdát	vdát	k5eAaPmF	vdát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Chopin	Chopin	k1gMnSc1	Chopin
to	ten	k3xDgNnSc4	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
dlouho	dlouho	k6eAd1	dlouho
živý	živý	k2eAgMnSc1d1	živý
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
října	říjen	k1gInSc2	říjen
1848	[number]	k4	1848
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
v	v	k7c6	v
domě	dům	k1gInSc6	dům
polského	polský	k2eAgMnSc2d1	polský
lékaře	lékař	k1gMnSc2	lékař
Adama	Adam	k1gMnSc2	Adam
Łyszczyńského	Łyszczyńský	k1gMnSc2	Łyszczyńský
a	a	k8xC	a
sepsal	sepsat	k5eAaPmAgMnS	sepsat
zde	zde	k6eAd1	zde
poslední	poslední	k2eAgFnSc4d1	poslední
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
si	se	k3xPyFc3	se
vyvolával	vyvolávat	k5eAaImAgMnS	vyvolávat
scenérie	scenérie	k1gFnPc4	scenérie
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
svých	svůj	k3xOyFgFnPc2	svůj
melodií	melodie	k1gFnPc2	melodie
inspirovaných	inspirovaný	k2eAgInPc2d1	inspirovaný
polskou	polský	k2eAgFnSc7d1	polská
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vystoupení	vystoupení	k1gNnSc4	vystoupení
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Chopin	Chopin	k1gMnSc1	Chopin
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1848	[number]	k4	1848
v	v	k7c4	v
londýnské	londýnský	k2eAgInPc4d1	londýnský
Guildhall	Guildhall	k1gInSc4	Guildhall
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
posluchačů	posluchač	k1gMnPc2	posluchač
již	již	k6eAd1	již
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
neoslovilo	oslovit	k5eNaPmAgNnS	oslovit
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zimu	zima	k1gFnSc4	zima
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
neustálých	neustálý	k2eAgFnPc6d1	neustálá
nemocech	nemoc	k1gFnPc6	nemoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
stýkal	stýkat	k5eAaImAgMnS	stýkat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
churavějícího	churavějící	k2eAgMnSc4d1	churavějící
Adama	Adam	k1gMnSc4	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
konejšil	konejšit	k5eAaImAgMnS	konejšit
svou	svůj	k3xOyFgFnSc7	svůj
hrou	hra	k1gFnSc7	hra
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
již	již	k6eAd1	již
neměl	mít	k5eNaImAgMnS	mít
síly	síla	k1gFnPc4	síla
dávat	dávat	k5eAaImF	dávat
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
komponoval	komponovat	k5eAaImAgMnS	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
Nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
na	na	k7c4	na
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
svůj	svůj	k3xOyFgInSc4	svůj
hodnotnější	hodnotný	k2eAgInSc4d2	hodnotnější
nábytek	nábytek	k1gInSc4	nábytek
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
<g/>
Zdravotně	zdravotně	k6eAd1	zdravotně
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
stále	stále	k6eAd1	stále
hůř	zle	k6eAd2	zle
a	a	k8xC	a
hůř	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
mít	mít	k5eAaImF	mít
na	na	k7c6	na
blízku	blízko	k1gNnSc6	blízko
někoho	někdo	k3yInSc4	někdo
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1849	[number]	k4	1849
jeho	jeho	k3xOp3gMnSc2	jeho
sestra	sestra	k1gFnSc1	sestra
Ludwika	Ludwikum	k1gNnSc2	Ludwikum
Jędrzejewicz	Jędrzejewicza	k1gFnPc2	Jędrzejewicza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
mládí	mládí	k1gNnSc6	mládí
dávala	dávat	k5eAaImAgFnS	dávat
první	první	k4xOgFnSc1	první
lekce	lekce	k1gFnSc1	lekce
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
pozváním	pozvání	k1gNnSc7	pozvání
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Chopinem	Chopin	k1gMnSc7	Chopin
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
hezkém	hezký	k2eAgNnSc6d1	hezké
slunném	slunný	k2eAgNnSc6d1	slunné
apartmá	apartmá	k1gNnSc6	apartmá
na	na	k7c6	na
place	plac	k1gInSc6	plac
Vendôme	Vendôm	k1gInSc5	Vendôm
č.	č.	k?	č.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k1gFnSc2	ranní
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
Chopin	Chopin	k1gMnSc1	Chopin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
mnoho	mnoho	k4c1	mnoho
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
u	u	k7c2	u
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgInP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Být	být	k5eAaImF	být
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
jeho	jeho	k3xOp3gMnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
<g/>
,	,	kIx,	,
Tad	Tad	k1gMnSc1	Tad
Szulc	Szulc	k1gFnSc1	Szulc
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
zaručovalo	zaručovat	k5eAaImAgNnS	zaručovat
historické	historický	k2eAgNnSc1d1	historické
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
postavení	postavení	k1gNnSc1	postavení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
skutečně	skutečně	k6eAd1	skutečně
byli	být	k5eAaImAgMnP	být
u	u	k7c2	u
Chopinova	Chopinův	k2eAgNnSc2d1	Chopinovo
smrtelného	smrtelný	k2eAgNnSc2d1	smrtelné
lože	lože	k1gNnSc2	lože
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Ludwika	Ludwika	k1gFnSc1	Ludwika
<g/>
,	,	kIx,	,
kněžna	kněžna	k1gFnSc1	kněžna
Marcelina	Marcelin	k2eAgNnSc2d1	Marcelino
Czartoryska	Czartorysko	k1gNnSc2	Czartorysko
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
George	Georg	k1gMnSc2	Georg
Sandové	Sandové	k2eAgInSc2d1	Sandové
Solange	Solang	k1gInSc2	Solang
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
manžel	manžel	k1gMnSc1	manžel
Auguste	August	k1gMnSc5	August
Clésinger	Clésinger	k1gMnSc1	Clésinger
<g/>
,	,	kIx,	,
Chopinův	Chopinův	k2eAgMnSc1d1	Chopinův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
Adolf	Adolf	k1gMnSc1	Adolf
Gutmann	Gutmann	k1gMnSc1	Gutmann
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Thomas	Thomas	k1gMnSc1	Thomas
Albrecht	Albrecht	k1gMnSc1	Albrecht
a	a	k8xC	a
také	také	k9	také
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
Aleksander	Aleksander	k1gMnSc1	Aleksander
Jełowicki	Jełowicki	k1gNnPc2	Jełowicki
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgNnSc2	ten
rána	ráno	k1gNnSc2	ráno
sňal	snít	k5eAaPmAgMnS	snít
Auguste	August	k1gMnSc5	August
Clésinger	Clésingero	k1gNnPc2	Clésingero
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
posmrtnou	posmrtný	k2eAgFnSc4d1	posmrtná
masku	maska	k1gFnSc4	maska
a	a	k8xC	a
odlitek	odlitek	k1gInSc4	odlitek
jeho	jeho	k3xOp3gFnSc2	jeho
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
,	,	kIx,	,
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
Chopinovým	Chopinův	k2eAgNnSc7d1	Chopinovo
posledním	poslední	k2eAgNnSc7d1	poslední
přáním	přání	k1gNnSc7	přání
(	(	kIx(	(
<g/>
souvisejícím	související	k2eAgInPc3d1	související
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
obavou	obava	k1gFnSc7	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
zaživa	zaživa	k6eAd1	zaživa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
vyjmuto	vyjmut	k2eAgNnSc1d1	vyjmuto
a	a	k8xC	a
uloženo	uložen	k2eAgNnSc1d1	uloženo
do	do	k7c2	do
lihu	líh	k1gInSc2	líh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
později	pozdě	k6eAd2	pozdě
odvezla	odvézt	k5eAaPmAgFnS	odvézt
srdce	srdce	k1gNnSc4	srdce
v	v	k7c6	v
urně	urna	k1gFnSc6	urna
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
do	do	k7c2	do
pilíře	pilíř	k1gInSc2	pilíř
chrámu	chrám	k1gInSc2	chrám
Svatého	svatý	k2eAgInSc2d1	svatý
kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
Krakovském	krakovský	k2eAgNnSc6d1	Krakovské
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
citátem	citát	k1gInSc7	citát
z	z	k7c2	z
bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
6	[number]	k4	6
<g/>
,	,	kIx,	,
21	[number]	k4	21
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vždyť	vždyť	k9	vždyť
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
váš	váš	k3xOp2gInSc1	váš
poklad	poklad	k1gInSc1	poklad
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
bude	být	k5eAaImBp3nS	být
i	i	k9	i
vaše	váš	k3xOp2gNnPc4	váš
srdce	srdce	k1gNnPc4	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chopinovo	Chopinův	k2eAgNnSc1d1	Chopinovo
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uloženo	uložit	k5eAaPmNgNnS	uložit
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
zničen	zničit	k5eAaPmNgInS	zničit
během	během	k7c2	během
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
na	na	k7c6	na
bezpečném	bezpečný	k2eAgNnSc6d1	bezpečné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
stojí	stát	k5eAaImIp3nS	stát
nedaleko	nedaleko	k7c2	nedaleko
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
Chopin	Chopin	k1gMnSc1	Chopin
naposledy	naposledy	k6eAd1	naposledy
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
–	–	k?	–
paláce	palác	k1gInSc2	palác
Krasińských	Krasińská	k1gFnPc2	Krasińská
na	na	k7c6	na
Krakovském	krakovský	k2eAgNnSc6d1	Krakovské
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Église	Églis	k1gInSc6	Églis
de	de	k?	de
la	la	k1gNnSc2	la
Madeleine	Madeleine	k1gFnSc1	Madeleine
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
hráno	hrát	k5eAaImNgNnS	hrát
Mozartovo	Mozartův	k2eAgNnSc1d1	Mozartovo
Requiem	Requius	k1gMnSc7	Requius
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
ženských	ženský	k2eAgInPc2d1	ženský
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
chrámu	chrám	k1gInSc6	chrám
nikdy	nikdy	k6eAd1	nikdy
nezpíval	zpívat	k5eNaImAgMnS	zpívat
sbor	sbor	k1gInSc4	sbor
se	s	k7c7	s
ženskými	ženská	k1gFnPc7	ženská
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
speciální	speciální	k2eAgNnPc4d1	speciální
povolení	povolení	k1gNnPc4	povolení
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ženský	ženský	k2eAgInSc1d1	ženský
sbor	sbor	k1gInSc1	sbor
nakonec	nakonec	k6eAd1	nakonec
mohl	moct	k5eAaImAgInS	moct
zádušní	zádušní	k2eAgFnPc4d1	zádušní
mše	mše	k1gFnPc4	mše
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
ukryt	ukryt	k2eAgMnSc1d1	ukryt
za	za	k7c7	za
černou	černý	k2eAgFnSc7d1	černá
sametovou	sametový	k2eAgFnSc7d1	sametová
oponou	opona	k1gFnSc7	opona
<g/>
.	.	kIx.	.
</s>
<s>
Sólisty	sólista	k1gMnPc4	sólista
byli	být	k5eAaImAgMnP	být
basista	basista	k1gMnSc1	basista
Luigi	Luig	k1gFnSc2	Luig
Lablache	Lablache	k1gInSc1	Lablache
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stejný	stejný	k2eAgInSc1d1	stejný
part	part	k1gInSc1	part
zpíval	zpívat	k5eAaImAgInS	zpívat
na	na	k7c4	na
Haydnovu	Haydnův	k2eAgFnSc4d1	Haydnova
pohřbu	pohřeb	k1gInSc6	pohřeb
a	a	k8xC	a
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
též	též	k9	též
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Belliniho	Bellini	k1gMnSc2	Bellini
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezzosopranistka	mezzosopranistka	k1gFnSc1	mezzosopranistka
Pauline	Paulin	k1gInSc5	Paulin
Viardotová	Viardotový	k2eAgNnPc5d1	Viardotový
<g/>
.	.	kIx.	.
</s>
<s>
Zazněla	zaznít	k5eAaPmAgFnS	zaznít
Chopinova	Chopinův	k2eAgNnSc2d1	Chopinovo
Preludia	preludium	k1gNnSc2	preludium
č.	č.	k?	č.
4	[number]	k4	4
e	e	k0	e
moll	moll	k1gNnSc6	moll
a	a	k8xC	a
č.	č.	k?	č.
6	[number]	k4	6
h	h	k?	h
moll	moll	k1gNnSc2	moll
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
okolo	okolo	k7c2	okolo
tří	tři	k4xCgInPc2	tři
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
chyběla	chybět	k5eAaImAgFnS	chybět
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
George	George	k1gNnSc7	George
Sandová	Sandová	k1gFnSc1	Sandová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
přání	přání	k1gNnSc2	přání
<g/>
,	,	kIx,	,
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
hrán	hrát	k5eAaImNgInS	hrát
Pohřební	pohřební	k2eAgInSc1d1	pohřební
pochod	pochod	k1gInSc1	pochod
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
č.	č.	k?	č.
2	[number]	k4	2
b	b	k?	b
moll	moll	k1gNnSc6	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
,	,	kIx,	,
v	v	k7c6	v
instrumentaci	instrumentace	k1gFnSc6	instrumentace
Napoléona	Napoléon	k1gMnSc4	Napoléon
Henriho	Henri	k1gMnSc4	Henri
Rebera	Reber	k1gMnSc4	Reber
<g/>
.	.	kIx.	.
<g/>
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
hrob	hrob	k1gInSc1	hrob
s	s	k7c7	s
náhrobkem	náhrobek	k1gInSc7	náhrobek
vytesaným	vytesaný	k2eAgInSc7d1	vytesaný
Clésingerem	Clésinger	k1gInSc7	Clésinger
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
množství	množství	k1gNnSc1	množství
návštěvníků	návštěvník	k1gMnPc2	návštěvník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
vyzdoben	vyzdobit	k5eAaPmNgMnS	vyzdobit
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
spory	spor	k1gInPc1	spor
o	o	k7c4	o
příčinu	příčina	k1gFnSc4	příčina
Chopinova	Chopinův	k2eAgNnSc2d1	Chopinovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
tradované	tradovaný	k2eAgFnSc2d1	tradovaná
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
cystické	cystický	k2eAgFnSc6d1	cystická
fibróze	fibróza	k1gFnSc6	fibróza
<g/>
,	,	kIx,	,
dědičné	dědičný	k2eAgFnSc3d1	dědičná
chorobě	choroba	k1gFnSc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Nedospělo	dochvít	k5eNaPmAgNnS	dochvít
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
jednotnému	jednotný	k2eAgInSc3d1	jednotný
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
odběr	odběr	k1gInSc4	odběr
vzorku	vzorek	k1gInSc2	vzorek
z	z	k7c2	z
Chopinova	Chopinův	k2eAgNnSc2d1	Chopinovo
srdce	srdce	k1gNnSc2	srdce
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
testů	test	k1gInPc2	test
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chopin	Chopin	k1gMnSc1	Chopin
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
==	==	k?	==
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Čechy	Čech	k1gMnPc4	Čech
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
navštívil	navštívit	k5eAaPmAgInS	navštívit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
přátel	přítel	k1gMnPc2	přítel
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
Prahu	práh	k1gInSc2	práh
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
dopise	dopis	k1gInSc6	dopis
rodičům	rodič	k1gMnPc3	rodič
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
pěkné	pěkný	k2eAgNnSc1d1	pěkné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hradního	hradní	k2eAgNnSc2d1	hradní
návrší	návrší	k1gNnSc2	návrší
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc4d1	velká
<g/>
,	,	kIx,	,
starobylé	starobylý	k2eAgFnPc4d1	starobylá
a	a	k8xC	a
kdysi	kdysi	k6eAd1	kdysi
bohaté	bohatý	k2eAgNnSc1d1	bohaté
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
U	u	k7c2	u
černého	černé	k1gNnSc2	černé
koně	kůň	k1gMnSc2	kůň
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
umístěna	umístit	k5eAaPmNgFnS	umístit
jeho	jeho	k3xOp3gFnSc1	jeho
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
doporučujícím	doporučující	k2eAgInSc7d1	doporučující
dopisem	dopis	k1gInSc7	dopis
Václava	Václav	k1gMnSc2	Václav
Viléma	Vilém	k1gMnSc2	Vilém
Würfla	Würfla	k1gMnSc2	Würfla
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
pražský	pražský	k2eAgInSc1d1	pražský
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
trému	tréma	k1gFnSc4	tréma
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
ředitelem	ředitel	k1gMnSc7	ředitel
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Pixisem	Pixis	k1gInSc7	Pixis
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
drážďanským	drážďanský	k2eAgMnSc7d1	drážďanský
varhaníkem	varhaník	k1gMnSc7	varhaník
Augustinem	Augustin	k1gMnSc7	Augustin
Klenglem	Klengl	k1gMnSc7	Klengl
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
též	též	k9	též
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Hankou	Hanka	k1gFnSc7	Hanka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
jako	jako	k8xS	jako
mazurku	mazurka	k1gFnSc4	mazurka
G	G	kA	G
dur	dur	k1gNnSc1	dur
v	v	k7c6	v
lydickém	lydický	k2eAgInSc6d1	lydický
módu	mód	k1gInSc6	mód
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Hankovi	Hankův	k2eAgMnPc1d1	Hankův
napsali	napsat	k5eAaPmAgMnP	napsat
a	a	k8xC	a
věnovali	věnovat	k5eAaImAgMnP	věnovat
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Mazurka	mazurka	k1gFnSc1	mazurka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
znělkou	znělka	k1gFnSc7	znělka
Chopinova	Chopinův	k2eAgInSc2d1	Chopinův
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
.	.	kIx.	.
<g/>
Následně	následně	k6eAd1	následně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Teplice	Teplice	k1gFnPc4	Teplice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Ludwikem	Ludwik	k1gMnSc7	Ludwik
Lempickým	Lempický	k2eAgMnSc7d1	Lempický
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgInS	navštívit
místní	místní	k2eAgInSc1d1	místní
zámek	zámek	k1gInSc1	zámek
hraběte	hrabě	k1gMnSc2	hrabě
Aldringena	Aldringen	k1gMnSc2	Aldringen
<g/>
.	.	kIx.	.
</s>
<s>
Teplice	Teplice	k1gFnPc4	Teplice
poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgInS	navštívit
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
do	do	k7c2	do
Děčína	Děčín	k1gInSc2	Děčín
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1835	[number]	k4	1835
navštívil	navštívit	k5eAaPmAgInS	navštívit
lázně	lázeň	k1gFnSc2	lázeň
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
a	a	k8xC	a
setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
také	také	k9	také
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Thunem	Thun	k1gMnSc7	Thun
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
mj.	mj.	kA	mj.
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
knížetem	kníže	k1gMnSc7	kníže
Lvem	Lev	k1gMnSc7	Lev
Thun-Hohensteinem	Thun-Hohenstein	k1gMnSc7	Thun-Hohenstein
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
deníku	deník	k1gInSc2	deník
dcery	dcera	k1gFnSc2	dcera
hraběte	hrabě	k1gMnSc2	hrabě
vepsal	vepsat	k5eAaPmAgMnS	vepsat
Valčík	valčík	k1gInSc4	valčík
As	as	k9	as
dur	dur	k1gNnSc4	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
34	[number]	k4	34
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Děčínský	děčínský	k2eAgMnSc1d1	děčínský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Chotkově	Chotkův	k2eAgFnSc6d1	Chotkova
cestě	cesta	k1gFnSc6	cesta
umístěna	umístěn	k2eAgNnPc1d1	umístěno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
plaketa	plaketa	k1gFnSc1	plaketa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Mariánské	mariánský	k2eAgFnPc4d1	Mariánská
Lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
v	v	k7c6	v
pensionu	pension	k1gInSc6	pension
U	u	k7c2	u
bílé	bílý	k2eAgFnSc2d1	bílá
labutě	labuť	k1gFnSc2	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
skladatelova	skladatelův	k2eAgInSc2d1	skladatelův
pobytu	pobyt	k1gInSc2	pobyt
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
umístěn	umístěn	k2eAgInSc1d1	umístěn
tzv.	tzv.	kA	tzv.
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
<g/>
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
několik	několik	k4yIc4	několik
významných	významný	k2eAgMnPc2d1	významný
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
prozaiků	prozaik	k1gMnPc2	prozaik
(	(	kIx(	(
<g/>
K.	K.	kA	K.
Bednáře	Bednář	k1gMnSc2	Bednář
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Karena	Karena	k1gFnSc1	Karena
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pávka	pávek	k1gMnSc2	pávek
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Mareše	Mareš	k1gMnSc2	Mareš
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
k	k	k7c3	k
básnickým	básnický	k2eAgNnPc3d1	básnické
a	a	k8xC	a
novelistickým	novelistický	k2eAgNnPc3d1	novelistický
dílům	dílo	k1gNnPc3	dílo
<g/>
,	,	kIx,	,
korespondencí	korespondence	k1gFnSc7	korespondence
mezi	mezi	k7c7	mezi
Chopinem	Chopin	k1gMnSc7	Chopin
a	a	k8xC	a
Sandovou	Sandová	k1gFnSc7	Sandová
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
básnické	básnický	k2eAgFnSc6d1	básnická
tvorbě	tvorba	k1gFnSc6	tvorba
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
básnířka	básnířka	k1gFnSc1	básnířka
K.	K.	kA	K.
Erbová	erbový	k2eAgFnSc1d1	Erbová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Memorabilia	memorabilia	k1gNnPc4	memorabilia
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
stého	stý	k4xOgInSc2	stý
výročí	výročí	k1gNnSc3	výročí
Chopinova	Chopinův	k2eAgNnSc2d1	Chopinovo
narození	narození	k1gNnSc2	narození
složil	složit	k5eAaPmAgMnS	složit
ruský	ruský	k2eAgMnSc1d1	ruský
skladatel	skladatel	k1gMnSc1	skladatel
Sergej	Sergej	k1gMnSc1	Sergej
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Ljapunov	Ljapunov	k1gInSc4	Ljapunov
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Željazova	Željazův	k2eAgMnSc2d1	Željazův
Volja	Volja	k1gMnSc1	Volja
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
37	[number]	k4	37
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ж	Ж	k?	Ж
В	В	k?	В
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
v	v	k7c6	v
parku	park	k1gInSc6	park
Łazienki	Łazienk	k1gFnSc2	Łazienk
bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
sochař	sochař	k1gMnSc1	sochař
Wacław	Wacław	k1gFnSc4	Wacław
Szymanowski	Szymanowsk	k1gFnSc2	Szymanowsk
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
odhalena	odhalit	k5eAaPmNgFnS	odhalit
již	již	k9	již
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obřad	obřad	k1gInSc1	obřad
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
podobu	podoba	k1gFnSc4	podoba
sochy	socha	k1gFnSc2	socha
a	a	k8xC	a
následně	následně	k6eAd1	následně
kvůli	kvůli	k7c3	kvůli
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
zničena	zničit	k5eAaPmNgFnS	zničit
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
pořádány	pořádán	k2eAgInPc4d1	pořádán
o	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
nedělních	nedělní	k2eAgFnPc6d1	nedělní
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
recitály	recitál	k1gInPc7	recitál
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
modernistická	modernistický	k2eAgFnSc1d1	modernistická
sochu	socha	k1gFnSc4	socha
autorky	autorka	k1gFnSc2	autorka
Lu	Lu	k1gMnPc2	Lu
Pin	pin	k1gInSc1	pin
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
této	tento	k3xDgFnSc2	tento
Szymanowského	Szymanowského	k2eAgFnSc2d1	Szymanowského
sochy	socha	k1gFnSc2	socha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
městě	město	k1gNnSc6	město
Hamamacu	Hamamacus	k1gInSc2	Hamamacus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
též	též	k9	též
umístění	umístění	k1gNnSc4	umístění
další	další	k2eAgFnSc2d1	další
repliky	replika	k1gFnSc2	replika
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
vedle	vedle	k6eAd1	vedle
již	již	k9	již
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
existující	existující	k2eAgFnSc2d1	existující
sochy	socha	k1gFnSc2	socha
v	v	k7c6	v
Chopinově	Chopinův	k2eAgInSc6d1	Chopinův
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
každých	každý	k3xTgInPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
nejstarší	starý	k2eAgFnSc1d3	nejstarší
monotematická	monotematický	k2eAgFnSc1d1	Monotematická
hudební	hudební	k2eAgFnSc1d1	hudební
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klavírní	klavírní	k2eAgFnSc1d1	klavírní
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
klavírní	klavírní	k2eAgFnSc4d1	klavírní
soutěž	soutěž	k1gFnSc4	soutěž
Fryderyka	Fryderyek	k1gMnSc4	Fryderyek
Chopina	Chopin	k1gMnSc4	Chopin
a	a	k8xC	a
Chopinův	Chopinův	k2eAgInSc4d1	Chopinův
festival	festival	k1gInSc4	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
Památník	památník	k1gInSc1	památník
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
umístěný	umístěný	k2eAgInSc4d1	umístěný
v	v	k7c6	v
domu	dům	k1gInSc6wR	dům
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
s	s	k7c7	s
exponáty	exponát	k1gInPc7	exponát
a	a	k8xC	a
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
Chopinově	Chopinův	k2eAgInSc6d1	Chopinův
životě	život	k1gInSc6	život
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
pobytech	pobyt	k1gInPc6	pobyt
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
Pravidelně	pravidelně	k6eAd1	pravidelně
je	být	k5eAaImIp3nS	být
Chopinovou	Chopinová	k1gFnSc4	Chopinová
společností	společnost	k1gFnPc2	společnost
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
udělována	udělován	k2eAgFnSc1d1	udělována
cena	cena	k1gFnSc1	cena
Grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
du	du	k?	du
disque	disqu	k1gFnSc2	disqu
de	de	k?	de
F.	F.	kA	F.
Chopin	Chopin	k1gMnSc1	Chopin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
nahrávky	nahrávka	k1gFnPc4	nahrávka
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
<g/>
Životem	život	k1gInSc7	život
a	a	k8xC	a
dílem	dílo	k1gNnSc7	dílo
skladatele	skladatel	k1gMnSc2	skladatel
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
varšavský	varšavský	k2eAgInSc1d1	varšavský
Národní	národní	k2eAgInSc1d1	národní
institut	institut	k1gInSc1	institut
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Narodowy	Narodow	k2eAgInPc1d1	Narodow
Instytut	Instytut	k2eAgMnSc1d1	Instytut
Fryderyka	Fryderyka	k1gMnSc1	Fryderyka
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
mj.	mj.	kA	mj.
provozuje	provozovat	k5eAaImIp3nS	provozovat
Muzeum	muzeum	k1gNnSc1	muzeum
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skladateli	skladatel	k1gMnSc6	skladatel
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
největší	veliký	k2eAgFnSc1d3	veliký
polská	polský	k2eAgFnSc1d1	polská
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Uniwersytet	Uniwersytet	k1gInSc1	Uniwersytet
Muzyczny	Muzyczna	k1gFnSc2	Muzyczna
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varšavské	varšavský	k2eAgNnSc4d1	Varšavské
letiště	letiště	k1gNnSc4	letiště
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Port	port	k1gInSc1	port
Lotniczy	Lotnicza	k1gFnSc2	Lotnicza
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyka	Fryderyek	k1gMnSc4	Fryderyek
Chopina	Chopin	k1gMnSc4	Chopin
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
asteroid	asteroid	k1gInSc4	asteroid
3784	[number]	k4	3784
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
stručně	stručně	k6eAd1	stručně
Chopina	Chopin	k1gMnSc4	Chopin
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
chopinovské	chopinovský	k2eAgFnSc6d1	chopinovská
biografii	biografie	k1gFnSc6	biografie
(	(	kIx(	(
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
)	)	kIx)	)
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
Chopina	Chopin	k1gMnSc4	Chopin
jako	jako	k9	jako
"	"	kIx"	"
<g/>
milého	milý	k1gMnSc4	milý
<g/>
,	,	kIx,	,
harmonického	harmonický	k2eAgMnSc4d1	harmonický
génia	génius	k1gMnSc4	génius
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
popisuje	popisovat	k5eAaImIp3nS	popisovat
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
hudbu	hudba	k1gFnSc4	hudba
z	z	k7c2	z
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
období	období	k1gNnSc2	období
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
systematickou	systematický	k2eAgFnSc4d1	systematická
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
neúplnou	úplný	k2eNgFnSc4d1	neúplná
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
Chopinově	Chopinův	k2eAgInSc6d1	Chopinův
stylu	styl	k1gInSc6	styl
napsal	napsat	k5eAaPmAgMnS	napsat
F.	F.	kA	F.
P.	P.	kA	P.
Laurencin	Laurencin	k1gMnSc1	Laurencin
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Die	Die	k1gFnSc2	Die
Harmonik	harmonika	k1gFnPc2	harmonika
der	drát	k5eAaImRp2nS	drát
Neuzeit	Neuzeit	k1gInSc4	Neuzeit
<g/>
.	.	kIx.	.
</s>
<s>
Laurencin	Laurencin	k1gInSc1	Laurencin
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
klavírní	klavírní	k2eAgFnSc1d1	klavírní
hudba	hudba	k1gFnSc1	hudba
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
unikátní	unikátní	k2eAgInSc4d1	unikátní
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
rytmus	rytmus	k1gInSc4	rytmus
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
využívá	využívat	k5eAaImIp3nS	využívat
rubato	rubato	k6eAd1	rubato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
užití	užití	k1gNnSc1	užití
chromatiky	chromatika	k1gFnSc2	chromatika
a	a	k8xC	a
kontrapunktu	kontrapunkt	k1gInSc2	kontrapunkt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
dává	dávat	k5eAaImIp3nS	dávat
křehký	křehký	k2eAgInSc1d1	křehký
zvuk	zvuk	k1gInSc1	zvuk
v	v	k7c6	v
melodii	melodie	k1gFnSc6	melodie
a	a	k8xC	a
harmonii	harmonie	k1gFnSc6	harmonie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
podepřeny	podepřen	k2eAgInPc1d1	podepřen
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
kladl	klást	k5eAaImAgMnS	klást
velký	velký	k2eAgInSc4d1	velký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
legato	legato	k6eAd1	legato
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gFnPc1	jeho
skladby	skladba	k1gFnPc1	skladba
plynuly	plynout	k5eAaImAgFnP	plynout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
nechutí	nechuť	k1gFnSc7	nechuť
citoval	citovat	k5eAaBmAgMnS	citovat
své	své	k1gNnSc4	své
anglické	anglický	k2eAgFnSc2d1	anglická
obdivovatelky	obdivovatelka	k1gFnSc2	obdivovatelka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Rubinstein	Rubinstein	k1gMnSc1	Rubinstein
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
hudbě	hudba	k1gFnSc6	hudba
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgFnPc4d1	hudební
formy	forma	k1gFnPc4	forma
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
formálního	formální	k2eAgNnSc2d1	formální
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
tvorbu	tvorba	k1gFnSc4	tvorba
na	na	k7c6	na
3	[number]	k4	3
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
psal	psát	k5eAaImAgMnS	psát
hlavně	hlavně	k9	hlavně
skladby	skladba	k1gFnPc4	skladba
populární	populární	k2eAgFnPc4d1	populární
v	v	k7c6	v
post-klasicistní	postlasicistní	k2eAgFnSc6d1	post-klasicistní
klavírní	klavírní	k2eAgFnSc6d1	klavírní
literatuře	literatura	k1gFnSc6	literatura
–	–	k?	–
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
ronda	rondo	k1gNnPc1	rondo
<g/>
,	,	kIx,	,
polonézy	polonéz	k1gInPc1	polonéz
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgMnPc2	tento
stylů	styl	k1gInPc2	styl
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
hudební	hudební	k2eAgFnSc4d1	hudební
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
respektoval	respektovat	k5eAaImAgMnS	respektovat
hudební	hudební	k2eAgFnSc4d1	hudební
formu	forma	k1gFnSc4	forma
jako	jako	k8xC	jako
způsob	způsob	k1gInSc4	způsob
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
dosažení	dosažení	k1gNnSc3	dosažení
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
hudebního	hudební	k2eAgInSc2d1	hudební
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
nechával	nechávat	k5eAaImAgMnS	nechávat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
styly	styl	k1gInPc4	styl
vzájemně	vzájemně	k6eAd1	vzájemně
prolínat	prolínat	k5eAaImF	prolínat
–	–	k?	–
objevují	objevovat	k5eAaImIp3nP	objevovat
se	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
valčíku	valčík	k1gInSc2	valčík
v	v	k7c6	v
mazurkách	mazurka	k1gFnPc6	mazurka
<g/>
,	,	kIx,	,
chorál	chorál	k1gInSc1	chorál
v	v	k7c6	v
nokturnu	nokturn	k1gInSc6	nokturn
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Povaršavské	Povaršavský	k2eAgNnSc4d1	Povaršavský
období	období	k1gNnSc4	období
====	====	k?	====
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
proslavil	proslavit	k5eAaPmAgMnS	proslavit
a	a	k8xC	a
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
nový	nový	k2eAgInSc4d1	nový
salónní	salónní	k2eAgInSc4d1	salónní
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
nokturno	nokturno	k1gNnSc4	nokturno
<g/>
,	,	kIx,	,
vymyšlené	vymyšlený	k2eAgNnSc4d1	vymyšlené
irským	irský	k2eAgMnSc7d1	irský
skladatelem	skladatel	k1gMnSc7	skladatel
Johnem	John	k1gMnSc7	John
Fieldem	Field	k1gMnSc7	Field
<g/>
.	.	kIx.	.
</s>
<s>
Obohatil	obohatit	k5eAaPmAgInS	obohatit
také	také	k9	také
populární	populární	k2eAgInPc4d1	populární
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
polskou	polský	k2eAgFnSc4d1	polská
mazurku	mazurka	k1gFnSc4	mazurka
a	a	k8xC	a
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
valčík	valčík	k1gInSc4	valčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
mazurky	mazurka	k1gFnPc1	mazurka
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
tradičního	tradiční	k2eAgInSc2d1	tradiční
polského	polský	k2eAgInSc2d1	polský
tance	tanec	k1gInSc2	tanec
byly	být	k5eAaImAgInP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
tradičního	tradiční	k2eAgNnSc2d1	tradiční
pojetí	pojetí	k1gNnSc2	pojetí
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
použitelné	použitelný	k2eAgInPc1d1	použitelný
jak	jak	k8xC	jak
v	v	k7c6	v
koncertních	koncertní	k2eAgFnPc6d1	koncertní
síních	síň	k1gFnPc6	síň
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
tanečních	taneční	k2eAgInPc6d1	taneční
sálech	sál	k1gInPc6	sál
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
mazurkami	mazurka	k1gFnPc7	mazurka
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Chopin	Chopin	k1gMnSc1	Chopin
nové	nový	k2eAgNnSc4d1	nové
pojetí	pojetí	k1gNnSc4	pojetí
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
skladatele	skladatel	k1gMnPc4	skladatel
píšící	píšící	k2eAgFnSc1d1	píšící
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
nebo	nebo	k8xC	nebo
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pro	pro	k7c4	pro
skladatele	skladatel	k1gMnPc4	skladatel
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
své	svůj	k3xOyFgNnSc4	svůj
národní	národní	k2eAgNnSc4d1	národní
cítění	cítění	k1gNnSc4	cítění
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
byl	být	k5eAaImAgInS	být
také	také	k9	také
prvním	první	k4xOgMnSc7	první
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
národní	národní	k2eAgFnSc4d1	národní
hudbu	hudba	k1gFnSc4	hudba
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
proměnil	proměnit	k5eAaPmAgMnS	proměnit
jí	on	k3xPp3gFnSc3	on
v	v	k7c4	v
nový	nový	k2eAgInSc4d1	nový
žánr	žánr	k1gInSc4	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
nechal	nechat	k5eAaPmAgMnS	nechat
znovu	znovu	k6eAd1	znovu
ožít	ožít	k5eAaPmF	ožít
etudu	etuda	k1gFnSc4	etuda
rozšířením	rozšíření	k1gNnSc7	rozšíření
její	její	k3xOp3gFnSc2	její
myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
přetvořením	přetvoření	k1gNnSc7	přetvoření
do	do	k7c2	do
oslnivé	oslnivý	k2eAgFnSc2d1	oslnivá
<g/>
,	,	kIx,	,
výmluvné	výmluvný	k2eAgFnSc2d1	výmluvná
a	a	k8xC	a
emocionální	emocionální	k2eAgFnSc2d1	emocionální
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
používal	používat	k5eAaImAgMnS	používat
své	svůj	k3xOyFgFnPc4	svůj
etudy	etuda	k1gFnPc4	etuda
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
výuky	výuka	k1gFnSc2	výuka
svého	svůj	k3xOyFgInSc2	svůj
revolučního	revoluční	k2eAgInSc2d1	revoluční
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
slabšími	slabý	k2eAgInPc7d2	slabší
prsty	prst	k1gInPc7	prst
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rychlých	rychlý	k2eAgFnPc6d1	rychlá
figurách	figura	k1gFnPc6	figura
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
v	v	k7c6	v
oktávách	oktáva	k1gFnPc6	oktáva
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
,	,	kIx,	,
č.	č.	k?	č.
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
hru	hra	k1gFnSc4	hra
palcem	palec	k1gInSc7	palec
na	na	k7c6	na
černých	černý	k2eAgFnPc6d1	černá
klávesách	klávesa	k1gFnPc6	klávesa
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
též	též	k9	též
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
psal	psát	k5eAaImAgInS	psát
balady	balada	k1gFnPc4	balada
a	a	k8xC	a
scherza	scherzo	k1gNnPc4	scherzo
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc4d1	samostatná
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Série	série	k1gFnSc1	série
sedmi	sedm	k4xCc2	sedm
polonéz	polonéza	k1gFnPc2	polonéza
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
dalších	další	k2eAgMnPc2d1	další
devět	devět	k4xCc1	devět
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počínajíc	počínajíc	k7c7	počínajíc
opusovým	opusový	k2eAgNnSc7d1	opusové
číslem	číslo	k1gNnSc7	číslo
26	[number]	k4	26
<g/>
,	,	kIx,	,
nastavila	nastavit	k5eAaPmAgFnS	nastavit
nový	nový	k2eAgInSc4d1	nový
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
polskou	polský	k2eAgFnSc4d1	polská
kulturu	kultura	k1gFnSc4	kultura
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kco	kco	k?	kco
země	země	k1gFnSc1	země
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
ruského	ruský	k2eAgNnSc2d1	ruské
područí	područí	k1gNnSc2	područí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Polonéza	polonéza	k1gFnSc1	polonéza
A	a	k8xC	a
dur	dur	k1gNnSc1	dur
"	"	kIx"	"
<g/>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
,	,	kIx,	,
a	a	k8xC	a
Polonéza	polonéza	k1gFnSc1	polonéza
As	as	k1gNnSc7	as
dur	dur	k1gNnSc1	dur
"	"	kIx"	"
<g/>
Heroická	heroický	k2eAgFnSc1d1	heroická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
tvorba	tvorba	k1gFnSc1	tvorba
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pozdních	pozdní	k2eAgFnPc6d1	pozdní
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
Chopin	Chopin	k1gMnSc1	Chopin
často	často	k6eAd1	často
vracel	vracet	k5eAaImAgMnS	vracet
k	k	k7c3	k
původním	původní	k2eAgFnPc3d1	původní
formám	forma	k1gFnPc3	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
třetí	třetí	k4xOgFnSc1	třetí
klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
a	a	k8xC	a
sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
formálně	formálně	k6eAd1	formálně
čistšími	čistý	k2eAgFnPc7d2	čistší
než	než	k8xS	než
díla	dílo	k1gNnPc1	dílo
ranější	raný	k2eAgNnPc1d2	ranější
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
styly	styl	k1gInPc7	styl
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
psal	psát	k5eAaImAgMnS	psát
skladby	skladba	k1gFnPc4	skladba
netypických	typický	k2eNgMnPc2d1	netypický
názvů	název	k1gInPc2	název
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
takových	takový	k3xDgFnPc2	takový
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
Polonézu-fantasii	Polonézuantasie	k1gFnSc4	Polonézu-fantasie
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
61	[number]	k4	61
<g/>
,	,	kIx,	,
Berceuse	Berceuse	k1gFnSc1	Berceuse
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
57	[number]	k4	57
nebo	nebo	k8xC	nebo
Barkarolu	barkarola	k1gFnSc4	barkarola
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlivy	vliv	k1gInPc1	vliv
na	na	k7c4	na
Chopina	Chopin	k1gMnSc4	Chopin
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
Chopin	Chopin	k1gMnSc1	Chopin
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
hudební	hudební	k2eAgNnSc1d1	hudební
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
tradicích	tradice	k1gFnPc6	tradice
Beethovenových	Beethovenových	k2eAgMnSc2d1	Beethovenových
<g/>
,	,	kIx,	,
Haydnových	Haydnových	k2eAgMnSc2d1	Haydnových
<g/>
,	,	kIx,	,
Mozartových	Mozartových	k2eAgMnSc2d1	Mozartových
a	a	k8xC	a
Clementiho	Clementi	k1gMnSc2	Clementi
<g/>
;	;	kIx,	;
Clementiho	Clementi	k1gMnSc2	Clementi
klavírní	klavírní	k2eAgFnSc2d1	klavírní
metody	metoda	k1gFnSc2	metoda
používal	používat	k5eAaImAgInS	používat
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
svých	svůj	k3xOyFgMnPc2	svůj
vlastních	vlastní	k2eAgMnPc2d1	vlastní
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
také	také	k9	také
technikou	technika	k1gFnSc7	technika
Johanna	Johann	k1gMnSc2	Johann
Nepomuka	Nepomuk	k1gMnSc2	Nepomuk
Hummela	Hummel	k1gMnSc2	Hummel
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
uváděl	uvádět	k5eAaImAgMnS	uvádět
Bacha	Bacha	k?	Bacha
a	a	k8xC	a
Mozarta	Mozart	k1gMnSc4	Mozart
jako	jako	k8xS	jako
dva	dva	k4xCgInPc4	dva
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
skladatele	skladatel	k1gMnSc2	skladatel
utvářející	utvářející	k2eAgFnSc1d1	utvářející
jeho	jeho	k3xOp3gNnSc4	jeho
hudební	hudební	k2eAgNnSc4d1	hudební
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
<g/>
Okolo	okolo	k7c2	okolo
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
inspirací	inspirace	k1gFnPc2	inspirace
panuje	panovat	k5eAaImIp3nS	panovat
mnoho	mnoho	k6eAd1	mnoho
dohad	dohad	k1gInSc4	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
etuda	etuda	k1gFnSc1	etuda
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
jako	jako	k9	jako
inspirace	inspirace	k1gFnSc1	inspirace
poraženým	poražený	k2eAgMnSc7d1	poražený
polským	polský	k2eAgMnSc7d1	polský
povstáním	povstání	k1gNnSc7	povstání
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
či	či	k8xC	či
pouze	pouze	k6eAd1	pouze
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pohřební	pohřební	k2eAgInSc1d1	pohřební
pochod	pochod	k1gInSc1	pochod
z	z	k7c2	z
klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
b	b	k?	b
moll	moll	k1gNnSc2	moll
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
napsán	napsat	k5eAaPmNgInS	napsat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečná	skutečný	k2eAgFnSc1d1	skutečná
inspirace	inspirace	k1gFnSc1	inspirace
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
úmrtím	úmrtí	k1gNnSc7	úmrtí
nikoho	nikdo	k3yNnSc4	nikdo
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
<g/>
.	.	kIx.	.
<g/>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
Chopinových	Chopinových	k2eAgFnSc7d1	Chopinových
inspirací	inspirace	k1gFnSc7	inspirace
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
také	také	k9	také
jeho	jeho	k3xOp3gFnSc2	jeho
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Konstancii	Konstancie	k1gFnSc4	Konstancie
Gładkowskou	Gładkowský	k2eAgFnSc4d1	Gładkowský
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
Wodzińskou	Wodzińský	k2eAgFnSc4d1	Wodzińský
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
komponoval	komponovat	k5eAaImAgInS	komponovat
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
umělecká	umělecký	k2eAgFnSc1d1	umělecká
duše	duše	k1gFnSc1	duše
byla	být	k5eAaImAgFnS	být
též	též	k9	též
obohacována	obohacován	k2eAgFnSc1d1	obohacována
<g />
.	.	kIx.	.
</s>
<s>
přátelstvím	přátelství	k1gNnSc7	přátelství
s	s	k7c7	s
"	"	kIx"	"
<g/>
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
"	"	kIx"	"
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
varšavského	varšavský	k2eAgInSc2d1	varšavský
uměleckého	umělecký	k2eAgInSc2d1	umělecký
a	a	k8xC	a
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
světa	svět	k1gInSc2	svět
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Maurycy	Mauryc	k2eAgFnPc1d1	Mauryc
Mochnacki	Mochnack	k1gFnPc1	Mochnack
<g/>
,	,	kIx,	,
Józef	Józef	k1gInSc1	Józef
Bohdan	Bohdana	k1gFnPc2	Bohdana
Zaleski	Zalesk	k1gFnSc2	Zalesk
a	a	k8xC	a
Julian	Julian	k1gMnSc1	Julian
Fontana	Fontan	k1gMnSc2	Fontan
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgNnSc1	některý
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
opery	opera	k1gFnPc4	opera
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
díla	dílo	k1gNnPc1	dílo
Rossiniho	Rossini	k1gMnSc2	Rossini
<g/>
,	,	kIx,	,
Donizettiho	Donizetti	k1gMnSc2	Donizetti
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Belliniho	Bellini	k1gMnSc4	Bellini
<g/>
.	.	kIx.	.
</s>
<s>
Hedley	Hedley	k1gInPc4	Hedley
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Od	od	k7c2	od
velkých	velký	k2eAgMnPc2d1	velký
italských	italský	k2eAgMnPc2d1	italský
pěvců	pěvec	k1gMnPc2	pěvec
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
"	"	kIx"	"
<g/>
zpívat	zpívat	k5eAaImF	zpívat
<g/>
"	"	kIx"	"
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
nokturna	nokturno	k1gNnSc2	nokturno
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
kvalitu	kvalita	k1gFnSc4	kvalita
jeho	on	k3xPp3gNnSc2	on
cantabile	cantabile	k6eAd1	cantabile
a	a	k8xC	a
jemné	jemný	k2eAgNnSc4d1	jemné
kouzlo	kouzlo	k1gNnSc4	kouzlo
ornamentace	ornamentace	k1gFnSc2	ornamentace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
také	také	k9	také
použil	použít	k5eAaPmAgMnS	použít
sérii	série	k1gFnSc4	série
Bachových	Bachův	k2eAgNnPc2d1	Bachovo
preludií	preludium	k1gNnPc2	preludium
a	a	k8xC	a
fug	fuga	k1gFnPc2	fuga
z	z	k7c2	z
Dobře	dobře	k6eAd1	dobře
temperovaného	temperovaný	k2eAgInSc2d1	temperovaný
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
transformoval	transformovat	k5eAaBmAgInS	transformovat
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
vlastních	vlastní	k2eAgFnPc2d1	vlastní
preludií	preludie	k1gFnPc2	preludie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občas	občas	k6eAd1	občas
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
při	při	k7c6	při
kompozici	kompozice	k1gFnSc6	kompozice
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
umělci	umělec	k1gMnPc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgNnSc1d1	rané
dílo	dílo	k1gNnSc1	dílo
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
Introdukce	introdukce	k1gFnSc1	introdukce
a	a	k8xC	a
polonéza	polonéza	k1gFnSc1	polonéza
C	C	kA	C
dur	dur	k1gNnSc2	dur
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc2	vliv
Živného	živný	k2eAgInSc2d1	živný
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
pozdní	pozdní	k2eAgNnPc4d1	pozdní
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Sonáta	sonáta	k1gFnSc1	sonáta
g	g	kA	g
moll	moll	k1gNnSc2	moll
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
violoncellového	violoncellový	k2eAgMnSc2d1	violoncellový
virtuosa	virtuos	k1gMnSc2	virtuos
Augusta	August	k1gMnSc2	August
Franchomma	Franchomm	k1gMnSc2	Franchomm
<g/>
;	;	kIx,	;
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Duu	duo	k1gNnSc3	duo
E	E	kA	E
dur	dur	k1gNnSc2	dur
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
i	i	k9	i
přímo	přímo	k6eAd1	přímo
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
.	.	kIx.	.
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
se	se	k3xPyFc4	se
též	též	k9	též
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
Lisztově	Lisztův	k2eAgInSc6d1	Lisztův
Hexaméronu	Hexaméron	k1gInSc6	Hexaméron
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
složil	složit	k5eAaPmAgMnS	složit
šestou	šestý	k4xOgFnSc4	šestý
variaci	variace	k1gFnSc4	variace
na	na	k7c4	na
Belliniho	Bellini	k1gMnSc4	Bellini
téma	téma	k1gFnSc1	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rubato	rubato	k6eAd1	rubato
===	===	k?	===
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
často	často	k6eAd1	často
používal	používat	k5eAaImAgInS	používat
rubato	rubato	k6eAd1	rubato
(	(	kIx(	(
<g/>
manipulaci	manipulace	k1gFnSc3	manipulace
s	s	k7c7	s
rytmem	rytmus	k1gInSc7	rytmus
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
neklidně	klidně	k6eNd1	klidně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
protipól	protipól	k1gInSc1	protipól
ke	k	k7c3	k
striktní	striktní	k2eAgFnSc3d1	striktní
hře	hra	k1gFnSc3	hra
v	v	k7c6	v
tempu	tempo	k1gNnSc6	tempo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cituje	citovat	k5eAaBmIp3nS	citovat
<g/>
-li	i	k?	-li
někdo	někdo	k3yInSc1	někdo
Chopina	Chopin	k1gMnSc4	Chopin
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
rubata	rubat	k1gMnSc2	rubat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
omezí	omezit	k5eAaPmIp3nS	omezit
na	na	k7c4	na
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
doprovod	doprovod	k1gInSc1	doprovod
vždy	vždy	k6eAd1	vždy
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
striktním	striktní	k2eAgNnSc6d1	striktní
tempu	tempo	k1gNnSc6	tempo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
tento	tento	k3xDgInSc4	tento
citát	citát	k1gInSc4	citát
uvést	uvést	k5eAaPmF	uvést
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
okamžiku	okamžik	k1gInSc2	okamžik
a	a	k8xC	a
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
vyřčen	vyřčen	k2eAgMnSc1d1	vyřčen
<g/>
.	.	kIx.	.
</s>
<s>
Constantin	Constantin	k2eAgInSc1d1	Constantin
von	von	k1gInSc1	von
Sternberg	Sternberg	k1gInSc1	Sternberg
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sternbergův	Sternbergův	k2eAgInSc1d1	Sternbergův
názor	názor	k1gInSc1	názor
nepřímo	přímo	k6eNd1	přímo
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
také	také	k9	také
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
rozhodností	rozhodnost	k1gFnSc7	rozhodnost
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chopin	Chopin	k1gMnSc1	Chopin
neumí	umět	k5eNaImIp3nS	umět
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
pojetí	pojetí	k1gNnSc3	pojetí
rubata	rubat	k1gMnSc2	rubat
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k9	i
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Hallé	Hall	k1gMnPc1	Hall
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Chopinovi	Chopin	k1gMnSc3	Chopin
pomocí	pomocí	k7c2	pomocí
počítání	počítání	k1gNnSc2	počítání
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraje	hrát	k5eAaImIp3nS	hrát
některé	některý	k3yIgFnPc4	některý
mazurky	mazurka	k1gFnPc4	mazurka
ve	v	k7c6	v
čtyřčtvrtečním	čtyřčtvrteční	k2eAgInSc6d1	čtyřčtvrteční
rytmu	rytmus	k1gInSc6	rytmus
místo	místo	k7c2	místo
tříčtvrtečního	tříčtvrteční	k2eAgNnSc2d1	tříčtvrteční
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
naprostá	naprostý	k2eAgFnSc1d1	naprostá
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
zachází	zacházet	k5eAaImIp3nS	zacházet
s	s	k7c7	s
rytmem	rytmus	k1gInSc7	rytmus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přirozená	přirozený	k2eAgFnSc1d1	přirozená
a	a	k8xC	a
neuráží	urážet	k5eNaImIp3nS	urážet
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
zase	zase	k9	zase
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
důležitost	důležitost	k1gFnSc4	důležitost
míry	míra	k1gFnSc2	míra
rubata	rubata	k1gFnSc1	rubata
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Chopin	Chopin	k1gMnSc1	Chopin
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
rytmu	rytmus	k1gInSc2	rytmus
přes	přes	k7c4	přes
svoje	svůj	k3xOyFgMnPc4	svůj
rubata	rubata	k1gFnSc1	rubata
velmi	velmi	k6eAd1	velmi
striktní	striktní	k2eAgFnSc1d1	striktní
<g/>
,	,	kIx,	,
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
klavíru	klavír	k1gInSc6	klavír
vždy	vždy	k6eAd1	vždy
metronom	metronom	k1gInSc4	metronom
a	a	k8xC	a
pro	pro	k7c4	pro
utužení	utužení	k1gNnSc4	utužení
rytmu	rytmus	k1gInSc2	rytmus
dával	dávat	k5eAaImAgMnS	dávat
žákům	žák	k1gMnPc3	žák
často	často	k6eAd1	často
hrát	hrát	k5eAaImF	hrát
Bacha	Bacha	k?	Bacha
nebo	nebo	k8xC	nebo
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopinovo	Chopinův	k2eAgNnSc1d1	Chopinovo
pojetí	pojetí	k1gNnSc1	pojetí
rytmu	rytmus	k1gInSc2	rytmus
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
Paderewski	Paderewsk	k1gFnSc2	Paderewsk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Romantismus	romantismus	k1gInSc4	romantismus
===	===	k?	===
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
pohlížel	pohlížet	k5eAaImAgMnS	pohlížet
na	na	k7c4	na
mnoho	mnoho	k4c1	mnoho
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
s	s	k7c7	s
nezájmem	nezájem	k1gInSc7	nezájem
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
měl	mít	k5eAaImAgInS	mít
hodně	hodně	k6eAd1	hodně
známých	známý	k2eAgInPc2d1	známý
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
romantismem	romantismus	k1gInSc7	romantismus
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
literatuře	literatura	k1gFnSc6	literatura
nebo	nebo	k8xC	nebo
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
George	Georg	k1gFnSc2	Georg
Sandové	Sandová	k1gFnSc2	Sandová
<g/>
.	.	kIx.	.
</s>
<s>
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
však	však	k9	však
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nacionalismus	nacionalismus	k1gInSc1	nacionalismus
===	===	k?	===
</s>
</p>
<p>
<s>
Zdzisław	Zdzisław	k?	Zdzisław
Jachimecki	Jachimeck	k1gFnSc2	Jachimeck
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chopin	Chopin	k1gMnSc1	Chopin
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
svém	svůj	k3xOyFgInSc6	svůj
kroku	krok	k1gInSc6	krok
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
svůj	svůj	k3xOyFgInSc4	svůj
polský	polský	k2eAgInSc4d1	polský
původ	původ	k1gInSc4	původ
–	–	k?	–
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
psal	psát	k5eAaImAgInS	psát
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
,	,	kIx,	,
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
k	k	k7c3	k
polským	polský	k2eAgMnPc3d1	polský
emigrantům	emigrant	k1gMnPc3	emigrant
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
negativním	negativní	k2eAgInSc6d1	negativní
postoji	postoj	k1gInSc6	postoj
k	k	k7c3	k
okupaci	okupace	k1gFnSc3	okupace
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rovněž	rovněž	k9	rovněž
skládal	skládat	k5eAaImAgMnS	skládat
hudbu	hudba	k1gFnSc4	hudba
na	na	k7c4	na
polské	polský	k2eAgInPc4d1	polský
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nezhudebňoval	zhudebňovat	k5eNaImAgInS	zhudebňovat
německé	německý	k2eAgMnPc4d1	německý
nebo	nebo	k8xC	nebo
francouzské	francouzský	k2eAgMnPc4d1	francouzský
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
měl	mít	k5eAaImAgInS	mít
mezi	mezi	k7c7	mezi
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
a	a	k8xC	a
německými	německý	k2eAgMnPc7d1	německý
básníky	básník	k1gMnPc7	básník
mnoho	mnoho	k6eAd1	mnoho
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
<g/>
Arthur	Arthur	k1gMnSc1	Arthur
Hedley	Hedlea	k1gFnSc2	Hedlea
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
našel	najít	k5eAaPmAgMnS	najít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
a	a	k8xC	a
v	v	k7c6	v
tragickém	tragický	k2eAgInSc6d1	tragický
osudu	osud	k1gInSc6	osud
Polska	Polska	k1gFnSc1	Polska
svou	svůj	k3xOyFgFnSc4	svůj
hlavní	hlavní	k2eAgFnSc4d1	hlavní
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
polské	polský	k2eAgFnSc2d1	polská
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
přetvářel	přetvářet	k5eAaImAgMnS	přetvářet
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
rytmy	rytmus	k1gInPc4	rytmus
a	a	k8xC	a
melodie	melodie	k1gFnPc1	melodie
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc2	mládí
do	do	k7c2	do
trvalých	trvalý	k2eAgFnPc2d1	trvalá
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Svým	svůj	k3xOyFgNnSc7	svůj
vlastenectvím	vlastenectví	k1gNnSc7	vlastenectví
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Jachimeckého	Jachimecký	k2eAgInSc2d1	Jachimecký
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
množství	množství	k1gNnSc4	množství
skladatelů	skladatel	k1gMnPc2	skladatel
a	a	k8xC	a
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
jako	jako	k9	jako
Smetana	smetana	k1gFnSc1	smetana
a	a	k8xC	a
Grieg	Grieg	k1gInSc1	Grieg
to	ten	k3xDgNnSc4	ten
osobně	osobně	k6eAd1	osobně
potvrzovali	potvrzovat	k5eAaImAgMnP	potvrzovat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Chopin	Chopin	k1gMnSc1	Chopin
jako	jako	k8xS	jako
inspirace	inspirace	k1gFnSc1	inspirace
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
skladeb	skladba	k1gFnPc2	skladba
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
velmi	velmi	k6eAd1	velmi
známými	známý	k2eAgFnPc7d1	známá
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
etuda	etuda	k1gFnSc1	etuda
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
Minutový	minutový	k2eAgInSc1d1	minutový
valčík	valčík	k1gInSc1	valčík
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
64	[number]	k4	64
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
či	či	k8xC	či
třetí	třetí	k4xOgFnSc1	třetí
věta	věta	k1gFnSc1	věta
z	z	k7c2	z
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
č.	č.	k?	č.
2	[number]	k4	2
b	b	k?	b
moll	moll	k1gNnSc6	moll
Pohřební	pohřební	k2eAgInSc1d1	pohřební
pochod	pochod	k1gInSc1	pochod
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nepojmenovával	pojmenovávat	k5eNaImAgMnS	pojmenovávat
svá	svůj	k3xOyFgNnPc4	svůj
instrumentální	instrumentální	k2eAgNnPc4d1	instrumentální
díla	dílo	k1gNnPc4	dílo
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
pomocí	pomocí	k7c2	pomocí
žánru	žánr	k1gInSc2	žánr
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
ponechávaje	ponechávat	k5eAaImSgInS	ponechávat
všechny	všechen	k3xTgFnPc4	všechen
mimohudební	mimohudební	k2eAgFnSc1d1	mimohudební
asociace	asociace	k1gFnSc1	asociace
na	na	k7c6	na
posluchači	posluchač	k1gMnSc6	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
známa	znám	k2eAgNnPc1d1	známo
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
dali	dát	k5eAaPmAgMnP	dát
jiní	jiný	k1gMnPc1	jiný
<g/>
.	.	kIx.	.
<g/>
Témata	téma	k1gNnPc4	téma
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
skladeb	skladba	k1gFnPc2	skladba
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
populárních	populární	k2eAgFnPc6d1	populární
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pomalá	pomalý	k2eAgFnSc1d1	pomalá
část	část	k1gFnSc1	část
z	z	k7c2	z
Fantaisie-Impromptu	Fantaisie-Imprompt	k1gInSc2	Fantaisie-Imprompt
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
66	[number]	k4	66
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
z	z	k7c2	z
Etudy	etuda	k1gFnSc2	etuda
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
č.	č.	k?	č.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
styl	styl	k1gInSc1	styl
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
mnoho	mnoho	k4c4	mnoho
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
jeho	jeho	k3xOp3gFnSc2	jeho
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
Chopinovy	Chopinův	k2eAgFnPc4d1	Chopinova
melodie	melodie	k1gFnPc4	melodie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
suity	suita	k1gFnSc2	suita
Karneval	karneval	k1gInSc1	karneval
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obdiv	obdiv	k1gInSc1	obdiv
nebyl	být	k5eNaImAgInS	být
obecně	obecně	k6eAd1	obecně
vždy	vždy	k6eAd1	vždy
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
Chopin	Chopin	k1gMnSc1	Chopin
dedikoval	dedikovat	k5eAaBmAgMnS	dedikovat
Schumannovi	Schumann	k1gMnSc3	Schumann
svou	svůj	k3xOyFgFnSc4	svůj
Baladu	balada	k1gFnSc4	balada
č.	č.	k?	č.
2	[number]	k4	2
F	F	kA	F
dur	dur	k1gNnSc2	dur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
byl	být	k5eAaImAgMnS	být
dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
Chopinových	Chopinových	k2eAgMnPc2d1	Chopinových
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
a	a	k8xC	a
blízkých	blízký	k2eAgMnPc2d1	blízký
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
transkripce	transkripce	k1gFnSc2	transkripce
šesti	šest	k4xCc2	šest
Chopinových	Chopinových	k2eAgFnPc2d1	Chopinových
polských	polský	k2eAgFnPc2d1	polská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
však	však	k9	však
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gInSc1	jeho
Funérailles	Funérailles	k1gInSc1	Funérailles
(	(	kIx(	(
<g/>
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Říjen	říjen	k1gInSc4	říjen
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
sedmá	sedmý	k4xOgFnSc1	sedmý
věta	věta	k1gFnSc1	věta
jeho	jeho	k3xOp3gFnSc2	jeho
klavírní	klavírní	k2eAgFnSc2d1	klavírní
suity	suita	k1gFnSc2	suita
Harmonies	Harmonies	k1gMnSc1	Harmonies
poétiques	poétiques	k1gMnSc1	poétiques
et	et	k?	et
religieuses	religieuses	k1gMnSc1	religieuses
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
na	na	k7c4	na
Chopinovu	Chopinův	k2eAgFnSc4d1	Chopinova
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
oktávové	oktávový	k2eAgFnSc6d1	oktávová
sekci	sekce	k1gFnSc6	sekce
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
Polonézy	polonéza	k1gFnSc2	polonéza
As	as	k9	as
dur	dur	k1gNnSc4	dur
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
53	[number]	k4	53
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Liszt	Liszt	k1gMnSc1	Liszt
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
smrtí	smrt	k1gFnSc7	smrt
svých	svůj	k3xOyFgInPc2	svůj
tří	tři	k4xCgInPc2	tři
maďarských	maďarský	k2eAgInPc2d1	maďarský
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
ruští	ruský	k2eAgMnPc1d1	ruský
skladatelé	skladatel	k1gMnPc1	skladatel
také	také	k9	také
nalézali	nalézat	k5eAaImAgMnP	nalézat
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
Chopinových	Chopinových	k2eAgNnPc6d1	Chopinových
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc2	jeho
preludia	preludium	k1gNnSc2	preludium
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
)	)	kIx)	)
a	a	k8xC	a
etudy	etuda	k1gFnSc2	etuda
(	(	kIx(	(
<g/>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
a	a	k8xC	a
25	[number]	k4	25
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
staly	stát	k5eAaPmAgInP	stát
standardy	standard	k1gInPc4	standard
a	a	k8xC	a
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
jak	jak	k8xS	jak
Liszta	Liszta	k1gFnSc1	Liszta
v	v	k7c6	v
Transcendentálních	transcendentální	k2eAgFnPc6d1	transcendentální
etudách	etuda	k1gFnPc6	etuda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Schumanna	Schumanen	k2eAgFnSc1d1	Schumanen
v	v	k7c6	v
Symfonických	symfonický	k2eAgFnPc6d1	symfonická
etudách	etuda	k1gFnPc6	etuda
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Skrjabin	Skrjabina	k1gFnPc2	Skrjabina
byl	být	k5eAaImAgMnS	být
též	též	k9	též
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Chopinem	Chopin	k1gMnSc7	Chopin
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
24	[number]	k4	24
preludiích	preludie	k1gFnPc6	preludie
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
inspirován	inspirovat	k5eAaBmNgMnS	inspirovat
Chopinovým	Chopinův	k2eAgMnSc7d1	Chopinův
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
–	–	k?	–
<g/>
Katovice	Katovice	k1gFnPc4	Katovice
<g/>
–	–	k?	–
<g/>
Bohumín	Bohumín	k1gInSc1	Bohumín
<g/>
–	–	k?	–
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
–	–	k?	–
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
je	být	k5eAaImIp3nS	být
provozován	provozován	k2eAgInSc4d1	provozován
pár	pár	k1gInSc4	pár
nočních	noční	k2eAgInPc2d1	noční
vlaků	vlak	k1gInPc2	vlak
EuroNight	EuroNighta	k1gFnPc2	EuroNighta
EN	EN	kA	EN
406	[number]	k4	406
(	(	kIx(	(
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
→	→	k?	→
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
)	)	kIx)	)
a	a	k8xC	a
EN	EN	kA	EN
407	[number]	k4	407
(	(	kIx(	(
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
→	→	k?	→
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Interpretace	interpretace	k1gFnSc2	interpretace
===	===	k?	===
</s>
</p>
<p>
<s>
Jeremy	Jerem	k1gInPc1	Jerem
Siepmann	Siepmanna	k1gFnPc2	Siepmanna
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
biografii	biografie	k1gFnSc6	biografie
předkládá	předkládat	k5eAaImIp3nS	předkládat
výčet	výčet	k1gInSc4	výčet
pianistů	pianista	k1gMnPc2	pianista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
nahrávky	nahrávka	k1gFnPc1	nahrávka
Chopinových	Chopinových	k2eAgNnPc2d1	Chopinových
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
všeobecně	všeobecně	k6eAd1	všeobecně
známými	známá	k1gFnPc7	známá
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
nejzdařilejší	zdařilý	k2eAgMnPc4d3	nejzdařilejší
<g/>
:	:	kIx,	:
Vladimir	Vladimir	k1gInSc1	Vladimir
de	de	k?	de
Pachmann	Pachmann	k1gInSc4	Pachmann
<g/>
,	,	kIx,	,
Raoul	Raoul	k1gInSc4	Raoul
Pugno	Pugno	k1gNnSc4	Pugno
<g/>
,	,	kIx,	,
Ignacy	Ignacy	k1gInPc4	Ignacy
Jan	Jan	k1gMnSc1	Jan
Paderewski	Paderewsk	k1gFnSc2	Paderewsk
<g/>
,	,	kIx,	,
Moriz	Moriz	k1gMnSc1	Moriz
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Cortot	Cortot	k1gMnSc1	Cortot
<g/>
,	,	kIx,	,
Ignaz	Ignaz	k1gInSc1	Ignaz
<g />
.	.	kIx.	.
</s>
<s>
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
Raoul	Raoul	k1gInSc1	Raoul
Koczalski	Koczalsk	k1gFnSc2	Koczalsk
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Rubinstein	Rubinstein	k1gMnSc1	Rubinstein
<g/>
,	,	kIx,	,
Mieczysław	Mieczysław	k1gMnSc1	Mieczysław
Horszowski	Horszowsk	k1gFnSc2	Horszowsk
<g/>
,	,	kIx,	,
Claudio	Claudio	k1gNnSc1	Claudio
Arrau	Arraus	k1gInSc2	Arraus
<g/>
,	,	kIx,	,
Vlado	Vlado	k1gNnSc1	Vlado
Perlemuter	Perlemuter	k1gMnSc1	Perlemuter
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Horowitz	Horowitz	k1gMnSc1	Horowitz
<g/>
,	,	kIx,	,
Dinu	din	k1gInSc2	din
Lipatti	Lipatti	k1gNnSc2	Lipatti
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Ashkenazy	Ashkenaz	k1gInPc4	Ashkenaz
<g/>
,	,	kIx,	,
Martha	Martha	k1gFnSc1	Martha
Argerichová	Argerichová	k1gFnSc1	Argerichová
<g/>
,	,	kIx,	,
Maurizio	Maurizio	k6eAd1	Maurizio
Pollini	Pollin	k2eAgMnPc1d1	Pollin
<g/>
,	,	kIx,	,
Murray	Murra	k1gMnPc7	Murra
Perahia	Perahium	k1gNnSc2	Perahium
<g/>
,	,	kIx,	,
Krystian	Krystian	k1gMnSc1	Krystian
Zimerman	Zimerman	k1gMnSc1	Zimerman
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Igorjevič	Igorjevič	k1gMnSc1	Igorjevič
Kissin	Kissin	k1gMnSc1	Kissin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
generace	generace	k1gFnSc2	generace
lze	lze	k6eAd1	lze
dodat	dodat	k5eAaPmF	dodat
ještě	ještě	k9	ještě
Yundiho	Yundi	k1gMnSc4	Yundi
Li	li	k8xS	li
<g/>
,	,	kIx,	,
Rafała	Rafał	k2eAgFnSc1d1	Rafała
Blechacze	Blechacze	k1gFnSc1	Blechacze
a	a	k8xC	a
Yulianu	Yulian	k1gMnSc3	Yulian
Avdeevu	Avdeev	k1gInSc2	Avdeev
<g/>
,	,	kIx,	,
vítěze	vítěz	k1gMnSc2	vítěz
posledních	poslední	k2eAgInPc2d1	poslední
ročníků	ročník	k1gInPc2	ročník
Chopinovy	Chopinův	k2eAgFnSc2d1	Chopinova
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klavírní	klavírní	k2eAgFnSc2d1	klavírní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
interpretů	interpret	k1gMnPc2	interpret
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Kahánek	kahánek	k1gInSc1	kahánek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kasík	Kasík	k1gMnSc1	Kasík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
230	[number]	k4	230
Chopinových	Chopinových	k2eAgNnPc2d1	Chopinových
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
rukopisy	rukopis	k1gInPc1	rukopis
a	a	k8xC	a
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
dva	dva	k4xCgInPc1	dva
klavírní	klavírní	k2eAgInPc1d1	klavírní
koncerty	koncert	k1gInPc1	koncert
nebo	nebo	k8xC	nebo
komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnSc2	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
3	[number]	k4	3
klavírní	klavírní	k2eAgFnPc4d1	klavírní
sonáty	sonáta	k1gFnPc4	sonáta
</s>
</p>
<p>
<s>
4	[number]	k4	4
scherza	scherzo	k1gNnPc1	scherzo
</s>
</p>
<p>
<s>
4	[number]	k4	4
balady	balada	k1gFnPc1	balada
</s>
</p>
<p>
<s>
17	[number]	k4	17
polonéz	polonéza	k1gFnPc2	polonéza
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jedné	jeden	k4xCgFnSc2	jeden
s	s	k7c7	s
orchestrálním	orchestrální	k2eAgInSc7d1	orchestrální
doprovodem	doprovod	k1gInSc7	doprovod
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
s	s	k7c7	s
klavírním	klavírní	k2eAgInSc7d1	klavírní
doprovodem	doprovod	k1gInSc7	doprovod
</s>
</p>
<p>
<s>
58	[number]	k4	58
mazurek	mazurek	k1gInSc1	mazurek
</s>
</p>
<p>
<s>
20	[number]	k4	20
valčíků	valčík	k1gInPc2	valčík
</s>
</p>
<p>
<s>
26	[number]	k4	26
preludií	preludie	k1gFnPc2	preludie
(	(	kIx(	(
<g/>
24	[number]	k4	24
z	z	k7c2	z
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
imprompta	imprompt	k1gInSc2	imprompt
</s>
</p>
<p>
<s>
21	[number]	k4	21
nokturn	nokturn	k1gInSc1	nokturn
</s>
</p>
<p>
<s>
27	[number]	k4	27
etud	etuda	k1gFnPc2	etuda
(	(	kIx(	(
<g/>
12	[number]	k4	12
v	v	k7c6	v
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
v	v	k7c6	v
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
25	[number]	k4	25
a	a	k8xC	a
3	[number]	k4	3
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
bez	bez	k7c2	bez
opusového	opusový	k2eAgNnSc2d1	opusové
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
koncerty	koncert	k1gInPc1	koncert
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
11	[number]	k4	11
a	a	k8xC	a
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
Vydáváním	vydávání	k1gNnSc7	vydávání
souborných	souborný	k2eAgNnPc2d1	souborné
děl	dělo	k1gNnPc2	dělo
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
např.	např.	kA	např.
krakovské	krakovský	k2eAgNnSc1d1	Krakovské
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Polskie	Polskius	k1gMnSc5	Polskius
Wydawnictwo	Wydawnictwa	k1gMnSc5	Wydawnictwa
Muzycne	Muzycn	k1gMnSc5	Muzycn
<g/>
,	,	kIx,	,
aktuálně	aktuálně	k6eAd1	aktuálně
např.	např.	kA	např.
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Národní	národní	k2eAgFnSc6d1	národní
edici	edice	k1gFnSc6	edice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Ekiera	Ekier	k1gMnSc2	Ekier
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dřívější	dřívější	k2eAgFnSc6d1	dřívější
edici	edice	k1gFnSc6	edice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Paderewského	Paderewské	k1gNnSc2	Paderewské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opusová	opusový	k2eAgNnPc1d1	opusové
čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
studentských	studentský	k2eAgFnPc2d1	studentská
skladeb	skladba	k1gFnPc2	skladba
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgInSc7	první
vydaným	vydaný	k2eAgInSc7d1	vydaný
opusem	opus	k1gInSc7	opus
Klavírní	klavírní	k2eAgNnSc4d1	klavírní
rondo	rondo	k1gNnSc4	rondo
c	c	k0	c
moll	moll	k1gNnSc6	moll
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
opusové	opusový	k2eAgNnSc1d1	opusové
číslo	číslo	k1gNnSc1	číslo
použité	použitý	k2eAgNnSc1d1	Použité
Chopinem	Chopin	k1gMnSc7	Chopin
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
65	[number]	k4	65
<g/>
,	,	kIx,	,
přidělené	přidělený	k2eAgFnSc6d1	přidělená
Sonátě	sonáta	k1gFnSc6	sonáta
g	g	kA	g
moll	moll	k1gNnSc2	moll
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
si	se	k3xPyFc3	se
na	na	k7c6	na
smrtelném	smrtelný	k2eAgNnSc6d1	smrtelné
loži	lože	k1gNnSc6	lože
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
nepublikované	publikovaný	k2eNgInPc1d1	nepublikovaný
rukopisy	rukopis	k1gInPc1	rukopis
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Julian	Julian	k1gMnSc1	Julian
Fontana	Fontan	k1gMnSc4	Fontan
výběr	výběr	k1gInSc4	výběr
23	[number]	k4	23
dosud	dosud	k6eAd1	dosud
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
seskupil	seskupit	k5eAaPmAgMnS	seskupit
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
opusová	opusový	k2eAgNnPc4d1	opusové
čísla	číslo	k1gNnPc4	číslo
66	[number]	k4	66
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
17	[number]	k4	17
polských	polský	k2eAgFnPc2d1	polská
písní	píseň	k1gFnPc2	píseň
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
písně	píseň	k1gFnPc4	píseň
objevené	objevený	k2eAgFnPc4d1	objevená
později	pozdě	k6eAd2	pozdě
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgNnSc2	tento
opusového	opusový	k2eAgNnSc2d1	opusové
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díla	dílo	k1gNnPc1	dílo
vydaná	vydaný	k2eAgNnPc1d1	vydané
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
nenesou	nést	k5eNaImIp3nP	nést
opusová	opusový	k2eAgNnPc4d1	opusové
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
===	===	k?	===
</s>
</p>
<p>
<s>
VÁLEK	Válek	k1gMnSc1	Válek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Portréty	portrét	k1gInPc1	portrét
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
41	[number]	k4	41
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IWASZKIEWICZ	IWASZKIEWICZ	kA	IWASZKIEWICZ
<g/>
,	,	kIx,	,
Jaroslaw	Jaroslaw	k1gFnSc1	Jaroslaw
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
:	:	kIx,	:
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Simonides	Simonides	k1gMnSc1	Simonides
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgInPc4d1	hudební
profily	profil	k1gInPc4	profil
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LISZT	LISZT	kA	LISZT
<g/>
,	,	kIx,	,
Ferenc	Ferenc	k1gMnSc1	Ferenc
<g/>
.	.	kIx.	.
</s>
<s>
Fr.	Fr.	k?	Fr.
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Bělohlavý	bělohlavý	k2eAgMnSc1d1	bělohlavý
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Topičovy	topičův	k2eAgFnPc1d1	Topičova
bílé	bílý	k2eAgFnPc1d1	bílá
knihy	kniha	k1gFnPc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RUGGIERI	RUGGIERI	kA	RUGGIERI
<g/>
,	,	kIx,	,
Éve	Éve	k1gFnSc1	Éve
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Citový	citový	k2eAgInSc4d1	citový
itinerář	itinerář	k1gInSc4	itinerář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Iris	iris	k1gInSc1	iris
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7176	[number]	k4	7176
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
SIMONIDES	SIMONIDES	kA	SIMONIDES
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
354	[number]	k4	354
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MARTINEK	Martinek	k1gMnSc1	Martinek
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Opava	Opava	k1gFnSc1	Opava
<g/>
:	:	kIx,	:
Slezská	slezský	k2eAgFnSc1d1	Slezská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polská	polský	k2eAgFnSc1d1	polská
===	===	k?	===
</s>
</p>
<p>
<s>
CHOPIN	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
<g/>
.	.	kIx.	.
</s>
<s>
Korespondencja	Korespondencja	k1gFnSc1	Korespondencja
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
Wydawnictwa	Wydawnictwa	k1gFnSc1	Wydawnictwa
Uniwersytetu	Uniwersytet	k1gInSc2	Uniwersytet
Warszawskiego	Warszawskiego	k6eAd1	Warszawskiego
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788323504818	[number]	k4	9788323504818
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GORDON-SMITH	GORDON-SMITH	k?	GORDON-SMITH
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
Czytelnik	Czytelnik	k1gMnSc1	Czytelnik
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
8307015588	[number]	k4	8307015588
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STROMENGER	STROMENGER	kA	STROMENGER
<g/>
,	,	kIx,	,
Karol	Karol	k1gInSc1	Karol
<g/>
.	.	kIx.	.
</s>
<s>
Almanach	almanach	k1gInSc1	almanach
Chopinowski	Chopinowski	k1gNnSc1	Chopinowski
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
Kronika	kronika	k1gFnSc1	kronika
życia	życia	k1gFnSc1	życia
<g/>
,	,	kIx,	,
dzieło	dzieło	k1gNnSc1	dzieło
<g/>
,	,	kIx,	,
bibliografia	bibliografia	k1gFnSc1	bibliografia
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
ikonografia	ikonografia	k1gFnSc1	ikonografia
<g/>
,	,	kIx,	,
varia	varia	k1gNnPc1	varia
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
Czytelnik	Czytelnik	k1gMnSc1	Czytelnik
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMASZEWSKI	TOMASZEWSKI	kA	TOMASZEWSKI
<g/>
,	,	kIx,	,
Mieczysław	Mieczysław	k1gFnSc1	Mieczysław
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
:	:	kIx,	:
człowiek	człowiek	k1gMnSc1	człowiek
<g/>
,	,	kIx,	,
dzieło	dzieło	k1gMnSc1	dzieło
<g/>
,	,	kIx,	,
rezonans	rezonans	k1gInSc1	rezonans
<g/>
.	.	kIx.	.
</s>
<s>
Poznaň	Poznaň	k1gFnSc1	Poznaň
<g/>
:	:	kIx,	:
Podsiedlik-Raniowski	Podsiedlik-Raniowski	k1gNnSc1	Podsiedlik-Raniowski
i	i	k8xC	i
Spółka	Spółka	k1gFnSc1	Spółka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7212	[number]	k4	7212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ZAMOYSKI	ZAMOYSKI	kA	ZAMOYSKI
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
Państwowy	Państwowa	k1gFnPc1	Państwowa
Instytut	Instytut	k1gInSc4	Instytut
Wydawniczy	Wydawnicza	k1gFnSc2	Wydawnicza
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Biografie	biografie	k1gFnSc1	biografie
Sławnych	Sławnych	k1gInSc1	Sławnych
Ludzi	Ludze	k1gFnSc4	Ludze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
8306012348	[number]	k4	8306012348
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAKUBOWSKI	JAKUBOWSKI	kA	JAKUBOWSKI
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zygmunt	Zygmunt	k1gMnSc1	Zygmunt
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
polska	polska	k1gFnSc1	polska
od	od	k7c2	od
średniowiecza	średniowiecz	k1gMnSc2	średniowiecz
to	ten	k3xDgNnSc1	ten
pozytywizmu	pozytywizmus	k1gInSc6	pozytywizmus
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
Państwowe	Państwowe	k1gInSc1	Państwowe
Wydawnictwo	Wydawnictwo	k1gNnSc1	Wydawnictwo
Naukowe	Naukowe	k1gInSc1	Naukowe
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JACHIMECKI	JACHIMECKI	kA	JACHIMECKI
<g/>
,	,	kIx,	,
Zdzisław	Zdzisław	k1gFnSc1	Zdzisław
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Franciszek	Franciszek	k1gInSc1	Franciszek
<g/>
.	.	kIx.	.
</s>
<s>
Polski	Polski	k6eAd1	Polski
słownik	słownik	k1gInSc1	słownik
biograficzny	biograficzna	k1gFnSc2	biograficzna
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgFnSc1wB	čís
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
,	,	kIx,	,
s.	s.	k?	s.
420	[number]	k4	420
<g/>
–	–	k?	–
<g/>
426	[number]	k4	426
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
3639	[number]	k4	3639
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOBYLAŃSKA	KOBYLAŃSKA	kA	KOBYLAŃSKA
<g/>
,	,	kIx,	,
Krystyna	Krystyno	k1gNnSc2	Krystyno
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
w	w	k?	w
kraju	kraju	k5eAaPmIp1nS	kraju
:	:	kIx,	:
Dokumenty	dokument	k1gInPc1	dokument
i	i	k8xC	i
pamiątki	pamiątk	k1gFnPc1	pamiątk
<g/>
.	.	kIx.	.
</s>
<s>
Krakov	Krakov	k1gInSc1	Krakov
<g/>
:	:	kIx,	:
Polskie	Polskius	k1gMnSc5	Polskius
Wydawanictwo	Wydawanictwa	k1gMnSc5	Wydawanictwa
Muzycne	Muzycn	k1gMnSc5	Muzycn
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZIELIŃSKI	ZIELIŃSKI	kA	ZIELIŃSKI
<g/>
,	,	kIx,	,
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
A.	A.	kA	A.
Chopin	Chopin	k1gMnSc1	Chopin
–	–	k?	–
Życie	Życie	k1gFnSc1	Życie
i	i	k8xC	i
droga	droga	k1gFnSc1	droga
twórcza	twórcza	k1gFnSc1	twórcza
<g/>
.	.	kIx.	.
</s>
<s>
Krakov	Krakov	k1gInSc1	Krakov
<g/>
:	:	kIx,	:
Polskie	Polskie	k1gFnSc1	Polskie
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Muzyczne	Muzyczne	k1gMnSc1	Muzyczne
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
224	[number]	k4	224
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
456	[number]	k4	456
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anglická	anglický	k2eAgNnPc4d1	anglické
===	===	k?	===
</s>
</p>
<p>
<s>
MICHAŁOWSKI	MICHAŁOWSKI	kA	MICHAŁOWSKI
<g/>
,	,	kIx,	,
Kornel	Kornel	k1gMnSc1	Kornel
<g/>
;	;	kIx,	;
SAMSON	Samson	k1gMnSc1	Samson
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Franciszek	Franciszek	k1gInSc1	Franciszek
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
L.	L.	kA	L.
Macy	Macy	k1gInPc1	Macy
<g/>
.	.	kIx.	.
</s>
<s>
Grove	Grovat	k5eAaPmIp3nS	Grovat
Music	Music	k1gMnSc1	Music
Online	Onlin	k1gInSc5	Onlin
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zpoplatněný	zpoplatněný	k2eAgInSc1d1	zpoplatněný
přístup	přístup	k1gInSc1	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEDLEY	HEDLEY	kA	HEDLEY
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Frédéric	Frédéric	k1gMnSc1	Frédéric
(	(	kIx(	(
<g/>
François	François	k1gInSc1	François
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannica	k1gMnSc1	Britannica
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
263	[number]	k4	263
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EISLER	EISLER	kA	EISLER
<g/>
,	,	kIx,	,
Benita	Benitum	k1gNnSc2	Benitum
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Funeral	Funeral	k1gFnSc7	Funeral
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Abacus	Abacus	k1gMnSc1	Abacus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
375409459	[number]	k4	375409459
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PONIATOWSKA	PONIATOWSKA	kA	PONIATOWSKA
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
and	and	k?	and
His	his	k1gNnPc2	his
Work	Worko	k1gNnPc2	Worko
in	in	k?	in
the	the	k?	the
Context	Context	k1gInSc1	Context
of	of	k?	of
Culture	Cultur	k1gInSc5	Cultur
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
the	the	k?	the
Second	Second	k1gMnSc1	Second
International	International	k1gMnSc1	International
Musicological	Musicological	k1gMnSc1	Musicological
Congress	Congressa	k1gFnPc2	Congressa
<g/>
,	,	kIx,	,
Warsaw	Warsaw	k1gFnPc2	Warsaw
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
October	October	k1gInSc1	October
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788370991272	[number]	k4	9788370991272
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SZULC	SZULC	kA	SZULC
<g/>
,	,	kIx,	,
Tad	Tad	k1gFnSc1	Tad
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Times	Times	k1gMnSc1	Times
of	of	k?	of
the	the	k?	the
Romantic	Romantice	k1gFnPc2	Romantice
Composer	Composer	k1gMnSc1	Composer
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Scribner	Scribner	k1gInSc1	Scribner
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
684	[number]	k4	684
<g/>
-	-	kIx~	-
<g/>
82458	[number]	k4	82458
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAMSON	Samson	k1gMnSc1	Samson
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
816495	[number]	k4	816495
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SIEPMANN	SIEPMANN	kA	SIEPMANN
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc7	Jerem
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Reluctant	Reluctant	k1gMnSc1	Reluctant
Romantic	Romantice	k1gFnPc2	Romantice
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Victor	Victor	k1gMnSc1	Victor
Gollancz	Gollancz	k1gMnSc1	Gollancz
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
575	[number]	k4	575
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5692	[number]	k4	5692
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAMSON	Samson	k1gMnSc1	Samson
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cambridge	Cambridge	k1gFnSc1	Cambridge
Companion	Companion	k1gInSc1	Companion
to	ten	k3xDgNnSc1	ten
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780521477529	[number]	k4	9780521477529
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAMSON	Samson	k1gMnSc1	Samson
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Music	Music	k1gMnSc1	Music
of	of	k?	of
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gNnSc1	Routledge
and	and	k?	and
Kegan	Kegan	k1gMnSc1	Kegan
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
198164029	[number]	k4	198164029
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EIGELDINGER	EIGELDINGER	kA	EIGELDINGER
<g/>
,	,	kIx,	,
Jean-Jacques	Jean-Jacques	k1gInSc1	Jean-Jacques
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
:	:	kIx,	:
Pianist	Pianist	k1gInSc1	Pianist
and	and	k?	and
Teacher	Teachra	k1gFnPc2	Teachra
<g/>
,	,	kIx,	,
as	as	k1gNnSc1	as
Seen	Seena	k1gFnPc2	Seena
by	by	k9	by
His	his	k1gNnSc1	his
Pupils	Pupilsa	k1gFnPc2	Pupilsa
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
36709	[number]	k4	36709
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ABRAHAM	Abraham	k1gMnSc1	Abraham
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
Frédéric	Frédéric	k1gMnSc1	Frédéric
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Encyclopedia	Encyclopedium	k1gNnPc1	Encyclopedium
Americana	Americana	k1gFnSc1	Americana
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780717201396	[number]	k4	9780717201396
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZAMOYSKI	ZAMOYSKI	kA	ZAMOYSKI
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
:	:	kIx,	:
A	A	kA	A
Biography	Biograph	k1gInPc1	Biograph
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Doubleday	Doubleday	k1gInPc1	Doubleday
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
13597	[number]	k4	13597
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAREK	Marek	k1gMnSc1	Marek
R.	R.	kA	R.
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
<g/>
;	;	kIx,	;
GORDON-SMITH	GORDON-SMITH	k1gFnSc1	GORDON-SMITH
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
:	:	kIx,	:
A	A	kA	A
biography	biograph	k1gInPc1	biograph
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Harper	Harper	k1gMnSc1	Harper
&	&	k?	&
Row	Row	k1gMnSc1	Row
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780060128432	[number]	k4	9780060128432
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOYNICH	VOYNICH	kA	VOYNICH
<g/>
,	,	kIx,	,
E.L.	E.L.	k1gMnSc1	E.L.
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Dover	Dover	k1gInSc1	Dover
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
486255644	[number]	k4	486255644
<g/>
.	.	kIx.	.
</s>
<s>
Sesbíral	sesbírat	k5eAaPmAgMnS	sesbírat
Henryk	Henryk	k1gMnSc1	Henryk
Opieński	Opieńsk	k1gFnSc2	Opieńsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LISSA	LISSA	kA	LISSA
<g/>
,	,	kIx,	,
Zofia	Zofium	k1gNnSc2	Zofium
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Book	Book	k1gInSc1	Book
of	of	k?	of
the	the	k?	the
First	First	k1gMnSc1	First
International	International	k1gMnSc1	International
Musicological	Musicological	k1gMnSc1	Musicological
Congress	Congressa	k1gFnPc2	Congressa
Devoted	Devoted	k1gMnSc1	Devoted
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Works	Works	kA	Works
of	of	k?	of
Frederick	Frederick	k1gMnSc1	Frederick
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Warsaw	Warsaw	k1gMnSc1	Warsaw
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
February	Februara	k1gFnSc2	Februara
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
755	[number]	k4	755
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
900938207	[number]	k4	900938207
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEDLEY	HEDLEY	kA	HEDLEY
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
.	.	kIx.	.
</s>
<s>
Selected	Selected	k1gMnSc1	Selected
Correspondence	Correspondence	k1gFnSc2	Correspondence
of	of	k?	of
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Da	Da	k1gMnSc3	Da
Capo	capa	k1gFnSc5	capa
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780306795794	[number]	k4	9780306795794
<g/>
.	.	kIx.	.
</s>
<s>
Sesbíral	sesbírat	k5eAaPmAgMnS	sesbírat
B.E.	B.E.	k1gMnSc1	B.E.
Sydow	Sydow	k1gMnSc1	Sydow
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FINCK	FINCK	kA	FINCK
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
T.	T.	kA	T.
Chopin	Chopin	k1gMnSc1	Chopin
and	and	k?	and
Other	Other	k1gInSc1	Other
Musical	musical	k1gInSc1	musical
Essays	Essays	k1gInSc1	Essays
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německá	německý	k2eAgFnSc1d1	německá
===	===	k?	===
</s>
</p>
<p>
<s>
BAUR	BAUR	kA	BAUR
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Gesine	Gesin	k1gMnSc5	Gesin
<g/>
.	.	kIx.	.
</s>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
oder	odra	k1gFnPc2	odra
Die	Die	k1gMnSc1	Die
Sehnsucht	Sehnsucht	k1gMnSc1	Sehnsucht
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
:	:	kIx,	:
C.	C.	kA	C.
H.	H.	kA	H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
406	[number]	k4	406
<g/>
-	-	kIx~	-
<g/>
59056	[number]	k4	59056
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BURGER	BURGER	kA	BURGER
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k6eAd1	Eine
Lebenschronik	Lebenschronik	k1gMnSc1	Lebenschronik
in	in	k?	in
Bildern	Bildern	k1gMnSc1	Bildern
und	und	k?	und
Dokumenten	Dokumentno	k1gNnPc2	Dokumentno
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
:	:	kIx,	:
Hirmer	Hirmer	k1gInSc1	Hirmer
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7774	[number]	k4	7774
<g/>
-	-	kIx~	-
<g/>
5370	[number]	k4	5370
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOURNIQUEL	BOURNIQUEL	kA	BOURNIQUEL
<g/>
,	,	kIx,	,
Camille	Camille	k1gNnSc2	Camille
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Mit	Mit	k?	Mit
Selbstzeugnissen	Selbstzeugnissen	k2eAgInSc1d1	Selbstzeugnissen
und	und	k?	und
Bilddokumenten	Bilddokumentno	k1gNnPc2	Bilddokumentno
<g/>
.	.	kIx.	.
</s>
<s>
Reinbek	Reinbek	k1gMnSc1	Reinbek
<g/>
:	:	kIx,	:
Rowohlt	Rowohlt	k1gMnSc1	Rowohlt
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
499	[number]	k4	499
<g/>
-	-	kIx~	-
<g/>
50025	[number]	k4	50025
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LEICHTENTRITT	LEICHTENTRITT	kA	LEICHTENTRITT
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Nabu	Naba	k1gFnSc4	Naba
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1279176863	[number]	k4	1279176863
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LOTZ	LOTZ	kA	LOTZ
<g/>
,	,	kIx,	,
Jürgen	Jürgen	k1gInSc1	Jürgen
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
.	.	kIx.	.
</s>
<s>
Reinbek	Reinbek	k1gMnSc1	Reinbek
<g/>
:	:	kIx,	:
Rowohlt	Rowohlt	k1gMnSc1	Rowohlt
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
499	[number]	k4	499
<g/>
-	-	kIx~	-
<g/>
50564	[number]	k4	50564
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PLÖGER	PLÖGER	kA	PLÖGER
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
.	.	kIx.	.
</s>
<s>
Auf	Auf	k?	Auf
der	drát	k5eAaImRp2nS	drát
Suche	Suche	k1gNnSc4	Suche
nach	nach	k1gInSc1	nach
seinem	seino	k1gNnSc7	seino
Leben	Leben	k2eAgMnSc1d1	Leben
<g/>
:	:	kIx,	:
Auf	Auf	k1gMnSc1	Auf
Chopins	Chopinsa	k1gFnPc2	Chopinsa
Wegen	Wegna	k1gFnPc2	Wegna
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
:	:	kIx,	:
Schweikert-Bonn-Verlag	Schweikert-Bonn-Verlag	k1gInSc1	Schweikert-Bonn-Verlag
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
940259	[number]	k4	940259
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WÜST	WÜST	kA	WÜST
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Werner	Werner	k1gMnSc1	Werner
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
–	–	k?	–
Briefe	Brief	k1gInSc5	Brief
und	und	k?	und
Zeitzeugnisse	Zeitzeugniss	k1gMnSc4	Zeitzeugniss
<g/>
,	,	kIx,	,
Ein	Ein	k1gMnSc1	Ein
Portrait	Portrait	k1gMnSc1	Portrait
<g/>
.	.	kIx.	.
</s>
<s>
Bonn	Bonn	k1gInSc1	Bonn
<g/>
:	:	kIx,	:
Bouvier	Bouvier	k1gInSc1	Bouvier
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
416	[number]	k4	416
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3164	[number]	k4	3164
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgFnSc1d1	jiná
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
BASTET	BASTET	kA	BASTET
<g/>
,	,	kIx,	,
Frédéric	Frédéric	k1gMnSc1	Frédéric
L.	L.	kA	L.
Helse	Helse	k1gFnSc1	Helse
liefde	liefde	k6eAd1	liefde
<g/>
:	:	kIx,	:
Biografisch	Biografisch	k1gInSc1	Biografisch
essay	essaa	k1gFnSc2	essaa
over	overa	k1gFnPc2	overa
Marie	Maria	k1gFnSc2	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
<g/>
,	,	kIx,	,
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Sand	Sand	k1gInSc1	Sand
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
:	:	kIx,	:
Querido	Querida	k1gFnSc5	Querida
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
214	[number]	k4	214
<g/>
-	-	kIx~	-
<g/>
5157	[number]	k4	5157
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
MAUROIS	MAUROIS	kA	MAUROIS
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
<g/>
.	.	kIx.	.
</s>
<s>
Lelia	Lelia	k1gFnSc1	Lelia
ou	ou	k0	ou
la	la	k1gNnPc1	la
Vie	Vie	k1gMnSc2	Vie
de	de	k?	de
George	Georg	k1gMnSc4	Georg
Sand	Sand	k1gMnSc1	Sand
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
French	French	k1gInSc1	French
&	&	k?	&
European	European	k1gInSc1	European
Pubns	Pubnsa	k1gFnPc2	Pubnsa
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
685369432	[number]	k4	685369432
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
</s>
</p>
<p>
<s>
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Chopinův	Chopinův	k2eAgInSc1d1	Chopinův
festival	festival	k1gInSc1	festival
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
Společnosti	společnost	k1gFnSc2	společnost
Fryderyka	Fryderyek	k1gMnSc4	Fryderyek
Chopina	Chopin	k1gMnSc4	Chopin
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
společností	společnost	k1gFnPc2	společnost
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Polská	polský	k2eAgFnSc1d1	polská
společnosti	společnost	k1gFnPc1	společnost
Fryderyka	Fryderyek	k1gMnSc2	Fryderyek
Chopina	Chopin	k1gMnSc2	Chopin
</s>
</p>
<p>
<s>
Biografie	biografie	k1gFnSc1	biografie
v	v	k7c6	v
Projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Life	Life	k6eAd1	Life
of	of	k?	of
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
</s>
</p>
<p>
<s>
Frederick	Frederick	k1gMnSc1	Frederick
Chopin	Chopin	k1gMnSc1	Chopin
as	as	k1gNnPc2	as
a	a	k8xC	a
Man	mana	k1gFnPc2	mana
and	and	k?	and
Musician	Musician	k1gMnSc1	Musician
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
Niecks	Niecksa	k1gFnPc2	Niecksa
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Man	mana	k1gFnPc2	mana
and	and	k?	and
his	his	k1gNnSc1	his
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Huneker	Huneker	k1gMnSc1	Huneker
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
F.	F.	kA	F.
Chopina	Chopin	k1gMnSc2	Chopin
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
