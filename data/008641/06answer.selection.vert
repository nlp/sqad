<s>
Janna	Janna	k1gFnSc1	Janna
J.	J.	kA	J.
Levinová	Levinová	k1gFnSc1	Levinová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
teoretická	teoretický	k2eAgFnSc1d1	teoretická
kosmoložka	kosmoložka	k1gFnSc1	kosmoložka
<g/>
,	,	kIx,	,
popularizátorka	popularizátorka	k1gFnSc1	popularizátorka
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
docentka	docentka	k1gFnSc1	docentka
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Barnardově	Barnardův	k2eAgFnSc6d1	Barnardova
koleji	kolej	k1gFnSc6	kolej
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
