<p>
<s>
Janna	Janna	k1gFnSc1	Janna
J.	J.	kA	J.
Levinová	Levinová	k1gFnSc1	Levinová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
teoretická	teoretický	k2eAgFnSc1d1	teoretická
kosmoložka	kosmoložka	k1gFnSc1	kosmoložka
<g/>
,	,	kIx,	,
popularizátorka	popularizátorka	k1gFnSc1	popularizátorka
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
docentka	docentka	k1gFnSc1	docentka
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Barnardově	Barnardův	k2eAgFnSc6d1	Barnardova
koleji	kolej	k1gFnSc6	kolej
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
raného	raný	k2eAgInSc2d1	raný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc4	teorie
chaosu	chaos	k1gInSc2	chaos
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
především	především	k9	především
topologii	topologie	k1gFnSc4	topologie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
dilematu	dilema	k1gNnSc2	dilema
jeho	jeho	k3xOp3gFnSc2	jeho
konečnosti	konečnost	k1gFnSc2	konečnost
či	či	k8xC	či
nekonečnosti	nekonečnost	k1gFnSc2	nekonečnost
<g/>
,	,	kIx,	,
kosmologickým	kosmologický	k2eAgInPc3d1	kosmologický
aspektům	aspekt	k1gInPc3	aspekt
teorie	teorie	k1gFnSc2	teorie
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
více	hodně	k6eAd2	hodně
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
gravitačním	gravitační	k2eAgFnPc3d1	gravitační
vlnám	vlna	k1gFnPc3	vlna
v	v	k7c6	v
modelu	model	k1gInSc6	model
prostoročasu	prostoročas	k1gInSc2	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
jako	jako	k9	jako
ředitelka	ředitelka	k1gFnSc1	ředitelka
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
v	v	k7c6	v
brooklynském	brooklynský	k2eAgNnSc6d1	Brooklynské
středisku	středisko	k1gNnSc6	středisko
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
inovace	inovace	k1gFnSc2	inovace
Pioneer	Pioneer	kA	Pioneer
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
ukončila	ukončit	k5eAaPmAgFnS	ukončit
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
program	program	k1gInSc4	program
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Barnardově	Barnardův	k2eAgFnSc6d1	Barnardova
koleji	kolej	k1gFnSc6	kolej
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
filosofii	filosofie	k1gFnSc6	filosofie
(	(	kIx(	(
<g/>
B.A.	B.A.	k1gFnSc6	B.A.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
studia	studio	k1gNnSc2	studio
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
projekty	projekt	k1gInPc4	projekt
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
částicové	částicový	k2eAgFnSc2d1	částicová
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
Cambridgeské	cambridgeský	k2eAgFnSc2d1	Cambridgeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
koleje	kolej	k1gFnSc2	kolej
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikační	publikační	k2eAgFnSc1d1	publikační
a	a	k8xC	a
přednášková	přednáškový	k2eAgFnSc1d1	přednášková
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydala	vydat	k5eAaPmAgFnS	vydat
román	román	k1gInSc4	román
A	a	k9	a
Madman	Madman	k1gMnSc1	Madman
Dreams	Dreamsa	k1gFnPc2	Dreamsa
of	of	k?	of
Turing	Turing	k1gInSc1	Turing
Machines	Machines	k1gInSc1	Machines
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslovně	doslovně	k6eAd1	doslovně
<g/>
:	:	kIx,	:
Šílenec	šílenec	k1gMnSc1	šílenec
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
Turingových	Turingový	k2eAgInPc6d1	Turingový
strojích	stroj	k1gInPc6	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
beletrizovala	beletrizovat	k5eAaImAgFnS	beletrizovat
životy	život	k1gInPc4	život
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc4	úmrtí
matematiků	matematik	k1gMnPc2	matematik
Kurta	Kurt	k1gMnSc4	Kurt
Gödela	Gödel	k1gMnSc4	Gödel
a	a	k8xC	a
Alana	Alan	k1gMnSc4	Alan
Turinga	Turing	k1gMnSc4	Turing
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepotkali	potkat	k5eNaPmAgMnP	potkat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
ji	on	k3xPp3gFnSc4	on
dovedlo	dovést	k5eAaPmAgNnS	dovést
pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
hranicích	hranice	k1gFnPc6	hranice
vědění	vědění	k1gNnSc2	vědění
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
PEN	PEN	kA	PEN
Klub	klub	k1gInSc1	klub
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
udělil	udělit	k5eAaPmAgInS	udělit
Cenu	cena	k1gFnSc4	cena
Roberta	Robert	k1gMnSc2	Robert
W.	W.	kA	W.
Binghama	Bingham	k1gMnSc2	Bingham
a	a	k8xC	a
obdržela	obdržet	k5eAaPmAgFnS	obdržet
také	také	k9	také
Cenu	cena	k1gFnSc4	cena
Mary	Mary	k1gFnSc2	Mary
Shelleyové	Shelleyová	k1gFnSc2	Shelleyová
<g/>
.	.	kIx.	.
<g/>
Esejemi	esej	k1gFnPc7	esej
doprovodila	doprovodit	k5eAaPmAgFnS	doprovodit
umělecké	umělecký	k2eAgFnPc4d1	umělecká
výstavy	výstava	k1gFnPc4	výstava
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
galeriích	galerie	k1gFnPc6	galerie
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
malířská	malířský	k2eAgFnSc1d1	malířská
akademie	akademie	k1gFnSc1	akademie
Ruskin	Ruskin	k1gMnSc1	Ruskin
School	School	k1gInSc1	School
of	of	k?	of
Drawing	Drawing	k1gInSc1	Drawing
and	and	k?	and
Fine	Fin	k1gMnSc5	Fin
Art	Art	k1gMnSc5	Art
nebo	nebo	k8xC	nebo
Hayward	Hayward	k1gInSc4	Hayward
Gallery	Galler	k1gInPc1	Galler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
TED	Ted	k1gMnSc1	Ted
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
přednesla	přednést	k5eAaPmAgFnS	přednést
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
příspěvek	příspěvek	k1gInSc1	příspěvek
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
sound	sound	k1gMnSc1	sound
the	the	k?	the
universe	universe	k1gFnSc1	universe
makes	makes	k1gMnSc1	makes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vydává	vydávat	k5eAaPmIp3nS	vydávat
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
příjemkyní	příjemkyně	k1gFnSc7	příjemkyně
Guggenheimova	Guggenheimův	k2eAgNnSc2d1	Guggenheimovo
stipendia	stipendium	k1gNnSc2	stipendium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
dílaJak	dílaJak	k1gMnSc1	dílaJak
vesmír	vesmír	k1gInSc4	vesmír
přišel	přijít	k5eAaPmAgMnS	přijít
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
skvrnám	skvrna	k1gFnPc3	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
o	o	k7c6	o
konečném	konečný	k2eAgInSc6d1	konečný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
z	z	k7c2	z
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
How	How	k?	How
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
Got	Got	k1gFnSc2	Got
Its	Its	k1gFnSc2	Its
Spots	Spots	k1gInSc1	Spots
<g/>
:	:	kIx,	:
Diary	Diara	k1gFnPc1	Diara
of	of	k?	of
a	a	k8xC	a
Finite	Finit	k1gInSc5	Finit
Time	Time	k1gFnPc7	Time
in	in	k?	in
a	a	k8xC	a
Finite	Finit	k1gMnSc5	Finit
Space	Space	k1gMnSc5	Space
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
504	[number]	k4	504
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Madman	Madman	k1gMnSc1	Madman
Dreams	Dreamsa	k1gFnPc2	Dreamsa
of	of	k?	of
Turing	Turing	k1gInSc1	Turing
Machines	Machines	k1gInSc1	Machines
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
blues	blues	k1gNnSc4	blues
<g/>
:	:	kIx,	:
Černé	Černé	k2eAgFnSc2d1	Černé
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc2d1	gravitační
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
epochálního	epochální	k2eAgInSc2d1	epochální
objevu	objev	k1gInSc2	objev
(	(	kIx(	(
<g/>
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
z	z	k7c2	z
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k6eAd1	Black
Hole	hole	k1gFnSc1	hole
Blues	blues	k1gFnSc2	blues
and	and	k?	and
Other	Other	k1gInSc1	Other
Songs	Songsa	k1gFnPc2	Songsa
from	from	k1gMnSc1	from
Outer	Outer	k1gMnSc1	Outer
Space	Space	k1gMnSc1	Space
<g/>
;	;	kIx,	;
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
LIGO	liga	k1gFnSc5	liga
a	a	k8xC	a
detekci	detekce	k1gFnSc3	detekce
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
746	[number]	k4	746
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
matematice	matematika	k1gFnSc3	matematika
ani	ani	k8xC	ani
fyzice	fyzika	k1gFnSc3	fyzika
nevěnovala	věnovat	k5eNaImAgFnS	věnovat
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
školu	škola	k1gFnSc4	škola
neukončila	ukončit	k5eNaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
se	se	k3xPyFc4	se
před	před	k7c7	před
jejími	její	k3xOp3gInPc7	její
sedmnáctými	sedmnáctý	k4xOgInPc7	sedmnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
stala	stát	k5eAaPmAgFnS	stát
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
cestovala	cestovat	k5eAaImAgFnS	cestovat
<g/>
,	,	kIx,	,
narazilo	narazit	k5eAaPmAgNnS	narazit
do	do	k7c2	do
mostu	most	k1gInSc2	most
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
do	do	k7c2	do
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
pak	pak	k6eAd1	pak
strávila	strávit	k5eAaPmAgFnS	strávit
období	období	k1gNnSc3	období
konce	konec	k1gInSc2	konec
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
Barnardovu	Barnardův	k2eAgFnSc4d1	Barnardova
kolej	kolej	k1gFnSc4	kolej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Janna	Jann	k1gInSc2	Jann
Levin	Levina	k1gFnPc2	Levina
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Janna	Jann	k1gInSc2	Jann
Levin	Levina	k1gFnPc2	Levina
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
na	na	k7c6	na
TEDu	Ted	k1gMnSc6	Ted
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Janna	Janen	k2eAgFnSc1d1	Janna
Levinová	Levinová	k1gFnSc1	Levinová
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
