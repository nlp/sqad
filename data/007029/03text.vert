<s>
Válečná	válečný	k2eAgFnSc1d1	válečná
fotografie	fotografie	k1gFnSc1	fotografie
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
fotografie	fotografia	k1gFnPc4	fotografia
z	z	k7c2	z
vojenských	vojenský	k2eAgInPc2d1	vojenský
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
život	život	k1gInSc4	život
ve	v	k7c6	v
válečných	válečný	k2eAgFnPc6d1	válečná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
hrůzy	hrůza	k1gFnPc4	hrůza
války	válka	k1gFnSc2	válka
včetně	včetně	k7c2	včetně
hrdinských	hrdinský	k2eAgInPc2d1	hrdinský
lidských	lidský	k2eAgInPc2d1	lidský
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
malby	malba	k1gFnSc2	malba
a	a	k8xC	a
kresby	kresba	k1gFnSc2	kresba
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
nejsou	být	k5eNaImIp3nP	být
snadno	snadno	k6eAd1	snadno
zaměnitelné	zaměnitelný	k2eAgFnPc1d1	zaměnitelná
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázán	k2eAgNnSc1d1	dokázáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografové	fotograf	k1gMnPc1	fotograf
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
a	a	k8xC	a
zobrazovanou	zobrazovaný	k2eAgFnSc7d1	zobrazovaná
scénou	scéna	k1gFnSc7	scéna
manipulovali	manipulovat	k5eAaImAgMnP	manipulovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografie	k1gFnSc1	fotografie
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
zcela	zcela	k6eAd1	zcela
objektivní	objektivní	k2eAgInSc4d1	objektivní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
přesnou	přesný	k2eAgFnSc7d1	přesná
reprezentací	reprezentace	k1gFnSc7	reprezentace
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
bylo	být	k5eAaImAgNnS	být
předpovězeno	předpovězen	k2eAgNnSc1d1	předpovězeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografie	k1gFnSc1	fotografie
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vizuálně	vizuálně	k6eAd1	vizuálně
dokumentovat	dokumentovat	k5eAaBmF	dokumentovat
budoucí	budoucí	k2eAgFnSc2d1	budoucí
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
dokumentovat	dokumentovat	k5eAaBmF	dokumentovat
bitvy	bitva	k1gFnPc4	bitva
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnPc4	pevnost
<g/>
,	,	kIx,	,
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
vojenské	vojenský	k2eAgMnPc4d1	vojenský
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Louis	Louis	k1gMnSc1	Louis
Daguerre	Daguerr	k1gMnSc5	Daguerr
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc5	vynálezce
prvního	první	k4xOgInSc2	první
komerčního	komerční	k2eAgInSc2d1	komerční
fotografického	fotografický	k2eAgInSc2d1	fotografický
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
kamerou	kamera	k1gFnSc7	kamera
obscurou	obscura	k1gFnSc7	obscura
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgInSc1d1	absolutní
pravdou	pravda	k1gFnSc7	pravda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nekonečně	konečně	k6eNd1	konečně
přesnější	přesný	k2eAgMnSc1d2	přesnější
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
obraz	obraz	k1gInSc1	obraz
lidské	lidský	k2eAgFnSc2d1	lidská
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Fotografie	fotografia	k1gFnPc1	fotografia
skutečně	skutečně	k6eAd1	skutečně
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
pro	pro	k7c4	pro
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
systematicky	systematicky	k6eAd1	systematicky
fotografovanou	fotografovaný	k2eAgFnSc4d1	fotografovaná
válečnou	válečná	k1gFnSc4	válečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
byla	být	k5eAaImAgFnS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
informována	informovat	k5eAaBmNgFnS	informovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zpráv	zpráva	k1gFnPc2	zpráva
posílaných	posílaný	k2eAgFnPc2d1	posílaná
telegrafem	telegraf	k1gInSc7	telegraf
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
reportérem	reportér	k1gMnSc7	reportér
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
William	William	k1gInSc4	William
Howard	Howarda	k1gFnPc2	Howarda
Russell	Russella	k1gFnPc2	Russella
z	z	k7c2	z
The	The	k1gFnSc2	The
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
novinář	novinář	k1gMnSc1	novinář
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
válečný	válečný	k2eAgMnSc1d1	válečný
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
fotoreportérem	fotoreportér	k1gMnSc7	fotoreportér
byl	být	k5eAaImAgMnS	být
Carol	Carola	k1gFnPc2	Carola
Szathmari	Szathmar	k1gFnSc2	Szathmar
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
pak	pak	k8xC	pak
britští	britský	k2eAgMnPc1d1	britský
reportéři	reportér	k1gMnPc1	reportér
William	William	k1gInSc4	William
Simpson	Simpsona	k1gFnPc2	Simpsona
z	z	k7c2	z
Illustrated	Illustrated	k1gInSc1	Illustrated
London	London	k1gMnSc1	London
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
Fenton	Fenton	k1gInSc1	Fenton
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Robertson	Robertson	k1gMnSc1	Robertson
a	a	k8xC	a
Felice	Felice	k1gFnSc1	Felice
Beato	Beato	k1gNnSc4	Beato
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
obrázky	obrázek	k1gInPc1	obrázek
byly	být	k5eAaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
otiskovány	otiskovat	k5eAaImNgFnP	otiskovat
jako	jako	k9	jako
rytiny	rytina	k1gFnPc1	rytina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tisk	tisk	k1gInSc1	tisk
polotónových	polotónový	k2eAgFnPc2d1	polotónová
fotografií	fotografia	k1gFnPc2	fotografia
byl	být	k5eAaImAgInS	být
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnPc3	jejich
zprávám	zpráva	k1gFnPc3	zpráva
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
povědomost	povědomost	k1gFnSc1	povědomost
o	o	k7c6	o
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
nekompetentnosti	nekompetentnost	k1gFnSc6	nekompetentnost
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1855	[number]	k4	1855
mohlo	moct	k5eAaImAgNnS	moct
také	také	k6eAd1	také
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
méně	málo	k6eAd2	málo
známý	známý	k2eAgMnSc1d1	známý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
zpravodaj	zpravodaj	k1gInSc4	zpravodaj
Sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
Němec	Němec	k1gMnSc1	Němec
Károly	Károla	k1gFnSc2	Károla
Pap	Pap	k1gFnSc2	Pap
Szathmáry	Szathmár	k1gInPc1	Szathmár
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Alessandriové	Alessandriový	k2eAgFnSc2d1	Alessandriový
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francesco	Francesco	k6eAd1	Francesco
Paolo	Paolo	k1gNnSc1	Paolo
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgMnPc4	první
válečné	válečný	k2eAgMnPc4d1	válečný
reportéry	reportér	k1gMnPc4	reportér
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
fotografovali	fotografovat	k5eAaImAgMnP	fotografovat
tábory	tábor	k1gInPc4	tábor
Zouaves	Zouaves	k1gMnSc1	Zouaves
pontificaux	pontificaux	k1gInSc4	pontificaux
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
rozsah	rozsah	k1gInSc1	rozsah
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Mentana	Mentan	k1gMnSc2	Mentan
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
dodával	dodávat	k5eAaImAgMnS	dodávat
fotograf	fotograf	k1gMnSc1	fotograf
Mathew	Mathew	k1gMnSc2	Mathew
Brady	Brada	k1gMnSc2	Brada
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
války	válka	k1gFnSc2	válka
"	"	kIx"	"
<g/>
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
jihu	jih	k1gInSc3	jih
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
generace	generace	k1gFnPc4	generace
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
osud	osud	k1gInSc4	osud
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
zachycovat	zachycovat	k5eAaImF	zachycovat
průběh	průběh	k1gInSc4	průběh
války	válka	k1gFnSc2	válka
na	na	k7c6	na
více	hodně	k6eAd2	hodně
bojištích	bojiště	k1gNnPc6	bojiště
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
asistentů	asistent	k1gMnPc2	asistent
několik	několik	k4yIc1	několik
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
cestovaly	cestovat	k5eAaImAgInP	cestovat
s	s	k7c7	s
pojízdnými	pojízdný	k2eAgFnPc7d1	pojízdná
laboratořemi	laboratoř	k1gFnPc7	laboratoř
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
frontách	fronta	k1gFnPc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Bradyho	Bradyze	k6eAd1	Bradyze
významným	významný	k2eAgMnSc7d1	významný
týmovým	týmový	k2eAgMnSc7d1	týmový
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
byl	být	k5eAaImAgMnS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Gardner	Gardner	k1gMnSc1	Gardner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Gardner	Gardner	k1gMnSc1	Gardner
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
většinou	většinou	k6eAd1	většinou
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bitev	bitva	k1gFnPc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhýbal	vyhýbat	k5eNaImAgInS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k9	ani
detailním	detailní	k2eAgInPc3d1	detailní
pohledům	pohled	k1gInPc3	pohled
na	na	k7c4	na
mrtvé	mrtvý	k2eAgInPc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejméně	málo	k6eAd3	málo
jedna	jeden	k4xCgFnSc1	jeden
Gardnerova	Gardnerův	k2eAgFnSc1d1	Gardnerova
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
zmanipulovaná	zmanipulovaný	k2eAgFnSc1d1	zmanipulovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
Frederic	Frederic	k1gMnSc1	Frederic
Ray	Ray	k1gMnSc1	Ray
z	z	k7c2	z
magazínu	magazín	k1gInSc2	magazín
Civil	civil	k1gMnSc1	civil
War	War	k1gMnSc1	War
Times	Times	k1gMnSc1	Times
srovnával	srovnávat	k5eAaImAgMnS	srovnávat
několik	několik	k4yIc4	několik
Gardnerových	Gardnerův	k2eAgFnPc2d1	Gardnerova
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
konfederační	konfederační	k2eAgMnPc1d1	konfederační
ostřelovači	ostřelovač	k1gMnPc1	ostřelovač
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejný	stejný	k2eAgMnSc1d1	stejný
padlý	padlý	k1gMnSc1	padlý
byl	být	k5eAaImAgMnS	být
fotografován	fotografovat	k5eAaImNgMnS	fotografovat
o	o	k7c4	o
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gardner	Gardner	k1gMnSc1	Gardner
nebyl	být	k5eNaImAgMnS	být
s	s	k7c7	s
kompozicí	kompozice	k1gFnSc7	kompozice
spokojen	spokojit	k5eAaPmNgMnS	spokojit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
těla	tělo	k1gNnSc2	tělo
různě	různě	k6eAd1	různě
zpřeházet	zpřeházet	k5eAaPmF	zpřeházet
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
tak	tak	k9	tak
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Rayovu	Rayův	k2eAgFnSc4d1	Rayova
analýzu	analýza	k1gFnSc4	analýza
ještě	ještě	k6eAd1	ještě
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
William	William	k1gInSc1	William
Frassanito	Frassanita	k1gFnSc5	Frassanita
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
N.	N.	kA	N.
Barnard	Barnarda	k1gFnPc2	Barnarda
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
známý	známý	k1gMnSc1	známý
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
snímky	snímek	k1gInPc7	snímek
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Barnard	Barnard	k1gInSc1	Barnard
posílán	posílán	k2eAgInSc1d1	posílán
fotografovat	fotografovat	k5eAaImF	fotografovat
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Washingtonu	Washington	k1gInSc2	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
portrétech	portrét	k1gInPc6	portrét
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgFnPc2d1	významná
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Timothy	Timoth	k1gInPc7	Timoth
H.	H.	kA	H.
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Sullivanem	Sullivan	k1gMnSc7	Sullivan
<g/>
,	,	kIx,	,
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Gardnerem	Gardner	k1gMnSc7	Gardner
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
asistenty	asistent	k1gMnPc7	asistent
slavného	slavný	k2eAgMnSc2d1	slavný
fotografa	fotograf	k1gMnSc2	fotograf
Mathew	Mathew	k1gMnSc2	Mathew
Bradyho	Brady	k1gMnSc2	Brady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Williamem	William	k1gInSc7	William
Tecumsehem	Tecumseh	k1gInSc7	Tecumseh
Shermanem	Sherman	k1gInSc7	Sherman
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
bitvu	bitva	k1gFnSc4	bitva
za	za	k7c4	za
Atlantu	Atlanta	k1gFnSc4	Atlanta
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
pochod	pochod	k1gInSc4	pochod
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgInPc4d1	výsledný
snímky	snímek	k1gInPc4	snímek
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Photographic	Photographic	k1gMnSc1	Photographic
Views	Viewsa	k1gFnPc2	Viewsa
of	of	k?	of
the	the	k?	the
Sherman	Sherman	k1gMnSc1	Sherman
Campaign	Campaign	k1gMnSc1	Campaign
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
William	William	k1gInSc1	William
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
Anglie	Anglie	k1gFnSc2	Anglie
-	-	kIx~	-
1910	[number]	k4	1910
USA	USA	kA	USA
<g/>
)	)	kIx)	)
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
choroby	choroba	k1gFnPc4	choroba
vojáků	voják	k1gMnPc2	voják
pro	pro	k7c4	pro
muzeum	muzeum	k1gNnSc4	muzeum
Army	Arma	k1gFnSc2	Arma
Medical	Medical	k1gFnSc2	Medical
Museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
strávil	strávit	k5eAaPmAgInS	strávit
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
dokumentací	dokumentace	k1gFnSc7	dokumentace
různých	různý	k2eAgNnPc2d1	různé
poranění	poranění	k1gNnPc2	poranění
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
amputací	amputace	k1gFnPc2	amputace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Medical	Medical	k1gFnSc2	Medical
and	and	k?	and
Surgical	Surgical	k1gFnSc2	Surgical
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
War	War	k1gFnSc2	War
of	of	k?	of
the	the	k?	the
Rebellion	Rebellion	k?	Rebellion
(	(	kIx(	(
<g/>
Historie	historie	k1gFnSc1	historie
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc2	chirurgie
válečné	válečný	k2eAgFnSc2d1	válečná
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Ricalton	Ricalton	k1gInSc1	Ricalton
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
opustil	opustit	k5eAaPmAgMnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaPmF	stát
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
fotografem	fotograf	k1gMnSc7	fotograf
a	a	k8xC	a
válečným	válečný	k2eAgInSc7d1	válečný
zpravodajem	zpravodaj	k1gInSc7	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
příštích	příští	k2eAgInPc2d1	příští
15	[number]	k4	15
let	léto	k1gNnPc2	léto
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
a	a	k8xC	a
zaznamenával	zaznamenávat	k5eAaImAgMnS	zaznamenávat
události	událost	k1gFnPc4	událost
z	z	k7c2	z
Španělsko-americké	španělskomerický	k2eAgFnSc2d1	španělsko-americká
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boxerské	boxerský	k2eAgNnSc1d1	boxerské
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rusko-japonskou	ruskoaponský	k2eAgFnSc4d1	rusko-japonská
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
japonské	japonský	k2eAgMnPc4d1	japonský
vojáky	voják	k1gMnPc4	voják
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Port	porta	k1gFnPc2	porta
Arthur	Arthura	k1gFnPc2	Arthura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
držen	držet	k5eAaImNgInS	držet
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k8xS	co
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
generála	generál	k1gMnSc2	generál
Nogi	Nog	k1gFnSc2	Nog
Maresukeho	Maresuke	k1gMnSc2	Maresuke
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
amerického	americký	k2eAgMnSc4d1	americký
fotografa	fotograf	k1gMnSc4	fotograf
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
může	moct	k5eAaImIp3nS	moct
fotografovat	fotografovat	k5eAaImF	fotografovat
co	co	k3yRnSc4	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Luis	Luisa	k1gFnPc2	Luisa
Ramón	Ramón	k1gMnSc1	Ramón
Marín	Marína	k1gFnPc2	Marína
(	(	kIx(	(
<g/>
*	*	kIx~	*
1884	[number]	k4	1884
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
–	–	k?	–
1944	[number]	k4	1944
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
během	během	k7c2	během
Španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
pořídil	pořídit	k5eAaPmAgMnS	pořídit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Madridu	Madrid	k1gInSc2	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předchůdců	předchůdce	k1gMnPc2	předchůdce
významných	významný	k2eAgMnPc2d1	významný
fotografů	fotograf	k1gMnPc2	fotograf
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
uznáván	uznáván	k2eAgMnSc1d1	uznáván
také	také	k9	také
voják	voják	k1gMnSc1	voják
Tony	Tony	k1gMnSc1	Tony
Vaccaro	Vaccara	k1gFnSc5	Vaccara
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
skromným	skromný	k2eAgInSc7d1	skromný
aparátem	aparát	k1gInSc7	aparát
Argus	Argus	k1gMnSc1	Argus
C3	C3	k1gMnSc1	C3
zachycoval	zachycovat	k5eAaImAgMnS	zachycovat
hrůzné	hrůzný	k2eAgInPc4d1	hrůzný
okamžiky	okamžik	k1gInPc4	okamžik
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Capa	cap	k1gMnSc2	cap
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
padl	padnout	k5eAaPmAgInS	padnout
<g/>
,	,	kIx,	,
stejným	stejný	k2eAgInSc7d1	stejný
aparátem	aparát	k1gInSc7	aparát
zachycoval	zachycovat	k5eAaImAgInS	zachycovat
stěžejní	stěžejní	k2eAgInPc4d1	stěžejní
momenty	moment	k1gInPc4	moment
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c4	v
Den	den	k1gInSc4	den
D	D	kA	D
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
Omaha	Omah	k1gMnSc2	Omah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
působili	působit	k5eAaImAgMnP	působit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
fotografové	fotograf	k1gMnPc1	fotograf
jako	jako	k9	jako
například	například	k6eAd1	například
David	David	k1gMnSc1	David
Seymour	Seymour	k1gMnSc1	Seymour
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Baltermanc	Baltermanc	k1gFnSc1	Baltermanc
<g/>
,	,	kIx,	,
Anatolij	Anatolij	k1gMnSc1	Anatolij
Garanin	Garanin	k2eAgMnSc1d1	Garanin
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
Kudojarov	Kudojarov	k1gInSc1	Kudojarov
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Chalděj	Chalděj	k1gMnSc1	Chalděj
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Trachman	Trachman	k1gMnSc1	Trachman
<g/>
,	,	kIx,	,
Arkadij	Arkadij	k1gMnSc1	Arkadij
Šajchet	Šajchet	k1gMnSc1	Šajchet
<g/>
,	,	kIx,	,
Galina	Galina	k1gFnSc1	Galina
Saňková	Saňková	k1gFnSc1	Saňková
<g/>
;	;	kIx,	;
a	a	k8xC	a
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
fotografové	fotograf	k1gMnPc1	fotograf
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Tmej	Tmej	k1gMnSc1	Tmej
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Marco	Marco	k1gMnSc1	Marco
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Straka	Straka	k1gMnSc1	Straka
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jírů	Jíra	k1gMnPc2	Jíra
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Chochola	Chochola	k1gFnSc1	Chochola
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
fotografů	fotograf	k1gMnPc2	fotograf
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
válečné	válečný	k2eAgInPc1d1	válečný
reportážní	reportážní	k2eAgFnSc4d1	reportážní
fotografii	fotografia	k1gFnSc4	fotografia
věnují	věnovat	k5eAaImIp3nP	věnovat
fotografové	fotograf	k1gMnPc1	fotograf
Donald	Donald	k1gMnSc1	Donald
McCullin	McCullin	k2eAgMnSc1d1	McCullin
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Jones	Jones	k1gMnSc1	Jones
Griffith	Griffith	k1gMnSc1	Griffith
nebo	nebo	k8xC	nebo
James	James	k1gMnSc1	James
Nachtwey	Nachtwea	k1gFnSc2	Nachtwea
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
novodobé	novodobý	k2eAgMnPc4d1	novodobý
válečné	válečný	k2eAgMnPc4d1	válečný
fotografy	fotograf	k1gMnPc4	fotograf
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Ron	ron	k1gInSc1	ron
Haviv	Havivo	k1gNnPc2	Havivo
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Stanmeyer	Stanmeyra	k1gFnPc2	Stanmeyra
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Morris	Morris	k1gFnPc2	Morris
<g/>
,	,	kIx,	,
Alexandra	Alexandra	k1gFnSc1	Alexandra
Boulat	Boule	k1gNnPc2	Boule
i	i	k8xC	i
český	český	k2eAgMnSc1d1	český
fotograf	fotograf	k1gMnSc1	fotograf
Antonín	Antonín	k1gMnSc1	Antonín
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
V	v	k7c6	v
dánsko-pruské	dánskoruský	k2eAgFnSc6d1	dánsko-pruský
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
fotografovali	fotografovat	k5eAaImAgMnP	fotografovat
Christian	Christian	k1gMnSc1	Christian
Friedrich	Friedrich	k1gMnSc1	Friedrich
Brandt	Brandt	k1gMnSc1	Brandt
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Junod	Junoda	k1gFnPc2	Junoda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
většinu	většina	k1gFnSc4	většina
snímků	snímek	k1gInPc2	snímek
pořídili	pořídit	k5eAaPmAgMnP	pořídit
na	na	k7c4	na
stereo	stereo	k2eAgInSc4d1	stereo
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německo-francouzském	německorancouzský	k2eAgInSc6d1	německo-francouzský
konfliktu	konflikt	k1gInSc6	konflikt
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
fotografovali	fotografovat	k5eAaImAgMnP	fotografovat
většinou	většina	k1gFnSc7	většina
zcela	zcela	k6eAd1	zcela
neznámí	známý	k2eNgMnPc1d1	neznámý
fotografové	fotograf	k1gMnPc1	fotograf
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
Carla	Carl	k1gMnSc4	Carl
Friedricha	Friedrich	k1gMnSc4	Friedrich
Myliuse	Myliuse	k1gFnSc2	Myliuse
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Friedrich	Friedrich	k1gMnSc1	Friedrich
Brandt	Brandt	k1gMnSc1	Brandt
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
průkopník	průkopník	k1gMnSc1	průkopník
umělecké	umělecký	k2eAgFnSc2d1	umělecká
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
důležité	důležitý	k2eAgNnSc1d1	důležité
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pouhých	pouhý	k2eAgInPc2d1	pouhý
čtyř	čtyři	k4xCgMnPc2	čtyři
fotografů	fotograf	k1gMnPc2	fotograf
pořídil	pořídit	k5eAaPmAgMnS	pořídit
během	během	k7c2	během
dánsko-německé	dánskoěmecký	k2eAgFnSc2d1	dánsko-německý
války	válka	k1gFnSc2	válka
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snímcích	snímek	k1gInPc6	snímek
z	z	k7c2	z
Krymské	krymský	k2eAgFnSc2d1	Krymská
války	válka	k1gFnSc2	válka
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
fotografie	fotografia	k1gFnPc4	fotografia
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
válečných	válečný	k2eAgFnPc2d1	válečná
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Sinner	Sinner	k1gMnSc1	Sinner
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
představitele	představitel	k1gMnSc4	představitel
své	svůj	k3xOyFgFnSc2	svůj
profese	profes	k1gFnSc2	profes
v	v	k7c6	v
Tübingenu	Tübingen	k1gInSc6	Tübingen
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
fotografů	fotograf	k1gMnPc2	fotograf
Württemberska	Württembersko	k1gNnSc2	Württembersko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
německých	německý	k2eAgMnPc2d1	německý
válečných	válečný	k2eAgMnPc2d1	válečný
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
pořídil	pořídit	k5eAaPmAgMnS	pořídit
mobilní	mobilní	k2eAgFnSc4d1	mobilní
kabinu	kabina	k1gFnSc4	kabina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
předělal	předělat	k5eAaPmAgMnS	předělat
na	na	k7c4	na
temnou	temný	k2eAgFnSc4d1	temná
komoru	komora	k1gFnSc4	komora
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1	mobilní
fotokomora	fotokomora	k1gFnSc1	fotokomora
mu	on	k3xPp3gMnSc3	on
dobře	dobře	k6eAd1	dobře
sloužila	sloužit	k5eAaImAgFnS	sloužit
během	během	k7c2	během
Prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
/	/	kIx~	/
<g/>
71	[number]	k4	71
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
vojáky	voják	k1gMnPc4	voják
jdoucí	jdoucí	k2eAgInPc1d1	jdoucí
do	do	k7c2	do
Alsaska	Alsasko	k1gNnSc2	Alsasko
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc4d1	mobilní
temnou	temný	k2eAgFnSc4d1	temná
komoru	komora	k1gFnSc4	komora
později	pozdě	k6eAd2	pozdě
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
Německé	německý	k2eAgNnSc1d1	německé
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
"	"	kIx"	"
<g/>
Fotografie	fotografie	k1gFnSc1	fotografie
a	a	k8xC	a
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
Millerová	Millerová	k1gFnSc1	Millerová
dokumentovala	dokumentovat	k5eAaBmAgFnS	dokumentovat
Buchenwald	Buchenwald	k1gInSc4	Buchenwald
a	a	k8xC	a
Dachau	Dachaa	k1gFnSc4	Dachaa
Julien	Julien	k2eAgMnSc1d1	Julien
Hequembourg	Hequembourg	k1gMnSc1	Hequembourg
Bryan	Bryan	k1gMnSc1	Bryan
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
USA	USA	kA	USA
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgNnSc1d3	nejznámější
díky	díky	k7c3	díky
svým	svůj	k3xOyFgMnPc3	svůj
dokumentům	dokument	k1gInPc3	dokument
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
až	až	k9	až
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
Bourke-White	Bourke-Whit	k1gInSc5	Bourke-Whit
dokumentovala	dokumentovat	k5eAaBmAgFnS	dokumentovat
Auschwitz	Auschwitz	k1gMnSc1	Auschwitz
Rusko	Rusko	k1gNnSc1	Rusko
Roger	Roger	k1gMnSc1	Roger
Fenton	Fenton	k1gInSc1	Fenton
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
válečný	válečný	k2eAgMnSc1d1	válečný
fotograf	fotograf	k1gMnSc1	fotograf
v	v	k7c6	v
Krymské	krymský	k2eAgFnSc6d1	Krymská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
Polsko	Polsko	k1gNnSc1	Polsko
Julia	Julius	k1gMnSc2	Julius
Pirotte	Pirott	k1gInSc5	Pirott
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
–	–	k?	–
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
USA	USA	kA	USA
Mathew	Mathew	k1gFnPc2	Mathew
B.	B.	kA	B.
Brady	brada	k1gFnSc2	brada
a	a	k8xC	a
Timothy	Timotha	k1gFnSc2	Timotha
H.	H.	kA	H.
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Sullivan	Sullivan	k1gMnSc1	Sullivan
dokumentovali	dokumentovat	k5eAaBmAgMnP	dokumentovat
Americkou	americký	k2eAgFnSc4d1	americká
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
Robert	Robert	k1gMnSc1	Robert
Capa	cap	k1gMnSc4	cap
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
Frank	Frank	k1gMnSc1	Frank
Hurley	Hurlea	k1gFnSc2	Hurlea
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Australské	australský	k2eAgFnSc3d1	australská
Imperial	Imperial	k1gInSc1	Imperial
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
AIF	AIF	kA	AIF
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
čestný	čestný	k2eAgMnSc1d1	čestný
kapitán	kapitán	k1gMnSc1	kapitán
a	a	k8xC	a
zachytil	zachytit	k5eAaPmAgMnS	zachytit
mnoho	mnoho	k4c4	mnoho
scén	scéna	k1gFnPc2	scéna
bojiště	bojiště	k1gNnSc2	bojiště
během	během	k7c2	během
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Passchendaele	Passchendael	k1gInSc2	Passchendael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
dobrodružným	dobrodružný	k2eAgMnSc7d1	dobrodružný
duchem	duch	k1gMnSc7	duch
podstupoval	podstupovat	k5eAaImAgMnS	podstupovat
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
značná	značný	k2eAgNnPc1d1	značné
rizika	riziko	k1gNnPc1	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
AIF	AIF	kA	AIF
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Hurley	Hurle	k1gMnPc4	Hurle
také	také	k9	také
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
válečný	válečný	k2eAgMnSc1d1	válečný
fotograf	fotograf	k1gMnSc1	fotograf
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Rider-Rider	Rider-Rider	k1gInSc1	Rider-Rider
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Stamford	Stamford	k1gMnSc1	Stamford
Hill	Hill	k1gMnSc1	Hill
-	-	kIx~	-
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
válečný	válečný	k2eAgMnSc1d1	válečný
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
kancelář	kancelář	k1gFnSc4	kancelář
Canadian	Canadian	k1gInSc1	Canadian
War	War	k1gMnSc1	War
Records	Recordsa	k1gFnPc2	Recordsa
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
kanadské	kanadský	k2eAgMnPc4d1	kanadský
vojáky	voják	k1gMnPc4	voják
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ken	Ken	k?	Ken
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
poručík	poručík	k1gMnSc1	poručík
v	v	k7c6	v
armádní	armádní	k2eAgFnSc6d1	armádní
jednotce	jednotka	k1gFnSc6	jednotka
pro	pro	k7c4	pro
film	film	k1gInSc4	film
a	a	k8xC	a
fotografii	fotografia	k1gFnSc4	fotografia
(	(	kIx(	(
<g/>
Canadian	Canadian	k1gMnSc1	Canadian
Army	Arma	k1gFnSc2	Arma
Film	film	k1gInSc1	film
and	and	k?	and
Photo	Photo	k1gNnSc1	Photo
Unit	Unit	k1gMnSc1	Unit
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
a	a	k8xC	a
natáčení	natáčení	k1gNnSc6	natáčení
osvobození	osvobození	k1gNnSc2	osvobození
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
Holandska	Holandsko	k1gNnSc2	Holandsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
okupaci	okupace	k1gFnSc4	okupace
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
dokumentaci	dokumentace	k1gFnSc4	dokumentace
přistání	přistání	k1gNnSc2	přistání
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
fotografií	fotografia	k1gFnPc2	fotografia
bylo	být	k5eAaImAgNnS	být
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
na	na	k7c4	na
barevný	barevný	k2eAgInSc4d1	barevný
film	film	k1gInSc4	film
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
armádě	armáda	k1gFnSc6	armáda
-	-	kIx~	-
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
však	však	k9	však
dostaly	dostat	k5eAaPmAgInP	dostat
až	až	k9	až
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediné	jediný	k2eAgFnPc4d1	jediná
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
barevné	barevný	k2eAgFnPc4d1	barevná
fotografie	fotografia	k1gFnPc4	fotografia
vylodění	vylodění	k1gNnSc2	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
CFPU	CFPU	kA	CFPU
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
ve	v	k7c6	v
frontové	frontový	k2eAgFnSc6d1	frontová
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Dieppe	Diepp	k1gInSc5	Diepp
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
Manitoba	Manitoba	k1gFnSc1	Manitoba
Dragoons	Dragoonsa	k1gFnPc2	Dragoonsa
očekávali	očekávat	k5eAaImAgMnP	očekávat
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
CFPU	CFPU	kA	CFPU
včetně	včetně	k7c2	včetně
Kena	Ken	k1gInSc2	Ken
Bella	Bella	k1gFnSc1	Bella
a	a	k8xC	a
Briana	Briana	k1gFnSc1	Briana
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Regana	Regan	k1gMnSc4	Regan
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgMnPc7	první
spojeneckými	spojenecký	k2eAgMnPc7d1	spojenecký
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gNnSc1	George
Silk	silk	k1gInSc1	silk
Nick	Nick	k1gMnSc1	Nick
Ut	Ut	k1gMnSc1	Ut
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vietnamsko-americký	vietnamskomerický	k2eAgMnSc1d1	vietnamsko-americký
fotograf	fotograf	k1gMnSc1	fotograf
pracující	pracující	k1gMnSc1	pracující
pro	pro	k7c4	pro
agenturu	agentura	k1gFnSc4	agentura
Associated	Associated	k1gMnSc1	Associated
Press	Pressa	k1gFnPc2	Pressa
(	(	kIx(	(
<g/>
AP	ap	kA	ap
<g/>
)	)	kIx)	)
z	z	k7c2	z
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnSc7d3	nejznámější
fotografií	fotografia	k1gFnSc7	fotografia
je	být	k5eAaImIp3nS	být
snímek	snímek	k1gInSc1	snímek
Phan	Phano	k1gNnPc2	Phano
Thị	Thị	k1gFnSc2	Thị
Kim	Kim	k1gFnSc1	Kim
Phúc	Phúc	k1gFnSc1	Phúc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zachytil	zachytit	k5eAaPmAgMnS	zachytit
jako	jako	k9	jako
nahou	nahý	k2eAgFnSc4d1	nahá
9	[number]	k4	9
<g/>
letou	letý	k2eAgFnSc4d1	letá
dívku	dívka	k1gFnSc4	dívka
popálenou	popálený	k2eAgFnSc4d1	popálená
napalmem	napalm	k1gInSc7	napalm
běžící	běžící	k2eAgInSc4d1	běžící
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
fotoaparátu	fotoaparát	k1gInSc3	fotoaparát
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Trang	Trang	k1gMnSc1	Trang
Bang	Bang	k1gMnSc1	Bang
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
cenu	cena	k1gFnSc4	cena
World	Worldo	k1gNnPc2	Worldo
Press	Pressa	k1gFnPc2	Pressa
Photo	Photo	k1gNnSc4	Photo
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Eddie	Eddie	k1gFnSc1	Eddie
Adams	Adamsa	k1gFnPc2	Adamsa
James	James	k1gMnSc1	James
Nachtwey	Nachtwea	k1gFnSc2	Nachtwea
Patrick	Patrick	k1gMnSc1	Patrick
Chauvel	Chauvel	k1gMnSc1	Chauvel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
válečný	válečný	k2eAgMnSc1d1	válečný
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
téměř	téměř	k6eAd1	téměř
dvacet	dvacet	k4xCc4	dvacet
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
šestidenní	šestidenní	k2eAgFnSc1d1	šestidenní
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gInSc1	Roger
Fenton	Fenton	k1gInSc1	Fenton
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
Krymskou	krymský	k2eAgFnSc4d1	Krymská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
také	také	k9	také
krajinu	krajina	k1gFnSc4	krajina
blízko	blízko	k7c2	blízko
Light	Lighta	k1gFnPc2	Lighta
Brigade	Brigad	k1gInSc5	Brigad
příznačně	příznačně	k6eAd1	příznačně
popsaném	popsaný	k2eAgNnSc6d1	popsané
v	v	k7c6	v
proslulém	proslulý	k2eAgInSc6d1	proslulý
Tennysonově	Tennysonův	k2eAgInSc6d1	Tennysonův
snímku	snímek	k1gInSc6	snímek
Útok	útok	k1gInSc1	útok
lehké	lehký	k2eAgFnSc2d1	lehká
brigády	brigáda	k1gFnSc2	brigáda
jako	jako	k8xS	jako
Údolí	údolí	k1gNnSc2	údolí
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fenton	Fenton	k1gInSc1	Fenton
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
své	svůj	k3xOyFgFnPc4	svůj
fotografie	fotografia	k1gFnPc4	fotografia
podobně	podobně	k6eAd1	podobně
<g/>
:	:	kIx,	:
Údolí	údolí	k1gNnSc1	údolí
stínu	stín	k1gInSc2	stín
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
údolí	údolí	k1gNnSc6	údolí
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
dva	dva	k4xCgInPc1	dva
obrázky	obrázek	k1gInPc1	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
s	s	k7c7	s
několika	několik	k4yIc2	několik
dělovými	dělový	k2eAgFnPc7d1	dělová
koulemi	koule	k1gFnPc7	koule
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
silnicí	silnice	k1gFnSc7	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
jako	jako	k9	jako
první	první	k4xOgInSc1	první
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Errol	Errol	k1gInSc4	Errol
Morris	Morris	k1gFnSc2	Morris
napsal	napsat	k5eAaPmAgMnS	napsat
řadu	řada	k1gFnSc4	řada
pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
důkazech	důkaz	k1gInPc6	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografia	k1gFnPc1	fotografia
bez	bez	k7c2	bez
dělových	dělový	k2eAgFnPc2d1	dělová
koulí	koule	k1gFnPc2	koule
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
přesunul	přesunout	k5eAaPmAgMnS	přesunout
koule	koule	k1gFnPc4	koule
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
obrázku	obrázek	k1gInSc6	obrázek
<g/>
?	?	kIx.	?
</s>
<s>
Byly	být	k5eAaImAgInP	být
záměrně	záměrně	k6eAd1	záměrně
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
Fentonem	Fenton	k1gInSc7	Fenton
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
image	image	k1gFnSc2	image
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
koule	koule	k1gFnSc1	koule
z	z	k7c2	z
příkopu	příkop	k1gInSc2	příkop
vyndali	vyndat	k5eAaPmAgMnP	vyndat
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
mohli	moct	k5eAaImAgMnP	moct
znovu	znovu	k6eAd1	znovu
použít	použít	k5eAaPmF	použít
<g/>
?	?	kIx.	?
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
obecné	obecný	k2eAgFnSc2d1	obecná
averze	averze	k1gFnSc2	averze
britského	britský	k2eAgInSc2d1	britský
národa	národ	k1gInSc2	národ
k	k	k7c3	k
nepopulární	populární	k2eNgFnSc3d1	nepopulární
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
ke	k	k7c3	k
zmaření	zmaření	k1gNnSc3	zmaření
protiválečného	protiválečný	k2eAgNnSc2d1	protiválečné
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
v	v	k7c6	v
méně	málo	k6eAd2	málo
kritickém	kritický	k2eAgInSc6d1	kritický
Illustrated	Illustrated	k1gInSc1	Illustrated
London	London	k1gMnSc1	London
News	News	k1gInSc1	News
<g/>
.	.	kIx.	.
</s>
<s>
Fenton	Fenton	k1gInSc1	Fenton
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgInS	bránit
vytváření	vytváření	k1gNnSc4	vytváření
obrázků	obrázek	k1gInPc2	obrázek
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
zraněných	zraněný	k2eAgMnPc2d1	zraněný
nebo	nebo	k8xC	nebo
zmrzačených	zmrzačený	k2eAgMnPc2d1	zmrzačený
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Parshall	Parshallum	k1gNnPc2	Parshallum
&	&	k?	&
Tully	Tulla	k1gFnSc2	Tulla
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
fotografií	fotografia	k1gFnPc2	fotografia
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pořídil	pořídit	k5eAaPmAgMnS	pořídit
J.	J.	kA	J.
A.	A.	kA	A.
Mihalovic	Mihalovice	k1gFnPc2	Mihalovice
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ní	on	k3xPp3gFnSc2	on
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
záď	záď	k1gFnSc4	záď
a	a	k8xC	a
levobok	levobok	k1gInSc4	levobok
japonského	japonský	k2eAgInSc2d1	japonský
těžkého	těžký	k2eAgInSc2d1	těžký
křižníku	křižník	k1gInSc2	křižník
Mikuma	Mikum	k1gMnSc2	Mikum
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
potopením	potopení	k1gNnSc7	potopení
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Midway	Midwaa	k1gFnSc2	Midwaa
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
jí	jíst	k5eAaImIp3nS	jíst
pořídil	pořídit	k5eAaPmAgMnS	pořídit
během	během	k7c2	během
letu	let	k1gInSc2	let
v	v	k7c6	v
SBD	SBD	kA	SBD
6-S-18	[number]	k4	6-S-18
od	od	k7c2	od
VS-6	VS-6	k1gFnPc2	VS-6
z	z	k7c2	z
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc2	Enterprise
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
známé	známý	k2eAgFnPc4d1	známá
fotografie	fotografia	k1gFnPc4	fotografia
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pořídil	pořídit	k5eAaPmAgInS	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Chalděj	Chalděj	k1gMnSc1	Chalděj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
záběr	záběr	k1gInSc4	záběr
vztyčování	vztyčování	k1gNnSc2	vztyčování
vítězné	vítězný	k2eAgFnSc2d1	vítězná
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlajky	vlajka	k1gFnSc2	vlajka
nad	nad	k7c7	nad
budovou	budova	k1gFnSc7	budova
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
autor	autor	k1gMnSc1	autor
nezachytil	zachytit	k5eNaPmAgMnS	zachytit
autentickou	autentický	k2eAgFnSc4d1	autentická
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
záběr	záběr	k1gInSc4	záběr
naaranžoval	naaranžovat	k5eAaPmAgMnS	naaranžovat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
skutečném	skutečný	k2eAgNnSc6d1	skutečné
umístění	umístění	k1gNnSc6	umístění
vlajky	vlajka	k1gFnSc2	vlajka
nad	nad	k7c7	nad
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Chalděj	Chaldět	k5eAaImRp2nS	Chaldět
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednomu	jeden	k4xCgMnSc3	jeden
vojákovi	voják	k1gMnSc6	voják
vymazal	vymazat	k5eAaPmAgMnS	vymazat
hodinky	hodinka	k1gFnPc4	hodinka
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
zápěstích	zápěstí	k1gNnPc6	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
fotografie	fotografia	k1gFnSc2	fotografia
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
poštovní	poštovní	k2eAgFnSc4d1	poštovní
známku	známka	k1gFnSc4	známka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydaná	vydaný	k2eAgFnSc1d1	vydaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
osvobození	osvobození	k1gNnSc2	osvobození
od	od	k7c2	od
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
voják	voják	k1gMnSc1	voják
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Michail	Michail	k1gMnSc1	Michail
Minin	Minin	k1gMnSc1	Minin
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
23	[number]	k4	23
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
se	se	k3xPyFc4	se
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
budovy	budova	k1gFnSc2	budova
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
ve	v	k7c4	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
reportážní	reportážní	k2eAgNnSc4d1	reportážní
fotografování	fotografování	k1gNnSc4	fotografování
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
tma	tma	k6eAd1	tma
<g/>
.	.	kIx.	.
</s>
