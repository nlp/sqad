<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
malby	malba	k1gFnSc2	malba
a	a	k8xC	a
kresby	kresba	k1gFnSc2	kresba
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
nejsou	být	k5eNaImIp3nP	být
snadno	snadno	k6eAd1	snadno
zaměnitelné	zaměnitelný	k2eAgFnPc1d1	zaměnitelná
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázán	k2eAgNnSc1d1	dokázáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografové	fotograf	k1gMnPc1	fotograf
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
a	a	k8xC	a
zobrazovanou	zobrazovaný	k2eAgFnSc7d1	zobrazovaná
scénou	scéna	k1gFnSc7	scéna
manipulovali	manipulovat	k5eAaImAgMnP	manipulovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografie	k1gFnSc1	fotografie
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
zcela	zcela	k6eAd1	zcela
objektivní	objektivní	k2eAgInSc4d1	objektivní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
