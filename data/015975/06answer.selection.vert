<s>
Silnice	silnice	k1gFnSc1
D8	D8	k1gFnSc2
(	(	kIx(
<g/>
chorvatsky	chorvatsky	k6eAd1
Državna	Državna	k1gFnSc1
cesta	cesta	k1gFnSc1
D	D	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
Státní	státní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
D	D	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
též	též	k6eAd1
Jadranska	Jadransko	k1gNnSc2
magistrala	magistrat	k5eAaPmAgFnS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
hlavních	hlavní	k2eAgFnPc2d1
silnic	silnice	k1gFnPc2
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
neformálně	formálně	k6eNd1
je	být	k5eAaImIp3nS
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Jadranská	jadranský	k2eAgFnSc1d1
magistrála	magistrála	k1gFnSc1
<g/>
.	.	kIx.
</s>