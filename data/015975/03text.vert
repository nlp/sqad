<s>
Silnice	silnice	k1gFnSc1
D8	D8	k1gFnSc2
</s>
<s>
Státní	státní	k2eAgFnPc1d1
silnice	silnice	k1gFnPc1
D8	D8	k1gFnPc2
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Stát	stát	k5eAaPmF,k5eAaImF
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Silnice	silnice	k1gFnSc1
vede	vést	k5eAaImIp3nS
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Silnice	silnice	k1gFnSc1
D8	D8	k1gFnSc2
(	(	kIx(
<g/>
chorvatsky	chorvatsky	k6eAd1
Državna	Državna	k1gFnSc1
cesta	cesta	k1gFnSc1
D	D	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
Státní	státní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
D	D	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
též	též	k6eAd1
Jadranska	Jadransko	k1gNnSc2
magistrala	magistrat	k5eAaPmAgFnS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
hlavních	hlavní	k2eAgFnPc2d1
silnic	silnice	k1gFnPc2
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
neformálně	formálně	k6eNd1
je	být	k5eAaImIp3nS
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Jadranská	jadranský	k2eAgFnSc1d1
magistrála	magistrála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS
severní	severní	k2eAgFnSc4d1
a	a	k8xC
jižní	jižní	k2eAgFnSc4d1
polovinu	polovina	k1gFnSc4
chorvatské	chorvatský	k2eAgFnSc2d1
části	část	k1gFnSc2
Jadranského	jadranský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
již	již	k6eAd1
od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
pak	pak	k9
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
primární	primární	k2eAgFnSc1d1
spojnice	spojnice	k1gFnSc1
v	v	k7c6
Dalmácii	Dalmácie	k1gFnSc6
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
ovšem	ovšem	k9
nebyla	být	k5eNaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
A	a	k9
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odvedla	odvést	k5eAaPmAgFnS
především	především	k6eAd1
dálkovou	dálkový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
do	do	k7c2
Dubrovníku	Dubrovník	k1gInSc2
a	a	k8xC
Splitu	Split	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
D8	D8	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
658	#num#	k4
km	km	kA
<g/>
,	,	kIx,
pokračuje	pokračovat	k5eAaImIp3nS
však	však	k9
i	i	k9
dále	daleko	k6eAd2
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
a	a	k8xC
Černé	Černé	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
tam	tam	k6eAd1
již	již	k6eAd1
pod	pod	k7c7
jiným	jiný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
prochází	procházet	k5eAaImIp3nS
přes	přes	k7c4
šest	šest	k4xCc4
chorvatských	chorvatský	k2eAgFnPc2d1
žup	župa	k1gFnPc2
<g/>
:	:	kIx,
Přímořsko-gorskokotarskou	Přímořsko-gorskokotarský	k2eAgFnSc4d1
<g/>
,	,	kIx,
Licko-senjskou	Licko-senjský	k2eAgFnSc4d1
<g/>
,	,	kIx,
Zadarskou	zadarský	k2eAgFnSc4d1
<g/>
,	,	kIx,
Šibenicko-kninskou	Šibenicko-kninský	k2eAgFnSc4d1
<g/>
,	,	kIx,
Splitsko-dalmatskou	splitsko-dalmatský	k2eAgFnSc4d1
a	a	k8xC
Dubrovnicko-neretvanskou	Dubrovnicko-neretvanský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Državna	Državna	k1gFnSc1
cesta	cesta	k1gFnSc1
D8	D8	k1gFnSc1
na	na	k7c6
chorvatské	chorvatský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Státní	státní	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
D8	D8	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Státní	státní	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
1	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
</s>
<s>
D1	D1	k4
</s>
<s>
D2	D2	k4
</s>
<s>
D3	D3	k4
</s>
<s>
D5	D5	k4
</s>
<s>
D6	D6	k4
</s>
<s>
D7	D7	k4
</s>
<s>
D8	D8	k4
</s>
<s>
D9	D9	k4
</s>
<s>
D10	D10	k4
</s>
<s>
D12	D12	k4
</s>
<s>
D14	D14	k4
20	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
</s>
<s>
D20	D20	k4
</s>
<s>
D22	D22	k4
</s>
<s>
D23	D23	k4
</s>
<s>
D24	D24	k4
</s>
<s>
D25	D25	k4
</s>
<s>
D26	D26	k4
</s>
<s>
D27	D27	k4
</s>
<s>
D28	D28	k4
</s>
<s>
D29	D29	k4
</s>
<s>
D30	D30	k4
</s>
<s>
D31	D31	k4
</s>
<s>
D32	D32	k4
</s>
<s>
D33	D33	k4
</s>
<s>
D34	D34	k4
</s>
<s>
D35	D35	k4
</s>
<s>
D36	D36	k4
</s>
<s>
D37	D37	k4
</s>
<s>
D38	D38	k4
</s>
<s>
D39	D39	k4
</s>
<s>
D40	D40	k4
</s>
<s>
D41	D41	k4
</s>
<s>
D42	D42	k4
</s>
<s>
D43	D43	k4
</s>
<s>
D44	D44	k4
</s>
<s>
D45	D45	k4
</s>
<s>
D46	D46	k4
</s>
<s>
D47	D47	k4
</s>
<s>
D48	D48	k4
</s>
<s>
D49	D49	k4
</s>
<s>
D50	D50	k4
</s>
<s>
D51	D51	k4
</s>
<s>
D52	D52	k4
</s>
<s>
D53	D53	k4
</s>
<s>
D54	D54	k4
</s>
<s>
D55	D55	k4
</s>
<s>
D56	D56	k4
</s>
<s>
D57	D57	k4
</s>
<s>
D58	D58	k4
</s>
<s>
D59	D59	k4
</s>
<s>
D60	D60	k4
</s>
<s>
D62	D62	k4
</s>
<s>
D64	D64	k4
</s>
<s>
D66	D66	k4
</s>
<s>
D69	D69	k4
</s>
<s>
D70	D70	k4
</s>
<s>
D72	D72	k4
</s>
<s>
D74	D74	k4
</s>
<s>
D75	D75	k4
</s>
<s>
D76	D76	k4
</s>
<s>
D77	D77	k4
100	#num#	k4
<g/>
–	–	k?
<g/>
129	#num#	k4
</s>
<s>
D100	D100	k4
</s>
<s>
D101	D101	k4
</s>
<s>
D102	D102	k4
</s>
<s>
D103	D103	k4
</s>
<s>
D104	D104	k4
</s>
<s>
D105	D105	k4
</s>
<s>
D106	D106	k4
</s>
<s>
D109	D109	k4
</s>
<s>
D110	D110	k4
</s>
<s>
D111	D111	k4
</s>
<s>
D112	D112	k4
</s>
<s>
D113	D113	k4
</s>
<s>
D114	D114	k4
</s>
<s>
D115	D115	k4
</s>
<s>
D116	D116	k4
</s>
<s>
D117	D117	k4
</s>
<s>
D118	D118	k4
</s>
<s>
D119	D119	k4
</s>
<s>
D120	D120	k4
</s>
<s>
D121	D121	k4
</s>
<s>
D123	D123	k4
</s>
<s>
D124	D124	k4
</s>
<s>
D125	D125	k4
</s>
<s>
D126	D126	k4
</s>
<s>
D128	D128	k4
</s>
<s>
D129	D129	k4
200	#num#	k4
<g/>
–	–	k?
<g/>
235	#num#	k4
</s>
<s>
D200	D200	k4
</s>
<s>
D201	D201	k4
</s>
<s>
D203	D203	k4
</s>
<s>
D204	D204	k4
</s>
<s>
D205	D205	k4
</s>
<s>
D206	D206	k4
</s>
<s>
D207	D207	k4
</s>
<s>
D208	D208	k4
</s>
<s>
D209	D209	k4
</s>
<s>
D210	D210	k4
</s>
<s>
D211	D211	k4
</s>
<s>
D212	D212	k4
</s>
<s>
D213	D213	k4
</s>
<s>
D214	D214	k4
</s>
<s>
D216	D216	k4
</s>
<s>
D217	D217	k4
</s>
<s>
D218	D218	k4
</s>
<s>
D219	D219	k4
</s>
<s>
D220	D220	k4
</s>
<s>
D222	D222	k4
</s>
<s>
D223	D223	k4
</s>
<s>
D224	D224	k4
</s>
<s>
D225	D225	k4
</s>
<s>
D227	D227	k4
</s>
<s>
D228	D228	k4
</s>
<s>
D229	D229	k4
</s>
<s>
D231	D231	k4
</s>
<s>
D232	D232	k4
</s>
<s>
D233	D233	k4
</s>
<s>
D235	D235	k4
300	#num#	k4
<g/>
–	–	k?
<g/>
316	#num#	k4
</s>
<s>
D300	D300	k4
</s>
<s>
D301	D301	k4
</s>
<s>
D302	D302	k4
</s>
<s>
D303	D303	k4
</s>
<s>
D304	D304	k4
</s>
<s>
D305	D305	k4
</s>
<s>
D306	D306	k4
</s>
<s>
D307	D307	k4
</s>
<s>
D310	D310	k4
</s>
<s>
D312	D312	k4
</s>
<s>
D313	D313	k4
</s>
<s>
D314	D314	k4
</s>
<s>
D315	D315	k4
</s>
<s>
D316	D316	k4
400	#num#	k4
<g/>
–	–	k?
<g/>
431	#num#	k4
</s>
<s>
D400	D400	k4
</s>
<s>
D401	D401	k4
</s>
<s>
D402	D402	k4
</s>
<s>
D403	D403	k4
</s>
<s>
D404	D404	k4
</s>
<s>
D405	D405	k4
</s>
<s>
D406	D406	k4
</s>
<s>
D407	D407	k4
</s>
<s>
D408	D408	k4
</s>
<s>
D409	D409	k4
</s>
<s>
D410	D410	k4
</s>
<s>
D411	D411	k4
</s>
<s>
D412	D412	k4
</s>
<s>
D413	D413	k4
</s>
<s>
D414	D414	k4
</s>
<s>
D415	D415	k4
</s>
<s>
D416	D416	k4
</s>
<s>
D417	D417	k4
</s>
<s>
D418	D418	k4
</s>
<s>
D420	D420	k4
</s>
<s>
D421	D421	k4
</s>
<s>
D422	D422	k4
</s>
<s>
D423	D423	k4
</s>
<s>
D424	D424	k4
</s>
<s>
D425	D425	k4
</s>
<s>
D427	D427	k4
</s>
<s>
D429	D429	k4
</s>
<s>
D430	D430	k4
</s>
<s>
D431	D431	k4
500	#num#	k4
<g/>
–	–	k?
<g/>
545	#num#	k4
</s>
<s>
D500	D500	k4
</s>
<s>
D501	D501	k4
</s>
<s>
D502	D502	k4
</s>
<s>
D503	D503	k4
</s>
<s>
D507	D507	k4
</s>
<s>
D510	D510	k4
</s>
<s>
D512	D512	k4
</s>
<s>
D514	D514	k4
</s>
<s>
D515	D515	k4
</s>
<s>
D516	D516	k4
</s>
<s>
D517	D517	k4
</s>
<s>
D518	D518	k4
</s>
<s>
D519	D519	k4
</s>
<s>
D520	D520	k4
</s>
<s>
D522	D522	k4
</s>
<s>
D525	D525	k4
</s>
<s>
D526	D526	k4
</s>
<s>
D528	D528	k4
</s>
<s>
D530	D530	k4
</s>
<s>
D531	D531	k4
</s>
<s>
D534	D534	k4
</s>
<s>
D535	D535	k4
</s>
<s>
D536	D536	k4
</s>
<s>
D537	D537	k4
</s>
<s>
D538	D538	k4
</s>
<s>
D539	D539	k4
</s>
<s>
D540	D540	k4
</s>
<s>
D541	D541	k4
</s>
<s>
D542	D542	k4
</s>
<s>
D543	D543	k4
</s>
<s>
D544	D544	k4
</s>
<s>
D545	D545	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Doprava	doprava	k1gFnSc1
|	|	kIx~
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
