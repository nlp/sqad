<s>
Seinen	Seinen	k1gInSc1	Seinen
(	(	kIx(	(
<g/>
青	青	k?	青
<g/>
,	,	kIx,	,
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
"	"	kIx"	"
<g/>
dospělou	dospělý	k2eAgFnSc7d1	dospělá
osobou	osoba	k1gFnSc7	osoba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
成	成	k?	成
seinen	seinen	k1gInSc1	seinen
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přednostně	přednostně	k6eAd1	přednostně
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
ve	v	k7c6	v
věkovém	věkový	k2eAgNnSc6d1	věkové
rozmezí	rozmezí	k1gNnSc6	rozmezí
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
širokou	široký	k2eAgFnSc7d1	široká
paletou	paleta	k1gFnSc7	paleta
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
tematických	tematický	k2eAgFnPc2d1	tematická
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
od	od	k7c2	od
avantgardy	avantgarda	k1gFnSc2	avantgarda
až	až	k9	až
k	k	k7c3	k
pornografii	pornografie	k1gFnSc3	pornografie
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
obdoba	obdoba	k1gFnSc1	obdoba
seinen	seinna	k1gFnPc2	seinna
mangy	mango	k1gNnPc7	mango
je	on	k3xPp3gInPc4	on
džosei	džosei	k1gNnSc1	džosei
manga	mango	k1gNnSc2	mango
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
seinen	seinen	k1gInSc4	seinen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podívat	podívat	k5eAaPmF	podívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
není	být	k5eNaImIp3nS	být
nad	nad	k7c7	nad
originálním	originální	k2eAgMnSc7d1	originální
kandži	kandž	k1gFnSc3	kandž
názvem	název	k1gInSc7	název
použita	použít	k5eAaPmNgFnS	použít
furigana	furigana	k1gFnSc1	furigana
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
furigany	furigana	k1gFnSc2	furigana
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc1	titul
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgNnSc4d1	dospělé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Napomoci	napomoct	k5eAaPmF	napomoct
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jménu	jméno	k1gNnSc6	jméno
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
bylo	být	k5eAaImAgNnS	být
seinen	seinna	k1gFnPc2	seinna
publikováno	publikovat	k5eAaBmNgNnS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
mají	mít	k5eAaImIp3nP	mít
japonské	japonský	k2eAgFnPc1d1	japonská
manga	mango	k1gNnSc2	mango
časopisy	časopis	k1gInPc4	časopis
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
seinen	seinen	k1gInSc4	seinen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
názvu	název	k1gInSc6	název
slovo	slovo	k1gNnSc1	slovo
mladý	mladý	k2eAgMnSc1d1	mladý
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
young	young	k1gInSc1	young
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Young	Young	k1gMnSc1	Young
Jump	Jump	k1gMnSc1	Jump
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
časopisy	časopis	k1gInPc4	časopis
o	o	k7c4	o
seinen	seinen	k1gInSc4	seinen
manze	manze	k6eAd1	manze
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Ultra	ultra	k2eAgMnSc1d1	ultra
Jump	Jump	k1gMnSc1	Jump
<g/>
,	,	kIx,	,
Afternoon	Afternoon	k1gMnSc1	Afternoon
a	a	k8xC	a
Big	Big	k1gMnSc1	Big
Comic	Comic	k1gMnSc1	Comic
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
manga	mango	k1gNnSc2	mango
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
vydávané	vydávaný	k2eAgInPc1d1	vydávaný
v	v	k7c4	v
seinen	seinen	k1gInSc4	seinen
sbírkách	sbírka	k1gFnPc6	sbírka
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgInP	být
shromážděny	shromáždit	k5eAaPmNgInP	shromáždit
do	do	k7c2	do
tankóbon	tankóbona	k1gFnPc2	tankóbona
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svazku	svazek	k1gInSc2	svazek
<g/>
:	:	kIx,	:
((	((	k?	((
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
))	))	k?	))
Anime	Anim	k1gInSc5	Anim
News	News	k1gInSc1	News
Network	network	k1gInSc1	network
–	–	k?	–
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
pojmu	pojem	k1gInSc2	pojem
seinen	seinna	k1gFnPc2	seinna
</s>
