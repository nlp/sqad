<s>
Rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Plantae	Planta	k1gFnPc1	Planta
<g/>
,	,	kIx,	,
též	též	k9	též
nově	nově	k6eAd1	nově
Archaeplastida	Archaeplastida	k1gFnSc1	Archaeplastida
či	či	k8xC	či
Primoplantae	Primoplantae	k1gFnSc1	Primoplantae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
říše	říše	k1gFnSc1	říše
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
fotosyntetických	fotosyntetický	k2eAgInPc2d1	fotosyntetický
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
kapraďorostů	kapraďorost	k1gInPc2	kapraďorost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
asi	asi	k9	asi
290	[number]	k4	290
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
260	[number]	k4	260
000	[number]	k4	000
semenných	semenné	k1gFnPc2	semenné
<g/>
,	,	kIx,	,
15	[number]	k4	15
000	[number]	k4	000
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
kapraďorosty	kapraďorost	k1gInPc1	kapraďorost
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc1	plastid
s	s	k7c7	s
dvoujednotkovou	dvoujednotkový	k2eAgFnSc7d1	dvoujednotkový
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
primární	primární	k2eAgFnSc7d1	primární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
eukaryotní	eukaryotní	k2eAgFnSc2d1	eukaryotní
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
prokaryotní	prokaryotní	k2eAgFnSc2d1	prokaryotní
cyanobakterie	cyanobakterie	k1gFnSc2	cyanobakterie
(	(	kIx(	(
<g/>
sinice	sinice	k1gFnSc2	sinice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
mívají	mívat	k5eAaImIp3nP	mívat
ploché	plochý	k2eAgFnPc1d1	plochá
kristy	krista	k1gFnPc1	krista
<g/>
,	,	kIx,	,
centrioly	centriol	k1gInPc1	centriol
většinou	většinou	k6eAd1	většinou
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celulózu	celulóza	k1gFnSc4	celulóza
<g/>
,	,	kIx,	,
zásobní	zásobní	k2eAgFnSc7d1	zásobní
látkou	látka	k1gFnSc7	látka
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
škrobu	škrob	k1gInSc2	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
získává	získávat	k5eAaImIp3nS	získávat
energii	energie	k1gFnSc4	energie
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
ze	z	k7c2	z
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
rostliny	rostlina	k1gFnPc1	rostlina
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
používaných	používaný	k2eAgNnPc2d1	používané
fotosyntetických	fotosyntetický	k2eAgNnPc2d1	fotosyntetické
barviv	barvivo	k1gNnPc2	barvivo
se	se	k3xPyFc4	se
rostliny	rostlina	k1gFnPc1	rostlina
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
Glaukofyty	Glaukofyt	k1gInPc4	Glaukofyt
a	a	k8xC	a
ruduchy	ruducha	k1gFnPc1	ruducha
mají	mít	k5eAaImIp3nP	mít
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
fykobiliny	fykobilin	k1gInPc4	fykobilin
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
sinice	sinice	k1gFnSc1	sinice
<g/>
,	,	kIx,	,
zeleným	zelený	k2eAgFnPc3d1	zelená
řasám	řasa	k1gFnPc3	řasa
a	a	k8xC	a
rostlinám	rostlina	k1gFnPc3	rostlina
fykobiliny	fykobilin	k1gInPc4	fykobilin
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Glaukofyty	Glaukofyt	k1gInPc1	Glaukofyt
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
endosymbióza	endosymbióza	k1gFnSc1	endosymbióza
se	s	k7c7	s
sinicí	sinice	k1gFnSc7	sinice
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
-	-	kIx~	-
nemají	mít	k5eNaImIp3nP	mít
pravé	pravý	k2eAgInPc1d1	pravý
plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cyanely	cyanela	k1gFnPc1	cyanela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
někde	někde	k6eAd1	někde
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
plastidem	plastid	k1gInSc7	plastid
a	a	k8xC	a
cyanobakterií	cyanobakterie	k1gFnSc7	cyanobakterie
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
peptidoglykanovou	peptidoglykanový	k2eAgFnSc4d1	peptidoglykanový
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
botanika	botanika	k1gFnSc1	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
rostlinami	rostlina	k1gFnPc7	rostlina
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
nacházely	nacházet	k5eAaImAgFnP	nacházet
například	například	k6eAd1	například
různé	různý	k2eAgFnPc1d1	různá
léčivé	léčivý	k2eAgFnPc1d1	léčivá
byliny	bylina	k1gFnPc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
rostlinách	rostlina	k1gFnPc6	rostlina
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
zásadní	zásadní	k2eAgFnPc1d1	zásadní
například	například	k6eAd1	například
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nastal	nastat	k5eAaPmAgInS	nastat
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
12	[number]	k4	12
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ve	v	k7c6	v
staroindických	staroindický	k2eAgFnPc6d1	staroindická
védách	véda	k1gFnPc6	véda
<g/>
,	,	kIx,	,
rostlinami	rostlina	k1gFnPc7	rostlina
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
antické	antický	k2eAgNnSc4d1	antické
dílo	dílo	k1gNnSc4	dílo
Historia	Historium	k1gNnSc2	Historium
plantarum	plantarum	k1gInSc1	plantarum
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
autor	autor	k1gMnSc1	autor
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
botaniky	botanika	k1gFnSc2	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
botanika	botanika	k1gFnSc1	botanika
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
ke	k	k7c3	k
známějším	známý	k2eAgFnPc3d2	známější
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Al-Dinawari	Al-Dinawar	k1gFnSc2	Al-Dinawar
či	či	k8xC	či
Al-Nabati	Al-Nabati	k1gFnSc2	Al-Nabati
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
novověkem	novověk	k1gInSc7	novověk
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
botaniky	botanika	k1gFnSc2	botanika
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc1d1	nový
pohledy	pohled	k1gInPc1	pohled
a	a	k8xC	a
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gInSc2	Hook
objevil	objevit	k5eAaPmAgMnS	objevit
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
buňky	buňka	k1gFnPc4	buňka
v	v	k7c6	v
korku	korek	k1gInSc6	korek
<g/>
,	,	kIx,	,
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc4	von
Linné	Linná	k1gFnSc2	Linná
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
rostliny	rostlina	k1gFnPc4	rostlina
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
Systema	Systema	k1gFnSc1	Systema
naturae	naturaat	k5eAaPmIp3nS	naturaat
na	na	k7c4	na
25	[number]	k4	25
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
poměrně	poměrně	k6eAd1	poměrně
typické	typický	k2eAgFnPc4d1	typická
eukaryotické	eukaryotický	k2eAgFnPc4d1	eukaryotická
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
vlastních	vlastní	k2eAgInPc2d1	vlastní
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
přítomností	přítomnost	k1gFnSc7	přítomnost
plastidů	plastid	k1gInPc2	plastid
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc2d1	centrální
vakuoly	vakuola	k1gFnSc2	vakuola
<g/>
,	,	kIx,	,
celulózové	celulózový	k2eAgFnSc2d1	celulózová
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
mezibuněčných	mezibuněčný	k2eAgInPc2d1	mezibuněčný
spojů	spoj	k1gInPc2	spoj
-	-	kIx~	-
plazmodezmat	plazmodezma	k1gNnPc2	plazmodezma
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
1	[number]	k4	1
μ	μ	k?	μ
u	u	k7c2	u
zelené	zelený	k2eAgFnSc2d1	zelená
řasy	řasa	k1gFnSc2	řasa
Ostreococcus	Ostreococcus	k1gMnSc1	Ostreococcus
až	až	k6eAd1	až
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
metr	metr	k1gInSc4	metr
u	u	k7c2	u
zelených	zelený	k2eAgFnPc2d1	zelená
řas	řasa	k1gFnPc2	řasa
Caulerpa	Caulerpa	k1gFnSc1	Caulerpa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
pak	pak	k6eAd1	pak
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
podobného	podobný	k2eAgInSc2d1	podobný
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
vznikají	vznikat	k5eAaImIp3nP	vznikat
rostlinná	rostlinný	k2eAgNnPc4d1	rostlinné
pletiva	pletivo	k1gNnPc4	pletivo
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
vegetativní	vegetativní	k2eAgInPc4d1	vegetativní
a	a	k8xC	a
generativní	generativní	k2eAgInPc4d1	generativní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
anatomie	anatomie	k1gFnSc2	anatomie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
vodní	vodní	k2eAgFnPc1d1	vodní
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
<g/>
,	,	kIx,	,
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
však	však	k9	však
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
mnohobuněčnost	mnohobuněčnost	k1gFnSc1	mnohobuněčnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
a	a	k8xC	a
necévnaté	cévnatý	k2eNgFnPc1d1	cévnatý
mnohobuněčné	mnohobuněčný	k2eAgFnPc1d1	mnohobuněčná
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
stélkaté	stélkatý	k2eAgFnPc1d1	stélkatý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stélka	stélka	k1gFnSc1	stélka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
nečleněná	členěný	k2eNgFnSc1d1	nečleněná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
<g/>
.	.	kIx.	.
</s>
<s>
Morfologicky	morfologicky	k6eAd1	morfologicky
a	a	k8xC	a
funkčně	funkčně	k6eAd1	funkčně
rozlišené	rozlišený	k2eAgFnPc1d1	rozlišená
části	část	k1gFnPc1	část
stélky	stélka	k1gFnSc2	stélka
však	však	k9	však
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c7	za
orgány	orgán	k1gInPc7	orgán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fyloidy	fyloid	k1gInPc4	fyloid
a	a	k8xC	a
rhizoidy	rhizoid	k1gInPc4	rhizoid
u	u	k7c2	u
mechorostů	mechorost	k1gInPc2	mechorost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
tzv.	tzv.	kA	tzv.
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kormus	kormus	k1gInSc1	kormus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
členěné	členěný	k2eAgNnSc1d1	členěné
na	na	k7c4	na
specializované	specializovaný	k2eAgInPc4d1	specializovaný
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc4d1	tvořen
soubory	soubor	k1gInPc4	soubor
pletiv	pletivo	k1gNnPc2	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
na	na	k7c4	na
orgány	orgán	k1gInPc4	orgán
vegetativní	vegetativní	k2eAgInPc4d1	vegetativní
-	-	kIx~	-
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
život	život	k1gInSc4	život
jedince	jedinec	k1gMnSc4	jedinec
-	-	kIx~	-
např.	např.	kA	např.
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
kořen	kořen	k1gInSc1	kořen
a	a	k8xC	a
generativní	generativní	k2eAgNnSc1d1	generativní
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
buňky	buňka	k1gFnPc1	buňka
-	-	kIx~	-
např.	např.	kA	např.
květ	květ	k1gInSc4	květ
<g/>
,	,	kIx,	,
semeno	semeno	k1gNnSc4	semeno
a	a	k8xC	a
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fotoautotrofní	fotoautotrofní	k2eAgFnPc4d1	fotoautotrofní
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
termínů	termín	k1gInPc2	termín
fototrofie	fototrofie	k1gFnSc2	fototrofie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc4	rostlina
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
používají	používat	k5eAaImIp3nP	používat
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
a	a	k8xC	a
autotrofie	autotrofie	k1gFnSc1	autotrofie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vlastní	vlastní	k2eAgFnPc4d1	vlastní
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
anorganických	anorganický	k2eAgFnPc2d1	anorganická
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
zájmu	zájem	k1gInSc2	zájem
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
skladný	skladný	k2eAgInSc1d1	skladný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
těl	tělo	k1gNnPc2	tělo
zabudovává	zabudovávat	k5eAaImIp3nS	zabudovávat
uhlík	uhlík	k1gInSc4	uhlík
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
energie	energie	k1gFnSc2	energie
ze	z	k7c2	z
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
nejsou	být	k5eNaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
organizmy	organizmus	k1gInPc1	organizmus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
fotosyntetizují	fotosyntetizovat	k5eAaPmIp3nP	fotosyntetizovat
–	–	k?	–
tento	tento	k3xDgInSc4	tento
nápad	nápad	k1gInSc4	nápad
"	"	kIx"	"
<g/>
přebraly	přebrat	k5eAaPmAgFnP	přebrat
<g/>
"	"	kIx"	"
od	od	k7c2	od
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
od	od	k7c2	od
rostlin	rostlina	k1gFnPc2	rostlina
ho	on	k3xPp3gInSc4	on
zase	zase	k9	zase
přebraly	přebrat	k5eAaPmAgFnP	přebrat
některé	některý	k3yIgFnPc1	některý
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
chloroplastech	chloroplast	k1gInPc6	chloroplast
<g/>
;	;	kIx,	;
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
ji	on	k3xPp3gFnSc4	on
zejména	zejména	k9	zejména
zelená	zelený	k2eAgNnPc1d1	zelené
barviva	barvivo	k1gNnPc1	barvivo
<g/>
,	,	kIx,	,
chlorofyly	chlorofyl	k1gInPc1	chlorofyl
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navazující	navazující	k2eAgInPc4d1	navazující
fotosystémy	fotosystém	k1gInPc4	fotosystém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
světelné	světelný	k2eAgFnSc3d1	světelná
fázi	fáze	k1gFnSc3	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnSc1d1	následující
(	(	kIx(	(
<g/>
temnostní	temnostní	k2eAgFnSc1d1	temnostní
<g/>
)	)	kIx)	)
fáze	fáze	k1gFnSc1	fáze
však	však	k9	však
již	již	k6eAd1	již
světlo	světlo	k1gNnSc1	světlo
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
;	;	kIx,	;
jejím	její	k3xOp3gInSc7	její
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
Calvinův	Calvinův	k2eAgInSc1d1	Calvinův
cyklus	cyklus	k1gInSc1	cyklus
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
Hatch-Slackův	Hatch-Slackův	k2eAgInSc4d1	Hatch-Slackův
cyklus	cyklus	k1gInSc4	cyklus
u	u	k7c2	u
určitých	určitý	k2eAgFnPc2d1	určitá
skupin	skupina	k1gFnPc2	skupina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
přijímají	přijímat	k5eAaImIp3nP	přijímat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
obvykle	obvykle	k6eAd1	obvykle
především	především	k9	především
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
biologická	biologický	k2eAgFnSc1d1	biologická
fixace	fixace	k1gFnSc1	fixace
dusíku	dusík	k1gInSc2	dusík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
sušiny	sušina	k1gFnSc2	sušina
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
těl	tělo	k1gNnPc2	tělo
představují	představovat	k5eAaImIp3nP	představovat
atomy	atom	k1gInPc1	atom
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
a	a	k8xC	a
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
již	již	k6eAd1	již
obsah	obsah	k1gInSc1	obsah
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
desetin	desetina	k1gFnPc2	desetina
procenta	procento	k1gNnSc2	procento
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
individuálními	individuální	k2eAgFnPc7d1	individuální
rostlinami	rostlina	k1gFnPc7	rostlina
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
;	;	kIx,	;
např.	např.	kA	např.
slanomilné	slanomilný	k2eAgFnPc4d1	slanomilná
rostliny	rostlina	k1gFnPc4	rostlina
hromadí	hromadit	k5eAaImIp3nS	hromadit
sodík	sodík	k1gInSc1	sodík
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
a	a	k8xC	a
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kompenzovaly	kompenzovat	k5eAaBmAgFnP	kompenzovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
solí	sůl	k1gFnPc2	sůl
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
minerální	minerální	k2eAgInSc1d1	minerální
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
u	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
následován	následovat	k5eAaImNgInS	následovat
asimilací	asimilace	k1gFnSc7	asimilace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zabudováním	zabudování	k1gNnSc7	zabudování
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
z	z	k7c2	z
dusičnanů	dusičnan	k1gInPc2	dusičnan
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
do	do	k7c2	do
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
či	či	k8xC	či
jejich	jejich	k3xOp3gInPc2	jejich
amidů	amid	k1gInPc2	amid
<g/>
.	.	kIx.	.
</s>
<s>
Sírany	síran	k1gInPc1	síran
se	se	k3xPyFc4	se
zase	zase	k9	zase
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
redukují	redukovat	k5eAaBmIp3nP	redukovat
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
se	se	k3xPyFc4	se
do	do	k7c2	do
sirných	sirný	k2eAgFnPc2d1	sirná
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
cystein	cystein	k1gInSc1	cystein
<g/>
,	,	kIx,	,
methionin	methionin	k1gInSc1	methionin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zásobními	zásobní	k2eAgFnPc7d1	zásobní
látkami	látka	k1gFnPc7	látka
rostlin	rostlina	k1gFnPc2	rostlina
bývají	bývat	k5eAaImIp3nP	bývat
zejména	zejména	k9	zejména
triacylglyceroly	triacylglycerola	k1gFnPc1	triacylglycerola
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
tuky	tuk	k1gInPc1	tuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
α	α	k?	α
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
škrob	škrob	k1gInSc4	škrob
<g/>
)	)	kIx)	)
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
zásobní	zásobní	k2eAgFnPc1d1	zásobní
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasifikace	klasifikace	k1gFnSc2	klasifikace
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
Linné	Linná	k1gFnSc2	Linná
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
říši	říše	k1gFnSc4	říše
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Vegetabilia	Vegetabilius	k1gMnSc2	Vegetabilius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnSc1d3	nejjednodušší
dělení	dělení	k1gNnSc1	dělení
rostlin	rostlina	k1gFnPc2	rostlina
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
nižší	nízký	k2eAgFnPc4d2	nižší
rostliny	rostlina	k1gFnPc4	rostlina
(	(	kIx(	(
<g/>
Thallobionta	Thallobionta	k1gMnSc1	Thallobionta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tělem	tělo	k1gNnSc7	tělo
je	být	k5eAaImIp3nS	být
stélka	stélka	k1gFnSc1	stélka
(	(	kIx(	(
<g/>
thallus	thallus	k1gInSc1	thallus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc1d2	vyšší
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Cormobionta	Cormobionta	k1gFnSc1	Cormobionta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
primitivních	primitivní	k2eAgFnPc2d1	primitivní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
mechorosty	mechorost	k1gInPc4	mechorost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
již	již	k9	již
používají	používat	k5eAaImIp3nP	používat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
"	"	kIx"	"
totiž	totiž	k9	totiž
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
příliš	příliš	k6eAd1	příliš
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
systematice	systematika	k1gFnSc6	systematika
hledí	hledět	k5eAaImIp3nS	hledět
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
fylogenetickou	fylogenetický	k2eAgFnSc4d1	fylogenetická
příbuznost	příbuznost	k1gFnSc4	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
různá	různý	k2eAgNnPc4d1	různé
pojetí	pojetí	k1gNnPc4	pojetí
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
nižší	nízký	k2eAgFnPc1d2	nižší
rostliny	rostlina	k1gFnPc1	rostlina
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
Protista	protista	k1gMnSc1	protista
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
říše	říše	k1gFnSc1	říše
však	však	k9	však
rovněž	rovněž	k9	rovněž
není	být	k5eNaImIp3nS	být
přirozená	přirozený	k2eAgFnSc1d1	přirozená
(	(	kIx(	(
<g/>
monofyletická	monofyletický	k2eAgFnSc1d1	monofyletická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
protisté	protista	k1gMnPc1	protista
rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
tak	tak	k8xS	tak
představují	představovat	k5eAaImIp3nP	představovat
buď	buď	k8xC	buď
jen	jen	k9	jen
tzv.	tzv.	kA	tzv.
vyšší	vysoký	k2eAgFnPc4d2	vyšší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnPc1d2	vyšší
rostliny	rostlina	k1gFnPc1	rostlina
společně	společně	k6eAd1	společně
s	s	k7c7	s
zelenými	zelený	k2eAgFnPc7d1	zelená
řasami	řasa	k1gFnPc7	řasa
(	(	kIx(	(
<g/>
Chlorophyta	Chlorophyto	k1gNnPc4	Chlorophyto
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruduchami	ruducha	k1gFnPc7	ruducha
(	(	kIx(	(
<g/>
Rhodophyta	Rhodophyto	k1gNnSc2	Rhodophyto
<g/>
)	)	kIx)	)
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
Glaucophyta	Glaucophyt	k1gInSc2	Glaucophyt
<g/>
..	..	k?	..
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
Rhodophyta	Rhodophyta	k1gFnSc1	Rhodophyta
a	a	k8xC	a
Glaucophyta	Glaucophyta	k1gFnSc1	Glaucophyta
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Biliphytae	Biliphyta	k1gFnSc2	Biliphyta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
zelené	zelený	k2eAgFnPc4d1	zelená
rostliny	rostlina	k1gFnPc4	rostlina
(	(	kIx(	(
<g/>
Viridiplantae	Viridiplanta	k1gFnPc4	Viridiplanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
vyšší	vysoký	k2eAgFnSc2d2	vyšší
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podříši	podříše	k1gFnSc3	podříše
vyšší	vysoký	k2eAgFnSc2d2	vyšší
rostliny	rostlina	k1gFnSc2	rostlina
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
několik	několik	k4yIc1	několik
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
,	,	kIx,	,
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
přirozené	přirozený	k2eAgInPc4d1	přirozený
taxony	taxon	k1gInPc4	taxon
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
oddělení	oddělení	k1gNnSc2	oddělení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
publikacích	publikace	k1gFnPc6	publikace
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přibližný	přibližný	k2eAgInSc1d1	přibližný
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
však	však	k9	však
přežilo	přežít	k5eAaPmAgNnS	přežít
jen	jen	k9	jen
asi	asi	k9	asi
11	[number]	k4	11
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
játrovky	játrovka	k1gFnSc2	játrovka
(	(	kIx(	(
<g/>
Hepatophyta	Hepatophyta	k1gMnSc1	Hepatophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlevíky	hlevík	k1gInPc1	hlevík
(	(	kIx(	(
<g/>
Anthocerophyta	Anthocerophyta	k1gFnSc1	Anthocerophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mechy	mech	k1gInPc1	mech
(	(	kIx(	(
<g/>
Bryophyta	Bryophyta	k1gFnSc1	Bryophyta
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavuně	plavuň	k1gFnSc2	plavuň
(	(	kIx(	(
<g/>
Lycopodiophyta	Lycopodiophyta	k1gMnSc1	Lycopodiophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesličky	přeslička	k1gFnSc2	přeslička
(	(	kIx(	(
<g/>
Equisetophyta	Equisetophyta	k1gMnSc1	Equisetophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapradiny	kapradina	k1gFnSc2	kapradina
(	(	kIx(	(
<g/>
Polypodiophyta	Polypodiophyta	k1gMnSc1	Polypodiophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cykasorosty	cykasorost	k1gInPc1	cykasorost
(	(	kIx(	(
<g/>
Cycadophyta	Cycadophyta	k1gFnSc1	Cycadophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinany	jinan	k1gInPc1	jinan
(	(	kIx(	(
<g/>
Ginkgophyta	Ginkgophyta	k1gFnSc1	Ginkgophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehličnany	jehličnan	k1gInPc1	jehličnan
(	(	kIx(	(
<g/>
Pinophyta	Pinophyta	k1gFnSc1	Pinophyta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
liánovce	liánovka	k1gFnSc6	liánovka
(	(	kIx(	(
<g/>
Gnetophyta	Gnetophyta	k1gFnSc1	Gnetophyta
<g/>
)	)	kIx)	)
a	a	k8xC	a
krytosemenné	krytosemenný	k2eAgNnSc1d1	krytosemenné
(	(	kIx(	(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1	Magnoliophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasifikace	klasifikace	k1gFnSc2	klasifikace
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
#	#	kIx~	#
<g/>
Fylogenetické	fylogenetický	k2eAgInPc1d1	fylogenetický
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
vývoj	vývoj	k1gInSc1	vývoj
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
pomyslný	pomyslný	k2eAgInSc1d1	pomyslný
strom	strom	k1gInSc1	strom
plný	plný	k2eAgInSc1d1	plný
drobných	drobná	k1gFnPc2	drobná
odštěpujících	odštěpující	k2eAgFnPc2d1	odštěpující
se	se	k3xPyFc4	se
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
zjednodušovat	zjednodušovat	k5eAaImF	zjednodušovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
verze	verze	k1gFnSc1	verze
fylogenetického	fylogenetický	k2eAgInSc2d1	fylogenetický
stromu	strom	k1gInSc2	strom
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
názorů	názor	k1gInPc2	názor
vypadá	vypadat	k5eAaPmIp3nS	vypadat
přibližně	přibližně	k6eAd1	přibližně
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
rostlin	rostlina	k1gFnPc2	rostlina
poněkud	poněkud	k6eAd1	poněkud
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
zobecnění	zobecnění	k1gNnSc1	zobecnění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
v	v	k7c6	v
genetice	genetika	k1gFnSc6	genetika
a	a	k8xC	a
genomice	genomika	k1gFnSc6	genomika
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
DNA	dno	k1gNnSc2	dno
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
C	C	kA	C
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc6	množství
přítomném	přítomný	k2eAgNnSc6d1	přítomné
v	v	k7c6	v
pohlavních	pohlavní	k2eAgFnPc6d1	pohlavní
buňkách	buňka	k1gFnPc6	buňka
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
:	:	kIx,	:
např.	např.	kA	např.
huseníček	huseníčko	k1gNnPc2	huseníčko
rolní	rolní	k2eAgFnPc1d1	rolní
(	(	kIx(	(
<g/>
Arabidopsis	Arabidopsis	k1gInSc1	Arabidopsis
thaliana	thaliana	k1gFnSc1	thaliana
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
pouhých	pouhý	k2eAgFnPc2d1	pouhá
125	[number]	k4	125
Mbp	Mbp	k1gFnPc2	Mbp
(	(	kIx(	(
<g/>
milionů	milion	k4xCgInPc2	milion
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
řebčík	řebčík	k1gInSc1	řebčík
Fritillaria	Fritillarium	k1gNnSc2	Fritillarium
assyriaca	assyriac	k1gInSc2	assyriac
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
tisíckrát	tisíckrát	k6eAd1	tisíckrát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
genomu	genom	k1gInSc2	genom
sice	sice	k8xC	sice
příliš	příliš	k6eAd1	příliš
nevypovídá	vypovídat	k5eNaPmIp3nS	vypovídat
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c4	nad
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
DNA	DNA	kA	DNA
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
všechny	všechen	k3xTgFnPc1	všechen
rostliny	rostlina	k1gFnPc1	rostlina
trvalky	trvalka	k1gFnSc2	trvalka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
korelace	korelace	k1gFnPc1	korelace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
příklad	příklad	k1gInSc1	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvedena	uvést	k5eAaPmNgFnS	uvést
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
DNA	dno	k1gNnSc2	dno
jsou	být	k5eAaImIp3nP	být
odolnější	odolný	k2eAgInSc4d2	odolnější
radioaktivnímu	radioaktivní	k2eAgNnSc3d1	radioaktivní
záření	záření	k1gNnSc3	záření
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chromozomů	chromozom	k1gInPc2	chromozom
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
pásmu	pásmo	k1gNnSc6	pásmo
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Rozchodník	rozchodník	k1gInSc1	rozchodník
Sedum	Sedum	k1gNnSc1	Sedum
suaveolens	suaveolens	k6eAd1	suaveolens
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
diploidní	diploidní	k2eAgInPc4d1	diploidní
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
buňce	buňka	k1gFnSc6	buňka
640	[number]	k4	640
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
rostlina	rostlina	k1gFnSc1	rostlina
Machaeranthera	Machaeranthera	k1gFnSc1	Machaeranthera
gracilis	gracilis	k1gFnSc1	gracilis
má	mít	k5eAaImIp3nS	mít
pouhé	pouhý	k2eAgNnSc4d1	pouhé
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
počty	počet	k1gInPc1	počet
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k9	i
od	od	k7c2	od
kapradin	kapradina	k1gFnPc2	kapradina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
mívají	mívat	k5eAaImIp3nP	mívat
jedinou	jediný	k2eAgFnSc4d1	jediná
centromeru	centromera	k1gFnSc4	centromera
<g/>
,	,	kIx,	,
některé	některý	k3yIgNnSc1	některý
však	však	k9	však
jich	on	k3xPp3gMnPc2	on
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
chromozomu	chromozom	k1gInSc6	chromozom
i	i	k9	i
více	hodně	k6eAd2	hodně
kinetochorů	kinetochor	k1gInPc2	kinetochor
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
dělící	dělící	k2eAgNnSc1d1	dělící
vřeténko	vřeténko	k1gNnSc1	vřeténko
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
polyploidie	polyploidie	k1gFnSc1	polyploidie
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
obsah	obsah	k1gInSc4	obsah
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgFnPc2	dva
identických	identický	k2eAgFnPc2d1	identická
kopií	kopie	k1gFnPc2	kopie
genomu	genom	k1gInSc2	genom
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
tělní	tělní	k2eAgFnSc6d1	tělní
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
evoluční	evoluční	k2eAgFnSc6d1	evoluční
historii	historie	k1gFnSc6	historie
zkopírují	zkopírovat	k5eAaPmIp3nP	zkopírovat
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
genom	genom	k1gInSc4	genom
a	a	k8xC	a
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
přítomna	přítomen	k2eAgFnSc1d1	přítomna
v	v	k7c6	v
několikanásobně	několikanásobně	k6eAd1	několikanásobně
vyšším	vysoký	k2eAgNnSc6d2	vyšší
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Známí	známý	k1gMnPc1	známý
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
tetraploidi	tetraploid	k1gMnPc1	tetraploid
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
DNA	DNA	kA	DNA
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
než	než	k8xS	než
jejich	jejich	k3xOp3gMnPc1	jejich
diploidní	diploidní	k2eAgMnPc1d1	diploidní
předci	předek	k1gMnPc1	předek
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
genů	gen	k1gInPc2	gen
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
příliš	příliš	k6eAd1	příliš
neodvíjí	odvíjet	k5eNaImIp3nS	odvíjet
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Modelová	modelový	k2eAgFnSc1d1	modelová
rostlina	rostlina	k1gFnSc1	rostlina
huseníček	huseníčko	k1gNnPc2	huseníčko
rolní	rolní	k2eAgFnPc1d1	rolní
jich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
srovnatelně	srovnatelně	k6eAd1	srovnatelně
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
počty	počet	k1gInPc1	počet
genů	gen	k1gInPc2	gen
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
rostlin	rostlina	k1gFnPc2	rostlina
zřejmě	zřejmě	k6eAd1	zřejmě
nebudou	být	k5eNaImBp3nP	být
nijak	nijak	k6eAd1	nijak
radikálně	radikálně	k6eAd1	radikálně
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
genů	gen	k1gInPc2	gen
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
v	v	k7c6	v
několika	několik	k4yIc6	několik
kopiích	kopie	k1gFnPc6	kopie
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
během	během	k7c2	během
času	čas	k1gInSc2	čas
přizpůsobená	přizpůsobený	k2eAgFnSc1d1	přizpůsobená
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vlastní	vlastní	k2eAgFnSc3d1	vlastní
funkci	funkce	k1gFnSc3	funkce
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
kopie	kopie	k1gFnPc1	kopie
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
množství	množství	k1gNnSc4	množství
genových	genový	k2eAgFnPc2d1	genová
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
DNA	DNA	kA	DNA
jen	jen	k6eAd1	jen
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
představuje	představovat	k5eAaImIp3nS	představovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
plastidová	plastidový	k2eAgFnSc1d1	plastidová
DNA	dna	k1gFnSc1	dna
v	v	k7c6	v
plastidech	plastid	k1gInPc6	plastid
a	a	k8xC	a
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
díky	díky	k7c3	díky
fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
organickou	organický	k2eAgFnSc4d1	organická
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
život	život	k1gInSc4	život
dalších	další	k2eAgInPc2d1	další
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
kořeny	kořen	k1gInPc1	kořen
zadržují	zadržovat	k5eAaImIp3nP	zadržovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
zpevňují	zpevňovat	k5eAaImIp3nP	zpevňovat
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
brání	bránit	k5eAaImIp3nP	bránit
erozi	eroze	k1gFnSc4	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Nenahraditelný	nahraditelný	k2eNgInSc1d1	nenahraditelný
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
biosféru	biosféra	k1gFnSc4	biosféra
mají	mít	k5eAaImIp3nP	mít
díky	dík	k1gInPc7	dík
unikatní	unikatní	k2eAgFnSc2d1	unikatní
schopnosti	schopnost	k1gFnSc2	schopnost
zadržovat	zadržovat	k5eAaImF	zadržovat
a	a	k8xC	a
řízeně	řízeně	k6eAd1	řízeně
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ničení	ničení	k1gNnSc1	ničení
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
porostů	porost	k1gInPc2	porost
má	mít	k5eAaImIp3nS	mít
prokazatelně	prokazatelně	k6eAd1	prokazatelně
následek	následek	k1gInSc4	následek
v	v	k7c6	v
lokálním	lokální	k2eAgNnSc6d1	lokální
snížení	snížení	k1gNnSc6	snížení
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
symbiotické	symbiotický	k2eAgInPc4d1	symbiotický
organizmy	organizmus	k1gInPc4	organizmus
pro	pro	k7c4	pro
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
biogenní	biogenní	k2eAgInPc1d1	biogenní
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
regenerují	regenerovat	k5eAaBmIp3nP	regenerovat
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
veškerá	veškerý	k3xTgFnSc1	veškerý
lidská	lidský	k2eAgFnSc1d1	lidská
strava	strava	k1gFnSc1	strava
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
přírodních	přírodní	k2eAgFnPc6d1	přírodní
plodinách	plodina	k1gFnPc6	plodina
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
výživné	výživný	k2eAgFnPc1d1	výživná
jsou	být	k5eAaImIp3nP	být
obiloviny	obilovina	k1gFnPc1	obilovina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
nebo	nebo	k8xC	nebo
další	další	k2eAgFnPc4d1	další
důležité	důležitý	k2eAgFnPc4d1	důležitá
plodiny	plodina	k1gFnPc4	plodina
jako	jako	k8xC	jako
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc4	luštěnina
a	a	k8xC	a
olejniny	olejnina	k1gFnPc4	olejnina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
koření	koření	k1gNnPc1	koření
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Cukr	cukr	k1gInSc1	cukr
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
a	a	k8xC	a
margarín	margarín	k1gInSc1	margarín
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
sóji	sója	k1gFnSc2	sója
<g/>
,	,	kIx,	,
oliv	oliva	k1gFnPc2	oliva
<g/>
,	,	kIx,	,
slunečnic	slunečnice	k1gFnPc2	slunečnice
a	a	k8xC	a
z	z	k7c2	z
palmy	palma	k1gFnSc2	palma
olejné	olejný	k2eAgFnSc2d1	olejná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
využívají	využívat	k5eAaPmIp3nP	využívat
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc4	dřevo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
používáme	používat	k5eAaImIp1nP	používat
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
nebo	nebo	k8xC	nebo
sportovního	sportovní	k2eAgNnSc2d1	sportovní
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
lnu	len	k1gInSc2	len
a	a	k8xC	a
ze	z	k7c2	z
syntetických	syntetický	k2eAgFnPc2d1	syntetická
vláken	vlákna	k1gFnPc2	vlákna
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
přírodního	přírodní	k2eAgNnSc2d1	přírodní
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
ropa	ropa	k1gFnSc1	ropa
jsou	být	k5eAaImIp3nP	být
fosilní	fosilní	k2eAgNnPc4d1	fosilní
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
také	také	k9	také
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
slouží	sloužit	k5eAaImIp3nP	sloužit
též	též	k9	též
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
,	,	kIx,	,
drog	droga	k1gFnPc2	droga
nebo	nebo	k8xC	nebo
jedů	jed	k1gInPc2	jed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tepelné	tepelný	k2eAgFnSc2d1	tepelná
či	či	k8xC	či
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
jejich	jejich	k3xOp3gNnSc7	jejich
spalováním	spalování	k1gNnSc7	spalování
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
dřevní	dřevní	k2eAgInPc1d1	dřevní
odpady	odpad	k1gInPc1	odpad
<g/>
,	,	kIx,	,
sláma	sláma	k1gFnSc1	sláma
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
popř.	popř.	kA	popř.
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
biolihu	biolih	k1gInSc2	biolih
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
coby	coby	k?	coby
palivo	palivo	k1gNnSc1	palivo
ve	v	k7c6	v
spalovacích	spalovací	k2eAgInPc6d1	spalovací
motorech	motor	k1gInPc6	motor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
lidské	lidský	k2eAgFnSc2d1	lidská
i	i	k8xC	i
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
stravy	strava	k1gFnSc2	strava
rostliny	rostlina	k1gFnSc2	rostlina
také	také	k9	také
snižují	snižovat	k5eAaImIp3nP	snižovat
prašnost	prašnost	k1gFnSc4	prašnost
a	a	k8xC	a
hlučnost	hlučnost	k1gFnSc4	hlučnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
také	také	k9	také
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
mikroklima	mikroklima	k1gNnSc4	mikroklima
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
esteticky	esteticky	k6eAd1	esteticky
-	-	kIx~	-
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
pokojové	pokojový	k2eAgFnPc1d1	pokojová
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
ozdobné	ozdobný	k2eAgFnPc1d1	ozdobná
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
zámecké	zámecký	k2eAgInPc1d1	zámecký
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
stromořadí	stromořadí	k1gNnSc1	stromořadí
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
apod.	apod.	kA	apod.
</s>
