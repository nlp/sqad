<s>
Rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Plantae	Planta	k1gFnPc1	Planta
<g/>
,	,	kIx,	,
též	též	k9	též
nově	nově	k6eAd1	nově
Archaeplastida	Archaeplastida	k1gFnSc1	Archaeplastida
či	či	k8xC	či
Primoplantae	Primoplantae	k1gFnSc1	Primoplantae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
říše	říše	k1gFnSc1	říše
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
fotosyntetických	fotosyntetický	k2eAgInPc2d1	fotosyntetický
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
