<s>
Mojmír	Mojmír	k1gMnSc1	Mojmír
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Františkem	František	k1gMnSc7	František
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
Veselí	veselí	k1gNnSc2	veselí
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
)	)	kIx)	)
motorovou	motorový	k2eAgFnSc4d1	motorová
tříkolku	tříkolka	k1gFnSc4	tříkolka
Oskar	Oskar	k1gMnSc1	Oskar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Solnici	solnice	k1gFnSc6	solnice
vyrábět	vyrábět	k5eAaImF	vyrábět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Velorex	Velorex	k1gInSc1	Velorex
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
František	František	k1gMnSc1	František
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
právě	právě	k9	právě
ve	v	k7c6	v
Velorexu	Velorex	k1gInSc6	Velorex
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
ruční	ruční	k2eAgNnSc4d1	ruční
řízení	řízení	k1gNnSc4	řízení
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
účinkovalo	účinkovat	k5eAaImAgNnS	účinkovat
na	na	k7c6	na
mechanickém	mechanický	k2eAgInSc6d1	mechanický
principu	princip	k1gInSc6	princip
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
použitelné	použitelný	k2eAgNnSc1d1	použitelné
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
automobilů	automobil	k1gInPc2	automobil
tehdy	tehdy	k6eAd1	tehdy
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
provozovaných	provozovaný	k2eAgNnPc2d1	provozované
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
patentována	patentovat	k5eAaBmNgFnS	patentovat
a	a	k8xC	a
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
(	(	kIx(	(
<g/>
a	a	k8xC	a
ještě	ještě	k9	ještě
používaná	používaný	k2eAgFnSc1d1	používaná
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
systém	systém	k1gInSc4	systém
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
montovala	montovat	k5eAaImAgFnS	montovat
do	do	k7c2	do
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
autodílně	autodílna	k1gFnSc6	autodílna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
–	–	k?	–
Javorce	Javorka	k1gFnSc6	Javorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
zpracování	zpracování	k1gNnSc3	zpracování
a	a	k8xC	a
výrobě	výroba	k1gFnSc3	výroba
z	z	k7c2	z
laminátu	laminát	k1gInSc2	laminát
a	a	k8xC	a
termoplastů	termoplast	k1gInPc2	termoplast
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
patentů	patent	k1gInPc2	patent
a	a	k8xC	a
zlepšovacích	zlepšovací	k2eAgInPc2d1	zlepšovací
návrhů	návrh	k1gInPc2	návrh
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
domově	domov	k1gInSc6	domov
důchodců	důchodce	k1gMnPc2	důchodce
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
<g/>
.	.	kIx.	.
</s>
