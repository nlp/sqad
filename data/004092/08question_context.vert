<s>
Mojmír	Mojmír	k1gMnSc1	Mojmír
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Františkem	František	k1gMnSc7	František
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
Veselí	veselí	k1gNnSc2	veselí
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
)	)	kIx)	)
motorovou	motorový	k2eAgFnSc4d1	motorová
tříkolku	tříkolka	k1gFnSc4	tříkolka
Oskar	Oskar	k1gMnSc1	Oskar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Solnici	solnice	k1gFnSc6	solnice
vyrábět	vyrábět	k5eAaImF	vyrábět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Velorex	Velorex	k1gInSc1	Velorex
<g/>
.	.	kIx.	.
</s>
