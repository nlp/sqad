<p>
<s>
Město	město	k1gNnSc1	město
Bystřice	Bystřice	k1gFnSc1	Bystřice
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Bistritz	Bistritz	k1gInSc1	Bistritz
nebo	nebo	k8xC	nebo
Bistritz	Bistritz	k1gInSc1	Bistritz
bei	bei	k?	bei
Beneschau	Beneschaus	k1gInSc2	Beneschaus
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
okrese	okres	k1gInSc6	okres
Benešov	Benešov	k1gInSc1	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
6339	[number]	k4	6339
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
7	[number]	k4	7
km	km	kA	km
severně	severně	k6eAd1	severně
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
16	[number]	k4	16
km	km	kA	km
východně	východně	k6eAd1	východně
město	město	k1gNnSc1	město
Vlašim	Vlašim	k1gFnSc1	Vlašim
<g/>
,	,	kIx,	,
19	[number]	k4	19
km	km	kA	km
západně	západně	k6eAd1	západně
město	město	k1gNnSc4	město
Sedlčany	Sedlčany	k1gInPc4	Sedlčany
a	a	k8xC	a
30	[number]	k4	30
km	km	kA	km
severně	severně	k6eAd1	severně
město	město	k1gNnSc1	město
Říčany	Říčany	k1gInPc7	Říčany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
a	a	k8xC	a
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
nůše	nůše	k1gFnSc2	nůše
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
skloňování	skloňování	k1gNnSc1	skloňování
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Bystřice	Bystřice	k1gFnSc1	Bystřice
bez	bez	k7c2	bez
Bystřice	Bystřice	k1gFnSc2	Bystřice
-	-	kIx~	-
nikoliv	nikoliv	k9	nikoliv
bez	bez	k7c2	bez
Bystřic	Bystřice	k1gFnPc2	Bystřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
deseti	deset	k4xCc6	deset
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
–	–	k?	–
část	část	k1gFnSc1	část
Bystřice	Bystřice	k1gFnSc1	Bystřice
</s>
</p>
<p>
<s>
Božkovice	Božkovice	k1gFnSc1	Božkovice
–	–	k?	–
části	část	k1gFnSc2	část
Božkovice	Božkovice	k1gFnSc2	Božkovice
<g/>
,	,	kIx,	,
Radošovice	Radošovice	k1gFnSc2	Radošovice
a	a	k8xC	a
Tožice	Tožice	k1gFnSc2	Tožice
</s>
</p>
<p>
<s>
Drachkov	Drachkov	k1gInSc1	Drachkov
–	–	k?	–
části	část	k1gFnSc2	část
Drachkov	Drachkov	k1gInSc4	Drachkov
a	a	k8xC	a
Zahořany	Zahořan	k1gMnPc4	Zahořan
</s>
</p>
<p>
<s>
Jinošice	Jinošice	k1gFnSc1	Jinošice
–	–	k?	–
části	část	k1gFnSc2	část
Jinošice	Jinošice	k1gFnSc2	Jinošice
<g/>
,	,	kIx,	,
Líštěnec	Líštěnec	k1gMnSc1	Líštěnec
a	a	k8xC	a
Opřetice	Opřetika	k1gFnSc6	Opřetika
</s>
</p>
<p>
<s>
Jírovice	Jírovice	k1gFnSc1	Jírovice
–	–	k?	–
části	část	k1gFnSc2	část
Jírovice	Jírovice	k1gFnSc1	Jírovice
<g/>
,	,	kIx,	,
Hůrka	hůrka	k1gFnSc1	hůrka
<g/>
,	,	kIx,	,
Jarkovice	Jarkovice	k1gFnSc1	Jarkovice
a	a	k8xC	a
Semovice	Semovice	k1gFnSc1	Semovice
</s>
</p>
<p>
<s>
Kobylí	kobylí	k2eAgFnSc1d1	kobylí
–	–	k?	–
části	část	k1gFnSc3	část
Kobylí	kobylí	k2eAgMnPc1d1	kobylí
<g/>
,	,	kIx,	,
Hlivín	Hlivín	k1gMnSc1	Hlivín
a	a	k8xC	a
Vojslavice	Vojslavice	k1gFnSc1	Vojslavice
</s>
</p>
<p>
<s>
Líšno	Líšno	k6eAd1	Líšno
–	–	k?	–
části	část	k1gFnPc4	část
Líšno	Líšno	k6eAd1	Líšno
a	a	k8xC	a
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
Lhota	Lhota	k1gFnSc1	Lhota
</s>
</p>
<p>
<s>
Nesvačily	svačit	k5eNaImAgFnP	svačit
u	u	k7c2	u
Bystřice	Bystřice	k1gFnSc2	Bystřice
–	–	k?	–
části	část	k1gFnPc1	část
Nesvačily	svačit	k5eNaImAgFnP	svačit
a	a	k8xC	a
Petrovice	Petrovice	k1gFnPc1	Petrovice
</s>
</p>
<p>
<s>
Ouběnice	Ouběnice	k1gFnSc1	Ouběnice
u	u	k7c2	u
Votic	Votice	k1gFnPc2	Votice
–	–	k?	–
části	část	k1gFnSc2	část
Ouběnice	Ouběnice	k1gFnSc2	Ouběnice
<g/>
,	,	kIx,	,
Jeleneč	Jeleneč	k1gMnSc1	Jeleneč
<g/>
,	,	kIx,	,
Jiřín	Jiřín	k1gMnSc1	Jiřín
a	a	k8xC	a
Strženec	Strženec	k1gMnSc1	Strženec
</s>
</p>
<p>
<s>
Tvoršovice	Tvoršovice	k1gFnSc1	Tvoršovice
–	–	k?	–
části	část	k1gFnSc2	část
Tvoršovice	Tvoršovice	k1gFnSc2	Tvoršovice
a	a	k8xC	a
Mlýny	mlýn	k1gInPc4	mlýn
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
připomínána	připomínat	k5eAaImNgFnS	připomínat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
potřeb	potřeba	k1gFnPc2	potřeba
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
mírné	mírný	k2eAgFnSc6d1	mírná
kotlině	kotlina	k1gFnSc6	kotlina
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
říčky	říčka	k1gFnSc2	říčka
lákala	lákat	k5eAaImAgFnS	lákat
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
a	a	k8xC	a
napojení	napojení	k1gNnSc4	napojení
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
vísce	víska	k1gFnSc6	víska
Léště	Léšť	k1gFnSc2	Léšť
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Líšně	Líšeň	k1gFnPc1	Líšeň
<g/>
,	,	kIx,	,
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc4d1	původní
vlastníky	vlastník	k1gMnPc4	vlastník
Benešovice	Benešovice	k1gFnSc2	Benešovice
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Šternberkové	Šternberková	k1gFnSc2	Šternberková
<g/>
.	.	kIx.	.
</s>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
byla	být	k5eAaImAgFnS	být
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1471	[number]	k4	1471
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
městys	městys	k1gInSc4	městys
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
mnoha	mnoho	k4c2	mnoho
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
západně	západně	k6eAd1	západně
od	od	k7c2	od
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičiště	cvičiště	k1gNnSc2	cvičiště
Zbraní	zbraň	k1gFnPc2	zbraň
SS	SS	kA	SS
Benešov	Benešov	k1gInSc1	Benešov
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Bystřice	Bystřice	k1gFnSc1	Bystřice
vydalo	vydat	k5eAaPmAgNnS	vydat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
vystěhování	vystěhování	k1gNnSc3	vystěhování
publikaci	publikace	k1gFnSc6	publikace
Poskládané	poskládaný	k2eAgFnPc4d1	poskládaná
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
sestavenou	sestavený	k2eAgFnSc4d1	sestavená
ze	z	k7c2	z
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
pamětníků	pamětník	k1gMnPc2	pamětník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
se	se	k3xPyFc4	se
během	během	k7c2	během
války	válka	k1gFnSc2	válka
nacházel	nacházet	k5eAaImAgMnS	nacházet
také	také	k9	také
koncentrační	koncentrační	k2eAgInSc4d1	koncentrační
tábor	tábor	k1gInSc4	tábor
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
smíšených	smíšený	k2eAgFnPc2d1	smíšená
manželství	manželství	k1gNnPc4	manželství
a	a	k8xC	a
manžele	manžel	k1gMnPc4	manžel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
rozvést	rozvést	k5eAaPmF	rozvést
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
židovskými	židovský	k2eAgFnPc7d1	židovská
manželkami	manželka	k1gFnPc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Táborem	Tábor	k1gInSc7	Tábor
tak	tak	k6eAd1	tak
prošlo	projít	k5eAaPmAgNnS	projít
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
českých	český	k2eAgFnPc2d1	Česká
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekora	k1gFnSc1	Sekora
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Jára	Jára	k1gMnSc1	Jára
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rychman	Rychman	k1gMnSc1	Rychman
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Jiří	Jiří	k1gMnSc1	Jiří
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Alfred	Alfred	k1gMnSc1	Alfred
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
,	,	kIx,	,
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
tábora	tábor	k1gInSc2	tábor
připomíná	připomínat	k5eAaImIp3nS	připomínat
pomník	pomník	k1gInSc1	pomník
s	s	k7c7	s
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
umístěný	umístěný	k2eAgInSc4d1	umístěný
západně	západně	k6eAd1	západně
od	od	k7c2	od
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
nedaleko	nedaleko	k7c2	nedaleko
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Benešov	Benešov	k1gInSc1	Benešov
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Benešov	Benešov	k1gInSc1	Benešov
</s>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1932	[number]	k4	1932
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bystřice	Bystřice	k1gFnSc2	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
(	(	kIx(	(
<g/>
1600	[number]	k4	1600
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Instituce	instituce	k1gFnSc1	instituce
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgInSc1d1	telefonní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
katol	katol	k1gInSc1	katol
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
2	[number]	k4	2
cihelny	cihelna	k1gFnPc4	cihelna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
dláta	dláto	k1gNnPc4	dláto
<g/>
,	,	kIx,	,
nebozezy	nebozez	k1gInPc1	nebozez
a	a	k8xC	a
hoblíková	hoblíkový	k2eAgNnPc1d1	hoblíkový
železa	železo	k1gNnPc1	železo
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
karoserií	karoserie	k1gFnPc2	karoserie
<g/>
,	,	kIx,	,
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
pletárna	pletárna	k1gFnSc1	pletárna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
velkostatek	velkostatek	k1gInSc1	velkostatek
Söller	Söller	k1gInSc1	Söller
(	(	kIx(	(
<g/>
pamětníci	pamětník	k1gMnPc1	pamětník
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
"	"	kIx"	"
<g/>
šoler	šoler	k1gInSc1	šoler
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
3	[number]	k4	3
lékaři	lékař	k1gMnPc7	lékař
<g/>
,	,	kIx,	,
2	[number]	k4	2
autodopravci	autodopravce	k1gMnPc7	autodopravce
<g/>
,	,	kIx,	,
bio	bio	k?	bio
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
2	[number]	k4	2
cukráři	cukrář	k1gMnPc7	cukrář
<g/>
,	,	kIx,	,
drogerie	drogerie	k1gFnSc1	drogerie
<g/>
,	,	kIx,	,
hodinář	hodinář	k1gMnSc1	hodinář
<g/>
,	,	kIx,	,
3	[number]	k4	3
hostince	hostinec	k1gInPc1	hostinec
<g/>
,	,	kIx,	,
7	[number]	k4	7
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
mechanik	mechanik	k1gMnSc1	mechanik
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
obuví	obuv	k1gFnSc7	obuv
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
5	[number]	k4	5
sadařů	sadař	k1gMnPc2	sadař
<g/>
,	,	kIx,	,
3	[number]	k4	3
sedláři	sedlář	k1gMnPc7	sedlář
<g/>
,	,	kIx,	,
Okresní	okresní	k2eAgFnSc1d1	okresní
záložna	záložna	k1gFnSc1	záložna
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
spořitelna	spořitelna	k1gFnSc1	spořitelna
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
,	,	kIx,	,
Živnostensko-obchodnická	živnostenskobchodnický	k2eAgFnSc1d1	živnostensko-obchodnická
záložna	záložna	k1gFnSc1	záložna
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
<g/>
,	,	kIx,	,
2	[number]	k4	2
zahradníci	zahradník	k1gMnPc1	zahradník
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Božkovice	Božkovice	k1gFnSc2	Božkovice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Radošovice	Radošovice	k1gFnSc1	Radošovice
<g/>
,	,	kIx,	,
Tožice	Tožice	k1gFnSc1	Tožice
<g/>
,	,	kIx,	,
564	[number]	k4	564
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
družstvo	družstvo	k1gNnSc1	družstvo
pro	pro	k7c4	pro
rozvod	rozvod	k1gInSc4	rozvod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
Božkovicích	Božkovice	k1gFnPc6	Božkovice
<g/>
,	,	kIx,	,
5	[number]	k4	5
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
2	[number]	k4	2
koláři	kolář	k1gMnPc7	kolář
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
3	[number]	k4	3
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Drachkov	Drachkov	k1gInSc1	Drachkov
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Zahořany	Zahořan	k1gMnPc4	Zahořan
<g/>
,	,	kIx,	,
550	[number]	k4	550
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
Spořitelní	spořitelní	k2eAgInSc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
Drachkov	Drachkov	k1gInSc4	Drachkov
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
velkostatek	velkostatek	k1gInSc4	velkostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Jinošice	Jinošice	k1gFnSc2	Jinošice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Líštěnec	Líštěnec	k1gMnSc1	Líštěnec
<g/>
,	,	kIx,	,
Opřetice	Opřetika	k1gFnSc6	Opřetika
<g/>
,	,	kIx,	,
Hutě	huť	k1gFnPc1	huť
<g/>
,	,	kIx,	,
Vrbětín	Vrbětín	k1gInSc1	Vrbětín
<g/>
,	,	kIx,	,
315	[number]	k4	315
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
2	[number]	k4	2
trafiky	trafika	k1gFnSc2	trafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Jirovice	Jirovice	k1gFnSc2	Jirovice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Hanzlov	Hanzlov	k1gInSc1	Hanzlov
<g/>
,	,	kIx,	,
Hůrka	hůrka	k1gFnSc1	hůrka
<g/>
,	,	kIx,	,
Chvojen	Chvojen	k2eAgInSc1d1	Chvojen
<g/>
,	,	kIx,	,
Jarkovice	Jarkovice	k1gFnSc1	Jarkovice
<g/>
,	,	kIx,	,
Mariánovice	Mariánovice	k1gFnSc1	Mariánovice
<g/>
,	,	kIx,	,
Semovice	Semovice	k1gFnSc1	Semovice
<g/>
,	,	kIx,	,
606	[number]	k4	606
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
pokrývač	pokrývač	k1gMnSc1	pokrývač
<g/>
,	,	kIx,	,
rolník	rolník	k1gMnSc1	rolník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
tesařský	tesařský	k2eAgMnSc1d1	tesařský
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
2	[number]	k4	2
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
2	[number]	k4	2
velkostatky	velkostatek	k1gInPc4	velkostatek
Správy	správa	k1gFnSc2	správa
státních	státní	k2eAgInPc2d1	státní
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Líšno	Líšno	k6eAd1	Líšno
(	(	kIx(	(
<g/>
přísl	přísnout	k5eAaPmAgMnS	přísnout
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgNnSc1d1	horní
Podhájí	Podhájí	k1gNnSc1	Podhájí
<g/>
,	,	kIx,	,
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
754	[number]	k4	754
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInPc1	hostinec
<g/>
,	,	kIx,	,
2	[number]	k4	2
koláři	kolář	k1gMnPc7	kolář
<g/>
,	,	kIx,	,
2	[number]	k4	2
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
2	[number]	k4	2
obuvníci	obuvník	k1gMnPc1	obuvník
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
3	[number]	k4	3
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
2	[number]	k4	2
velkostatky	velkostatek	k1gInPc1	velkostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Nesvačily	svačit	k5eNaImAgFnP	svačit
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Petrovice	Petrovice	k1gFnSc1	Petrovice
<g/>
,	,	kIx,	,
633	[number]	k4	633
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
2	[number]	k4	2
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
řezník	řezník	k1gMnSc1	řezník
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
Krása	krása	k1gFnSc1	krása
(	(	kIx(	(
<g/>
nájemce	nájemce	k1gMnSc1	nájemce
dvora	dvůr	k1gInSc2	dvůr
Petrovice	Petrovice	k1gFnSc2	Petrovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Ouběnice	Ouběnice	k1gFnSc2	Ouběnice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Jiřín	Jiřín	k1gMnSc1	Jiřín
<g/>
,	,	kIx,	,
Strženec	Strženec	k1gMnSc1	Strženec
<g/>
,	,	kIx,	,
547	[number]	k4	547
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
katol	katol	k1gInSc1	katol
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
chudobinec	chudobinec	k1gInSc1	chudobinec
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
2	[number]	k4	2
koláři	kolář	k1gMnPc7	kolář
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
4	[number]	k4	4
hostince	hostinec	k1gInPc1	hostinec
<g/>
,	,	kIx,	,
2	[number]	k4	2
kováři	kovář	k1gMnPc7	kovář
<g/>
,	,	kIx,	,
3	[number]	k4	3
obuvníci	obuvník	k1gMnPc1	obuvník
<g/>
,	,	kIx,	,
2	[number]	k4	2
porodní	porodní	k2eAgFnPc1d1	porodní
asistentky	asistentka	k1gFnPc1	asistentka
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
3	[number]	k4	3
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
3	[number]	k4	3
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
Spořitelní	spořitelní	k2eAgFnSc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Ouběnicích	Ouběnice	k1gFnPc6	Ouběnice
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Tvoršovice	Tvoršovice	k1gFnSc2	Tvoršovice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
284	[number]	k4	284
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchodníci	obchodník	k1gMnPc1	obchodník
s	s	k7c7	s
dobytkem	dobytek	k1gInSc7	dobytek
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
lihovar	lihovar	k1gInSc1	lihovar
<g/>
,	,	kIx,	,
3	[number]	k4	3
mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
řezník	řezník	k1gMnSc1	řezník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
majitel	majitel	k1gMnSc1	majitel
paláce	palác	k1gInSc2	palác
U	u	k7c2	u
Nováků	Novák	k1gMnPc2	Novák
ve	v	k7c6	v
Vodičkově	Vodičkův	k2eAgFnSc6d1	Vodičkova
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatých	svatý	k1gMnPc2	svatý
Šimona	Šimon	k1gMnSc2	Šimon
a	a	k8xC	a
Judy	judo	k1gNnPc7	judo
–	–	k?	–
barokní	barokní	k2eAgInSc4d1	barokní
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
gotické	gotický	k2eAgFnSc2d1	gotická
stavby	stavba	k1gFnSc2	stavba
</s>
</p>
<p>
<s>
Fara	fara	k1gFnSc1	fara
–	–	k?	–
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
</s>
</p>
<p>
<s>
Výklenková	výklenkový	k2eAgFnSc1d1	výklenková
kaplička	kaplička	k1gFnSc1	kaplička
</s>
</p>
<p>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
</s>
</p>
<p>
<s>
Usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
76	[number]	k4	76
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
občanskou	občanský	k2eAgFnSc4d1	občanská
vybavenost	vybavenost	k1gFnSc4	vybavenost
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
,	,	kIx,	,
domov	domov	k1gInSc1	domov
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
i	i	k8xC	i
školka	školka	k1gFnSc1	školka
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
katolická	katolický	k2eAgFnSc1d1	katolická
duchovní	duchovní	k2eAgFnSc1d1	duchovní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
kolem	kolem	k7c2	kolem
náměstí	náměstí	k1gNnSc2	náměstí
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředěn	k2eAgInPc1d1	soustředěn
četné	četný	k2eAgInPc1d1	četný
obchody	obchod	k1gInPc1	obchod
a	a	k8xC	a
několik	několik	k4yIc1	několik
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
místním	místní	k2eAgMnPc3d1	místní
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
patří	patřit	k5eAaImIp3nS	patřit
ZD	ZD	kA	ZD
NOVA	nova	k1gFnSc1	nova
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
,	,	kIx,	,
NAREX	NAREX	kA	NAREX
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
,	,	kIx,	,
ALPLA	ALPLA	kA	ALPLA
Petrovice	Petrovice	k1gFnSc1	Petrovice
a	a	k8xC	a
četní	četný	k2eAgMnPc1d1	četný
drobní	drobný	k2eAgMnPc1d1	drobný
živnostníci	živnostník	k1gMnPc1	živnostník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dynamicky	dynamicky	k6eAd1	dynamicky
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nové	nový	k2eAgFnSc3d1	nová
bytové	bytový	k2eAgFnSc3d1	bytová
výstavbě	výstavba	k1gFnSc3	výstavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženský	náboženský	k2eAgInSc4d1	náboženský
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
funguje	fungovat	k5eAaImIp3nS	fungovat
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Bystřice	Bystřice	k1gFnSc1	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
s	s	k7c7	s
farním	farní	k2eAgInSc7d1	farní
úřadem	úřad	k1gInSc7	úřad
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
<g/>
,	,	kIx,	,
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
patří	patřit	k5eAaImIp3nS	patřit
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Ouběnic	Ouběnice	k1gFnPc2	Ouběnice
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
farnosti	farnost	k1gFnSc2	farnost
votické	votický	k2eAgFnSc2d1	Votická
<g/>
.	.	kIx.	.
</s>
<s>
Farnost	farnost	k1gFnSc1	farnost
je	být	k5eAaImIp3nS	být
spravována	spravován	k2eAgFnSc1d1	spravována
administrátorem	administrátor	k1gMnSc7	administrátor
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
vikariátu	vikariát	k1gInSc2	vikariát
benešovského	benešovský	k2eAgInSc2d1	benešovský
a	a	k8xC	a
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
4276	[number]	k4	4276
obyvatel	obyvatel	k1gMnPc2	obyvatel
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
buďto	buďto	k8xC	buďto
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
1280	[number]	k4	1280
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
víru	víra	k1gFnSc4	víra
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
(	(	kIx(	(
<g/>
2168	[number]	k4	2168
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k9	jako
věřící	věřící	k1gFnPc4	věřící
nehlásící	hlásící	k2eNgFnPc4d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
278	[number]	k4	278
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jako	jako	k9	jako
věřící	věřící	k1gFnSc4	věřící
<g/>
,	,	kIx,	,
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
hlásící	hlásící	k2eAgNnSc1d1	hlásící
se	se	k3xPyFc4	se
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
550	[number]	k4	550
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvíce	hodně	k6eAd3	hodně
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
(	(	kIx(	(
<g/>
430	[number]	k4	430
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
katolické	katolický	k2eAgInPc1d1	katolický
kostely	kostel	k1gInPc1	kostel
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
(	(	kIx(	(
<g/>
farní	farní	k2eAgMnSc1d1	farní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tožici	Tožice	k1gFnSc6	Tožice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ouběnicích	Ouběnice	k1gFnPc6	Ouběnice
a	a	k8xC	a
v	v	k7c6	v
Nesvačilech	Nesvačil	k1gInPc6	Nesvačil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nesvačilech	Nesvačil	k1gInPc6	Nesvačil
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
kostel	kostel	k1gInSc1	kostel
církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
spravovaný	spravovaný	k2eAgMnSc1d1	spravovaný
z	z	k7c2	z
Jílového	Jílové	k1gNnSc2	Jílové
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
vikariátu	vikariát	k1gInSc2	vikariát
Praha-Venkov	Praha-Venkov	k1gInSc1	Praha-Venkov
<g/>
,	,	kIx,	,
do	do	k7c2	do
diecéze	diecéze	k1gFnSc2	diecéze
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
komunikace	komunikace	k1gFnSc1	komunikace
–	–	k?	–
Městem	město	k1gNnSc7	město
procházejí	procházet	k5eAaImIp3nP	procházet
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
Mirošovice	Mirošovice	k1gFnSc1	Mirošovice
-	-	kIx~	-
Benešov	Benešov	k1gInSc1	Benešov
-	-	kIx~	-
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Tábor	Tábor	k1gInSc1	Tábor
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
111	[number]	k4	111
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Divišov	Divišov	k1gInSc1	Divišov
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
.	.	kIx.	.
<g/>
Železnice	železnice	k1gFnSc1	železnice
–	–	k?	–
Město	město	k1gNnSc1	město
Bystřice	Bystřice	k1gFnSc1	Bystřice
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
220	[number]	k4	220
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
<g/>
)	)	kIx)	)
Benešov	Benešov	k1gInSc1	Benešov
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
-	-	kIx~	-
Tábor	Tábor	k1gInSc1	Tábor
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
4	[number]	k4	4
<g/>
.	.	kIx.	.
koridoru	koridor	k1gInSc2	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
zásadní	zásadní	k2eAgFnPc1d1	zásadní
modernizace	modernizace	k1gFnPc1	modernizace
včetně	včetně	k7c2	včetně
přeložení	přeložení	k1gNnSc2	přeložení
a	a	k8xC	a
napřímení	napřímení	k1gNnSc2	napřímení
některých	některý	k3yIgInPc2	některý
úseků	úsek	k1gInPc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napřímení	napřímení	k1gNnSc6	napřímení
trati	trať	k1gFnSc2	trať
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
myšlenka	myšlenka	k1gFnSc1	myšlenka
využít	využít	k5eAaPmF	využít
starou	starý	k2eAgFnSc4d1	stará
trasu	trasa	k1gFnSc4	trasa
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
z	z	k7c2	z
votického	votický	k2eAgNnSc2d1	Votické
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
olbramovickému	olbramovický	k2eAgInSc3d1	olbramovický
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Bystřice	Bystřice	k1gFnSc2	Bystřice
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
smyslem	smysl	k1gInSc7	smysl
by	by	kYmCp3nS	by
kromě	kromě	k7c2	kromě
rekreace	rekreace	k1gFnSc2	rekreace
byla	být	k5eAaImAgFnS	být
i	i	k9	i
snazší	snadný	k2eAgFnSc1d2	snazší
individuální	individuální	k2eAgFnSc1d1	individuální
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
olbramovické	olbramovický	k2eAgFnSc3d1	olbramovický
stanici	stanice	k1gFnSc3	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
staví	stavit	k5eAaBmIp3nS	stavit
i	i	k8xC	i
rychlíky	rychlík	k1gInPc4	rychlík
<g/>
.	.	kIx.	.
<g/>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
Městem	město	k1gNnSc7	město
projíždějí	projíždět	k5eAaImIp3nP	projíždět
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Bechyně	Bechyně	k1gFnPc1	Bechyně
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
Sedlčany	Sedlčany	k1gInPc1	Sedlčany
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
,	,	kIx,	,
Týn	Týn	k1gInSc1	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Votice	Votice	k1gFnPc1	Votice
<g/>
.	.	kIx.	.
<g/>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
Železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
Bystřice	Bystřice	k1gFnSc2	Bystřice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
13	[number]	k4	13
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
8	[number]	k4	8
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
zde	zde	k6eAd1	zde
i	i	k9	i
spěšné	spěšný	k2eAgInPc1d1	spěšný
vlaky	vlak	k1gInPc1	vlak
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Rychlíky	rychlík	k1gInPc1	rychlík
zde	zde	k6eAd1	zde
projíždějí	projíždět	k5eAaImIp3nP	projíždět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Cyklistika	cyklistika	k1gFnSc1	cyklistika
–	–	k?	–
Městem	město	k1gNnSc7	město
vedou	vést	k5eAaImIp3nP	vést
cyklotrasy	cyklotrasa	k1gFnPc4	cyklotrasa
č.	č.	k?	č.
0076	[number]	k4	0076
Konopiště	Konopiště	k1gNnSc2	Konopiště
-	-	kIx~	-
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Votice	Votice	k1gFnPc1	Votice
a	a	k8xC	a
č.	č.	k?	č.
0094	[number]	k4	0094
Neveklov	Neveklov	k1gInSc1	Neveklov
-	-	kIx~	-
Nesvačily	svačit	k5eNaImAgFnP	svačit
-	-	kIx~	-
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Postupice	Postupice	k1gFnSc1	Postupice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěší	pěší	k2eAgFnSc1d1	pěší
turistika	turistika	k1gFnSc1	turistika
–	–	k?	–
Územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
procházejí	procházet	k5eAaImIp3nP	procházet
turistické	turistický	k2eAgFnPc1d1	turistická
trasy	trasa	k1gFnPc1	trasa
Konopiště	Konopiště	k1gNnSc2	Konopiště
-	-	kIx~	-
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Líšno	Líšno	k1gNnSc1	Líšno
-	-	kIx~	-
Žebrák	Žebrák	k1gInSc1	Žebrák
-	-	kIx~	-
Ouběnice	Ouběnice	k1gFnPc1	Ouběnice
-	-	kIx~	-
Votice	Votice	k1gFnPc1	Votice
<g/>
,	,	kIx,	,
Bystřice	Bystřice	k1gFnSc1	Bystřice
-	-	kIx~	-
Jinošice	Jinošice	k1gFnSc1	Jinošice
-	-	kIx~	-
Ouběnice	Ouběnice	k1gFnSc1	Ouběnice
a	a	k8xC	a
Líšno	Líšno	k1gNnSc1	Líšno
-	-	kIx~	-
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
Lhota	Lhota	k1gFnSc1	Lhota
-	-	kIx~	-
Konopiště	Konopiště	k1gNnSc1	Konopiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bystřice	Bystřice	k1gFnSc2	Bystřice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Bystřice	Bystřice	k1gFnSc1	Bystřice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bystřice	Bystřice	k1gFnSc1	Bystřice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
