<s>
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
</s>
<s>
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
407	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
Vládní	vládní	k2eAgNnSc1d1
obvod	obvod	k1gInSc4
</s>
<s>
Horní	horní	k2eAgFnSc4d1
Falc	Falc	k1gFnSc4
Zemský	zemský	k2eAgInSc4d1
okres	okres	k1gInSc4
</s>
<s>
Cham	Cham	k6eAd1
</s>
<s>
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
67,1	67,1	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
9	#num#	k4
093	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
135,6	135,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Sandro	Sandra	k1gFnSc5
Bauer	Bauer	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2011	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.furth.de	www.furth.de	k6eAd1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Burgstrasse	Burgstrasse	k6eAd1
193437	#num#	k4
Furth	Furtha	k1gFnPc2
im	im	k?
Wald	Walda	k1gFnPc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
09973	#num#	k4
PSČ	PSČ	kA
</s>
<s>
93437	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
CHA	cha	k0
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Brod	Brod	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německé	německý	k2eAgNnSc1d1
pohraniční	pohraniční	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
Bavorsku	Bavorsko	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
vládním	vládní	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
Horní	horní	k2eAgFnSc1d1
Falc	Falc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
4	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
hraničního	hraniční	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
Folmava	Folmava	k1gFnSc1
/	/	kIx~
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
<g/>
,	,	kIx,
17	#num#	k4
km	km	kA
od	od	k7c2
českého	český	k2eAgNnSc2d1
města	město	k1gNnSc2
Domažlice	Domažlice	k1gFnPc1
a	a	k8xC
17	#num#	k4
km	km	kA
od	od	k7c2
německého	německý	k2eAgNnSc2d1
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
Cham	Chama	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městem	město	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
říčka	říčka	k1gFnSc1
Chamb	Chamba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
přibližném	přibližný	k2eAgNnSc6d1
rozhraní	rozhraní	k1gNnSc6
mezi	mezi	k7c7
německými	německý	k2eAgFnPc7d1
částmi	část	k1gFnPc7
Šumavy	Šumava	k1gFnSc2
(	(	kIx(
<g/>
Bayerischer	Bayerischra	k1gFnPc2
Wald	Wald	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
(	(	kIx(
<g/>
Oberpfälzer	Oberpfälzer	k1gMnSc1
Wald	Wald	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Poprvé	poprvé	k6eAd1
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
připomínáno	připomínat	k5eAaImNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1086	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1426	#num#	k4
bylo	být	k5eAaImAgNnS
vypleněno	vyplenit	k5eAaPmNgNnS
husitskými	husitský	k2eAgInPc7d1
nájezdy	nájezd	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1590	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Furthu	Furth	k1gInSc6
poprvé	poprvé	k6eAd1
slavily	slavit	k5eAaImAgFnP
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
známé	známá	k1gFnSc2
„	„	k?
<g/>
Dračí	dračí	k2eAgFnSc2d1
slavnosti	slavnost	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
v	v	k7c6
Německu	Německo	k1gNnSc6
údajně	údajně	k6eAd1
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
nejstarší	starý	k2eAgNnSc4d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
poničeno	poničit	k5eAaPmNgNnS
velkým	velký	k2eAgInSc7d1
požárem	požár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
města	město	k1gNnSc2
dorazila	dorazit	k5eAaPmAgFnS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
významnou	významný	k2eAgFnSc7d1
spojnicí	spojnice	k1gFnSc7
Čech	Čechy	k1gFnPc2
a	a	k8xC
Horní	horní	k2eAgFnSc2d1
Falce	Falc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Radnice	radnice	k1gFnSc1
ve	v	k7c4
Furth	Furth	k1gInSc4
im	im	k?
Waldu	Wald	k1gInSc2
</s>
<s>
Město	město	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
16	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
–	–	k?
Äpflet	Äpflet	k1gInSc1
<g/>
,	,	kIx,
Daberg	Daberg	k1gMnSc1
(	(	kIx(
<g/>
Ösbühl	Ösbühl	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Einberg	Einberg	k1gMnSc1
<g/>
,	,	kIx,
Dieberg	Dieberg	k1gMnSc1
<g/>
,	,	kIx,
Grabitz	Grabitz	k1gMnSc1
<g/>
,	,	kIx,
Grub	Grub	k1gMnSc1
<g/>
,	,	kIx,
Oberrappendorf	Oberrappendorf	k1gMnSc1
<g/>
,	,	kIx,
Ränkam	Ränkam	k1gInSc1
<g/>
,	,	kIx,
Schafberg	Schafberg	k1gMnSc1
<g/>
,	,	kIx,
Sengenbühl	Sengenbühl	k1gMnSc1
<g/>
,	,	kIx,
Unterrappendorf	Unterrappendorf	k1gMnSc1
<g/>
,	,	kIx,
Vogelherd	Vogelherd	k1gMnSc1
<g/>
,	,	kIx,
Voithenberghütte	Voithenberghütt	k1gMnSc5
<g/>
,	,	kIx,
Wutzmühle	Wutzmühle	k1gFnSc5
<g/>
,	,	kIx,
Blätterberg	Blätterberg	k1gMnSc1
a	a	k8xC
Grasmannsdorf	Grasmannsdorf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Furth	Furth	k1gMnSc1
im	im	k?
Wald	Wald	k1gMnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
spolkové	spolkový	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
B20	B20	k1gFnSc2
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gMnSc4
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
jihobavorským	jihobavorský	k2eAgInSc7d1
Berchtesgadenem	Berchtesgaden	k1gInSc7
při	při	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
na	na	k7c6
hraničním	hraniční	k2eAgInSc6d1
přechodu	přechod	k1gInSc6
plynule	plynule	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
českou	český	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
I	I	kA
<g/>
/	/	kIx~
<g/>
26	#num#	k4
do	do	k7c2
Domažlic	Domažlice	k1gFnPc2
a	a	k8xC
Plzně	Plzeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zvonkohra	zvonkohra	k1gFnSc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
procházející	procházející	k2eAgFnSc1d1
Furthem	Furth	k1gInSc7
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
českou	český	k2eAgFnSc4d1
Trať	trať	k1gFnSc4
180	#num#	k4
z	z	k7c2
Plzně	Plzeň	k1gFnSc2
do	do	k7c2
Domažlic	Domažlice	k1gFnPc2
a	a	k8xC
na	na	k7c4
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
pokračuje	pokračovat	k5eAaImIp3nS
dále	daleko	k6eAd2
přes	přes	k7c4
Cham	Cham	k1gInSc4
do	do	k7c2
Schwandorfu	Schwandorf	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pokračovat	pokračovat	k5eAaImF
např.	např.	kA
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
nebo	nebo	k8xC
Řezna	Řezno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
trasu	trasa	k1gFnSc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
také	také	k9
české	český	k2eAgInPc4d1
mezinárodní	mezinárodní	k2eAgInPc4d1
rychlíky	rychlík	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
také	také	k9
přeshraniční	přeshraniční	k2eAgNnSc4d1
vlakové	vlakový	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
Domažlice	Domažlice	k1gFnPc1
-	-	kIx~
Furth	Furth	k1gInSc1
i.	i.	k?
Wald	Wald	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
má	mít	k5eAaImIp3nS
tradici	tradice	k1gFnSc4
sklářství	sklářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
vybudována	vybudován	k2eAgFnSc1d1
moderní	moderní	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
např.	např.	kA
s	s	k7c7
různými	různý	k2eAgInPc7d1
podniky	podnik	k1gInPc7
elektrotechnického	elektrotechnický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
od	od	k7c2
severu	sever	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Náměstí	náměstí	k1gNnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Ludres	Ludres	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Furth	Furth	k1gInSc1
bei	bei	k?
Göttweig	Göttweig	k1gInSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Domažlice	Domažlice	k1gFnPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Vaduz	Vaduz	k1gInSc1
<g/>
,	,	kIx,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Furth	Furtha	k1gFnPc2
im	im	k?
Wald	Walda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Furth	Furtha	k1gFnPc2
im	im	k?
Wald	Walda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc4
Královské	královský	k2eAgNnSc4d1
hraniční	hraniční	k2eAgNnSc4d1
město	město	k1gNnSc4
Brod	Brod	k1gInSc1
v	v	k7c6
Lese	les	k1gInSc6
v	v	k7c6
Bavořích	Bavory	k1gInPc6
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Slavnosti	slavnost	k1gFnPc1
skolení	skolení	k1gNnSc2
draka	drak	k1gMnSc2
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
v	v	k7c6
zemském	zemský	k2eAgInSc6d1
okrese	okres	k1gInSc6
Cham	Chama	k1gFnPc2
</s>
<s>
Arnschwang	Arnschwang	k1gMnSc1
•	•	k?
Arrach	Arrach	k1gMnSc1
•	•	k?
Bad	Bad	k1gFnSc1
Kötzting	Kötzting	k1gInSc1
•	•	k?
Blaibach	Blaibach	k1gInSc1
•	•	k?
Cham	Cham	k1gInSc1
•	•	k?
Chamerau	Chameraus	k1gInSc2
•	•	k?
Eschlkam	Eschlkam	k1gInSc1
•	•	k?
Falkenstein	Falkenstein	k1gInSc1
•	•	k?
Furth	Furth	k1gInSc1
im	im	k?
Wald	Wald	k1gInSc1
•	•	k?
Gleissenberg	Gleissenberg	k1gInSc1
•	•	k?
Grafenwiesen	Grafenwiesen	k2eAgInSc1d1
•	•	k?
Hohenwarth	Hohenwarth	k1gInSc1
•	•	k?
Lam	lama	k1gFnPc2
•	•	k?
Lohberg	Lohberg	k1gMnSc1
•	•	k?
Michelsneukirchen	Michelsneukirchen	k1gInSc1
•	•	k?
Miltach	Miltach	k1gInSc1
•	•	k?
Neukirchen	Neukirchen	k1gInSc1
b.	b.	k?
<g/>
Hl.	Hl.	k1gFnSc2
<g/>
Blut	Blut	k1gMnSc1
•	•	k?
Pemfling	Pemfling	k1gInSc1
•	•	k?
Pösing	Pösing	k1gInSc1
•	•	k?
Reichenbach	Reichenbach	k1gInSc1
•	•	k?
Rettenbach	Rettenbach	k1gInSc1
•	•	k?
Rimbach	Rimbach	k1gInSc1
•	•	k?
Roding	Roding	k1gInSc1
•	•	k?
Rötz	Rötz	k1gInSc1
•	•	k?
Runding	Runding	k1gInSc1
•	•	k?
Schönthal	Schönthal	k1gMnSc1
•	•	k?
Schorndorf	Schorndorf	k1gMnSc1
•	•	k?
Stamsried	Stamsried	k1gMnSc1
•	•	k?
Tiefenbach	Tiefenbach	k1gMnSc1
•	•	k?
Traitsching	Traitsching	k1gInSc1
•	•	k?
Treffelstein	Treffelstein	k1gMnSc1
•	•	k?
Waffenbrunn	Waffenbrunn	k1gMnSc1
•	•	k?
Wald	Wald	k1gMnSc1
•	•	k?
Walderbach	Walderbach	k1gMnSc1
•	•	k?
Waldmünchen	Waldmünchen	k1gInSc1
•	•	k?
Weiding	Weiding	k1gInSc1
•	•	k?
Willmering	Willmering	k1gInSc1
•	•	k?
Zandt	Zandt	k1gInSc1
•	•	k?
Zell	Zell	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
129171	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4018956-9	4018956-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83052506	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
126139469	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83052506	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
