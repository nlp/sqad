<s>
Společnost	společnost	k1gFnSc1	společnost
Intel	Intel	kA	Intel
Corporation	Corporation	k1gInSc1	Corporation
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
výrobcem	výrobce	k1gMnSc7	výrobce
polovodičových	polovodičový	k2eAgInPc2d1	polovodičový
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
městě	město	k1gNnSc6	město
Santa	Santa	k1gFnSc1	Santa
Clara	Clara	k1gFnSc1	Clara
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc1	Valley
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
sídlo	sídlo	k1gNnSc1	sídlo
Intelu	Intel	k1gInSc2	Intel
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
komplexu	komplex	k1gInSc2	komplex
několika	několik	k4yIc2	několik
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
centrála	centrála	k1gFnSc1	centrála
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
zakladateli	zakladatel	k1gMnSc6	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Robertu	Robert	k1gMnSc3	Robert
Noyceovi	Noyceus	k1gMnSc3	Noyceus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
Intelu	Intel	k1gInSc2	Intel
také	také	k9	také
naleznete	nalézt	k5eAaBmIp2nP	nalézt
muzeum	muzeum	k1gNnSc4	muzeum
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
pracuje	pracovat	k5eAaImIp3nS	pracovat
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
Intel	Intel	kA	Intel
přes	přes	k7c4	přes
96	[number]	k4	96
500	[number]	k4	500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
světových	světový	k2eAgFnPc2d1	světová
společností	společnost	k1gFnPc2	společnost
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
obratem	obrat	k1gInSc7	obrat
37,6	[number]	k4	37,6
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
hodnotou	hodnota	k1gFnSc7	hodnota
127	[number]	k4	127
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
(	(	kIx(	(
<g/>
k	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
kótována	kótován	k2eAgFnSc1d1	kótována
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
světových	světový	k2eAgFnPc6d1	světová
burzách	burza	k1gFnPc6	burza
(	(	kIx(	(
<g/>
NASDAQ	NASDAQ	kA	NASDAQ
<g/>
:	:	kIx,	:
INTC	INTC	kA	INTC
<g/>
;	;	kIx,	;
SEHK	SEHK	kA	SEHK
<g/>
:	:	kIx,	:
4335	[number]	k4	4335
<g/>
)	)	kIx)	)
Intel	Intel	kA	Intel
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
mezi	mezi	k7c7	mezi
běžnými	běžný	k2eAgMnPc7d1	běžný
lidmi	člověk	k1gMnPc7	člověk
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
procesory	procesor	k1gInPc7	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
jich	on	k3xPp3gNnPc2	on
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
přes	přes	k7c4	přes
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
produkce	produkce	k1gFnSc2	produkce
x	x	k?	x
<g/>
86	[number]	k4	86
procesorů	procesor	k1gInPc2	procesor
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
procesorů	procesor	k1gInPc2	procesor
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
procesory	procesor	k1gInPc7	procesor
pro	pro	k7c4	pro
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
čipsety	čipseta	k1gFnSc2	čipseta
<g/>
,	,	kIx,	,
flash	flash	k1gInSc4	flash
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
telekomunikační	telekomunikační	k2eAgInPc1d1	telekomunikační
čipy	čip	k1gInPc1	čip
i	i	k8xC	i
multimediální	multimediální	k2eAgNnPc1d1	multimediální
vybavení	vybavení	k1gNnPc1	vybavení
domácností	domácnost	k1gFnPc2	domácnost
(	(	kIx(	(
<g/>
dětské	dětský	k2eAgInPc1d1	dětský
elektronické	elektronický	k2eAgInPc1d1	elektronický
mikroskopy	mikroskop	k1gInPc1	mikroskop
a	a	k8xC	a
web	web	k1gInSc1	web
kamery	kamera	k1gFnSc2	kamera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
své	své	k1gNnSc4	své
37	[number]	k4	37
let	léto	k1gNnPc2	léto
staré	starý	k2eAgNnSc4d1	staré
logo	logo	k1gNnSc4	logo
i	i	k8xC	i
známý	známý	k2eAgInSc1d1	známý
slogan	slogan	k1gInSc1	slogan
<g/>
.	.	kIx.	.
</s>
<s>
Slogan	slogan	k1gInSc1	slogan
Intel	Intel	kA	Intel
inside	insid	k1gInSc5	insid
tak	tak	k9	tak
nahradil	nahradit	k5eAaPmAgInS	nahradit
claim	claim	k6eAd1	claim
Leap	Leap	k1gInSc1	Leap
ahead	ahead	k1gInSc1	ahead
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
skok	skok	k1gInSc1	skok
kupředu	kupředu	k6eAd1	kupředu
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Robertem	Robert	k1gMnSc7	Robert
Noycem	Noyce	k1gMnSc7	Noyce
<g/>
,	,	kIx,	,
Gordonem	Gordon	k1gMnSc7	Gordon
Moorem	Moor	k1gMnSc7	Moor
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
Moorově	Moorův	k2eAgInSc6d1	Moorův
zákonu	zákon	k1gInSc6	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arthurem	Arthur	k1gInSc7	Arthur
Rockem	rock	k1gInSc7	rock
a	a	k8xC	a
Maxem	Max	k1gMnSc7	Max
Palevskym	Palevskymum	k1gNnPc2	Palevskymum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Integrated	Integrated	k1gInSc1	Integrated
Electronics	Electronicsa	k1gFnPc2	Electronicsa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Moore	Moor	k1gMnSc5	Moor
a	a	k8xC	a
Noyce	Noyce	k1gMnSc1	Noyce
přišli	přijít	k5eAaPmAgMnP	přijít
z	z	k7c2	z
Fairchild	Fairchilda	k1gFnPc2	Fairchilda
Semiconductor	Semiconductor	k1gInSc4	Semiconductor
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
prvními	první	k4xOgInPc7	první
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Rock	rock	k1gInSc4	rock
nebyl	být	k5eNaImAgMnS	být
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
ale	ale	k8xC	ale
investor	investor	k1gMnSc1	investor
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
počáteční	počáteční	k2eAgFnSc1d1	počáteční
investice	investice	k1gFnSc1	investice
činila	činit	k5eAaImAgFnS	činit
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
dollarů	dollar	k1gInPc2	dollar
směnitelných	směnitelný	k2eAgInPc2d1	směnitelný
dluhopisů	dluhopis	k1gInPc2	dluhopis
a	a	k8xC	a
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
dollarů	dollar	k1gInPc2	dollar
od	od	k7c2	od
Arthura	Arthur	k1gMnSc2	Arthur
Rocka	Rocek	k1gMnSc2	Rocek
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
roky	rok	k1gInPc7	rok
poté	poté	k6eAd1	poté
dokončil	dokončit	k5eAaPmAgInS	dokončit
Intel	Intel	kA	Intel
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
nabídku	nabídka	k1gFnSc4	nabídka
akcií	akcie	k1gFnPc2	akcie
(	(	kIx(	(
<g/>
IPO	IPO	kA	IPO
<g/>
)	)	kIx)	)
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
6,8	[number]	k4	6,8
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
23	[number]	k4	23
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
akcii	akcie	k1gFnSc4	akcie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Intelu	Intela	k1gFnSc4	Intela
Andy	Anda	k1gFnSc2	Anda
Grove	Groev	k1gFnSc2	Groev
byl	být	k5eAaImAgMnS	být
chemický	chemický	k2eAgMnSc1d1	chemický
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
řídil	řídit	k5eAaImAgInS	řídit
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Procesory	procesor	k1gInPc1	procesor
Intel	Intel	kA	Intel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
uvedl	uvést	k5eAaPmAgInS	uvést
Intel	Intel	kA	Intel
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
platformy	platforma	k1gFnPc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
Core	Cor	k1gInSc2	Cor
i	i	k9	i
<g/>
7	[number]	k4	7
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
Nehalem	Nehal	k1gInSc7	Nehal
do	do	k7c2	do
Patice	patice	k1gFnSc2	patice
1366	[number]	k4	1366
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Atom	atom	k1gInSc1	atom
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
netbooky	netbook	k1gInPc4	netbook
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
441	[number]	k4	441
<g/>
-ball	alla	k1gFnPc2	-balla
μ	μ	k?	μ
pouzdře	pouzdro	k1gNnSc6	pouzdro
(	(	kIx(	(
<g/>
letování	letování	k1gNnSc6	letování
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
PCB	PCB	kA	PCB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
třetím	třetí	k4xOgInSc7	třetí
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
kvartálem	kvartál	k1gInSc7	kvartál
2009	[number]	k4	2009
uvedl	uvést	k5eAaPmAgMnS	uvést
Intel	Intel	kA	Intel
na	na	k7c4	na
trh	trh	k1gInSc4	trh
Core	Cor	k1gFnSc2	Cor
i	i	k9	i
<g/>
5	[number]	k4	5
a	a	k8xC	a
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Patici	patice	k1gFnSc4	patice
1156	[number]	k4	1156
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
intel	intel	k1gMnSc1	intel
zahájil	zahájit	k5eAaPmAgMnS	zahájit
prodej	prodej	k1gInSc4	prodej
Core	Cor	k1gInSc2	Cor
i	i	k8xC	i
<g/>
5	[number]	k4	5
a	a	k8xC	a
i	i	k9	i
<g/>
7	[number]	k4	7
2	[number]	k4	2
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
s	s	k7c7	s
produktovým	produktový	k2eAgNnSc7d1	produktové
označením	označení	k1gNnSc7	označení
Sandy	Sanda	k1gFnSc2	Sanda
Bridge	Bridg	k1gFnSc2	Bridg
pro	pro	k7c4	pro
patici	patice	k1gFnSc4	patice
1155	[number]	k4	1155
do	do	k7c2	do
vydaní	vydaný	k2eAgMnPc1d1	vydaný
Sandy	Sand	k1gInPc4	Sand
Bridge-E	Bridge-E	k1gFnSc4	Bridge-E
jedná	jednat	k5eAaImIp3nS	jednat
z	z	k7c2	z
nejvýkonnějších	výkonný	k2eAgInPc2d3	nejvýkonnější
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
procesoru	procesor	k1gInSc2	procesor
i	i	k9	i
<g/>
7	[number]	k4	7
Sandy	Sanda	k1gMnSc2	Sanda
Bridge-E	Bridge-E	k1gMnSc2	Bridge-E
nejvýkonnější	výkonný	k2eAgInPc1d3	nejvýkonnější
procesory	procesor	k1gInPc1	procesor
současnosti	současnost	k1gFnSc2	současnost
pro	pro	k7c4	pro
patici	patice	k1gFnSc4	patice
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vydaní	vydaný	k2eAgMnPc1d1	vydaný
procesoru	procesor	k1gInSc3	procesor
Ivy	Ivo	k1gMnSc2	Ivo
Bridge	Bridg	k1gMnSc2	Bridg
pro	pro	k7c4	pro
patici	patice	k1gFnSc4	patice
1155	[number]	k4	1155
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
Intel	Intel	kA	Intel
vydal	vydat	k5eAaPmAgMnS	vydat
procesory	procesor	k1gInPc4	procesor
Core	Core	k1gNnSc2	Core
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
Haswell	Haswell	k1gInSc1	Haswell
s	s	k7c7	s
paticí	patice	k1gFnSc7	patice
1150	[number]	k4	1150
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Core	Core	k1gInSc4	Core
2	[number]	k4	2
Duo	duo	k1gNnSc1	duo
a	a	k8xC	a
Quad	Quad	k1gInSc1	Quad
má	mít	k5eAaImIp3nS	mít
nejnovější	nový	k2eAgFnSc4d3	nejnovější
čipovou	čipový	k2eAgFnSc4d1	čipová
sadu	sada	k1gFnSc4	sada
x	x	k?	x
<g/>
4	[number]	k4	4
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Core	Core	k1gInSc4	Core
i	i	k8xC	i
<g/>
7	[number]	k4	7
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
X	X	kA	X
<g/>
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Intel	Intel	kA	Intel
GMA	GMA	kA	GMA
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
věnuje	věnovat	k5eAaImIp3nS	věnovat
vývoji	vývoj	k1gInSc3	vývoj
pouze	pouze	k6eAd1	pouze
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
grafických	grafický	k2eAgInPc2d1	grafický
čipů	čip	k1gInPc2	čip
Intel	Intel	kA	Intel
Graphics	Graphics	k1gInSc1	Graphics
Media	medium	k1gNnSc2	medium
Accelerator	Accelerator	k1gInSc1	Accelerator
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
GMA	GMA	kA	GMA
<g/>
)	)	kIx)	)
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
čipsetů	čipset	k1gInPc2	čipset
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
je	být	k5eAaImIp3nS	být
X4500	X4500	k1gFnSc1	X4500
v	v	k7c6	v
x	x	k?	x
<g/>
45	[number]	k4	45
čipsetech	čipset	k1gInPc6	čipset
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Larrabee	Larrabe	k1gFnSc2	Larrabe
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
jít	jít	k5eAaImF	jít
o	o	k7c4	o
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
x	x	k?	x
<g/>
86	[number]	k4	86
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
4Q	[number]	k4	4Q
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
s	s	k7c7	s
x	x	k?	x
<g/>
86	[number]	k4	86
instrukcemi	instrukce	k1gFnPc7	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
je	být	k5eAaImIp3nS	být
Intel	Intel	kA	Intel
CT	CT	kA	CT
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
malých	malá	k1gFnPc2	malá
x	x	k?	x
<g/>
86	[number]	k4	86
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
připojenou	připojený	k2eAgFnSc4d1	připojená
512	[number]	k4	512
<g/>
bitovou	bitový	k2eAgFnSc4d1	bitová
vektorovou	vektorový	k2eAgFnSc4d1	vektorová
jednotku	jednotka	k1gFnSc4	jednotka
a	a	k8xC	a
speciální	speciální	k2eAgFnSc4d1	speciální
jednotku	jednotka	k1gFnSc4	jednotka
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
texturami	textura	k1gFnPc7	textura
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
nm	nm	k?	nm
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
600	[number]	k4	600
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
jádra	jádro	k1gNnSc2	jádro
P	P	kA	P
<g/>
55	[number]	k4	55
<g/>
C.	C.	kA	C.
Počet	počet	k1gInSc1	počet
tranzistorů	tranzistor	k1gInPc2	tranzistor
je	být	k5eAaImIp3nS	být
1,65	[number]	k4	1,65
<g/>
-	-	kIx~	-
<g/>
1,7	[number]	k4	1,7
miliardy	miliarda	k4xCgFnSc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Centrino	Centrin	k2eAgNnSc1d1	Centrino
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dané	daný	k2eAgNnSc1d1	dané
zařízení	zařízení	k1gNnSc1	zařízení
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
specifikované	specifikovaný	k2eAgFnPc4d1	specifikovaná
součásti	součást	k1gFnPc4	součást
(	(	kIx(	(
<g/>
hardware	hardware	k1gInSc4	hardware
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
čipsetu	čipset	k1gInSc2	čipset
<g/>
+	+	kIx~	+
<g/>
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
WIFI	WIFI	kA	WIFI
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
vyráběné	vyráběný	k2eAgNnSc1d1	vyráběné
Intelem	Intel	k1gMnSc7	Intel
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
součástky	součástka	k1gFnPc4	součástka
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
vyměřila	vyměřit	k5eAaPmAgFnS	vyměřit
firmě	firma	k1gFnSc6	firma
Intel	Intel	kA	Intel
rekordní	rekordní	k2eAgFnSc4d1	rekordní
pokutu	pokuta	k1gFnSc4	pokuta
1,06	[number]	k4	1,06
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
za	za	k7c2	za
poskytování	poskytování	k1gNnPc2	poskytování
slev	sleva	k1gFnPc2	sleva
odběratelům	odběratel	k1gMnPc3	odběratel
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
omezení	omezení	k1gNnSc2	omezení
použití	použití	k1gNnSc2	použití
čipů	čip	k1gInPc2	čip
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgNnP	být
narušena	narušen	k2eAgNnPc1d1	narušeno
evropská	evropský	k2eAgNnPc1d1	Evropské
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
Intel	Intel	kA	Intel
poškozoval	poškozovat	k5eAaImAgMnS	poškozovat
konkurenci	konkurence	k1gFnSc4	konkurence
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
porušování	porušování	k1gNnSc2	porušování
pravidel	pravidlo	k1gNnPc2	pravidlo
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
především	především	k9	především
čipů	čip	k1gInPc2	čip
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
x	x	k?	x
<g/>
86	[number]	k4	86
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
EK	EK	kA	EK
platil	platit	k5eAaImAgInS	platit
Intel	Intel	kA	Intel
majiteli	majitel	k1gMnSc3	majitel
řetězce	řetězec	k1gInSc2	řetězec
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
MediaMarkt	MediaMarkt	k1gInSc4	MediaMarkt
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skladoval	skladovat	k5eAaImAgMnS	skladovat
výhradně	výhradně	k6eAd1	výhradně
počítače	počítač	k1gInPc4	počítač
s	s	k7c7	s
čipem	čip	k1gInSc7	čip
Intel	Intel	kA	Intel
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
komise	komise	k1gFnSc2	komise
je	být	k5eAaImIp3nS	být
konečné	konečný	k2eAgNnSc1d1	konečné
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc4d1	možné
se	se	k3xPyFc4	se
již	již	k6eAd1	již
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
přesto	přesto	k8xC	přesto
obvinění	obvinění	k1gNnSc4	obvinění
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
jako	jako	k8xS	jako
vinen	vinen	k2eAgMnSc1d1	vinen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
že	že	k8xS	že
i	i	k9	i
zde	zde	k6eAd1	zde
nakonec	nakonec	k6eAd1	nakonec
odvolání	odvolání	k1gNnSc3	odvolání
neuspěje	uspět	k5eNaPmIp3nS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
se	se	k3xPyFc4	se
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
AMD	AMD	kA	AMD
mimosoudně	mimosoudně	k6eAd1	mimosoudně
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
na	na	k7c4	na
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
ztráty	ztráta	k1gFnSc2	ztráta
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
částkou	částka	k1gFnSc7	částka
1,25	[number]	k4	1,25
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
AMD	AMD	kA	AMD
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
omezování	omezování	k1gNnSc3	omezování
konkurentem	konkurent	k1gMnSc7	konkurent
přišla	přijít	k5eAaPmAgFnS	přijít
odhadem	odhad	k1gInSc7	odhad
o	o	k7c6	o
cca	cca	kA	cca
30-50	[number]	k4	30-50
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Intel	Intel	kA	Intel
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Advanced	Advanced	k1gInSc1	Advanced
Micro	Micro	k1gNnSc1	Micro
Devices	Devices	k1gInSc1	Devices
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
společností	společnost	k1gFnPc2	společnost
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
Seznam	seznam	k1gInSc1	seznam
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
Intelu	Intel	k1gInSc2	Intel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Intel	Intel	kA	Intel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Intel	Intel	kA	Intel
Detaily	detail	k1gInPc1	detail
o	o	k7c6	o
Arrandale	Arrandala	k1gFnSc6	Arrandala
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
dala	dát	k5eAaPmAgFnS	dát
Intelu	Intela	k1gFnSc4	Intela
téměř	téměř	k6eAd1	téměř
$	$	kIx~	$
<g/>
1,5	[number]	k4	1,5
<g/>
miliardovou	miliardový	k2eAgFnSc4d1	miliardová
pokutu	pokuta	k1gFnSc4	pokuta
Intel	Intel	kA	Intel
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
EU	EU	kA	EU
pokutu	pokuta	k1gFnSc4	pokuta
1,06	[number]	k4	1,06
<g />
.	.	kIx.	.
</s>
<s>
miliardy	miliarda	k4xCgFnPc1	miliarda
EUR	euro	k1gNnPc2	euro
Larrabee	Larrabee	k1gFnSc4	Larrabee
<g/>
,	,	kIx,	,
revoluční	revoluční	k2eAgInPc4d1	revoluční
GPU	GPU	kA	GPU
od	od	k7c2	od
Intelu	Intel	k1gInSc2	Intel
Intel	Intel	kA	Intel
Larrabee	Larrabee	k1gFnSc4	Larrabee
proti	proti	k7c3	proti
ATI	ATI	kA	ATI
a	a	k8xC	a
NVIDIA	NVIDIA	kA	NVIDIA
<g/>
:	:	kIx,	:
Paralelismus	paralelismus	k1gInSc1	paralelismus
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
CL	CL	kA	CL
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
86	[number]	k4	86
architektura	architektura	k1gFnSc1	architektura
Larrabee	Larrabee	k1gNnSc2	Larrabee
na	na	k7c4	na
IDF	IDF	kA	IDF
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
A	a	k9	a
First	First	k1gMnSc1	First
Look	Look	k1gMnSc1	Look
at	at	k?	at
the	the	k?	the
Larrabee	Larrabee	k1gInSc1	Larrabee
New	New	k1gMnSc1	New
Instructions	Instructions	k1gInSc1	Instructions
(	(	kIx(	(
<g/>
LRBni	LRBn	k1gMnPc1	LRBn
<g/>
)	)	kIx)	)
Grafika	grafika	k1gFnSc1	grafika
od	od	k7c2	od
Intelu	Intel	k1gInSc2	Intel
–	–	k?	–
Larrabee	Larrabee	k1gInSc1	Larrabee
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
1,7	[number]	k4	1,7
miliardy	miliarda	k4xCgFnSc2	miliarda
tranzistorů	tranzistor	k1gInPc2	tranzistor
na	na	k7c6	na
600	[number]	k4	600
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
Larrabee	Larrabe	k1gFnSc2	Larrabe
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
fyzika	fyzika	k1gFnSc1	fyzika
Grafika	grafika	k1gFnSc1	grafika
od	od	k7c2	od
Intelu	Intel	k1gInSc2	Intel
–	–	k?	–
Intel	Intel	kA	Intel
LARABEE	LARABEE	kA	LARABEE
vyjde	vyjít	k5eAaPmIp3nS	vyjít
až	až	k9	až
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
</s>
