<s>
Rhythm	Rhythm	k1gInSc1
and	and	k?
blues	blues	k1gNnSc2
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
R	R	kA
<g/>
&	&	k?
<g/>
B.	B.	kA
Tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
o	o	k7c4
rhythm	rhythm	k1gInSc4
and	and	k?
blues	blues	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
moderním	moderní	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
stylu	styl	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Contemporary	Contemporara	k1gFnSc2
R	R	kA
<g/>
&	&	k?
<g/>
B.	B.	kA
</s>
<s>
Rhythm	Rhythm	k6eAd1
and	and	k?
bluesPůvod	bluesPůvoda	k1gFnPc2
ve	v	k7c6
stylech	styl	k1gInPc6
</s>
<s>
Jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgNnSc1d1
blues	blues	k1gNnSc1
<g/>
,	,	kIx,
jive	jive	k1gInSc1
<g/>
,	,	kIx,
blues	blues	k1gInSc1
<g/>
,	,	kIx,
gospel	gospel	k1gInSc1
<g/>
,	,	kIx,
boogie-woogie	boogie-woogie	k1gNnSc1
a	a	k8xC
jump	jump	k1gInSc1
blues	blues	k1gNnSc2
Kulturní	kulturní	k2eAgInSc1d1
pozadí	pozadí	k1gNnSc6
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
,	,	kIx,
USA	USA	kA
Typické	typický	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
</s>
<s>
Kytara	kytara	k1gFnSc1
-	-	kIx~
Baskytara	baskytara	k1gFnSc1
-	-	kIx~
Harmonika	harmonika	k1gFnSc1
-	-	kIx~
Saxofon	saxofon	k1gInSc1
-	-	kIx~
Bicí	bicí	k2eAgInSc1d1
-	-	kIx~
Klavír	klavír	k1gInSc1
-	-	kIx~
Varhany	varhany	k1gFnPc1
-	-	kIx~
Klávesy	klávesa	k1gFnSc2
Všeobecná	všeobecný	k2eAgFnSc1d1
popularita	popularita	k1gFnSc1
</s>
<s>
Významná	významný	k2eAgFnSc1d1
od	od	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
;	;	kIx,
později	pozdě	k6eAd2
ikonická	ikonický	k2eAgFnSc1d1
Odvozené	odvozený	k2eAgFnSc3d1
styly	styl	k1gInPc1
</s>
<s>
Rock	rock	k1gInSc1
and	and	k?
roll	roll	k1gInSc1
-	-	kIx~
Soul	Soul	k1gInSc1
-	-	kIx~
Funk	funk	k1gInSc1
-	-	kIx~
Ska	Ska	k1gFnSc1
-	-	kIx~
Reggae	Reggae	k1gInSc1
-	-	kIx~
Contemporary	Contemporara	k1gFnSc2
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
Podstyly	Podstyl	k1gInPc5
</s>
<s>
Doo-wop	Doo-wop	k1gInSc4
Jiná	jiný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Seznam	seznam	k1gInSc1
moderních	moderní	k2eAgFnPc2d1
a	a	k8xC
typických	typický	k2eAgFnPc2d1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
hudebníků	hudebník	k1gMnPc2
</s>
<s>
Rhythm	Rhythm	k1gInSc1
and	and	k?
Blues	blues	k1gNnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
též	též	k9
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
<g/>
,	,	kIx,
či	či	k8xC
Rhythm	Rhythm	k1gInSc1
&	&	k?
Blues	blues	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
široký	široký	k2eAgInSc1d1
termín	termín	k1gInSc1
<g/>
,	,	kIx,
aplikovaný	aplikovaný	k2eAgInSc1d1
na	na	k7c4
populární	populární	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
vytvořenou	vytvořený	k2eAgFnSc4d1
Afroameričany	Afroameričan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
termín	termín	k1gInSc1
užíval	užívat	k5eAaImAgInS
na	na	k7c4
styl	styl	k1gInSc4
hudby	hudba	k1gFnSc2
ze	z	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kombinující	kombinující	k2eAgInPc4d1
prvky	prvek	k1gInPc4
jazzu	jazz	k1gInSc2
<g/>
,	,	kIx,
gospelu	gospel	k1gInSc2
a	a	k8xC
blues	blues	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3
nástroji	nástroj	k1gInPc7
původního	původní	k2eAgInSc2d1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
byly	být	k5eAaImAgInP
basová	basový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
saxofon	saxofon	k1gInSc4
<g/>
,	,	kIx,
bicí	bicí	k2eAgInPc4d1
a	a	k8xC
klávesové	klávesový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	s	k7c7
(	(	kIx(
<g/>
moderní	moderní	k2eAgFnSc7d1
<g/>
)	)	kIx)
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
komerční	komerční	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
splynulo	splynout	k5eAaPmAgNnS
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
například	například	k6eAd1
Hip	hip	k0
hop	hop	k0
<g/>
,	,	kIx,
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
populární	populární	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
do	do	k7c2
něj	on	k3xPp3gNnSc2
vnikat	vnikat	k5eAaImF
prvky	prvek	k1gInPc4
pop	pop	k1gMnSc1
rocku	rock	k1gInSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
do	do	k7c2
mnoha	mnoho	k4c2
jiných	jiný	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Terminologie	terminologie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
tohoto	tento	k3xDgInSc2
hudebního	hudební	k2eAgInSc2d1
stylu	styl	k1gInSc2
vymyslel	vymyslet	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
novinář	novinář	k1gMnSc1
Jerry	Jerra	k1gMnSc2
Wexler	Wexler	k1gMnSc1
z	z	k7c2
amerického	americký	k2eAgInSc2d1
týdeníku	týdeník	k1gInSc2
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
rovněž	rovněž	k9
nahradil	nahradit	k5eAaPmAgInS
rasistický	rasistický	k2eAgInSc1d1
výraz	výraz	k1gInSc1
"	"	kIx"
<g/>
race	racat	k5eAaPmIp3nS
music	music	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
znamenalo	znamenat	k5eAaImAgNnS
"	"	kIx"
<g/>
rasová	rasový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rober	Rober	k1gMnSc1
Palmer	Palmer	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
producent	producent	k1gMnSc1
<g/>
,	,	kIx,
hudbu	hudba	k1gFnSc4
Rhythm	Rhythma	k1gFnPc2
and	and	k?
Blues	blues	k1gNnSc1
definuje	definovat	k5eAaBmIp3nS
jako	jako	k9
"	"	kIx"
<g/>
chytlavý	chytlavý	k2eAgInSc1d1
termín	termín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
veškerou	veškerý	k3xTgFnSc4
hudbu	hudba	k1gFnSc4
vytvořenou	vytvořený	k2eAgFnSc4d1
černými	černý	k2eAgFnPc7d1
umělci	umělec	k1gMnPc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
je	být	k5eAaImIp3nS
synonymum	synonymum	k1gNnSc4
pro	pro	k7c4
Jump	Jump	k1gInSc4
blues	blues	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jenže	jenže	k8xC
Allmusic	Allmusic	k1gMnSc1
definuje	definovat	k5eAaBmIp3nS
Jump	Jump	k1gInSc4
blues	blues	k1gNnSc2
jako	jako	k8xC,k8xS
více	hodně	k6eAd2
[	[	kIx(
<g/>
rytmicky	rytmicky	k6eAd1
<g/>
]	]	kIx)
gospelově	gospelově	k6eAd1
orientovaný	orientovaný	k2eAgInSc1d1
žánr	žánr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lawrence	Lawrenec	k1gInSc2
Cohn	Cohn	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
Nothing	Nothing	k1gInSc1
but	but	k?
the	the	k?
Blues	blues	k1gNnSc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
rhythm	rhythm	k1gInSc1
and	and	k?
blues	blues	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
jenom	jenom	k9
pomocný	pomocný	k2eAgInSc1d1
termín	termín	k1gInSc1
<g/>
,	,	kIx,
škatulka	škatulka	k1gFnSc1
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pokrývá	pokrývat	k5eAaImIp3nS
veškeré	veškerý	k3xTgInPc4
černošské	černošský	k2eAgInPc4d1
žánry	žánr	k1gInPc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
klasické	klasický	k2eAgFnSc2d1
a	a	k8xC
náboženské	náboženský	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
pod	pod	k7c7
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
rozumíme	rozumět	k5eAaImIp1nP
také	také	k9
funk	funk	k1gInSc4
<g/>
,	,	kIx,
soul	soul	k1gInSc4
a	a	k8xC
disco	disco	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
označuje	označovat	k5eAaImIp3nS
především	především	k6eAd1
moderní	moderní	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
hip	hip	k0
hopu	hopus	k1gInSc3
a	a	k8xC
soulu	soul	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
V	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
styl	styl	k1gInSc1
Rhythm	Rhythm	k1gInSc1
and	and	k?
Blues	blues	k1gFnSc2
předchůdcem	předchůdce	k1gMnSc7
rockabilly	rockabilla	k1gFnSc2
a	a	k8xC
rock	rock	k1gInSc4
and	and	k?
rollu	roll	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
styly	styl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
ho	on	k3xPp3gMnSc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ovlivňovaly	ovlivňovat	k5eAaImAgFnP
zařazujeme	zařazovat	k5eAaImIp1nP
jazz	jazz	k1gInSc4
<g/>
,	,	kIx,
gospel	gospel	k1gInSc4
a	a	k8xC
africkou	africký	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
50	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Rhythm	Rhythm	k1gInSc1
and	and	k?
Blues	blues	k1gNnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
regionální	regionální	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
tohoto	tento	k3xDgInSc2
žánru	žánr	k1gInSc2
<g/>
,	,	kIx,
příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
New	New	k1gFnSc4
Orleans	Orleans	k1gInSc1
R	R	kA
<g/>
&	&	k?
<g/>
B.	B.	kA
Mezi	mezi	k7c4
výrazné	výrazný	k2eAgMnPc4d1
interprety	interpret	k1gMnPc4
New	New	k1gFnPc2
Orleanského	Orleanský	k2eAgInSc2d1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
řadíme	řadit	k5eAaImIp1nP
například	například	k6eAd1
Fatse	Fatse	k1gFnPc4
Domina	domino	k1gNnSc2
či	či	k8xC
Smiley	Smilea	k1gFnSc2
Lewis	Lewis	k1gFnSc2
<g/>
,	,	kIx,
Professor	Professor	k1gMnSc1
Longhair	Longhair	k1gMnSc1
<g/>
,	,	kIx,
Lloyd	Lloyd	k1gMnSc1
Price	Price	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
dalo	dát	k5eAaPmAgNnS
také	také	k6eAd1
"	"	kIx"
<g/>
život	život	k1gInSc4
<g/>
"	"	kIx"
velice	velice	k6eAd1
populárnímu	populární	k2eAgNnSc3d1
podžánru	podžánro	k1gNnSc3
50	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
"	"	kIx"
<g/>
Doo-Wop	Doo-Wop	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
harmonický	harmonický	k2eAgInSc4d1
hudební	hudební	k2eAgInSc4d1
crossover	crossover	k1gInSc4
styl	styl	k1gInSc1
provozovaný	provozovaný	k2eAgInSc1d1
větší	veliký	k2eAgFnSc7d2
vokální	vokální	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
(	(	kIx(
<g/>
původ	původ	k1gInSc1
tohoto	tento	k3xDgInSc2
stylu	styl	k1gInSc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c4
Barbershop	Barbershop	k1gInSc4
Quartets	Quartetsa	k1gFnPc2
<g/>
,	,	kIx,
A	a	k9
cappella	cappella	k6eAd1
či	či	k8xC
gospelu	gospel	k1gInSc2
<g/>
)	)	kIx)
s	s	k7c7
emotivní	emotivní	k2eAgFnSc7d1
melodií	melodie	k1gFnSc7
baladického	baladický	k2eAgInSc2d1
rock	rock	k1gInSc4
and	and	k?
rollu	roll	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
Fatse	Fatse	k1gFnSc1
Domina	domino	k1gNnSc2
<g/>
,	,	kIx,
The	The	k1gFnSc2
Drifters	Driftersa	k1gFnPc2
<g/>
,	,	kIx,
The	The	k1gFnPc2
Coasters	Coastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Americe	Amerika	k1gFnSc3
vládla	vládnout	k5eAaImAgFnS
soulová	soulový	k2eAgFnSc1d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k8xC
vyvíjející	vyvíjející	k2eAgFnSc1d1
se	se	k3xPyFc4
funková	funkový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc4
žánry	žánr	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
pozdější	pozdní	k2eAgInSc4d2
sound	sound	k1gInSc4
moderního	moderní	k2eAgNnSc2d1
R	R	kA
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
B.	B.	kA
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
James	James	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
,	,	kIx,
Wilson	Wilson	k1gMnSc1
Pickett	Pickett	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Temptations	Temptations	k1gInSc1
<g/>
,	,	kIx,
Aretha	Aretha	k1gFnSc1
Franklin	Franklin	k1gInSc1
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
&	&	k?
Dave	Dav	k1gInSc5
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Tex	Tex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
Marvin	Marvina	k1gFnPc2
Gaye	gay	k1gMnSc2
<g/>
,	,	kIx,
Curtis	Curtis	k1gInSc4
Mayfield	Mayfielda	k1gFnPc2
<g/>
,	,	kIx,
Donny	donna	k1gFnSc2
Hathaway	Hathawaa	k1gFnSc2
či	či	k8xC
Kool	Koola	k1gFnPc2
and	and	k?
the	the	k?
Gang	gang	k1gInSc1
</s>
<s>
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Popularita	popularita	k1gFnSc1
moderního	moderní	k2eAgInSc2d1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
vzrůstala	vzrůstat	k5eAaImAgFnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
s	s	k7c7
příchodem	příchod	k1gInSc7
umělců	umělec	k1gMnPc2
jako	jako	k8xS,k8xC
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Prince	princ	k1gMnSc2
<g/>
,	,	kIx,
Luther	Luthra	k1gFnPc2
Vandross	Vandrossa	k1gFnPc2
<g/>
,	,	kIx,
Whitney	Whitney	k1gInPc1
Houston	Houston	k1gInSc1
a	a	k8xC
New	New	k1gFnSc1
Edition	Edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
kombinací	kombinace	k1gFnSc7
černošské	černošský	k2eAgFnSc2d1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
hudby	hudba	k1gFnSc2
(	(	kIx(
<g/>
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
blues	blues	k1gInSc1
<g/>
,	,	kIx,
Motown	Motown	k1gInSc1
<g/>
,	,	kIx,
hip	hip	k0
hop	hop	k0
<g/>
)	)	kIx)
s	s	k7c7
popovými	popový	k2eAgInPc7d1
(	(	kIx(
<g/>
dance-pop	dance-pop	k1gInSc1
<g/>
,	,	kIx,
synthpop	synthpop	k1gInSc1
<g/>
,	,	kIx,
new	new	k?
wave	wav	k1gInPc4
atd	atd	kA
<g/>
)	)	kIx)
rytmy	rytmus	k1gInPc1
podžánr	podžánra	k1gFnPc2
New	New	k1gFnPc2
jack	jack	k6eAd1
swing	swing	k1gInSc4
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
Bobby	Bobb	k1gInPc1
Womack	Womacko	k1gNnPc2
<g/>
,	,	kIx,
Freddie	Freddie	k1gFnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Bobby	Bobba	k1gFnPc1
Brown	Brown	k1gMnSc1
<g/>
,	,	kIx,
Ray	Ray	k1gMnSc1
<g/>
,	,	kIx,
Goodman	Goodman	k1gMnSc1
&	&	k?
Brown	Brown	k1gMnSc1
<g/>
,	,	kIx,
Lakeside	Lakesid	k1gMnSc5
<g/>
,	,	kIx,
Maze	Maga	k1gFnSc6
<g/>
,	,	kIx,
Babyface	Babyface	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
MC	MC	kA
Hammer	Hammer	k1gInSc1
</s>
<s>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
stále	stále	k6eAd1
víc	hodně	k6eAd2
ovlivňováno	ovlivňovat	k5eAaImNgNnS
Hip-hopem	Hip-hop	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
byly	být	k5eAaImAgFnP
oblíbené	oblíbený	k2eAgInPc1d1
r	r	kA
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
b	b	k?
balady	balada	k1gFnPc4
s	s	k7c7
neo-soulovým	o-soulův	k2eNgInSc7d1
nádechem	nádech	k1gInSc7
(	(	kIx(
<g/>
Boyz	Boyz	k1gMnSc1
II	II	kA
Men	Men	k1gMnSc1
<g/>
,	,	kIx,
Toni	Toni	k1gMnSc1
Braxton	Braxton	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
vliv	vliv	k1gInSc1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
dostal	dostat	k5eAaPmAgMnS
až	až	k6eAd1
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
proudové	proudový	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
prvky	prvek	k1gInPc4
slyšíme	slyšet	k5eAaImIp1nP
v	v	k7c6
písních	píseň	k1gFnPc6
popových	popový	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
jako	jako	k8xS,k8xC
Alicia	Alicium	k1gNnSc2
Keys	Keysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
Alicia	Alicia	k1gFnSc1
Keys	Keys	k1gInSc1
<g/>
,	,	kIx,
Monica	Monica	k1gFnSc1
<g/>
,	,	kIx,
Brandy	brandy	k1gFnSc1
</s>
<s>
Hudebníci	hudebník	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
hudebníků	hudebník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
Brown	Brown	k1gMnSc1
</s>
<s>
Wilson	Wilson	k1gMnSc1
Pickett	Pickett	k1gMnSc1
</s>
<s>
The	The	k?
Temptations	Temptations	k1gInSc1
</s>
<s>
The	The	k?
Four	Four	k1gInSc1
Tops	Tops	k1gInSc1
</s>
<s>
Joe	Joe	k?
Tex	Tex	k1gFnSc1
</s>
<s>
Sam	Sam	k1gMnSc1
Cooke	Cook	k1gFnSc2
</s>
<s>
Otis	Otis	k6eAd1
Redding	Redding	k1gInSc1
</s>
<s>
Big	Big	k?
Joe	Joe	k1gMnSc1
Turner	turner	k1gMnSc1
</s>
<s>
Roy	Roy	k1gMnSc1
Brown	Brown	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PALMER	PALMER	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deep	Deep	k1gInSc1
Blues	blues	k1gNnSc2
<g/>
:	:	kIx,
A	a	k9
Musical	musical	k1gInSc1
and	and	k?
Cultural	Cultural	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
the	the	k?
Mississippi	Mississippi	k1gFnSc1
Delta	delta	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Viking	Viking	k1gMnSc1
Adult	Adult	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
670495115	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85113834	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85113834	#num#	k4
</s>
