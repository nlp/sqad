<s>
Jan	Jan	k1gMnSc1	Jan
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
nejčetnější	četný	k2eAgNnSc4d3	nejčetnější
české	český	k2eAgNnSc4d1	české
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
také	také	k9	také
druhé	druhý	k4xOgNnSc4	druhý
nejčastější	častý	k2eAgNnSc4d3	nejčastější
české	český	k2eAgNnSc4d1	české
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
י	י	k?	י
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ח	ח	k?	ח
<g/>
ָ	ָ	k?	ָ
<g/>
נ	נ	k?	נ
<g/>
ָ	ָ	k?	ָ
<g/>
ן	ן	k?	ן
<g/>
,	,	kIx,	,
Jo-chanan	Johanan	k1gInSc1	Jo-chanan
(	(	kIx(	(
<g/>
י	י	k?	י
ח	ח	k?	ח
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Hospodin	Hospodin	k1gMnSc1	Hospodin
je	být	k5eAaImIp3nS	být
milostivý	milostivý	k2eAgMnSc1d1	milostivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
domácí	domácí	k2eAgFnSc7d1	domácí
podobou	podoba	k1gFnSc7	podoba
Honza	Honza	k1gMnSc1	Honza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
zdrobněliny	zdrobnělina	k1gFnSc2	zdrobnělina
Hans	Hans	k1gMnSc1	Hans
(	(	kIx(	(
<g/>
od	od	k7c2	od
Johannes	Johannesa	k1gFnPc2	Johannesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
i	i	k9	i
v	v	k7c6	v
matričních	matriční	k2eAgInPc6d1	matriční
zápisech	zápis	k1gInPc6	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ještě	ještě	k6eAd1	ještě
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
matričních	matriční	k2eAgInPc6d1	matriční
zápisech	zápis	k1gInPc6	zápis
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
běžná	běžný	k2eAgFnSc1d1	běžná
varianta	varianta	k1gFnSc1	varianta
Ján	Ján	k1gMnSc1	Ján
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
pociťována	pociťován	k2eAgFnSc1d1	pociťována
jako	jako	k8xS	jako
slovakismus	slovakismus	k1gInSc1	slovakismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
např.	např.	kA	např.
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
svatojánský	svatojánský	k2eAgMnSc1d1	svatojánský
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jan	Jan	k1gMnSc1	Jan
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc4d1	populární
mezi	mezi	k7c7	mezi
papeži	papež	k1gMnPc7	papež
<g/>
,	,	kIx,	,
nosilo	nosit	k5eAaImAgNnS	nosit
je	být	k5eAaImIp3nS	být
23	[number]	k4	23
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
2	[number]	k4	2
vzdoropapežové	vzdoropapež	k1gMnPc1	vzdoropapež
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
dva	dva	k4xCgMnPc1	dva
papežové	papež	k1gMnPc1	papež
přijali	přijmout	k5eAaPmAgMnP	přijmout
jméno	jméno	k1gNnSc4	jméno
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Ženskou	ženský	k2eAgFnSc7d1	ženská
variantou	varianta	k1gFnSc7	varianta
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Honza	Honza	k1gMnSc1	Honza
<g/>
,	,	kIx,	,
Honzík	Honzík	k1gMnSc1	Honzík
<g/>
,	,	kIx,	,
Honzíček	Honzíček	k1gMnSc1	Honzíček
<g/>
,	,	kIx,	,
Jenda	Jenda	k1gMnSc1	Jenda
<g/>
,	,	kIx,	,
Jeníček	Jeníček	k1gMnSc1	Jeníček
<g/>
,	,	kIx,	,
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
Janík	Janík	k1gMnSc1	Janík
<g/>
,	,	kIx,	,
Janíček	Janíček	k1gMnSc1	Janíček
<g/>
,	,	kIx,	,
Janek	Janek	k1gMnSc1	Janek
<g/>
,	,	kIx,	,
Jéňa	Jéň	k2eAgFnSc1d1	Jéňa
Historická	historický	k2eAgFnSc1d1	historická
podoba	podoba	k1gFnSc1	podoba
<g/>
:	:	kIx,	:
Ješek	Ješek	k6eAd1	Ješek
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
–	–	k?	–
<g/>
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
nastává	nastávat	k5eAaImIp3nS	nastávat
přírůstek	přírůstek	k1gInSc4	přírůstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
mezi	mezi	k7c7	mezi
novorozenci	novorozenec	k1gMnPc7	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
nositele	nositel	k1gMnPc4	nositel
jména	jméno	k1gNnSc2	jméno
Jan	Jana	k1gFnPc2	Jana
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
může	moct	k5eAaImIp3nS	moct
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
označovat	označovat	k5eAaImF	označovat
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Jan	Jan	k1gMnSc1	Jan
Zebedeův	Zebedeův	k2eAgMnSc1d1	Zebedeův
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
ztotožňovaný	ztotožňovaný	k2eAgMnSc1d1	ztotožňovaný
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
Evangelia	evangelium	k1gNnPc4	evangelium
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
tří	tři	k4xCgMnPc2	tři
Listů	list	k1gInPc2	list
Janových	Janových	k2eAgFnSc1d1	Janových
a	a	k8xC	a
Zjevení	zjevení	k1gNnSc1	zjevení
Janova	Janov	k1gInSc2	Janov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
Papias	Papias	k1gInSc1	Papias
<g/>
,	,	kIx,	,
Eusebios	Eusebios	k1gInSc1	Eusebios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Presbyter	presbyter	k1gMnSc1	presbyter
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
autory	autor	k1gMnPc4	autor
janovských	janovský	k2eAgInPc2d1	janovský
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Zlatoústý	zlatoústý	k2eAgMnSc1d1	zlatoústý
neboli	neboli	k8xC	neboli
Chryzostom	Chryzostom	k1gInSc1	Chryzostom
(	(	kIx(	(
<g/>
347	[number]	k4	347
<g/>
-	-	kIx~	-
<g/>
408	[number]	k4	408
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
církvi	církev	k1gFnSc6	církev
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Avily	Avila	k1gFnSc2	Avila
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
-	-	kIx~	-
<g/>
1569	[number]	k4	1569
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Boha	bůh	k1gMnSc2	bůh
(	(	kIx(	(
<g/>
1495	[number]	k4	1495
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1440	[number]	k4	1440
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
Milosrdných	milosrdný	k2eAgMnPc2d1	milosrdný
bratří	bratr	k1gMnPc2	bratr
Jan	Jan	k1gMnSc1	Jan
Bosco	Bosco	k1gMnSc1	Bosco
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
řádu	řád	k1gInSc2	řád
salesiánů	salesián	k1gMnPc2	salesián
Jan	Jana	k1gFnPc2	Jana
od	od	k7c2	od
Kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
1542	[number]	k4	1542
<g/>
-	-	kIx~	-
<g/>
1591	[number]	k4	1591
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
karmelitán	karmelitán	k1gMnSc1	karmelitán
a	a	k8xC	a
mystický	mystický	k2eAgMnSc1d1	mystický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
(	(	kIx(	(
<g/>
před	před	k7c7	před
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1393	[number]	k4	1393
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
biskupský	biskupský	k2eAgMnSc1d1	biskupský
vikář	vikář	k1gMnSc1	vikář
a	a	k8xC	a
mučedník	mučedník	k1gMnSc1	mučedník
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Neumann	Neumann	k1gMnSc1	Neumann
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
(	(	kIx(	(
<g/>
1576	[number]	k4	1576
<g/>
-	-	kIx~	-
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
farář	farář	k1gMnSc1	farář
Jan	Jan	k1gMnSc1	Jan
Maria	Maria	k1gFnSc1	Maria
Vianney	Viannea	k1gMnSc2	Viannea
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
farář	farář	k1gMnSc1	farář
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
–	–	k?	–
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
Očko	očko	k1gNnSc4	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
Jan	Jan	k1gMnSc1	Jan
IX	IX	kA	IX
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
–	–	k?	–
poslední	poslední	k2eAgMnSc1d1	poslední
biskup	biskup	k1gMnSc1	biskup
jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Dukas	Dukas	k1gInSc1	Dukas
Laskaris	Laskaris	k1gFnSc2	Laskaris
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
<g/>
–	–	k?	–
kolem	kolem	k7c2	kolem
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikájský	nikájský	k2eAgMnSc1d1	nikájský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Etiopský	etiopský	k2eAgMnSc1d1	etiopský
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etiopský	etiopský	k2eAgMnSc1d1	etiopský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Krnovský	krnovský	k2eAgMnSc1d1	krnovský
(	(	kIx(	(
<g/>
1440	[number]	k4	1440
<g/>
-	-	kIx~	-
<g/>
1483	[number]	k4	1483
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
krnovsko-ratibořský	krnovskoatibořský	k2eAgMnSc1d1	krnovsko-ratibořský
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Bretaňský	bretaňský	k2eAgMnSc1d1	bretaňský
(	(	kIx(	(
<g/>
Moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
;	;	kIx,	;
1389	[number]	k4	1389
<g/>
-	-	kIx~	-
<g/>
1442	[number]	k4	1442
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bretaňský	bretaňský	k2eAgMnSc1d1	bretaňský
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Montfortu	Montfort	k1gInSc2	Montfort
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Palaiologos	Palaiologos	k1gMnSc1	Palaiologos
(	(	kIx(	(
<g/>
1332	[number]	k4	1332
<g/>
-	-	kIx~	-
<g/>
1391	[number]	k4	1391
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
(	(	kIx(	(
<g/>
1689	[number]	k4	1689
<g/>
-	-	kIx~	-
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
<g/>
–	–	k?	–
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Algarve	Algarev	k1gFnSc2	Algarev
Jan	Jan	k1gMnSc1	Jan
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Palaiologos	Palaiologos	k1gInSc1	Palaiologos
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
<g/>
-	-	kIx~	-
<g/>
1408	[number]	k4	1408
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Palaiologos	Palaiologos	k1gInSc1	Palaiologos
(	(	kIx(	(
<g/>
1392	[number]	k4	1392
<g/>
-	-	kIx~	-
<g/>
1448	[number]	k4	1448
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Jan	Jan	k1gMnSc1	Jan
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Jan	Jana	k1gFnPc2	Jana
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
a	a	k8xC	a
Kastilský	kastilský	k2eAgMnSc1d1	kastilský
(	(	kIx(	(
<g/>
1478	[number]	k4	1478
<g/>
-	-	kIx~	-
<g/>
1497	[number]	k4	1497
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
Aragonu	Aragon	k1gInSc2	Aragon
Jan	Jan	k1gMnSc1	Jan
Bezzemek	bezzemek	k1gMnSc1	bezzemek
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Lackland	Lacklanda	k1gFnPc2	Lacklanda
<g/>
;	;	kIx,	;
1166	[number]	k4	1166
<g/>
-	-	kIx~	-
<g/>
1216	[number]	k4	1216
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Anglie	Anglie	k1gFnSc2	Anglie
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
Jan	Jan	k1gMnSc1	Jan
Bourbonský	bourbonský	k2eAgMnSc1d1	bourbonský
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
barcelonský	barcelonský	k2eAgMnSc1d1	barcelonský
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Brienne	Brienn	k1gInSc5	Brienn
(	(	kIx(	(
<g/>
1148	[number]	k4	1148
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1237	[number]	k4	1237
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
jeruzalémský	jeruzalémský	k2eAgMnSc1d1	jeruzalémský
Jan	Jan	k1gMnSc1	Jan
Cicero	Cicero	k1gMnSc1	Cicero
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
(	(	kIx(	(
<g/>
1455	[number]	k4	1455
<g/>
-	-	kIx~	-
<g/>
1499	[number]	k4	1499
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Braniborského	braniborský	k2eAgNnSc2d1	braniborské
markrabství	markrabství	k1gNnSc2	markrabství
Jan	Jan	k1gMnSc1	Jan
Fridrich	Fridrich	k1gMnSc1	Fridrich
Württemberský	Württemberský	k2eAgMnSc1d1	Württemberský
(	(	kIx(	(
<g/>
1582	[number]	k4	1582
<g/>
-	-	kIx~	-
<g/>
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1608	[number]	k4	1608
až	až	k8xS	až
1628	[number]	k4	1628
vévoda	vévoda	k1gMnSc1	vévoda
württemberský	württemberský	k2eAgMnSc1d1	württemberský
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
(	(	kIx(	(
<g/>
1340	[number]	k4	1340
<g/>
–	–	k?	–
<g/>
1399	[number]	k4	1399
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lancasteru	Lancaster	k1gInSc2	Lancaster
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Habsbursko-Lotrinský	habsburskootrinský	k2eAgMnSc1d1	habsbursko-lotrinský
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
a	a	k8xC	a
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
1322	[number]	k4	1322
<g/>
-	-	kIx~	-
<g/>
1375	[number]	k4	1375
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
Anhaltsko-Desavský	Anhaltsko-Desavský	k2eAgMnSc1d1	Anhaltsko-Desavský
(	(	kIx(	(
<g/>
1567	[number]	k4	1567
<g/>
-	-	kIx~	-
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Anhaltsko-desavský	Anhaltskoesavský	k2eAgMnSc1d1	Anhaltsko-desavský
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
Saský	saský	k2eAgMnSc1d1	saský
(	(	kIx(	(
<g/>
1585	[number]	k4	1585
<g/>
-	-	kIx~	-
<g/>
1656	[number]	k4	1656
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Wettinů	Wettin	k1gMnPc2	Wettin
Jan	Jan	k1gMnSc1	Jan
Karel	Karel	k1gMnSc1	Karel
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
-	-	kIx~	-
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Jan	Jan	k1gMnSc1	Jan
Kazimír	Kazimír	k1gMnSc1	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vasa	Vasa	k1gFnSc1	Vasa
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
-	-	kIx~	-
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
1296	[number]	k4	1296
<g/>
-	-	kIx~	-
<g/>
1346	[number]	k4	1346
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Minsterbersko-Olešnický	Minsterbersko-Olešnický	k2eAgMnSc1d1	Minsterbersko-Olešnický
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
-	-	kIx~	-
<g/>
1565	[number]	k4	1565
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
minsterbersko-olešnický	minsterberskolešnický	k2eAgMnSc1d1	minsterbersko-olešnický
Jan	Jan	k1gMnSc1	Jan
Soběslav	Soběslav	k1gMnSc1	Soběslav
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
1355	[number]	k4	1355
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
1380	[number]	k4	1380
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
Jan	Jan	k1gMnSc1	Jan
Tristan	Tristan	k1gInSc1	Tristan
z	z	k7c2	z
Nevers	Neversa	k1gFnPc2	Neversa
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
<g/>
-	-	kIx~	-
<g/>
1270	[number]	k4	1270
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Nevers	Neversa	k1gFnPc2	Neversa
a	a	k8xC	a
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Kapetovců	Kapetovec	k1gMnPc2	Kapetovec
Jan	Jan	k1gMnSc1	Jan
Vilém	Vilém	k1gMnSc1	Vilém
Falcký	falcký	k2eAgMnSc1d1	falcký
(	(	kIx(	(
<g/>
1658	[number]	k4	1658
<g/>
-	-	kIx~	-
<g/>
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
jülišský	jülišský	k2eAgMnSc1d1	jülišský
a	a	k8xC	a
bergský	bergský	k2eAgMnSc1d1	bergský
a	a	k8xC	a
kurfiřt-falckrabě	kurfiřtalckrabě	k6eAd1	kurfiřt-falckrabě
Jan	Jan	k1gMnSc1	Jan
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
(	(	kIx(	(
<g/>
1487	[number]	k4	1487
<g/>
-	-	kIx~	-
<g/>
1540	[number]	k4	1540
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Zhořelecký	zhořelecký	k2eAgMnSc1d1	zhořelecký
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
<g/>
-	-	kIx~	-
<g/>
1396	[number]	k4	1396
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
zhořelecký	zhořelecký	k2eAgMnSc1d1	zhořelecký
Jan	Jan	k1gMnSc1	Jan
Zikmund	Zikmund	k1gMnSc1	Zikmund
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
(	(	kIx(	(
<g/>
1572	[number]	k4	1572
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Jan	Jan	k1gMnSc1	Jan
Zikmund	Zikmund	k1gMnSc1	Zikmund
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
-	-	kIx~	-
<g/>
1571	[number]	k4	1571
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
–	–	k?	–
postava	postava	k1gFnSc1	postava
středověké	středověký	k2eAgFnSc2d1	středověká
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
mocném	mocný	k2eAgMnSc6d1	mocný
křesťanském	křesťanský	k2eAgMnSc6d1	křesťanský
vládci	vládce	k1gMnSc6	vládce
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
Libor	Libor	k1gMnSc1	Libor
Jan	Jan	k1gMnSc1	Jan
–	–	k?	–
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
Český	český	k2eAgInSc4d1	český
kalendář	kalendář	k1gInSc4	kalendář
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
)	)	kIx)	)
Slovenský	slovenský	k2eAgInSc1d1	slovenský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Římskokatolický	římskokatolický	k2eAgInSc4d1	římskokatolický
kalendář	kalendář	k1gInSc4	kalendář
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gNnSc1	Bosco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
–	–	k?	–
narození	narození	k1gNnSc2	narození
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
Vianney	Viannea	k1gFnSc2	Viannea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
–	–	k?	–
stětí	stětí	k1gNnPc2	stětí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Zlatoústý	zlatoústý	k2eAgMnSc1d1	zlatoústý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Kapistránský	Kapistránský	k2eAgMnSc1d1	Kapistránský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
od	od	k7c2	od
Kříže	kříž	k1gInSc2	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
křestních	křestní	k2eAgNnPc2d1	křestní
jmen	jméno	k1gNnPc2	jméno
Jeníček	Jeníček	k1gMnSc1	Jeníček
Janík	Janík	k1gMnSc1	Janík
Janko	Janko	k1gMnSc1	Janko
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Ján	Ján	k1gMnSc1	Ján
<g/>
"	"	kIx"	"
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jana	k1gFnPc2	Jana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Jan	Jan	k1gMnSc1	Jan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Četnost	četnost	k1gFnSc4	četnost
jmen	jméno	k1gNnPc2	jméno
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČSÚ	ČSÚ	kA	ČSÚ
Četnost	četnost	k1gFnSc4	četnost
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
na	na	k7c6	na
webu	web	k1gInSc6	web
MV	MV	kA	MV
ČR	ČR	kA	ČR
</s>
