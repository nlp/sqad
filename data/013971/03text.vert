<s>
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
WTO	WTO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
WTO	WTO	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
zakládající	zakládající	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
WTO	WTO	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
regulace	regulace	k1gFnSc1
mezinárodního	mezinárodní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
Úřední	úřední	k2eAgNnSc1d1
jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
<g/>
,	,	kIx,
italština	italština	k1gFnSc1
(	(	kIx(
<g/>
neoficiálně	neoficiálně	k6eAd1,k6eNd1
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
</s>
<s>
164	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
Generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
</s>
<s>
Ngozi	Ngoze	k1gFnSc4
Okonjo-Iweala	Okonjo-Iweal	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
640	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.wto.org	www.wto.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
WTO	WTO	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
jako	jako	k8xC,k8xS
nástupce	nástupce	k1gMnSc1
Všeobecné	všeobecný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primárním	primární	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
rovnost	rovnost	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
otevřeného	otevřený	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	s	k7c7
členským	členský	k2eAgInSc7d1
státům	stát	k1gInPc3
poskytnout	poskytnout	k5eAaPmF
mechanismus	mechanismus	k1gInSc1
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
něhož	jenž	k3xRgMnSc2
mohou	moct	k5eAaImIp3nP
země	zem	k1gFnPc1
redukovat	redukovat	k5eAaBmF
obchodní	obchodní	k2eAgFnPc4d1
bariéry	bariéra	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgMnPc4d3
hráče	hráč	k1gMnPc4
obchodní	obchodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
činnost	činnost	k1gFnSc4
neprobíhá	probíhat	k5eNaImIp3nS
bez	bez	k7c2
problémů	problém	k1gInPc2
<g/>
:	:	kIx,
zásadním	zásadní	k2eAgInSc7d1
sporem	spor	k1gInSc7
je	být	k5eAaImIp3nS
rozdíl	rozdíl	k1gInSc1
zájmů	zájem	k1gInPc2
mezi	mezi	k7c7
bohatými	bohatý	k2eAgMnPc7d1
(	(	kIx(
<g/>
průmyslovými	průmyslový	k2eAgMnPc7d1
<g/>
)	)	kIx)
a	a	k8xC
chudšími	chudý	k2eAgFnPc7d2
(	(	kIx(
<g/>
zemědělskými	zemědělský	k2eAgFnPc7d1
<g/>
)	)	kIx)
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
</s>
<s>
Místo	místo	k7c2
konání	konání	k1gNnSc2
Brettonwoodské	Brettonwoodský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
</s>
<s>
I	i	k9
když	když	k8xS
samotná	samotný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
ji	on	k3xPp3gFnSc4
můžeme	moct	k5eAaImIp1nP
označit	označit	k5eAaPmF
za	za	k7c4
nástupce	nástupce	k1gMnSc4
GATT	GATT	kA
(	(	kIx(
<g/>
General	General	k1gMnSc1
Agreement	Agreement	k1gMnSc1
on	on	k3xPp3gMnSc1
Tariffs	Tariffs	k1gInSc1
and	and	k?
Trade	Trad	k1gInSc5
–	–	k?
Všeobecná	všeobecný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jednání	jednání	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Brettonwoodské	Brettonwoodský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
byl	být	k5eAaImAgInS
vznik	vznik	k1gInSc1
Mezinárodního	mezinárodní	k2eAgInSc2d1
měnového	měnový	k2eAgInSc2d1
fondu	fond	k1gInSc2
(	(	kIx(
<g/>
International	International	k1gFnSc1
Monetary	Monetara	k1gFnSc2
Fund	fund	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Světové	světový	k2eAgFnPc1d1
banky	banka	k1gFnPc1
(	(	kIx(
<g/>
World	World	k1gInSc1
Bank	bank	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
vytvořit	vytvořit	k5eAaPmF
odbornou	odborný	k2eAgFnSc4d1
agenturu	agentura	k1gFnSc4
ITO	ITO	kA
(	(	kIx(
<g/>
International	International	k1gMnSc5
Trade	Trad	k1gMnSc5
Organization	Organization	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neshody	neshoda	k1gFnSc2
při	při	k7c6
vytváření	vytváření	k1gNnSc6
organizace	organizace	k1gFnSc2
však	však	k9
vedly	vést	k5eAaImAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ITO	ITO	kA
nikdy	nikdy	k6eAd1
nevešla	vejít	k5eNaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
přijata	přijmout	k5eAaPmNgFnS
méně	málo	k6eAd2
ctižádostivá	ctižádostivý	k2eAgFnSc1d1
Všeobecná	všeobecný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
GATT	GATT	kA
se	se	k3xPyFc4
od	od	k7c2
současné	současný	k2eAgFnSc2d1
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
liší	lišit	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
dohodu	dohoda	k1gFnSc4
obsahující	obsahující	k2eAgFnSc2d1
smluvní	smluvní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
WTO	WTO	kA
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
mezinárodní	mezinárodní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
s	s	k7c7
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uruguayské	uruguayský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1
během	během	k7c2
Uruguayského	uruguayský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
začalo	začít	k5eAaPmAgNnS
osmé	osmý	k4xOgNnSc1
vyjednávací	vyjednávací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
Všeobecné	všeobecný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
trvalo	trvat	k5eAaImAgNnS
téměř	téměř	k6eAd1
osm	osm	k4xCc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
podle	podle	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednání	jednání	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
–	–	k?
uruguayského	uruguayský	k2eAgNnSc2d1
města	město	k1gNnSc2
Punta	punto	k1gNnSc2
del	del	k?
Este	Est	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
bodů	bod	k1gInPc2
agendy	agenda	k1gFnSc2
bylo	být	k5eAaImAgNnS
shodnout	shodnout	k5eAaBmF,k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
podmínkách	podmínka	k1gFnPc6
vytvoření	vytvoření	k1gNnSc2
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
stanoveny	stanoven	k2eAgInPc1d1
i	i	k8xC
další	další	k2eAgInPc1d1
cíle	cíl	k1gInPc1
jednání	jednání	k1gNnSc1
–	–	k?
liberalizace	liberalizace	k1gFnSc2
mezinárodního	mezinárodní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
obrana	obrana	k1gFnSc1
vůči	vůči	k7c3
protekcionismu	protekcionismus	k1gInSc3
a	a	k8xC
snaha	snaha	k1gFnSc1
vylepšit	vylepšit	k5eAaPmF
mezinárodní	mezinárodní	k2eAgInSc4d1
obchodní	obchodní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
však	však	k9
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
dubnu	duben	k1gInSc6
1994	#num#	k4
v	v	k7c6
marockém	marocký	k2eAgInSc6d1
Marrakéši	Marrakéš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
celého	celý	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
Dohody	dohoda	k1gFnSc2
o	o	k7c4
zřízení	zřízení	k1gNnSc4
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ratifikovalo	ratifikovat	k5eAaBmAgNnS
všech	všecek	k3xTgMnPc2
125	#num#	k4
zástupců	zástupce	k1gMnPc2
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
dohodě	dohoda	k1gFnSc6
byly	být	k5eAaImAgInP
přijaty	přijmout	k5eAaPmNgInP
nové	nový	k2eAgInPc1d1
cíle	cíl	k1gInPc1
v	v	k7c6
rámci	rámec	k1gInSc6
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgInPc1d1
WTO	WTO	kA
<g/>
:	:	kIx,
snížení	snížení	k1gNnSc4
nezaměstnanosti	nezaměstnanost	k1gFnSc2
<g/>
,	,	kIx,
zvýšení	zvýšení	k1gNnSc1
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
zlepšení	zlepšení	k1gNnSc2
životní	životní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
a	a	k8xC
také	také	k9
snaha	snaha	k1gFnSc1
o	o	k7c6
dosažení	dosažení	k1gNnSc6
co	co	k9
nejefektivnější	efektivní	k2eAgFnPc4d3
poptávky	poptávka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
tedy	tedy	k9
oficiálně	oficiálně	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
v	v	k7c6
tomto	tento	k3xDgNnSc6
kole	kolo	k1gNnSc6
tzv.	tzv.	kA
jednotný	jednotný	k2eAgInSc4d1
závazek	závazek	k1gInSc4
(	(	kIx(
<g/>
single	singl	k1gInSc5
undertaking	undertaking	k1gInSc4
<g/>
)	)	kIx)
k	k	k7c3
prosazení	prosazení	k1gNnSc3
svých	svůj	k3xOyFgInPc2
zájmů	zájem	k1gInPc2
a	a	k8xC
preferencí	preference	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
přijetí	přijetí	k1gNnSc1
závazků	závazek	k1gInPc2
ohledně	ohledně	k7c2
ochrany	ochrana	k1gFnSc2
práv	právo	k1gNnPc2
k	k	k7c3
duševnímu	duševní	k2eAgNnSc3d1
vlastnictví	vlastnictví	k1gNnSc3
a	a	k8xC
liberalizaci	liberalizace	k1gFnSc3
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
závazek	závazek	k1gInSc1
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
rozvojové	rozvojový	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nejevily	jevit	k5eNaImAgInP
o	o	k7c4
tyto	tento	k3xDgFnPc4
dohody	dohoda	k1gFnPc4
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
před	před	k7c4
ultimátum	ultimátum	k1gNnSc4
<g/>
:	:	kIx,
buď	buď	k8xC
přijmou	přijmout	k5eAaPmIp3nP
závazky	závazek	k1gInPc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
ohledech	ohled	k1gInPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
nebudou	být	k5eNaImBp3nP
moci	moct	k5eAaImF
připojit	připojit	k5eAaPmF
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
Výsledkem	výsledek	k1gInSc7
tohoto	tento	k3xDgInSc2
závazku	závazek	k1gInSc2
bylo	být	k5eAaImAgNnS
přijetí	přijetí	k1gNnSc1
Dohody	dohoda	k1gFnSc2
o	o	k7c6
obchodních	obchodní	k2eAgInPc6d1
aspektech	aspekt	k1gInPc6
práv	právo	k1gNnPc2
k	k	k7c3
duševnímu	duševní	k2eAgNnSc3d1
vlastnictví	vlastnictví	k1gNnSc3
(	(	kIx(
<g/>
Agreement	Agreement	k1gInSc4
on	on	k3xPp3gMnSc1
Trade-Related	Trade-Related	k1gMnSc1
Aspects	Aspectsa	k1gFnPc2
of	of	k?
Intellectual	Intellectual	k1gMnSc1
Property	Propert	k1gMnPc4
Rights	Rights	k1gInSc4
<g/>
,	,	kIx,
TRIPS	TRIPS	kA
<g/>
)	)	kIx)
a	a	k8xC
Všeobecná	všeobecný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
obchodu	obchod	k1gInSc6
službami	služba	k1gFnPc7
(	(	kIx(
<g/>
General	General	k1gMnPc1
Agreement	Agreement	k1gMnSc1
on	on	k3xPp3gMnSc1
Trade	Trad	k1gInSc5
in	in	k?
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
objevují	objevovat	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc1
o	o	k7c6
exportu	export	k1gInSc6
a	a	k8xC
importu	import	k1gInSc3
terciárního	terciární	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
dvě	dva	k4xCgNnPc4
důležitá	důležitý	k2eAgNnPc4d1
ustanovení	ustanovení	k1gNnPc4
<g/>
:	:	kIx,
ustanovení	ustanovení	k1gNnPc4
o	o	k7c6
národním	národní	k2eAgNnSc6d1
zacházení	zacházení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
brání	bránit	k5eAaImIp3nS
exportéry	exportér	k1gMnPc4
před	před	k7c7
protekcionismem	protekcionismus	k1gInSc7
a	a	k8xC
zacházení	zacházení	k1gNnSc1
podle	podle	k7c2
nejvyšších	vysoký	k2eAgFnPc2d3
dohod	dohoda	k1gFnPc2
upravující	upravující	k2eAgNnSc4d1
vzájemné	vzájemný	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
jednání	jednání	k1gNnSc2
bylo	být	k5eAaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
několik	několik	k4yIc1
úmluv	úmluva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
doplnily	doplnit	k5eAaPmAgFnP
tehdy	tehdy	k6eAd1
existující	existující	k2eAgInSc4d1
GATT	GATT	kA
(	(	kIx(
<g/>
označovanou	označovaný	k2eAgFnSc4d1
jako	jako	k8xC,k8xS
GATT	GATT	kA
47	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zasazení	zasazení	k1gNnSc6
těchto	tento	k3xDgFnPc2
úmluv	úmluva	k1gFnPc2
a	a	k8xC
menších	malý	k2eAgFnPc6d2
úpravách	úprava	k1gFnPc6
vznikla	vzniknout	k5eAaPmAgFnS
tzv.	tzv.	kA
GATT	GATT	kA
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecná	všeobecný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
tedy	tedy	k9
stále	stále	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
jen	jen	k9
s	s	k7c7
menšími	malý	k2eAgFnPc7d2
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katarské	katarský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Katarské	katarský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
Rozvojová	rozvojový	k2eAgFnSc1d1
agenda	agenda	k1gFnSc1
z	z	k7c2
Dohá	Dohá	k1gFnSc1
či	či	k8xC
Kolo	kolo	k1gNnSc1
z	z	k7c2
Dohá	Dohá	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
současné	současný	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
vyjednávání	vyjednávání	k1gNnSc2
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
ministerskou	ministerský	k2eAgFnSc7d1
radou	rada	k1gFnSc7
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2001	#num#	k4
v	v	k7c6
katarském	katarský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Dohá	Dohá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1
bodem	bod	k1gInSc7
agendy	agenda	k1gFnSc2
je	být	k5eAaImIp3nS
eliminace	eliminace	k1gFnSc1
obchodních	obchodní	k2eAgFnPc2d1
bariér	bariéra	k1gFnPc2
a	a	k8xC
větší	veliký	k2eAgNnSc4d2
zapojení	zapojení	k1gNnSc4
rozvojových	rozvojový	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
katarského	katarský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
je	být	k5eAaImIp3nS
i	i	k9
tzv.	tzv.	kA
„	„	k?
<g/>
balíček	balíček	k1gInSc1
z	z	k7c2
Bali	Bal	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Bali	Bal	k1gFnSc2
Package	Packag	k1gFnSc2
<g/>
)	)	kIx)
přijatý	přijatý	k2eAgInSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
ministerské	ministerský	k2eAgNnSc1d1
konferenci	konference	k1gFnSc4
na	na	k7c4
Bali	Bale	k1gFnSc4
v	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
12	#num#	k4
letech	let	k1gInPc6
neúspěšného	úspěšný	k2eNgNnSc2d1
jednání	jednání	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
konečně	konečně	k6eAd1
ke	k	k7c3
shodě	shoda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
balíčku	balíček	k1gInSc2
byla	být	k5eAaImAgFnS
tzv.	tzv.	kA
Dohoda	dohoda	k1gFnSc1
o	o	k7c4
usnadnění	usnadnění	k1gNnSc4
obchodu	obchod	k1gInSc2
(	(	kIx(
<g/>
Trade	Trad	k1gInSc5
Facilitation	Facilitation	k1gInSc4
Agreement	Agreement	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgFnSc4
multilaterální	multilaterální	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Indie	Indie	k1gFnSc1
zpočátku	zpočátku	k6eAd1
tuto	tento	k3xDgFnSc4
dohodu	dohoda	k1gFnSc4
vetovala	vetovat	k5eAaBmAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
skladování	skladování	k1gNnSc2
potravin	potravina	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
food	food	k6eAd1
stock	stock	k6eAd1
holding	holding	k1gInSc4
programme	programit	k5eAaPmRp1nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
obavy	obava	k1gFnPc1
vyřešily	vyřešit	k5eAaPmAgFnP
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
Indii	Indie	k1gFnSc6
ujistily	ujistit	k5eAaPmAgFnP
o	o	k7c6
účinnosti	účinnost	k1gFnSc6
klauzule	klauzule	k1gFnSc2
mírového	mírový	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
obchodu	obchod	k1gInSc2
se	s	k7c7
zemědělskými	zemědělský	k2eAgInPc7d1
produkty	produkt	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Balíček	balíček	k1gMnSc1
se	se	k3xPyFc4
také	také	k6eAd1
dotkl	dotknout	k5eAaPmAgInS
určitých	určitý	k2eAgInPc2d1
sektorů	sektor	k1gInPc2
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
–	–	k?
zejména	zejména	k9
snaha	snaha	k1gFnSc1
stanovit	stanovit	k5eAaPmF
celní	celní	k2eAgFnSc4d1
kvóty	kvóta	k1gFnPc4
<g/>
,	,	kIx,
omezit	omezit	k5eAaPmF
domácí	domácí	k2eAgFnPc4d1
subvence	subvence	k1gFnPc4
a	a	k8xC
pomoci	pomoc	k1gFnPc4
nejméně	málo	k6eAd3
rozvinutým	rozvinutý	k2eAgInPc3d1
státům	stát	k1gInPc3
(	(	kIx(
<g/>
LDCs	LDCsa	k1gFnPc2
–	–	k?
Least	Least	k1gMnSc1
Developed	Developed	k1gMnSc1
Countries	Countries	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
organizace	organizace	k1gFnSc2
</s>
<s>
Generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
-	-	kIx~
Roberto	Roberta	k1gFnSc5
Azevê	Azevê	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Sekretariát	sekretariát	k1gInSc1
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
</s>
<s>
Sekretariát	sekretariát	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
634	#num#	k4
úředníků	úředník	k1gMnPc2
<g/>
,	,	kIx,
reprezentující	reprezentující	k2eAgFnSc1d1
78	#num#	k4
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
brazilský	brazilský	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
a	a	k8xC
dlouholetý	dlouholetý	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
Brazílie	Brazílie	k1gFnSc2
ve	v	k7c6
WTO	WTO	kA
Roberto	Roberta	k1gFnSc5
Azevê	Azevê	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
skončilo	skončit	k5eAaPmAgNnS
druhé	druhý	k4xOgFnSc6
období	období	k1gNnSc4
Francouzovi	Francouz	k1gMnSc3
Pascalovi	Pascal	k1gMnSc3
Lamymu	Lamym	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
dohlížet	dohlížet	k5eAaImF
na	na	k7c4
správnost	správnost	k1gFnSc4
jednání	jednání	k1gNnSc2
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
role	role	k1gFnPc4
fungovala	fungovat	k5eAaImAgFnS
již	již	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Všeobecnou	všeobecný	k2eAgFnSc7d1
dohodou	dohoda	k1gFnSc7
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
(	(	kIx(
<g/>
GATT	GATT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sekretariát	sekretariát	k1gInSc1
nemá	mít	k5eNaImIp3nS
pravomoc	pravomoc	k1gFnSc4
rozhodovat	rozhodovat	k5eAaImF
<g/>
,	,	kIx,
tu	tu	k6eAd1
mají	mít	k5eAaImIp3nP
pouze	pouze	k6eAd1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
úlohou	úloha	k1gFnSc7
je	být	k5eAaImIp3nS
technická	technický	k2eAgFnSc1d1
a	a	k8xC
organizační	organizační	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
konkrétních	konkrétní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekretariát	sekretariát	k1gInSc4
rovněž	rovněž	k9
plní	plnit	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
zprostředkovatele	zprostředkovatel	k1gMnSc2
informací	informace	k1gFnPc2
skrze	skrze	k?
média	médium	k1gNnPc4
a	a	k8xC
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
radí	radit	k5eAaImIp3nS
potenciálním	potenciální	k2eAgMnPc3d1
zájemcům	zájemce	k1gMnPc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
stát	stát	k5eAaImF,k5eAaPmF
členem	člen	k1gInSc7
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úředním	úřední	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
a	a	k8xC
francouzština	francouzština	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ministerské	ministerský	k2eAgFnPc1d1
konference	konference	k1gFnPc1
</s>
<s>
Ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
členský	členský	k2eAgInSc1d1
stát	stát	k1gInSc1
má	mít	k5eAaImIp3nS
zde	zde	k6eAd1
svého	svůj	k3xOyFgMnSc4
zástupce	zástupce	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
hlasovat	hlasovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasuje	hlasovat	k5eAaImIp3nS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
konsenzu	konsenz	k1gInSc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
formálním	formální	k2eAgNnSc7d1
hlasováním	hlasování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
zpracovává	zpracovávat	k5eAaImIp3nS
agendu	agenda	k1gFnSc4
pro	pro	k7c4
budoucí	budoucí	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
a	a	k8xC
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
schází	scházet	k5eAaImIp3nS
minimálně	minimálně	k6eAd1
každé	každý	k3xTgNnSc4
2	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Každé	každý	k3xTgNnSc1
vyjednávací	vyjednávací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
4	#num#	k4
fáze	fáze	k1gFnSc2
<g/>
:	:	kIx,
vyjednávání	vyjednávání	k1gNnSc1
před	před	k7c7
vlastním	vlastní	k2eAgNnSc7d1
vyjednáváním	vyjednávání	k1gNnSc7
<g/>
,	,	kIx,
vytváření	vytváření	k1gNnSc3
agendy	agenda	k1gFnSc2
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgNnSc1d1
vyjednávání	vyjednávání	k1gNnSc1
a	a	k8xC
vyjednávání	vyjednávání	k1gNnSc1
po	po	k7c6
vlastním	vlastní	k2eAgNnSc6d1
vyjednávání	vyjednávání	k1gNnSc6
uzavřených	uzavřený	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
důraz	důraz	k1gInSc1
se	se	k3xPyFc4
klade	klást	k5eAaImIp3nS
na	na	k7c4
samotné	samotný	k2eAgNnSc4d1
vytváření	vytváření	k1gNnSc4
agendy	agenda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomuto	tento	k3xDgInSc3
bodu	bod	k1gInSc3
nejprve	nejprve	k6eAd1
selhalo	selhat	k5eAaPmAgNnS
zahájení	zahájení	k1gNnSc1
tzv.	tzv.	kA
uruguayského	uruguayský	k2eAgNnSc2d1
i	i	k8xC
katarského	katarský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
nedospěly	dochvít	k5eNaPmAgInP
ke	k	k7c3
konsenzu	konsenz	k1gInSc3
ohledně	ohledně	k7c2
obsahu	obsah	k1gInSc2
agendy	agenda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
ministerských	ministerský	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
</s>
<s>
První	první	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1996	#num#	k4
v	v	k7c6
Singapuru	Singapur	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
ministři	ministr	k1gMnPc1
obchodu	obchod	k1gInSc2
se	se	k3xPyFc4
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
na	na	k7c6
spolupráci	spolupráce	k1gFnSc6
týkající	týkající	k2eAgFnSc1d1
se	se	k3xPyFc4
zapojení	zapojení	k1gNnSc2
nejméně	málo	k6eAd3
rozvinutých	rozvinutý	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
LDCs	LDCs	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
Protesty	protest	k1gInPc1
v	v	k7c6
Seattlu	Seattl	k1gInSc6
během	během	k7c2
ministerské	ministerský	k2eAgFnSc2d1
konferenceTřetí	konferenceTřetit	k5eAaPmIp3nS
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
od	od	k7c2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1999	#num#	k4
v	v	k7c6
Seattlu	Seattl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
tohoto	tento	k3xDgNnSc2
setkání	setkání	k1gNnSc2
bylo	být	k5eAaImAgNnS
shodnout	shodnout	k5eAaPmF,k5eAaBmF
se	se	k3xPyFc4
na	na	k7c6
agendě	agenda	k1gFnSc6
příštího	příští	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
jednání	jednání	k1gNnSc2
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
ministři	ministr	k1gMnPc1
nedospěli	dochvít	k5eNaPmAgMnP
ke	k	k7c3
konsenzu	konsenz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
boj	boj	k1gInSc1
v	v	k7c6
Seattlu	Seattl	k1gInSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Battle	Battle	k1gFnSc1
at	at	k?
Seattle	Seattle	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednání	jednání	k1gNnSc2
doprovázely	doprovázet	k5eAaImAgInP
pouliční	pouliční	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
a	a	k8xC
antiglobalizační	antiglobalizační	k2eAgInPc1d1
protesty	protest	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
Dohá	Dohá	k1gFnSc1
během	během	k7c2
9	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
jednací	jednací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
organizace	organizace	k1gFnSc2
zvané	zvaný	k2eAgNnSc1d1
Katarské	katarský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Pátá	pátý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
Cancúnu	Cancúno	k1gNnSc6
od	od	k7c2
10	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednání	jednání	k1gNnSc4
skončila	skončit	k5eAaPmAgFnS
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgInS
nalezen	nalezen	k2eAgInSc1d1
konsenzus	konsenzus	k1gInSc1
ohledně	ohledně	k7c2
zapojení	zapojení	k1gNnSc2
rozvojových	rozvojový	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šestá	šestý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
a	a	k8xC
osmá	osmý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
uskutečnily	uskutečnit	k5eAaPmAgInP
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
probíhala	probíhat	k5eAaImAgFnS
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Devátá	devátý	k4xOgFnSc1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
během	během	k7c2
3	#num#	k4
<g/>
.	.	kIx.
až	až	k9
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
na	na	k7c6
Bali	Bal	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatím	zatím	k6eAd1
poslední	poslední	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k9
desátá	desátý	k4xOgFnSc1
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
v	v	k7c6
Nairobi	Nairobi	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc1d1
konference	konference	k1gFnSc1
je	být	k5eAaImIp3nS
naplánována	naplánovat	k5eAaBmNgFnS
na	na	k7c4
rok	rok	k1gInSc4
2017	#num#	k4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Aires	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Generální	generální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Generální	generální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgInSc7
hlavním	hlavní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
hned	hned	k6eAd1
po	po	k7c6
ministerské	ministerský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
vyplývají	vyplývat	k5eAaImIp3nP
i	i	k9
její	její	k3xOp3gFnPc4
povinnosti	povinnost	k1gFnPc4
<g/>
:	:	kIx,
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
koordinaci	koordinace	k1gFnSc4
veškerých	veškerý	k3xTgFnPc2
činností	činnost	k1gFnPc2
organizace	organizace	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nezasedá	zasedat	k5eNaImIp3nS
ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
generální	generální	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
ministerské	ministerský	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
každý	každý	k3xTgInSc4
členský	členský	k2eAgInSc4d1
stát	stát	k1gInSc4
svého	svůj	k3xOyFgMnSc2
zástupce	zástupce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schází	scházet	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
však	však	k9
jednou	jeden	k4xCgFnSc7
měsíčně	měsíčně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Generální	generální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
se	se	k3xPyFc4
střetává	střetávat	k5eAaImIp3nS
na	na	k7c6
dvou	dva	k4xCgFnPc6
různých	různý	k2eAgFnPc6d1
platformách	platforma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
Orgán	orgán	k1gInSc1
pro	pro	k7c4
prověrky	prověrka	k1gFnPc4
obchodních	obchodní	k2eAgFnPc2d1
praktik	praktika	k1gFnPc2
(	(	kIx(
<g/>
Trade	Trad	k1gInSc5
Policy	Polica	k1gFnSc2
Review	Review	k1gFnSc7
Body	bod	k1gInPc4
<g/>
,	,	kIx,
TPRB	TPRB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhou	druhý	k4xOgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Těleso	těleso	k1gNnSc1
pro	pro	k7c4
řešení	řešení	k1gNnSc4
sporů	spor	k1gInPc2
(	(	kIx(
<g/>
Dispute	Dispute	k?
Settlement	settlement	k1gInSc4
Body	bod	k1gInPc7
<g/>
,	,	kIx,
DSB	DSB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
řešit	řešit	k5eAaImF
konflikty	konflikt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vznikají	vznikat	k5eAaImIp3nP
mezi	mezi	k7c7
členy	člen	k1gMnPc7
během	během	k7c2
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
formace	formace	k1gFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
monitorováním	monitorování	k1gNnSc7
a	a	k8xC
řešením	řešení	k1gNnSc7
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
orgány	orgán	k1gInPc1
generální	generální	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
působí	působit	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
pro	pro	k7c4
obchod	obchod	k1gInSc4
se	s	k7c7
zbožím	zboží	k1gNnSc7
(	(	kIx(
<g/>
Council	Council	k1gInSc1
for	forum	k1gNnPc2
Trade	Trad	k1gInSc5
in	in	k?
Goods	Goods	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
obchodních	obchodní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
výbor	výbor	k1gInSc1
pro	pro	k7c4
zemědělství	zemědělství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
tuto	tento	k3xDgFnSc4
radu	rada	k1gFnSc4
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
Information	Information	k1gInSc1
Technology	technolog	k1gMnPc4
Agreement	Agreement	k1gInSc4
Committee	Committee	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rada	rada	k1gFnSc1
pro	pro	k7c4
obchod	obchod	k1gInSc4
se	s	k7c7
službami	služba	k1gFnPc7
(	(	kIx(
<g/>
Council	Council	k1gInSc1
for	forum	k1gNnPc2
Trade	Trad	k1gInSc5
in	in	k?
Services	Services	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
transparentností	transparentnost	k1gFnSc7
terciárního	terciární	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
ekonomik	ekonomika	k1gFnPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
dohody	dohoda	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
pravidla	pravidlo	k1gNnPc4
určující	určující	k2eAgInSc4d1
obchod	obchod	k1gInSc4
se	s	k7c7
službami	služba	k1gFnPc7
<g/>
:	:	kIx,
pohyb	pohyb	k1gInSc1
osob	osoba	k1gFnPc2
obchodující	obchodující	k2eAgFnSc1d1
se	s	k7c7
službami	služba	k1gFnPc7
<g/>
,	,	kIx,
finanční	finanční	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
telekomunikace	telekomunikace	k1gFnPc4
a	a	k8xC
leteckou	letecký	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rada	rada	k1gFnSc1
pro	pro	k7c4
obchodní	obchodní	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
práv	právo	k1gNnPc2
k	k	k7c3
duševnímu	duševní	k2eAgNnSc3d1
vlastnictví	vlastnictví	k1gNnSc3
(	(	kIx(
<g/>
Council	Councila	k1gFnPc2
for	forum	k1gNnPc2
Trade-Related	Trade-Related	k1gMnSc1
Aspects	Aspectsa	k1gFnPc2
of	of	k?
Intellectual	Intellectual	k1gMnSc1
Property	Propert	k1gMnPc4
Rights	Rights	k1gInSc4
<g/>
,	,	kIx,
TRIPS	TRIPS	kA
<g/>
)	)	kIx)
monitoruje	monitorovat	k5eAaImIp3nS
dohody	dohoda	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
copyrightu	copyright	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Generální	generální	k2eAgFnSc6d1
radě	rada	k1gFnSc6
působí	působit	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
výborů	výbor	k1gInPc2
a	a	k8xC
rad	rada	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
obchodem	obchod	k1gInSc7
<g/>
,	,	kIx,
financemi	finance	k1gFnPc7
a	a	k8xC
transportem	transport	k1gInSc7
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
zde	zde	k6eAd1
také	také	k9
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nejsou	být	k5eNaImIp3nP
přítomni	přítomen	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
všech	všecek	k3xTgInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgMnPc1d3
exportéři	exportér	k1gMnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
WTO	WTO	kA
</s>
<s>
Green	Green	k2eAgInSc1d1
Room	Room	k1gInSc1
</s>
<s>
Mechanismus	mechanismus	k1gInSc4
tzv.	tzv.	kA
Zeleného	Zeleného	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
(	(	kIx(
<g/>
Green	Green	k2eAgInSc1d1
Room	Room	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
podstatě	podstata	k1gFnSc6
nahrazuje	nahrazovat	k5eAaImIp3nS
výkonnou	výkonný	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
by	by	kYmCp3nS
zasedal	zasedat	k5eAaImAgInS
omezený	omezený	k2eAgInSc1d1
počet	počet	k1gInSc1
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
zelený	zelený	k2eAgInSc1d1
pokoj	pokoj	k1gInSc1
objevil	objevit	k5eAaPmAgInS
během	během	k7c2
Tokijského	tokijský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
kuloárního	kuloární	k2eAgNnSc2d1
jednání	jednání	k1gNnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
zeleného	zelený	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
zúčastnilo	zúčastnit	k5eAaPmAgNnS
10	#num#	k4
států	stát	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
počet	počet	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
až	až	k9
na	na	k7c4
30	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
OECD	OECD	kA
a	a	k8xC
pár	pár	k4xCyI
rozvojových	rozvojový	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgMnS
ale	ale	k8xC
také	také	k6eAd1
několikrát	několikrát	k6eAd1
kritizován	kritizovat	k5eAaImNgMnS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
umožnil	umožnit	k5eAaPmAgInS
hlavním	hlavní	k2eAgMnPc3d1
aktérům	aktér	k1gMnPc3
mezinárodního	mezinárodní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
ovlivňovat	ovlivňovat	k5eAaImF
průběh	průběh	k1gInSc4
vyjednávání	vyjednávání	k1gNnSc2
v	v	k7c4
jejich	jejich	k3xOp3gInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
platforma	platforma	k1gFnSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
jen	jen	k9
sporadicky	sporadicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodovací	rozhodovací	k2eAgInSc1d1
proces	proces	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
kontroverzním	kontroverzní	k2eAgInSc7d1
zejména	zejména	k9
v	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
z	z	k7c2
vyjednávání	vyjednávání	k1gNnSc2
”	”	k?
<g/>
vyloučeny	vyloučit	k5eAaPmNgFnP
<g/>
”	”	k?
rozvojové	rozvojový	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Katarského	katarský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
proces	proces	k1gInSc1
vyjednávání	vyjednávání	k1gNnSc2
daleko	daleko	k6eAd1
transparentnější	transparentní	k2eAgFnPc1d2
a	a	k8xC
inkluzivní	inkluzivní	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řešení	řešení	k1gNnSc1
sporů	spor	k1gInPc2
</s>
<s>
Dispute	Dispute	k?
Settlement	settlement	k1gInSc1
Body	bod	k1gInPc4
</s>
<s>
WTO	WTO	kA
disponuje	disponovat	k5eAaBmIp3nS
tzv.	tzv.	kA
Tělesem	těleso	k1gNnSc7
pro	pro	k7c4
řešení	řešení	k1gNnSc4
sporů	spor	k1gInPc2
(	(	kIx(
<g/>
Dispute	Dispute	k?
Settlement	settlement	k1gInSc4
Body	bod	k1gInPc7
<g/>
,	,	kIx,
DSB	DSB	kA
<g/>
)	)	kIx)
a	a	k8xC
Mechanismem	mechanismus	k1gInSc7
pro	pro	k7c4
řešení	řešení	k1gNnSc4
sporů	spor	k1gInPc2
(	(	kIx(
<g/>
Dispute	Dispute	k?
Settlement	settlement	k1gInSc1
Mechanism	Mechanism	k1gInSc1
<g/>
,	,	kIx,
DSM	DSM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
využívány	využívat	k5eAaPmNgInP,k5eAaImNgInP
<g/>
,	,	kIx,
pokud	pokud	k8xS
mezi	mezi	k7c7
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
rozporu	rozpor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1
pro	pro	k7c4
řešení	řešení	k1gNnSc4
sporů	spor	k1gInPc2
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
až	až	k9
4	#num#	k4
kroky	krok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
krokem	krok	k1gInSc7
jsou	být	k5eAaImIp3nP
konzultace	konzultace	k1gFnPc1
<g/>
,	,	kIx,
druhým	druhý	k4xOgNnSc7
je	on	k3xPp3gMnPc4
panel	panel	k1gInSc4
<g/>
,	,	kIx,
třetím	třetí	k4xOgMnSc7
je	být	k5eAaImIp3nS
případné	případný	k2eAgNnSc1d1
odvolání	odvolání	k1gNnSc1
a	a	k8xC
čtvrtým	čtvrtá	k1gFnPc3
je	být	k5eAaImIp3nS
realizace	realizace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dispute	Dispute	k?
Settlement	settlement	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
v	v	k7c6
rámci	rámec	k1gInSc6
Uruguayského	uruguayský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c4
platnost	platnost	k1gFnSc4
vstoupil	vstoupit	k5eAaPmAgMnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
a	a	k8xC
dává	dávat	k5eAaImIp3nS
každému	každý	k3xTgMnSc3
z	z	k7c2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
WTO	WTO	kA
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
domáhat	domáhat	k5eAaImF
svých	svůj	k3xOyFgNnPc2
práv	právo	k1gNnPc2
vyplývajících	vyplývající	k2eAgNnPc2d1
z	z	k7c2
dohod	dohoda	k1gFnPc2
WTO	WTO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
žalujícího	žalující	k2eAgInSc2d1
státu	stát	k1gInSc2
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
očekává	očekávat	k5eAaImIp3nS
iniciace	iniciace	k1gFnSc1
bilaterální	bilaterální	k2eAgFnSc2d1
konzultace	konzultace	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
lhůta	lhůta	k1gFnSc1
zpravidla	zpravidla	k6eAd1
60	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uplynutí	uplynutí	k1gNnSc6
této	tento	k3xDgFnSc2
lhůty	lhůta	k1gFnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
svolat	svolat	k5eAaPmF
panel	panel	k1gInSc4
na	na	k7c6
základě	základ	k1gInSc6
tzv.	tzv.	kA
principu	princip	k1gInSc2
negativního	negativní	k2eAgInSc2d1
konsenzu	konsenz	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
pokud	pokud	k8xS
nehlasují	hlasovat	k5eNaImIp3nP
členové	člen	k1gMnPc1
DSB	DSB	kA
proti	proti	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
panelu	panel	k1gInSc6
nezasedají	zasedat	k5eNaImIp3nP
zástupci	zástupce	k1gMnPc1
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
3	#num#	k4
až	až	k9
5	#num#	k4
odborníků	odborník	k1gMnPc2
z	z	k7c2
oblasti	oblast	k1gFnSc2
mezinárodního	mezinárodní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
diplomacie	diplomacie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Panel	panel	k1gInSc1
po	po	k7c6
řádném	řádný	k2eAgNnSc6d1
prozkoumání	prozkoumání	k1gNnSc6
a	a	k8xC
konzultací	konzultace	k1gFnPc2
se	s	k7c7
zúčastněnými	zúčastněný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
zprávu	zpráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
zprávy	zpráva	k1gFnSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
používá	používat	k5eAaImIp3nS
princip	princip	k1gInSc1
negativního	negativní	k2eAgInSc2d1
konsenzu	konsenz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strany	strana	k1gFnPc1
mohou	moct	k5eAaImIp3nP
podat	podat	k5eAaPmF
proti	proti	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
panelu	panel	k1gInSc2
odvolání	odvolání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
podrobněji	podrobně	k6eAd2
zabývají	zabývat	k5eAaImIp3nP
odborníci	odborník	k1gMnPc1
z	z	k7c2
Odvolacího	odvolací	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
(	(	kIx(
<g/>
Appellate	Appellat	k1gInSc5
Body	bod	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvolací	odvolací	k2eAgInSc4d1
těleso	těleso	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
7	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
voleni	volit	k5eAaImNgMnP
na	na	k7c4
4	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mechanismus	mechanismus	k1gInSc1
pro	pro	k7c4
řešení	řešení	k1gNnSc4
sporů	spor	k1gInPc2
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
již	již	k6eAd1
Všeobecné	všeobecný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zde	zde	k6eAd1
projednáno	projednat	k5eAaPmNgNnS
297	#num#	k4
žádostí	žádost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vzniku	vznik	k1gInSc2
WTO	WTO	kA
do	do	k7c2
současnosti	současnost	k1gFnSc2
projednal	projednat	k5eAaPmAgInS
DSB	DSB	kA
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
žádostí	žádost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3
důvodem	důvod	k1gInSc7
sporů	spor	k1gInPc2
je	být	k5eAaImIp3nS
porušení	porušení	k1gNnSc1
norem	norma	k1gFnPc2
doložek	doložka	k1gFnPc2
nejvyšších	vysoký	k2eAgFnPc2d3
výhod	výhoda	k1gFnPc2
a	a	k8xC
národního	národní	k2eAgNnSc2d1
zacházení	zacházení	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
oblast	oblast	k1gFnSc1
dočasných	dočasný	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
překážek	překážka	k1gFnPc2
(	(	kIx(
<g/>
antidumping	antidumping	k1gInSc1
a	a	k8xC
subvence	subvence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řešením	řešení	k1gNnSc7
sporů	spor	k1gInPc2
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
účastní	účastnit	k5eAaImIp3nS
pouze	pouze	k6eAd1
malé	malý	k2eAgNnSc1d1
procento	procento	k1gNnSc1
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
vystupují	vystupovat	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ať	ať	k8xC,k8xS
už	už	k6eAd1
jako	jako	k9
žalující	žalující	k2eAgFnSc1d1
nebo	nebo	k8xC
žalovaná	žalovaný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děje	děj	k1gInSc2
se	se	k3xPyFc4
tak	tak	k9
hlavně	hlavně	k9
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gNnSc3
silnému	silný	k2eAgNnSc3d1
postavení	postavení	k1gNnSc3
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
časté	častý	k2eAgMnPc4d1
žalované	žalovaný	k1gMnPc4
země	zem	k1gFnSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
i	i	k9
Brazílie	Brazílie	k1gFnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
přes	přes	k7c4
její	její	k3xOp3gNnSc4
relativně	relativně	k6eAd1
krátké	krátký	k2eAgNnSc4d1
členství	členství	k1gNnSc4
(	(	kIx(
<g/>
připojila	připojit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
skupiny	skupina	k1gFnPc1
kritizovaly	kritizovat	k5eAaImAgFnP
proces	proces	k1gInSc4
řešení	řešení	k1gNnSc2
sporů	spor	k1gInPc2
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
transparentnosti	transparentnost	k1gFnSc2
a	a	k8xC
demokratické	demokratický	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
pro	pro	k7c4
necitlivost	necitlivost	k1gFnSc4
k	k	k7c3
sociálním	sociální	k2eAgFnPc3d1
normám	norma	k1gFnPc3
a	a	k8xC
normám	norma	k1gFnPc3
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostoucí	rostoucí	k2eAgInSc1d1
využívání	využívání	k1gNnSc4
systému	systém	k1gInSc2
rozvojovými	rozvojový	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
je	být	k5eAaImIp3nS
ukazatelem	ukazatel	k1gInSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
institucionálního	institucionální	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
řešení	řešení	k1gNnSc2
sporů	spor	k1gInPc2
je	být	k5eAaImIp3nS
významným	významný	k2eAgInSc7d1
milníkem	milník	k1gInSc7
ve	v	k7c6
vývoji	vývoj	k1gInSc6
založeného	založený	k2eAgInSc2d1
na	na	k7c6
pravidlech	pravidlo	k1gNnPc6
mnohostranného	mnohostranný	k2eAgInSc2d1
obchodního	obchodní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
organizace	organizace	k1gFnSc2
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
každého	každý	k3xTgNnSc2
vyjednávání	vyjednávání	k1gNnSc2
podají	podat	k5eAaPmIp3nP
členové	člen	k1gMnPc1
WTO	WTO	kA
návrhy	návrh	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
jak	jak	k6eAd1
struktury	struktura	k1gFnPc1
<g/>
,	,	kIx,
tak	tak	k9
obsahu	obsah	k1gInSc2
vyjednávání	vyjednávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vyjednávání	vyjednávání	k1gNnPc1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
zájmů	zájem	k1gInPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
členů	člen	k1gInPc2
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
jednotlivých	jednotlivý	k2eAgFnPc2d1
zájmových	zájmový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Článek	článek	k1gInSc1
IX	IX	kA
Dohody	dohoda	k1gFnSc2
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
všechna	všechen	k3xTgNnPc1
důležitá	důležitý	k2eAgNnPc1d1
usnesení	usnesení	k1gNnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
přijata	přijmout	k5eAaPmNgFnS
na	na	k7c6
základě	základ	k1gInSc6
konsenzu	konsenz	k1gInSc2
všech	všecek	k3xTgFnPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
zaručit	zaručit	k5eAaPmF
všeobecnou	všeobecný	k2eAgFnSc4d1
shodu	shoda	k1gFnSc4
na	na	k7c6
přijímaných	přijímaný	k2eAgNnPc6d1
rozhodnutích	rozhodnutí	k1gNnPc6
všemi	všecek	k3xTgMnPc7
zúčastněnými	zúčastněný	k2eAgMnPc7d1
<g/>
,	,	kIx,
takže	takže	k8xS
i	i	k9
zájmy	zájem	k1gInPc1
méně	málo	k6eAd2
rozvinutých	rozvinutý	k2eAgInPc2d1
států	stát	k1gInPc2
jsou	být	k5eAaImIp3nP
ochráněny	ochráněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
dojít	dojít	k5eAaPmF
ke	k	k7c3
konsenzu	konsenz	k1gInSc3
mezi	mezi	k7c7
více	hodně	k6eAd2
než	než	k8xS
160	#num#	k4
členy	člen	k1gMnPc7
organizace	organizace	k1gFnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
kol	kolo	k1gNnPc2
jednání	jednání	k1gNnSc2
a	a	k8xC
času	čas	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
všeobecné	všeobecný	k2eAgFnSc2d1
shody	shoda	k1gFnSc2
vůbec	vůbec	k9
nepodaří	podařit	k5eNaPmIp3nS
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dokazuje	dokazovat	k5eAaImIp3nS
Katarské	katarský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
vyjednávání	vyjednávání	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
nebylo	být	k5eNaImAgNnS
doposud	doposud	k6eAd1
ukončeno	ukončit	k5eAaPmNgNnS
a	a	k8xC
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
bodů	bod	k1gInPc2
agendy	agenda	k1gFnSc2
stále	stále	k6eAd1
nebyla	být	k5eNaImAgFnS
vyřešena	vyřešit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Často	často	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
stává	stávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
alespoň	alespoň	k9
jedna	jeden	k4xCgFnSc1
členská	členský	k2eAgFnSc1d1
země	země	k1gFnSc1
má	mít	k5eAaImIp3nS
k	k	k7c3
návrhu	návrh	k1gInSc3
výhrady	výhrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takových	takový	k3xDgFnPc6
situacích	situace	k1gFnPc6
přichází	přicházet	k5eAaImIp3nS
na	na	k7c4
řadu	řada	k1gFnSc4
zdlouhavá	zdlouhavý	k2eAgFnSc1d1
úsilí	úsilí	k1gNnSc4
k	k	k7c3
dosažení	dosažení	k1gNnSc3
konsenzu	konsenz	k1gInSc2
většinou	většinou	k6eAd1
nalezením	nalezení	k1gNnSc7
kompromisu	kompromis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
nefunguje	fungovat	k5eNaImIp3nS
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
vydáno	vydat	k5eAaPmNgNnS
žádné	žádný	k3yNgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
určitých	určitý	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
členové	člen	k1gMnPc1
nejsou	být	k5eNaImIp3nP
schopni	schopen	k2eAgMnPc1d1
dosáhnout	dosáhnout	k5eAaPmF
konsenzu	konsenz	k1gInSc3
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
WTO	WTO	kA
hlasování	hlasování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
má	mít	k5eAaImIp3nS
každá	každý	k3xTgFnSc1
země	země	k1gFnSc1
jeden	jeden	k4xCgInSc1
hlas	hlas	k1gInSc1
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
váhou	váha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
dostane	dostat	k5eAaPmIp3nS
více	hodně	k6eAd2
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
vyhrává	vyhrávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
WTO	WTO	kA
byla	být	k5eAaImAgFnS
mnohokrát	mnohokrát	k6eAd1
kritizována	kritizovat	k5eAaImNgFnS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jednání	jednání	k1gNnPc1
nejsou	být	k5eNaImIp3nP
spravedlivá	spravedlivý	k2eAgNnPc1d1
a	a	k8xC
dostatečně	dostatečně	k6eAd1
transparentní	transparentní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
totiž	totiž	k9
nejsou	být	k5eNaImIp3nP
WTO	WTO	kA
dohody	dohoda	k1gFnPc4
schvalovány	schvalován	k2eAgFnPc4d1
konsenzem	konsenz	k1gInSc7
všech	všecek	k3xTgMnPc2
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
procesem	proces	k1gInSc7
neformálních	formální	k2eNgNnPc2d1
uzavřených	uzavřený	k2eAgNnPc2d1
jednání	jednání	k1gNnPc2
v	v	k7c6
menších	malý	k2eAgFnPc6d2
skupinkách	skupinka	k1gFnPc6
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
dokazuje	dokazovat	k5eAaImIp3nS
existence	existence	k1gFnSc1
tzv.	tzv.	kA
zeleného	zelený	k2eAgInSc2d1
pokoje	pokoj	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
během	během	k7c2
tajných	tajný	k2eAgFnPc2d1
schůzek	schůzka	k1gFnPc2
nejvýznamnějších	významný	k2eAgInPc2d3
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
formování	formování	k1gNnSc3
základů	základ	k1gInPc2
dohod	dohoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
má	mít	k5eAaImIp3nS
při	při	k7c6
rozhodování	rozhodování	k1gNnSc6
skupina	skupina	k1gFnSc1
největších	veliký	k2eAgMnPc2d3
exportérů	exportér	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatních	ostatní	k2eAgInPc2d1
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
minimální	minimální	k2eAgFnSc4d1
reálnou	reálný	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
jakkoliv	jakkoliv	k8xS
jednání	jednání	k1gNnSc4
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
rozhodujícím	rozhodující	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
jsou	být	k5eAaImIp3nP
ministerské	ministerský	k2eAgFnPc1d1
konference	konference	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
ministři	ministr	k1gMnPc1
všech	všecek	k3xTgInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
prezentují	prezentovat	k5eAaBmIp3nP
vlastní	vlastní	k2eAgInPc4d1
postoje	postoj	k1gInPc4
k	k	k7c3
právě	právě	k6eAd1
projednávaným	projednávaný	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Ministerské	ministerský	k2eAgFnPc1d1
konference	konference	k1gFnPc1
se	se	k3xPyFc4
podle	podle	k7c2
Marákéšské	Marákéšský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
konají	konat	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
nebylo	být	k5eNaImAgNnS
za	za	k7c2
dvacetileté	dvacetiletý	k2eAgFnSc2d1
existence	existence	k1gFnSc2
organizace	organizace	k1gFnSc2
dodrženo	dodržet	k5eAaPmNgNnS
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ke	k	k7c3
konferenci	konference	k1gFnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1
Ministerská	ministerský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
v	v	k7c6
Nairobi	Nairobi	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
bodem	bod	k1gInSc7
agendy	agenda	k1gFnSc2
byl	být	k5eAaImAgInS
tzv.	tzv.	kA
„	„	k?
<g/>
Nairobi	Nairobi	k1gNnSc2
Package	Packag	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zahrnoval	zahrnovat	k5eAaImAgMnS
dohody	dohoda	k1gFnPc4
o	o	k7c4
zemědělství	zemědělství	k1gNnSc4
a	a	k8xC
otázky	otázka	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	s	k7c7
LDCs	LDCsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Reálně	reálně	k6eAd1
se	se	k3xPyFc4
však	však	k9
organizace	organizace	k1gFnSc1
vzdaluje	vzdalovat	k5eAaImIp3nS
dohodám	dohoda	k1gFnPc3
z	z	k7c2
ministerské	ministerský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Dohá	Dohá	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
organizace	organizace	k1gFnSc2
</s>
<s>
Protesty	protest	k1gInPc1
proti	proti	k7c3
Světové	světový	k2eAgFnSc3d1
obchodní	obchodní	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
během	během	k7c2
ministerské	ministerský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Seattlu	Seattl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
díky	díky	k7c3
vytváření	vytváření	k1gNnSc3
koalic	koalice	k1gFnPc2
daří	dařit	k5eAaImIp3nS
do	do	k7c2
jednání	jednání	k1gNnSc2
zapojovat	zapojovat	k5eAaImF
více	hodně	k6eAd2
členů	člen	k1gInPc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
aktéři	aktér	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
výrazně	výrazně	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
jednání	jednání	k1gNnSc4
na	na	k7c4
úkor	úkor	k1gInSc4
jiných	jiný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
a	a	k8xC
Evropskou	evropský	k2eAgFnSc4d1
unii	unie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
v	v	k7c6
současnosti	současnost	k1gFnSc6
vzrostl	vzrůst	k5eAaPmAgInS
význam	význam	k1gInSc4
Indie	Indie	k1gFnSc2
a	a	k8xC
Brazílie	Brazílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
střední	střední	k2eAgFnSc7d1
výstupovou	výstupový	k2eAgFnSc7d1
účelností	účelnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjednávací	vyjednávací	k2eAgFnSc1d1
kola	kola	k1gFnSc1
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
sice	sice	k8xC
vedly	vést	k5eAaImAgFnP
k	k	k7c3
zásadní	zásadní	k2eAgFnSc3d1
liberalizaci	liberalizace	k1gFnSc3
obchodu	obchod	k1gInSc2
–	–	k?
snížení	snížení	k1gNnSc2
tarifů	tarif	k1gInPc2
na	na	k7c4
průmyslové	průmyslový	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
stále	stále	k6eAd1
víc	hodně	k6eAd2
a	a	k8xC
víc	hodně	k6eAd2
se	se	k3xPyFc4
řeší	řešit	k5eAaImIp3nS
otázka	otázka	k1gFnSc1
neúčelnosti	neúčelnost	k1gFnSc2
WTO	WTO	kA
–	–	k?
už	už	k9
patnáct	patnáct	k4xCc4
let	léto	k1gNnPc2
trvající	trvající	k2eAgNnSc1d1
katarské	katarský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
nepřineslo	přinést	k5eNaPmAgNnS
nic	nic	k3yNnSc1
nového	nový	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
WTO	WTO	kA
je	být	k5eAaImIp3nS
také	také	k6eAd1
kritizována	kritizován	k2eAgFnSc1d1
zástupci	zástupce	k1gMnSc3
ekologie	ekologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
volný	volný	k2eAgInSc1d1
trh	trh	k1gInSc1
a	a	k8xC
deregulace	deregulace	k1gFnSc1
má	mít	k5eAaImIp3nS
nepříznivý	příznivý	k2eNgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
jako	jako	k8xC,k8xS
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
zastánců	zastánce	k1gMnPc2
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
termín	termín	k1gInSc1
úspěch	úspěch	k1gInSc4
v	v	k7c6
interpretaci	interpretace	k1gFnSc6
WTO	WTO	kA
není	být	k5eNaImIp3nS
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
růst	růst	k1gInSc1
HDP	HDP	kA
a	a	k8xC
příjmů	příjem	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
automaticky	automaticky	k6eAd1
způsobuje	způsobovat	k5eAaImIp3nS
lhostejnost	lhostejnost	k1gFnSc1
k	k	k7c3
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výsledku	výsledek	k1gInSc6
ochrana	ochrana	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
v	v	k7c6
rozporu	rozpor	k1gInSc6
se	s	k7c7
zvýšením	zvýšení	k1gNnSc7
efektivnosti	efektivnost	k1gFnSc2
v	v	k7c6
ekonomickém	ekonomický	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
bylo	být	k5eAaImAgNnS
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
<g/>
,	,	kIx,
respektive	respektive	k9
signatářem	signatář	k1gMnSc7
Všeobecné	všeobecný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
o	o	k7c6
clech	clo	k1gNnPc6
a	a	k8xC
obchodu	obchod	k1gInSc2
(	(	kIx(
<g/>
GATT	GATT	kA
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
GATT	GATT	kA
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
neopustilo	opustit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
byla	být	k5eAaImAgFnS
suspenze	suspenze	k1gFnSc1
smluvních	smluvní	k2eAgMnPc2d1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
Československem	Československo	k1gNnSc7
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1993	#num#	k4
Česká	český	k2eAgFnSc1d1
a	a	k8xC
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
podepsaly	podepsat	k5eAaPmAgInP
protokoly	protokol	k1gInPc1
o	o	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
WTO	WTO	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vycházely	vycházet	k5eAaImAgFnP
z	z	k7c2
práv	právo	k1gNnPc2
a	a	k8xC
závazků	závazek	k1gInPc2
Československa	Československo	k1gNnSc2
jako	jako	k8xC,k8xS
zakládající	zakládající	k2eAgFnSc2d1
smluvní	smluvní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
GATT	GATT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
samostatný	samostatný	k2eAgInSc1d1
subjekt	subjekt	k1gInSc1
zastupující	zastupující	k2eAgFnSc2d1
všech	všecek	k3xTgInPc2
27	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
prostřednictvím	prostřednictvím	k7c2
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
ji	on	k3xPp3gFnSc4
reprezentuje	reprezentovat	k5eAaImIp3nS
na	na	k7c6
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
respektive	respektive	k9
2005	#num#	k4
tedy	tedy	k9
i	i	k9
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
jakožto	jakožto	k8xS
jednoho	jeden	k4xCgInSc2
z	z	k7c2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
WTO	WTO	kA
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
Okonjová-Iwealová	Okonjová-Iwealový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působila	působit	k5eAaImAgFnS
ve	v	k7c6
Světové	světový	k2eAgFnSc6d1
bance	banka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Nigérii	Nigérie	k1gFnSc6
bojovala	bojovat	k5eAaImAgFnS
proti	proti	k7c3
korupci	korupce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
new	new	k?
DG	dg	kA
Ngozi	Ngoze	k1gFnSc4
Okonjo-Iweala	Okonjo-Iweala	k1gFnSc1
<g/>
:	:	kIx,
Nigerian	Nigerian	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organisation	Organisation	k1gInSc1
Director	Director	k1gMnSc1
General	General	k1gMnSc1
fans	fans	k6eAd1
dress	dress	k6eAd1
up	up	k?
like	like	k1gNnSc7
her	hra	k1gFnPc2
to	ten	k3xDgNnSc1
pay	pay	k?
tribute	tribut	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
NEWS	NEWS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
legal	legal	k1gInSc1
texts	texts	k1gInSc1
-	-	kIx~
Marrakesh	Marrakesh	k1gMnSc1
agreement	agreement	k1gMnSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
legal	legal	k1gInSc1
texts	texts	k1gInSc1
-	-	kIx~
A	a	k9
Summary	Summara	k1gFnSc2
of	of	k?
the	the	k?
Final	Final	k1gMnSc1
Act	Act	k1gMnSc1
of	of	k?
the	the	k?
Uruguay	Uruguay	k1gFnSc1
Round	round	k1gInSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
234	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MATSUSHITA	MATSUSHITA	kA
<g/>
,	,	kIx,
Mitsuo	Mitsuo	k1gMnSc1
<g/>
;	;	kIx,
SCHOENBAUM	SCHOENBAUM	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
J.	J.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
practice	practice	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
policy	polica	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
9780199571857	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
260	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
The	The	k1gMnSc2
Doha	Doha	k1gFnSc1
Round	round	k1gInSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ELIASON	ELIASON	kA
<g/>
,	,	kIx,
Antonia	Antonio	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Trade	Trad	k1gInSc5
Facilitation	Facilitation	k1gInSc4
Agreement	Agreement	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
New	New	k1gFnSc1
Hope	Hope	k1gNnSc2
for	forum	k1gNnPc2
the	the	k?
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Review	Review	k1gMnSc6
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
643	#num#	k4
<g/>
–	–	k?
<g/>
670	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1474	#num#	k4
<g/>
-	-	kIx~
<g/>
7456	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
1474745615000191	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
-	-	kIx~
Ninth	Ninth	k1gMnSc1
WTO	WTO	kA
Ministerial	Ministerial	k1gMnSc1
Conference	Conferenec	k1gMnSc2
-	-	kIx~
Briefing	briefing	k1gInSc1
notes	notes	k1gInSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
practice	practice	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
policy	polica	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780199571857	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
923583927	#num#	k4
S.	S.	kA
229	#num#	k4
<g/>
-	-	kIx~
<g/>
231	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Secretariat	Secretariat	k1gInSc1
and	and	k?
budget	budget	k1gInSc1
overview	overview	k?
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Secretariat	Secretariat	k1gInSc1
and	and	k?
budget	budget	k1gInSc1
overview	overview	k?
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Secretariat	Secretariat	k1gInSc1
and	and	k?
budget	budget	k1gInSc1
overview	overview	k?
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
227	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
231	#num#	k4
<g/>
-	-	kIx~
<g/>
232	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FERGUSSON	FERGUSSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
F.	F.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
-	-	kIx~
Hong	Honga	k1gFnPc2
Kong	Kongo	k1gNnPc2
6	#num#	k4
<g/>
th	th	k?
Ministerial	Ministerial	k1gMnSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
-	-	kIx~
Ninth	Ninth	k1gInSc1
WTO	WTO	kA
Ministerial	Ministerial	k1gInSc1
Conference	Conference	k1gFnSc1
-	-	kIx~
Indonesia	Indonesia	k1gFnSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
-	-	kIx~
Tenth	Tenth	k1gMnSc1
WTO	WTO	kA
Ministerial	Ministerial	k1gMnSc1
Conference	Conferenec	k1gMnSc2
-	-	kIx~
Nairobi	Nairobi	k1gNnSc2
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praaha	Praaha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
227	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
251	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
254	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FERGUSSON	FERGUSSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
F.	F.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FERGUSSON	FERGUSSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
F.	F.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FERGUSSSON	FERGUSSSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
F.	F.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FERGUSSON	FERGUSSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
F.	F.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
228	#num#	k4
<g/>
-	-	kIx~
<g/>
229	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
26	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOEKMAN	HOEKMAN	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
MAVROIDIS	MAVROIDIS	kA
<g/>
,	,	kIx,
Petros	Petrosa	k1gFnPc2
C.	C.	kA
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
:	:	kIx,
law	law	k?
<g/>
,	,	kIx,
economics	economics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
politics	politics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
203946537	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
116	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NARLIKAR	NARLIKAR	kA
<g/>
,	,	kIx,
Amrita	Amritum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Oxford	Oxford	k1gInSc4
handbook	handbook	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780199586103	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Understanding	Understanding	k1gInSc1
the	the	k?
WTO	WTO	kA
-	-	kIx~
A	A	kA
unique	unique	k1gInSc1
contribution	contribution	k1gInSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NARLIKAR	NARLIKAR	kA
<g/>
,	,	kIx,
Amrita	Amritum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Oxford	Oxford	k1gInSc4
handbook	handbook	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780199586103	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dispute	Dispute	k?
Settlement	settlement	k1gInSc4
Summary	Summara	k1gFnSc2
<g/>
.	.	kIx.
www.cid.harvard.edu	www.cid.harvard.edu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NARLIKAR	NARLIKAR	kA
<g/>
,	,	kIx,
Amrita	Amritum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc1
:	:	kIx,
a	a	k8xC
very	vera	k1gFnSc2
short	shorta	k1gFnPc2
introduction	introduction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
192806084	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
REGE	REGE	kA
<g/>
,	,	kIx,
Vinod	Vinod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negotiating	Negotiating	k1gInSc1
at	at	k?
the	the	k?
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Commonwealth	Commonwealth	k1gInSc1
Secretariat	Secretariat	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1848590717	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
27	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NARLIKAR	NARLIKAR	kA
<g/>
,	,	kIx,
Amrita	Amritum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc1
:	:	kIx,
a	a	k8xC
very	vera	k1gFnSc2
short	shorta	k1gFnPc2
introduction	introduction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
192806084	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
REGE	REGE	kA
<g/>
,	,	kIx,
Vinod	Vinod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negotiating	Negotiating	k1gInSc1
at	at	k?
the	the	k?
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organization	Organization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Commonwealth	Commonwealth	k1gInSc1
Secretariat	Secretariat	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1848590717	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
Ministerial	Ministerial	k1gMnSc1
conferences	conferences	k1gMnSc1
-	-	kIx~
Tenth	Tenth	k1gMnSc1
WTO	WTO	kA
Ministerial	Ministerial	k1gMnSc1
Conference	Conferenec	k1gMnSc2
-	-	kIx~
Nairobi	Nairobi	k1gNnSc2
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WTO	WTO	kA
Ministerial	Ministerial	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
Time	Time	k1gFnSc3
for	forum	k1gNnPc2
Reflection	Reflection	k1gInSc1
in	in	k?
Nairobi	Nairobi	k1gNnSc3
on	on	k3xPp3gMnSc1
the	the	k?
Future	Futur	k1gMnSc5
of	of	k?
Global	globat	k5eAaImAgInS
Trade	Trad	k1gMnSc5
|	|	kIx~
International	International	k1gMnSc6
Centre	centr	k1gInSc5
for	forum	k1gNnPc2
Trade	Trad	k1gInSc5
and	and	k?
Sustainable	Sustainable	k1gMnSc1
Development	Development	k1gMnSc1
<g/>
.	.	kIx.
www.ictsd.org	www.ictsd.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788074191794	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NEUMAYER	NEUMAYER	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
WTO	WTO	kA
and	and	k?
the	the	k?
Environment	Environment	k1gMnSc1
<g/>
:	:	kIx,
Its	Its	k1gMnSc1
Past	pasta	k1gFnPc2
Record	Record	k1gMnSc1
is	is	k?
Better	Better	k1gMnSc1
than	than	k1gMnSc1
Critics	Critics	k1gInSc4
Believe	Belieev	k1gFnSc2
<g/>
,	,	kIx,
but	but	k?
the	the	k?
Future	Futur	k1gMnSc5
Outlook	Outlook	k1gInSc4
is	is	k?
Bleak	Bleak	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KARLAS	KARLAS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
:	:	kIx,
systémy	systém	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologické	sociologický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8021024747	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
112	#num#	k4
<g/>
-	-	kIx~
<g/>
113	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WTO	WTO	kA
|	|	kIx~
European	European	k1gInSc1
Union	union	k1gInSc1
-	-	kIx~
Member	Member	k1gInSc1
information	information	k1gInSc1
<g/>
.	.	kIx.
www.wto.org	www.wto.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Světová	světový	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.wto.org	http://www.wto.org	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711437	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2145784-0	2145784-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2296	#num#	k4
2735	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
94018277	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500292980	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
153400694	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
94018277	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
