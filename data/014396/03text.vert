<s>
DORIS	DORIS	kA
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Doris	Doris	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Inframaják	Inframaják	k1gInSc1
u	u	k7c2
vjezdu	vjezd	k1gInSc2
do	do	k7c2
vozovny	vozovna	k1gFnSc2
Žižkov	Žižkov	k1gInSc1
</s>
<s>
Typický	typický	k2eAgInSc1d1
inframaják	inframaják	k1gInSc1
skrytý	skrytý	k2eAgInSc1d1
v	v	k7c6
hlavě	hlava	k1gFnSc6
zastávkového	zastávkový	k2eAgInSc2d1
sloupku	sloupek	k1gInSc2
(	(	kIx(
<g/>
zastávka	zastávka	k1gFnSc1
Anděl	Anděla	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Přijímač	přijímač	k1gInSc1
signálu	signál	k1gInSc2
na	na	k7c6
střeše	střecha	k1gFnSc6
tramvaje	tramvaj	k1gFnSc2
</s>
<s>
DORIS	DORIS	kA
(	(	kIx(
<g/>
akronym	akronym	k1gInSc1
s	s	k7c7
významem	význam	k1gInSc7
dopravní	dopravní	k2eAgInSc1d1
řídicí	řídicí	k2eAgInSc1d1
a	a	k8xC
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
název	název	k1gInSc4
telematického	telematický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
sleduje	sledovat	k5eAaImIp3nS
provoz	provoz	k1gInSc4
tramvají	tramvaj	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
založený	založený	k2eAgInSc1d1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
modul	modul	k1gInSc1
umístěný	umístěný	k2eAgInSc1d1
na	na	k7c6
vozidle	vozidlo	k1gNnSc6
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
bodech	bod	k1gInPc6
na	na	k7c6
trati	trať	k1gFnSc6
s	s	k7c7
pomocí	pomoc	k1gFnSc7
inframajáků	inframaják	k1gInPc2
umístěných	umístěný	k2eAgInPc2d1
na	na	k7c6
některých	některý	k3yIgInPc6
zastávkových	zastávkový	k2eAgInPc6d1
sloupcích	sloupec	k1gInPc6
<g/>
,	,	kIx,
výjezdech	výjezd	k1gInPc6
z	z	k7c2
vozoven	vozovna	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
dalších	další	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
identifikuje	identifikovat	k5eAaBmIp3nS
polohu	poloha	k1gFnSc4
vlaku	vlak	k1gInSc2
a	a	k8xC
odešle	odeslat	k5eAaPmIp3nS
informaci	informace	k1gFnSc4
vozidlovou	vozidlový	k2eAgFnSc7d1
radiostanicí	radiostanice	k1gFnSc7
do	do	k7c2
informační	informační	k2eAgFnSc2d1
centrály	centrála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
provozním	provozní	k2eAgInSc6d1
dispečinku	dispečink	k1gInSc6
tramvají	tramvaj	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
výpravnách	výpravna	k1gFnPc6
vozoven	vozovna	k1gFnPc2
<g/>
,	,	kIx,
střídacích	střídací	k2eAgInPc6d1
bodech	bod	k1gInPc6
a	a	k8xC
na	na	k7c6
dalších	další	k2eAgNnPc6d1
místech	místo	k1gNnPc6
tak	tak	k9
dispečeři	dispečer	k1gMnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
mají	mít	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
průběžně	průběžně	k6eAd1
sledovat	sledovat	k5eAaImF
polohu	poloha	k1gFnSc4
všech	všecek	k3xTgFnPc2
tramvají	tramvaj	k1gFnPc2
v	v	k7c6
síti	síť	k1gFnSc6
a	a	k8xC
u	u	k7c2
vlaků	vlak	k1gInPc2
určených	určený	k2eAgInPc2d1
jízdním	jízdní	k2eAgInSc7d1
řádem	řád	k1gInSc7
i	i	k8xC
odchylku	odchylka	k1gFnSc4
od	od	k7c2
jízdního	jízdní	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
systém	systém	k1gInSc1
tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
a	a	k8xC
propojuje	propojovat	k5eAaImIp3nS
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
činnostmi	činnost	k1gFnPc7
dispečinku	dispečink	k1gInSc2
a	a	k8xC
telematickými	telematický	k2eAgInPc7d1
výstupy	výstup	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
začleněna	začlenit	k5eAaPmNgFnS
také	také	k9
radiová	radiový	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
řidičů	řidič	k1gMnPc2
s	s	k7c7
dispečinkem	dispečink	k1gInSc7
a	a	k8xC
seřizování	seřizování	k1gNnSc4
a	a	k8xC
zobrazování	zobrazování	k1gNnSc4
jednotného	jednotný	k2eAgInSc2d1
času	čas	k1gInSc2
ve	v	k7c6
vozidlech	vozidlo	k1gNnPc6
(	(	kIx(
<g/>
na	na	k7c6
zobrazovači	zobrazovač	k1gInSc6
času	čas	k1gInSc2
i	i	k9
na	na	k7c6
označovacích	označovací	k2eAgInPc6d1
strojcích	strojek	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Inframajáky	Inframaják	k1gInPc1
a	a	k8xC
kontrolní	kontrolní	k2eAgInPc1d1
body	bod	k1gInPc1
</s>
<s>
Inframajáky	Inframaják	k1gInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
zejména	zejména	k9
v	v	k7c6
těch	ten	k3xDgFnPc6
zastávkách	zastávka	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
řidiči	řidič	k1gMnPc1
mají	mít	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
ve	v	k7c6
vlakových	vlakový	k2eAgInPc6d1
jízdních	jízdní	k2eAgInPc6d1
řádech	řád	k1gInPc6
–	–	k?
takzvaných	takzvaný	k2eAgInPc6d1
kontrolních	kontrolní	k2eAgInPc6d1
bodech	bod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
zejména	zejména	k9
o	o	k7c6
konečné	konečná	k1gFnSc6
zastávky	zastávka	k1gFnSc2
<g/>
,	,	kIx,
zastávky	zastávka	k1gFnSc2
u	u	k7c2
významných	významný	k2eAgFnPc2d1
křižovatek	křižovatka	k1gFnPc2
nebo	nebo	k8xC
stanic	stanice	k1gFnPc2
metra	metro	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
o	o	k7c4
místa	místo	k1gNnPc4
vjezdu	vjezd	k1gInSc2
a	a	k8xC
výjezdu	výjezd	k1gInSc2
vozoven	vozovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
místa	místo	k1gNnPc4
jsou	být	k5eAaImIp3nP
volena	volit	k5eAaImNgFnS
s	s	k7c7
různou	různý	k2eAgFnSc7d1
vzdáleností	vzdálenost	k1gFnSc7
<g/>
,	,	kIx,
nejběžněji	běžně	k6eAd3
po	po	k7c4
5	#num#	k4
až	až	k9
10	#num#	k4
minutách	minuta	k1gFnPc6
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
přijímá	přijímat	k5eAaImIp3nS
informace	informace	k1gFnSc1
o	o	k7c6
všech	všecek	k3xTgInPc6
průjezdech	průjezd	k1gInPc6
kolem	kolem	k7c2
čidel	čidlo	k1gNnPc2
(	(	kIx(
<g/>
resp.	resp.	kA
majáků	maják	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
i	i	k9
o	o	k7c6
těch	ten	k3xDgInPc6
průjezdech	průjezd	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
příslušná	příslušný	k2eAgFnSc1d1
linka	linka	k1gFnSc1
nemá	mít	k5eNaImIp3nS
u	u	k7c2
majáku	maják	k1gInSc2
<g/>
,	,	kIx,
kolem	kolem	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
projíždí	projíždět	k5eAaImIp3nP
<g/>
,	,	kIx,
stanovený	stanovený	k2eAgInSc4d1
kontrolní	kontrolní	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
na	na	k7c6
trati	trať	k1gFnSc6
Hlubočepy	Hlubočepa	k1gFnSc2
–	–	k?
Barrandov	Barrandov	k1gInSc1
jsou	být	k5eAaImIp3nP
čidla	čidlo	k1gNnPc1
nejen	nejen	k6eAd1
v	v	k7c6
každé	každý	k3xTgFnSc6
zastávce	zastávka	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
dokonce	dokonce	k9
i	i	k9
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
mezi	mezi	k7c7
zastávkami	zastávka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zastávce	zastávka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
není	být	k5eNaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
inframajákem	inframaják	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odeslání	odeslání	k1gNnSc1
informace	informace	k1gFnSc2
iniciováno	iniciovat	k5eAaBmNgNnS
vyhlášením	vyhlášení	k1gNnSc7
názvu	název	k1gInSc2
zastávky	zastávka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
bylo	být	k5eAaImAgNnS
instalováno	instalovat	k5eAaBmNgNnS
182	#num#	k4
inframajáků	inframaják	k1gMnPc2
a	a	k8xC
předpokládal	předpokládat	k5eAaImAgInS
se	se	k3xPyFc4
konečný	konečný	k2eAgInSc1d1
počet	počet	k1gInSc1
maximálně	maximálně	k6eAd1
250	#num#	k4
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
tramvajové	tramvajový	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přenos	přenos	k1gInSc1
informací	informace	k1gFnPc2
</s>
<s>
Radiostanice	radiostanice	k1gFnSc1
a	a	k8xC
palubní	palubní	k2eAgInSc1d1
počítač	počítač	k1gInSc1
tramvajového	tramvajový	k2eAgInSc2d1
vlaku	vlak	k1gInSc2
jsou	být	k5eAaImIp3nP
automaticky	automaticky	k6eAd1
aktivovány	aktivovat	k5eAaBmNgInP
zapnutím	zapnutí	k1gNnSc7
řízení	řízení	k1gNnSc2
vlaku	vlak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
DORIS	DORIS	kA
v	v	k7c6
minulosti	minulost	k1gFnSc6
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
dva	dva	k4xCgInPc4
simplexní	simplexní	k2eAgInPc4d1
datové	datový	k2eAgInPc4d1
radiokanály	radiokanál	k1gInPc4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
simplexní	simplexní	k2eAgInPc1d1
zvukové	zvukový	k2eAgInPc1d1
radiokanály	radiokanál	k1gInPc1
a	a	k8xC
jeden	jeden	k4xCgInSc1
radiokanál	radiokanál	k1gInSc1
pro	pro	k7c4
dispečerský	dispečerský	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
DORIS	DORIS	kA
<g/>
2005	#num#	k4
<g/>
T	T	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
pro	pro	k7c4
přenos	přenos	k1gInSc4
fonické	fonický	k2eAgFnSc2d1
a	a	k8xC
datové	datový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
mezi	mezi	k7c7
provozním	provozní	k2eAgInSc7d1
dispečinkem	dispečink	k1gInSc7
tramvají	tramvaj	k1gFnPc2
a	a	k8xC
každým	každý	k3xTgInSc7
tramvajovým	tramvajový	k2eAgInSc7d1
vlakem	vlak	k1gInSc7
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
městská	městský	k2eAgFnSc1d1
radiová	radiový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
TETRA	tetra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pokrytí	pokrytí	k1gNnSc3
města	město	k1gNnSc2
radiovým	radiový	k2eAgInSc7d1
signálem	signál	k1gInSc7
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
systém	systém	k1gInSc1
22	#num#	k4
stacionárních	stacionární	k2eAgFnPc2d1
základnových	základnový	k2eAgFnPc2d1
radiostanic	radiostanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c6
monitoru	monitor	k1gInSc6
počítače	počítač	k1gInSc2
</s>
<s>
Ve	v	k7c6
výstupu	výstup	k1gInSc6
systému	systém	k1gInSc2
na	na	k7c6
monitoru	monitor	k1gInSc6
počítače	počítač	k1gInSc2
na	na	k7c6
dispečinku	dispečink	k1gInSc6
lze	lze	k6eAd1
údaje	údaj	k1gInPc4
zobrazovat	zobrazovat	k5eAaImF
ve	v	k7c6
formě	forma	k1gFnSc6
schematické	schematický	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
i	i	k8xC
navzájem	navzájem	k6eAd1
propojených	propojený	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
s	s	k7c7
údaji	údaj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barevně	barevně	k6eAd1
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
znázorněn	znázornit	k5eAaPmNgInS
okamžitý	okamžitý	k2eAgInSc1d1
vztah	vztah	k1gInSc1
každého	každý	k3xTgInSc2
vlaku	vlak	k1gInSc2
k	k	k7c3
jízdnímu	jízdní	k2eAgInSc3d1
řádu	řád	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeleně	zeleně	k6eAd1
se	se	k3xPyFc4
zobrazují	zobrazovat	k5eAaImIp3nP
spoje	spoj	k1gInPc1
jedoucí	jedoucí	k2eAgInPc1d1
ve	v	k7c6
stanovené	stanovený	k2eAgFnSc6d1
toleranci	tolerance	k1gFnSc6
přesnosti	přesnost	k1gFnSc2
(	(	kIx(
<g/>
t.	t.	k?
j.	j.	k?
u	u	k7c2
posledního	poslední	k2eAgNnSc2d1
čidla	čidlo	k1gNnSc2
přesně	přesně	k6eAd1
nebo	nebo	k8xC
se	s	k7c7
zpožděním	zpoždění	k1gNnSc7
nejvýše	nejvýše	k6eAd1,k6eAd3
3	#num#	k4
minuty	minuta	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenou	červený	k2eAgFnSc7d1
ikonou	ikona	k1gFnSc7
se	se	k3xPyFc4
zobrazují	zobrazovat	k5eAaImIp3nP
nadjeté	nadjetý	k2eAgInPc1d1
spoje	spoj	k1gInPc1
(	(	kIx(
<g/>
spoje	spoj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
kolem	kolem	k7c2
posledního	poslední	k2eAgNnSc2d1
čidla	čidlo	k1gNnSc2
projely	projet	k5eAaPmAgFnP
před	před	k7c7
stanoveným	stanovený	k2eAgInSc7d1
časem	čas	k1gInSc7
odjezdu	odjezd	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žlutá	žlutý	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
zpoždění	zpoždění	k1gNnSc4
mezi	mezi	k7c7
3	#num#	k4
a	a	k8xC
5	#num#	k4
minutami	minuta	k1gFnPc7
<g/>
,	,	kIx,
blikající	blikající	k2eAgFnSc1d1
žlutá	žlutý	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
zpoždění	zpoždění	k1gNnSc1
5	#num#	k4
minut	minuta	k1gFnPc2
nebo	nebo	k8xC
vyšší	vysoký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bíle	bíle	k6eAd1
jsou	být	k5eAaImIp3nP
znázorněny	znázorněn	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
zpoždění	zpoždění	k1gNnSc4
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
doba	doba	k1gFnSc1
obratu	obrat	k1gInSc2
na	na	k7c6
následující	následující	k2eAgFnSc6d1
konečné	konečná	k1gFnSc6
(	(	kIx(
<g/>
tj.	tj.	kA
tyto	tento	k3xDgInPc4
vlaky	vlak	k1gInPc4
nemohou	moct	k5eNaImIp3nP
zpoždění	zpožděný	k2eAgMnPc1d1
na	na	k7c6
konečné	konečná	k1gFnSc6
dohnat	dohnat	k5eAaPmF
a	a	k8xC
vyjedou	vyjet	k5eAaPmIp3nP
z	z	k7c2
nástupní	nástupní	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
opožděně	opožděně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrá	modrý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
přísluší	příslušet	k5eAaImIp3nS
vlakům	vlak	k1gInPc3
neurčeným	určený	k2eNgInSc7d1
jízdním	jízdní	k2eAgInSc7d1
řádem	řád	k1gInSc7
(	(	kIx(
<g/>
cvičné	cvičný	k2eAgFnSc2d1
<g/>
,	,	kIx,
služební	služební	k2eAgFnSc2d1
a	a	k8xC
zkušební	zkušební	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fialová	fialový	k2eAgFnSc1d1
barva	barva	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
poruchu	porucha	k1gFnSc4
palubního	palubní	k2eAgInSc2d1
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
níž	jenž	k3xRgFnSc3
nelze	lze	k6eNd1
identifikovat	identifikovat	k5eAaBmF
polohu	poloha	k1gFnSc4
vlaku	vlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
výstup	výstup	k1gInSc1
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
v	v	k7c6
některých	některý	k3yIgFnPc6
vozovnách	vozovna	k1gFnPc6
i	i	k8xC
řidičům	řidič	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
tak	tak	k9
před	před	k7c7
střídáním	střídání	k1gNnSc7
mohou	moct	k5eAaImIp3nP
ověřit	ověřit	k5eAaPmF
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
trati	trať	k1gFnSc6
vlak	vlak	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
mají	mít	k5eAaImIp3nP
nastoupit	nastoupit	k5eAaPmF
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
propojený	propojený	k2eAgInSc1d1
s	s	k7c7
vybavením	vybavení	k1gNnSc7
dispečinku	dispečink	k1gInSc2
pro	pro	k7c4
radiofonní	radiofonní	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
řidiči	řidič	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tedy	tedy	k9
některý	některý	k3yIgMnSc1
z	z	k7c2
řidičů	řidič	k1gMnPc2
radiostanicí	radiostanice	k1gFnSc7
kontaktuje	kontaktovat	k5eAaImIp3nS
dispečink	dispečink	k1gInSc1
<g/>
,	,	kIx,
dispečerovi	dispečer	k1gMnSc6
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
na	na	k7c6
monitoru	monitor	k1gInSc6
zobrazí	zobrazit	k5eAaPmIp3nS
dostupné	dostupný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
vozidle	vozidlo	k1gNnSc6
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgNnSc2
řidič	řidič	k1gInSc1
volá	volat	k5eAaImIp3nS
(	(	kIx(
<g/>
typ	typ	k1gInSc1
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
evidenční	evidenční	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
vozovna	vozovna	k1gFnSc1
<g/>
,	,	kIx,
údaje	údaj	k1gInPc1
o	o	k7c6
řidiči	řidič	k1gInSc6
<g/>
,	,	kIx,
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyhodnocování	vyhodnocování	k1gNnSc1
údajů	údaj	k1gInPc2
</s>
<s>
Pokud	pokud	k8xS
systém	systém	k1gInSc1
signalizuje	signalizovat	k5eAaImIp3nS
nepravidelnost	nepravidelnost	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
příčina	příčina	k1gFnSc1
není	být	k5eNaImIp3nS
dispečinku	dispečink	k1gInSc2
známa	známo	k1gNnSc2
<g/>
,	,	kIx,
dispečer	dispečer	k1gMnSc1
se	se	k3xPyFc4
radiostanicí	radiostanice	k1gFnSc7
dotáže	dotázat	k5eAaPmIp3nS
řidiče	řidič	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoreticky	teoreticky	k6eAd1
podle	podle	k7c2
provozního	provozní	k2eAgInSc2d1
předpisu	předpis	k1gInSc2
jsou	být	k5eAaImIp3nP
řidiči	řidič	k1gMnPc1
povinni	povinen	k2eAgMnPc1d1
větší	veliký	k2eAgFnSc3d2
nepravidelnosti	nepravidelnost	k1gFnSc3
hlásit	hlásit	k5eAaImF
sami	sám	k3xTgMnPc1
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
zpoždění	zpoždění	k1gNnPc2
způsobených	způsobený	k2eAgInPc2d1
běžným	běžný	k2eAgNnSc7d1
zahlcením	zahlcení	k1gNnSc7
komunikací	komunikace	k1gFnPc2
však	však	k9
s	s	k7c7
důsledným	důsledný	k2eAgNnSc7d1
dodržováním	dodržování	k1gNnSc7
této	tento	k3xDgFnSc2
povinnosti	povinnost	k1gFnSc2
nelze	lze	k6eNd1
počítat	počítat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dispečer	dispečer	k1gMnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k6eAd1
upozorněn	upozornit	k5eAaPmNgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
některý	některý	k3yIgInSc1
vlak	vlak	k1gInSc1
na	na	k7c6
trase	trasa	k1gFnSc6
30	#num#	k4
minut	minuta	k1gFnPc2
nezaslal	zaslat	k5eNaPmAgMnS
informaci	informace	k1gFnSc4
o	o	k7c6
své	svůj	k3xOyFgFnSc6
poloze	poloha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokud	pokud	k8xS
z	z	k7c2
kontrolního	kontrolní	k2eAgInSc2d1
bodu	bod	k1gInSc2
odjede	odjet	k5eAaPmIp3nS
tramvaj	tramvaj	k1gFnSc1
před	před	k7c7
stanoveným	stanovený	k2eAgInSc7d1
časem	čas	k1gInSc7
<g/>
,	,	kIx,
systém	systém	k1gInSc1
automaticky	automaticky	k6eAd1
zapracuje	zapracovat	k5eAaPmIp3nS
událost	událost	k1gFnSc1
do	do	k7c2
zprávy	zpráva	k1gFnSc2
pro	pro	k7c4
nadřízené	nadřízený	k1gMnPc4
příslušného	příslušný	k2eAgInSc2d1
řidiče	řidič	k1gInSc2
(	(	kIx(
<g/>
pro	pro	k7c4
vedoucího	vedoucí	k1gMnSc4
provozu	provoz	k1gInSc2
příslušné	příslušný	k2eAgFnSc2d1
vozovny	vozovna	k1gFnSc2
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
podklad	podklad	k1gInSc4
k	k	k7c3
finančnímu	finanční	k2eAgInSc3d1
postihu	postih	k1gInSc3
za	za	k7c4
nedodržení	nedodržení	k1gNnSc4
jízdního	jízdní	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedodržení	nedodržení	k1gNnSc3
jízdního	jízdní	k2eAgInSc2d1
řádu	řád	k1gInSc2
u	u	k7c2
majáků	maják	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
linka	linka	k1gFnSc1
nemá	mít	k5eNaImIp3nS
kontrolní	kontrolní	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
zastávkách	zastávka	k1gFnPc6
bez	bez	k7c2
majáku	maják	k1gInSc2
takto	takto	k6eAd1
postihováno	postihován	k2eAgNnSc4d1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
ve	v	k7c6
vozovně	vozovna	k1gFnSc6
Strašnice	Strašnice	k1gFnPc1
bylo	být	k5eAaImAgNnS
první	první	k4xOgNnSc1
nadjetí	nadjetí	k1gNnSc1
v	v	k7c6
měsíci	měsíc	k1gInSc6
za	za	k7c4
napomenutí	napomenutí	k1gNnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
za	za	k7c4
druhé	druhý	k4xOgMnPc4
bylo	být	k5eAaImAgNnS
řidiči	řidič	k1gMnSc3
strženo	stržen	k2eAgNnSc1d1
50	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
za	za	k7c2
třetí	třetí	k4xOgFnSc2
100	#num#	k4
Kč	Kč	kA
a	a	k8xC
za	za	k7c4
každé	každý	k3xTgNnSc4
další	další	k1gNnSc4
o	o	k7c4
50	#num#	k4
Kč	Kč	kA
více	hodně	k6eAd2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
<g/>
-li	-li	k?
první	první	k4xOgNnSc4
zjištěné	zjištěný	k2eAgNnSc4d1
nadjetí	nadjetí	k1gNnSc4
jednu	jeden	k4xCgFnSc4
minutu	minuta	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
za	za	k7c4
něj	on	k3xPp3gMnSc4
strženo	stržen	k2eAgNnSc1d1
již	již	k6eAd1
50	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Do	do	k7c2
konce	konec	k1gInSc2
června	červen	k1gInSc2
2006	#num#	k4
bylo	být	k5eAaImAgNnS
řidičům	řidič	k1gMnPc3
tolerováno	tolerován	k2eAgNnSc4d1
nadjetí	nadjetí	k1gNnSc4
do	do	k7c2
20	#num#	k4
sekund	sekunda	k1gFnPc2
jako	jako	k8xS,k8xC
korekce	korekce	k1gFnSc2
možné	možný	k2eAgFnSc2d1
chyby	chyba	k1gFnSc2
zaměření	zaměření	k1gNnSc2
polohy	poloha	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
během	během	k7c2
stání	stání	k1gNnSc2
v	v	k7c6
zastávce	zastávka	k1gFnSc6
(	(	kIx(
<g/>
pokud	pokud	k8xS
tramvaj	tramvaj	k1gFnSc1
při	při	k7c6
zastavování	zastavování	k1gNnSc6
přejede	přejet	k5eAaPmIp3nS
čelem	čelo	k1gNnSc7
sloupek	sloupek	k1gInSc1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
zaregistruje	zaregistrovat	k5eAaPmIp3nS
příjezd	příjezd	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
odjezd	odjezd	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
i	i	k9
různé	různý	k2eAgInPc4d1
průběžné	průběžný	k2eAgInPc4d1
a	a	k8xC
souhrnné	souhrnný	k2eAgInPc4d1
statistické	statistický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
jako	jako	k8xS,k8xC
objednatel	objednatel	k1gMnSc1
dopravy	doprava	k1gFnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
85	#num#	k4
%	%	kIx~
procent	procento	k1gNnPc2
spojů	spoj	k1gInPc2
(	(	kIx(
<g/>
resp.	resp.	kA
vozidel	vozidlo	k1gNnPc2
v	v	k7c6
okamžitém	okamžitý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
)	)	kIx)
v	v	k7c6
toleranci	tolerance	k1gFnSc6
přesnosti	přesnost	k1gFnSc2
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
požadavek	požadavek	k1gInSc1
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
naplňován	naplňován	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soulad	soulad	k1gInSc1
pozice	pozice	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
s	s	k7c7
jízdním	jízdní	k2eAgInSc7d1
řádem	řád	k1gInSc7
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
i	i	k9
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
palubním	palubní	k2eAgInSc7d1
počítačem	počítač	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
na	na	k7c6
displeji	displej	k1gInSc6
v	v	k7c6
kabině	kabina	k1gFnSc6
řidiče	řidič	k1gMnPc4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
i	i	k9
akusticky	akusticky	k6eAd1
informuje	informovat	k5eAaBmIp3nS
řidiče	řidič	k1gMnSc4
o	o	k7c6
případném	případný	k2eAgNnSc6d1
zpoždění	zpoždění	k1gNnSc6
nebo	nebo	k8xC
nadjetí	nadjetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
po	po	k7c6
průjezdu	průjezd	k1gInSc6
kolem	kolem	k7c2
inframajáku	inframaják	k1gInSc2
zároveň	zároveň	k6eAd1
kontroluje	kontrolovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
hlášení	hlášení	k1gNnSc1
zastávek	zastávka	k1gFnPc2
pro	pro	k7c4
cestující	cestující	k1gMnPc4
ve	v	k7c6
vozidle	vozidlo	k1gNnSc6
odpovídá	odpovídat	k5eAaImIp3nS
skutečné	skutečný	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
nesrovnalostí	nesrovnalost	k1gFnPc2
je	být	k5eAaImIp3nS
hlásič	hlásič	k1gInSc1
automaticky	automaticky	k6eAd1
korigován	korigovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
také	také	k6eAd1
upozorňuje	upozorňovat	k5eAaImIp3nS
řidiče	řidič	k1gMnPc4
před	před	k7c7
časem	čas	k1gInSc7
pravidelného	pravidelný	k2eAgInSc2d1
odjezdu	odjezd	k1gInSc2
z	z	k7c2
konečné	konečný	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
vyčkávacího	vyčkávací	k2eAgInSc2d1
přestupního	přestupní	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
pokynu	pokyn	k1gInSc2
dispečera	dispečer	k1gMnSc2
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
schopen	schopen	k2eAgInSc1d1
vydat	vydat	k5eAaPmF
pokyn	pokyn	k1gInSc4
k	k	k7c3
odjezdu	odjezd	k1gInSc3
všem	všecek	k3xTgInPc3
vlakům	vlak	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
v	v	k7c4
danou	daný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
grafikonem	grafikon	k1gInSc7
hromadný	hromadný	k2eAgInSc4d1
odjezd	odjezd	k1gInSc4
z	z	k7c2
určeného	určený	k2eAgInSc2d1
bodu	bod	k1gInSc2
–	–	k?
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
nočním	noční	k2eAgInSc6d1
provozu	provoz	k1gInSc6
při	při	k7c6
řízení	řízení	k1gNnSc6
odjezdu	odjezd	k1gInSc2
linek	linka	k1gFnPc2
z	z	k7c2
centrálního	centrální	k2eAgInSc2d1
přestupního	přestupní	k2eAgInSc2d1
bodu	bod	k1gInSc2
Lazarská	Lazarský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Archivovaná	archivovaný	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
využívána	využívat	k5eAaImNgNnP,k5eAaPmNgNnP
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
dalších	další	k2eAgNnPc2d1
hodnocení	hodnocení	k1gNnPc2
a	a	k8xC
výkazů	výkaz	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
nepravidelností	nepravidelnost	k1gFnSc7
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
úspěšnosti	úspěšnost	k1gFnSc2
radiového	radiový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
optimalizace	optimalizace	k1gFnSc1
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
a	a	k8xC
plánování	plánování	k1gNnSc4
dopravy	doprava	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
preference	preference	k1gFnSc1
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Výstupy	výstup	k1gInPc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
</s>
<s>
Prototypový	prototypový	k2eAgInSc1d1
zobrazovač	zobrazovač	k1gInSc1
odjezdů	odjezd	k1gInPc2
s	s	k7c7
aktualizací	aktualizace	k1gFnSc7
dle	dle	k7c2
okamžité	okamžitý	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
tramvají	tramvaj	k1gFnPc2
v	v	k7c6
zastávce	zastávka	k1gFnSc6
Malostranská	malostranský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Novější	nový	k2eAgFnSc1d2
LCD	LCD	kA
verze	verze	k1gFnSc1
výstupu	výstup	k1gInSc2
ve	v	k7c6
stanici	stanice	k1gFnSc6
Hradčanská	hradčanský	k2eAgFnSc1d1
</s>
<s>
Údaje	údaj	k1gInPc1
ze	z	k7c2
systému	systém	k1gInSc2
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
v	v	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
i	i	k8xC
elektronické	elektronický	k2eAgInPc1d1
zobrazovače	zobrazovač	k1gInPc1
zastávkového	zastávkový	k2eAgInSc2d1
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
zkušebně	zkušebně	k6eAd1
Malostranská	malostranský	k2eAgFnSc1d1
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc4
zastávky	zastávka	k1gFnPc4
na	na	k7c6
nové	nový	k2eAgFnSc6d1
barrandovské	barrandovský	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tabule	tabule	k1gFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
údaje	údaj	k1gInPc4
o	o	k7c6
pravidelném	pravidelný	k2eAgInSc6d1
odjezdu	odjezd	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c4
zpoždění	zpoždění	k1gNnSc4
nebo	nebo	k8xC
nadjetí	nadjetí	k1gNnSc4
ani	ani	k8xC
konkrétní	konkrétní	k2eAgFnSc4d1
identifikaci	identifikace	k1gFnSc4
spojů	spoj	k1gInPc2
například	například	k6eAd1
pořadovým	pořadový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
spoje	spoj	k1gFnSc2
nebo	nebo	k8xC
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
počet	počet	k1gInSc1
minut	minuta	k1gFnPc2
do	do	k7c2
předpokládaného	předpokládaný	k2eAgInSc2d1
skutečného	skutečný	k2eAgInSc2d1
odjezdu	odjezd	k1gInSc2
spojů	spoj	k1gInPc2
příslušných	příslušný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
reakce	reakce	k1gFnSc1
zobrazovače	zobrazovač	k1gInSc2
na	na	k7c4
průjezd	průjezd	k1gInSc4
vlaku	vlak	k1gInSc2
není	být	k5eNaImIp3nS
okamžitá	okamžitý	k2eAgFnSc1d1
a	a	k8xC
čidla	čidlo	k1gNnPc4
nejsou	být	k5eNaImIp3nP
na	na	k7c6
tratích	trať	k1gFnPc6
umístěna	umístit	k5eAaPmNgFnS
příliš	příliš	k6eAd1
hustě	hustě	k6eAd1
<g/>
,	,	kIx,
zobrazené	zobrazený	k2eAgInPc4d1
údaje	údaj	k1gInPc4
mnohdy	mnohdy	k6eAd1
nejsou	být	k5eNaImIp3nP
příliš	příliš	k6eAd1
přesné	přesný	k2eAgFnPc1d1
a	a	k8xC
někdy	někdy	k6eAd1
jsou	být	k5eAaImIp3nP
i	i	k9
matoucí	matoucí	k2eAgMnSc1d1
(	(	kIx(
<g/>
údaj	údaj	k1gInSc1
se	se	k3xPyFc4
skokově	skokově	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
udává	udávat	k5eAaImIp3nS
odjezd	odjezd	k1gInSc1
„	„	k?
<g/>
za	za	k7c4
1	#num#	k4
minutu	minut	k2eAgFnSc4d1
<g/>
“	“	k?
u	u	k7c2
spoje	spoj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
již	již	k6eAd1
ze	z	k7c2
zastávky	zastávka	k1gFnSc2
odjel	odjet	k5eAaPmAgInS
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1
zastávkové	zastávkový	k2eAgFnPc1d1
informační	informační	k2eAgFnPc1d1
tabule	tabule	k1gFnPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
čerpající	čerpající	k2eAgInPc1d1
údaje	údaj	k1gInPc1
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
od	od	k7c2
května	květen	k1gInSc2
2004	#num#	k4
v	v	k7c6
Jesenici	Jesenice	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
této	tento	k3xDgFnSc6
obci	obec	k1gFnSc6
sídlí	sídlet	k5eAaImIp3nS
firma	firma	k1gFnSc1
APEX	apex	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
telematické	telematický	k2eAgInPc4d1
systémy	systém	k1gInPc4
vyvíjí	vyvíjet	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Strančicích	Strančice	k1gFnPc6
a	a	k8xC
Českém	český	k2eAgInSc6d1
Brodě	Brod	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jesenici	Jesenice	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Pražské	pražský	k2eAgFnSc2d1
integrované	integrovaný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
poprvé	poprvé	k6eAd1
použita	použit	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
poskytující	poskytující	k2eAgFnSc1d1
integrovaně	integrovaně	k6eAd1
informace	informace	k1gFnPc4
o	o	k7c6
autobusových	autobusový	k2eAgFnPc6d1
i	i	k8xC
vlakových	vlakový	k2eAgFnPc6d1
spojích	spoj	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vizuální	vizuální	k2eAgInSc4d1
zobrazení	zobrazení	k1gNnSc1
je	být	k5eAaImIp3nS
doplněno	doplnit	k5eAaPmNgNnS
možností	možnost	k1gFnSc7
akustického	akustický	k2eAgNnSc2d1
hlášení	hlášení	k1gNnSc2
pro	pro	k7c4
nevidomé	nevidomý	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
začal	začít	k5eAaPmAgInS
zkušební	zkušební	k2eAgInSc1d1
provoz	provoz	k1gInSc1
podávání	podávání	k1gNnSc2
informací	informace	k1gFnPc2
o	o	k7c6
poloze	poloha	k1gFnSc6
tramvají	tramvaj	k1gFnSc7
SMS	SMS	kA
zprávami	zpráva	k1gFnPc7
na	na	k7c4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
–	–	k?
nejprve	nejprve	k6eAd1
pro	pro	k7c4
služební	služební	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
řidičů	řidič	k1gMnPc2
střídajících	střídající	k2eAgFnPc2d1
kolegy	kolega	k1gMnPc7
na	na	k7c6
trati	trať	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2007	#num#	k4
pilotně	pilotně	k6eAd1
i	i	k9
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
obsahuje	obsahovat	k5eAaImIp3nS
údaj	údaj	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
za	za	k7c4
jak	jak	k8xC,k8xS
dlouho	dlouho	k6eAd1
tramvaj	tramvaj	k1gFnSc1
pojede	jet	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
o	o	k7c6
pravidelném	pravidelný	k2eAgInSc6d1
odjezdu	odjezd	k1gInSc6
ani	ani	k8xC
o	o	k7c6
zpoždění	zpoždění	k1gNnSc6
nebo	nebo	k8xC
nadjetí	nadjetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
výstupů	výstup	k1gInPc2
vyhledávačů	vyhledávač	k1gMnPc2
spojení	spojení	k1gNnSc2
zatím	zatím	k6eAd1
informace	informace	k1gFnPc1
o	o	k7c6
aktuálním	aktuální	k2eAgInSc6d1
stavu	stav	k1gInSc6
provozu	provoz	k1gInSc2
začleněna	začlenit	k5eAaPmNgFnS
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
správa	správa	k1gFnSc1
systému	systém	k1gInSc2
</s>
<s>
Teoreticky	teoreticky	k6eAd1
a	a	k8xC
laboratorně	laboratorně	k6eAd1
byly	být	k5eAaImAgInP
podobné	podobný	k2eAgInPc1d1
systémy	systém	k1gInPc1
vyvíjeny	vyvíjet	k5eAaImNgInP
již	již	k6eAd1
koncem	koncem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1982	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
byl	být	k5eAaImAgInS
ve	v	k7c6
zkušebním	zkušební	k2eAgInSc6d1
provozu	provoz	k1gInSc6
na	na	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
autobusové	autobusový	k2eAgFnSc6d1
lince	linka	k1gFnSc6
144	#num#	k4
(	(	kIx(
<g/>
Staroměstská	staroměstský	k2eAgFnSc1d1
–	–	k?
sídliště	sídliště	k1gNnSc4
Bohnice	Bohnice	k1gInPc4
jih	jih	k1gInSc1
<g/>
)	)	kIx)
systém	systém	k1gInSc1
KŘS	KŘS	kA
(	(	kIx(
<g/>
Kontrolní	kontrolní	k2eAgInSc1d1
řídicí	řídicí	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
ještě	ještě	k9
málo	málo	k6eAd1
spolehlivý	spolehlivý	k2eAgMnSc1d1
a	a	k8xC
potřebné	potřebný	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
drahé	drahý	k2eAgFnPc1d1
a	a	k8xC
obtížně	obtížně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgMnS
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
tramvajové	tramvajový	k2eAgFnSc6d1
lince	linka	k1gFnSc6
18	#num#	k4
(	(	kIx(
<g/>
Vozovna	vozovna	k1gFnSc1
Pankrác	Pankrác	k1gFnSc1
–	–	k?
Petřiny	Petřin	k2eAgFnPc4d1
<g/>
)	)	kIx)
použit	použit	k2eAgInSc4d1
systém	systém	k1gInSc4
ISYDR	ISYDR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
bratislavský	bratislavský	k2eAgInSc4d1
systém	systém	k1gInSc4
DRAHUŠA	DRAHUŠA	kA
upravený	upravený	k2eAgInSc1d1
pro	pro	k7c4
pražské	pražský	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
ISYDR	ISYDR	kA
znamenalo	znamenat	k5eAaImAgNnS
Inovovaný	inovovaný	k2eAgInSc4d1
SYstém	systém	k1gInSc4
DRahuša	DRahuš	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
vyhlásilo	vyhlásit	k5eAaPmAgNnS
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
soutěž	soutěž	k1gFnSc1
na	na	k7c4
dodávku	dodávka	k1gFnSc4
odbavovacích	odbavovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
v	v	k7c6
MHD	MHD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
integrovaného	integrovaný	k2eAgInSc2d1
odbavovacího	odbavovací	k2eAgInSc2d1
a	a	k8xC
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
OIS	OIS	kA
<g/>
)	)	kIx)
ve	v	k7c6
vozidlech	vozidlo	k1gNnPc6
<g/>
,	,	kIx,
vyvíjeného	vyvíjený	k2eAgNnSc2d1
sdružením	sdružení	k1gNnSc7
MYPOL	MYPOL	kA
v.	v.	k?
o.	o.	k?
s.	s.	k?
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
systém	systém	k1gInSc4
ISYDR	ISYDR	kA
převeden	převeden	k2eAgMnSc1d1
z	z	k7c2
analogových	analogový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
na	na	k7c4
digitální	digitální	k2eAgInSc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
změnil	změnit	k5eAaPmAgInS
název	název	k1gInSc1
na	na	k7c4
DORIS	DORIS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
byl	být	k5eAaImAgInS
DORIS	DORIS	kA
uveden	uveden	k2eAgInSc4d1
do	do	k7c2
rutinního	rutinní	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
sledoval	sledovat	k5eAaImAgMnS
DORIS	DORIS	kA
v	v	k7c6
dopravní	dopravní	k2eAgFnSc6d1
špičce	špička	k1gFnSc6
pracovního	pracovní	k2eAgInSc2d1
dne	den	k1gInSc2
všech	všecek	k3xTgInPc2
415	#num#	k4
vlaků	vlak	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
současně	současně	k6eAd1
na	na	k7c6
trasách	trasa	k1gFnPc6
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
tramvajové	tramvajový	k2eAgFnSc6d1
síti	síť	k1gFnSc6
a	a	k8xC
v	v	k7c6
noci	noc	k1gFnSc6
55	#num#	k4
jednovozových	jednovozový	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
špičce	špička	k1gFnSc6
v	v	k7c6
provozu	provoz	k1gInSc6
maximálně	maximálně	k6eAd1
326	#num#	k4
vlaků	vlak	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
noci	noc	k1gFnSc6
60	#num#	k4
jednovozových	jednovozový	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
zaznamenané	zaznamenaný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
dohledat	dohledat	k5eAaPmF
až	až	k6eAd1
6	#num#	k4
let	léto	k1gNnPc2
zpětně	zpětně	k6eAd1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
údajně	údajně	k6eAd1
jsou	být	k5eAaImIp3nP
uložené	uložený	k2eAgInPc4d1
všechny	všechen	k3xTgInPc4
údaje	údaj	k1gInPc4
od	od	k7c2
rutinního	rutinní	k2eAgNnSc2d1
zprovoznění	zprovoznění	k1gNnSc2
systému	systém	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc4
údaje	údaj	k1gInPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
i	i	k9
při	při	k7c6
prověřování	prověřování	k1gNnSc6
stížnosti	stížnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
používal	používat	k5eAaImAgInS
DORIS	DORIS	kA
ve	v	k7c6
verzi	verze	k1gFnSc6
z	z	k7c2
dubna	duben	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
verze	verze	k1gFnSc1
DORIS	DORIS	kA
-	-	kIx~
2005	#num#	k4
T.	T.	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
provoz	provoz	k1gInSc4
systému	systém	k1gInSc2
DORIS	DORIS	kA
(	(	kIx(
<g/>
vedení	vedení	k1gNnSc1
příslušného	příslušný	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
DP	DP	kA
<g/>
)	)	kIx)
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
odpovídá	odpovídat	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgMnSc1d1
řidič	řidič	k1gMnSc1
tramvaje	tramvaj	k1gFnSc2
a	a	k8xC
dispečer	dispečer	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobné	podobný	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
AUDIS	AUDIS	kA
</s>
<s>
V	v	k7c6
síti	síť	k1gFnSc6
autobusových	autobusový	k2eAgFnPc2d1
linek	linka	k1gFnPc2
PID	PID	kA
není	být	k5eNaImIp3nS
zjišťování	zjišťování	k1gNnSc4
polohy	poloha	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
signálu	signál	k1gInSc6
od	od	k7c2
inframajáku	inframaják	k1gInSc2
umístěného	umístěný	k2eAgInSc2d1
na	na	k7c6
zastávce	zastávka	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
systému	systém	k1gInSc6
GPS	GPS	kA
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
polohu	poloha	k1gFnSc4
vozidla	vozidlo	k1gNnSc2
zjistí	zjistit	k5eAaPmIp3nP
s	s	k7c7
přesností	přesnost	k1gFnSc7
20	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
zaveden	zavést	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
na	na	k7c6
lince	linka	k1gFnSc6
177	#num#	k4
(	(	kIx(
<g/>
35	#num#	k4
vozů	vůz	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
následně	následně	k6eAd1
na	na	k7c6
lince	linka	k1gFnSc6
187	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
příslušným	příslušný	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
vybavena	vybavit	k5eAaPmNgFnS
jen	jen	k9
asi	asi	k9
pětina	pětina	k1gFnSc1
autobusů	autobus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
tímto	tento	k3xDgNnSc7
zařízením	zařízení	k1gNnSc7
vybaveny	vybavit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
autobusy	autobus	k1gInPc1
provozované	provozovaný	k2eAgInPc1d1
Dopravní	dopravní	k2eAgFnSc7d1
podnikem	podnik	k1gInSc7
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.s.	a.s.	k?
</s>
<s>
Přenos	přenos	k1gInSc1
krátkých	krátká	k1gFnPc2
datových	datový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
SDS	SDS	kA
<g/>
)	)	kIx)
mezi	mezi	k7c7
vozidlem	vozidlo	k1gNnSc7
a	a	k8xC
centrálním	centrální	k2eAgInSc7d1
počítačem	počítač	k1gInSc7
zajišťuje	zajišťovat	k5eAaImIp3nS
digitální	digitální	k2eAgFnSc1d1
radiová	radiový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
TETRA	tetra	k1gFnSc1
Městského	městský	k2eAgInSc2d1
radiového	radiový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1
obrazovka	obrazovka	k1gFnSc1
„	„	k?
<g/>
perlová	perlový	k2eAgFnSc1d1
šňůra	šňůra	k1gFnSc1
<g/>
“	“	k?
zobrazuje	zobrazovat	k5eAaImIp3nS
polohu	poloha	k1gFnSc4
vozidel	vozidlo	k1gNnPc2
na	na	k7c6
konkrétní	konkrétní	k2eAgFnSc6d1
lince	linka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
stavu	stav	k1gInSc2
vozů	vůz	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
vyhledat	vyhledat	k5eAaPmF
vůz	vůz	k1gInSc4
podle	podle	k7c2
čísla	číslo	k1gNnSc2
linky	linka	k1gFnSc2
a	a	k8xC
pořadového	pořadový	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeleně	zeleně	k6eAd1
se	se	k3xPyFc4
zobrazují	zobrazovat	k5eAaImIp3nP
vozy	vůz	k1gInPc1
jedoucí	jedoucí	k2eAgInPc1d1
přesně	přesně	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
povolené	povolený	k2eAgFnSc6d1
časové	časový	k2eAgFnSc6d1
toleranci	tolerance	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
červeně	červeně	k6eAd1
vozy	vůz	k1gInPc1
nadjeté	nadjetý	k2eAgInPc1d1
(	(	kIx(
<g/>
jedoucí	jedoucí	k2eAgFnPc1d1
předčasně	předčasně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
červeně	červeně	k6eAd1
blikající	blikající	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
nadjeté	nadjetý	k2eAgFnPc1d1
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
žluté	žlutý	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
znamená	znamenat	k5eAaImIp3nS
zpoždění	zpoždění	k1gNnSc4
nad	nad	k7c4
stanovenou	stanovený	k2eAgFnSc4d1
toleranci	tolerance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhém	druhý	k4xOgInSc6
monitoru	monitor	k1gInSc6
se	se	k3xPyFc4
dispečerům	dispečer	k1gMnPc3
zobrazuje	zobrazovat	k5eAaImIp3nS
situace	situace	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Třetí	třetí	k4xOgInSc1
monitor	monitor	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
ke	k	k7c3
zobrazení	zobrazení	k1gNnSc2
dispečerské	dispečerský	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
sítě	síť	k1gFnSc2
TETRA	tetra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
dispečinku	dispečink	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
kromě	kromě	k7c2
komunikace	komunikace	k1gFnSc2
s	s	k7c7
řidičem	řidič	k1gInSc7
centrálně	centrálně	k6eAd1
a	a	k8xC
dálkově	dálkově	k6eAd1
hlásit	hlásit	k5eAaImF
informace	informace	k1gFnPc4
cestujícím	cestující	k1gMnPc3
do	do	k7c2
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
měnit	měnit	k5eAaImF
například	například	k6eAd1
zobrazení	zobrazení	k1gNnSc4
cílové	cílový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
na	na	k7c6
vozidle	vozidlo	k1gNnSc6
atd.	atd.	kA
Systém	systém	k1gInSc1
sleduje	sledovat	k5eAaImIp3nS
případy	případ	k1gInPc1
vozidel	vozidlo	k1gNnPc2
nekomunikujících	komunikující	k2eNgFnPc2d1
se	s	k7c7
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
případy	případ	k1gInPc1
sjetí	sjetí	k1gNnPc2
ze	z	k7c2
stanovené	stanovený	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
nebo	nebo	k8xC
chybného	chybný	k2eAgNnSc2d1
hlášení	hlášení	k1gNnSc2
zastávek	zastávka	k1gFnPc2
atd.	atd.	kA
Na	na	k7c6
propojení	propojení	k1gNnSc6
systému	systém	k1gInSc2
s	s	k7c7
dalšími	další	k2eAgInPc7d1
telematickými	telematický	k2eAgInPc7d1
systémy	systém	k1gInPc7
a	a	k8xC
prvky	prvek	k1gInPc7
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontrola	kontrola	k1gFnSc1
provozu	provoz	k1gInSc2
autobusů	autobus	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
je	být	k5eAaImIp3nS
kombinována	kombinovat	k5eAaImNgFnS
se	s	k7c7
2	#num#	k4
stálými	stálý	k2eAgMnPc7d1
dispečerskými	dispečerský	k2eAgNnPc7d1
stanovišti	stanoviště	k1gNnPc7
(	(	kIx(
<g/>
Na	na	k7c4
Knížecí	knížecí	k2eAgInSc4d1
a	a	k8xC
Černý	černý	k2eAgInSc4d1
most	most	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
mobilními	mobilní	k2eAgFnPc7d1
kontrolami	kontrola	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
bylo	být	k5eAaImAgNnS
alespoň	alespoň	k9
v	v	k7c6
jednom	jeden	k4xCgInSc6
místě	místo	k1gNnSc6
trasy	trasa	k1gFnSc2
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
kolem	kolem	k7c2
50	#num#	k4
%	%	kIx~
spojů	spoj	k1gInPc2
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
a	a	k8xC
kolem	kolem	k7c2
25	#num#	k4
%	%	kIx~
spojů	spoj	k1gInPc2
ve	v	k7c6
volných	volný	k2eAgInPc6d1
dnech	den	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
pražských	pražský	k2eAgInPc6d1
autobusech	autobus	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
tramvají	tramvaj	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
řidičovi	řidičův	k2eAgMnPc1d1
na	na	k7c6
palubním	palubní	k2eAgInSc6d1
počítači	počítač	k1gInSc6
zobrazuje	zobrazovat	k5eAaImIp3nS
plánovaný	plánovaný	k2eAgInSc4d1
čas	čas	k1gInSc4
odjezdu	odjezd	k1gInSc2
pro	pro	k7c4
každou	každý	k3xTgFnSc4
konkrétní	konkrétní	k2eAgFnSc4d1
zastávku	zastávka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodržení	dodržení	k1gNnSc3
jízdního	jízdní	k2eAgInSc2d1
řádu	řád	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
řidičů	řidič	k1gMnPc2
vyžadováno	vyžadován	k2eAgNnSc1d1
pro	pro	k7c4
každou	každý	k3xTgFnSc4
zastávku	zastávka	k1gFnSc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
jen	jen	k9
pro	pro	k7c4
vybrané	vybraný	k2eAgInPc4d1
kontrolní	kontrolní	k2eAgInPc4d1
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
FarWay	FarWaa	k1gFnPc1
</s>
<s>
Monitorován	monitorován	k2eAgInSc1d1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
pohyb	pohyb	k1gInSc1
dispečerských	dispečerský	k2eAgFnPc2d1
a	a	k8xC
některých	některý	k3yIgNnPc2
údržbových	údržbový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
systémem	systém	k1gInSc7
FarWay	FarWa	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
v	v	k7c6
intervalu	interval	k1gInSc6
5	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
sekund	sekunda	k1gFnPc2
sleduje	sledovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zobrazuje	zobrazovat	k5eAaImIp3nS
a	a	k8xC
zaznamenává	zaznamenávat	k5eAaImIp3nS
na	na	k7c6
dispečinku	dispečink	k1gInSc6
nejen	nejen	k6eAd1
polohu	poloha	k1gFnSc4
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
použití	použití	k1gNnSc4
modrého	modrý	k2eAgInSc2d1
či	či	k8xC
oranžového	oranžový	k2eAgInSc2d1
majáku	maják	k1gInSc2
<g/>
,	,	kIx,
okamžitou	okamžitý	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
vozidla	vozidlo	k1gNnSc2
(	(	kIx(
<g/>
se	s	k7c7
zvýrazněním	zvýraznění	k1gNnSc7
překročení	překročení	k1gNnSc2
rychlosti	rychlost	k1gFnSc2
50	#num#	k4
a	a	k8xC
90	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výstupy	výstup	k1gInPc1
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgInPc1d1
i	i	k8xC
u	u	k7c2
garážmistrů	garážmistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledování	sledování	k1gNnSc1
je	být	k5eAaImIp3nS
propojeno	propojit	k5eAaPmNgNnS
s	s	k7c7
integrovaným	integrovaný	k2eAgInSc7d1
záchranným	záchranný	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
dispečeři	dispečer	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
přehled	přehled	k1gInSc4
i	i	k9
o	o	k7c6
pohybu	pohyb	k1gInSc6
mimopodnikových	mimopodnikový	k2eAgFnPc2d1
záchranných	záchranný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
V	v	k7c6
pražském	pražský	k2eAgNnSc6d1
metru	metro	k1gNnSc6
se	se	k3xPyFc4
systém	systém	k1gInSc1
s	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
nazývá	nazývat	k5eAaImIp3nS
Automatický	automatický	k2eAgInSc1d1
systém	systém	k1gInSc1
dispečerského	dispečerský	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
propojený	propojený	k2eAgInSc1d1
se	s	k7c7
zabezpečovacím	zabezpečovací	k2eAgInSc7d1
systémem	systém	k1gInSc7
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
metro	metro	k1gNnSc1
je	být	k5eAaImIp3nS
druhem	druh	k1gInSc7
železniční	železniční	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
podobný	podobný	k2eAgInSc1d1
DORISu	DORISa	k1gFnSc4
a	a	k8xC
AUDISu	AUDISa	k1gFnSc4
je	být	k5eAaImIp3nS
testován	testován	k2eAgInSc1d1
i	i	k9
v	v	k7c6
příměstské	příměstský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
u	u	k7c2
jiných	jiný	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
jeho	jeho	k3xOp3gInPc7
výstupy	výstup	k1gInPc7
patří	patřit	k5eAaImIp3nP
i	i	k9
informace	informace	k1gFnPc1
řidičům	řidič	k1gMnPc3
využitelné	využitelný	k2eAgFnPc1d1
v	v	k7c6
případě	případ	k1gInSc6
zpoždění	zpoždění	k1gNnSc2
spojů	spoj	k1gInPc2
k	k	k7c3
řešení	řešení	k1gNnSc3
přestupních	přestupní	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
(	(	kIx(
<g/>
otázka	otázka	k1gFnSc1
vyčkávání	vyčkávání	k1gNnSc2
na	na	k7c4
zpožděné	zpožděný	k2eAgInPc4d1
spoje	spoj	k1gInPc4
v	v	k7c6
přestupním	přestupní	k2eAgInSc6d1
bodě	bod	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
lze	lze	k6eAd1
informaci	informace	k1gFnSc4
doplnit	doplnit	k5eAaPmF
i	i	k9
obrazem	obraz	k1gInSc7
z	z	k7c2
městského	městský	k2eAgInSc2d1
kamerového	kamerový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
sice	sice	k8xC
provozuje	provozovat	k5eAaImIp3nS
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
dispečink	dispečink	k1gInSc1
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
má	mít	k5eAaImIp3nS
přístup	přístup	k1gInSc4
k	k	k7c3
jeho	jeho	k3xOp3gInPc3
výstupům	výstup	k1gInPc3
a	a	k8xC
může	moct	k5eAaImIp3nS
policii	policie	k1gFnSc4
v	v	k7c6
případě	případ	k1gInSc6
požádat	požádat	k5eAaPmF
o	o	k7c4
dálkově	dálkově	k6eAd1
zajištěnou	zajištěný	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
polohy	poloha	k1gFnSc2
konkrétní	konkrétní	k2eAgFnSc2d1
kamery	kamera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dispečerský	dispečerský	k2eAgInSc1d1
a	a	k8xC
řídicí	řídicí	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
Brně	Brno	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
RIS	RIS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgInSc4
systém	systém	k1gInSc1
funguje	fungovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Plzni	Plzeň	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c4
podzim	podzim	k1gInSc4
2007	#num#	k4
chystá	chystat	k5eAaImIp3nS
výběrové	výběrový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
na	na	k7c4
řídicí	řídicí	k2eAgInSc4d1
a	a	k8xC
informační	informační	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
sledování	sledování	k1gNnSc2
tramvají	tramvaj	k1gFnSc7
podobný	podobný	k2eAgInSc4d1
DORISu	DORISum	k1gNnSc6
mají	mít	k5eAaImIp3nP
také	také	k9
například	například	k6eAd1
v	v	k7c6
německých	německý	k2eAgInPc6d1
Drážďanech	Drážďany	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Funkce	funkce	k1gFnSc1
Aktuální	aktuální	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
vlaku	vlak	k1gInSc2
pro	pro	k7c4
sledování	sledování	k1gNnSc4
vlaků	vlak	k1gInPc2
na	na	k7c6
hlavních	hlavní	k2eAgFnPc6d1
železničních	železniční	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
funguje	fungovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
je	být	k5eAaImIp3nS
zapracována	zapracovat	k5eAaPmNgFnS
do	do	k7c2
výstupů	výstup	k1gInPc2
vyhledávače	vyhledávač	k1gMnSc2
spojení	spojení	k1gNnSc2
IDOS	IDOS	kA
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
i	i	k9
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
SMS	SMS	kA
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaje	údaj	k1gInSc2
jsou	být	k5eAaImIp3nP
do	do	k7c2
systému	systém	k1gInSc2
vkládány	vkládat	k5eAaImNgInP
ručně	ručně	k6eAd1
výpravčími	výpravčí	k1gMnPc7
ve	v	k7c6
vybraných	vybraný	k2eAgFnPc6d1
železničních	železniční	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehledný	přehledný	k2eAgInSc1d1
grafický	grafický	k2eAgInSc1d1
výstup	výstup	k1gInSc1
a	a	k8xC
archiv	archiv	k1gInSc1
s	s	k7c7
využitím	využití	k1gNnSc7
těchto	tento	k3xDgInPc2
údajů	údaj	k1gInPc2
provozuje	provozovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
jako	jako	k8xC,k8xS
soukromou	soukromý	k2eAgFnSc4d1
zájmovou	zájmový	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
pod	pod	k7c7
názvem	název	k1gInSc7
Babitron	Babitron	k1gInSc1
Robert	Roberta	k1gFnPc2
Babilon	Babilon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
EMA	Ema	k1gFnSc1
(	(	kIx(
<g/>
elektronická	elektronický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
trati	trať	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
DORIS	DORIS	kA
<g/>
,	,	kIx,
zpracoval	zpracovat	k5eAaPmAgInS
Dopravní	dopravní	k2eAgInSc1d1
úsek	úsek	k1gInSc1
ředitelství	ředitelství	k1gNnSc2
DP	DP	kA
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
DP	DP	kA
Kontakt	kontakt	k1gInSc4
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
81	#num#	k4
2	#num#	k4
Tomislav	Tomislava	k1gFnPc2
Dratva	Dratvo	k1gNnSc2
<g/>
:	:	kIx,
Integrovaný	integrovaný	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
a	a	k8xC
řídicí	řídicí	k2eAgInSc1d1
systém	systém	k1gInSc1
MSP	MSP	kA
v	v	k7c6
hl.	hl.	k?
m.	m.	k?
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
IIŘS	IIŘS	kA
MSP	MSP	kA
HMP	HMP	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídicí	řídicí	k2eAgInPc1d1
a	a	k8xC
informační	informační	k2eAgInPc1d1
systémy	systém	k1gInPc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
(	(	kIx(
<g/>
ÚDI	ÚDI	kA
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Tonda	Tonda	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
:	:	kIx,
poznámky	poznámka	k1gFnPc1
k	k	k7c3
sérii	série	k1gFnSc3
článků	článek	k1gInPc2
Jana	Jana	k1gFnSc1
Pucla	pucnout	k5eAaPmAgFnS
o	o	k7c6
sledování	sledování	k1gNnSc6
dodržování	dodržování	k1gNnSc2
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
v	v	k7c6
PID	PID	kA
ve	v	k7c6
Večerníku	večerník	k1gInSc6
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
mhdcr	mhdcra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
biz	biz	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Předjetí	předjetí	k1gNnSc4
nově	nově	k6eAd1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
diskuse	diskuse	k1gFnPc1
OSPEA	OSPEA	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Předjetí	předjetí	k1gNnSc1
II	II	kA
nově	nově	k6eAd1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Radek	Radek	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
diskuse	diskuse	k1gFnSc1
OSPEA	OSPEA	kA
<g/>
,	,	kIx,
dopis	dopis	k1gInSc4
předsedy	předseda	k1gMnSc2
OSPEA	OSPEA	kA
Vratislava	Vratislav	k1gMnSc2
Feigela	Feigel	k1gMnSc2
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2006.1	2006.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Pavel	Pavel	k1gMnSc1
Kovář	Kovář	k1gMnSc1
<g/>
:	:	kIx,
Doris	Doris	k1gFnSc1
bdí	bdít	k5eAaImIp3nS
dnem	den	k1gInSc7
i	i	k8xC
nocí	noc	k1gFnSc7
(	(	kIx(
<g/>
Reflex	reflex	k1gInSc1
25	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
DORIS	DORIS	kA
–	–	k?
2005	#num#	k4
T	T	kA
Informační	informační	k2eAgInSc1d1
panel	panel	k1gInSc1
na	na	k7c6
Dni	den	k1gInSc6
otevřených	otevřený	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Jan	Jan	k1gMnSc1
Pucl	pucnout	k5eAaPmAgMnS
<g/>
:	:	kIx,
série	série	k1gFnSc1
článků	článek	k1gInPc2
o	o	k7c6
sledování	sledování	k1gNnSc6
dodržování	dodržování	k1gNnSc2
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
PID	PID	kA
Archivováno	archivovat	k5eAaBmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
Večerník	večerník	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Michal	Michal	k1gMnSc1
Nohejl	Nohejl	k1gMnSc1
<g/>
:	:	kIx,
AUDIS	AUDIS	kA
<g/>
,	,	kIx,
dispečerský	dispečerský	k2eAgInSc1d1
řídicí	řídicí	k2eAgInSc1d1
systém	systém	k1gInSc1
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
DP	DP	kA
kontakt	kontakt	k1gInSc4
10	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
↑	↑	k?
Michal	Michal	k1gMnSc1
Nohejl	Nohejl	k1gMnSc1
<g/>
:	:	kIx,
FarWay	FarWay	k1gInPc1
–	–	k?
systém	systém	k1gInSc1
pro	pro	k7c4
sledování	sledování	k1gNnSc4
pohybu	pohyb	k1gInSc2
pohotovostních	pohotovostní	k2eAgNnPc2d1
a	a	k8xC
servisních	servisní	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
DP	DP	kA
<g/>
,	,	kIx,
DP	DP	kA
kontakt	kontakt	k1gInSc4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
↑	↑	k?
Den	den	k1gInSc1
otevřených	otevřený	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
-	-	kIx~
DPMB	DPMB	kA
a.s.	a.s.	k?
<g/>
,	,	kIx,
dopravní	dopravní	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
(	(	kIx(
<g/>
BUSportál	BUSportál	k1gInSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Havíř	havíř	k1gMnSc1
<g/>
:	:	kIx,
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Vítězslavem	Vítězslav	k1gMnSc7
Zemanem	Zeman	k1gMnSc7
<g/>
,	,	kIx,
vedoucím	vedoucí	k1gMnSc7
dopravního	dopravní	k2eAgInSc2d1
a	a	k8xC
energetického	energetický	k2eAgInSc2d1
dispečinku	dispečink	k1gInSc2
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
nejen	nejen	k6eAd1
o	o	k7c6
RIS	RIS	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
BUSportál	BUSportál	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
DORIS	DORIS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
DORIS	DORIS	kA
<g/>
,	,	kIx,
zpracoval	zpracovat	k5eAaPmAgInS
Dopravní	dopravní	k2eAgInSc1d1
úsek	úsek	k1gInSc1
ředitelství	ředitelství	k1gNnSc2
DP	DP	kA
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
DP	DP	kA
Kontakt	kontakt	k1gInSc4
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kovář	Kovář	k1gMnSc1
<g/>
:	:	kIx,
Doris	Doris	k1gFnSc1
bdí	bdít	k5eAaImIp3nS
dnem	den	k1gInSc7
i	i	k8xC
nocí	noc	k1gFnSc7
(	(	kIx(
<g/>
Reflex	reflex	k1gInSc1
25	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Pucl	pucnout	k5eAaPmAgMnS
<g/>
:	:	kIx,
série	série	k1gFnSc1
článků	článek	k1gInPc2
o	o	k7c6
sledování	sledování	k1gNnSc6
dodržování	dodržování	k1gNnSc2
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
v	v	k7c6
PID	PID	kA
<g/>
:	:	kIx,
Doris	Doris	k1gInSc1
vidí	vidět	k5eAaImIp3nS
rudě	rudě	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
tramvaj	tramvaj	k1gFnSc1
ujede	ujet	k5eAaPmIp3nS
<g/>
,	,	kIx,
Metro	metro	k1gNnSc1
má	mít	k5eAaImIp3nS
místo	místo	k1gNnSc4
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
grafikon	grafikon	k1gNnSc1
<g/>
,	,	kIx,
Autobusy	autobus	k1gInPc1
hlídají	hlídat	k5eAaImIp3nP
signály	signál	k1gInPc4
i	i	k8xC
lidé	člověk	k1gMnPc1
(	(	kIx(
<g/>
Večerník	večerník	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
webu	web	k1gInSc6
mhdcr	mhdcra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
biz	biz	k?
zveřejněno	zveřejnit	k5eAaPmNgNnS
s	s	k7c7
upřesňujícím	upřesňující	k2eAgInSc7d1
dodatkem	dodatek	k1gInSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Tonda	Tonda	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Téma	téma	k1gNnSc1
č.	č.	k?
1	#num#	k4
-	-	kIx~
Telematika	telematika	k1gFnSc1
v	v	k7c6
dopravě	doprava	k1gFnSc6
-	-	kIx~
inteligentní	inteligentní	k2eAgInPc1d1
dopravní	dopravní	k2eAgInPc1d1
systémy	systém	k1gInPc1
(	(	kIx(
<g/>
ÚDI	ÚDI	kA
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tomislav	Tomislav	k1gMnSc1
Dratva	Dratvo	k1gNnSc2
<g/>
:	:	kIx,
Integrovaný	integrovaný	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
a	a	k8xC
řídicí	řídicí	k2eAgInSc1d1
systém	systém	k1gInSc1
MSP	MSP	kA
v	v	k7c6
hl.	hl.	k?
m.	m.	k?
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
IIŘS	IIŘS	kA
MSP	MSP	kA
HMP	HMP	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídicí	řídicí	k2eAgInPc1d1
a	a	k8xC
informační	informační	k2eAgInPc1d1
systémy	systém	k1gInPc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
(	(	kIx(
<g/>
ÚDI	ÚDI	kA
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odborová	odborový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
OSPEA	OSPEA	kA
<g/>
,	,	kIx,
články	článek	k1gInPc1
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
DORIS	DORIS	kA
avizovány	avizován	k2eAgFnPc4d1
v	v	k7c6
samostatné	samostatný	k2eAgFnSc6d1
sekci	sekce	k1gFnSc6
</s>
<s>
Záznam	záznam	k1gInSc1
v	v	k7c6
digitálních	digitální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
TETRA	tetra	k1gFnSc1
a	a	k8xC
COMPACT	COMPACT	kA
TETRA	tetra	k1gFnSc1
(	(	kIx(
<g/>
Schéma	schéma	k1gNnSc1
propojení	propojení	k1gNnSc2
systémů	systém	k1gInPc2
Tetra	tetra	k1gFnSc1
a	a	k8xC
DORIS	DORIS	kA
<g/>
)	)	kIx)
</s>
<s>
Řídící	řídící	k2eAgInPc4d1
systémy	systém	k1gInPc4
DORIS	DORIS	kA
<g/>
,	,	kIx,
AUDIS	AUDIS	kA
<g/>
,	,	kIx,
ZIS	ZIS	kA
(	(	kIx(
<g/>
Referenční	referenční	k2eAgInPc1d1
projekty	projekt	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
</s>
