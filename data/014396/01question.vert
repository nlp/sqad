<s>
Jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
dopravního	dopravní	k2eAgInSc2d1
řídícího	řídící	k2eAgInSc2d1
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
sleduje	sledovat	k5eAaImIp3nS
tramvaje	tramvaj	k1gFnPc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
?	?	kIx.
</s>