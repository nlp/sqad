<s>
Jonas	Jonas	k1gMnSc1	Jonas
Edward	Edward	k1gMnSc1	Edward
Salk	Salk	k1gMnSc1	Salk
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
virologů	virolog	k1gMnPc2	virolog
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k6eAd1	především
objevem	objev	k1gInSc7	objev
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
dětské	dětský	k2eAgFnSc3d1	dětská
obrně	obrna	k1gFnSc3	obrna
<g/>
.	.	kIx.	.
</s>
