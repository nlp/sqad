<p>
<s>
Jonas	Jonas	k1gMnSc1	Jonas
Edward	Edward	k1gMnSc1	Edward
Salk	Salk	k1gMnSc1	Salk
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
virologů	virolog	k1gMnPc2	virolog
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k6eAd1	především
objevem	objev	k1gInSc7	objev
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
dětské	dětský	k2eAgFnSc3d1	dětská
obrně	obrna	k1gFnSc3	obrna
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
objev	objev	k1gInSc1	objev
si	se	k3xPyFc3	se
nedal	dát	k5eNaPmAgInS	dát
patentovat	patentovat	k5eAaBmF	patentovat
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
ho	on	k3xPp3gInSc4	on
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
šíření	šíření	k1gNnSc3	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
mohl	moct	k5eAaImAgInS	moct
připravit	připravit	k5eAaPmF	připravit
o	o	k7c6	o
asi	asi	k9	asi
7	[number]	k4	7
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
krok	krok	k1gInSc4	krok
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
patentovat	patentovat	k5eAaBmF	patentovat
lék	lék	k1gInSc4	lék
proti	proti	k7c3	proti
obrně	obrna	k1gFnSc3	obrna
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
stejné	stejné	k1gNnSc1	stejné
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
patentovat	patentovat	k5eAaBmF	patentovat
slunce	slunce	k1gNnSc4	slunce
<g/>
"	"	kIx"	"
–	–	k?	–
jeho	jeho	k3xOp3gFnSc4	jeho
odpověď	odpověď	k1gFnSc4	odpověď
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
memem	memem	k6eAd1	memem
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
strávil	strávit	k5eAaPmAgInS	strávit
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
hledáním	hledání	k1gNnSc7	hledání
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
několik	několik	k4yIc4	několik
filozofických	filozofický	k2eAgFnPc2d1	filozofická
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
"	"	kIx"	"
<g/>
biofilozofie	biofilozofie	k1gFnSc1	biofilozofie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gFnSc2	Tim
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
osobností	osobnost	k1gFnPc2	osobnost
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
Unfolding	Unfolding	k1gInSc1	Unfolding
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Survival	Survivat	k5eAaImAgMnS	Survivat
of	of	k?	of
the	the	k?	the
Wisest	Wisest	k1gInSc1	Wisest
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
World	World	k6eAd1	World
Population	Population	k1gInSc1	Population
and	and	k?	and
Human	Human	k1gInSc1	Human
Values	Values	k1gInSc1	Values
<g/>
:	:	kIx,	:
A	a	k9	a
New	New	k1gFnSc4	New
Reality	realita	k1gFnSc2	realita
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anatomy	anatom	k1gMnPc4	anatom
of	of	k?	of
Reality	realita	k1gFnSc2	realita
<g/>
:	:	kIx,	:
Merging	Merging	k1gInSc1	Merging
of	of	k?	of
Intuition	Intuition	k1gInSc1	Intuition
and	and	k?	and
Reason	Reason	k1gInSc1	Reason
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bookchin	Bookchin	k1gInSc1	Bookchin
<g/>
,	,	kIx,	,
Debbie	Debbie	k1gFnSc1	Debbie
<g/>
,	,	kIx,	,
and	and	k?	and
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Virus	virus	k1gInSc1	virus
and	and	k?	and
the	the	k?	the
Vaccine	Vaccin	k1gMnSc5	Vaccin
<g/>
,	,	kIx,	,
Macmillan	Macmillan	k1gMnSc1	Macmillan
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-312-34272-1	[number]	k4	0-312-34272-1
</s>
</p>
<p>
<s>
Oshinsky	Oshinsky	k6eAd1	Oshinsky
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
M.	M.	kA	M.
Polio	Polio	k1gMnSc1	Polio
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Story	story	k1gFnSc4	story
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc4	Oxford
Univ	Univa	k1gFnPc2	Univa
<g/>
.	.	kIx.	.
</s>
<s>
Press	Press	k1gInSc1	Press
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
