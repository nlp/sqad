<s>
Většina	většina	k1gFnSc1	většina
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
realizuje	realizovat	k5eAaBmIp3nS	realizovat
přes	přes	k7c4	přes
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
