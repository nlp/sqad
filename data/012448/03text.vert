<p>
<s>
Podplukovník	podplukovník	k1gMnSc1	podplukovník
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
pplk.	pplk.	kA	pplk.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
policejní	policejní	k2eAgFnSc1d1	policejní
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
zkratku	zkratka	k1gFnSc4	zkratka
pplk.	pplk.	kA	pplk.
</s>
<s>
V	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jde	jít	k5eAaImIp3nS	jít
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
o	o	k7c4	o
šestou	šestý	k4xOgFnSc4	šestý
(	(	kIx(	(
<g/>
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
<g/>
)	)	kIx)	)
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
podplukovník	podplukovník	k1gMnSc1	podplukovník
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
major	major	k1gMnSc1	major
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgNnSc4d1	armádní
označení	označení	k1gNnSc4	označení
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
zlaté	zlatý	k2eAgFnPc1d1	zlatá
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
postranní	postranní	k2eAgFnSc7d1	postranní
tzv.	tzv.	kA	tzv.
kolejničkou	kolejnička	k1gFnSc7	kolejnička
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
proužek	proužek	k1gInSc1	proužek
zlaté	zlatý	k2eAgFnSc2d1	zlatá
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podplukovník	podplukovník	k1gMnSc1	podplukovník
zpravidla	zpravidla	k6eAd1	zpravidla
velí	velet	k5eAaImIp3nS	velet
praporu	prapor	k1gInSc2	prapor
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgFnPc4d1	vojenská
hodnosti	hodnost	k1gFnPc4	hodnost
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
podplukovník	podplukovník	k1gMnSc1	podplukovník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Podplukovník	podplukovník	k1gMnSc1	podplukovník
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Hodnosti	hodnost	k1gFnPc4	hodnost
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Důstojnické	důstojnický	k2eAgFnPc4d1	důstojnická
hodnosti	hodnost	k1gFnPc4	hodnost
Armády	armáda	k1gFnSc2	armáda
ČR	ČR	kA	ČR
</s>
</p>
