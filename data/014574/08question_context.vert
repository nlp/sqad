<s>
Sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
(	(	kIx(
<g/>
podle	podle	k7c2
Pravidel	pravidlo	k1gNnPc2
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
psáno	psát	k5eAaImNgNnS
s	s	k7c7
malým	malý	k2eAgInSc7d1
s	s	k7c7
<g/>
,	,	kIx,
tedy	tedy	k8xC
sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ovšem	ovšem	k9
Česká	český	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
doporučuje	doporučovat	k5eAaImIp3nS
psaní	psaní	k1gNnSc4
s	s	k7c7
velkým	velký	k2eAgInSc7d1
S	s	k7c7
<g/>
,	,	kIx,
tedy	tedy	k8xC
Sluneční	sluneční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
planetární	planetární	k2eAgInSc1d1
systém	systém	k1gInSc1
hvězdy	hvězda	k1gFnSc2
známé	známý	k2eAgFnPc4d1
pod	pod	k7c7
názvem	název	k1gInSc7
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
planeta	planeta	k1gFnSc1
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>