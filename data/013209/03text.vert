<p>
<s>
Posláním	poslání	k1gNnSc7	poslání
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
činnosti	činnost	k1gFnSc2	činnost
Českého	český	k2eAgInSc2d1	český
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
ČIA	ČIA	kA	ČIA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poskytování	poskytování	k1gNnSc1	poskytování
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
akreditace	akreditace	k1gFnSc2	akreditace
komerčním	komerční	k2eAgInPc3d1	komerční
subjektům	subjekt	k1gInPc3	subjekt
a	a	k8xC	a
orgánům	orgán	k1gInPc3	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgInSc1d1	národní
akreditační	akreditační	k2eAgInSc1d1	akreditační
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
akreditaci	akreditace	k1gFnSc4	akreditace
jako	jako	k8xC	jako
orgán	orgán	k1gInSc4	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
tedy	tedy	k9	tedy
akt	akt	k1gInSc1	akt
"	"	kIx"	"
<g/>
vyjádření	vyjádření	k1gNnSc1	vyjádření
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
subjekty	subjekt	k1gInPc4	subjekt
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
neprovádí	provádět	k5eNaImIp3nS	provádět
žádné	žádný	k3yNgFnPc4	žádný
zkoušky	zkouška	k1gFnPc4	zkouška
ani	ani	k8xC	ani
podobné	podobný	k2eAgFnPc4d1	podobná
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
ČIA	ČIA	kA	ČIA
je	být	k5eAaImIp3nS	být
notifikován	notifikovat	k5eAaBmNgInS	notifikovat
a	a	k8xC	a
uznán	uznat	k5eAaPmNgInS	uznat
Evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
akreditační	akreditační	k2eAgInSc1d1	akreditační
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
ČIA	ČIA	kA	ČIA
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
všechny	všechen	k3xTgInPc4	všechen
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
národní	národní	k2eAgInPc4d1	národní
akreditační	akreditační	k2eAgInPc4d1	akreditační
orgány	orgán	k1gInPc1	orgán
kladeny	klást	k5eAaImNgInP	klást
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
plnění	plnění	k1gNnSc1	plnění
je	být	k5eAaImIp3nS	být
vyhodnocováno	vyhodnocovat	k5eAaImNgNnS	vyhodnocovat
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
týmy	tým	k1gInPc7	tým
evaluátorů	evaluátor	k1gInPc2	evaluátor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
evaluací	evaluace	k1gFnPc2	evaluace
je	být	k5eAaImIp3nS	být
ČIA	ČIA	kA	ČIA
umožněno	umožnit	k5eAaPmNgNnS	umožnit
uzavírat	uzavírat	k5eAaImF	uzavírat
tzv.	tzv.	kA	tzv.
Multilaterální	multilaterální	k2eAgFnSc2d1	multilaterální
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
(	(	kIx(	(
<g/>
MLA	MLA	kA	MLA
<g/>
)	)	kIx)	)
jak	jak	k8xC	jak
na	na	k7c4	na
evropské	evropský	k2eAgMnPc4d1	evropský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
provádí	provádět	k5eAaImIp3nS	provádět
nestranné	nestranný	k2eAgNnSc4d1	nestranné
<g/>
,	,	kIx,	,
objektivní	objektivní	k2eAgNnSc4d1	objektivní
a	a	k8xC	a
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
posouzení	posouzení	k1gNnSc4	posouzení
způsobilosti	způsobilost	k1gFnSc2	způsobilost
shody	shoda	k1gFnSc2	shoda
(	(	kIx(	(
<g/>
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zkušební	zkušební	k2eAgFnSc2d1	zkušební
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kalibrační	kalibrační	k2eAgFnPc1d1	kalibrační
laboratoře	laboratoř	k1gFnPc1	laboratoř
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zdravotnické	zdravotnický	k2eAgFnPc4d1	zdravotnická
laboratoře	laboratoř	k1gFnPc4	laboratoř
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
certifikační	certifikační	k2eAgInPc1d1	certifikační
orgány	orgán	k1gInPc1	orgán
certifikující	certifikující	k2eAgInPc1d1	certifikující
systémy	systém	k1gInPc1	systém
managementu	management	k1gInSc2	management
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
certifikační	certifikační	k2eAgInPc1d1	certifikační
orgány	orgán	k1gInPc1	orgán
certifikující	certifikující	k2eAgInPc1d1	certifikující
produkty	produkt	k1gInPc1	produkt
vč.	vč.	k?	vč.
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
certifikační	certifikační	k2eAgInPc1d1	certifikační
orgány	orgán	k1gInPc1	orgán
certifikující	certifikující	k2eAgFnSc2d1	certifikující
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
inspekční	inspekční	k2eAgInPc1d1	inspekční
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
poskytovatele	poskytovatel	k1gMnSc4	poskytovatel
zkoušení	zkoušení	k1gNnSc2	zkoušení
způsobilosti	způsobilost	k1gFnSc2	způsobilost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ověřovatele	ověřovatel	k1gMnPc4	ověřovatel
výkazů	výkaz	k1gInPc2	výkaz
emisí	emise	k1gFnSc7	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
environmentální	environmentální	k2eAgMnPc4d1	environmentální
ověřovatele	ověřovatel	k1gMnPc4	ověřovatel
programů	program	k1gInPc2	program
EMAS	EMAS	kA	EMAS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Milníky	milník	k1gInPc4	milník
akreditačního	akreditační	k2eAgInSc2d1	akreditační
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
počátek	počátek	k1gInSc4	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
akreditace	akreditace	k1gFnSc2	akreditace
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Federálního	federální	k2eAgInSc2d1	federální
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
(	(	kIx(	(
<g/>
FÚNM	FÚNM	kA	FÚNM
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
samostatný	samostatný	k2eAgInSc1d1	samostatný
odbor	odbor	k1gInSc1	odbor
akreditace	akreditace	k1gFnSc2	akreditace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
bylo	být	k5eAaImAgNnS	být
uděleno	udělen	k2eAgNnSc4d1	uděleno
první	první	k4xOgNnSc4	první
osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c4	o
akreditaci	akreditace	k1gFnSc4	akreditace
pro	pro	k7c4	pro
zkušební	zkušební	k2eAgFnSc4d1	zkušební
laboratoř	laboratoř	k1gFnSc4	laboratoř
č.	č.	k?	č.
1001.1992	[number]	k4	1001.1992
</s>
</p>
<p>
<s>
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
bude	být	k5eAaImBp3nS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
bude	být	k5eAaImBp3nS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
nestátní	státní	k2eNgInSc4d1	nestátní
subjekt	subjekt	k1gInSc4	subjekt
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
ČR	ČR	kA	ČR
vydalo	vydat	k5eAaPmAgNnS	vydat
Zřizovací	zřizovací	k2eAgFnSc4d1	zřizovací
listinu	listina	k1gFnSc4	listina
Českého	český	k2eAgInSc2d1	český
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
.1993	.1993	k4	.1993
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc2	organizace
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
Národní	národní	k2eAgInSc1d1	národní
akreditační	akreditační	k2eAgInSc1d1	akreditační
orgán	orgán	k1gInSc1	orgán
<g/>
.1994	.1994	k4	.1994
</s>
</p>
<p>
<s>
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
Institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
provádění	provádění	k1gNnSc4	provádění
akreditací	akreditace	k1gFnPc2	akreditace
<g/>
,	,	kIx,	,
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
<g/>
.1995	.1995	k4	.1995
</s>
</p>
<p>
<s>
odborní	odborný	k2eAgMnPc1d1	odborný
pracovníci	pracovník	k1gMnPc1	pracovník
ČIA	ČIA	kA	ČIA
prošli	projít	k5eAaPmAgMnP	projít
certifikací	certifikace	k1gFnPc2	certifikace
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
certifikát	certifikát	k1gInSc4	certifikát
pro	pro	k7c4	pro
manažery	manažer	k1gInPc4	manažer
jakosti	jakost	k1gFnSc2	jakost
EOQ	EOQ	kA	EOQ
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jako	jako	k9	jako
první	první	k4xOgInSc1	první
akreditační	akreditační	k2eAgInSc1d1	akreditační
orgán	orgán	k1gInSc1	orgán
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
přidruženým	přidružený	k2eAgInSc7d1	přidružený
členem	člen	k1gInSc7	člen
evropských	evropský	k2eAgFnPc2d1	Evropská
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
akreditace	akreditace	k1gFnSc2	akreditace
EAL	EAL	kA	EAL
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Cooperation	Cooperation	k1gInSc1	Cooperation
for	forum	k1gNnPc2	forum
Accreditation	Accreditation	k1gInSc1	Accreditation
of	of	k?	of
Laboratories	Laboratories	k1gInSc1	Laboratories
<g/>
)	)	kIx)	)
a	a	k8xC	a
EAC	EAC	kA	EAC
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Cooperation	Cooperation	k1gInSc1	Cooperation
for	forum	k1gNnPc2	forum
Accreditation	Accreditation	k1gInSc1	Accreditation
for	forum	k1gNnPc2	forum
Certification	Certification	k1gInSc1	Certification
<g/>
)	)	kIx)	)
<g/>
.1996	.1996	k4	.1996
</s>
</p>
<p>
<s>
plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
ILAC	ILAC	kA	ILAC
(	(	kIx(	(
<g/>
International	International	k1gMnSc1	International
Laboratory	Laborator	k1gMnPc7	Laborator
Accreditation	Accreditation	k1gInSc4	Accreditation
Cooperation	Cooperation	k1gInSc1	Cooperation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
hostil	hostit	k5eAaImAgInS	hostit
jednání	jednání	k1gNnSc4	jednání
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
EAL	EAL	kA	EAL
a	a	k8xC	a
EAC	EAC	kA	EAC
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.1997	.1997	k4	.1997
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
ČIA	ČIA	kA	ČIA
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
pověřilo	pověřit	k5eAaPmAgNnS	pověřit
ČIA	ČIA	kA	ČIA
výkonem	výkon	k1gInSc7	výkon
akreditace	akreditace	k1gFnPc1	akreditace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
sloučením	sloučení	k1gNnSc7	sloučení
EAL	EAL	kA	EAL
a	a	k8xC	a
EAC	EAC	kA	EAC
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
organizace	organizace	k1gFnSc1	organizace
EA	EA	kA	EA
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
co-operation	coperation	k1gInSc1	co-operation
for	forum	k1gNnPc2	forum
Accreditation	Accreditation	k1gInSc1	Accreditation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
IAF	IAF	kA	IAF
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Accreditation	Accreditation	k1gInSc1	Accreditation
Forum	forum	k1gNnSc1	forum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
evaluace	evaluace	k1gFnSc2	evaluace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EA	EA	kA	EA
<g/>
.1998	.1998	k4	.1998
</s>
</p>
<p>
<s>
dosažení	dosažení	k1gNnSc1	dosažení
nezávilosti	nezávilost	k1gFnSc2	nezávilost
akreditačního	akreditační	k2eAgInSc2d1	akreditační
orgánu	orgán	k1gInSc2	orgán
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
transformací	transformace	k1gFnSc7	transformace
v	v	k7c6	v
obecně	obecně	k6eAd1	obecně
prospěšnou	prospěšný	k2eAgFnSc4d1	prospěšná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zakladatelem	zakladatel	k1gMnSc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
statutárním	statutární	k2eAgInSc7d1	statutární
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
ČIA	ČIA	kA	ČIA
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
plnoprávný	plnoprávný	k2eAgMnSc1d1	plnoprávný
člen	člen	k1gMnSc1	člen
EA	EA	kA	EA
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
signatář	signatář	k1gMnSc1	signatář
multilaterální	multilaterální	k2eAgFnSc2d1	multilaterální
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
EA	EA	kA	EA
MLA	MLA	kA	MLA
<g/>
.1999	.1999	k4	.1999
</s>
</p>
<p>
<s>
signatář	signatář	k1gMnSc1	signatář
multilaterální	multilaterální	k2eAgFnSc2d1	multilaterální
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
IAF	IAF	kA	IAF
MLA	MLA	kA	MLA
<g/>
.2000	.2000	k4	.2000
</s>
</p>
<p>
<s>
signatář	signatář	k1gMnSc1	signatář
multilaterální	multilaterální	k2eAgFnSc2d1	multilaterální
dohody	dohoda	k1gFnSc2	dohoda
ILAC	ILAC	kA	ILAC
MRA	mřít	k5eAaImSgInS	mřít
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
výsledků	výsledek	k1gInPc2	výsledek
akreditace	akreditace	k1gFnSc2	akreditace
<g/>
.2002	.2002	k4	.2002
</s>
</p>
<p>
<s>
reevaluační	reevaluační	k2eAgInSc4d1	reevaluační
audit	audit	k1gInSc4	audit
EA	EA	kA	EA
s	s	k7c7	s
doporučením	doporučení	k1gNnSc7	doporučení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ČIA	ČIA	kA	ČIA
setrval	setrvat	k5eAaPmAgInS	setrvat
jako	jako	k9	jako
signatář	signatář	k1gMnSc1	signatář
EA	EA	kA	EA
MLA	MLA	kA	MLA
<g/>
.2003	.2003	k4	.2003
</s>
</p>
<p>
<s>
rozšíření	rozšíření	k1gNnSc1	rozšíření
EA	EA	kA	EA
MLA	MLA	kA	MLA
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
inspekčních	inspekční	k2eAgInPc2d1	inspekční
orgánů	orgán	k1gInPc2	orgán
<g/>
.2004	.2004	k4	.2004
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
podepsal	podepsat	k5eAaPmAgInS	podepsat
Etický	etický	k2eAgInSc1d1	etický
kodex	kodex	k1gInSc1	kodex
akreditačních	akreditační	k2eAgInPc2d1	akreditační
orgánů	orgán	k1gInPc2	orgán
IAF	IAF	kA	IAF
<g/>
.2005	.2005	k4	.2005
</s>
</p>
<p>
<s>
evaluace	evaluace	k1gFnSc1	evaluace
ČIA	ČIA	kA	ČIA
Fórem	fórum	k1gNnSc7	fórum
akreditačních	akreditační	k2eAgInPc2d1	akreditační
orgánů	orgán	k1gInPc2	orgán
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
akreditace	akreditace	k1gFnSc2	akreditace
environmentálních	environmentální	k2eAgMnPc2d1	environmentální
ověřovatelů	ověřovatel	k1gMnPc2	ověřovatel
(	(	kIx(	(
<g/>
FALB	FALB	kA	FALB
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
EMAS	EMAS	kA	EMAS
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc1	první
z	z	k7c2	z
nových	nový	k2eAgInPc2d1	nový
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
národní	národní	k2eAgInSc1d1	národní
orgán	orgán	k1gInSc1	orgán
tuto	tento	k3xDgFnSc4	tento
evaluaci	evaluace	k1gFnSc4	evaluace
získal	získat	k5eAaPmAgMnS	získat
<g/>
.2006	.2006	k4	.2006
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
si	se	k3xPyFc3	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
15	[number]	k4	15
let	léto	k1gNnPc2	léto
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
proklientský	proklientský	k2eAgInSc4d1	proklientský
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
spuštěn	spuštěn	k2eAgInSc1d1	spuštěn
nový	nový	k2eAgInSc1d1	nový
web	web	k1gInSc1	web
a	a	k8xC	a
ČIA	ČIA	kA	ČIA
realizuje	realizovat	k5eAaBmIp3nS	realizovat
projekt	projekt	k1gInSc4	projekt
Vnímání	vnímání	k1gNnSc2	vnímání
problematiky	problematika	k1gFnSc2	problematika
jakosti	jakost	k1gFnSc2	jakost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
evaluace	evaluace	k1gFnSc2	evaluace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EA	EA	kA	EA
<g/>
.2009	.2009	k4	.2009
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
je	být	k5eAaImIp3nS	být
notifikován	notifikovat	k5eAaBmNgInS	notifikovat
u	u	k7c2	u
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
vnitrostátní	vnitrostátní	k2eAgInSc1d1	vnitrostátní
akreditační	akreditační	k2eAgInSc1d1	akreditační
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.2010	.2010	k4	.2010
</s>
</p>
<p>
<s>
nabytí	nabytí	k1gNnSc1	nabytí
účinnosti	účinnost	k1gFnSc2	účinnost
nařízení	nařízení	k1gNnSc2	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
<g/>
765	[number]	k4	765
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
a	a	k8xC	a
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
490	[number]	k4	490
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
poskytování	poskytování	k1gNnSc2	poskytování
akreditace	akreditace	k1gFnSc2	akreditace
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
orgánu	orgán	k1gInSc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
pověřilo	pověřit	k5eAaPmAgNnS	pověřit
ČIA	ČIA	kA	ČIA
výkonem	výkon	k1gInSc7	výkon
akreditace	akreditace	k1gFnPc1	akreditace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
evaluace	evaluace	k1gFnSc2	evaluace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EA	EA	kA	EA
<g/>
.2011	.2011	k4	.2011
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
evaluace	evaluace	k1gFnSc2	evaluace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EA	EA	kA	EA
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
statutárním	statutární	k2eAgInSc7d1	statutární
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
ředitel	ředitel	k1gMnSc1	ředitel
ČIA	ČIA	kA	ČIA
<g/>
.2012	.2012	k4	.2012
</s>
</p>
<p>
<s>
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
povýšit	povýšit	k5eAaPmF	povýšit
datovou	datový	k2eAgFnSc4d1	datová
schránku	schránka	k1gFnSc4	schránka
ČIA	ČIA	kA	ČIA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
plnila	plnit	k5eAaImAgFnS	plnit
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
u	u	k7c2	u
orgánu	orgán	k1gInSc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
.2014	.2014	k4	.2014
</s>
</p>
<p>
<s>
možnost	možnost	k1gFnSc1	možnost
provádět	provádět	k5eAaImF	provádět
akreditaci	akreditace	k1gFnSc4	akreditace
také	také	k9	také
na	na	k7c6	na
základě	základ	k1gInSc6	základ
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
hostí	hostit	k5eAaImIp3nS	hostit
jednání	jednání	k1gNnSc4	jednání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
EA	EA	kA	EA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
podpis	podpis	k1gInSc1	podpis
multilaterální	multilaterální	k2eAgFnSc2d1	multilaterální
dohody	dohoda	k1gFnSc2	dohoda
EA	EA	kA	EA
MLA	MLA	kA	MLA
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
oblast	oblast	k1gFnSc4	oblast
-	-	kIx~	-
ověřování	ověřování	k1gNnSc4	ověřování
výkazů	výkaz	k1gInPc2	výkaz
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.2016	.2016	k4	.2016
</s>
</p>
<p>
<s>
akreditační	akreditační	k2eAgInSc1d1	akreditační
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
25	[number]	k4	25
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc1	existence
<g/>
.2017	.2017	k4	.2017
</s>
</p>
<p>
<s>
implementována	implementován	k2eAgFnSc1d1	implementována
organizační	organizační	k2eAgFnSc1d1	organizační
změna	změna	k1gFnSc1	změna
a	a	k8xC	a
spuštěn	spuštěn	k2eAgInSc1d1	spuštěn
update	update	k1gInSc1	update
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
<g/>
.2018	.2018	k4	.2018
</s>
</p>
<p>
<s>
ČIA	ČIA	kA	ČIA
jako	jako	k8xC	jako
obecně	obecně	k6eAd1	obecně
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
20	[number]	k4	20
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
evaluace	evaluace	k1gFnSc2	evaluace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EA	EA	kA	EA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akreditační	akreditační	k2eAgInSc4d1	akreditační
systém	systém	k1gInSc4	systém
spravovaný	spravovaný	k2eAgInSc4d1	spravovaný
ČIA	ČIA	kA	ČIA
==	==	k?	==
</s>
</p>
<p>
<s>
Akreditace	akreditace	k1gFnSc1	akreditace
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
záležitostí	záležitost	k1gFnSc7	záležitost
komerčních	komerční	k2eAgFnPc2d1	komerční
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
ČIA	ČIA	kA	ČIA
hraje	hrát	k5eAaImIp3nS	hrát
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
i	i	k9	i
u	u	k7c2	u
orgánů	orgán	k1gInPc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
projednává	projednávat	k5eAaImIp3nS	projednávat
a	a	k8xC	a
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
dohody	dohoda	k1gFnPc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
akreditace	akreditace	k1gFnSc2	akreditace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dohodách	dohoda	k1gFnPc6	dohoda
je	být	k5eAaImIp3nS	být
ukotveno	ukotven	k2eAgNnSc1d1	ukotveno
například	například	k6eAd1	například
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
poskytování	poskytování	k1gNnSc1	poskytování
informací	informace	k1gFnPc2	informace
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
akreditace	akreditace	k1gFnSc2	akreditace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dohody	dohoda	k1gFnPc1	dohoda
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
navrhovat	navrhovat	k5eAaImF	navrhovat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
poradních	poradní	k2eAgInPc2d1	poradní
orgánů	orgán	k1gInPc2	orgán
ČIA	ČIA	kA	ČIA
a	a	k8xC	a
odborníky	odborník	k1gMnPc7	odborník
do	do	k7c2	do
databáze	databáze	k1gFnSc2	databáze
odborných	odborný	k2eAgMnPc2d1	odborný
posuzovatelů	posuzovatel	k1gMnPc2	posuzovatel
ČIA	ČIA	kA	ČIA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
uvedených	uvedený	k2eAgFnPc2d1	uvedená
dohod	dohoda	k1gFnPc2	dohoda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
signatáře	signatář	k1gMnSc4	signatář
lehčí	lehčit	k5eAaImIp3nS	lehčit
využít	využít	k5eAaPmF	využít
akreditované	akreditovaný	k2eAgInPc4d1	akreditovaný
subjekty	subjekt	k1gInPc4	subjekt
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hygienických	hygienický	k2eAgFnPc2d1	hygienická
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
svařování	svařování	k1gNnSc2	svařování
<g/>
,	,	kIx,	,
státního	státní	k2eAgNnSc2d1	státní
zkušebnictví	zkušebnictví	k1gNnSc2	zkušebnictví
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ČIA	ČIA	kA	ČIA
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
evropské	evropský	k2eAgFnSc2d1	Evropská
legislativy	legislativa	k1gFnSc2	legislativa
promítl	promítnout	k5eAaPmAgInS	promítnout
v	v	k7c6	v
nařízení	nařízení	k1gNnSc6	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
765	[number]	k4	765
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
akreditaci	akreditace	k1gFnSc4	akreditace
a	a	k8xC	a
dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
trhem	trh	k1gInSc7	trh
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
uvádění	uvádění	k1gNnSc4	uvádění
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
předpisů	předpis	k1gInPc2	předpis
tzv.	tzv.	kA	tzv.
Nového	Nového	k2eAgInSc2d1	Nového
legislativního	legislativní	k2eAgInSc2d1	legislativní
rámce	rámec	k1gInSc2	rámec
(	(	kIx(	(
<g/>
NLF	NLF	kA	NLF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dokumentech	dokument	k1gInPc6	dokument
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
akreditace	akreditace	k1gFnSc2	akreditace
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
odborné	odborný	k2eAgFnSc6d1	odborná
způsobilosti	způsobilost	k1gFnSc6	způsobilost
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
shodu	shoda	k1gFnSc4	shoda
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
požadavky	požadavek	k1gInPc7	požadavek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
akreditační	akreditační	k2eAgInSc4d1	akreditační
systém	systém	k1gInSc4	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
norma	norma	k1gFnSc1	norma
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17011	[number]	k4	17011
ukládá	ukládat	k5eAaImIp3nS	ukládat
akreditačním	akreditační	k2eAgMnPc3d1	akreditační
orgánům	orgán	k1gMnPc3	orgán
povinnost	povinnost	k1gFnSc4	povinnost
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
,	,	kIx,	,
realizovat	realizovat	k5eAaBmF	realizovat
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
systém	systém	k1gInSc4	systém
managementu	management	k1gInSc2	management
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
jeho	jeho	k3xOp3gFnSc4	jeho
efektivnost	efektivnost	k1gFnSc4	efektivnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
činnost	činnost	k1gFnSc1	činnost
akreditačního	akreditační	k2eAgInSc2d1	akreditační
orgánu	orgán	k1gInSc2	orgán
byly	být	k5eAaImAgFnP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
akreditace	akreditace	k1gFnPc1	akreditace
budou	být	k5eAaImBp3nP	být
důvěryhodné	důvěryhodný	k2eAgFnPc1d1	důvěryhodná
<g/>
.	.	kIx.	.
</s>
<s>
Dodržování	dodržování	k1gNnSc1	dodržování
této	tento	k3xDgFnSc2	tento
normy	norma	k1gFnSc2	norma
je	být	k5eAaImIp3nS	být
kontrolováno	kontrolovat	k5eAaImNgNnS	kontrolovat
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
hodnocením	hodnocení	k1gNnSc7	hodnocení
–	–	k?	–
evaluací	evaluace	k1gFnPc2	evaluace
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
norma	norma	k1gFnSc1	norma
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17011	[number]	k4	17011
<g/>
:	:	kIx,	:
<g/>
2004	[number]	k4	2004
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
české	český	k2eAgFnSc2d1	Česká
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17011	[number]	k4	17011
<g/>
:	:	kIx,	:
<g/>
2005	[number]	k4	2005
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
revidovaná	revidovaný	k2eAgFnSc1d1	revidovaná
verze	verze	k1gFnSc1	verze
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17011	[number]	k4	17011
<g/>
:	:	kIx,	:
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
je	být	k5eAaImIp3nS	být
závazná	závazný	k2eAgFnSc1d1	závazná
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
akreditační	akreditační	k2eAgInPc4d1	akreditační
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
signatáře	signatář	k1gMnSc2	signatář
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
výsledků	výsledek	k1gInPc2	výsledek
akreditace	akreditace	k1gFnSc2	akreditace
MLA	MLA	kA	MLA
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
i	i	k8xC	i
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
systém	systém	k1gInSc4	systém
managementu	management	k1gInSc2	management
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jakosti	jakost	k1gFnSc2	jakost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
cyklů	cyklus	k1gInPc2	cyklus
zlepšování	zlepšování	k1gNnSc2	zlepšování
<g/>
.	.	kIx.	.
</s>
<s>
Postupy	postup	k1gInPc1	postup
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
založeny	založit	k5eAaPmNgInP	založit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
na	na	k7c6	na
formulovaných	formulovaný	k2eAgFnPc6d1	formulovaná
zásadách	zásada	k1gFnPc6	zásada
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
politikách	politikum	k1gNnPc6	politikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
vedení	vedení	k1gNnSc1	vedení
ČIA	ČIA	kA	ČIA
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
a	a	k8xC	a
dokumentovalo	dokumentovat	k5eAaBmAgNnS	dokumentovat
politiky	politik	k1gMnPc4	politik
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
činností	činnost	k1gFnPc2	činnost
včetně	včetně	k7c2	včetně
politiky	politika	k1gFnSc2	politika
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
rámec	rámec	k1gInSc1	rámec
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
rozvoje	rozvoj	k1gInSc2	rozvoj
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Koncepce	koncepce	k1gFnSc1	koncepce
ČIA	ČIA	kA	ČIA
(	(	kIx(	(
<g/>
mise	mise	k1gFnSc2	mise
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
formulovanou	formulovaný	k2eAgFnSc4d1	formulovaná
koncepci	koncepce	k1gFnSc4	koncepce
přijalo	přijmout	k5eAaPmAgNnS	přijmout
vedení	vedení	k1gNnSc3	vedení
strategický	strategický	k2eAgInSc4d1	strategický
rámec	rámec	k1gInSc4	rámec
a	a	k8xC	a
vizi	vize	k1gFnSc4	vize
rozvoje	rozvoj	k1gInSc2	rozvoj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
kvality	kvalita	k1gFnSc2	kvalita
ČIA	ČIA	kA	ČIA
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
,	,	kIx,	,
vize	vize	k1gFnSc2	vize
<g/>
,	,	kIx,	,
strategického	strategický	k2eAgInSc2d1	strategický
rámce	rámec	k1gInSc2	rámec
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17011	[number]	k4	17011
a	a	k8xC	a
čl	čl	kA	čl
<g/>
.	.	kIx.	.
8	[number]	k4	8
nařízení	nařízení	k1gNnSc2	nařízení
ES	ES	kA	ES
<g/>
.	.	kIx.	.
</s>
<s>
Politiky	politika	k1gFnPc1	politika
a	a	k8xC	a
cíle	cíl	k1gInPc1	cíl
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
činností	činnost	k1gFnPc2	činnost
ČIA	ČIA	kA	ČIA
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
politiky	politika	k1gFnSc2	politika
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
kapitolách	kapitola	k1gFnPc6	kapitola
příručky	příručka	k1gFnSc2	příručka
kvality	kvalita	k1gFnSc2	kvalita
ČIA	ČIA	kA	ČIA
nebo	nebo	k8xC	nebo
v	v	k7c6	v
související	související	k2eAgFnSc6d1	související
řízené	řízený	k2eAgFnSc6d1	řízená
dokumentaci	dokumentace	k1gFnSc6	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
strategický	strategický	k2eAgInSc4d1	strategický
rámec	rámec	k1gInSc4	rámec
ČIA	ČIA	kA	ČIA
navazují	navazovat	k5eAaImIp3nP	navazovat
funkční	funkční	k2eAgFnPc1d1	funkční
strategie	strategie	k1gFnPc1	strategie
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
jsou	být	k5eAaImIp3nP	být
stanoveny	stanoven	k2eAgInPc1d1	stanoven
měřitelné	měřitelný	k2eAgInPc1d1	měřitelný
cíle	cíl	k1gInPc1	cíl
činností	činnost	k1gFnPc2	činnost
ČIA	ČIA	kA	ČIA
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
specifických	specifický	k2eAgFnPc6d1	specifická
oblastech	oblast	k1gFnPc6	oblast
strategického	strategický	k2eAgNnSc2d1	strategické
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanovené	stanovený	k2eAgInPc4d1	stanovený
roky	rok	k1gInPc4	rok
jsou	být	k5eAaImIp3nP	být
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
činnosti	činnost	k1gFnSc2	činnost
ČIA	ČIA	kA	ČIA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nastiňují	nastiňovat	k5eAaImIp3nP	nastiňovat
rámcový	rámcový	k2eAgInSc4d1	rámcový
plán	plán	k1gInSc4	plán
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
posléze	posléze	k6eAd1	posléze
projednávány	projednávat	k5eAaImNgInP	projednávat
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
,	,	kIx,	,
dozorčí	dozorčí	k2eAgFnSc6d1	dozorčí
a	a	k8xC	a
správní	správní	k2eAgFnSc6d1	správní
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
na	na	k7c4	na
businessinfo	businessinfo	k1gNnSc4	businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17020	[number]	k4	17020
<g/>
,	,	kIx,	,
Posuzování	posuzování	k1gNnSc1	posuzování
shody	shoda	k1gFnSc2	shoda
-	-	kIx~	-
Požadavky	požadavek	k1gInPc1	požadavek
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
orgánů	orgán	k1gInPc2	orgán
provádějících	provádějící	k2eAgInPc2d1	provádějící
inspekci	inspekce	k1gFnSc4	inspekce
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17021	[number]	k4	17021
<g/>
,	,	kIx,	,
Posuzování	posuzování	k1gNnSc1	posuzování
shody	shoda	k1gFnSc2	shoda
–	–	k?	–
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
orgány	orgán	k1gInPc4	orgán
poskytující	poskytující	k2eAgFnSc2d1	poskytující
služby	služba	k1gFnSc2	služba
auditů	audit	k1gInPc2	audit
a	a	k8xC	a
certifikace	certifikace	k1gFnSc2	certifikace
systémů	systém	k1gInPc2	systém
managementu	management	k1gInSc2	management
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17025	[number]	k4	17025
<g/>
,	,	kIx,	,
Posuzování	posuzování	k1gNnSc1	posuzování
shody	shoda	k1gFnSc2	shoda
-	-	kIx~	-
Všeobecné	všeobecný	k2eAgInPc1d1	všeobecný
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
způsobilost	způsobilost	k1gFnSc4	způsobilost
zkušebních	zkušební	k2eAgFnPc2d1	zkušební
a	a	k8xC	a
kalibračních	kalibrační	k2eAgFnPc2d1	kalibrační
laboratoří	laboratoř	k1gFnPc2	laboratoř
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17043	[number]	k4	17043
<g/>
,	,	kIx,	,
Posuzování	posuzování	k1gNnSc1	posuzování
shody	shoda	k1gFnSc2	shoda
-	-	kIx~	-
Všeobecné	všeobecný	k2eAgInPc1d1	všeobecný
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
zkoušení	zkoušení	k1gNnSc4	zkoušení
způsobilosti	způsobilost	k1gFnSc2	způsobilost
</s>
</p>
