<s>
Posláním	poslání	k1gNnSc7	poslání
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
činnosti	činnost	k1gFnSc2	činnost
Českého	český	k2eAgInSc2d1	český
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
akreditaci	akreditace	k1gFnSc4	akreditace
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
ČIA	ČIA	kA	ČIA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poskytování	poskytování	k1gNnSc1	poskytování
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
akreditace	akreditace	k1gFnSc2	akreditace
komerčním	komerční	k2eAgInPc3d1	komerční
subjektům	subjekt	k1gInPc3	subjekt
a	a	k8xC	a
orgánům	orgán	k1gInPc3	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
