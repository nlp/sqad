<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Space	Space	k1gFnSc2	Space
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
́	́	k?	́
<g/>
д	д	k?	д
к	к	k?	к
<g/>
́	́	k?	́
<g/>
ч	ч	k?	ч
с	с	k?	с
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jediná	jediný	k2eAgFnSc1d1	jediná
trvale	trvale	k6eAd1	trvale
obydlená	obydlený	k2eAgFnSc1d1	obydlená
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
modul	modul	k1gInSc1	modul
Zarja	Zarjum	k1gNnSc2	Zarjum
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vynesen	vynést	k5eAaPmNgInS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
první	první	k4xOgFnSc1	první
stálá	stálý	k2eAgFnSc1d1	stálá
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
obydlena	obydlen	k2eAgFnSc1d1	obydlena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
každých	každý	k3xTgInPc2	každý
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
obměňuje	obměňovat	k5eAaImIp3nS	obměňovat
<g/>
,	,	kIx,	,
tvořena	tvořen	k2eAgFnSc1d1	tvořena
6	[number]	k4	6
členy	člen	k1gInPc7	člen
(	(	kIx(	(
<g/>
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
400	[number]	k4	400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
rychlosti	rychlost	k1gFnSc6	rychlost
okolo	okolo	k7c2	okolo
7	[number]	k4	7
700	[number]	k4	700
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
27	[number]	k4	27
720	[number]	k4	720
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
pravidelně	pravidelně	k6eAd1	pravidelně
obíhá	obíhat	k5eAaImIp3nS	obíhat
Zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
cca	cca	kA	cca
92	[number]	k4	92
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
ISS	ISS	kA	ISS
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
sloučení	sloučení	k1gNnSc4	sloučení
předchozích	předchozí	k2eAgFnPc2d1	předchozí
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnSc2d1	ruská
stanice	stanice	k1gFnSc2	stanice
Mir	mir	k1gInSc4	mir
2	[number]	k4	2
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
Freedom	Freedom	k1gInSc4	Freedom
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ruských	ruský	k2eAgInPc2d1	ruský
a	a	k8xC	a
amerických	americký	k2eAgInPc2d1	americký
modulů	modul	k1gInPc2	modul
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
pro	pro	k7c4	pro
tyto	tento	k3xDgMnPc4	tento
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
připojen	připojit	k5eAaPmNgInS	připojit
evropský	evropský	k2eAgInSc1d1	evropský
laboratorní	laboratorní	k2eAgInSc1d1	laboratorní
modul	modul	k1gInSc1	modul
Columbus	Columbus	k1gInSc1	Columbus
a	a	k8xC	a
japonský	japonský	k2eAgInSc1d1	japonský
laboratorní	laboratorní	k2eAgInSc1d1	laboratorní
modul	modul	k1gInSc1	modul
Kibó	Kibó	k1gFnSc2	Kibó
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
projektem	projekt	k1gInSc7	projekt
pěti	pět	k4xCc2	pět
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
<g/>
:	:	kIx,	:
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
Roskosmos	Roskosmos	k1gMnSc1	Roskosmos
<g/>
,	,	kIx,	,
JAXA	JAXA	kA	JAXA
<g/>
,	,	kIx,	,
CSA	CSA	kA	CSA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
11	[number]	k4	11
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
;	;	kIx,	;
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
musí	muset	k5eAaImIp3nP	muset
mluvit	mluvit	k5eAaImF	mluvit
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
rusky	rusky	k6eAd1	rusky
<g/>
.	.	kIx.	.
<g/>
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
kontraktu	kontrakt	k1gInSc2	kontrakt
s	s	k7c7	s
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Italská	italský	k2eAgFnSc1d1	italská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
má	mít	k5eAaImIp3nS	mít
oddělené	oddělený	k2eAgFnPc4d1	oddělená
smlouvy	smlouva	k1gFnPc4	smlouva
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nedělá	dělat	k5eNaImIp3nS	dělat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
úloh	úloha	k1gFnPc2	úloha
Evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
ISS	ISS	kA	ISS
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
je	být	k5eAaImIp3nS	být
Itálie	Itálie	k1gFnSc2	Itálie
také	také	k9	také
plným	plný	k2eAgMnSc7d1	plný
účastníkem	účastník	k1gMnSc7	účastník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopravu	doprava	k1gFnSc4	doprava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
transportní	transportní	k2eAgFnPc4d1	transportní
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
.	.	kIx.	.
</s>
<s>
Zásobování	zásobování	k1gNnPc1	zásobování
stanice	stanice	k1gFnSc2	stanice
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
automatické	automatický	k2eAgFnPc4d1	automatická
nákladní	nákladní	k2eAgFnPc4d1	nákladní
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
–	–	k?	–
ruské	ruský	k2eAgMnPc4d1	ruský
Progressy	Progress	k1gMnPc4	Progress
<g/>
,	,	kIx,	,
americké	americký	k2eAgMnPc4d1	americký
Dragony	Dragon	k1gMnPc4	Dragon
a	a	k8xC	a
Cygnusy	Cygnus	k1gMnPc4	Cygnus
a	a	k8xC	a
japonské	japonský	k2eAgFnSc2d1	japonská
HTV	HTV	kA	HTV
<g/>
.	.	kIx.	.
</s>
<s>
Kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
i	i	k8xC	i
zásoby	zásoba	k1gFnPc4	zásoba
dříve	dříve	k6eAd2	dříve
vozily	vozit	k5eAaImAgInP	vozit
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
i	i	k8xC	i
americké	americký	k2eAgInPc4d1	americký
raketoplány	raketoplán	k1gInPc4	raketoplán
Space	Spaka	k1gFnSc3	Spaka
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
,	,	kIx,	,
zásoby	zásoba	k1gFnPc4	zásoba
evropské	evropský	k2eAgFnSc2d1	Evropská
automatické	automatický	k2eAgFnSc2d1	automatická
lodi	loď	k1gFnSc2	loď
ATV	ATV	kA	ATV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
životnost	životnost	k1gFnSc1	životnost
ISS	ISS	kA	ISS
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
finanční	finanční	k2eAgNnSc1d1	finanční
krytí	krytí	k1gNnSc1	krytí
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2024	[number]	k4	2024
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
plán	plán	k1gInSc4	plán
stavby	stavba	k1gFnSc2	stavba
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Freedom	Freedom	k1gInSc4	Freedom
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
roky	rok	k1gInPc1	rok
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
oddalování	oddalování	k1gNnSc2	oddalování
začátku	začátek	k1gInSc2	začátek
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
k	k	k7c3	k
plánu	plán	k1gInSc3	plán
výstavby	výstavba	k1gFnSc2	výstavba
připojili	připojit	k5eAaPmAgMnP	připojit
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
připojilo	připojit	k5eAaPmAgNnS	připojit
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
nazývat	nazývat	k5eAaImF	nazývat
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1995	[number]	k4	1995
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
9	[number]	k4	9
zkušebních	zkušební	k2eAgNnPc2d1	zkušební
spojení	spojení	k1gNnPc2	spojení
s	s	k7c7	s
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
stanicí	stanice	k1gFnSc7	stanice
Mir	Mira	k1gFnPc2	Mira
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
trénovalo	trénovat	k5eAaImAgNnS	trénovat
připojování	připojování	k1gNnSc1	připojování
a	a	k8xC	a
výměny	výměna	k1gFnPc1	výměna
posádek	posádka	k1gFnPc2	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
start	start	k1gInSc1	start
prvního	první	k4xOgInSc2	první
modulu	modul	k1gInSc2	modul
Zarja	Zarja	k1gMnSc1	Zarja
byl	být	k5eAaImAgMnS	být
zpožděn	zpozdit	k5eAaPmNgMnS	zpozdit
opět	opět	k6eAd1	opět
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
raději	rád	k6eAd2	rád
celý	celý	k2eAgInSc1d1	celý
zaplacen	zaplacen	k2eAgInSc1d1	zaplacen
americkou	americký	k2eAgFnSc7d1	americká
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
raketa	raketa	k1gFnSc1	raketa
Proton	proton	k1gInSc1	proton
se	s	k7c7	s
zmiňovaným	zmiňovaný	k2eAgInSc7d1	zmiňovaný
modulem	modul	k1gInSc7	modul
Zarja	Zarj	k1gInSc2	Zarj
z	z	k7c2	z
Bajkonuru	Bajkonur	k1gInSc2	Bajkonur
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
ISS	ISS	kA	ISS
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
16	[number]	k4	16
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
se	s	k7c7	s
Zarjou	Zarja	k1gFnSc7	Zarja
setkal	setkat	k5eAaPmAgInS	setkat
raketoplán	raketoplán	k1gInSc1	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nákladovém	nákladový	k2eAgInSc6d1	nákladový
prostoru	prostor	k1gInSc6	prostor
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
připojil	připojit	k5eAaPmAgInS	připojit
modul	modul	k1gInSc1	modul
Unity	unita	k1gMnSc2	unita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modul	modul	k1gInSc1	modul
Zarja	Zarj	k1gInSc2	Zarj
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgInS	sloužit
pro	pro	k7c4	pro
zajišťování	zajišťování	k1gNnSc4	zajišťování
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
především	především	k6eAd1	především
jako	jako	k8xS	jako
skladovací	skladovací	k2eAgInSc1d1	skladovací
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
zásobník	zásobník	k1gInSc1	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
Unity	unita	k1gMnSc2	unita
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
křižovatka	křižovatka	k1gFnSc1	křižovatka
modulů	modul	k1gInPc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
být	být	k5eAaImF	být
připojeno	připojit	k5eAaPmNgNnS	připojit
až	až	k9	až
6	[number]	k4	6
dalších	další	k2eAgInPc2d1	další
modulů	modul	k1gInPc2	modul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
vypuštěné	vypuštěný	k2eAgInPc1d1	vypuštěný
moduly	modul	k1gInPc1	modul
nebyly	být	k5eNaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
nutné	nutný	k2eAgFnPc1d1	nutná
časté	častý	k2eAgFnPc1d1	častá
korekce	korekce	k1gFnPc1	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Čekalo	čekat	k5eAaImAgNnS	čekat
se	se	k3xPyFc4	se
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
modul	modul	k1gInSc4	modul
–	–	k?	–
ruská	ruský	k2eAgFnSc1d1	ruská
Zvezda	Zvezda	k1gFnSc1	Zvezda
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
finanční	finanční	k2eAgFnPc1d1	finanční
potíže	potíž	k1gFnPc1	potíž
způsobily	způsobit	k5eAaPmAgFnP	způsobit
zpoždění	zpoždění	k1gNnSc4	zpoždění
jejího	její	k3xOp3gNnSc2	její
vypuštění	vypuštění	k1gNnSc2	vypuštění
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
pokračování	pokračování	k1gNnSc3	pokračování
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
byl	být	k5eAaImAgInS	být
vypuštěn	vypustit	k5eAaPmNgInS	vypustit
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2000	[number]	k4	2000
a	a	k8xC	a
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
modulu	modul	k1gInSc3	modul
Zarja	Zarjum	k1gNnSc2	Zarjum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
tvořil	tvořit	k5eAaImAgInS	tvořit
základ	základ	k1gInSc1	základ
ruské	ruský	k2eAgFnSc2d1	ruská
části	část	k1gFnSc2	část
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
servisní	servisní	k2eAgInSc1d1	servisní
modul	modul	k1gInSc1	modul
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
dočasný	dočasný	k2eAgInSc4d1	dočasný
obytný	obytný	k2eAgInSc4d1	obytný
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dodávky	dodávka	k1gFnSc2	dodávka
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
telekomunikaci	telekomunikace	k1gFnSc4	telekomunikace
s	s	k7c7	s
pozemními	pozemní	k2eAgNnPc7d1	pozemní
středisky	středisko	k1gNnPc7	středisko
a	a	k8xC	a
korekce	korekce	k1gFnSc1	korekce
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
mohla	moct	k5eAaImAgFnS	moct
stanici	stanice	k1gFnSc4	stanice
navštívit	navštívit	k5eAaPmF	navštívit
první	první	k4xOgFnSc1	první
stálá	stálý	k2eAgFnSc1d1	stálá
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
byla	být	k5eAaImAgFnS	být
aktivace	aktivace	k1gFnSc1	aktivace
a	a	k8xC	a
zabydlení	zabydlení	k1gNnSc1	zabydlení
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
dále	daleko	k6eAd2	daleko
vybalovali	vybalovat	k5eAaImAgMnP	vybalovat
uložené	uložený	k2eAgFnPc4d1	uložená
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
na	na	k7c6	na
přijetí	přijetí	k1gNnSc6	přijetí
raketoplánu	raketoplán	k1gInSc2	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
modulem	modul	k1gInSc7	modul
Destiny	Destina	k1gFnSc2	Destina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
modul	modul	k1gInSc1	modul
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2001	[number]	k4	2001
k	k	k7c3	k
modulu	modul	k1gInSc3	modul
Unity	unita	k1gMnSc2	unita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modul	modul	k1gInSc1	modul
Destiny	Destina	k1gFnSc2	Destina
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
vědeckých	vědecký	k2eAgInPc2d1	vědecký
experimentů	experiment	k1gInPc2	experiment
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
oživili	oživit	k5eAaPmAgMnP	oživit
modul	modul	k1gInSc4	modul
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c4	na
raketoplán	raketoplán	k1gInSc4	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
přivezl	přivézt	k5eAaPmAgMnS	přivézt
novou	nový	k2eAgFnSc4d1	nová
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
nákladní	nákladní	k2eAgInSc4d1	nákladní
modul	modul	k1gInSc4	modul
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Raketoplán	raketoplán	k1gInSc1	raketoplán
naložil	naložit	k5eAaPmAgInS	naložit
modul	modul	k1gInSc4	modul
plný	plný	k2eAgInSc4d1	plný
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
první	první	k4xOgFnSc7	první
posádkou	posádka	k1gFnSc7	posádka
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
robotický	robotický	k2eAgMnSc1d1	robotický
manipulátor	manipulátor	k1gMnSc1	manipulátor
Canadarm	Canadarm	k1gInSc4	Canadarm
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přivezl	přivézt	k5eAaPmAgInS	přivézt
raketoplán	raketoplán	k1gInSc1	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Robotický	robotický	k2eAgInSc1d1	robotický
manipulátor	manipulátor	k1gInSc1	manipulátor
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
17,6	[number]	k4	17,6
m	m	kA	m
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přemisťování	přemisťování	k1gNnSc3	přemisťování
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
nákladu	náklad	k1gInSc2	náklad
podél	podél	k7c2	podél
příhradové	příhradový	k2eAgFnSc2d1	příhradová
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jemné	jemný	k2eAgFnPc4d1	jemná
a	a	k8xC	a
přesné	přesný	k2eAgFnPc4d1	přesná
montážní	montážní	k2eAgFnPc4d1	montážní
práce	práce	k1gFnPc4	práce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
k	k	k7c3	k
manipulátoru	manipulátor	k1gInSc3	manipulátor
připojit	připojit	k5eAaPmF	připojit
robotickou	robotický	k2eAgFnSc4d1	robotická
nástavbu	nástavba	k1gFnSc4	nástavba
Dextre	Dextr	k1gMnSc5	Dextr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbie	Columbia	k1gFnSc2	Columbia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několikaletému	několikaletý	k2eAgNnSc3d1	několikaleté
pozastavení	pozastavení	k1gNnSc3	pozastavení
letů	let	k1gInPc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc1	stanice
tak	tak	k6eAd1	tak
nabrala	nabrat	k5eAaPmAgFnS	nabrat
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
<g/>
leté	letý	k2eAgFnPc4d1	letá
zpoždění	zpoždění	k1gNnPc4	zpoždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
plném	plný	k2eAgNnSc6d1	plné
obnovení	obnovení	k1gNnSc6	obnovení
letů	let	k1gInPc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
připojeny	připojit	k5eAaPmNgFnP	připojit
dvě	dva	k4xCgFnPc1	dva
další	další	k2eAgFnPc1d1	další
laboratoře	laboratoř	k1gFnPc1	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
komplet	komplet	k1gInSc1	komplet
Kibó	Kibó	k1gFnSc2	Kibó
a	a	k8xC	a
evropský	evropský	k2eAgInSc4d1	evropský
laboratorní	laboratorní	k2eAgInSc4d1	laboratorní
modul	modul	k1gInSc4	modul
Columbus	Columbus	k1gInSc4	Columbus
s	s	k7c7	s
venkovní	venkovní	k2eAgFnSc7d1	venkovní
plošinou	plošina	k1gFnSc7	plošina
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
součástmi	součást	k1gFnPc7	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
stanice	stanice	k1gFnSc2	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
stanice	stanice	k1gFnSc2	stanice
ISS	ISS	kA	ISS
byla	být	k5eAaImAgFnS	být
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
montážních	montážní	k2eAgInPc2d1	montážní
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
obstaraly	obstarat	k5eAaPmAgFnP	obstarat
35	[number]	k4	35
americké	americký	k2eAgInPc4d1	americký
raketoplány	raketoplán	k1gInPc4	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
starty	start	k1gInPc1	start
jsou	být	k5eAaImIp3nP	být
zabezpečovány	zabezpečovat	k5eAaImNgInP	zabezpečovat
klasickými	klasický	k2eAgFnPc7d1	klasická
raketami	raketa	k1gFnPc7	raketa
jako	jako	k8xC	jako
Proton	proton	k1gInSc1	proton
a	a	k8xC	a
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
doplněk	doplněk	k1gInSc1	doplněk
montážních	montážní	k2eAgInPc2d1	montážní
letů	let	k1gInPc2	let
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
září	září	k1gNnSc2	září
2017	[number]	k4	2017
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
67	[number]	k4	67
letů	let	k1gInPc2	let
bezpilotní	bezpilotní	k2eAgFnSc2d1	bezpilotní
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodě	loď	k1gFnSc2	loď
Progress	Progressa	k1gFnPc2	Progressa
<g/>
,	,	kIx,	,
6	[number]	k4	6
letů	let	k1gInPc2	let
japonské	japonský	k2eAgFnSc2d1	japonská
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodi	loď	k1gFnSc2	loď
HTV	HTV	kA	HTV
<g/>
,	,	kIx,	,
5	[number]	k4	5
letů	let	k1gInPc2	let
evropské	evropský	k2eAgFnSc2d1	Evropská
automatizované	automatizovaný	k2eAgFnSc2d1	automatizovaná
bezpilotní	bezpilotní	k2eAgFnSc2d1	bezpilotní
lodi	loď	k1gFnSc2	loď
ATV	ATV	kA	ATV
<g/>
,	,	kIx,	,
6	[number]	k4	6
letů	let	k1gInPc2	let
americké	americký	k2eAgFnSc2d1	americká
automatické	automatický	k2eAgFnSc2d1	automatická
zásobovací	zásobovací	k2eAgFnSc2d1	zásobovací
lodi	loď	k1gFnSc2	loď
Cygnus	Cygnus	k1gInSc1	Cygnus
a	a	k8xC	a
12	[number]	k4	12
letů	let	k1gInPc2	let
zásobovací	zásobovací	k2eAgFnSc2d1	zásobovací
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
ověřována	ověřován	k2eAgFnSc1d1	ověřována
nová	nový	k2eAgFnSc1d1	nová
koncepce	koncepce	k1gFnSc1	koncepce
tzv.	tzv.	kA	tzv.
hlavního	hlavní	k2eAgInSc2d1	hlavní
nosníku	nosník	k1gInSc2	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
m	m	kA	m
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
příhradové	příhradový	k2eAgFnSc6d1	příhradová
konstrukci	konstrukce	k1gFnSc6	konstrukce
tvořící	tvořící	k2eAgFnSc4d1	tvořící
kostru	kostra	k1gFnSc4	kostra
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
Integrated	Integrated	k1gInSc1	Integrated
Truss	Trussa	k1gFnPc2	Trussa
Structure	Structur	k1gMnSc5	Structur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
konstrukce	konstrukce	k1gFnSc2	konstrukce
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
pár	pár	k4xCyI	pár
fotovoltaických	fotovoltaický	k2eAgInPc2d1	fotovoltaický
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nosníku	nosník	k1gInSc3	nosník
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
středu	středa	k1gFnSc4	středa
připevněny	připevněn	k2eAgInPc4d1	připevněn
vlastní	vlastní	k2eAgInPc4d1	vlastní
hermetizované	hermetizovaný	k2eAgInPc4d1	hermetizovaný
moduly	modul	k1gInPc4	modul
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
části	část	k1gFnPc4	část
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
usnadnění	usnadnění	k1gNnSc3	usnadnění
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
redukci	redukce	k1gFnSc4	redukce
výstupů	výstup	k1gInPc2	výstup
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
podél	podél	k7c2	podél
hlavního	hlavní	k2eAgInSc2d1	hlavní
nosníku	nosník	k1gInSc2	nosník
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
mobilní	mobilní	k2eAgInSc1d1	mobilní
servisní	servisní	k2eAgInSc1d1	servisní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
servisní	servisní	k2eAgNnSc4d1	servisní
robotické	robotický	k2eAgNnSc4d1	robotické
rameno	rameno	k1gNnSc4	rameno
Canadarm	Canadarm	k1gInSc1	Canadarm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dokončení	dokončení	k1gNnSc6	dokončení
má	mít	k5eAaImIp3nS	mít
ISS	ISS	kA	ISS
celkový	celkový	k2eAgInSc1d1	celkový
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
přetlakový	přetlakový	k2eAgInSc1d1	přetlakový
objem	objem	k1gInSc1	objem
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
m3	m3	k4	m3
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
okolo	okolo	k7c2	okolo
450	[number]	k4	450
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
moduly	modul	k1gInPc4	modul
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
mnoho	mnoho	k4c1	mnoho
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
fotovoltaických	fotovoltaický	k2eAgInPc2d1	fotovoltaický
článků	článek	k1gInPc2	článek
o	o	k7c6	o
energetickém	energetický	k2eAgInSc6d1	energetický
výkonu	výkon	k1gInSc6	výkon
110	[number]	k4	110
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
108,4	[number]	k4	108,4
metru	metr	k1gInSc2	metr
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
pak	pak	k6eAd1	pak
74	[number]	k4	74
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zrušené	zrušený	k2eAgInPc1d1	zrušený
moduly	modul	k1gInPc1	modul
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
modulů	modul	k1gInPc2	modul
nebyly	být	k5eNaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
realizovány	realizovat	k5eAaBmNgFnP	realizovat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
či	či	k8xC	či
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zbytečnými	zbytečný	k2eAgFnPc7d1	zbytečná
nebo	nebo	k8xC	nebo
po	po	k7c6	po
zkáze	zkáza	k1gFnSc6	zkáza
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zrušené	zrušený	k2eAgInPc4d1	zrušený
moduly	modul	k1gInPc4	modul
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
USA	USA	kA	USA
Centrifuge	Centrifuge	k1gFnSc1	Centrifuge
Accommodations	Accommodations	k1gInSc1	Accommodations
Module	modul	k1gInSc5	modul
<g/>
,	,	kIx,	,
modul	modul	k1gInSc1	modul
pro	pro	k7c4	pro
experimenty	experiment	k1gInPc4	experiment
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
umělé	umělý	k2eAgFnSc2d1	umělá
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Habitation	Habitation	k1gInSc1	Habitation
Module	modul	k1gInSc5	modul
<g/>
,	,	kIx,	,
ubytovací	ubytovací	k2eAgInSc1d1	ubytovací
modul	modul	k1gInSc1	modul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
obytné	obytný	k2eAgInPc4d1	obytný
prostory	prostor	k1gInPc4	prostor
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
testován	testován	k2eAgMnSc1d1	testován
je	být	k5eAaImIp3nS	být
nafukovací	nafukovací	k2eAgInSc1d1	nafukovací
modul	modul	k1gInSc1	modul
BEAM	BEAM	kA	BEAM
(	(	kIx(	(
<g/>
Bigelow	Bigelow	k1gMnSc1	Bigelow
Expandable	Expandable	k1gMnSc2	Expandable
Activity	Activita	k1gFnSc2	Activita
Module	modul	k1gInSc5	modul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vynesla	vynést	k5eAaPmAgFnS	vynést
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
8	[number]	k4	8
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnPc1	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
<g/>
.	.	kIx.	.
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Crew	Crew	k1gFnSc1	Crew
Return	Return	k1gMnSc1	Return
Vehicle	Vehicle	k1gInSc1	Vehicle
<g/>
,	,	kIx,	,
miniraketoplán	miniraketoplán	k1gInSc1	miniraketoplán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
záchranné	záchranný	k2eAgNnSc1d1	záchranné
plavidlo	plavidlo	k1gNnSc1	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
nyní	nyní	k6eAd1	nyní
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodě	loď	k1gFnPc4	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
.	.	kIx.	.
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Interim	interim	k1gInSc4	interim
Control	Controla	k1gFnPc2	Controla
Module	modul	k1gInSc5	modul
<g/>
,	,	kIx,	,
nouzový	nouzový	k2eAgInSc1d1	nouzový
řídící	řídící	k2eAgInSc1d1	řídící
a	a	k8xC	a
pohonný	pohonný	k2eAgInSc1d1	pohonný
modul	modul	k1gInSc1	modul
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
nahradit	nahradit	k5eAaPmF	nahradit
funkcí	funkce	k1gFnSc7	funkce
Zvezda	Zvezd	k1gMnSc2	Zvezd
v	v	k7c6	v
případě	případ	k1gInSc6	případ
selhání	selhání	k1gNnSc1	selhání
<g/>
.	.	kIx.	.
<g/>
Ruský	ruský	k2eAgMnSc1d1	ruský
<g/>
,	,	kIx,	,
Universal	Universal	k1gFnSc1	Universal
Docking	Docking	k1gInSc1	Docking
Module	modul	k1gInSc5	modul
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgInSc4d1	univerzální
spojovací	spojovací	k2eAgInSc4d1	spojovací
modul	modul	k1gInSc4	modul
<g/>
.	.	kIx.	.
<g/>
Ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
,	,	kIx,	,
Science	Science	k1gFnSc2	Science
Power	Powra	k1gFnPc2	Powra
platform	platform	k1gInSc1	platform
<g/>
,	,	kIx,	,
solární	solární	k2eAgInPc1d1	solární
panely	panel	k1gInPc1	panel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
napájení	napájení	k1gNnSc4	napájení
ruského	ruský	k2eAgInSc2d1	ruský
orbitálního	orbitální	k2eAgInSc2d1	orbitální
segmentu	segment	k1gInSc2	segment
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgInPc4d1	nezávislý
na	na	k7c4	na
ITS	ITS	kA	ITS
<g/>
.	.	kIx.	.
<g/>
Dva	dva	k4xCgInPc1	dva
ruské	ruský	k2eAgInPc1d1	ruský
výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
moduly	modul	k1gInPc1	modul
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
naplánovány	naplánován	k2eAgInPc1d1	naplánován
pro	pro	k7c4	pro
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Transportní	transportní	k2eAgInPc1d1	transportní
systémy	systém	k1gInPc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalé	bývalý	k2eAgNnSc1d1	bývalé
===	===	k?	===
</s>
</p>
<p>
<s>
kosmické	kosmický	k2eAgInPc1d1	kosmický
raketoplány	raketoplán	k1gInPc1	raketoplán
Space	Spaec	k1gInSc2	Spaec
Shuttle	Shuttle	k1gFnSc2	Shuttle
–	–	k?	–
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgInPc1d1	hlavní
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
udržované	udržovaný	k2eAgInPc1d1	udržovaný
americkou	americký	k2eAgFnSc7d1	americká
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nákladovém	nákladový	k2eAgInSc6d1	nákladový
prostoru	prostor	k1gInSc6	prostor
byly	být	k5eAaImAgInP	být
dopravovány	dopravován	k2eAgInPc1d1	dopravován
stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
stanice	stanice	k1gFnSc2	stanice
nebo	nebo	k8xC	nebo
přetlakové	přetlakový	k2eAgInPc4d1	přetlakový
logistické	logistický	k2eAgInPc4d1	logistický
moduly	modul	k1gInPc4	modul
MPLM	MPLM	kA	MPLM
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
skončily	skončit	k5eAaPmAgInP	skončit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ATV	ATV	kA	ATV
(	(	kIx(	(
<g/>
Automated	Automated	k1gInSc1	Automated
Transfer	transfer	k1gInSc1	transfer
Vehicle	Vehicle	k1gFnSc1	Vehicle
<g/>
)	)	kIx)	)
–	–	k?	–
Evropská	evropský	k2eAgFnSc1d1	Evropská
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
nosností	nosnost	k1gFnSc7	nosnost
než	než	k8xS	než
ruský	ruský	k2eAgInSc1d1	ruský
Progress	Progress	k1gInSc1	Progress
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
až	až	k9	až
2014	[number]	k4	2014
vypraveno	vypravit	k5eAaPmNgNnS	vypravit
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Kourou	Kourou	k?	Kourou
5	[number]	k4	5
těchto	tento	k3xDgFnPc2	tento
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současné	současný	k2eAgNnSc1d1	současné
===	===	k?	===
</s>
</p>
<p>
<s>
kosmické	kosmický	k2eAgFnPc1d1	kosmická
lodě	loď	k1gFnPc1	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
dopravní	dopravní	k2eAgInSc1d1	dopravní
a	a	k8xC	a
záchranný	záchranný	k2eAgInSc1d1	záchranný
prostředek	prostředek	k1gInSc1	prostředek
připojený	připojený	k2eAgInSc1d1	připojený
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
<g/>
,	,	kIx,	,
udržované	udržovaný	k2eAgNnSc1d1	udržované
ruskou	ruský	k2eAgFnSc7d1	ruská
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
lodě	loď	k1gFnPc1	loď
Progress	Progress	k1gInSc1	Progress
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pravidelné	pravidelný	k2eAgFnSc3d1	pravidelná
dopravě	doprava	k1gFnSc3	doprava
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
zásob	zásoba	k1gFnPc2	zásoba
(	(	kIx(	(
<g/>
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
náhradní	náhradní	k2eAgInPc1d1	náhradní
díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
pohonné	pohonný	k2eAgFnPc1d1	pohonná
hmoty	hmota	k1gFnPc1	hmota
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
motorickým	motorický	k2eAgInPc3d1	motorický
manévrům	manévr	k1gInPc3	manévr
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
udržované	udržovaný	k2eAgNnSc1d1	udržované
ruskou	ruský	k2eAgFnSc7d1	ruská
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HTV	HTV	kA	HTV
(	(	kIx(	(
<g/>
H-II	H-II	k1gFnSc1	H-II
Transfer	transfer	k1gInSc1	transfer
Vehicle	Vehicle	k1gFnSc1	Vehicle
<g/>
)	)	kIx)	)
–	–	k?	–
Japonská	japonský	k2eAgFnSc1d1	japonská
(	(	kIx(	(
<g/>
JAXA	JAXA	kA	JAXA
<g/>
)	)	kIx)	)
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
zásobování	zásobování	k1gNnSc4	zásobování
japonského	japonský	k2eAgInSc2d1	japonský
segmentu	segment	k1gInSc2	segment
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
start	start	k1gInSc1	start
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
Dragon	Dragon	k1gMnSc1	Dragon
–	–	k?	–
loď	loď	k1gFnSc1	loď
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
soukromou	soukromý	k2eAgFnSc7d1	soukromá
firmou	firma	k1gFnSc7	firma
SpaceX	SpaceX	k1gFnSc2	SpaceX
s	s	k7c7	s
<g/>
–	–	k?	–
<g/>
podporou	podpora	k1gFnSc7	podpora
NASA	NASA	kA	NASA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
"	"	kIx"	"
<g/>
Commercial	Commercial	k1gInSc1	Commercial
Orbital	orbital	k1gInSc1	orbital
Transportation	Transportation	k1gInSc1	Transportation
Services	Services	k1gInSc4	Services
–	–	k?	–
COTS	COTS	kA	COTS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Cygnus	Cygnus	k1gInSc1	Cygnus
–	–	k?	–
loď	loď	k1gFnSc1	loď
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
soukromou	soukromý	k2eAgFnSc7d1	soukromá
firmou	firma	k1gFnSc7	firma
Orbital	orbital	k1gInSc1	orbital
Sciences	Sciences	k1gMnSc1	Sciences
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
firmou	firma	k1gFnSc7	firma
Northrop	Northrop	k1gInSc4	Northrop
Grumman	Grumman	k1gMnSc1	Grumman
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
NASA	NASA	kA	NASA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
"	"	kIx"	"
<g/>
Commercial	Commercial	k1gInSc1	Commercial
Orbital	orbital	k1gInSc1	orbital
Transportation	Transportation	k1gInSc1	Transportation
Services	Services	k1gInSc4	Services
–	–	k?	–
COTS	COTS	kA	COTS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
start	start	k1gInSc1	start
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Budoucí	budoucí	k2eAgMnPc1d1	budoucí
===	===	k?	===
</s>
</p>
<p>
<s>
Dream	Dream	k6eAd1	Dream
Chaser	Chaser	k1gInSc1	Chaser
–	–	k?	–
miniraketoplán	miniraketoplán	k1gInSc1	miniraketoplán
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
firmou	firma	k1gFnSc7	firma
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
Corporation	Corporation	k1gInSc4	Corporation
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
posádek	posádka	k1gFnPc2	posádka
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
bude	být	k5eAaImBp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
zásobovací	zásobovací	k2eAgFnSc4d1	zásobovací
loď	loď	k1gFnSc4	loď
</s>
</p>
<p>
<s>
CST-100	CST-100	k4	CST-100
–	–	k?	–
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
firmou	firma	k1gFnSc7	firma
Boeing	boeing	k1gInSc4	boeing
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
dopravy	doprava	k1gFnSc2	doprava
posádek	posádka	k1gFnPc2	posádka
na	na	k7c6	na
ISS	ISS	kA	ISS
</s>
</p>
<p>
<s>
Dragon	Dragon	k1gMnSc1	Dragon
2	[number]	k4	2
–	–	k?	–
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
pilotovaná	pilotovaný	k2eAgFnSc1d1	pilotovaná
verze	verze	k1gFnSc1	verze
dopravní	dopravní	k2eAgFnSc2d1	dopravní
lodi	loď	k1gFnSc2	loď
Dragon	Dragon	k1gMnSc1	Dragon
společnosti	společnost	k1gFnSc2	společnost
SpaceX	SpaceX	k1gMnSc1	SpaceX
</s>
</p>
<p>
<s>
Federace	federace	k1gFnSc1	federace
–	–	k?	–
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
firmou	firma	k1gFnSc7	firma
RKK	RKK	kA	RKK
Eněrgija	Eněrgija	k1gMnSc1	Eněrgija
</s>
</p>
<p>
<s>
==	==	k?	==
Dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
orientace	orientace	k1gFnSc1	orientace
stanice	stanice	k1gFnSc2	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
ISS	ISS	kA	ISS
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
mírně	mírně	k6eAd1	mírně
eliptické	eliptický	k2eAgFnSc6d1	eliptická
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
400	[number]	k4	400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
dráhy	dráha	k1gFnSc2	dráha
vůči	vůči	k7c3	vůči
rovníku	rovník	k1gInSc3	rovník
je	být	k5eAaImIp3nS	být
51,6	[number]	k4	51,6
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dráha	dráha	k1gFnSc1	dráha
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
dostupnost	dostupnost	k1gFnSc4	dostupnost
z	z	k7c2	z
amerických	americký	k2eAgMnPc2d1	americký
i	i	k8xC	i
ruských	ruský	k2eAgMnPc2d1	ruský
kosmodromů	kosmodrom	k1gInPc2	kosmodrom
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
možnosti	možnost	k1gFnPc4	možnost
pozorování	pozorování	k1gNnSc2	pozorování
většiny	většina	k1gFnSc2	většina
nejobydlenějších	obydlený	k2eAgNnPc2d3	obydlený
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nepatrné	nepatrný	k2eAgInPc1d1	nepatrný
zbytky	zbytek	k1gInPc1	zbytek
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
díky	díky	k7c3	díky
tření	tření	k1gNnSc3	tření
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
snižování	snižování	k1gNnSc3	snižování
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
tak	tak	k9	tak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
periodicky	periodicky	k6eAd1	periodicky
udržována	udržovat	k5eAaImNgFnS	udržovat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
stanice	stanice	k1gFnSc1	stanice
během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
hustých	hustý	k2eAgFnPc2d1	hustá
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
shořela	shořet	k5eAaPmAgFnS	shořet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udržování	udržování	k1gNnSc1	udržování
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
raketovými	raketový	k2eAgInPc7d1	raketový
motory	motor	k1gInPc7	motor
servisního	servisní	k2eAgInSc2d1	servisní
modulu	modul	k1gInSc2	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
pomocí	pomoc	k1gFnSc7	pomoc
motorů	motor	k1gInPc2	motor
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
lodí	loď	k1gFnPc2	loď
Progress	Progressa	k1gFnPc2	Progressa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orientace	orientace	k1gFnSc1	orientace
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
je	být	k5eAaImIp3nS	být
určována	určovat	k5eAaImNgFnS	určovat
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
optimální	optimální	k2eAgFnSc4d1	optimální
polohu	poloha	k1gFnSc4	poloha
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
pozici	pozice	k1gFnSc4	pozice
radiátorů	radiátor	k1gInPc2	radiátor
chladicího	chladicí	k2eAgInSc2d1	chladicí
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
též	též	k9	též
usnadnit	usnadnit	k5eAaPmF	usnadnit
manévrování	manévrování	k1gNnSc4	manévrování
připojovaných	připojovaný	k2eAgFnPc2d1	připojovaná
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
různé	různý	k2eAgFnPc4d1	různá
orientace	orientace	k1gFnPc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgNnSc1d3	nejčastější
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Orientace	orientace	k1gFnSc1	orientace
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
modulů	modul	k1gInPc2	modul
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
letu	let	k1gInSc2	let
a	a	k8xC	a
zenitovou	zenitový	k2eAgFnSc7d1	zenitová
osou	osa	k1gFnSc7	osa
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
směřující	směřující	k2eAgFnSc2d1	směřující
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orientace	orientace	k1gFnSc1	orientace
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
hlavního	hlavní	k2eAgInSc2d1	hlavní
nosníku	nosník	k1gInSc2	nosník
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
letu	let	k1gInSc2	let
a	a	k8xC	a
zenitovou	zenitový	k2eAgFnSc7d1	zenitová
osou	osa	k1gFnSc7	osa
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
směřující	směřující	k2eAgFnSc2d1	směřující
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
stálá	stálý	k2eAgFnSc1d1	stálá
orientace	orientace	k1gFnSc1	orientace
na	na	k7c4	na
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
udržování	udržování	k1gNnSc4	udržování
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
orientace	orientace	k1gFnSc2	orientace
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
systémy	systém	k1gInPc4	systém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
gyroskopů	gyroskop	k1gInPc2	gyroskop
(	(	kIx(	(
<g/>
CMG	CMG	kA	CMG
–	–	k?	–
Control	Control	k1gInSc1	Control
Moment	moment	k1gInSc1	moment
Gyro	Gyro	k1gNnSc1	Gyro
<g/>
)	)	kIx)	)
–	–	k?	–
využívá	využívat	k5eAaImIp3nS	využívat
momentu	moment	k1gInSc3	moment
masivních	masivní	k2eAgInPc2d1	masivní
rotujících	rotující	k2eAgInPc2d1	rotující
setrvačníků	setrvačník	k1gInPc2	setrvačník
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
rychlosti	rychlost	k1gFnSc2	rychlost
jejich	jejich	k3xOp3gNnSc2	jejich
otáčení	otáčení	k1gNnSc2	otáčení
je	být	k5eAaImIp3nS	být
dosahováno	dosahovat	k5eAaImNgNnS	dosahovat
otáčení	otáčení	k1gNnSc1	otáčení
stanice	stanice	k1gFnSc2	stanice
kolem	kolem	k7c2	kolem
příslušné	příslušný	k2eAgFnSc2d1	příslušná
osy	osa	k1gFnSc2	osa
X	X	kA	X
<g/>
/	/	kIx~	/
<g/>
Y	Y	kA	Y
<g/>
/	/	kIx~	/
<g/>
Z.	Z.	kA	Z.
Využívá	využívat	k5eAaImIp3nS	využívat
pouze	pouze	k6eAd1	pouze
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
dodávané	dodávaný	k2eAgFnSc2d1	dodávaná
ze	z	k7c2	z
solárních	solární	k2eAgInPc2d1	solární
článků	článek	k1gInPc2	článek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
raketových	raketový	k2eAgFnPc2d1	raketová
korekčních	korekční	k2eAgFnPc2d1	korekční
trysek	tryska	k1gFnPc2	tryska
–	–	k?	–
využívá	využívat	k5eAaImIp3nS	využívat
malé	malý	k2eAgFnPc4d1	malá
raketové	raketový	k2eAgFnPc4d1	raketová
motorky	motorka	k1gFnPc4	motorka
na	na	k7c4	na
kapalné	kapalný	k2eAgFnPc4d1	kapalná
pohonné	pohonný	k2eAgFnPc4d1	pohonná
látky	látka	k1gFnPc4	látka
umístěné	umístěný	k2eAgFnPc4d1	umístěná
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
drahém	drahý	k2eAgNnSc6d1	drahé
doplňování	doplňování	k1gNnSc6	doplňování
paliva	palivo	k1gNnSc2	palivo
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
pomocí	pomocí	k7c2	pomocí
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
lodí	loď	k1gFnPc2	loď
Progress	Progressa	k1gFnPc2	Progressa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
ISS	ISS	kA	ISS
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
obydlena	obydlet	k5eAaPmNgFnS	obydlet
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Několikačlenná	několikačlenný	k2eAgFnSc1d1	několikačlenná
posádka	posádka	k1gFnSc1	posádka
pobývá	pobývat	k5eAaImIp3nS	pobývat
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Expedice	expedice	k1gFnSc1	expedice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
složena	složit	k5eAaPmNgFnS	složit
jako	jako	k9	jako
smíšená	smíšený	k2eAgFnSc1d1	smíšená
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zemí	zem	k1gFnPc2	zem
projektu	projekt	k1gInSc2	projekt
(	(	kIx(	(
<g/>
USA	USA	kA	USA
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
alespoň	alespoň	k9	alespoň
jedním	jeden	k4xCgNnSc7	jeden
svým	své	k1gNnSc7	své
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
doplňováni	doplňován	k2eAgMnPc1d1	doplňován
buďto	buďto	k8xC	buďto
opět	opět	k6eAd1	opět
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
národů	národ	k1gInPc2	národ
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
nominováni	nominovat	k5eAaBmNgMnP	nominovat
dalšími	další	k2eAgFnPc7d1	další
kosmickými	kosmický	k2eAgFnPc7d1	kosmická
agenturami	agentura	k1gFnPc7	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnPc4d1	hlavní
rozhodovací	rozhodovací	k2eAgFnPc4d1	rozhodovací
pravomoci	pravomoc	k1gFnPc4	pravomoc
na	na	k7c6	na
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
palubní	palubní	k2eAgMnPc1d1	palubní
inženýři	inženýr	k1gMnPc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
označuje	označovat	k5eAaImIp3nS	označovat
NASA	NASA	kA	NASA
kosmonauta	kosmonaut	k1gMnSc4	kosmonaut
zodpovědného	zodpovědný	k2eAgMnSc2d1	zodpovědný
za	za	k7c4	za
vědecký	vědecký	k2eAgInSc4d1	vědecký
program	program	k1gInSc4	program
stanice	stanice	k1gFnSc2	stanice
za	za	k7c4	za
"	"	kIx"	"
<g/>
vědeckého	vědecký	k2eAgMnSc4d1	vědecký
pracovníka	pracovník	k1gMnSc4	pracovník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Science	Science	k1gFnSc1	Science
Officer	Officer	k1gMnSc1	Officer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
takto	takto	k6eAd1	takto
označena	označen	k2eAgFnSc1d1	označena
–	–	k?	–
jako	jako	k8xC	jako
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
–	–	k?	–
Peggy	Pegga	k1gFnSc2	Pegga
Whitsonová	Whitsonový	k2eAgFnSc1d1	Whitsonová
v	v	k7c6	v
Expedici	expedice	k1gFnSc6	expedice
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
období	období	k1gNnSc6	období
osídlení	osídlení	k1gNnPc2	osídlení
ISS	ISS	kA	ISS
–	–	k?	–
Expedice	expedice	k1gFnSc1	expedice
1	[number]	k4	1
až	až	k9	až
6	[number]	k4	6
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
do	do	k7c2	do
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
–	–	k?	–
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
obsazována	obsazovat	k5eAaImNgFnS	obsazovat
tříčlennými	tříčlenný	k2eAgFnPc7d1	tříčlenná
posádkami	posádka	k1gFnPc7	posádka
složenými	složený	k2eAgFnPc7d1	složená
z	z	k7c2	z
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
USA	USA	kA	USA
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
expedice	expedice	k1gFnSc1	expedice
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
v	v	k7c6	v
Sojuzu	Sojuz	k1gInSc6	Sojuz
TM-	TM-	k1gFnPc2	TM-
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
výměny	výměna	k1gFnPc4	výměna
probíhaly	probíhat	k5eAaImAgFnP	probíhat
pomocí	pomocí	k7c2	pomocí
raketoplánů	raketoplán	k1gInPc2	raketoplán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
období	období	k1gNnSc1	období
osídlení	osídlení	k1gNnSc4	osídlení
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
Expedice	expedice	k1gFnPc1	expedice
7	[number]	k4	7
až	až	k9	až
13	[number]	k4	13
od	od	k7c2	od
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkáze	zkáza	k1gFnSc6	zkáza
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
pozastavení	pozastavení	k1gNnSc6	pozastavení
letu	let	k1gInSc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
nosných	nosný	k2eAgFnPc2d1	nosná
a	a	k8xC	a
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
kapacit	kapacita	k1gFnPc2	kapacita
na	na	k7c6	na
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
členů	člen	k1gInPc2	člen
posádek	posádka	k1gFnPc2	posádka
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgMnSc1	jeden
Rus	Rus	k1gMnSc1	Rus
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Američan	Američan	k1gMnSc1	Američan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
rotace	rotace	k1gFnSc2	rotace
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pouze	pouze	k6eAd1	pouze
lodi	loď	k1gFnPc4	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
etapa	etapa	k1gFnSc1	etapa
osídlení	osídlení	k1gNnSc2	osídlení
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Expedice	expedice	k1gFnSc2	expedice
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
letů	let	k1gInPc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgMnSc6	který
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
základní	základní	k2eAgFnSc2d1	základní
posádky	posádka	k1gFnSc2	posádka
rotováni	rotován	k2eAgMnPc1d1	rotován
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
pomocí	pomocí	k7c2	pomocí
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
lodi	loď	k1gFnSc6	loď
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zhruba	zhruba	k6eAd1	zhruba
týdenním	týdenní	k2eAgInSc6d1	týdenní
společném	společný	k2eAgInSc6d1	společný
letu	let	k1gInSc6	let
obou	dva	k4xCgFnPc2	dva
posádek	posádka	k1gFnPc2	posádka
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
aklimatizaci	aklimatizace	k1gFnSc3	aklimatizace
a	a	k8xC	a
předávání	předávání	k1gNnSc4	předávání
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
stará	starý	k2eAgFnSc1d1	stará
posádka	posádka	k1gFnSc1	posádka
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
starší	starý	k2eAgFnSc2d2	starší
lodi	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
a	a	k8xC	a
navracela	navracet	k5eAaBmAgFnS	navracet
se	se	k3xPyFc4	se
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
přiletivší	přiletivší	k2eAgFnSc6d1	přiletivší
posádce	posádka	k1gFnSc6	posádka
přítomen	přítomen	k2eAgMnSc1d1	přítomen
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
neměl	mít	k5eNaImAgMnS	mít
stát	stát	k5eAaPmF	stát
součástí	součást	k1gFnSc7	součást
stálé	stálý	k2eAgFnSc2d1	stálá
posádky	posádka	k1gFnSc2	posádka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kosmický	kosmický	k2eAgMnSc1d1	kosmický
turista	turista	k1gMnSc1	turista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
navracejícího	navracející	k2eAgMnSc2d1	navracející
se	se	k3xPyFc4	se
Sojuzu	Sojuz	k1gInSc2	Sojuz
své	svůj	k3xOyFgNnSc4	svůj
anatomické	anatomický	k2eAgNnSc4d1	anatomické
křeslo	křeslo	k1gNnSc4	křeslo
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Raketoplány	raketoplán	k1gInPc1	raketoplán
přivážely	přivážet	k5eAaImAgInP	přivážet
a	a	k8xC	a
odvážely	odvážet	k5eAaImAgInP	odvážet
třetí	třetí	k4xOgFnSc3	třetí
členy	člen	k1gMnPc7	člen
základní	základní	k2eAgFnSc2d1	základní
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jejich	jejich	k3xOp3gInSc2	jejich
pobytu	pobyt	k1gInSc2	pobyt
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
letů	let	k1gInPc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
proto	proto	k8xC	proto
kolísala	kolísat	k5eAaImAgFnS	kolísat
mezi	mezi	k7c7	mezi
šesti	šest	k4xCc7	šest
týdny	týden	k1gInPc7	týden
až	až	k6eAd1	až
šesti	šest	k4xCc7	šest
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příletů	přílet	k1gInPc2	přílet
kosmických	kosmický	k2eAgInPc2d1	kosmický
raketoplánů	raketoplán	k1gInPc2	raketoplán
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
ISS	ISS	kA	ISS
bývala	bývat	k5eAaImAgFnS	bývat
na	na	k7c6	na
období	období	k1gNnSc6	období
zhruba	zhruba	k6eAd1	zhruba
dvou	dva	k4xCgMnPc2	dva
týdnů	týden	k1gInPc2	týden
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
základní	základní	k2eAgFnSc1d1	základní
posádka	posádka	k1gFnSc1	posádka
stanice	stanice	k1gFnSc2	stanice
o	o	k7c4	o
návštěvnickou	návštěvnický	k2eAgFnSc4d1	návštěvnická
posádku	posádka	k1gFnSc4	posádka
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mívala	mívat	k5eAaImAgFnS	mívat
až	až	k6eAd1	až
7	[number]	k4	7
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
pak	pak	k6eAd1	pak
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
přebývalo	přebývat	k5eAaImAgNnS	přebývat
až	až	k6eAd1	až
10	[number]	k4	10
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
efektivně	efektivně	k6eAd1	efektivně
plnit	plnit	k5eAaImF	plnit
náročný	náročný	k2eAgInSc4d1	náročný
harmonogram	harmonogram	k1gInSc4	harmonogram
výstavby	výstavba	k1gFnSc2	výstavba
včetně	včetně	k7c2	včetně
kosmických	kosmický	k2eAgInPc2d1	kosmický
výstupů	výstup	k1gInPc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
posádka	posádka	k1gFnSc1	posádka
ovšem	ovšem	k9	ovšem
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
zatěžovala	zatěžovat	k5eAaImAgFnS	zatěžovat
zdroje	zdroj	k1gInPc4	zdroj
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
nutné	nutný	k2eAgNnSc1d1	nutné
posilovat	posilovat	k5eAaImF	posilovat
polouzavřený	polouzavřený	k2eAgInSc4d1	polouzavřený
systém	systém	k1gInSc4	systém
úpravy	úprava	k1gFnSc2	úprava
palubní	palubní	k2eAgFnSc2d1	palubní
atmosféry	atmosféra	k1gFnSc2	atmosféra
jednorázovými	jednorázový	k2eAgInPc7d1	jednorázový
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
<g/>
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
ISS	ISS	kA	ISS
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c4	na
šest	šest	k4xCc4	šest
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
zabezpečen	zabezpečit	k5eAaPmNgInS	zabezpečit
zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
trvale	trvale	k6eAd1	trvale
zakotvených	zakotvený	k2eAgFnPc2d1	zakotvená
lodí	loď	k1gFnPc2	loď
Sojuz	Sojuz	k1gInSc4	Sojuz
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Denní	denní	k2eAgInSc1d1	denní
program	program	k1gInSc1	program
===	===	k?	===
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
den	den	k1gInSc1	den
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
začíná	začínat	k5eAaImIp3nS	začínat
probuzením	probuzení	k1gNnSc7	probuzení
v	v	k7c4	v
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
ranní	ranní	k2eAgFnSc1d1	ranní
toaleta	toaleta	k1gFnSc1	toaleta
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
stavu	stav	k1gInSc2	stav
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
posnídá	posnídat	k5eAaPmIp3nS	posnídat
<g/>
,	,	kIx,	,
připraví	připravit	k5eAaPmIp3nS	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
denní	denní	k2eAgFnSc4d1	denní
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
střediskem	středisko	k1gNnSc7	středisko
upřesní	upřesnit	k5eAaPmIp3nS	upřesnit
denní	denní	k2eAgInSc1d1	denní
program	program	k1gInSc1	program
a	a	k8xC	a
po	po	k7c6	po
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
začne	začít	k5eAaPmIp3nS	začít
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
práce	práce	k1gFnSc1	práce
do	do	k7c2	do
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
hodinová	hodinový	k2eAgFnSc1d1	hodinová
přestávka	přestávka	k1gFnSc1	přestávka
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
opět	opět	k6eAd1	opět
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
vystřídají	vystřídat	k5eAaPmIp3nP	vystřídat
cvičení	cvičení	k1gNnSc4	cvičení
(	(	kIx(	(
<g/>
celkem	celek	k1gInSc7	celek
2,5	[number]	k4	2,5
hodiny	hodina	k1gFnSc2	hodina
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sejdou	sejít	k5eAaPmIp3nP	sejít
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
střediskem	středisko	k1gNnSc7	středisko
nad	nad	k7c7	nad
programem	program	k1gInSc7	program
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
začíná	začínat	k5eAaImIp3nS	začínat
večeře	večeře	k1gFnSc1	večeře
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
jídla	jídlo	k1gNnSc2	jídlo
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
systémů	systém	k1gInPc2	systém
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
večerní	večerní	k2eAgFnSc1d1	večerní
osobní	osobní	k2eAgFnSc1d1	osobní
hygiena	hygiena	k1gFnSc1	hygiena
a	a	k8xC	a
od	od	k7c2	od
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
uloží	uložit	k5eAaPmIp3nS	uložit
do	do	k7c2	do
spacích	spací	k2eAgInPc2d1	spací
pytlů	pytel	k1gInPc2	pytel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
na	na	k7c4	na
ISS	ISS	kA	ISS
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
ISS	ISS	kA	ISS
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
využívají	využívat	k5eAaPmIp3nP	využívat
specifického	specifický	k2eAgNnSc2d1	specifické
prostředí	prostředí	k1gNnSc2	prostředí
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
mikrogravitace	mikrogravitace	k1gFnSc1	mikrogravitace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
oblasti	oblast	k1gFnPc4	oblast
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
experimenty	experiment	k1gInPc1	experiment
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
biologie	biologie	k1gFnSc2	biologie
(	(	kIx(	(
<g/>
biomedicína	biomedicín	k1gInSc2	biomedicín
a	a	k8xC	a
biotechnologie	biotechnologie	k1gFnSc2	biotechnologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
dynamika	dynamika	k1gFnSc1	dynamika
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
materiálové	materiálový	k2eAgFnSc2d1	materiálová
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
(	(	kIx(	(
<g/>
kosmologie	kosmologie	k1gFnSc1	kosmologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
biologie	biologie	k1gFnSc2	biologie
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
úsilí	úsilí	k1gNnSc1	úsilí
věnováno	věnován	k2eAgNnSc1d1	věnováno
studiu	studio	k1gNnSc3	studio
vlivu	vliv	k1gInSc2	vliv
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
pobytu	pobyt	k1gInSc2	pobyt
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
důkladněji	důkladně	k6eAd2	důkladně
analyzovány	analyzovat	k5eAaImNgInP	analyzovat
negativní	negativní	k2eAgInPc1d1	negativní
vlivy	vliv	k1gInPc1	vliv
jako	jako	k8xS	jako
odvápňování	odvápňování	k1gNnSc1	odvápňování
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
svalová	svalový	k2eAgFnSc1d1	svalová
atrofie	atrofie	k1gFnSc1	atrofie
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
tělních	tělní	k2eAgFnPc2d1	tělní
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
zastává	zastávat	k5eAaImIp3nS	zastávat
výzkum	výzkum	k1gInSc1	výzkum
vlivu	vliv	k1gInSc2	vliv
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
radiace	radiace	k1gFnSc2	radiace
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sledováno	sledován	k2eAgNnSc1d1	sledováno
chování	chování	k1gNnSc1	chování
malé	malý	k2eAgFnSc2d1	malá
posádky	posádka	k1gFnSc2	posádka
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
prostředí	prostředí	k1gNnSc6	prostředí
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
těchto	tento	k3xDgInPc2	tento
výzkumů	výzkum	k1gInPc2	výzkum
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
nutných	nutný	k2eAgInPc2d1	nutný
pro	pro	k7c4	pro
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
mise	mise	k1gFnPc4	mise
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
výhledově	výhledově	k6eAd1	výhledově
i	i	k9	i
mise	mise	k1gFnSc2	mise
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
plášti	plášť	k1gInSc6	plášť
ISS	ISS	kA	ISS
objeven	objevit	k5eAaPmNgInS	objevit
mořský	mořský	k2eAgInSc1d1	mořský
plankton	plankton	k1gInSc1	plankton
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
program	program	k1gInSc1	program
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mikroby	mikrob	k1gInPc1	mikrob
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
živočichové	živočich	k1gMnPc1	živočich
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
modulů	modul	k1gInPc2	modul
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
ISS	ISS	kA	ISS
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
Destiny	Destina	k1gFnSc2	Destina
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nejstarší	starý	k2eAgInSc1d3	nejstarší
vědecký	vědecký	k2eAgInSc1d1	vědecký
modul	modul	k1gInSc1	modul
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
vypuštěný	vypuštěný	k2eAgInSc1d1	vypuštěný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
Columbus	Columbus	k1gInSc1	Columbus
–	–	k?	–
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
biologické	biologický	k2eAgInPc4d1	biologický
a	a	k8xC	a
biomedicínské	biomedicínský	k2eAgInPc4d1	biomedicínský
experimenty	experiment	k1gInPc4	experiment
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
</s>
</p>
<p>
<s>
Japonský	japonský	k2eAgMnSc1d1	japonský
Kibó	Kibó	k1gMnSc1	Kibó
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
laboratoř	laboratoř	k1gFnSc1	laboratoř
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
materiálový	materiálový	k2eAgInSc4d1	materiálový
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
astronomiiDalším	astronomiiDalší	k2eAgNnPc3d1	astronomiiDalší
zařízením	zařízení	k1gNnPc3	zařízení
jsou	být	k5eAaImIp3nP	být
venkovní	venkovní	k2eAgFnSc2d1	venkovní
nepřetlakové	přetlakový	k2eNgFnSc2d1	přetlakový
plošiny	plošina	k1gFnSc2	plošina
EXPRESS	express	k1gInSc1	express
Logistics	Logistics	k1gInSc1	Logistics
Carriers	Carriers	k1gInSc4	Carriers
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
prováděny	prováděn	k2eAgInPc1d1	prováděn
experimenty	experiment	k1gInPc1	experiment
v	v	k7c6	v
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
umístěn	umístit	k5eAaPmNgInS	umístit
alfa	alfa	k1gNnSc7	alfa
částicový	částicový	k2eAgInSc1d1	částicový
spektrometr	spektrometr	k1gInSc1	spektrometr
AMS-	AMS-	k1gFnSc1	AMS-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
antihmoty	antihmota	k1gFnSc2	antihmota
<g/>
,	,	kIx,	,
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
umístěna	umístit	k5eAaPmNgFnS	umístit
3D	[number]	k4	3D
tiskárna	tiskárna	k1gFnSc1	tiskárna
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
pro	pro	k7c4	pro
NASA	NASA	kA	NASA
americkou	americký	k2eAgFnSc7d1	americká
firmou	firma	k1gFnSc7	firma
Made	Mad	k1gFnSc2	Mad
in	in	k?	in
Space	Space	k1gMnSc1	Space
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
vytisknout	vytisknout	k5eAaPmF	vytisknout
první	první	k4xOgInSc4	první
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgFnSc4d1	reklamní
destičku	destička	k1gFnSc4	destička
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
podařilo	podařit	k5eAaPmAgNnS	podařit
vytisknout	vytisknout	k5eAaPmF	vytisknout
první	první	k4xOgInSc4	první
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nástrčkový	nástrčkový	k2eAgInSc4d1	nástrčkový
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
dopravila	dopravit	k5eAaPmAgFnS	dopravit
na	na	k7c4	na
ISS	ISS	kA	ISS
bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
společnosti	společnost	k1gFnSc2	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
testovací	testovací	k2eAgInSc4d1	testovací
komunikační	komunikační	k2eAgInSc4d1	komunikační
modul	modul	k1gInSc4	modul
OPALS	OPALS	kA	OPALS
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
experimentální	experimentální	k2eAgInSc1d1	experimentální
modul	modul	k1gInSc1	modul
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
laserového	laserový	k2eAgInSc2d1	laserový
paprsku	paprsek	k1gInSc2	paprsek
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
stanicí	stanice	k1gFnSc7	stanice
a	a	k8xC	a
Zemí	zem	k1gFnSc7	zem
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
klasická	klasický	k2eAgFnSc1d1	klasická
radiová	radiový	k2eAgFnSc1d1	radiová
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
testování	testování	k1gNnSc2	testování
bylo	být	k5eAaImAgNnS	být
laserovým	laserový	k2eAgMnSc7d1	laserový
paprskem	paprsek	k1gInSc7	paprsek
posláno	poslat	k5eAaPmNgNnS	poslat
mezi	mezi	k7c7	mezi
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
Mib	Mib	k1gFnPc2	Mib
inženýrských	inženýrský	k2eAgNnPc2d1	inženýrské
dat	datum	k1gNnPc2	datum
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
během	během	k7c2	během
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
radiovou	radiový	k2eAgFnSc7d1	radiová
komunikací	komunikace	k1gFnSc7	komunikace
by	by	kYmCp3nP	by
odeslání	odeslání	k1gNnSc4	odeslání
stejného	stejné	k1gNnSc2	stejné
souboru	soubor	k1gInSc2	soubor
dat	datum	k1gNnPc2	datum
trvalo	trvat	k5eAaImAgNnS	trvat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cena	cena	k1gFnSc1	cena
a	a	k8xC	a
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
stanice	stanice	k1gFnSc2	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
===	===	k?	===
</s>
</p>
<p>
<s>
Určení	určení	k1gNnSc1	určení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
stanice	stanice	k1gFnSc2	stanice
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
podílí	podílet	k5eAaImIp3nS	podílet
několik	několik	k4yIc1	několik
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
většinu	většina	k1gFnSc4	většina
dopravy	doprava	k1gFnSc2	doprava
zabezpečující	zabezpečující	k2eAgInSc1d1	zabezpečující
kosmický	kosmický	k2eAgInSc1d1	kosmický
raketoplán	raketoplán	k1gInSc1	raketoplán
byl	být	k5eAaImAgInS	být
financován	financovat	k5eAaBmNgInS	financovat
z	z	k7c2	z
oddělených	oddělený	k2eAgInPc2d1	oddělený
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkově	celkově	k6eAd1	celkově
přijde	přijít	k5eAaPmIp3nS	přijít
projekt	projekt	k1gInSc1	projekt
ISS	ISS	kA	ISS
na	na	k7c4	na
100	[number]	k4	100
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Pesimističtější	pesimistický	k2eAgInPc1d2	pesimističtější
odhady	odhad	k1gInPc1	odhad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
dokonce	dokonce	k9	dokonce
100	[number]	k4	100
mld	mld	k?	mld
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
<g/>
Organizace	organizace	k1gFnSc1	organizace
NASA	NASA	kA	NASA
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
financování	financování	k1gNnSc6	financování
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
25,6	[number]	k4	25,6
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
sumy	suma	k1gFnSc2	suma
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
započítány	započítat	k5eAaPmNgInP	započítat
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
lety	let	k1gInPc4	let
kosmického	kosmický	k2eAgInSc2d1	kosmický
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
činil	činit	k5eAaImAgInS	činit
rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c6	na
ISS	ISS	kA	ISS
zhruba	zhruba	k6eAd1	zhruba
1,7	[number]	k4	1,7
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
navyšován	navyšovat	k5eAaImNgInS	navyšovat
až	až	k9	až
po	po	k7c4	po
konečných	konečný	k2eAgInPc2d1	konečný
2,3	[number]	k4	2,3
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
držet	držet	k5eAaImF	držet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
NASA	NASA	kA	NASA
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
organizace	organizace	k1gFnSc2	organizace
NASA	NASA	kA	NASA
jen	jen	k9	jen
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
ISS	ISS	kA	ISS
tedy	tedy	k8xC	tedy
budou	být	k5eAaImBp3nP	být
přes	přes	k7c4	přes
53	[number]	k4	53
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
kosmického	kosmický	k2eAgInSc2d1	kosmický
raketoplánu	raketoplán	k1gInSc2	raketoplán
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2005	[number]	k4	2005
byly	být	k5eAaImAgInP	být
24	[number]	k4	24
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
5	[number]	k4	5
mld	mld	k?	mld
USD	USD	kA	USD
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
lety	let	k1gInPc4	let
nesouvisející	související	k2eNgInPc4d1	nesouvisející
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
lety	let	k1gInPc4	let
raketoplánu	raketoplán	k1gInSc2	raketoplán
vynaloženo	vynaložit	k5eAaPmNgNnS	vynaložit
21,5	[number]	k4	21,5
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
19	[number]	k4	19
mld	mld	k?	mld
USD	USD	kA	USD
bylo	být	k5eAaImAgNnS	být
vynaloženo	vynaložen	k2eAgNnSc1d1	vynaloženo
na	na	k7c4	na
lety	let	k1gInPc4	let
související	související	k2eAgInPc4d1	související
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
lety	let	k1gInPc4	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
ISS	ISS	kA	ISS
vynaloženo	vynaložen	k2eAgNnSc4d1	vynaloženo
38	[number]	k4	38
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
organizace	organizace	k1gFnSc1	organizace
JAXA	JAXA	kA	JAXA
investuje	investovat	k5eAaBmIp3nS	investovat
do	do	k7c2	do
výstavby	výstavba	k1gFnSc2	výstavba
ISS	ISS	kA	ISS
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgNnPc1d1	Evropské
ESA	eso	k1gNnPc1	eso
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
mld	mld	k?	mld
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
agentur	agentura	k1gFnPc2	agentura
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc6d2	nižší
úrovni	úroveň	k1gFnSc6	úroveň
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ruského	ruský	k2eAgInSc2d1	ruský
Roskosmosu	Roskosmos	k1gInSc2	Roskosmos
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příspěvek	příspěvek	k1gInSc1	příspěvek
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
velice	velice	k6eAd1	velice
obtížně	obtížně	k6eAd1	obtížně
vyčíslitelný	vyčíslitelný	k2eAgInSc1d1	vyčíslitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
stanice	stanice	k1gFnSc2	stanice
===	===	k?	===
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
právním	právní	k2eAgInSc7d1	právní
dokumentem	dokument	k1gInSc7	dokument
určujícím	určující	k2eAgInSc7d1	určující
povinnosti	povinnost	k1gFnPc4	povinnost
zemí	zem	k1gFnPc2	zem
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
na	na	k7c6	na
programu	program	k1gInSc6	program
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
kosmické	kosmický	k2eAgFnSc6d1	kosmická
stanici	stanice	k1gFnSc6	stanice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Space	Space	k1gFnSc1	Space
Station	station	k1gInSc1	station
Intergovernmental	Intergovernmental	k1gMnSc1	Intergovernmental
Agreement	Agreement	k1gMnSc1	Agreement
<g/>
,	,	kIx,	,
IGA	IGA	kA	IGA
<g/>
)	)	kIx)	)
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
patnácti	patnáct	k4xCc7	patnáct
státy	stát	k1gInPc1	stát
–	–	k?	–
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
jedenácti	jedenáct	k4xCc2	jedenáct
státy	stát	k1gInPc1	stát
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
partnery	partner	k1gMnPc7	partner
specifikují	specifikovat	k5eAaBmIp3nP	specifikovat
dvoustranné	dvoustranný	k2eAgFnPc1d1	dvoustranná
"	"	kIx"	"
<g/>
Dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c4	o
porozumění	porozumění	k1gNnSc4	porozumění
<g/>
"	"	kIx"	"
mezi	mezi	k7c4	mezi
NASA	NASA	kA	NASA
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Roskosmosem	Roskosmos	k1gInSc7	Roskosmos
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
CSA	CSA	kA	CSA
a	a	k8xC	a
JAXA	JAXA	kA	JAXA
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
takto	takto	k6eAd1	takto
stanovených	stanovený	k2eAgNnPc2d1	stanovené
pravidel	pravidlo	k1gNnPc2	pravidlo
jsou	být	k5eAaImIp3nP	být
uzavírány	uzavírán	k2eAgFnPc4d1	uzavírána
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
dohody	dohoda	k1gFnPc4	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
využívání	využívání	k1gNnSc6	využívání
zdrojů	zdroj	k1gInPc2	zdroj
partnerů	partner	k1gMnPc2	partner
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
letech	let	k1gInPc6	let
neruských	ruský	k2eNgMnPc2d1	neruský
astronautů	astronaut	k1gMnPc2	astronaut
na	na	k7c6	na
Sojuzech	Sojuz	k1gInPc6	Sojuz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
není	být	k5eNaImIp3nS	být
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
nikým	nikdo	k3yNnSc7	nikdo
jako	jako	k9	jako
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
modul	modul	k1gInSc1	modul
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
vlastněn	vlastnit	k5eAaImNgMnS	vlastnit
jediným	jediný	k2eAgMnSc7d1	jediný
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ruský	ruský	k2eAgInSc4d1	ruský
a	a	k8xC	a
americký	americký	k2eAgInSc4d1	americký
segment	segment	k1gInSc4	segment
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ruského	ruský	k2eAgInSc2d1	ruský
segmentu	segment	k1gInSc2	segment
patří	patřit	k5eAaImIp3nS	patřit
moduly	modul	k1gInPc4	modul
a	a	k8xC	a
díly	díl	k1gInPc4	díl
vlastněné	vlastněný	k2eAgInPc4d1	vlastněný
a	a	k8xC	a
vyrobené	vyrobený	k2eAgMnPc4d1	vyrobený
Rusy	Rus	k1gMnPc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
Zarja	Zarj	k1gInSc2	Zarj
<g/>
,	,	kIx,	,
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zaplacený	zaplacený	k2eAgMnSc1d1	zaplacený
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
americké	americký	k2eAgFnSc2d1	americká
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
NASA	NASA	kA	NASA
a	a	k8xC	a
tedy	tedy	k9	tedy
součástí	součást	k1gFnSc7	součást
amerického	americký	k2eAgInSc2d1	americký
segmentu	segment	k1gInSc2	segment
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
segment	segment	k1gInSc1	segment
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zbytkem	zbytek	k1gInSc7	zbytek
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
modulů	modul	k1gInPc2	modul
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnPc2	konstrukce
evropských	evropský	k2eAgFnPc2d1	Evropská
(	(	kIx(	(
<g/>
modul	modul	k1gInSc1	modul
Columbus	Columbus	k1gInSc1	Columbus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
japonských	japonský	k2eAgFnPc2d1	japonská
(	(	kIx(	(
<g/>
modul	modul	k1gInSc1	modul
Kibó	Kibó	k1gFnSc2	Kibó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanadských	kanadský	k2eAgFnPc2d1	kanadská
(	(	kIx(	(
<g/>
manipulátor	manipulátor	k1gInSc1	manipulátor
Canadarm	Canadarm	k1gInSc1	Canadarm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
amerického	americký	k2eAgInSc2d1	americký
segmentu	segment	k1gInSc2	segment
platí	platit	k5eAaImIp3nS	platit
systém	systém	k1gInSc1	systém
dohod	dohoda	k1gFnPc2	dohoda
NASA	NASA	kA	NASA
s	s	k7c7	s
partnery	partner	k1gMnPc7	partner
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
ESA	eso	k1gNnSc2	eso
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
51	[number]	k4	51
%	%	kIx~	%
využití	využití	k1gNnSc2	využití
zdrojů	zdroj	k1gInPc2	zdroj
modulu	modul	k1gInSc2	modul
Columbus	Columbus	k1gInSc4	Columbus
<g/>
,	,	kIx,	,
analogicky	analogicky	k6eAd1	analogicky
JAXA	JAXA	kA	JAXA
na	na	k7c4	na
51	[number]	k4	51
%	%	kIx~	%
modulu	modul	k1gInSc3	modul
Kibó	Kibó	k1gFnSc4	Kibó
<g/>
.	.	kIx.	.
</s>
<s>
CSA	CSA	kA	CSA
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
2,3	[number]	k4	2,3
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
neruských	ruský	k2eNgInPc2d1	neruský
komponentů	komponent	k1gInPc2	komponent
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
posádek	posádka	k1gFnPc2	posádka
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
ze	z	k7c2	z
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
a	a	k8xC	a
telekomunikační	telekomunikační	k2eAgFnPc4d1	telekomunikační
služby	služba	k1gFnPc4	služba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
NASA	NASA	kA	NASA
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
12,8	[number]	k4	12,8
%	%	kIx~	%
pro	pro	k7c4	pro
JAXA	JAXA	kA	JAXA
<g/>
,	,	kIx,	,
8,3	[number]	k4	8,3
%	%	kIx~	%
pro	pro	k7c4	pro
ESA	eso	k1gNnPc4	eso
a	a	k8xC	a
2,3	[number]	k4	2,3
%	%	kIx~	%
pro	pro	k7c4	pro
CSA	CSA	kA	CSA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
vypadá	vypadat	k5eAaPmIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgInSc1d1	ruský
segment	segment	k1gInSc1	segment
–	–	k?	–
100	[number]	k4	100
%	%	kIx~	%
Roskosmos	Roskosmos	k1gInSc1	Roskosmos
</s>
</p>
<p>
<s>
americký	americký	k2eAgInSc1d1	americký
segment	segment	k1gInSc1	segment
</s>
</p>
<p>
<s>
modul	modul	k1gInSc1	modul
Kibó	Kibó	k1gFnSc2	Kibó
–	–	k?	–
51	[number]	k4	51
%	%	kIx~	%
JAXA	JAXA	kA	JAXA
<g/>
,	,	kIx,	,
46,7	[number]	k4	46,7
%	%	kIx~	%
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
%	%	kIx~	%
CSA	CSA	kA	CSA
</s>
</p>
<p>
<s>
modul	modul	k1gInSc1	modul
Columbus	Columbus	k1gInSc1	Columbus
–	–	k?	–
51	[number]	k4	51
%	%	kIx~	%
ESA	eso	k1gNnPc4	eso
<g/>
,	,	kIx,	,
46,7	[number]	k4	46,7
%	%	kIx~	%
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
%	%	kIx~	%
CSA	CSA	kA	CSA
</s>
</p>
<p>
<s>
americké	americký	k2eAgInPc1d1	americký
moduly	modul	k1gInPc1	modul
–	–	k?	–
97,7	[number]	k4	97,7
%	%	kIx~	%
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
%	%	kIx~	%
CSA	CSA	kA	CSA
</s>
</p>
<p>
<s>
čas	čas	k1gInSc1	čas
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
,	,	kIx,	,
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
služby	služba	k1gFnSc2	služba
–	–	k?	–
76,6	[number]	k4	76,6
%	%	kIx~	%
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
12,8	[number]	k4	12,8
%	%	kIx~	%
JAXA	JAXA	kA	JAXA
<g/>
,	,	kIx,	,
8,3	[number]	k4	8,3
%	%	kIx~	%
ESA	eso	k1gNnPc4	eso
<g/>
,	,	kIx,	,
2,3	[number]	k4	2,3
%	%	kIx~	%
CSA	CSA	kA	CSA
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
přiznala	přiznat	k5eAaPmAgFnS	přiznat
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
že	že	k8xS	že
notebooky	notebook	k1gInPc4	notebook
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
kosmické	kosmický	k2eAgFnSc3d1	kosmická
stanici	stanice	k1gFnSc3	stanice
byly	být	k5eAaImAgFnP	být
nakaženy	nakažen	k2eAgInPc1d1	nakažen
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
starým	starý	k2eAgInSc7d1	starý
počítačovým	počítačový	k2eAgInSc7d1	počítačový
virem	vir	k1gInSc7	vir
W	W	kA	W
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
<g/>
Gammima	Gammima	k1gFnSc1	Gammima
<g/>
.	.	kIx.	.
<g/>
AG	AG	kA	AG
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
původně	původně	k6eAd1	původně
sbíral	sbírat	k5eAaImAgInS	sbírat
přihlašovací	přihlašovací	k2eAgInPc4d1	přihlašovací
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
on-line	onin	k1gInSc5	on-lin
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
do	do	k7c2	do
notebooků	notebook	k1gInPc2	notebook
dostal	dostat	k5eAaPmAgMnS	dostat
nejspíše	nejspíše	k9	nejspíše
pomocí	pomoc	k1gFnPc2	pomoc
přenosného	přenosný	k2eAgInSc2d1	přenosný
USB	USB	kA	USB
flash	flash	k1gMnSc1	flash
disku	disk	k1gInSc2	disk
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
řídicí	řídicí	k2eAgFnSc4d1	řídicí
jednotku	jednotka	k1gFnSc4	jednotka
ani	ani	k8xC	ani
provozní	provozní	k2eAgInPc4d1	provozní
systémy	systém	k1gInPc4	systém
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
celkovými	celkový	k2eAgInPc7d1	celkový
náklady	náklad	k1gInPc7	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
odhadovanými	odhadovaný	k2eAgFnPc7d1	odhadovaná
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
přes	přes	k7c4	přes
100	[number]	k4	100
mld	mld	k?	mld
USD	USD	kA	USD
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejdražším	drahý	k2eAgInSc7d3	nejdražší
objektem	objekt	k1gInSc7	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kdy	kdy	k6eAd1	kdy
lidstvo	lidstvo	k1gNnSc1	lidstvo
zkonstruovalo	zkonstruovat	k5eAaPmAgNnS	zkonstruovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cena	cena	k1gFnSc1	cena
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
budou	být	k5eAaImBp3nP	být
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
činit	činit	k5eAaImF	činit
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rozpočet	rozpočet	k1gInSc4	rozpočet
NASA	NASA	kA	NASA
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
72,4	[number]	k4	72,4
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
starty	start	k1gInPc4	start
raketoplánů	raketoplán	k1gInPc2	raketoplán
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
odhadované	odhadovaný	k2eAgFnPc1d1	odhadovaná
na	na	k7c4	na
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
každý	každý	k3xTgInSc4	každý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
50,4	[number]	k4	50,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
celkem	celkem	k6eAd1	celkem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
stála	stát	k5eAaImAgFnS	stát
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
157	[number]	k4	157
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
nejdražší	drahý	k2eAgFnSc1d3	nejdražší
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
kdy	kdy	k6eAd1	kdy
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
rozložil	rozložit	k5eAaPmAgInS	rozložit
evropský	evropský	k2eAgInSc1d1	evropský
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
euro	euro	k1gNnSc1	euro
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
od	od	k7c2	od
každého	každý	k3xTgMnSc2	každý
Evropana	Evropan	k1gMnSc2	Evropan
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
méně	málo	k6eAd2	málo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
šálku	šálek	k1gInSc2	šálek
kávy	káva	k1gFnSc2	káva
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gMnSc2	Space
Gateway	Gatewaa	k1gMnSc2	Gatewaa
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nepilotovaných	pilotovaný	k2eNgInPc2d1	nepilotovaný
letů	let	k1gInPc2	let
k	k	k7c3	k
ISS	ISS	kA	ISS
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pilotovaných	pilotovaný	k2eAgInPc2d1	pilotovaný
letů	let	k1gInPc2	let
k	k	k7c3	k
ISS	ISS	kA	ISS
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
výstupů	výstup	k1gInPc2	výstup
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
z	z	k7c2	z
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
na	na	k7c6	na
ISS	ISS	kA	ISS
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
ISS	ISS	kA	ISS
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
–	–	k?	–
USA	USA	kA	USA
</s>
</p>
<p>
<s>
CSA	CSA	kA	CSA
–	–	k?	–
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Energia	Energia	k1gFnSc1	Energia
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
ESA	eso	k1gNnSc2	eso
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
</s>
</p>
<p>
<s>
JAXA	JAXA	kA	JAXA
–	–	k?	–
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
AEB	AEB	kA	AEB
–	–	k?	–
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
ASI	asi	k9	asi
–	–	k?	–
ItálieDalší	ItálieDalší	k2eAgInPc4d1	ItálieDalší
odkazy	odkaz	k1gInPc4	odkaz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
pozice	pozice	k1gFnSc1	pozice
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Animace	animace	k1gFnSc1	animace
výstavby	výstavba	k1gFnSc2	výstavba
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
SPACE-40	SPACE-40	k1gFnSc2	SPACE-40
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
na	na	k7c4	na
www.kosmo.cz	www.kosmo.cz	k1gInSc4	www.kosmo.cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Statistiky	statistika	k1gFnPc4	statistika
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
stav	stav	k1gInSc1	stav
posádky	posádka	k1gFnSc2	posádka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
</s>
</p>
