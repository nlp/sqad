<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gMnSc1	International
Space	Space	k1gFnSc2	Space
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
́	́	k?	́
<g/>
д	д	k?	д
к	к	k?	к
<g/>
́	́	k?	́
<g/>
ч	ч	k?	ч
с	с	k?	с
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
ISS	ISS	kA	ISS
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jediná	jediný	k2eAgFnSc1d1	jediná
trvale	trvale	k6eAd1	trvale
obydlená	obydlený	k2eAgFnSc1d1	obydlená
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
