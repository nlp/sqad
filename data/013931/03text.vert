<s>
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c4
French	French	k1gInSc4
Open	Open	k1gInSc4
2011	#num#	k4
<g/>
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Rafa	Rafa	k1gMnSc1
<g/>
,	,	kIx,
antukový	antukový	k2eAgMnSc1d1
král	král	k1gMnSc1
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Manacor	Manacor	k1gInSc1
<g/>
,	,	kIx,
Mallorca	Mallorca	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Manacor	Manacor	k1gInSc1
<g/>
,	,	kIx,
Mallorca	Mallorca	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
185	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
85	#num#	k4
kg	kg	kA
Profesionál	profesionál	k1gMnSc1
od	od	k7c2
</s>
<s>
2001	#num#	k4
Držení	držení	k1gNnSc1
rakety	raketa	k1gFnPc1
</s>
<s>
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
,	,	kIx,
bekhend	bekhend	k1gInSc1
obouruč	obouruč	k6eAd1
<g/>
(	(	kIx(
<g/>
rozený	rozený	k2eAgMnSc1d1
pravák	pravák	k1gMnSc1
<g/>
)	)	kIx)
Výdělek	výdělek	k1gInSc1
</s>
<s>
124	#num#	k4
111	#num#	k4
811	#num#	k4
USD	USD	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
historickém	historický	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
<g/>
)	)	kIx)
Tenisová	tenisový	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Babolat	Babolat	k5eAaPmF
Pure	Pure	k1gNnSc4
Aero	aero	k1gNnSc4
Play	play	k0
Dvouhra	dvouhra	k1gFnSc1
Poměr	poměra	k1gFnPc2
zápasů	zápas	k1gInPc2
</s>
<s>
1015	#num#	k4
<g/>
–	–	k?
<g/>
206	#num#	k4
Tituly	titul	k1gInPc1
</s>
<s>
87	#num#	k4
ATP	atp	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
20	#num#	k4
grandslamů	grandslam	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
)	)	kIx)
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
<g/>
)	)	kIx)
Dvouhra	dvouhra	k1gFnSc1
na	na	k7c6
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc2
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
US	US	kA
Open	Open	k1gMnSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
Velké	velký	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
</s>
<s>
finále	finále	k1gNnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
)	)	kIx)
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
zlato	zlato	k1gNnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Čtyřhra	čtyřhra	k1gFnSc1
Poměr	poměra	k1gFnPc2
zápasů	zápas	k1gInPc2
</s>
<s>
137	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
Tituly	titul	k1gInPc1
</s>
<s>
11	#num#	k4
ATP	atp	kA
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
<g/>
)	)	kIx)
Čtyřhra	čtyřhra	k1gFnSc1
na	na	k7c6
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc2
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
US	US	kA
Open	Open	k1gMnSc1
</s>
<s>
semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Velké	velký	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Týmové	týmový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Davis	Davis	k1gFnSc2
Cup	cup	k1gInSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktualizovány	aktualizován	k2eAgInPc4d1
dne	den	k1gInSc2
20210426	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Tenis	tenis	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Peking	Peking	k1gInSc1
2008	#num#	k4
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
2016	#num#	k4
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
Rafael	Rafael	k1gMnSc1
„	„	k?
<g/>
Rafa	Rafa	k1gMnSc1
<g/>
“	“	k?
Nadal	nadat	k5eAaPmAgMnS
Parera	Parer	k1gMnSc4
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986	#num#	k4
Manacor	Manacor	k1gMnSc1
<g/>
,	,	kIx,
Mallorca	Mallorca	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělský	španělský	k2eAgMnSc1d1
profesionální	profesionální	k2eAgMnSc1d1
tenista	tenista	k1gMnSc1
<g/>
,	,	kIx,
nejlepší	dobrý	k2eAgMnSc1d3
antukář	antukář	k1gMnSc1
historie	historie	k1gFnSc2
tenisu	tenis	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jediný	jediný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
s	s	k7c7
třinácti	třináct	k4xCc7
tituly	titul	k1gInPc7
z	z	k7c2
dvouhry	dvouhra	k1gFnSc2
konkrétního	konkrétní	k2eAgInSc2d1
grandslamu	grandslam	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
vybojoval	vybojovat	k5eAaPmAgMnS
na	na	k7c6
Roland-Garros	Roland-Garrosa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Pekingských	pekingský	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
dvouhru	dvouhra	k1gFnSc4
a	a	k8xC
na	na	k7c4
Hrách	hrách	k1gInSc4
XXXI	XXXI	kA
<g/>
.	.	kIx.
olympiády	olympiáda	k1gFnSc2
v	v	k7c6
Riu	Riu	k1gFnSc6
de	de	k?
Janeiru	Janeira	k1gMnSc4
ovládl	ovládnout	k5eAaPmAgInS
s	s	k7c7
Marcem	Marce	k1gMnSc7
Lópezem	López	k1gInSc7
mužskou	mužský	k2eAgFnSc4d1
čtyřhru	čtyřhra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
dvaceti	dvacet	k4xCc7
grandslamy	grandslam	k1gInPc7
je	být	k5eAaImIp3nS
v	v	k7c6
mužských	mužský	k2eAgFnPc6d1
historických	historický	k2eAgFnPc6d1
statistikách	statistika	k1gFnPc6
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
spolu	spolu	k6eAd1
s	s	k7c7
Federerem	Federer	k1gInSc7
<g/>
,	,	kIx,
když	když	k8xS
vyjma	vyjma	k7c4
French	French	k1gInSc4
Open	Open	k1gMnSc1
čtyřikrát	čtyřikrát	k6eAd1
triumfoval	triumfovat	k5eAaBmAgMnS
na	na	k7c6
US	US	kA
Open	Open	k1gNnSc1
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
a	a	k8xC
jednou	jeden	k4xCgFnSc7
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
byl	být	k5eAaImAgInS
opakovaně	opakovaně	k6eAd1
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
stal	stát	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
historicky	historicky	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
pořadí	pořadí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmi	osm	k4xCc6
obdobích	období	k1gNnPc6
na	na	k7c6
čele	čelo	k1gNnSc6
strávil	strávit	k5eAaPmAgMnS
209	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pětkrát	pětkrát	k6eAd1
sezónu	sezóna	k1gFnSc4
zakončil	zakončit	k5eAaPmAgMnS
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
jako	jako	k8xC,k8xS
nejstarší	starý	k2eAgFnSc1d3
konečná	konečný	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
vůbec	vůbec	k9
<g/>
,	,	kIx,
rekord	rekord	k1gInSc4
překonaný	překonaný	k2eAgInSc4d1
v	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
Djokovićem	Djokovićem	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
ziskem	zisk	k1gInSc7
35	#num#	k4
singlových	singlový	k2eAgInPc2d1
titulů	titul	k1gInPc2
zůstává	zůstávat	k5eAaImIp3nS
druhý	druhý	k4xOgMnSc1
v	v	k7c6
sérii	série	k1gFnSc6
Masters	Masters	k1gInSc4
za	za	k7c7
Djokovićem	Djoković	k1gInSc7
a	a	k8xC
s	s	k7c7
22	#num#	k4
trofejemi	trofej	k1gFnPc7
mu	on	k3xPp3gMnSc3
také	také	k9
patří	patřit	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
příčka	příčka	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
ATP	atp	kA
500	#num#	k4
za	za	k7c7
Federerem	Federero	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
své	svůj	k3xOyFgInPc4
výkony	výkon	k1gInPc4
na	na	k7c6
antuce	antuka	k1gFnSc6
získal	získat	k5eAaPmAgMnS
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
antukový	antukový	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Vítězstvím	vítězství	k1gNnSc7
na	na	k7c6
Barcelona	Barcelona	k1gFnSc1
Open	Open	k1gInSc1
2016	#num#	k4
vyrovnal	vyrovnat	k5eAaPmAgInS,k5eAaBmAgInS
rekord	rekord	k1gInSc1
49	#num#	k4
antukových	antukový	k2eAgFnPc2d1
trofejí	trofej	k1gFnPc2
Argentince	Argentinec	k1gMnSc4
Guillerma	Guillerm	k1gMnSc4
Vilase	Vilasa	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
překonal	překonat	k5eAaPmAgInS
triumfem	triumf	k1gInSc7
na	na	k7c4
Monte-Carlo	Monte-Carlo	k1gNnSc4
Masters	Masters	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
rekordních	rekordní	k2eAgFnPc2d1
61	#num#	k4
antukových	antukový	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
s	s	k7c7
třinácti	třináct	k4xCc7
tituly	titul	k1gInPc7
z	z	k7c2
jediného	jediný	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
rekordní	rekordní	k2eAgInSc1d1
počet	počet	k1gInSc1
drží	držet	k5eAaImIp3nS
na	na	k7c4
French	French	k1gInSc4
Open	Opena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvanáctkrát	dvanáctkrát	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
Barcelona	Barcelona	k1gFnSc1
Open	Openo	k1gNnPc2
a	a	k8xC
jedenáctkrát	jedenáctkrát	k6eAd1
Monte-Carlo	Monte-Carlo	k1gNnSc1
Masters	Mastersa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výhrou	výhra	k1gFnSc7
na	na	k7c4
US	US	kA
Open	Open	k1gInSc4
2010	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
po	po	k7c6
Laverovi	Laver	k1gMnSc6
<g/>
,	,	kIx,
Agassim	Agassim	k1gInPc3
a	a	k8xC
Federerovi	Federer	k1gMnSc6
čtvrtým	čtvrtý	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
tenisu	tenis	k1gInSc2
a	a	k8xC
celkově	celkově	k6eAd1
sedmým	sedmý	k4xOgMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zkompletovat	zkompletovat	k5eAaPmF
všechny	všechen	k3xTgInPc1
čtyři	čtyři	k4xCgInPc1
grandslamové	grandslamový	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Andre	Andr	k1gInSc5
Agassim	Agassim	k1gMnSc1
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
tzv.	tzv.	kA
Zlatého	zlatý	k1gInSc2
Slamu	slam	k1gInSc2
<g/>
,	,	kIx,
všech	všecek	k3xTgInPc2
čtyř	čtyři	k4xCgInPc2
grandslamových	grandslamový	k2eAgInPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
olympijského	olympijský	k2eAgNnSc2d1
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2010	#num#	k4
a	a	k8xC
2013	#num#	k4
odešel	odejít	k5eAaPmAgInS
jako	jako	k9
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
ze	z	k7c2
závěrečné	závěrečný	k2eAgFnSc2d1
události	událost	k1gFnSc2
Turnaje	turnaj	k1gInSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
prvním	první	k4xOgInSc6
případě	případ	k1gInSc6
nestačil	stačit	k5eNaBmAgInS
na	na	k7c4
Federera	Federer	k1gMnSc4
a	a	k8xC
ve	v	k7c6
druhém	druhý	k4xOgInSc6
na	na	k7c4
Djokoviće	Djoković	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jedinou	jediný	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
z	z	k7c2
velkých	velký	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
nevyhrál	vyhrát	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
monackém	monacký	k2eAgNnSc6d1
finále	finále	k1gNnSc6
2010	#num#	k4
porazil	porazit	k5eAaPmAgMnS
Verdasca	Verdasca	k1gMnSc1
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
v	v	k7c6
nejkratším	krátký	k2eAgInSc6d3
finálovém	finálový	k2eAgInSc6d1
duelu	duel	k1gInSc6
série	série	k1gFnSc1
Masters	Masters	k1gInSc1
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Titulem	titul	k1gInSc7
na	na	k7c4
Monte-Carlo	Monte-Carlo	k1gNnSc4
Masters	Masters	k1gInSc1
2011	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
jako	jako	k9
první	první	k4xOgMnSc1
tenista	tenista	k1gMnSc1
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
stejný	stejný	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
sedmkrát	sedmkrát	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
řadě	řada	k1gFnSc6
a	a	k8xC
odpoutal	odpoutat	k5eAaPmAgInS
se	se	k3xPyFc4
od	od	k7c2
Vilasových	Vilasův	k2eAgInPc2d1
šesti	šest	k4xCc2
triumfů	triumf	k1gInPc2
na	na	k7c6
ATP	atp	kA
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Vítězný	vítězný	k2eAgInSc1d1
boj	boj	k1gInSc1
o	o	k7c4
titul	titul	k1gInSc4
na	na	k7c4
Barcelona	Barcelona	k1gFnSc1
Open	Openo	k1gNnPc2
2021	#num#	k4
proti	proti	k7c3
Tsitsipasovi	Tsitsipas	k1gMnSc3
znamenal	znamenat	k5eAaImAgMnS
časem	čas	k1gInSc7
3	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
38	#num#	k4
minut	minuta	k1gFnPc2
nejdelší	dlouhý	k2eAgFnPc1d3
finále	finále	k1gNnSc7
na	na	k7c6
túře	túra	k1gFnSc6
ATP	atp	kA
od	od	k7c2
začátku	začátek	k1gInSc2
počítání	počítání	k1gNnSc2
statistik	statistika	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
zápasech	zápas	k1gInPc6
na	na	k7c4
dvě	dva	k4xCgFnPc4
vítězné	vítězný	k2eAgFnPc4d1
sady	sada	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
a	a	k8xC
2019	#num#	k4
zakončil	zakončit	k5eAaPmAgInS
sezónu	sezóna	k1gFnSc4
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
dvouhry	dvouhra	k1gFnSc2
žebříčku	žebříček	k1gInSc2
ATP.	atp.	kA
Tím	ten	k3xDgMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc6
takovým	takový	k3xDgMnSc7
hráčem	hráč	k1gMnSc7
v	v	k7c6
pěti	pět	k4xCc6
nenavazujících	navazující	k2eNgFnPc6d1
sezónách	sezóna	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
1990	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
působil	působit	k5eAaImAgMnS
v	v	k7c6
úloze	úloha	k1gFnSc6
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
strýc	strýc	k1gMnSc1
Toni	Toni	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2017	#num#	k4
jej	on	k3xPp3gNnSc4
nahradil	nahradit	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
tenista	tenista	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
je	být	k5eAaImIp3nS
také	také	k9
členem	člen	k1gMnSc7
trenérského	trenérský	k2eAgInSc2d1
týmu	tým	k1gInSc2
Francisco	Francisco	k1gMnSc1
Roig	Roig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
S	s	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
mladší	mladý	k2eAgFnSc7d2
sestrou	sestra	k1gFnSc7
Mariou	Mariý	k2eAgFnSc7d1
Isabel	Isabela	k1gFnPc2
Nadalovou	Nadalová	k1gFnSc7
vyrůstal	vyrůstat	k5eAaImAgInS
v	v	k7c6
pětipodlažním	pětipodlažní	k2eAgInSc6d1
činžovním	činžovní	k2eAgInSc6d1
domě	dům	k1gInSc6
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
Manacoru	Manacor	k1gInSc6
na	na	k7c6
Mallorce	Mallorka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2009	#num#	k4
přinesl	přinést	k5eAaPmAgInS
španělský	španělský	k2eAgInSc1d1
deník	deník	k1gInSc1
La	la	k1gNnSc2
Vanguardia	Vanguardium	k1gNnSc2
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
poté	poté	k6eAd1
vydal	vydat	k5eAaPmAgMnS
také	také	k9
The	The	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
rodiče	rodič	k1gMnPc1
Ana	Ana	k1gFnSc1
Maria	Maria	k1gFnSc1
a	a	k8xC
Sebastian	Sebastian	k1gMnSc1
Nadalovi	Nadalův	k2eAgMnPc1d1
odloučili	odloučit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xC,k8xS
rodilý	rodilý	k2eAgMnSc1d1
pravák	pravák	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
přibližně	přibližně	k6eAd1
do	do	k7c2
deseti	deset	k4xCc2
let	léto	k1gNnPc2
forhend	forhend	k1gInSc4
i	i	k8xC
bekhend	bekhend	k1gInSc4
obouruč	obouruč	k6eAd1
a	a	k8xC
následně	následně	k6eAd1
začal	začít	k5eAaPmAgMnS
hrát	hrát	k5eAaImF
forhend	forhend	k1gInSc4
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
strýc	strýc	k1gMnSc1
Toni	Toni	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ho	on	k3xPp3gMnSc4
koučoval	koučovat	k5eAaImAgMnS
od	od	k7c2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc4
pojetí	pojetí	k1gNnSc4
podpořil	podpořit	k5eAaPmAgMnS
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
výhodu	výhoda	k1gFnSc4
leváků	levák	k1gMnPc2
na	na	k7c6
dvorci	dvorec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýtus	mýtus	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
strýcův	strýcův	k2eAgInSc4d1
nápad	nápad	k1gInSc4
<g/>
,	,	kIx,
vyvrátil	vyvrátit	k5eAaPmAgMnS
Toni	Toni	k1gFnSc4
Nadal	nadat	k5eAaPmAgMnS
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
z	z	k7c2
června	červen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžném	běžný	k2eAgInSc6d1
životě	život	k1gInSc6
je	být	k5eAaImIp3nS
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
výhradně	výhradně	k6eAd1
pravák	pravák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
časopis	časopis	k1gInSc4
Sports	Sportsa	k1gFnPc2
Illustrated	Illustrated	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
se	se	k3xPyFc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
agnostika	agnostik	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
školním	školní	k2eAgInSc6d1
věku	věk	k1gInSc6
sledoval	sledovat	k5eAaImAgInS
příběhy	příběh	k1gInPc7
Gokiho	Goki	k1gMnSc2
v	v	k7c6
japonském	japonský	k2eAgNnSc6d1
anime	animat	k5eAaPmIp3nS
Dragon	Dragon	k1gMnSc1
Ball	Ball	k1gMnSc1
(	(	kIx(
<g/>
Dračí	dračí	k2eAgFnSc2d1
koule	koule	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgInSc1d2
článek	článek	k1gInSc1
CNN	CNN	kA
<g/>
,	,	kIx,
věnující	věnující	k2eAgMnSc1d1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnPc3
dětským	dětský	k2eAgFnPc3d1
inspiracím	inspirace	k1gFnPc3
<g/>
,	,	kIx,
jej	on	k3xPp3gInSc4
v	v	k7c6
narážce	narážka	k1gFnSc6
na	na	k7c4
anime	animat	k5eAaPmIp3nS
označil	označit	k5eAaPmAgMnS
za	za	k7c4
„	„	k?
<g/>
dračí	dračí	k2eAgFnSc4d1
kouli	koule	k1gFnSc4
v	v	k7c6
tenisu	tenis	k1gInSc6
/	/	kIx~
<g/>
dračí	dračí	k2eAgInSc4d1
tenisák	tenisák	k1gInSc4
<g/>
/	/	kIx~
<g/>
“	“	k?
(	(	kIx(
<g/>
Dragon	Dragon	k1gMnSc1
Ball	Ball	k1gMnSc1
of	of	k?
tennis	tennis	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
neortodoxní	ortodoxní	k2eNgInSc4d1
herní	herní	k2eAgInSc4d1
styl	styl	k1gInSc4
„	„	k?
<g/>
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyjma	vyjma	k7c2
tenisu	tenis	k1gInSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
také	také	k9
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
golfu	golf	k1gInSc2
a	a	k8xC
pokeru	poker	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
dubna	duben	k1gInSc2
2014	#num#	k4
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
Monaku	Monako	k1gNnSc6
poker	poker	k1gInSc4
s	s	k7c7
ženskou	ženská	k1gFnSc7
světovou	světový	k2eAgFnSc4d1
jedničku	jednička	k1gFnSc4
Vanessou	Vanessý	k2eAgFnSc7d1
Selbstovou	Selbstová	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Knižní	knižní	k2eAgInSc1d1
životopis	životopis	k1gInSc1
Rafa	Rafa	k1gFnSc1
(	(	kIx(
<g/>
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4013	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
1092	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
napsaný	napsaný	k2eAgMnSc1d1
s	s	k7c7
novinářem	novinář	k1gMnSc7
Johnem	John	k1gMnSc7
Carlinem	Carlin	k1gInSc7
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
navázal	navázat	k5eAaPmAgInS
partnerský	partnerský	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
Maríou	Maríý	k2eAgFnSc7d1
Franciscou	Francisca	k1gFnSc7
(	(	kIx(
<g/>
Xisca	Xisca	k1gFnSc1
<g/>
)	)	kIx)
Perellóovou	Perellóová	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
lednu	leden	k1gInSc6
2019	#num#	k4
následovalo	následovat	k5eAaImAgNnS
zasnoubení	zasnoubení	k1gNnSc1
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sňatek	sňatek	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
v	v	k7c6
mallorské	mallorský	k2eAgFnSc6d1
pevnosti	pevnost	k1gFnSc6
La	la	k1gNnSc2
Fortaleza	Fortalez	k1gMnSc4
<g/>
,	,	kIx,
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
bývalého	bývalý	k2eAgMnSc2d1
španělského	španělský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Juana	Juan	k1gMnSc2
Carlose	Carlosa	k1gFnSc3
I.	I.	kA
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tenisová	tenisový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Na	na	k7c6
žebříčku	žebříčko	k1gNnSc6
ATP	atp	kA
byl	být	k5eAaImAgInS
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
nejvýše	nejvýše	k6eAd1,k6eAd3
klasifikován	klasifikovat	k5eAaImNgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
2008	#num#	k4
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
vydržel	vydržet	k5eAaPmAgMnS
nepřetržitě	přetržitě	k6eNd1
46	#num#	k4
týdnů	týden	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
se	s	k7c7
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
do	do	k7c2
čela	čelo	k1gNnSc2
vrátil	vrátit	k5eAaPmAgMnS
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tenise	tenis	k1gInSc6
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
na	na	k7c6
antukovém	antukový	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
dubnem	duben	k1gInSc7
2005	#num#	k4
až	až	k8xS
květnem	květno	k1gNnSc7
2007	#num#	k4
na	na	k7c6
antuce	antuka	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgMnS
sérii	série	k1gFnSc4
81	#num#	k4
<g/>
zápasové	zápasový	k2eAgFnSc2d1
neporazitelnosti	neporazitelnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
skončila	skončit	k5eAaPmAgFnS
finálovou	finálový	k2eAgFnSc7d1
prohrou	prohra	k1gFnSc7
v	v	k7c6
Hamburku	Hamburk	k1gInSc6
s	s	k7c7
Federerem	Federer	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
šňůry	šňůra	k1gFnSc2
získal	získat	k5eAaPmAgMnS
třináct	třináct	k4xCc4
antukových	antukový	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představuje	představovat	k5eAaImIp3nS
historický	historický	k2eAgInSc1d1
rekord	rekord	k1gInSc1
mezi	mezi	k7c7
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgInSc1d1
vybojoval	vybojovat	k5eAaPmAgInS
padesát	padesát	k4xCc1
devět	devět	k4xCc4
antukových	antukový	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
dosavadní	dosavadní	k2eAgFnSc6d1
kariéře	kariéra	k1gFnSc6
na	na	k7c6
okruhu	okruh	k1gInSc6
ATP	atp	kA
Tour	Tour	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
osmdesát	osmdesát	k4xCc4
sedm	sedm	k4xCc4
turnajů	turnaj	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
čtvrtou	čtvrtý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
rekordních	rekordní	k2eAgInPc2d1
dvacet	dvacet	k4xCc4
grandslamů	grandslam	k1gInPc2
–	–	k?
French	French	k1gInSc1
Open	Open	k1gInSc1
v	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
a	a	k8xC
2020	#num#	k4
<g/>
,	,	kIx,
Wimbledon	Wimbledon	k1gInSc1
2008	#num#	k4
i	i	k9
2010	#num#	k4
<g/>
,	,	kIx,
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2009	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
US	US	kA
Open	Open	k1gInSc4
2010	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
,	,	kIx,
2017	#num#	k4
i	i	k8xC
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
wimbledonských	wimbledonský	k2eAgNnPc2d1
finále	finále	k1gNnPc2
2006	#num#	k4
a	a	k8xC
2007	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
nestačil	stačit	k5eNaBmAgInS
na	na	k7c4
světovou	světový	k2eAgFnSc4d1
jedničku	jednička	k1gFnSc4
Rogera	Roger	k1gMnSc2
Federera	Federer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
singlovým	singlový	k2eAgFnPc3d1
trofejím	trofej	k1gFnPc3
přidal	přidat	k5eAaPmAgMnS
na	na	k7c6
túře	túra	k1gFnSc6
ATP	atp	kA
jedenáct	jedenáct	k4xCc4
triumfů	triumf	k1gInPc2
ze	z	k7c2
čtyřhry	čtyřhra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2009	#num#	k4
musel	muset	k5eAaImAgMnS
kvůli	kvůli	k7c3
chronickým	chronický	k2eAgFnPc3d1
potížím	potíž	k1gFnPc3
s	s	k7c7
koleny	koleno	k1gNnPc7
odříct	odříct	k5eAaPmF
účast	účast	k1gFnSc4
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgMnS
obhajovat	obhajovat	k5eAaImF
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
turnaji	turnaj	k1gInSc6
v	v	k7c6
Monte-Carlu	Monte-Carl	k1gInSc6
2010	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
tenista	tenista	k1gMnSc1
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
stejný	stejný	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
šestkrát	šestkrát	k6eAd1
v	v	k7c6
řadě	řada	k1gFnSc6
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
porazil	porazit	k5eAaPmAgMnS
Verdasca	Verdasca	k1gMnSc1
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
nejkratší	krátký	k2eAgNnSc4d3
finále	finále	k1gNnSc4
kategorie	kategorie	k1gFnSc2
Masters	Mastersa	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
finále	finále	k1gNnSc6
turnaje	turnaj	k1gInSc2
v	v	k7c6
Madridu	Madrid	k1gInSc6
v	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
porazil	porazit	k5eAaPmAgMnS
obhájce	obhájce	k1gMnSc1
titulu	titul	k1gInSc2
Rogera	Rogera	k1gFnSc1
Federera	Federera	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
získal	získat	k5eAaPmAgMnS
rekordní	rekordní	k2eAgInSc4d1
osmnáctý	osmnáctý	k4xOgInSc4
titul	titul	k1gInSc4
z	z	k7c2
kategorie	kategorie	k1gFnSc2
Masters	Masters	k1gInSc1
<g/>
,	,	kIx,
nejvíce	nejvíce	k6eAd1,k6eAd3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
tenistů	tenista	k1gMnPc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
Andre	Andr	k1gInSc5
Agassi	Agasse	k1gFnSc3
jich	on	k3xPp3gMnPc2
nasbíral	nasbírat	k5eAaPmAgInS
sedmnáct	sedmnáct	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
tenista	tenista	k1gMnSc1
také	také	k9
vyhrál	vyhrát	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
za	za	k7c7
sebou	se	k3xPyFc7
jdoucí	jdoucí	k2eAgInPc1d1
antukové	antukový	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
v	v	k7c6
Monte-Carlu	Monte-Carl	k1gInSc6
<g/>
,	,	kIx,
Římě	Řím	k1gInSc6
a	a	k8xC
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
těchto	tento	k3xDgInPc6
turnajích	turnaj	k1gInPc6
ztratil	ztratit	k5eAaPmAgMnS
celkově	celkově	k6eAd1
jen	jen	k6eAd1
dva	dva	k4xCgInPc4
sety	set	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
výsledky	výsledek	k1gInPc7
mu	on	k3xPp3gMnSc3
k	k	k7c3
17	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2010	#num#	k4
zajistily	zajistit	k5eAaPmAgFnP
návrat	návrat	k1gInSc4
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
žebříčku	žebříček	k1gInSc2
ATP.	atp.	kA
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
na	na	k7c4
French	French	k1gInSc4
Open	Open	k1gInSc4
2010	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
vrchol	vrchol	k1gInSc4
žebříčku	žebříček	k1gInSc2
ATP.	atp.	kA
Následně	následně	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
zbývající	zbývající	k2eAgInPc4d1
dva	dva	k4xCgInPc4
grandslamy	grandslam	k1gInPc4
sezóny	sezóna	k1gFnSc2
Wimbledon	Wimbledon	k1gInSc1
i	i	k8xC
US	US	kA
Open	Open	k1gInSc4
a	a	k8xC
potvrdil	potvrdit	k5eAaPmAgMnS
nadvládu	nadvláda	k1gFnSc4
v	v	k7c6
daném	daný	k2eAgInSc6d1
kalendářním	kalendářní	k2eAgInSc6d1
roku	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
převzal	převzít	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Laureus	Laureus	k1gMnSc1
<g/>
,	,	kIx,
označovanou	označovaný	k2eAgFnSc4d1
za	za	k7c4
sportovního	sportovní	k2eAgMnSc4d1
Oscara	Oscar	k1gMnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
sportovce	sportovec	k1gMnSc4
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2012	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
opustil	opustit	k5eAaPmAgInS
čtyřikrát	čtyřikrát	k6eAd1
v	v	k7c6
řadě	řada	k1gFnSc6
Wimbledon	Wimbledon	k1gInSc4
v	v	k7c6
rané	raný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
raketě	raketa	k1gFnSc6
tenisty	tenista	k1gMnSc2
postaveného	postavený	k2eAgInSc2d1
na	na	k7c4
100	#num#	k4
<g/>
.	.	kIx.
či	či	k8xC
nižším	nízký	k2eAgNnSc6d2
místě	místo	k1gNnSc6
světové	světový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
tak	tak	k9
Španěla	Španěl	k1gMnSc4
vyřadili	vyřadit	k5eAaPmAgMnP
Lukáš	Lukáš	k1gMnSc1
Rosol	Rosol	k1gMnSc1
(	(	kIx(
<g/>
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
Darcis	Darcis	k1gFnSc2
(	(	kIx(
<g/>
135	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nick	Nick	k1gMnSc1
Kyrgios	Kyrgios	k1gMnSc1
(	(	kIx(
<g/>
144	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
konečně	konečně	k6eAd1
německý	německý	k2eAgMnSc1d1
kvalifikant	kvalifikant	k1gMnSc1
Dustin	Dustin	k1gMnSc1
Brown	Brown	k1gMnSc1
(	(	kIx(
<g/>
102	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
zisku	zisk	k1gInSc6
desáté	desátá	k1gFnSc2
trofeje	trofej	k1gFnSc2
na	na	k7c6
Barcelona	Barcelona	k1gFnSc1
Open	Open	k1gNnSc1
2017	#num#	k4
po	po	k7c6
něm	on	k3xPp3gMnSc6
organizátoři	organizátor	k1gMnPc1
pojmenovali	pojmenovat	k5eAaPmAgMnP
centrální	centrální	k2eAgInSc4d1
dvorec	dvorec	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
následném	následný	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
barcelonského	barcelonský	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
2018	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
jako	jako	k9
čtvrtý	čtvrtý	k4xOgMnSc1
hráč	hráč	k1gMnSc1
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
400	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
na	na	k7c6
antuce	antuka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navázal	navázat	k5eAaPmAgMnS
tak	tak	k9
na	na	k7c4
Guillerma	Guillerm	k1gMnSc4
Vilase	Vilasa	k1gFnSc6
(	(	kIx(
<g/>
659	#num#	k4
výher	výhra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Manuela	Manuela	k1gFnSc1
Orantese	Orantese	k1gFnSc1
(	(	kIx(
<g/>
502	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Thomase	Thomas	k1gMnSc2
Mustera	Muster	k1gMnSc2
(	(	kIx(
<g/>
422	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Antukovou	antukový	k2eAgFnSc4d1
neporazitelnost	neporazitelnost	k1gFnSc4
čítající	čítající	k2eAgFnSc4d1
21	#num#	k4
zápasů	zápas	k1gInPc2
a	a	k8xC
50	#num#	k4
setů	set	k1gInPc2
zaznamenal	zaznamenat	k5eAaPmAgInS
od	od	k7c2
French	Frencha	k1gFnPc2
Open	Open	k1gNnSc1
2017	#num#	k4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
Mutua	Mutu	k1gInSc2
Madrid	Madrid	k1gInSc1
Open	Open	k1gInSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohraničily	ohraničit	k5eAaPmAgFnP
ji	on	k3xPp3gFnSc4
prohry	prohra	k1gFnSc2
s	s	k7c7
Rakušanem	Rakušan	k1gMnSc7
Dominicem	Dominic	k1gMnSc7
Thiemem	Thiem	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2017	#num#	k4
stal	stát	k5eAaPmAgMnS
jediným	jediný	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
Nadala	nadat	k5eAaPmAgFnS
na	na	k7c6
antuce	antuka	k1gFnSc6
porazil	porazit	k5eAaPmAgMnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
květnového	květnový	k2eAgNnSc2d1
Rome	Rom	k1gMnSc5
Masters	Mastersa	k1gFnPc2
2017	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
týdnu	týden	k1gInSc6
před	před	k7c4
Roland	Roland	k1gInSc4
Garros	Garrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
Španěl	Španěl	k1gMnSc1
Thiema	Thiemum	k1gNnSc2
zdolal	zdolat	k5eAaPmAgMnS
ve	v	k7c6
finále	finále	k1gNnSc6
předcházejícího	předcházející	k2eAgInSc2d1
Mutua	Mutu	k1gInSc2
Madrid	Madrid	k1gInSc1
Open	Open	k1gInSc1
2017	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
semifinále	semifinále	k1gNnSc6
French	Frencha	k1gFnPc2
Open	Openo	k1gNnPc2
2017	#num#	k4
a	a	k8xC
deklasoval	deklasovat	k5eAaBmAgMnS
jej	on	k3xPp3gMnSc4
na	na	k7c4
Monte-Carlo	Monte-Carlo	k1gNnSc4
Rolex	Rolex	k1gInSc4
Masters	Masters	k1gInSc4
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouskému	rakouský	k2eAgMnSc3d1
tenistovi	tenista	k1gMnSc3
pak	pak	k6eAd1
podlehl	podlehnout	k5eAaPmAgMnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
Mutua	Mutu	k1gInSc2
Madrid	Madrid	k1gInSc1
Open	Open	k1gInSc1
2018	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
přišel	přijít	k5eAaPmAgMnS
na	na	k7c4
jeden	jeden	k4xCgInSc4
týden	týden	k1gInSc4
o	o	k7c4
post	post	k1gInSc4
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thiem	Thiem	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
Djokovićovi	Djokovića	k1gMnSc6
a	a	k8xC
Gaudiovi	Gaudius	k1gMnSc6
třetím	třetí	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
mallorského	mallorský	k2eAgMnSc4d1
rodáka	rodák	k1gMnSc4
dokázal	dokázat	k5eAaPmAgMnS
na	na	k7c6
antuce	antuka	k1gFnSc6
porazit	porazit	k5eAaPmF
alespoň	alespoň	k9
třikrát	třikrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Nadalově	Nadalův	k2eAgInSc6d1
osmém	osmý	k4xOgInSc6
triumfu	triumf	k1gInSc6
na	na	k7c6
Rome	Rom	k1gMnSc5
Masters	Masters	k1gInSc1
2018	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
světové	světový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Vítězství	vítězství	k1gNnSc1
na	na	k7c4
Rogers	Rogers	k1gInSc4
Cupu	cup	k1gInSc2
2018	#num#	k4
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Canada	Canada	k1gFnSc1
Masters	Mastersa	k1gFnPc2
stal	stát	k5eAaPmAgInS
prvním	první	k4xOgInSc7
turnajem	turnaj	k1gInSc7
série	série	k1gFnSc2
Masters	Masters	k1gInSc4
s	s	k7c7
tvrdým	tvrdý	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgNnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
čtyřikrát	čtyřikrát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
French	Fren	k1gFnPc6
Open	Open	k1gNnSc1
2019	#num#	k4
postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
přes	přes	k7c4
Federera	Federer	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
titul	titul	k1gInSc4
porazil	porazit	k5eAaPmAgMnS
světovou	světový	k2eAgFnSc4d1
čtyřku	čtyřka	k1gFnSc4
Dominica	Dominicus	k1gMnSc2
Thiema	Thiem	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
mu	on	k3xPp3gMnSc3
odebral	odebrat	k5eAaPmAgMnS
jediný	jediný	k2eAgInSc4d1
set	set	k1gInSc4
na	na	k7c6
turnaji	turnaj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ziskem	zisk	k1gInSc7
dvanáctého	dvanáctý	k4xOgInSc2
triumfu	triumf	k1gInSc2
překonal	překonat	k5eAaPmAgInS
historický	historický	k2eAgInSc1d1
grandslamový	grandslamový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
držel	držet	k5eAaImAgMnS
s	s	k7c7
Australankou	Australanka	k1gFnSc7
Margaret	Margareta	k1gFnPc2
Courtovou	Courtová	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Finálová	finálový	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
950	#num#	k4
<g/>
.	.	kIx.
vítězný	vítězný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
na	na	k7c6
okruhu	okruh	k1gInSc6
ATP	atp	kA
Tour	Toura	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
řadilo	řadit	k5eAaImAgNnS
na	na	k7c4
4	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
za	za	k7c2
Federera	Federero	k1gNnSc2
<g/>
,	,	kIx,
Connorse	Connorse	k1gFnSc2
a	a	k8xC
Lendla	Lendla	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
se	se	k3xPyFc4
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
2019	#num#	k4
probojoval	probojovat	k5eAaPmAgMnS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
čtyřech	čtyři	k4xCgInPc6
setech	set	k1gInPc6
podlehl	podlehnout	k5eAaPmAgMnS
Federerovi	Federer	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Vítězně	vítězně	k6eAd1
pak	pak	k6eAd1
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
pětisetové	pětisetový	k2eAgFnSc2d1
finálové	finálový	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
na	na	k7c4
US	US	kA
Open	Open	k1gInSc4
2019	#num#	k4
proti	proti	k7c3
Rusu	Rus	k1gMnSc3
Daniilu	Daniil	k1gMnSc3
Medveděvovi	Medveděva	k1gMnSc3
a	a	k8xC
ve	v	k7c6
Flushing	Flushing	k1gInSc1
Meadows	Meadows	k1gInSc1
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgInS
čtvrtou	čtvrtý	k4xOgFnSc4
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
Australian	Australian	k1gInSc4
Open	Open	k1gInSc4
2020	#num#	k4
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
vyřadil	vyřadit	k5eAaPmAgMnS
Dominik	Dominik	k1gMnSc1
Thiem	Thiem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc4
trofej	trofej	k1gFnSc4
z	z	k7c2
acapulského	acapulský	k2eAgMnSc2d1
Mexican	Mexican	k1gInSc1
Open	Open	k1gNnSc1
získal	získat	k5eAaPmAgInS
na	na	k7c6
únorovém	únorový	k2eAgInSc6d1
Abierto	Abierta	k1gFnSc5
Mexicano	Mexicana	k1gFnSc5
Telcel	Telcel	k1gMnSc7
2020	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
titul	titul	k1gInSc4
přehrál	přehrát	k5eAaPmAgMnS
Američana	Američan	k1gMnSc4
Taylora	Taylor	k1gMnSc4
Fritze	Fritza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
pětiměsíční	pětiměsíční	k2eAgNnSc1d1
přerušení	přerušení	k1gNnSc1
sezóny	sezóna	k1gFnSc2
kvůli	kvůli	k7c3
koronavirové	koronavirový	k2eAgFnSc3d1
pandemii	pandemie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
se	se	k3xPyFc4
odhlásil	odhlásit	k5eAaPmAgMnS
z	z	k7c2
US	US	kA
Open	Open	k1gInSc4
2020	#num#	k4
pro	pro	k7c4
neuspokojivý	uspokojivý	k2eNgInSc4d1
průběh	průběh	k1gInSc4
infekce	infekce	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
turnajem	turnaj	k1gInSc7
v	v	k7c6
obnovené	obnovený	k2eAgFnSc6d1
části	část	k1gFnSc6
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
římský	římský	k2eAgMnSc1d1
Internazionali	Internazionali	k1gMnSc1
BNL	BNL	kA
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italia	k1gFnSc1
2020	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgMnSc6
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc2
podlehl	podlehnout	k5eAaPmAgMnS
Diegu	Dieg	k1gMnSc3
Schwartzmanovi	Schwartzman	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argentinec	Argentinec	k1gMnSc1
jej	on	k3xPp3gMnSc4
přehrál	přehrát	k5eAaPmAgMnS
až	až	k9
v	v	k7c6
desátém	desátý	k4xOgNnSc6
vzájemném	vzájemný	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
starou	starý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
Schwartzmanovi	Schwartzman	k1gMnSc6
oplatil	oplatit	k5eAaPmAgMnS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
French	French	k1gMnSc1
Open	Open	k1gMnSc1
2020	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zvládl	zvládnout	k5eAaPmAgMnS
i	i	k9
finálový	finálový	k2eAgInSc4d1
duel	duel	k1gInSc4
proti	proti	k7c3
Djokovićovi	Djokovića	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordní	rekordní	k2eAgInPc1d1
třináctou	třináctý	k4xOgFnSc4
trofejí	trofej	k1gFnPc2
z	z	k7c2
Roland	Rolanda	k1gFnPc2
Garros	Garrosa	k1gFnPc2
navýšil	navýšit	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
grandslamový	grandslamový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
v	v	k7c6
počtu	počet	k1gInSc6
titulů	titul	k1gInPc2
z	z	k7c2
dvouhry	dvouhra	k1gFnSc2
jediného	jediné	k1gNnSc2
majoru	major	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
rovněž	rovněž	k9
prvním	první	k4xOgMnSc7
tenistou	tenista	k1gMnSc7
s	s	k7c7
třinácti	třináct	k4xCc7
trofejemi	trofej	k1gFnPc7
z	z	k7c2
jediného	jediný	k2eAgInSc2d1
profesionálního	profesionální	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvacátým	dvacátý	k4xOgInSc7
kariérním	kariérní	k2eAgInSc7d1
grandslamem	grandslam	k1gInSc7
se	se	k3xPyFc4
v	v	k7c6
mužských	mužský	k2eAgFnPc6d1
statistikách	statistika	k1gFnPc6
vyrovnal	vyrovnat	k5eAaPmAgMnS,k5eAaBmAgMnS
prvnímu	první	k4xOgMnSc3
Federerovi	Federer	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finálovou	finálový	k2eAgFnSc7d1
výhrou	výhra	k1gFnSc7
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
1	#num#	k4
200	#num#	k4
<g/>
.	.	kIx.
zápasu	zápas	k1gInSc2
na	na	k7c6
ATP	atp	kA
Tour	Tour	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
tenista	tenista	k1gMnSc1
na	na	k7c4
jubilejní	jubilejní	k2eAgFnPc4d1
100	#num#	k4
<g/>
.	.	kIx.
pařížské	pařížský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
<g/>
,	,	kIx,
respektive	respektive	k9
999	#num#	k4
<g/>
.	.	kIx.
vítězný	vítězný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
na	na	k7c6
okruhu	okruh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
turnaje	turnaj	k1gInSc2
odjížděl	odjíždět	k5eAaImAgMnS
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
bilancí	bilance	k1gFnSc7
zápasů	zápas	k1gInPc2
100	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
a	a	k8xC
setů	set	k1gInPc2
299	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
<g/>
,	,	kIx,
včetně	včetně	k7c2
semifinálových	semifinálový	k2eAgFnPc2d1
a	a	k8xC
finálových	finálový	k2eAgFnPc2d1
výher	výhra	k1gFnPc2
v	v	k7c6
poměru	poměr	k1gInSc6
26	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
prvním	první	k4xOgMnSc7
mužem	muž	k1gMnSc7
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
počtvrté	počtvrté	k4xO
ovládl	ovládnout	k5eAaPmAgInS
grandslam	grandslam	k1gInSc1
bez	bez	k7c2
ztráty	ztráta	k1gFnSc2
setu	set	k1gInSc2
a	a	k8xC
prvním	první	k4xOgMnSc7
tenistou	tenista	k1gMnSc7
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
podesáté	podesáté	k4xO
obhájit	obhájit	k5eAaPmF
titul	titul	k1gInSc4
z	z	k7c2
jediného	jediný	k2eAgInSc2d1
majoru	major	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Týmové	týmový	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Emoce	emoce	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
French	Frencha	k1gFnPc2
Open	Openo	k1gNnPc2
2007	#num#	k4
proti	proti	k7c3
Federerovi	Federerovi	k1gRnPc1
</s>
<s>
Davis	Davis	k1gInSc1
Cup	cup	k1gInSc1
</s>
<s>
Ve	v	k7c6
španělském	španělský	k2eAgInSc6d1
daviscupovém	daviscupový	k2eAgInSc6d1
týmu	tým	k1gInSc6
debutoval	debutovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
brněnským	brněnský	k2eAgMnPc3d1
1	#num#	k4
<g/>
.	.	kIx.
kolem	kolem	k7c2
Světové	světový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
proti	proti	k7c3
České	český	k2eAgFnSc3d1
republice	republika	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
prohrál	prohrát	k5eAaPmAgMnS
s	s	k7c7
Jiřím	Jiří	k1gMnSc7
Novákem	Novák	k1gMnSc7
a	a	k8xC
zdolal	zdolat	k5eAaPmAgMnS
Radka	Radek	k1gMnSc4
Štěpánka	Štěpánek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
odešel	odejít	k5eAaPmAgMnS
poražen	porazit	k5eAaPmNgMnS
také	také	k6eAd1
ze	z	k7c2
čtyřhry	čtyřhra	k1gFnSc2
<g/>
,	,	kIx,
Španělé	Španěl	k1gMnPc1
postoupili	postoupit	k5eAaPmAgMnP
po	po	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
na	na	k7c4
zápasy	zápas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porážka	porážka	k1gFnSc1
od	od	k7c2
Nováka	Novák	k1gMnSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
prohrou	prohra	k1gFnSc7
z	z	k7c2
celkových	celkový	k2eAgInPc2d1
30	#num#	k4
odehraných	odehraný	k2eAgInPc2d1
singlů	singl	k1gInPc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
a	a	k8xC
2019	#num#	k4
dovedl	dovést	k5eAaPmAgInS
španělský	španělský	k2eAgInSc1d1
tým	tým	k1gInSc1
k	k	k7c3
zisku	zisk	k1gInSc3
salátové	salátový	k2eAgFnSc2d1
mísy	mísa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
v	v	k7c6
Davis	Davis	k1gFnSc6
Cupu	cupat	k5eAaImIp1nS
nastoupil	nastoupit	k5eAaPmAgMnS
k	k	k7c3
dvaceti	dvacet	k4xCc3
třem	tři	k4xCgInPc3
mezistátním	mezistátní	k2eAgNnPc3d1
utkáním	utkání	k1gNnSc7
s	s	k7c7
bilancí	bilance	k1gFnSc7
29	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
a	a	k8xC
8	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
reprezentoval	reprezentovat	k5eAaImAgInS
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mužské	mužský	k2eAgFnSc6d1
dvouhře	dvouhra	k1gFnSc6
startoval	startovat	k5eAaBmAgMnS
jako	jako	k9
druhý	druhý	k4xOgMnSc1
nasazený	nasazený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
zdolal	zdolat	k5eAaPmAgMnS
Novaka	Novak	k1gMnSc4
Djokoviće	Djoković	k1gMnSc4
a	a	k8xC
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
přehrál	přehrát	k5eAaPmAgInS
chilskou	chilský	k2eAgFnSc4d1
turnajovou	turnajový	k2eAgFnSc4d1
dvanáctku	dvanáctka	k1gFnSc4
Fernanda	Fernando	k1gNnSc2
Gonzáleze	Gonzáleze	k1gFnSc2
bez	bez	k7c2
ztráty	ztráta	k1gFnSc2
setu	set	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
mužské	mužský	k2eAgFnSc2d1
čtyřhry	čtyřhra	k1gFnSc2
nastoupili	nastoupit	k5eAaPmAgMnP
s	s	k7c7
Tommym	Tommymum	k1gNnPc2
Robredem	Robred	k1gInSc7
z	z	k7c2
pozice	pozice	k1gFnSc2
šestých	šestý	k4xOgInPc2
nasazených	nasazený	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěž	soutěž	k1gFnSc1
opustili	opustit	k5eAaPmAgMnP
po	po	k7c6
prohře	prohra	k1gFnSc6
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
kole	kolo	k1gNnSc6
od	od	k7c2
australského	australský	k2eAgInSc2d1
páru	pár	k1gInSc2
Chris	Chris	k1gFnSc2
Guccione	Guccion	k1gInSc5
a	a	k8xC
Lleyton	Lleyton	k1gInSc4
Hewitt	Hewitta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
londýnských	londýnský	k2eAgFnPc2d1
Her	hra	k1gFnPc2
XXX	XXX	kA
<g/>
.	.	kIx.
olympiády	olympiáda	k1gFnSc2
se	se	k3xPyFc4
odhlásil	odhlásit	k5eAaPmAgMnS
pro	pro	k7c4
poranění	poranění	k1gNnSc4
kolena	koleno	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c4
řadu	řada	k1gFnSc4
měsíců	měsíc	k1gInPc2
posledním	poslední	k2eAgInSc7d1
turnajem	turnaj	k1gInSc7
byl	být	k5eAaImAgMnS
červencový	červencový	k2eAgInSc4d1
Wimbledon	Wimbledon	k1gInSc4
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
zahajovacím	zahajovací	k2eAgInSc6d1
ceremoniálu	ceremoniál	k1gInSc6
Letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2016	#num#	k4
v	v	k7c6
Riu	Riu	k1gFnSc6
de	de	k?
Janeiru	Janeira	k1gMnSc4
byl	být	k5eAaImAgMnS
vlajkonošem	vlajkonoš	k1gMnSc7
španělské	španělský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ATP	atp	kA
Cup	cup	k1gInSc1
</s>
<s>
Na	na	k7c6
premiérovém	premiérový	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
australské	australský	k2eAgFnSc2d1
týmové	týmový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
dovedl	dovést	k5eAaPmAgInS
španělský	španělský	k2eAgInSc1d1
výběr	výběr	k1gInSc1
z	z	k7c2
pozice	pozice	k1gFnSc2
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
vyhrál	vyhrát	k5eAaPmAgMnS
čtyři	čtyři	k4xCgInPc4
ze	z	k7c2
šesti	šest	k4xCc2
dvouher	dvouhra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
přemožiteli	přemožitel	k1gMnPc7
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
Belgičan	Belgičan	k1gMnSc1
David	David	k1gMnSc1
Goffin	Goffin	k1gMnSc1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
a	a	k8xC
druhý	druhý	k4xOgMnSc1
hráč	hráč	k1gMnSc1
žebříčku	žebříček	k1gInSc2
Srb	Srb	k1gMnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
titul	titul	k1gInSc4
Španělsko	Španělsko	k1gNnSc1
podlehlo	podlehnout	k5eAaPmAgNnS
Srbsku	Srbsko	k1gNnSc3
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
na	na	k7c4
zápasy	zápas	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzájemná	vzájemný	k2eAgNnPc1d1
soupeření	soupeření	k1gNnPc1
</s>
<s>
Federer	Federer	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Soupeření	soupeření	k1gNnSc2
Federera	Federer	k1gMnSc2
a	a	k8xC
Nadala	nadat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
a	a	k8xC
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
na	na	k7c6
okruhu	okruh	k1gInSc6
ATP	atp	kA
střetávají	střetávat	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
vzájemná	vzájemný	k2eAgNnPc1d1
utkání	utkání	k1gNnPc1
představují	představovat	k5eAaImIp3nP
významnou	významný	k2eAgFnSc4d1
část	část	k1gFnSc4
profesionální	profesionální	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
pro	pro	k7c4
každého	každý	k3xTgMnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Wimbledonu	Wimbledon	k1gInSc2
2006	#num#	k4
<g/>
:	:	kIx,
<g/>
Federer	Federer	k1gMnSc1
<g/>
–	–	k?
<g/>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Oba	dva	k4xCgMnPc1
hráči	hráč	k1gMnPc1
figurovali	figurovat	k5eAaImAgMnP
na	na	k7c4
první	první	k4xOgInSc4
a	a	k8xC
druhé	druhý	k4xOgFnSc3
příčce	příčka	k1gFnSc3
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
nepřetržitě	přetržitě	k6eNd1
od	od	k7c2
července	červenec	k1gInSc2
2005	#num#	k4
až	až	k9
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Nadala	nadat	k5eAaPmAgFnS
na	na	k7c6
druhé	druhý	k4xOgFnSc6
pozici	pozice	k1gFnSc6
vystřídal	vystřídat	k5eAaPmAgMnS
Skot	Skot	k1gMnSc1
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Švýcar	Švýcary	k1gInPc2
a	a	k8xC
Španěl	Španěly	k1gInPc2
představují	představovat	k5eAaImIp3nP
jedinou	jediný	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
mužského	mužský	k2eAgInSc2d1
tenisu	tenis	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zakončila	zakončit	k5eAaPmAgFnS
šest	šest	k4xCc4
sezón	sezóna	k1gFnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
na	na	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgNnPc6
místech	místo	k1gNnPc6
světové	světový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
období	období	k1gNnSc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
února	únor	k1gInSc2
2004	#num#	k4
byl	být	k5eAaImAgInS
Federer	Federer	k1gInSc4
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
nepřetržitě	přetržitě	k6eNd1
237	#num#	k4
týdnů	týden	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
představuje	představovat	k5eAaImIp3nS
rekordní	rekordní	k2eAgNnSc4d1
období	období	k1gNnSc4
v	v	k7c6
tenise	tenis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
mladší	mladý	k2eAgFnSc3d2
Nadal	nadat	k5eAaPmAgMnS
se	se	k3xPyFc4
propracoval	propracovat	k5eAaPmAgMnS
na	na	k7c4
druhou	druhý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
v	v	k7c6
červenci	červenec	k1gInSc6
2005	#num#	k4
a	a	k8xC
držel	držet	k5eAaImAgInS
ji	on	k3xPp3gFnSc4
160	#num#	k4
týdnů	týden	k1gInPc2
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
také	také	k9
vytvořil	vytvořit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
na	na	k7c6
této	tento	k3xDgFnSc6
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
srpnu	srpen	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
vystřídal	vystřídat	k5eAaPmAgMnS
basilejského	basilejský	k2eAgMnSc4d1
rodáka	rodák	k1gMnSc4
na	na	k7c6
čele	čelo	k1gNnSc6
mužského	mužský	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
má	mít	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc4d1
bilanci	bilance	k1gFnSc4
vzájemných	vzájemný	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
v	v	k7c6
poměru	poměr	k1gInSc6
24	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestnáct	šestnáct	k4xCc1
z	z	k7c2
třiceti	třicet	k4xCc2
devíti	devět	k4xCc2
duelů	duel	k1gInPc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
na	na	k7c6
antuce	antuka	k1gFnSc6
<g/>
,	,	kIx,
nejdominantnějším	dominantní	k2eAgInSc6d3
povrchu	povrch	k1gInSc6
mallorského	mallorský	k2eAgMnSc2d1
rodáka	rodák	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Federer	Federer	k1gMnSc1
vícekrát	vícekrát	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
na	na	k7c4
jím	on	k3xPp3gMnSc7
preferované	preferovaný	k2eAgFnSc6d1
trávě	tráva	k1gFnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
v	v	k7c6
hale	hala	k1gFnSc6
na	na	k7c6
tvrdém	tvrdý	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
(	(	kIx(
<g/>
5	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
španělský	španělský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
je	být	k5eAaImIp3nS
úspěšnější	úspěšný	k2eAgMnSc1d2
na	na	k7c6
otevřených	otevřený	k2eAgInPc6d1
dvorcích	dvorec	k1gInPc6
s	s	k7c7
tvrdým	tvrdý	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
antuce	antuka	k1gFnSc6
(	(	kIx(
<g/>
14	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Vzhledem	vzhledem	k7c3
ke	k	k7c3
skutečnosti	skutečnost	k1gFnSc3
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
že	že	k8xS
nasazování	nasazování	k1gNnSc1
na	na	k7c6
turnajích	turnaj	k1gInPc6
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
žebříčkového	žebříčkový	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
utkali	utkat	k5eAaPmAgMnP
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
hráči	hráč	k1gMnPc1
čtyřiadvacetkrát	čtyřiadvacetkrát	k6eAd1
až	až	k6eAd1
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
devětkrát	devětkrát	k6eAd1
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
grandslamový	grandslamový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
také	také	k9
rekordní	rekordní	k2eAgInSc1d1
zápis	zápis	k1gInSc1
dvojice	dvojice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
až	až	k9
2008	#num#	k4
proti	proti	k7c3
sobě	se	k3xPyFc3
nastoupili	nastoupit	k5eAaPmAgMnP
v	v	k7c6
každém	každý	k3xTgNnSc6
finále	finále	k1gNnSc6
French	French	k1gMnSc1
Open	Open	k1gMnSc1
a	a	k8xC
Wimbledonu	Wimbledon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potkali	potkat	k5eAaPmAgMnP
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
zápasu	zápas	k1gInSc6
Australian	Australiana	k1gFnPc2
Open	Open	k1gNnSc1
2009	#num#	k4
a	a	k8xC
French	French	k1gMnSc1
Open	Open	k1gInSc4
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
získal	získat	k5eAaPmAgMnS
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
šest	šest	k4xCc1
výher	výhra	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
ztratil	ztratit	k5eAaPmAgMnS
pouze	pouze	k6eAd1
první	první	k4xOgNnSc4
dvě	dva	k4xCgNnPc4
wimbledonská	wimbledonský	k2eAgNnPc4d1
finále	finále	k1gNnPc4
a	a	k8xC
jedno	jeden	k4xCgNnSc1
melbournské	melbournský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc1
z	z	k7c2
devíti	devět	k4xCc2
grandslamových	grandslamový	k2eAgNnPc2d1
klání	klání	k1gNnPc2
o	o	k7c4
titul	titul	k1gInSc4
měla	mít	k5eAaImAgFnS
pětisetový	pětisetový	k2eAgInSc4d1
průběh	průběh	k1gInSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
2007	#num#	k4
a	a	k8xC
2008	#num#	k4
a	a	k8xC
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
2009	#num#	k4
i	i	k9
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wimbledonské	wimbledonský	k2eAgFnSc6d1
finále	finála	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
bývá	bývat	k5eAaImIp3nS
řadou	řada	k1gFnSc7
tenisových	tenisový	k2eAgMnPc2d1
analytiků	analytik	k1gMnPc2
označováno	označován	k2eAgNnSc1d1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
zápas	zápas	k1gInSc4
historie	historie	k1gFnSc2
tenisu	tenis	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třináct	třináct	k4xCc1
střetnutí	střetnutí	k1gNnPc2
dospělo	dochvít	k5eAaPmAgNnS
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
rozhodující	rozhodující	k2eAgFnSc2d1
sady	sada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pár	pár	k4xCyI
také	také	k9
vytvořil	vytvořit	k5eAaPmAgMnS
rekordní	rekordní	k2eAgInSc4d1
počet	počet	k1gInSc4
dvanácti	dvanáct	k4xCc2
vzájemných	vzájemný	k2eAgNnPc2d1
finále	finále	k1gNnPc2
v	v	k7c6
sérii	série	k1gFnSc6
Masters	Mastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgInPc4
finálové	finálový	k2eAgInPc4d1
boje	boj	k1gInPc4
patří	patřit	k5eAaImIp3nS
i	i	k9
dlouhá	dlouhý	k2eAgFnSc1d1
pětihodinová	pětihodinový	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
na	na	k7c4
Rome	Rom	k1gMnSc5
Masters	Mastersa	k1gFnPc2
2006	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
získal	získat	k5eAaPmAgMnS
Španěl	Španěl	k1gMnSc1
až	až	k9
v	v	k7c6
tiebreaku	tiebreak	k1gInSc6
pátého	pátý	k4xOgInSc2
setu	set	k1gInSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
předtím	předtím	k6eAd1
odvrátil	odvrátit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
mečboly	mečbol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Djoković	Djoković	k?
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Soupeření	soupeření	k1gNnSc2
Djokoviće	Djoković	k1gFnSc2
a	a	k8xC
Nadala	nadat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Novak	Novak	k1gMnSc1
DjokovićRafael	DjokovićRafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
a	a	k8xC
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
svedli	svést	k5eAaPmAgMnP
větší	veliký	k2eAgInSc4d2
počet	počet	k1gInSc4
vzájemných	vzájemný	k2eAgNnPc2d1
střetnutí	střetnutí	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
jakákoli	jakýkoli	k3yIgFnSc1
jiná	jiný	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
tenisu	tenis	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
utkali	utkat	k5eAaPmAgMnP
v	v	k7c6
padesáti	padesát	k4xCc6
šesti	šest	k4xCc6
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
kladnou	kladný	k2eAgFnSc4d1
bilanci	bilance	k1gFnSc4
výher	výhra	k1gFnPc2
a	a	k8xC
proher	prohra	k1gFnPc2
poprvé	poprvé	k6eAd1
drží	držet	k5eAaImIp3nS
Djoković	Djoković	k1gFnSc4
těsným	těsný	k2eAgInSc7d1
poměrem	poměr	k1gInSc7
29	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezónách	sezóna	k1gFnPc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
byla	být	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
série	série	k1gFnSc1
zastiňována	zastiňovat	k5eAaImNgFnS
soupeřením	soupeření	k1gNnSc7
Federera	Federero	k1gNnSc2
a	a	k8xC
Nadala	nadat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
pozornost	pozornost	k1gFnSc1
na	na	k7c4
sebe	sebe	k3xPyFc4
začala	začít	k5eAaPmAgNnP
poutat	poutat	k5eAaImF
od	od	k7c2
prvního	první	k4xOgNnSc2
finále	finále	k1gNnSc4
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc2
na	na	k7c4
US	US	kA
Open	Open	k1gInSc4
2010	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
dvojice	dvojice	k1gFnSc1
utkala	utkat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
pár	pár	k1gInSc4
tenistů	tenista	k1gMnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
členové	člen	k1gMnPc1
proti	proti	k7c3
sobě	se	k3xPyFc3
odehráli	odehrát	k5eAaPmAgMnP
finálové	finálový	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
na	na	k7c6
všech	všecek	k3xTgInPc6
čtyřech	čtyři	k4xCgInPc6
grandslamových	grandslamový	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finálový	finálový	k2eAgInSc1d1
duel	duel	k1gInSc1
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Open	k1gInSc1
2012	#num#	k4
je	být	k5eAaImIp3nS
řadou	řada	k1gFnSc7
odborníků	odborník	k1gMnPc2
řazen	řadit	k5eAaImNgInS
k	k	k7c3
nejlepším	dobrý	k2eAgInPc3d3
utkáním	utkání	k1gNnSc7
tenisové	tenisový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mary	Mary	k1gFnSc1
Carillová	Carillová	k1gFnSc1
a	a	k8xC
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
označili	označit	k5eAaPmAgMnP
tento	tento	k3xDgInSc4
zápas	zápas	k1gInSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
semifinále	semifinále	k1gNnSc7
na	na	k7c4
French	French	k1gInSc4
Open	Open	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
vůbec	vůbec	k9
za	za	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
klání	klání	k1gNnSc4
hraná	hraný	k2eAgFnSc1d1
na	na	k7c6
tvrdém	tvrdý	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
a	a	k8xC
antuce	antuka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Web	web	k1gInSc1
ATPworldtour	ATPworldtoura	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
hodnotil	hodnotit	k5eAaImAgMnS
soupeření	soupeření	k1gNnSc4
této	tento	k3xDgFnSc2
dvojice	dvojice	k1gFnSc2
za	za	k7c4
třetí	třetí	k4xOgNnSc4
nejlepší	dobrý	k2eAgNnSc4d3
v	v	k7c6
první	první	k4xOgFnSc6
dekádě	dekáda	k1gFnSc6
třetího	třetí	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
duel	duel	k1gInSc1
oba	dva	k4xCgMnPc1
proti	proti	k7c3
sobě	se	k3xPyFc3
odehráli	odehrát	k5eAaPmAgMnP
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
French	French	k1gMnSc1
Open	Open	k1gMnSc1
2006	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgMnSc2
vítězně	vítězně	k6eAd1
vyšel	vyjít	k5eAaPmAgMnS
Španěl	Španěl	k1gMnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
byl	být	k5eAaImAgMnS
srbský	srbský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
přinucen	přinutit	k5eAaPmNgMnS
utkání	utkání	k1gNnSc3
ve	v	k7c6
třetí	třetí	k4xOgFnSc6
sadě	sada	k1gFnSc6
skrečovat	skrečovat	k5eAaPmF
pro	pro	k7c4
zranění	zranění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zápasu	zápas	k1gInSc6
Djoković	Djoković	k1gFnSc2
médiím	médium	k1gNnPc3
sdělil	sdělit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pochopil	pochopit	k5eAaPmAgMnS
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Nadala	nadat	k5eAaPmAgFnS
přehrál	přehrát	k5eAaPmAgMnS
a	a	k8xC
dodal	dodat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
soupeř	soupeř	k1gMnSc1
byl	být	k5eAaImAgMnS
„	„	k?
<g/>
na	na	k7c6
antuce	antuka	k1gFnSc6
k	k	k7c3
poražení	poražení	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zaujal	zaujmout	k5eAaPmAgMnS
Srb	Srb	k1gMnSc1
roli	role	k1gFnSc4
hlavního	hlavní	k2eAgMnSc2d1
Nadalova	Nadalův	k2eAgMnSc2d1
soupeře	soupeř	k1gMnSc2
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
nejdominantnější	dominantní	k2eAgFnSc6d3
antuce	antuka	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
na	na	k7c6
tomto	tento	k3xDgInSc6
povrchu	povrch	k1gInSc6
uštědřil	uštědřit	k5eAaPmAgMnS
přes	přes	k7c4
polovinu	polovina	k1gFnSc4
všech	všecek	k3xTgFnPc2
porážek	porážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
premiérovému	premiérový	k2eAgNnSc3d1
finále	finále	k1gNnSc3
oba	dva	k4xCgMnPc1
nastoupili	nastoupit	k5eAaPmAgMnP
na	na	k7c6
Indian	Indiana	k1gFnPc2
Wells	Wells	k1gInSc4
Masters	Masters	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vyšel	vyjít	k5eAaPmAgInS
vítězně	vítězně	k6eAd1
mallorský	mallorský	k2eAgMnSc1d1
rodák	rodák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
teprve	teprve	k6eAd1
druhé	druhý	k4xOgNnSc4
vzájemné	vzájemný	k2eAgNnSc4d1
střetnutí	střetnutí	k1gNnSc4
na	na	k7c6
okruhu	okruh	k1gInSc6
ATP	atp	kA
Tour	Tour	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
týden	týden	k1gInSc4
později	pozdě	k6eAd2
slavil	slavit	k5eAaImAgInS
debutový	debutový	k2eAgInSc1d1
triumf	triumf	k1gInSc1
Djoković	Djoković	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
soupeře	soupeř	k1gMnSc4
zdolal	zdolat	k5eAaPmAgMnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
Miami	Miami	k1gNnSc2
Masters	Masters	k1gInSc4
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
března	březen	k1gInSc2
2011	#num#	k4
do	do	k7c2
dubna	duben	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
dvojice	dvojice	k1gFnSc1
utkala	utkat	k5eAaPmAgFnS
ve	v	k7c6
třinácti	třináct	k4xCc6
finálových	finálový	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
osm	osm	k4xCc4
vyhrál	vyhrát	k5eAaPmAgMnS
srbský	srbský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výběr	výběr	k1gInSc1
historických	historický	k2eAgInPc2d1
turnajových	turnajový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
</s>
<s>
rekord	rekord	k1gInSc1
</s>
<s>
sdílení	sdílení	k1gNnSc4
</s>
<s>
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc4
<g/>
187713	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
dvouhry	dvouhra	k1gFnSc2
jediného	jediné	k1gNnSc2
grandslamuzůstává	grandslamuzůstávat	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
</s>
<s>
10	#num#	k4
sezón	sezóna	k1gFnPc2
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
vítěz	vítěz	k1gMnSc1
alespoň	alespoň	k9
1	#num#	k4
titulu	titul	k1gInSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
vítěz	vítěz	k1gMnSc1
alespoň	alespoň	k9
1	#num#	k4
titulu	titul	k1gInSc2
v	v	k7c6
14	#num#	k4
sezónách	sezóna	k1gFnPc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vítěz	vítěz	k1gMnSc1
titulů	titul	k1gInPc2
na	na	k7c6
3	#num#	k4
površích	površí	k1gNnPc6
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
kalendářním	kalendářní	k2eAgInSc6d1
roce	rok	k1gInSc6
</s>
<s>
3	#num#	k4
navazující	navazující	k2eAgInPc1d1
tituly	titul	k1gInPc1
na	na	k7c4
3	#num#	k4
různých	různý	k2eAgInPc2d1
površích	povrch	k1gInPc6
</s>
<s>
9	#num#	k4
obhájených	obhájený	k2eAgMnPc2d1
titulů	titul	k1gInPc2
v	v	k7c6
mužské	mužský	k2eAgFnSc6d1
dvouhře	dvouhra	k1gFnSc6
jediného	jediný	k2eAgInSc2d1
grandslamu	grandslam	k1gInSc2
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
192513	#num#	k4
titulů	titul	k1gInPc2
v	v	k7c6
mužské	mužský	k2eAgFnSc6d1
dvouhře	dvouhra	k1gFnSc6
</s>
<s>
Antukové	antukový	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
<g/>
189161	#num#	k4
titulů	titul	k1gInPc2
v	v	k7c6
mužské	mužský	k2eAgFnSc6d1
dvouhře	dvouhra	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3
úspěšnost	úspěšnost	k1gFnSc1
vyhraných	vyhraný	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
445	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
,	,	kIx,
91,75	91,75	k4
%	%	kIx~
v	v	k7c6
letech	let	k1gInPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ATP	atp	kA
Masters	Masters	k1gInSc4
1000197034	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
jediného	jediný	k2eAgInSc2d1
Mastersu	Masters	k1gInSc2
(	(	kIx(
<g/>
Monte-Carlo	Monte-Carlo	k1gNnSc1
Masters	Mastersa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
sezón	sezóna	k1gFnPc2
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
vítěz	vítěz	k1gMnSc1
alespoň	alespoň	k9
1	#num#	k4
titulu	titul	k1gInSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
čtvrtfinále	čtvrtfinále	k1gNnPc1
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
Open	Open	k1gInSc1
<g/>
195312	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Monte-Carlo	Monte-Carlo	k1gNnSc4
Masters	Mastersa	k1gFnPc2
<g/>
189711	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Rome	Rom	k1gMnSc5
Masters	Mastersa	k1gFnPc2
<g/>
19309	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Madrid	Madrid	k1gInSc1
Open	Open	k1gInSc1
<g/>
20025	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
<g/>
1973	#num#	k4
<g/>
nejstarší	starý	k2eAgFnSc1d3
konečná	konečný	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
(	(	kIx(
<g/>
33	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
interval	interval	k1gInSc4
poprvé	poprvé	k6eAd1
a	a	k8xC
naposledy	naposledy	k6eAd1
konečnou	konečný	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
(	(	kIx(
<g/>
11	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nejdelší	dlouhý	k2eAgNnSc1d3
období	období	k1gNnSc1
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
531	#num#	k4
týdnů	týden	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
na	na	k7c6
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc2
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
<g/>
:	:	kIx,
28	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stav	stav	k1gInSc1
</s>
<s>
rok	rok	k1gInSc4
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
</s>
<s>
povrch	povrch	k1gInSc4
</s>
<s>
soupeř	soupeř	k1gMnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2005	#num#	k4
<g/>
French	French	k1gInSc1
Openantuka	Openantuk	k1gMnSc2
Mariano	Mariana	k1gFnSc5
Puerta	Puert	k1gMnSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2006	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2006	#num#	k4
<g/>
Wimbledontráva	Wimbledontráv	k1gMnSc2
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2007	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2007	#num#	k4
<g/>
Wimbledon	Wimbledon	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
tráva	tráva	k1gFnSc1
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2008	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2008	#num#	k4
<g/>
Wimbledontráva	Wimbledontráv	k1gMnSc2
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2009	#num#	k4
<g/>
Australian	Australian	k1gInSc1
Opentvrdý	Opentvrdý	k2eAgInSc4d1
Roger	Roger	k1gInSc4
Federer	Federra	k1gFnPc2
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2010	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Robin	Robina	k1gFnPc2
Söderling	Söderling	k1gInSc4
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2010	#num#	k4
<g/>
Wimbledon	Wimbledon	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
tráva	tráva	k1gFnSc1
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2010	#num#	k4
<g/>
US	US	kA
Opentvrdý	Opentvrdý	k2eAgInSc1d1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2011	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Roger	Roger	k1gInSc4
Federer	Federer	k1gInSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2011	#num#	k4
<g/>
Wimbledon	Wimbledon	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
tráva	tráva	k1gFnSc1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2011	#num#	k4
<g/>
US	US	kA
Opentvrdý	Opentvrdý	k2eAgInSc1d1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2012	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Opentvrdý	Opentvrdý	k2eAgInSc1d1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2012	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2013	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2013	#num#	k4
<g/>
US	US	kA
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc1d1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2014	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc1d1
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2014	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2017	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc1d1
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2017	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2017	#num#	k4
<g/>
US	US	kA
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgMnSc1d1
Kevin	Kevin	k1gMnSc1
Anderson	Anderson	k1gMnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2018	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Dominic	Dominice	k1gFnPc2
Thiem	Thi	k1gInSc7
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
<g/>
2019	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Opentvrdý	Opentvrdý	k2eAgInSc1d1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2019	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Dominic	Dominice	k1gFnPc2
Thiem	Thi	k1gInSc7
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2019	#num#	k4
<g/>
US	US	kA
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc1d1
Daniil	Daniil	k1gInSc1
Medveděv	Medveděv	k1gFnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
<g/>
2020	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Opena	k1gFnPc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
antuka	antuka	k1gFnSc1
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
na	na	k7c6
Turnaji	turnaj	k1gInSc6
mistrů	mistr	k1gMnPc2
</s>
<s>
Sezóna	sezóna	k1gFnSc1
<g/>
200520062007200820092010201120122013201420152016201720182019	#num#	k4
<g/>
SRV	SRV	kA
<g/>
–	–	k?
<g/>
P	P	kA
%	%	kIx~
</s>
<s>
A	a	k9
</s>
<s>
SF	SF	kA
</s>
<s>
SF	SF	kA
</s>
<s>
A	a	k9
</s>
<s>
ZS	ZS	kA
</s>
<s>
F	F	kA
</s>
<s>
ZS	ZS	kA
</s>
<s>
A	a	k9
</s>
<s>
F	F	kA
</s>
<s>
A	a	k9
</s>
<s>
SF	SF	kA
</s>
<s>
A	a	k9
</s>
<s>
ZS	ZS	kA
</s>
<s>
A	a	k9
</s>
<s>
ZS	ZS	kA
</s>
<s>
0	#num#	k4
/	/	kIx~
9	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
</s>
<s>
56	#num#	k4
%	%	kIx~
</s>
<s>
Finále	finále	k1gNnSc1
na	na	k7c6
okruhu	okruh	k1gInSc6
ATP	atp	kA
Tour	Tour	k1gMnSc1
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
<g/>
:	:	kIx,
124	#num#	k4
(	(	kIx(
<g/>
87	#num#	k4
<g/>
–	–	k?
<g/>
37	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olympijské	olympijský	k2eAgNnSc4d1
zlato	zlato	k1gNnSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
Masters	Masters	k1gInSc1
Series	Series	k1gInSc1
/	/	kIx~
ATP	atp	kA
Tour	Tour	k1gInSc1
Masters	Masters	k1gInSc1
1000	#num#	k4
(	(	kIx(
<g/>
35	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
International	International	k1gMnSc1
Series	Series	k1gMnSc1
Gold	Gold	k1gMnSc1
/	/	kIx~
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
International	International	k1gMnSc1
Series	Series	k1gMnSc1
/	/	kIx~
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
dle	dle	k7c2
povrchu	povrch	k1gInSc2
</s>
<s>
tvrdý	tvrdý	k2eAgMnSc1d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
(	(	kIx(
<g/>
61	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
koberec	koberec	k1gInSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
dle	dle	k7c2
místa	místo	k1gNnSc2
</s>
<s>
venku	venku	k6eAd1
(	(	kIx(
<g/>
85	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hala	hala	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stav	stav	k1gInSc1
</s>
<s>
č.	č.	k?
</s>
<s>
datum	datum	k1gNnSc1
</s>
<s>
turnaj	turnaj	k1gInSc1
</s>
<s>
povrch	povrch	k1gInSc4
</s>
<s>
soupeř	soupeř	k1gMnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
</s>
<s>
Auckland	Auckland	k1gInSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Dominik	Dominik	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2004	#num#	k4
</s>
<s>
Sopoty	sopot	k1gInPc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
José	José	k6eAd1
Acasuso	Acasusa	k1gFnSc5
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
</s>
<s>
Costa	Costa	k1gFnSc1
do	do	k7c2
Sauípe	Sauíp	k1gInSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Alberto	Alberta	k1gFnSc5
Martín	Martín	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
</s>
<s>
Acapulco	Acapulco	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Montañ	Montañ	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
</s>
<s>
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc4
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc4
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Guillermo	Guillermo	k6eAd1
Coria	Coria	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Ferrero	Ferrero	k1gNnSc4
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Guillermo	Guillermo	k6eAd1
Coria	Coria	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2005	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Mariano	Mariana	k1gFnSc5
Puerta	Puerta	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
</s>
<s>
Bastad	Bastad	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Gastón	Gastón	k1gMnSc1
Gaudio	Gaudio	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
</s>
<s>
Montreal	Montreal	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Guillermo	Guillermo	k6eAd1
Coria	Coria	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc4d1
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Ljubičić	Ljubičić	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
</s>
<s>
Dubaj	Dubaj	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Tommy	Tomma	k1gFnPc1
Robredo	Robredo	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2006	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2006	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
Masters	Masters	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Guillermo	Guillermo	k6eAd1
Cañ	Cañ	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Fernando	Fernando	k6eAd1
González	González	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
</s>
<s>
Hamburk	Hamburk	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2007	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2007	#num#	k4
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Stanislas	Stanislas	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
</s>
<s>
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
koberec	koberec	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Nalbandian	Nalbandian	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
</s>
<s>
Čennaí	Čennaí	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Michail	Michail	k1gMnSc1
Južnyj	Južnyj	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
</s>
<s>
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Davyděnko	Davyděnka	k1gFnSc5
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
</s>
<s>
Monte-Carlo	Monte-Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
</s>
<s>
Hamburk	Hamburk	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2008	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2008	#num#	k4
</s>
<s>
Queen	Queen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Club	club	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
</s>
<s>
Toronto	Toronto	k1gNnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Nicolas	Nicolas	k1gMnSc1
Kiefer	Kiefer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Fernando	Fernando	k6eAd1
González	González	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
</s>
<s>
Rotterdam	Rotterdam	k1gInSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Andy	Anda	k1gFnPc1
Murray	Murraa	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2009	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Andy	Anda	k1gFnPc1
Murray	Murraa	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Davyděnko	Davyděnka	k1gFnSc5
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gInSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Davyděnko	Davyděnka	k1gFnSc5
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Fernando	Fernando	k6eAd1
Verdasco	Verdasco	k6eAd1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Robin	robin	k2eAgInSc1d1
Söderling	Söderling	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2010	#num#	k4
</s>
<s>
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Gaël	Gaël	k1gInSc1
Monfils	Monfilsa	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
</s>
<s>
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc4d1
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
</s>
<s>
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
sntuka	sntuk	k1gMnSc4
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2011	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
</s>
<s>
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Andy	Anda	k1gFnPc1
Murray	Murraa	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
49	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
Viñ	Viñ	k1gFnSc1
del	del	k?
Mar	Mar	k1gFnSc2
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc2
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Horacio	Horacio	k1gMnSc1
Zeballos	Zeballos	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
Sã	Sã	k1gFnSc5
Paulo	Paula	k1gFnSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Nalbandian	Nalbandian	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2013	#num#	k4
</s>
<s>
Acapulco	Acapulco	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2013	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Juan	Juan	k1gMnSc1
Martín	Martín	k1gMnSc1
del	del	k?
Potro	Potro	k1gNnSc4
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2013	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc4
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc4
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2013	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Nicolás	Nicolás	k6eAd1
Almagro	Almagro	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Stanislas	Stanislas	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
</s>
<s>
Montréal	Montréal	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Milos	Milos	k1gInSc1
Raonic	Raonice	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
59	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
</s>
<s>
Cincinnati	Cincinnat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
John	John	k1gMnSc1
Isner	Isner	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
</s>
<s>
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc4d1
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gInSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Gaël	Gaël	k1gInSc1
Monfils	Monfilsa	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Stanislas	Stanislas	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Dolgopolov	Dolgopolov	k1gInSc4
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
</s>
<s>
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Kei	Kei	k?
Nišikori	Nišikori	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
skreč	skreč	k1gInSc1
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Mónaco	Mónaco	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Andy	Anda	k1gFnPc1
Murray	Murraa	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
67	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
</s>
<s>
Hamburk	Hamburk	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Fabio	Fabio	k6eAd1
Fognini	Fognin	k2eAgMnPc1d1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
</s>
<s>
Basilej	Basilej	k1gFnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc4d1
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Gaël	Gaël	k1gInSc1
Monfils	Monfilsa	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Kei	Kei	k?
Nišikori	Nišikori	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
</s>
<s>
Acapulco	Acapulco	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Sam	Sam	k1gMnSc1
Querrey	Querrea	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20170402	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
</s>
<s>
Miami	Miami	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Ramos-Viñ	Ramos-Viñ	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Kevin	Kevin	k1gMnSc1
Anderson	Anderson	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Nick	Nick	k1gMnSc1
Kyrgios	Kyrgios	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Kei	Kei	k?
Nišikori	Nišikori	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Zverev	Zverva	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
</s>
<s>
Toronto	Toronto	k1gNnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
81	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
83	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
</s>
<s>
Montréal	Montréal	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
85	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
Acapulco	Acapulco	k1gNnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Taylor	Taylor	k1gMnSc1
Fritz	Fritz	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
<g/>
:	:	kIx,
15	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olympijské	olympijský	k2eAgNnSc4d1
zlato	zlato	k1gNnSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc1
Masters	Masters	k1gInSc1
1000	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
dle	dle	k7c2
povrchu	povrch	k1gInSc2
</s>
<s>
tvrdý	tvrdý	k2eAgMnSc1d1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
antuka	antuka	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tráva	tráva	k1gFnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
koberec	koberec	k1gInSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
dle	dle	k7c2
místa	místo	k1gNnSc2
</s>
<s>
venku	venku	k6eAd1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hala	hala	k1gFnSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stav	stav	k1gInSc1
</s>
<s>
č.	č.	k?
</s>
<s>
datum	datum	k1gNnSc1
</s>
<s>
turnaj	turnaj	k1gInSc1
</s>
<s>
povrch	povrch	k1gInSc4
</s>
<s>
spoluhráč	spoluhráč	k1gMnSc1
</s>
<s>
soupeři	soupeř	k1gMnPc1
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2003	#num#	k4
</s>
<s>
Umag	Umag	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Álex	Álex	k1gInSc1
López	López	k1gMnSc1
Morón	Morón	k1gMnSc1
</s>
<s>
Todd	Todd	k1gMnSc1
Perry	Perra	k1gFnSc2
Thomas	Thomas	k1gMnSc1
Šimada	Šimada	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
</s>
<s>
Čennaí	Čennaí	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Tommy	Tomma	k1gFnPc1
Robredo	Robredo	k1gNnSc1
</s>
<s>
Jonatan	Jonatan	k1gMnSc1
Erlich	Erlich	k1gMnSc1
Andy	Anda	k1gFnSc2
Ram	Ram	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Albert	Albert	k1gMnSc1
Costa	Costa	k1gMnSc1
</s>
<s>
Andrei	Andrea	k1gFnSc3
Pavel	Pavel	k1gMnSc1
Michail	Michail	k1gMnSc1
Južnyj	Južnyj	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Feliciano	Feliciana	k1gFnSc5
López	Lópeza	k1gFnPc2
</s>
<s>
Leander	Leander	k1gMnSc1
Paes	Paesa	k1gFnPc2
Nenad	Nenad	k1gInSc4
Zimonjić	Zimonjić	k1gFnSc4
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
</s>
<s>
Čennaí	Čennaí	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Tomeu	Tome	k2eAgFnSc4d1
Salvà	Salvà	k1gFnSc4
</s>
<s>
Xavier	Xavier	k1gMnSc1
Malisse	Malisse	k1gFnSc2
Dick	Dick	k1gMnSc1
Norman	Norman	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Tomeu	Tome	k2eAgFnSc4d1
Salvà	Salvà	k1gFnSc4
</s>
<s>
Andrei	Andrea	k1gFnSc3
Pavel	Pavel	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Waske	Waske	k1gInSc4
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Carlo	Carlo	k1gNnSc4
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc4
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Tommy	Tomma	k1gFnPc1
Robredo	Robredo	k1gNnSc1
</s>
<s>
Mahesh	Mahesh	k1gMnSc1
Bhupathi	Bhupathi	k1gNnSc2
Mark	Mark	k1gMnSc1
Knowles	Knowles	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
Nenad	Nenad	k1gInSc4
Zimonjić	Zimonjić	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
Nenad	Nenad	k1gInSc4
Zimonjić	Zimonjić	k1gMnSc3
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
Daniele	Daniela	k1gFnSc3
Bracciali	Bracciali	k1gMnSc1
Andreas	Andreas	k1gMnSc1
Seppi	Seppe	k1gFnSc4
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
</s>
<s>
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
John	John	k1gMnSc1
Isner	Isner	k1gMnSc1
Sam	Sam	k1gMnSc1
Querrey	Querrea	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
Viñ	Viñ	k1gFnSc1
del	del	k?
Mar	Mar	k1gFnSc2
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc2
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Mónaco	Mónaco	k1gMnSc1
</s>
<s>
Paolo	Paola	k1gMnSc5
Lorenzi	Lorenz	k1gMnSc5
Potito	Potita	k1gMnSc5
Starace	Starace	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
</s>
<s>
Dauhá	Dauhat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Juan	Juan	k1gMnSc1
Mónaco	Mónaco	k1gMnSc1
</s>
<s>
Julian	Julian	k1gMnSc1
Knowle	Knowle	k1gNnSc2
Philipp	Philipp	k1gMnSc1
Oswald	Oswald	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
Florin	Florin	k1gInSc1
Mergea	Merge	k2eAgFnSc1d1
Horia	Horia	k1gFnSc1
Tecău	Tecăus	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Pablo	Pabnout	k5eAaImAgNnS,k5eAaPmAgNnS
Carreñ	Carreñ	k1gNnSc1
Busta	busta	k1gFnSc1
</s>
<s>
Jack	Jack	k1gMnSc1
Sock	Sock	k1gMnSc1
Bernard	Bernard	k1gMnSc1
Tomic	Tomic	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
−	−	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Utkání	utkání	k1gNnSc1
o	o	k7c4
olympijské	olympijský	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
zlato	zlato	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
č.	č.	k?
</s>
<s>
datum	datum	k1gNnSc1
</s>
<s>
turnaj	turnaj	k1gInSc1
</s>
<s>
povrch	povrch	k1gInSc4
</s>
<s>
soupeř	soupeř	k1gMnSc1
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
medaili	medaile	k1gFnSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
</s>
<s>
LOH	LOH	kA
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Fernando	Fernando	k6eAd1
González	González	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
zlato	zlato	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
č.	č.	k?
</s>
<s>
datum	datum	k1gNnSc1
</s>
<s>
turnaj	turnaj	k1gInSc1
</s>
<s>
povrch	povrch	k1gInSc4
</s>
<s>
spoluhráč	spoluhráč	k1gMnSc1
</s>
<s>
soupeři	soupeř	k1gMnPc1
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
medaili	medaile	k1gFnSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
</s>
<s>
LOH	LOH	kA
2016	#num#	k4
<g/>
,	,	kIx,
Rio	Rio	k1gFnSc1
de	de	k?
Janeiro	Janeiro	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgInSc1d1
</s>
<s>
Marc	Marc	k1gFnSc1
López	Lópeza	k1gFnPc2
</s>
<s>
Florin	Florin	k1gInSc1
Mergea	Merge	k2eAgFnSc1d1
Horia	Horia	k1gFnSc1
Tecău	Tecăus	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Chronologie	chronologie	k1gFnSc1
výsledků	výsledek	k1gInPc2
na	na	k7c4
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc6
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
<g/>
2003200420052006200720082009201020112012201320142015201620172018201920202021	#num#	k4
</s>
<s>
SR	SR	kA
</s>
<s>
V	v	k7c6
<g/>
–	–	k?
<g/>
P	P	kA
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
A	a	k9
</s>
<s>
3R	3R	k4
</s>
<s>
4R	4R	k4
</s>
<s>
A	a	k9
</s>
<s>
ČF	ČF	kA
</s>
<s>
SF	SF	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
ČF	ČF	kA
</s>
<s>
ČF	ČF	kA
</s>
<s>
F	F	kA
</s>
<s>
A	a	k9
</s>
<s>
F	F	kA
</s>
<s>
ČF	ČF	kA
</s>
<s>
1R	1R	k4
</s>
<s>
F	F	kA
</s>
<s>
ČF	ČF	kA
</s>
<s>
F	F	kA
</s>
<s>
ČF	ČF	kA
</s>
<s>
ČF	ČF	kA
</s>
<s>
1	#num#	k4
/	/	kIx~
16	#num#	k4
</s>
<s>
69	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
A	a	k9
</s>
<s>
A	a	k9
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
4R	4R	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
ČF	ČF	kA
</s>
<s>
3R	3R	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
13	#num#	k4
/	/	kIx~
16	#num#	k4
</s>
<s>
100	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
3R	3R	k4
</s>
<s>
A	a	k9
</s>
<s>
2R	2R	k4
</s>
<s>
F	F	kA
</s>
<s>
F	F	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
A	a	k9
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
F	F	kA
</s>
<s>
2R	2R	k4
</s>
<s>
1R	1R	k4
</s>
<s>
4R	4R	k4
</s>
<s>
2R	2R	k4
</s>
<s>
A	a	k9
</s>
<s>
4R	4R	k4
</s>
<s>
SF	SF	kA
</s>
<s>
SF	SF	kA
</s>
<s>
NH	NH	kA
</s>
<s>
2	#num#	k4
/	/	kIx~
14	#num#	k4
</s>
<s>
53	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
</s>
<s>
2R	2R	k4
</s>
<s>
2R	2R	k4
</s>
<s>
3R	3R	k4
</s>
<s>
ČF	ČF	kA
</s>
<s>
4R	4R	k4
</s>
<s>
SF	SF	kA
</s>
<s>
SF	SF	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
F	F	kA
</s>
<s>
A	a	k9
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
A	a	k9
</s>
<s>
3R	3R	k4
</s>
<s>
4R	4R	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
SF	SF	kA
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
A	a	k9
</s>
<s>
4	#num#	k4
/	/	kIx~
15	#num#	k4
</s>
<s>
64	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
</s>
<s>
výhry	výhra	k1gFnPc1
<g/>
–	–	k?
<g/>
prohry	prohra	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
20	#num#	k4
/	/	kIx~
61	#num#	k4
</s>
<s>
286	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
</s>
<s>
*	*	kIx~
Nadal	nadat	k5eAaPmAgInS
odstoupil	odstoupit	k5eAaPmAgMnS
před	před	k7c7
třetím	třetí	k4xOgNnSc7
kolem	kolo	k1gNnSc7
French	Frencha	k1gFnPc2
Open	Open	k1gNnSc1
2016	#num#	k4
pro	pro	k7c4
poranění	poranění	k1gNnSc4
zápěstí	zápěstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neodehrané	odehraný	k2eNgNnSc1d1
utkání	utkání	k1gNnSc1
tak	tak	k6eAd1
nebylo	být	k5eNaImAgNnS
započítáno	započítat	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
prohraný	prohraný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
*	*	kIx~
Nadal	nadat	k5eAaPmAgMnS
postoupil	postoupit	k5eAaPmAgMnS
bez	bez	k7c2
boje	boj	k1gInSc2
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
kole	kolo	k1gNnSc6
US	US	kA
Open	Open	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neodehrané	odehraný	k2eNgNnSc1d1
utkání	utkání	k1gNnSc1
tak	tak	k6eAd1
nebylo	být	k5eNaImAgNnS
započítáno	započítat	k5eAaPmNgNnS
jako	jako	k8xC,k8xS
vyhraný	vyhraný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
SR	SR	kA
</s>
<s>
poměr	poměr	k1gInSc1
vyhraných	vyhraný	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
ku	k	k7c3
všem	všecek	k3xTgMnPc3
odehraným	odehraný	k2eAgMnPc3d1
</s>
<s>
W	W	kA
<g/>
–	–	k?
<g/>
LV	LV	kA
<g/>
–	–	k?
<g/>
P	P	kA
</s>
<s>
výhry	výhra	k1gFnPc1
<g/>
–	–	k?
<g/>
prohry	prohra	k1gFnSc2
</s>
<s>
NH	NH	kA
</s>
<s>
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
se	se	k3xPyFc4
turnaj	turnaj	k1gInSc4
nekonal	konat	k5eNaImAgInS
</s>
<s>
A	a	k9
</s>
<s>
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
nezúčastnil	zúčastnit	k5eNaPmAgMnS
</s>
<s>
1Q	1Q	k4
/	/	kIx~
LQ	LQ	kA
</s>
<s>
prohra	prohra	k1gFnSc1
v	v	k7c6
(	(	kIx(
<g/>
kole	kolo	k1gNnSc6
<g/>
)	)	kIx)
kvalifikace	kvalifikace	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
k	k	k7c3
/	/	kIx~
1R	1R	k4
</s>
<s>
prohra	prohra	k1gFnSc1
v	v	k7c6
daném	daný	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
turnaje	turnaj	k1gInSc2
</s>
<s>
QF	QF	kA
/	/	kIx~
ČF	ČF	kA
</s>
<s>
prohra	prohra	k1gFnSc1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
</s>
<s>
SF	SF	kA
</s>
<s>
prohra	prohra	k1gFnSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
</s>
<s>
F	F	kA
</s>
<s>
prohra	prohra	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
v	v	k7c6
turnaji	turnaj	k1gInSc6
</s>
<s>
Postavení	postavení	k1gNnSc1
na	na	k7c6
konečném	konečný	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
ATP	atp	kA
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
818	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
235	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▬	▬	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▬	▬	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
1537	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
1116	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
179	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
314	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
92	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
132	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
113	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
384	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
805	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▲	▲	k?
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
132	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
▼	▼	k?
542	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
475	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
TIGNOR	TIGNOR	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc2
Amazing	Amazing	k1gInSc4
Race	Race	k1gNnSc2
toward	towarda	k1gFnPc2
GOAT	GOAT	kA
status	status	k1gInSc1
<g/>
:	:	kIx,
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
résumé	résumé	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnPc2
<g/>
,	,	kIx,
2020-01-14	2020-01-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jan	Jan	k1gMnSc1
Vojkůvka	Vojkůvka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
výsledek	výsledek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fantazie	fantazie	k1gFnSc1
<g/>
,	,	kIx,
neskrývá	skrývat	k5eNaImIp3nS
Nadal	nadat	k5eAaPmAgMnS
radost	radost	k1gFnSc4
z	z	k7c2
návratu	návrat	k1gInSc2
do	do	k7c2
čela	čelo	k1gNnSc2
žebříčku	žebříček	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisovýsvět	Tenisovýsvět	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gInSc1
titul	titul	k1gInSc4
v	v	k7c6
Halle	Hall	k1gInSc6
neobhájil	obhájit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
podlehl	podlehnout	k5eAaPmAgMnS
Čoričovi	Čorič	k1gMnSc3
a	a	k8xC
přijde	přijít	k5eAaPmIp3nS
o	o	k7c4
tenisový	tenisový	k2eAgInSc4d1
trůn	trůn	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-06-24	2018-06-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Nadal	nadat	k5eAaPmAgMnS
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
ATP	atp	kA
Ranking	Ranking	k1gInSc1
For	forum	k1gNnPc2
Fifth	Fifth	k1gMnSc1
Time	Time	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2019-11-14	2019-11-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZABLOUDIL	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
zdolal	zdolat	k5eAaPmAgMnS
Thiema	Thiema	k1gFnSc1
a	a	k8xC
v	v	k7c6
Madridu	Madrid	k1gInSc6
slaví	slavit	k5eAaImIp3nS
rekordní	rekordní	k2eAgInSc1d1
30	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
z	z	k7c2
Masters	Mastersa	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-05-14	2017-05-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HARWITT	HARWITT	kA
<g/>
,	,	kIx,
Sandra	Sandra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Is	Is	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
the	the	k?
best	best	k2eAgInSc4d1
clay-court	clay-court	k1gInSc4
player	player	k1gMnSc1
ever	ever	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PERROTTA	PERROTTA	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
Appearing	Appearing	k1gInSc4
Unbeatable	Unbeatable	k1gMnSc2
on	on	k3xPp3gMnSc1
Clay	Cla	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
magazine	magazinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
Sun	Sun	kA
<g/>
,	,	kIx,
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZABLOUDI	zabloudit	k5eAaPmRp2nS
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
získal	získat	k5eAaPmAgInS
49	#num#	k4
<g/>
.	.	kIx.
antukový	antukový	k2eAgInSc4d1
titul	titul	k1gInSc4
a	a	k8xC
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Vilase	Vilasa	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-04-24	2016-04-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ZABLOUDIL	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
slaví	slavit	k5eAaImIp3nS
70	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
a	a	k8xC
rekordní	rekordní	k2eAgFnPc1d1
trofeje	trofej	k1gFnPc1
<g/>
:	:	kIx,
50	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
antuky	antuka	k1gFnSc2
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
z	z	k7c2
Monte	Mont	k1gInSc5
Carla	Carl	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-04-23	2017-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Unstoppable	Unstoppable	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
Captures	Captures	k1gMnSc1
Sixth	Sixth	k1gMnSc1
Straight	Straight	k1gMnSc1
Title	titla	k1gFnSc6
Monte-Carlo	Monte-Carlo	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MONTE-CARLO	MONTE-CARLO	k1gFnPc2
ROLEX	ROLEX	kA
MASTERS	MASTERS	kA
<g/>
,	,	kIx,
2010-04-18	2010-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHRISTOPHER	CHRISTOPHER	kA
CLAREY	CLAREY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
<g/>
’	’	k?
<g/>
s	s	k7c7
Reign	Reign	k1gMnSc1
in	in	k?
Monte	Mont	k1gMnSc5
Carlo	Carla	k1gMnSc5
Continues	Continues	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2011-04-17	2011-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
Saves	Saves	k1gInSc4
M.	M.	kA
<g/>
P.	P.	kA
<g/>
,	,	kIx,
Beats	Beats	k1gInSc1
Stefanos	Stefanos	k1gInSc1
Tsitsipas	Tsitsipasa	k1gFnPc2
For	forum	k1gNnPc2
12	#num#	k4
<g/>
th	th	k?
Barcelona	Barcelona	k1gFnSc1
Title	titla	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2021-04-2	2021-04-2	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
hires	hires	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Moya	Moya	k1gMnSc1
as	as	k9
he	he	k0
bids	bids	k6eAd1
to	ten	k3xDgNnSc1
revive	revivat	k5eAaPmIp3nS
injury-hit	injury-hit	k2eAgInSc1d1
careeer	careeer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
17-12-2016	17-12-2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Personal	Personal	k1gFnSc2
woes	woesa	k1gFnPc2
affecting	affecting	k1gInSc1
Rafa	Raf	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
sports	sports	k1gInSc1
<g/>
.	.	kIx.
<g/>
espn	espn	k1gInSc1
<g/>
.	.	kIx.
<g/>
go	go	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
<g/>
,	,	kIx,
23	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
roj	roj	k1gInSc1
<g/>
,	,	kIx,
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
vydělal	vydělat	k5eAaPmAgInS
miliardy	miliarda	k4xCgFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
svému	svůj	k3xOyFgMnSc3
letitému	letitý	k2eAgMnSc3d1
kouči	kouč	k1gMnSc3
nedal	dát	k5eNaPmAgMnS
ani	ani	k8xC
euro	euro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-09	2020-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Q	Q	kA
<g/>
&	&	k?
<g/>
A	a	k9
with	with	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sports	Sports	k1gInSc1
Illustrated	Illustrated	k1gInSc4
<g/>
.	.	kIx.
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
:	:	kIx,
The	The	k1gMnPc1
'	'	kIx"
<g/>
Dragon	Dragon	k1gMnSc1
Ball	Ball	k1gMnSc1
<g/>
'	'	kIx"
of	of	k?
tennis	tennis	k1gInSc1
<g/>
.	.	kIx.
www.cnngo.com	www.cnngo.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
31	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Away	Awaa	k1gFnSc2
from	froma	k1gFnPc2
tennis	tennis	k1gFnSc7
court	courta	k1gFnPc2
<g/>
,	,	kIx,
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
seeks	seeks	k1gInSc4
a	a	k8xC
golf	golf	k1gInSc4
course	course	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Miami	Miami	k1gNnSc2
Herald.	Herald.	k1gFnSc2
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rafa	Rafa	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Account	Accounta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Great	Great	k1gInSc4
experience	experienec	k1gInSc2
playing	playing	k1gInSc1
with	with	k1gInSc1
the	the	k?
world	world	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
number	number	k1gInSc1
1	#num#	k4
female	female	k6eAd1
poker	poker	k1gInSc1
player	player	k1gInSc1
Vanessa	Vaness	k1gMnSc2
and	and	k?
very	ver	k2eAgFnPc1d1
happy	happa	k1gFnPc1
to	ten	k3xDgNnSc4
have	have	k6eAd1
learned	learned	k1gInSc1
a	a	k8xC
lot	lot	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twitter	Twittra	k1gFnPc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TANDON	TANDON	kA
<g/>
,	,	kIx,
Kamakshi	Kamakshi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Open	Open	k1gMnSc1
–	–	k?
Twenty	Twenta	k1gFnSc2
things	thingsa	k1gFnPc2
we	we	k?
learn	learn	k1gMnSc1
in	in	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
autobiography	autobiograph	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
<g/>
,	,	kIx,
26-08-2011	26-08-2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OSBORNE	OSBORNE	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2017	#num#	k4
<g/>
:	:	kIx,
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
Johanna	Johanna	k1gFnSc1
Konta	konto	k1gNnSc2
through	througha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
03-01-2017	03-01-2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
engaged	engaged	k1gInSc4
to	ten	k3xDgNnSc1
girlfriend	girlfriend	k1gInSc1
of	of	k?
14	#num#	k4
years	yearsa	k1gFnPc2
Mery	Mera	k1gFnSc2
Perello	Perello	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
31-01-2019	31-01-2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BOYLE	BOYLE	kA
<g/>
,	,	kIx,
Kelli	Kelle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnPc2
Star	Star	kA
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
Marries	Marries	k1gInSc4
Maria	Mario	k1gMnSc2
Francisca	Franciscus	k1gMnSc2
Perello	Perello	k1gNnSc1
In	In	k1gMnSc1
Spain	Spain	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
<g/>
,	,	kIx,
19-10-2019	19-10-2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
i	i	k9
za	za	k7c2
pomoci	pomoc	k1gFnSc2
deště	dešť	k1gInSc2
udolal	udolat	k5eAaPmAgMnS
Zvereva	Zvereva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmý	osmý	k4xOgInSc1
titul	titul	k1gInSc1
z	z	k7c2
Říma	Řím	k1gInSc2
ho	on	k3xPp3gMnSc4
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
čela	čelo	k1gNnSc2
žebříčku	žebříček	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-05-20	2018-05-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
v	v	k7c6
Madridu	Madrid	k1gInSc6
získal	získat	k5eAaPmAgMnS
rekordní	rekordní	k2eAgInSc4d1
osmnáctý	osmnáctý	k4xOgInSc4
titul	titul	k1gInSc4
z	z	k7c2
turnajů	turnaj	k1gInPc2
Masters	Mastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-05-16	2010-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
a	a	k8xC
Vonnová	Vonnová	k1gFnSc1
získali	získat	k5eAaPmAgMnP
"	"	kIx"
<g/>
sportovní	sportovní	k2eAgFnSc1d1
Oscary	Oscara	k1gFnPc1
<g/>
"	"	kIx"
Laureus	Laureus	k1gInSc1
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
7.2	7.2	k4
<g/>
.2011	.2011	k4
<g/>
↑	↑	k?
UBHA	UBHA	kA
<g/>
,	,	kIx,
Ravi	Ravi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wimbledon	Wimbledon	k1gInSc1
2015	#num#	k4
<g/>
:	:	kIx,
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
upset	upset	k1gInSc4
by	by	kYmCp3nS
Dustin	Dustin	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
.	.	kIx.
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZABLOUDIL	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
má	mít	k5eAaImIp3nS
další	další	k2eAgMnSc1d1
'	'	kIx"
<g/>
desítku	desítka	k1gFnSc4
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
trofej	trofej	k1gFnSc4
získal	získat	k5eAaPmAgMnS
také	také	k9
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-04-30	2017-04-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
má	mít	k5eAaImIp3nS
11	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
i	i	k9
z	z	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
přehrál	přehrát	k5eAaPmAgMnS
řeckého	řecký	k2eAgMnSc4d1
mladíka	mladík	k1gMnSc4
Tsitsipase	Tsitsipasa	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BARCELONA	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-04-29	2018-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Tsitsipas	Tsitsipas	k1gMnSc1
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
řeckým	řecký	k2eAgMnSc7d1
finalistou	finalista	k1gMnSc7
po	po	k7c6
45	#num#	k4
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
Nadal	nadat	k5eAaPmAgMnS
vyhrál	vyhrát	k5eAaPmAgInS
400	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
na	na	k7c6
antuce	antuka	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BARCELONA	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-04-28	2018-04-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
Thiemem	Thiem	k1gInSc7
o	o	k7c4
antukovou	antukový	k2eAgFnSc4d1
neporazitelnost	neporazitelnost	k1gFnSc4
a	a	k8xC
ztratí	ztratit	k5eAaPmIp3nS
post	post	k1gInSc4
jedničky	jednička	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-05-10	2018-05-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejúspěšnější	úspěšný	k2eAgInSc1d3
turnaj	turnaj	k1gInSc1
na	na	k7c4
hardu	harda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
v	v	k7c6
Torontu	Toronto	k1gNnSc6
zastavil	zastavit	k5eAaPmAgMnS
Tsitsipase	Tsitsipasa	k1gFnSc3
a	a	k8xC
má	mít	k5eAaImIp3nS
80	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-08-13	2018-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
dodges	dodges	k1gInSc4
rivals	rivals	k6eAd1
like	like	k1gInSc1
Alexander	Alexandra	k1gFnPc2
Zverev	Zverva	k1gFnPc2
and	and	k?
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
-	-	kIx~
Roland	Roland	k1gInSc1
Garros	Garrosa	k1gFnPc2
draw	draw	k?
talking	talking	k1gInSc1
points	points	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSN	MSN	kA
<g/>
,	,	kIx,
2018-02-25	2018-02-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
má	mít	k5eAaImIp3nS
12	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
z	z	k7c2
Roland	Rolanda	k1gFnPc2
Garros	Garrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tolik	tolik	k6eAd1
trofejí	trofej	k1gFnSc7
z	z	k7c2
jednoho	jeden	k4xCgInSc2
grandslamu	grandslam	k1gInSc2
nikdo	nikdo	k3yNnSc1
nemá	mít	k5eNaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-06-09	2019-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Alex	Alex	k1gMnSc1
Sharp	Sharp	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
career	career	k1gMnSc1
by	by	kYmCp3nS
numbers	numbers	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roland-Garros	Roland-Garrosa	k1gFnPc2
<g/>
,	,	kIx,
2019-06-09	2019-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MITCHELL	MITCHELL	kA
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
beats	beatsa	k1gFnPc2
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
in	in	k?
four	four	k1gMnSc1
sets	sets	k6eAd1
to	ten	k3xDgNnSc4
reach	reach	k1gInSc1
Wimbledon	Wimbledon	k1gInSc1
men	men	k?
<g/>
’	’	k?
<g/>
s	s	k7c7
final	final	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-12	2019-07-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
3077	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JUREJKO	JUREJKO	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
wins	wins	k1gInSc4
US	US	kA
Open	Open	k1gNnSc1
for	forum	k1gNnPc2
19	#num#	k4
<g/>
th	th	k?
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
2019-09-09	2019-09-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Schwartzman	Schwartzman	k1gMnSc1
Stuns	Stuns	k1gInSc4
Nadal	nadat	k5eAaPmAgMnS
In	In	k1gMnSc1
Rome	Rom	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-09-19	2020-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SEMINARA	SEMINARA	kA
<g/>
,	,	kIx,
Dave	Dav	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafa	Raf	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
'	'	kIx"
<g/>
Moon	Moon	k1gInSc1
Landing	Landing	k1gInSc1
<g/>
'	'	kIx"
Among	Among	k1gInSc1
Sports	Sports	k1gInSc1
<g/>
'	'	kIx"
Greatest	Greatest	k1gInSc1
All-Time	All-Tim	k1gInSc5
Records	Records	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tennis	Tennis	k1gFnPc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-10-11	2020-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ODDO	ODDO	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
100	#num#	k4
for	forum	k1gNnPc2
the	the	k?
King	King	k1gMnSc1
<g/>
:	:	kIx,
Numbers	Numbers	k1gInSc1
behind	behind	k1gMnSc1
Rafa	Rafa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
latest	latest	k1gInSc1
Paris	Paris	k1gMnSc1
success	successa	k1gFnPc2
-	-	kIx~
Roland-Garros	Roland-Garrosa	k1gFnPc2
-	-	kIx~
The	The	k1gFnPc2
2020	#num#	k4
Roland-Garros	Roland-Garrosa	k1gFnPc2
Tournament	Tournament	k1gMnSc1
official	official	k1gMnSc1
site	sitat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roland	Rolanda	k1gFnPc2
Garros	Garrosa	k1gFnPc2
<g/>
,	,	kIx,
2020-10-11	2020-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
stránkách	stránka	k1gFnPc6
Davis	Davis	k1gFnSc2
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
<g/>
:	:	kIx,
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
<g/>
↑	↑	k?
BBC	BBC	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
ČT	ČT	kA
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
esp	esp	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
Time	Time	k1gInSc1
<g/>
,	,	kIx,
US	US	kA
Today	Todaa	k1gFnPc1
<g/>
,	,	kIx,
wtatennis	wtatennis	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phelps	Phelpsa	k1gFnPc2
<g/>
,	,	kIx,
Murray	Murraa	k1gFnSc2
či	či	k8xC
Nadal	nadat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
přivedou	přivést	k5eAaPmIp3nP
hvězdní	hvězdný	k2eAgMnPc1d1
vlajkonoši	vlajkonoš	k1gMnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2016-08-05	2016-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Serbia	Serbium	k1gNnSc2
wins	winsa	k1gFnPc2
atp	atp	kA
cup	cup	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
news	newsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Federer-Nadal	Federer-Nadal	k1gMnSc2
rivalry	rivalra	k1gMnSc2
as	as	k9
good	good	k6eAd1
as	as	k9
it	it	k?
gets	gets	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Herald	Herald	k1gMnSc1
Tribune	tribun	k1gMnSc5
(	(	kIx(
<g/>
Associated	Associated	k1gInSc1
Press	Press	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
7	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
23	#num#	k4
August	August	k1gMnSc1
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WEAVER	WEAVER	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Move	Move	k1gNnSc1
over	overa	k1gFnPc2
McEnroe	McEnroe	k1gNnSc2
and	and	k?
Borg	Borg	k1gMnSc1
<g/>
,	,	kIx,
this	this	k6eAd1
one	one	k?
will	will	k1gInSc1
run	run	k1gInSc1
and	and	k?
run	run	k1gInSc1
in	in	k?
the	the	k?
memory	memora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UK	UK	kA
<g/>
:	:	kIx,
7	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FLANAGAN	FLANAGAN	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gInSc1
v	v	k7c6
Nadal	nadat	k5eAaPmAgInS
as	as	k9
good	good	k6eAd1
as	as	k9
sport	sport	k1gInSc1
gets	getsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Age	Age	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Melbourne	Melbourne	k1gNnPc1
<g/>
:	:	kIx,
12	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BODO	BODO	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivalry	Rivalro	k1gNnPc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
30	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MACGREGOR	MACGREGOR	kA
<g/>
,	,	kIx,
Jeff	Jeff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Greatest	Greatest	k1gFnSc1
rivalry	rivalra	k1gFnSc2
of	of	k?
the	the	k?
21	#num#	k4
<g/>
st	st	kA
century	centura	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
<g/>
,	,	kIx,
3	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JAGO	Jaga	k1gFnSc5
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murray	Murray	k1gInPc7
reaches	reaches	k1gMnSc1
world	world	k6eAd1
No	no	k9
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Observer	Observer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
15	#num#	k4
August	August	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
16	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
official	official	k1gInSc1
<g/>
:	:	kIx,
Nadal	nadat	k5eAaPmAgInS
will	will	k1gMnSc1
pass	passa	k1gFnPc2
Federer	Federer	k1gMnSc1
for	forum	k1gNnPc2
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NBC	NBC	kA
Sports	Sportsa	k1gFnPc2
(	(	kIx(
<g/>
Associated	Associated	k1gInSc1
Press	Press	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
August	August	k1gMnSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FedEx	FedEx	k1gInSc1
ATP	atp	kA
Reliability	Reliabilita	k1gFnSc2
Index	index	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Head	Head	k1gInSc1
to	ten	k3xDgNnSc4
Head	Head	k1gMnSc1
player	player	k1gMnSc1
details	details	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rafa	Rafa	k1gMnSc1
&	&	k?
Roger	Roger	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Rivalry	Rivalra	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
29	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JENKINS	JENKINS	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Greatest	Greatest	k1gMnSc1
Match	Match	k1gMnSc1
Ever	Ever	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Chronicle	Chronicle	k1gFnSc1
<g/>
.	.	kIx.
7	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ALLEYNE	ALLEYNE	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wimbledon	Wimbledon	k1gInSc1
2008	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
hails	hails	k1gInSc1
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
victory	victor	k1gMnPc4
as	as	k9
greatest	greatest	k5eAaPmF
final	final	k1gMnSc1
ever	ever	k1gMnSc1
<g/>
.	.	kIx.
www.telegraph.co.uk	www.telegraph.co.uk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WERTHEIM	WERTHEIM	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Without	Without	k1gMnSc1
a	a	k8xC
doubt	doubt	k1gMnSc1
<g/>
,	,	kIx,
it	it	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
the	the	k?
greatest	greatest	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Mailbag	Mailbaga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SI	si	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
9	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TIGNOR	TIGNOR	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Concrete	Concre	k1gNnSc2
Elbow	Elbow	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
8	#num#	k4
July	Jula	k1gFnSc2
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
Edges	Edges	k1gMnSc1
Djokovic	Djokovice	k1gFnPc2
In	In	k1gFnSc2
Montreal	Montreal	k1gInSc1
Thriller	thriller	k1gInSc1
<g/>
,	,	kIx,
Faces	Faces	k1gMnSc1
Raonic	Raonice	k1gFnPc2
in	in	k?
Final	Final	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Poměr	poměr	k1gInSc1
vzájemných	vzájemný	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
:	:	kIx,
Djoković	Djoković	k1gFnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
oficiální	oficiální	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
ATP	atp	kA
Tour	Tour	k1gInSc1
<g/>
↑	↑	k?
BBC	BBC	kA
Sport	sport	k1gInSc1
–	–	k?
Novak	Novak	k1gInSc1
Djokovic	Djokovice	k1gFnPc2
v	v	k7c6
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
<g/>
:	:	kIx,
Players	Players	k1gInSc1
&	&	k?
pundits	pundits	k1gInSc1
hail	hail	k1gMnSc1
'	'	kIx"
<g/>
greatest	greatest	k1gMnSc1
<g/>
'	'	kIx"
match	match	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bbc	Bbc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2012-01-29	2012-01-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RIVALRIES	RIVALRIES	kA
OF	OF	kA
THE	THE	kA
DECADE	DECADE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novak	Novak	k1gMnSc1
&	&	k?
Rafa	Rafa	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Rivalry	Rivalra	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
6	#num#	k4
November	November	k1gInSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgInS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
–	–	k?
oficiální	oficiální	k2eAgInSc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
stránkách	stránka	k1gFnPc6
ATP	atp	kA
Tour	Tour	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
stránkách	stránka	k1gFnPc6
Mezinárodní	mezinárodní	k2eAgFnSc2d1
tenisové	tenisový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
stránkách	stránka	k1gFnPc6
Davis	Davis	k1gFnSc2
Cupu	cup	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
na	na	k7c6
Twitteru	Twitter	k1gInSc6
</s>
<s>
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
♦	♦	k?
20210426	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
♦	♦	k?
Žebříček	žebříček	k1gInSc1
WTA	WTA	kA
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhramužská	dvouhramužský	k2eAgFnSc1d1
čtyřhraženská	čtyřhraženský	k2eAgFnSc1d1
dvouhraženská	dvouhraženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
▬	▬	k?
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
▲	▲	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
▼	▼	k?
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
</s>
<s>
▬	▬	k?
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
▬	▬	k?
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
▬	▬	k?
Alexander	Alexandra	k1gFnPc2
Zverev	Zverev	k1gFnSc1
</s>
<s>
▬	▬	k?
Andrej	Andrej	k1gMnSc1
Rubljov	Rubljov	k1gInSc1
</s>
<s>
▬	▬	k?
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
▬	▬	k?
Diego	Diego	k1gMnSc1
Schwartzman	Schwartzman	k1gMnSc1
</s>
<s>
▬	▬	k?
Matteo	Matteo	k6eAd1
Berrettini	Berrettin	k2eAgMnPc5d1
</s>
<s>
▬	▬	k?
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gFnSc1
</s>
<s>
▬	▬	k?
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
</s>
<s>
▬	▬	k?
Juan	Juan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
</s>
<s>
▬	▬	k?
Nikola	Nikola	k1gMnSc1
Mektić	Mektić	k1gMnSc1
</s>
<s>
▲	▲	k?
Nicolas	Nicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
</s>
<s>
▼	▼	k?
Bruno	Bruno	k1gMnSc1
Soares	Soares	k1gMnSc1
</s>
<s>
▬	▬	k?
Horacio	Horacio	k1gMnSc1
Zeballos	Zeballos	k1gMnSc1
</s>
<s>
▬	▬	k?
Filip	Filip	k1gMnSc1
Polášek	Polášek	k1gMnSc1
</s>
<s>
▬	▬	k?
Ivan	Ivan	k1gMnSc1
Dodig	Dodig	k1gMnSc1
</s>
<s>
▬	▬	k?
Marcel	Marcel	k1gMnSc1
Granollers	Granollers	k1gInSc1
</s>
<s>
▬	▬	k?
Ashleigh	Ashleigh	k1gInSc1
Bartyová	Bartyová	k1gFnSc1
</s>
<s>
▬	▬	k?
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaový	k2eAgFnSc5d1
</s>
<s>
▬	▬	k?
Simona	Simona	k1gFnSc1
Halepová	Halepová	k1gFnSc1
</s>
<s>
▬	▬	k?
Sofia	Sofia	k1gFnSc1
Keninová	Keninová	k1gFnSc1
</s>
<s>
▬	▬	k?
Elina	Elina	k1gFnSc1
Svitolinová	Svitolinový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Bianca	Bianc	k2eAgFnSc1d1
Andreescuová	Andreescuová	k1gFnSc1
</s>
<s>
▬	▬	k?
Aryna	Aryna	k1gFnSc1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
</s>
<s>
▬	▬	k?
Karolína	Karolína	k1gFnSc1
Plíšková	plíškový	k2eAgFnSc1d1
</s>
<s>
▲	▲	k?
Kiki	Kiki	k1gNnSc1
Bertensová	Bertensová	k1gFnSc1
</s>
<s>
▬	▬	k?
Sie	Sie	k1gFnSc1
Su-wej	Su-wej	k1gInSc1
</s>
<s>
▬	▬	k?
Barbora	Barbora	k1gFnSc1
Strýcová	Strýcová	k1gFnSc1
</s>
<s>
▲	▲	k?
Elise	elise	k1gFnSc1
Mertensová	Mertensová	k1gFnSc1
</s>
<s>
▬	▬	k?
Aryna	Aryna	k1gFnSc1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
</s>
<s>
▼	▼	k?
Tímea	Tíme	k2eAgFnSc1d1
Babosová	Babosová	k1gFnSc1
</s>
<s>
▬	▬	k?
Kristina	Kristina	k1gFnSc1
Mladenovicová	Mladenovicový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
</s>
<s>
▬	▬	k?
Kateřina	Kateřina	k1gFnSc1
Siniaková	Siniakový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Sü	Sü	k1gMnSc1
I-fan	I-fan	k1gMnSc1
</s>
<s>
▬	▬	k?
Nicole	Nicole	k1gFnSc1
Melicharová	Melicharová	k1gFnSc1
</s>
<s>
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
–	–	k?
velké	velká	k1gFnPc4
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Opena	k1gFnPc2
–	–	k?
Vítězové	vítěz	k1gMnPc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
Arthur	Arthura	k1gFnPc2
Ashe	Ashe	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1971	#num#	k4
–	–	k?
1972	#num#	k4
<g/>
)	)	kIx)
Ken	Ken	k1gMnSc1
Rosewall	Rosewall	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Edmondson	Edmondson	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
leden	leden	k1gInSc1
<g/>
)	)	kIx)
Roscoe	Roscoe	k1gInSc1
Tanner	Tanner	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
prosinec	prosinec	k1gInSc1
<g/>
)	)	kIx)
Vitas	Vitas	k1gInSc1
Gerulaitis	Gerulaitis	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
Guillermo	Guillerma	k1gFnSc5
Vilas	Vilas	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
Brian	Brian	k1gMnSc1
Teacher	Teachra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
Johan	Johan	k1gMnSc1
Kriek	Kriek	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
nekonalo	konat	k5eNaImAgNnS
se	se	k3xPyFc4
•	•	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Jim	on	k3xPp3gFnPc3
Courier	Courier	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Thomas	Thomas	k1gMnSc1
Johansson	Johansson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
Marat	Marat	k2eAgInSc1d1
Safin	Safin	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
French	French	k1gMnSc1
Open	Opena	k1gFnPc2
–	–	k?
Vítězové	vítěz	k1gMnPc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
Ken	Ken	k1gMnSc1
Rosewall	Rosewall	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
Jan	Jan	k1gMnSc1
Kodeš	Kodeš	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
Andrés	Andrés	k1gInSc1
Gimeno	Gimen	k2eAgNnSc1d1
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
Ilie	Ili	k1gInSc2
Năstase	Năstasa	k1gFnSc3
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
Adriano	Adriana	k1gFnSc5
Panatta	Panatto	k1gNnPc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
Guillermo	Guillerma	k1gFnSc5
Vilas	Vilas	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
Yannick	Yannick	k1gMnSc1
Noah	Noah	k1gMnSc1
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
Michael	Michael	k1gMnSc1
Chang	Chang	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Andrés	Andrés	k1gInSc1
Gómez	Gómez	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
Jim	on	k3xPp3gFnPc3
Courier	Courier	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Sergi	Serg	k1gFnSc3
Bruguera	Bruguero	k1gNnSc2
•	•	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
Thomas	Thomas	k1gMnSc1
Muster	Muster	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Gustavo	Gustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Gustavo	Gustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Albert	Albert	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Ferrero	Ferrero	k1gNnSc1
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Gastón	Gastón	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Gaudio	Gaudio	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
–	–	k?
Vítězové	vítěz	k1gMnPc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Smith	Smith	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
Jan	Jan	k1gMnSc1
Kodeš	Kodeš	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1975	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Arthur	Arthura	k1gFnPc2
Ashe	Ashe	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Pat	pat	k1gInSc1
Cash	cash	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Richard	Richard	k1gMnSc1
Krajicek	Krajicka	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Goran	Goran	k1gInSc1
Ivanišević	Ivanišević	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc4
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2012	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
–	–	k?
Vítězové	vítěz	k1gMnPc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
Arthur	Arthura	k1gFnPc2
Ashe	Ashe	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
Ken	Ken	k1gMnSc1
Rosewall	Rosewall	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Smith	Smith	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
Ilie	Ili	k1gInSc2
Năstase	Năstasa	k1gFnSc3
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
Manuel	Manuel	k1gMnSc1
Orantes	Orantes	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
Guillermo	Guillerma	k1gFnSc5
Vilas	Vilas	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1990	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Marat	Marat	k2eAgInSc1d1
Safin	Safin	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Roddick	Roddick	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Juan	Juan	k1gMnSc1
Martín	Martín	k1gMnSc1
del	del	k?
Potro	Potro	k1gNnSc4
•	•	k?
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
</s>
<s>
2013	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Marin	Marina	k1gFnPc2
Čilić	Čilić	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc1
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
tenise	tenis	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Pius	Pius	k1gMnSc1
Boland	Bolanda	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
Lawrence	Lawrence	k1gFnSc2
Doherty	Dohert	k1gInPc4
•	•	k?
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
Beals	Beals	k1gInSc1
Wright	Wright	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
Josiah	Josiah	k1gInSc1
Ritchie	Ritchie	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1908	#num#	k4
–	–	k?
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
Arthur	Arthur	k1gMnSc1
Gore	Gor	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
Charles	Charles	k1gMnSc1
Winslow	Winslow	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1912	#num#	k4
–	–	k?
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
André	André	k1gMnSc1
Gobert	Gobert	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
Louis	Louis	k1gMnSc1
Raymond	Raymond	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
Vincent	Vincent	k1gMnSc1
Richards	Richardsa	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Miloslav	Miloslav	k1gMnSc1
Mečíř	mečíř	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
Marc	Marc	k1gFnSc1
Rosset	Rosset	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Nicolás	Nicolás	k1gInSc1
Massú	Massú	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
tenise	tenis	k1gInSc6
–	–	k?
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
1896	#num#	k4
–	–	k?
20002004	#num#	k4
–	–	k?
2016	#num#	k4
</s>
<s>
1896	#num#	k4
<g/>
:	:	kIx,
ZZX	ZZX	kA
John	John	k1gMnSc1
Pius	Pius	k1gMnSc1
Boland	Bolanda	k1gFnPc2
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Traun	Traun	k1gMnSc1
</s>
<s>
1900	#num#	k4
<g/>
:	:	kIx,
GBR	GBR	kA
Lawrence	Lawrence	k1gFnSc1
Doherty	Dohert	k1gInPc1
•	•	k?
Reginald	Reginald	k1gInSc1
Doherty	Dohert	k1gInPc5
</s>
<s>
1904	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
Edgar	Edgar	k1gMnSc1
Leonard	Leonard	k1gMnSc1
•	•	k?
Beals	Beals	k1gInSc1
Wright	Wright	k1gMnSc1
</s>
<s>
1908	#num#	k4
<g/>
:	:	kIx,
GBR	GBR	kA
Reginald	Reginald	k1gInSc1
Doherty	Dohert	k1gInPc1
•	•	k?
George	Georg	k1gInSc2
Hillyard	Hillyarda	k1gFnPc2
hala	halo	k1gNnSc2
GBR	GBR	kA
Herbert	Herbert	k1gMnSc1
Barrett	Barrett	k1gMnSc1
•	•	k?
Arthur	Arthur	k1gMnSc1
Gore	Gor	k1gFnSc2
</s>
<s>
1912	#num#	k4
<g/>
:	:	kIx,
RSA	RSA	kA
Harold	Harold	k1gMnSc1
Kitson	Kitson	k1gMnSc1
•	•	k?
Charles	Charles	k1gMnSc1
Winslow	Winslow	k1gMnSc1
hala	hala	k1gFnSc1
FRA	FRA	kA
Maurice	Maurika	k1gFnSc3
Germot	Germot	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Gobert	Gobert	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
:	:	kIx,
GBR	GBR	kA
Oswald	Oswald	k1gMnSc1
Turnbull	Turnbull	k1gMnSc1
•	•	k?
Max	max	kA
Woosnam	Woosnam	k1gInSc1
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
Francis	Francis	k1gFnSc1
Hunter	Hunter	k1gMnSc1
•	•	k?
Vincent	Vincent	k1gMnSc1
Richards	Richards	k1gInSc4
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
Ken	Ken	k1gMnSc1
Flach	Flach	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
GER	Gera	k1gFnPc2
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
AUS	AUS	kA
Todd	Todd	k1gMnSc1
Woodbridge	Woodbridg	k1gFnSc2
•	•	k?
Mark	Mark	k1gMnSc1
Woodforde	Woodford	k1gMnSc5
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
CAN	CAN	kA
Sébastien	Sébastien	k1gInSc4
Lareau	Lareaus	k1gInSc2
•	•	k?
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
CHI	chi	k0
Fernando	Fernanda	k1gFnSc5
González	González	k1gMnSc1
•	•	k?
Nicolás	Nicolás	k1gInSc1
Massú	Massú	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
SUI	SUI	kA
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
•	•	k?
Mike	Mike	k1gInSc1
Bryan	Bryan	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
ESP	ESP	kA
Marc	Marc	k1gFnSc1
López	López	k1gMnSc1
•	•	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
ATP	atp	kA
–	–	k?
Tenisté	tenista	k1gMnPc1
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
světového	světový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
</s>
<s>
Ilie	Ilie	k6eAd1
Năstase	Năstasa	k1gFnSc3
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
–	–	k?
40	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
(	(	kIx(
<g/>
1974	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jimmy	Jimm	k1gInPc1
Connors	Connors	k1gInSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
–	–	k?
268	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
109	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
McEnroe	McEnro	k1gMnSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
–	–	k?
170	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
–	–	k?
270	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mats	Mats	k1gInSc1
Wilander	Wilander	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
–	–	k?
20	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
•	•	k?
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
72	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
–	–	k?
12	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jim	on	k3xPp3gMnPc3
Courier	Courier	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
–	–	k?
58	#num#	k4
t.	t.	k?
•	•	k?
Pete	Pete	k1gInSc1
Sampras	Sampras	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2000	#num#	k4
–	–	k?
286	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
–	–	k?
101	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Thomas	Thomas	k1gMnSc1
Muster	Muster	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
–	–	k?
6	#num#	k4
<g/>
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Marcelo	Marcela	k1gFnSc5
Ríos	Ríosa	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
2	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
1	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Marat	Marat	k1gInSc1
Safin	Safin	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
–	–	k?
9	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Gustavo	Gustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2001	#num#	k4
–	–	k?
43	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
–	–	k?
80	#num#	k4
t	t	k?
•	•	k?
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Ferrero	Ferrero	k1gNnSc4
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andy	Anda	k1gFnSc2
Roddick	Roddick	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
–	–	k?
13	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
310	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
–	–	k?
209	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
–	–	k?
318	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
–	–	k?
41	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
•	•	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
prvně	prvně	k?
<g/>
/	/	kIx~
<g/>
naposledy	naposledy	k6eAd1
umístěn	umístit	k5eAaPmNgInS
–	–	k?
počet	počet	k1gInSc1
týdnů	týden	k1gInPc2
(	(	kIx(
<g/>
t.	t.	k?
<g/>
))	))	k?
•	•	k?
současná	současný	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
tučně	tučně	k6eAd1
<g/>
,	,	kIx,
stav	stav	k1gInSc1
k	k	k7c3
26	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc3
2021	#num#	k4
</s>
<s>
Sportovec	sportovec	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
Laureus	Laureus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
:	:	kIx,
Tiger	Tiger	k1gInSc1
Woods	Woods	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Lance	lance	k1gNnSc2
Armstrong	Armstrong	k1gMnSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc1
•	•	k?
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettela	k1gFnPc2
•	•	k?
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2020	#num#	k4
<g/>
:	:	kIx,
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
/	/	kIx~
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
sportovec	sportovec	k1gMnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
1958	#num#	k4
Zdzisław	Zdzisław	k1gMnSc1
Krzyszkowiak	Krzyszkowiak	k1gMnSc1
•	•	k?
1959	#num#	k4
Vasilij	Vasilij	k1gFnPc2
Kuzněcov	Kuzněcov	k1gInSc4
•	•	k?
1960	#num#	k4
Jurij	Jurij	k1gFnPc2
Vlasov	Vlasov	k1gInSc4
•	•	k?
1961	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1962	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1963	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1964	#num#	k4
Lidija	Lidij	k2eAgFnSc1d1
Skoblikovová	Skoblikovová	k1gFnSc1
•	•	k?
1965	#num#	k4
Michel	Michel	k1gInSc1
Jazy	Jaza	k1gFnSc2
•	•	k?
1966	#num#	k4
Irena	Irena	k1gFnSc1
Szewińská	Szewińská	k1gFnSc1
•	•	k?
1967	#num#	k4
Jean-Claude	Jean-Claud	k1gInSc5
Killy	Kill	k1gInPc7
•	•	k?
1968	#num#	k4
Jean-Claude	Jean-Claud	k1gInSc5
Killy	Kill	k1gInPc7
•	•	k?
1969	#num#	k4
Eddy	Edda	k1gFnSc2
Merckx	Merckx	k1gInSc1
•	•	k?
1970	#num#	k4
Eddy	Edda	k1gFnSc2
Merckx	Merckx	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1971	#num#	k4
Juha	Juha	k1gFnSc1
Väätäinen	Väätäinna	k1gFnPc2
•	•	k?
1972	#num#	k4
Lasse	Lasse	k1gFnSc2
Virén	Virén	k1gInSc1
•	•	k?
1973	#num#	k4
Kornelia	Kornelia	k1gFnSc1
Enderová	Enderová	k1gFnSc1
•	•	k?
1974	#num#	k4
Irena	Irena	k1gFnSc1
Szewińská	Szewińská	k1gFnSc1
•	•	k?
1975	#num#	k4
Kornelia	Kornelia	k1gFnSc1
Enderová	Enderová	k1gFnSc1
•	•	k?
1976	#num#	k4
Nadia	Nadius	k1gMnSc2
Comaneciová	Comaneciový	k2eAgFnSc1d1
•	•	k?
1977	#num#	k4
Rosemarie	Rosemarie	k1gFnSc1
Ackermannová	Ackermannová	k1gFnSc1
•	•	k?
1978	#num#	k4
Vladimir	Vladimir	k1gInSc1
Jaščenko	Jaščenka	k1gFnSc5
•	•	k?
1979	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
•	•	k?
1980	#num#	k4
Vladimir	Vladimir	k1gInSc1
Salnikov	Salnikov	k1gInSc4
•	•	k?
1981	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
•	•	k?
1982	#num#	k4
Daley	Dalea	k1gFnSc2
Thompson	Thompsona	k1gFnPc2
•	•	k?
1983	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jarmila	Jarmila	k1gFnSc1
Kratochvílová	Kratochvílová	k1gFnSc1
•	•	k?
1984	#num#	k4
Michael	Michael	k1gMnSc1
Gross	Gross	k1gMnSc1
•	•	k?
1985	#num#	k4
Sergej	Sergej	k1gMnSc1
Bubka	Bubek	k1gInSc2
•	•	k?
1986	#num#	k4
Heike	Heike	k1gFnSc1
Drechslerová	Drechslerová	k1gFnSc1
•	•	k?
1987	#num#	k4
Stephen	Stephen	k2eAgInSc4d1
Roche	Roche	k1gInSc4
•	•	k?
1988	#num#	k4
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
1989	#num#	k4
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
1990	#num#	k4
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
1991	#num#	k4
Katrin	Katrin	k1gInSc1
Krabbeová	Krabbeová	k1gFnSc1
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Linford	Linford	k1gInSc1
Christie	Christie	k1gFnSc2
•	•	k?
1994	#num#	k4
Johann	Johanno	k1gNnPc2
Olav	Olava	k1gFnPc2
Koss	Kossa	k1gFnPc2
•	•	k?
1995	#num#	k4
Jonathan	Jonathan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Edwards	Edwards	k1gInSc1
•	•	k?
1996	#num#	k4
Světlana	Světlana	k1gFnSc1
Mastěrkovová	Mastěrkovová	k1gFnSc1
•	•	k?
1997	#num#	k4
Martina	Martin	k1gMnSc2
Hingisová	Hingisový	k2eAgFnSc1d1
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Gabriela	Gabriela	k1gFnSc1
Szabóová	Szabóová	k1gFnSc1
•	•	k?
2000	#num#	k4
Inge	Inge	k1gFnSc2
de	de	k?
Bruijnová	Bruijnová	k1gFnSc1
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
2005	#num#	k4
Roger	Rogero	k1gNnPc2
Federer	Federra	k1gFnPc2
a	a	k8xC
Jelena	Jelena	k1gFnSc1
Isinbajevová	Isinbajevová	k1gFnSc1
•	•	k?
2006	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
2007	#num#	k4
Roger	Rogra	k1gFnPc2
Federer	Federra	k1gFnPc2
•	•	k?
2008	#num#	k4
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
2009	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
2010	#num#	k4
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
2011	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2016	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Robert	Robert	k1gMnSc1
Lewandowski	Lewandowske	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
148884	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
139076395	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8163	#num#	k4
1971	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2005075233	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
89826459	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2005075233	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tenis	tenis	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
