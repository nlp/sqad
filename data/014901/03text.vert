<s>
BusyBox	BusyBox	k1gInSc1
</s>
<s>
BusyBox	BusyBox	k1gInSc1
</s>
<s>
Ukázka	ukázka	k1gFnSc1
příkazové	příkazový	k2eAgFnSc2d1
řádky	řádka	k1gFnSc2
BusyBoxuVývojář	BusyBoxuVývojář	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
Andersen	Andersen	k1gMnSc1
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
Landley	Landlea	k1gFnSc2
<g/>
,	,	kIx,
Denys	Denys	k1gInSc4
Vlasenko	Vlasenka	k1gFnSc5
a	a	k8xC
další	další	k2eAgFnPc1d1
Aktuální	aktuální	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
1.32	1.32	k4
<g/>
.1	.1	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Unix-like	Unix-like	k6eAd1
Vyvíjeno	vyvíjet	k5eAaImNgNnS
v	v	k7c6
</s>
<s>
C	C	kA
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
Linux	linux	k1gInSc1
pro	pro	k7c4
embedded	embedded	k1gInSc4
systémy	systém	k1gInPc4
Licence	licence	k1gFnPc1
</s>
<s>
GNU	gnu	k1gMnSc1
GPLv	GPLv	k1gMnSc1
<g/>
2	#num#	k4
(	(	kIx(
<g/>
verze	verze	k1gFnSc1
1.2	1.2	k4
<g/>
.3	.3	k4
a	a	k8xC
novější	nový	k2eAgFnSc1d2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.busybox.net	www.busybox.net	k5eAaPmF,k5eAaImF,k5eAaBmF
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
BusyBox	BusyBox	k1gInSc1
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
program	program	k1gInSc1
pod	pod	k7c7
licencí	licence	k1gFnSc7
GNU	gnu	k1gMnSc1
GPLv	GPLv	k1gMnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
UN	UN	kA
<g/>
*	*	kIx~
<g/>
Xové	Xový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
příkazový	příkazový	k2eAgInSc4d1
procesor	procesor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
sobě	se	k3xPyFc3
vestavěné	vestavěný	k2eAgFnPc1d1
implementace	implementace	k1gFnPc1
mnoha	mnoho	k4c2
standardních	standardní	k2eAgMnPc2d1
unixových	unixový	k2eAgMnPc2d1
příkazů	příkaz	k1gInPc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
GNU	gnu	k1gMnSc1
Core	Core	k1gFnPc2
Utilities	Utilities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
návrhu	návrh	k1gInSc2
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
všestranný	všestranný	k2eAgInSc4d1
malý	malý	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
linuxovým	linuxový	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
bez	bez	k7c2
potřeby	potřeba	k1gFnSc2
dalších	další	k2eAgInPc2d1
programů	program	k1gInPc2
bude	být	k5eAaImBp3nS
blížit	blížit	k5eAaImF
použitelnosti	použitelnost	k1gFnPc4
běžného	běžný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
malým	malý	k2eAgInPc3d1
nárokům	nárok	k1gInPc3
bude	být	k5eAaImBp3nS
dobře	dobře	k6eAd1
použitelný	použitelný	k2eAgInSc1d1
ve	v	k7c6
vestavěných	vestavěný	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Původní	původní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
BusyBoxu	BusyBox	k1gInSc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
Bruce	Bruka	k1gFnSc3
Perens	Perens	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
chtěl	chtít	k5eAaImAgMnS
vyrobit	vyrobit	k5eAaPmF
kompletní	kompletní	k2eAgInSc4d1
bootovatelný	bootovatelný	k2eAgInSc4d1
systém	systém	k1gInSc4
na	na	k7c4
jedinou	jediný	k2eAgFnSc4d1
disketu	disketa	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
sloužil	sloužit	k5eAaImAgInS
zároveň	zároveň	k6eAd1
jako	jako	k9
záchranná	záchranný	k2eAgFnSc1d1
disketa	disketa	k1gFnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
jako	jako	k9
instalační	instalační	k2eAgInSc4d1
program	program	k1gInSc4
systému	systém	k1gInSc2
Debian	Debiana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
z	z	k7c2
BusyBoxu	BusyBox	k1gInSc2
stal	stát	k5eAaPmAgInS
fakticky	fakticky	k6eAd1
standardní	standardní	k2eAgInSc1d1
shell	shell	k1gInSc1
pro	pro	k7c4
instalátory	instalátor	k1gMnPc7
distribucí	distribuce	k1gFnSc7
Linuxu	linux	k1gInSc2
i	i	k9
pro	pro	k7c4
Linux	linux	k1gInSc4
ve	v	k7c6
vestavěných	vestavěný	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
každý	každý	k3xTgInSc1
samostatný	samostatný	k2eAgInSc1d1
spustitelný	spustitelný	k2eAgInSc1d1
binární	binární	k2eAgInSc1d1
soubor	soubor	k1gInSc1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
ve	v	k7c6
formátu	formát	k1gInSc6
ELF	elf	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
Linux	Linux	kA
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc4
kilobajtů	kilobajt	k1gInPc2
nadbytečných	nadbytečný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
BusyBoxu	BusyBox	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
poskytuje	poskytovat	k5eAaImIp3nS
základní	základní	k2eAgFnSc4d1
funkčnost	funkčnost	k1gFnSc4
více	hodně	k6eAd2
než	než	k8xS
dvou	dva	k4xCgFnPc2
stovek	stovka	k1gFnPc2
drobných	drobný	k2eAgFnPc2d1
utilit	utilita	k1gFnPc2
<g/>
,	,	kIx,
využívanou	využívaný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
jak	jak	k8xS,k8xC
ušetřit	ušetřit	k5eAaPmF
znatelně	znatelně	k6eAd1
místa	místo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
BusyBox	BusyBox	k1gInSc1
implementuje	implementovat	k5eAaImIp3nS
funkčnost	funkčnost	k1gFnSc4
většiny	většina	k1gFnSc2
programů	program	k1gInPc2
požadovaných	požadovaný	k2eAgInPc2d1
Single	singl	k1gInSc5
UNIX	UNIX	kA
Specification	Specification	k1gInSc4
a	a	k8xC
ještě	ještě	k9
řady	řada	k1gFnSc2
programů	program	k1gInPc2
navíc	navíc	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastním	vlastní	k2eAgNnSc7d1
shellem	shello	k1gNnSc7
je	být	k5eAaImIp3nS
u	u	k7c2
BusyBoxu	BusyBox	k1gInSc2
ash	ash	k?
shell	shell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
BusyBox	BusyBox	k1gInSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
souborem	soubor	k1gInSc7
<g/>
,	,	kIx,
volání	volání	k1gNnSc4
jeho	jeho	k3xOp3gFnPc2
různých	různý	k2eAgFnPc2d1
podob	podoba	k1gFnPc2
se	se	k3xPyFc4
ovšem	ovšem	k9
uskutečňuje	uskutečňovat	k5eAaImIp3nS
transparentně	transparentně	k6eAd1
použitím	použití	k1gNnSc7
symbolických	symbolický	k2eAgInPc2d1
nebo	nebo	k8xC
pevných	pevný	k2eAgInPc2d1
odkazů	odkaz	k1gInPc2
vedoucích	vedoucí	k1gMnPc2
ze	z	k7c2
jmen	jméno	k1gNnPc2
implementovaných	implementovaný	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Příkazy	příkaz	k1gInPc1
</s>
<s>
ash	ash	k?
</s>
<s>
awk	awk	k?
</s>
<s>
cat	cat	k?
</s>
<s>
chmod	chmod	k1gInSc1
</s>
<s>
cp	cp	k?
</s>
<s>
date	date	k6eAd1
</s>
<s>
dd	dd	k?
</s>
<s>
df	df	k?
</s>
<s>
dmesg	dmesg	k1gMnSc1
</s>
<s>
echo	echo	k1gNnSc1
</s>
<s>
egrep	egrep	k1gMnSc1
</s>
<s>
fgrep	fgrep	k1gMnSc1
</s>
<s>
getty	getta	k1gFnPc1
</s>
<s>
grep	grep	k1gInSc1
</s>
<s>
gunzip	gunzip	k1gMnSc1
</s>
<s>
gzip	gzip	k1gMnSc1
</s>
<s>
init	init	k1gMnSc1
</s>
<s>
kill	kill	k1gMnSc1
</s>
<s>
ln	ln	k?
</s>
<s>
login	login	k1gMnSc1
</s>
<s>
ls	ls	k?
</s>
<s>
mdev	mdev	k1gMnSc1
</s>
<s>
mkdir	mkdir	k1gInSc1
—	—	k?
Vytvoří	vytvořit	k5eAaPmIp3nS
složku	složka	k1gFnSc4
</s>
<s>
more	mor	k1gInSc5
</s>
<s>
mount	mount	k1gInSc1
—	—	k?
Připojí	připojit	k5eAaPmIp3nS
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
mv	mv	k?
</s>
<s>
nc	nc	k?
</s>
<s>
netstat	netstat	k1gInSc1
</s>
<s>
ntpc	ntpc	k6eAd1
</s>
<s>
ntpsync	ntpsync	k6eAd1
</s>
<s>
nvram	nvram	k6eAd1
</s>
<s>
pidof	pidof	k1gMnSc1
</s>
<s>
ping	pingo	k1gNnPc2
</s>
<s>
ps	ps	k0
</s>
<s>
pwd	pwd	k?
</s>
<s>
rm	rm	k?
</s>
<s>
rmdir	rmdir	k1gMnSc1
</s>
<s>
rstats	rstats	k6eAd1
</s>
<s>
sed	sed	k1gInSc1
</s>
<s>
sh	sh	k?
</s>
<s>
sleep	sleep	k1gMnSc1
</s>
<s>
sync	sync	k6eAd1
</s>
<s>
tar	tar	k?
</s>
<s>
touch	touch	k1gMnSc1
</s>
<s>
udhcpc	udhcpc	k6eAd1
</s>
<s>
umount	umount	k1gInSc1
—	—	k?
Odpojí	odpojit	k5eAaPmIp3nS
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
uname	unamat	k5eAaPmIp3nS
</s>
<s>
usleep	usleep	k1gMnSc1
</s>
<s>
vi	vi	k?
—	—	k?
Textový	textový	k2eAgInSc1d1
editor	editor	k1gInSc1
</s>
<s>
watch	watch	k1gMnSc1
</s>
<s>
zcat	zcat	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.busybox.net/license.html	http://www.busybox.net/license.html	k1gInSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
BusyBox	BusyBox	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
