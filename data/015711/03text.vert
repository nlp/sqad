<s>
Stříbro	stříbro	k1gNnSc1
(	(	kIx(
<g/>
minerál	minerál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1
stříbro	stříbro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Minerál	minerál	k1gInSc1
</s>
<s>
Chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Ag	Ag	k?
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
stříbrobílá	stříbrobílý	k2eAgFnSc1d1
</s>
<s>
Vzhled	vzhled	k1gInSc1
krystalu	krystal	k1gInSc2
</s>
<s>
drátkovité	drátkovitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
plíškovité	plíškovitý	k2eAgFnPc1d1
</s>
<s>
Soustava	soustava	k1gFnSc1
</s>
<s>
krychlová	krychlový	k2eAgFnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
2,5	2,5	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Lesk	lesk	k1gInSc1
</s>
<s>
kovový	kovový	k2eAgInSc1d1
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1
</s>
<s>
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
??	??	k?
</s>
<s>
Vryp	vryp	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
9,6	9,6	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
g	g	kA
⋅	⋅	k?
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
</s>
<s>
v	v	k7c6
HNO3	HNO3	k1gFnSc6
a	a	k8xC
HCl	HCl	k1gFnSc6
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
kujný	kujný	k2eAgMnSc1d1
<g/>
,	,	kIx,
tepelně	tepelně	k6eAd1
vodivý	vodivý	k2eAgInSc1d1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
chemický	chemický	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
Ag	Ag	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
krychlový	krychlový	k2eAgInSc4d1
minerál	minerál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Krystalizuje	krystalizovat	k5eAaImIp3nS
z	z	k7c2
horkých	horký	k2eAgInPc2d1
roztoků	roztok	k1gInPc2
<g/>
,	,	kIx,
hromadí	hromadit	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
cementačních	cementační	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
nebo	nebo	k8xC
v	v	k7c6
usazeninách	usazenina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
pseudomorfózy	pseudomorfóza	k1gFnPc4
po	po	k7c6
jiných	jiný	k2eAgInPc6d1
minerálech	minerál	k1gInPc6
obsahujících	obsahující	k2eAgFnPc2d1
stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Morfologie	morfologie	k1gFnSc1
</s>
<s>
Dokonalé	dokonalý	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
jsou	být	k5eAaImIp3nP
vzácné	vzácný	k2eAgInPc1d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
to	ten	k3xDgNnSc4
jsou	být	k5eAaImIp3nP
tvary	tvar	k1gInPc1
krychle	krychle	k1gFnSc2
<g/>
,	,	kIx,
dvanáctistěnu	dvanáctistěn	k2eAgFnSc4d1
a	a	k8xC
osmistěnu	osmistěn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krystaly	krystal	k1gInPc7
jsou	být	k5eAaImIp3nP
obyčejně	obyčejně	k6eAd1
jednostranně	jednostranně	k6eAd1
nebo	nebo	k8xC
kostrovitě	kostrovitě	k6eAd1
vyvinuté	vyvinutý	k2eAgInPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
vznikají	vznikat	k5eAaImIp3nP
tvary	tvar	k1gInPc1
o	o	k7c4
zdánlivě	zdánlivě	k6eAd1
nižší	nízký	k2eAgFnPc4d2
souměrnosti	souměrnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
kostrovitý	kostrovitý	k2eAgInSc1d1
a	a	k8xC
drátkovitý	drátkovitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
mědi	měď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drátkovité	drátkovitý	k2eAgInPc1d1
agregáty	agregát	k1gInPc1
bývají	bývat	k5eAaImIp3nP
zkroucené	zkroucený	k2eAgFnPc1d1
nebo	nebo	k8xC
jakoby	jakoby	k8xS
sestavené	sestavený	k2eAgFnPc4d1
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
rovnoběžných	rovnoběžný	k2eAgInPc2d1
drátků	drátek	k1gInPc2
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
plechy	plech	k1gInPc1
<g/>
,	,	kIx,
plíšky	plíšek	k1gInPc1
<g/>
,	,	kIx,
tenké	tenký	k2eAgInPc1d1
kůry	kůr	k1gInPc1
a	a	k8xC
povlaky	povlak	k1gInPc1
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
častější	častý	k2eAgInPc1d2
jsou	být	k5eAaImIp3nP
kusy	kus	k1gInPc1
zcela	zcela	k6eAd1
nepravidelné	pravidelný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Tvrdost	tvrdost	k1gFnSc1
2,5	2,5	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
hustota	hustota	k1gFnSc1
kolísá	kolísat	k5eAaImIp3nS
podle	podle	k7c2
příměsí	příměs	k1gFnPc2
od	od	k7c2
9,6	9,6	k4
do	do	k7c2
12	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěpnost	štěpnost	k1gFnSc1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
lom	lom	k1gInSc1
hákovitý	hákovitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kujné	kujný	k2eAgInPc1d1
<g/>
,	,	kIx,
tažné	tažný	k2eAgInPc1d1
<g/>
,	,	kIx,
ohebné	ohebný	k2eAgInPc1d1
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
vede	vést	k5eAaImIp3nS
teplo	teplo	k1gNnSc4
a	a	k8xC
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Barva	barva	k1gFnSc1
<g/>
:	:	kIx,
stříbrobílá	stříbrobílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
časem	časem	k6eAd1
matní	matnět	k5eAaImIp3nS
a	a	k8xC
černá	černý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průhlednost	průhlednost	k1gFnSc1
<g/>
:	:	kIx,
opakní	opaknit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovový	kovový	k2eAgInSc4d1
lesk	lesk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Složení	složení	k1gNnSc1
<g/>
:	:	kIx,
Ag	Ag	k1gFnSc1
téměř	téměř	k6eAd1
100	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
příměsi	příměs	k1gFnPc4
Au	au	k0
<g/>
,	,	kIx,
Bi	Bi	k1gMnSc1
<g/>
,	,	kIx,
Cu	Cu	k1gMnSc1
<g/>
,	,	kIx,
Sb	sb	kA
<g/>
,	,	kIx,
Hg	Hg	k1gFnSc1
<g/>
,	,	kIx,
Pt	Pt	k1gFnSc1
<g/>
,	,	kIx,
As	as	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
dmuchavkou	dmuchavka	k1gFnSc7
taje	taj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpouští	rozpouštět	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
HNO	HNO	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
H2S	H2S	k1gFnSc7
černá	černat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Získávání	získávání	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
ceny	cena	k1gFnSc2
stříbra	stříbro	k1gNnSc2
na	na	k7c6
komoditních	komoditní	k2eAgFnPc6d1
burzách	burza	k1gFnPc6
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stříbro	stříbro	k1gNnSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
hornickou	hornický	k2eAgFnSc7d1
těžbou	těžba	k1gFnSc7
nebo	nebo	k8xC
rýžováním	rýžování	k1gNnSc7
z	z	k7c2
naplavenin	naplavenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
nalezeny	nalezen	k2eAgInPc1d1
kompaktní	kompaktní	k2eAgInPc1d1
kusy	kus	k1gInPc1
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k8xC
značných	značný	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
i	i	k9
několika	několik	k4yIc2
set	sto	k4xCgNnPc2
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Široké	Široké	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
ve	v	k7c6
šperkařství	šperkařství	k1gNnSc6
<g/>
,	,	kIx,
klenotnictví	klenotnictví	k1gNnSc6
<g/>
,	,	kIx,
mincovnictví	mincovnictví	k1gNnSc6
<g/>
,	,	kIx,
elektroprůmyslu	elektroprůmysl	k1gInSc6
<g/>
,	,	kIx,
lékařství	lékařství	k1gNnSc6
a	a	k8xC
při	při	k7c6
výrobě	výroba	k1gFnSc6
fotografických	fotografický	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
při	při	k7c6
úpravě	úprava	k1gFnSc6
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
výrobě	výroba	k1gFnSc6
zrcadel	zrcadlo	k1gNnPc2
či	či	k8xC
při	při	k7c6
přípravě	příprava	k1gFnSc6
speciálních	speciální	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Stříbro	stříbro	k1gNnSc1
je	být	k5eAaImIp3nS
komoditní	komoditní	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
stejně	stejně	k6eAd1
jako	jako	k9
např.	např.	kA
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
měď	měď	k1gFnSc1
nebo	nebo	k8xC
ropa	ropa	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
obchoduje	obchodovat	k5eAaImIp3nS
na	na	k7c6
komoditních	komoditní	k2eAgFnPc6d1
burzách	burza	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Naleziště	naleziště	k1gNnSc1
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Jáchymov	Jáchymov	k1gInSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
-	-	kIx~
nálezy	nález	k1gInPc1
drátů	drát	k1gInPc2
dlouhých	dlouhý	k2eAgInPc2d1
až	až	k6eAd1
30	#num#	k4
<g/>
cm	cm	kA
</s>
<s>
Příbram	Příbram	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Přerov	Přerov	k1gInSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Norsko	Norsko	k1gNnSc1
-	-	kIx~
Kongsberg	Kongsberg	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
-	-	kIx~
nalezen	nalezen	k2eAgInSc1d1
kus	kus	k1gInSc1
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
612	#num#	k4
kg	kg	kA
</s>
<s>
USA	USA	kA
-	-	kIx~
V	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Arizoně	Arizona	k1gFnSc6
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
kus	kus	k1gInSc1
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
1350	#num#	k4
kg	kg	kA
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
</s>
<s>
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
…	…	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Komodity	komodita	k1gFnPc1
-	-	kIx~
Stříbro	stříbro	k1gNnSc1
-	-	kIx~
Vývoj	vývoj	k1gInSc1
ceny	cena	k1gFnSc2
stříbra	stříbro	k1gNnSc2
na	na	k7c6
komoditních	komoditní	k2eAgFnPc6d1
burzách	burza	k1gFnPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
stříbro	stříbro	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stříbro	stříbro	k1gNnSc1
na	na	k7c6
webu	web	k1gInSc6
mindat	mindat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Stříbrné	stříbrný	k2eAgInPc1d1
doly	dol	k1gInPc1
v	v	k7c6
ČR	ČR	kA
</s>
