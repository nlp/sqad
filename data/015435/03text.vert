<s>
Děkanát	děkanát	k1gInSc1
Třebíč	Třebíč	k1gFnSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
TřebíčDiecéze	TřebíčDiecéza	k1gFnSc6
</s>
<s>
brněnská	brněnský	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Jiří	Jiří	k1gMnSc1
Dobeš	Dobeš	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
v	v	k7c6
Třebíči-městě	Třebíči-města	k1gFnSc6
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2017	#num#	k4
(	(	kIx(
<g/>
IX	IX	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martin	k1gMnSc2
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Třebíč-město	Třebíč-města	k1gMnSc5
</s>
<s>
Děkanát	děkanát	k1gInSc1
Třebíč	Třebíč	k1gFnSc1
nebo	nebo	k8xC
děkanství	děkanství	k1gNnSc1
třebíčské	třebíčský	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
brněnské	brněnský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zahrnuje	zahrnovat	k5eAaImIp3nS
32	#num#	k4
římskokatolických	římskokatolický	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Funkcí	funkce	k1gFnSc7
děkana	děkan	k1gMnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
pověřen	pověřit	k5eAaPmNgMnS
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Dobeš	Dobeš	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
farnosti	farnost	k1gFnSc2
Třebíč-město	Třebíč-města	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
P.	P.	kA
Ervína	Ervín	k1gMnSc4
Jansu	Jansa	k1gMnSc4
<g/>
,	,	kIx,
bývalého	bývalý	k2eAgMnSc4d1
faráře	farář	k1gMnSc4
farnosti	farnost	k1gFnSc2
Třebíč-Jejkov	Třebíč-Jejkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
FarnostSprávceFarní	FarnostSprávceFarní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Benetice	Benetika	k1gFnSc3
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Rudíkov	Rudíkov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Marka	Marek	k1gMnSc4
</s>
<s>
Březník	Březník	k1gMnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Jinošov	Jinošov	k1gInSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Čáslavice	Čáslavice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Stařeč	Stařeč	k1gInSc1
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Přibyslavice	Přibyslavice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
Dalešice	Dalešice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Valeč	Valeč	k1gInSc1
u	u	k7c2
Hrotovic	Hrotovice	k1gFnPc2
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Dukovany	Dukovany	k1gInPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Rouchovany	Rouchovan	k1gMnPc7
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Hartvíkovice	Hartvíkovice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
</s>
<s>
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc1
</s>
<s>
Heraltice	Heraltice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Přibyslavice	Přibyslavice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Vlastimil	Vlastimil	k1gMnSc1
Protivínský	protivínský	k2eAgMnSc1d1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Hrotovice	Hrotovice	k1gFnPc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Kovář	Kovář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
Chlum	chlum	k1gInSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Kamenice	Kamenice	k1gFnSc1
u	u	k7c2
Jihlavy	Jihlava	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Jinošov	Jinošov	k1gInSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Josef	Josef	k1gMnSc1
Požár	požár	k1gInSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Kněžice	kněžice	k1gFnSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
</s>
<s>
R.	R.	kA
D.	D.	kA
Václav	Václav	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
staršího	starší	k1gMnSc4
</s>
<s>
Koněšín	Koněšín	k1gMnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Bartoloměj	Bartoloměj	k1gMnSc1
</s>
<s>
Lipník	Lipník	k1gInSc1
u	u	k7c2
Hrotovic	Hrotovice	k1gFnPc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Mohelno	Mohelna	k1gFnSc5
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Rouchovany	Rouchovan	k1gMnPc4
</s>
<s>
Všech	všecek	k3xTgMnPc2
svatých	svatý	k1gMnPc2
</s>
<s>
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
</s>
<s>
R.	R.	kA
D.	D.	kA
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Holý	Holý	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Okříšky	Okříšek	k1gInPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Přibyslavice	Přibyslavice	k1gFnSc1
</s>
<s>
Jména	jméno	k1gNnPc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Opatov	Opatov	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Kněžice	kněžice	k1gFnSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Předín	Předín	k1gMnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Želetava	Želetava	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Přibyslavice	Přibyslavice	k1gFnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jacek	Jacky	k1gFnPc2
Kruczek	Kruczka	k1gFnPc2
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Rokytnice	Rokytnice	k1gFnSc1
nad	nad	k7c7
Rokytnou	Rokytný	k2eAgFnSc7d1
</s>
<s>
R.	R.	kA
D.	D.	kA
Jiří	Jiří	k1gMnSc1
Plhoň	Plhoň	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
Rouchovany	Rouchovan	k1gMnPc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Václavek	Václavek	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Rudíkov	Rudíkov	k1gInSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Milan	Milan	k1gMnSc1
Těžký	těžký	k2eAgMnSc1d1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Stařeč	Stařeč	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Stanislav	Stanislav	k1gMnSc1
Mahovský	Mahovský	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
</s>
<s>
Střížov-Číměř	Střížov-Číměř	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Rudíkov	Rudíkov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Třebíč-Jejkov	Třebíč-Jejkov	k1gInSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
<g/>
,	,	kIx,
Třebíč-zámek	Třebíč-zámek	k1gInSc1
</s>
<s>
Proměnění	proměnění	k1gNnSc1
Páně	páně	k2eAgNnSc2d1
</s>
<s>
Třebíč-město	Třebíč-město	k6eAd1
</s>
<s>
R.	R.	kA
D.	D.	kA
Jiří	Jiří	k1gMnSc1
Dobeš	Dobeš	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
z	z	k7c2
Tours	Toursa	k1gFnPc2
</s>
<s>
Třebíč-zámek	Třebíč-zámek	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Jakub	Jakub	k1gMnSc1
Holík	Holík	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc4
</s>
<s>
Valeč	Valeč	k1gInSc1
u	u	k7c2
Hrotovic	Hrotovice	k1gFnPc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
ad	ad	k7c4
interim	interim	k1gInSc4
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Seknička	seknička	k1gFnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
</s>
<s>
D.	D.	kA
PhDr.	PhDr.	kA
Jindřich	Jindřich	k1gMnSc1
Zdík	Zdík	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Charouz	Charouz	k1gMnSc1
<g/>
,	,	kIx,
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Děkanství	děkanství	k1gNnSc1
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
snad	snad	k9
již	již	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1440	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
opuštěno	opuštěn	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
kolem	kolem	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
nepůsobil	působit	k5eNaImAgMnS
žádný	žádný	k3yNgMnSc1
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1607	#num#	k4
bylo	být	k5eAaImAgNnS
Smilem	smil	k1gInSc7
Osovským	Osovský	k2eAgInSc7d1
povoleno	povolen	k2eAgNnSc1d1
kostelu	kostel	k1gInSc3
sv.	sv.	kA
Martina	Martin	k1gInSc2
řídit	řídit	k5eAaImF
se	se	k3xPyFc4
augšpurskou	augšpurský	k2eAgFnSc7d1
konfesí	konfese	k1gFnSc7
a	a	k8xC
právo	právo	k1gNnSc4
pochovávat	pochovávat	k5eAaImF
na	na	k7c4
třebíčský	třebíčský	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1622	#num#	k4
třebíčští	třebíčský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
navštěvovali	navštěvovat	k5eAaImAgMnP
katolické	katolický	k2eAgFnPc4d1
mše	mše	k1gFnPc4
v	v	k7c6
Rudíkově	Rudíkův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1630	#num#	k4
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgMnS
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
i	i	k9
do	do	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Martina	Martin	k1gMnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sídlem	sídlo	k1gNnSc7
děkanství	děkanství	k1gNnSc2
se	se	k3xPyFc4
Třebíč	Třebíč	k1gFnSc1
stala	stát	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1671	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
děkanátu	děkanát	k1gInSc3
byly	být	k5eAaImAgFnP
přiděleny	přidělen	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Třebičská	Třebičský	k2eAgNnPc1d1
<g/>
,	,	kIx,
Vladislavská	vladislavský	k2eAgNnPc1d1
<g/>
,	,	kIx,
Kamenická	kamenický	k2eAgNnPc1d1
<g/>
,	,	kIx,
Rudíkovská	Rudíkovský	k2eAgNnPc1d1
<g/>
,	,	kIx,
Náměšťská	náměšťský	k2eAgNnPc1d1
<g/>
,	,	kIx,
Heraltická	Heraltický	k2eAgNnPc1d1
<g/>
,	,	kIx,
Opatovská	Opatovský	k2eAgNnPc1d1
<g/>
,	,	kIx,
Stařecká	stařecký	k2eAgNnPc1d1
<g/>
,	,	kIx,
Čáslavická	Čáslavický	k2eAgNnPc1d1
<g/>
,	,	kIx,
Roketnická	Roketnický	k2eAgNnPc1d1
<g/>
,	,	kIx,
Valečská	Valečský	k2eAgNnPc1d1
a	a	k8xC
Dalešická	Dalešický	k2eAgNnPc1d1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
patřily	patřit	k5eAaImAgInP
sem	sem	k6eAd1
farnosti	farnost	k1gFnSc2
<g/>
:	:	kIx,
Třebíčská	třebíčský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Stařecká	stařecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Roketnická	Roketnický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Opatovská	Opatovský	k2eAgFnSc1d1
a	a	k8xC
Heraltická	Heraltický	k2eAgFnSc1d1
a	a	k8xC
2	#num#	k4
lokální	lokální	k2eAgInSc4d1
kaplanství	kaplanství	k1gNnSc1
:	:	kIx,
Vladislavské	vladislavský	k2eAgFnPc1d1
a	a	k8xC
Čáslavické	Čáslavický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1786	#num#	k4
byla	být	k5eAaImAgFnS
vyloučena	vyloučit	k5eAaPmNgFnS
farnost	farnost	k1gFnSc1
Opatovská	Opatovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1788	#num#	k4
farnosti	farnost	k1gFnSc2
Stařecká	stařecký	k2eAgNnPc1d1
<g/>
,	,	kIx,
Roketnická	Roketnický	k2eAgNnPc1d1
<g/>
,	,	kIx,
lokální	lokální	k2eAgNnPc1d1
kaplanství	kaplanství	k1gNnPc1
Čáslavické	Čáslavická	k1gFnSc2
a	a	k8xC
farnost	farnost	k1gFnSc1
Heraltická	Heraltický	k2eAgFnSc1d1
<g/>
,	,	kIx,
za	za	k7c4
to	ten	k3xDgNnSc4
přivtěleny	přivtělen	k2eAgFnPc1d1
nově	nově	k6eAd1
zřízené	zřízený	k2eAgFnPc1d1
farnosti	farnost	k1gFnPc1
<g/>
,	,	kIx,
u	u	k7c2
Kapucínů	kapucín	k1gMnPc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Červené	Červené	k2eAgFnSc6d1
Lhotě	Lhota	k1gFnSc6
<g/>
,	,	kIx,
farnost	farnost	k1gFnSc1
Přibyslavická	Přibyslavická	k1gFnSc1
<g/>
,	,	kIx,
kurácie	kurácie	k1gFnSc1
v	v	k7c6
zámku	zámek	k1gInSc6
a	a	k8xC
lokální	lokální	k2eAgNnSc1d1
kaplanství	kaplanství	k1gNnSc1
<g/>
:	:	kIx,
Benetické	Benetický	k2eAgFnPc1d1
<g/>
,	,	kIx,
Trnavské	trnavský	k2eAgFnPc1d1
a	a	k8xC
Střížovské	Střížovský	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HEŘMANOVÁ	Heřmanová	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
kůru	kůr	k1gInSc2
farního	farní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
sv.	sv.	kA
Martina	Martin	k1gMnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
45	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
a	a	k8xC
Collegium	Collegium	k1gNnSc1
Marianum	Marianum	k?
-	-	kIx~
Týnská	týnský	k2eAgFnSc1d1
vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Slavická	Slavická	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
18	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
-	-	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RŮŽIČKOVÁ	Růžičková	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
školství	školství	k1gNnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
43	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
-	-	kIx~
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
-	-	kIx~
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Kopeček	Kopeček	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DVORSKÝ	Dvorský	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
-	-	kIx~
Třebický	Třebický	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
.	.	kIx.
481	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
138	#num#	k4
<g/>
-	-	kIx~
<g/>
141	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Biskupství	biskupství	k1gNnSc4
brněnské	brněnský	k2eAgNnSc4d1
<g/>
:	:	kIx,
děkanství	děkanství	k1gNnSc4
třebíčské	třebíčský	k2eAgFnSc2d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Farnosti	farnost	k1gFnSc2
Děkanátu	děkanát	k1gInSc2
Třebíč	Třebíč	k1gFnSc1
</s>
<s>
Benetice	Benetika	k1gFnSc3
•	•	k?
Březník	Březník	k1gMnSc1
•	•	k?
Čáslavice	Čáslavice	k1gFnSc2
•	•	k?
Červená	červený	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Dalešice	Dalešice	k1gFnSc1
•	•	k?
Dukovany	Dukovany	k1gInPc1
•	•	k?
Hartvíkovice	Hartvíkovice	k1gFnSc2
•	•	k?
Heraltice	Heraltice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Hrotovice	Hrotovice	k1gFnPc4
•	•	k?
Chlum	chlum	k1gInSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
•	•	k?
Jinošov	Jinošov	k1gInSc1
•	•	k?
Kněžice	kněžic	k1gMnSc2
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
•	•	k?
Koněšín	Koněšín	k1gInSc1
•	•	k?
Lipník	Lipník	k1gInSc1
u	u	k7c2
Hrotovic	Hrotovice	k1gFnPc2
•	•	k?
Mohelno	Mohelna	k1gFnSc5
•	•	k?
Náměšť	Náměšť	k1gFnSc4
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
•	•	k?
Okříšky	Okříšek	k1gInPc1
•	•	k?
Opatov	Opatov	k1gInSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Předín	Předín	k1gInSc1
•	•	k?
Přibyslavice	Přibyslavice	k1gFnSc2
•	•	k?
Rokytnice	Rokytnice	k1gFnSc1
nad	nad	k7c7
Rokytnou	Rokytný	k2eAgFnSc7d1
•	•	k?
Rouchovany	Rouchovan	k1gMnPc4
•	•	k?
Rudíkov	Rudíkov	k1gInSc1
•	•	k?
Stařeč	Stařeč	k1gInSc1
•	•	k?
Střížov-Číměř	Střížov-Číměř	k1gInSc1
•	•	k?
Třebíč-Jejkov	Třebíč-Jejkov	k1gInSc1
•	•	k?
Třebíč-město	Třebíč-města	k1gMnSc5
•	•	k?
Třebíč-zámek	Třebíč-zámek	k1gInSc1
•	•	k?
Trnava	Trnava	k1gFnSc1
u	u	k7c2
Třebíče	Třebíč	k1gFnSc2
•	•	k?
Valeč	Valeč	k1gMnSc1
u	u	k7c2
Hrotovic	Hrotovice	k1gFnPc2
•	•	k?
Vladislav	Vladislav	k1gMnSc1
</s>
<s>
Děkanáty	děkanát	k1gInPc1
Diecéze	diecéze	k1gFnSc2
brněnské	brněnský	k2eAgFnSc2d1
</s>
<s>
Blansko	Blansko	k1gNnSc1
•	•	k?
Boskovice	Boskovice	k1gInPc1
•	•	k?
Brno	Brno	k1gNnSc4
•	•	k?
Břeclav	Břeclav	k1gFnSc1
•	•	k?
Hodonín	Hodonín	k1gInSc1
•	•	k?
Hustopeče	Hustopeč	k1gFnSc2
•	•	k?
Jihlava	Jihlava	k1gFnSc1
•	•	k?
Mikulov	Mikulov	k1gInSc1
•	•	k?
Modřice	Modřice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Rosice	Rosice	k1gFnPc4
•	•	k?
Slavkov	Slavkov	k1gInSc1
•	•	k?
Telč	Telč	k1gFnSc1
•	•	k?
Tišnov	Tišnov	k1gInSc1
•	•	k?
Třebíč	Třebíč	k1gFnSc1
•	•	k?
Velké	velký	k2eAgNnSc4d1
Meziříčí	Meziříčí	k1gNnSc4
•	•	k?
Vranov	Vranov	k1gInSc1
•	•	k?
Znojmo	Znojmo	k1gNnSc4
•	•	k?
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
