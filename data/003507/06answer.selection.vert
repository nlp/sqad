<s>
Epica	Epica	k1gFnSc1	Epica
poté	poté	k6eAd1	poté
sestavila	sestavit	k5eAaPmAgFnS	sestavit
sbor	sbor	k1gInSc4	sbor
-	-	kIx~	-
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
šesti	šest	k4xCc2	šest
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
šesti	šest	k4xCc2	šest
žen	žena	k1gFnPc2	žena
-	-	kIx~	-
a	a	k8xC	a
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
orchestr	orchestr	k1gInSc1	orchestr
-	-	kIx~	-
troje	troje	k4xRgFnPc1	troje
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
violy	viola	k1gFnPc1	viola
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
violoncella	violoncello	k1gNnPc4	violoncello
a	a	k8xC	a
kontrabas	kontrabas	k1gInSc4	kontrabas
-	-	kIx~	-
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
