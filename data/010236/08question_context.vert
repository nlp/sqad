<s>
Příklopka	příklopka	k1gFnSc1	příklopka
hrtanová	hrtanový	k2eAgFnSc1d1	hrtanová
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
epiglottis	epiglottis	k1gFnSc1	epiglottis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
odstupu	odstup	k1gInSc6	odstup
hrtanu	hrtan	k1gInSc2	hrtan
a	a	k8xC	a
hltanu	hltan	k1gInSc2	hltan
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
lístku	lístek	k1gInSc2	lístek
<g/>
.	.	kIx.	.
</s>
