<p>
<s>
Příklopka	příklopka	k1gFnSc1	příklopka
hrtanová	hrtanový	k2eAgFnSc1d1	hrtanová
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
epiglottis	epiglottis	k1gFnSc1	epiglottis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
odstupu	odstup	k1gInSc6	odstup
hrtanu	hrtan	k1gInSc2	hrtan
a	a	k8xC	a
hltanu	hltan	k1gInSc2	hltan
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
lístku	lístek	k1gInSc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
elastická	elastický	k2eAgFnSc1d1	elastická
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
drobné	drobný	k2eAgInPc4d1	drobný
svaly	sval	k1gInPc4	sval
laryngu	larynx	k1gInSc2	larynx
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
funkci	funkce	k1gFnSc3	funkce
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přikládá	přikládat	k5eAaImIp3nS	přikládat
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
jej	on	k3xPp3gMnSc4	on
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vdechnutí	vdechnutí	k1gNnSc1	vdechnutí
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tekutin	tekutina	k1gFnPc2	tekutina
do	do	k7c2	do
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anatomie	anatomie	k1gFnSc2	anatomie
==	==	k?	==
</s>
</p>
<p>
<s>
Částilamina	Částilamin	k2eAgFnSc1d1	Částilamin
epiglottidis	epiglottidis	k1gFnSc1	epiglottidis
</s>
</p>
<p>
<s>
petiolus	petiolus	k1gInSc1	petiolus
epiglottidis	epiglottidis	k1gInSc1	epiglottidis
(	(	kIx(	(
<g/>
stopka	stopka	k1gFnSc1	stopka
<g/>
)	)	kIx)	)
<g/>
Vazyligamentum	Vazyligamentum	k1gNnSc1	Vazyligamentum
thyroepiglotticum	thyroepiglotticum	k1gInSc1	thyroepiglotticum
–	–	k?	–
připojuje	připojovat	k5eAaImIp3nS	připojovat
stopku	stopka	k1gFnSc4	stopka
k	k	k7c3	k
štítné	štítný	k2eAgFnSc3d1	štítná
chrupavce	chrupavka	k1gFnSc3	chrupavka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ligamentum	ligamentum	k1gNnSc1	ligamentum
hyoepiglotticum	hyoepiglotticum	k1gInSc1	hyoepiglotticum
–	–	k?	–
spojuje	spojovat	k5eAaImIp3nS	spojovat
přední	přední	k2eAgFnSc4d1	přední
plochu	plocha	k1gFnSc4	plocha
záklopky	záklopka	k1gFnSc2	záklopka
hrtanové	hrtanový	k2eAgFnSc2d1	hrtanová
s	s	k7c7	s
jazylkou	jazylka	k1gFnSc7	jazylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Histologie	histologie	k1gFnSc2	histologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tunica	Tunic	k2eAgFnSc1d1	Tunica
mucosa	mucosa	k1gFnSc1	mucosa
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Lamina	lamin	k2eAgFnSc1d1	lamina
epitelialis	epitelialis	k1gFnSc1	epitelialis
====	====	k?	====
</s>
</p>
<p>
<s>
Epiglottis	Epiglottis	k1gInSc1	Epiglottis
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
povrchy	povrch	k1gInPc4	povrch
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lingvální	lingvální	k2eAgInSc1d1	lingvální
–	–	k?	–
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
jej	on	k3xPp3gInSc4	on
vrstevnatý	vrstevnatý	k2eAgInSc4d1	vrstevnatý
dlaždicový	dlaždicový	k2eAgInSc4d1	dlaždicový
epitel	epitel	k1gInSc4	epitel
bez	bez	k7c2	bez
rohovatění	rohovatění	k1gNnSc2	rohovatění
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
laryngeální	laryngeální	k2eAgInSc1d1	laryngeální
–	–	k?	–
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
jej	on	k3xPp3gInSc4	on
víceřadý	víceřadý	k2eAgInSc4d1	víceřadý
cylindrický	cylindrický	k2eAgInSc4d1	cylindrický
epitel	epitel	k1gInSc4	epitel
s	s	k7c7	s
řasinkami	řasinka	k1gFnPc7	řasinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lamina	lamin	k2eAgNnPc1d1	lamino
propria	proprium	k1gNnPc1	proprium
mucosae	mucosaat	k5eAaPmIp3nS	mucosaat
====	====	k?	====
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
vrstvou	vrstva	k1gFnSc7	vrstva
(	(	kIx(	(
<g/>
od	od	k7c2	od
chrupavky	chrupavka	k1gFnSc2	chrupavka
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lamina	lamino	k1gNnPc4	lamino
propria	proprium	k1gNnSc2	proprium
mucosae	mucosa	k1gFnSc2	mucosa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
serozní	serozní	k2eAgFnSc1d1	serozní
(	(	kIx(	(
<g/>
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
tmavší	tmavý	k2eAgInSc4d2	tmavší
<g/>
)	)	kIx)	)
a	a	k8xC	a
mucinózní	mucinózní	k2eAgMnPc1d1	mucinózní
(	(	kIx(	(
<g/>
světlejší	světlý	k2eAgFnPc4d2	světlejší
<g/>
)	)	kIx)	)
žlázky	žlázka	k1gFnPc4	žlázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chrupavka	chrupavka	k1gFnSc1	chrupavka
===	===	k?	===
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
epiglottis	epiglottis	k1gFnSc2	epiglottis
je	být	k5eAaImIp3nS	být
elastická	elastický	k2eAgFnSc1d1	elastická
chrupavka	chrupavka	k1gFnSc1	chrupavka
obalená	obalený	k2eAgFnSc1d1	obalená
perichondriem	perichondrium	k1gNnSc7	perichondrium
<g/>
.	.	kIx.	.
</s>
<s>
Chrupavka	chrupavka	k1gFnSc1	chrupavka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
izogenetické	izogenetický	k2eAgFnPc4d1	izogenetický
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
skupiny	skupina	k1gFnPc4	skupina
jedné	jeden	k4xCgFnSc2	jeden
až	až	k9	až
dvou	dva	k4xCgFnPc2	dva
buněk	buňka	k1gFnPc2	buňka
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
delením	delení	k1gNnSc7	delení
jednoho	jeden	k4xCgInSc2	jeden
chondrocytu	chondrocyt	k1gInSc2	chondrocyt
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
společně	společně	k6eAd1	společně
obalené	obalený	k2eAgFnSc3d1	obalená
pouzdrem	pouzdro	k1gNnSc7	pouzdro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Foniatrie	foniatrie	k1gFnSc2	foniatrie
==	==	k?	==
</s>
</p>
<p>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
epiglotály	epiglotála	k1gFnPc1	epiglotála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
kouření	kouření	k1gNnSc6	kouření
se	se	k3xPyFc4	se
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kroužky	kroužek	k1gInPc1	kroužek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MUDR.	MUDR.	k?	MUDR.
EIS	eis	k1gNnSc1	eis
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
MUDR.	MUDR.	k1gMnSc1	MUDR.
JELÍNEK	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
;	;	kIx,	;
MUDR.	MUDR.	k1gMnSc1	MUDR.
ŠPAČEK	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JUNQUIERA	JUNQUIERA	kA	JUNQUIERA
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Carlos	Carlos	k1gMnSc1	Carlos
<g/>
;	;	kIx,	;
CARNEIRO	CARNEIRO	kA	CARNEIRO
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
<g/>
;	;	kIx,	;
KELLEY	KELLEY	kA	KELLEY
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
O.	O.	kA	O.
Základy	základ	k1gInPc1	základ
histologie	histologie	k1gFnSc2	histologie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
<g/>
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85787	[number]	k4	85787
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
text	text	k1gInSc1	text
článku	článek	k1gInSc2	článek
Epiglottis	Epiglottis	k1gFnPc2	Epiglottis
ve	v	k7c6	v
WikiSkriptech	WikiSkript	k1gInPc6	WikiSkript
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
zapojených	zapojený	k2eAgFnPc2d1	zapojená
v	v	k7c6	v
MEFANETu	MEFANETus	k1gInSc6	MEFANETus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
text	text	k1gInSc1	text
článku	článek	k1gInSc2	článek
Epiglottis	Epiglottis	k1gFnPc2	Epiglottis
(	(	kIx(	(
<g/>
histologický	histologický	k2eAgInSc1d1	histologický
preparát	preparát	k1gInSc1	preparát
<g/>
)	)	kIx)	)
ve	v	k7c6	v
WikiSkriptech	WikiSkript	k1gInPc6	WikiSkript
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
zapojených	zapojený	k2eAgFnPc2d1	zapojená
v	v	k7c6	v
MEFANETu	MEFANETus	k1gInSc6	MEFANETus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hltan	hltan	k1gInSc1	hltan
</s>
</p>
<p>
<s>
Hrtan	hrtan	k1gInSc1	hrtan
</s>
</p>
<p>
<s>
Polykání	polykání	k1gNnSc1	polykání
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Příklopka	příklopka	k1gFnSc1	příklopka
hrtanová	hrtanový	k2eAgFnSc1d1	hrtanová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Epiglottis	Epiglottis	k1gFnSc2	Epiglottis
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
