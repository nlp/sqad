<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc4d1	fénixův
řád	řád	k1gInSc4	řád
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
kniha	kniha	k1gFnSc1	kniha
řady	řada	k1gFnSc2	řada
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
,	,	kIx,	,
autorkou	autorka	k1gFnSc7	autorka
této	tento	k3xDgFnSc2	tento
série	série	k1gFnSc2	série
je	být	k5eAaImIp3nS	být
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
dohromady	dohromady	k6eAd1	dohromady
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
38	[number]	k4	38
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
255	[number]	k4	255
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
společnost	společnost	k1gFnSc1	společnost
Warner	Warnra	k1gFnPc2	Warnra
Brothers	Brothers	k1gInSc4	Brothers
oznámila	oznámit	k5eAaPmAgFnS	oznámit
film	film	k1gInSc4	film
téhož	týž	k3xTgInSc2	týž
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
překlad	překlad	k1gInSc1	překlad
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
oficiální	oficiální	k2eAgNnSc4d1	oficiální
evropské	evropský	k2eAgNnSc4d1	Evropské
překladové	překladový	k2eAgNnSc4d1	překladové
vydání	vydání	k1gNnSc4	vydání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
Černé	Černé	k2eAgFnSc6d1	Černé
Hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
srbštině	srbština	k1gFnSc6	srbština
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Narodna	Narodno	k1gNnSc2	Narodno
Knjiga	Knjiga	k1gFnSc1	Knjiga
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
skupina	skupina	k1gFnSc1	skupina
studentů	student	k1gMnPc2	student
přeložila	přeložit	k5eAaPmAgFnS	přeložit
knihu	kniha	k1gFnSc4	kniha
již	již	k6eAd1	již
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
oficiální	oficiální	k2eAgNnSc1d1	oficiální
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
14	[number]	k4	14
<g/>
letý	letý	k2eAgMnSc1d1	letý
školák	školák	k1gMnSc1	školák
ji	on	k3xPp3gFnSc4	on
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
způsobil	způsobit	k5eAaPmAgInS	způsobit
zmatek	zmatek	k1gInSc4	zmatek
<g/>
;	;	kIx,	;
Albatros	albatros	k1gMnSc1	albatros
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
chlapce	chlapec	k1gMnPc4	chlapec
zažalovat	zažalovat	k5eAaPmF	zažalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
události	událost	k1gFnPc4	událost
pátého	pátý	k4xOgInSc2	pátý
roku	rok	k1gInSc2	rok
Harryho	Harry	k1gMnSc2	Harry
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
po	po	k7c4	po
znovu	znovu	k6eAd1	znovu
nabytí	nabytí	k1gNnSc4	nabytí
sil	síla	k1gFnPc2	síla
jeho	jeho	k3xOp3gMnSc2	jeho
největšího	veliký	k2eAgMnSc2d3	veliký
nepřítele	nepřítel	k1gMnSc2	nepřítel
-	-	kIx~	-
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
(	(	kIx(	(
<g/>
Toma	Tom	k1gMnSc2	Tom
Raddlea	Raddleus	k1gMnSc2	Raddleus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
ještě	ještě	k9	ještě
v	v	k7c6	v
Kvikálkově	Kvikálkův	k2eAgInSc6d1	Kvikálkův
napadnou	napadnout	k5eAaPmIp3nP	napadnout
dva	dva	k4xCgMnPc1	dva
mozkomorové	mozkomorové	k2eAgMnPc1d1	mozkomorové
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgInSc2d1	hlavní
štábu	štáb	k1gInSc2	štáb
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
nucen	nucen	k2eAgMnSc1d1	nucen
se	se	k3xPyFc4	se
dostavit	dostavit	k5eAaPmF	dostavit
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
kvůli	kvůli	k7c3	kvůli
disciplinárnímu	disciplinární	k2eAgNnSc3d1	disciplinární
řízení	řízení	k1gNnSc3	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
Harry	Harr	k1gMnPc4	Harr
zproštěn	zproštěn	k2eAgInSc1d1	zproštěn
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
provedl	provést	k5eAaPmAgMnS	provést
kouzlo	kouzlo	k1gNnSc4	kouzlo
před	před	k7c7	před
mudly	mudl	k1gMnPc7	mudl
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
uspěje	uspět	k5eAaPmIp3nS	uspět
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
moct	moct	k5eAaImF	moct
dál	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
blázna	blázen	k1gMnSc4	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
však	však	k9	však
rok	rok	k1gInSc4	rok
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
studentů	student	k1gMnPc2	student
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
Harry	Harr	k1gInPc4	Harr
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
a	a	k8xC	a
Harry	Harr	k1gInPc4	Harr
si	se	k3xPyFc3	se
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pan	pan	k1gMnSc1	pan
Weasley	Weaslea	k1gFnSc2	Weaslea
napaden	napadnout	k5eAaPmNgMnS	napadnout
hadem	had	k1gMnSc7	had
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
ostatní	ostatní	k2eAgMnPc1d1	ostatní
spolužáci	spolužák	k1gMnPc1	spolužák
ho	on	k3xPp3gNnSc4	on
vzbudí	vzbudit	k5eAaPmIp3nP	vzbudit
a	a	k8xC	a
zavolají	zavolat	k5eAaPmIp3nP	zavolat
profesorku	profesorka	k1gFnSc4	profesorka
McGonagallovou	McGonagallová	k1gFnSc4	McGonagallová
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
šla	jít	k5eAaImAgFnS	jít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
za	za	k7c7	za
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řešili	řešit	k5eAaImAgMnP	řešit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přemístili	přemístit	k5eAaPmAgMnP	přemístit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
šli	jít	k5eAaImAgMnP	jít
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Svatého	svatý	k2eAgMnSc2d1	svatý
Munga	mungo	k1gMnSc2	mungo
za	za	k7c7	za
panem	pan	k1gMnSc7	pan
Arturem	Artur	k1gMnSc7	Artur
Weaslym	Weaslym	k1gInSc4	Weaslym
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Weasly	Weasly	k1gMnSc1	Weasly
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
u	u	k7c2	u
svatého	svatý	k2eAgMnSc2d1	svatý
Munga	mungo	k1gMnSc2	mungo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
potká	potkat	k5eAaPmIp3nS	potkat
Harry	Harra	k1gFnPc4	Harra
rodiče	rodič	k1gMnSc2	rodič
Neville	Nevill	k1gMnSc2	Nevill
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
Harry	Harra	k1gFnPc4	Harra
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
nitrobranu	nitrobrana	k1gFnSc4	nitrobrana
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Severuse	Severuse	k1gFnSc2	Severuse
Snapea	Snape	k1gInSc2	Snape
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
lordu	lord	k1gMnSc3	lord
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
ve	v	k7c6	v
vnikání	vnikání	k1gNnSc6	vnikání
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nitrobraně	nitrobrana	k1gFnSc6	nitrobrana
však	však	k9	však
neuspěje	uspět	k5eNaPmIp3nS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zkouškách	zkouška	k1gFnPc6	zkouška
náležité	náležitý	k2eAgFnSc2d1	náležitá
kouzelnické	kouzelnický	k2eAgFnSc2d1	kouzelnická
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
usne	usnout	k5eAaPmIp3nS	usnout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
další	další	k2eAgNnSc4d1	další
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vidění	vidění	k1gNnSc1	vidění
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
odboru	odbor	k1gInSc6	odbor
záhad	záhada	k1gFnPc2	záhada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
jakoby	jakoby	k8xS	jakoby
mučí	mučet	k5eAaImIp3nS	mučet
jeho	jeho	k3xOp3gFnSc1	jeho
kmotra	kmotra	k1gFnSc1	kmotra
Siriuse	Siriuse	k1gFnSc1	Siriuse
Blacka	Blacka	k1gFnSc1	Blacka
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vydá	vydat	k5eAaPmIp3nS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
však	však	k9	však
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
již	již	k6eAd1	již
čekají	čekat	k5eAaImIp3nP	čekat
následovníci	následovník	k1gMnPc1	následovník
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
-	-	kIx~	-
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Nikomu	nikdo	k3yNnSc3	nikdo
se	se	k3xPyFc4	se
však	však	k9	však
nic	nic	k3yNnSc1	nic
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věštba	věštba	k1gFnSc1	věštba
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc1	její
záznam	záznam	k1gInSc1	záznam
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozbije	rozbít	k5eAaPmIp3nS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
jediné	jediné	k1gNnSc4	jediné
<g/>
,	,	kIx,	,
co	co	k8xS	co
Pán	pán	k1gMnSc1	pán
zla	zlo	k1gNnSc2	zlo
(	(	kIx(	(
<g/>
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
)	)	kIx)	)
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
dorazí	dorazit	k5eAaPmIp3nP	dorazit
členové	člen	k1gMnPc1	člen
Řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Siriusem	Sirius	k1gMnSc7	Sirius
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zabije	zabít	k5eAaPmIp3nS	zabít
Belatrix	Belatrix	k1gInSc1	Belatrix
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
sestřenice	sestřenice	k1gFnSc1	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc3	Harrym
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
když	když	k8xS	když
umře	umřít	k5eAaPmIp3nS	umřít
už	už	k9	už
jeho	jeho	k3xOp3gMnSc1	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
příbuzný	příbuzný	k1gMnSc1	příbuzný
s	s	k7c7	s
kouzelnickou	kouzelnický	k2eAgFnSc7d1	kouzelnická
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Harrymu	Harrym	k1gInSc2	Harrym
Brumbál	brumbál	k1gMnSc1	brumbál
později	pozdě	k6eAd2	pozdě
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
věštbě	věštba	k1gFnSc6	věštba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nP	vracet
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
