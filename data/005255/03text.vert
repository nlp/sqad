<p>
<s>
404	[number]	k4	404
nebo	nebo	k8xC	nebo
Not	nota	k1gFnPc2	nota
Found	Found	k1gMnSc1	Found
je	být	k5eAaImIp3nS	být
stavový	stavový	k2eAgInSc4d1	stavový
kód	kód	k1gInSc4	kód
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
klientských	klientský	k2eAgFnPc2d1	klientská
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
vracený	vracený	k2eAgInSc1d1	vracený
serverem	server	k1gInSc7	server
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
soubor	soubor	k1gInSc1	soubor
nebyl	být	k5eNaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavový	stavový	k2eAgInSc4d1	stavový
kód	kód	k1gInSc4	kód
HTTP	HTTP	kA	HTTP
protokolu	protokol	k1gInSc2	protokol
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Timothy	Timotha	k1gFnSc2	Timotha
Bernes-Lee	Bernes-Lee	k1gInSc1	Bernes-Lee
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
www	www	k?	www
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
konsorciu	konsorcium	k1gNnSc6	konsorcium
W3C	W3C	k1gFnSc2	W3C
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Kódy	kód	k1gInPc1	kód
chyb	chyba	k1gFnPc2	chyba
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
specifikace	specifikace	k1gFnSc2	specifikace
HTTP	HTTP	kA	HTTP
verze	verze	k1gFnSc1	verze
0.9	[number]	k4	0.9
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
se	se	k3xPyFc4	se
použily	použít	k5eAaPmAgInP	použít
stavy	stav	k1gInPc1	stav
z	z	k7c2	z
FTP	FTP	kA	FTP
protokolu	protokol	k1gInSc2	protokol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgNnSc1d1	ostré
nasazení	nasazení	k1gNnSc1	nasazení
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgInP	dočkat
stavové	stavový	k2eAgInPc1d1	stavový
kódy	kód	k1gInPc1	kód
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
HTTP	HTTP	kA	HTTP
1.0	[number]	k4	1.0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavové	stavový	k2eAgNnSc1d1	stavové
hlášení	hlášení	k1gNnSc1	hlášení
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
popisu	popis	k1gInSc2	popis
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
pět	pět	k4xCc4	pět
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejznámější	známý	k2eAgNnSc1d3	nejznámější
značení	značení	k1gNnSc1	značení
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
xx	xx	k?	xx
a	a	k8xC	a
5	[number]	k4	5
<g/>
xx	xx	k?	xx
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
hlášení	hlášení	k1gNnSc2	hlášení
o	o	k7c6	o
chybě	chyba	k1gFnSc6	chyba
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
klienta	klient	k1gMnSc4	klient
4	[number]	k4	4
<g/>
xx	xx	k?	xx
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
serveru	server	k1gInSc2	server
5	[number]	k4	5
<g/>
xx	xx	k?	xx
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
dvě	dva	k4xCgNnPc1	dva
čísla	číslo	k1gNnPc1	číslo
pak	pak	k6eAd1	pak
udávají	udávat	k5eAaImIp3nP	udávat
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
webový	webový	k2eAgInSc1d1	webový
server	server	k1gInSc1	server
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
kódem	kód	k1gInSc7	kód
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
čtyřka	čtyřka	k1gFnSc1	čtyřka
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chyba	chyba	k1gFnSc1	chyba
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
dvě	dva	k4xCgFnPc1	dva
číslice	číslice	k1gFnPc1	číslice
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klient	klient	k1gMnSc1	klient
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
neexistující	existující	k2eNgFnSc4d1	neexistující
adresu	adresa	k1gFnSc4	adresa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odezva	odezva	k1gFnSc1	odezva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kód	kód	k1gInSc4	kód
a	a	k8xC	a
označení	označení	k1gNnSc4	označení
chyby	chyba	k1gFnSc2	chyba
v	v	k7c6	v
lidem	lid	k1gInSc7	lid
srozumitelné	srozumitelný	k2eAgFnSc3d1	srozumitelná
podobě	podoba	k1gFnSc3	podoba
-	-	kIx~	-
specifikace	specifikace	k1gFnSc1	specifikace
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
frázi	fráze	k1gFnSc4	fráze
Not	nota	k1gFnPc2	nota
Found	Founda	k1gFnPc2	Founda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
serverů	server	k1gInPc2	server
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
výchozí	výchozí	k2eAgFnSc1d1	výchozí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chyba	chyba	k1gFnSc1	chyba
404	[number]	k4	404
bývá	bývat	k5eAaImIp3nS	bývat
vrácena	vrácen	k2eAgFnSc1d1	vrácena
<g/>
,	,	kIx,	,
i	i	k8xC	i
pokud	pokud	k8xS	pokud
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
soubor	soubor	k1gInSc4	soubor
existoval	existovat	k5eAaImAgInS	existovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
smazán	smazán	k2eAgInSc1d1	smazán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
použít	použít	k5eAaPmF	použít
kód	kód	k1gInSc4	kód
HTTP	HTTP	kA	HTTP
301	[number]	k4	301
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
druhém	druhý	k4xOgNnSc6	druhý
HTTP	HTTP	kA	HTTP
410	[number]	k4	410
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chyby	chyba	k1gFnPc1	chyba
404	[number]	k4	404
by	by	k9	by
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
zaměňovány	zaměňován	k2eAgInPc1d1	zaměňován
s	s	k7c7	s
chybami	chyba	k1gFnPc7	chyba
DNS	DNS	kA	DNS
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nebyl	být	k5eNaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
chybové	chybový	k2eAgFnPc1d1	chybová
stránky	stránka	k1gFnPc1	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
chybovou	chybový	k2eAgFnSc4d1	chybová
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorra	k1gFnPc2	Explorra
(	(	kIx(	(
<g/>
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
7	[number]	k4	7
<g/>
)	)	kIx)	)
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zobrazit	zobrazit	k5eAaPmF	zobrazit
chybovou	chybový	k2eAgFnSc4d1	chybová
stránku	stránka	k1gFnSc4	stránka
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
512	[number]	k4	512
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
ní	on	k3xPp3gFnSc2	on
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
vlastní	vlastní	k2eAgInSc1d1	vlastní
"	"	kIx"	"
<g/>
přívětivou	přívětivý	k2eAgFnSc4d1	přívětivá
<g/>
"	"	kIx"	"
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
funkcionalitu	funkcionalita	k1gFnSc4	funkcionalita
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
Google	Google	k1gFnSc1	Google
Chrome	chromat	k5eAaImIp3nS	chromat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zobrazí	zobrazit	k5eAaPmIp3nP	zobrazit
návrhy	návrh	k1gInPc1	návrh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Falešná	falešný	k2eAgFnSc1d1	falešná
404	[number]	k4	404
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
servery	server	k1gInPc1	server
nevrací	vracet	k5eNaImIp3nP	vracet
kód	kód	k1gInSc4	kód
404	[number]	k4	404
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kód	kód	k1gInSc4	kód
HTTP	HTTP	kA	HTTP
200	[number]	k4	200
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
problém	problém	k1gInSc4	problém
programům	program	k1gInPc3	program
kontrolujícím	kontrolující	k2eAgFnPc3d1	kontrolující
funkčnost	funkčnost	k1gFnSc4	funkčnost
odkazů	odkaz	k1gInPc2	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
</s>
</p>
<p>
<s>
Hypertext	hypertext	k1gInSc1	hypertext
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
</s>
</p>
<p>
<s>
Stavové	stavový	k2eAgInPc1d1	stavový
kódy	kód	k1gInPc1	kód
HTTPExterní	HTTPExterní	k2eAgInPc1d1	HTTPExterní
odkazy	odkaz	k1gInPc1	odkaz
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HTTP	HTTP	kA	HTTP
404	[number]	k4	404
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Error	Error	k1gInSc1	Error
404	[number]	k4	404
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
číhají	číhat	k5eAaImIp3nP	číhat
na	na	k7c6	na
webu	web	k1gInSc6	web
</s>
</p>
