<p>
<s>
Parsek	parsek	k1gInSc1	parsek
<g/>
,	,	kIx,	,
také	také	k9	také
parsec	parsec	k1gInSc1	parsec
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
pc	pc	k?	pc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgNnPc1d1	používané
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
parsek	parsek	k1gInSc1	parsek
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
astronomická	astronomický	k2eAgFnSc1d1	astronomická
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
1	[number]	k4	1
AU	au	k0	au
<g/>
)	)	kIx)	)
úhlový	úhlový	k2eAgInSc4d1	úhlový
rozměr	rozměr	k1gInSc4	rozměr
jedné	jeden	k4xCgFnSc2	jeden
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
1	[number]	k4	1
pc	pc	k?	pc
=	=	kIx~	=
1	[number]	k4	1
AU	au	k0	au
/	/	kIx~	/
tg	tg	kA	tg
1	[number]	k4	1
<g/>
''	''	k?	''
≈	≈	k?	≈
206	[number]	k4	206
265	[number]	k4	265
AU	au	k0	au
≈	≈	k?	≈
3,262	[number]	k4	3,262
ly	ly	k?	ly
≈	≈	k?	≈
3,086	[number]	k4	3,086
<g/>
×	×	k?	×
<g/>
1016	[number]	k4	1016
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočet	výpočet	k1gInSc1	výpočet
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
z	z	k7c2	z
paralaxy	paralaxa	k1gFnSc2	paralaxa
==	==	k?	==
</s>
</p>
<p>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
d	d	k?	d
<g/>
,	,	kIx,	,
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
v	v	k7c6	v
parsecích	parsec	k1gInPc6	parsec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
převrácenou	převrácený	k2eAgFnSc7d1	převrácená
hodnotou	hodnota	k1gFnSc7	hodnota
poloviny	polovina	k1gFnSc2	polovina
půlroční	půlroční	k2eAgFnSc2d1	půlroční
paralaxy	paralaxa	k1gFnSc2	paralaxa
p	p	k?	p
<g/>
,	,	kIx,	,
vyjádřené	vyjádřený	k2eAgFnSc2d1	vyjádřená
v	v	k7c6	v
obloukových	obloukový	k2eAgFnPc6d1	oblouková
vteřinách	vteřina	k1gFnPc6	vteřina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
d	d	k?	d
=	=	kIx~	=
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
čas	čas	k1gInSc4	čas
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
při	při	k7c6	při
kolmém	kolmý	k2eAgInSc6d1	kolmý
pohledu	pohled	k1gInSc6	pohled
(	(	kIx(	(
<g/>
2	[number]	k4	2
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
uplatnění	uplatnění	k1gNnSc6	uplatnění
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
malé	malý	k2eAgInPc4d1	malý
úhly	úhel	k1gInPc4	úhel
<g/>
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
tg	tg	kA	tg
a.	a.	k?	a.
<g/>
Jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
však	však	k9	však
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
čekání	čekání	k1gNnSc2	čekání
při	při	k7c6	při
praktickém	praktický	k2eAgNnSc6d1	praktické
měření	měření	k1gNnSc6	měření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
i	i	k9	i
sekulární	sekulární	k2eAgFnSc1d1	sekulární
paralaxa	paralaxa	k1gFnSc1	paralaxa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pohyb	pohyb	k1gInSc1	pohyb
soustav	soustava	k1gFnPc2	soustava
uvnitř	uvnitř	k7c2	uvnitř
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
větších	veliký	k2eAgFnPc2d2	veliký
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
násobky	násobek	k1gInPc4	násobek
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
</s>
</p>
<p>
<s>
kiloparsek	kiloparsek	k1gInSc1	kiloparsek
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
kpc	kpc	k?	kpc
<g/>
,	,	kIx,	,
1	[number]	k4	1
kpc	kpc	k?	kpc
=	=	kIx~	=
103	[number]	k4	103
pc	pc	k?	pc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
megaparsek	megaparsek	k1gInSc1	megaparsek
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
Mpc	Mpc	k1gFnSc1	Mpc
<g/>
,	,	kIx,	,
1	[number]	k4	1
Mpc	Mpc	k1gFnPc2	Mpc
=	=	kIx~	=
106	[number]	k4	106
pc	pc	k?	pc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
délkovou	délkový	k2eAgFnSc7d1	délková
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
extragalaktické	extragalaktický	k2eAgFnSc6d1	extragalaktická
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
gigaparsek	gigaparsek	k1gInSc1	gigaparsek
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
Gpc	Gpc	k1gFnSc1	Gpc
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
Gpc	Gpc	k1gFnPc2	Gpc
=	=	kIx~	=
109	[number]	k4	109
pc	pc	k?	pc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Některé	některý	k3yIgFnPc4	některý
přibližné	přibližný	k2eAgFnPc4d1	přibližná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
v	v	k7c6	v
Mpc	Mpc	k1gFnSc6	Mpc
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgFnSc1d1	historická
poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc4	první
stanovení	stanovení	k1gNnSc4	stanovení
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
měření	měření	k1gNnSc2	měření
jejich	jejich	k3xOp3gFnSc2	jejich
roční	roční	k2eAgFnSc2d1	roční
paralaxy	paralaxa	k1gFnSc2	paralaxa
(	(	kIx(	(
<g/>
hvězda	hvězda	k1gFnSc1	hvězda
61	[number]	k4	61
Cyg	Cyg	k1gFnPc2	Cyg
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
)	)	kIx)	)
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bessel	Bessel	k1gMnSc1	Bessel
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
tuto	tento	k3xDgFnSc4	tento
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
běžně	běžně	k6eAd1	běžně
používanou	používaný	k2eAgFnSc7d1	používaná
jako	jako	k8xS	jako
jednotka	jednotka	k1gFnSc1	jednotka
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
astron	astron	k1gMnSc1	astron
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
Frank	Frank	k1gMnSc1	Frank
Watson	Watson	k1gMnSc1	Watson
Dyson	Dyson	k1gMnSc1	Dyson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Charlier	Charlier	k1gMnSc1	Charlier
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
název	název	k1gInSc1	název
siriometer	siriometrum	k1gNnPc2	siriometrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
návrh	návrh	k1gInSc1	návrh
parsec	parsec	k1gInSc4	parsec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podal	podat	k5eAaPmAgMnS	podat
Herbert	Herbert	k1gMnSc1	Herbert
Hall	Hall	k1gMnSc1	Hall
Turner	turner	k1gMnSc1	turner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
prvních	první	k4xOgFnPc2	první
slabik	slabika	k1gFnPc2	slabika
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
paralaxa	paralaxa	k1gFnSc1	paralaxa
a	a	k8xC	a
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
z	z	k7c2	z
parallax	parallax	k1gInSc4	parallax
a	a	k8xC	a
second	second	k1gInSc4	second
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
