<s>
Mjr.	mjr.	kA	mjr.
Roman	Roman	k1gMnSc1	Roman
Šebrle	Šebrl	k1gMnSc2	Šebrl
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
Lanškroun	Lanškroun	k1gInSc1	Lanškroun
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
atletický	atletický	k2eAgMnSc1d1	atletický
vícebojař	vícebojař	k1gMnSc1	vícebojař
a	a	k8xC	a
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
překonal	překonat	k5eAaPmAgInS	překonat
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
bájnou	bájný	k2eAgFnSc4d1	bájná
hranici	hranice	k1gFnSc4	hranice
9000	[number]	k4	9000
bodů	bod	k1gInPc2	bod
podle	podle	k7c2	podle
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
bodovacích	bodovací	k2eAgFnPc2d1	bodovací
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
;	;	kIx,	;
výkonem	výkon	k1gInSc7	výkon
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
26	[number]	k4	26
<g/>
.	.	kIx.	.
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
v	v	k7c6	v
Götzisu	Götzis	k1gInSc6	Götzis
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
nepřekonaný	překonaný	k2eNgInSc4d1	nepřekonaný
dalších	další	k2eAgInPc2d1	další
jedenáct	jedenáct	k4xCc1	jedenáct
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
vzal	vzít	k5eAaPmAgMnS	vzít
mu	on	k3xPp3gMnSc3	on
jej	on	k3xPp3gInSc4	on
teprve	teprve	k9	teprve
Američan	Američan	k1gMnSc1	Američan
Ashton	Ashton	k1gInSc4	Ashton
Eaton	Eaton	k1gNnSc1	Eaton
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
výkonem	výkon	k1gInSc7	výkon
9039	[number]	k4	9039
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sportem	sport	k1gInSc7	sport
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodišti	rodiště	k1gNnSc6	rodiště
jako	jako	k9	jako
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
sportu	sport	k1gInSc3	sport
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
devatenácti	devatenáct	k4xCc2	devatenáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
atletikou	atletika	k1gFnSc7	atletika
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
Týništi	týniště	k1gNnSc6	týniště
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
T	T	kA	T
<g/>
&	&	k?	&
<g/>
F	F	kA	F
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
startoval	startovat	k5eAaBmAgMnS	startovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Dukly	Dukla	k1gFnSc2	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
trenérem	trenér	k1gMnSc7	trenér
Jiří	Jiří	k1gMnSc1	Jiří
Čechák	Čechák	k1gMnSc1	Čechák
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dukle	Dukla	k1gFnSc6	Dukla
Praha	Praha	k1gFnSc1	Praha
nejdříve	dříve	k6eAd3	dříve
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Dalibor	Dalibor	k1gMnSc1	Dalibor
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
nového	nový	k2eAgInSc2d1	nový
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
výkonu	výkon	k1gInSc2	výkon
nad	nad	k7c4	nad
9000	[number]	k4	9000
bodů	bod	k1gInPc2	bod
podle	podle	k7c2	podle
současného	současný	k2eAgNnSc2d1	současné
bodování	bodování	k1gNnSc2	bodování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Götzisu	Götzis	k1gInSc6	Götzis
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
celkem	celek	k1gInSc7	celek
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vydržel	vydržet	k5eAaPmAgInS	vydržet
11	[number]	k4	11
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
světový	světový	k2eAgInSc1d1	světový
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
evropským	evropský	k2eAgMnSc7d1	evropský
a	a	k8xC	a
národním	národní	k2eAgInSc7d1	národní
rekordem	rekord	k1gInSc7	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vrcholem	vrchol	k1gInSc7	vrchol
kariéry	kariéra	k1gFnSc2	kariéra
Romana	Roman	k1gMnSc2	Roman
Šebrleho	Šebrle	k1gMnSc2	Šebrle
bylo	být	k5eAaImAgNnS	být
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
LOH	LOH	kA	LOH
2004	[number]	k4	2004
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dosud	dosud	k6eAd1	dosud
platný	platný	k2eAgInSc4d1	platný
olympijský	olympijský	k2eAgInSc4d1	olympijský
rekord	rekord	k1gInSc4	rekord
8893	[number]	k4	8893
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
ke	k	k7c3	k
stříbru	stříbro	k1gNnSc3	stříbro
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
přidal	přidat	k5eAaPmAgInS	přidat
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgMnS	vést
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
sezónách	sezóna	k1gFnPc6	sezóna
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
získal	získat	k5eAaPmAgInS	získat
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
HMS	HMS	kA	HMS
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
za	za	k7c4	za
6420	[number]	k4	6420
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bral	brát	k5eAaImAgInS	brát
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
výkonem	výkon	k1gInSc7	výkon
6438	[number]	k4	6438
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
evropský	evropský	k2eAgInSc4d1	evropský
a	a	k8xC	a
národní	národní	k2eAgInSc4d1	národní
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
halovém	halový	k2eAgInSc6d1	halový
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Dana	Dana	k1gFnSc1	Dana
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Briena	Brieno	k1gNnPc4	Brieno
(	(	kIx(	(
<g/>
6476	[number]	k4	6476
b.	b.	k?	b.
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
i	i	k8xC	i
kvůli	kvůli	k7c3	kvůli
relativně	relativně	k6eAd1	relativně
slabší	slabý	k2eAgFnSc3d2	slabší
tyči	tyč	k1gFnSc3	tyč
(	(	kIx(	(
<g/>
480	[number]	k4	480
cm	cm	kA	cm
<g/>
)	)	kIx)	)
utekl	utéct	k5eAaPmAgMnS	utéct
jen	jen	k9	jen
o	o	k7c4	o
38	[number]	k4	38
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
předností	přednost	k1gFnSc7	přednost
Šebrleho	Šebrle	k1gMnSc4	Šebrle
byla	být	k5eAaImAgFnS	být
výborná	výborný	k2eAgFnSc1d1	výborná
konzistence	konzistence	k1gFnSc1	konzistence
<g/>
,	,	kIx,	,
udržení	udržení	k1gNnSc1	udržení
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
kvality	kvalita	k1gFnSc2	kvalita
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
disciplín	disciplína	k1gFnPc2	disciplína
(	(	kIx(	(
<g/>
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
skákal	skákat	k5eAaImAgInS	skákat
kolem	kolem	k7c2	kolem
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
výrazně	výrazně	k6eAd1	výrazně
nad	nad	k7c4	nad
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
házel	házet	k5eAaImAgMnS	házet
daleko	daleko	k6eAd1	daleko
oštěpem	oštěp	k1gInSc7	oštěp
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obhajobě	obhajoba	k1gFnSc6	obhajoba
titulu	titul	k1gInSc2	titul
evropského	evropský	k2eAgMnSc4d1	evropský
šampiona	šampion	k1gMnSc4	šampion
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
7	[number]	k4	7
<g/>
.	.	kIx.	.
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
atletické	atletický	k2eAgFnSc2d1	atletická
akce	akce	k1gFnSc2	akce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zároveň	zároveň	k6eAd1	zároveň
prvním	první	k4xOgMnSc7	první
desetibojařem	desetibojař	k1gMnSc7	desetibojař
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokončil	dokončit	k5eAaPmAgMnS	dokončit
35	[number]	k4	35
závodů	závod	k1gInPc2	závod
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
součtem	součet	k1gInSc7	součet
přes	přes	k7c4	přes
8000	[number]	k4	8000
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgInSc7	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
další	další	k2eAgMnSc1d1	další
český	český	k2eAgMnSc1d1	český
desetibojař	desetibojař	k1gMnSc1	desetibojař
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	s	k7c7	s
34	[number]	k4	34
desetiboji	desetiboj	k1gInPc7	desetiboj
nad	nad	k7c4	nad
8000	[number]	k4	8000
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
MS	MS	kA	MS
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
8	[number]	k4	8
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
vrcholných	vrcholný	k2eAgFnPc2d1	vrcholná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
unikátních	unikátní	k2eAgInPc2d1	unikátní
20	[number]	k4	20
desetibojů	desetiboj	k1gInPc2	desetiboj
se	s	k7c7	s
součtem	součet	k1gInSc7	součet
nad	nad	k7c4	nad
8500	[number]	k4	8500
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
již	již	k9	již
hranice	hranice	k1gFnSc1	hranice
světové	světový	k2eAgFnSc2d1	světová
extratřídy	extratřída	k1gFnSc2	extratřída
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
překonal	překonat	k5eAaPmAgMnS	překonat
také	také	k9	také
šestkrát	šestkrát	k6eAd1	šestkrát
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
hranici	hranice	k1gFnSc4	hranice
8800	[number]	k4	8800
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
k	k	k7c3	k
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc3	srpen
2009	[number]	k4	2009
již	již	k9	již
45	[number]	k4	45
<g/>
krát	krát	k6eAd1	krát
8000	[number]	k4	8000
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
drží	držet	k5eAaImIp3nP	držet
historický	historický	k2eAgInSc4d1	historický
primát	primát	k1gInSc4	primát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
musel	muset	k5eAaImAgMnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
60	[number]	k4	60
m.	m.	k?	m.
př	př	kA	př
<g/>
.	.	kIx.	.
medailově	medailově	k6eAd1	medailově
rozjetý	rozjetý	k2eAgInSc4d1	rozjetý
sedmiboj	sedmiboj	k1gInSc4	sedmiboj
na	na	k7c6	na
HMS	HMS	kA	HMS
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
pro	pro	k7c4	pro
svalové	svalový	k2eAgNnSc4d1	svalové
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
výkonem	výkon	k1gInSc7	výkon
6142	[number]	k4	6142
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkovním	venkovní	k2eAgNnSc6d1	venkovní
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
však	však	k9	však
skončil	skončit	k5eAaPmAgMnS	skončit
výkonem	výkon	k1gInSc7	výkon
8266	[number]	k4	8266
bodů	bod	k1gInPc2	bod
až	až	k9	až
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
halové	halový	k2eAgFnSc6d1	halová
sezóně	sezóna	k1gFnSc6	sezóna
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
umístěním	umístění	k1gNnSc7	umístění
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
HMS	HMS	kA	HMS
v	v	k7c6	v
katarském	katarský	k2eAgInSc6d1	katarský
Dauhá	Dauhý	k2eAgNnPc1d1	Dauhý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výkonem	výkon	k1gInSc7	výkon
6024	[number]	k4	6024
bodů	bod	k1gInPc2	bod
znovu	znovu	k6eAd1	znovu
překonal	překonat	k5eAaPmAgMnS	překonat
šestitisícovou	šestitisícový	k2eAgFnSc4d1	šestitisícová
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
překonal	překonat	k5eAaPmAgInS	překonat
veteránský	veteránský	k2eAgInSc1d1	veteránský
světový	světový	k2eAgInSc1d1	světový
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nad	nad	k7c4	nad
35	[number]	k4	35
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
předchozí	předchozí	k2eAgMnSc1d1	předchozí
měl	mít	k5eAaImAgMnS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
5839	[number]	k4	5839
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
tento	tento	k3xDgInSc4	tento
výkon	výkon	k1gInSc1	výkon
ještě	ještě	k6eAd1	ještě
vylepšil	vylepšit	k5eAaPmAgInS	vylepšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
mistrovství	mistrovství	k1gNnSc6	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
Praze-Stromovce	Praze-Stromovka	k1gFnSc6	Praze-Stromovka
posbíral	posbírat	k5eAaPmAgInS	posbírat
6117	[number]	k4	6117
bodů	bod	k1gInPc2	bod
<g/>
..	..	k?	..
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
HME	HME	kA	HME
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
výkonem	výkon	k1gInSc7	výkon
nového	nový	k2eAgNnSc2d1	nové
veteránského	veteránský	k2eAgNnSc2d1	veteránské
SR	SR	kA	SR
6178	[number]	k4	6178
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
na	na	k7c4	na
ME	ME	kA	ME
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
výkonem	výkon	k1gInSc7	výkon
8052	[number]	k4	8052
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
již	již	k9	již
o	o	k7c4	o
jeho	jeho	k3xOp3gNnPc2	jeho
49	[number]	k4	49
<g/>
.	.	kIx.	.
desetiboj	desetiboj	k1gInSc1	desetiboj
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
nad	nad	k7c4	nad
8000	[number]	k4	8000
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výkonem	výkon	k1gInSc7	výkon
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
na	na	k7c4	na
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
už	už	k6eAd1	už
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
po	po	k7c6	po
první	první	k4xOgFnSc6	první
disciplíně	disciplína	k1gFnSc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
desetiboj	desetiboj	k1gInSc1	desetiboj
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pak	pak	k6eAd1	pak
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
zdravotních	zdravotní	k2eAgInPc6d1	zdravotní
problémech	problém	k1gInPc6	problém
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
člen	člen	k1gMnSc1	člen
Armádního	armádní	k2eAgNnSc2d1	armádní
sportovního	sportovní	k2eAgNnSc2d1	sportovní
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
ASC	ASC	kA	ASC
<g/>
)	)	kIx)	)
Dukla	Dukla	k1gFnSc1	Dukla
je	být	k5eAaImIp3nS	být
také	také	k9	také
vojákem	voják	k1gMnSc7	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
majora	major	k1gMnSc2	major
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
po	po	k7c6	po
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
vítězství	vítězství	k1gNnSc6	vítězství
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
již	již	k6eAd1	již
popáté	popáté	k4xO	popáté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
atletem	atlet	k1gMnSc7	atlet
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
.	.	kIx.	.
+	+	kIx~	+
<g/>
)	)	kIx)	)
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
Romana	Roman	k1gMnSc2	Roman
Šebrleho	Šebrle	k1gMnSc2	Šebrle
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
disciplínách	disciplína	k1gFnPc6	disciplína
desetiboje	desetiboj	k1gInSc2	desetiboj
dávají	dávat	k5eAaImIp3nP	dávat
součet	součet	k1gInSc1	součet
9326	[number]	k4	9326
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
roku	rok	k1gInSc2	rok
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Armádní	armádní	k2eAgMnSc1d1	armádní
sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2005	[number]	k4	2005
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2004	[number]	k4	2004
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
EAA	EAA	kA	EAA
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
Armádní	armádní	k2eAgMnSc1d1	armádní
sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2003	[number]	k4	2003
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2002	[number]	k4	2002
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2001	[number]	k4	2001
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2000	[number]	k4	2000
-	-	kIx~	-
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
<g/>
:	:	kIx,	:
2004	[number]	k4	2004
-	-	kIx~	-
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
propůjčena	propůjčen	k2eAgFnSc1d1	propůjčena
hodnost	hodnost	k1gFnSc1	hodnost
majora	major	k1gMnSc2	major
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
udělena	udělen	k2eAgFnSc1d1	udělena
medaile	medaile	k1gFnSc1	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
2001	[number]	k4	2001
a	a	k8xC	a
2004	[number]	k4	2004
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
cena	cena	k1gFnSc1	cena
ČOV	ČOV	kA	ČOV
J.	J.	kA	J.
Gutha	Guth	k1gMnSc2	Guth
-	-	kIx~	-
Jarkovského	Jarkovský	k2eAgMnSc2d1	Jarkovský
za	za	k7c4	za
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
sportovní	sportovní	k2eAgInSc4d1	sportovní
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
uplynulém	uplynulý	k2eAgInSc6d1	uplynulý
roce	rok	k1gInSc6	rok
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Desetiboj	desetiboj	k1gInSc1	desetiboj
-	-	kIx~	-
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
Götzis	Götzis	k1gInSc1	Götzis
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
bývalý	bývalý	k2eAgMnSc1d1	bývalý
SR	SR	kA	SR
<g/>
,	,	kIx,	,
ER	ER	kA	ER
<g/>
,	,	kIx,	,
NR	NR	kA	NR
<g/>
)	)	kIx)	)
Sedmiboj	sedmiboj	k1gInSc1	sedmiboj
-	-	kIx~	-
6438	[number]	k4	6438
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
Budapešť	Budapešť	k1gFnSc1	Budapešť
2004	[number]	k4	2004
<g/>
;	;	kIx,	;
ER	ER	kA	ER
<g/>
,	,	kIx,	,
NR	NR	kA	NR
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
veteránský	veteránský	k2eAgInSc1d1	veteránský
SR	SR	kA	SR
6178	[number]	k4	6178
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
zahájil	zahájit	k5eAaPmAgMnS	zahájit
golfovou	golfový	k2eAgFnSc4d1	golfová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
trenér	trenér	k1gMnSc1	trenér
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Carter	Carter	k1gMnSc1	Carter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
golfového	golfový	k2eAgInSc2d1	golfový
turnaje	turnaj	k1gInSc2	turnaj
série	série	k1gFnSc2	série
Czech	Czech	k1gMnSc1	Czech
PGA	PGA	kA	PGA
Tour	Tour	k1gMnSc1	Tour
v	v	k7c6	v
Kuřimi	Kuři	k1gFnPc7	Kuři
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
moderuje	moderovat	k5eAaBmIp3nS	moderovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
relaci	relace	k1gFnSc4	relace
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
Prima	primo	k1gNnSc2	primo
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorskou	moderátorský	k2eAgFnSc4d1	moderátorská
dvojici	dvojice	k1gFnSc4	dvojice
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
Gabrielou	Gabriela	k1gFnSc7	Gabriela
Kratochvílovou	Kratochvílová	k1gFnSc7	Kratochvílová
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Roman	Romana	k1gFnPc2	Romana
Šebrle	Šebrle	k1gFnSc2	Šebrle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
www.olympic.cz	www.olympic.cz	k1gInSc4	www.olympic.cz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Roman	Roman	k1gMnSc1	Roman
Šebrle	Šebrle	k1gFnSc2	Šebrle
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
Profil	profil	k1gInSc4	profil
na	na	k7c6	na
LOH	LOH	kA	LOH
2008	[number]	k4	2008
Roman	Romana	k1gFnPc2	Romana
Šebrle	Šebrle	k1gFnSc2	Šebrle
(	(	kIx(	(
<g/>
rozhovor	rozhovor	k1gInSc1	rozhovor
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
"	"	kIx"	"
<g/>
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
video	video	k1gNnSc1	video
on-line	onin	k1gInSc5	on-lin
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
Statistika	statistik	k1gMnSc2	statistik
golfisty	golfista	k1gMnSc2	golfista
na	na	k7c6	na
Golfparada	Golfparada	k1gFnSc1	Golfparada
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Video	video	k1gNnSc1	video
z	z	k7c2	z
vítězného	vítězný	k2eAgInSc2d1	vítězný
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
LOH	LOH	kA	LOH
2004	[number]	k4	2004
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
Video	video	k1gNnSc4	video
bývalého	bývalý	k2eAgMnSc2d1	bývalý
SR	SR	kA	SR
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
Götzisu	Götzis	k1gInSc2	Götzis
2001	[number]	k4	2001
</s>
