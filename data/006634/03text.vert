<s>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
VV	VV	kA	VV
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
období	období	k1gNnSc6	období
2010	[number]	k4	2010
až	až	k9	až
2012	[number]	k4	2012
participovala	participovat	k5eAaImAgFnS	participovat
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
jako	jako	k9	jako
strana	strana	k1gFnSc1	strana
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc2d1	vycházející
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
registrovanými	registrovaný	k2eAgMnPc7d1	registrovaný
příznivci	příznivec	k1gMnPc7	příznivec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
véčkaři	véčkař	k1gMnSc3	véčkař
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
prosazující	prosazující	k2eAgNnPc1d1	prosazující
referenda	referendum	k1gNnPc1	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vládní	vládní	k2eAgFnSc6d1	vládní
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
vnitrostranických	vnitrostranický	k2eAgInPc6d1	vnitrostranický
sporech	spor	k1gInPc6	spor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
založila	založit	k5eAaPmAgFnS	založit
část	část	k1gFnSc1	část
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
novou	nový	k2eAgFnSc4d1	nová
stranu	strana	k1gFnSc4	strana
LIDEM	lid	k1gInSc7	lid
–	–	k?	–
liberální	liberální	k2eAgMnPc1d1	liberální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
úzce	úzko	k6eAd1	úzko
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Pražan	Pražan	k1gMnSc1	Pražan
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vydává	vydávat	k5eAaPmIp3nS	vydávat
časopisy	časopis	k1gInPc7	časopis
Pražan	Pražan	k1gMnSc1	Pražan
a	a	k8xC	a
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnPc1d1	veřejná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgNnSc4d1	veřejné
transformovala	transformovat	k5eAaBmAgFnS	transformovat
na	na	k7c4	na
spolek	spolek	k1gInSc4	spolek
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc4	první
předsedkyní	předsedkyně	k1gFnPc2	předsedkyně
spolku	spolek	k1gInSc2	spolek
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnPc1d1	veřejná
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
strana	strana	k1gFnSc1	strana
byly	být	k5eAaImAgFnP	být
zaregistrovány	zaregistrován	k2eAgFnPc4d1	zaregistrována
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získaly	získat	k5eAaPmAgFnP	získat
VV	VV	kA	VV
jeden	jeden	k4xCgInSc4	jeden
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
MČ	MČ	kA	MČ
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
a	a	k8xC	a
působily	působit	k5eAaImAgFnP	působit
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
v	v	k7c6	v
MČ	MČ	kA	MČ
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
zaměřily	zaměřit	k5eAaPmAgFnP	zaměřit
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
občanům	občan	k1gMnPc3	občan
v	v	k7c6	v
každodenním	každodenní	k2eAgNnSc6d1	každodenní
řešení	řešení	k1gNnSc6	řešení
jejich	jejich	k3xOp3gInPc2	jejich
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
radnicí	radnice	k1gFnSc7	radnice
–	–	k?	–
hluk	hluk	k1gInSc4	hluk
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInPc4d1	sociální
a	a	k8xC	a
právní	právní	k2eAgInPc4d1	právní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
deregulace	deregulace	k1gFnSc1	deregulace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgMnSc2	svůj
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
zastupitele	zastupitel	k1gMnSc2	zastupitel
předkládaly	předkládat	k5eAaImAgFnP	předkládat
návrhy	návrh	k1gInPc4	návrh
usnesení	usnesení	k1gNnSc2	usnesení
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
fungování	fungování	k1gNnSc2	fungování
MČ	MČ	kA	MČ
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
aktivity	aktivita	k1gFnPc1	aktivita
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
občany	občan	k1gMnPc4	občan
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
místní	místní	k2eAgFnSc2d1	místní
buňky	buňka	k1gFnSc2	buňka
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
<g/>
,	,	kIx,	,
Černošicích	Černošice	k1gFnPc6	Černošice
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Věci	věc	k1gFnSc2	věc
černošické	černošický	k2eAgFnSc2d1	Černošická
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
z	z	k7c2	z
portálu	portál	k1gInSc2	portál
Novinky	novinka	k1gFnSc2	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
15.2	[number]	k4	15.2
<g/>
.2012	.2012	k4	.2012
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
slábnoucí	slábnoucí	k2eAgFnSc4d1	slábnoucí
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
členský	členský	k2eAgInSc1d1	členský
příspěvek	příspěvek	k1gInSc1	příspěvek
cca	cca	kA	cca
1	[number]	k4	1
700	[number]	k4	700
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
však	však	k8xC	však
prudce	prudko	k6eAd1	prudko
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
a	a	k8xC	a
čekatelů	čekatel	k1gMnPc2	čekatel
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
4	[number]	k4	4
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
nezdarech	nezdar	k1gInPc6	nezdar
<g/>
,	,	kIx,	,
korupčních	korupční	k2eAgFnPc6d1	korupční
aférách	aféra	k1gFnPc6	aféra
a	a	k8xC	a
nepopulárních	populární	k2eNgInPc6d1	nepopulární
krocích	krok	k1gInPc6	krok
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
členský	členský	k2eAgInSc1d1	členský
příspěvek	příspěvek	k1gInSc1	příspěvek
(	(	kIx(	(
<g/>
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
opětovně	opětovně	k6eAd1	opětovně
řádnými	řádný	k2eAgInPc7d1	řádný
členy	člen	k1gInPc7	člen
<g/>
)	)	kIx)	)
jen	jen	k9	jen
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
800	[number]	k4	800
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgNnSc4d1	veřejné
transformovala	transformovat	k5eAaBmAgFnS	transformovat
na	na	k7c4	na
spolek	spolek	k1gInSc4	spolek
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc4	první
předsedkyní	předsedkyně	k1gFnPc2	předsedkyně
spolku	spolek	k1gInSc2	spolek
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
si	se	k3xPyFc3	se
strana	strana	k1gFnSc1	strana
zvolila	zvolit	k5eAaPmAgFnS	zvolit
vedení	vedení	k1gNnSc4	vedení
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Kohout	Kohout	k1gMnSc1	Kohout
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
David	David	k1gMnSc1	David
Kádner	Kádner	k1gMnSc1	Kádner
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Miroslav	Miroslav	k1gMnSc1	Miroslav
Malchar	Malchar	k1gMnSc1	Malchar
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Simona	Simona	k1gFnSc1	Simona
Chytrová	Chytrový	k2eAgFnSc1d1	Chytrová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Alice	Alice	k1gFnSc1	Alice
Čečilová	Čečilový	k2eAgFnSc1d1	Čečilový
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
(	(	kIx(	(
<g/>
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Ježovica	Ježovica	k1gMnSc1	Ježovica
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Petr	Petr	k1gMnSc1	Petr
Vittek	Vittek	k1gMnSc1	Vittek
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
kooptován	kooptován	k2eAgMnSc1d1	kooptován
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Toto	tento	k3xDgNnSc1	tento
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
stranu	strana	k1gFnSc4	strana
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
transformace	transformace	k1gFnSc2	transformace
na	na	k7c4	na
spolek	spolek	k1gInSc4	spolek
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgMnPc1d1	následující
členové	člen	k1gMnPc1	člen
strany	strana	k1gFnSc2	strana
zasedali	zasedat	k5eAaImAgMnP	zasedat
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
za	za	k7c2	za
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
:	:	kIx,	:
Odvoláni	odvolat	k5eAaPmNgMnP	odvolat
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2010	[number]	k4	2010
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
odvolán	odvolán	k2eAgMnSc1d1	odvolán
po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2010	[number]	k4	2010
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
odvolán	odvolán	k2eAgMnSc1d1	odvolán
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
premiéra	premiér	k1gMnSc2	premiér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dobeš	Dobeš	k1gMnSc1	Dobeš
–	–	k?	–
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc1	výkon
funkce	funkce	k1gFnSc2	funkce
skončil	skončit	k5eAaPmAgInS	skončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
–	–	k?	–
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2010	[number]	k4	2010
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
odvolán	odvolán	k2eAgMnSc1d1	odvolán
po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Karolína	Karolína	k1gFnSc1	Karolína
Peake	Peake	k1gFnSc1	Peake
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
Legislativní	legislativní	k2eAgFnSc2d1	legislativní
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zůstala	zůstat	k5eAaPmAgFnS	zůstat
členkou	členka	k1gFnSc7	členka
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Dobeš	Dobeš	k1gMnSc1	Dobeš
–	–	k?	–
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zůstal	zůstat	k5eAaPmAgMnS	zůstat
členem	člen	k1gMnSc7	člen
vlády	vláda	k1gFnSc2	vláda
Kamil	Kamil	k1gMnSc1	Kamil
<g />
.	.	kIx.	.
</s>
<s>
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
–	–	k?	–
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zůstal	zůstat	k5eAaPmAgMnS	zůstat
členem	člen	k1gMnSc7	člen
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
2013	[number]	k4	2013
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Jana	Jan	k1gMnSc2	Jan
Drastichová	Drastichový	k2eAgFnSc1d1	Drastichová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jarolím	Jarole	k1gFnPc3	Jarole
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Kateřina	Kateřina	k1gFnSc1	Kateřina
Klasnová	Klasnová	k1gFnSc1	Klasnová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc2	místopředsedkyně
Jiří	Jiří	k1gMnSc1	Jiří
Kohout	Kohout	k1gMnSc1	Kohout
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Petr	Petr	k1gMnSc1	Petr
Skokan	Skokan	k1gMnSc1	Skokan
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
2013	[number]	k4	2013
Na	na	k7c6	na
XVI	XVI	kA	XVI
<g/>
<g />
.	.	kIx.	.
</s>
<s>
stranické	stranický	k2eAgInPc1d1	stranický
konferenci	konference	k1gFnSc6	konference
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Petra	Petra	k1gFnSc1	Petra
Quittová	Quittový	k2eAgFnSc1d1	Quittový
–	–	k?	–
výkonná	výkonný	k2eAgFnSc1d1	výkonná
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
(	(	kIx(	(
<g/>
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
<g />
.	.	kIx.	.
</s>
<s>
Drastichová	Drastichový	k2eAgFnSc1d1	Drastichová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
(	(	kIx(	(
<g/>
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Babák	Babák	k1gMnSc1	Babák
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
Skokan	Skokan	k1gMnSc1	Skokan
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Jiří	Jiří	k1gMnSc1	Jiří
Lexa	Lexa	k1gMnSc1	Lexa
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Renc	Renc	k1gFnSc1	Renc
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
<g />
.	.	kIx.	.
</s>
<s>
Kádner	Kádner	k1gMnSc1	Kádner
–	–	k?	–
místopředseda	místopředseda	k1gMnSc1	místopředseda
(	(	kIx(	(
<g/>
kooptován	kooptován	k2eAgMnSc1d1	kooptován
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
–	–	k?	–
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
(	(	kIx(	(
<g/>
kooptována	kooptován	k2eAgFnSc1d1	kooptována
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Za	za	k7c4	za
skutečného	skutečný	k2eAgMnSc4d1	skutečný
lídra	lídr	k1gMnSc4	lídr
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
médii	médium	k1gNnPc7	médium
do	do	k7c2	do
zvolení	zvolení	k1gNnSc2	zvolení
předsedou	předseda	k1gMnSc7	předseda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
považován	považován	k2eAgMnSc1d1	považován
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
účastnil	účastnit	k5eAaImAgMnS	účastnit
koaličních	koaliční	k2eAgNnPc2d1	koaliční
vyjednávání	vyjednávání	k1gNnPc2	vyjednávání
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
a	a	k8xC	a
následných	následný	k2eAgNnPc2d1	následné
koaličních	koaliční	k2eAgNnPc2d1	koaliční
jednání	jednání	k1gNnPc2	jednání
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
K9	K9	k1gFnSc2	K9
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
vládní	vládní	k2eAgFnSc4d1	vládní
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
zvolení	zvolený	k2eAgMnPc1d1	zvolený
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Andrýsová	Andrýsový	k2eAgFnSc1d1	Andrýsová
Lenka	Lenka	k1gFnSc1	Lenka
Babák	Babák	k1gMnSc1	Babák
Michal	Michal	k1gMnSc1	Michal
Bárta	Bárta	k1gMnSc1	Bárta
Vít	Vít	k1gMnSc1	Vít
Dobeš	Dobeš	k1gMnSc1	Dobeš
Josef	Josef	k1gMnSc1	Josef
Drastichová	Drastichová	k1gFnSc1	Drastichová
Jana	Jan	k1gMnSc2	Jan
Fraňková	Fraňková	k1gFnSc1	Fraňková
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
Otto	Otto	k1gMnSc1	Otto
John	John	k1gMnSc1	John
Radek	Radek	k1gMnSc1	Radek
Kádner	Kádner	k1gMnSc1	Kádner
David	David	k1gMnSc1	David
Klasnová	Klasnová	k1gFnSc1	Klasnová
Kateřina	Kateřina	k1gFnSc1	Kateřina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
Dagmar	Dagmar	k1gFnSc1	Dagmar
Novotný	Novotný	k1gMnSc1	Novotný
Josef	Josef	k1gMnSc1	Josef
Paggio	Paggio	k1gMnSc1	Paggio
Viktor	Viktor	k1gMnSc1	Viktor
Peake	Peak	k1gFnSc2	Peak
Karolína	Karolína	k1gFnSc1	Karolína
Rusnok	Rusnok	k1gInSc1	Rusnok
Jiří	Jiří	k1gMnSc1	Jiří
Skokan	Skokan	k1gMnSc1	Skokan
Petr	Petr	k1gMnSc1	Petr
Suchá	Suchá	k1gFnSc1	Suchá
Jana	Jan	k1gMnSc2	Jan
Štětina	štětina	k1gFnSc1	štětina
Jiří	Jiří	k1gMnSc1	Jiří
Šťovíček	Šťovíček	k1gMnSc1	Šťovíček
Milan	Milan	k1gMnSc1	Milan
Vacek	Vacek	k1gMnSc1	Vacek
Martin	Martin	k1gMnSc1	Martin
Vysloužil	Vysloužil	k1gMnSc1	Vysloužil
Radim	Radim	k1gMnSc1	Radim
Strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
nechává	nechávat	k5eAaImIp3nS	nechávat
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
registrované	registrovaný	k2eAgMnPc4d1	registrovaný
příznivce	příznivec	k1gMnPc4	příznivec
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
v	v	k7c6	v
internetových	internetový	k2eAgNnPc6d1	internetové
referendech	referendum	k1gNnPc6	referendum
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
hlasovat	hlasovat	k5eAaImF	hlasovat
je	on	k3xPp3gNnSc4	on
po	po	k7c4	po
registraci	registrace	k1gFnSc4	registrace
možná	možná	k9	možná
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
www.veciverejne.cz	www.veciverejne.cza	k1gFnPc2	www.veciverejne.cza
<g/>
.	.	kIx.	.
</s>
<s>
Zaregistrovaní	zaregistrovaný	k2eAgMnPc1d1	zaregistrovaný
příznivci	příznivec	k1gMnPc1	příznivec
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
hlasovat	hlasovat	k5eAaImF	hlasovat
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Véčkaři	véčkař	k1gMnPc1	véčkař
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
internetového	internetový	k2eAgNnSc2d1	internetové
referenda	referendum	k1gNnSc2	referendum
<g/>
:	:	kIx,	:
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Podporujete	podporovat	k5eAaImIp2nP	podporovat
šrotovné	šrotovný	k2eAgFnPc1d1	šrotovný
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
ANO	ano	k9	ano
25,04	[number]	k4	25,04
%	%	kIx~	%
Véčkařů	véčkař	k1gMnPc2	véčkař
<g/>
,	,	kIx,	,
NE	ne	k9	ne
74,96	[number]	k4	74,96
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
proto	proto	k8xC	proto
bude	být	k5eAaImBp3nS	být
vystupovat	vystupovat	k5eAaImF	vystupovat
proti	proti	k7c3	proti
zavedení	zavedení	k1gNnSc3	zavedení
šrotovného	šrotovné	k1gNnSc2	šrotovné
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
prodloužení	prodloužení	k1gNnSc4	prodloužení
ochranné	ochranný	k2eAgFnSc2d1	ochranná
doby	doba	k1gFnSc2	doba
autorského	autorský	k2eAgNnSc2d1	autorské
díla	dílo	k1gNnSc2	dílo
ze	z	k7c2	z
70	[number]	k4	70
let	léto	k1gNnPc2	léto
na	na	k7c4	na
90	[number]	k4	90
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
získaly	získat	k5eAaPmAgFnP	získat
22	[number]	k4	22
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
však	však	k9	však
koalice	koalice	k1gFnSc1	koalice
bez	bez	k7c2	bez
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
mezi	mezi	k7c7	mezi
ODS	ODS	kA	ODS
a	a	k8xC	a
stranami	strana	k1gFnPc7	strana
ED	ED	kA	ED
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
v	v	k7c6	v
Černoších	černoší	k2eAgInPc6d1	černoší
a	a	k8xC	a
Kostelci	Kostelec	k1gInSc6	Kostelec
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
zastupitelstvech	zastupitelstvo	k1gNnPc6	zastupitelstvo
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
dva	dva	k4xCgInPc4	dva
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
buňka	buňka	k1gFnSc1	buňka
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
účastnily	účastnit	k5eAaImAgFnP	účastnit
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gMnPc1	jejich
dva	dva	k4xCgMnPc1	dva
kandidáti	kandidát	k1gMnPc1	kandidát
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
okrscích	okrsek	k1gInPc6	okrsek
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
nebyli	být	k5eNaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
ohlásily	ohlásit	k5eAaPmAgFnP	ohlásit
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Lídrem	lídr	k1gMnSc7	lídr
kandidátní	kandidátní	k2eAgFnSc2d1	kandidátní
listiny	listina	k1gFnSc2	listina
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
Jana	Jana	k1gFnSc1	Jana
Zachová	Zachová	k1gFnSc1	Zachová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
odvolána	odvolat	k5eAaPmNgFnS	odvolat
volebním	volební	k2eAgMnSc7d1	volební
zmocněncem	zmocněnec	k1gMnSc7	zmocněnec
VV	VV	kA	VV
Ing.	ing.	kA	ing.
Jiřím	Jiří	k1gMnSc7	Jiří
Vejmelkou	Vejmelka	k1gMnSc7	Vejmelka
<g/>
.	.	kIx.	.
</s>
<s>
Lídrem	lídr	k1gMnSc7	lídr
kandidátní	kandidátní	k2eAgFnSc2d1	kandidátní
listiny	listina	k1gFnSc2	listina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
dvojka	dvojka	k1gFnSc1	dvojka
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
<g/>
,	,	kIx,	,
zastupitelka	zastupitelka	k1gFnSc1	zastupitelka
Prahy	Praha	k1gFnSc2	Praha
1	[number]	k4	1
ThDr.	ThDr.	k1gFnSc1	ThDr.
Kateřina	Kateřina	k1gFnSc1	Kateřina
Klasnová	Klasnová	k1gFnSc1	Klasnová
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
se	se	k3xPyFc4	se
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
2,40	[number]	k4	2,40
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
3,13	[number]	k4	3,13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
1,51	[number]	k4	1,51
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
museli	muset	k5eAaImAgMnP	muset
všichni	všechen	k3xTgMnPc1	všechen
kandidáti	kandidát	k1gMnPc1	kandidát
podepsat	podepsat	k5eAaPmF	podepsat
vázací	vázací	k2eAgFnSc4d1	vázací
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
ústavou	ústava	k1gFnSc7	ústava
garantovaného	garantovaný	k2eAgInSc2d1	garantovaný
volného	volný	k2eAgInSc2d1	volný
mandátu	mandát	k1gInSc2	mandát
a	a	k8xC	a
zavázali	zavázat	k5eAaPmAgMnP	zavázat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
pokuty	pokuta	k1gFnSc2	pokuta
sedm	sedm	k4xCc1	sedm
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
hlasovat	hlasovat	k5eAaImF	hlasovat
podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
stranického	stranický	k2eAgNnSc2d1	stranické
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgMnSc1d1	ústavní
právník	právník	k1gMnSc1	právník
Kysela	Kysela	k1gMnSc1	Kysela
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
označil	označit	k5eAaPmAgInS	označit
tyto	tento	k3xDgFnPc4	tento
smlouvy	smlouva	k1gFnPc4	smlouva
za	za	k7c4	za
protiústavní	protiústavní	k2eAgFnPc4d1	protiústavní
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
získala	získat	k5eAaPmAgFnS	získat
10,88	[number]	k4	10,88
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
i	i	k8xC	i
strany	strana	k1gFnSc2	strana
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Škárka	škárka	k1gFnSc1	škárka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nezařazeným	zařazený	k2eNgMnSc7d1	nezařazený
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
disponuje	disponovat	k5eAaBmIp3nS	disponovat
23	[number]	k4	23
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidovali	kandidovat	k5eAaImAgMnP	kandidovat
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
hnutí	hnutí	k1gNnSc2	hnutí
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
Tomio	Tomio	k6eAd1	Tomio
Okamury	Okamura	k1gFnSc2	Okamura
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
poslanci	poslanec	k1gMnPc1	poslanec
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
<g/>
:	:	kIx,	:
za	za	k7c4	za
Jihočeský	jihočeský	k2eAgInSc4d1	jihočeský
kraj	kraj	k1gInSc4	kraj
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
za	za	k7c4	za
Ústecký	ústecký	k2eAgInSc4d1	ústecký
kraj	kraj	k1gInSc4	kraj
-	-	kIx~	-
David	David	k1gMnSc1	David
Kádner	Kádner	k1gMnSc1	Kádner
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj-	kraj-	k?	kraj-
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
.	.	kIx.	.
</s>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rovněž	rovněž	k9	rovněž
MUDr.	MUDr.	kA	MUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Štětina	štětina	k1gFnSc1	štětina
za	za	k7c4	za
Královehradecký	královehradecký	k2eAgInSc4d1	královehradecký
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
exposlanec	exposlanec	k1gMnSc1	exposlanec
VV	VV	kA	VV
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
Úsvitu	úsvit	k1gInSc2	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
období	období	k1gNnSc4	období
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
získaly	získat	k5eAaPmAgFnP	získat
VV	VV	kA	VV
celkem	celek	k1gInSc7	celek
6	[number]	k4	6
988	[number]	k4	988
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
0,46	[number]	k4	0,46
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
angažovaných	angažovaný	k2eAgMnPc2d1	angažovaný
podnikatelů	podnikatel	k1gMnPc2	podnikatel
Lukáš	Lukáš	k1gMnSc1	Lukáš
Semerák	Semerák	k1gMnSc1	Semerák
<g/>
,	,	kIx,	,
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
a	a	k8xC	a
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
S.	S.	kA	S.
<g/>
P.	P.	kA	P.
<g/>
I.	I.	kA	I.
Holdings	Holdingsa	k1gFnPc2	Holdingsa
Michal	Michal	k1gMnSc1	Michal
Babák	Babák	k1gMnSc1	Babák
<g/>
,	,	kIx,	,
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
majoritní	majoritní	k2eAgMnSc1d1	majoritní
vlastník	vlastník	k1gMnSc1	vlastník
a	a	k8xC	a
jednatel	jednatel	k1gMnSc1	jednatel
MxB	MxB	k1gFnSc2	MxB
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
VV	VV	kA	VV
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
<g />
.	.	kIx.	.
</s>
<s>
kraji	kraj	k1gInSc6	kraj
Kamil	Kamil	k1gMnSc1	Kamil
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
<g/>
,	,	kIx,	,
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Phar-service	Pharervice	k1gFnSc2	Phar-service
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
nezvolen	zvolen	k2eNgInSc4d1	nezvolen
za	za	k7c4	za
VV	VV	kA	VV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
ABL	ABL	kA	ABL
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
VV	VV	kA	VV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
oznámil	oznámit	k5eAaPmAgMnS	oznámit
podnikatel	podnikatel	k1gMnSc1	podnikatel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bakala	Bakal	k1gMnSc2	Bakal
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
věnoval	věnovat	k5eAaImAgMnS	věnovat
politické	politický	k2eAgFnSc3d1	politická
straně	strana	k1gFnSc3	strana
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
nepřímo	přímo	k6eNd1	přímo
sponzorovaná	sponzorovaný	k2eAgFnSc1d1	sponzorovaná
zadáváním	zadávání	k1gNnSc7	zadávání
drahé	drahá	k1gFnSc2	drahá
inzerce	inzerce	k1gFnSc2	inzerce
firmou	firma	k1gFnSc7	firma
ABL	ABL	kA	ABL
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
"	"	kIx"	"
<g/>
VĚCI	věc	k1gFnSc6	věc
VEŘEJNÉ	veřejný	k2eAgFnSc6d1	veřejná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vrátila	vrátit	k5eAaPmAgFnS	vrátit
šest	šest	k4xCc4	šest
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
volební	volební	k2eAgFnSc4d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
od	od	k7c2	od
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Bakaly	Bakal	k1gMnPc4	Bakal
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
předala	předat	k5eAaPmAgFnS	předat
sdružení	sdružení	k1gNnSc4	sdružení
nájemníků	nájemník	k1gMnPc2	nájemník
BYTYOKD	BYTYOKD	kA	BYTYOKD
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
zastupujícího	zastupující	k2eAgInSc2d1	zastupující
nájemníky	nájemník	k1gMnPc4	nájemník
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
koupi	koupě	k1gFnSc4	koupě
bývalých	bývalý	k2eAgInPc2d1	bývalý
bytů	byt	k1gInPc2	byt
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nyní	nyní	k6eAd1	nyní
vlastní	vlastní	k2eAgFnSc1d1	vlastní
společnost	společnost	k1gFnSc1	společnost
RPG	RPG	kA	RPG
Byty	byt	k1gInPc1	byt
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
Bakalou	Bakala	k1gFnSc7	Bakala
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zaorálka	Zaorálek	k1gMnSc2	Zaorálek
je	být	k5eAaImIp3nS	být
absurdní	absurdní	k2eAgFnSc1d1	absurdní
<g/>
,	,	kIx,	,
že	že	k8xS	že
strana	strana	k1gFnSc1	strana
Bakalu	Bakal	k1gMnSc3	Bakal
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nájemníky	nájemník	k1gMnPc4	nájemník
bytů	byt	k1gInPc2	byt
OKD	OKD	kA	OKD
obelhal	obelhat	k5eAaPmAgInS	obelhat
slibem	slib	k1gInSc7	slib
o	o	k7c4	o
umožnění	umožnění	k1gNnSc4	umožnění
odkoupit	odkoupit	k5eAaPmF	odkoupit
za	za	k7c4	za
netržní	tržní	k2eNgFnSc4d1	netržní
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
dává	dávat	k5eAaImIp3nS	dávat
financovat	financovat	k5eAaBmF	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
svého	svůj	k3xOyFgMnSc2	svůj
věřitele	věřitel	k1gMnSc2	věřitel
<g/>
,	,	kIx,	,
mediální	mediální	k2eAgFnSc2d1	mediální
agentury	agentura	k1gFnSc2	agentura
MÉDEA	Médea	k1gFnSc1	Médea
podnikatele	podnikatel	k1gMnSc4	podnikatel
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Soukupa	Soukup	k1gMnSc4	Soukup
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
insolvenčního	insolvenční	k2eAgNnSc2d1	insolvenční
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
následně	následně	k6eAd1	následně
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věřitel	věřitel	k1gMnSc1	věřitel
vzal	vzít	k5eAaPmAgMnS	vzít
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Respekt	respekt	k1gInSc1	respekt
vyšel	vyjít	k5eAaPmAgInS	vyjít
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc4	jenž
výkonný	výkonný	k2eAgMnSc1d1	výkonný
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Škárka	škárka	k1gFnSc1	škárka
redaktorům	redaktor	k1gMnPc3	redaktor
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravidelně	pravidelně	k6eAd1	pravidelně
bere	brát	k5eAaImIp3nS	brát
od	od	k7c2	od
Víta	Vít	k1gMnSc2	Vít
Bárty	Bárta	k1gMnPc4	Bárta
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
loajalitu	loajalita	k1gFnSc4	loajalita
a	a	k8xC	a
mlčení	mlčení	k1gNnSc4	mlčení
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
Škárka	škárka	k1gFnSc1	škárka
i	i	k8xC	i
Bárta	Bárta	k1gMnSc1	Bárta
zprvu	zprvu	k6eAd1	zprvu
vnitrostranickou	vnitrostranický	k2eAgFnSc4d1	vnitrostranická
korupci	korupce	k1gFnSc4	korupce
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
;	;	kIx,	;
Škárka	škárka	k1gFnSc1	škárka
ovšem	ovšem	k9	ovšem
zanedlouho	zanedlouho	k6eAd1	zanedlouho
verzi	verze	k1gFnSc4	verze
Respektu	respekt	k1gInSc2	respekt
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgInS	podat
na	na	k7c4	na
Víta	Vít	k1gMnSc4	Vít
Bártu	Bárta	k1gMnSc4	Bárta
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgInS	učinit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
z	z	k7c2	z
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
strany	strana	k1gFnSc2	strana
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Víta	Vít	k1gMnSc2	Vít
Bárty	Bárta	k1gMnPc4	Bárta
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
mlčenlivost	mlčenlivost	k1gFnSc4	mlčenlivost
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
financování	financování	k1gNnSc2	financování
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
půjčku	půjčka	k1gFnSc4	půjčka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
poslanci	poslanec	k1gMnPc1	poslanec
strany	strana	k1gFnSc2	strana
pak	pak	k6eAd1	pak
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
neformální	formální	k2eNgFnPc1d1	neformální
lídr	lídr	k1gMnSc1	lídr
strany	strana	k1gFnSc2	strana
podobné	podobný	k2eAgFnSc2d1	podobná
půjčky	půjčka	k1gFnSc2	půjčka
také	také	k9	také
nabízel	nabízet	k5eAaImAgInS	nabízet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
finanční	finanční	k2eAgFnSc4d1	finanční
kompenzaci	kompenzace	k1gFnSc4	kompenzace
za	za	k7c7	za
jimi	on	k3xPp3gFnPc7	on
odhlasované	odhlasovaný	k2eAgNnSc1d1	odhlasované
citelné	citelný	k2eAgNnSc1d1	citelné
snížení	snížení	k1gNnSc1	snížení
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
platů	plat	k1gInPc2	plat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
kauza	kauza	k1gFnSc1	kauza
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
další	další	k2eAgMnPc4d1	další
poslance	poslanec	k1gMnPc4	poslanec
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
Kristýna	Kristýna	k1gFnSc1	Kristýna
Kočí	Kočí	k1gFnSc1	Kočí
podala	podat	k5eAaPmAgFnS	podat
kvůli	kvůli	k7c3	kvůli
podobnému	podobný	k2eAgInSc3d1	podobný
činu	čin	k1gInSc3	čin
na	na	k7c4	na
Bártu	Bárta	k1gMnSc4	Bárta
též	též	k9	též
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
a	a	k8xC	a
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
z	z	k7c2	z
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
i	i	k8xC	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc3	on
šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
případě	případ	k1gInSc6	případ
o	o	k7c4	o
půlmilionový	půlmilionový	k2eAgInSc4d1	půlmilionový
úplatek	úplatek	k1gInSc4	úplatek
za	za	k7c4	za
loajalitu	loajalita	k1gFnSc4	loajalita
Bártovi	Bárta	k1gMnSc3	Bárta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dala	dát	k5eAaPmAgFnS	dát
ihned	ihned	k6eAd1	ihned
do	do	k7c2	do
notářské	notářský	k2eAgFnSc2d1	notářská
úschovy	úschova	k1gFnSc2	úschova
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
Radek	Radka	k1gFnPc2	Radka
John	John	k1gMnSc1	John
ale	ale	k9	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc4	tento
peníze	peníz	k1gInPc4	peníz
půjčila	půjčit	k5eAaPmAgFnS	půjčit
na	na	k7c4	na
kabelky	kabelka	k1gFnPc4	kabelka
a	a	k8xC	a
pak	pak	k6eAd1	pak
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
udělala	udělat	k5eAaPmAgFnS	udělat
úplatek	úplatek	k1gInSc4	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
Poslanec	poslanec	k1gMnSc1	poslanec
Stanislav	Stanislav	k1gMnSc1	Stanislav
Huml	Huml	k1gMnSc1	Huml
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
odešel	odejít	k5eAaPmAgMnS	odejít
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Věcmi	věc	k1gFnPc7	věc
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
již	již	k6eAd1	již
nechce	chtít	k5eNaImIp3nS	chtít
mít	mít	k5eAaImF	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
jej	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
i	i	k9	i
tak	tak	k6eAd1	tak
formálně	formálně	k6eAd1	formálně
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Výhrady	výhrada	k1gFnPc4	výhrada
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
poslankyně	poslankyně	k1gFnSc1	poslankyně
Jana	Jana	k1gFnSc1	Jana
Suchá	Suchá	k1gFnSc1	Suchá
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
chce	chtít	k5eAaImIp3nS	chtít
v	v	k7c6	v
poslaneckém	poslanecký	k2eAgInSc6d1	poslanecký
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
dopravy	doprava	k1gFnSc2	doprava
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
se	s	k7c7	s
sdělením	sdělení	k1gNnSc7	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
zatěžovat	zatěžovat	k5eAaImF	zatěžovat
reformní	reformní	k2eAgFnSc4d1	reformní
činnost	činnost	k1gFnSc4	činnost
vlády	vláda	k1gFnSc2	vláda
svou	svůj	k3xOyFgFnSc7	svůj
kauzou	kauza	k1gFnSc7	kauza
a	a	k8xC	a
hodlá	hodlat	k5eAaImIp3nS	hodlat
všechny	všechen	k3xTgFnPc4	všechen
nejasnosti	nejasnost	k1gFnPc4	nejasnost
okolo	okolo	k7c2	okolo
financování	financování	k1gNnSc2	financování
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
kritiku	kritika	k1gFnSc4	kritika
jasně	jasně	k6eAd1	jasně
objasnit	objasnit	k5eAaPmF	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Kandidatury	kandidatura	k1gFnPc1	kandidatura
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
nevzdal	vzdát	k5eNaPmAgInS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Bártovi	Bárta	k1gMnSc3	Bárta
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
obvinění	obvinění	k1gNnSc2	obvinění
až	až	k6eAd1	až
19.12	[number]	k4	19.12
<g/>
.2012	.2012	k4	.2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
5	[number]	k4	5
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc4	peníz
byly	být	k5eAaImAgInP	být
úplatky	úplatek	k1gInPc1	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
dovolání	dovolání	k1gNnSc4	dovolání
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
proti	proti	k7c3	proti
dřívějšímu	dřívější	k2eAgInSc3d1	dřívější
rozsudku	rozsudek	k1gInSc3	rozsudek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byli	být	k5eAaImAgMnP	být
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
i	i	k8xC	i
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Škárka	škárka	k1gFnSc1	škárka
definitivně	definitivně	k6eAd1	definitivně
zproštěni	zprostit	k5eAaPmNgMnP	zprostit
všech	všecek	k3xTgNnPc2	všecek
obvinění	obvinění	k1gNnSc2	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
aférou	aféra	k1gFnSc7	aféra
strany	strana	k1gFnSc2	strana
probíhající	probíhající	k2eAgFnSc2d1	probíhající
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zjistil	zjistit	k5eAaPmAgInS	zjistit
časopis	časopis	k1gInSc1	časopis
Respekt	respekt	k1gInSc1	respekt
<g/>
,	,	kIx,	,
že	že	k8xS	že
obnos	obnos	k1gInSc1	obnos
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Michal	Michal	k1gMnSc1	Michal
Babák	Babák	k1gMnSc1	Babák
daroval	darovat	k5eAaPmAgMnS	darovat
Věcem	věc	k1gFnPc3	věc
veřejným	veřejný	k2eAgFnPc3d1	veřejná
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
WCM	WCM	kA	WCM
CZ	CZ	kA	CZ
(	(	kIx(	(
<g/>
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
figuruje	figurovat	k5eAaImIp3nS	figurovat
coby	coby	k?	coby
jednatel	jednatel	k1gMnSc1	jednatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
splácí	splácet	k5eAaImIp3nP	splácet
jí	on	k3xPp3gFnSc3	on
je	on	k3xPp3gFnPc4	on
z	z	k7c2	z
výtěžku	výtěžek	k1gInSc2	výtěžek
prodeje	prodej	k1gInSc2	prodej
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
jméno	jméno	k1gNnSc4	jméno
si	se	k3xPyFc3	se
údajně	údajně	k6eAd1	údajně
nevzpomněl	vzpomnít	k5eNaPmAgMnS	vzpomnít
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kauze	kauza	k1gFnSc6	kauza
figuroval	figurovat	k5eAaImAgMnS	figurovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
prostředník	prostředník	k1gInSc1	prostředník
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
zůstat	zůstat	k5eAaPmF	zůstat
veřejnosti	veřejnost	k1gFnPc4	veřejnost
neznám	neznámo	k1gNnPc2	neznámo
<g/>
,	,	kIx,	,
Babák	Babák	k1gMnSc1	Babák
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přiznání	přiznání	k1gNnSc6	přiznání
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
příjmů	příjem	k1gInPc2	příjem
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
Babák	Babák	k1gInSc1	Babák
přiznal	přiznat	k5eAaPmAgInS	přiznat
pouze	pouze	k6eAd1	pouze
měsíční	měsíční	k2eAgInSc1d1	měsíční
příjem	příjem	k1gInSc1	příjem
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
tří	tři	k4xCgFnPc2	tři
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	(
<g/>
zatížených	zatížený	k2eAgFnPc2d1	zatížená
hypotékou	hypotéka	k1gFnSc7	hypotéka
<g/>
)	)	kIx)	)
a	a	k8xC	a
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
svých	svůj	k3xOyFgInPc2	svůj
podílů	podíl	k1gInPc2	podíl
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
MB	MB	kA	MB
consulting	consulting	k1gInSc4	consulting
a	a	k8xC	a
Dark	Dark	k1gInSc4	Dark
dog	doga	k1gFnPc2	doga
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
získal	získat	k5eAaPmAgInS	získat
50	[number]	k4	50
000	[number]	k4	000
resp.	resp.	kA	resp.
40	[number]	k4	40
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
že	že	k8xS	že
materiály	materiál	k1gInPc4	materiál
k	k	k7c3	k
Babákovu	Babákův	k2eAgNnSc3d1	Babákovo
financování	financování	k1gNnSc3	financování
má	mít	k5eAaImIp3nS	mít
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
tašce	taška	k1gFnSc6	taška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
je	být	k5eAaImIp3nS	být
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Babák	Babák	k1gMnSc1	Babák
ve	v	k7c4	v
vyjádření	vyjádření	k1gNnSc4	vyjádření
deníku	deník	k1gInSc2	deník
Právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
že	že	k8xS	že
650	[number]	k4	650
000	[number]	k4	000
Kč	Kč	kA	Kč
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Věcem	věc	k1gFnPc3	věc
veřejným	veřejný	k2eAgFnPc3d1	veřejná
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
soukromých	soukromý	k2eAgInPc2d1	soukromý
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
firmy	firma	k1gFnSc2	firma
MxB	MxB	k1gFnPc2	MxB
a	a	k8xC	a
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
WCM	WCM	kA	WCM
CZ	CZ	kA	CZ
<g/>
;	;	kIx,	;
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
dluhy	dluh	k1gInPc4	dluh
chce	chtít	k5eAaImIp3nS	chtít
splatit	splatit	k5eAaPmF	splatit
prodejem	prodej	k1gInSc7	prodej
15	[number]	k4	15
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
společnosti	společnost	k1gFnSc2	společnost
Bene	bene	k6eAd1	bene
Factum	Factum	k1gNnSc1	Factum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
v	v	k7c6	v
letošním	letošní	k2eAgInSc6d1	letošní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
těchto	tento	k3xDgFnPc2	tento
akcií	akcie	k1gFnPc2	akcie
Babák	Babák	k1gMnSc1	Babák
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
prohlášení	prohlášení	k1gNnSc2	prohlášení
dílem	dílem	k6eAd1	dílem
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neznal	neznat	k5eAaImAgInS	neznat
jejich	jejich	k3xOp3gFnSc4	jejich
přesnou	přesný	k2eAgFnSc4d1	přesná
aktuální	aktuální	k2eAgFnSc4d1	aktuální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
dílem	díl	k1gInSc7	díl
protože	protože	k8xS	protože
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gInPc1	jeho
majetkové	majetkový	k2eAgInPc1d1	majetkový
poměry	poměr	k1gInPc1	poměr
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jeho	jeho	k3xOp3gNnSc2	jeho
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
veřejně	veřejně	k6eAd1	veřejně
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
však	však	k9	však
po	po	k7c4	po
50	[number]	k4	50
%	%	kIx~	%
společnosti	společnost	k1gFnSc6	společnost
Bene	bene	k6eAd1	bene
Factum	Factum	k1gNnSc1	Factum
patří	patřit	k5eAaImIp3nS	patřit
Miloši	Miloš	k1gMnSc3	Miloš
Havránkovi	Havránek	k1gMnSc3	Havránek
a	a	k8xC	a
Petru	Petr	k1gMnSc3	Petr
Šrámkovi	Šrámek	k1gMnSc3	Šrámek
<g/>
.	.	kIx.	.
</s>
<s>
Poslanec	poslanec	k1gMnSc1	poslanec
Babák	Babák	k1gMnSc1	Babák
existenci	existence	k1gFnSc4	existence
svého	svůj	k3xOyFgInSc2	svůj
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Bene	bene	k6eAd1	bene
factum	factum	k1gNnSc4	factum
nedoložil	doložit	k5eNaPmAgMnS	doložit
<g/>
,	,	kIx,	,
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
obchodní	obchodní	k2eAgNnSc4d1	obchodní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
tvrzení	tvrzení	k1gNnSc4	tvrzení
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
tiskovým	tiskový	k2eAgNnSc7d1	tiskové
prohlášením	prohlášení	k1gNnSc7	prohlášení
společnosti	společnost	k1gFnSc2	společnost
Bene	bene	k6eAd1	bene
factum	factum	k1gNnSc1	factum
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvy	smlouva	k1gFnPc1	smlouva
dokládající	dokládající	k2eAgFnPc1d1	dokládající
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c4	na
sponzoring	sponzoring	k1gInSc4	sponzoring
strany	strana	k1gFnSc2	strana
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
od	od	k7c2	od
společností	společnost	k1gFnPc2	společnost
WCM	WCM	kA	WCM
CZ	CZ	kA	CZ
a	a	k8xC	a
MxB	MxB	k1gMnPc1	MxB
jsou	být	k5eAaImIp3nP	být
podepsány	podepsán	k2eAgInPc4d1	podepsán
pouze	pouze	k6eAd1	pouze
Babákem	Babák	k1gInSc7	Babák
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
jednatelem	jednatel	k1gMnSc7	jednatel
první	první	k4xOgFnSc6	první
a	a	k8xC	a
spoluvlastníkem	spoluvlastník	k1gMnSc7	spoluvlastník
druhé	druhý	k4xOgFnSc2	druhý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
možnost	možnost	k1gFnSc4	možnost
že	že	k8xS	že
smlouvy	smlouva	k1gFnPc4	smlouva
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
až	až	k6eAd1	až
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
,	,	kIx,	,
a	a	k8xC	a
Babák	Babák	k1gMnSc1	Babák
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
zástupcům	zástupce	k1gMnPc3	zástupce
tisku	tisk	k1gInSc2	tisk
účetní	účetní	k2eAgFnSc2d1	účetní
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
data	datum	k1gNnPc4	datum
zaznamenána	zaznamenán	k2eAgNnPc4d1	zaznamenáno
<g/>
,	,	kIx,	,
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
obchodní	obchodní	k2eAgNnSc4d1	obchodní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
půjček	půjčka	k1gFnPc2	půjčka
dokládá	dokládat	k5eAaImIp3nS	dokládat
výpisy	výpis	k1gInPc4	výpis
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
bankovního	bankovní	k2eAgInSc2d1	bankovní
účtu	účet	k1gInSc2	účet
<g/>
,	,	kIx,	,
a	a	k8xC	a
existenci	existence	k1gFnSc6	existence
3,5	[number]	k4	3,5
milionové	milionový	k2eAgFnSc2d1	milionová
půjčky	půjčka	k1gFnSc2	půjčka
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
WCM	WCM	kA	WCM
CZ	CZ	kA	CZ
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
minoritní	minoritní	k2eAgMnSc1d1	minoritní
vlastník	vlastník	k1gMnSc1	vlastník
WCM	WCM	kA	WCM
CZ	CZ	kA	CZ
Josef	Josef	k1gMnSc1	Josef
Liška	Liška	k1gMnSc1	Liška
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
výnos	výnos	k1gInSc1	výnos
(	(	kIx(	(
<g/>
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
svého	svůj	k3xOyFgInSc2	svůj
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Bene	bene	k6eAd1	bene
factum	factum	k1gNnSc1	factum
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
poslanec	poslanec	k1gMnSc1	poslanec
Babák	Babák	k1gInSc1	Babák
jejím	její	k3xOp3gInSc7	její
obratem	obrat	k1gInSc7	obrat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
poslední	poslední	k2eAgFnSc2d1	poslední
dostupné	dostupný	k2eAgFnSc2d1	dostupná
účetní	účetní	k2eAgFnSc2d1	účetní
uzávěrky	uzávěrka	k1gFnSc2	uzávěrka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
však	však	k9	však
byla	být	k5eAaImAgFnS	být
účetní	účetní	k2eAgFnSc1d1	účetní
hodnota	hodnota	k1gFnSc1	hodnota
celé	celý	k2eAgFnSc2d1	celá
společnosti	společnost	k1gFnSc2	společnost
1,363	[number]	k4	1,363
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
mírnou	mírný	k2eAgFnSc4d1	mírná
ztrátu	ztráta	k1gFnSc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Převody	převod	k1gInPc1	převod
peněžních	peněžní	k2eAgFnPc2d1	peněžní
sum	suma	k1gFnPc2	suma
na	na	k7c4	na
Babákův	Babákův	k2eAgInSc4d1	Babákův
účet	účet	k1gInSc4	účet
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zčásti	zčásti	k6eAd1	zčásti
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
<g/>
,	,	kIx,	,
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
podezření	podezření	k1gNnSc4	podezření
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
své	svůj	k3xOyFgFnPc4	svůj
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
původu	původ	k1gInSc6	původ
peněz	peníze	k1gInPc2	peníze
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Finančně	finančně	k6eAd1	finančně
analytickému	analytický	k2eAgInSc3d1	analytický
útvaru	útvar	k1gInSc3	útvar
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
zahájil	zahájit	k5eAaPmAgInS	zahájit
prověřování	prověřování	k1gNnSc4	prověřování
transakcí	transakce	k1gFnPc2	transakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
aférou	aféra	k1gFnSc7	aféra
financování	financování	k1gNnSc2	financování
poslanců	poslanec	k1gMnPc2	poslanec
strany	strana	k1gFnSc2	strana
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příspěvky	příspěvek	k1gInPc1	příspěvek
Lukáše	Lukáš	k1gMnSc2	Lukáš
Vícha	Vích	k1gMnSc2	Vích
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dle	dle	k7c2	dle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
prohlášení	prohlášení	k1gNnSc2	prohlášení
daroval	darovat	k5eAaPmAgMnS	darovat
straně	strana	k1gFnSc6	strana
27	[number]	k4	27
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
výročních	výroční	k2eAgFnPc6d1	výroční
zprávách	zpráva	k1gFnPc6	zpráva
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
netransparentnost	netransparentnost	k1gFnSc4	netransparentnost
jejího	její	k3xOp3gNnSc2	její
financování	financování	k1gNnSc2	financování
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
týdeníkem	týdeník	k1gInSc7	týdeník
Respekt	respekt	k1gInSc4	respekt
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
smlouvách	smlouva	k1gFnPc6	smlouva
s	s	k7c7	s
finančními	finanční	k2eAgMnPc7d1	finanční
přispěvateli	přispěvatel	k1gMnPc7	přispěvatel
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
podpis	podpis	k1gInSc1	podpis
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
prvního	první	k4xOgMnSc2	první
místopředsedy	místopředseda	k1gMnSc2	místopředseda
strany	strana	k1gFnSc2	strana
J.	J.	kA	J.
Škárky	škárka	k1gFnSc2	škárka
zfalšován	zfalšovat	k5eAaPmNgMnS	zfalšovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
odborným	odborný	k2eAgInSc7d1	odborný
grafologickým	grafologický	k2eAgInSc7d1	grafologický
posudkem	posudek	k1gInSc7	posudek
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc4	věc
veřejné	veřejný	k2eAgFnPc4d1	veřejná
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
záležitosti	záležitost	k1gFnSc3	záležitost
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
Škárky	škárka	k1gFnSc2	škárka
(	(	kIx(	(
<g/>
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vyloučeného	vyloučený	k2eAgMnSc2d1	vyloučený
protože	protože	k8xS	protože
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Víta	Vít	k1gMnSc4	Vít
Bártu	Bárta	k1gMnSc4	Bárta
z	z	k7c2	z
uplácení	uplácení	k1gNnSc2	uplácení
za	za	k7c4	za
mlčení	mlčení	k1gNnSc4	mlčení
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ještě	ještě	k6eAd1	ještě
coby	coby	k?	coby
místopředseda	místopředseda	k1gMnSc1	místopředseda
správnost	správnost	k1gFnSc4	správnost
dokumentace	dokumentace	k1gFnSc2	dokumentace
souhrnně	souhrnně	k6eAd1	souhrnně
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
svým	svůj	k3xOyFgInSc7	svůj
podpisem	podpis	k1gInSc7	podpis
ve	v	k7c6	v
výroční	výroční	k2eAgFnSc6d1	výroční
zprávě	zpráva	k1gFnSc6	zpráva
VV	VV	kA	VV
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
sponzorů	sponzor	k1gMnPc2	sponzor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
povinnou	povinný	k2eAgFnSc7d1	povinná
součástí	součást	k1gFnSc7	součást
výroční	výroční	k2eAgFnSc2d1	výroční
zprávy	zpráva	k1gFnSc2	zpráva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
dary	dar	k1gInPc1	dar
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desetitisíců	desetitisíce	k1gInPc2	desetitisíce
korun	koruna	k1gFnPc2	koruna
od	od	k7c2	od
Vratislava	Vratislav	k1gMnSc2	Vratislav
Vařejky	vařejka	k1gFnSc2	vařejka
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
asistenta	asistent	k1gMnSc2	asistent
poslance	poslanec	k1gMnSc2	poslanec
VV	VV	kA	VV
Otto	Otto	k1gMnSc1	Otto
Chaloupky	Chaloupka	k1gMnSc2	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ročním	roční	k2eAgNnSc6d1	roční
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
protikorupční	protikorupční	k2eAgFnSc7d1	protikorupční
policií	policie	k1gFnSc7	policie
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
že	že	k8xS	že
financování	financování	k1gNnSc1	financování
VV	VV	kA	VV
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
některé	některý	k3yIgFnPc4	některý
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
v	v	k7c6	v
účetnictví	účetnictví	k1gNnSc6	účetnictví
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
spáchání	spáchání	k1gNnSc4	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
či	či	k8xC	či
praní	praní	k1gNnSc1	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
nezjistila	zjistit	k5eNaPmAgFnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Protikorupční	protikorupční	k2eAgInSc1d1	protikorupční
útvar	útvar	k1gInSc1	útvar
provedl	provést	k5eAaPmAgInS	provést
výslechy	výslech	k1gInPc4	výslech
mnoha	mnoho	k4c2	mnoho
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
zadal	zadat	k5eAaPmAgInS	zadat
znalecký	znalecký	k2eAgInSc4d1	znalecký
posudek	posudek	k1gInSc4	posudek
a	a	k8xC	a
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
forenzního	forenzní	k2eAgInSc2d1	forenzní
auditu	audit	k1gInSc2	audit
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Babák	Babák	k1gMnSc1	Babák
navíc	navíc	k6eAd1	navíc
nechal	nechat	k5eAaPmAgMnS	nechat
provést	provést	k5eAaPmF	provést
audit	audit	k1gInSc4	audit
účetnictví	účetnictví	k1gNnSc2	účetnictví
vlastních	vlastní	k2eAgFnPc2d1	vlastní
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
i	i	k8xC	i
VV	VV	kA	VV
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mark	Mark	k1gMnSc1	Mark
<g/>
2	[number]	k4	2
Corporation	Corporation	k1gInSc1	Corporation
Czech	Czech	k1gInSc4	Czech
<g/>
#	#	kIx~	#
<g/>
Strategie	strategie	k1gFnSc1	strategie
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
dokument	dokument	k1gInSc1	dokument
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
jeho	jeho	k3xOp3gMnSc1	jeho
údajný	údajný	k2eAgMnSc1d1	údajný
původce	původce	k1gMnSc1	původce
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
faktický	faktický	k2eAgMnSc1d1	faktický
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
činitel	činitel	k1gMnSc1	činitel
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
<g/>
,	,	kIx,	,
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
koncepci	koncepce	k1gFnSc4	koncepce
navýšení	navýšení	k1gNnSc2	navýšení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
moci	moc	k1gFnSc2	moc
společnosti	společnost	k1gFnSc2	společnost
ABL	ABL	kA	ABL
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
do	do	k7c2	do
r.	r.	kA	r.
2014	[number]	k4	2014
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zvýšení	zvýšení	k1gNnSc2	zvýšení
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgInPc2d1	veřejný
(	(	kIx(	(
<g/>
koalice	koalice	k1gFnSc1	koalice
s	s	k7c7	s
ODS	ODS	kA	ODS
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
hl.	hl.	k?	hl.
<g/>
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
posléze	posléze	k6eAd1	posléze
plynoucího	plynoucí	k2eAgNnSc2d1	plynoucí
navýšení	navýšení	k1gNnSc2	navýšení
zakázek	zakázka	k1gFnPc2	zakázka
od	od	k7c2	od
veřejné	veřejný	k2eAgFnSc2d1	veřejná
sféry	sféra	k1gFnSc2	sféra
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
elit	elita	k1gFnPc2	elita
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bárta	Bárta	k1gMnSc1	Bárta
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
vedení	vedení	k1gNnSc4	vedení
agentury	agentura	k1gFnSc2	agentura
ABL	ABL	kA	ABL
už	už	k6eAd1	už
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
neměl	mít	k5eNaImAgMnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kritice	kritika	k1gFnSc6	kritika
kvůli	kvůli	k7c3	kvůli
střetu	střet	k1gInSc3	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
také	také	k6eAd1	také
prodal	prodat	k5eAaPmAgInS	prodat
svou	svůj	k3xOyFgFnSc4	svůj
polovinu	polovina	k1gFnSc4	polovina
této	tento	k3xDgFnSc2	tento
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
mezi	mezi	k7c7	mezi
voliči	volič	k1gMnPc7	volič
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Nečasovy	Nečasův	k2eAgFnSc2d1	Nečasova
vlády	vláda	k1gFnSc2	vláda
začaly	začít	k5eAaPmAgFnP	začít
volební	volební	k2eAgFnPc1d1	volební
preference	preference	k1gFnPc1	preference
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
pod	pod	k7c4	pod
pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
u	u	k7c2	u
agentury	agentura	k1gFnSc2	agentura
Factum	Factum	k1gNnSc1	Factum
Invenio	Invenio	k1gNnSc1	Invenio
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
by	by	kYmCp3nS	by
strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
spadla	spadnout	k5eAaPmAgFnS	spadnout
v	v	k7c6	v
preferencích	preference	k1gFnPc6	preference
na	na	k7c6	na
historické	historický	k2eAgNnSc4d1	historické
minimum	minimum	k1gNnSc4	minimum
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
pouhé	pouhý	k2eAgFnPc4d1	pouhá
1,4	[number]	k4	1,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ještě	ještě	k9	ještě
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
kauzou	kauza	k1gFnSc7	kauza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
špičky	špička	k1gFnSc2	špička
VV	VV	kA	VV
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
oznámily	oznámit	k5eAaPmAgInP	oznámit
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
zahájily	zahájit	k5eAaPmAgFnP	zahájit
jednání	jednání	k1gNnPc1	jednání
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
tím	ten	k3xDgNnSc7	ten
velice	velice	k6eAd1	velice
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přiklonit	přiklonit	k5eAaPmF	přiklonit
ani	ani	k8xC	ani
k	k	k7c3	k
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozkoly	rozkol	k1gInPc1	rozkol
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vygradovaly	vygradovat	k5eAaPmAgFnP	vygradovat
až	až	k9	až
ve	v	k7c4	v
faktickou	faktický	k2eAgFnSc4d1	faktická
sebedestrukci	sebedestrukce	k1gFnSc4	sebedestrukce
původně	původně	k6eAd1	původně
jednotného	jednotný	k2eAgNnSc2d1	jednotné
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
i	i	k8xC	i
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Víta	Vít	k1gMnSc2	Vít
Bárty	Bárta	k1gMnSc2	Bárta
premiér	premiér	k1gMnSc1	premiér
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
odvolání	odvolání	k1gNnSc1	odvolání
dvou	dva	k4xCgMnPc2	dva
ministrů	ministr	k1gMnPc2	ministr
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Radka	Radek	k1gMnSc2	Radek
Johna	John	k1gMnSc2	John
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Dobeše	Dobeš	k1gMnSc2	Dobeš
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
demisi	demise	k1gFnSc4	demise
ministra	ministr	k1gMnSc2	ministr
Bárty	Bárta	k1gMnSc2	Bárta
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
setrvání	setrvání	k1gNnSc1	setrvání
na	na	k7c6	na
trojkoaličním	trojkoaliční	k2eAgInSc6d1	trojkoaliční
projektu	projekt	k1gInSc6	projekt
strana	strana	k1gFnSc1	strana
podmínila	podmínit	k5eAaPmAgFnS	podmínit
demisí	demise	k1gFnPc2	demise
ministrů	ministr	k1gMnPc2	ministr
Vondry	Vondra	k1gMnSc2	Vondra
<g/>
,	,	kIx,	,
Fuksy	Fuksa	k1gMnSc2	Fuksa
a	a	k8xC	a
Kalouska	Kalousek	k1gMnSc2	Kalousek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
"	"	kIx"	"
<g/>
presumpce	presumpce	k1gFnSc1	presumpce
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
uplatněn	uplatnit	k5eAaPmNgInS	uplatnit
na	na	k7c4	na
jejich	jejich	k3xOp3gMnPc4	jejich
členy	člen	k1gMnPc4	člen
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
demisi	demise	k1gFnSc4	demise
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vondra	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neblokoval	blokovat	k5eNaImAgMnS	blokovat
vládní	vládní	k2eAgFnPc4d1	vládní
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
budoucí	budoucí	k2eAgFnSc4d1	budoucí
spolupráci	spolupráce	k1gFnSc4	spolupráce
trojkoalice	trojkoalice	k1gFnSc2	trojkoalice
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Nečas	Nečas	k1gMnSc1	Nečas
od	od	k7c2	od
VV	VV	kA	VV
požadoval	požadovat	k5eAaImAgInS	požadovat
odchod	odchod	k1gInSc1	odchod
lidí	člověk	k1gMnPc2	člověk
spojených	spojený	k2eAgMnPc2d1	spojený
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
ABL	ABL	kA	ABL
a	a	k8xC	a
demisy	demis	k1gInPc1	demis
Vondry	Vondra	k1gMnSc2	Vondra
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
dohody	dohoda	k1gFnSc2	dohoda
nebyly	být	k5eNaImAgInP	být
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
termínu	termín	k1gInSc3	termín
byl	být	k5eAaImAgInS	být
vyjednán	vyjednán	k2eAgInSc1d1	vyjednán
dodatek	dodatek	k1gInSc1	dodatek
ke	k	k7c3	k
smlouvě	smlouva	k1gFnSc3	smlouva
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
stanovený	stanovený	k2eAgInSc4d1	stanovený
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
se	se	k3xPyFc4	se
premiér	premiéra	k1gFnPc2	premiéra
a	a	k8xC	a
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
"	"	kIx"	"
<g/>
úpravách	úprava	k1gFnPc6	úprava
<g/>
"	"	kIx"	"
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
přiřknutí	přiřknutí	k1gNnSc2	přiřknutí
čtyř	čtyři	k4xCgInPc2	čtyři
ministerských	ministerský	k2eAgInPc2d1	ministerský
postů	post	k1gInPc2	post
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vzešla	vzejít	k5eAaPmAgFnS	vzejít
z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
K12	K12	k1gFnPc2	K12
koaliční	koaliční	k2eAgFnSc1d1	koaliční
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
zůstane	zůstat	k5eAaPmIp3nS	zůstat
vicepremiérem	vicepremiér	k1gMnSc7	vicepremiér
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
s	s	k7c7	s
gescí	gesce	k1gFnSc7	gesce
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Dobeš	Dobeš	k1gMnSc1	Dobeš
setrvá	setrvat	k5eAaPmIp3nS	setrvat
na	na	k7c6	na
postu	post	k1gInSc6	post
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
,	,	kIx,	,
novým	nový	k2eAgMnSc7d1	nový
ministrem	ministr	k1gMnSc7	ministr
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Radek	Radek	k1gMnSc1	Radek
Šmerda	Šmerda	k1gMnSc1	Šmerda
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
pak	pak	k8xC	pak
Jan	Jan	k1gMnSc1	Jan
Kubice	kubika	k1gFnSc3	kubika
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pak	pak	k6eAd1	pak
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
dva	dva	k4xCgMnPc4	dva
nové	nový	k2eAgMnPc4d1	nový
ministry	ministr	k1gMnPc4	ministr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vleklé	vleklý	k2eAgFnSc2d1	vleklá
vládní	vládní	k2eAgFnSc2d1	vládní
politické	politický	k2eAgFnSc2d1	politická
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
vnitrostranických	vnitrostranický	k2eAgInPc2d1	vnitrostranický
sporů	spor	k1gInPc2	spor
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgMnPc2d1	veřejný
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
pouhé	pouhý	k2eAgInPc4d1	pouhý
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
rozsudku	rozsudek	k1gInSc2	rozsudek
nad	nad	k7c7	nad
poslanci	poslanec	k1gMnPc7	poslanec
VV	VV	kA	VV
Vítem	Vít	k1gMnSc7	Vít
Bártou	Bárta	k1gMnSc7	Bárta
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Škárkou	škárka	k1gFnSc7	škárka
<g/>
,	,	kIx,	,
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
VV	VV	kA	VV
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
a	a	k8xC	a
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
vlády	vláda	k1gFnSc2	vláda
za	za	k7c2	za
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
Karolína	Karolína	k1gFnSc1	Karolína
Peake	Peake	k1gFnPc2	Peake
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
nové	nový	k2eAgFnSc2d1	nová
politické	politický	k2eAgFnSc2d1	politická
platformy	platforma	k1gFnSc2	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgInPc2d1	veřejný
a	a	k8xC	a
přechod	přechod	k1gInSc1	přechod
do	do	k7c2	do
platformy	platforma	k1gFnSc2	platforma
Karolíny	Karolína	k1gFnSc2	Karolína
Peake	Peak	k1gFnSc2	Peak
následně	následně	k6eAd1	následně
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
oba	dva	k4xCgMnPc1	dva
zbývající	zbývající	k2eAgMnPc1d1	zbývající
ministři	ministr	k1gMnPc1	ministr
za	za	k7c2	za
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Dobeš	Dobeš	k1gMnSc1	Dobeš
a	a	k8xC	a
Kamil	Kamil	k1gMnSc1	Kamil
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
4	[number]	k4	4
z	z	k7c2	z
21	[number]	k4	21
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
disponovala	disponovat	k5eAaBmAgFnS	disponovat
–	–	k?	–
Lenka	Lenka	k1gFnSc1	Lenka
Andrýsová	Andrýsová	k1gFnSc1	Andrýsová
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Paggio	Paggio	k1gMnSc1	Paggio
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Suchá	Suchá	k1gFnSc1	Suchá
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karolínou	Karolína	k1gFnSc7	Karolína
Peake	Peake	k1gNnSc2	Peake
prý	prý	k9	prý
nadále	nadále	k6eAd1	nadále
hodlají	hodlat	k5eAaImIp3nP	hodlat
plnit	plnit	k5eAaImF	plnit
koaliční	koaliční	k2eAgFnSc4d1	koaliční
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
frakce	frakce	k1gFnSc1	frakce
včetně	včetně	k7c2	včetně
samotné	samotný	k2eAgFnSc2d1	samotná
K.	K.	kA	K.
Peake	Peak	k1gFnSc2	Peak
a	a	k8xC	a
přistoupivších	přistoupivší	k2eAgFnPc2d1	přistoupivší
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
a	a	k8xC	a
Martina	Martin	k1gMnSc2	Martin
Vacka	Vacek	k1gMnSc2	Vacek
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
již	již	k6eAd1	již
7	[number]	k4	7
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Nečas	Nečas	k1gMnSc1	Nečas
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
koaliční	koaliční	k2eAgFnSc3d1	koaliční
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Věcmi	věc	k1gFnPc7	věc
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Karolíny	Karolína	k1gFnSc2	Karolína
Peake	Peak	k1gInSc2	Peak
bude	být	k5eAaImBp3nS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
<g/>
-li	i	k?	-li
tito	tento	k3xDgMnPc1	tento
poslanci	poslanec	k1gMnPc1	poslanec
alespoň	alespoň	k9	alespoň
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
klub	klub	k1gInSc4	klub
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
platí	platit	k5eAaImIp3nS	platit
minimum	minimum	k1gNnSc4	minimum
10	[number]	k4	10
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
takovéto	takovýto	k3xDgFnSc2	takovýto
podpory	podpora	k1gFnSc2	podpora
by	by	kYmCp3nS	by
většinu	většina	k1gFnSc4	většina
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
již	již	k6eAd1	již
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
a	a	k8xC	a
raději	rád	k6eAd2	rád
vyhlásí	vyhlásit	k5eAaPmIp3nP	vyhlásit
předčasné	předčasný	k2eAgFnPc4d1	předčasná
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
Karolíny	Karolína	k1gFnSc2	Karolína
Peake	Peak	k1gInSc2	Peak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
šokován	šokovat	k5eAaBmNgMnS	šokovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
situace	situace	k1gFnSc1	situace
mu	on	k3xPp3gMnSc3	on
připomíná	připomínat	k5eAaImIp3nS	připomínat
"	"	kIx"	"
<g/>
Sarajevský	sarajevský	k2eAgInSc4d1	sarajevský
atentát	atentát	k1gInSc4	atentát
<g/>
"	"	kIx"	"
v	v	k7c6	v
ODS	ODS	kA	ODS
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
a	a	k8xC	a
poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
se	se	k3xPyFc4	se
prý	prý	k9	prý
sejde	sejít	k5eAaPmIp3nS	sejít
<g />
.	.	kIx.	.
</s>
<s>
co	co	k3yInSc4	co
nejdříve	dříve	k6eAd3	dříve
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
situaci	situace	k1gFnSc4	situace
řešit	řešit	k5eAaImF	řešit
s	s	k7c7	s
rozvahou	rozvaha	k1gFnSc7	rozvaha
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pak	pak	k6eAd1	pak
poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
jednak	jednak	k8xC	jednak
Víta	Vít	k1gMnSc4	Vít
Bártu	Bárta	k1gMnSc4	Bárta
a	a	k8xC	a
jednak	jednak	k8xC	jednak
již	již	k6eAd1	již
sedm	sedm	k4xCc1	sedm
"	"	kIx"	"
<g/>
přeběhlíků	přeběhlík	k1gMnPc2	přeběhlík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
opustili	opustit	k5eAaPmAgMnP	opustit
stranu	strana	k1gFnSc4	strana
pro	pro	k7c4	pro
frakci	frakce	k1gFnSc4	frakce
Karolíny	Karolína	k1gFnSc2	Karolína
Peake	Peak	k1gInSc2	Peak
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
,	,	kIx,	,
Martina	Martin	k1gMnSc2	Martin
Vacka	Vacek	k1gMnSc2	Vacek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
zbývajících	zbývající	k2eAgInPc2d1	zbývající
12	[number]	k4	12
členů	člen	k1gInPc2	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
z	z	k7c2	z
Věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
platformy	platforma	k1gFnSc2	platforma
Karolíny	Karolína	k1gFnSc2	Karolína
Peake	Peak	k1gFnSc2	Peak
osmý	osmý	k4xOgMnSc1	osmý
poslanec	poslanec	k1gMnSc1	poslanec
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Vysloužil	Vysloužil	k1gMnSc1	Vysloužil
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnPc1d1	veřejná
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
koalice	koalice	k1gFnSc2	koalice
soustředily	soustředit	k5eAaPmAgInP	soustředit
na	na	k7c6	na
opoziční	opoziční	k2eAgFnSc6d1	opoziční
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
VV	VV	kA	VV
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
vtiskl	vtisknout	k5eAaPmAgMnS	vtisknout
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
za	za	k7c4	za
předsedu	předseda	k1gMnSc4	předseda
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
novou	nový	k2eAgFnSc4d1	nová
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
oligopolům	oligopol	k1gInPc3	oligopol
a	a	k8xC	a
kartelům	kartel	k1gInPc3	kartel
<g/>
.	.	kIx.	.
</s>
<s>
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Babák	Babák	k1gMnSc1	Babák
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
poslanci	poslanec	k1gMnPc1	poslanec
předložili	předložit	k5eAaPmAgMnP	předložit
několik	několik	k4yIc4	několik
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
novelu	novela	k1gFnSc4	novela
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
spotřebitelském	spotřebitelský	k2eAgInSc6d1	spotřebitelský
úvěru	úvěr	k1gInSc6	úvěr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
omezit	omezit	k5eAaPmF	omezit
lichvu	lichva	k1gFnSc4	lichva
<g/>
,	,	kIx,	,
novelu	novela	k1gFnSc4	novela
energetického	energetický	k2eAgInSc2d1	energetický
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
přinášející	přinášející	k2eAgFnSc3d1	přinášející
liberalizaci	liberalizace	k1gFnSc3	liberalizace
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
anonymních	anonymní	k2eAgFnPc2d1	anonymní
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
VV	VV	kA	VV
také	také	k6eAd1	také
podpořily	podpořit	k5eAaPmAgFnP	podpořit
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
loteriích	loterie	k1gFnPc6	loterie
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
podobných	podobný	k2eAgFnPc6d1	podobná
hrách	hra	k1gFnPc6	hra
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
ministra	ministr	k1gMnSc4	ministr
financí	finance	k1gFnPc2	finance
Miroslava	Miroslav	k1gMnSc4	Miroslav
Kalouska	Kalousek	k1gMnSc4	Kalousek
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
stažení	stažení	k1gNnSc4	stažení
nelegálně	legálně	k6eNd1	legálně
povolených	povolený	k2eAgInPc2d1	povolený
automatů	automat	k1gInPc2	automat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
Kalousek	Kalousek	k1gMnSc1	Kalousek
se	se	k3xPyFc4	se
v	v	k7c6	v
klíčovém	klíčový	k2eAgNnSc6d1	klíčové
hlasování	hlasování	k1gNnSc6	hlasování
13	[number]	k4	13
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
zdržel	zdržet	k5eAaPmAgMnS	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Klasnová	Klasnová	k1gFnSc1	Klasnová
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
sněmovně	sněmovna	k1gFnSc6	sněmovna
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
přijetím	přijetí	k1gNnSc7	přijetí
poslanci	poslanec	k1gMnPc1	poslanec
zavázali	zavázat	k5eAaPmAgMnP	zavázat
vládu	vláda	k1gFnSc4	vláda
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
sKaret	sKareta	k1gFnPc2	sKareta
<g/>
,	,	kIx,	,
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
došlo	dojít	k5eAaPmAgNnS	dojít
8	[number]	k4	8
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgNnSc4d1	veřejné
transformovala	transformovat	k5eAaBmAgFnS	transformovat
na	na	k7c4	na
spolek	spolek	k1gInSc4	spolek
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
transformace	transformace	k1gFnSc2	transformace
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
straně	strana	k1gFnSc3	strana
chyběly	chybět	k5eAaImAgInP	chybět
peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
začínali	začínat	k5eAaImAgMnP	začínat
postupně	postupně	k6eAd1	postupně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
subjekty	subjekt	k1gInPc7	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgNnSc1d1	veřejné
však	však	k9	však
úplně	úplně	k6eAd1	úplně
nezmizela	zmizet	k5eNaPmAgFnS	zmizet
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
sdružení	sdružení	k1gNnSc4	sdružení
a	a	k8xC	a
kandidovat	kandidovat	k5eAaImF	kandidovat
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
spolku	spolek	k1gInSc2	spolek
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
Olga	Olga	k1gFnSc1	Olga
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc7	jeho
řadovými	řadový	k2eAgInPc7d1	řadový
členy	člen	k1gInPc7	člen
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
i	i	k9	i
bývalí	bývalý	k2eAgMnPc1d1	bývalý
předsedové	předseda	k1gMnPc1	předseda
Jiří	Jiří	k1gMnSc1	Jiří
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
a	a	k8xC	a
Radek	Radek	k1gMnSc1	Radek
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
