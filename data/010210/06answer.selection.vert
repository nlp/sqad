<s>
Horalka	horalka	k1gFnSc1	horalka
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
La	la	k1gNnSc2	la
ciociara	ciociara	k1gFnSc1	ciociara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italské	italský	k2eAgNnSc4d1	italské
filmové	filmový	k2eAgNnSc4d1	filmové
válečné	válečný	k2eAgNnSc4d1	válečné
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
režisérem	režisér	k1gMnSc7	režisér
Vittoriem	Vittorium	k1gNnSc7	Vittorium
De	De	k?	De
Sica	Sicus	k1gMnSc2	Sicus
podle	podle	k7c2	podle
románové	románový	k2eAgFnSc2d1	románová
předlohy	předloha	k1gFnSc2	předloha
od	od	k7c2	od
Alberto	Alberta	k1gFnSc5	Alberta
Moravii	Moravie	k1gFnSc3	Moravie
<g/>
.	.	kIx.	.
</s>
