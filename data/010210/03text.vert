<p>
<s>
Horalka	horalka	k1gFnSc1	horalka
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
La	la	k1gNnSc2	la
ciociara	ciociara	k1gFnSc1	ciociara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italské	italský	k2eAgNnSc4d1	italské
filmové	filmový	k2eAgNnSc4d1	filmové
válečné	válečný	k2eAgNnSc4d1	válečné
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
režisérem	režisér	k1gMnSc7	režisér
Vittoriem	Vittorium	k1gNnSc7	Vittorium
De	De	k?	De
Sica	Sicus	k1gMnSc2	Sicus
podle	podle	k7c2	podle
románové	románový	k2eAgFnSc2d1	románová
předlohy	předloha	k1gFnSc2	předloha
od	od	k7c2	od
Alberto	Alberta	k1gFnSc5	Alberta
Moravii	Moravie	k1gFnSc3	Moravie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvrárnila	ztvrárnit	k5eAaPmAgFnS	ztvrárnit
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
italská	italský	k2eAgFnSc1d1	italská
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
Sophia	Sophia	k1gFnSc1	Sophia
Lorenová	Lorenová	k1gFnSc1	Lorenová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
ochránit	ochránit	k5eAaPmF	ochránit
svojí	svůj	k3xOyFgFnSc7	svůj
dceru	dcera	k1gFnSc4	dcera
od	od	k7c2	od
hrůz	hrůza	k1gFnPc2	hrůza
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
vymyšlený	vymyšlený	k2eAgMnSc1d1	vymyšlený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Italové	Ital	k1gMnPc1	Ital
nazývají	nazývat	k5eAaImIp3nP	nazývat
Marocchinate	Marocchinat	k1gInSc5	Marocchinat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrají	hrát	k5eAaImIp3nP	hrát
kromě	kromě	k7c2	kromě
Sophie	Sophie	k1gFnSc2	Sophie
Lorenové	Lorenové	k2eAgFnPc2d1	Lorenové
Jean-Paul	Jean-Paula	k1gFnPc2	Jean-Paula
Belmondo	Belmondo	k6eAd1	Belmondo
<g/>
,	,	kIx,	,
Eleonora	Eleonora	k1gFnSc1	Eleonora
Brown	Brown	k1gInSc1	Brown
<g/>
,	,	kIx,	,
Carlo	Carlo	k1gNnSc1	Carlo
Ninchi	Ninchi	k1gNnSc1	Ninchi
a	a	k8xC	a
Andrea	Andrea	k1gFnSc1	Andrea
Checchi	Checch	k1gFnSc2	Checch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
nominován	nominován	k2eAgMnSc1d1	nominován
a	a	k8xC	a
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
udělovanou	udělovaný	k2eAgFnSc7d1	udělovaná
americkou	americký	k2eAgFnSc7d1	americká
filmovou	filmový	k2eAgFnSc7d1	filmová
Akademií	akademie	k1gFnSc7	akademie
skutečně	skutečně	k6eAd1	skutečně
získal	získat	k5eAaPmAgMnS	získat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
hlavní	hlavní	k2eAgInSc4d1	hlavní
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Sophia	Sophia	k1gFnSc1	Sophia
Lorenová	Lorenová	k1gFnSc1	Lorenová
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k8xS	co
Oskara	Oskar	k1gMnSc4	Oskar
získal	získat	k5eAaPmAgMnS	získat
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Lorenová	Lorenová	k1gFnSc1	Lorenová
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
nervózní	nervózní	k2eAgFnSc1d1	nervózní
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
udělování	udělování	k1gNnSc2	udělování
Oskarů	Oskar	k1gMnPc2	Oskar
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Two	Two	k1gFnSc2	Two
Women	Womna	k1gFnPc2	Womna
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
