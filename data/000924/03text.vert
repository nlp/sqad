<s>
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
je	být	k5eAaImIp3nS	být
skalní	skalní	k2eAgInSc4d1	skalní
útvar	útvar	k1gInSc4	útvar
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
části	část	k1gFnSc6	část
katastru	katastr	k1gInSc2	katastr
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Slatina	Brno-Slatina	k1gFnSc1	Brno-Slatina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Slatinou	slatina	k1gFnSc7	slatina
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
Hviezdoslavova	Hviezdoslavův	k2eAgFnSc1d1	Hviezdoslavova
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
MHD	MHD	kA	MHD
31	[number]	k4	31
nebo	nebo	k8xC	nebo
33	[number]	k4	33
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
konečná	konečná	k1gFnSc1	konečná
tramvaje	tramvaj	k1gFnSc2	tramvaj
č.	č.	k?	č.
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
Líšní	Líšeň	k1gFnSc7	Líšeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
černovické	černovický	k2eAgFnSc2d1	černovická
a	a	k8xC	a
tuřanské	tuřanský	k2eAgFnSc2d1	tuřanská
terasy	terasa	k1gFnSc2	terasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Židenice	Židenice	k1gFnSc2	Židenice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
národní	národní	k2eAgFnSc7d1	národní
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
bylo	být	k5eAaImAgNnS	být
provedeno	proveden	k2eAgNnSc1d1	provedeno
vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
205	[number]	k4	205
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geologickém	geologický	k2eAgInSc6d1	geologický
vývoji	vývoj	k1gInSc6	vývoj
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
historie	historie	k1gFnSc1	historie
dvou	dva	k4xCgFnPc2	dva
velkých	velký	k2eAgFnPc2d1	velká
středoevropských	středoevropský	k2eAgFnPc2d1	středoevropská
geologických	geologický	k2eAgFnPc2d1	geologická
jednotek	jednotka	k1gFnPc2	jednotka
-	-	kIx~	-
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
a	a	k8xC	a
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
masiv	masiv	k1gInSc1	masiv
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyvrásněna	vyvrásnit	k5eAaPmNgFnS	vyvrásnit
kadomským	kadomský	k2eAgNnSc7d1	kadomské
vrásněním	vrásnění	k1gNnSc7	vrásnění
(	(	kIx(	(
<g/>
přelom	přelom	k1gInSc1	přelom
prekambria	prekambrium	k1gNnSc2	prekambrium
a	a	k8xC	a
prvohor	prvohory	k1gFnPc2	prvohory
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
etapě	etapa	k1gFnSc3	etapa
patří	patřit	k5eAaImIp3nS	patřit
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
většinou	většinou	k6eAd1	většinou
nasedají	nasedat	k5eAaImIp3nP	nasedat
devonské	devonský	k2eAgInPc4d1	devonský
slepence	slepenec	k1gInPc4	slepenec
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInPc4	vápenec
a	a	k8xC	a
břidlice	břidlice	k1gFnPc4	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Stránské	Stránské	k2eAgFnSc6d1	Stránské
skále	skála	k1gFnSc6	skála
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
přes	přes	k7c4	přes
50	[number]	k4	50
m	m	kA	m
mocný	mocný	k2eAgInSc4d1	mocný
sled	sled	k1gInSc4	sled
vápenců	vápenec	k1gInPc2	vápenec
starých	starý	k2eAgInPc2d1	starý
cca	cca	kA	cca
156	[number]	k4	156
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
horninotvornou	horninotvorný	k2eAgFnSc4d1	horninotvorný
funkci	funkce	k1gFnSc4	funkce
sehrály	sehrát	k5eAaPmAgFnP	sehrát
vedle	vedle	k7c2	vedle
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
schránek	schránka	k1gFnPc2	schránka
rozsivek	rozsivka	k1gFnPc2	rozsivka
<g/>
,	,	kIx,	,
dírkovců	dírkovec	k1gMnPc2	dírkovec
a	a	k8xC	a
jehlic	jehlice	k1gFnPc2	jehlice
i	i	k8xC	i
články	článek	k1gInPc4	článek
lilijic	lilijice	k1gFnPc2	lilijice
(	(	kIx(	(
<g/>
Crinoidea	Crinoidea	k1gFnSc1	Crinoidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vápenité	vápenitý	k2eAgInPc1d1	vápenitý
jíly	jíl	k1gInPc1	jíl
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bohatou	bohatý	k2eAgFnSc4d1	bohatá
mikrofaunu	mikrofauna	k1gFnSc4	mikrofauna
-	-	kIx~	-
dírkovci	dírkovec	k1gMnPc1	dírkovec
<g/>
,	,	kIx,	,
jehlice	jehlice	k1gFnPc1	jehlice
živočišných	živočišný	k2eAgFnPc2d1	živočišná
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
skořepatci	skořepatec	k1gMnPc1	skořepatec
<g/>
,	,	kIx,	,
ostny	osten	k1gInPc1	osten
ježovek	ježovka	k1gFnPc2	ježovka
<g/>
.	.	kIx.	.
</s>
<s>
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
skalních	skalní	k2eAgInPc2d1	skalní
výchozů	výchoz	k1gInPc2	výchoz
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jurský	jurský	k2eAgInSc1d1	jurský
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
pohoří	pohoří	k1gNnSc2	pohoří
Jura	jura	k1gFnSc1	jura
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zájemce	zájemce	k1gMnSc4	zájemce
o	o	k7c4	o
živou	živý	k2eAgFnSc4d1	živá
a	a	k8xC	a
neživou	živý	k2eNgFnSc4d1	neživá
přírodu	příroda	k1gFnSc4	příroda
skýtá	skýtat	k5eAaImIp3nS	skýtat
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
četné	četný	k2eAgFnSc2d1	četná
příležitosti	příležitost	k1gFnSc2	příležitost
studovat	studovat	k5eAaImF	studovat
a	a	k8xC	a
poznávat	poznávat	k5eAaImF	poznávat
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
řadu	řad	k1gInSc2	řad
geologických	geologický	k2eAgMnPc2d1	geologický
i	i	k8xC	i
biologických	biologický	k2eAgFnPc2d1	biologická
zajímavostí	zajímavost	k1gFnPc2	zajímavost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologické	paleontologický	k2eAgInPc1d1	paleontologický
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
jak	jak	k8xS	jak
v	v	k7c6	v
jurských	jurský	k2eAgInPc6d1	jurský
vápencích	vápenec	k1gInPc6	vápenec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
čtvrtohorních	čtvrtohorní	k2eAgInPc6d1	čtvrtohorní
sedimentech	sediment	k1gInPc6	sediment
<g/>
.	.	kIx.	.
<g/>
Dlouholeté	dlouholetý	k2eAgInPc1d1	dlouholetý
výzkumy	výzkum	k1gInPc1	výzkum
odborníků	odborník	k1gMnPc2	odborník
i	i	k8xC	i
návštěvy	návštěva	k1gFnSc2	návštěva
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
výletníků	výletník	k1gMnPc2	výletník
objevily	objevit	k5eAaPmAgFnP	objevit
mnoho	mnoho	k4c4	mnoho
fosilií	fosilie	k1gFnPc2	fosilie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
mořských	mořský	k2eAgMnPc2d1	mořský
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
a	a	k8xC	a
vzácné	vzácný	k2eAgInPc4d1	vzácný
objevy	objev	k1gInPc4	objev
zubů	zub	k1gInPc2	zub
žraloků	žralok	k1gMnPc2	žralok
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
fosilií	fosilie	k1gFnPc2	fosilie
<g/>
:	:	kIx,	:
dírkovci	dírkovec	k1gMnPc1	dírkovec
<g/>
,	,	kIx,	,
jehlice	jehlice	k1gFnPc1	jehlice
živočišných	živočišný	k2eAgFnPc2d1	živočišná
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
skořepatci	skořepatec	k1gMnPc1	skořepatec
<g/>
,	,	kIx,	,
ježovky	ježovka	k1gFnPc1	ježovka
<g/>
,	,	kIx,	,
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
(	(	kIx(	(
<g/>
amoniti	amonit	k5eAaImF	amonit
<g/>
,	,	kIx,	,
belemniti	belemnit	k5eAaPmF	belemnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
členovci	členovec	k1gMnPc1	členovec
(	(	kIx(	(
<g/>
vzácně	vzácně	k6eAd1	vzácně
krunýře	krunýř	k1gInPc1	krunýř
mořských	mořský	k2eAgInPc2d1	mořský
ráčků	ráček	k1gInPc2	ráček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obratlovci	obratlovec	k1gMnPc1	obratlovec
(	(	kIx(	(
<g/>
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
ryboještěři	ryboještěr	k1gMnPc1	ryboještěr
<g/>
,	,	kIx,	,
žraloci	žralok	k1gMnPc1	žralok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
korály	korál	k1gInPc1	korál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Absolonově	Absolonův	k2eAgFnSc6d1	Absolonova
jeskyni	jeskyně	k1gFnSc6	jeskyně
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
645	[number]	k4	645
kostí	kost	k1gFnPc2	kost
pleistocénních	pleistocénní	k2eAgMnPc2d1	pleistocénní
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
19	[number]	k4	19
rodům	rod	k1gInPc3	rod
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
24	[number]	k4	24
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
geologicko-paleontologického	geologickoaleontologický	k2eAgNnSc2d1	geologicko-paleontologický
oddělení	oddělení	k1gNnSc2	oddělení
Moravského	moravský	k2eAgNnSc2d1	Moravské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgInPc4d1	důležitý
nálezy	nález	k1gInPc4	nález
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
patří	patřit	k5eAaImIp3nS	patřit
kamenný	kamenný	k2eAgInSc1d1	kamenný
kvádr	kvádr	k1gInSc1	kvádr
s	s	k7c7	s
otisky	otisk	k1gInPc7	otisk
zubů	zub	k1gInPc2	zub
dýkozubce	dýkozubce	k1gMnSc4	dýkozubce
moravského	moravský	k2eAgMnSc4d1	moravský
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgMnSc1d1	umístěný
v	v	k7c6	v
Anthroposu	Anthropos	k1gInSc6	Anthropos
v	v	k7c6	v
Brně-Pisárkách	Brně-Pisárka	k1gFnPc6	Brně-Pisárka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
prve	prve	k6eAd1	prve
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
pravěké	pravěký	k2eAgFnSc2d1	pravěká
šelmy	šelma	k1gFnSc2	šelma
Homotherium	Homotherium	k1gNnSc1	Homotherium
Moravicum	Moravicum	k1gInSc1	Moravicum
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
uměle	uměle	k6eAd1	uměle
hloubených	hloubený	k2eAgFnPc2d1	hloubená
štol	štola	k1gFnPc2	štola
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgNnPc1	tři
pásma	pásmo	k1gNnPc1	pásmo
vápencových	vápencový	k2eAgFnPc2d1	vápencová
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
posloužily	posloužit	k5eAaPmAgFnP	posloužit
domovem	domov	k1gInSc7	domov
pravěkému	pravěký	k2eAgMnSc3d1	pravěký
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
asi	asi	k9	asi
29	[number]	k4	29
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
rohovců	rohovec	k1gInPc2	rohovec
<g/>
,	,	kIx,	,
vhodných	vhodný	k2eAgNnPc2d1	vhodné
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
osídlení	osídlení	k1gNnSc4	osídlení
lokality	lokalita	k1gFnSc2	lokalita
již	již	k6eAd1	již
od	od	k7c2	od
starého	starý	k2eAgInSc2d1	starý
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
doloženy	doložit	k5eAaPmNgInP	doložit
první	první	k4xOgInPc1	první
doklady	doklad	k1gInPc1	doklad
těžby	těžba	k1gFnSc2	těžba
suroviny	surovina	k1gFnSc2	surovina
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
štípaných	štípaný	k2eAgInPc2d1	štípaný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Surovina	surovina	k1gFnSc1	surovina
však	však	k9	však
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
těžena	těžit	k5eAaImNgFnS	těžit
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
-	-	kIx~	-
neolitu	neolit	k1gInSc2	neolit
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
-	-	kIx~	-
eneolitu	eneolit	k1gInSc3	eneolit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
sloužily	sloužit	k5eAaImAgFnP	sloužit
zdejší	zdejší	k2eAgInSc4d1	zdejší
vápence	vápenec	k1gInPc4	vápenec
jako	jako	k8xC	jako
stavební	stavební	k2eAgInSc4d1	stavební
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
použitý	použitý	k2eAgInSc4d1	použitý
například	například	k6eAd1	například
pro	pro	k7c4	pro
kašnu	kašna	k1gFnSc4	kašna
Parnas	Parnas	k1gInSc4	Parnas
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
nebo	nebo	k8xC	nebo
Zderadův	Zderadův	k2eAgInSc1d1	Zderadův
sloup	sloup	k1gInSc1	sloup
v	v	k7c6	v
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
svah	svah	k1gInSc1	svah
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
zničen	zničit	k5eAaPmNgInS	zničit
velkými	velký	k2eAgInPc7d1	velký
kamenolomy	kamenolom	k1gInPc7	kamenolom
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
výzkumy	výzkum	k1gInPc4	výzkum
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Ústav	ústav	k1gInSc1	ústav
Anthropos	Anthroposa	k1gFnPc2	Anthroposa
Moravského	moravský	k2eAgNnSc2d1	Moravské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
1972	[number]	k4	1972
zde	zde	k6eAd1	zde
probíhal	probíhat	k5eAaImAgInS	probíhat
výzkum	výzkum	k1gInSc1	výzkum
řízený	řízený	k2eAgInSc1d1	řízený
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Musilem	Musil	k1gMnSc7	Musil
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
objasnění	objasnění	k1gNnSc4	objasnění
stratigrafických	stratigrafický	k2eAgFnPc2d1	stratigrafická
a	a	k8xC	a
chronologických	chronologický	k2eAgFnPc2d1	chronologická
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Překvapující	překvapující	k2eAgInSc1d1	překvapující
objev	objev	k1gInSc1	objev
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
zlomků	zlomek	k1gInPc2	zlomek
přepálených	přepálený	k2eAgFnPc2d1	přepálená
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
závažný	závažný	k2eAgInSc1d1	závažný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996-1998	[number]	k4	1996-1998
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Karla	Karel	k1gMnSc2	Karel
Valocha	Valoch	k1gMnSc2	Valoch
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgInPc1d1	nalezený
artefakty	artefakt	k1gInPc1	artefakt
a	a	k8xC	a
spálené	spálený	k2eAgFnPc1d1	spálená
kosti	kost	k1gFnPc1	kost
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
staropaleolitické	staropaleolitický	k2eAgNnSc4d1	staropaleolitický
stáří	stáří	k1gNnSc4	stáří
této	tento	k3xDgFnSc2	tento
významné	významný	k2eAgFnSc2d1	významná
evropské	evropský	k2eAgFnSc2d1	Evropská
lokality	lokalita	k1gFnSc2	lokalita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dnes	dnes	k6eAd1	dnes
klademe	klást	k5eAaImIp1nP	klást
do	do	k7c2	do
období	období	k1gNnSc2	období
staršího	starší	k1gMnSc2	starší
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
významnou	významný	k2eAgFnSc7d1	významná
badatelskou	badatelský	k2eAgFnSc7d1	badatelská
aktivitou	aktivita	k1gFnSc7	aktivita
ústavu	ústav	k1gInSc2	ústav
Anthropos	Anthropos	k1gInSc1	Anthropos
byl	být	k5eAaImAgInS	být
výzkum	výzkum	k1gInSc4	výzkum
polohy	poloha	k1gFnSc2	poloha
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
III	III	kA	III
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
vedl	vést	k5eAaImAgMnS	vést
opět	opět	k6eAd1	opět
Karel	Karel	k1gMnSc1	Karel
Valoch	Valoch	k1gMnSc1	Valoch
<g/>
.	.	kIx.	.
</s>
<s>
Odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
zde	zde	k6eAd1	zde
paleolitickou	paleolitický	k2eAgFnSc4d1	paleolitická
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
objevil	objevit	k5eAaPmAgMnS	objevit
dílenské	dílenský	k2eAgInPc4d1	dílenský
objekty	objekt	k1gInPc4	objekt
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
místního	místní	k2eAgInSc2d1	místní
rohovce	rohovec	k1gInSc2	rohovec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
odkryvy	odkryv	k1gInPc4	odkryv
zde	zde	k6eAd1	zde
realizoval	realizovat	k5eAaBmAgInS	realizovat
výzkum	výzkum	k1gInSc1	výzkum
Archeologického	archeologický	k2eAgInSc2d1	archeologický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
Brno	Brno	k1gNnSc1	Brno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jiřího	Jiří	k1gMnSc2	Jiří
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
pokračováním	pokračování	k1gNnSc7	pokračování
výzkumu	výzkum	k1gInSc2	výzkum
eneolitické	eneolitický	k2eAgFnSc2d1	eneolitická
fáze	fáze	k1gFnSc2	fáze
osídlení	osídlení	k1gNnSc2	osídlení
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
realizovalo	realizovat	k5eAaBmAgNnS	realizovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
Muzeum	muzeum	k1gNnSc4	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Archeologickým	archeologický	k2eAgInSc7d1	archeologický
ústavem	ústav	k1gInSc7	ústav
ČSAV	ČSAV	kA	ČSAV
Brno	Brno	k1gNnSc1	Brno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jany	Jana	k1gFnSc2	Jana
Čižmářové	Čižmářové	k?	Čižmářové
a	a	k8xC	a
Iva	Ivo	k1gMnSc2	Ivo
Rakovského	Rakovský	k1gMnSc2	Rakovský
<g/>
.	.	kIx.	.
</s>
<s>
Artefakty	artefakt	k1gInPc1	artefakt
z	z	k7c2	z
výzkumů	výzkum	k1gInPc2	výzkum
K.	K.	kA	K.
Valocha	Valoch	k1gMnSc2	Valoch
a	a	k8xC	a
J.	J.	kA	J.
Svobody	svoboda	k1gFnPc1	svoboda
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
kulturou	kultura	k1gFnSc7	kultura
bohunicienu	bohunicien	k1gInSc2	bohunicien
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
produkcí	produkce	k1gFnSc7	produkce
čepelí	čepel	k1gFnPc2	čepel
a	a	k8xC	a
hrotů	hrot	k1gInPc2	hrot
z	z	k7c2	z
levalloiských	levalloiský	k2eAgNnPc2d1	levalloiský
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
staršího	starý	k2eAgInSc2d2	starší
pleistocénu	pleistocén	k1gInSc2	pleistocén
(	(	kIx(	(
<g/>
před	před	k7c7	před
600	[number]	k4	600
000	[number]	k4	000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
již	jenž	k3xRgFnSc4	jenž
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
svah	svah	k1gInSc1	svah
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
narušený	narušený	k2eAgInSc1d1	narušený
několika	několik	k4yIc7	několik
velkými	velký	k2eAgInPc7d1	velký
kamenolomy	kamenolom	k1gInPc7	kamenolom
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
strmý	strmý	k2eAgMnSc1d1	strmý
a	a	k8xC	a
skalnatý	skalnatý	k2eAgInSc1d1	skalnatý
a	a	k8xC	a
nacházely	nacházet	k5eAaImAgInP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
malé	malý	k2eAgFnPc4d1	malá
i	i	k8xC	i
větší	veliký	k2eAgFnPc4d2	veliký
jeskyňky	jeskyňka	k1gFnPc4	jeskyňka
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
však	však	k9	však
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
zcela	zcela	k6eAd1	zcela
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sníženině	sníženina	k1gFnSc6	sníženina
mezi	mezi	k7c7	mezi
Stránskou	Stránská	k1gFnSc7	Stránská
skálou	skála	k1gFnSc7	skála
a	a	k8xC	a
Bílou	bílý	k2eAgFnSc7d1	bílá
horou	hora	k1gFnSc7	hora
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgNnSc4	svůj
koryto	koryto	k1gNnSc4	koryto
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Svitava	Svitava	k1gFnSc1	Svitava
<g/>
,	,	kIx,	,
přitékající	přitékající	k2eAgFnSc1d1	přitékající
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
od	od	k7c2	od
Obřan	Obřany	k1gInPc2	Obřany
a	a	k8xC	a
rozlévající	rozlévající	k2eAgMnSc1d1	rozlévající
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Tuřanům	Tuřan	k1gInPc3	Tuřan
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
četná	četný	k2eAgNnPc4d1	četné
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
zanechávala	zanechávat	k5eAaImAgFnS	zanechávat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
opuštěná	opuštěný	k2eAgNnPc1d1	opuštěné
koryta	koryto	k1gNnPc1	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
Bílé	bílý	k2eAgFnSc2d1	bílá
hory	hora	k1gFnSc2	hora
byl	být	k5eAaImAgInS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
stepním	stepní	k2eAgInSc7d1	stepní
porostem	porost	k1gInSc7	porost
s	s	k7c7	s
roztroušenými	roztroušený	k2eAgInPc7d1	roztroušený
keři	keř	k1gInPc7	keř
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozprostíraly	rozprostírat	k5eAaImAgInP	rozprostírat
listnaté	listnatý	k2eAgInPc1d1	listnatý
lesy	les	k1gInPc1	les
parkového	parkový	k2eAgInSc2d1	parkový
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
bylo	být	k5eAaImAgNnS	být
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
roční	roční	k2eAgFnSc7d1	roční
teplotou	teplota	k1gFnSc7	teplota
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Příznivé	příznivý	k2eAgFnPc1d1	příznivá
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
život	život	k1gInSc4	život
rozmanité	rozmanitý	k2eAgFnSc2d1	rozmanitá
zvířeně	zvířena	k1gFnSc3	zvířena
a	a	k8xC	a
početnému	početný	k2eAgNnSc3d1	početné
ptactvu	ptactvo	k1gNnSc3	ptactvo
vodnímu	vodní	k2eAgNnSc3d1	vodní
i	i	k9	i
brodivému	brodivý	k2eAgMnSc3d1	brodivý
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
velkých	velký	k2eAgMnPc2d1	velký
býložravců	býložravec	k1gMnPc2	býložravec
tu	tu	k6eAd1	tu
žili	žít	k5eAaImAgMnP	žít
lesní	lesní	k2eAgMnPc1d1	lesní
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
stepní	stepní	k2eAgMnPc1d1	stepní
nosorožci	nosorožec	k1gMnPc1	nosorožec
<g/>
,	,	kIx,	,
bizoni	bizon	k1gMnPc1	bizon
<g/>
,	,	kIx,	,
velcí	velký	k2eAgMnPc1d1	velký
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
šelmy	šelma	k1gFnPc1	šelma
byly	být	k5eAaImAgFnP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
tygrem	tygr	k1gMnSc7	tygr
šavlozubým	šavlozubý	k2eAgFnPc3d1	šavlozubá
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc7d3	veliký
kočkovitou	kočkovitý	k2eAgFnSc7d1	kočkovitá
šelmou	šelma	k1gFnSc7	šelma
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
žila	žít	k5eAaImAgFnS	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lvem	lev	k1gMnSc7	lev
<g/>
,	,	kIx,	,
hyenou	hyena	k1gFnSc7	hyena
<g/>
,	,	kIx,	,
vlkem	vlk	k1gMnSc7	vlk
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
psovitých	psovitý	k2eAgFnPc2d1	psovitá
a	a	k8xC	a
kunovitých	kunovitý	k2eAgFnPc2d1	kunovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
obyvatelem	obyvatel	k1gMnSc7	obyvatel
převážně	převážně	k6eAd1	převážně
jeskyní	jeskyně	k1gFnPc2	jeskyně
byl	být	k5eAaImAgMnS	být
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Deningerův	Deningerův	k2eAgMnSc1d1	Deningerův
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
jeskynních	jeskynní	k2eAgMnPc2d1	jeskynní
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
prostředí	prostředí	k1gNnSc2	prostředí
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
Stránskou	Stránská	k1gFnSc4	Stránská
skálu	skála	k1gFnSc4	skála
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
byli	být	k5eAaImAgMnP	být
ještě	ještě	k6eAd1	ještě
odlišní	odlišný	k2eAgMnPc1d1	odlišný
od	od	k7c2	od
dnešních	dnešní	k2eAgFnPc2d1	dnešní
moderních	moderní	k2eAgFnPc2d1	moderní
populací	populace	k1gFnPc2	populace
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
druhu	druh	k1gInSc2	druh
člověka	člověk	k1gMnSc2	člověk
vzpřímeného	vzpřímený	k2eAgMnSc2d1	vzpřímený
(	(	kIx(	(
<g/>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
potulovali	potulovat	k5eAaImAgMnP	potulovat
v	v	k7c6	v
tlupách	tlupa	k1gFnPc6	tlupa
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
napajedel	napajedlo	k1gNnPc2	napajedlo
snáze	snadno	k6eAd2	snadno
ulovili	ulovit	k5eAaPmAgMnP	ulovit
nějakou	nějaký	k3yIgFnSc4	nějaký
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgInPc1d1	říční
štěrky	štěrk	k1gInPc1	štěrk
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
skýtaly	skýtat	k5eAaImAgInP	skýtat
vhodné	vhodný	k2eAgInPc1d1	vhodný
valouny	valoun	k1gInPc1	valoun
křemene	křemen	k1gInSc2	křemen
a	a	k8xC	a
křemence	křemenec	k1gInSc2	křemenec
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
nástrojů	nástroj	k1gInPc2	nástroj
potřebných	potřebný	k2eAgInPc2d1	potřebný
ke	k	k7c3	k
každodenním	každodenní	k2eAgFnPc3d1	každodenní
činnostem	činnost	k1gFnPc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
vyvýšených	vyvýšený	k2eAgNnPc6d1	vyvýšené
místech	místo	k1gNnPc6	místo
si	se	k3xPyFc3	se
také	také	k9	také
zakládali	zakládat	k5eAaImAgMnP	zakládat
svá	svůj	k3xOyFgNnPc4	svůj
tábořiště	tábořiště	k1gNnPc4	tábořiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Stránské	Stránské	k2eAgFnSc6d1	Stránské
skále	skála	k1gFnSc6	skála
našli	najít	k5eAaPmAgMnP	najít
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
<g/>
:	:	kIx,	:
v	v	k7c6	v
jeskyňkách	jeskyňka	k1gFnPc6	jeskyňka
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
nalezli	naleznout	k5eAaPmAgMnP	naleznout
úkryt	úkryt	k1gInSc4	úkryt
<g/>
,	,	kIx,	,
zvěře	zvěř	k1gFnSc2	zvěř
i	i	k8xC	i
ptactva	ptactvo	k1gNnSc2	ptactvo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nadbytek	nadbytek	k1gInSc4	nadbytek
<g/>
,	,	kIx,	,
z	z	k7c2	z
místních	místní	k2eAgInPc2d1	místní
rohovců	rohovec	k1gInPc2	rohovec
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
naštípat	naštípat	k5eAaPmF	naštípat
libovolné	libovolný	k2eAgInPc4d1	libovolný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nebylo	být	k5eNaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
žádné	žádný	k3yNgNnSc1	žádný
neporušené	porušený	k2eNgNnSc1d1	neporušené
ohniště	ohniště	k1gNnSc1	ohniště
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
spálených	spálený	k2eAgInPc2d1	spálený
kousků	kousek	k1gInPc2	kousek
kostí	kost	k1gFnPc2	kost
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepochybně	pochybně	k6eNd1	pochybně
používali	používat	k5eAaImAgMnP	používat
oheň	oheň	k1gInSc4	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
zdrželi	zdržet	k5eAaPmAgMnP	zdržet
<g/>
,	,	kIx,	,
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
potulní	potulný	k2eAgMnPc1d1	potulný
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
<g/>
.	.	kIx.	.
</s>
<s>
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
sídliště	sídliště	k1gNnSc4	sídliště
člověka	člověk	k1gMnSc2	člověk
typu	typ	k1gInSc2	typ
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
přinesla	přinést	k5eAaPmAgFnS	přinést
také	také	k9	také
snad	snad	k9	snad
nejstarší	starý	k2eAgInSc1d3	nejstarší
doklad	doklad	k1gInSc1	doklad
užívání	užívání	k1gNnSc2	užívání
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
doklady	doklad	k1gInPc1	doklad
přítomnosti	přítomnost	k1gFnSc2	přítomnost
člověka	člověk	k1gMnSc2	člověk
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
období	období	k1gNnSc2	období
počátku	počátek	k1gInSc2	počátek
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mezi	mezi	k7c4	mezi
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgNnSc2	první
chladného	chladný	k2eAgNnSc2d1	chladné
období	období	k1gNnSc2	období
posledního	poslední	k2eAgInSc2d1	poslední
glaciálu	glaciál	k1gInSc2	glaciál
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ještě	ještě	k6eAd1	ještě
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
stepní	stepní	k2eAgFnSc1d1	stepní
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
na	na	k7c6	na
Stránské	Stránské	k2eAgFnSc6d1	Stránské
skále	skála	k1gFnSc6	skála
objevili	objevit	k5eAaPmAgMnP	objevit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
nositeli	nositel	k1gMnPc7	nositel
kultury	kultura	k1gFnSc2	kultura
bohunicienu	bohunicien	k1gInSc2	bohunicien
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
podle	podle	k7c2	podle
nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
brněnských	brněnský	k2eAgFnPc2d1	brněnská
Bohunic	Bohunice	k1gFnPc2	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Rohovce	rohovec	k1gInPc1	rohovec
ze	z	k7c2	z
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základní	základní	k2eAgInPc1d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dnes	dnes	k6eAd1	dnes
nacházíme	nacházet	k5eAaImIp1nP	nacházet
i	i	k9	i
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Nositele	nositel	k1gMnSc4	nositel
bohunicienu	bohunicien	k1gInSc3	bohunicien
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
docela	docela	k6eAd1	docela
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	on	k3xPp3gNnSc7	on
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
neandertálec	neandertálec	k1gMnSc1	neandertálec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplejším	teplý	k2eAgNnSc6d2	teplejší
období	období	k1gNnSc6	období
posledního	poslední	k2eAgInSc2d1	poslední
glaciálu	glaciál	k1gInSc2	glaciál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
Stránské	Stránské	k2eAgFnSc6d1	Stránské
skále	skála	k1gFnSc6	skála
usadil	usadit	k5eAaPmAgInS	usadit
lid	lid	k1gInSc1	lid
jiné	jiná	k1gFnSc2	jiná
<g/>
,	,	kIx,	,
časně	časně	k6eAd1	časně
mladopaleolitické	mladopaleolitický	k2eAgFnSc2d1	mladopaleolitická
kultury	kultura	k1gFnSc2	kultura
-	-	kIx~	-
aurignacienu	aurignacien	k1gInSc2	aurignacien
<g/>
.	.	kIx.	.
</s>
<s>
Nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
lokalit	lokalita	k1gFnPc2	lokalita
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
nositelem	nositel	k1gMnSc7	nositel
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
člověk	člověk	k1gMnSc1	člověk
moderního	moderní	k2eAgInSc2d1	moderní
typu	typ	k1gInSc2	typ
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
kromaňonský	kromaňonský	k2eAgMnSc1d1	kromaňonský
(	(	kIx(	(
<g/>
Homo	Homo	k6eAd1	Homo
sapiens	sapiens	k6eAd1	sapiens
sapiens	sapiens	k6eAd1	sapiens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nalezených	nalezený	k2eAgInPc2d1	nalezený
předmětů	předmět	k1gInPc2	předmět
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
kultury	kultura	k1gFnPc1	kultura
využívaly	využívat	k5eAaImAgFnP	využívat
Stránskou	Stránská	k1gFnSc4	Stránská
skálu	skála	k1gFnSc4	skála
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zpracovávaly	zpracovávat	k5eAaImAgFnP	zpracovávat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Rámcově	rámcově	k6eAd1	rámcově
před	před	k7c7	před
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
studeného	studený	k2eAgNnSc2d1	studené
klimatu	klima	k1gNnSc2	klima
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
,	,	kIx,	,
provázenému	provázený	k2eAgInSc3d1	provázený
postupnou	postupný	k2eAgFnSc7d1	postupná
změnou	změna	k1gFnSc7	změna
flóry	flóra	k1gFnSc2	flóra
a	a	k8xC	a
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
také	také	k9	také
poslední	poslední	k2eAgFnPc4d1	poslední
stopy	stopa	k1gFnPc4	stopa
osídlení	osídlení	k1gNnSc2	osídlení
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
míst	místo	k1gNnPc2	místo
jejího	její	k3xOp3gNnSc2	její
severního	severní	k2eAgNnSc2d1	severní
úpatí	úpatí	k1gNnSc2	úpatí
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc3d1	místní
rohovce	rohovka	k1gFnSc3	rohovka
už	už	k6eAd1	už
nesehrávaly	sehrávat	k5eNaImAgFnP	sehrávat
takovou	takový	k3xDgFnSc4	takový
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
v	v	k7c6	v
předcházejícím	předcházející	k2eAgNnSc6d1	předcházející
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
nalezené	nalezený	k2eAgFnSc2d1	nalezená
industrie	industrie	k1gFnSc2	industrie
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
suroviny	surovina	k1gFnSc2	surovina
-	-	kIx~	-
křišťálu	křišťál	k1gInSc2	křišťál
přineseného	přinesený	k2eAgInSc2d1	přinesený
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
porcelanitu	porcelanit	k1gInSc2	porcelanit
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
a	a	k8xC	a
obsidiánu	obsidián	k1gInSc2	obsidián
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
nejbližší	blízký	k2eAgInPc1d3	Nejbližší
zdroje	zdroj	k1gInPc1	zdroj
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
a	a	k8xC	a
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
import	import	k1gInSc4	import
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
zkoumaných	zkoumaný	k2eAgNnPc6d1	zkoumané
místech	místo	k1gNnPc6	místo
různá	různý	k2eAgNnPc4d1	různé
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
kosti	kost	k1gFnPc1	kost
mamuta	mamut	k1gMnSc2	mamut
<g/>
,	,	kIx,	,
nosorožce	nosorožec	k1gMnSc2	nosorožec
a	a	k8xC	a
soba	sob	k1gMnSc2	sob
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc1d1	západní
dominují	dominovat	k5eAaImIp3nP	dominovat
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
však	však	k9	však
chybí	chybět	k5eAaImIp3nP	chybět
lebky	lebka	k1gFnPc1	lebka
<g/>
,	,	kIx,	,
femury	femura	k1gFnPc1	femura
a	a	k8xC	a
humery	humera	k1gFnPc1	humera
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
části	část	k1gFnPc4	část
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
anatomické	anatomický	k2eAgFnPc4d1	anatomická
části	část	k1gFnPc4	část
lovci	lovec	k1gMnPc1	lovec
zřejmě	zřejmě	k6eAd1	zřejmě
odnášeli	odnášet	k5eAaImAgMnP	odnášet
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
místo	místo	k1gNnSc4	místo
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
artefaktů	artefakt	k1gInPc2	artefakt
i	i	k8xC	i
množství	množství	k1gNnSc2	množství
nalezené	nalezený	k2eAgFnSc2d1	nalezená
fauny	fauna	k1gFnSc2	fauna
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
spíše	spíše	k9	spíše
na	na	k7c4	na
specializované	specializovaný	k2eAgNnSc4d1	specializované
loviště	loviště	k1gNnSc4	loviště
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc4d1	vlastní
sídliště	sídliště	k1gNnSc4	sídliště
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
situováno	situovat	k5eAaBmNgNnS	situovat
někde	někde	k6eAd1	někde
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pravděpodobnou	pravděpodobný	k2eAgFnSc7d1	pravděpodobná
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
nalezných	nalezný	k2eAgFnPc2d1	nalezný
kostí	kost	k1gFnPc2	kost
je	být	k5eAaImIp3nS	být
dokladem	doklad	k1gInSc7	doklad
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
zvěř	zvěř	k1gFnSc1	zvěř
z	z	k7c2	z
plání	pláň	k1gFnPc2	pláň
východně	východně	k6eAd1	východně
od	od	k7c2	od
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
hnána	hnát	k5eAaImNgFnS	hnát
na	na	k7c4	na
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zřítila	zřítit	k5eAaPmAgFnS	zřítit
a	a	k8xC	a
pod	pod	k7c7	pod
skalou	skala	k1gFnSc7	skala
byla	být	k5eAaImAgFnS	být
dobita	dobit	k2eAgFnSc1d1	dobita
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
nalezišti	naleziště	k1gNnSc6	naleziště
La	la	k1gNnSc2	la
Roche	Roch	k1gMnSc2	Roch
de	de	k?	de
Solutré	Solutrý	k2eAgFnPc1d1	Solutrý
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
románovou	románový	k2eAgFnSc4d1	románová
fikci	fikce	k1gFnSc4	fikce
Adriena	Adriena	k1gFnSc1	Adriena
Arcelina	Arcelina	k1gFnSc1	Arcelina
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc2d1	francouzský
geologa	geolog	k1gMnSc2	geolog
a	a	k8xC	a
archeologa	archeolog	k1gMnSc2	archeolog
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
lokalitu	lokalita	k1gFnSc4	lokalita
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
a	a	k8xC	a
kromě	kromě	k7c2	kromě
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
zhodnocení	zhodnocení	k1gNnPc2	zhodnocení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výzkumů	výzkum	k1gInPc2	výzkum
napsal	napsat	k5eAaPmAgInS	napsat
i	i	k9	i
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
lovu	lov	k1gInSc2	lov
koní	koní	k2eAgInSc1d1	koní
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
"	"	kIx"	"
<g/>
vypůjčil	vypůjčit	k5eAaPmAgInS	vypůjčit
<g/>
"	"	kIx"	"
Eduard	Eduard	k1gMnSc1	Eduard
Štorch	Štorch	k1gMnSc1	Štorch
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nejoblíbenější	oblíbený	k2eAgFnSc6d3	nejoblíbenější
a	a	k8xC	a
nejslavnější	slavný	k2eAgFnSc6d3	nejslavnější
knize	kniha	k1gFnSc6	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lovci	lovec	k1gMnSc3	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
knihu	kniha	k1gFnSc4	kniha
psal	psát	k5eAaImAgInS	psát
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
znát	znát	k5eAaImF	znát
nálezovou	nálezový	k2eAgFnSc4d1	nálezová
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
Stránské	Stránské	k2eAgFnSc6d1	Stránské
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
odkryta	odkrýt	k5eAaPmNgFnS	odkrýt
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc6d1	kamenná
-	-	kIx~	-
eneolitu	eneolit	k1gInSc6	eneolit
začal	začít	k5eAaPmAgMnS	začít
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
před	před	k7c7	před
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
lid	lid	k1gInSc1	lid
kultury	kultura	k1gFnSc2	kultura
nálevkovitých	nálevkovitý	k2eAgInPc2d1	nálevkovitý
pohárů	pohár	k1gInPc2	pohár
opět	opět	k6eAd1	opět
využívat	využívat	k5eAaPmF	využívat
místní	místní	k2eAgInPc4d1	místní
rohovce	rohovec	k1gInPc4	rohovec
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgInPc1d1	povrchový
zdroje	zdroj	k1gInPc1	zdroj
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
společenská	společenský	k2eAgFnSc1d1	společenská
organizace	organizace	k1gFnSc1	organizace
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vznik	vznik	k1gInSc4	vznik
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
dílny	dílna	k1gFnSc2	dílna
na	na	k7c4	na
štípanou	štípaný	k2eAgFnSc4d1	štípaná
industrii	industrie	k1gFnSc4	industrie
<g/>
.	.	kIx.	.
</s>
<s>
Surovina	surovina	k1gFnSc1	surovina
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
vápencových	vápencový	k2eAgFnPc2d1	vápencová
skal	skála	k1gFnPc2	skála
zřejmě	zřejmě	k6eAd1	zřejmě
vylamována	vylamovat	k5eAaImNgFnS	vylamovat
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
blocích	blok	k1gInPc6	blok
nebo	nebo	k8xC	nebo
vykopávána	vykopáván	k2eAgFnSc1d1	vykopáván
ze	z	k7c2	z
zvětralé	zvětralý	k2eAgFnSc2d1	zvětralá
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
následně	následně	k6eAd1	následně
přepravována	přepravovat	k5eAaImNgFnS	přepravovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
dílny	dílna	k1gFnSc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
pazourkové	pazourkový	k2eAgFnPc4d1	Pazourková
hlízy	hlíza	k1gFnPc4	hlíza
žárem	žár	k1gInSc7	žár
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
ochlazováním	ochlazování	k1gNnSc7	ochlazování
vodou	voda	k1gFnSc7	voda
uvolňovány	uvolňován	k2eAgMnPc4d1	uvolňován
z	z	k7c2	z
vápencových	vápencový	k2eAgInPc2d1	vápencový
bloků	blok	k1gInPc2	blok
pomocí	pomocí	k7c2	pomocí
parohových	parohový	k2eAgInPc2d1	parohový
kopáčů	kopáč	k1gInPc2	kopáč
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získaná	získaný	k2eAgNnPc1d1	získané
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
metodou	metoda	k1gFnSc7	metoda
štípání	štípání	k1gNnSc2	štípání
zpracovávala	zpracovávat	k5eAaImAgFnS	zpracovávat
na	na	k7c4	na
žádané	žádaný	k2eAgInPc4d1	žádaný
polotovary	polotovar	k1gInPc4	polotovar
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
teprve	teprve	k6eAd1	teprve
retušováním	retušování	k1gNnPc3	retušování
vznikaly	vznikat	k5eAaImAgInP	vznikat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
nález	nález	k1gInSc1	nález
depotu	depot	k1gInSc2	depot
kamenných	kamenný	k2eAgInPc2d1	kamenný
štípaných	štípaný	k2eAgInPc2d1	štípaný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
:	:	kIx,	:
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
keramické	keramický	k2eAgFnSc6d1	keramická
nádobce	nádobka	k1gFnSc6	nádobka
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
43	[number]	k4	43
kamenných	kamenný	k2eAgInPc2d1	kamenný
úštěpů	úštěp	k1gInPc2	úštěp
ze	z	k7c2	z
suroviny	surovina	k1gFnSc2	surovina
různé	různý	k2eAgFnSc2d1	různá
provenience	provenience	k1gFnSc2	provenience
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologicky	paleontologicky	k6eAd1	paleontologicky
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
jeskyní	jeskyně	k1gFnSc7	jeskyně
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
medvědí	medvědí	k2eAgFnSc1d1	medvědí
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
dělníky	dělník	k1gMnPc4	dělník
při	při	k7c6	při
ražení	ražení	k1gNnSc6	ražení
štol	štola	k1gFnPc2	štola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
dělníků	dělník	k1gMnPc2	dělník
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
jako	jako	k9	jako
významné	významný	k2eAgNnSc4d1	významné
paleontologické	paleontologický	k2eAgNnSc4d1	paleontologické
naleziště	naleziště	k1gNnSc4	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
před	před	k7c7	před
60	[number]	k4	60
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
úkrytem	úkryt	k1gInSc7	úkryt
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
dostávali	dostávat	k5eAaImAgMnP	dostávat
komínem	komín	k1gInSc7	komín
a	a	k8xC	a
žili	žít	k5eAaImAgMnP	žít
tam	tam	k6eAd1	tam
po	po	k7c4	po
více	hodně	k6eAd2	hodně
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
nálezy	nález	k1gInPc1	nález
-	-	kIx~	-
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
hlíny	hlína	k1gFnPc1	hlína
z	z	k7c2	z
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
těchto	tento	k3xDgMnPc2	tento
hnědých	hnědý	k2eAgMnPc2d1	hnědý
medvědů	medvěd	k1gMnPc2	medvěd
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
lokalitu	lokalita	k1gFnSc4	lokalita
rozdělovala	rozdělovat	k5eAaImAgFnS	rozdělovat
řeka	řeka	k1gFnSc1	řeka
Svitava	Svitava	k1gFnSc1	Svitava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
několik	několik	k4yIc4	několik
slepých	slepý	k2eAgNnPc2d1	slepé
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tak	tak	k6eAd1	tak
různé	různý	k2eAgInPc4d1	různý
biotopy	biotop	k1gInPc4	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
nálezy	nález	k1gInPc7	nález
jsou	být	k5eAaImIp3nP	být
skořápky	skořápka	k1gFnSc2	skořápka
vajec	vejce	k1gNnPc2	vejce
rozličných	rozličný	k2eAgInPc2d1	rozličný
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
velkých	velký	k2eAgMnPc2d1	velký
koní	kůň	k1gMnPc2	kůň
apod.	apod.	kA	apod.
Jeskyně	jeskyně	k1gFnSc1	jeskyně
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
komín	komín	k1gInSc4	komín
byl	být	k5eAaImAgMnS	být
posléze	posléze	k6eAd1	posléze
zavalen	zavalit	k5eAaPmNgMnS	zavalit
a	a	k8xC	a
jeskyně	jeskyně	k1gFnSc1	jeskyně
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zapečetěná	zapečetěný	k2eAgFnSc1d1	zapečetěná
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
jeskyně	jeskyně	k1gFnSc1	jeskyně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
z	z	k7c2	z
nejspodnějšího	spodní	k2eAgNnSc2d3	nejspodnější
patra	patro	k1gNnSc2	patro
štol	štola	k1gFnPc2	štola
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hloubkový	hloubkový	k2eAgInSc4d1	hloubkový
deník	deník	k1gInSc4	deník
(	(	kIx(	(
<g/>
návštěvní	návštěvní	k2eAgFnSc1d1	návštěvní
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Docent	docent	k1gMnSc1	docent
KU	k	k7c3	k
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josef	Josef	k1gMnSc1	Josef
Woldřich	Woldřich	k1gMnSc1	Woldřich
je	být	k5eAaImIp3nS	být
sběratelem	sběratel	k1gMnSc7	sběratel
jurských	jurský	k2eAgFnPc2d1	jurská
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
několik	několik	k4yIc4	několik
vápencových	vápencový	k2eAgInPc2d1	vápencový
lomů	lom	k1gInPc2	lom
na	na	k7c4	na
štěrk	štěrk	k1gInSc4	štěrk
<g/>
,	,	kIx,	,
k	k	k7c3	k
pálení	pálení	k1gNnSc3	pálení
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Stránský	Stránský	k1gMnSc1	Stránský
vápenec	vápenec	k1gInSc1	vápenec
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
dekorační	dekorační	k2eAgInSc1d1	dekorační
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
a	a	k8xC	a
destrukce	destrukce	k1gFnSc1	destrukce
starých	starý	k2eAgFnPc2d1	stará
brněnských	brněnský	k2eAgFnPc2d1	brněnská
staveb	stavba	k1gFnPc2	stavba
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
(	(	kIx(	(
<g/>
Josefská	josefský	k2eAgFnSc1d1	Josefská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kašna	kašna	k1gFnSc1	kašna
Parnas	Parnas	k1gInSc1	Parnas
a	a	k8xC	a
Zderadův	Zderadův	k2eAgInSc1d1	Zderadův
sloup	sloup	k1gInSc1	sloup
na	na	k7c4	na
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Křenové	křenový	k2eAgNnSc4d1	Křenové
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
i	i	k9	i
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
základech	základ	k1gInPc6	základ
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
a	a	k8xC	a
minoritského	minoritský	k2eAgInSc2d1	minoritský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Těžen	těžen	k2eAgMnSc1d1	těžen
pro	pro	k7c4	pro
stavební	stavební	k2eAgInPc4d1	stavební
účely	účel	k1gInPc4	účel
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lámání	lámání	k1gNnSc6	lámání
kamene	kámen	k1gInSc2	kámen
dělníci	dělník	k1gMnPc1	dělník
občas	občas	k6eAd1	občas
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
pukliny	puklina	k1gFnPc4	puklina
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
jeskyně	jeskyně	k1gFnPc4	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
při	při	k7c6	při
odstřelu	odstřel	k1gInSc6	odstřel
lomové	lomový	k2eAgFnSc2d1	Lomová
stěny	stěna	k1gFnSc2	stěna
objevili	objevit	k5eAaPmAgMnP	objevit
jeskyni	jeskyně	k1gFnSc4	jeskyně
plnou	plný	k2eAgFnSc4d1	plná
jílovité	jílovitý	k2eAgMnPc4d1	jílovitý
hlíny	hlína	k1gFnSc2	hlína
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
vedla	vést	k5eAaImAgFnS	vést
1,5	[number]	k4	1,5
m	m	kA	m
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
9	[number]	k4	9
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústila	ústit	k5eAaImAgFnS	ústit
ve	v	k7c4	v
větší	veliký	k2eAgFnPc4d2	veliký
síně	síň	k1gFnPc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
doupě	doupě	k1gNnSc4	doupě
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kromě	kromě	k7c2	kromě
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
hyen	hyena	k1gFnPc2	hyena
<g/>
,	,	kIx,	,
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
turů	tur	k1gMnPc2	tur
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgInP	být
objeveny	objeven	k2eAgInPc1d1	objeven
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
ohniště	ohniště	k1gNnSc2	ohniště
a	a	k8xC	a
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
unikátní	unikátní	k2eAgInPc4d1	unikátní
nálezy	nález	k1gInPc4	nález
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dodnes	dodnes	k6eAd1	dodnes
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
analogii	analogie	k1gFnSc4	analogie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
meziledové	meziledový	k2eAgFnSc2d1	meziledová
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
etapy	etapa	k1gFnSc2	etapa
jeskyni	jeskyně	k1gFnSc4	jeskyně
obýval	obývat	k5eAaImAgMnS	obývat
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k9	již
jeskyně	jeskyně	k1gFnSc1	jeskyně
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
předválečných	předválečný	k2eAgNnPc2d1	předválečné
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
těžbou	těžba	k1gFnSc7	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
nedaleko	nedaleko	k7c2	nedaleko
ulice	ulice	k1gFnSc2	ulice
Stránská	Stránská	k1gFnSc1	Stránská
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
zakryt	zakrýt	k5eAaPmNgInS	zakrýt
těžkým	těžký	k2eAgInSc7d1	těžký
uzamčeným	uzamčený	k2eAgInSc7d1	uzamčený
poklopem	poklop	k1gInSc7	poklop
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
jeskyně	jeskyně	k1gFnSc1	jeskyně
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
vertikální	vertikální	k2eAgFnSc1d1	vertikální
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
průzkum	průzkum	k1gInSc1	průzkum
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
neznalého	znalý	k2eNgMnSc4d1	neznalý
i	i	k8xC	i
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejnižším	nízký	k2eAgInSc6d3	nejnižší
bodě	bod	k1gInSc6	bod
jeskyně	jeskyně	k1gFnSc2	jeskyně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
podzemní	podzemní	k2eAgNnSc4d1	podzemní
jezírko	jezírko	k1gNnSc4	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
komín	komín	k1gInSc1	komín
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
lana	lano	k1gNnSc2	lano
naprosto	naprosto	k6eAd1	naprosto
neschůdný	schůdný	k2eNgMnSc1d1	neschůdný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
deštích	dešť	k1gInPc6	dešť
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
z	z	k7c2	z
komínu	komín	k1gInSc2	komín
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
past	past	k1gFnSc1	past
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dolního	dolní	k2eAgNnSc2d1	dolní
jezírka	jezírko	k1gNnSc2	jezírko
cesta	cesta	k1gFnSc1	cesta
"	"	kIx"	"
<g/>
suchou	suchý	k2eAgFnSc7d1	suchá
nohou	noha	k1gFnSc7	noha
<g/>
"	"	kIx"	"
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jezírko	jezírko	k1gNnSc1	jezírko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
podzemím	podzemí	k1gNnSc7	podzemí
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
krasových	krasový	k2eAgFnPc6d1	krasová
jeskyních	jeskyně	k1gFnPc6	jeskyně
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
studená	studený	k2eAgFnSc1d1	studená
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
průzračná	průzračný	k2eAgFnSc1d1	průzračná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
přívalových	přívalový	k2eAgInPc2d1	přívalový
dešťů	dešť	k1gInPc2	dešť
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
byla	být	k5eAaImAgFnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
začleněním	začlenění	k1gNnSc7	začlenění
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
chráněných	chráněný	k2eAgFnPc2d1	chráněná
lokalit	lokalita	k1gFnPc2	lokalita
ČR	ČR	kA	ČR
značně	značně	k6eAd1	značně
využívána	využívat	k5eAaPmNgNnP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
stopy	stopa	k1gFnPc4	stopa
řady	řada	k1gFnSc2	řada
menších	malý	k2eAgInPc2d2	menší
vápencových	vápencový	k2eAgInPc2d1	vápencový
kamenolomů	kamenolom	k1gInPc2	kamenolom
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
narušena	narušit	k5eAaPmNgFnS	narušit
umělým	umělý	k2eAgNnSc7d1	umělé
hloubením	hloubení	k1gNnSc7	hloubení
štol	štola	k1gFnPc2	štola
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
blízké	blízký	k2eAgFnSc2d1	blízká
německé	německý	k2eAgFnSc2d1	německá
továrny	továrna	k1gFnSc2	továrna
Flugmotorenwerke	Flugmotorenwerke	k1gFnPc2	Flugmotorenwerke
Ostmark	Ostmark	k1gInSc1	Ostmark
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovšem	ovšem	k9	ovšem
přestavby	přestavba	k1gFnSc2	přestavba
nestihla	stihnout	k5eNaPmAgFnS	stihnout
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
náletem	nálet	k1gInSc7	nálet
amerických	americký	k2eAgNnPc2d1	americké
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Továrny	továrna	k1gFnPc1	továrna
vybudovaly	vybudovat	k5eAaPmAgFnP	vybudovat
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
systém	systém	k1gInSc1	systém
propojených	propojený	k2eAgFnPc2d1	propojená
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
bunkrů	bunkr	k1gInPc2	bunkr
a	a	k8xC	a
šachet	šachta	k1gFnPc2	šachta
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
horizontálních	horizontální	k2eAgNnPc6d1	horizontální
patrech	patro	k1gNnPc6	patro
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
plánovaly	plánovat	k5eAaImAgInP	plánovat
přesunout	přesunout	k5eAaPmF	přesunout
stroje	stroj	k1gInPc1	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
leteckých	letecký	k2eAgInPc2d1	letecký
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
ukrýt	ukrýt	k5eAaPmF	ukrýt
je	on	k3xPp3gInPc4	on
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
bombardováním	bombardování	k1gNnSc7	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
zčásti	zčásti	k6eAd1	zčásti
elektrifikována	elektrifikovat	k5eAaBmNgFnS	elektrifikovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
později	pozdě	k6eAd2	pozdě
ocenily	ocenit	k5eAaPmAgFnP	ocenit
zahrádkové	zahrádkový	k2eAgFnPc1d1	zahrádková
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
svahů	svah	k1gInPc2	svah
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
<g/>
.	.	kIx.	.
</s>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
Němci	Němec	k1gMnPc1	Němec
i	i	k9	i
po	po	k7c6	po
náletu	nálet	k1gInSc6	nálet
ve	v	k7c6	v
skalním	skalní	k2eAgInSc6d1	skalní
masivu	masiv	k1gInSc6	masiv
nadále	nadále	k6eAd1	nadále
budovali	budovat	k5eAaImAgMnP	budovat
kryty	kryt	k1gInPc4	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Padla	padnout	k5eAaPmAgFnS	padnout
i	i	k9	i
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
továrně	továrna	k1gFnSc6	továrna
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
rakety	raketa	k1gFnPc1	raketa
V-	V-	k1gFnSc1	V-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
ražení	ražení	k1gNnSc6	ražení
štol	štola	k1gFnPc2	štola
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
obsadila	obsadit	k5eAaPmAgFnS	obsadit
část	část	k1gFnSc1	část
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
protiatomový	protiatomový	k2eAgInSc4d1	protiatomový
kryt	kryt	k1gInSc4	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
převzal	převzít	k5eAaPmAgInS	převzít
závod	závod	k1gInSc1	závod
ZKL	ZKL	kA	ZKL
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stavbu	stavba	k1gFnSc4	stavba
dokončil	dokončit	k5eAaPmAgMnS	dokončit
a	a	k8xC	a
vybavil	vybavit	k5eAaPmAgInS	vybavit
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
velitelské	velitelský	k2eAgNnSc1d1	velitelské
stanoviště	stanoviště	k1gNnSc1	stanoviště
civilní	civilní	k2eAgFnSc2d1	civilní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dnes	dnes	k6eAd1	dnes
leží	ležet	k5eAaImIp3nS	ležet
připraveno	připravit	k5eAaPmNgNnS	připravit
za	za	k7c7	za
zakovanými	zakovaný	k2eAgFnPc7d1	zakovaná
železnými	železný	k2eAgFnPc7d1	železná
dveřmi	dveře	k1gFnPc7	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Chodby	chodba	k1gFnPc1	chodba
nejhornějšího	horní	k2eAgNnSc2d3	nejhořejší
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xS	jako
kryty	kryt	k1gInPc4	kryt
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
136	[number]	k4	136
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
štol	štola	k1gFnPc2	štola
středního	střední	k2eAgNnSc2d1	střední
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
1	[number]	k4	1
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c7	za
krytem	kryt	k1gInSc7	kryt
civilní	civilní	k2eAgFnSc2d1	civilní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jehož	jehož	k3xOyRp3gInSc4	jehož
strop	strop	k1gInSc4	strop
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
přeplazit	přeplazit	k5eAaPmF	přeplazit
<g/>
.	.	kIx.	.
</s>
<s>
Nejspodnější	spodní	k2eAgNnSc1d3	nejspodnější
patro	patro	k1gNnSc1	patro
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
patro	patro	k1gNnSc4	patro
bezprostředně	bezprostředně	k6eAd1	bezprostředně
navazuje	navazovat	k5eAaImIp3nS	navazovat
jeskyně	jeskyně	k1gFnSc1	jeskyně
o	o	k7c6	o
souhrnné	souhrnný	k2eAgFnSc6d1	souhrnná
délce	délka	k1gFnSc6	délka
chodeb	chodba	k1gFnPc2	chodba
asi	asi	k9	asi
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
přístupné	přístupný	k2eAgFnPc1d1	přístupná
jeskyně	jeskyně	k1gFnPc1	jeskyně
a	a	k8xC	a
štoly	štola	k1gFnPc1	štola
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
úkrytem	úkryt	k1gInSc7	úkryt
bezdomovců	bezdomovec	k1gMnPc2	bezdomovec
<g/>
,	,	kIx,	,
narkomanů	narkoman	k1gMnPc2	narkoman
a	a	k8xC	a
vyznavačů	vyznavač	k1gMnPc2	vyznavač
různých	různý	k2eAgInPc2d1	různý
vír	vír	k1gInSc4	vír
<g/>
.	.	kIx.	.
kavyl	kavyl	k1gInSc1	kavyl
koniklec	koniklec	k1gInSc1	koniklec
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
bělozářka	bělozářka	k1gFnSc1	bělozářka
větvená	větvený	k2eAgFnSc1d1	větvená
čilimník	čilimník	k1gInSc4	čilimník
černající	černající	k2eAgInSc1d1	černající
čilimník	čilimník	k1gInSc1	čilimník
řezenský	řezenský	k2eAgInSc1d1	řezenský
rozrazil	rozrazil	k1gInSc1	rozrazil
rozprostřený	rozprostřený	k2eAgInSc1d1	rozprostřený
osívka	osívka	k1gFnSc1	osívka
kulatoplodá	kulatoplodý	k2eAgFnSc1d1	kulatoplodá
mochna	mochna	k1gFnSc1	mochna
písečná	písečný	k2eAgFnSc1d1	písečná
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
ostřice	ostřice	k1gFnSc2	ostřice
nízká	nízký	k2eAgFnSc1d1	nízká
kostřava	kostřava	k1gFnSc1	kostřava
waliská	waliskat	k5eAaPmIp3nS	waliskat
pěchava	pěchava	k1gFnSc1	pěchava
vápnomilná	vápnomilný	k2eAgFnSc1d1	vápnomilná
šalvěj	šalvěj	k1gFnSc1	šalvěj
luční	luční	k2eAgFnSc4d1	luční
šalvěj	šalvěj	k1gFnSc4	šalvěj
hajní	hajní	k2eAgInSc4d1	hajní
šalvěj	šalvěj	k1gInSc4	šalvěj
přeslenitá	přeslenitý	k2eAgFnSc1d1	přeslenitá
kuřička	kuřička	k1gFnSc1	kuřička
svazčitá	svazčitý	k2eAgFnSc1d1	svazčitá
len	len	k1gInSc4	len
tenkolistý	tenkolistý	k2eAgInSc4d1	tenkolistý
<g />
.	.	kIx.	.
</s>
<s>
hvězdnice	hvězdnice	k1gFnSc1	hvězdnice
chlumní	chlumní	k2eAgFnSc1d1	chlumní
starček	starček	k1gInSc4	starček
přímětník	přímětník	k1gInSc1	přímětník
jaterník	jaterník	k1gInSc1	jaterník
podléška	podléška	k1gFnSc1	podléška
lomikámen	lomikámen	k1gInSc4	lomikámen
trojprstý	trojprstý	k2eAgInSc1d1	trojprstý
vičenec	vičenec	k1gInSc1	vičenec
ligrus	ligrus	k1gInSc4	ligrus
lnice	lnice	k1gFnSc2	lnice
květel	květel	k1gInSc1	květel
plamének	plamének	k1gInSc1	plamének
přímý	přímý	k2eAgInSc1d1	přímý
hvozdík	hvozdík	k1gInSc1	hvozdík
pontederův	pontederův	k2eAgInSc1d1	pontederův
pumpava	pumpava	k1gFnSc1	pumpava
rozpuková	rozpukový	k2eAgFnSc1d1	rozpuková
trýzel	trýzel	k1gInSc4	trýzel
vonný	vonný	k2eAgInSc1d1	vonný
pryšec	pryšec	k1gInSc1	pryšec
chvojka	chvojka	k1gFnSc1	chvojka
pryšec	pryšec	k1gInSc4	pryšec
mnohobarvý	mnohobarvý	k2eAgInSc1d1	mnohobarvý
plevel	plevel	k1gInSc1	plevel
okoličnatý	okoličnatý	k2eAgInSc1d1	okoličnatý
čistec	čistec	k1gInSc4	čistec
přímý	přímý	k2eAgInSc4d1	přímý
penízek	penízek	k1gInSc4	penízek
prorostlý	prorostlý	k2eAgInSc1d1	prorostlý
netřesk	netřesk	k1gInSc1	netřesk
výběžkatý	výběžkatý	k2eAgInSc4d1	výběžkatý
jitrocel	jitrocel	k1gInSc4	jitrocel
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
devaterník	devaterník	k1gInSc1	devaterník
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
česnek	česnek	k1gInSc4	česnek
šerý	šerý	k2eAgInSc4d1	šerý
horský	horský	k2eAgInSc4d1	horský
česnek	česnek	k1gInSc4	česnek
žlutý	žlutý	k2eAgInSc4d1	žlutý
osívka	osívka	k1gFnSc1	osívka
kulatoplodá	kulatoplodý	k2eAgFnSc1d1	kulatoplodá
hrachor	hrachor	k1gInSc4	hrachor
lesní	lesní	k2eAgInSc1d1	lesní
zvonek	zvonek	k1gInSc1	zvonek
klubkatý	klubkatý	k2eAgInSc1d1	klubkatý
marulka	marulka	k1gFnSc1	marulka
pamětník	pamětník	k1gMnSc1	pamětník
zlatovlásek	zlatovlásek	k1gMnSc1	zlatovlásek
<g />
.	.	kIx.	.
</s>
<s>
obecný	obecný	k2eAgInSc1d1	obecný
zapalice	zapalice	k1gFnPc4	zapalice
žluťochovitá	žluťochovitý	k2eAgFnSc1d1	žluťochovitý
čistec	čistec	k1gInSc1	čistec
přímý	přímý	k2eAgInSc1d1	přímý
líska	líska	k1gFnSc1	líska
obecná	obecná	k1gFnSc1	obecná
dřín	dřín	k1gInSc4	dřín
jarní	jarní	k2eAgMnSc1d1	jarní
netopýr	netopýr	k1gMnSc1	netopýr
velký	velký	k2eAgMnSc1d1	velký
netopýr	netopýr	k1gMnSc1	netopýr
dlouhouchý	dlouhouchý	k2eAgMnSc1d1	dlouhouchý
užovka	užovka	k1gFnSc1	užovka
hladká	hladký	k2eAgFnSc1d1	hladká
užovka	užovka	k1gFnSc1	užovka
obojková	obojkový	k2eAgFnSc1d1	obojková
slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgInSc1d1	křehký
ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
ještěrka	ještěrka	k1gFnSc1	ještěrka
zelená	zelená	k1gFnSc1	zelená
ropucha	ropucha	k1gFnSc1	ropucha
obecná	obecná	k1gFnSc1	obecná
rosnička	rosnička	k1gFnSc1	rosnička
zelená	zelenat	k5eAaImIp3nS	zelenat
skřivan	skřivan	k1gMnSc1	skřivan
polní	polní	k2eAgFnSc4d1	polní
koroptev	koroptev	k1gFnSc4	koroptev
polní	polní	k2eAgFnSc1d1	polní
křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgMnSc1d1	polní
konipas	konipas	k1gMnSc1	konipas
bílý	bílý	k1gMnSc1	bílý
kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
pěnice	pěnice	k1gFnSc1	pěnice
kos	kosa	k1gFnPc2	kosa
černý	černý	k2eAgInSc1d1	černý
zvonek	zvonek	k1gInSc1	zvonek
zelený	zelený	k2eAgMnSc1d1	zelený
stehlík	stehlík	k1gMnSc1	stehlík
obecný	obecný	k2eAgInSc4d1	obecný
pěnkava	pěnkava	k1gFnSc1	pěnkava
obecná	obecná	k1gFnSc1	obecná
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc2d1	skalní
<g />
.	.	kIx.	.
</s>
<s>
zajíc	zajíc	k1gMnSc1	zajíc
polní	polní	k2eAgFnSc2d1	polní
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
rejsek	rejsek	k1gInSc4	rejsek
obecný	obecný	k2eAgInSc1d1	obecný
rejsek	rejsek	k1gInSc1	rejsek
malý	malý	k2eAgMnSc1d1	malý
Karel	Karel	k1gMnSc1	Karel
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Sklenářová	Sklenářová	k1gFnSc1	Sklenářová
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Slabina	slabina	k1gFnSc1	slabina
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
pravěku	pravěk	k1gInSc2	pravěk
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
-	-	kIx~	-
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
na	na	k7c4	na
mojeBrno	mojeBrno	k1gNnSc4	mojeBrno
<g/>
.	.	kIx.	.
<g/>
jecool	jecool	k1gInSc1	jecool
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
-	-	kIx~	-
protiatomový	protiatomový	k2eAgInSc1d1	protiatomový
kryt	kryt	k1gInSc1	kryt
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
-	-	kIx~	-
štoly	štola	k1gFnSc2	štola
</s>
