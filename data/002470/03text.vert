<s>
Paul	Paul	k1gMnSc1	Paul
Marie	Maria	k1gFnSc2	Maria
Verlaine	Verlain	k1gInSc5	Verlain
[	[	kIx(	[
<g/>
Verlén	Verlén	k1gInSc4	Verlén
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1844	[number]	k4	1844
Mety	meta	k1gFnSc2	meta
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1896	[number]	k4	1896
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
prokletých	prokletý	k2eAgMnPc2d1	prokletý
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
po	po	k7c6	po
seznámení	seznámení	k1gNnSc6	seznámení
s	s	k7c7	s
Rimbaudem	Rimbaud	k1gInSc7	Rimbaud
opustil	opustit	k5eAaPmAgMnS	opustit
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
žít	žít	k5eAaImF	žít
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
velmi	velmi	k6eAd1	velmi
závislý	závislý	k2eAgMnSc1d1	závislý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
propadl	propadnout	k5eAaPmAgInS	propadnout
tuláctví	tuláctví	k1gNnSc4	tuláctví
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
na	na	k7c4	na
Rimbauda	Rimbaud	k1gMnSc4	Rimbaud
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
(	(	kIx(	(
<g/>
rozešli	rozejít	k5eAaPmAgMnP	rozejít
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
prodělal	prodělat	k5eAaPmAgInS	prodělat
léčbu	léčba	k1gFnSc4	léčba
<g/>
,	,	kIx,	,
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1894	[number]	k4	1894
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Leconte	Lecont	k1gInSc5	Lecont
de	de	k?	de
Lisle	Lisle	k1gNnPc7	Lisle
<g/>
.	.	kIx.	.
</s>
<s>
Verlaine	Verlainout	k5eAaPmIp3nS	Verlainout
chtěl	chtít	k5eAaImAgMnS	chtít
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
uvolněné	uvolněný	k2eAgInPc4d1	uvolněný
14	[number]	k4	14
<g/>
.	.	kIx.	.
křeslo	křeslo	k1gNnSc4	křeslo
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
úmyslu	úmysl	k1gInSc2	úmysl
odrazován	odrazován	k2eAgMnSc1d1	odrazován
a	a	k8xC	a
na	na	k7c4	na
kandidaturu	kandidatura	k1gFnSc4	kandidatura
nakonec	nakonec	k6eAd1	nakonec
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Smrtí	smrtit	k5eAaImIp3nS	smrtit
de	de	k?	de
Lisleovou	Lisleův	k2eAgFnSc7d1	Lisleův
se	se	k3xPyFc4	se
ale	ale	k9	ale
rovněž	rovněž	k9	rovněž
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
titul	titul	k1gInSc4	titul
Kníže	kníže	k1gMnSc1	kníže
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
Prince	princ	k1gMnSc2	princ
des	des	k1gNnSc2	des
poè	poè	k?	poè
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
anketa	anketa	k1gFnSc1	anketa
na	na	k7c4	na
udělení	udělení	k1gNnSc4	udělení
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
organizoval	organizovat	k5eAaBmAgMnS	organizovat
Georges	Georges	k1gMnSc1	Georges
Docquois	Docquois	k1gFnSc2	Docquois
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
osloveno	oslovit	k5eAaPmNgNnS	oslovit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
sta	sto	k4xCgNnPc4	sto
literátů	literát	k1gMnPc2	literát
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc4	odpověď
zaslalo	zaslat	k5eAaPmAgNnS	zaslat
189	[number]	k4	189
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
(	(	kIx(	(
<g/>
70	[number]	k4	70
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
Verlaine	Verlain	k1gInSc5	Verlain
<g/>
,	,	kIx,	,
následovali	následovat	k5eAaImAgMnP	následovat
<g/>
:	:	kIx,	:
José-María	José-María	k1gFnSc1	José-María
de	de	k?	de
Heredia	Heredium	k1gNnSc2	Heredium
(	(	kIx(	(
<g/>
38	[number]	k4	38
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sully	Sulla	k1gMnSc2	Sulla
Prudhomme	Prudhomme	k1gMnSc2	Prudhomme
(	(	kIx(	(
<g/>
36	[number]	k4	36
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stéphane	Stéphan	k1gMnSc5	Stéphan
Mallarmé	Mallarmý	k2eAgInPc4d1	Mallarmý
(	(	kIx(	(
<g/>
36	[number]	k4	36
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
François	François	k1gFnSc1	François
Coppée	Coppée	k1gFnSc1	Coppée
(	(	kIx(	(
<g/>
12	[number]	k4	12
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sbírkách	sbírka	k1gFnPc6	sbírka
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
citové	citový	k2eAgInPc4d1	citový
a	a	k8xC	a
náladové	náladový	k2eAgInPc4d1	náladový
odstíny	odstín	k1gInPc4	odstín
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
a	a	k8xC	a
představami	představa	k1gFnPc7	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
poezii	poezie	k1gFnSc6	poezie
se	se	k3xPyFc4	se
pocity	pocit	k1gInPc1	pocit
vyvozují	vyvozovat	k5eAaImIp3nP	vyvozovat
z	z	k7c2	z
dojmů	dojem	k1gInPc2	dojem
a	a	k8xC	a
doteků	dotek	k1gInPc2	dotek
(	(	kIx(	(
<g/>
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
verších	verš	k1gInPc6	verš
je	být	k5eAaImIp3nS	být
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
způsobem	způsob	k1gInSc7	způsob
zdůrazněn	zdůrazněn	k2eAgInSc1d1	zdůrazněn
rytmus	rytmus	k1gInSc1	rytmus
a	a	k8xC	a
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
toho	ten	k3xDgNnSc2	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasický	klasický	k2eAgInSc4d1	klasický
verš	verš	k1gInSc4	verš
nahradil	nahradit	k5eAaPmAgMnS	nahradit
veršem	verš	k1gInSc7	verš
o	o	k7c6	o
lichém	lichý	k2eAgInSc6d1	lichý
počtu	počet	k1gInSc6	počet
slabik	slabika	k1gFnPc2	slabika
a	a	k8xC	a
rým	rýma	k1gFnPc2	rýma
asonancí	asonance	k1gFnPc2	asonance
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
poezie	poezie	k1gFnSc2	poezie
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
mnoho	mnoho	k6eAd1	mnoho
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Saturnské	saturnský	k2eAgFnPc1d1	saturnská
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Poemes	Poemesa	k1gFnPc2	Poemesa
saturniens	saturniensa	k1gFnPc2	saturniensa
<g/>
)	)	kIx)	)
Galantní	galantní	k2eAgFnPc1d1	galantní
slavnosti	slavnost	k1gFnPc1	slavnost
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Fete	Fete	k1gInSc1	Fete
galantes	galantes	k1gInSc1	galantes
<g/>
)	)	kIx)	)
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Bonne	Bonn	k1gInSc5	Bonn
chanson	chanson	k1gInSc4	chanson
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
prostý	prostý	k2eAgInSc4d1	prostý
a	a	k8xC	a
klidný	klidný	k2eAgInSc4d1	klidný
život	život	k1gInSc4	život
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Mathildou	Mathilda	k1gFnSc7	Mathilda
Mauteovou	Mauteův	k2eAgFnSc7d1	Mauteův
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
před	před	k7c7	před
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
Rimbaudem	Rimbaud	k1gInSc7	Rimbaud
<g/>
)	)	kIx)	)
Moudrost	moudrost	k1gFnSc1	moudrost
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sagesse	Sagesse	k1gFnSc1	Sagesse
<g/>
)	)	kIx)	)
Kdysi	kdysi	k6eAd1	kdysi
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Jadis	Jadis	k1gInSc1	Jadis
et	et	k?	et
Naguere	Naguer	k1gInSc5	Naguer
<g/>
)	)	kIx)	)
Poetické	poetický	k2eAgNnSc1d1	poetické
umění	umění	k1gNnSc1	umění
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
definici	definice	k1gFnSc4	definice
symbolistické	symbolistický	k2eAgFnSc2d1	symbolistická
poetiky	poetika	k1gFnSc2	poetika
Romance	romance	k1gFnSc2	romance
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Romances	Romances	k1gInSc1	Romances
sans	sans	k1gInSc1	sans
paroles	paroles	k1gInSc4	paroles
<g/>
)	)	kIx)	)
-	-	kIx~	-
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
melodičnost	melodičnost	k1gFnSc1	melodičnost
<g/>
,	,	kIx,	,
zvukomalbu	zvukomalba	k1gFnSc4	zvukomalba
<g/>
,	,	kIx,	,
vyjádření	vyjádření	k1gNnSc4	vyjádření
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
nálad	nálada	k1gFnPc2	nálada
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
spojené	spojený	k2eAgInPc4d1	spojený
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Psáno	psán	k2eAgNnSc1d1	psáno
v	v	k7c6	v
době	doba	k1gFnSc6	doba
soužití	soužití	k1gNnSc2	soužití
s	s	k7c7	s
Rimbaudem	Rimbaud	k1gInSc7	Rimbaud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
paralel	paralela	k1gFnPc2	paralela
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
životy	život	k1gInPc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Amour	Amour	k1gMnSc1	Amour
<g/>
)	)	kIx)	)
Štěstí	štěstit	k5eAaImIp3nS	štěstit
Paralelně	paralelně	k6eAd1	paralelně
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
Ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Femmes	Femmes	k1gInSc1	Femmes
<g/>
)	)	kIx)	)
Písně	píseň	k1gFnPc1	píseň
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Intimní	intimní	k2eAgFnSc2d1	intimní
liturgie	liturgie	k1gFnSc2	liturgie
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gFnSc1	můj
nemocnice	nemocnice	k1gFnSc1	nemocnice
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Mes	Mes	k1gFnSc1	Mes
hôpitaux	hôpitaux	k1gInSc1	hôpitaux
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gNnSc1	můj
vězení	vězení	k1gNnSc1	vězení
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Mes	Mes	k1gFnSc1	Mes
prisons	prisons	k1gInSc1	prisons
<g/>
)	)	kIx)	)
Vyznání	vyznání	k1gNnSc1	vyznání
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Confessions	Confessions	k1gInSc1	Confessions
<g/>
)	)	kIx)	)
Božská	božský	k2eAgFnSc1d1	božská
láska	láska	k1gFnSc1	láska
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
básníci	básník	k1gMnPc1	básník
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
eseji	esej	k1gInSc6	esej
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
esej	esej	k1gInSc1	esej
velmi	velmi	k6eAd1	velmi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
symbolismus	symbolismus	k1gInSc1	symbolismus
<g/>
.	.	kIx.	.
</s>
