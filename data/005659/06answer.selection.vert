<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
příkop	příkop	k1gInSc1	příkop
<g/>
,	,	kIx,	,
při	při	k7c6	při
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Severního	severní	k2eAgInSc2d1	severní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
10	[number]	k4	10
994	[number]	k4	994
metrů	metr	k1gInPc2	metr
nejhlubším	hluboký	k2eAgNnSc7d3	nejhlubší
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
