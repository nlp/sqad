<p>
<s>
Točkolotoč	Točkolotočit	k5eAaPmRp2nS	Točkolotočit
je	on	k3xPp3gNnSc4	on
česká	český	k2eAgFnSc1d1	Česká
folková	folkový	k2eAgFnSc1d1	folková
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
ve	v	k7c6	v
Svitavách	Svitava	k1gFnPc6	Svitava
příslušníci	příslušník	k1gMnPc1	příslušník
romské	romský	k2eAgFnSc2d1	romská
rodiny	rodina	k1gFnSc2	rodina
Peštů	Pešta	k1gMnPc2	Pešta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
rockových	rockový	k2eAgFnPc6d1	rocková
skupinách	skupina	k1gFnPc6	skupina
The	The	k1gFnSc2	The
Raffaels	Raffaels	k1gInSc1	Raffaels
a	a	k8xC	a
Verze	verze	k1gFnSc1	verze
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Porta	porta	k1gFnSc1	porta
skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
poroty	porota	k1gFnSc2	porota
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
vítězům	vítěz	k1gMnPc3	vítěz
interpretační	interpretační	k2eAgFnSc2d1	interpretační
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
hudbu	hudba	k1gFnSc4	hudba
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
ve	v	k7c6	v
sleevenote	sleevenot	k1gInSc5	sleevenot
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
Čhave	Čhaev	k1gFnSc2	Čhaev
Svitevendar	Svitevendar	k1gMnSc1	Svitevendar
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Točkolotoč	Točkolotoč	k1gFnSc1	Točkolotoč
překvapivě	překvapivě	k6eAd1	překvapivě
rychle	rychle	k6eAd1	rychle
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
osobitý	osobitý	k2eAgInSc4d1	osobitý
autorský	autorský	k2eAgInSc4d1	autorský
a	a	k8xC	a
interpretační	interpretační	k2eAgInSc4d1	interpretační
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
prvky	prvek	k1gInPc1	prvek
romského	romský	k2eAgMnSc2d1	romský
a	a	k8xC	a
moravského	moravský	k2eAgInSc2d1	moravský
folklóru	folklór	k1gInSc2	folklór
<g/>
,	,	kIx,	,
tuzemského	tuzemský	k2eAgInSc2d1	tuzemský
folku	folk	k1gInSc2	folk
i	i	k8xC	i
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozeznatelný	rozeznatelný	k2eAgMnSc1d1	rozeznatelný
podle	podle	k7c2	podle
obou	dva	k4xCgMnPc2	dva
sólových	sólový	k2eAgMnPc2d1	sólový
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
Gejzy	Gejza	k1gFnPc1	Gejza
Pešty	Pešta	k1gMnSc2	Pešta
a	a	k8xC	a
Milana	Milan	k1gMnSc2	Milan
Bendíka	Bendík	k1gMnSc2	Bendík
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
romských	romský	k2eAgInPc2d1	romský
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
melodiky	melodika	k1gFnPc1	melodika
Jiřího	Jiří	k1gMnSc2	Jiří
kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
Mirkovy	Mirkův	k2eAgFnSc2d1	Mirkova
mandolíny	mandolína	k1gFnSc2	mandolína
a	a	k8xC	a
efektních	efektní	k2eAgInPc2d1	efektní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
spontánně	spontánně	k6eAd1	spontánně
působících	působící	k2eAgInPc2d1	působící
mnohovrstevnatých	mnohovrstevnatý	k2eAgInPc2d1	mnohovrstevnatý
rytmů	rytmus	k1gInPc2	rytmus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
skupina	skupina	k1gFnSc1	skupina
prošla	projít	k5eAaPmAgFnS	projít
personálními	personální	k2eAgFnPc7d1	personální
změnami	změna	k1gFnPc7	změna
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
neromští	romský	k2eNgMnPc1d1	neromský
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
v	v	k7c6	v
novějších	nový	k2eAgFnPc6d2	novější
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k6eAd1	také
inspirace	inspirace	k1gFnSc2	inspirace
styly	styl	k1gInPc1	styl
jako	jako	k8xC	jako
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
country	country	k2eAgInSc1d1	country
nebo	nebo	k8xC	nebo
reggae	reggae	k6eAd1	reggae
<g/>
.	.	kIx.	.
</s>
<s>
Točkolotoč	Točkolotočit	k5eAaPmRp2nS	Točkolotočit
úspěšně	úspěšně	k6eAd1	úspěšně
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c4	na
Smithsonian	Smithsonian	k1gInSc4	Smithsonian
Folklife	Folklif	k1gInSc5	Folklif
Festivalu	festival	k1gInSc3	festival
<g/>
,	,	kIx,	,
uznale	uznale	k6eAd1	uznale
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
Frank	Frank	k1gMnSc1	Frank
Zappa	Zapp	k1gMnSc4	Zapp
i	i	k9	i
Paul	Paul	k1gMnSc1	Paul
Simon	Simon	k1gMnSc1	Simon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Gejza	Gejza	k1gFnSc1	Gejza
Pešta	Pešta	k1gMnSc1	Pešta
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pešta	Pešta	k1gMnSc1	Pešta
–	–	k?	–
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Pešta	Pešta	k1gMnSc1	Pešta
–	–	k?	–
perkuse	perkuse	k1gFnSc1	perkuse
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Pešta	Pešta	k1gMnSc1	Pešta
–	–	k?	–
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
Vaško	Vaško	k1gNnSc1	Vaško
–	–	k?	–
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Janko	Janko	k1gMnSc1	Janko
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Bolcek	Bolcka	k1gFnPc2	Bolcka
–	–	k?	–
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Polák	Polák	k1gMnSc1	Polák
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Frýbort	Frýbort	k1gInSc1	Frýbort
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Čhave	Čhaev	k1gFnSc2	Čhaev
Svitavendar	Svitavendara	k1gFnPc2	Svitavendara
(	(	kIx(	(
<g/>
Kluci	kluk	k1gMnPc1	kluk
ze	z	k7c2	z
Svitav	Svitava	k1gFnPc2	Svitava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Kale	kale	k6eAd1	kale
bala	bala	k6eAd1	bala
<g/>
,	,	kIx,	,
kale	kale	k6eAd1	kale
jakha	jakha	k1gFnSc1	jakha
(	(	kIx(	(
<g/>
Černé	Černé	k2eAgInPc1d1	Černé
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
černé	černý	k2eAgInPc1d1	černý
oči	oko	k1gNnPc4	oko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
So	So	kA	So
has	hasit	k5eAaImRp2nS	hasit
oda	oda	k?	oda
has	hasit	k5eAaImRp2nS	hasit
(	(	kIx(	(
<g/>
Co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
