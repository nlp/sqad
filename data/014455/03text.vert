<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
Název	název	k1gInSc1
</s>
<s>
Chelsea	Chelsea	k6eAd1
Football	Football	k1gInSc1
Club	club	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
The	The	k?
Blues	blues	k1gNnSc1
Země	zem	k1gFnSc2
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
Město	město	k1gNnSc1
</s>
<s>
Londýn	Londýn	k1gInSc1
(	(	kIx(
<g/>
Fulham	Fulham	k1gInSc1
<g/>
)	)	kIx)
Založen	založen	k2eAgInSc1d1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1905	#num#	k4
Asociace	asociace	k1gFnSc2
</s>
<s>
Football	Football	k1gInSc1
Association	Association	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_chelsea	_chelsea	k1gFnSc1
<g/>
2021	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_chelsea	_chelsea	k1gMnSc1
<g/>
2021	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_chelsea	_chelsea	k1gFnSc1
<g/>
2021	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Stamford	Stamford	k6eAd1
Bridge	Bridge	k1gFnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
41	#num#	k4
841	#num#	k4
Vedení	vedení	k1gNnPc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Abramovič	Abramovič	k1gMnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Bruce	Bruce	k1gMnSc1
Buck	Buck	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Tuchel	Tuchel	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Anglie	Anglie	k1gFnSc2
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
FA	fa	kA
Cup	cup	k1gInSc4
<g/>
5	#num#	k4
<g/>
×	×	k?
EFL	EFL	kA
Cup	cup	k1gInSc4
<g/>
4	#num#	k4
<g/>
×	×	k?
Community	Communita	k1gFnSc2
Shield	Shield	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
2	#num#	k4
<g/>
×	×	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
2	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
1	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1998	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2012	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2013	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2019	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
2012	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
/	/	kIx~
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
1955	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
</s>
<s>
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gFnSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
jihozápadní	jihozápadní	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Fulham	Fulham	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc1d1	
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnPc1d1
a	a	k8xC
bílá	bílý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Jejími	její	k3xOp3gMnPc7
rivaly	rival	k1gMnPc7
jsou	být	k5eAaImIp3nP
další	další	k2eAgInPc1d1
kluby	klub	k1gInPc1
z	z	k7c2
Londýna	Londýn	k1gInSc2
jako	jako	k8xC,k8xS
Arsenal	Arsenal	k1gFnSc4
a	a	k8xC
Tottenham	Tottenham	k1gInSc4
Hotspur	Hotspura	k1gFnPc2
<g/>
,	,	kIx,
vzájemná	vzájemný	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
těchto	tento	k3xDgInPc2
klubů	klub	k1gInPc2
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
ještě	ještě	k6eAd1
intenzivnější	intenzivní	k2eAgMnSc1d2
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
obou	dva	k4xCgFnPc6
vůči	vůči	k7c3
Chelsea	Chelse	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rivalita	rivalita	k1gFnSc1
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
United	United	k1gMnSc1
vznikla	vzniknout	k5eAaPmAgFnS
na	na	k7c6
základě	základ	k1gInSc6
ambicí	ambice	k1gFnSc7
obou	dva	k4xCgInPc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vyzývatelem	vyzývatel	k1gMnSc7
United	United	k1gInSc1
jako	jako	k9
dříve	dříve	k6eAd2
dominantní	dominantní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
vnímali	vnímat	k5eAaImAgMnP
její	její	k3xOp3gMnPc1
příznivci	příznivec	k1gMnPc1
jako	jako	k8xC,k8xS
rivala	rival	k1gMnSc4
též	též	k9
Leeds	Leeds	k1gInSc4
United	United	k1gInSc1
<g/>
,	,	kIx,
vzestup	vzestup	k1gInSc1
na	na	k7c6
evropském	evropský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
zase	zase	k9
několikrát	několikrát	k6eAd1
dostal	dostat	k5eAaPmAgMnS
Chelsea	Chelsea	k1gMnSc1
do	do	k7c2
křížku	křížek	k1gInSc2
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
stadionu	stadion	k1gInSc6
Stamford	Stamforda	k1gFnPc2
Bridge	Bridge	k1gInSc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
41	#num#	k4
853	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
získal	získat	k5eAaPmAgMnS
po	po	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
mostů	most	k1gInPc2
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Temži	Temže	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
stál	stát	k5eAaImAgInS
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Chelsea	Chelse	k1gInSc2
poblíž	poblíž	k6eAd1
dnešního	dnešní	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
klubu	klub	k1gInSc2
</s>
<s>
Založení	založení	k1gNnSc1
a	a	k8xC
první	první	k4xOgInPc4
roky	rok	k1gInPc4
</s>
<s>
Roku	rok	k1gInSc2
1896	#num#	k4
koupil	koupit	k5eAaPmAgMnS
obchodník	obchodník	k1gMnSc1
s	s	k7c7
nemovitostmi	nemovitost	k1gFnPc7
Henry	Henry	k1gMnSc1
Augustus	Augustus	k1gMnSc1
"	"	kIx"
<g/>
Gus	Gus	k1gMnSc1
<g/>
"	"	kIx"
Mears	Mears	k1gInSc1
spolu	spolu	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Josephem	Joseph	k1gInSc7
atletické	atletický	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridg	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
sousedící	sousedící	k2eAgFnSc4d1
zahradu	zahrada	k1gFnSc4
jako	jako	k8xS,k8xC
základ	základ	k1gInSc4
pro	pro	k7c4
fotbalový	fotbalový	k2eAgInSc4d1
stadión	stadión	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zde	zde	k6eAd1
chtěl	chtít	k5eAaImAgMnS
postavit	postavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
The	The	k1gFnPc2
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
Railway	Railwaa	k1gFnSc2
mu	on	k3xPp3gMnSc3
ovšem	ovšem	k9
předložila	předložit	k5eAaPmAgFnS
atraktivní	atraktivní	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
ke	k	k7c3
koupi	koupě	k1gFnSc3
plochy	plocha	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
a	a	k8xC
následné	následný	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
tovární	tovární	k2eAgFnSc2d1
vlečky	vlečka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mears	Mearsa	k1gFnPc2
prodej	prodej	k1gInSc4
sice	sice	k8xC
zvažoval	zvažovat	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
sešel	sejít	k5eAaPmAgMnS
s	s	k7c7
architektem	architekt	k1gMnSc7
fotbalových	fotbalový	k2eAgFnPc2d1
tribun	tribuna	k1gFnPc2
Archibaldem	Archibald	k1gMnSc7
Leitchem	Leitch	k1gMnSc7
a	a	k8xC
vyměřili	vyměřit	k5eAaPmAgMnP
hřiště	hřiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
dokonce	dokonce	k9
ani	ani	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
se	se	k3xPyFc4
neuvažovalo	uvažovat	k5eNaImAgNnS
o	o	k7c6
utvoření	utvoření	k1gNnSc6
nového	nový	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrací	hrací	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
totiž	totiž	k9
byla	být	k5eAaImAgFnS
nabídnuta	nabídnout	k5eAaPmNgFnS
týmu	tým	k1gInSc2
Fulham	Fulham	k1gInSc1
FC	FC	kA
pro	pro	k7c4
jejich	jejich	k3xOp3gMnPc4
domácí	domácí	k1gMnPc4
zápasy	zápas	k1gInPc4
za	za	k7c4
nájem	nájem	k1gInSc4
1500	#num#	k4
liber	libra	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužstvo	mužstvo	k1gNnSc1
Fulhamu	Fulham	k1gInSc2
se	se	k3xPyFc4
však	však	k9
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridg	k1gFnSc2
nakonec	nakonec	k6eAd1
nepřestěhovalo	přestěhovat	k5eNaPmAgNnS
<g/>
,	,	kIx,
zmíněná	zmíněný	k2eAgFnSc1d1
suma	suma	k1gFnSc1
prý	prý	k9
byla	být	k5eAaImAgFnS
příliš	příliš	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mearsův	Mearsův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
finančník	finančník	k1gMnSc1
Fred	Fred	k1gMnSc1
W.	W.	kA
Parker	Parker	k1gMnSc1
mu	on	k3xPp3gMnSc3
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
založil	založit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mearsovi	Mearsův	k2eAgMnPc5d1
se	se	k3xPyFc4
nápad	nápad	k1gInSc4
líbil	líbit	k5eAaImAgInS
a	a	k8xC
souhlasil	souhlasit	k5eAaImAgInS
–	–	k?
oficiálním	oficiální	k2eAgNnSc7d1
datem	datum	k1gNnSc7
založení	založení	k1gNnSc2
klubu	klub	k1gInSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
Fulham	Fulham	k1gInSc1
FC	FC	kA
zabrané	zabraný	k2eAgFnPc1d1
<g/>
,	,	kIx,
klub	klub	k1gInSc1
si	se	k3xPyFc3
zvolil	zvolit	k5eAaPmAgInS
název	název	k1gInSc1
podle	podle	k7c2
přilehlého	přilehlý	k2eAgInSc2d1
okrsku	okrsek	k1gInSc2
Metropolitan	metropolitan	k1gInSc1
Borough	Borough	k1gMnSc1
of	of	k?
Chelsea	Chelsea	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
nosili	nosit	k5eAaImAgMnP
světlé	světlý	k2eAgInPc4d1
modré	modrý	k2eAgInPc4d1
dresy	dres	k1gInPc4
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc4d1
trenýrky	trenýrky	k1gFnPc4
a	a	k8xC
tmavě	tmavě	k6eAd1
modré	modrý	k2eAgFnSc2d1
štulpny	štulpna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chelsea	Chelsea	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
byla	být	k5eAaImAgFnS
podána	podat	k5eAaPmNgFnS
žádost	žádost	k1gFnSc1
o	o	k7c4
připojení	připojení	k1gNnSc4
do	do	k7c2
Jižní	jižní	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
po	po	k7c6
protestech	protest	k1gInPc6
Fulhamu	Fulham	k1gInSc2
<g/>
,	,	kIx,
Tottenhamu	Tottenham	k1gInSc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zamítnuta	zamítnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
síle	síla	k1gFnSc3
angažovaných	angažovaný	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
<g/>
,	,	kIx,
novému	nový	k2eAgNnSc3d1
hřišti	hřiště	k1gNnSc3
a	a	k8xC
osobnosti	osobnost	k1gFnSc2
Freda	Fred	k1gMnSc4
Parkera	Parker	k1gMnSc4
byla	být	k5eAaImAgFnS
Chelsea	Chelsea	k1gFnSc1
nakonec	nakonec	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
do	do	k7c2
Druhé	druhý	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
utkání	utkání	k1gNnSc4
sehrála	sehrát	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1905	#num#	k4
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Stockportu	Stockport	k1gInSc2
County	Counta	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
prohrála	prohrát	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
vítězství	vítězství	k1gNnPc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
týden	týden	k1gInSc4
později	pozdě	k6eAd2
proti	proti	k7c3
Blackpoolu	Blackpool	k1gInSc2
<g/>
,	,	kIx,
trefil	trefit	k5eAaPmAgMnS
se	se	k3xPyFc4
John	John	k1gMnSc1
Tait	Tait	k1gMnSc1
"	"	kIx"
<g/>
Jacky	Jacka	k1gMnSc2
<g/>
"	"	kIx"
Robertson	Robertson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiérový	premiérový	k2eAgInSc1d1
ročník	ročník	k1gInSc1
se	se	k3xPyFc4
opravdu	opravdu	k6eAd1
povedl	povést	k5eAaPmAgMnS
a	a	k8xC
Chelsea	Chelsea	k1gFnSc1
dlouho	dlouho	k6eAd1
bojovala	bojovat	k5eAaImAgFnS
o	o	k7c4
postup	postup	k1gInSc4
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
sérii	série	k1gFnSc3
čtyř	čtyři	k4xCgFnPc2
utkání	utkání	k1gNnPc2
bez	bez	k7c2
vítězství	vítězství	k1gNnPc2
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
čtyřech	čtyři	k4xCgInPc6
zápasech	zápas	k1gInPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
nestalo	stát	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
již	již	k6eAd1
byla	být	k5eAaImAgFnS
snaha	snaha	k1gFnSc1
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
První	první	k4xOgFnSc4
divizi	divize	k1gFnSc4
se	se	k3xPyFc4
Chelsea	Chelsea	k1gFnSc1
významněji	významně	k6eAd2
neprosadila	prosadit	k5eNaPmAgFnS
a	a	k8xC
do	do	k7c2
začátku	začátek	k1gInSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
střídala	střídat	k5eAaImAgFnS
První	první	k4xOgFnSc4
a	a	k8xC
Druhou	druhý	k4xOgFnSc4
divizi	divize	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
této	tento	k3xDgFnSc2
éry	éra	k1gFnSc2
zůstala	zůstat	k5eAaPmAgFnS
účast	účast	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
FA	fa	kA
Cupu	cup	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
vyhrál	vyhrát	k5eAaPmAgMnS
poměrem	poměr	k1gInSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Sheffield	Sheffield	k1gMnSc1
United	United	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výrazným	výrazný	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
patřili	patřit	k5eAaImAgMnP
výstřední	výstřední	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
William	William	k1gInSc4
Foulke	Foulk	k1gFnSc2
<g/>
,	,	kIx,
útočníci	útočník	k1gMnPc1
George	Georg	k1gFnSc2
Hilsdon	Hilsdon	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Whittingham	Whittingham	k1gInSc1
nebo	nebo	k8xC
Jimmy	Jimma	k1gFnPc1
Windridge	Windridg	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Meziválečné	meziválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
a	a	k8xC
první	první	k4xOgFnSc1
úspěšná	úspěšný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
V	v	k7c6
meziválečných	meziválečný	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
v	v	k7c6
modrém	modrý	k2eAgInSc6d1
dresu	dres	k1gInSc6
objevilo	objevit	k5eAaPmAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dobrých	dobrý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
např.	např.	kA
skotský	skotský	k2eAgMnSc1d1
internacionál	internacionál	k1gMnSc1
Hughie	Hughie	k1gFnSc2
Gallagher	Gallaghra	k1gFnPc2
<g/>
,	,	kIx,
George	George	k1gFnPc2
Mills	Millsa	k1gFnPc2
<g/>
,	,	kIx,
Harry	Harra	k1gFnPc1
Wilding	Wilding	k1gInSc1
<g/>
,	,	kIx,
Vic	Vic	k1gMnSc1
Woodley	Woodlea	k1gFnSc2
nebo	nebo	k8xC
Ben	Ben	k1gInSc4
Baker	Bakra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
čas	čas	k1gInSc4
byli	být	k5eAaImAgMnP
dokonce	dokonce	k9
všichni	všechen	k3xTgMnPc1
fotbalisté	fotbalista	k1gMnPc1
útočné	útočný	k2eAgFnSc2d1
vozby	vozba	k1gFnSc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
pětičlenné	pětičlenný	k2eAgFnSc2d1
<g/>
)	)	kIx)
reprezentanti	reprezentant	k1gMnPc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
ale	ale	k8xC
moc	moc	k6eAd1
důvodů	důvod	k1gInPc2
k	k	k7c3
oslavám	oslava	k1gFnPc3
zaplněné	zaplněný	k2eAgInPc4d1
ochozy	ochoz	k1gInPc4
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridge	k1gFnSc3
neměly	mít	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenéři	trenér	k1gMnPc1
Calderhead	Calderhead	k1gInSc4
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gMnSc6
Knighton	Knighton	k1gInSc4
nedokázali	dokázat	k5eNaPmAgMnP
vytvořit	vytvořit	k5eAaPmF
v	v	k7c6
týmu	tým	k1gInSc6
vítězný	vítězný	k2eAgMnSc1d1
duch	duch	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
největšími	veliký	k2eAgInPc7d3
úspěchy	úspěch	k1gInPc7
byly	být	k5eAaImAgFnP
2	#num#	k4
<g/>
×	×	k?
účast	účast	k1gFnSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
První	první	k4xOgFnSc6
divizi	divize	k1gFnSc6
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
převzal	převzít	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
týmu	tým	k1gInSc2
Willy	Willa	k1gFnSc2
Birrell	Birrella	k1gFnPc2
a	a	k8xC
zasadil	zasadit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
mládeže	mládež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
Chelsea	Chelsea	k1gFnSc1
marně	marně	k6eAd1
snažila	snažit	k5eAaImAgFnS
získat	získat	k5eAaPmF
nějaký	nějaký	k3yIgInSc4
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
dobrých	dobrý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
bylo	být	k5eAaImAgNnS
opět	opět	k6eAd1
dost	dost	k6eAd1
–	–	k?
Tommy	Tomma	k1gFnPc4
Laxton	Laxton	k1gInSc4
<g/>
,	,	kIx,
Len	len	k1gInSc4
Goulden	Gouldna	k1gFnPc2
<g/>
,	,	kIx,
Billy	Bill	k1gMnPc4
Hughes	Hughesa	k1gFnPc2
<g/>
,	,	kIx,
Roy	Roy	k1gMnSc1
Bentley	Bentlea	k1gFnSc2
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příchodu	příchod	k1gInSc6
trenéra	trenér	k1gMnSc2
Teda	Ted	k1gMnSc2
Drakea	Drake	k1gInSc2
se	se	k3xPyFc4
tým	tým	k1gInSc1
začal	začít	k5eAaPmAgInS
zlepšovat	zlepšovat	k5eAaImF
<g/>
,	,	kIx,
až	až	k8xS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
1955	#num#	k4
získal	získat	k5eAaPmAgInS
poměrně	poměrně	k6eAd1
překvapivě	překvapivě	k6eAd1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelsea	k1gFnSc1
využila	využít	k5eAaPmAgFnS
velké	velký	k2eAgFnPc4d1
vyrovnanosti	vyrovnanost	k1gFnPc4
v	v	k7c6
horní	horní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
tabulky	tabulka	k1gFnSc2
sérií	série	k1gFnPc2
deseti	deset	k4xCc2
utkání	utkání	k1gNnPc2
bez	bez	k7c2
porážky	porážka	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
sedmi	sedm	k4xCc2
vítězství	vítězství	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
prvenství	prvenství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
díky	díky	k7c3
předvídavosti	předvídavost	k1gFnSc3
a	a	k8xC
dobrému	dobrý	k2eAgInSc3d1
odhadu	odhad	k1gInSc3
Dickieho	Dickie	k1gMnSc2
Fosse	Foss	k1gMnSc2
a	a	k8xC
výbornému	výborný	k2eAgInSc3d1
scoutingu	scouting	k1gInSc3
Jimmyho	Jimmy	k1gMnSc2
Thompsona	Thompson	k1gMnSc2
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridg	k1gFnSc2
dostala	dostat	k5eAaPmAgFnS
řada	řada	k1gFnSc1
mladých	mladý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
zde	zde	k6eAd1
během	během	k7c2
pár	pár	k4xCyI
let	léto	k1gNnPc2
udělali	udělat	k5eAaPmAgMnP
jméno	jméno	k1gNnSc4
a	a	k8xC
stali	stát	k5eAaPmAgMnP
se	se	k3xPyFc4
oporami	opora	k1gFnPc7
mužstva	mužstvo	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Jimmy	Jimm	k1gInPc1
Greaves	Greavesa	k1gFnPc2
či	či	k8xC
Ron	Ron	k1gMnSc1
Tindall	Tindall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c4
titul	titul	k1gInSc4
nepodařilo	podařit	k5eNaPmAgNnS
navázat	navázat	k5eAaPmF
<g/>
,	,	kIx,
zárodky	zárodek	k1gInPc1
budoucích	budoucí	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
byly	být	k5eAaImAgFnP
zasety	zaset	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Příchod	příchod	k1gInSc1
Tommyho	Tommy	k1gMnSc2
Dochertyho	Docherty	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
odstartoval	odstartovat	k5eAaPmAgMnS
první	první	k4xOgNnSc4
zlaté	zlatý	k2eAgNnSc4d1
období	období	k1gNnSc4
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladí	mladý	k2eAgMnPc1d1
odchovanci	odchovanec	k1gMnPc1
přebrali	přebrat	k5eAaPmAgMnP
otěže	otěž	k1gFnPc4
a	a	k8xC
i	i	k8xC
když	když	k8xS
první	první	k4xOgInSc4
rok	rok	k1gInSc4
Chelsea	Chelse	k1gInSc2
sestoupila	sestoupit	k5eAaPmAgFnS
do	do	k7c2
Druhé	druhý	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
hned	hned	k6eAd1
v	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
vrátila	vrátit	k5eAaPmAgFnS
zpět	zpět	k6eAd1
a	a	k8xC
v	v	k7c6
následujících	následující	k2eAgInPc6d1
devíti	devět	k4xCc6
ročnících	ročník	k1gInPc6
sahala	sahat	k5eAaImAgFnS
po	po	k7c6
metách	meta	k1gFnPc6
nejvyšších	vysoký	k2eAgMnPc2d3
(	(	kIx(
<g/>
sled	sled	k1gInSc1
umístění	umístění	k1gNnSc2
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
:	:	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
mohli	moct	k5eAaImAgMnP
Blues	blues	k1gFnSc4
(	(	kIx(
<g/>
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
týmu	tým	k1gInSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
přezdívat	přezdívat	k5eAaImF
<g/>
)	)	kIx)
radovat	radovat	k5eAaImF
ze	z	k7c2
zisku	zisk	k1gInSc2
Anglického	anglický	k2eAgInSc2d1
ligového	ligový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
dařilo	dařit	k5eAaImAgNnS
i	i	k9
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
(	(	kIx(
<g/>
dvakrát	dvakrát	k6eAd1
semifinále	semifinále	k1gNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
finále	finále	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
bylo	být	k5eAaImAgNnS
vítězné	vítězný	k2eAgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
následně	následně	k6eAd1
triumf	triumf	k1gInSc4
v	v	k7c6
Poháru	pohár	k1gInSc6
Vítězů	vítěz	k1gMnPc2
Pohárů	pohár	k1gInPc2
proti	proti	k7c3
Realu	Real	k1gInSc3
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základními	základní	k2eAgFnPc7d1
pilíři	pilíř	k1gInSc6
tohoto	tento	k3xDgInSc2
týmu	tým	k1gInSc2
byli	být	k5eAaImAgMnP
Peter	Peter	k1gMnSc1
Houseman	Houseman	k1gMnSc1
<g/>
,	,	kIx,
Bobby	Bobba	k1gFnPc1
Tambling	Tambling	k1gInSc1
<g/>
,	,	kIx,
Ron	ron	k1gInSc1
Harris	Harris	k1gFnSc2
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Osgood	Osgood	k1gInSc1
nebo	nebo	k8xC
legendární	legendární	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
Peter	Peter	k1gMnSc1
Bonnetti	Bonnetti	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
hraně	hrana	k1gFnSc6
zániku	zánik	k1gInSc2
<g/>
,	,	kIx,
reinkarnace	reinkarnace	k1gFnSc2
a	a	k8xC
stabilizace	stabilizace	k1gFnSc2
</s>
<s>
Po	po	k7c6
skvělých	skvělý	k2eAgNnPc6d1
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
přišla	přijít	k5eAaPmAgFnS
velmi	velmi	k6eAd1
slabá	slabý	k2eAgFnSc1d1
sedmdesátá	sedmdesátý	k4xOgNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
odcházející	odcházející	k2eAgInPc4d1
staré	starý	k2eAgInPc4d1
opory	opor	k1gInPc4
nebyla	být	k5eNaImAgFnS
kvalitní	kvalitní	k2eAgFnSc1d1
náhrada	náhrada	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
</s>
<s>
výstavba	výstavba	k1gFnSc1
East	Easta	k1gFnPc2
Stand	Standa	k1gFnPc2
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridge	k1gNnSc2
odčerpala	odčerpat	k5eAaPmAgFnS
z	z	k7c2
klubové	klubový	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
všechny	všechen	k3xTgInPc4
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
muselo	muset	k5eAaImAgNnS
prodávat	prodávat	k5eAaImF
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
trenéři	trenér	k1gMnPc1
se	se	k3xPyFc4
střídali	střídat	k5eAaImAgMnP
jak	jak	k6eAd1
na	na	k7c6
běžícím	běžící	k2eAgInSc6d1
pásu	pás	k1gInSc6
a	a	k8xC
Chelsea	Chelsea	k1gFnSc1
se	se	k3xPyFc4
propadla	propadnout	k5eAaPmAgFnS
o	o	k7c4
soutěž	soutěž	k1gFnSc4
níž	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
to	ten	k3xDgNnSc1
vypadalo	vypadat	k5eAaImAgNnS,k5eAaPmAgNnS
s	s	k7c7
klubem	klub	k1gInSc7
téměř	téměř	k6eAd1
beznadějně	beznadějně	k6eAd1
<g/>
:	:	kIx,
obrovské	obrovský	k2eAgInPc4d1
dluhy	dluh	k1gInPc4
<g/>
,	,	kIx,
zastavené	zastavený	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
<g/>
,	,	kIx,
přestárlý	přestárlý	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akcie	akcie	k1gFnSc1
klubu	klub	k1gInSc2
tehdy	tehdy	k6eAd1
koupil	koupit	k5eAaPmAgMnS
za	za	k7c4
symbolickou	symbolický	k2eAgFnSc4d1
1	#num#	k4
libru	libra	k1gFnSc4
Ken	Ken	k1gFnSc2
Bates	Bates	k1gInSc1
a	a	k8xC
začal	začít	k5eAaPmAgInS
s	s	k7c7
důkladnou	důkladný	k2eAgFnSc7d1
reorganizací	reorganizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angažoval	angažovat	k5eAaBmAgMnS
Johna	John	k1gMnSc4
Neila	Neil	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
musel	muset	k5eAaImAgMnS
zachraňovat	zachraňovat	k5eAaImF
topící	topící	k2eAgFnSc1d1
se	se	k3xPyFc4
Chelsea	Chelsea	k1gFnSc1
ze	z	k7c2
dna	dno	k1gNnSc2
tabulky	tabulka	k1gFnSc2
Druhé	druhý	k4xOgFnSc2
divize	divize	k1gFnSc2
a	a	k8xC
svými	svůj	k3xOyFgInPc7
chytrými	chytrý	k2eAgInPc7d1
transfery	transfer	k1gInPc7
tak	tak	k6eAd1
položil	položit	k5eAaPmAgInS
základy	základ	k1gInPc4
nového	nový	k2eAgInSc2d1
týmu	tým	k1gInSc2
(	(	kIx(
<g/>
mj.	mj.	kA
Nevin	nevina	k1gFnPc2
<g/>
,	,	kIx,
McLaughlin	McLaughlina	k1gFnPc2
<g/>
,	,	kIx,
Speedie	Speedie	k1gFnSc1
<g/>
,	,	kIx,
Dixon	Dixon	k1gMnSc1
<g/>
,	,	kIx,
Spackman	Spackman	k1gMnSc1
<g/>
,	,	kIx,
Niedzwiecki	Niedzwieck	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
druhodivizní	druhodivizní	k2eAgInSc1d1
titul	titul	k1gInSc1
a	a	k8xC
postoupil	postoupit	k5eAaPmAgInS
opět	opět	k6eAd1
mezi	mezi	k7c4
elitu	elita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týmu	tým	k1gInSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgInS
John	John	k1gMnSc1
Hollins	Hollinsa	k1gFnPc2
a	a	k8xC
dvakrát	dvakrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
velmi	velmi	k6eAd1
lichotivé	lichotivý	k2eAgFnSc6d1
šesté	šestý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nevydařeném	vydařený	k2eNgInSc6d1
závěru	závěr	k1gInSc6
ročníku	ročník	k1gInSc2
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
Chelsea	Chelsea	k1gFnSc1
sestoupila	sestoupit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
rok	rok	k1gInSc4
na	na	k7c4
to	ten	k3xDgNnSc4
famózní	famózní	k2eAgNnSc4d1
jízdou	jízda	k1gFnSc7
vyhrála	vyhrát	k5eAaPmAgFnS
Druhou	druhý	k4xOgFnSc4
divizi	divize	k1gFnSc4
a	a	k8xC
ročník	ročník	k1gInSc1
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
uzavřela	uzavřít	k5eAaPmAgFnS
nejlépe	dobře	k6eAd3
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
když	když	k8xS
skončila	skončit	k5eAaPmAgFnS
pátá	pátá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
byly	být	k5eAaImAgInP
konečně	konečně	k6eAd1
vyřešeny	vyřešit	k5eAaPmNgInP
spory	spor	k1gInPc1
o	o	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridg	k1gMnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
navržena	navržen	k2eAgFnSc1d1
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
značnou	značný	k2eAgFnSc4d1
finanční	finanční	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
se	se	k3xPyFc4
týmu	tým	k1gInSc3
podařilo	podařit	k5eAaPmAgNnS
setrvat	setrvat	k5eAaPmF
v	v	k7c6
klidných	klidný	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnPc1d1
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
vybojovat	vybojovat	k5eAaPmF
finále	finále	k1gNnSc2
FA	fa	kA
Cupu	cup	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
byl	být	k5eAaImAgInS
lepší	dobrý	k2eAgInSc1d2
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
poměrem	poměr	k1gInSc7
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
a	a	k8xC
druhá	druhý	k4xOgFnSc1
úspěšná	úspěšný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
Po	po	k7c6
Euru	euro	k1gNnSc6
1996	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
začali	začít	k5eAaPmAgMnP
hrnout	hrnout	k5eAaImF
hráči	hráč	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Chelsea	Chelsea	k1gFnSc1
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
vydělaly	vydělat	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouč	kouč	k1gMnSc1
Glenn	Glenn	k1gMnSc1
Hoddle	Hoddle	k1gMnSc1
dokázal	dokázat	k5eAaPmAgMnS
přivábit	přivábit	k5eAaPmF
skvělé	skvělý	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
jako	jako	k9
byli	být	k5eAaImAgMnP
Dan	Dan	k1gMnSc1
Petrescu	Petrescus	k1gInSc2
nebo	nebo	k8xC
Ruud	Ruud	k1gMnSc1
Gullit	Gullita	k1gFnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
jmenovaný	jmenovaný	k1gMnSc1
po	po	k7c6
něm	on	k3xPp3gInSc6
později	pozdě	k6eAd2
převzal	převzít	k5eAaPmAgMnS
trenérskou	trenérský	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Blues	blues	k1gInSc1
<g/>
“	“	k?
pak	pak	k6eAd1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
útočili	útočit	k5eAaImAgMnP
na	na	k7c4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
jenže	jenže	k8xC
nejlépe	dobře	k6eAd3
skončili	skončit	k5eAaPmAgMnP
těsně	těsně	k6eAd1
třetí	třetí	k4xOgFnSc7
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
nedařilo	dařit	k5eNaImAgNnS
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
vycházelo	vycházet	k5eAaImAgNnS
jinde	jinde	k6eAd1
<g/>
:	:	kIx,
vítězství	vítězství	k1gNnSc1
v	v	k7c4
Community	Communit	k1gInPc4
Shield	Shield	k1gInSc1
2000	#num#	k4
<g/>
,	,	kIx,
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
1998	#num#	k4
<g/>
,	,	kIx,
dvojnásobné	dvojnásobný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
1997	#num#	k4
a	a	k8xC
2000	#num#	k4
(	(	kIx(
<g/>
plus	plus	k6eAd1
jedno	jeden	k4xCgNnSc1
semifinále	semifinále	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
opět	opět	k6eAd1
v	v	k7c6
Poháru	pohár	k1gInSc6
Vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
semifinále	semifinále	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
Superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
už	už	k6eAd1
byl	být	k5eAaImAgMnS
trenérem	trenér	k1gMnSc7
Gianluca	Gianluc	k1gInSc2
Vialli	Vialle	k1gFnSc3
a	a	k8xC
oporami	opora	k1gFnPc7
týmu	tým	k1gInSc2
Ed	Ed	k1gFnPc2
De	De	k?
Goey	Goea	k1gFnSc2
<g/>
,	,	kIx,
Franck	Franck	k1gMnSc1
Leboeuf	Leboeuf	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
<g/>
,	,	kIx,
Dennis	Dennis	k1gFnSc1
Wise	Wise	k1gFnSc1
nebo	nebo	k8xC
Gianfranco	Gianfranco	k1gMnSc1
Zola	Zola	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
náročný	náročný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
klubu	klub	k1gInSc2
Ken	Ken	k1gMnSc1
Bates	Bates	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
účast	účast	k1gFnSc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
a	a	k8xC
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
vysoký	vysoký	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
zisk	zisk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
potřeboval	potřebovat	k5eAaImAgInS
na	na	k7c4
nákladnou	nákladný	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
stadionu	stadion	k1gInSc2
Stamford	Stamford	k1gInSc1
Bridge	Bridg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěšné	úspěšný	k2eNgFnSc6d1
sezóně	sezóna	k1gFnSc6
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
byl	být	k5eAaImAgMnS
trenér	trenér	k1gMnSc1
Vialli	Vialle	k1gFnSc4
vyhozen	vyhozen	k2eAgMnSc1d1
a	a	k8xC
přišel	přijít	k5eAaPmAgMnS
Claudio	Claudio	k1gNnSc4
Ranieri	Ranier	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
počátku	počátek	k1gInSc2
dostával	dostávat	k5eAaImAgInS
Ranieri	Ranieri	k1gNnSc4
nové	nový	k2eAgNnSc4d1
hráčské	hráčský	k2eAgFnPc4d1
posily	posila	k1gFnPc4
za	za	k7c4
vysoké	vysoký	k2eAgFnPc4d1
částky	částka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
výrazný	výrazný	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
nepřicházel	přicházet	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
finance	finance	k1gFnPc1
vysychaly	vysychat	k5eAaImAgFnP
a	a	k8xC
hovořilo	hovořit	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
vysokých	vysoký	k2eAgInPc6d1
dluzích	dluh	k1gInPc6
a	a	k8xC
rozprodání	rozprodání	k1gNnSc6
hráčů	hráč	k1gMnPc2
základní	základní	k2eAgFnSc2d1
jedenáctky	jedenáctka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2003	#num#	k4
prodal	prodat	k5eAaPmAgMnS
Ken	Ken	k1gMnSc1
Bates	Bates	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
většinový	většinový	k2eAgInSc4d1
podíl	podíl	k1gInSc4
v	v	k7c6
klubu	klub	k1gInSc6
překvapivě	překvapivě	k6eAd1
za	za	k7c4
60	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
ruskému	ruský	k2eAgMnSc3d1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
rusko-izraelskému	rusko-izraelský	k2eAgInSc3d1
<g/>
)	)	kIx)
miliardářovi	miliardář	k1gMnSc3
Romanu	Roman	k1gMnSc3
Abramovičovi	Abramovič	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
nastala	nastat	k5eAaPmAgFnS
zatím	zatím	k6eAd1
nejúspěšnější	úspěšný	k2eAgFnSc1d3
doba	doba	k1gFnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abramovič	Abramovič	k1gInSc1
přišel	přijít	k5eAaPmAgInS
do	do	k7c2
Chelsea	Chelse	k1gInSc2
s	s	k7c7
jasným	jasný	k2eAgInSc7d1
plánem	plán	k1gInSc7
–	–	k?
udělat	udělat	k5eAaPmF
z	z	k7c2
něj	on	k3xPp3gMnSc2
jeden	jeden	k4xCgInSc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
klubů	klub	k1gInPc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ambice	ambice	k1gFnSc1
sahaly	sahat	k5eAaImAgFnP
až	až	k9
k	k	k7c3
výšinám	výšina	k1gFnPc3
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
nebo	nebo	k8xC
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
klubu	klub	k1gInSc2
s	s	k7c7
sebou	se	k3xPyFc7
přivedl	přivést	k5eAaPmAgMnS
i	i	k9
Marinu	Marina	k1gFnSc4
Granovskaiu	Granovskaium	k1gNnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
později	pozdě	k6eAd2
vybudovala	vybudovat	k5eAaPmAgFnS
reputaci	reputace	k1gFnSc4
jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvlivnějších	vlivný	k2eAgFnPc2d3
žen	žena	k1gFnPc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
ředitelkou	ředitelka	k1gFnSc7
a	a	k8xC
pravou	pravý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
Abramoviče	Abramovič	k1gMnSc2
a	a	k8xC
někteří	některý	k3yIgMnPc1
ji	on	k3xPp3gFnSc4
přezdívají	přezdívat	k5eAaImIp3nP
„	„	k?
<g/>
Železná	železný	k2eAgFnSc1d1
lady	lady	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Éra	éra	k1gFnSc1
Romana	Roman	k1gMnSc2
Abramoviče	Abramovič	k1gMnSc2
</s>
<s>
Maskot	maskot	k1gInSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
klub	klub	k1gInSc4
Premier	Premira	k1gFnPc2
League	Leagu	k1gInSc2
a	a	k8xC
Carling	Carling	k1gInSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
obhájil	obhájit	k5eAaPmAgMnS
ligový	ligový	k2eAgInSc4d1
triumf	triumf	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
sice	sice	k8xC
potřetí	potřetí	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
titul	titul	k1gInSc4
anglického	anglický	k2eAgMnSc2d1
šampiona	šampion	k1gMnSc2
Chelsea	Chelseus	k1gMnSc2
nezískala	získat	k5eNaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tak	tak	k6eAd1
lze	lze	k6eAd1
označit	označit	k5eAaPmF
tuto	tento	k3xDgFnSc4
sezónu	sezóna	k1gFnSc4
za	za	k7c4
úspěšnou	úspěšný	k2eAgFnSc4d1
<g/>
:	:	kIx,
v	v	k7c4
Premier	Premier	k1gInSc4
League	League	k1gNnSc2
obsadila	obsadit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
za	za	k7c7
vítězným	vítězný	k2eAgInSc7d1
Manchesterem	Manchester	k1gInSc7
United	United	k1gMnSc1
<g/>
,	,	kIx,
vyhrála	vyhrát	k5eAaPmAgFnS
FA	fa	k1gNnSc3
Cup	cup	k1gInSc1
i	i	k8xC
Carling	Carling	k1gInSc1
Cup	cup	k1gInSc1
a	a	k8xC
dostala	dostat	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
semifinále	semifinále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
na	na	k7c6
penalty	penalty	k1gNnSc6
vyřadil	vyřadit	k5eAaPmAgInS
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
odešel	odejít	k5eAaPmAgMnS
po	po	k7c6
dlouhodobých	dlouhodobý	k2eAgFnPc6d1
neshodách	neshoda	k1gFnPc6
s	s	k7c7
majitelem	majitel	k1gMnSc7
klubu	klub	k1gInSc2
Abramovičem	Abramovič	k1gMnSc7
portugalský	portugalský	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
José	José	k1gNnSc2
Mourinho	Mourin	k1gMnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
jehož	jehož	k3xOyRp3gNnSc7
vedením	vedení	k1gNnSc7
Chelsea	Chelse	k1gInSc2
během	během	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
získala	získat	k5eAaPmAgFnS
dva	dva	k4xCgInPc4
ligové	ligový	k2eAgInPc4d1
tituly	titul	k1gInPc4
a	a	k8xC
tři	tři	k4xCgInPc4
poháry	pohár	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
izraelský	izraelský	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Avram	Avram	k1gInSc1
Grant	grant	k1gInSc4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
klubu	klub	k1gInSc2
a	a	k8xC
Abramovičův	Abramovičův	k2eAgMnSc1d1
osobní	osobní	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc4
ale	ale	k8xC
klub	klub	k1gInSc4
opustil	opustit	k5eAaPmAgMnS
po	po	k7c6
prohraném	prohraný	k2eAgNnSc6d1
finálovém	finálový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
Champions	Championsa	k1gFnPc2
League	League	k1gNnPc2
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
United	United	k1gInSc1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
a	a	k8xC
kormidlo	kormidlo	k1gNnSc4
Chelsea	Chelse	k1gInSc2
po	po	k7c6
něm	on	k3xPp3gInSc6
převzal	převzít	k5eAaPmAgInS
po	po	k7c6
Euru	euro	k1gNnSc6
2008	#num#	k4
Brazilec	Brazilec	k1gMnSc1
Luiz	Luiz	k1gMnSc1
Felipe	Felip	k1gMnSc5
Scolari	Scolar	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
do	do	k7c2
Chelsea	Chelse	k1gInSc2
přivedl	přivést	k5eAaPmAgInS
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
oblíbenců	oblíbenec	k1gMnPc2
z	z	k7c2
portugalské	portugalský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
(	(	kIx(
<g/>
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
předtím	předtím	k6eAd1
trénoval	trénovat	k5eAaImAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
záložníka	záložník	k1gMnSc4
Deca	Decus	k1gMnSc4
z	z	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
ten	ten	k3xDgMnSc1
však	však	k9
zanedlouho	zanedlouho	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
příchodu	příchod	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
málo	málo	k6eAd1
úspěšném	úspěšný	k2eAgNnSc6d1
působení	působení	k1gNnSc6
<g/>
,	,	kIx,
u	u	k7c2
klubu	klub	k1gInSc2
skončil	skončit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
poté	poté	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
vedl	vést	k5eAaImAgMnS
jako	jako	k9
hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Nizozemec	Nizozemec	k1gMnSc1
Guus	Guusa	k1gFnPc2
Hiddink	Hiddink	k1gInSc1
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
také	také	k9
trenér	trenér	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ještě	ještě	k9
dokázal	dokázat	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
Anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezonách	sezona	k1gFnPc6
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
a	a	k8xC
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
vedl	vést	k5eAaImAgInS
tým	tým	k1gInSc1
Chelsea	Chelseum	k1gNnSc2
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
hned	hned	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
sezoně	sezona	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
Premier	Premier	k1gMnSc1
League	Leagu	k1gInSc2
<g/>
,	,	kIx,
Anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc4
a	a	k8xC
Community	Communit	k1gInPc4
Shield	Shielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gMnSc6
byl	být	k5eAaImAgMnS
trenérem	trenér	k1gMnSc7
Chelsea	Chelse	k1gInSc2
poměrně	poměrně	k6eAd1
krátce	krátce	k6eAd1
André	André	k1gMnSc1
Villas-Boas	Villas-Boas	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
skončil	skončit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
působení	působení	k1gNnSc4
v	v	k7c6
březnu	březen	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
sezóna	sezóna	k1gFnSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
skončí	skončit	k5eAaPmIp3nS
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
nového	nový	k2eAgMnSc2d1
prozatímního	prozatímní	k2eAgMnSc2d1
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
<g/>
,	,	kIx,
Itala	Ital	k1gMnSc2
Roberta	Robert	k1gMnSc2
Di	Di	k1gMnSc2
Mattea	Matteus	k1gMnSc2
<g/>
,	,	kIx,
velký	velký	k2eAgInSc4d1
obrat	obrat	k1gInSc4
a	a	k8xC
postup	postup	k1gInSc4
až	až	k6eAd1
do	do	k7c2
finále	finále	k1gNnSc2
Champions	Championsa	k1gFnPc2
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
skončilo	skončit	k5eAaPmAgNnS
největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
v	v	k7c6
historii	historie	k1gFnSc6
FC	FC	kA
Chelsea	Chelseus	k1gMnSc2
–	–	k?
vítězstvím	vítězství	k1gNnSc7
proti	proti	k7c3
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utkání	utkání	k1gNnSc1
sice	sice	k8xC
skončilo	skončit	k5eAaPmAgNnS
nerozhodně	rozhodně	k6eNd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
penaltový	penaltový	k2eAgInSc4d1
rozstřel	rozstřel	k1gInSc4
vyhrála	vyhrát	k5eAaPmAgFnS
Chelsea	Chelsea	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Velkou	velký	k2eAgFnSc4d1
zásluhu	zásluha	k1gFnSc4
na	na	k7c6
tomto	tento	k3xDgInSc6
triumfu	triumf	k1gInSc6
měli	mít	k5eAaImAgMnP
brankář	brankář	k1gMnSc1
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
(	(	kIx(
<g/>
mj.	mj.	kA
zneškodnil	zneškodnit	k5eAaPmAgMnS
Arjenu	Arjena	k1gFnSc4
Robbenovi	Robben	k1gMnSc3
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
na	na	k7c6
začátku	začátek	k1gInSc6
prodloužení	prodloužení	k1gNnSc2
a	a	k8xC
v	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
další	další	k2eAgMnPc1d1
dva	dva	k4xCgInPc4
<g/>
)	)	kIx)
a	a	k8xC
útočník	útočník	k1gMnSc1
Didier	Didier	k1gMnSc1
Drogba	Drogba	k1gMnSc1
(	(	kIx(
<g/>
vstřelil	vstřelit	k5eAaPmAgMnS
vyrovnávací	vyrovnávací	k2eAgInSc4d1
gól	gól	k1gInSc4
na	na	k7c4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
krátce	krátce	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
řádné	řádný	k2eAgFnSc2d1
hrací	hrací	k2eAgFnSc2d1
doby	doba	k1gFnSc2
a	a	k8xC
proměnil	proměnit	k5eAaPmAgMnS
rozhodující	rozhodující	k2eAgInSc4d1
pokutový	pokutový	k2eAgInSc4d1
kop	kop	k1gInSc4
v	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
vítězství	vítězství	k1gNnSc1
také	také	k9
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Chelsea	Chelsea	k1gFnSc1
kvalifikovala	kvalifikovat	k5eAaBmAgFnS
do	do	k7c2
ročníku	ročník	k1gInSc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
skončila	skončit	k5eAaPmAgFnS
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
až	až	k9
na	na	k7c6
šesté	šestý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
obhájce	obhájce	k1gMnSc4
trofeje	trofej	k1gFnSc2
tak	tak	k9
překazila	překazit	k5eAaPmAgFnS
případnou	případný	k2eAgFnSc4d1
účast	účast	k1gFnSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
svému	svůj	k3xOyFgMnSc3
londýnskému	londýnský	k2eAgMnSc3d1
rivalovi	rival	k1gMnSc3
Tottenhamu	Tottenham	k1gInSc2
Hotspur	Hotspur	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
skončil	skončit	k5eAaPmAgMnS
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
na	na	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
Tottenham	Tottenham	k1gInSc1
by	by	kYmCp3nS
hrál	hrát	k5eAaImAgInS
předkolo	předkolo	k1gNnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
triumfu	triumf	k1gInSc3
Chelsea	Chelse	k1gInSc2
v	v	k7c6
LM	LM	kA
byl	být	k5eAaImAgInS
přeřazen	přeřadit	k5eAaPmNgInS
do	do	k7c2
základní	základní	k2eAgFnSc2d1
části	část	k1gFnSc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
LM	LM	kA
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
představit	představit	k5eAaPmF
maximálně	maximálně	k6eAd1
4	#num#	k4
celky	celek	k1gInPc7
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobná	obdobný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
nastala	nastat	k5eAaPmAgFnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
LM	LM	kA
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
obhájce	obhájce	k1gMnSc1
trofeje	trofej	k1gFnSc2
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
pátý	pátý	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
tehdy	tehdy	k6eAd1
dostal	dostat	k5eAaPmAgInS
výjimku	výjimka	k1gFnSc4
od	od	k7c2
UEFA	UEFA	kA
a	a	k8xC
byl	být	k5eAaImAgInS
zařazen	zařadit	k5eAaPmNgInS
do	do	k7c2
prvního	první	k4xOgNnSc2
předkola	předkolo	k1gNnSc2
LM	LM	kA
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
si	se	k3xPyFc3
zajistil	zajistit	k5eAaPmAgMnS
postup	postup	k1gInSc4
do	do	k7c2
skupinové	skupinový	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
soutěže	soutěž	k1gFnSc2
(	(	kIx(
<g/>
tam	tam	k6eAd1
se	se	k3xPyFc4
představily	představit	k5eAaPmAgInP
celkem	celkem	k6eAd1
4	#num#	k4
anglické	anglický	k2eAgInPc1d1
týmy	tým	k1gInPc1
–	–	k?
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
<g/>
,	,	kIx,
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
a	a	k8xC
Arsenal	Arsenal	k1gFnSc1
<g/>
,	,	kIx,
pátý	pátý	k4xOgInSc1
anglický	anglický	k2eAgInSc1d1
tým	tým	k1gInSc1
Everton	Everton	k1gInSc1
FC	FC	kA
vypadl	vypadnout	k5eAaPmAgInS
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
předkole	předkolo	k1gNnSc6
se	s	k7c7
španělským	španělský	k2eAgInSc7d1
Villarrealem	Villarreal	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roberto	Roberta	k1gFnSc5
di	di	k?
Matteo	Matteo	k1gMnSc1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
trenérem	trenér	k1gMnSc7
Chelsea	Chelseus	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
v	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
Rafaelem	Rafael	k1gMnSc7
Benítezem	Benítez	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
ten	ten	k3xDgInSc1
však	však	k9
jako	jako	k8xC,k8xS
bývalý	bývalý	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Liverpoolu	Liverpool	k1gInSc2
neměl	mít	k5eNaImAgMnS
u	u	k7c2
fanoušků	fanoušek	k1gMnPc2
velkou	velký	k2eAgFnSc4d1
oblibu	obliba	k1gFnSc4
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
stále	stále	k6eAd1
volali	volat	k5eAaImAgMnP
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
odvolání	odvolání	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
konci	konec	k1gInSc6
sezóny	sezóna	k1gFnSc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
se	se	k3xPyFc4
Chelsea	Chelsea	k1gFnSc1
jako	jako	k8xS,k8xC
staronový	staronový	k2eAgMnSc1d1
kouč	kouč	k1gMnSc1
ujal	ujmout	k5eAaPmAgMnS
José	José	k1gNnSc4
Mourinho	Mourin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
nezískala	získat	k5eNaPmAgFnS
Chelsea	Chelse	k2eAgFnSc1d1
žádnou	žádný	k3yNgFnSc4
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
Capital	Capital	k1gMnSc1
One	One	k1gMnSc1
Cupu	cupat	k5eAaImIp1nS
překvapivě	překvapivě	k6eAd1
vypadla	vypadnout	k5eAaPmAgFnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
se	s	k7c7
Sunderlandem	Sunderland	k1gInSc7
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
City	city	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
proti	proti	k7c3
Atléticu	Atléticum	k1gNnSc3
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
dvojzápas	dvojzápas	k1gInSc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
skončili	skončit	k5eAaPmAgMnP
The	The	k1gMnPc1
Blues	blues	k1gNnSc2
třetí	třetí	k4xOgMnSc1
se	s	k7c7
4	#num#	k4
<g/>
bodovou	bodový	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
na	na	k7c4
výherce	výherce	k1gMnPc4
soutěže	soutěž	k1gFnSc2
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
slov	slovo	k1gNnPc2
Josého	Josý	k1gMnSc2
Mourinha	Mourinh	k1gMnSc2
musel	muset	k5eAaImAgInS
tým	tým	k1gInSc4
vyzrát	vyzrát	k5eAaPmF
a	a	k8xC
trofeje	trofej	k1gFnPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
přijít	přijít	k5eAaPmF
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
slova	slovo	k1gNnSc2
potvrdila	potvrdit	k5eAaPmAgFnS
a	a	k8xC
Chelsea	Chelsea	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
jak	jak	k6eAd1
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
Ligový	ligový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
vytvořila	vytvořit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
dnů	den	k1gInPc2
strávených	strávený	k2eAgInPc2d1
na	na	k7c6
první	první	k4xOgFnSc6
pozici	pozice	k1gFnSc6
–	–	k?
274	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
vypadla	vypadnout	k5eAaPmAgFnS
v	v	k7c6
osmi	osm	k4xCc6
finále	finále	k1gNnPc6
proti	proti	k7c3
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
FA	fa	k1gNnSc2
Cupu	cupat	k5eAaImIp1nS
jí	on	k3xPp3gFnSc3
vyřadil	vyřadit	k5eAaPmAgMnS
ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
Bradford	Bradford	k1gInSc1
po	po	k7c6
prohře	prohra	k1gFnSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
ovšem	ovšem	k9
Chelsea	Chelsea	k1gFnSc1
začala	začít	k5eAaPmAgFnS
prohrávat	prohrávat	k5eAaImF
<g/>
,	,	kIx,
propadla	propadnout	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
sestupovým	sestupový	k2eAgFnPc3d1
příčkám	příčka	k1gFnPc3
a	a	k8xC
vypadla	vypadnout	k5eAaPmAgFnS
z	z	k7c2
Ligového	ligový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
na	na	k7c4
penalty	penalta	k1gFnPc4
proti	proti	k7c3
Stoke	Stoke	k1gFnSc3
City	City	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
Chelsea	Chelseum	k1gNnSc2
podařilo	podařit	k5eAaPmAgNnS
postoupit	postoupit	k5eAaPmF
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
z	z	k7c2
prvního	první	k4xOgNnSc2
místa	místo	k1gNnSc2
se	se	k3xPyFc4
13	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
kdy	kdy	k6eAd1
se	se	k3xPyFc4
klub	klub	k1gInSc1
nacházel	nacházet	k5eAaImAgInS
na	na	k7c4
16	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
propustila	propustit	k5eAaPmAgFnS
José	José	k1gNnSc7
Mourinha	Mourinh	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
tímto	tento	k3xDgNnSc7
rozhodnutím	rozhodnutí	k1gNnSc7
nesouhlasila	souhlasit	k5eNaImAgFnS
většina	většina	k1gFnSc1
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
dalším	další	k2eAgInSc6d1
domácím	domácí	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
Sunderlandu	Sunderland	k1gInSc2
zpívali	zpívat	k5eAaImAgMnP
Mourinhovo	Mourinhův	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
vyvěšovali	vyvěšovat	k5eAaImAgMnP
transparenty	transparent	k1gInPc4
a	a	k8xC
bučeli	bučet	k5eAaImAgMnP
na	na	k7c4
některé	některý	k3yIgMnPc4
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Chelsea	Chelse	k1gInSc2
poté	poté	k6eAd1
po	po	k7c6
zápase	zápas	k1gInSc6
převzal	převzít	k5eAaPmAgInS
do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
Guus	Guusa	k1gFnPc2
Hiddink	Hiddink	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
taktovkou	taktovka	k1gFnSc7
Guuse	Guuse	k1gFnSc1
Hiddinka	Hiddinka	k1gFnSc1
Chelsea	Chelsea	k1gFnSc1
stoupla	stoupnout	k5eAaPmAgFnS
tabulkou	tabulka	k1gFnSc7
a	a	k8xC
sezónu	sezóna	k1gFnSc4
zakončila	zakončit	k5eAaPmAgFnS
na	na	k7c6
desátém	desátý	k4xOgInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
místě	místo	k1gNnSc6
s	s	k7c7
50	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
Chelsea	Chelsea	k1gFnSc1
vypadla	vypadnout	k5eAaPmAgFnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
po	po	k7c6
prohře	prohra	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
s	s	k7c7
Evertonem	Everton	k1gInSc7
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
z	z	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
byla	být	k5eAaImAgFnS
vyřazena	vyřadit	k5eAaPmNgFnS
taktéž	taktéž	k?
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
po	po	k7c6
domácí	domácí	k2eAgFnSc6d1
i	i	k8xC
venkovní	venkovní	k2eAgFnSc6d1
prohře	prohra	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
s	s	k7c7
francouzským	francouzský	k2eAgMnSc7d1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
neúspěch	neúspěch	k1gInSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
znamenal	znamenat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
Chelsea	Chelsea	k1gFnSc1
nehrála	hrát	k5eNaImAgFnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
o	o	k7c4
evropské	evropský	k2eAgInPc4d1
poháry	pohár	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
Chelsea	Chelsea	k1gFnSc1
angažovala	angažovat	k5eAaBmAgFnS
italského	italský	k2eAgMnSc4d1
trenéra	trenér	k1gMnSc4
Antonia	Antonio	k1gMnSc4
Conteho	Conte	k1gMnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
podepsala	podepsat	k5eAaPmAgFnS
tříletý	tříletý	k2eAgInSc4d1
kontrakt	kontrakt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conte	Cont	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
trenér	trenér	k1gMnSc1
italské	italský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
týmu	tým	k1gInSc2
připojil	připojit	k5eAaPmAgInS
v	v	k7c6
červenci	červenec	k1gInSc6
po	po	k7c6
skončení	skončení	k1gNnSc6
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
létě	léto	k1gNnSc6
do	do	k7c2
Chelsea	Chelseum	k1gNnSc2
přestoupil	přestoupit	k5eAaPmAgMnS
z	z	k7c2
mistrovského	mistrovský	k2eAgInSc2d1
Leicesteru	Leicester	k1gInSc2
City	City	k1gFnSc2
N	N	kA
<g/>
'	'	kIx"
<g/>
Golo	Golo	k6eAd1
Kanté	Kantý	k2eAgNnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
Olympique	Olympiqu	k1gFnSc2
de	de	k?
Marseille	Marseille	k1gFnSc2
byl	být	k5eAaImAgInS
koupen	koupen	k2eAgInSc1d1
Michy	Mich	k1gMnPc7
Batshuayi	Batshuayi	k1gNnPc2
a	a	k8xC
z	z	k7c2
PSG	PSG	kA
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
David	David	k1gMnSc1
Luiz	Luiz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Chelsea	Chelsea	k1gMnSc1
pod	pod	k7c7
Contem	Cont	k1gMnSc7
začala	začít	k5eAaPmAgFnS
ligu	liga	k1gFnSc4
vítězně	vítězně	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
porazila	porazit	k5eAaPmAgFnS
West	West	k1gInSc4
Ham	ham	k0
<g/>
,	,	kIx,
Watford	Watford	k1gMnSc1
a	a	k8xC
Burnley	Burnlea	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
remíze	remíza	k1gFnSc6
se	se	k3xPyFc4
Swansea	Swansea	k1gMnSc1
ovšem	ovšem	k9
následovaly	následovat	k5eAaImAgFnP
prohry	prohra	k1gFnPc1
s	s	k7c7
Liverpoolem	Liverpool	k1gInSc7
a	a	k8xC
městským	městský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
Arsenalem	Arsenal	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
těchto	tento	k3xDgFnPc6
prohrách	prohra	k1gFnPc6
Conte	Cont	k1gInSc5
změnil	změnit	k5eAaPmAgInS
sestavu	sestava	k1gFnSc4
na	na	k7c4
3-4-3	3-4-3	k4
a	a	k8xC
začala	začít	k5eAaPmAgFnS
vítězná	vítězný	k2eAgFnSc1d1
série	série	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
činila	činit	k5eAaImAgFnS
13	#num#	k4
vyhraných	vyhraný	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
Chelsea	Chelseus	k1gMnSc4
vyrovnala	vyrovnat	k5eAaBmAgFnS,k5eAaPmAgFnS
rekord	rekord	k1gInSc4
počtu	počet	k1gInSc2
výher	výhra	k1gFnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
držel	držet	k5eAaImAgMnS
Arsenal	Arsenal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
byla	být	k5eAaImAgFnS
začátkem	začátkem	k7c2
ledna	leden	k1gInSc2
utnuta	utnut	k2eAgFnSc1d1
Tottenhamem	Tottenham	k1gInSc7
venkovní	venkovní	k2eAgFnSc7d1
prohrou	prohra	k1gFnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
již	již	k6eAd1
ale	ale	k8xC
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
neopustila	opustit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obranu	obrana	k1gFnSc4
Blues	blues	k1gNnSc2
výrazně	výrazně	k6eAd1
doplňoval	doplňovat	k5eAaImAgMnS
nový	nový	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
N	N	kA
<g/>
'	'	kIx"
<g/>
Golo	Golo	k6eAd1
Kanté	Kantý	k2eAgInPc1d1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
hráčem	hráč	k1gMnSc7
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyrovnané	vyrovnaný	k2eAgInPc4d1
výkony	výkon	k1gInPc4
podával	podávat	k5eAaImAgMnS
krajní	krajní	k2eAgMnSc1d1
obránce	obránce	k1gMnSc1
César	César	k1gMnSc1
Azpilicueta	Azpilicueta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změna	změna	k1gFnSc1
rozestavení	rozestavení	k1gNnSc2
prospěla	prospět	k5eAaPmAgFnS
útočné	útočný	k2eAgFnSc6d1
hvězdě	hvězda	k1gFnSc3
Edenu	Eden	k1gInSc2
Hazardovi	hazard	k1gMnSc3
<g/>
,	,	kIx,
v	v	k7c6
průběhu	průběh	k1gInSc6
podzimu	podzim	k1gInSc2
to	ten	k3xDgNnSc4
však	však	k9
byl	být	k5eAaImAgMnS
hrotový	hrotový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Diego	Diego	k1gMnSc1
Costa	Costa	k1gMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
táhl	táhnout	k5eAaImAgMnS
Blues	blues	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
25	#num#	k4
<g/>
.	.	kIx.
sezóně	sezóna	k1gFnSc6
Premier	Premira	k1gFnPc2
League	Leagu	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k9
Chelsea	Chelsea	k1gFnSc1
chopila	chopit	k5eAaPmAgFnS
svého	svůj	k3xOyFgInSc2
pátého	pátý	k4xOgInSc2
mistrovského	mistrovský	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenér	trenér	k1gMnSc1
sezóny	sezóna	k1gFnSc2
Antonio	Antonio	k1gMnSc1
Conte	Cont	k1gInSc5
triumfoval	triumfovat	k5eAaBmAgMnS
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nově	nově	k6eAd1
působila	působit	k5eAaImAgNnP
též	též	k9
další	další	k2eAgNnPc1d1
velká	velký	k2eAgNnPc1d1
trenérská	trenérský	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
jako	jako	k8xC,k8xS
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pep	Pepa	k1gFnPc2
Guardiola	Guardiola	k1gFnSc1
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
City	city	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arsè	Arsè	k1gInSc5
Wenger	Wenger	k1gMnSc1
(	(	kIx(
<g/>
Arsenal	Arsenal	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc1
(	(	kIx(
<g/>
Liverpool	Liverpool	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
za	za	k7c7
obhajobou	obhajoba	k1gFnSc7
započala	započnout	k5eAaPmAgFnS
domácí	domácí	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
s	s	k7c7
Burnley	Burnleum	k1gNnPc7
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
Chelsea	Chelseum	k1gNnPc1
dohrávala	dohrávat	k5eAaImAgNnP
bez	bez	k7c2
vyloučené	vyloučený	k2eAgFnSc2d1
dvojice	dvojice	k1gFnSc2
Cahilla	Cahillo	k1gNnSc2
a	a	k8xC
Fà	Fà	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
To	ten	k3xDgNnSc4
Blues	blues	k1gNnSc4
odčinili	odčinit	k5eAaPmAgMnP
ve	v	k7c4
Wembley	Wemblea	k1gFnPc4
venku	venku	k6eAd1
proti	proti	k7c3
Tottenhamu	Tottenham	k1gInSc2
výhrou	výhra	k1gFnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
posily	posila	k1gFnPc1
jako	jako	k8xC,k8xS
Tiémoué	Tiémouý	k2eAgNnSc1d1
Bakayoko	Bakayoko	k1gNnSc1
a	a	k8xC
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k2eAgFnSc1d1
nenabraly	nabrat	k5eNaPmAgFnP
formu	forma	k1gFnSc4
a	a	k8xC
nenahradily	nahradit	k5eNaPmAgFnP
tak	tak	k9
Nemanju	Nemanju	k1gFnPc1
Matiće	Matić	k1gMnSc2
a	a	k8xC
Diega	Dieg	k1gMnSc2
Costu	Costa	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
z	z	k7c2
klubu	klub	k1gInSc2
odešli	odejít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
Chelsea	Chelseus	k1gMnSc2
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
Bournemouth	Bournemouth	k1gInSc4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
Watford	Watford	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Období	období	k1gNnSc1
mezi	mezi	k7c7
koncem	konec	k1gInSc7
ledna	leden	k1gInSc2
a	a	k8xC
začátkem	začátkem	k7c2
dubna	duben	k1gInSc2
přineslo	přinést	k5eAaPmAgNnS
pět	pět	k4xCc1
porážek	porážka	k1gFnPc2
v	v	k7c6
sedmi	sedm	k4xCc6
zápasech	zápas	k1gInPc6
Premier	Premier	k1gInSc4
League	League	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
38	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
se	se	k3xPyFc4
porážkou	porážka	k1gFnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
v	v	k7c6
Newcastlu	Newcastl	k1gInSc6
zpečetilo	zpečetit	k5eAaPmAgNnS
umístění	umístění	k1gNnSc4
na	na	k7c6
pátém	pátý	k4xOgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
Chelsea	Chelsea	k1gFnSc1
si	se	k3xPyFc3
tak	tak	k9
měla	mít	k5eAaImAgFnS
minimálně	minimálně	k6eAd1
rok	rok	k1gInSc4
dát	dát	k5eAaPmF
od	od	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
pauzu	pauza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
Arsenal	Arsenal	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vynahradila	vynahradit	k5eAaPmAgFnS
si	se	k3xPyFc3
to	ten	k3xDgNnSc1
vítězstvím	vítězství	k1gNnSc7
ve	v	k7c6
finále	finále	k1gNnSc6
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
nad	nad	k7c7
Manchesterem	Manchester	k1gInSc7
United	United	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
zásluhou	zásluha	k1gFnSc7
výhry	výhra	k1gFnSc2
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Atlética	Atlétic	k1gInSc2
ve	v	k7c4
skupiny	skupina	k1gFnPc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
postoupila	postoupit	k5eAaPmAgFnS
Chelsea	Chelsea	k1gFnSc1
ze	z	k7c2
druhého	druhý	k4xOgNnSc2
místa	místo	k1gNnSc2
za	za	k7c7
prvním	první	k4xOgMnSc7
AS	as	k9
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmifinále	osmifinále	k1gNnSc6
ale	ale	k8xC
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
Barcelonu	Barcelona	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgMnPc4d3
hráče	hráč	k1gMnPc4
Blues	blues	k1gFnSc2
patřil	patřit	k5eAaImAgInS
Eden	Eden	k1gInSc1
Hazard	hazard	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
gólů	gól	k1gInPc2
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Willian	Willian	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
sezóny	sezóna	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
zamířila	zamířit	k5eAaPmAgFnS
Chelsea	Chelsea	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
italského	italský	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
Maurizia	Maurizius	k1gMnSc2
Sarriho	Sarri	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
s	s	k7c7
sebou	se	k3xPyFc7
z	z	k7c2
Neapole	Neapol	k1gFnSc2
přivedl	přivést	k5eAaPmAgMnS
záložníka	záložník	k1gMnSc4
Jorginha	Jorginh	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
úvod	úvod	k1gInSc4
se	se	k3xPyFc4
tým	tým	k1gInSc1
utkal	utkat	k5eAaPmAgInS
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
City	City	k1gFnSc2
o	o	k7c4
Community	Communit	k1gInPc4
Shield	Shielda	k1gFnPc2
(	(	kIx(
<g/>
anglický	anglický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
prohrál	prohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdařilý	zdařilý	k2eAgInSc1d1
začátek	začátek	k1gInSc1
ligy	liga	k1gFnSc2
se	se	k3xPyFc4
nesl	nést	k5eAaImAgInS
na	na	k7c6
12	#num#	k4
zápasech	zápas	k1gInPc6
bez	bez	k7c2
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
našla	najít	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
konec	konec	k1gInSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
stadionu	stadion	k1gInSc6
Tottenhamu	Tottenham	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
kol	kolo	k1gNnPc2
později	pozdě	k6eAd2
uštědřila	uštědřit	k5eAaPmAgFnS
Chelsea	Chelsea	k1gFnSc1
porážku	porážka	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Manchesteru	Manchester	k1gInSc2
City	City	k1gFnSc2
a	a	k8xC
oplatila	oplatit	k5eAaPmAgFnS
jim	on	k3xPp3gFnPc3
vlastní	vlastnit	k5eAaImIp3nS
srpnovou	srpnový	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Server	server	k1gInSc1
90	#num#	k4
<g/>
.	.	kIx.
<g/>
min	min	kA
popsal	popsat	k5eAaPmAgInS
ligovou	ligový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
jako	jako	k8xC,k8xS
jízdu	jízda	k1gFnSc4
na	na	k7c6
horské	horský	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
nového	nový	k2eAgInSc2d1
roku	rok	k1gInSc2
prohrála	prohrát	k5eAaPmAgFnS
Chelsea	Chelsea	k1gFnSc1
venku	venku	k6eAd1
s	s	k7c7
Arsenalem	Arsenal	k1gInSc7
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bournemouthem	Bournemouth	k1gInSc7
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Manchesterem	Manchester	k1gInSc7
City	city	k1gNnSc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
mezitím	mezitím	k6eAd1
vyzrála	vyzrát	k5eAaPmAgFnS
na	na	k7c4
Huddersfield	Huddersfield	k1gInSc4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sarriho	Sarrize	k6eAd1
svěřenci	svěřenec	k1gMnPc1
se	se	k3xPyFc4
probojovávali	probojovávat	k5eAaImAgMnP
Ligovým	ligový	k2eAgInSc7d1
pohárem	pohár	k1gInSc7
(	(	kIx(
<g/>
Carabao	Carabao	k6eAd1
Cup	cup	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
postupně	postupně	k6eAd1
přehráli	přehrát	k5eAaPmAgMnP
pozdějšího	pozdní	k2eAgMnSc4d2
vítěze	vítěz	k1gMnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
Derby	derby	k1gNnSc1
County	Counta	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
Frankem	Frank	k1gMnSc7
Lampardem	Lampard	k1gMnSc7
<g/>
,	,	kIx,
Bournemouth	Bournemouth	k1gMnSc1
a	a	k8xC
pozdějšího	pozdní	k2eAgMnSc4d2
finalistu	finalista	k1gMnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
Tottenham	Tottenham	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
proti	proti	k7c3
Manchesteru	Manchester	k1gInSc3
City	City	k1gFnSc2
ale	ale	k8xC
v	v	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
neuspěli	uspět	k5eNaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
konečné	konečný	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
si	se	k3xPyFc3
Chelsea	Chelsea	k1gFnSc1
zajistila	zajistit	k5eAaPmAgFnS
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
po	po	k7c6
roční	roční	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
měla	mít	k5eAaImAgFnS
opět	opět	k6eAd1
ukázat	ukázat	k5eAaPmF
Lize	liga	k1gFnSc3
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
získala	získat	k5eAaPmAgFnS
trofej	trofej	k1gFnSc4
–	–	k?
ve	v	k7c6
finále	finále	k1gNnSc6
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
v	v	k7c6
Baku	Baku	k1gNnSc6
porazila	porazit	k5eAaPmAgFnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Arsenal	Arsenal	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužstvo	mužstvo	k1gNnSc4
táhl	táhnout	k5eAaImAgInS
zejména	zejména	k9
Eden	Eden	k1gInSc1
Hazard	hazard	k2eAgInSc1d1
<g/>
,	,	kIx,
pro	pro	k7c4
něhož	jenž	k3xRgNnSc4
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
v	v	k7c6
klubu	klub	k1gInSc6
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
totiž	totiž	k9
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
nastal	nastat	k5eAaPmAgInS
návrat	návrat	k1gInSc4
velké	velký	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
Franka	Frank	k1gMnSc2
Lamparda	Lampard	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
k	k	k7c3
týmu	tým	k1gInSc2
připojil	připojit	k5eAaPmAgInS
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
angažmá	angažmá	k1gNnSc6
u	u	k7c2
týmu	tým	k1gInSc2
Derby	derby	k1gNnSc2
County	Counta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
zákaz	zákaz	k1gInSc4
nákupů	nákup	k1gInPc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
přivést	přivést	k5eAaPmF
Cristiana	Cristian	k1gMnSc4
Pulišiće	Pulišić	k1gMnSc4
z	z	k7c2
Borussie	Borussie	k1gFnSc2
Dortmund	Dortmund	k1gInSc1
a	a	k8xC
Matea	Matea	k1gFnSc1
Kovačiče	Kovačič	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
týmu	tým	k1gInSc6
již	již	k6eAd1
hostoval	hostovat	k5eAaImAgInS
z	z	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
trenér	trenér	k1gMnSc1
dokončil	dokončit	k5eAaPmAgMnS
sezónu	sezóna	k1gFnSc4
i	i	k9
přes	přes	k7c4
nevyrovnané	vyrovnaný	k2eNgInPc4d1
výkony	výkon	k1gInPc4
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
zaručující	zaručující	k2eAgFnSc4d1
účast	účast	k1gFnSc4
v	v	k7c6
dalším	další	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
do	do	k7c2
týmu	tým	k1gInSc2
zapracoval	zapracovat	k5eAaPmAgMnS
mnoho	mnoho	k4c4
mladíků	mladík	k1gMnPc2
z	z	k7c2
akademie	akademie	k1gFnSc2
Chelsea	Chelse	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
nejvíce	nejvíce	k6eAd1,k6eAd3
zapůsobili	zapůsobit	k5eAaPmAgMnP
Mason	mason	k1gMnSc1
Mount	Mount	k1gMnSc1
<g/>
,	,	kIx,
Reece	Reece	k1gMnSc1
James	James	k1gMnSc1
<g/>
,	,	kIx,
Tammy	Tamm	k1gInPc7
Abraham	Abraham	k1gMnSc1
a	a	k8xC
Billy	Bill	k1gMnPc7
Gilmour	Gilmoura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgInPc1d1
přestupy	přestup	k1gInPc1
před	před	k7c7
ročníkem	ročník	k1gInSc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
slibovaly	slibovat	k5eAaImAgFnP
úspěšnou	úspěšný	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
již	již	k6eAd1
dříve	dříve	k6eAd2
avizovaným	avizovaný	k2eAgInPc3d1
příchodům	příchod	k1gInPc3
Hakima	Hakim	k1gMnSc2
Ziyecha	Ziyecha	k1gMnSc1
a	a	k8xC
Timo	Timo	k1gMnSc1
Wernera	Werner	k1gMnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
přidali	přidat	k5eAaPmAgMnP
Ben	Ben	k1gInSc4
Chillwell	Chillwella	k1gFnPc2
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
Mendy	Menda	k1gFnSc2
<g/>
,	,	kIx,
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
a	a	k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
talentů	talent	k1gInPc2
fotbalového	fotbalový	k2eAgInSc2d1
světa	svět	k1gInSc2
Kai	Kai	k1gMnSc1
Havertz	Havertz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
byl	být	k5eAaImAgInS
poznamenán	poznamenat	k5eAaPmNgInS
proticovidovými	proticovidový	k2eAgNnPc7d1
opatřeními	opatření	k1gNnPc7
<g/>
,	,	kIx,
fotbalové	fotbalový	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
se	se	k3xPyFc4
odehrávaly	odehrávat	k5eAaImAgInP
před	před	k7c7
prázdnými	prázdný	k2eAgFnPc7d1
tribunami	tribuna	k1gFnPc7
a	a	k8xC
nejen	nejen	k6eAd1
díky	díky	k7c3
vyššímu	vysoký	k2eAgNnSc3d2
vytížení	vytížení	k1gNnSc3
hráčů	hráč	k1gMnPc2
podávali	podávat	k5eAaImAgMnP
týmy	tým	k1gInPc4
nekonzistentní	konzistentní	k2eNgInPc4d1
výkony	výkon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
slibném	slibný	k2eAgInSc6d1
začátku	začátek	k1gInSc6
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgNnP
Chelsea	Chelseum	k1gNnPc1
favorizována	favorizován	k2eAgNnPc1d1
k	k	k7c3
aspirantům	aspirant	k1gMnPc3
na	na	k7c4
titul	titul	k1gInSc4
přišla	přijít	k5eAaPmAgFnS
herní	herní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
a	a	k8xC
propad	propad	k1gInSc1
tabulkou	tabulka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frank	Frank	k1gMnSc1
Lampard	Lampard	k1gMnSc1
byl	být	k5eAaImAgMnS
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
odvolán	odvolat	k5eAaPmNgInS
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
Thomasem	Thomas	k1gMnSc7
Tuchelem	Tuchel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
při	při	k7c6
utkání	utkání	k1gNnSc6
s	s	k7c7
Tottenhamem	Tottenham	k1gInSc7
Hotspur	Hotspura	k1gFnPc2
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
domácí	domácí	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
/	/	kIx~
Premier	Premier	k1gInSc1
League	Leagu	k1gInSc2
(	(	kIx(
6	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
(	(	kIx(
8	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
EFL	EFL	kA
Cup	cup	k1gInSc1
(	(	kIx(
5	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Community	Communit	k1gInPc1
Shield	Shielda	k1gFnPc2
(	(	kIx(
4	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1955	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
2	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
2	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1998	#num#	k4
</s>
<s>
Hráči	hráč	k1gMnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Chelsea	Chelse	k1gInSc2
FC	FC	kA
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgFnSc1d1
soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgMnPc1d1
k	k	k7c3
datu	datum	k1gNnSc3
<g/>
:	:	kIx,
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Kepa	kep	k1gMnSc4
Arrizabalaga	Arrizabalag	k1gMnSc4
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Antonio	Antonio	k1gMnSc1
Rüdiger	Rüdiger	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Marcos	Marcos	k1gMnSc1
Alonso	Alonsa	k1gFnSc5
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Andreas	Andreas	k1gInSc1
Christensen	Christensna	k1gFnPc2
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jorginho	Jorginze	k6eAd1
(	(	kIx(
<g/>
zástupce	zástupce	k1gMnSc1
kapitána	kapitán	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
(	(	kIx(
<g/>
třetí	třetí	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
N	N	kA
<g/>
'	'	kIx"
<g/>
Golo	Golo	k6eAd1
Kanté	Kantý	k2eAgNnSc1d1
(	(	kIx(
<g/>
čtvrtý	čtvrtý	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Tammy	Tamma	k1gFnPc1
Abraham	Abraham	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Christian	Christian	k1gMnSc1
Pulišić	Pulišić	k1gMnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Timo	Timo	k1gMnSc1
Werner	Werner	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Willy	Willa	k1gMnSc2
Caballero	caballero	k1gMnSc1
</s>
<s>
15	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Kurt	Kurt	k1gMnSc1
Zouma	Zoumum	k1gNnSc2
</s>
<s>
16	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Édouard	Édouard	k1gInSc1
Mendy	Menda	k1gFnSc2
</s>
<s>
17	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Olivier	Olivier	k1gMnSc1
Giroud	Giroud	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Mason	mason	k1gMnSc1
Mount	Mount	k1gMnSc1
</s>
<s>
20	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Callum	Callum	k1gInSc1
Hudson-Odoi	Hudson-Odo	k1gFnSc2
</s>
<s>
21	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ben	Ben	k1gInSc1
Chilwell	Chilwella	k1gFnPc2
</s>
<s>
22	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Hakim	Hakim	k1gMnSc1
Zijach	Zijach	k1gMnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Billy	Bill	k1gMnPc4
Gilmour	Gilmour	k1gMnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Reece	Reece	k1gMnSc1
James	James	k1gMnSc1
</s>
<s>
28	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
César	César	k1gMnSc1
Azpilicueta	Azpilicuet	k1gMnSc2
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
29	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Kai	Kai	k?
Havertz	Havertz	k1gInSc1
</s>
<s>
33	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Emerson	Emerson	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
B	B	kA
</s>
<s>
Teddy	Teddy	k6eAd1
Sharman-Lowe	Sharman-Lowe	k1gFnSc1
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Dujon	Dujon	k1gInSc1
Sterling	sterling	k1gInSc1
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Charly	Charla	k1gFnPc1
Musonda	Musond	k1gMnSc2
</s>
<s>
Hráči	hráč	k1gMnPc1
bez	bez	k7c2
smlouvy	smlouva	k1gFnSc2
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
B	B	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
B	B	kA
</s>
<s>
Jamal	Jamal	k1gMnSc1
Blackman	Blackman	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Rotherhamu	Rotherham	k1gInSc6
United	United	k1gInSc4
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
B	B	kA
</s>
<s>
Jamie	Jamie	k1gFnSc1
Cumming	Cumming	k1gInSc1
(	(	kIx(
<g/>
ve	v	k7c6
Stevenage	Stevenage	k1gFnSc6
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
B	B	kA
</s>
<s>
Nathan	Nathan	k1gMnSc1
Baxter	Baxter	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Accringtonu	Accrington	k1gInSc6
Stanley	Stanlea	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Abdul	Abdul	k1gMnSc1
Rahman	Rahman	k1gMnSc1
Baba	baba	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
PAOKu	PAOKum	k1gNnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Davide	David	k1gMnSc5
Zappacosta	Zappacosta	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Janově	Janov	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Ethan	ethan	k1gInSc1
Ampadu	Ampad	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
Sheffieldu	Sheffieldo	k1gNnSc6
United	United	k1gInSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Fikayo	Fikayo	k1gNnSc1
Tomori	Tomor	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
AC	AC	kA
Milán	Milán	k1gInSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Ian	Ian	k?
Maatsen	Maatsen	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
Charltonu	Charlton	k1gInSc6
Athletic	Athletice	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Juan	Juan	k1gMnSc1
Castillo	Castillo	k1gNnSc4
(	(	kIx(
<g/>
v	v	k7c6
ADO	ADO	kA
Den	den	k1gInSc1
Haag	Haag	k1gInSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Jake	Jake	k1gFnSc1
Clarke-Salter	Clarke-Saltra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Birminghamu	Birmingham	k1gInSc6
City	City	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Marc	Marc	k1gFnSc1
Guehi	Gueh	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c4
Swansea	Swanseum	k1gNnPc4
City	City	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Matt	Matt	k1gMnSc1
Miazga	Miazg	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
Anderlechtu	Anderlecht	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Malang	Malang	k1gMnSc1
Sarr	Sarr	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Porto	porto	k1gNnSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
</s>
<s>
Trevoh	Trevoh	k1gMnSc1
Chalobah	Chalobah	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Lorient	Lorient	k1gInSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Conor	Conor	k1gInSc1
Gallagher	Gallaghra	k1gFnPc2
(	(	kIx(
<g/>
ve	v	k7c6
WBA	WBA	kA
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Danny	Dann	k1gInPc1
Drinkwater	Drinkwatra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Kası	Kası	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Danilo	danit	k5eAaImAgNnS
Pantić	Pantić	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
Čukarički	Čukaričk	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Jon	Jon	k?
Russell	Russell	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Accringtonu	Accrington	k1gInSc6
Stanley	Stanlea	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Kenedy	Kened	k1gMnPc4
(	(	kIx(
<g/>
v	v	k7c6
Granadě	Granada	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Lewis	Lewis	k1gFnSc1
Baker	Baker	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Trabzonspor	Trabzonspor	k1gInSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Luke	Luke	k1gFnSc1
McCormick	McCormicka	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c4
Bristol	Bristol	k1gInSc4
Rovers	Roversa	k1gFnPc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Marco	Marco	k1gMnSc1
van	vana	k1gFnPc2
Ginkel	Ginkel	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
PSV	PSV	kA
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Ross	Ross	k1gInSc1
Barkley	Barklea	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c4
Aston	Aston	k1gNnSc4
Ville	Ville	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Ruben	ruben	k2eAgInSc1d1
Loftus-Cheek	Loftus-Cheek	k1gInSc1
(	(	kIx(
<g/>
ve	v	k7c6
Fulhamu	Fulham	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Tariq	Tariq	k?
Uwakwe	Uwakwe	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
Accringtonu	Accrington	k1gInSc6
Stanley	Stanlea	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Tiémoué	Tiémouý	k2eAgNnSc1d1
Bakayoko	Bakayoko	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
Neapoli	Neapol	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
Victor	Victor	k1gMnSc1
Moses	Moses	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Spartaku	Spartak	k1gInSc6
Moskva	Moskva	k1gFnSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
Armando	Armando	k6eAd1
Broja	Broj	k2eAgFnSc1d1
(	(	kIx(
<g/>
ve	v	k7c6
Vitesse	Vitessa	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
Izzy	Izzy	k1gInPc1
Brown	Browna	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Sheffieldu	Sheffield	k1gInSc6
Wednesday	Wednesdaa	k1gFnSc2
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
Ike	Ike	k?
Ugbo	Ugbo	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c4
Cercle	cercle	k1gInSc4
Brugge	Brugge	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ú	Ú	kA
</s>
<s>
Michy	Mich	k1gInPc1
Batshuayi	Batshuay	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Crystal	Crystal	k1gFnSc6
Palace	Palace	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Známí	známý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Frank	Frank	k1gMnSc1
Lampard	Lampard	k1gMnSc1
</s>
<s>
Známí	známý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
působí	působit	k5eAaImIp3nP
nebo	nebo	k8xC
působili	působit	k5eAaImAgMnP
v	v	k7c4
Chelsea	Chelseus	k1gMnSc4
FC	FC	kA
<g/>
:	:	kIx,
</s>
<s>
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
Joe	Joe	k?
Cole	cola	k1gFnSc3
</s>
<s>
Nicolas	Nicolas	k1gMnSc1
Anelka	Anelka	k1gMnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Ballack	Ballack	k1gMnSc1
</s>
<s>
Roy	Roy	k1gMnSc1
Bentley	Bentlea	k1gFnSc2
</s>
<s>
Frank	Frank	k1gMnSc1
Blunstone	Blunston	k1gInSc5
</s>
<s>
Peter	Peter	k1gMnSc1
Bonetti	Bonetť	k1gFnSc2
</s>
<s>
Steve	Steve	k1gMnSc1
Clarke	Clark	k1gFnSc2
</s>
<s>
Ashley	Ashlea	k1gFnPc1
Cole	cola	k1gFnSc3
</s>
<s>
Charlie	Charlie	k1gMnSc1
Cooke	Cook	k1gFnSc2
</s>
<s>
Carlo	Carlo	k1gNnSc1
Cudicini	Cudicin	k2eAgMnPc1d1
</s>
<s>
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
</s>
<s>
Roberto	Roberta	k1gFnSc5
Di	Di	k1gFnSc5
Matteo	Matteo	k1gNnSc5
</s>
<s>
Kerry	Kerra	k1gFnPc1
Dixon	Dixona	k1gFnPc2
</s>
<s>
Didier	Didier	k1gMnSc1
Drogba	Drogba	k1gMnSc1
</s>
<s>
Tore	Tor	k1gMnSc5
André	André	k1gMnSc5
Flo	Flo	k1gMnSc5
</s>
<s>
William	William	k1gInSc1
Foulke	Foulk	k1gFnSc2
</s>
<s>
Hughie	Hughie	k1gFnSc1
Gallacher	Gallachra	k1gFnPc2
</s>
<s>
William	William	k6eAd1
Gallas	Gallas	k1gInSc1
</s>
<s>
Jimmy	Jimma	k1gFnPc1
Greaves	Greavesa	k1gFnPc2
</s>
<s>
Ruud	Ruud	k1gMnSc1
Gullit	Gullita	k1gFnPc2
</s>
<s>
Eið	Eið	k1gMnSc1
Guð	Guð	k1gFnPc2
</s>
<s>
Ron	ron	k1gInSc1
Harris	Harris	k1gFnSc2
</s>
<s>
Jimmy	Jimma	k1gFnSc2
Floyd	Floyda	k1gFnPc2
Hasselbaink	Hasselbaink	k1gInSc1
</s>
<s>
George	Georg	k1gMnSc2
Hilsdon	Hilsdon	k1gMnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Hughes	Hughes	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Lampard	Lampard	k1gMnSc1
</s>
<s>
Graeme	Graout	k5eAaPmIp1nP,k5eAaImIp1nP,k5eAaBmIp1nP
le	le	k?
Saux	Saux	k1gInSc4
</s>
<s>
Claude	Claude	k6eAd1
Makélélé	Makélélý	k2eAgNnSc1d1
</s>
<s>
Pat	pat	k1gInSc1
Nevin	nevina	k1gFnPc2
</s>
<s>
Peter	Peter	k1gMnSc1
Osgood	Osgooda	k1gFnPc2
</s>
<s>
Dan	Dan	k1gMnSc1
Petrescu	Petrescus	k1gInSc2
</s>
<s>
Gustavo	Gustava	k1gFnSc5
Poyet	Poyeta	k1gFnPc2
</s>
<s>
Arjen	Arjen	k2eAgInSc1d1
Robben	Robben	k2eAgInSc1d1
</s>
<s>
Mark	Mark	k1gMnSc1
Schwarzer	Schwarzer	k1gMnSc1
</s>
<s>
Andrij	Andrít	k5eAaPmRp2nS
Ševčenko	Ševčenka	k1gFnSc5
</s>
<s>
Bobby	Bobba	k1gFnPc1
Tambling	Tambling	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
Terry	Terra	k1gFnSc2
</s>
<s>
Fernando	Fernando	k6eAd1
Torres	Torres	k1gInSc1
</s>
<s>
Terry	Terra	k1gMnSc2
Venables	Venables	k1gMnSc1
</s>
<s>
Clive	Clivat	k5eAaPmIp3nS
Walker	Walker	k1gInSc1
</s>
<s>
Ray	Ray	k?
Wilkins	Wilkins	k1gInSc1
</s>
<s>
Dennis	Dennis	k1gFnSc1
Wise	Wis	k1gFnSc2
</s>
<s>
Gianfranco	Gianfranco	k1gMnSc1
Zola	Zola	k1gMnSc1
</s>
<s>
Eden	Eden	k1gInSc1
Hazard	hazard	k2eAgFnSc2d1
</s>
<s>
Deco	Deco	k6eAd1
</s>
<s>
Trenéři	trenér	k1gMnPc1
</s>
<s>
Současný	současný	k2eAgInSc1d1
realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Trenérský	trenérský	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Tuchel	Tuchel	k1gMnSc1
</s>
<s>
Asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
</s>
<s>
Jody	jod	k1gInPc1
Morris	Morris	k1gFnSc2
</s>
<s>
Joe	Joe	k?
Edwards	Edwards	k1gInSc1
</s>
<s>
Chris	Chris	k1gFnSc1
Jones	Jonesa	k1gFnPc2
</s>
<s>
Anthony	Anthona	k1gFnPc1
Barry	Barra	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
brankářů	brankář	k1gMnPc2
</s>
<s>
Henrique	Henrique	k6eAd1
Hilário	Hilário	k6eAd1
</s>
<s>
James	James	k1gMnSc1
Russell	Russell	k1gMnSc1
</s>
<s>
Kondiční	kondiční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Matt	Matt	k1gMnSc1
Birnie	Birnie	k1gFnSc2
</s>
<s>
Will	Will	k1gMnSc1
Tullett	Tullett	k1gMnSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
trenéři	trenér	k1gMnPc1
</s>
<s>
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
trenéři	trenér	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
alespoň	alespoň	k9
jednu	jeden	k4xCgFnSc4
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
vedli	vést	k5eAaImAgMnP
tým	tým	k1gInSc4
Chelsea	Chelseus	k1gMnSc4
<g/>
:	:	kIx,
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Trofej	trofej	k1gFnSc1
</s>
<s>
Ted	Ted	k1gMnSc1
Drake	Drak	k1gFnSc2
</s>
<s>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
</s>
<s>
Mistr	mistr	k1gMnSc1
první	první	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
Community	Communita	k1gFnSc2
Shield	Shielda	k1gFnPc2
</s>
<s>
Tommy	Tomma	k1gFnPc1
Docherty	Dochert	k1gInPc1
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
</s>
<s>
Carling	Carling	k1gInSc1
Cup	cup	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Sexton	Sexton	k1gInSc1
</s>
<s>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
John	John	k1gMnSc1
Neal	Neal	k1gMnSc1
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
</s>
<s>
Mistr	mistr	k1gMnSc1
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
</s>
<s>
John	John	k1gMnSc1
Hollins	Hollinsa	k1gFnPc2
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
</s>
<s>
Full	Full	k1gMnSc1
Members	Membersa	k1gFnPc2
Cup	cup	k1gInSc4
</s>
<s>
Bobby	Bobba	k1gMnSc2
Campbell	Campbell	k1gMnSc1
</s>
<s>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
Mistr	mistr	k1gMnSc1
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
Full	Full	k1gMnSc1
Members	Membersa	k1gFnPc2
Cup	cup	k1gInSc4
</s>
<s>
Ruud	Ruud	k1gMnSc1
Gullit	Gullita	k1gFnPc2
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
</s>
<s>
Gianluca	Gianluca	k6eAd1
Vialli	Viall	k1gMnPc1
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
League	League	k1gInSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
Community	Communita	k1gFnPc1
Shield	Shieldo	k1gNnPc2
<g/>
,	,	kIx,
UEFA	UEFA	kA
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
</s>
<s>
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
20072013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
Premier	Premier	k1gInSc1
League	Leagu	k1gInSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
×	×	k?
League	Leagu	k1gInSc2
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Community	Communit	k1gInPc1
Shield	Shielda	k1gFnPc2
</s>
<s>
Guus	Guus	k6eAd1
Hiddink	Hiddink	k1gInSc1
</s>
<s>
2009	#num#	k4
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
FA	fa	kA
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Community	Communit	k1gInPc1
Shield	Shielda	k1gFnPc2
</s>
<s>
Roberto	Roberta	k1gFnSc5
Di	Di	k1gFnSc5
Matteo	Matteo	k1gNnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Rafael	Rafael	k1gMnSc1
Benítez	Benítez	k1gMnSc1
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Antonio	Antonio	k1gMnSc5
Conte	Cont	k1gMnSc5
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
Maurizio	Maurizio	k1gNnSc1
Sarri	Sarr	k1gFnSc2
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1905	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1907	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1924	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1930	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
1905	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23822979037	#num#	k4
<g/>
+	+	kIx~
<g/>
5353	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23826578034	#num#	k4
<g/>
+	+	kIx~
<g/>
4657	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138148165362-936	138148165362-936	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138149155661-537	138149155661-537	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138117204770-2329	138117204770-2329	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23820997135	#num#	k4
<g/>
+	+	kIx~
<g/>
3649	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23824686434	#num#	k4
<g/>
+	+	kIx~
<g/>
3054	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138116215173-2228	138116215173-2228	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138167154655-939	138167154655-939	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138813175165-1429	138813175165-1429	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1915	#num#	k4
až	až	k8xS
1918	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142225155651	#num#	k4
<g/>
+	+	kIx~
<g/>
549	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421313164858-1039	1421313164858-1039	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421712134043-346	1421712134043-346	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142918154553-836	142918154553-836	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142914193153-2232	142914193153-2232	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421615115137	#num#	k4
<g/>
+	+	kIx~
<g/>
1447	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242191497649	#num#	k4
<g/>
+	+	kIx~
<g/>
2752	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2422012106252	#num#	k4
<g/>
+	+	kIx~
<g/>
1052	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242238117545	#num#	k4
<g/>
+	+	kIx~
<g/>
3054	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421710156465-144	2421710156465-144	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242221197446	#num#	k4
<g/>
+	+	kIx~
<g/>
2855	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421510176467-340	1421510176467-340	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142168186973-440	142168186973-440	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142147216373-1035	142147216373-1035	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142148206769-236	142148206769-236	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142169177382-941	142169177382-941	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421513146572-743	1421513146572-743	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421413155255-341	1421413155255-341	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421413156565041	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142129216480-1633	142129216480-1633	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
až	až	k8xS
1945	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142167196984-1539	142167196984-1539	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142149195371-1837	142149195371-1837	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421214166968	#num#	k4
<g/>
+	+	kIx~
<g/>
138	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421216145865-740	1421216145865-740	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142128225365-1232	142128225365-1232	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142148205272-2036	142148205272-2036	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421211195666-1035	1421211195666-1035	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421612147468	#num#	k4
<g/>
+	+	kIx~
<g/>
644	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422012108157-2452	1422012108157-2452	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421411176477-1339	1421411176477-1339	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421313167373039	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421512158379	#num#	k4
<g/>
+	+	kIx~
<g/>
440	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142184207798-2140	142184207798-2140	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142149197691-1537	142149197691-1537	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421572098100-237	1421572098100-237	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142910236394-3128	142910236394-3128	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242244148142	#num#	k4
<g/>
+	+	kIx~
<g/>
3952	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422010127256	#num#	k4
<g/>
+	+	kIx~
<g/>
1650	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142248108954	#num#	k4
<g/>
+	+	kIx~
<g/>
3556	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142227136553	#num#	k4
<g/>
+	+	kIx~
<g/>
1251	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421514136762	#num#	k4
<g/>
+	+	kIx~
<g/>
544	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421812126268-648	1421812126268-648	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422010127353	#num#	k4
<g/>
+	+	kIx~
<g/>
2050	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142211387050	#num#	k4
<g/>
+	+	kIx~
<g/>
2055	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142181595242	#num#	k4
<g/>
+	+	kIx~
<g/>
1051	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421812125849	#num#	k4
<g/>
+	+	kIx~
<g/>
948	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421314154951-240	1421314154951-240	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421213175660-437	1421213175660-437	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142915184272-3033	142915184272-3033	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421216145354-140	2421216145354-140	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242211387353	#num#	k4
<g/>
+	+	kIx~
<g/>
2055	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421114174669-2336	1421114174669-2336	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142510274492-4820	142510274492-4820	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242237126652	#num#	k4
<g/>
+	+	kIx~
<g/>
1453	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421412164641	#num#	k4
<g/>
+	+	kIx~
<g/>
540	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421512156060057	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421114175161-1047	2421114175161-1047	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242251349040	#num#	k4
<g/>
+	+	kIx~
<g/>
5088	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421812126348	#num#	k4
<g/>
+	+	kIx~
<g/>
1566	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422011115756	#num#	k4
<g/>
+	+	kIx~
<g/>
171	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421313165364-1152	1421313165364-1152	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
140915165068-1842	140915165068-1842	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
246291259650	#num#	k4
<g/>
+	+	kIx~
<g/>
4699	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381612155850	#num#	k4
<g/>
+	+	kIx~
<g/>
860	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381310155869-1149	1381310155869-1149	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421314155060-1053	1421314155060-1053	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1421414145154-356	1421414145154-356	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1421312174953-451	1421312174953-451	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1421315145055-554	1421315145055-554	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381214124644	#num#	k4
<g/>
+	+	kIx~
<g/>
250	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381611115855	#num#	k4
<g/>
+	+	kIx~
<g/>
359	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138203157143	#num#	k4
<g/>
+	+	kIx~
<g/>
2863	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138201535730	#num#	k4
<g/>
+	+	kIx~
<g/>
2775	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138181195334	#num#	k4
<g/>
+	+	kIx~
<g/>
1965	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381710116845	#num#	k4
<g/>
+	+	kIx~
<g/>
2361	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138171386638	#num#	k4
<g/>
+	+	kIx~
<g/>
2864	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138191096838	#num#	k4
<g/>
+	+	kIx~
<g/>
3067	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13824776730	#num#	k4
<g/>
+	+	kIx~
<g/>
3779	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13829817215	#num#	k4
<g/>
+	+	kIx~
<g/>
5795	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13829457222	#num#	k4
<g/>
+	+	kIx~
<g/>
5091	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138241136424	#num#	k4
<g/>
+	+	kIx~
<g/>
4083	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138251036526	#num#	k4
<g/>
+	+	kIx~
<g/>
3985	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825856824	#num#	k4
<g/>
+	+	kIx~
<g/>
4483	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138275610332	#num#	k4
<g/>
+	+	kIx~
<g/>
7186	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13821896933	#num#	k4
<g/>
+	+	kIx~
<g/>
3671	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381810106546	#num#	k4
<g/>
+	+	kIx~
<g/>
1964	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13822977539	#num#	k4
<g/>
+	+	kIx~
<g/>
3675	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825767127	#num#	k4
<g/>
+	+	kIx~
<g/>
4482	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13826937332	#num#	k4
<g/>
+	+	kIx~
<g/>
4187	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381214125953	#num#	k4
<g/>
+	+	kIx~
<g/>
650	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13830358533	#num#	k4
<g/>
+	+	kIx~
<g/>
5293	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138217106238	#num#	k4
<g/>
+	+	kIx~
<g/>
2470	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13821986339	#num#	k4
<g/>
+	+	kIx~
<g/>
2472	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138206126954	#num#	k4
<g/>
+	+	kIx~
<g/>
1566	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Venku	venku	k6eAd1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Kodaň	Kodaň	k1gFnSc1
XI	XI	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Bělehrad	Bělehrad	k1gInSc1
XI	XI	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Wiener	Wienra	k1gFnPc2
Sport-Club	Sport-Cluba	k1gFnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc7
TSV	TSV	kA
1860	#num#	k4
München	München	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Greenock	Greenock	k1gInSc1
Morton	Morton	k1gInSc4
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AFC	AFC	kA
Door	Door	k1gInSc4
Wilskracht	Wilskracht	k2eAgInSc1d1
Sterk	Sterk	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Aris	Aris	k1gInSc1
Soluň	Soluň	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Jeunesse	Jeunesse	k1gFnSc1
Hautcharage	Hautcharage	k1gNnSc1
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Å	Å	k1gFnPc2
FF	ff	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ŠK	ŠK	kA
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Tromsø	Tromsø	k1gFnPc2
IL	IL	kA
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Real	Real	k1gInSc1
Betis	Betis	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Vicenza	Vicenza	k1gFnSc1
Calcio	Calcio	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
VfB	VfB	k1gFnSc2
Stuttgart	Stuttgart	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Helsingborgs	Helsingborgsa	k1gFnPc2
IF	IF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Kø	Kø	k1gNnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Vå	Vå	k1gMnSc2
IF	IF	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
RCD	RCD	kA
Mallorca	Mallorc	k1gInSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Skonto	skonto	k1gNnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Hertha	Hertha	k1gFnSc1
BSC	BSC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Feyenoord	Feyenoord	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
St.	st.	kA
Gallen	Gallen	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
Levski	Levske	k1gFnSc4
Sofia	Sofia	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hapoel	Hapoela	k1gFnPc2
Tel	tel	kA
Aviv	Aviv	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Viking	Viking	k1gMnSc1
FK	FK	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
MŠK	MŠK	kA
Žilina	Žilina	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Beşiktaş	Beşiktaş	k?
JK	JK	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
VfB	VfB	k1gFnSc2
Stuttgart	Stuttgart	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AS	as	k9
Monaco	Monaco	k6eAd1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Betis	Betis	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
SV	sv	kA
Werder	Werder	k1gMnSc1
Bremen	Bremen	k2eAgMnSc1d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
PFK	PFK	kA
Levski	Levske	k1gFnSc4
Sofia	Sofia	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Rosenborg	Rosenborg	k1gInSc1
BK	BK	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Schalke	Schalke	k1gFnSc1
0	#num#	k4
<g/>
40	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympiacos	Olympiacosa	k1gFnPc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Fenerbahçe	Fenerbahç	k1gInSc2
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
FC	FC	kA
Girondins	Girondins	k1gInSc1
de	de	k?
Bordeaux	Bordeaux	k1gNnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
CFR	CFR	kA
Cluj	Cluj	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
APOEL	APOEL	kA
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
MŠK	MŠK	kA
Žilina	Žilina	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Kø	Kø	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
KRC	KRC	kA
Genk	Genk	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
SSC	SSC	kA
Napoli	Napole	k1gFnSc6
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Nordsjæ	Nordsjæ	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Basel	Basel	k1gInSc4
18933	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2013	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
FC	FC	kA
Schalke	Schalke	k1gFnSc1
0	#num#	k4
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18931	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Steaua	Steaua	k1gFnSc1
Bucureș	Bucureș	k1gFnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Galatasaray	Galatasaraa	k1gFnSc2
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
FC	FC	kA
Schalke	Schalke	k1gFnSc1
0	#num#	k4
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
NK	NK	kA
Maribor	Maribor	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
Maccabi	Maccabi	k6eAd1
Tel	tel	kA
Aviv	Aviva	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Qarabağ	Qarabağ	k?
FK	FK	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
L	L	kA
</s>
<s>
PAOK	PAOK	kA
Soluň	Soluň	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
MOL	mol	k1gMnSc1
Vidi	vidi	kA
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
FK	FK	kA
BATE	BATE	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
Malmö	Malmö	k1gFnPc2
FF	ff	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc4
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Eintracht	Eintracht	k2eAgInSc1d1
Frankfurt	Frankfurt	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Lille	Lille	k1gFnSc1
OSC	OSC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Krasnodar	Krasnodar	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Stade	Stade	k6eAd1
Rennais	Rennais	k1gInSc1
FC	FC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Hymna	hymna	k1gFnSc1
</s>
<s>
Hymnou	hymna	k1gFnSc7
příznivců	příznivec	k1gMnPc2
Chelsea	Chelseum	k1gNnSc2
je	být	k5eAaImIp3nS
píseň	píseň	k1gFnSc1
Blue	Blu	k1gMnSc2
Is	Is	k1gMnSc2
The	The	k1gMnSc1
Colour	Colour	k1gMnSc1
(	(	kIx(
<g/>
narážka	narážka	k1gFnSc1
na	na	k7c4
modré	modrý	k2eAgInPc4d1
dresy	dres	k1gInPc4
týmu	tým	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
Daniel	Daniel	k1gMnSc1
Boone	Boon	k1gInSc5
a	a	k8xC
Rod	rod	k1gInSc4
McQueen	McQuena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazpívali	nazpívat	k5eAaBmAgMnP,k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
tehdejší	tehdejší	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
skladbu	skladba	k1gFnSc4
uvedl	uvést	k5eAaPmAgMnS
František	František	k1gMnSc1
Ringo	Ringo	k1gMnSc1
Čech	Čech	k1gMnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Zelená	Zelená	k1gFnSc1
je	být	k5eAaImIp3nS
tráva	tráva	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
ji	on	k3xPp3gFnSc4
používala	používat	k5eAaImAgFnS
Československá	československý	k2eAgFnSc1d1
televize	televize	k1gFnSc1
jako	jako	k9
znělku	znělka	k1gFnSc4
fotbalových	fotbalový	k2eAgInPc2d1
přenosů	přenos	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
PROBERT	PROBERT	kA
<g/>
,	,	kIx,
Greg	Greg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
:	:	kIx,
Ranking	Ranking	k1gInSc1
the	the	k?
Blues	blues	k1gNnSc1
<g/>
'	'	kIx"
5	#num#	k4
Most	most	k1gInSc1
Hated	Hated	k1gMnSc1
Rivals	Rivals	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-02-01	2012-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARKOVIĆ	MARKOVIĆ	kA
<g/>
,	,	kIx,
Nebojša	Nebojšum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelse	k1gInSc2
v	v	k7c6
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
craziest	craziest	k1gMnSc1
European	European	k1gMnSc1
rivalry	rivalra	k1gFnSc2
of	of	k?
the	the	k?
21	#num#	k4
<g/>
st	st	kA
century	centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medium	medium	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-02-20	2018-02-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Anglická	anglický	k2eAgFnSc1d1
Premier	Premier	k1gInSc4
League	Leagu	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-17	2020-07-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARKETA	MARKETA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglická	anglický	k2eAgFnSc1d1
Premier	Premier	k1gInSc4
League	Leagu	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-09	2020-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Anglická	anglický	k2eAgFnSc1d1
Premier	Premier	k1gInSc4
League	Leagu	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-11	2020-11-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Shoot-out	Shoot-out	k1gMnSc1
win	win	k?
ends	ends	k6eAd1
Chelsea	Chelse	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
long	long	k1gInSc4
wait	wait	k1gInSc4
for	forum	k1gNnPc2
glory	glora	k1gMnSc2
<g/>
.	.	kIx.
www.uefa.com	www.uefa.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Benitez	Benitez	k1gMnSc1
replaces	replaces	k1gMnSc1
Roberto	Roberta	k1gFnSc5
Di	Di	k1gFnPc6
Matteo	Matteo	k1gMnSc1
as	as	k1gNnSc2
Chelsea	Chelsea	k1gMnSc1
manager	manager	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2012-11-21	2012-11-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chelsea	Chelsea	k1gMnSc1
<g/>
:	:	kIx,
Rafael	Rafael	k1gMnSc1
Benitez	Benitez	k1gMnSc1
boos	boos	k6eAd1
will	wilnout	k5eAaPmAgMnS
continue	continue	k1gInSc4
say	say	k?
fans	fans	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2012-11-26	2012-11-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.tyden.cz/rubriky/sport/fotbal/zahranicni-souteze/cech-ziskal-dalsi-titul-chelsea-vyhrala-premier-league_341661.html	http://www.tyden.cz/rubriky/sport/fotbal/zahranicni-souteze/cech-ziskal-dalsi-titul-chelsea-vyhrala-premier-league_341661.html	k1gInSc1
<g/>
↑	↑	k?
www.capitalonecup.co.uk	www.capitalonecup.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.dailymail.co.uk/sport/football/article-3092701/Chelsea-led-Premier-League-table-record-274-days-Sunday.html	http://www.dailymail.co.uk/sport/football/article-3092701/Chelsea-led-Premier-League-table-record-274-days-Sunday.html	k1gInSc1
<g/>
↑	↑	k?
http://www.bbc.com/sport/0/football/31816148	http://www.bbc.com/sport/0/football/31816148	k4
<g/>
↑	↑	k?
http://www.bbc.com/sport/0/football/30860394	http://www.bbc.com/sport/0/football/30860394	k4
<g/>
↑	↑	k?
http://www.bbc.com/sport/0/football/34577998	http://www.bbc.com/sport/0/football/34577998	k4
<g/>
↑	↑	k?
http://www.liga.cz/fotbal/evropa/liga-mistru/?stage=4Y8uZGlQ	http://www.liga.cz/fotbal/evropa/liga-mistru/?stage=4Y8uZGlQ	k4
<g/>
↑	↑	k?
http://www.bbc.com/sport/0/football/34670192	http://www.bbc.com/sport/0/football/34670192	k4
<g/>
↑	↑	k?
http://www.mirror.co.uk/sport/row-zed/chelsea-fans-angry-team-playing-7040884	http://www.mirror.co.uk/sport/row-zed/chelsea-fans-angry-team-playing-7040884	k4
<g/>
↑	↑	k?
http://www.chelsea-fc.cz/10174-hiddink-novym-manazerem	http://www.chelsea-fc.cz/10174-hiddink-novym-manazer	k1gInSc7
<g/>
↑	↑	k?
Premier	Premier	k1gInSc1
League	League	k1gInSc1
table	tablo	k1gNnSc6
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
16	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Match	Match	k1gInSc1
Report	report	k1gInSc1
Everton	Everton	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Chelsea	Chelseum	k1gNnSc2
<g/>
,	,	kIx,
chelseafc	chelseafc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Match	Match	k1gInSc1
Report	report	k1gInSc1
<g/>
:	:	kIx,
PSG	PSG	kA
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Chelsea	Chelseum	k1gNnSc2
<g/>
,	,	kIx,
chelseafc	chelseafc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Match	Match	k1gInSc1
Report	report	k1gInSc1
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
PSG	PSG	kA
<g/>
,	,	kIx,
chelseafc	chelseafc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
wonderkid	wonderkid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conte	Cont	k1gInSc5
novým	nový	k2eAgMnSc7d1
manažerem	manažer	k1gMnSc7
Chelsea	Chelse	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
chelsea-fc	chelsea-fc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PODMOL	podmol	k1gInSc1
<g/>
,	,	kIx,
Hanis	Hanis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonio	Antonio	k1gMnSc1
Conte	Cont	k1gInSc5
<g/>
:	:	kIx,
Italský	italský	k2eAgMnSc1d1
krejčí	krejčí	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
chelsea-fc	chelsea-fc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fixtures	Fixtures	k1gInSc1
&	&	k?
Results	Results	k1gInSc1
2016-2017	2016-2017	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
chelseafc	chelseafc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KINSELLA	KINSELLA	kA
<g/>
,	,	kIx,
Nizaar	Nizaar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forget	Forget	k1gMnSc1
Arsenal	Arsenal	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Invincibles	Invincibles	k1gInSc1
and	and	k?
meet	meet	k1gInSc1
Conte	Cont	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
invincible	invincible	k6eAd1
Chelsea	Chelse	k2eAgNnPc1d1
as	as	k1gNnPc1
they	thea	k1gFnSc2
go	go	k?
13	#num#	k4
in	in	k?
a	a	k8xC
row	row	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
goal	goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
Season	Season	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
premierleague	premierleague	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnSc1
2016-17	2016-17	k4
season	season	k1gMnSc1
review	review	k?
<g/>
:	:	kIx,
our	our	k?
writers	writers	k1gInSc1
<g/>
’	’	k?
best	best	k1gInSc1
and	and	k?
worst	worst	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-22	2017-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MERSON	MERSON	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelse	k1gInSc2
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
Premier	Premier	k1gMnSc1
League	Leagu	k1gInSc2
season	season	k1gMnSc1
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gFnSc1
Sports	Sportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-14	2018-05-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
LAURENCE	LAURENCE	kA
<g/>
,	,	kIx,
Wilfred	Wilfred	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
End	End	k1gMnSc1
of	of	k?
Season	Season	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Report	report	k1gInSc1
Card	Card	k1gMnSc1
From	From	k1gInSc1
the	the	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
Campaign	Campaigna	k1gFnPc2
<g/>
.	.	kIx.
90	#num#	k4
<g/>
min	mina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-22	2018-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIMON	Simon	k1gMnSc1
<g/>
,	,	kIx,
Burnton	Burnton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
2018-19	2018-19	k4
preview	preview	k?
No	no	k9
6	#num#	k4
<g/>
:	:	kIx,
Chelsea	Chelse	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-01	2018-08-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEVAN	BEVAN	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelse	k1gInSc2
0	#num#	k4
Man	mana	k1gFnPc2
City	city	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-05	2018-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
LAURENCE	LAURENCE	kA
<g/>
,	,	kIx,
Wilfred	Wilfred	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelse	k1gInSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
Review	Review	k1gFnPc2
<g/>
:	:	kIx,
End	End	k1gFnSc1
of	of	k?
Season	Season	k1gInSc1
Report	report	k1gInSc1
Card	Carda	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Blues	blues	k1gFnSc2
<g/>
.	.	kIx.
90	#num#	k4
<g/>
.	.	kIx.
<g/>
min	mina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-14	2019-05-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelsea	k1gMnSc1
je	být	k5eAaImIp3nS
anglickým	anglický	k2eAgMnSc7d1
šampionem	šampion	k1gMnSc7
<g/>
,	,	kIx,
titul	titul	k1gInSc4
stvrdil	stvrdit	k5eAaPmAgMnS
náhradník	náhradník	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12.05	12.05	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Benfica	Benfica	k1gFnSc1
-	-	kIx~
Chelsea	Chelsea	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-15	2013-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chelsea	Chelsea	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
squad	squad	k1gInSc1
named	named	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chelsea	Chelse	k1gInSc2
F.	F.	kA
<g/>
C.	C.	kA
<g/>
,	,	kIx,
20	#num#	k4
October	October	k1gInSc1
2020	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
"	"	kIx"
<g/>
CHELSEA	CHELSEA	kA
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fchd	fchd	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chelsea	Chelse	k1gInSc2
FC	FC	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Největší	veliký	k2eAgFnPc1d3
česko-slovenské	česko-slovenský	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
-	-	kIx~
jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
Domácí	domácí	k2eAgInSc1d1
stadion	stadion	k1gInSc4
</s>
<s>
Stamford	Stamford	k1gMnSc1
Bridge	Bridg	k1gInSc2
Jednotlivé	jednotlivý	k2eAgFnPc1d1
sezony	sezona	k1gFnPc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Premier	Premier	k1gMnSc1
League	Leagu	k1gInSc2
–	–	k?
anglická	anglický	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Arsenal	Arsenat	k5eAaPmAgMnS,k5eAaImAgMnS
FC	FC	kA
•	•	k?
Aston	Aston	k1gMnSc1
Villa	Villa	k1gMnSc1
FC	FC	kA
•	•	k?
Brighton	Brighton	k1gInSc1
&	&	k?
Hove	Hove	k1gInSc1
Albion	Albion	k1gInSc1
FC	FC	kA
•	•	k?
Burnley	Burnlea	k1gFnPc4
FC	FC	kA
•	•	k?
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
•	•	k?
Crystal	Crystal	k1gMnSc7
Palace	Palace	k1gFnSc2
FC	FC	kA
•	•	k?
Everton	Everton	k1gInSc1
FC	FC	kA
•	•	k?
Leicester	Leicester	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Newcastle	Newcastle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Sheffield	Sheffield	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Southampton	Southampton	k1gInSc1
FC	FC	kA
•	•	k?
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Wolverhampton	Wolverhampton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
Football	Football	k1gInSc1
League	Leagu	k1gFnSc2
<g/>
:	:	kIx,
1888	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1889	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1890	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1891	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
FL	FL	kA
First	First	k1gInSc1
Division	Division	k1gInSc1
<g/>
:	:	kIx,
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1894	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1895	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1896	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
1900	#num#	k4
•	•	k?
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
1903	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
1917	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
•	•	k?
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
26	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
<g/>
:	:	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Accrington	Accrington	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Darwen	Darwen	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Glossop	Glossop	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gInSc1
Park	park	k1gInSc1
Avenue	avenue	k1gFnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bury	Bury	k?
FC	FC	kA
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Brentford	Brentford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Grimsby	Grimsba	k1gFnSc2
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Preston	Preston	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leyton	Leyton	k1gInSc1
Orient	Orient	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Northampton	Northampton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carlisle	Carlisle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bristol	Bristol	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxford	Oxford	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Millwall	Millwall	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Luton	Luton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Notts	Notts	k1gInSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldham	Oldham	k1gInSc1
Athletic	Athletice	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swindon	Swindon	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barnsley	Barnslea	k1gFnPc4
FC	FC	kA
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sheffield	Sheffield	k1gInSc1
Wednesday	Wednesdaa	k1gMnSc2
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wimbledon	Wimbledon	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Coventry	Coventr	k1gMnPc4
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ipswich	Ipswich	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charlton	Charlton	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Derby	derby	k1gNnSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portsmouth	Portsmouth	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Birmingham	Birmingham	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Blackpool	Blackpool	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Blackburn	Blackburn	k1gInSc1
Rovers	Rovers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bolton	Bolton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wigan	Wigan	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Reading	Reading	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Queens	Queens	k1gInSc1
Park	park	k1gInSc1
Rangers	Rangers	k1gInSc4
FC	FC	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hull	Hull	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Middlesbrough	Middlesbrough	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sunderland	Sunderland	k1gInSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stoke	Stoke	k1gNnSc2
City	City	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swansea	Swanseum	k1gNnSc2
City	City	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cardiff	Cardiff	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Huddersfield	Huddersfield	k1gInSc1
Town	Town	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFC	AFC	kA
Bournemouth	Bournemouth	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norwich	Norwich	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Watford	Watford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Golden	Goldno	k1gNnPc2
Boot	Boot	k1gInSc1
•	•	k?
Golden	Goldna	k1gFnPc2
Glove	Gloev	k1gFnSc2
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Playmaker	Playmaker	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
dle	dle	k7c2
FWA	FWA	kA
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
•	•	k?
Football	Football	k1gInSc1
League	League	k1gFnSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
•	•	k?
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
Asia	Asius	k1gMnSc2
Trophy	Tropha	k1gFnSc2
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
•	•	k?
Professional	Professional	k1gMnSc1
Development	Development	k1gMnSc1
League	League	k1gFnPc2
U18	U18	k1gMnSc1
&	&	k?
U23	U23	k1gMnSc1
•	•	k?
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
International	International	k1gMnSc1
Cup	cup	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Football	Footballa	k1gFnPc2
Association	Association	k1gInSc1
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
:	:	kIx,
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Super	super	k2eAgNnSc7d1
League	League	k1gNnSc7
•	•	k?
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
League	League	k1gNnSc7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Community	Communit	k1gInPc7
Shield	Shielda	k1gFnPc2
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2008478809	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4221430-0	4221430-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0612	#num#	k4
5760	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2004016488	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
144199101	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2004016488	#num#	k4
</s>
