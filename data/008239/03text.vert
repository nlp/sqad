<p>
<s>
Inženýr	inženýr	k1gMnSc1	inženýr
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
ingénieur	ingénieur	k1gMnSc1	ingénieur
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
lat.	lat.	k?	lat.
ingeniator	ingeniator	k1gInSc1	ingeniator
<g/>
,	,	kIx,	,
přechýlená	přechýlený	k2eAgFnSc1d1	přechýlená
podoba	podoba	k1gFnSc1	podoba
inženýrka	inženýrka	k1gFnSc1	inženýrka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
absolventa	absolvent	k1gMnSc2	absolvent
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc2	lesnictví
nebo	nebo	k8xC	nebo
vojenství	vojenství	k1gNnSc2	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
udělován	udělován	k2eAgInSc1d1	udělován
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
technických	technický	k2eAgInPc2d1	technický
<g/>
,	,	kIx,	,
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
<g/>
,	,	kIx,	,
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
a	a	k8xC	a
na	na	k7c6	na
technických	technický	k2eAgInPc6d1	technický
směrech	směr	k1gInPc6	směr
vysokých	vysoký	k2eAgFnPc2d1	vysoká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
i	i	k9	i
zkratka	zkratka	k1gFnSc1	zkratka
ing.	ing.	kA	ing.
nebo	nebo	k8xC	nebo
inž	inž	k?	inž
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
ingerum	ingerum	k1gInSc1	ingerum
architectus	architectus	k1gInSc1	architectus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecněji	obecně	k6eAd2	obecně
je	být	k5eAaImIp3nS	být
inženýr	inženýr	k1gMnSc1	inženýr
označení	označení	k1gNnSc2	označení
–	–	k?	–
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
či	či	k8xC	či
titul	titul	k1gInSc4	titul
–	–	k?	–
pro	pro	k7c4	pro
odborníka	odborník	k1gMnSc4	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Získá	získat	k5eAaPmIp3nS	získat
ho	on	k3xPp3gMnSc4	on
absolvent	absolvent	k1gMnSc1	absolvent
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
navazujícího	navazující	k2eAgNnSc2d1	navazující
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
případě	případ	k1gInSc6	případ
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
samostatného	samostatný	k2eAgMnSc2d1	samostatný
<g/>
,	,	kIx,	,
celistvého	celistvý	k2eAgInSc2d1	celistvý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
souvislého	souvislý	k2eAgNnSc2d1	souvislé
a	a	k8xC	a
nenavazujícího	navazující	k2eNgNnSc2d1	nenavazující
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
případně	případně	k6eAd1	případně
i	i	k9	i
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
diplomant	diplomant	k1gMnSc1	diplomant
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
student	student	k1gMnSc1	student
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
student	student	k1gMnSc1	student
pracující	pracující	k1gMnSc1	pracující
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
diplomové	diplomový	k2eAgNnSc4d1	diplomové
(	(	kIx(	(
<g/>
magisterské	magisterský	k2eAgNnSc4d1	magisterské
<g/>
/	/	kIx~	/
<g/>
inženýrské	inženýrský	k2eAgNnSc4d1	inženýrské
<g/>
)	)	kIx)	)
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Magisterské	magisterský	k2eAgNnSc1d1	magisterské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
řádné	řádný	k2eAgNnSc4d1	řádné
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
státnice	státnice	k1gFnSc1	státnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
Tituly	titul	k1gInPc1	titul
této	tento	k3xDgFnSc2	tento
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
praxi	praxe	k1gFnSc4	praxe
běžně	běžně	k6eAd1	běžně
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
profesních	profesní	k2eAgInPc2d1	profesní
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
tak	tak	k9	tak
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývají	bývat	k5eAaImIp3nP	bývat
užívány	užíván	k2eAgInPc1d1	užíván
až	až	k8xS	až
tituly	titul	k1gInPc1	titul
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gNnSc4	jejich
formální	formální	k2eAgNnSc4d1	formální
užívání	užívání	k1gNnSc4	užívání
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
častější	častý	k2eAgInSc1d2	častější
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k9	jako
formálně	formálně	k6eAd1	formálně
správné	správný	k2eAgNnSc1d1	správné
oslovování	oslovování	k1gNnSc1	oslovování
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívá	užívat	k5eAaImIp3nS	užívat
pane	pan	k1gMnSc5	pan
inženýre	inženýr	k1gMnSc5	inženýr
/	/	kIx~	/
paní	paní	k1gFnSc3	paní
inženýrko	inženýrka	k1gFnSc5	inženýrka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kodifikovaná	kodifikovaný	k2eAgFnSc1d1	kodifikovaná
zkratka	zkratka	k1gFnSc1	zkratka
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Ing.	ing.	kA	ing.
<g/>
"	"	kIx"	"
Takto	takto	k6eAd1	takto
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
zavedena	zavést	k5eAaPmNgFnS	zavést
jakožto	jakožto	k8xS	jakožto
chráněné	chráněný	k2eAgNnSc1d1	chráněné
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
označení	označení	k1gNnSc1	označení
císařským	císařský	k2eAgNnSc7d1	císařské
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
130	[number]	k4	130
ř.	ř.	k?	ř.
z.	z.	k?	z.
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
a	a	k8xC	a
výnosem	výnos	k1gInSc7	výnos
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
osvěty	osvěta	k1gFnSc2	osvěta
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
takto	takto	k6eAd1	takto
určena	určit	k5eAaPmNgFnS	určit
všemi	všecek	k3xTgInPc7	všecek
vysokoškolskými	vysokoškolský	k2eAgInPc7d1	vysokoškolský
zákony	zákon	k1gInPc7	zákon
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
však	však	k9	však
byla	být	k5eAaImAgFnS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
také	také	k9	také
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
"	"	kIx"	"
<g/>
ing.	ing.	kA	ing.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc1d1	obchodní
(	(	kIx(	(
<g/>
VŠO	VŠO	kA	VŠO
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
fakulta	fakulta	k1gFnSc1	fakulta
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
(	(	kIx(	(
<g/>
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
původní	původní	k2eAgNnSc1d1	původní
tříleté	tříletý	k2eAgNnSc1d1	tříleté
studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
při	při	k7c6	při
reorganizaci	reorganizace	k1gFnSc6	reorganizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgMnPc4d1	čtyřletý
a	a	k8xC	a
studenti	student	k1gMnPc1	student
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
dokončení	dokončený	k2eAgMnPc1d1	dokončený
získávali	získávat	k5eAaImAgMnP	získávat
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
označení	označení	k1gNnSc4	označení
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
též	též	k9	též
udělován	udělován	k2eAgInSc1d1	udělován
doktorát	doktorát	k1gInSc1	doktorát
–	–	k?	–
titul	titul	k1gInSc1	titul
doktor	doktor	k1gMnSc1	doktor
věd	věda	k1gFnPc2	věda
obchodních	obchodní	k2eAgFnPc2d1	obchodní
(	(	kIx(	(
<g/>
RCDr	RCDra	k1gFnPc2	RCDra
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byl	být	k5eAaImAgInS	být
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
udělován	udělován	k2eAgInSc1d1	udělován
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rigoróza	rigorózum	k1gNnSc2	rigorózum
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
RTDr	RTDr	k1gInSc1	RTDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
též	též	k9	též
v	v	k7c6	v
uváděných	uváděný	k2eAgFnPc6d1	uváděná
oblastech	oblast	k1gFnPc6	oblast
uděloval	udělovat	k5eAaImAgMnS	udělovat
doktor	doktor	k1gMnSc1	doktor
montánních	montánní	k2eAgFnPc2d1	montánní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
mont	mont	k1gMnSc1	mont
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
agr	agr	k?	agr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přijat	přijat	k2eAgInSc1d1	přijat
Nejedlého	Nejedlého	k2eAgInSc1d1	Nejedlého
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
zrušil	zrušit	k5eAaPmAgInS	zrušit
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
stavovská	stavovský	k2eAgNnPc4d1	Stavovské
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
absolventy	absolvent	k1gMnPc4	absolvent
a	a	k8xC	a
udíleny	udílen	k2eAgFnPc4d1	udílena
tak	tak	k8xC	tak
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
promovaný	promovaný	k2eAgMnSc1d1	promovaný
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgMnSc1d1	dřevařský
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
inženýr-zemědělský	inženýremědělský	k2eAgMnSc1d1	inženýr-zemědělský
mechanisátor	mechanisátor	k1gMnSc1	mechanisátor
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
dalších	další	k2eAgNnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byly	být	k5eAaImAgInP	být
tituly	titul	k1gInPc1	titul
opět	opět	k6eAd1	opět
udělovány	udělován	k2eAgInPc1d1	udělován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
titulu	titul	k1gInSc2	titul
inženýr	inženýr	k1gMnSc1	inženýr
(	(	kIx(	(
<g/>
Ing.	ing.	kA	ing.
<g/>
)	)	kIx)	)
či	či	k8xC	či
inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
Ing.	ing.	kA	ing.
arch	arch	k1gInSc4	arch
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
zpětně	zpětně	k6eAd1	zpětně
přiznal	přiznat	k5eAaPmAgMnS	přiznat
dřívějším	dřívější	k2eAgMnSc7d1	dřívější
absolventům	absolvent	k1gMnPc3	absolvent
technických	technický	k2eAgFnPc2d1	technická
<g/>
,	,	kIx,	,
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
či	či	k8xC	či
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
příslušný	příslušný	k2eAgInSc4d1	příslušný
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
nový	nový	k2eAgInSc1d1	nový
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
udílen	udílen	k2eAgMnSc1d1	udílen
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
<g/>
"	"	kIx"	"
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
pak	pak	k6eAd1	pak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
oficiálně	oficiálně	k6eAd1	oficiálně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
obsahově	obsahově	k6eAd1	obsahově
<g/>
)	)	kIx)	)
ucelená	ucelený	k2eAgFnSc1d1	ucelená
část	část	k1gFnSc1	část
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
primárně	primárně	k6eAd1	primárně
určenou	určený	k2eAgFnSc7d1	určená
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
–	–	k?	–
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvoustupňový	dvoustupňový	k2eAgInSc1d1	dvoustupňový
systém	systém	k1gInSc1	systém
"	"	kIx"	"
<g/>
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
<g/>
"	"	kIx"	"
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
vědecké	vědecký	k2eAgFnSc2d1	vědecká
aspirantury	aspirantura	k1gFnSc2	aspirantura
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
transformoval	transformovat	k5eAaBmAgInS	transformovat
do	do	k7c2	do
třístupňového	třístupňový	k2eAgInSc2d1	třístupňový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
systému	systém	k1gInSc2	systém
standardního	standardní	k2eAgInSc2d1	standardní
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obdobné	obdobný	k2eAgInPc4d1	obdobný
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
zemích	zem	k1gFnPc6	zem
zejména	zejména	k9	zejména
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Západního	západní	k2eAgInSc2d1	západní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ČR	ČR	kA	ČR
či	či	k8xC	či
zemí	zem	k1gFnSc7	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
uváděných	uváděný	k2eAgFnPc6d1	uváděná
oblastech	oblast	k1gFnPc6	oblast
tohoto	tento	k3xDgNnSc2	tento
obvykle	obvykle	k6eAd1	obvykle
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
letého	letý	k2eAgNnSc2d1	leté
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
udílet	udílet	k5eAaImF	udílet
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
či	či	k8xC	či
ekvivalenty	ekvivalent	k1gInPc4	ekvivalent
engineer	enginera	k1gFnPc2	enginera
–	–	k?	–
master	master	k1gMnSc1	master
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ekonomie	ekonomie	k1gFnSc2	ekonomie
MA	MA	kA	MA
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
MSc	MSc	k1gMnSc1	MSc
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Různé	různý	k2eAgFnPc1d1	různá
akademické	akademický	k2eAgFnPc1d1	akademická
kvalifikace	kvalifikace	k1gFnPc1	kvalifikace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
inženýr	inženýr	k1gMnSc1	inženýr
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
středoškolského	středoškolský	k2eAgNnSc2d1	středoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
technického	technický	k2eAgInSc2d1	technický
směru	směr	k1gInSc2	směr
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
pro	pro	k7c4	pro
VŠ	vš	k0	vš
vzdělání	vzdělání	k1gNnSc4	vzdělání
příslušného	příslušný	k2eAgInSc2d1	příslušný
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
titul	titul	k1gInSc1	titul
Dipl	Dipla	k1gFnPc2	Dipla
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
Ing.	ing.	kA	ing.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
použití	použití	k1gNnSc1	použití
titulu	titul	k1gInSc2	titul
Ing.	ing.	kA	ing.
pro	pro	k7c4	pro
stavaře	stavař	k1gMnPc4	stavař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
dodělají	dodělat	k5eAaPmIp3nP	dodělat
po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
doplňující	doplňující	k2eAgFnSc2d1	doplňující
zkoušky	zkouška	k1gFnSc2	zkouška
po	po	k7c6	po
minimálně	minimálně	k6eAd1	minimálně
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
Dipl-	Dipl-	k1gFnSc2	Dipl-
<g/>
Ing.	ing.	kA	ing.
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
technických	technický	k2eAgFnPc6d1	technická
školách	škola	k1gFnPc6	škola
titulem	titul	k1gInSc7	titul
udělovaným	udělovaný	k2eAgInSc7d1	udělovaný
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
bakaláře	bakalář	k1gMnSc2	bakalář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
USA	USA	kA	USA
udělují	udělovat	k5eAaImIp3nP	udělovat
titul	titul	k1gInSc4	titul
Engineer	Enginera	k1gFnPc2	Enginera
v	v	k7c6	v
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
(	(	kIx(	(
<g/>
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
<g/>
)	)	kIx)	)
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
technických	technický	k2eAgInPc6d1	technický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc6d1	ekonomický
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
magisterského	magisterský	k2eAgInSc2d1	magisterský
titulu	titul	k1gInSc2	titul
M.	M.	kA	M.
<g/>
Sc	Sc	k1gMnSc1	Sc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
Eng	Eng	k1gFnSc1	Eng
<g/>
.	.	kIx.	.
či	či	k8xC	či
M.	M.	kA	M.
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
Náročnost	náročnost	k1gFnSc1	náročnost
takového	takový	k3xDgNnSc2	takový
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
jednoletým	jednoletý	k2eAgNnSc7d1	jednoleté
magisterským	magisterský	k2eAgNnSc7d1	magisterské
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studia	studio	k1gNnPc4	studio
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
připravit	připravit	k5eAaPmF	připravit
odborníka	odborník	k1gMnSc4	odborník
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
povolání	povolání	k1gNnSc4	povolání
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
lze	lze	k6eAd1	lze
po	po	k7c6	po
11	[number]	k4	11
<g/>
leté	letý	k2eAgFnSc6d1	letá
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
5-	[number]	k4	5-
až	až	k9	až
6	[number]	k4	6
<g/>
letý	letý	k2eAgInSc4d1	letý
obor	obor	k1gInSc4	obor
Specialista	specialista	k1gMnSc1	specialista
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
evropskému	evropský	k2eAgNnSc3d1	Evropské
magisterskému	magisterský	k2eAgNnSc3d1	magisterské
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
student	student	k1gMnSc1	student
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Inženýr	inženýr	k1gMnSc1	inženýr
specialista	specialista	k1gMnSc1	specialista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
není	být	k5eNaImIp3nS	být
zakotveno	zakotven	k2eAgNnSc1d1	zakotveno
používání	používání	k1gNnSc1	používání
titulů	titul	k1gInPc2	titul
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
vyšších	vysoký	k2eAgInPc2d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
používaný	používaný	k2eAgInSc1d1	používaný
titul	titul	k1gInSc1	titul
Ing.	ing.	kA	ing.
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
německému	německý	k2eAgInSc3d1	německý
Dipl	Dipl	k1gInSc4	Dipl
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
Ing.	ing.	kA	ing.
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Master	master	k1gMnSc1	master
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
Eng	Eng	k1gFnSc1	Eng
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInSc1d1	další
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
inženýra	inženýr	k1gMnSc4	inženýr
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
faktické	faktický	k2eAgNnSc1d1	faktické
vzdělání	vzdělání	k1gNnSc1	vzdělání
–	–	k?	–
např.	např.	kA	např.
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
technolog	technolog	k1gMnSc1	technolog
apod.	apod.	kA	apod.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
funkční	funkční	k2eAgNnSc4d1	funkční
zařazení	zařazení	k1gNnSc4	zařazení
respektive	respektive	k9	respektive
funkční	funkční	k2eAgFnSc4d1	funkční
systemizaci	systemizace	k1gFnSc4	systemizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
listopadem	listopad	k1gInSc7	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
používal	používat	k5eAaImAgInS	používat
ve	v	k7c6	v
státních	státní	k2eAgInPc6d1	státní
podnicích	podnik	k1gInPc6	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
v	v	k7c6	v
podnicích	podnik	k1gInPc6	podnik
se	s	k7c7	s
zahraničním	zahraniční	k2eAgInSc7d1	zahraniční
managementem	management	k1gInSc7	management
<g/>
,	,	kIx,	,
převzato	převzat	k2eAgNnSc1d1	převzato
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
engineer	engineer	k1gInSc1	engineer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Titul	titul	k1gInSc1	titul
inženýr	inženýr	k1gMnSc1	inženýr
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
<g/>
,	,	kIx,	,
písnička	písnička	k1gFnSc1	písnička
skupiny	skupina	k1gFnSc2	skupina
Plavci	plavec	k1gMnPc1	plavec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
==	==	k?	==
Titul	titul	k1gInSc1	titul
inženýr	inženýr	k1gMnSc1	inženýr
v	v	k7c6	v
místopisu	místopis	k1gInSc6	místopis
==	==	k?	==
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
Ing.	ing.	kA	ing.
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
názvu	název	k1gInSc6	název
několika	několik	k4yIc2	několik
současných	současný	k2eAgFnPc2d1	současná
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ing.	ing.	kA	ing.
Kašpara	Kašpar	k1gMnSc2	Kašpar
(	(	kIx(	(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Ing.	ing.	kA	ing.
Straku	straka	k1gFnSc4	straka
(	(	kIx(	(
<g/>
Spišská	spišský	k2eAgFnSc1d1	Spišská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Master	master	k1gMnSc1	master
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Engineer	Engineer	k1gInSc4	Engineer
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Inženýr	inženýr	k1gMnSc1	inženýr
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
kopec	kopec	k1gInSc1	kopec
</s>
</p>
<p>
<s>
Inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Promovaní	promovaný	k2eAgMnPc1d1	promovaný
inženýři	inženýr	k1gMnPc1	inženýr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
skupiny	skupina	k1gFnSc2	skupina
Plavci	plavec	k1gMnPc1	plavec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
inženýr	inženýr	k1gMnSc1	inženýr
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Ing.	ing.	kA	ing.
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
