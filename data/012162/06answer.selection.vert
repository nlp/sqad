<s>
Plastiku	plastika	k1gFnSc4	plastika
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
blok	blok	k1gInSc1	blok
černé	černý	k2eAgFnSc2d1	černá
žuly	žula	k1gFnSc2	žula
z	z	k7c2	z
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vytesána	vytesat	k5eAaPmNgFnS	vytesat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
projektilu	projektil	k1gInSc2	projektil
<g/>
.	.	kIx.	.
</s>
