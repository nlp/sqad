<p>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
kamenná	kamenný	k2eAgFnSc1d1	kamenná
plastika	plastika	k1gFnSc1	plastika
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
projektilu	projektil	k1gInSc2	projektil
s	s	k7c7	s
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojem	stroj	k1gInSc7	stroj
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
vypouštějící	vypouštějící	k2eAgFnPc1d1	vypouštějící
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c4	v
11.00	[number]	k4	11.00
hodin	hodina	k1gFnPc2	hodina
kuličku	kulička	k1gFnSc4	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
před	před	k7c7	před
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
Omega	omega	k1gNnSc2	omega
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ji	on	k3xPp3gFnSc4	on
sochaři	sochař	k1gMnPc1	sochař
Oldřich	Oldřich	k1gMnSc1	Oldřich
Rujbr	Rujbr	k1gMnSc1	Rujbr
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Kameník	Kameník	k1gMnSc1	Kameník
<g/>
,	,	kIx,	,
za	za	k7c4	za
iniciátora	iniciátor	k1gMnSc4	iniciátor
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
primátor	primátor	k1gMnSc1	primátor
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
365	[number]	k4	365
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
odolání	odolání	k1gNnSc2	odolání
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
při	při	k7c6	při
švédském	švédský	k2eAgNnSc6d1	švédské
obléhání	obléhání	k1gNnSc6	obléhání
v	v	k7c6	v
době	doba	k1gFnSc6	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
roční	roční	k2eAgInSc4d1	roční
provoz	provoz	k1gInSc4	provoz
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
stál	stát	k5eAaImAgInS	stát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
orloj	orloj	k1gInSc1	orloj
či	či	k8xC	či
brněnský	brněnský	k2eAgInSc1d1	brněnský
orloj	orloj	k1gInSc1	orloj
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
orlojem	orloj	k1gInSc7	orloj
(	(	kIx(	(
<g/>
horologiem	horologium	k1gNnSc7	horologium
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc4	dílo
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
či	či	k8xC	či
multifunkční	multifunkční	k2eAgInSc1d1	multifunkční
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
díla	dílo	k1gNnSc2	dílo
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
veřejnost	veřejnost	k1gFnSc1	veřejnost
k	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
dalším	další	k2eAgInPc3d1	další
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
podoby	podoba	k1gFnSc2	podoba
s	s	k7c7	s
falem	falos	k1gInSc7	falos
(	(	kIx(	(
<g/>
falické	falický	k2eAgFnPc4d1	falická
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
Brněnský	brněnský	k2eAgInSc1d1	brněnský
falus	falus	k1gInSc1	falus
<g/>
)	)	kIx)	)
či	či	k8xC	či
obřím	obří	k2eAgInSc7d1	obří
vibrátorem	vibrátor	k1gInSc7	vibrátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Plastiku	plastika	k1gFnSc4	plastika
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
blok	blok	k1gInSc1	blok
černé	černý	k2eAgFnSc2d1	černá
žuly	žula	k1gFnSc2	žula
z	z	k7c2	z
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vytesána	vytesat	k5eAaPmNgFnS	vytesat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
projektilu	projektil	k1gInSc2	projektil
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
materiálu	materiál	k1gInSc6	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
časostroj	časostroj	k1gInSc1	časostroj
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
byl	být	k5eAaImAgMnS	být
citován	citován	k2eAgMnSc1d1	citován
Lukáš	Lukáš	k1gMnSc1	Lukáš
Krmíček	Krmíček	k1gMnSc1	Krmíček
z	z	k7c2	z
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
mineralogicky	mineralogicky	k6eAd1	mineralogicky
materiál	materiál	k1gInSc1	materiál
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
žule	žula	k1gFnSc3	žula
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
gabru	gabro	k1gNnSc6	gabro
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
díla	dílo	k1gNnSc2	dílo
Petr	Petr	k1gMnSc1	Petr
Kameník	Kameník	k1gMnSc1	Kameník
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
materiál	materiál	k1gInSc4	materiál
s	s	k7c7	s
certifikátem	certifikát	k1gInSc7	certifikát
jihoafrického	jihoafrický	k2eAgInSc2d1	jihoafrický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
Hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
koordinovány	koordinovat	k5eAaBmNgFnP	koordinovat
podle	podle	k7c2	podle
signálu	signál	k1gInSc2	signál
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
otáčením	otáčení	k1gNnSc7	otáčení
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
kamenných	kamenný	k2eAgInPc2d1	kamenný
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
špice	špice	k1gFnSc1	špice
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ní	on	k3xPp3gFnSc7	on
ostrý	ostrý	k2eAgInSc1d1	ostrý
hranol	hranol	k1gInSc1	hranol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ukazatel	ukazatel	k1gInSc1	ukazatel
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
skleněný	skleněný	k2eAgInSc1d1	skleněný
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ukazatel	ukazatel	k1gInSc1	ukazatel
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Číslice	číslice	k1gFnSc1	číslice
ciferníku	ciferník	k1gInSc2	ciferník
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžného	běžný	k2eAgMnSc4d1	běžný
návštěvníka	návštěvník	k1gMnSc4	návštěvník
a	a	k8xC	a
bez	bez	k7c2	bez
návodu	návod	k1gInSc2	návod
je	být	k5eAaImIp3nS	být
však	však	k9	však
čas	čas	k1gInSc1	čas
nezjistitelný	zjistitelný	k2eNgInSc1d1	nezjistitelný
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
údajně	údajně	k6eAd1	údajně
rozmohlo	rozmoct	k5eAaPmAgNnS	rozmoct
výsměšné	výsměšný	k2eAgNnSc1d1	výsměšné
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
se	se	k3xPyFc4	se
otočit	otočit	k5eAaPmF	otočit
k	k	k7c3	k
orloji	orloj	k1gInSc3	orloj
čelem	čelo	k1gNnSc7	čelo
<g/>
,	,	kIx,	,
udělat	udělat	k5eAaPmF	udělat
pět	pět	k4xCc4	pět
kroků	krok	k1gInPc2	krok
od	od	k7c2	od
orloje	orloj	k1gInSc2	orloj
vzad	vzad	k6eAd1	vzad
a	a	k8xC	a
podívat	podívat	k5eAaPmF	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
dáli	dál	k1gFnSc6	dál
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
celou	celý	k2eAgFnSc4d1	celá
hodinu	hodina	k1gFnSc4	hodina
stroj	stroj	k1gInSc1	stroj
krátce	krátce	k6eAd1	krátce
zazvoní	zazvonit	k5eAaPmIp3nS	zazvonit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
složitých	složitý	k2eAgInPc2d1	složitý
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
kuličku	kulička	k1gFnSc4	kulička
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Brna	Brno	k1gNnSc2	Brno
i	i	k8xC	i
jiných	jiný	k2eAgFnPc6d1	jiná
různých	různý	k2eAgFnPc6d1	různá
grafických	grafický	k2eAgFnPc6d1	grafická
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
10.15	[number]	k4	10.15
<g/>
,	,	kIx,	,
10.30	[number]	k4	10.30
<g/>
,	,	kIx,	,
10.45	[number]	k4	10.45
a	a	k8xC	a
v	v	k7c6	v
11.00	[number]	k4	11.00
h	h	k?	h
zahraje	zahrát	k5eAaPmIp3nS	zahrát
zvonkohra	zvonkohra	k1gFnSc1	zvonkohra
melodii	melodie	k1gFnSc3	melodie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
projíždějí	projíždět	k5eAaImIp3nP	projíždět
škvírami	škvíra	k1gFnPc7	škvíra
a	a	k8xC	a
narážejí	narážet	k5eAaImIp3nP	narážet
do	do	k7c2	do
jmen	jméno	k1gNnPc2	jméno
účastníků	účastník	k1gMnPc2	účastník
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Nahoru	nahoru	k6eAd1	nahoru
koule	koule	k1gFnPc1	koule
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
dopravníkem	dopravník	k1gInSc7	dopravník
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
11.00	[number]	k4	11.00
po	po	k7c6	po
zahrání	zahrání	k1gNnSc6	zahrání
zvonkohry	zvonkohra	k1gFnSc2	zvonkohra
jedna	jeden	k4xCgFnSc1	jeden
kulička	kulička	k1gFnSc1	kulička
sjede	sjet	k5eAaPmIp3nS	sjet
po	po	k7c6	po
drahách	draha	k1gFnPc6	draha
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
odnést	odnést	k5eAaPmF	odnést
jako	jako	k8xC	jako
suvenýr	suvenýr	k1gInSc4	suvenýr
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Kuličky	kulička	k1gFnPc1	kulička
jsou	být	k5eAaImIp3nP	být
ruční	ruční	k2eAgFnPc1d1	ruční
práce	práce	k1gFnPc1	práce
ze	z	k7c2	z
sklárny	sklárna	k1gFnSc2	sklárna
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objekt	objekt	k1gInSc1	objekt
má	mít	k5eAaImIp3nS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
vítězství	vítězství	k1gNnSc4	vítězství
i	i	k8xC	i
odvahu	odvaha	k1gFnSc4	odvaha
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
obránců	obránce	k1gMnPc2	obránce
při	při	k7c6	při
švédském	švédský	k2eAgNnSc6d1	švédské
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
symbolem	symbol	k1gInSc7	symbol
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Časostroj	Časostroj	k1gInSc1	Časostroj
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
plynutí	plynutí	k1gNnSc4	plynutí
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odhalení	odhalení	k1gNnPc2	odhalení
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
byly	být	k5eAaImAgFnP	být
oficiálně	oficiálně	k6eAd1	oficiálně
představeny	představit	k5eAaPmNgFnP	představit
veřejnosti	veřejnost	k1gFnPc1	veřejnost
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2010	[number]	k4	2010
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
10.30	[number]	k4	10.30
do	do	k7c2	do
11.00	[number]	k4	11.00
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
slavnost	slavnost	k1gFnSc1	slavnost
začala	začít	k5eAaPmAgFnS	začít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
útvar	útvar	k1gInSc1	útvar
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
uniformách	uniforma	k1gFnPc6	uniforma
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
představujících	představující	k2eAgNnPc2d1	představující
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vedl	vést	k5eAaImAgMnS	vést
Louis	Louis	k1gMnSc1	Louis
Raduit	Raduit	k1gMnSc1	Raduit
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
<g/>
,	,	kIx,	,
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
velitel	velitel	k1gMnSc1	velitel
obrany	obrana	k1gFnSc2	obrana
Brna	Brno	k1gNnSc2	Brno
za	za	k7c2	za
časů	čas	k1gInPc2	čas
švédského	švédský	k2eAgNnSc2d1	švédské
obležení	obležení	k1gNnSc2	obležení
<g/>
,	,	kIx,	,
vpochodoval	vpochodovat	k5eAaPmAgInS	vpochodovat
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proslovu	proslov	k1gInSc6	proslov
velitele	velitel	k1gMnSc4	velitel
přijela	přijet	k5eAaPmAgFnS	přijet
speciální	speciální	k2eAgFnSc1d1	speciální
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
tramvaj	tramvaj	k1gFnSc1	tramvaj
Škoda	škoda	k1gFnSc1	škoda
13	[number]	k4	13
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
přivezla	přivézt	k5eAaPmAgFnS	přivézt
primátora	primátor	k1gMnSc4	primátor
města	město	k1gNnSc2	město
Romana	Roman	k1gMnSc4	Roman
Onderku	Onderka	k1gMnSc4	Onderka
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
čelné	čelný	k2eAgMnPc4d1	čelný
představitele	představitel	k1gMnPc4	představitel
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
projevu	projev	k1gInSc6	projev
byl	být	k5eAaImAgInS	být
stroj	stroj	k1gInSc1	stroj
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgFnSc4	první
kuličku	kulička	k1gFnSc4	kulička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikomu	nikdo	k3yNnSc3	nikdo
nepodařilo	podařit	k5eNaPmAgNnS	podařit
chytit	chytit	k5eAaPmF	chytit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
rezervní	rezervní	k2eAgFnSc7d1	rezervní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
odhalením	odhalení	k1gNnSc7	odhalení
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
hojně	hojně	k6eAd1	hojně
diskutováno	diskutovat	k5eAaImNgNnS	diskutovat
na	na	k7c6	na
mnohých	mnohý	k2eAgFnPc6d1	mnohá
internetových	internetový	k2eAgFnPc6d1	internetová
diskuzích	diskuze	k1gFnPc6	diskuze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgInPc1d3	veliký
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
byla	být	k5eAaImAgFnS	být
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
patřičnost	patřičnost	k1gFnSc4	patřičnost
takto	takto	k6eAd1	takto
moderního	moderní	k2eAgNnSc2d1	moderní
díla	dílo	k1gNnSc2	dílo
do	do	k7c2	do
historické	historický	k2eAgFnSc2d1	historická
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
interpretace	interpretace	k1gFnPc4	interpretace
tvaru	tvar	k1gInSc2	tvar
plastiky	plastika	k1gFnSc2	plastika
i	i	k8xC	i
obtížnost	obtížnost	k1gFnSc1	obtížnost
zjištění	zjištění	k1gNnSc2	zjištění
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
brněnský	brněnský	k2eAgInSc1d1	brněnský
magistrát	magistrát	k1gInSc1	magistrát
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
turistické	turistický	k2eAgInPc4d1	turistický
suvenýry	suvenýr	k1gInPc4	suvenýr
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
této	tento	k3xDgFnSc2	tento
plastiky	plastika	k1gFnSc2	plastika
<g/>
,	,	kIx,	,
18	[number]	k4	18
cm	cm	kA	cm
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Torre	torr	k1gInSc5	torr
Glò	Glò	k1gMnSc3	Glò
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brněnský	brněnský	k2eAgInSc4d1	brněnský
orloj	orloj	k1gInSc4	orloj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sejdeme	sejít	k5eAaPmIp1nP	sejít
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hodinami	hodina	k1gFnPc7	hodina
+	+	kIx~	+
návod	návod	k1gInSc1	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
zjistit	zjistit	k5eAaPmF	zjistit
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
a	a	k8xC	a
PDF	PDF	kA	PDF
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgInSc1d1	alternativní
odkaz	odkaz	k1gInSc1	odkaz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
Urban	Urban	k1gMnSc1	Urban
centrum	centrum	k1gNnSc1	centrum
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
ve	v	k7c6	v
webovém	webový	k2eAgInSc6d1	webový
archivu	archiv	k1gInSc6	archiv
<g/>
)	)	kIx)	)
</s>
</p>
