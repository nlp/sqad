<s>
Tradiční	tradiční	k2eAgFnPc1d1
klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
Liverpoolu	Liverpool	k1gInSc2
FC	FC	kA
jsou	být	k5eAaImIp3nP
červená	červený	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
klubové	klubový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
hrál	hrát	k5eAaImAgInS
klub	klub	k1gInSc1
v	v	k7c6
modrých	modrý	k2eAgInPc6d1
a	a	k8xC
bílých	bílý	k2eAgInPc6d1
dresech	dres	k1gInPc6
<g/>
.	.	kIx.
</s>