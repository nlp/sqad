<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
stejnojmenný	stejnojmenný	k2eAgInSc4d1
uruguayský	uruguayský	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Montevideo	Montevideo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
Název	název	k1gInSc1
</s>
<s>
Liverpool	Liverpool	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc4
Motto	motto	k1gNnSc4
</s>
<s>
"	"	kIx"
<g/>
You	You	k1gFnPc3
<g/>
‘	‘	k?
<g/>
ll	ll	k?
Never	Never	k1gMnSc1
Walk	Walk	k1gMnSc1
Alone	Alon	k1gInSc5
<g/>
"	"	kIx"
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
The	The	k?
Reds	Reds	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
Město	město	k1gNnSc1
</s>
<s>
Liverpool	Liverpool	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1892	#num#	k4
Asociace	asociace	k1gFnSc1
</s>
<s>
Football	Football	k1gInSc1
Association	Association	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_liverpool	_liverpool	k1gInSc1
<g/>
2021	#num#	k4
<g/>
H	H	kA
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_liverpool	_liverpool	k1gInSc1
<g/>
2021	#num#	k4
<g/>
A	A	kA
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_liverpool	_liverpool	k1gInSc1
<g/>
2021	#num#	k4
<g/>
T	T	kA
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Anfield	Anfield	k1gInSc1
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
54	#num#	k4
167	#num#	k4
Vedení	vedení	k1gNnPc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Fenway	Fenwaa	k1gFnSc2
Sports	Sportsa	k1gFnPc2
Group	Group	k1gMnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Tom	Tom	k1gMnSc1
Werner	Werner	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
19	#num#	k4
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
FA	fa	kA
Cup	cup	k1gInSc4
8	#num#	k4
<g/>
×	×	k?
EFL	EFL	kA
Cup	cup	k1gInSc4
15	#num#	k4
<g/>
×	×	k?
Community	Communita	k1gFnSc2
Shield	Shield	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
PMEZ	PMEZ	kA
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
3	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
4	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
PMEZ	PMEZ	kA
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Pohár	pohár	k1gInSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1977	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1978	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1984	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2001	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2005	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2019	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
1981	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1984	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
2005	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2019	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
/	/	kIx~
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
1899	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
1901	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
1906	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
1910	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
1922	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
1923	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1980	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Liverpool	Liverpool	k1gInSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Liverpool	Liverpool	k1gInSc1
v	v	k7c6
metropolitním	metropolitní	k2eAgNnSc6d1
hrabství	hrabství	k1gNnSc6
Merseyside	Merseysid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrál	vyhrát	k5eAaPmAgMnS
devatenáctkrát	devatenáctkrát	k6eAd1
anglickou	anglický	k2eAgFnSc4d1
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Liverpool	Liverpool	k1gInSc1
prožil	prožít	k5eAaPmAgInS
úspěšné	úspěšný	k2eAgNnSc4d1
období	období	k1gNnSc4
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
však	však	k9
vzedmul	vzedmout	k5eAaPmAgMnS
rival	rival	k1gMnSc1
Manchester	Manchester	k1gInSc4
United	United	k1gInSc1
úspěšný	úspěšný	k2eAgInSc1d1
následující	následující	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
anglickým	anglický	k2eAgMnSc7d1
(	(	kIx(
<g/>
potažmo	potažmo	k6eAd1
britským	britský	k2eAgInSc7d1
<g/>
)	)	kIx)
klubem	klub	k1gInSc7
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
–	–	k?
6	#num#	k4
<g/>
×	×	k?
vyhrál	vyhrát	k5eAaPmAgInS
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
×	×	k?
vyhrál	vyhrát	k5eAaPmAgInS
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
1	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
francouzského	francouzský	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
týdeníku	týdeník	k1gInSc2
France	Franc	k1gMnSc4
Football	Footballa	k1gFnPc2
je	být	k5eAaImIp3nS
Liverpool	Liverpool	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
pátým	pátý	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
fotbalovým	fotbalový	k2eAgInSc7d1
klubem	klub	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
stadionu	stadion	k1gInSc6
Anfield	Anfielda	k1gFnPc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
54	#num#	k4
167	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Založení-	Založení-	k?
<g/>
1923	#num#	k4
</s>
<s>
Počátky	počátek	k1gInPc4
fotbalu	fotbal	k1gInSc2
v	v	k7c6
Liverpoolu	Liverpool	k1gInSc6
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
ve	v	k7c6
městě	město	k1gNnSc6
založen	založit	k5eAaPmNgInS
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
St.	st.	kA
Domingo	Domingo	k1gMnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c4
Everton	Everton	k1gInSc4
FC	FC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Football	Footballa	k1gFnPc2
League	League	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Everton	Everton	k1gInSc1
svoje	svůj	k3xOyFgInPc4
zápasy	zápas	k1gInPc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
Anfield	Anfield	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
se	se	k3xPyFc4
John	John	k1gMnSc1
Houlding	Houlding	k1gInSc1
<g/>
,	,	kIx,
místní	místní	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
starosta	starosta	k1gMnSc1
Liverpoolu	Liverpool	k1gInSc2
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
klubu	klub	k1gInSc3
navýšit	navýšit	k5eAaPmF
nájem	nájem	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
čímž	což	k3yRnSc7,k3yQnSc7
akcionáři	akcionář	k1gMnPc1
Evertonu	Everton	k1gInSc2
nesouhlasili	souhlasit	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
řadě	řada	k1gFnSc6
jednání	jednání	k1gNnSc2
se	se	k3xPyFc4
Everton	Everton	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
Anfield	Anfield	k1gInSc4
opustit	opustit	k5eAaPmF
a	a	k8xC
přestěhoval	přestěhovat	k5eAaPmAgMnS
se	se	k3xPyFc4
po	po	k7c6
7	#num#	k4
letech	léto	k1gNnPc6
působení	působení	k1gNnSc2
na	na	k7c6
Anfieldu	Anfield	k1gInSc6
do	do	k7c2
Goodison	Goodisona	k1gFnPc2
Parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Houlding	Houlding	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
reagoval	reagovat	k5eAaBmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1892	#num#	k4
založil	založit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
klub	klub	k1gInSc1
jménem	jméno	k1gNnSc7
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
svoje	svůj	k3xOyFgInPc4
zápasy	zápas	k1gInPc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
Anfield	Anfield	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7
klubu	klub	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
John	John	k1gMnSc1
McKenna	McKenn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
do	do	k7c2
klubu	klub	k1gInSc2
přivedl	přivést	k5eAaPmAgMnS
13	#num#	k4
skotských	skotský	k2eAgMnPc2d1
profesionálních	profesionální	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
Lancashire	Lancashir	k1gMnSc5
League	Leaguus	k1gMnSc5
<g/>
,	,	kIx,
následující	následující	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Football	Footballa	k1gFnPc2
League	Leagu	k1gFnSc2
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
sezónu	sezóna	k1gFnSc4
Liverpool	Liverpool	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
jako	jako	k8xC,k8xS
neporažený	poražený	k2eNgInSc4d1
celek	celek	k1gInSc4
a	a	k8xC
přešel	přejít	k5eAaPmAgMnS
do	do	k7c2
First	First	k1gFnSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
znovu	znovu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
hrál	hrát	k5eAaImAgInS
Liverpool	Liverpool	k1gInSc1
svoje	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
finále	finále	k1gNnSc4
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
ale	ale	k8xC
prohrál	prohrát	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
s	s	k7c7
Burnley	Burnley	k1gInPc7
FC	FC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1923-1983	1923-1983	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
a	a	k8xC
1924	#num#	k4
klub	klub	k1gInSc1
poprvé	poprvé	k6eAd1
získal	získat	k5eAaPmAgInS
dva	dva	k4xCgInPc4
ligové	ligový	k2eAgInPc4d1
tituly	titul	k1gInPc4
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
vedeni	vést	k5eAaImNgMnP
anglickým	anglický	k2eAgMnSc7d1
obráncem	obránce	k1gMnSc7
Ephraimem	Ephraim	k1gMnSc7
Longworthem	Longworth	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
ale	ale	k9
přišel	přijít	k5eAaPmAgInS
ústup	ústup	k1gInSc1
a	a	k8xC
klub	klub	k1gInSc1
musel	muset	k5eAaImAgInS
protrpět	protrpět	k5eAaPmF
nejdelší	dlouhý	k2eAgFnSc4d3
sérii	série	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
bez	bez	k7c2
získání	získání	k1gNnSc2
ligového	ligový	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čekání	čekání	k1gNnSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
ukončeno	ukončen	k2eAgNnSc1d1
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
Liverpool	Liverpool	k1gInSc1
sestupuje	sestupovat	k5eAaImIp3nS
do	do	k7c2
Second	Seconda	k1gFnPc2
Division	Division	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1959	#num#	k4
se	s	k7c7
klubovým	klubový	k2eAgMnSc7d1
manažerem	manažer	k1gMnSc7
stává	stávat	k5eAaImIp3nS
legendární	legendární	k2eAgMnSc1d1
Bill	Bill	k1gMnSc1
Shankly	Shankly	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
během	během	k7c2
následujících	následující	k2eAgNnPc2d1
15	#num#	k4
let	léto	k1gNnPc2
klub	klub	k1gInSc1
přetransformoval	přetransformovat	k5eAaPmAgInS
v	v	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
klubů	klub	k1gInPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
třech	tři	k4xCgInPc6
letech	let	k1gInPc6
Shanklyho	Shankly	k1gMnSc2
působení	působení	k1gNnSc2
<g/>
,	,	kIx,
klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
Second	Second	k1gInSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
postupuje	postupovat	k5eAaImIp3nS
zpátky	zpátky	k6eAd1
do	do	k7c2
první	první	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
již	již	k9
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nikdy	nikdy	k6eAd1
nesestoupil	sestoupit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
již	již	k9
Liverpool	Liverpool	k1gInSc1
získává	získávat	k5eAaImIp3nS
další	další	k2eAgInSc4d1
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
po	po	k7c6
dlouhých	dlouhý	k2eAgNnPc6d1
17	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
poráží	porážet	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Leeds	Leeds	k1gInSc1
United	United	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
znamená	znamenat	k5eAaImIp3nS
začátek	začátek	k1gInSc4
zlaté	zlatý	k2eAgFnSc2d1
éry	éra	k1gFnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
osmého	osmý	k4xOgInSc2
ligového	ligový	k2eAgInSc2d1
titulu	titul	k1gInSc2
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
poprvé	poprvé	k6eAd1
prosazuje	prosazovat	k5eAaImIp3nS
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
poráží	porážet	k5eAaImIp3nS
Borussii	Borussie	k1gFnSc4
Mönchengladbach	Mönchengladbacha	k1gFnPc2
a	a	k8xC
získává	získávat	k5eAaImIp3nS
první	první	k4xOgFnSc4
evropskou	evropský	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
další	další	k2eAgInSc1d1
FA	fa	kA
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
něm	on	k3xPp3gMnSc6
ale	ale	k8xC
z	z	k7c2
pozice	pozice	k1gFnSc2
manažera	manažer	k1gMnSc4
Bill	Bill	k1gMnSc1
Shankly	Shankly	k1gMnSc1
odchází	odcházet	k5eAaImIp3nS
do	do	k7c2
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídku	nabídka	k1gFnSc4
na	na	k7c4
to	ten	k3xDgNnSc4
ho	on	k3xPp3gNnSc4
nahradit	nahradit	k5eAaPmF
dostává	dostávat	k5eAaImIp3nS
Shanklyho	Shanklyha	k1gFnSc5
asistent	asistent	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Paisley	Paislea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
druhém	druhý	k4xOgInSc6
roce	rok	k1gInSc6
Paisleyho	Paisley	k1gMnSc2
působení	působení	k1gNnSc2
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
a	a	k8xC
znovu	znovu	k6eAd1
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
následuje	následovat	k5eAaImIp3nS
další	další	k2eAgInSc4d1
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
prohra	prohra	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
první	první	k4xOgFnSc1
výhra	výhra	k1gFnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnPc7
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
na	na	k7c4
Stadio	Stadio	k1gNnSc4
Olimpico	Olimpico	k6eAd1
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
poráží	porážet	k5eAaImIp3nS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Borussii	Borussie	k1gFnSc4
Mönchengladbach	Mönchengladbacha	k1gFnPc2
góly	gól	k1gInPc4
McDermotta	McDermott	k1gMnSc4
<g/>
,	,	kIx,
Smithe	Smith	k1gMnSc4
a	a	k8xC
Neala	Neal	k1gMnSc4
z	z	k7c2
penalty	penalta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
poté	poté	k6eAd1
Liverpool	Liverpool	k1gInSc1
trofej	trofej	k1gFnSc4
obhajuje	obhajovat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c4
Wembley	Wemble	k1gMnPc4
poráží	porážet	k5eAaImIp3nS
Club	club	k1gInSc1
Brugge	Brugge	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
gólem	gól	k1gInSc7
legendárního	legendární	k2eAgMnSc4d1
Kennyho	Kenny	k1gMnSc4
Dalglishe	Dalglish	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
Liverpool	Liverpool	k1gInSc1
získává	získávat	k5eAaImIp3nS
11	#num#	k4
ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
ustanovil	ustanovit	k5eAaPmAgInS
ligový	ligový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
získal	získat	k5eAaPmAgMnS
celkem	celek	k1gInSc7
64	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
42	#num#	k4
zápasech	zápas	k1gInPc6
inkasoval	inkasovat	k5eAaBmAgInS
pouze	pouze	k6eAd1
16	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
Liverpool	Liverpool	k1gInSc1
získává	získávat	k5eAaImIp3nS
celkem	celkem	k6eAd1
12	#num#	k4
ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
pořadí	pořadí	k1gNnSc6
čtvrtý	čtvrtý	k4xOgMnSc1
během	běh	k1gInSc7
posledních	poslední	k2eAgFnPc2d1
pěti	pět	k4xCc2
sezón	sezóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
v	v	k7c6
Poháru	pohár	k1gInSc6
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
hraném	hraný	k2eAgNnSc6d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
Liverpool	Liverpool	k1gInSc1
poráží	porážet	k5eAaImIp3nS
gólem	gól	k1gInSc7
Alana	Alan	k1gMnSc2
Kennedyho	Kennedy	k1gMnSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
double	double	k2eAgInSc7d1
ziskem	zisk	k1gInSc7
ligového	ligový	k2eAgInSc2d1
titulu	titul	k1gInSc2
a	a	k8xC
výhrou	výhra	k1gFnSc7
v	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devíti	devět	k4xCc2
sezónách	sezóna	k1gFnPc6
Bob	Bob	k1gMnSc1
Paisley	Paisley	k1gInPc4
celkem	celkem	k6eAd1
získal	získat	k5eAaPmAgMnS
neuvěřitelných	uvěřitelný	k2eNgFnPc2d1
23	#num#	k4
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
UEFA	UEFA	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
×	×	k?
ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
×	×	k?
Ligový	ligový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
další	další	k2eAgInSc4d1
díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
v	v	k7c4
Charity	charita	k1gFnPc4
Shield	Shielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Paisley	Paisle	k1gMnPc4
nezískal	získat	k5eNaPmAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
FA	fa	k1gNnSc4
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	bob	k1gInSc1
Paisley	Paislea	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
předává	předávat	k5eAaImIp3nS
trenérské	trenérský	k2eAgNnSc4d1
žezlo	žezlo	k1gNnSc4
Joe	Joe	k1gFnSc1
Faganovi	Faganovi	k?
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Úspěšně	úspěšně	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
Paisleyho	Paisley	k1gMnSc4
práci	práce	k1gFnSc4
a	a	k8xC
Liverpool	Liverpool	k1gInSc1
se	se	k3xPyFc4
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
stává	stávat	k5eAaImIp3nS
prvním	první	k4xOgInSc7
anglickým	anglický	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
získává	získávat	k5eAaImIp3nS
3	#num#	k4
hlavní	hlavní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
a	a	k8xC
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1985-1991	1985-1991	k4
(	(	kIx(
<g/>
Katastrofy	katastrofa	k1gFnSc2
v	v	k7c6
Hillsborough	Hillsborough	k1gMnSc1
a	a	k8xC
Heysel	Heysel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
dalšího	další	k2eAgNnSc2d1
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
jeho	jeho	k3xOp3gFnSc1
účast	účast	k1gFnSc1
se	se	k3xPyFc4
proměnila	proměnit	k5eAaPmAgFnS
ve	v	k7c4
velkou	velký	k2eAgFnSc4d1
tragédii	tragédie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
obrovskou	obrovský	k2eAgFnSc7d1
černou	černý	k2eAgFnSc7d1
kaňkou	kaňka	k1gFnSc7
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
proti	proti	k7c3
Juventusu	Juventus	k1gInSc3
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
Heysel	Heysel	k1gInSc4
Stadium	stadium	k1gNnSc1
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ve	v	k7c6
velmi	velmi	k6eAd1
nevyhovujícím	vyhovující	k2eNgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
podpořeno	podpořen	k2eAgNnSc1d1
chybou	chyba	k1gFnSc7
organizátorů	organizátor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
umístili	umístit	k5eAaPmAgMnP
tábory	tábor	k1gInPc4
fanoušků	fanoušek	k1gMnPc2
vedle	vedle	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpoolští	liverpoolský	k2eAgMnPc1d1
fanoušci	fanoušek	k1gMnPc1
před	před	k7c7
zápasem	zápas	k1gInSc7
prolomili	prolomit	k5eAaPmAgMnP
plot	plot	k1gInSc4
a	a	k8xC
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
fanoušky	fanoušek	k1gMnPc4
Juventusu	Juventus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
bránili	bránit	k5eAaImAgMnP
únikem	únik	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
cestě	cesta	k1gFnSc6
jim	on	k3xPp3gMnPc3
ale	ale	k9
stála	stát	k5eAaImAgFnS
zeď	zeď	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
prolomila	prolomit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
všeho	všecek	k3xTgNnSc2
bylo	být	k5eAaImAgNnS
39	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
Italů	Ital	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přes	přes	k7c4
tuto	tento	k3xDgFnSc4
tragédii	tragédie	k1gFnSc4
se	se	k3xPyFc4
utkání	utkání	k1gNnSc1
nakonec	nakonec	k6eAd1
odehrálo	odehrát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
Platiniho	Platini	k1gMnSc4
gólem	gól	k1gInSc7
z	z	k7c2
penalty	penalta	k1gFnSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
i	i	k9
s	s	k7c7
odstupem	odstup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zápas	zápas	k1gInSc1
vůbec	vůbec	k9
neměl	mít	k5eNaImAgInS
odehrát	odehrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglické	anglický	k2eAgInPc1d1
kluby	klub	k1gInPc1
dostaly	dostat	k5eAaPmAgInP
zákaz	zákaz	k1gInSc4
působení	působení	k1gNnSc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
po	po	k7c4
dobu	doba	k1gFnSc4
5	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
po	po	k7c4
dobu	doba	k1gFnSc4
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trest	trest	k1gInSc1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
ale	ale	k9
nakonec	nakonec	k6eAd1
snížen	snížit	k5eAaPmNgInS
na	na	k7c4
6	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
silně	silně	k6eAd1
zasáhlo	zasáhnout	k5eAaPmAgNnS
i	i	k9
druhý	druhý	k4xOgInSc1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Beatles	Beatles	k1gFnPc2
<g/>
,	,	kIx,
Everton	Everton	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
prožíval	prožívat	k5eAaImAgMnS
zlatou	zlatý	k2eAgFnSc4d1
éru	éra	k1gFnSc4
své	svůj	k3xOyFgFnSc2
historie	historie	k1gFnSc2
a	a	k8xC
po	po	k7c6
uplynutí	uplynutí	k1gNnSc6
trestu	trest	k1gInSc2
na	na	k7c4
tuto	tento	k3xDgFnSc4
éru	éra	k1gFnSc4
již	již	k6eAd1
nedokázal	dokázat	k5eNaPmAgMnS
navázat	navázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
přebírá	přebírat	k5eAaImIp3nS
žezlo	žezlo	k1gNnSc4
Kenny	Kenna	k1gMnSc2
Dalglish	Dalglish	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
prvním	první	k4xOgInSc7
hrajícím	hrající	k2eAgInSc7d1
manažerem	manažer	k1gInSc7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
ligové	ligový	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
FA	fa	k1gNnPc7
Cupy	cup	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1985-86	1985-86	k4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
double	double	k1gInSc1
za	za	k7c4
ligu	liga	k1gFnSc4
a	a	k8xC
FA	fa	k1gNnSc4
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
bohužel	bohužel	k6eAd1
přichází	přicházet	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
tragická	tragický	k2eAgFnSc1d1
událost	událost	k1gFnSc1
v	v	k7c6
klubových	klubový	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1989	#num#	k4
Liverpool	Liverpool	k1gInSc1
hraje	hrát	k5eAaImIp3nS
semifinále	semifinále	k1gNnSc1
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
s	s	k7c7
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
na	na	k7c4
Hillsborough	Hillsborough	k1gInSc4
Stadium	stadium	k1gNnSc1
v	v	k7c6
Sheffieldu	Sheffield	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policii	policie	k1gFnSc6
se	se	k3xPyFc4
situace	situace	k1gFnSc1
u	u	k7c2
stadionu	stadion	k1gInSc2
vymkla	vymknout	k5eAaPmAgFnS
kontrole	kontrola	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c4
tribunu	tribuna	k1gFnSc4
za	za	k7c7
bránou	brána	k1gFnSc7
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
více	hodně	k6eAd2
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
maximální	maximální	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stovky	stovka	k1gFnPc1
fanoušků	fanoušek	k1gMnPc2
bylo	být	k5eAaImAgNnS
natlačeno	natlačit	k5eAaBmNgNnS
na	na	k7c4
plot	plot	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odděloval	oddělovat	k5eAaImAgInS
tribunu	tribuna	k1gFnSc4
od	od	k7c2
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
94	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
další	další	k2eAgMnSc1d1
fanoušek	fanoušek	k1gMnSc1
4	#num#	k4
roky	rok	k1gInPc4
po	po	k7c6
tragédii	tragédie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
tedy	tedy	k9
zahynulo	zahynout	k5eAaPmAgNnS
96	#num#	k4
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
tragédii	tragédie	k1gFnSc6
vláda	vláda	k1gFnSc1
nařídila	nařídit	k5eAaPmAgFnS
přehodnocení	přehodnocení	k1gNnSc3
veškerých	veškerý	k3xTgNnPc2
opatření	opatření	k1gNnPc2
na	na	k7c6
stadionech	stadion	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
toho	ten	k3xDgNnSc2
byla	být	k5eAaImAgNnP
tzv.	tzv.	kA
Taylorova	Taylorův	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ní	on	k3xPp3gFnSc6
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
stadionech	stadion	k1gInPc6
k	k	k7c3
opatřením	opatření	k1gNnPc3
jako	jako	k8xC,k8xS
např.	např.	kA
odstranění	odstranění	k1gNnSc1
plotů	plot	k1gInPc2
mezi	mezi	k7c7
tribunami	tribuna	k1gFnPc7
a	a	k8xC
hřištěm	hřiště	k1gNnSc7
<g/>
,	,	kIx,
stadiony	stadion	k1gInPc1
byly	být	k5eAaImAgInP
povinně	povinně	k6eAd1
osazeny	osazen	k2eAgFnPc1d1
sedačkami	sedačka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
se	se	k3xPyFc4
také	také	k9
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
možnost	možnost	k1gFnSc1
odložení	odložení	k1gNnSc4
začátku	začátek	k1gInSc2
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c4
stadion	stadion	k1gInSc4
nedostanou	dostat	k5eNaPmIp3nP
včas	včas	k6eAd1
všichni	všechen	k3xTgMnPc1
fanoušci	fanoušek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
k	k	k7c3
uctění	uctění	k1gNnSc3
památky	památka	k1gFnSc2
převážně	převážně	k6eAd1
mladých	mladý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
vystavěl	vystavět	k5eAaPmAgMnS
u	u	k7c2
stadionu	stadion	k1gInSc2
Anfield	Anfield	k1gInSc1
vedle	vedle	k7c2
Shankleyho	Shankley	k1gMnSc2
brány	brána	k1gFnSc2
památník	památník	k1gInSc1
těmto	tento	k3xDgFnPc3
obětem	oběť	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
znaku	znak	k1gInSc6
klubu	klub	k1gInSc2
jsou	být	k5eAaImIp3nP
také	také	k9
umístěny	umístit	k5eAaPmNgInP
dva	dva	k4xCgInPc1
plamínky	plamínek	k1gInPc1
připomínající	připomínající	k2eAgNnSc1d1
tyto	tento	k3xDgFnPc4
oběti	oběť	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
1991-2015	1991-2015	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
přebírá	přebírat	k5eAaImIp3nS
žezlo	žezlo	k1gNnSc4
Graeme	Graem	k1gInSc5
Souness	Souness	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
získává	získávat	k5eAaImIp3nS
FA	fa	kA
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
ale	ale	k8xC
klub	klub	k1gInSc4
šokujícím	šokující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
vypadává	vypadávat	k5eAaImIp3nS
z	z	k7c2
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
po	po	k7c6
nepovedeném	povedený	k2eNgInSc6d1
zápase	zápas	k1gInSc6
s	s	k7c7
Bristol	Bristol	k1gInSc1
City	city	k1gNnSc4
FC	FC	kA
na	na	k7c4
Anfield	Anfield	k1gInSc4
<g/>
,	,	kIx,
Sounese	Sounese	k1gFnPc4
nahrazuje	nahrazovat	k5eAaImIp3nS
Roy	Roy	k1gMnSc1
Evans	Evans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
hry	hra	k1gFnSc2
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
pět	pět	k4xCc4
sezón	sezóna	k1gFnPc2
klub	klub	k1gInSc1
v	v	k7c6
lize	liga	k1gFnSc6
neskončil	skončit	k5eNaPmAgMnS
lépe	dobře	k6eAd2
než	než	k8xS
ne	ne	k9
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
získanou	získaný	k2eAgFnSc7d1
trofejí	trofej	k1gFnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
Ligový	ligový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1998-99	1998-99	k4
přichází	přicházet	k5eAaImIp3nS
do	do	k7c2
klubu	klub	k1gInSc2
Gérard	Gérard	k1gMnSc1
Houllier	Houllier	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
francouzské	francouzský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
oba	dva	k4xCgMnPc1
působí	působit	k5eAaImIp3nP
jako	jako	k8xC,k8xS
partneři	partner	k1gMnPc1
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc2
ale	ale	k8xC
nefunguje	fungovat	k5eNaImIp3nS
a	a	k8xC
tak	tak	k9
Evans	Evans	k1gInSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
1998	#num#	k4
klub	klub	k1gInSc1
opouští	opouštět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgFnPc4d3
sezónu	sezóna	k1gFnSc4
pod	pod	k7c7
Houllierovým	Houllierův	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
zažívá	zažívat	k5eAaImIp3nS
Liverpool	Liverpool	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
nejlepší	dobrý	k2eAgFnSc1d3
sezóna	sezóna	k1gFnSc1
za	za	k7c4
poslední	poslední	k2eAgNnPc4d1
léta	léto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
získává	získávat	k5eAaImIp3nS
unikátní	unikátní	k2eAgNnSc1d1
treble	treble	k6eAd1
díky	díky	k7c3
výhře	výhra	k1gFnSc3
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
a	a	k8xC
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
ještě	ještě	k6eAd1
korunuje	korunovat	k5eAaBmIp3nS
ziskem	zisk	k1gInSc7
Evropského	evropský	k2eAgInSc2d1
Superpoháru	superpohár	k1gInSc2
a	a	k8xC
výhrou	výhra	k1gFnSc7
v	v	k7c6
utkání	utkání	k1gNnSc6
o	o	k7c4
pohár	pohár	k1gInSc4
charity	charita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působí	působit	k5eAaImIp3nP
čeští	český	k2eAgMnPc1d1
reprezentanti	reprezentant	k1gMnPc1
Patrik	Patrik	k1gMnSc1
Berger	Berger	k1gMnSc1
a	a	k8xC
Vladimír	Vladimír	k1gMnSc1
Šmicer	Šmicer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnSc1
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
Dortmundu	Dortmund	k1gInSc6
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
se	se	k3xPyFc4
utkal	utkat	k5eAaPmAgInS
se	s	k7c7
španělským	španělský	k2eAgInSc7d1
Deportivo	Deportiva	k1gFnSc5
Alavés	Alavés	k1gInSc4
a	a	k8xC
vyhrál	vyhrát	k5eAaPmAgInS
divokým	divoký	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysněný	vysněný	k2eAgInSc4d1
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
se	se	k3xPyFc4
ale	ale	k9
Houllierovi	Houllier	k1gMnSc3
nedaří	dařit	k5eNaImIp3nS
získat	získat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2001-02	2001-02	k4
klub	klub	k1gInSc1
končí	končit	k5eAaImIp3nS
ligu	liga	k1gFnSc4
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
má	mít	k5eAaImIp3nS
ale	ale	k8xC
Houllier	Houllier	k1gInSc4
problémy	problém	k1gInPc1
se	s	k7c7
srdcem	srdce	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyústí	vyústit	k5eAaPmIp3nS
v	v	k7c6
operaci	operace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
návratu	návrat	k1gInSc6
do	do	k7c2
fotbalu	fotbal	k1gInSc2
ale	ale	k8xC
klub	klub	k1gInSc1
získává	získávat	k5eAaImIp3nS
již	již	k6eAd1
jen	jen	k9
Ligový	ligový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
stále	stále	k6eAd1
nikde	nikde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
sezóny	sezóna	k1gFnSc2
2003-04	2003-04	k4
Houllier	Houllier	k1gMnSc1
svoje	svůj	k3xOyFgNnSc4
angažmá	angažmá	k1gNnSc4
ukončuje	ukončovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
2004	#num#	k4
se	se	k3xPyFc4
manažerem	manažer	k1gMnSc7
stává	stávat	k5eAaImIp3nS
Španěl	Španěl	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Benítez	Benítez	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
předtím	předtím	k6eAd1
koučoval	koučovat	k5eAaImAgInS
Valencii	Valencie	k1gFnSc4
CF	CF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
ale	ale	k8xC
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
zklamání	zklamání	k1gNnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
umístil	umístit	k5eAaPmAgInS
v	v	k7c6
lize	liga	k1gFnSc6
až	až	k9
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
ale	ale	k8xC
sezóna	sezóna	k1gFnSc1
končí	končit	k5eAaImIp3nS
příjemným	příjemný	k2eAgNnSc7d1
překvapením	překvapení	k1gNnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
na	na	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
do	do	k7c2
finále	finále	k1gNnSc2
musel	muset	k5eAaImAgMnS
překonat	překonat	k5eAaPmF
překážky	překážka	k1gFnPc4
v	v	k7c6
podobě	podoba	k1gFnSc6
Juventusu	Juventus	k1gInSc2
a	a	k8xC
Chelsea	Chelse	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
domácí	domácí	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
ozdobou	ozdoba	k1gFnSc7
soutěže	soutěž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
ve	v	k7c6
finále	finále	k1gNnSc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
čekal	čekat	k5eAaImAgInS
italský	italský	k2eAgInSc1d1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
AC	AC	kA
Milán	Milán	k1gInSc1
po	po	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
vedl	vést	k5eAaImAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
se	se	k3xPyFc4
neuvěřitelně	uvěřitelně	k6eNd1
dostal	dostat	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
zápasu	zápas	k1gInSc2
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
druhého	druhý	k4xOgInSc2
poločasu	poločas	k1gInSc2
dokázal	dokázat	k5eAaPmAgMnS
skóre	skóre	k1gNnSc4
srovnat	srovnat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
góly	gól	k1gInPc4
přitom	přitom	k6eAd1
padly	padnout	k5eAaPmAgInP,k5eAaImAgInP
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
šesti	šest	k4xCc2
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
za	za	k7c2
pekelné	pekelný	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
liverpoolských	liverpoolský	k2eAgMnPc2d1
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
měli	mít	k5eAaImAgMnP
na	na	k7c6
stadionu	stadion	k1gInSc6
absolutní	absolutní	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
řadu	řada	k1gFnSc4
nakonec	nakonec	k6eAd1
přišly	přijít	k5eAaPmAgFnP
penalty	penalta	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6,k3yQgFnPc6,k3yIgFnPc6
zazářil	zazářit	k5eAaPmAgMnS
polský	polský	k2eAgMnSc1d1
gólman	gólman	k1gMnSc1
Jerzy	Jerza	k1gFnSc2
Dudek	Dudek	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
z	z	k7c2
pěti	pět	k4xCc2
penalt	penalta	k1gFnPc2
dostal	dostat	k5eAaPmAgMnS
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgFnSc4d1
penaltu	penalta	k1gFnSc4
proměnil	proměnit	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Šmicer	Šmicer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
navíc	navíc	k6eAd1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
poločase	poločas	k1gInSc6
svým	svůj	k3xOyFgInSc7
gólem	gól	k1gInSc7
snižoval	snižovat	k5eAaImAgInS
na	na	k7c6
rozdíl	rozdíl	k1gInSc1
jednoho	jeden	k4xCgInSc2
gólu	gól	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
utkání	utkání	k1gNnSc6
také	také	k9
nastoupil	nastoupit	k5eAaPmAgMnS
další	další	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stali	stát	k5eAaPmAgMnP
prvními	první	k4xOgMnPc7
českými	český	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
nejprestižnější	prestižní	k2eAgFnSc4d3
evropskou	evropský	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2005-06	2005-06	k4
Liverpool	Liverpool	k1gInSc4
v	v	k7c6
lize	liga	k1gFnSc6
získává	získávat	k5eAaImIp3nS
82	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
nejvíce	nejvíce	k6eAd1,k6eAd3
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
titulu	titul	k1gInSc2
to	ten	k3xDgNnSc4
ale	ale	k9
opět	opět	k6eAd1
nestačí	stačit	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
nakonec	nakonec	k6eAd1
zakončil	zakončit	k5eAaPmAgInS
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
dalším	další	k2eAgNnSc6d1
dramatickém	dramatický	k2eAgNnSc6d1
finále	finále	k1gNnSc6
získal	získat	k5eAaPmAgInS
FA	fa	k1gNnSc7
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
hraném	hraný	k2eAgNnSc6d1
v	v	k7c6
Cardiffu	Cardiff	k1gInSc6
Liverpool	Liverpool	k1gInSc1
těsně	těsně	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
prohrával	prohrávat	k5eAaImAgInS
s	s	k7c7
West	West	k1gMnSc1
Hamem	Ham	k1gMnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
začátku	začátek	k1gInSc6
natavení	natavení	k1gNnSc2
ale	ale	k8xC
kapitán	kapitán	k1gMnSc1
Steven	Steven	k2eAgInSc1d1
Gerrard	Gerrard	k1gInSc1
střelou	střela	k1gFnSc7
ze	z	k7c2
40	#num#	k4
metrů	metr	k1gInPc2
vyrovnal	vyrovnat	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
utkání	utkání	k1gNnSc1
nakonec	nakonec	k6eAd1
dospělo	dochvít	k5eAaPmAgNnS
až	až	k9
do	do	k7c2
penalt	penalta	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
se	se	k3xPyFc4
vyznamenal	vyznamenat	k5eAaPmAgMnS
španělský	španělský	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
a	a	k8xC
specialista	specialista	k1gMnSc1
na	na	k7c4
penalty	penalty	k1gNnSc4
Pepe	Pep	k1gMnSc2
Reina	Rein	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2007	#num#	k4
znamená	znamenat	k5eAaImIp3nS
pro	pro	k7c4
klub	klub	k1gInSc4
velkou	velký	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
letech	léto	k1gNnPc6
jednání	jednání	k1gNnPc1
s	s	k7c7
různými	různý	k2eAgMnPc7d1
investory	investor	k1gMnPc7
klub	klub	k1gInSc4
souhlasí	souhlasit	k5eAaImIp3nS
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
dua	duo	k1gNnSc2
amerických	americký	k2eAgMnPc2d1
obchodníků	obchodník	k1gMnPc2
George	Georg	k1gFnSc2
Gillett	Gillett	k1gMnSc1
–	–	k?
Tom	Tom	k1gMnSc1
Hicks	Hicks	k1gInSc1
a	a	k8xC
klub	klub	k1gInSc1
tak	tak	k6eAd1
přechází	přecházet	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
než	než	k8xS
anglických	anglický	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
převzetí	převzetí	k1gNnSc2
–	–	k?
přes	přes	k7c4
400	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
liber	libra	k1gFnPc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
priority	priorita	k1gFnPc4
nových	nový	k2eAgMnPc2d1
majitelů	majitel	k1gMnPc2
patří	patřit	k5eAaImIp3nS
výstavba	výstavba	k1gFnSc1
nového	nový	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
během	během	k7c2
několika	několik	k4yIc2
let	léto	k1gNnPc2
vyrůst	vyrůst	k5eAaPmF
v	v	k7c6
sousedním	sousední	k2eAgInSc6d1
Stanley	Stanleum	k1gNnPc7
Parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
končí	končit	k5eAaImIp3nS
sezónu	sezóna	k1gFnSc4
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
ligové	ligový	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafael	Rafael	k1gMnSc1
Benítez	Benítez	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
klub	klub	k1gInSc4
znovu	znovu	k6eAd1
dovedl	dovést	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
znovu	znovu	k6eAd1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
vyřadil	vyřadit	k5eAaPmAgMnS
Chelsea	Chelsea	k1gMnSc1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
po	po	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hrdinou	hrdina	k1gMnSc7
opět	opět	k6eAd1
Pepe	Pepe	k1gInSc4
Reina	Rein	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnPc7
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Aténách	Atény	k1gFnPc6
<g/>
,	,	kIx,
soupeřem	soupeř	k1gMnSc7
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
tentokrát	tentokrát	k6eAd1
nad	nad	k7c4
síly	síla	k1gFnPc4
hráčů	hráč	k1gMnPc2
a	a	k8xC
Liverpool	Liverpool	k1gInSc1
ho	on	k3xPp3gNnSc4
prohrává	prohrávat	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
<g/>
-Současnost	-Současnost	k1gFnSc1
(	(	kIx(
<g/>
Německý	německý	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Jürgen	Jürgen	k1gInSc4
Klopp	Klopp	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Trenér	trenér	k1gMnSc1
Jürgen	Jürgen	k1gInSc4
Klopp	Klopp	k1gMnSc1
a	a	k8xC
útočník	útočník	k1gMnSc1
Sadio	Sadio	k6eAd1
Mané	Maná	k1gFnSc2
v	v	k7c6
obleku	oblek	k1gInSc6
</s>
<s>
Začátek	začátek	k1gInSc1
podzimu	podzim	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
připravil	připravit	k5eAaPmAgInS
o	o	k7c4
trenérské	trenérský	k2eAgNnSc4d1
místo	místo	k1gNnSc4
Brendana	Brendan	k1gMnSc2
Rodgerse	Rodgerse	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
nahradil	nahradit	k5eAaPmAgInS
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
posílené	posílená	k1gFnSc2
v	v	k7c6
létě	léto	k1gNnSc6
o	o	k7c4
jména	jméno	k1gNnPc4
jako	jako	k8xS,k8xC
Roberto	Roberta	k1gFnSc5
Firmino	Firmin	k2eAgNnSc4d1
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
Benteke	Benteke	k1gFnSc1
<g/>
,	,	kIx,
Nathaniel	Nathaniel	k1gInSc1
Clyne	Clyn	k1gInSc5
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Gomez	Gomez	k1gMnSc1
a	a	k8xC
James	James	k1gMnSc1
Milner	Milnra	k1gFnPc2
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
tabulce	tabulka	k1gFnSc6
osmé	osmý	k4xOgFnSc6
<g/>
,	,	kIx,
pozornost	pozornost	k1gFnSc4
si	se	k3xPyFc3
však	však	k9
získalo	získat	k5eAaPmAgNnS
postupem	postup	k1gInSc7
do	do	k7c2
finále	finále	k1gNnSc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
a	a	k8xC
finále	finále	k1gNnSc2
Ligového	ligový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
EFL	EFL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Benfice	Benfika	k1gFnSc3
ani	ani	k8xC
proti	proti	k7c3
Manchesteru	Manchester	k1gInSc3
City	City	k1gFnSc2
ale	ale	k8xC
Liverpool	Liverpool	k1gInSc1
nevyhrál	vyhrát	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
další	další	k2eAgFnSc7d1
sezónou	sezóna	k1gFnSc7
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
už	už	k9
Klopp	Klopp	k1gInSc1
mohl	moct	k5eAaImAgInS
využít	využít	k5eAaPmF
letní	letní	k2eAgFnPc4d1
přípravy	příprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
úvod	úvod	k1gInSc4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnPc3
hráčům	hráč	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
na	na	k7c6
půdě	půda	k1gFnSc6
Arsenalu	Arsenal	k1gInSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
prohráli	prohrát	k5eAaPmAgMnP
s	s	k7c7
Burnley	Burnle	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oproti	oproti	k7c3
podzimu	podzim	k1gInSc3
se	se	k3xPyFc4
jarní	jarní	k2eAgInPc1d1
výkony	výkon	k1gInPc1
zhoršily	zhoršit	k5eAaPmAgInP
<g/>
,	,	kIx,
po	po	k7c6
výhře	výhra	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Manchesterem	Manchester	k1gInSc7
City	City	k1gFnSc1
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
během	během	k7c2
jara	jaro	k1gNnSc2
musel	muset	k5eAaImAgMnS
sklonit	sklonit	k5eAaPmF
před	před	k7c4
Swansea	Swanse	k2eAgMnSc4d1
<g/>
,	,	kIx,
Hullem	Hull	k1gInSc7
a	a	k8xC
Crystal	Crystal	k1gFnSc7
Palace	Palace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Liverpool	Liverpool	k1gInSc1
navíc	navíc	k6eAd1
vypadl	vypadnout	k5eAaPmAgInS
v	v	k7c6
EFL	EFL	kA
Cupu	cupat	k5eAaImIp1nS
se	s	k7c7
Southamptonem	Southampton	k1gInSc7
a	a	k8xC
též	též	k9
FA	fa	k1gNnSc1
Cupu	cup	k1gInSc2
s	s	k7c7
Wolverhamptonem	Wolverhampton	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
dosáhl	dosáhnout	k5eAaPmAgInS
čtvrtého	čtvrtý	k4xOgNnSc2
místa	místo	k1gNnSc2
a	a	k8xC
Klopp	Klopp	k1gMnSc1
tak	tak	k8xC,k8xS
týmu	tým	k1gInSc2
vrátil	vrátit	k5eAaPmAgInS
účast	účast	k1gFnSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
umístění	umístění	k1gNnSc4
mezi	mezi	k7c7
první	první	k4xOgFnSc7
čtveřicí	čtveřice	k1gFnSc7
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
teprve	teprve	k6eAd1
podruhé	podruhé	k6eAd1
během	během	k7c2
osmi	osm	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příslibem	příslib	k1gInSc7
byla	být	k5eAaImAgFnS
útočná	útočný	k2eAgFnSc1d1
vozba	vozba	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
utvořili	utvořit	k5eAaPmAgMnP
Roberto	Roberta	k1gFnSc5
Firmino	Firmin	k2eAgNnSc1d1
<g/>
,	,	kIx,
Philippe	Philipp	k1gMnSc5
Coutinho	Coutinha	k1gMnSc5
a	a	k8xC
Sadio	Sadio	k1gNnSc4
Mané	Maná	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
podpořily	podpořit	k5eAaPmAgFnP
výkony	výkon	k1gInPc4
záložníků	záložník	k1gMnPc2
Emre	Emre	k1gNnSc2
Cana	Canum	k1gNnSc2
a	a	k8xC
i	i	k9
Adama	Adam	k1gMnSc4
Lallany	Lallana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útočné	útočný	k2eAgNnSc1d1
těžiště	těžiště	k1gNnSc1
mužstvo	mužstvo	k1gNnSc1
následně	následně	k6eAd1
opět	opět	k6eAd1
posílilo	posílit	k5eAaPmAgNnS
–	–	k?
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
Premier	Premira	k1gFnPc2
League	League	k1gNnSc1
již	již	k6eAd1
seznámený	seznámený	k2eAgMnSc1d1
Mohamed	Mohamed	k1gMnSc1
Salah	Salah	k1gMnSc1
z	z	k7c2
AS	as	k1gNnSc2
Řím	Řím	k1gInSc1
a	a	k8xC
Dominic	Dominice	k1gFnPc2
Solanke	Solank	k1gFnSc2
z	z	k7c2
Chelsea	Chelse	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
levém	levý	k2eAgInSc6d1
kraji	kraj	k1gInSc6
obrany	obrana	k1gFnSc2
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
nově	nově	k6eAd1
představit	představit	k5eAaPmF
Andrew	Andrew	k1gFnSc4
Robertson	Robertsona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
pravém	pravý	k2eAgInSc6d1
se	se	k3xPyFc4
o	o	k7c4
slovo	slovo	k1gNnSc4
hlásil	hlásit	k5eAaImAgMnS
18	#num#	k4
<g/>
letý	letý	k2eAgInSc1d1
Trent	Trent	k1gInSc1
Alexander-Arnold	Alexander-Arnolda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Liverpoolské	liverpoolský	k2eAgNnSc1d1
angažmá	angažmá	k1gNnSc1
bylo	být	k5eAaImAgNnS
po	po	k7c6
10	#num#	k4
letech	léto	k1gNnPc6
u	u	k7c2
konce	konec	k1gInSc2
pro	pro	k7c4
záložníka	záložník	k1gMnSc4
Lucase	Lucasa	k1gFnSc6
Leivu	Leiv	k1gInSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Lazia	Lazium	k1gNnSc2
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ligový	ligový	k2eAgInSc1d1
ročník	ročník	k1gInSc1
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
začala	začít	k5eAaPmAgFnS
pro	pro	k7c4
Jürgena	Jürgen	k2eAgMnSc4d1
Kloppa	Klopp	k1gMnSc4
remízou	remíza	k1gFnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
na	na	k7c6
stadionu	stadion	k1gInSc6
Watfordu	Watforda	k1gFnSc4
<g/>
,	,	kIx,
branku	branka	k1gFnSc4
zaznamenal	zaznamenat	k5eAaPmAgMnS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
debutant	debutant	k1gMnSc1
Salah	Salah	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
přesvědčil	přesvědčit	k5eAaPmAgInS
o	o	k7c6
síle	síla	k1gFnSc6
Manchesteru	Manchester	k1gInSc2
City	City	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
koučem	kouč	k1gMnSc7
Pepem	Pepem	k?
Guardiolou	Guardiola	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
celek	celek	k1gInSc1
z	z	k7c2
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
prohrál	prohrát	k5eAaPmAgInS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
ledna	leden	k1gInSc2
ale	ale	k8xC
domácí	domácí	k2eAgInSc1d1
Liverpool	Liverpool	k1gInSc1
na	na	k7c6
Anfieldu	Anfieldo	k1gNnSc6
nad	nad	k7c7
City	city	k1gNnSc7
vyhrál	vyhrát	k5eAaPmAgInS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
a	a	k8xC
ukončil	ukončit	k5eAaPmAgInS
jejich	jejich	k3xOp3gFnSc4
sérii	série	k1gFnSc4
bez	bez	k7c2
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
38	#num#	k4
<g/>
.	.	kIx.
závěrečném	závěrečný	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
proti	proti	k7c3
Brightonu	Brighton	k1gInSc3
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
přispěl	přispět	k5eAaPmAgMnS
i	i	k8xC
Salah	Salah	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
s	s	k7c7
32	#num#	k4
brankami	branka	k1gFnPc7
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
soutěže	soutěž	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
31	#num#	k4
branek	branka	k1gFnPc2
rozdělený	rozdělený	k2eAgMnSc1d1
mezi	mezi	k7c4
trojici	trojice	k1gFnSc4
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
<g/>
,	,	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
a	a	k8xC
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
se	se	k3xPyFc4
Liverpoolu	Liverpool	k1gInSc6
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
přes	přes	k7c4
předkolo	předkolo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
dvěma	dva	k4xCgInPc7
výhrami	výhra	k1gFnPc7
nad	nad	k7c7
Hoffenheimem	Hoffenheimo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
(	(	kIx(
<g/>
Sevilla	Sevilla	k1gFnSc1
<g/>
,	,	kIx,
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Maribor	Maribor	k1gInSc1
<g/>
)	)	kIx)
neprohrál	prohrát	k5eNaPmAgInS
a	a	k8xC
po	po	k7c6
třech	tři	k4xCgFnPc6
výhrách	výhra	k1gFnPc6
a	a	k8xC
třech	tři	k4xCgFnPc6
remízách	remíza	k1gFnPc6
z	z	k7c2
prvního	první	k4xOgNnSc2
místa	místo	k1gNnSc2
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Portu	port	k1gInSc3
vyhrálo	vyhrát	k5eAaPmAgNnS
mužstvo	mužstvo	k1gNnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
v	v	k7c6
odvetném	odvetný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
tak	tak	k6eAd1
stačila	stačit	k5eAaBmAgFnS
remíza	remíza	k1gFnSc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
pokořilo	pokořit	k5eAaPmAgNnS
ligovou	ligový	k2eAgFnSc4d1
konkurenci	konkurence	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
Manchesteru	Manchester	k1gInSc2
City	City	k1gFnSc2
a	a	k8xC
po	po	k7c6
dvou	dva	k4xCgFnPc6
výhrách	výhra	k1gFnPc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
postoupilo	postoupit	k5eAaPmAgNnS
dále	daleko	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
AS	as	k1gNnSc3
Řím	Řím	k1gInSc1
doma	doma	k6eAd1
vedl	vést	k5eAaImAgInS
Liverpool	Liverpool	k1gInSc1
zásluhou	zásluhou	k7c2
ofenzivní	ofenzivní	k2eAgFnSc2d1
trojice	trojice	k1gFnSc2
Mané	Maná	k1gFnSc2
<g/>
–	–	k?
<g/>
Firmino	Firmin	k2eAgNnSc1d1
<g/>
–	–	k?
<g/>
Salah	Salah	k1gInSc1
již	již	k6eAd1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
Italové	Ital	k1gMnPc1
v	v	k7c6
závěru	závěr	k1gInSc6
snížili	snížit	k5eAaPmAgMnP
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
třech	tři	k4xCgFnPc2
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
přestože	přestože	k8xS
v	v	k7c6
Římě	Řím	k1gInSc6
podlehl	podlehnout	k5eAaPmAgMnS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
a	a	k8xC
zaznamenal	zaznamenat	k5eAaPmAgMnS
první	první	k4xOgFnSc4
porážku	porážka	k1gFnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
dokráčel	dokráčet	k5eAaPmAgMnS
do	do	k7c2
finálového	finálový	k2eAgNnSc2d1
střetnutí	střetnutí	k1gNnSc2
s	s	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
prvního	první	k4xOgInSc2
finálového	finálový	k2eAgInSc2d1
poločasu	poločas	k1gInSc2
gól	gól	k1gInSc1
nepadl	padnout	k5eNaPmAgInS
<g/>
,	,	kIx,
anglický	anglický	k2eAgInSc4d1
celek	celek	k1gInSc4
však	však	k9
utrpěl	utrpět	k5eAaPmAgMnS
ztrátu	ztráta	k1gFnSc4
Salaha	Salaha	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zranil	zranit	k5eAaPmAgMnS
po	po	k7c6
souboji	souboj	k1gInSc6
se	s	k7c7
Sergiem	Sergius	k1gMnSc7
Ramosem	Ramos	k1gMnSc7
<g/>
,	,	kIx,
kapitánem	kapitán	k1gMnSc7
soupeře	soupeř	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smolařem	smolař	k1gMnSc7
zápasu	zápas	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
brankář	brankář	k1gMnSc1
Liverpoolu	Liverpool	k1gInSc2
Loris	Loris	k1gFnPc2
Karius	Karius	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
51	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
špatně	špatně	k6eAd1
nahodil	nahodit	k5eAaPmAgInS
balón	balón	k1gInSc1
na	na	k7c6
Karima	Karima	k?
Benzemu	Benzem	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
otevřel	otevřít	k5eAaPmAgMnS
skóre	skóre	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
minutách	minuta	k1gFnPc6
srovnal	srovnat	k5eAaPmAgMnS
na	na	k7c4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Sadio	Sadio	k6eAd1
Mané	Maný	k2eAgNnSc1d1
<g/>
,	,	kIx,
střídající	střídající	k2eAgMnSc1d1
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
však	však	k9
vstřelil	vstřelit	k5eAaPmAgMnS
branku	branka	k1gFnSc4
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velšan	Velšan	k1gMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
rovněž	rovněž	k9
poslední	poslední	k2eAgFnSc4d1
branku	branka	k1gFnSc4
zápasu	zápas	k1gInSc2
na	na	k7c4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
jeho	jeho	k3xOp3gFnSc4
střelu	střela	k1gFnSc4
ze	z	k7c2
skoro	skoro	k6eAd1
30	#num#	k4
metrů	metr	k1gInPc2
špatně	špatně	k6eAd1
odhadl	odhadnout	k5eAaPmAgInS
Karius	Karius	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finalista	finalista	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
posléze	posléze	k6eAd1
investoval	investovat	k5eAaBmAgMnS
do	do	k7c2
posil	posila	k1gFnPc2
přes	přes	k7c4
170	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
reálně	reálně	k6eAd1
schopen	schopen	k2eAgMnSc1d1
konkurovat	konkurovat	k5eAaImF
obhajujícímu	obhajující	k2eAgInSc3d1
Manchesteru	Manchester	k1gInSc2
City	City	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
ledna	leden	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
mužstvu	mužstvo	k1gNnSc6
střední	střední	k2eAgMnSc1d1
obránce	obránce	k1gMnSc1
Virgil	Virgil	k1gMnSc1
van	vana	k1gFnPc2
Dijk	Dijk	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c2
něhož	jenž	k3xRgNnSc2
Liverpool	Liverpool	k1gInSc1
zaplatil	zaplatit	k5eAaPmAgInS
částku	částka	k1gFnSc4
75	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
tak	tak	k9
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejdražším	drahý	k2eAgMnSc7d3
obráncem	obránce	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Van	van	k1gInSc1
Dijkův	Dijkův	k2eAgInSc4d1
přestup	přestup	k1gInSc4
zaplatil	zaplatit	k5eAaPmAgMnS
odchod	odchod	k1gInSc4
Brazilce	Brazilec	k1gMnSc2
Coutinha	Coutinh	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
za	za	k7c4
jinou	jiný	k2eAgFnSc4d1
rekordní	rekordní	k2eAgFnSc4d1
částku	částka	k1gFnSc4
koupila	koupit	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nešťastníka	nešťastník	k1gMnSc2
finálového	finálový	k2eAgNnSc2d1
utkání	utkání	k1gNnSc2
Kariuse	Kariuse	k1gFnSc2
v	v	k7c6
bráně	brána	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
brazilský	brazilský	k2eAgMnSc1d1
gólman	gólman	k1gMnSc1
Alisson	Alisson	k1gMnSc1
<g/>
,	,	kIx,
záložní	záložní	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
vyztužily	vyztužit	k5eAaPmAgFnP
Naby	Naba	k1gFnPc1
Keï	Keï	k1gInSc2
s	s	k7c7
Fabinhem	Fabinh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočnou	útočný	k2eAgFnSc4d1
trojici	trojice	k1gFnSc4
měl	mít	k5eAaImAgMnS
v	v	k7c6
případě	případ	k1gInSc6
nouze	nouze	k1gFnSc1
suplovat	suplovat	k5eAaImF
Xherdan	Xherdan	k1gInSc4
Shaqiri	Shaqir	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zranění	zranění	k1gNnSc1
neumožnilo	umožnit	k5eNaPmAgNnS
využít	využít	k5eAaPmF
Alexe	Alex	k1gMnSc5
Oxlade-Chamberlaina	Oxlade-Chamberlaina	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
tak	tak	k6eAd1
zameškal	zameškat	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
ročníku	ročník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypršení	vypršení	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
jiný	jiný	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
Emre	Emre	k1gNnSc2
Can	Can	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
upřednostnil	upřednostnit	k5eAaPmAgMnS
Juventus	Juventus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátek	začátek	k1gInSc1
ročníku	ročník	k1gInSc2
pro	pro	k7c4
Liverpool	Liverpool	k1gInSc4
odstartoval	odstartovat	k5eAaPmAgInS
domácím	domácí	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
proti	proti	k7c3
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
gólům	gól	k1gInPc3
Mohameda	Mohamed	k1gMnSc4
Salaha	Salaha	k1gFnSc1
<g/>
,	,	kIx,
Sadio	Sadio	k6eAd1
Maného	Maného	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
a	a	k8xC
Daniela	Daniela	k1gFnSc1
Sturridge	Sturridge	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Série	série	k1gFnSc1
výher	výhra	k1gFnPc2
skončila	skončit	k5eAaPmAgFnS
počínaje	počínaje	k7c7
7	#num#	k4
<g/>
.	.	kIx.
kolem	kolem	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
hrál	hrát	k5eAaImAgInS
Liverpool	Liverpool	k1gInSc1
na	na	k7c6
půdě	půda	k1gFnSc6
Chelsea	Chelse	k1gInSc2
nerozhodně	rozhodně	k6eNd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
nadešlo	nadejít	k5eAaPmAgNnS
klání	klání	k1gNnSc1
se	se	k3xPyFc4
City	City	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
Anfieldu	Anfield	k1gInSc6
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
nerozhodně	rozhodně	k6eNd1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
pár	pár	k4xCyI
kol	kolo	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
Liverpool	Liverpool	k1gInSc1
nadmul	nadmout	k5eAaPmAgInS
k	k	k7c3
sérii	série	k1gFnSc3
výher	výhra	k1gFnPc2
čítající	čítající	k2eAgNnSc1d1
devět	devět	k4xCc1
ligových	ligový	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
výhry	výhra	k1gFnSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nad	nad	k7c4
United	United	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
uzmul	uzmout	k5eAaPmAgMnS
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
stadionu	stadion	k1gInSc6
Etihad	Etihad	k1gInSc1
ale	ale	k8xC
podlehl	podlehnout	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
Manchesteru	Manchester	k1gInSc2
City	City	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
na	na	k7c6
jaře	jaro	k1gNnSc6
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
formy	forma	k1gFnSc2
a	a	k8xC
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
už	už	k6eAd1
nepustil	pustit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klopp	Klopp	k1gMnSc1
soupeře	soupeř	k1gMnSc4
stíhal	stíhat	k5eAaImAgMnS
do	do	k7c2
samého	samý	k3xTgInSc2
závěru	závěr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k9
96	#num#	k4
bodů	bod	k1gInPc2
nestačilo	stačit	k5eNaBmAgNnS
na	na	k7c4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
City	City	k1gFnPc1
měly	mít	k5eAaImAgFnP
na	na	k7c6
kontě	konto	k1gNnSc6
o	o	k7c4
dva	dva	k4xCgInPc4
body	bod	k1gInPc4
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brankář	brankář	k1gMnSc1
Allison	Allison	k1gMnSc1
vychytal	vychytat	k5eAaPmAgMnS
nejvíce	hodně	k6eAd3,k6eAd1
čistých	čistý	k2eAgNnPc2d1
kont	konto	k1gNnPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
útočníci	útočník	k1gMnPc1
Salah	Salaha	k1gFnPc2
a	a	k8xC
Mané	Maná	k1gFnSc2
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
k	k	k7c3
Aubameyangovi	Aubameyang	k1gMnSc3
na	na	k7c6
čele	čelo	k1gNnSc6
střelecké	střelecký	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
(	(	kIx(
<g/>
22	#num#	k4
gólů	gól	k1gInPc2
všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
obránce	obránce	k1gMnSc1
Van	vana	k1gFnPc2
Dijk	Dijk	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
nejlepším	dobrý	k2eAgMnSc7d3
obráncem	obránce	k1gMnSc7
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgMnPc1d1
k	k	k7c3
datu	datum	k1gNnSc3
<g/>
:	:	kIx,
8	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2021	#num#	k4
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Alisson	Alisson	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Fabinho	Fabinze	k6eAd1
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Virgil	Virgil	k1gMnSc1
van	vana	k1gFnPc2
Dijk	Dijk	k1gMnSc1
(	(	kIx(
<g/>
třetí	třetí	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Georginio	Georginio	k6eAd1
Wijnaldum	Wijnaldum	k1gNnSc1
(	(	kIx(
<g/>
čtvrtý	čtvrtý	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Thiago	Thiago	k6eAd1
Alcântara	Alcântara	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
James	James	k1gMnSc1
Milner	Milner	k1gMnSc1
(	(	kIx(
<g/>
zástupce	zástupce	k1gMnSc1
kapitána	kapitán	k1gMnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Naby	Naba	k1gFnPc1
Keï	Keï	k1gNnSc2
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Roberto	Roberta	k1gFnSc5
Firmino	Firmin	k2eAgNnSc4d1
</s>
<s>
10	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Sadio	Sadio	k1gNnSc1
Mané	Maná	k1gFnSc2
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
Salah	Salah	k1gMnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Joe	Joe	k?
Gomez	Gomez	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Adrián	Adrián	k1gMnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jordan	Jordan	k1gMnSc1
Henderson	Henderson	k1gMnSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Alex	Alex	k1gMnSc1
Oxlade-Chamberlain	Oxlade-Chamberlain	k1gMnSc1
</s>
<s>
17	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Curtis	Curtis	k1gFnSc1
Jones	Jonesa	k1gFnPc2
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ozan	Ozan	k1gMnSc1
Kabak	Kabak	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c6
hostování	hostování	k1gNnSc6
z	z	k7c2
Schalke	Schalk	k1gFnSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Diogo	Diogo	k6eAd1
Jota	jota	k1gFnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Kostas	Kostas	k1gMnSc1
Tsimikas	Tsimikas	k1gMnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Xherdan	Xherdan	k1gInSc1
Shaqiri	Shaqir	k1gFnSc2
</s>
<s>
26	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Andrew	Andrew	k?
Robertson	Robertson	k1gInSc1
</s>
<s>
27	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Divock	Divock	k1gInSc1
Origi	Orig	k1gFnSc2
</s>
<s>
28	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ben	Ben	k1gInSc1
Davies	Daviesa	k1gFnPc2
</s>
<s>
32	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Joël	Joël	k1gMnSc1
Matip	Matip	k1gMnSc1
</s>
<s>
46	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Rhys	Rhys	k6eAd1
Williams	Williams	k1gInSc1
</s>
<s>
47	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Nathaniel	Nathaniel	k1gInSc1
Phillips	Phillipsa	k1gFnPc2
</s>
<s>
58	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ben	Ben	k1gInSc1
Woodburn	Woodburna	k1gFnPc2
</s>
<s>
62	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Caoimhín	Caoimhín	k1gInSc1
Kelleher	Kellehra	k1gFnPc2
</s>
<s>
66	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Trent	Trent	k1gMnSc1
Alexander-Arnold	Alexander-Arnold	k1gMnSc1
</s>
<s>
76	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Neco	Neco	k6eAd1
Williams	Williams	k1gInSc1
</s>
<s>
B	B	kA
</s>
<s>
Marcelo	Marcela	k1gFnSc5
Pitaluga	Pitalug	k1gMnSc4
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Marko	Marko	k1gMnSc1
Grujić	Grujić	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Portu	porto	k1gNnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
18	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Takumi	Taku	k1gFnPc7
Minamino	Minamin	k2eAgNnSc4d1
(	(	kIx(
<g/>
v	v	k7c6
Southamptonu	Southampton	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Loris	Loris	k1gInSc1
Karius	Karius	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
Unionu	union	k1gInSc6
Berlin	berlina	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
54	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Sheyi	Sheyi	k1gNnSc1
Ojo	Ojo	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Cardiffu	Cardiff	k1gInSc6
City	City	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
56	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
St	St	kA
Patrick	Patrick	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Athletic	Athletice	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
59	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Harry	Harr	k1gInPc1
Wilson	Wilsona	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Cardiffu	Cardiff	k1gInSc6
City	City	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
67	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Harvey	Harvey	k1gInPc1
Elliott	Elliotta	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
Blackburnu	Blackburno	k1gNnSc6
Rovers	Roversa	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
72	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Sepp	Sepp	k1gMnSc1
van	vana	k1gFnPc2
den	den	k1gInSc1
Berg	Berg	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Prestonu	Preston	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
73	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Kamil	Kamil	k1gMnSc1
Grabara	Grabar	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
AGF	AGF	kA
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Ú	Ú	kA
</s>
<s>
Taiwo	Taiwo	k1gNnSc1
Awoniyi	Awoniy	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Unionu	union	k1gInSc6
Berlin	berlina	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Trenérský	trenérský	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc1
</s>
<s>
Asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
</s>
<s>
Pepijn	Pepijn	k1gNnSc1
Lijnders	Lijndersa	k1gFnPc2
</s>
<s>
Peter	Peter	k1gMnSc1
Krawietz	Krawietz	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
brankářů	brankář	k1gMnPc2
</s>
<s>
John	John	k1gMnSc1
Achterberg	Achterberg	k1gMnSc1
</s>
<s>
Kondiční	kondiční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Andreas	Andreas	k1gMnSc1
Kornmayer	Kornmayer	k1gMnSc1
</s>
<s>
Stadion	stadion	k1gInSc1
</s>
<s>
Klub	klub	k1gInSc1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
hraje	hrát	k5eAaImIp3nS
domácí	domácí	k2eAgInSc4d1
zápasy	zápas	k1gInPc4
na	na	k7c6
stadionu	stadion	k1gInSc6
Anfield	Anfield	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadion	stadion	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Stanley	Stanlea	k1gFnSc2
Parku	park	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
něm	on	k3xPp3gInSc6
začal	začít	k5eAaPmAgMnS
hrát	hrát	k5eAaImF
svoje	svůj	k3xOyFgInPc4
zápasy	zápas	k1gInPc4
Everton	Everton	k1gInSc4
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
rozepři	rozepře	k1gFnSc6
s	s	k7c7
majitelem	majitel	k1gMnSc7
stadionu	stadion	k1gInSc2
Johnem	John	k1gMnSc7
Houldingem	Houlding	k1gInSc7
<g/>
,	,	kIx,
Everton	Everton	k1gInSc1
FC	FC	kA
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Goodison	Goodisona	k1gFnPc2
Parku	park	k1gInSc2
a	a	k8xC
John	John	k1gMnSc1
Houlding	Houlding	k1gInSc4
založil	založit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
klub	klub	k1gInSc4
jménem	jméno	k1gNnSc7
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
na	na	k7c4
Anfield	Anfield	k1gInSc4
Road	Road	k1gInSc4
domácím	domácí	k2eAgInSc7d1
týmem	tým	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dokončen	dokončit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
New	New	k1gMnSc1
Anfield	Anfield	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
pojme	pojmout	k5eAaPmIp3nS
přibližně	přibližně	k6eAd1
60	#num#	k4
tisíc	tisíc	k4xCgInPc2
diváků	divák	k1gMnPc2
s	s	k7c7
možností	možnost	k1gFnSc7
rozšíření	rozšíření	k1gNnSc2
kapacity	kapacita	k1gFnSc2
na	na	k7c4
73	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jeho	jeho	k3xOp3gMnSc7
unikátním	unikátní	k2eAgInSc7d1
designem	design	k1gInSc7
bude	být	k5eAaImBp3nS
patřit	patřit	k5eAaImF
mezi	mezi	k7c4
přední	přední	k2eAgInPc4d1
evropské	evropský	k2eAgInPc4d1
a	a	k8xC
světové	světový	k2eAgInPc4d1
stadiony	stadion	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Hymnou	hymna	k1gFnSc7
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
píseň	píseň	k1gFnSc1
You	You	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Never	Never	k1gMnSc1
Walk	Walk	k1gMnSc1
Alone	Alon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
zpívá	zpívat	k5eAaImIp3nS
téměř	téměř	k6eAd1
na	na	k7c6
každém	každý	k3xTgNnSc6
domácím	domácí	k2eAgNnSc6d1
(	(	kIx(
<g/>
a	a	k8xC
často	často	k6eAd1
i	i	k9
venkovním	venkovní	k2eAgInSc7d1
<g/>
)	)	kIx)
utkání	utkání	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
liverpoolské	liverpoolský	k2eAgNnSc1d1
mužstvo	mužstvo	k1gNnSc1
vyhraje	vyhrát	k5eAaPmIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
silnější	silný	k2eAgNnSc4d2
mužstvo	mužstvo	k1gNnSc4
<g/>
,	,	kIx,
remizuje	remizovat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
také	také	k9
rivalita	rivalita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
úplně	úplně	k6eAd1
nejstarší	starý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
klubem	klub	k1gInSc7
Everton	Everton	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
v	v	k7c6
Liverpoolu	Liverpool	k1gInSc6
<g/>
,	,	kIx,
své	svůj	k3xOyFgInPc4
zápasy	zápas	k1gInPc4
7	#num#	k4
let	léto	k1gNnPc2
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
Anfield	Anfield	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
ale	ale	k8xC
stadion	stadion	k1gInSc4
opustil	opustit	k5eAaPmAgMnS
a	a	k8xC
majitel	majitel	k1gMnSc1
Anfield	Anfield	k1gMnSc1
založil	založit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápasy	zápas	k1gInPc4
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
kluby	klub	k1gInPc7
jsou	být	k5eAaImIp3nP
označovány	označován	k2eAgInPc1d1
jako	jako	k8xC,k8xS
Merseyside	Merseysid	k1gInSc5
derby	derby	k1gNnPc4
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
10	#num#	k4
největších	veliký	k2eAgNnPc2d3
derby	derby	k1gNnPc2
zápasů	zápas	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
během	během	k7c2
těchto	tento	k3xDgInPc2
zápasů	zápas	k1gInPc2
stala	stát	k5eAaPmAgFnS
řada	řada	k1gFnSc1
afér	aféra	k1gFnPc2
<g/>
,	,	kIx,
dle	dle	k7c2
oficiálních	oficiální	k2eAgFnPc2d1
statistik	statistika	k1gFnPc2
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
zápas	zápas	k1gInSc1
dvou	dva	k4xCgInPc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
v	v	k7c6
historii	historie	k1gFnSc6
anglické	anglický	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
bylo	být	k5eAaImAgNnS
uděleno	udělen	k2eAgNnSc1d1
nejvíce	nejvíce	k6eAd1,k6eAd3
červených	červený	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
v	v	k7c6
tom	ten	k3xDgNnSc6
posledním	poslední	k2eAgNnSc6d1
<g/>
,	,	kIx,
hraném	hraný	k2eAgNnSc6d1
v	v	k7c6
Goodison	Goodisona	k1gFnPc2
parku	park	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
rozhodčí	rozhodčí	k1gMnSc1
udělil	udělit	k5eAaPmAgMnS
2	#num#	k4
červené	červený	k2eAgFnSc2d1
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
také	také	k9
panuje	panovat	k5eAaImIp3nS
s	s	k7c7
týmem	tým	k1gInSc7
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
vše	všechen	k3xTgNnSc4
může	moct	k5eAaImIp3nS
blízkost	blízkost	k1gFnSc4
obou	dva	k4xCgNnPc2
měst	město	k1gNnPc2
a	a	k8xC
také	také	k9
úspěchy	úspěch	k1gInPc4
obou	dva	k4xCgInPc2
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
a	a	k8xC
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc1
nejúspěšnější	úspěšný	k2eAgInPc1d3
kluby	klub	k1gInPc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
fanoušků	fanoušek	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
dominoval	dominovat	k5eAaImAgInS
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
Manchester	Manchester	k1gInSc1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
a	a	k8xC
v	v	k7c6
začátcích	začátek	k1gInPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemné	vzájemný	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
jsou	být	k5eAaImIp3nP
kvůli	kvůli	k7c3
televizním	televizní	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
hrány	hrát	k5eAaImNgFnP
po	po	k7c6
obědě	oběd	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
díky	díky	k7c3
zvýšeným	zvýšený	k2eAgNnPc3d1
bezpečnostním	bezpečnostní	k2eAgNnPc3d1
rizikům	riziko	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rivalitě	rivalita	k1gFnSc6
obou	dva	k4xCgInPc2
klubů	klub	k1gInPc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
i	i	k9
dvě	dva	k4xCgFnPc1
scénky	scénka	k1gFnPc1
ve	v	k7c6
filmu	film	k1gInSc6
51	#num#	k4
<g/>
st	st	kA
State	status	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gMnSc6
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
Felix	Felix	k1gMnSc1
DeSouza	DeSouza	k1gFnSc1
<g/>
,	,	kIx,
fanatický	fanatický	k2eAgMnSc1d1
liverpoolský	liverpoolský	k2eAgMnSc1d1
fanoušek	fanoušek	k1gMnSc1
<g/>
,	,	kIx,
před	před	k7c7
vzájemným	vzájemný	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
provokativně	provokativně	k6eAd1
navštíví	navštívit	k5eAaPmIp3nS
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
hospodu	hospodu	k?
plnou	plný	k2eAgFnSc4d1
fanoušků	fanoušek	k1gMnPc2
konkurence	konkurence	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
odpálí	odpálit	k5eAaPmIp3nS
světlici	světlice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
scénka	scénka	k1gFnSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
přímo	přímo	k6eAd1
na	na	k7c6
stadionu	stadion	k1gInSc6
Anfield	Anfield	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
,	,	kIx,
přímo	přímo	k6eAd1
během	během	k7c2
utkání	utkání	k1gNnSc2
těchto	tento	k3xDgMnPc2
dvou	dva	k4xCgMnPc2
rivalů	rival	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2007	#num#	k4
klub	klub	k1gInSc1
oznámil	oznámit	k5eAaPmAgInS
záměr	záměr	k1gInSc4
spuštění	spuštění	k1gNnSc4
vysílání	vysílání	k1gNnSc2
klubové	klubový	k2eAgNnSc4d1
TV	TV	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
mohou	moct	k5eAaImIp3nP
fanoušci	fanoušek	k1gMnPc1
sledovat	sledovat	k5eAaImF
placenou	placený	k2eAgFnSc7d1
formou	forma	k1gFnSc7
jak	jak	k8xC,k8xS
prostřednictvím	prostřednictví	k1gNnSc7
internetu	internet	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
prostřednictvím	prostřednictvím	k7c2
satelitu	satelit	k1gInSc2
Setanta	Setanta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubová	klubový	k2eAgFnSc1d1
TV	TV	kA
svůj	svůj	k3xOyFgInSc4
oficiální	oficiální	k2eAgInSc4d1
provoz	provoz	k1gInSc4
spustila	spustit	k5eAaPmAgFnS
září	zář	k1gFnSc7
a	a	k8xC
kromě	kromě	k7c2
živě	živě	k6eAd1
přenášených	přenášený	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
přináší	přinášet	k5eAaImIp3nS
i	i	k9
archívní	archívní	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
rozhovory	rozhovor	k1gInPc1
<g/>
,	,	kIx,
diskuzní	diskuzní	k2eAgNnPc1d1
studia	studio	k1gNnPc1
atd.	atd.	kA
</s>
<s>
Dresy	dres	k1gInPc1
<g/>
,	,	kIx,
sponzoři	sponzor	k1gMnPc1
</s>
<s>
První	první	k4xOgFnSc7
firmou	firma	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
dodávala	dodávat	k5eAaImAgFnS
klubu	klub	k1gInSc3
dresy	dres	k1gInPc4
a	a	k8xC
vybavení	vybavení	k1gNnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1976	#num#	k4
–	–	k?
1985	#num#	k4
firma	firma	k1gFnSc1
Umbro	umbra	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
–	–	k?
1996	#num#	k4
nahradil	nahradit	k5eAaPmAgInS
německý	německý	k2eAgInSc1d1
Adidas	Adidas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1996	#num#	k4
–	–	k?
2006	#num#	k4
se	se	k3xPyFc4
o	o	k7c4
hráčské	hráčský	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
staral	starat	k5eAaImAgInS
Reebok	Reebok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
dodavatelem	dodavatel	k1gMnSc7
stal	stát	k5eAaPmAgMnS
znovu	znovu	k6eAd1
Adidas	Adidas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
však	však	k9
obléká	oblékat	k5eAaImIp3nS
dresy	dres	k1gInPc4
Warrior	Warriora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
do	do	k7c2
sezóny	sezóna	k1gFnSc2
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
dodavatelem	dodavatel	k1gMnSc7
dresů	dres	k1gInPc2
New	New	k1gMnSc4
Balance	balanc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
dodavatelem	dodavatel	k1gMnSc7
soupravy	souprava	k1gFnSc2
pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Nike	Nike	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Liverpool	Liverpool	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
prvním	první	k4xOgInSc7
britským	britský	k2eAgInSc7d1
profesionálním	profesionální	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
začal	začít	k5eAaPmAgInS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
dresu	dres	k1gInSc6
nosit	nosit	k5eAaImF
logo	logo	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
sponzora	sponzor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
firmou	firma	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
firma	firma	k1gFnSc1
Hitachi	Hitach	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
se	s	k7c7
sponzorem	sponzor	k1gMnSc7
s	s	k7c7
logem	logo	k1gNnSc7
na	na	k7c6
dresu	dres	k1gInSc6
stala	stát	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Crown	Crown	k1gMnSc1
Paints	Paints	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
firma	firma	k1gFnSc1
Candy	Canda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
dresech	dres	k1gInPc6
objevilo	objevit	k5eAaPmAgNnS
logo	logo	k1gNnSc4
dánského	dánský	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
Carlsbergu	Carlsberg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
partnerství	partnerství	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejdéle	dlouho	k6eAd3
trvající	trvající	k2eAgFnSc4d1
sponzorskou	sponzorský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
top	topit	k5eAaImRp2nS
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezony	sezona	k1gFnSc2
2010-2011	2010-2011	k4
Liverpool	Liverpool	k1gInSc1
obléká	oblékat	k5eAaImIp3nS
dresy	dres	k1gInPc4
s	s	k7c7
logem	log	k1gInSc7
banky	banka	k1gFnSc2
Standard	standard	k1gInSc1
Chartered	Chartered	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgMnPc1d1
sponzoři	sponzor	k1gMnPc1
Liverpoolu	Liverpool	k1gInSc2
pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Oficiální	oficiální	k2eAgMnSc1d1
hlavní	hlavní	k2eAgMnSc1d1
partner	partner	k1gMnSc1
<g/>
:	:	kIx,
Standard	standard	k1gInSc1
Chartered	Chartered	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgMnSc1d1
dodavatel	dodavatel	k1gMnSc1
<g/>
:	:	kIx,
Nike	Nike	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgMnPc1d1
hlavní	hlavní	k2eAgMnPc1d1
partneři	partner	k1gMnPc1
<g/>
:	:	kIx,
AXA	AXA	kA
<g/>
,	,	kIx,
Expedia	Expedium	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgMnPc1d1
partneři	partner	k1gMnPc1
<g/>
:	:	kIx,
Carlsberg	Carlsberg	k1gInSc1
<g/>
,	,	kIx,
Mauritius	Mauritius	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
XBet	XBeta	k1gFnPc2
<g/>
,	,	kIx,
MG	mg	kA
Motor	motor	k1gInSc1
UK	UK	kA
<g/>
,	,	kIx,
Hollyfrontier	Hollyfrontier	k1gMnSc1
<g/>
,	,	kIx,
TigerWit	TigerWit	k1gMnSc1
UK	UK	kA
<g/>
,	,	kIx,
TRIBUS	TRIBUS	kA
<g/>
,	,	kIx,
EA	EA	kA
Sports	Sports	k1gInSc1
<g/>
,	,	kIx,
Quorn	Quorn	k1gInSc1
<g/>
,	,	kIx,
Acronis	Acronis	k1gInSc1
<g/>
,	,	kIx,
Vodafone	Vodafon	k1gInSc5
<g/>
,	,	kIx,
NH	NH	kA
Foods	Foods	k1gInSc1
<g/>
,	,	kIx,
lugis	lugis	k1gFnSc1
<g/>
,	,	kIx,
Joie	Joie	k1gFnSc1
<g/>
,	,	kIx,
Reducose	Reducosa	k1gFnSc6
<g/>
,	,	kIx,
Mitel	Mitel	k1gInSc1
<g/>
,	,	kIx,
Levi	Levi	k1gNnSc1
<g/>
´	´	k?
<g/>
s	s	k7c7
<g/>
,	,	kIx,
Alexbank	Alexbank	k1gInSc1
<g/>
,	,	kIx,
Extra	extra	k2eAgInSc1d1
Joss	Joss	k1gInSc1
<g/>
,	,	kIx,
Verbier	Verbier	k1gInSc1
</s>
<s>
Barvy	barva	k1gFnPc1
a	a	k8xC
logo	logo	k1gNnSc1
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1
klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
Liverpoolu	Liverpool	k1gInSc2
FC	FC	kA
jsou	být	k5eAaImIp3nP
červená	červený	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
klubové	klubový	k2eAgFnSc2d1
historie	historie	k1gFnSc2
hrál	hrát	k5eAaImAgInS
klub	klub	k1gInSc1
v	v	k7c6
modrých	modrý	k2eAgInPc6d1
a	a	k8xC
bílých	bílý	k2eAgInPc6d1
dresech	dres	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
hrál	hrát	k5eAaImAgInS
tým	tým	k1gInSc1
v	v	k7c6
červených	červený	k2eAgFnPc6d1
košilích	košile	k1gFnPc6
a	a	k8xC
bílých	bílý	k2eAgFnPc6d1
kalhotách	kalhoty	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
ponožek	ponožka	k1gFnPc2
neustále	neustále	k6eAd1
je	on	k3xPp3gFnPc4
střídaly	střídat	k5eAaImAgFnP
<g/>
,	,	kIx,
proto	proto	k8xC
hráči	hráč	k1gMnPc1
jednou	jednou	k6eAd1
hrály	hrát	k5eAaImAgInP
s	s	k7c7
červenými	červený	k2eAgFnPc7d1
ponožkami	ponožka	k1gFnPc7
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
zase	zase	k9
s	s	k7c7
bílými	bílý	k2eAgFnPc7d1
a	a	k8xC
občas	občas	k6eAd1
zase	zase	k9
s	s	k7c7
černými	černý	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
třetí	třetí	k4xOgFnSc1
souprava	souprava	k1gFnSc1
ve	v	k7c6
žlutém	žlutý	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
zejména	zejména	k9
na	na	k7c4
zápasy	zápas	k1gInPc4
venku	venku	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
soupeř	soupeř	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
červeno-bílé	červeno-bílý	k2eAgFnSc6d1
(	(	kIx(
<g/>
např.	např.	kA
Sunderland	Sunderland	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
na	na	k7c6
několik	několik	k4yIc4
let	léto	k1gNnPc2
přešel	přejít	k5eAaPmAgInS
náhradní	náhradní	k2eAgInSc1d1
stojan	stojan	k1gInSc1
v	v	k7c6
šedé	šedý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
a	a	k8xC
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
existovaly	existovat	k5eAaImAgInP
náhradní	náhradní	k2eAgInPc4d1
regály	regál	k1gInPc4
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
ze	z	k7c2
zelených	zelený	k2eAgFnPc2d1
košil	košile	k1gFnPc2
a	a	k8xC
bílých	bílý	k2eAgFnPc2d1
kalhot	kalhoty	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
tým	tým	k1gInSc1
používá	používat	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
typické	typický	k2eAgInPc4d1
červené	červený	k2eAgInPc4d1
dresy	dres	k1gInPc4
s	s	k7c7
bílými	bílý	k2eAgInPc7d1
a	a	k8xC
tyrkysovými	tyrkysový	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
na	na	k7c6
rukávech	rukáv	k1gInPc6
dresů	dres	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
šortkách	šortky	k1gFnPc6
i	i	k8xC
na	na	k7c6
štulpnách	štulpna	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c4
domácí	domácí	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Venkovní	venkovní	k2eAgInPc4d1
dresy	dres	k1gInPc4
týmu	tým	k1gInSc2
jsou	být	k5eAaImIp3nP
tyrkysové	tyrkysový	k2eAgInPc1d1
s	s	k7c7
černými	černý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
na	na	k7c6
rukávech	rukáv	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c6
šortkách	šortky	k1gFnPc6
i	i	k8xC
na	na	k7c6
štulpnách	štulpna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
sada	sada	k1gFnSc1
dresů	dres	k1gInPc2
je	být	k5eAaImIp3nS
černo-šedá	černo-šedat	k5eAaImIp3nS
se	se	k3xPyFc4
šachovnicovým	šachovnicový	k2eAgInSc7d1
stylem	styl	k1gInSc7
a	a	k8xC
červenými	červený	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
na	na	k7c6
bocích	bok	k1gInPc6
dresu	dres	k1gInSc2
a	a	k8xC
na	na	k7c6
štulpnách	štulpna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Klubový	klubový	k2eAgInSc1d1
odznak	odznak	k1gInSc1
Liverpoolu	Liverpool	k1gInSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
štítu	štít	k1gInSc2
s	s	k7c7
textem	text	k1gInSc7
Liverpool	Liverpool	k1gInSc1
Football	Footballa	k1gFnPc2
Club	club	k1gInSc1
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
obraz	obraz	k1gInSc1
takzvaného	takzvaný	k2eAgMnSc2d1
"	"	kIx"
<g/>
liver	livra	k1gFnPc2
bird	bird	k1gInSc1
<g/>
"	"	kIx"
–	–	k?
mytického	mytický	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
<g/>
,	,	kIx,
symbolu	symbol	k1gInSc6
města	město	k1gNnSc2
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
skutečných	skutečný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
podobný	podobný	k2eAgInSc4d1
kormoránovi	kormorán	k1gMnSc6
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
štítem	štít	k1gInSc7
je	být	k5eAaImIp3nS
text	text	k1gInSc1
You	You	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ll	ll	k?
never	never	k1gInSc1
walk	walk	k6eAd1
alone	alonout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
česky	česky	k6eAd1
Už	už	k6eAd1
nikdy	nikdy	k6eAd1
nebudeš	být	k5eNaImBp2nS
chodit	chodit	k5eAaImF
sám	sám	k3xTgInSc1
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
se	s	k7c7
železnými	železný	k2eAgInPc7d1
uměleckými	umělecký	k2eAgInPc7d1
díly	díl	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Shankly	Shankly	k1gMnSc1
Gates	Gates	k1gMnSc1
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
vchodů	vchod	k1gInPc2
do	do	k7c2
Anfieldu	Anfield	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
štítu	štít	k1gInSc2
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc4
plameny	plamen	k1gInPc4
symbolizující	symbolizující	k2eAgInSc4d1
Památník	památník	k1gInSc4
Hillsborough	Hillsborougha	k1gFnPc2
<g/>
,	,	kIx,
pamětní	pamětní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
před	před	k7c7
Anfieldem	Anfield	k1gInSc7
na	na	k7c4
Shanklyho	Shankly	k1gMnSc4
bráně	brána	k1gFnSc6
<g/>
,	,	kIx,
postavené	postavený	k2eAgFnSc6d1
na	na	k7c4
počest	počest	k1gFnSc4
96	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zahynuli	zahynout	k5eAaPmAgMnP
při	při	k7c6
nehodě	nehoda	k1gFnSc6
Hillsborough	Hillsborougha	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
starší	starý	k2eAgFnSc4d2
variantu	varianta	k1gFnSc4
klubové	klubový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
na	na	k7c6
košilové	košilový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
,	,	kIx,
místo	místo	k7c2
obvyklého	obvyklý	k2eAgInSc2d1
štítu	štít	k1gInSc2
měl	mít	k5eAaImAgInS
žlutě	žlutě	k6eAd1
/	/	kIx~
zlatě	zlatě	k6eAd1
zbarveného	zbarvený	k2eAgMnSc2d1
jaterního	jaterní	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
s	s	k7c7
textem	text	k1gInSc7
„	„	k?
<g/>
LFC	LFC	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Gordon	Gordon	k1gMnSc1
Hodgson	Hodgson	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albert	Albert	k1gMnSc1
Stubbins	Stubbinsa	k1gFnPc2
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gerry	Gerra	k1gFnPc1
Byrne	Byrn	k1gInSc5
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roger	Roger	k1gInSc1
Hunt	hunt	k1gInSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ian	Ian	k?
Callaghan	Callaghan	k1gInSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gordon	Gordon	k1gMnSc1
Milne	Miln	k1gInSc5
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Chris	Chris	k1gInSc1
Lawler	Lawler	k1gInSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tommy	Tomm	k1gInPc1
Smith	Smitha	k1gFnPc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Peter	Peter	k1gMnSc1
Thompson	Thompson	k1gMnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ray	Ray	k?
Clemence	Clemence	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Emlyn	Emlyn	k1gMnSc1
Hughes	Hughes	k1gMnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alec	Alec	k1gFnSc1
Lindsay	Lindsaa	k1gFnSc2
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kevin	Kevin	k1gMnSc1
Keegan	Keegan	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Phil	Phil	k1gMnSc1
Thompson	Thompson	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jimmy	Jimm	k1gInPc1
Case	Cas	k1gFnSc2
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ray	Ray	k?
Kennedy	Kenned	k1gMnPc7
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Terry	Terr	k1gInPc1
McDermott	McDermotta	k1gFnPc2
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Phil	Phil	k1gMnSc1
Neal	Neal	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alan	Alan	k1gMnSc1
Kennedy	Kenneda	k1gMnSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Steve	Steve	k1gMnSc1
McMahon	McMahon	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Peter	Peter	k1gMnSc1
Beardsley	Beardslea	k1gFnSc2
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Barnes	Barnes	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Steve	Steve	k1gMnSc1
McManaman	McManaman	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Robbie	Robbie	k1gFnSc1
Fowler	Fowler	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jamie	Jamie	k1gFnSc1
Carragher	Carraghra	k1gFnPc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Steven	Steven	k2eAgInSc1d1
Gerrard	Gerrard	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Daniel	Daniel	k1gMnSc1
Sturridge	Sturridg	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Billy	Bill	k1gMnPc4
Liddell	Liddella	k1gFnPc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ian	Ian	k?
St	St	kA
John	John	k1gMnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ron	ron	k1gInSc1
Yeats	Yeats	k1gInSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Willie	Willie	k1gFnSc1
Stevenson	Stevensona	k1gFnPc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kenny	Kenn	k1gInPc1
Dalglish	Dalglisha	k1gFnPc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alan	Alan	k1gMnSc1
Hansen	Hansna	k1gFnPc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Graeme	Graat	k5eAaBmIp1nP,k5eAaImIp1nP,k5eAaPmIp1nP
Souness	Souness	k1gInSc4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Steve	Steve	k1gMnSc1
Nicol	Nicola	k1gFnPc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Toshack	Toshack	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ian	Ian	k?
Rush	Rush	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Elisha	Elisha	k1gMnSc1
Scott	Scott	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Steve	Steve	k1gMnSc1
Heighway	Heighwaa	k1gFnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ronnie	Ronnie	k1gFnSc1
Whelan	Whelana	k1gFnPc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mark	Mark	k1gMnSc1
Lawrenson	Lawrenson	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Aldridge	Aldridg	k1gFnSc2
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Xabi	Xab	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fernando	Fernando	k6eAd1
Torres	Torres	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dietmar	Dietmar	k1gMnSc1
Hamann	Hamann	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stéphane	Stéphanout	k5eAaPmIp3nS
Henchoz	Henchoz	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Mø	Mø	k1gFnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sami	sám	k3xTgMnPc1
Hyypiä	Hyypiä	k1gMnPc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Šmicer	Šmicer	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Patrik	Patrik	k1gMnSc1
Berger	Berger	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdroje	zdroj	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
domácí	domácí	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
/	/	kIx~
Premier	Premier	k1gInSc1
League	Leagu	k1gInSc2
(	(	kIx(
19	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
FA	fa	k1gNnSc1
Cup	cup	k1gInSc1
(	(	kIx(
7	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
EFL	EFL	kA
Cup	cup	k1gInSc1
(	(	kIx(
8	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Community	Communit	k1gInPc1
Shield	Shielda	k1gFnPc2
(	(	kIx(
15	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1964	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
(	(	kIx(
<g/>
*	*	kIx~
hvězdičkou	hvězdička	k1gFnSc7
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
dělené	dělený	k2eAgInPc1d1
tituly	titul	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
6	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
3	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
4	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1977	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
:	:	kIx,
Lancashire	Lancashir	k1gMnSc5
League	Leaguus	k1gMnSc5
</s>
<s>
1893	#num#	k4
<g/>
–	–	k?
<g/>
1894	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1895	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1896	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1904	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1905	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
1892	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Lancashire	Lancashir	k1gMnSc5
League	Leaguus	k1gMnSc5
</s>
<s>
–	–	k?
<g/>
2217236619	#num#	k4
<g/>
+	+	kIx~
<g/>
4736	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
22822607718	#num#	k4
<g/>
+	+	kIx~
<g/>
5950	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1894	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13078155170-1922	13078155170-1922	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1895	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
230222610632	#num#	k4
<g/>
+	+	kIx~
<g/>
7446	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1896	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13012994638	#num#	k4
<g/>
+	+	kIx~
<g/>
833	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
130116134845	#num#	k4
<g/>
+	+	kIx~
<g/>
328	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
134195104933	#num#	k4
<g/>
+	+	kIx~
<g/>
1643	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
134145154945	#num#	k4
<g/>
+	+	kIx~
<g/>
433	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13419785935	#num#	k4
<g/>
+	+	kIx~
<g/>
2445	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1341012124238	#num#	k4
<g/>
+	+	kIx~
<g/>
432	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
134174136849	#num#	k4
<g/>
+	+	kIx~
<g/>
1938	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1903	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13498174962-1326	13498174962-1326	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23427439325	#num#	k4
<g/>
+	+	kIx~
<g/>
6858	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138235107946	#num#	k4
<g/>
+	+	kIx~
<g/>
3351	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138137186465-133	138137186465-133	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138166166861	#num#	k4
<g/>
+	+	kIx~
<g/>
738	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138156175765-836	138156175765-836	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138216117857	#num#	k4
<g/>
+	+	kIx~
<g/>
2148	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138157165353037	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381210164955-634	1381210164955-634	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138165176171-1037	138165176171-1037	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138147174662-1635	138147174662-1635	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138149156575-1037	138149156575-1037	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1915	#num#	k4
až	až	k8xS
1918	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421910135944	#num#	k4
<g/>
+	+	kIx~
<g/>
1548	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142181596335	#num#	k4
<g/>
+	+	kIx~
<g/>
2851	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221376336	#num#	k4
<g/>
+	+	kIx~
<g/>
2757	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14226887031	#num#	k4
<g/>
+	+	kIx~
<g/>
3960	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421511164948	#num#	k4
<g/>
+	+	kIx~
<g/>
141	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422010126355	#num#	k4
<g/>
+	+	kIx~
<g/>
850	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142146127063	#num#	k4
<g/>
+	+	kIx~
<g/>
744	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142187176961	#num#	k4
<g/>
+	+	kIx~
<g/>
843	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421313168487-339	1421313168487-339	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421712139064	#num#	k4
<g/>
+	+	kIx~
<g/>
2646	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142169176379-1641	142169176379-1641	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421512158685	#num#	k4
<g/>
+	+	kIx~
<g/>
142	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142196178193-1244	142196178193-1244	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421411177984-539	1421411177984-539	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421410187987-838	1421410187987-838	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142197168588-345	142197168588-345	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421312176064-438	1421312176064-438	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421211196284-2235	1421211196284-2235	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421511166571-641	1421511166571-641	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421414146263-142	1421414146263-142	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
až	až	k8xS
1945	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142257108452	#num#	k4
<g/>
+	+	kIx~
<g/>
3257	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421610166561	#num#	k4
<g/>
+	+	kIx~
<g/>
442	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421314155343	#num#	k4
<g/>
+	+	kIx~
<g/>
1040	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421714116454	#num#	k4
<g/>
+	+	kIx~
<g/>
1048	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421611155359-643	1421611155359-643	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421219115761-443	1421219115761-443	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142148206182-2136	142148206182-2136	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142910236897-2928	142910236897-2928	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421610169296-442	2421610169296-442	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242216158563	#num#	k4
<g/>
+	+	kIx~
<g/>
2248	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2422111108254	#num#	k4
<g/>
+	+	kIx~
<g/>
2853	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2422210107954	#num#	k4
<g/>
+	+	kIx~
<g/>
2554	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242245138762	#num#	k4
<g/>
+	+	kIx~
<g/>
2553	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2422010129066	#num#	k4
<g/>
+	+	kIx~
<g/>
2450	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2422110118758	#num#	k4
<g/>
+	+	kIx~
<g/>
2952	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
24227879943	#num#	k4
<g/>
+	+	kIx~
<g/>
5662	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421710157159	#num#	k4
<g/>
+	+	kIx~
<g/>
1244	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142265119245	#num#	k4
<g/>
+	+	kIx~
<g/>
4757	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421710156773-644	1421710156773-644	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14226977934	#num#	k4
<g/>
+	+	kIx~
<g/>
4561	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421913106447	#num#	k4
<g/>
+	+	kIx~
<g/>
1751	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221197140	#num#	k4
<g/>
+	+	kIx~
<g/>
3155	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142251166324	#num#	k4
<g/>
+	+	kIx~
<g/>
3961	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422011116542	#num#	k4
<g/>
+	+	kIx~
<g/>
2351	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142171784224	#num#	k4
<g/>
+	+	kIx~
<g/>
1851	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14224996430	#num#	k4
<g/>
+	+	kIx~
<g/>
3057	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142251077242	#num#	k4
<g/>
+	+	kIx~
<g/>
3060	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221375231	#num#	k4
<g/>
+	+	kIx~
<g/>
2157	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422011116039	#num#	k4
<g/>
+	+	kIx~
<g/>
2151	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142231456631	#num#	k4
<g/>
+	+	kIx~
<g/>
3560	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142231186233	#num#	k4
<g/>
+	+	kIx~
<g/>
2957	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14224996534	#num#	k4
<g/>
+	+	kIx~
<g/>
3157	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14230848516	#num#	k4
<g/>
+	+	kIx~
<g/>
6968	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142251078130	#num#	k4
<g/>
+	+	kIx~
<g/>
5160	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142171786242	#num#	k4
<g/>
+	+	kIx~
<g/>
2051	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14226978032	#num#	k4
<g/>
+	+	kIx~
<g/>
4887	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142241088737	#num#	k4
<g/>
+	+	kIx~
<g/>
5082	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221467332	#num#	k4
<g/>
+	+	kIx~
<g/>
4180	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221196835	#num#	k4
<g/>
+	+	kIx~
<g/>
3377	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142261068937	#num#	k4
<g/>
+	+	kIx~
<g/>
5288	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142238117242	#num#	k4
<g/>
+	+	kIx~
<g/>
3077	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
140261228724	#num#	k4
<g/>
+	+	kIx~
<g/>
6390	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138221066528	#num#	k4
<g/>
+	+	kIx~
<g/>
3776	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138231057837	#num#	k4
<g/>
+	+	kIx~
<g/>
4179	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13823787740	#num#	k4
<g/>
+	+	kIx~
<g/>
3776	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421616104740	#num#	k4
<g/>
+	+	kIx~
<g/>
764	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1421611156255	#num#	k4
<g/>
+	+	kIx~
<g/>
759	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
142179165955	#num#	k4
<g/>
+	+	kIx~
<g/>
460	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1422111106537	#num#	k4
<g/>
+	+	kIx~
<g/>
2874	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138201177034	#num#	k4
<g/>
+	+	kIx~
<g/>
3671	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138191186237	#num#	k4
<g/>
+	+	kIx~
<g/>
2568	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138181196842	#num#	k4
<g/>
+	+	kIx~
<g/>
2665	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138159146849	#num#	k4
<g/>
+	+	kIx~
<g/>
1954	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138191095130	#num#	k4
<g/>
+	+	kIx~
<g/>
2167	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13820997139	#num#	k4
<g/>
+	+	kIx~
<g/>
3269	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13824866730	#num#	k4
<g/>
+	+	kIx~
<g/>
3780	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381810106141	#num#	k4
<g/>
+	+	kIx~
<g/>
2064	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381612105537	#num#	k4
<g/>
+	+	kIx~
<g/>
1860	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138177145241	#num#	k4
<g/>
+	+	kIx~
<g/>
1158	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825765725	#num#	k4
<g/>
+	+	kIx~
<g/>
3282	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138208105727	#num#	k4
<g/>
+	+	kIx~
<g/>
3068	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138211346728	#num#	k4
<g/>
+	+	kIx~
<g/>
3976	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138251127727	#num#	k4
<g/>
+	+	kIx~
<g/>
5086	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138189116135	#num#	k4
<g/>
+	+	kIx~
<g/>
2663	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138177145944	#num#	k4
<g/>
+	+	kIx~
<g/>
1558	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381410144740	#num#	k4
<g/>
+	+	kIx~
<g/>
752	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138161397143	#num#	k4
<g/>
+	+	kIx~
<g/>
2861	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138266610150	#num#	k4
<g/>
+	+	kIx~
<g/>
5184	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138188125248	#num#	k4
<g/>
+	+	kIx~
<g/>
462	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
1381612106350	#num#	k4
<g/>
+	+	kIx~
<g/>
1360	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138221067842	#num#	k4
<g/>
+	+	kIx~
<g/>
3676	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138211258438	#num#	k4
<g/>
+	+	kIx~
<g/>
4675	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13830718922	#num#	k4
<g/>
+	+	kIx~
<g/>
6797	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13832338533	#num#	k4
<g/>
+	+	kIx~
<g/>
5299	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138	#num#	k4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
:	:	kIx,
Klub	klub	k1gInSc1
uspěl	uspět	k5eAaPmAgMnS
v	v	k7c6
hlasování	hlasování	k1gNnSc6
o	o	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
Football	Footballa	k1gFnPc2
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
rozšíření	rozšíření	k1gNnSc1
Second	Seconda	k1gFnPc2
Division	Division	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Venku	venku	k6eAd1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
KR	KR	kA
Reykjavík	Reykjavík	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
RSC	RSC	kA
Anderlecht	Anderlechtum	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Köln	Köln	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Standard	standard	k1gInSc1
Liè	Liè	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Budapest	Budapest	k1gInSc1
Honvéd	honvéd	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Petrolul	Petrolul	k1gInSc1
Ploieşti	Ploieşti	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Malmö	Malmö	k1gFnPc2
FF	ff	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
TSV	TSV	kA
1860	#num#	k4
München	München	k1gInSc1
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Ferencvárosi	Ferencvárose	k1gFnSc3
TC	tc	k0
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dundalk	Dundalka	k1gFnPc2
FC	FC	kA
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vitória	Vitórium	k1gNnSc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Ferencvárosi	Ferencvárose	k1gFnSc3
TC	tc	k0
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Bucureşti	Bucureşti	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hibernian	Hiberniana	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
AFC	AFC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Servette	Servett	k1gInSc5
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Eintracht	Eintracht	k2eAgInSc1d1
Frankfurt	Frankfurt	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AEK	AEK	kA
Athény	Athéna	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Berliner	Berlinra	k1gFnPc2
FC	FC	kA
Dynamo	dynamo	k1gNnSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Dynamo	dynamo	k1gNnSc4
Dresden	Dresdna	k1gFnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Mönchengladbach	Mönchengladbach	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Jeunesse	Jeunesse	k1gFnSc2
Esch	Esch	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
Bělehrad	Bělehrad	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Strø	Strø	k1gFnPc2
IF	IF	kA
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Ferencvárosi	Ferencvárose	k1gFnSc3
TC	tc	k0
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hibernian	Hiberniana	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Śląsk	Śląsk	k1gInSc1
Wrocław	Wrocław	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Dynamo	dynamo	k1gNnSc4
Dresden	Dresdna	k1gFnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Crusaders	Crusadersa	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Trabzonspor	Trabzonspora	k1gFnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k9
Saint-Étienne	Saint-Étienn	k1gInSc5
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Zürich	Zürich	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Mönchengladbach	Mönchengladbacha	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Hamburger	hamburger	k1gInSc1
SV	sv	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dynamo	dynamo	k1gNnSc1
Dresden	Dresdno	k1gNnPc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Mönchengladbach	Mönchengladbach	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1978	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
RSC	RSC	kA
Anderlecht	Anderlecht	k1gMnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gMnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Tbilisi	Tbilisi	k1gNnSc6
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Oulun	Oulun	k1gInSc1
Palloseura	Palloseura	k1gFnSc1
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
111	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Aberdeen	Aberdena	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1981	#num#	k4
<g/>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohárFinále	pohárFinále	k1gNnSc1
Clube	club	k1gInSc5
de	de	k?
Regatas	Regatasa	k1gFnPc2
do	do	k7c2
Flamengo	flamengo	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Oulun	Oulun	k1gInSc1
Palloseura	Palloseura	k1gFnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AZ	AZ	kA
Alkmaar	Alkmaar	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dundalk	Dundalka	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
HJK	HJK	kA
Helsinki	Helsink	k1gFnSc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Widzew	Widzew	k1gFnSc2
Łódź	Łódź	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Odense	Odense	k1gFnSc2
BK	BK	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Bucureşti	Bucureşti	k1gNnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
Interkontinentální	interkontinentální	k2eAgFnSc6d1
pohárFinále	pohárFinála	k1gFnSc6
CA	ca	kA
Independiente	Independient	k1gMnSc5
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Lech	Lech	k1gMnSc1
Poznań	Poznań	k1gMnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Panathinaikos	Panathinaikosa	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Kuusysi	Kuusys	k1gMnSc3
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AJ	aj	k0
Auxerre	Auxerr	k1gInSc5
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Swarovski	Swarovski	k1gNnSc4
Tirol	Tirol	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Genoa	Geno	k1gInSc2
CFC	CFC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Apollon	Apollon	k1gMnSc1
Limassol	Limassol	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
Alanija	Alanija	k1gFnSc1
Vladikavkaz	Vladikavkaz	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Brø	Brø	k1gFnSc2
IF	IF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Myllykosken	Myllykosken	k1gInSc1
Pallo-	Pallo-	k1gFnSc1
<g/>
473	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Sion	Siono	k1gNnPc2
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SK	Sk	kA
Brann	Brann	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
RC	RC	kA
Strasbourg	Strasbourg	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Košice	Košice	k1gInPc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gNnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Rapid	rapid	k1gInSc1
Bucureşti	Bucureşti	k1gNnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Olympiacos	Olympiacosa	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Deportivo	Deportiva	k1gFnSc5
Alavés	Alavés	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
po	po	k7c4
prod	prod	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FC	FC	kA
Haka	Hak	k1gInSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Boavista	Boavista	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18931	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vitesse	Vitesse	k1gFnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AJ	aj	k0
Auxerre	Auxerr	k1gInSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
NK	NK	kA
Olimpija	Olimpijus	k1gMnSc2
Ljubljana	Ljubljan	k1gMnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
Levski	Levske	k1gFnSc4
Sofia	Sofia	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Olympique	Olympiqu	k1gInSc2
de	de	k?
Marseille	Marseille	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Grazer	Grazra	k1gFnPc2
AK	AK	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
AS	as	k9
Monaco	Monaco	k6eAd1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Olympiacos	Olympiacos	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc6
Coruñ	Coruñ	k1gInSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Deportivo	Deportiva	k1gFnSc5
Saprissa	Sapriss	k1gMnSc4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FBK	FBK	kA
Kaunas	Kaunas	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
Real	Real	k1gInSc1
Betis	Betis	k1gFnSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc4
Maccabi	Maccab	k1gFnSc2
Haifa	Haifa	k1gFnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
FC	FC	kA
Girondins	Girondins	k1gInSc1
de	de	k?
Bordeaux	Bordeaux	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Toulouse	Toulouse	k1gInSc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Beşiktaş	Beşiktaş	k?
JK	JK	kA
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Standard	standard	k1gInSc1
Liè	Liè	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Debreceni	Debrecen	k2eAgMnPc1d1
VSC	VSC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FC	FC	kA
Unirea	Unire	k1gInSc2
Urziceni	Urzicen	k2eAgMnPc1d1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Lille	Lille	k1gFnSc2
OSC	OSC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FK	FK	kA
Rabotnički	Rabotničk	k1gFnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc4
Trabzonspor	Trabzonspora	k1gFnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
K	k	k7c3
</s>
<s>
FC	FC	kA
Steaua	Steaua	k1gFnSc1
Bucureşti	Bucureşť	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Utrecht	Utrecht	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
SSC	SSC	kA
Napoli	Napole	k1gFnSc6
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
SC	SC	kA
Braga	Braga	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FK	FK	kA
Gomel	Gomel	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Heart	Hearta	k1gFnPc2
of	of	k?
Midlothian	Midlothiana	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
BSC	BSC	kA
Young	Young	k1gInSc1
Boys	boy	k1gMnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Udinese	Udinést	k5eAaPmIp3nS
Calcio	Calcio	k6eAd1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Anži	Anž	k1gMnPc1
Machačkala	Machačkala	k1gFnSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FK	FK	kA
Zenit	zenit	k1gInSc1
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18931	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
PFK	PFK	kA
Ludogorec	Ludogorec	k1gInSc1
Razgrad	Razgrad	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
Beşiktaş	Beşiktaş	k1gFnPc2
JK	JK	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
FC	FC	kA
Girondins	Girondins	k1gInSc1
de	de	k?
Bordeaux	Bordeaux	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Sion	Sion	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FC	FC	kA
Augsburg	Augsburg	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
TSG	TSG	kA
1899	#num#	k4
Hoffenheim	Hoffenheim	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
NK	NK	kA
Maribor	Maribor	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Crvena	Crvena	k1gFnSc1
zvezda	zvezda	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
Chelsea	Chelse	k1gInSc2
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
</s>
<s>
FC	FC	kA
Red	Red	k1gFnSc1
Bull	bulla	k1gFnPc2
Salzburg	Salzburg	k1gInSc4
</s>
<s>
KRC	KRC	kA
Genk	Genk	k1gMnSc1
</s>
<s>
Trenéři	trenér	k1gMnPc1
</s>
<s>
John	John	k1gMnSc1
McKenna	McKenn	k1gMnSc2
/	/	kIx~
William	William	k1gInSc1
Barclay	Barclaa	k1gFnSc2
1892-1896	1892-1896	k4
</s>
<s>
Tom	Tom	k1gMnSc1
Watson	Watson	k1gMnSc1
1896-1915	1896-1915	k4
</s>
<s>
bez	bez	k7c2
manažera	manažer	k1gMnSc2
1915-1920	1915-1920	k4
</s>
<s>
David	David	k1gMnSc1
Ashworth	Ashworth	k1gMnSc1
1920-1923	1920-1923	k4
</s>
<s>
Matt	Matt	k2eAgInSc1d1
McQueen	McQueen	k1gInSc1
1923-1928	1923-1928	k4
</s>
<s>
George	Georg	k1gFnPc1
Patterson	Patterson	k1gNnSc1
1928-1936	1928-1936	k4
</s>
<s>
George	Georg	k1gMnSc4
Kay	Kay	k1gMnSc4
1936-1950	1936-1950	k4
</s>
<s>
Don	Don	k1gMnSc1
Welsh	Welsh	k1gMnSc1
1951-1956	1951-1956	k4
</s>
<s>
Phil	Phil	k1gMnSc1
Taylor	Taylor	k1gMnSc1
1956-1959	1956-1959	k4
</s>
<s>
Bill	Bill	k1gMnSc1
Shankly	Shankly	k1gMnSc1
1959-1974	1959-1974	k4
</s>
<s>
Bob	Bob	k1gMnSc1
Paisley	Paislea	k1gFnSc2
1974-1983	1974-1983	k4
</s>
<s>
Joe	Joe	k?
Fagan	Fagan	k?
1983-1985	1983-1985	k4
</s>
<s>
Kenny	Kenn	k1gInPc1
Dalglish	Dalglish	k1gInSc1
1985-1991	1985-1991	k4
</s>
<s>
Graeme	Graout	k5eAaPmIp1nP,k5eAaImIp1nP,k5eAaBmIp1nP
Souness	Souness	k1gInSc4
1991-1994	1991-1994	k4
</s>
<s>
Roy	Roy	k1gMnSc1
Evans	Evans	k1gInSc4
1994-1998	1994-1998	k4
</s>
<s>
Roy	Roy	k1gMnSc1
Evans	Evans	k1gInSc1
/	/	kIx~
Gérard	Gérard	k1gMnSc1
Houllier	Houllier	k1gMnSc1
1998	#num#	k4
</s>
<s>
Gérard	Gérard	k1gMnSc1
Houllier	Houllier	k1gMnSc1
1998-2004	1998-2004	k4
</s>
<s>
Rafael	Rafael	k1gMnSc1
Benítez	Benítez	k1gMnSc1
2004-2010	2004-2010	k4
</s>
<s>
Roy	Roy	k1gMnSc1
Hodgson	Hodgson	k1gMnSc1
2010-2011	2010-2011	k4
</s>
<s>
Kenny	Kenn	k1gInPc1
Dalglish	Dalglish	k1gInSc1
2011-2012	2011-2012	k4
</s>
<s>
Brendan	Brendan	k1gInSc1
Rodgers	Rodgers	k1gInSc1
2012-2015	2012-2015	k4
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc1
2015-	2015-	k4
</s>
<s>
Klubové	klubový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
zápasů	zápas	k1gInPc2
za	za	k7c4
hlavní	hlavní	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Callaghan	Callaghan	k1gMnSc1
(	(	kIx(
<g/>
855	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
zápasů	zápas	k1gInPc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Callaghan	Callaghan	k1gMnSc1
(	(	kIx(
<g/>
640	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
zápasů	zápas	k1gInPc2
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Callaghan	Callaghan	k1gMnSc1
(	(	kIx(
<g/>
79	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
zápasů	zápas	k1gInPc2
v	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
78	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
zápasů	zápas	k1gInPc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
:	:	kIx,
Jamie	Jamie	k1gFnSc1
Carragher	Carraghra	k1gFnPc2
(	(	kIx(
<g/>
91	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
<g/>
:	:	kIx,
Ted	Ted	k1gMnSc1
Doig	Doig	k1gMnSc1
<g/>
,	,	kIx,
41	#num#	k4
let	léto	k1gNnPc2
&	&	k?
165	#num#	k4
dní	den	k1gInPc2
v	v	k7c6
zápase	zápas	k1gInSc6
venku	venku	k6eAd1
s	s	k7c7
Newcastle	Newcastle	k1gFnSc7
United	United	k1gMnSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1908	#num#	k4
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
<g/>
:	:	kIx,
Jerome	Jerom	k1gInSc5
Sinclair	Sinclair	k1gMnSc1
16	#num#	k4
let	léto	k1gNnPc2
&	&	k?
6	#num#	k4
dní	den	k1gInPc2
v	v	k7c6
zápase	zápas	k1gInSc6
venku	venek	k1gInSc2
proti	proti	k7c3
West	West	k1gMnSc1
Bromwich	Bromwich	k1gMnSc1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
zápasů	zápas	k1gInPc2
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
:	:	kIx,
Phil	Phil	k1gMnSc1
Neal	Neal	k1gMnSc1
(	(	kIx(
<g/>
417	#num#	k4
<g/>
)	)	kIx)
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
až	až	k9
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
</s>
<s>
Nejdéle	dlouho	k6eAd3
sloužící	sloužící	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
:	:	kIx,
Elisha	Elisha	k1gMnSc1
Scott	Scott	k1gMnSc1
–	–	k?
21	#num#	k4
let	léto	k1gNnPc2
&	&	k?
52	#num#	k4
dní	den	k1gInPc2
–	–	k?
sezóna	sezóna	k1gFnSc1
1913	#num#	k4
až	až	k9
1934	#num#	k4
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
debutant	debutant	k1gMnSc1
<g/>
:	:	kIx,
Ted	Ted	k1gMnSc1
Doig	Doig	k1gMnSc1
v	v	k7c6
37	#num#	k4
letech	let	k1gInPc6
&	&	k?
307	#num#	k4
dnech	den	k1gInPc6
doma	doma	k6eAd1
proti	proti	k7c3
Burton	Burton	k1gInSc4
United	United	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1904	#num#	k4
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
hlavním	hlavní	k2eAgInSc6d1
týmu	tým	k1gInSc6
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
346	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
:	:	kIx,
Roger	Roger	k1gInSc1
Hunt	hunt	k1gInSc1
(	(	kIx(
<g/>
245	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
39	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
48	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
:	:	kIx,
Steven	Steven	k2eAgInSc1d1
Gerrard	Gerrard	k1gInSc1
(	(	kIx(
<g/>
41	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlépe	dobře	k6eAd3
střílející	střílející	k2eAgMnSc1d1
náhradník	náhradník	k1gMnSc1
<g/>
:	:	kIx,
David	David	k1gMnSc1
Fairclough	Fairclough	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
hattriků	hattrik	k1gMnPc2
<g/>
:	:	kIx,
Gordon	Gordon	k1gMnSc1
Hodgson	Hodgson	k1gMnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
hattriků	hattrik	k1gMnPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
<g/>
:	:	kIx,
Roger	Roger	k1gInSc1
Hunt	hunt	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
proměněných	proměněný	k2eAgFnPc2d1
penalt	penalta	k1gFnPc2
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Molby	Molba	k1gMnSc2
(	(	kIx(
<g/>
42	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
zápasů	zápas	k1gInPc2
bez	bez	k7c2
vstřelení	vstřelení	k1gNnSc2
gólu	gól	k1gInSc2
<g/>
:	:	kIx,
Ephraim	Ephraim	k1gMnSc1
Longworth	Longworth	k1gMnSc1
(	(	kIx(
<g/>
371	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
<g/>
:	:	kIx,
Ben	Ben	k1gInSc1
Woodburn	Woodburna	k1gFnPc2
<g/>
,	,	kIx,
17	#num#	k4
let	léto	k1gNnPc2
&	&	k?
45	#num#	k4
dní	den	k1gInPc2
doma	doma	k6eAd1
proti	proti	k7c3
Leeds	Leeds	k1gInSc4
United	United	k1gInSc1
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
<g/>
:	:	kIx,
Billy	Bill	k1gMnPc7
Liddell	Liddella	k1gFnPc2
<g/>
,	,	kIx,
38	#num#	k4
let	léto	k1gNnPc2
&	&	k?
55	#num#	k4
dní	den	k1gInPc2
doma	doma	k6eAd1
proti	proti	k7c3
Stoke	Stoke	k1gNnSc3
City	City	k1gFnSc7
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1960	#num#	k4
</s>
<s>
Nejčastější	častý	k2eAgMnSc1d3
reprezentant	reprezentant	k1gMnSc1
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
67	#num#	k4
<g/>
)	)	kIx)
za	za	k7c4
Wales	Wales	k1gInSc4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
gólů	gól	k1gInPc2
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Rush	Rush	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rekordní	rekordní	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
<g/>
:	:	kIx,
11-0	11-0	k4
proti	proti	k7c3
Strø	Strø	k1gMnSc1
IF	IF	kA
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Rekordní	rekordní	k2eAgFnSc1d1
prohra	prohra	k1gFnSc1
<g/>
:	:	kIx,
1-9	1-9	k4
s	s	k7c7
Birmingham	Birmingham	k1gInSc1
City	city	k1gNnSc4
FC	FC	kA
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Nejdražší	drahý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
<g/>
:	:	kIx,
Virgil	Virgil	k1gMnSc1
van	vana	k1gFnPc2
Dijk	Dijk	k1gMnSc1
<g/>
,	,	kIx,
75	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejdražší	drahý	k2eAgMnSc1d3
odcházející	odcházející	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
:	:	kIx,
Phillipe	Phillip	k1gMnSc5
Coutinho	Coutinha	k1gMnSc5
<g/>
,	,	kIx,
142	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
ligová	ligový	k2eAgFnSc1d1
návštěva	návštěva	k1gFnSc1
<g/>
:	:	kIx,
58	#num#	k4
757	#num#	k4
proti	proti	k7c3
Chelsea	Chelse	k1gInSc2
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
návštěva	návštěva	k1gFnSc1
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
<g/>
:	:	kIx,
61	#num#	k4
905	#num#	k4
proti	proti	k7c3
Wolves	Wolves	k1gMnSc1
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
návštěva	návštěva	k1gFnSc1
v	v	k7c6
Ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
:	:	kIx,
50	#num#	k4
880	#num#	k4
proti	proti	k7c3
Nottingham	Nottingham	k1gInSc4
Forest	Forest	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
semifinále	semifinále	k1gNnSc1
<g/>
,	,	kIx,
odveta	odveta	k1gFnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
návštěva	návštěva	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
:	:	kIx,
55	#num#	k4
104	#num#	k4
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
UEFA	UEFA	kA
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
semifinále	semifinále	k1gNnSc1
<g/>
,	,	kIx,
odveta	odveta	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Red	Red	k1gFnSc1
rivalry	rivalra	k1gFnSc2
on	on	k3xPp3gInSc1
England	England	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
north-west	north-west	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-17	2016-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-04-15	2020-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
The	The	k1gFnPc2
30	#num#	k4
biggest	biggest	k1gFnSc4
clubs	clubsa	k1gFnPc2
in	in	k?
world	world	k1gMnSc1
football	football	k1gMnSc1
<g/>
,	,	kIx,
including	including	k1gInSc1
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
,	,	kIx,
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
talkSPORT	talkSPORT	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-12	2019-02-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRAHAM	Graham	k1gMnSc1
<g/>
,	,	kIx,
MATTHEW	MATTHEW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twickenham	Twickenham	k1gInSc1
<g/>
:	:	kIx,
Hamlyn	Hamlyn	k1gInSc1
124	#num#	k4
pages	pagesa	k1gFnPc2
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
600	#num#	k4
<g/>
-	-	kIx~
<g/>
50254	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
600	#num#	k4
<g/>
-	-	kIx~
<g/>
50254	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
13794563	#num#	k4
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
is	is	k?
formed	formed	k1gInSc1
-	-	kIx~
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-07-12	2010-07-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GRAHAM	Graham	k1gMnSc1
<g/>
,	,	kIx,
MATTHEW	MATTHEW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twickenham	Twickenham	k1gInSc1
<g/>
:	:	kIx,
Hamlyn	Hamlyn	k1gInSc1
124	#num#	k4
pages	pagesa	k1gFnPc2
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
600	#num#	k4
<g/>
-	-	kIx~
<g/>
50254	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
600	#num#	k4
<g/>
-	-	kIx~
<g/>
50254	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
13794563	#num#	k4
↑	↑	k?
KELLY	KELLY	kA
<g/>
,	,	kIx,
STEPHEN	STEPHEN	kA
F.	F.	kA
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
You	You	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ll	ll	k?
never	never	k1gInSc1
walk	walk	k6eAd1
alone	alonout	k5eAaImIp3nS,k5eAaPmIp3nS
:	:	kIx,
the	the	k?
official	official	k1gInSc1
illustrated	illustrated	k1gInSc1
history	histor	k1gInPc1
of	of	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Macdonald	Macdonald	k1gMnSc1
239	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
356	#num#	k4
<g/>
-	-	kIx~
<g/>
19594	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
356	#num#	k4
<g/>
-	-	kIx~
<g/>
19594	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
264962220	#num#	k4
↑	↑	k?
Heysel	Heysel	k1gMnSc1
disaster	disaster	k1gMnSc1
<g/>
:	:	kIx,
English	English	k1gMnSc1
football	football	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
forgotten	forgottno	k1gNnPc2
tragedy	trageda	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Hillsborough	Hillsborough	k1gMnSc1
tragedy	trageda	k1gMnSc2
(	(	kIx(
<g/>
Video	video	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
3-3	3-3	k4
West	Westa	k1gFnPc2
Ham	ham	k0
(	(	kIx(
<g/>
aet	aet	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
v	v	k7c6
amerických	americký	k2eAgFnPc6d1
rukou	ruka	k1gFnPc6
<g/>
↑	↑	k?
AC	AC	kA
Milan	Milan	k1gMnSc1
2-1	2-1	k4
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
confirm	confirm	k1gInSc1
Jürgen	Jürgen	k1gInSc1
Klopp	Klopp	k1gInSc1
appointment	appointment	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
liverpoolfc	liverpoolfc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2015-10-08	2015-10-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NAKRANI	NAKRANI	kA
<g/>
,	,	kIx,
Sachin	Sachin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
2015-16	2015-16	k4
preview	preview	k?
No	no	k9
<g/>
8	#num#	k4
<g/>
:	:	kIx,
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-06-30	2015-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
Season	Season	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
premierleague	premierleague	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCNULTY	MCNULTY	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
1-1	1-1	k4
Manchester	Manchester	k1gInSc1
City	city	k1gNnSc2
(	(	kIx(
<g/>
pens	pensum	k1gNnPc2
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-02-28	2016-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Premier	Premier	k1gInSc1
League	League	k1gFnSc1
2016-17	2016-17	k4
season	season	k1gMnSc1
review	review	k?
<g/>
:	:	kIx,
our	our	k?
writers	writers	k1gInSc1
<g/>
’	’	k?
best	best	k1gInSc1
and	and	k?
worst	worst	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-22	2017-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
THOMPSON	THOMPSON	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
Premier	Premier	k1gMnSc1
League	Leagu	k1gInSc2
season	season	k1gMnSc1
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gFnSc1
Sports	Sportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-21	2017-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCNULTY	MCNULTY	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phil	Phil	k1gInSc1
McNulty	McNult	k1gInPc1
<g/>
:	:	kIx,
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
end-of-season	end-of-season	k1gMnSc1
report	report	k1gInSc1
and	and	k?
manager	manager	k1gMnSc1
ratings	ratings	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-22	2017-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
HUNTER	HUNTER	kA
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
2017-18	2017-18	k4
preview	preview	k?
No	no	k9
<g/>
10	#num#	k4
<g/>
:	:	kIx,
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-04	2017-08-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KELLY	KELLY	kA
<g/>
,	,	kIx,
Ryan	Ryan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
2017-18	2017-18	k4
preview	preview	k?
<g/>
:	:	kIx,
Transfers	Transfers	k1gInSc1
<g/>
,	,	kIx,
full	full	k1gInSc1
squad	squad	k1gInSc1
<g/>
,	,	kIx,
fixtures	fixtures	k1gInSc1
<g/>
,	,	kIx,
shirt	shirt	k1gInSc1
numbers	numbers	k1gInSc1
&	&	k?
tickets	tickets	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-02	2017-08-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
Season	Season	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
premierleague	premierleague	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
STOKKERMANS	STOKKERMANS	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
European	European	k1gInSc1
Competitions	Competitions	k1gInSc1
2017-18	2017-18	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RSSSF	RSSSF	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2020-04-02	2020-04-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEGLEY	BEGLEY	kA
<g/>
,	,	kIx,
Emlyn	Emlyn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
5-2	5-2	k4
Roma	Rom	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-24	2018-04-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHNSTON	JOHNSTON	kA
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roma	Rom	k1gMnSc4
4-2	4-2	k4
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-02	2018-05-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MCNULTY	MCNULTY	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
3-1	3-1	k4
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-26	2018-05-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
HUNTER	HUNTER	kA
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
2018-19	2018-19	k4
preview	preview	k?
No	no	k9
12	#num#	k4
<g/>
:	:	kIx,
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-06	2018-08-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
válí	válet	k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
se	se	k3xPyFc4
o	o	k7c6
tom	ten	k3xDgNnSc6
přesvědčil	přesvědčit	k5eAaPmAgMnS
West	West	k1gMnSc1
Ham	ham	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurofotbal	Eurofotbal	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-13	2018-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
-	-	kIx~
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurofotbal	Eurofotbal	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-29	2018-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
Season	Season	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
premierleague	premierleague	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-29	2019-05-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
Player	Player	k1gInSc1
Profiles	Profiles	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
https://www.premierleague.com	https://www.premierleague.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHAW	SHAW	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milner	Milner	k1gInSc4
on	on	k3xPp3gMnSc1
vice-captain	vice-captain	k1gMnSc1
honour	honoura	k1gFnPc2
and	and	k?
Coutinho	Coutin	k1gMnSc2
class	classa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
F.	F.	kA
<g/>
C.	C.	kA
<g/>
,	,	kIx,
10	#num#	k4
August	August	k1gMnSc1
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Henderson	Henderson	k1gInSc1
appointed	appointed	k1gInSc1
Liverpool	Liverpool	k1gInSc1
captain	captain	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liverpool	Liverpool	k1gInSc1
F.	F.	kA
<g/>
C.	C.	kA
<g/>
,	,	kIx,
10	#num#	k4
July	Jula	k1gFnSc2
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BASCOMBE	BASCOMBE	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Best	Best	k2eAgInSc1d1
Liverpool	Liverpool	k1gInSc1
players	playersa	k1gFnPc2
ever	ever	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
top	topit	k5eAaImRp2nS
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-03-23	2015-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
LIVERPOOL	Liverpool	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fchd	fchd	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
West	West	k2eAgInSc4d1
Brom	brom	k1gInSc4
1-2	1-2	k4
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
news	news	k1gInSc1
<g/>
:	:	kIx,
Harvey	Harvea	k1gFnSc2
Elliott	Elliott	k1gMnSc1
becomes	becomes	k1gMnSc1
youngest	youngest	k1gMnSc1
player	player	k1gMnSc1
to	ten	k3xDgNnSc4
ever	ever	k1gInSc4
start	start	k1gInSc4
for	forum	k1gNnPc2
Reds	Reds	k1gInSc1
|	|	kIx~
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.goal.com	www.goal.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
All	All	k1gMnSc5
time	timus	k1gMnSc5
europe	europ	k1gMnSc5
goalscorers	goalscorers	k1gInSc1
-	-	kIx~
LFChistory	LFChistor	k1gInPc1
-	-	kIx~
Stats	Stats	k1gInSc1
galore	galor	k1gInSc5
for	forum	k1gNnPc2
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.lfchistory.net	www.lfchistory.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Virgil	Virgil	k1gInSc1
van	van	k1gInSc1
Dijk	Dijk	k1gInSc1
<g/>
:	:	kIx,
Liverpool	Liverpool	k1gInSc1
to	ten	k3xDgNnSc1
sign	signum	k1gNnPc2
Southampton	Southampton	k1gInSc4
defender	defender	k1gInSc4
for	forum	k1gNnPc2
world	world	k6eAd1
record	record	k6eAd1
£	£	k?
<g/>
75	#num#	k4
<g/>
m.	m.	k?
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Philippe	Philipp	k1gMnSc5
Coutinho	Coutinha	k1gMnSc5
<g/>
:	:	kIx,
Barcelona	Barcelona	k1gFnSc1
to	ten	k3xDgNnSc1
sign	signum	k1gNnPc2
Liverpool	Liverpool	k1gInSc1
and	and	k?
Brazil	Brazil	k1gFnSc2
midfielder	midfielder	k1gMnSc1
in	in	k?
£	£	k?
<g/>
142	#num#	k4
<g/>
m	m	kA
deal	deala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
na	na	k7c4
Transfermarkt	Transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
na	na	k7c6
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
Supporters	Supporters	k1gInSc1
Club	club	k1gInSc1
Slovakia	Slovakia	k1gFnSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CZ-SK	CZ-SK	k?
stránky	stránka	k1gFnPc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
-	-	kIx~
FotbaloveStadiony	FotbaloveStadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Premier	Premier	k1gInSc1
League	Leagu	k1gInSc2
–	–	k?
anglická	anglický	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Arsenal	Arsenat	k5eAaImAgMnS,k5eAaPmAgMnS
FC	FC	kA
•	•	k?
Aston	Aston	k1gMnSc1
Villa	Villa	k1gMnSc1
FC	FC	kA
•	•	k?
Brighton	Brighton	k1gInSc1
&	&	k?
Hove	Hove	k1gInSc1
Albion	Albion	k1gInSc1
FC	FC	kA
•	•	k?
Burnley	Burnlea	k1gFnPc4
FC	FC	kA
•	•	k?
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
•	•	k?
Crystal	Crystal	k1gMnSc7
Palace	Palace	k1gFnSc2
FC	FC	kA
•	•	k?
Everton	Everton	k1gInSc1
FC	FC	kA
•	•	k?
Leicester	Leicester	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Newcastle	Newcastle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Sheffield	Sheffield	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Southampton	Southampton	k1gInSc1
FC	FC	kA
•	•	k?
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Wolverhampton	Wolverhampton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
Football	Football	k1gInSc1
League	Leagu	k1gFnSc2
<g/>
:	:	kIx,
1888	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1889	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1890	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1891	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
FL	FL	kA
First	First	k1gInSc1
Division	Division	k1gInSc1
<g/>
:	:	kIx,
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1894	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1895	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1896	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
1900	#num#	k4
•	•	k?
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
1903	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
1917	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
•	•	k?
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
26	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
<g/>
:	:	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
</s>
<s>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Accrington	Accrington	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Darwen	Darwen	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Glossop	Glossop	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gInSc1
Park	park	k1gInSc1
Avenue	avenue	k1gFnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bury	Bury	k?
FC	FC	kA
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Brentford	Brentford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Grimsby	Grimsba	k1gFnSc2
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Preston	Preston	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leyton	Leyton	k1gInSc1
Orient	Orient	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Northampton	Northampton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carlisle	Carlisle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bristol	Bristol	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxford	Oxford	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Millwall	Millwall	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Luton	Luton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Notts	Notts	k1gInSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldham	Oldham	k1gInSc1
Athletic	Athletice	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swindon	Swindon	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barnsley	Barnslea	k1gFnPc4
FC	FC	kA
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sheffield	Sheffield	k1gInSc1
Wednesday	Wednesdaa	k1gMnSc2
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wimbledon	Wimbledon	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Coventry	Coventr	k1gMnPc4
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ipswich	Ipswich	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charlton	Charlton	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Derby	derby	k1gNnSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portsmouth	Portsmouth	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Birmingham	Birmingham	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Blackpool	Blackpool	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Blackburn	Blackburn	k1gInSc1
Rovers	Rovers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bolton	Bolton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wigan	Wigan	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Reading	Reading	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Queens	Queens	k1gInSc1
Park	park	k1gInSc1
Rangers	Rangers	k1gInSc4
FC	FC	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hull	Hull	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Middlesbrough	Middlesbrough	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sunderland	Sunderland	k1gInSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stoke	Stoke	k1gNnSc2
City	City	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swansea	Swanseum	k1gNnSc2
City	City	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cardiff	Cardiff	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Huddersfield	Huddersfield	k1gInSc1
Town	Town	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFC	AFC	kA
Bournemouth	Bournemouth	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norwich	Norwich	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Watford	Watford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Golden	Goldno	k1gNnPc2
Boot	Boot	k1gInSc1
•	•	k?
Golden	Goldna	k1gFnPc2
Glove	Gloev	k1gFnSc2
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Playmaker	Playmaker	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
dle	dle	k7c2
FWA	FWA	kA
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
•	•	k?
Football	Football	k1gInSc1
League	League	k1gFnSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
•	•	k?
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
Asia	Asius	k1gMnSc2
Trophy	Tropha	k1gFnSc2
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
•	•	k?
Professional	Professional	k1gMnSc1
Development	Development	k1gMnSc1
League	League	k1gFnPc2
U18	U18	k1gMnSc1
&	&	k?
U23	U23	k1gMnSc1
•	•	k?
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
International	International	k1gMnSc1
Cup	cup	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Football	Footballa	k1gFnPc2
Association	Association	k1gInSc1
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
:	:	kIx,
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Super	super	k2eAgNnSc7d1
League	League	k1gNnSc7
•	•	k?
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
League	League	k1gNnSc7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Community	Communit	k1gInPc7
Shield	Shielda	k1gFnPc2
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
223706	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
4904	#num#	k4
9761	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50049491	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
311595862	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50049491	#num#	k4
</s>
