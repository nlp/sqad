<s>
Uršula	Uršula	k1gFnSc1
</s>
<s>
Uršula	Uršula	k1gFnSc1
</s>
<s>
ženské	ženský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Svátek	svátek	k1gInSc4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
Původ	původ	k1gInSc1
</s>
<s>
latinský	latinský	k2eAgInSc1d1
Četnost	četnost	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
344	#num#	k4
Pořadí	pořadí	k1gNnPc1
podle	podle	k7c2
četnosti	četnost	k1gFnSc2
</s>
<s>
649	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Voršila	Voršila	k1gFnSc1
Četnost	četnost	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
44	#num#	k4
Pořadí	pořadí	k1gNnPc1
podle	podle	k7c2
četnosti	četnost	k1gFnSc2
</s>
<s>
1579	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Oršula	Oršula	k1gFnSc1
Četnost	četnost	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
0	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Urška	Urška	k1gFnSc1
Četnost	četnost	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
0	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Četnost	četnost	k1gFnSc1
jmen	jméno	k1gNnPc2
a	a	k8xC
příjmení	příjmení	k1gNnSc2
graficky	graficky	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
četnost	četnost	k1gFnSc1
dle	dle	k7c2
ročníků	ročník	k1gInPc2
v	v	k7c6
grafu	graf	k1gInSc6
<g/>
,	,	kIx,
původ	původ	k1gInSc4
a	a	k8xC
osobnostiTento	osobnostiTento	k1gNnSc4
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Uršula	Uršula	k1gFnSc1
je	být	k5eAaImIp3nS
ženské	ženský	k2eAgNnSc1d1
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
latinského	latinský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
ze	z	k7c2
slova	slovo	k1gNnSc2
ursa	urs	k1gInSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
malá	malý	k2eAgFnSc1d1
medvědice	medvědice	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
starého	starý	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
má	mít	k5eAaImIp3nS
svátek	svátek	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
jména	jméno	k1gNnSc2
je	být	k5eAaImIp3nS
Voršila	Voršila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Uršula	Uršula	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Jméno	jméno	k1gNnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
nepatří	patřit	k5eNaImIp3nS
mezi	mezi	k7c4
nejpoužívanější	používaný	k2eAgNnPc4d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
v	v	k7c6
Česku	Česko	k1gNnSc6
neslo	nést	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
Uršula	Uršula	k1gFnSc1
411	#num#	k4
občanek	občanka	k1gFnPc2
a	a	k8xC
registrovaných	registrovaný	k2eAgFnPc2d1
cizinek	cizinka	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
409	#num#	k4
toho	ten	k3xDgNnSc2
samostatně	samostatně	k6eAd1
a	a	k8xC
3	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
jiným	jiný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
významně	významně	k6eAd1
zastoupené	zastoupený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
ale	ale	k9
varianty	varianta	k1gFnPc1
Ursula	Ursulum	k1gNnSc2
(	(	kIx(
<g/>
296	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
266	#num#	k4
samostatně	samostatně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
polská	polský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Urszula	Urszula	k1gFnSc1
(	(	kIx(
<g/>
262	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
198	#num#	k4
samostatně	samostatně	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
oboje	oboj	k1gFnPc1
patrně	patrně	k6eAd1
nosí	nosit	k5eAaImIp3nP
hlavně	hlavně	k9
cizinky	cizinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Známé	známý	k2eAgFnPc1d1
nositelky	nositelka	k1gFnPc1
jména	jméno	k1gNnSc2
</s>
<s>
Svaté	svatý	k2eAgFnPc1d1
a	a	k8xC
blahoslavené	blahoslavený	k2eAgFnPc1d1
</s>
<s>
Sv.	sv.	kA
Urszula	Urszula	k1gFnSc1
Ledóchowska	Ledóchowska	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
sv.	sv.	kA
Voršila	Voršila	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
bl.	bl.	k?
Orsolina	Orsolina	k1gFnSc1
Veneri	Vener	k1gFnSc2
di	di	k?
Parma	Parma	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Weronika	Weronika	k1gFnSc1
Giuliani	Giuliaň	k1gFnSc6
(	(	kIx(
<g/>
1660	#num#	k4
<g/>
-	-	kIx~
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
–	–	k?
italská	italský	k2eAgFnSc1d1
řeholnice	řeholnice	k1gFnSc1
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Orsola	Orsola	k1gFnSc1
Giuliani	Giuliaň	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Urszula	Urszula	k1gFnSc1
Ledóchowska	Ledóchowska	k1gFnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
–	–	k?
polská	polský	k2eAgFnSc1d1
řeholnice	řeholnice	k1gFnSc1
<g/>
,	,	kIx,
zakladatelka	zakladatelka	k1gFnSc1
šedých	šedý	k2eAgFnPc2d1
uršulinek	uršulinka	k1gFnPc2
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Ursula	Ursula	k1gFnSc1
K.	K.	kA
Le	Le	k1gFnSc1
Guinová	Guinová	k1gFnSc1
–	–	k?
americká	americký	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Uršula	Uršula	k1gFnSc1
Kluková	Klukový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Ursula	Ursula	k1gFnSc1
Buschhorn	Buschhorna	k1gFnPc2
–	–	k?
německá	německý	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Ursula	Ursula	k1gFnSc1
von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Leyenová	Leyenový	k2eAgFnSc1d1
–	–	k?
německá	německý	k2eAgFnSc1d1
politička	politička	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Četnost	četnost	k1gFnSc1
ženských	ženský	k2eAgInPc2d1
jmeno	jmeno	k6eAd1
podle	podle	k7c2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
;	;	kIx,
dostupné	dostupný	k2eAgFnPc1d1
<g/>
:	:	kIx,
https://web.archive.org/web/20110811051934/http://aplikace.mvcr.cz/archiv2008/sprava/informat/cetnost/2007/jmena_zeny/cet_jmena_zeny_vsechny.xls	https://web.archive.org/web/20110811051934/http://aplikace.mvcr.cz/archiv2008/sprava/informat/cetnost/2007/jmena_zeny/cet_jmena_zeny_vsechny.xls	k1gInSc1
(	(	kIx(
<g/>
navštíveno	navštívit	k5eAaPmNgNnS
23	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ursula	Ursulum	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Voršila	Voršila	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
