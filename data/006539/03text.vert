<s>
Jsoucno	jsoucno	k1gNnSc1	jsoucno
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc1d1	obecné
filosofické	filosofický	k2eAgNnSc1d1	filosofické
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jest	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsoucna	jsoucno	k1gNnPc1	jsoucno
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
bytí	bytí	k1gNnSc1	bytí
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
nelze	lze	k6eNd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bytí	bytí	k1gNnSc1	bytí
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gFnSc7	jejich
vlastností	vlastnost	k1gFnSc7	vlastnost
(	(	kIx(	(
<g/>
atributem	atribut	k1gInSc7	atribut
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
něho	on	k3xPp3gNnSc2	on
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
vůbec	vůbec	k9	vůbec
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
bytím	bytí	k1gNnSc7	bytí
dělá	dělat	k5eAaImIp3nS	dělat
člověk	člověk	k1gMnSc1	člověk
zkušenost	zkušenost	k1gFnSc4	zkušenost
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
jsoucnech	jsoucno	k1gNnPc6	jsoucno
či	či	k8xC	či
entitách	entita	k1gFnPc6	entita
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
pojmy	pojem	k1gInPc7	pojem
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
jsoucna	jsoucno	k1gNnSc2	jsoucno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
ontologie	ontologie	k1gFnSc2	ontologie
Martina	Martin	k1gMnSc2	Martin
Heideggera	Heidegger	k1gMnSc2	Heidegger
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyložen	vyložit	k5eAaPmNgInS	vyložit
v	v	k7c6	v
heslu	heslo	k1gNnSc3	heslo
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
Parmenidem	Parmenid	k1gInSc7	Parmenid
filosofové	filosof	k1gMnPc1	filosof
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
jsoucna	jsoucno	k1gNnPc4	jsoucno
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
<g/>
.	.	kIx.	.
</s>
<s>
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
ostře	ostro	k6eAd1	ostro
rozlišil	rozlišit	k5eAaPmAgInS	rozlišit
jsoucí	jsoucí	k2eAgInSc1d1	jsoucí
a	a	k8xC	a
nejsoucí	jsoucí	k2eNgInSc1d1	nejsoucí
a	a	k8xC	a
domníval	domnívat	k5eAaImAgInS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
jest	být	k5eAaImIp3nS	být
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
a	a	k8xC	a
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
věci	věc	k1gFnPc1	věc
pomíjivé	pomíjivý	k2eAgFnPc1d1	pomíjivá
a	a	k8xC	a
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
nejsoucí	nejsoucí	k2eAgFnPc4d1	nejsoucí
<g/>
,	,	kIx,	,
klamné	klamný	k2eAgFnPc4d1	klamná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
i	i	k9	i
Platóna	Platón	k1gMnSc4	Platón
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
pojmu	pojem	k1gInSc2	pojem
ideje	idea	k1gFnSc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
tuto	tento	k3xDgFnSc4	tento
nesnáz	nesnáz	k1gFnSc4	nesnáz
řeší	řešit	k5eAaImIp3nS	řešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
možnost	možnost	k1gFnSc4	možnost
(	(	kIx(	(
<g/>
dynamis	dynamis	k1gFnSc4	dynamis
<g/>
)	)	kIx)	)
a	a	k8xC	a
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
(	(	kIx(	(
<g/>
energeia	energeia	k1gFnSc1	energeia
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
věc	věc	k1gFnSc1	věc
vzniká	vznikat	k5eAaImIp3nS	vznikat
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
byla	být	k5eAaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
důležité	důležitý	k2eAgNnSc1d1	důležité
rozlišení	rozlišení	k1gNnSc1	rozlišení
mezi	mezi	k7c7	mezi
podstatou	podstata	k1gFnSc7	podstata
a	a	k8xC	a
atributy	atribut	k1gInPc7	atribut
<g/>
:	:	kIx,	:
podstata	podstata	k1gFnSc1	podstata
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
věc	věc	k1gFnSc1	věc
činí	činit	k5eAaImIp3nS	činit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
psa	pes	k1gMnSc2	pes
psem	pes	k1gMnSc7	pes
nebo	nebo	k8xC	nebo
nůž	nůž	k1gInSc4	nůž
nožem	nůž	k1gInSc7	nůž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
atributy	atribut	k1gInPc1	atribut
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
může	moct	k5eAaImIp3nS	moct
vyrůst	vyrůst	k5eAaPmF	vyrůst
<g/>
,	,	kIx,	,
nůž	nůž	k1gInSc4	nůž
třeba	třeba	k6eAd1	třeba
zrezivět	zrezivět	k5eAaPmF	zrezivět
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozlišení	rozlišení	k1gNnSc1	rozlišení
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
:	:	kIx,	:
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
se	se	k3xPyFc4	se
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
různé	různý	k2eAgFnPc1d1	různá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
(	(	kIx(	(
<g/>
atributy	atribut	k1gInPc1	atribut
<g/>
,	,	kIx,	,
případky	případek	k1gInPc1	případek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nominalismus	nominalismus	k1gInSc1	nominalismus
a	a	k8xC	a
realismus	realismus	k1gInSc1	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
spor	spor	k1gInSc1	spor
o	o	k7c4	o
povahu	povaha	k1gFnSc4	povaha
jsoucna	jsoucno	k1gNnSc2	jsoucno
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
scholastice	scholastika	k1gFnSc6	scholastika
a	a	k8xC	a
týkal	týkat	k5eAaImAgInS	týkat
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
"	"	kIx"	"
<g/>
jsou	být	k5eAaImIp3nP	být
<g/>
"	"	kIx"	"
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
spíše	spíše	k9	spíše
univerzálie	univerzálie	k1gFnPc1	univerzálie
(	(	kIx(	(
<g/>
obecniny	obecnina	k1gFnPc1	obecnina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
denotáty	denotát	k1gInPc4	denotát
<g/>
)	)	kIx)	)
obecných	obecný	k2eAgInPc2d1	obecný
pojmů	pojem	k1gInPc2	pojem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stanovisko	stanovisko	k1gNnSc4	stanovisko
Platónovo	Platónův	k2eAgNnSc4d1	Platónovo
i	i	k8xC	i
středověkého	středověký	k2eAgInSc2d1	středověký
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
zda	zda	k8xS	zda
skutečně	skutečně	k6eAd1	skutečně
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
předměty	předmět	k1gInPc4	předmět
či	či	k8xC	či
"	"	kIx"	"
<g/>
výskyty	výskyt	k1gInPc1	výskyt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
individua	individuum	k1gNnSc2	individuum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
obecné	obecný	k2eAgInPc1d1	obecný
pojmy	pojem	k1gInPc1	pojem
jsou	být	k5eAaImIp3nP	být
pouhá	pouhý	k2eAgNnPc4d1	pouhé
označení	označení	k1gNnPc4	označení
čili	čili	k8xC	čili
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nominalismus	nominalismus	k1gInSc1	nominalismus
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
názorů	názor	k1gInPc2	názor
trvá	trvat	k5eAaImIp3nS	trvat
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
většinou	většinou	k6eAd1	většinou
domníváme	domnívat	k5eAaImIp1nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečné	skutečný	k2eAgInPc1d1	skutečný
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
"	"	kIx"	"
<g/>
konkrétní	konkrétní	k2eAgNnPc4d1	konkrétní
<g/>
"	"	kIx"	"
či	či	k8xC	či
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
také	také	k9	také
existencialismus	existencialismus	k1gInSc1	existencialismus
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
představu	představ	k1gInSc2	představ
obecného	obecný	k2eAgInSc2d1	obecný
silně	silně	k6eAd1	silně
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ale	ale	k8xC	ale
předmětem	předmět	k1gInSc7	předmět
empirické	empirický	k2eAgFnSc2d1	empirická
vědy	věda	k1gFnSc2	věda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
celé	celý	k2eAgFnPc1d1	celá
třídy	třída	k1gFnPc1	třída
"	"	kIx"	"
<g/>
stejných	stejný	k2eAgFnPc2d1	stejná
<g/>
"	"	kIx"	"
věcí	věc	k1gFnPc2	věc
–	–	k?	–
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
nebo	nebo	k8xC	nebo
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
současná	současný	k2eAgFnSc1d1	současná
genetika	genetika	k1gFnSc1	genetika
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
informaci	informace	k1gFnSc4	informace
(	(	kIx(	(
<g/>
genom	genom	k1gInSc1	genom
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
spíš	spíš	k9	spíš
na	na	k7c6	na
stanovisku	stanovisko	k1gNnSc6	stanovisko
realistickém	realistický	k2eAgNnSc6d1	realistické
<g/>
:	:	kIx,	:
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
organismus	organismus	k1gInSc4	organismus
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
"	"	kIx"	"
<g/>
vehikl	vehikl	k1gInSc1	vehikl
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
sobecký	sobecký	k2eAgInSc4d1	sobecký
gen	gen	k1gInSc4	gen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Logický	logický	k2eAgInSc1d1	logický
positivismus	positivismus	k1gInSc1	positivismus
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
jazyka	jazyk	k1gInSc2	jazyk
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
nominalistické	nominalistický	k2eAgFnSc2d1	nominalistická
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
jazykový	jazykový	k2eAgInSc1d1	jazykový
<g/>
:	:	kIx,	:
zdánlivé	zdánlivý	k2eAgFnPc1d1	zdánlivá
"	"	kIx"	"
<g/>
obecniny	obecnina	k1gFnPc1	obecnina
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
souhrnné	souhrnný	k2eAgInPc4d1	souhrnný
pojmy	pojem	k1gInPc4	pojem
pro	pro	k7c4	pro
třídy	třída	k1gFnPc4	třída
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
samy	sám	k3xTgInPc1	sám
žádná	žádný	k3yNgNnPc1	žádný
jsoucna	jsoucno	k1gNnPc1	jsoucno
nepředstavují	představovat	k5eNaImIp3nP	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jazyk	jazyk	k1gInSc1	jazyk
zdánlivě	zdánlivě	k6eAd1	zdánlivě
označuje	označovat	k5eAaImIp3nS	označovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
–	–	k?	–
například	například	k6eAd1	například
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
paní	paní	k1gFnSc2	paní
X.	X.	kA	X.
Y	Y	kA	Y
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
"	"	kIx"	"
–	–	k?	–
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
široké	široký	k2eAgNnSc1d1	široké
pole	pole	k1gNnSc1	pole
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
a	a	k8xC	a
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
rozmanitých	rozmanitý	k2eAgFnPc2d1	rozmanitá
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
někdejší	někdejší	k2eAgNnSc4d1	někdejší
novorozeně	novorozeně	k1gNnSc4	novorozeně
<g/>
,	,	kIx,	,
současné	současný	k2eAgNnSc4d1	současné
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
studentku	studentka	k1gFnSc4	studentka
<g/>
,	,	kIx,	,
možná	možná	k9	možná
významnou	významný	k2eAgFnSc4d1	významná
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
<g/>
,	,	kIx,	,
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
stařenku	stařenka	k1gFnSc4	stařenka
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
jednotlivina	jednotlivina	k1gFnSc1	jednotlivina
<g/>
"	"	kIx"	"
<g/>
?	?	kIx.	?
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
rostliny	rostlina	k1gFnSc2	rostlina
i	i	k8xC	i
živočichové	živočich	k1gMnPc1	živočich
se	se	k3xPyFc4	se
reprodukují	reprodukovat	k5eAaBmIp3nP	reprodukovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
žaludů	žalud	k1gInPc2	žalud
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
duby	dub	k1gInPc1	dub
a	a	k8xC	a
z	z	k7c2	z
pulců	pulec	k1gMnPc2	pulec
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
žáby	žába	k1gFnPc1	žába
téhož	týž	k3xTgInSc2	týž
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
jejich	jejich	k3xOp3gMnPc4	jejich
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
spíše	spíše	k9	spíše
jsoucí	jsoucí	k2eAgFnSc1d1	jsoucí
<g/>
:	:	kIx,	:
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
jepice	jepice	k1gFnSc1	jepice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
její	její	k3xOp3gNnSc1	její
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
trvalý	trvalý	k2eAgInSc4d1	trvalý
druh	druh	k1gInSc4	druh
<g/>
?	?	kIx.	?
</s>
<s>
Spor	spor	k1gInSc1	spor
realismu	realismus	k1gInSc2	realismus
s	s	k7c7	s
nominalismem	nominalismus	k1gInSc7	nominalismus
má	mít	k5eAaImIp3nS	mít
významné	významný	k2eAgInPc4d1	významný
důsledky	důsledek	k1gInPc4	důsledek
i	i	k9	i
pro	pro	k7c4	pro
chápání	chápání	k1gNnSc4	chápání
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
realisté	realista	k1gMnPc1	realista
trvají	trvat	k5eAaImIp3nP	trvat
například	například	k6eAd1	například
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
obecné	obecný	k2eAgInPc4d1	obecný
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
člověku	člověk	k1gMnSc3	člověk
jako	jako	k8xS	jako
takovému	takový	k3xDgNnSc3	takový
patří	patřit	k5eAaImIp3nP	patřit
jistá	jistý	k2eAgNnPc1d1	jisté
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
nominalisté	nominalista	k1gMnPc1	nominalista
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
spíš	spíš	k9	spíš
volnou	volný	k2eAgFnSc4d1	volná
hru	hra	k1gFnSc4	hra
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
konflikty	konflikt	k1gInPc1	konflikt
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nP	řešit
trhem	trh	k1gInSc7	trh
a	a	k8xC	a
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Realisté	realista	k1gMnPc1	realista
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
obecné	obecný	k2eAgInPc4d1	obecný
zájmy	zájem	k1gInPc4	zájem
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
pro	pro	k7c4	pro
krajní	krajní	k2eAgMnPc4d1	krajní
nominalisty	nominalista	k1gMnPc4	nominalista
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
dílčí	dílčí	k2eAgInPc4d1	dílčí
<g/>
,	,	kIx,	,
partikulární	partikulární	k2eAgInPc4d1	partikulární
zájmy	zájem	k1gInPc4	zájem
svobodných	svobodný	k2eAgMnPc2d1	svobodný
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
společnosti	společnost	k1gFnPc1	společnost
chápou	chápat	k5eAaImIp3nP	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedno	jeden	k4xCgNnSc1	jeden
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
druhého	druhý	k4xOgMnSc2	druhý
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
Aristotelovým	Aristotelův	k2eAgNnSc7d1	Aristotelovo
"	"	kIx"	"
<g/>
ničeho	nic	k3yNnSc2	nic
příliš	příliš	k6eAd1	příliš
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
obě	dva	k4xCgFnPc1	dva
krajnosti	krajnost	k1gFnPc1	krajnost
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
špatným	špatný	k2eAgInPc3d1	špatný
koncům	konec	k1gInPc3	konec
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
hledat	hledat	k5eAaImF	hledat
zlatý	zlatý	k2eAgInSc4d1	zlatý
střed	střed	k1gInSc4	střed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
věcí	věc	k1gFnSc7	věc
neustálého	neustálý	k2eAgNnSc2d1	neustálé
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
a	a	k8xC	a
opravování	opravování	k1gNnSc2	opravování
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
