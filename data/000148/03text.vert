<s>
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
u	u	k7c2	u
města	město	k1gNnSc2	město
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
stavby	stavba	k1gFnPc4	stavba
barokního	barokní	k2eAgMnSc2d1	barokní
stavitele	stavitel	k1gMnSc2	stavitel
Jana	Jan	k1gMnSc2	Jan
Blažeje	Blažej	k1gMnSc2	Blažej
Santiniho-Aichela	Santiniho-Aichel	k1gMnSc2	Santiniho-Aichel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
Santiniho	Santini	k1gMnSc4	Santini
výtvor	výtvor	k1gInSc1	výtvor
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zařazen	zařadit	k5eAaPmNgMnS	zařadit
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světových	světový	k2eAgFnPc2d1	světová
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Zámek	zámek	k1gInSc1	zámek
Žďár	Žďár	k1gInSc4	Žďár
nedaleko	nedaleko	k7c2	nedaleko
historické	historický	k2eAgFnSc2d1	historická
zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
ve	v	k7c6	v
slohu	sloh	k1gInSc6	sloh
barokní	barokní	k2eAgFnSc2d1	barokní
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
poutního	poutní	k2eAgInSc2d1	poutní
kostela	kostel	k1gInSc2	kostel
pojal	pojmout	k5eAaPmAgMnS	pojmout
žďárský	žďárský	k2eAgMnSc1d1	žďárský
opat	opat	k1gMnSc1	opat
Václav	Václav	k1gMnSc1	Václav
Vejmluva	Vejmluva	k1gFnSc1	Vejmluva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1719	[number]	k4	1719
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
hrobu	hrob	k1gInSc2	hrob
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1720	[number]	k4	1720
<g/>
-	-	kIx~	-
<g/>
1722	[number]	k4	1722
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
náklad	náklad	k1gInSc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
stavby	stavba	k1gFnSc2	stavba
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
oslavit	oslavit	k5eAaPmF	oslavit
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mocného	mocný	k2eAgMnSc2d1	mocný
patrona	patron	k1gMnSc2	patron
a	a	k8xC	a
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
stavby	stavba	k1gFnSc2	stavba
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1720	[number]	k4	1720
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1722	[number]	k4	1722
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
vršku	vršek	k1gInSc6	vršek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Václav	Václav	k1gMnSc1	Václav
Vejmluva	Vejmluva	k1gFnSc1	Vejmluva
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Zelená	zelený	k2eAgFnSc1d1	zelená
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
Černý	černý	k2eAgInSc1d1	černý
les	les	k1gInSc1	les
nebo	nebo	k8xC	nebo
Strmá	strmý	k2eAgFnSc1d1	strmá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
vrchu	vrch	k1gInSc2	vrch
u	u	k7c2	u
Nepomuku	Nepomuk	k1gInSc2	Nepomuk
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
i	i	k8xC	i
první	první	k4xOgMnPc1	první
žďárští	žďárský	k2eAgMnPc1d1	žďárský
mniši	mnich	k1gMnPc1	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
obklopena	obklopit	k5eAaPmNgFnS	obklopit
vysokým	vysoký	k2eAgInSc7d1	vysoký
borovým	borový	k2eAgInSc7d1	borový
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
původně	původně	k6eAd1	původně
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1784	[number]	k4	1784
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
kláštera	klášter	k1gInSc2	klášter
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
i	i	k9	i
na	na	k7c4	na
poutní	poutní	k2eAgInSc4d1	poutní
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zničil	zničit	k5eAaPmAgMnS	zničit
jeho	jeho	k3xOp3gFnSc4	jeho
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
bylo	být	k5eAaImAgNnS	být
zakrytí	zakrytí	k1gNnSc1	zakrytí
kostela	kostel	k1gInSc2	kostel
prkny	prkno	k1gNnPc7	prkno
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
žďárský	žďárský	k2eAgMnSc1d1	žďárský
obchodník	obchodník	k1gMnSc1	obchodník
Josef	Josef	k1gMnSc1	Josef
Pluhař	Pluhař	k1gMnSc1	Pluhař
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
mniši	mnich	k1gMnPc1	mnich
ze	z	k7c2	z
zrušeného	zrušený	k2eAgInSc2d1	zrušený
kláštera	klášter	k1gInSc2	klášter
i	i	k9	i
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
povolilo	povolit	k5eAaPmAgNnS	povolit
moravské	moravský	k2eAgNnSc1d1	Moravské
gubernium	gubernium	k1gNnSc1	gubernium
kostel	kostel	k1gInSc4	kostel
opravit	opravit	k5eAaPmF	opravit
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
:	:	kIx,	:
kostel	kostel	k1gInSc1	kostel
bude	být	k5eAaImBp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
pohřební	pohřební	k2eAgFnPc1d1	pohřební
a	a	k8xC	a
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
bude	být	k5eAaImBp3nS	být
přenesen	přenesen	k2eAgInSc1d1	přenesen
hřbitov	hřbitov	k1gInSc1	hřbitov
biskupství	biskupství	k1gNnSc1	biskupství
ani	ani	k8xC	ani
gubernium	gubernium	k1gNnSc1	gubernium
nepřispějí	přispět	k5eNaPmIp3nP	přispět
žádnými	žádný	k3yNgInPc7	žádný
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
žadatelé	žadatel	k1gMnPc1	žadatel
doloží	doložit	k5eAaPmIp3nP	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
i	i	k8xC	i
následný	následný	k2eAgInSc4d1	následný
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
kostel	kostel	k1gInSc1	kostel
stržen	strhnout	k5eAaPmNgInS	strhnout
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
vydražen	vydražit	k5eAaPmNgInS	vydražit
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
náboženského	náboženský	k2eAgInSc2d1	náboženský
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
podepsali	podepsat	k5eAaPmAgMnP	podepsat
zástupci	zástupce	k1gMnPc1	zástupce
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
Římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
farnosti	farnost	k1gFnSc2	farnost
Žďár	Žďár	k1gInSc4	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
II	II	kA	II
a	a	k8xC	a
Biskupství	biskupství	k1gNnSc2	biskupství
brněnského	brněnský	k2eAgNnSc2d1	brněnské
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
majetku	majetek	k1gInSc2	majetek
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
majetkovém	majetkový	k2eAgNnSc6d1	majetkové
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
s	s	k7c7	s
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
poutního	poutní	k2eAgInSc2d1	poutní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
kostel	kostel	k1gInSc1	kostel
i	i	k9	i
jej	on	k3xPp3gInSc4	on
obklopující	obklopující	k2eAgInSc4d1	obklopující
ambit	ambit	k1gInSc4	ambit
jsou	být	k5eAaImIp3nP	být
jednotně	jednotně	k6eAd1	jednotně
projektovanou	projektovaný	k2eAgFnSc7d1	projektovaná
a	a	k8xC	a
budovanou	budovaný	k2eAgFnSc7d1	budovaná
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
minimalistické	minimalistický	k2eAgNnSc1d1	minimalistické
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
účinné	účinný	k2eAgNnSc1d1	účinné
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
barokní	barokní	k2eAgInPc4d1	barokní
a	a	k8xC	a
gotické	gotický	k2eAgInPc4d1	gotický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
umučen	umučit	k5eAaPmNgMnS	umučit
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
kostela	kostel	k1gInSc2	kostel
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
geometrie	geometrie	k1gFnSc2	geometrie
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
opakuje	opakovat	k5eAaImIp3nS	opakovat
číslo	číslo	k1gNnSc1	číslo
pět	pět	k4xCc1	pět
jako	jako	k9	jako
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
pět	pět	k4xCc4	pět
hvězd	hvězda	k1gFnPc2	hvězda
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
objevily	objevit	k5eAaPmAgInP	objevit
nad	nad	k7c7	nad
tělem	tělo	k1gNnSc7	tělo
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
tak	tak	k6eAd1	tak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
Santini	Santin	k2eAgMnPc1d1	Santin
obvykle	obvykle	k6eAd1	obvykle
konstruoval	konstruovat	k5eAaImAgMnS	konstruovat
své	svůj	k3xOyFgFnPc4	svůj
stavby	stavba	k1gFnPc4	stavba
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
kružítka	kružítko	k1gNnSc2	kružítko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
budoval	budovat	k5eAaImAgMnS	budovat
celou	celý	k2eAgFnSc4d1	celá
stavbu	stavba	k1gFnSc4	stavba
pomocí	pomocí	k7c2	pomocí
úseků	úsek	k1gInPc2	úsek
kružnic	kružnice	k1gFnPc2	kružnice
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
poloměrem	poloměr	k1gInSc7	poloměr
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
násobky	násobek	k1gInPc4	násobek
základního	základní	k2eAgInSc2d1	základní
modulu	modul	k1gInSc2	modul
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
přitom	přitom	k6eAd1	přitom
určitých	určitý	k2eAgFnPc2d1	určitá
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
ikonografický	ikonografický	k2eAgInSc4d1	ikonografický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
stavbách	stavba	k1gFnPc6	stavba
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
čísla	číslo	k1gNnPc1	číslo
tři	tři	k4xCgInPc4	tři
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
dvanáct	dvanáct	k4xCc4	dvanáct
atp.	atp.	kA	atp.
Na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vedle	vedle	k7c2	vedle
čísla	číslo	k1gNnSc2	číslo
pět	pět	k4xCc4	pět
ještě	ještě	k9	ještě
číslo	číslo	k1gNnSc1	číslo
tři	tři	k4xCgNnPc1	tři
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
Boží	boží	k2eAgFnSc4d1	boží
Trojici	trojice	k1gFnSc4	trojice
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hvězd	hvězda	k1gFnPc2	hvězda
číslo	číslo	k1gNnSc1	číslo
šest	šest	k4xCc1	šest
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
odkazem	odkaz	k1gInSc7	odkaz
mariánským	mariánský	k2eAgInSc7d1	mariánský
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
byl	být	k5eAaImAgInS	být
chápán	chápat	k5eAaImNgInS	chápat
i	i	k8xC	i
jako	jako	k8xC	jako
obzvláštní	obzvláštní	k2eAgMnSc1d1	obzvláštní
ctitel	ctitel	k1gMnSc1	ctitel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
střídá	střídat	k5eAaImIp3nS	střídat
pět	pět	k4xCc4	pět
hrotitých	hrotitý	k2eAgFnPc2d1	hrotitá
kaplí	kaple	k1gFnPc2	kaple
s	s	k7c7	s
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
půdorysem	půdorys	k1gInSc7	půdorys
a	a	k8xC	a
pět	pět	k4xCc4	pět
kaplí	kaple	k1gFnPc2	kaple
oválných	oválný	k2eAgFnPc2d1	oválná
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
kostel	kostel	k1gInSc1	kostel
přitom	přitom	k6eAd1	přitom
svou	svůj	k3xOyFgFnSc7	svůj
konstrukcí	konstrukce	k1gFnSc7	konstrukce
připomene	připomenout	k5eAaPmIp3nS	připomenout
řez	řez	k1gInSc1	řez
chórem	chór	k1gInSc7	chór
gotické	gotický	k2eAgFnSc2d1	gotická
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
opěrným	opěrný	k2eAgInSc7d1	opěrný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Centralita	Centralita	k1gFnSc1	Centralita
kostela	kostel	k1gInSc2	kostel
pak	pak	k6eAd1	pak
buduje	budovat	k5eAaImIp3nS	budovat
vertikálu	vertikála	k1gFnSc4	vertikála
jako	jako	k8xS	jako
určitý	určitý	k2eAgInSc4d1	určitý
symbolický	symbolický	k2eAgInSc4d1	symbolický
poukaz	poukaz	k1gInSc4	poukaz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vertikála	vertikála	k1gFnSc1	vertikála
totiž	totiž	k9	totiž
nutně	nutně	k6eAd1	nutně
táhne	táhnout	k5eAaImIp3nS	táhnout
pohled	pohled	k1gInSc1	pohled
návštěvníků	návštěvník	k1gMnPc2	návštěvník
vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
prostoru	prostor	k1gInSc6	prostor
kostela	kostel	k1gInSc2	kostel
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgFnPc4d1	oválná
obvodové	obvodový	k2eAgFnPc4d1	obvodová
kaple	kaple	k1gFnPc4	kaple
spojeny	spojen	k2eAgFnPc4d1	spojena
s	s	k7c7	s
ústředním	ústřední	k2eAgInSc7d1	ústřední
válcovým	válcový	k2eAgInSc7d1	válcový
prostorem	prostor	k1gInSc7	prostor
užšími	úzký	k2eAgInPc7d2	užší
lomenými	lomený	k2eAgInPc7d1	lomený
oblouky	oblouk	k1gInPc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
zaklenut	zaklenout	k5eAaPmNgInS	zaklenout
kupolí	kupole	k1gFnSc7	kupole
s	s	k7c7	s
lunetami	luneta	k1gFnPc7	luneta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nesena	nést	k5eAaImNgFnS	nést
deseti	deset	k4xCc7	deset
pilíři	pilíř	k1gInPc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
jsou	být	k5eAaImIp3nP	být
tribuny	tribuna	k1gFnPc4	tribuna
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
přízemním	přízemní	k2eAgFnPc3d1	přízemní
kaplím	kaple	k1gFnPc3	kaple
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
kolem	kolem	k7c2	kolem
paty	pata	k1gFnSc2	pata
klenby	klenba	k1gFnSc2	klenba
desetidílná	desetidílný	k2eAgFnSc1d1	desetidílná
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Štuková	štukový	k2eAgFnSc1d1	štuková
dekorace	dekorace	k1gFnSc1	dekorace
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
protínajících	protínající	k2eAgMnPc2d1	protínající
se	se	k3xPyFc4	se
a	a	k8xC	a
přesekávaných	přesekávaný	k2eAgNnPc2d1	přesekávaný
žeber	žebro	k1gNnPc2	žebro
připomínajících	připomínající	k2eAgNnPc2d1	připomínající
gotické	gotický	k2eAgFnSc2d1	gotická
síťové	síťový	k2eAgFnSc2d1	síťová
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgInS	použít
i	i	k9	i
zde	zde	k6eAd1	zde
Santini	Santin	k2eAgMnPc1d1	Santin
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
prostoru	prostor	k1gInSc2	prostor
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
barokní	barokní	k2eAgFnSc2d1	barokní
interpretace	interpretace	k1gFnSc2	interpretace
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xS	jako
symbolu	symbol	k1gInSc2	symbol
boží	boží	k2eAgFnSc2d1	boží
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
o	o	k7c6	o
"	"	kIx"	"
<g/>
sakrálním	sakrální	k2eAgNnSc6d1	sakrální
světle	světlo	k1gNnSc6	světlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Světelné	světelný	k2eAgInPc1d1	světelný
poměry	poměr	k1gInPc1	poměr
interiéru	interiér	k1gInSc2	interiér
kostela	kostel	k1gInSc2	kostel
působí	působit	k5eAaImIp3nS	působit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
paradox	paradox	k1gInSc1	paradox
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
centrální	centrální	k2eAgInSc1d1	centrální
prostor	prostor	k1gInSc1	prostor
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
osvětlen	osvětlit	k5eAaPmNgInS	osvětlit
pouze	pouze	k6eAd1	pouze
zprostředkovaně	zprostředkovaně	k6eAd1	zprostředkovaně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
boční	boční	k2eAgNnPc4d1	boční
"	"	kIx"	"
<g/>
ochozové	ochozový	k2eAgFnSc6d1	ochozová
<g/>
"	"	kIx"	"
části	část	k1gFnSc6	část
kostela	kostel	k1gInSc2	kostel
hojně	hojně	k6eAd1	hojně
zality	zalít	k5eAaPmNgInP	zalít
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
světlo	světlo	k1gNnSc1	světlo
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
centrálního	centrální	k2eAgInSc2d1	centrální
prostoru	prostor	k1gInSc2	prostor
kostela	kostel	k1gInSc2	kostel
lomenými	lomený	k2eAgInPc7d1	lomený
oblouky	oblouk	k1gInPc7	oblouk
umístěnými	umístěný	k2eAgInPc7d1	umístěný
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
správně	správně	k6eAd1	správně
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgInP	umístit
nosné	nosný	k2eAgInPc1d1	nosný
pilíře	pilíř	k1gInPc1	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
podtržený	podtržený	k2eAgInSc1d1	podtržený
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
paradoxní	paradoxní	k2eAgInSc4d1	paradoxní
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
stěnou	stěna	k1gFnSc7	stěna
a	a	k8xC	a
klenbou	klenba	k1gFnSc7	klenba
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
stěně	stěna	k1gFnSc6	stěna
spočívající	spočívající	k2eAgFnSc6d1	spočívající
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
návštěvníky	návštěvník	k1gMnPc4	návštěvník
působit	působit	k5eAaImF	působit
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
skořápkou	skořápka	k1gFnSc7	skořápka
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
budována	budován	k2eAgFnSc1d1	budována
a	a	k8xC	a
nesena	nesen	k2eAgFnSc1d1	nesena
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
chrám	chrám	k1gInSc1	chrám
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
chápán	chápat	k5eAaImNgInS	chápat
také	také	k9	také
jako	jako	k8xS	jako
jakýsi	jakýsi	k3yIgInSc1	jakýsi
relikviář	relikviář	k1gInSc1	relikviář
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
relikvie	relikvie	k1gFnSc1	relikvie
jazyka	jazyk	k1gInSc2	jazyk
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
arkády	arkáda	k1gFnSc2	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
oltáře	oltář	k1gInSc2	oltář
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
zábradlí	zábradlí	k1gNnSc3	zábradlí
galerie	galerie	k1gFnSc2	galerie
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Řezby	řezba	k1gFnSc2	řezba
pěti	pět	k4xCc2	pět
andělů	anděl	k1gMnPc2	anděl
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
počtem	počet	k1gInSc7	počet
opět	opět	k6eAd1	opět
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Nepomuckému	Nepomucký	k1gMnSc3	Nepomucký
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
evangelisté	evangelista	k1gMnPc1	evangelista
jsou	být	k5eAaImIp3nP	být
prací	práce	k1gFnSc7	práce
chrudimského	chrudimský	k2eAgMnSc2d1	chrudimský
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
Čechpauera	Čechpauer	k1gMnSc2	Čechpauer
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1725	[number]	k4	1725
<g/>
-	-	kIx~	-
<g/>
1727	[number]	k4	1727
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
andělů	anděl	k1gMnPc2	anděl
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
nesou	nést	k5eAaImIp3nP	nést
kouli	koule	k1gFnSc4	koule
(	(	kIx(	(
<g/>
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
klenbu	klenba	k1gFnSc4	klenba
<g/>
)	)	kIx)	)
ozdobenou	ozdobený	k2eAgFnSc4d1	ozdobená
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
<g/>
)	)	kIx)	)
pěti	pět	k4xCc7	pět
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kouli	koule	k1gFnSc4	koule
stojí	stát	k5eAaImIp3nS	stát
postava	postava	k1gFnSc1	postava
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
plastika	plastika	k1gFnSc1	plastika
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
Řehoře	Řehoř	k1gMnSc2	Řehoř
Thenyho	Theny	k1gMnSc2	Theny
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
prací	práce	k1gFnSc7	práce
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
reliéfy	reliéf	k1gInPc4	reliéf
na	na	k7c6	na
nosítkách	nosítka	k1gNnPc6	nosítka
pro	pro	k7c4	pro
poutní	poutní	k2eAgFnSc4d1	poutní
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
sochu	socha	k1gFnSc4	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
od	od	k7c2	od
pražského	pražský	k2eAgMnSc2d1	pražský
zlatníka	zlatník	k1gMnSc2	zlatník
J.	J.	kA	J.
Diesbacha	Diesbach	k1gMnSc2	Diesbach
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
žel	žel	k9	žel
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
dílně	dílna	k1gFnSc3	dílna
Matyáše	Matyáš	k1gMnSc2	Matyáš
Bernarda	Bernard	k1gMnSc2	Bernard
Brauna	Braun	k1gMnSc2	Braun
(	(	kIx(	(
<g/>
Thény	Théna	k1gMnSc2	Théna
byl	být	k5eAaImAgMnS	být
Braunovým	Braunův	k2eAgMnSc7d1	Braunův
tovaryšem	tovaryš	k1gMnSc7	tovaryš
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
nebyla	být	k5eNaImAgFnS	být
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Santini	Santin	k2eAgMnPc1d1	Santin
mnohokrát	mnohokrát	k6eAd1	mnohokrát
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
realizacích	realizace	k1gFnPc6	realizace
právě	právě	k6eAd1	právě
s	s	k7c7	s
Braunem	Braun	k1gMnSc7	Braun
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
sochařský	sochařský	k2eAgInSc1d1	sochařský
jazyk	jazyk	k1gInSc1	jazyk
mu	on	k3xPp3gMnSc3	on
zjevně	zjevně	k6eAd1	zjevně
velmi	velmi	k6eAd1	velmi
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
na	na	k7c6	na
půdoryse	půdorys	k1gInSc6	půdorys
složeném	složený	k2eAgInSc6d1	složený
z	z	k7c2	z
deseti	deset	k4xCc2	deset
úseků	úsek	k1gInPc2	úsek
kružnic	kružnice	k1gFnPc2	kružnice
prstenec	prstenec	k1gInSc1	prstenec
ambitů	ambit	k1gInPc2	ambit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pročleněn	pročleněn	k2eAgInSc1d1	pročleněn
pěti	pět	k4xCc7	pět
pětiúhelnými	pětiúhelný	k2eAgFnPc7d1	pětiúhelný
kaplemi	kaple	k1gFnPc7	kaple
a	a	k8xC	a
pěti	pět	k4xCc7	pět
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Střechy	střecha	k1gFnPc1	střecha
kaplí	kaple	k1gFnPc2	kaple
původně	původně	k6eAd1	původně
vrcholily	vrcholit	k5eAaImAgFnP	vrcholit
pěti	pět	k4xCc7	pět
pylony	pylon	k1gInPc7	pylon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
opět	opět	k6eAd1	opět
poukazovaly	poukazovat	k5eAaImAgInP	poukazovat
na	na	k7c4	na
význam	význam	k1gInSc4	význam
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
věčnost	věčnost	k1gFnSc4	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ambity	ambit	k1gInPc1	ambit
s	s	k7c7	s
kaplemi	kaple	k1gFnPc7	kaple
sloužily	sloužit	k5eAaImAgFnP	sloužit
pro	pro	k7c4	pro
poutnické	poutnický	k2eAgFnPc4d1	poutnická
modlitby	modlitba	k1gFnPc4	modlitba
i	i	k9	i
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
poutníků	poutník	k1gMnPc2	poutník
před	před	k7c7	před
nepřízní	nepřízeň	k1gFnSc7	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
samotný	samotný	k2eAgInSc1d1	samotný
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tento	tento	k3xDgInSc1	tento
prstenec	prstenec	k1gInSc1	prstenec
ambitů	ambit	k1gInPc2	ambit
svým	svůj	k3xOyFgNnSc7	svůj
architektonickým	architektonický	k2eAgNnSc7d1	architektonické
řešením	řešení	k1gNnSc7	řešení
jasně	jasně	k6eAd1	jasně
dokládá	dokládat	k5eAaImIp3nS	dokládat
Santiniho	Santini	k1gMnSc4	Santini
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
potenci	potence	k1gFnSc4	potence
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1784	[number]	k4	1784
kostel	kostel	k1gInSc1	kostel
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
reálně	reálně	k6eAd1	reálně
hrozilo	hrozit	k5eAaImAgNnS	hrozit
jeho	jeho	k3xOp3gFnSc4	jeho
zničení	zničení	k1gNnSc1	zničení
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
péčí	péče	k1gFnSc7	péče
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
především	především	k6eAd1	především
péčí	péče	k1gFnSc7	péče
P.	P.	kA	P.
Matěje	Matěj	k1gMnSc2	Matěj
Sychry	Sychra	k1gMnSc2	Sychra
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
areál	areál	k1gInSc4	areál
zastřešit	zastřešit	k5eAaPmF	zastřešit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
zachráněn	zachránit	k5eAaPmNgMnS	zachránit
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnSc4d1	budoucí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc4	povolení
obnovit	obnovit	k5eAaPmF	obnovit
kostel	kostel	k1gInSc4	kostel
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
guberniem	gubernium	k1gNnSc7	gubernium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
kostel	kostel	k1gInSc1	kostel
nebude	být	k5eNaImBp3nS	být
již	již	k6eAd1	již
poutní	poutní	k2eAgFnSc1d1	poutní
a	a	k8xC	a
že	že	k8xS	že
sem	sem	k6eAd1	sem
bude	být	k5eAaImBp3nS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
žďárský	žďárský	k2eAgInSc1d1	žďárský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
hřbitově	hřbitov	k1gInSc6	hřbitov
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepohřbívá	pohřbívat	k5eNaImIp3nS	pohřbívat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stěhován	stěhován	k2eAgMnSc1d1	stěhován
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
ambitů	ambit	k1gInPc2	ambit
získal	získat	k5eAaPmAgInS	získat
původní	původní	k2eAgFnSc4d1	původní
barokní	barokní	k2eAgFnSc4d1	barokní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgInP	být
zelenohorské	zelenohorský	k2eAgInPc1d1	zelenohorský
zvony	zvon	k1gInPc1	zvon
zrekvírovány	zrekvírován	k2eAgInPc1d1	zrekvírován
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
zvony	zvon	k1gInPc1	zvon
byly	být	k5eAaImAgInP	být
odlity	odlit	k2eAgInPc1d1	odlit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
a	a	k8xC	a
odlil	odlít	k5eAaPmAgMnS	odlít
je	on	k3xPp3gNnPc4	on
zvonař	zvonař	k1gMnSc1	zvonař
Rudolf	Rudolf	k1gMnSc1	Rudolf
Manoušek	Manoušek	k1gMnSc1	Manoušek
<g/>
:	:	kIx,	:
zvon	zvon	k1gInSc1	zvon
svatý	svatý	k1gMnSc1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
37	[number]	k4	37
kg	kg	kA	kg
<g/>
,	,	kIx,	,
zvon	zvon	k1gInSc1	zvon
svatý	svatý	k1gMnSc1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc6	ladění
g	g	kA	g
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
57	[number]	k4	57
kg	kg	kA	kg
<g/>
,	,	kIx,	,
zvon	zvon	k1gInSc1	zvon
neznámého	známý	k2eNgNnSc2d1	neznámé
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc2	ladění
c	c	k0	c
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Zvony	zvon	k1gInPc1	zvon
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
zrekvírovány	zrekvírován	k2eAgFnPc4d1	zrekvírován
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
odlil	odlít	k5eAaPmAgMnS	odlít
zvonařský	zvonařský	k2eAgMnSc1d1	zvonařský
mistr	mistr	k1gMnSc1	mistr
Petr	Petr	k1gMnSc1	Petr
Rudolf	Rudolf	k1gMnSc1	Rudolf
Manoušek	Manoušek	k1gMnSc1	Manoušek
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Manouška	Manoušek	k1gMnSc2	Manoušek
<g/>
,	,	kIx,	,
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Astenu	Asten	k1gInSc6	Asten
nové	nový	k2eAgInPc4d1	nový
zvony	zvon	k1gInPc4	zvon
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
ladění	ladění	k1gNnSc6	ladění
<g/>
:	:	kIx,	:
zvon	zvon	k1gInSc1	zvon
svatý	svatý	k1gMnSc1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
35	[number]	k4	35
kg	kg	kA	kg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
věžičce	věžička	k1gFnSc6	věžička
ambitu	ambit	k1gInSc2	ambit
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
zvonit	zvonit	k5eAaImF	zvonit
ráno	ráno	k6eAd1	ráno
v	v	k7c4	v
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zvon	zvon	k1gInSc4	zvon
svatá	svatý	k2eAgFnSc1d1	svatá
Zdislava	Zdislava	k1gFnSc1	Zdislava
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc6	ladění
g	g	kA	g
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
60	[number]	k4	60
kg	kg	kA	kg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
věžičce	věžička	k1gFnSc6	věžička
ambitu	ambit	k1gInSc2	ambit
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
zvonit	zvonit	k5eAaImF	zvonit
klekání	klekání	k1gNnSc4	klekání
v	v	k7c4	v
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zvon	zvon	k1gInSc1	zvon
svatí	svatý	k2eAgMnPc1d1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc4	ladění
c	c	k0	c
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
84	[number]	k4	84
kg	kg	kA	kg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
zvonit	zvonit	k5eAaImF	zvonit
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
zvony	zvon	k1gInPc1	zvon
najednou	najednou	k6eAd1	najednou
budou	být	k5eAaImBp3nP	být
zvonit	zvonit	k5eAaImF	zvonit
před	před	k7c7	před
sobotní	sobotní	k2eAgFnSc7d1	sobotní
mší	mše	k1gFnSc7	mše
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stísněným	stísněný	k2eAgInPc3d1	stísněný
prostorům	prostor	k1gInPc3	prostor
ve	v	k7c6	v
věžích	věž	k1gFnPc6	věž
jsou	být	k5eAaImIp3nP	být
zvony	zvon	k1gInPc1	zvon
vybaveny	vybavit	k5eAaPmNgInP	vybavit
elektrickým	elektrický	k2eAgInSc7d1	elektrický
zvonícím	zvonící	k2eAgInSc7d1	zvonící
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
posvěcení	posvěcení	k1gNnSc1	posvěcení
zvonů	zvon	k1gInPc2	zvon
provedl	provést	k5eAaPmAgMnS	provést
brněnský	brněnský	k2eAgMnSc1d1	brněnský
biskup	biskup	k1gMnSc1	biskup
Mons	Monsa	k1gFnPc2	Monsa
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Cikrle	Cikrle	k1gFnSc2	Cikrle
během	během	k7c2	během
svatojanské	svatojanský	k2eAgFnSc2d1	Svatojanská
pouti	pouť	k1gFnSc2	pouť
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
věží	věž	k1gFnPc2	věž
zavěšeny	zavěšen	k2eAgInPc1d1	zavěšen
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vyzvánění	vyzvánění	k1gNnSc1	vyzvánění
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
