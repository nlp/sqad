<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
–	–	k?	–
léta	léto	k1gNnSc2	léto
šedesátá	šedesátý	k4xOgNnPc4	šedesátý
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
box	box	k1gInSc4	box
s	s	k7c7	s
jedenácti	jedenáct	k4xCc2	jedenáct
CD	CD	kA	CD
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc2	reedice
všech	všecek	k3xTgNnPc2	všecek
deseti	deset	k4xCc2	deset
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
Semaforu	Semafor	k1gInSc2	Semafor
vydaných	vydaný	k2eAgInPc2d1	vydaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
doplněná	doplněný	k2eAgFnSc1d1	doplněná
bonusy	bonus	k1gInPc7	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Bonusy	bonus	k1gInPc1	bonus
jsou	být	k5eAaImIp3nP	být
vybrány	vybrat	k5eAaPmNgInP	vybrat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nepřekrývaly	překrývat	k5eNaImAgFnP	překrývat
s	s	k7c7	s
výběry	výběr	k1gInPc7	výběr
ze	z	k7c2	z
série	série	k1gFnSc2	série
Písničky	písnička	k1gFnSc2	písnička
ze	z	k7c2	z
Semaforu	Semafor	k1gInSc2	Semafor
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
CD	CD	kA	CD
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
obalech	obal	k1gInPc6	obal
graficky	graficky	k6eAd1	graficky
užívající	užívající	k2eAgInPc4d1	užívající
původní	původní	k2eAgInPc4d1	původní
obaly	obal	k1gInPc4	obal
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
boxu	box	k1gInSc2	box
je	být	k5eAaImIp3nS	být
také	také	k9	také
sedmdesátistránková	sedmdesátistránkový	k2eAgFnSc1d1	sedmdesátistránková
knížečka	knížečka	k1gFnSc1	knížečka
s	s	k7c7	s
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
textem	text	k1gInSc7	text
Lukáše	Lukáš	k1gMnSc2	Lukáš
Berného	berný	k2eAgMnSc2d1	berný
a	a	k8xC	a
obrázky	obrázek	k1gInPc4	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Doplňující	doplňující	k2eAgInSc4d1	doplňující
text	text	k1gInSc4	text
napsal	napsat	k5eAaPmAgMnS	napsat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Just	just	k6eAd1	just
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
CD	CD	kA	CD
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
–	–	k?	–
Písničky	písnička	k1gFnSc2	písnička
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Plná	plný	k2eAgFnSc1d1	plná
hrst	hrst	k1gFnSc1	hrst
písniček	písnička	k1gFnPc2	písnička
/	/	kIx~	/
Hraj	hrát	k5eAaImRp2nS	hrát
hraj	hrát	k5eAaImRp2nS	hrát
hraj	hrát	k5eAaImRp2nS	hrát
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
+	+	kIx~	+
jako	jako	k8xS	jako
bonus	bonus	k1gInSc4	bonus
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
Zuzana	Zuzana	k1gFnSc1	Zuzana
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
nikoho	nikdo	k3yNnSc4	nikdo
doma	doma	k6eAd1	doma
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
procházka	procházka	k1gFnSc1	procházka
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
pohlednice	pohlednice	k1gFnSc1	pohlednice
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonus	bonus	k1gInSc1	bonus
+	+	kIx~	+
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
bonus	bonus	k1gInSc1	bonus
deska	deska	k1gFnSc1	deska
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
Semaforu	Semafor	k1gInSc2	Semafor
z	z	k7c2	z
kompletu	komplet	k1gInSc2	komplet
Divadla	divadlo	k1gNnSc2	divadlo
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Toulaví	toulavý	k2eAgMnPc1d1	toulavý
zpěváci	zpěvák	k1gMnPc1	zpěvák
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Jonáš	Jonáš	k1gMnSc1	Jonáš
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Matrace	matrace	k1gFnSc1	matrace
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
z	z	k7c2	z
Vinohrad	Vinohrady	k1gInPc2	Vinohrady
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
sedláci	sedlák	k1gMnPc1	sedlák
/	/	kIx~	/
Revizor	revizor	k1gMnSc1	revizor
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
</s>
</p>
<p>
<s>
Sardinka	sardinka	k1gFnSc1	sardinka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
rarity	rarita	k1gFnPc1	rarita
–	–	k?	–
bonusový	bonusový	k2eAgInSc4d1	bonusový
disk	disk	k1gInSc4	disk
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
písniček	písnička	k1gFnPc2	písnička
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
–	–	k?	–
Písničky	písnička	k1gFnSc2	písnička
===	===	k?	===
</s>
</p>
<p>
<s>
Letní	letní	k2eAgInSc1d1	letní
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
Bennie	Bennie	k1gFnSc1	Bennie
Benjamin	Benjamin	k1gMnSc1	Benjamin
a	a	k8xC	a
George	George	k1gInSc1	George
David	David	k1gMnSc1	David
Weiss	Weiss	k1gMnSc1	Weiss
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hovoří	hovořit	k5eAaImIp3nS	hovořit
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
Vlasta	Vlasta	k1gMnSc1	Vlasta
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Sodomovi	Sodomův	k2eAgMnPc1d1	Sodomův
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Bažant	Bažant	k1gMnSc1	Bažant
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Hulan	Hulan	k1gMnSc1	Hulan
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
myška	myška	k1gFnSc1	myška
v	v	k7c6	v
deliriu	delirium	k1gNnSc6	delirium
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
Rogl	Rogl	k1gMnSc1	Rogl
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Metamorfózy	metamorfóza	k1gFnPc1	metamorfóza
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Bažant	Bažant	k1gMnSc1	Bažant
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Hulan	Hulan	k1gMnSc1	Hulan
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
poslední	poslední	k2eAgFnSc4d1	poslední
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Bažant	Bažant	k1gMnSc1	Bažant
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Hulan	Hulan	k1gMnSc1	Hulan
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
žiju	žít	k5eAaImIp1nS	žít
dál	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc5	Jiří
Suchý	Suchý	k1gMnSc5	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc5	Jiří
Suchý	Suchý	k1gMnSc5	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Bažant	Bažant	k1gMnSc1	Bažant
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Hulan	Hulan	k1gMnSc1	Hulan
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
o	o	k7c6	o
zmoklé	zmoklý	k2eAgFnSc6d1	zmoklá
slepici	slepice	k1gFnSc6	slepice
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Barvy	barva	k1gFnPc1	barva
–	–	k?	–
laky	laka	k1gFnSc2	laka
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
kotě	kotě	k1gNnSc4	kotě
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Malý	malý	k2eAgMnSc1d1	malý
blbý	blbý	k2eAgMnSc1d1	blbý
psíček	psíček	k1gMnSc1	psíček
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Želví	želví	k2eAgFnSc1d1	želví
blues	blues	k1gFnSc1	blues
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Svítání	svítání	k1gNnSc1	svítání
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Suchánek	Suchánek	k1gMnSc1	Suchánek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Zlá	zlá	k1gFnSc1	zlá
neděle	neděle	k1gFnSc2	neděle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
o	o	k7c6	o
stabilitě	stabilita	k1gFnSc6	stabilita
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Tu	ten	k3xDgFnSc4	ten
krásu	krása	k1gFnSc4	krása
nelze	lze	k6eNd1	lze
popsat	popsat	k5eAaPmF	popsat
slovy	slovo	k1gNnPc7	slovo
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
V	v	k7c6	v
kašně	kašna	k1gFnSc6	kašna
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
orchestru	orchestr	k1gInSc2	orchestr
Karla	Karel	k1gMnSc2	Karel
Vlachabonusy	Vlachabonus	k1gInPc7	Vlachabonus
</s>
</p>
<p>
<s>
Trilobit	trilobit	k1gMnSc1	trilobit
se	se	k3xPyFc4	se
diví	divit	k5eAaImIp3nS	divit
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Sternwald	Sternwald	k1gMnSc1	Sternwald
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Ljuba	Ljuba	k1gFnSc1	Ljuba
Hermanová	Hermanová	k1gFnSc1	Hermanová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Kaňonem	kaňon	k1gInSc7	kaňon
takhle	takhle	k6eAd1	takhle
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Štědrý	štědrý	k2eAgMnSc1d1	štědrý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Starodávné	starodávný	k2eAgFnPc4d1	starodávná
brýle	brýle	k1gFnPc4	brýle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Škrhola	Škrhola	k1gMnSc1	Škrhola
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Koupil	koupit	k5eAaPmAgMnS	koupit
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
knot	knot	k1gInSc4	knot
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
jak	jak	k8xC	jak
ten	ten	k3xDgMnSc1	ten
Adam	Adam	k1gMnSc1	Adam
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Swing	swing	k1gInSc4	swing
Band	bando	k1gNnPc2	bando
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Vyvěste	vyvěsit	k5eAaPmRp2nP	vyvěsit
fangle	fangle	k1gFnPc4	fangle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Modré	modrý	k2eAgFnPc1d1	modrá
punčochy	punčocha	k1gFnPc1	punčocha
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Klementajn	Klementajn	k1gInSc1	Klementajn
(	(	kIx(	(
<g/>
tradicional	tradicionat	k5eAaImAgInS	tradicionat
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Honky	Honky	k6eAd1	Honky
tonky	tonky	k6eAd1	tonky
blues	blues	k1gNnSc2	blues
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rokl	Rokl	k1gMnSc1	Rokl
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Vobruba	Vobruba	k1gMnSc1	Vobruba
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Zlá	zlá	k1gFnSc1	zlá
neděle	neděle	k1gFnSc2	neděle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Domovina	domovina	k1gFnSc1	domovina
</s>
</p>
<p>
<s>
Purpura	purpura	k1gFnSc1	purpura
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
===	===	k?	===
Plná	plný	k2eAgFnSc1d1	plná
hrst	hrst	k1gFnSc1	hrst
písniček	písnička	k1gFnPc2	písnička
/	/	kIx~	/
Hraj	hrát	k5eAaImRp2nS	hrát
hraj	hrát	k5eAaImRp2nS	hrát
hraj	hrát	k5eAaImRp2nS	hrát
===	===	k?	===
</s>
</p>
<p>
<s>
Plná	plný	k2eAgFnSc1d1	plná
hrst	hrst	k1gFnSc1	hrst
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
myška	myška	k1gFnSc1	myška
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Betty	Betty	k1gFnSc1	Betty
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
V	v	k7c6	v
kašně	kašna	k1gFnSc6	kašna
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Zlá	zlá	k1gFnSc1	zlá
neděle	neděle	k1gFnSc2	neděle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
posedlá	posedlý	k2eAgFnSc1d1	posedlá
tmou	tma	k1gFnSc7	tma
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
o	o	k7c6	o
světle	světlo	k1gNnSc6	světlo
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
Dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zkazil	zkazit	k5eAaPmAgInS	zkazit
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
Duke	Duke	k1gInSc1	Duke
Ellington	Ellington	k1gInSc1	Ellington
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Půl	půl	k6eAd1	půl
párku	párek	k1gInSc2	párek
(	(	kIx(	(
<g/>
Louis	Louis	k1gMnSc1	Louis
Singer	Singer	k1gMnSc1	Singer
a	a	k8xC	a
Hy	hy	k0	hy
Zaret	Zaret	k1gMnSc1	Zaret
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
nejsem	být	k5eNaImIp1nS	být
dítě	dítě	k1gNnSc4	dítě
(	(	kIx(	(
<g/>
Frank	frank	k1gInSc4	frank
Perkins	Perkins	k1gInSc4	Perkins
/	/	kIx~	/
Jiří	Jiří	k1gMnSc5	Jiří
Suchý	Suchý	k1gMnSc5	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jana	Jana	k1gFnSc1	Jana
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
Pozvání	pozvánět	k5eAaImIp3nS	pozvánět
(	(	kIx(	(
<g/>
Cole	cola	k1gFnSc3	cola
Porter	porter	k1gInSc1	porter
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Množení	množení	k1gNnSc1	množení
(	(	kIx(	(
<g/>
Burton	Burton	k1gInSc1	Burton
Lane	Lan	k1gFnSc2	Lan
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
vokální	vokální	k2eAgNnSc1d1	vokální
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Votruba	Votruba	k1gMnSc1	Votruba
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
tu	tu	k6eAd1	tu
ta	ten	k3xDgFnSc1	ten
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Burton	Burton	k1gInSc1	Burton
Lane	Lan	k1gFnSc2	Lan
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Votruba	Votruba	k1gMnSc1	Votruba
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ulici	ulice	k1gFnSc6	ulice
bloumat	bloumat	k5eAaImF	bloumat
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Loesser	Loesser	k1gMnSc1	Loesser
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
Lilka	Lilka	k1gFnSc1	Lilka
Ročáková	Ročáková	k1gFnSc1	Ročáková
a	a	k8xC	a
Vlasta	Vlasta	k1gFnSc1	Vlasta
Kahovcová	Kahovcový	k2eAgFnSc1d1	Kahovcová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Hraj	hrát	k5eAaImRp2nS	hrát
<g/>
,	,	kIx,	,
hraj	hrát	k5eAaImRp2nS	hrát
<g/>
,	,	kIx,	,
hraj	hrát	k5eAaImRp2nS	hrát
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Hutchison	Hutchison	k1gMnSc1	Hutchison
Gabriel	Gabriel	k1gMnSc1	Gabriel
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
dívčí	dívčí	k2eAgInSc1d1	dívčí
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnSc7	svůj
orchestrembonusy	orchestrembonus	k1gInPc1	orchestrembonus
</s>
</p>
<p>
<s>
Říkají	říkat	k5eAaImIp3nP	říkat
lidé	člověk	k1gMnPc1	člověk
někteří	některý	k3yIgMnPc1	některý
(	(	kIx(	(
<g/>
Henry	Henry	k1gMnSc1	Henry
Marshall	Marshall	k1gMnSc1	Marshall
a	a	k8xC	a
Stanley	Stanle	k2eAgFnPc4d1	Stanle
Murphy	Murpha	k1gFnPc4	Murpha
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Kubernát	Kubernát	k1gInSc4	Kubernát
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Moritat	Moritat	k5eAaImF	Moritat
(	(	kIx(	(
<g/>
Kurt	Kurt	k1gMnSc1	Kurt
Weill	Weill	k1gMnSc1	Weill
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Komorní	komorní	k2eAgInSc1d1	komorní
instrumentální	instrumentální	k2eAgInSc1d1	instrumentální
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Pinkas	Pinkas	k1gMnSc1	Pinkas
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Votruba	Votruba	k1gMnSc1	Votruba
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Oči	oko	k1gNnPc4	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgFnSc2d1	zavátá
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Votruba	Votruba	k1gMnSc1	Votruba
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Sáně	sáně	k1gFnPc1	sáně
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Pavel	Pavel	k1gMnSc1	Pavel
Sedláček	Sedláček	k1gMnSc1	Sedláček
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Big	Big	k1gFnSc1	Big
Beatová	beatový	k2eAgFnSc1d1	beatová
skupina	skupina	k1gFnSc1	skupina
Divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
(	(	kIx(	(
<g/>
jonika	jonika	k1gFnSc1	jonika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Motýl	motýl	k1gMnSc1	motýl
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jana	Jana	k1gFnSc1	Jana
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Zdvořilý	zdvořilý	k2eAgInSc4d1	zdvořilý
Woody	Wood	k1gInPc4	Wood
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Semafor	Semafor	k1gInSc1	Semafor
</s>
</p>
<p>
<s>
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
<g/>
,	,	kIx,	,
verze	verze	k1gFnSc1	verze
bez	bez	k7c2	bez
textu	text	k1gInSc2	text
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Smíšený	smíšený	k2eAgInSc1d1	smíšený
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
===	===	k?	===
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
===	===	k?	===
</s>
</p>
<p>
<s>
Babetta	Babetta	k1gFnSc1	Babetta
–	–	k?	–
pochod	pochod	k1gInSc1	pochod
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Rytmická	rytmický	k2eAgFnSc1d1	rytmická
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rokl	Rokl	k1gMnSc1	Rokl
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
opeře	opera	k1gFnSc6	opera
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
diriguje	dirigovat	k5eAaImIp3nS	dirigovat
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
Kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
lodí	loď	k1gFnSc7	loď
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Kysilka	Kysilka	k1gFnSc1	Kysilka
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Míč	míč	k1gInSc1	míč
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Kühnův	Kühnův	k2eAgInSc1d1	Kühnův
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
abyste	aby	kYmCp2nP	aby
to	ten	k3xDgNnSc1	ten
věděla	vědět	k5eAaImAgFnS	vědět
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
</s>
</p>
<p>
<s>
Glory	Glora	k1gFnPc1	Glora
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
a	a	k8xC	a
dívčí	dívčí	k2eAgInSc1d1	dívčí
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
Babetta	Babetta	k1gFnSc1	Babetta
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Satchmo	Satchmo	k6eAd1	Satchmo
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Dotýkat	dotýkat	k5eAaImF	dotýkat
se	se	k3xPyFc4	se
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
Choď	chodit	k5eAaImRp2nS	chodit
po	po	k7c6	po
špičkách	špička	k1gFnPc6	špička
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rokl	Rokl	k1gMnSc1	Rokl
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Hulan	Hulan	k1gMnSc1	Hulan
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Dominák	Dominák	k1gMnSc1	Dominák
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spím	spát	k5eAaImIp1nS	spát
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
,	,	kIx,	,
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
verze	verze	k1gFnSc1	verze
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázdabonusy	Brázdabonus	k1gInPc4	Brázdabonus
<g/>
:	:	kIx,	:
Zuzana	Zuzana	k1gFnSc1	Zuzana
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
nikoho	nikdo	k3yNnSc4	nikdo
doma	doma	k6eAd1	doma
(	(	kIx(	(
<g/>
živě	živě	k6eAd1	živě
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
blues	blues	k1gNnSc4	blues
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jana	Jan	k1gMnSc4	Jan
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Theo	Thea	k1gFnSc5	Thea
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
tě	ty	k3xPp2nSc4	ty
mám	mít	k5eAaImIp1nS	mít
ráda	rád	k2eAgMnSc4d1	rád
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc5	Jiří
Suchý	Suchý	k1gMnSc5	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc5	Jiří
Suchý	Suchý	k1gMnSc5	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jana	Jana	k1gFnSc1	Jana
Malknechtová	Malknechtový	k2eAgFnSc1d1	Malknechtový
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Barová	barový	k2eAgFnSc1d1	barová
lavice	lavice	k1gFnSc1	lavice
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
lodí	loď	k1gFnSc7	loď
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovský	k2eAgFnSc1d1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Oči	oko	k1gNnPc4	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgFnSc2d1	zavátá
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Zdvořilý	zdvořilý	k2eAgInSc4d1	zdvořilý
Woody	Wood	k1gInPc4	Wood
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
nikoho	nikdo	k3yNnSc4	nikdo
doma	doma	k6eAd1	doma
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovská	k1gFnSc1	Filipovská
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
===	===	k?	===
Dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
procházka	procházka	k1gFnSc1	procházka
===	===	k?	===
</s>
</p>
<p>
<s>
Předehra	předehra	k1gFnSc1	předehra
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
<g/>
:	:	kIx,	:
Aurea	Aure	k2eAgFnSc1d1	Aurea
Prima	prima	k1gFnSc1	prima
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
/	/	kIx~	/
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
skupinou	skupina	k1gFnSc7	skupina
</s>
</p>
<p>
<s>
Nevyplacený	vyplacený	k2eNgInSc1d1	nevyplacený
blues	blues	k1gInSc1	blues
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Štrudl	Štrudl	k1gMnSc1	Štrudl
(	(	kIx(	(
<g/>
sopránsaxofon	sopránsaxofon	k1gInSc1	sopránsaxofon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
krajina	krajina	k1gFnSc1	krajina
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
dusno	dusno	k6eAd1	dusno
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
švarná	švarný	k2eAgFnSc1d1	švarná
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Haleluja	haleluja	k0	haleluja
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Good	Good	k1gMnSc1	Good
Bye	Bye	k1gMnSc1	Bye
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnSc7	svůj
orchestrembonusy	orchestrembonus	k1gInPc1	orchestrembonus
</s>
</p>
<p>
<s>
Klokočí	klokočí	k2eAgFnSc1d1	klokočí
<g/>
,	,	kIx,	,
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
verze	verze	k1gFnSc1	verze
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Trio	trio	k1gNnSc1	trio
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Alfa	alfa	k1gFnSc1	alfa
–	–	k?	–
Lo	Lo	k1gFnSc1	Lo
</s>
</p>
<p>
<s>
Zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Štědrý	štědrý	k2eAgMnSc1d1	štědrý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Jirmal	Jirmal	k1gMnSc1	Jirmal
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Tralala	Tralat	k5eAaImAgFnS	Tralat
(	(	kIx(	(
<g/>
Johnny	Johnna	k1gMnSc2	Johnna
Parker	Parker	k1gMnSc1	Parker
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Pavlína	Pavlína	k1gFnSc1	Pavlína
Filipovská	Filipovský	k2eAgFnSc1d1	Filipovská
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Jirmal	Jirmal	k1gMnSc1	Jirmal
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Miluj	milovat	k5eAaImRp2nS	milovat
mne	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
opusť	opustit	k5eAaPmRp2nS	opustit
(	(	kIx(	(
<g/>
Walter	Walter	k1gMnSc1	Walter
Donaldson	Donaldson	k1gMnSc1	Donaldson
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Štědrý	štědrý	k2eAgMnSc1d1	štědrý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kuník	Kuník	k1gMnSc1	Kuník
(	(	kIx(	(
<g/>
tenorsaxofon	tenorsaxofon	k1gInSc1	tenorsaxofon
<g/>
)	)	kIx)	)
a	a	k8xC	a
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
povídá	povídat	k5eAaImIp3nS	povídat
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Bohumil	Bohumil	k1gMnSc1	Bohumil
Zoula	Zoul	k1gMnSc2	Zoul
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc1	housle
<g/>
)	)	kIx)	)
a	a	k8xC	a
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Amfora	amfora	k1gFnSc1	amfora
<g/>
,	,	kIx,	,
lexikon	lexikon	k1gInSc1	lexikon
a	a	k8xC	a
preparát	preparát	k1gInSc1	preparát
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Vobruba	Vobruba	k1gMnSc1	Vobruba
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
zhaslo	zhasnout	k5eAaPmAgNnS	zhasnout
dojetím	dojetí	k1gNnSc7	dojetí
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Stárnutí	stárnutí	k1gNnSc1	stárnutí
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Duba	Duba	k1gMnSc1	Duba
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
tetu	teta	k1gFnSc4	teta
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc4	Kobylisy
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
koní	kůň	k1gMnPc2	kůň
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Kühnův	Kühnův	k2eAgInSc1d1	Kühnův
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
Markéta	Markéta	k1gFnSc1	Markéta
Kühnová	Kühnová	k1gFnSc1	Kühnová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Milana	Milan	k1gMnSc2	Milan
Dvořáka	Dvořák	k1gMnSc2	Dvořák
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc7	Kobylisy
</s>
</p>
<p>
<s>
Cecílie	Cecílie	k1gFnSc1	Cecílie
(	(	kIx(	(
<g/>
Irving	Irving	k1gInSc1	Irving
Caesar	Caesar	k1gMnSc1	Caesar
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Nerýmovaná	rýmovaný	k2eNgFnSc1d1	nerýmovaná
(	(	kIx(	(
<g/>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Ducháč	Ducháč	k1gMnSc1	Ducháč
a	a	k8xC	a
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hála	Hála	k1gMnSc1	Hála
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Na	na	k7c6	na
louce	louka	k1gFnSc6	louka
zpívaj	zpívaj	k?	zpívaj
drozdi	drozd	k1gMnPc5	drozd
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Hartlová	Hartlová	k1gFnSc1	Hartlová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Ba	ba	k9	ba
ne	ne	k9	ne
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jana	Jan	k1gMnSc4	Jan
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Modré	modrý	k2eAgFnPc1d1	modrá
džínsy	džínsy	k1gFnPc1	džínsy
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Vlny	vlna	k1gFnPc1	vlna
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Kytky	kytka	k1gFnPc1	kytka
se	se	k3xPyFc4	se
smály	smát	k5eAaImAgFnP	smát
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
===	===	k?	===
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
pohlednice	pohlednice	k1gFnSc1	pohlednice
===	===	k?	===
</s>
</p>
<p>
<s>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
pohlednice	pohlednice	k1gFnSc1	pohlednice
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
nájemník	nájemník	k1gMnSc1	nájemník
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Magda	Magda	k1gFnSc1	Magda
Křížková	křížkový	k2eAgFnSc1d1	Křížková
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Tommy	Tomma	k1gFnPc1	Tomma
(	(	kIx(	(
<g/>
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
kovbojská	kovbojský	k2eAgFnSc1d1	kovbojská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Grossmann	Grossmann	k1gMnSc1	Grossmann
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Country	country	k2eAgInSc1d1	country
Beat	beat	k1gInSc1	beat
Jiřího	Jiří	k1gMnSc2	Jiří
Brabce	Brabec	k1gMnSc2	Brabec
</s>
</p>
<p>
<s>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
tramvaj	tramvaj	k1gFnSc1	tramvaj
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Venku	venku	k6eAd1	venku
se	se	k3xPyFc4	se
zvolna	zvolna	k6eAd1	zvolna
šeří	šeřit	k5eAaImIp3nS	šeřit
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
dárků	dárek	k1gInPc2	dárek
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
ráda	rád	k2eAgFnSc1d1	ráda
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdy	Brázda	k1gMnSc2	Brázda
</s>
</p>
<p>
<s>
Purpura	purpura	k1gFnSc1	purpura
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Dalibora	Dalibor	k1gMnSc2	Dalibor
Brázdybonusy	Brázdybonus	k1gInPc4	Brázdybonus
</s>
</p>
<p>
<s>
Klokočí	klokočí	k2eAgFnSc1d1	klokočí
<g/>
,	,	kIx,	,
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
verze	verze	k1gFnSc1	verze
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nahrání	nahrání	k1gNnSc2	nahrání
neznámé	známý	k2eNgFnSc2d1	neznámá
</s>
</p>
<p>
<s>
Dítě	Dítě	k2eAgFnSc7d1	Dítě
školou	škola	k1gFnSc7	škola
povinné	povinný	k2eAgNnSc4d1	povinné
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Vokální	vokální	k2eAgInSc1d1	vokální
kvintet	kvintet	k1gInSc1	kvintet
Divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc4	fragment
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
</s>
</p>
<p>
<s>
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Lucerna	lucerna	k1gFnSc1	lucerna
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
rose	rosa	k1gFnSc6	rosa
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Lucerna	lucerna	k1gFnSc1	lucerna
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
koni	kůň	k1gMnSc6	kůň
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
/	/	kIx~	/
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc4	fragment
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
monolog	monolog	k1gInSc1	monolog
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Lucerna	lucerna	k1gFnSc1	lucerna
</s>
</p>
<p>
<s>
Kapka	kapka	k1gFnSc1	kapka
žárlivosti	žárlivost	k1gFnSc2	žárlivost
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc5	Kobylisy
</s>
</p>
<p>
<s>
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc4	fragment
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
monolog	monolog	k1gInSc1	monolog
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Lucerna	lucerna	k1gFnSc1	lucerna
</s>
</p>
<p>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
pentle	pentle	k1gFnSc1	pentle
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc5	Kobylisy
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
kotě	kotě	k1gNnSc4	kotě
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
mi	já	k3xPp1nSc3	já
ta	ten	k3xDgNnPc1	ten
jistota	jistota	k1gFnSc1	jistota
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Jonáš	Jonáš	k1gMnSc1	Jonáš
a	a	k8xC	a
tingl-tangl	tinglangl	k1gMnSc1	tingl-tangl
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc1	fragment
z	z	k7c2	z
představení	představení	k1gNnSc2	představení
</s>
</p>
<p>
<s>
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Semafor	Semafor	k1gInSc1	Semafor
</s>
</p>
<p>
<s>
Tulipán	tulipán	k1gMnSc1	tulipán
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Labutí	labutí	k2eAgFnSc1d1	labutí
píseň	píseň	k1gFnSc1	píseň
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Vlasta	Vlasta	k1gFnSc1	Vlasta
Kahovcová	Kahovcový	k2eAgFnSc1d1	Kahovcová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Karel	Karel	k1gMnSc1	Karel
Krautgartner	Krautgartner	k1gMnSc1	Krautgartner
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
o	o	k7c6	o
světle	světlo	k1gNnSc6	světlo
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
tympány	tympán	k1gInPc4	tympán
<g/>
:	:	kIx,	:
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Turnovský	turnovský	k2eAgMnSc1d1	turnovský
(	(	kIx(	(
<g/>
tympány	tympán	k1gInPc1	tympán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
</s>
</p>
<p>
<s>
===	===	k?	===
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
===	===	k?	===
</s>
</p>
<p>
<s>
Kočičí	kočičí	k2eAgInSc1d1	kočičí
bál	bál	k1gInSc1	bál
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Balada	balada	k1gFnSc1	balada
pod	pod	k7c7	pod
šibenicí	šibenice	k1gFnSc7	šibenice
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Kolena	koleno	k1gNnPc1	koleno
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Poklad	poklad	k1gInSc1	poklad
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
miluji	milovat	k5eAaImIp1nS	milovat
vás	vy	k3xPp2nPc4	vy
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
</s>
</p>
<p>
<s>
Mordsong	Mordsong	k1gInSc1	Mordsong
<g/>
,	,	kIx,	,
instrumentální	instrumentální	k2eAgInPc1d1	instrumentální
</s>
</p>
<p>
<s>
hraje	hrát	k5eAaImIp3nS	hrát
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníčekbonusy	Koníčekbonus	k1gInPc4	Koníčekbonus
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
šetří	šetřit	k5eAaImIp3nS	šetřit
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
tři	tři	k4xCgMnPc1	tři
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
netuší	tušit	k5eNaImIp3nS	tušit
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Madony	Madona	k1gFnPc1	Madona
na	na	k7c6	na
kolotoči	kolotoč	k1gInSc6	kolotoč
(	(	kIx(	(
<g/>
Zygmunt	Zygmunt	k1gInSc1	Zygmunt
Konieczny	Konieczna	k1gFnSc2	Konieczna
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Barová	barový	k2eAgFnSc1d1	barová
lavice	lavice	k1gFnSc1	lavice
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Blázen	blázen	k1gMnSc1	blázen
a	a	k8xC	a
dítě	dítě	k1gNnSc1	dítě
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Písnička	písnička	k1gFnSc1	písnička
pro	pro	k7c4	pro
kočku	kočka	k1gFnSc4	kočka
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrata	vrata	k1gNnPc4	vrata
přibili	přibít	k5eAaPmAgMnP	přibít
můj	můj	k3xOp1gInSc4	můj
stín	stín	k1gInSc4	stín
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
slečno	slečna	k1gFnSc5	slečna
<g/>
,	,	kIx,	,
nedělejte	dělat	k5eNaImRp2nP	dělat
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Strahov	Strahov	k1gInSc1	Strahov
</s>
</p>
<p>
<s>
Nedělejte	dělat	k5eNaImRp2nP	dělat
ze	z	k7c2	z
mě	já	k3xPp1nSc2	já
chudinku	chudinka	k1gFnSc4	chudinka
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Francis	Francis	k1gFnSc1	Francis
Lai	Lai	k1gFnSc1	Lai
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vokály	vokál	k1gInPc4	vokál
Eva	Eva	k1gFnSc1	Eva
Olmerová	Olmerová	k1gFnSc1	Olmerová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Obrať	obrátit	k5eAaPmRp2nS	obrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Helena	Helena	k1gFnSc1	Helena
Blehárová	Blehárový	k2eAgFnSc1d1	Blehárová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Ukrejvám	Ukrejvat	k5eAaPmIp1nS	Ukrejvat
rozpaky	rozpak	k1gInPc4	rozpak
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
a	a	k8xC	a
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Karla	Karel	k1gMnSc2	Karel
Krautgartnera	Krautgartner	k1gMnSc2	Krautgartner
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Vobruba	Vobruba	k1gMnSc1	Vobruba
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
zhaslo	zhasnout	k5eAaPmAgNnS	zhasnout
dojetím	dojetí	k1gNnSc7	dojetí
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Eva	Eva	k1gFnSc1	Eva
Olmerová	Olmerový	k2eAgFnSc1d1	Olmerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
larydou	laryda	k1gMnSc7	laryda
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Růže	růže	k1gFnSc1	růže
růžová	růžový	k2eAgFnSc1d1	růžová
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Karla	Karel	k1gMnSc2	Karel
Krautgartnera	Krautgartner	k1gMnSc2	Krautgartner
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Vobruba	Vobruba	k1gMnSc1	Vobruba
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
tričko	tričko	k1gNnSc4	tričko
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
karty	karta	k1gFnSc2	karta
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vodička	Vodička	k1gMnSc1	Vodička
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Marta	Marta	k1gFnSc1	Marta
Kubišová	Kubišová	k1gFnSc1	Kubišová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Taneční	taneční	k2eAgInSc1d1	taneční
orchestr	orchestr	k1gInSc1	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nahrání	nahrání	k1gNnSc2	nahrání
neznámé	známý	k2eNgFnSc2d1	neznámá
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nahrání	nahrání	k1gNnSc2	nahrání
neznámé	známý	k2eNgFnSc2d1	neznámá
</s>
</p>
<p>
<s>
===	===	k?	===
Toulaví	toulavý	k2eAgMnPc1d1	toulavý
zpěváci	zpěvák	k1gMnPc1	zpěvák
===	===	k?	===
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Hamleta	Hamlet	k1gMnSc2	Hamlet
o	o	k7c6	o
hvězdách	hvězda	k1gFnPc6	hvězda
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
pro	pro	k7c4	pro
Hamleta	Hamlet	k1gMnSc4	Hamlet
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrata	vrata	k1gNnPc4	vrata
přibili	přibít	k5eAaPmAgMnP	přibít
můj	můj	k3xOp1gInSc4	můj
stín	stín	k1gInSc4	stín
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
Máme	mít	k5eAaImIp1nP	mít
rádi	rád	k2eAgMnPc1d1	rád
zvířata	zvíře	k1gNnPc4	zvíře
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Hartlová	Hartlová	k1gFnSc1	Hartlová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
se	se	k3xPyFc4	se
nevyhne	vyhnout	k5eNaPmIp3nS	vyhnout
králi	král	k1gMnSc3	král
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
skupina	skupina	k1gFnSc1	skupina
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
</s>
</p>
<p>
<s>
Můra	můra	k1gFnSc1	můra
šedivá	šedivý	k2eAgFnSc1d1	šedivá
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
</s>
</p>
<p>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
Sádlo	sádlo	k1gNnSc1	sádlo
na	na	k7c6	na
chleba	chléb	k1gInSc2	chléb
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
Věštkyně	věštkyně	k1gFnSc1	věštkyně
(	(	kIx(	(
<g/>
Jack	Jack	k1gInSc1	Jack
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Ira	Ir	k1gMnSc2	Ir
Schuster	Schuster	k1gMnSc1	Schuster
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Bažanta	Bažant	k1gMnSc2	Bažant
</s>
</p>
<p>
<s>
Střevlík	střevlík	k1gMnSc1	střevlík
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vodička	Vodička	k1gMnSc1	Vodička
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Bažanta	Bažant	k1gMnSc2	Bažant
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
nálada	nálada	k1gFnSc1	nálada
(	(	kIx(	(
<g/>
Duke	Duke	k1gFnSc1	Duke
Ellington	Ellington	k1gInSc1	Ellington
<g/>
,	,	kIx,	,
Irving	Irving	k1gInSc1	Irving
Mills	Mills	k1gInSc1	Mills
<g/>
,	,	kIx,	,
Albany	Alban	k1gInPc1	Alban
Bigard	Bigard	k1gInSc1	Bigard
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Bažanta	Bažant	k1gMnSc2	Bažant
</s>
</p>
<p>
<s>
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
(	(	kIx(	(
<g/>
Gus	Gus	k1gMnSc1	Gus
Kahn	Kahn	k1gMnSc1	Kahn
<g/>
,	,	kIx,	,
Egbert	Egbert	k1gMnSc1	Egbert
van	vana	k1gFnPc2	vana
Alstyne	Alstyn	k1gInSc5	Alstyn
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Jackson	Jackson	k1gMnSc1	Jackson
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Bažanta	Bažant	k1gMnSc2	Bažant
</s>
</p>
<p>
<s>
Na	na	k7c6	na
rezavým	rezavý	k2eAgFnPc3d1	rezavá
dvojplošníku	dvojplošník	k1gInSc6	dvojplošník
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Hartlová	Hartlová	k1gFnSc1	Hartlová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Potkala	potkat	k5eAaPmAgFnS	potkat
ryba	ryba	k1gFnSc1	ryba
papouška	papoušek	k1gMnSc2	papoušek
(	(	kIx(	(
<g/>
Joe	Joe	k1gFnSc1	Joe
Darion	Darion	k1gInSc1	Darion
<g/>
,	,	kIx,	,
Larry	Larra	k1gFnPc1	Larra
Coleman	Coleman	k1gMnSc1	Coleman
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Gimbel	Gimbel	k1gMnSc1	Gimbel
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Skupina	skupina	k1gFnSc1	skupina
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitrabonusy	Šlitrabonus	k1gInPc4	Šlitrabonus
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
v	v	k7c6	v
životě	život	k1gInSc6	život
chodí	chodit	k5eAaImIp3nS	chodit
(	(	kIx(	(
<g/>
Bei	Bei	k1gFnSc1	Bei
mir	mir	k1gInSc1	mir
bist	bist	k1gMnSc1	bist
du	du	k?	du
schön	schön	k1gMnSc1	schön
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sholom	Sholom	k1gInSc1	Sholom
Sholem	Shol	k1gInSc7	Shol
Secunda	Secunda	k1gFnSc1	Secunda
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
Jacobs	Jacobsa	k1gFnPc2	Jacobsa
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
Chaplin	Chaplin	k1gInSc1	Chaplin
<g/>
,	,	kIx,	,
Sammy	Samm	k1gInPc1	Samm
Cahn	Cahn	k1gInSc1	Cahn
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Havlíka	Havlík	k1gMnSc2	Havlík
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Alfa	alfa	k1gFnSc1	alfa
-	-	kIx~	-
Lo	Lo	k1gFnSc1	Lo
</s>
</p>
<p>
<s>
Neotálej	otálet	k5eNaImRp2nS	otálet
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Zuzana	Zuzana	k1gFnSc1	Zuzana
Burianová	Burianová	k1gFnSc1	Burianová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Tulák	tulák	k1gMnSc1	tulák
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Lenka	Lenka	k1gFnSc1	Lenka
Hartlová	Hartlová	k1gFnSc1	Hartlová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Toulaví	toulavý	k2eAgMnPc1d1	toulavý
zpěváci	zpěvák	k1gMnPc1	zpěvák
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Lenka	Lenka	k1gFnSc1	Lenka
Hartlová	Hartlová	k1gFnSc1	Hartlová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Kamarádi	kamarád	k1gMnPc1	kamarád
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
básníky	básník	k1gMnPc4	básník
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Mozarteum	Mozarteum	k1gInSc1	Mozarteum
</s>
</p>
<p>
<s>
Na	na	k7c6	na
rezavým	rezavý	k2eAgInSc7d1	rezavý
dvojplošníku	dvojplošník	k1gInSc5	dvojplošník
</s>
</p>
<p>
<s>
zpívají	zpívat	k5eAaImIp3nP	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
Hálová	Hálová	k1gFnSc1	Hálová
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Mozarteum	Mozarteum	k1gInSc1	Mozarteum
</s>
</p>
<p>
<s>
Mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
vrabec	vrabec	k1gMnSc1	vrabec
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
blues	blues	k1gFnSc1	blues
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Imperial	Imperial	k1gInSc1	Imperial
blues	blues	k1gNnSc2	blues
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dejvice	Dejvice	k1gFnPc1	Dejvice
</s>
</p>
<p>
<s>
Kučeravý	kučeravý	k2eAgMnSc1d1	kučeravý
listonoš	listonoš	k1gMnSc1	listonoš
(	(	kIx(	(
<g/>
Jerry	Jerra	k1gFnPc1	Jerra
Livingston	Livingston	k1gInSc1	Livingston
<g/>
,	,	kIx,	,
Mack	Mack	k1gMnSc1	Mack
David	David	k1gMnSc1	David
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dukla	Dukla	k1gFnSc1	Dukla
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Odjakživa	odjakživa	k6eAd1	odjakživa
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc4	ten
vábí	vábit	k5eAaImIp3nS	vábit
(	(	kIx(	(
<g/>
Slam	slam	k1gInSc4	slam
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Slim	Slim	k1gMnSc1	Slim
Gaillard	Gaillarda	k1gFnPc2	Gaillarda
<g/>
,	,	kIx,	,
Bud	bouda	k1gFnPc2	bouda
Green	Grena	k1gFnPc2	Grena
/	/	kIx~	/
Josef	Josef	k1gMnSc1	Josef
Kainar	Kainar	k1gMnSc1	Kainar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dukla	Dukla	k1gFnSc1	Dukla
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
===	===	k?	===
Jonáš	Jonáš	k1gMnSc1	Jonáš
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Matrace	matrace	k1gFnSc1	matrace
===	===	k?	===
</s>
</p>
<p>
<s>
Margareta	Margareta	k1gFnSc1	Margareta
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Mississippi	Mississippi	k1gFnSc1	Mississippi
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Kos	Kos	k1gMnSc1	Kos
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Bledá	bledý	k2eAgFnSc1d1	bledá
slečna	slečna	k1gFnSc1	slečna
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Jó	jó	k0	jó
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
žil	žít	k5eAaImAgMnS	žít
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Kubistický	kubistický	k2eAgInSc1d1	kubistický
portrét	portrét	k1gInSc1	portrét
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Dialog	dialog	k1gInSc1	dialog
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitrbonusy	Šlitrbonus	k1gInPc1	Šlitrbonus
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
zní	znět	k5eAaImIp3nS	znět
hrdě	hrdě	k6eAd1	hrdě
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
chce	chtít	k5eAaImIp3nS	chtít
psa	pes	k1gMnSc4	pes
bít	bít	k5eAaImF	bít
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
si	se	k3xPyFc3	se
hůl	hůl	k1gFnSc1	hůl
najde	najít	k5eAaPmIp3nS	najít
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Pamatuju	pamatovat	k5eAaImIp1nS	pamatovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
léto	léto	k1gNnSc4	léto
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Voborníková	Voborníková	k1gFnSc1	Voborníková
</s>
</p>
<p>
<s>
Růžová	růžový	k2eAgFnSc1d1	růžová
známka	známka	k1gFnSc1	známka
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Voborníková	Voborníková	k1gFnSc1	Voborníková
</s>
</p>
<p>
<s>
Vidle	vidle	k1gFnSc1	vidle
–	–	k?	–
Teplé	Teplé	k2eAgNnSc4d1	Teplé
prádlo	prádlo	k1gNnSc4	prádlo
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
J.	J.	kA	J.
Šlitr	Šlitr	k1gInSc1	Šlitr
</s>
</p>
<p>
<s>
Třešně	třešně	k1gFnSc1	třešně
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
M.	M.	kA	M.
Drobný	drobný	k2eAgMnSc1d1	drobný
</s>
</p>
<p>
<s>
Klobouky	Klobouky	k1gInPc1	Klobouky
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Křesadlová	křesadlový	k2eAgFnSc1d1	Křesadlová
</s>
</p>
<p>
<s>
Povídal	povídal	k1gMnSc1	povídal
nám	my	k3xPp1nPc3	my
jeden	jeden	k4xCgMnSc1	jeden
zpěvu	zpěv	k1gInSc2	zpěv
znalec	znalec	k1gMnSc1	znalec
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Křesadlová	křesadlový	k2eAgFnSc1d1	Křesadlová
a	a	k8xC	a
N.	N.	kA	N.
Urbánková	Urbánková	k1gFnSc1	Urbánková
</s>
</p>
<p>
<s>
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Zlomil	zlomit	k5eAaPmAgMnS	zlomit
jsem	být	k5eAaImIp1nS	být
ruku	ruka	k1gFnSc4	ruka
tetičce	tetička	k1gFnSc3	tetička
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Happy	Happa	k1gFnPc1	Happa
End	End	k1gFnSc2	End
</s>
</p>
<p>
<s>
V.	V.	kA	V.
Křesadlová	křesadlový	k2eAgFnSc1d1	Křesadlová
</s>
</p>
<p>
<s>
Zpěváků	Zpěvák	k1gMnPc2	Zpěvák
a	a	k8xC	a
herců	herec	k1gMnPc2	herec
sbory	sbor	k1gInPc1	sbor
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
vám	vy	k3xPp2nPc3	vy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
děj	děj	k1gInSc4	děj
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
plaší	plašit	k5eAaImIp3nP	plašit
zlé	zlý	k2eAgInPc1d1	zlý
sny	sen	k1gInPc1	sen
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Ornament	ornament	k1gInSc1	ornament
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Plameny	plamen	k1gInPc1	plamen
plápolají	plápolat	k5eAaImIp3nP	plápolat
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Nevíme	vědět	k5eNaImIp1nP	vědět
nic	nic	k3yNnSc4	nic
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Blázen	blázen	k1gMnSc1	blázen
a	a	k8xC	a
dítě	dítě	k1gNnSc1	dítě
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
===	===	k?	===
Ďábel	ďábel	k1gMnSc1	ďábel
z	z	k7c2	z
Vinohrad	Vinohrady	k1gInPc2	Vinohrady
===	===	k?	===
</s>
</p>
<p>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
obhajoba	obhajoba	k1gFnSc1	obhajoba
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Propil	propít	k5eAaPmAgMnS	propít
jsem	být	k5eAaImIp1nS	být
gáži	gáže	k1gFnSc4	gáže
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Tragedie	tragedie	k1gFnSc1	tragedie
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Melancholické	melancholický	k2eAgNnSc4d1	melancholické
blues	blues	k1gNnSc4	blues
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Kdybyste	kdyby	kYmCp2nP	kdyby
nás	my	k3xPp1nPc4	my
totiž	totiž	k9	totiž
znali	znát	k5eAaImAgMnP	znát
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zachovat	zachovat	k5eAaPmF	zachovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Bíle	bíle	k6eAd1	bíle
mě	já	k3xPp1nSc4	já
matička	matička	k1gFnSc1	matička
oblékala	oblékat	k5eAaImAgFnS	oblékat
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Strangers	Strangers	k6eAd1	Strangers
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitrbonusy	Šlitrbonus	k1gInPc1	Šlitrbonus
</s>
</p>
<p>
<s>
Golem	Golem	k1gMnSc1	Golem
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
</s>
</p>
<p>
<s>
Krokodýl	krokodýl	k1gMnSc1	krokodýl
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
tety	teta	k1gFnPc1	teta
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
z	z	k7c2	z
Vinohrad	Vinohrady	k1gInPc2	Vinohrady
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgInS	mít
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
</s>
</p>
<p>
<s>
Spánek	spánek	k1gInSc1	spánek
má	mít	k5eAaImIp3nS	mít
strýčka	strýček	k1gMnSc4	strýček
portýra	portýr	k1gMnSc4	portýr
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Pánka	Pánek	k1gMnSc2	Pánek
</s>
</p>
<p>
<s>
Jonáš	Jonáš	k1gMnSc1	Jonáš
a	a	k8xC	a
tingl-tangl	tinglangl	k1gMnSc1	tingl-tangl
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Jonáš	Jonáš	k1gMnSc1	Jonáš
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Matrace	matrace	k1gFnSc1	matrace
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Miloslav	Miloslav	k1gMnSc1	Miloslav
Balcar	Balcar	k1gMnSc1	Balcar
</s>
</p>
<p>
<s>
===	===	k?	===
Básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
sedláci	sedlák	k1gMnPc1	sedlák
/	/	kIx~	/
Revizor	revizor	k1gMnSc1	revizor
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
===	===	k?	===
</s>
</p>
<p>
<s>
Ouvertura	ouvertura	k1gFnSc1	ouvertura
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Ša	Ša	k?	Ša
ba	ba	k9	ba
da	da	k?	da
ba	ba	k9	ba
da	da	k?	da
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
Jegorov	Jegorov	k1gInSc1	Jegorov
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zemek	zemek	k1gMnSc1	zemek
a	a	k8xC	a
František	František	k1gMnSc1	František
Sojka	Sojka	k1gMnSc1	Sojka
</s>
</p>
<p>
<s>
Áda	Áda	k?	Áda
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Jsem	být	k5eAaImIp1nS	být
zakletá	zakletý	k2eAgNnPc5d1	zakleté
</s>
</p>
<p>
<s>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Veselí	veselí	k1gNnSc1	veselí
kumpáni	kumpáni	k?	kumpáni
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
Jegorov	Jegorov	k1gInSc1	Jegorov
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zemek	zemek	k1gMnSc1	zemek
</s>
</p>
<p>
<s>
Básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
sedláci	sedlák	k1gMnPc1	sedlák
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
</s>
</p>
<p>
<s>
Šantánová	šantánový	k2eAgFnSc1d1	šantánová
myš	myš	k1gFnSc1	myš
</s>
</p>
<p>
<s>
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
Cirkus	cirkus	k1gInSc1	cirkus
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Monika	Monika	k1gFnSc1	Monika
Hálová	Hálová	k1gFnSc1	Hálová
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
nastydlejch	nastydlejch	k?	nastydlejch
holek	holka	k1gFnPc2	holka
</s>
</p>
<p>
<s>
Miluška	Miluška	k1gFnSc1	Miluška
Voborníková	Voborníková	k1gFnSc1	Voborníková
a	a	k8xC	a
Petra	Petra	k1gFnSc1	Petra
Černocká	Černocký	k2eAgFnSc1d1	Černocká
</s>
</p>
<p>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
a	a	k8xC	a
růže	růže	k1gFnSc1	růže
</s>
</p>
<p>
<s>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
</s>
</p>
<p>
<s>
Whisky	whisky	k1gFnSc1	whisky
dopita	dopit	k2eAgFnSc1d1	Dopita
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Křesadlová	křesadlový	k2eAgFnSc1d1	Křesadlová
</s>
</p>
<p>
<s>
Končí	končit	k5eAaImIp3nS	končit
divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
Hálová	Hálová	k1gFnSc1	Hálová
a	a	k8xC	a
Semafor	Semafor	k1gInSc4	Semafor
Girlsbonusy	Girlsbonus	k1gMnPc7	Girlsbonus
</s>
</p>
<p>
<s>
Líbej	líbat	k5eAaImRp2nS	líbat
mne	já	k3xPp1nSc2	já
víc	hodně	k6eAd2	hodně
</s>
</p>
<p>
<s>
Miluška	Miluška	k1gFnSc1	Miluška
Voborníková	Voborníková	k1gFnSc1	Voborníková
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
</s>
</p>
<p>
<s>
Miluška	Miluška	k1gFnSc1	Miluška
Voborníková	Voborníková	k1gFnSc1	Voborníková
</s>
</p>
<p>
<s>
Možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ten	ten	k3xDgMnSc1	ten
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Zánovní	zánovní	k2eAgFnSc1d1	zánovní
růže	růže	k1gFnSc1	růže
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
</s>
</p>
<p>
<s>
Jehličí	jehličí	k1gNnSc1	jehličí
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
</s>
</p>
<p>
<s>
Rybí	rybí	k2eAgFnSc1d1	rybí
polévka	polévka	k1gFnSc1	polévka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Margareta	Margareta	k1gFnSc1	Margareta
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
</s>
</p>
<p>
<s>
Jó	jó	k0	jó
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
žil	žít	k5eAaImAgInS	žít
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Kytka	kytka	k1gFnSc1	kytka
v	v	k7c6	v
květináči	květináč	k1gInSc6	květináč
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Sbor	sbor	k1gInSc1	sbor
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
</s>
</p>
<p>
<s>
Kamarádi	kamarád	k1gMnPc1	kamarád
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Ša	Ša	k?	Ša
ba	ba	k9	ba
da	da	k?	da
ba	ba	k9	ba
da	da	k?	da
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
Jegorov	Jegorov	k1gInSc1	Jegorov
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zemek	zemek	k1gMnSc1	zemek
a	a	k8xC	a
František	František	k1gMnSc1	František
Sojka	Sojka	k1gMnSc1	Sojka
</s>
</p>
<p>
<s>
Končí	končit	k5eAaImIp3nS	končit
divadlo	divadlo	k1gNnSc1	divadlo
–	–	k?	–
živě	živě	k6eAd1	živě
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
Hálová	Hálová	k1gFnSc1	Hálová
a	a	k8xC	a
Semafor	Semafor	k1gInSc1	Semafor
Girls	girl	k1gFnPc2	girl
</s>
</p>
<p>
<s>
===	===	k?	===
Sardinka	sardinka	k1gFnSc1	sardinka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
rarity	rarita	k1gFnPc1	rarita
===	===	k?	===
</s>
</p>
<p>
<s>
Léta	léto	k1gNnPc1	léto
dozrávání	dozrávání	k1gNnSc2	dozrávání
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
At	At	k?	At
The	The	k1gFnSc1	The
Jazz	jazz	k1gInSc1	jazz
Band	band	k1gInSc1	band
Ball	Ball	k1gInSc1	Ball
</s>
</p>
<p>
<s>
Czechoslovak	Czechoslovak	k6eAd1	Czechoslovak
Dixieland	dixieland	k1gInSc1	dixieland
Band	banda	k1gFnPc2	banda
</s>
</p>
<p>
<s>
Letní	letní	k2eAgInSc4d1	letní
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
blues	blues	k1gNnSc4	blues
</s>
</p>
<p>
<s>
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Sardinka	sardinka	k1gFnSc1	sardinka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdálo	zdát	k5eAaImAgNnS	zdát
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
nemám	mít	k5eNaImIp1nS	mít
náladu	nálada	k1gFnSc4	nálada
</s>
</p>
<p>
<s>
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
stánek	stánka	k1gFnPc2	stánka
stál	stát	k5eAaImAgInS	stát
</s>
</p>
<p>
<s>
Akord	akord	k1gInSc1	akord
club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
Laterna	laterna	k1gFnSc1	laterna
Magika	magika	k1gFnSc1	magika
Finale	Finala	k1gFnSc3	Finala
</s>
</p>
<p>
<s>
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
skladba	skladba	k1gFnSc1	skladba
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Písnička	písnička	k1gFnSc1	písnička
o	o	k7c6	o
hračkách	hračka	k1gFnPc6	hračka
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Princová	Princový	k2eAgFnSc1d1	Princová
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
raketě	raketa	k1gFnSc6	raketa
</s>
</p>
<p>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
</s>
</p>
<p>
<s>
Pozdrav	pozdrav	k1gInSc1	pozdrav
Gagarinovi	Gagarinův	k2eAgMnPc1d1	Gagarinův
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Potkal	potkat	k5eAaPmAgMnS	potkat
jsem	být	k5eAaImIp1nS	být
jelena	jelen	k1gMnSc4	jelen
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Striptýz	striptýz	k1gInSc1	striptýz
</s>
</p>
<p>
<s>
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
skladba	skladba	k1gFnSc1	skladba
(	(	kIx(	(
<g/>
FISYO	FISYO	kA	FISYO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sádlo	sádlo	k1gNnSc1	sádlo
na	na	k7c6	na
chleba	chléb	k1gInSc2	chléb
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
čtyřtaktní	čtyřtaktní	k2eAgFnSc2d1	čtyřtaktní
</s>
</p>
<p>
<s>
Ljuba	Ljuba	k1gFnSc1	Ljuba
Hermanová	Hermanová	k1gFnSc1	Hermanová
</s>
</p>
<p>
<s>
Takovou	takový	k3xDgFnSc4	takový
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaké	jaký	k3yRgNnSc4	jaký
jsem	být	k5eAaImIp1nS	být
snila	snít	k5eAaImAgFnS	snít
</s>
</p>
<p>
<s>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Horečky	horečka	k1gFnPc1	horečka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Boubelatá	boubelatý	k2eAgFnSc1d1	boubelatá
Betty	Betty	k1gFnSc1	Betty
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
</s>
</p>
<p>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
z	z	k7c2	z
Kutný	kutný	k2eAgInSc4d1	kutný
Hory	hora	k1gFnSc2	hora
koudelníkova	koudelníkův	k2eAgFnSc5d1	koudelníkův
dcera	dcera	k1gFnSc1	dcera
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Křesadlová	křesadlový	k2eAgFnSc1d1	Křesadlová
</s>
</p>
<p>
<s>
Vyznání	vyznání	k1gNnSc1	vyznání
lásky	láska	k1gFnSc2	láska
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
</s>
</p>
<p>
<s>
Siebenpunkt	Siebenpunkt	k1gInSc1	Siebenpunkt
(	(	kIx(	(
<g/>
Sluníčko	sluníčko	k1gNnSc1	sluníčko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aťka	Aťk	k2eAgFnSc1d1	Aťka
Janoušková	Janoušková	k1gFnSc1	Janoušková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Popper	Popper	k1gMnSc1	Popper
</s>
</p>
<p>
<s>
Dass	Dass	k1gInSc1	Dass
sie	sie	k?	sie
erröten	erröten	k2eAgInSc1d1	erröten
kann	kann	k1gInSc1	kann
(	(	kIx(	(
<g/>
Zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
letzte	letzit	k5eAaPmRp2nP	letzit
Schmetterling	Schmetterling	k1gInSc1	Schmetterling
(	(	kIx(	(
<g/>
Motýl	motýl	k1gMnSc1	motýl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Jelínek	Jelínek	k1gMnSc1	Jelínek
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Malknechtová	Malknechtová	k1gFnSc1	Malknechtová
</s>
</p>
<p>
<s>
Snowdrift	Snowdrift	k2eAgInSc1d1	Snowdrift
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
Oči	oko	k1gNnPc4	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgFnSc2d1	zavátá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
</s>
</p>
<p>
<s>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
</s>
</p>
<p>
<s>
Ou	ou	k0	ou
tu	ten	k3xDgFnSc4	ten
vas	vas	k?	vas
(	(	kIx(	(
<g/>
Oči	oko	k1gNnPc4	oko
sněhem	sníh	k1gInSc7	sníh
zaváté	zavátý	k2eAgFnSc2d1	zavátá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nicole	Nicola	k1gFnSc3	Nicola
Felix	Felix	k1gMnSc1	Felix
a	a	k8xC	a
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
KupšovskýPokud	KupšovskýPokud	k1gMnSc1	KupšovskýPokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
autory	autor	k1gMnPc7	autor
písní	píseň	k1gFnPc2	píseň
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
