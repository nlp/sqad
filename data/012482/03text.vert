<p>
<s>
Terminál	terminál	k1gInSc1	terminál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
elektronické	elektronický	k2eAgNnSc4d1	elektronické
nebo	nebo	k8xC	nebo
elektro-mechanické	elektroechanický	k2eAgNnSc4d1	elektro-mechanický
zařízení	zařízení	k1gNnSc4	zařízení
sloužící	sloužící	k2eAgMnSc1d1	sloužící
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Terminál	terminál	k1gInSc1	terminál
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Human-Machine	Human-Machin	k1gInSc5	Human-Machin
Interface	interface	k1gInSc1	interface
<g/>
,	,	kIx,	,
HMI	HMI	kA	HMI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yIgInSc2	který
uživatel	uživatel	k1gMnSc1	uživatel
počítač	počítač	k1gInSc4	počítač
ovládá	ovládat	k5eAaImIp3nS	ovládat
<g/>
,	,	kIx,	,
spouští	spouštět	k5eAaImIp3nS	spouštět
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
pořizuje	pořizovat	k5eAaImIp3nS	pořizovat
vstupy	vstup	k1gInPc4	vstup
pro	pro	k7c4	pro
programy	program	k1gInPc4	program
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
pomocí	pomocí	k7c2	pomocí
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
počítač	počítač	k1gInSc1	počítač
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
výstupy	výstup	k1gInPc4	výstup
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
terminály	terminála	k1gFnPc1	terminála
byly	být	k5eAaImAgFnP	být
znakové	znakový	k2eAgFnPc1d1	znaková
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgFnP	sloužit
operátorům	operátor	k1gMnPc3	operátor
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
sálovým	sálový	k2eAgInPc3d1	sálový
počítačům	počítač	k1gInPc3	počítač
pomocí	pomocí	k7c2	pomocí
sériového	sériový	k2eAgInSc2d1	sériový
kabelu	kabel	k1gInSc2	kabel
či	či	k8xC	či
modemu	modem	k1gInSc2	modem
a	a	k8xC	a
telefonní	telefonní	k2eAgFnSc2d1	telefonní
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
grafické	grafický	k2eAgInPc4d1	grafický
terminály	terminál	k1gInPc4	terminál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
využívat	využívat	k5eAaPmF	využívat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xS	jako
tenký	tenký	k2eAgMnSc1d1	tenký
klient	klient	k1gMnSc1	klient
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
terminály	terminála	k1gFnPc1	terminála
již	již	k6eAd1	již
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
emulátory	emulátor	k1gInPc1	emulátor
terminálu	terminál	k1gInSc2	terminál
(	(	kIx(	(
<g/>
PuTTY	PuTTY	k1gFnSc1	PuTTY
<g/>
,	,	kIx,	,
xterm	xterm	k1gInSc1	xterm
<g/>
,	,	kIx,	,
konsole	konsola	k1gFnSc6	konsola
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Textový	textový	k2eAgInSc1d1	textový
terminál	terminál	k1gInSc1	terminál
==	==	k?	==
</s>
</p>
<p>
<s>
Terminál	terminál	k1gInSc1	terminál
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
znakový	znakový	k2eAgInSc4d1	znakový
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
textový	textový	k2eAgInSc4d1	textový
terminál	terminál	k1gInSc4	terminál
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
specializované	specializovaný	k2eAgNnSc1d1	specializované
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
klávesnice	klávesnice	k1gFnSc1	klávesnice
a	a	k8xC	a
obrazovka	obrazovka	k1gFnSc1	obrazovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
operátorům	operátor	k1gInPc3	operátor
počítač	počítač	k1gInSc4	počítač
využívat	využívat	k5eAaImF	využívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
existovaly	existovat	k5eAaImAgInP	existovat
pouze	pouze	k6eAd1	pouze
extrémně	extrémně	k6eAd1	extrémně
drahé	drahý	k2eAgInPc1d1	drahý
sálové	sálový	k2eAgInPc1d1	sálový
počítače	počítač	k1gInPc1	počítač
(	(	kIx(	(
<g/>
mainframe	mainframe	k1gInSc1	mainframe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
firma	firma	k1gFnSc1	firma
měla	mít	k5eAaImAgFnS	mít
typicky	typicky	k6eAd1	typicky
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
výpočetním	výpočetní	k2eAgNnSc6d1	výpočetní
středisku	středisko	k1gNnSc6	středisko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
počítač	počítač	k1gInSc1	počítač
měl	mít	k5eAaImAgInS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dnes	dnes	k6eAd1	dnes
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
systémovou	systémový	k2eAgFnSc4d1	systémová
konzoli	konzole	k1gFnSc4	konzole
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
monitor	monitor	k1gInSc4	monitor
a	a	k8xC	a
klávesnici	klávesnice	k1gFnSc4	klávesnice
připojenou	připojený	k2eAgFnSc4d1	připojená
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
programátoři	programátor	k1gMnPc1	programátor
<g/>
,	,	kIx,	,
obsluha	obsluha	k1gFnSc1	obsluha
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
též	též	k9	též
pořizování	pořizování	k1gNnSc4	pořizování
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vkládání	vkládání	k1gNnSc1	vkládání
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
připojovali	připojovat	k5eAaImAgMnP	připojovat
právě	právě	k9	právě
pomocí	pomocí	k7c2	pomocí
terminálu	terminál	k1gInSc2	terminál
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
místností	místnost	k1gFnPc2	místnost
nebo	nebo	k8xC	nebo
vzdáleně	vzdáleně	k6eAd1	vzdáleně
přes	přes	k7c4	přes
telefonní	telefonní	k2eAgFnSc4d1	telefonní
linku	linka	k1gFnSc4	linka
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
modemu	modem	k1gInSc2	modem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řádkový	řádkový	k2eAgInSc4d1	řádkový
terminál	terminál	k1gInSc4	terminál
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
terminály	terminála	k1gFnPc1	terminála
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Stisknutí	stisknutí	k1gNnSc1	stisknutí
klávesy	klávesa	k1gFnSc2	klávesa
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vyslání	vyslání	k1gNnSc1	vyslání
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ASCII	ascii	kA	ascii
<g/>
,	,	kIx,	,
EBCDIC	EBCDIC	kA	EBCDIC
<g/>
)	)	kIx)	)
po	po	k7c6	po
sériové	sériový	k2eAgFnSc6d1	sériová
lince	linka	k1gFnSc6	linka
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
jako	jako	k8xS	jako
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
spuštěného	spuštěný	k2eAgInSc2d1	spuštěný
programu	program	k1gInSc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
program	program	k1gInSc1	program
znak	znak	k1gInSc1	znak
vytiskl	vytisknout	k5eAaPmAgInS	vytisknout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
po	po	k7c6	po
sériové	sériový	k2eAgFnSc6d1	sériová
lince	linka	k1gFnSc6	linka
do	do	k7c2	do
terminálu	terminál	k1gInSc2	terminál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
znak	znak	k1gInSc1	znak
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
(	(	kIx(	(
<g/>
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
kurzoru	kurzor	k1gInSc2	kurzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
na	na	k7c6	na
pozicích	pozice	k1gFnPc6	pozice
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgFnPc1d1	daná
šachovnice	šachovnice	k1gFnPc1	šachovnice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
typicky	typicky	k6eAd1	typicky
25	[number]	k4	25
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
80	[number]	k4	80
sloupců	sloupec	k1gInPc2	sloupec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
volit	volit	k5eAaImF	volit
barvu	barva	k1gFnSc4	barva
ani	ani	k8xC	ani
velikost	velikost	k1gFnSc4	velikost
či	či	k8xC	či
font	font	k1gInSc4	font
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
inverzní	inverzní	k2eAgNnSc1d1	inverzní
zobrazení	zobrazení	k1gNnSc1	zobrazení
znaku	znak	k1gInSc2	znak
či	či	k8xC	či
podtržení	podtržení	k1gNnSc2	podtržení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terminál	terminál	k1gInSc1	terminál
měl	mít	k5eAaImAgInS	mít
podobu	podoba	k1gFnSc4	podoba
znaků	znak	k1gInPc2	znak
uloženu	uložen	k2eAgFnSc4d1	uložena
ve	v	k7c4	v
své	své	k1gNnSc4	své
pevné	pevný	k2eAgFnSc2d1	pevná
paměti	paměť	k1gFnSc2	paměť
ROM	ROM	kA	ROM
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rastrové	rastrový	k2eAgFnSc2d1	rastrová
matice	matice	k1gFnSc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
Označovaly	označovat	k5eAaImAgInP	označovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
hloupé	hloupý	k2eAgInPc1d1	hloupý
terminály	terminál	k1gInPc1	terminál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
dumb	dumb	k1gMnSc1	dumb
terminal	terminat	k5eAaImAgMnS	terminat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pracovaly	pracovat	k5eAaImAgFnP	pracovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
řádky	řádek	k1gInPc7	řádek
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
též	též	k9	též
označení	označení	k1gNnSc1	označení
řádkové	řádkový	k2eAgFnSc2d1	řádková
terminály	terminála	k1gFnSc2	terminála
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracovaly	pracovat	k5eAaImAgFnP	pracovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
nebo	nebo	k8xC	nebo
počítačová	počítačový	k2eAgFnSc1d1	počítačová
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
koncepčně	koncepčně	k6eAd1	koncepčně
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
dálnopisu	dálnopis	k1gInSc2	dálnopis
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
kurzor	kurzor	k1gInSc1	kurzor
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
vytištěn	vytištěn	k2eAgInSc1d1	vytištěn
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
kurzor	kurzor	k1gInSc4	kurzor
se	se	k3xPyFc4	se
posunul	posunout	k5eAaPmAgMnS	posunout
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
pozici	pozice	k1gFnSc4	pozice
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
znak	znak	k1gInSc1	znak
odřádkování	odřádkování	k1gNnSc2	odřádkování
(	(	kIx(	(
<g/>
stisk	stisk	k1gInSc1	stisk
klávesy	klávesa	k1gFnSc2	klávesa
Enter	Entra	k1gFnPc2	Entra
<g/>
)	)	kIx)	)
způsobil	způsobit	k5eAaPmAgInS	způsobit
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
následujícího	následující	k2eAgInSc2d1	následující
řádku	řádek	k1gInSc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
obrazovka	obrazovka	k1gFnSc1	obrazovka
zaplnila	zaplnit	k5eAaPmAgFnS	zaplnit
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
řádku	řádek	k1gInSc2	řádek
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
odrolování	odrolování	k1gNnSc1	odrolování
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
obsah	obsah	k1gInSc1	obsah
obrazovky	obrazovka	k1gFnSc2	obrazovka
posunul	posunout	k5eAaPmAgInS	posunout
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
řádek	řádek	k1gInSc4	řádek
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vrchní	vrchní	k2eAgInSc4d1	vrchní
řádek	řádek	k1gInSc4	řádek
ztratil	ztratit	k5eAaPmAgMnS	ztratit
a	a	k8xC	a
dole	dole	k6eAd1	dole
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc4	jeden
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
výstup	výstup	k1gInSc4	výstup
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
opakoval	opakovat	k5eAaImAgInS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
se	se	k3xPyFc4	se
upravoval	upravovat	k5eAaImAgInS	upravovat
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
editorů	editor	k1gInPc2	editor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Celoobrazovkový	celoobrazovkový	k2eAgInSc4d1	celoobrazovkový
terminál	terminál	k1gInSc4	terminál
===	===	k?	===
</s>
</p>
<p>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
terminály	terminála	k1gFnPc1	terminála
přidávaly	přidávat	k5eAaImAgFnP	přidávat
další	další	k2eAgFnPc4d1	další
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
pokrokem	pokrok	k1gInSc7	pokrok
byly	být	k5eAaImAgFnP	být
celoobrazovkové	celoobrazovkový	k2eAgFnPc1d1	celoobrazovková
terminály	terminála	k1gFnPc1	terminála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
řídících	řídící	k2eAgInPc2d1	řídící
kódů	kód	k1gInPc2	kód
umístit	umístit	k5eAaPmF	umístit
kurzor	kurzor	k1gInSc4	kurzor
na	na	k7c4	na
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvářet	vytvářet	k5eAaImF	vytvářet
pokročilejší	pokročilý	k2eAgInPc4d2	pokročilejší
programy	program	k1gInPc4	program
–	–	k?	–
například	například	k6eAd1	například
textové	textový	k2eAgInPc1d1	textový
editory	editor	k1gInPc1	editor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
vidět	vidět	k5eAaImF	vidět
celá	celý	k2eAgFnSc1d1	celá
stránka	stránka	k1gFnSc1	stránka
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
textu	text	k1gInSc2	text
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
najetím	najetí	k1gNnSc7	najetí
kurzoru	kurzor	k1gInSc2	kurzor
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
textové	textový	k2eAgInPc1d1	textový
programy	program	k1gInPc1	program
s	s	k7c7	s
menu	menu	k1gNnPc7	menu
a	a	k8xC	a
tlačítky	tlačítko	k1gNnPc7	tlačítko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
textové	textový	k2eAgNnSc4d1	textové
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k8xC	i
znaky	znak	k1gInPc7	znak
předefinovávat	předefinovávat	k5eAaImF	předefinovávat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
poměrně	poměrně	k6eAd1	poměrně
toporným	toporný	k2eAgInSc7d1	toporný
způsobem	způsob	k1gInSc7	způsob
i	i	k8xC	i
grafické	grafický	k2eAgInPc4d1	grafický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Přibyly	přibýt	k5eAaPmAgFnP	přibýt
též	též	k9	též
možnosti	možnost	k1gFnPc1	možnost
změny	změna	k1gFnSc2	změna
barev	barva	k1gFnPc2	barva
písma	písmo	k1gNnSc2	písmo
nebo	nebo	k8xC	nebo
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komunikaci	komunikace	k1gFnSc4	komunikace
pomocí	pomocí	k7c2	pomocí
textového	textový	k2eAgNnSc2d1	textové
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vycházelo	vycházet	k5eAaImAgNnS	vycházet
ze	z	k7c2	z
schopností	schopnost	k1gFnSc7	schopnost
textového	textový	k2eAgInSc2d1	textový
terminálu	terminál	k1gInSc2	terminál
využívaly	využívat	k5eAaImAgInP	využívat
i	i	k9	i
starší	starý	k2eAgInPc1d2	starší
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Příkazový	příkazový	k2eAgInSc1d1	příkazový
řádek	řádek	k1gInSc1	řádek
zobrazený	zobrazený	k2eAgInSc1d1	zobrazený
na	na	k7c6	na
terminálu	terminál	k1gInSc6	terminál
používal	používat	k5eAaImAgInS	používat
například	například	k6eAd1	například
systém	systém	k1gInSc1	systém
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
DOS	DOS	kA	DOS
(	(	kIx(	(
<g/>
interpret	interpret	k1gMnSc1	interpret
příkazů	příkaz	k1gInPc2	příkaz
COMMAND	COMMAND	kA	COMMAND
<g/>
.	.	kIx.	.
<g/>
COM	COM	kA	COM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Unix	Unix	k1gInSc1	Unix
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
unixové	unixový	k2eAgFnPc4d1	unixová
shelly	shella	k1gFnPc4	shella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
grafickém	grafický	k2eAgNnSc6d1	grafické
prostředí	prostředí	k1gNnSc6	prostředí
stále	stále	k6eAd1	stále
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
emulátor	emulátor	k1gInSc1	emulátor
terminálu	terminál	k1gInSc2	terminál
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
programu	program	k1gInSc2	program
cmd	cmd	k?	cmd
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
příkazový	příkazový	k2eAgInSc4d1	příkazový
řádek	řádek	k1gInSc4	řádek
a	a	k8xC	a
celoobrazovkový	celoobrazovkový	k2eAgInSc4d1	celoobrazovkový
textový	textový	k2eAgInSc4d1	textový
terminál	terminál	k1gInSc4	terminál
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systémová	systémový	k2eAgFnSc1d1	systémová
konzole	konzola	k1gFnSc6	konzola
===	===	k?	===
</s>
</p>
<p>
<s>
Systémová	systémový	k2eAgFnSc1d1	systémová
konzola	konzola	k1gFnSc1	konzola
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jen	jen	k6eAd1	jen
konzole	konzola	k1gFnSc3	konzola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
terminál	terminál	k1gInSc4	terminál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
napevno	napevno	k6eAd1	napevno
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
má	mít	k5eAaImIp3nS	mít
typicky	typicky	k6eAd1	typicky
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
systémovou	systémový	k2eAgFnSc4d1	systémová
konzoli	konzole	k1gFnSc4	konzole
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sálových	sálový	k2eAgMnPc2d1	sálový
počítačů	počítač	k1gMnPc2	počítač
se	se	k3xPyFc4	se
systémová	systémový	k2eAgFnSc1d1	systémová
konzola	konzola	k1gFnSc1	konzola
používala	používat	k5eAaImAgFnS	používat
jen	jen	k9	jen
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
terminály	terminála	k1gFnPc1	terminála
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
používá	používat	k5eAaImIp3nS	používat
konzola	konzola	k1gFnSc1	konzola
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
konzoly	konzola	k1gFnSc2	konzola
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
,	,	kIx,	,
než	než	k8xS	než
terminálu	terminála	k1gFnSc4	terminála
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
schopnosti	schopnost	k1gFnPc4	schopnost
akcelerace	akcelerace	k1gFnSc2	akcelerace
GPU	GPU	kA	GPU
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Grafický	grafický	k2eAgInSc4d1	grafický
terminál	terminál	k1gInSc4	terminál
==	==	k?	==
</s>
</p>
<p>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zobrazování	zobrazování	k1gNnSc4	zobrazování
libovolných	libovolný	k2eAgInPc2d1	libovolný
grafických	grafický	k2eAgInPc2d1	grafický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
grafický	grafický	k2eAgInSc1d1	grafický
terminál	terminál	k1gInSc1	terminál
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ovládat	ovládat	k5eAaImF	ovládat
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgInPc4d1	grafický
terminály	terminál	k1gInPc4	terminál
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c6	na
vektorové	vektorový	k2eAgFnSc6d1	vektorová
a	a	k8xC	a
rastrové	rastrový	k2eAgFnSc6d1	rastrová
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
fonty	font	k1gInPc7	font
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
definují	definovat	k5eAaBmIp3nP	definovat
vzhled	vzhled	k1gInSc4	vzhled
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
unixovém	unixový	k2eAgNnSc6d1	unixové
grafickém	grafický	k2eAgNnSc6d1	grafické
prostředí	prostředí	k1gNnSc6	prostředí
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
ovládá	ovládat	k5eAaImIp3nS	ovládat
grafický	grafický	k2eAgInSc1d1	grafický
terminál	terminál	k1gInSc1	terminál
X	X	kA	X
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
aplikace	aplikace	k1gFnPc1	aplikace
jako	jako	k8xS	jako
klienti	klient	k1gMnPc1	klient
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
X	X	kA	X
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
programů	program	k1gInPc2	program
a	a	k8xC	a
zobrazovacích	zobrazovací	k2eAgNnPc2d1	zobrazovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
běžet	běžet	k5eAaImF	běžet
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
a	a	k8xC	a
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
svůj	svůj	k3xOyFgInSc4	svůj
(	(	kIx(	(
<g/>
grafický	grafický	k2eAgInSc4d1	grafický
<g/>
)	)	kIx)	)
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
grafické	grafický	k2eAgNnSc1d1	grafické
prostředí	prostředí	k1gNnSc1	prostředí
pevně	pevně	k6eAd1	pevně
svázáno	svázat	k5eAaPmNgNnS	svázat
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
grafickému	grafický	k2eAgInSc3d1	grafický
terminálu	terminál	k1gInSc3	terminál
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
protokol	protokol	k1gInSc1	protokol
RDP	RDP	kA	RDP
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenášet	přenášet	k5eAaImF	přenášet
i	i	k9	i
výstup	výstup	k1gInSc4	výstup
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
grafickému	grafický	k2eAgInSc3d1	grafický
terminálu	terminál	k1gInSc3	terminál
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
program	program	k1gInSc4	program
VNC	VNC	kA	VNC
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tenký	tenký	k2eAgMnSc1d1	tenký
klient	klient	k1gMnSc1	klient
===	===	k?	===
</s>
</p>
<p>
<s>
Grafický	grafický	k2eAgInSc1d1	grafický
terminál	terminál	k1gInSc1	terminál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
se	s	k7c7	s
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tenký	tenký	k2eAgMnSc1d1	tenký
klient	klient	k1gMnSc1	klient
(	(	kIx(	(
<g/>
též	též	k9	též
bezdisková	bezdiskový	k2eAgFnSc1d1	bezdisková
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emulátor	emulátor	k1gInSc1	emulátor
terminálu	terminál	k1gInSc2	terminál
===	===	k?	===
</s>
</p>
<p>
<s>
Emulátor	emulátor	k1gInSc1	emulátor
terminálu	terminál	k1gInSc2	terminál
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgFnSc1d1	speciální
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
grafickém	grafický	k2eAgNnSc6d1	grafické
uživatelském	uživatelský	k2eAgNnSc6d1	Uživatelské
prostředí	prostředí	k1gNnSc6	prostředí
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
okno	okno	k1gNnSc1	okno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
kterého	který	k3yIgMnSc2	který
je	být	k5eAaImIp3nS	být
emulován	emulován	k2eAgInSc1d1	emulován
textový	textový	k2eAgInSc1d1	textový
terminál	terminál	k1gInSc1	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
PuTTY	PuTTY	k1gFnSc4	PuTTY
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
i	i	k8xC	i
unixové	unixový	k2eAgInPc1d1	unixový
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
xterm	xterm	k1gInSc1	xterm
(	(	kIx(	(
<g/>
vzorová	vzorový	k2eAgFnSc1d1	vzorová
emulace	emulace	k1gFnSc1	emulace
pro	pro	k7c4	pro
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gnome-terminal	gnomeerminat	k5eAaPmAgMnS	gnome-terminat
<g/>
,	,	kIx,	,
konsole	konsola	k1gFnSc6	konsola
<g/>
,	,	kIx,	,
termux	termux	k1gInSc1	termux
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spolupráce	spolupráce	k1gFnSc1	spolupráce
aplikace	aplikace	k1gFnSc2	aplikace
s	s	k7c7	s
terminálem	terminál	k1gInSc7	terminál
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
program	program	k1gInSc4	program
změnit	změnit	k5eAaPmF	změnit
pozici	pozice	k1gFnSc4	pozice
kurzoru	kurzor	k1gInSc2	kurzor
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vytisknout	vytisknout	k5eAaPmF	vytisknout
následující	následující	k2eAgInSc4d1	následující
znak	znak	k1gInSc4	znak
na	na	k7c4	na
předem	předem	k6eAd1	předem
danou	daný	k2eAgFnSc4d1	daná
pozici	pozice	k1gFnSc4	pozice
šachovnice	šachovnice	k1gFnSc2	šachovnice
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
na	na	k7c6	na
textovém	textový	k2eAgInSc6d1	textový
terminálu	terminál	k1gInSc6	terminál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnit	změnit	k5eAaPmF	změnit
barvu	barva	k1gFnSc4	barva
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
podkladu	podklad	k1gInSc2	podklad
<g/>
,	,	kIx,	,
zapnout	zapnout	k5eAaPmF	zapnout
podtržení	podtržení	k1gNnSc4	podtržení
<g/>
,	,	kIx,	,
smazat	smazat	k5eAaPmF	smazat
obrazovku	obrazovka	k1gFnSc4	obrazovka
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc4d1	jiná
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
terminálu	terminála	k1gFnSc4	terminála
vyslat	vyslat	k5eAaPmF	vyslat
určitou	určitý	k2eAgFnSc4d1	určitá
řídící	řídící	k2eAgFnSc4d1	řídící
sekvenci	sekvence	k1gFnSc4	sekvence
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nezobrazí	zobrazit	k5eNaPmIp3nS	zobrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
způsobí	způsobit	k5eAaPmIp3nS	způsobit
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
chování	chování	k1gNnSc6	chování
terminálu	terminál	k1gInSc2	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
řídící	řídící	k2eAgFnPc1d1	řídící
sekvence	sekvence	k1gFnPc1	sekvence
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
terminálu	terminál	k1gInSc2	terminál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
terminály	terminála	k1gFnPc1	terminála
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
různé	různý	k2eAgFnPc1d1	různá
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
aplikace	aplikace	k1gFnSc1	aplikace
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
řídící	řídící	k2eAgFnSc2d1	řídící
sekvence	sekvence	k1gFnSc2	sekvence
použít	použít	k5eAaPmF	použít
(	(	kIx(	(
<g/>
např.	např.	kA	např.
escape	escapat	k5eAaPmIp3nS	escapat
sekvence	sekvence	k1gFnSc1	sekvence
pro	pro	k7c4	pro
xterm	xterm	k1gInSc4	xterm
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInSc4d1	řídící
kódy	kód	k1gInPc4	kód
pro	pro	k7c4	pro
terminál	terminál	k1gInSc4	terminál
VT100	VT100	k1gMnSc2	VT100
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
terminál	terminál	k1gInSc4	terminál
při	při	k7c6	při
přihlášení	přihlášení	k1gNnSc6	přihlášení
uživatele	uživatel	k1gMnSc2	uživatel
svůj	svůj	k3xOyFgInSc4	svůj
typ	typ	k1gInSc4	typ
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
řetězce	řetězec	k1gInSc2	řetězec
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
xterm	xterm	k1gInSc1	xterm
<g/>
,	,	kIx,	,
vt	vt	k?	vt
<g/>
100	[number]	k4	100
<g/>
,	,	kIx,	,
linux	linux	k1gInSc1	linux
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
identifikace	identifikace	k1gFnSc1	identifikace
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
proměnné	proměnná	k1gFnSc2	proměnná
prostředí	prostředí	k1gNnSc2	prostředí
TERM	termy	k1gFnPc2	termy
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
aplikace	aplikace	k1gFnPc4	aplikace
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
terminálů	terminál	k1gInPc2	terminál
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
příslušných	příslušný	k2eAgInPc2d1	příslušný
řídících	řídící	k2eAgInPc2d1	řídící
kódů	kód	k1gInPc2	kód
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
programátorům	programátor	k1gMnPc3	programátor
API	API	kA	API
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
na	na	k7c6	na
použitém	použitý	k2eAgInSc6d1	použitý
terminálu	terminál	k1gInSc6	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
proměnné	proměnná	k1gFnSc2	proměnná
TERM	termy	k1gFnPc2	termy
vybere	vybrat	k5eAaPmIp3nS	vybrat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
databáze	databáze	k1gFnSc2	databáze
příslušné	příslušný	k2eAgInPc4d1	příslušný
řídící	řídící	k2eAgInSc4d1	řídící
kódy	kód	k1gInPc4	kód
a	a	k8xC	a
odešle	odešle	k6eAd1	odešle
je	být	k5eAaImIp3nS	být
terminálu	terminála	k1gFnSc4	terminála
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
knihovny	knihovna	k1gFnPc4	knihovna
patří	patřit	k5eAaImIp3nP	patřit
termcap	termcap	k1gMnSc1	termcap
<g/>
,	,	kIx,	,
terminfo	terminfo	k1gMnSc1	terminfo
či	či	k8xC	či
curses	curses	k1gMnSc1	curses
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
textovém	textový	k2eAgInSc6d1	textový
terminálu	terminál	k1gInSc6	terminál
vypsat	vypsat	k5eAaPmF	vypsat
proměnnou	proměnná	k1gFnSc4	proměnná
prostředí	prostředí	k1gNnSc2	prostředí
TERM	termy	k1gFnPc2	termy
například	například	k6eAd1	například
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
echo	echo	k1gNnSc1	echo
$	$	kIx~	$
<g/>
TERM	term	k1gInSc1	term
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
počítačů	počítač	k1gMnPc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgInPc6d1	kompatibilní
je	být	k5eAaImIp3nS	být
ovládání	ovládání	k1gNnSc1	ovládání
terminálu	terminál	k1gInSc2	terminál
dáno	dát	k5eAaPmNgNnS	dát
použitou	použitý	k2eAgFnSc7d1	použitá
grafickou	grafický	k2eAgFnSc7d1	grafická
kartou	karta	k1gFnSc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
standard	standard	k1gInSc1	standard
MDA	MDA	kA	MDA
<g/>
,	,	kIx,	,
CGA	CGA	kA	CGA
nebo	nebo	k8xC	nebo
EGA	EGA	kA	EGA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
standard	standard	k1gInSc1	standard
VGA	VGA	kA	VGA
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
terminál	terminál	k1gInSc4	terminál
pak	pak	k6eAd1	pak
jeho	on	k3xPp3gInSc4	on
klasický	klasický	k2eAgInSc4d1	klasický
textový	textový	k2eAgInSc4d1	textový
režim	režim	k1gInSc4	režim
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
25	[number]	k4	25
<g/>
×	×	k?	×
<g/>
80	[number]	k4	80
(	(	kIx(	(
<g/>
řádky	řádka	k1gFnSc2	řádka
<g/>
×	×	k?	×
<g/>
sloupce	sloupec	k1gInSc2	sloupec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
DOS	DOS	kA	DOS
nebyl	být	k5eNaImAgInS	být
typ	typ	k1gInSc1	typ
terminálu	terminál	k1gInSc2	terminál
nijak	nijak	k6eAd1	nijak
aplikacím	aplikace	k1gFnPc3	aplikace
předáván	předávat	k5eAaImNgMnS	předávat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tomu	ten	k3xDgNnSc3	ten
nebylo	být	k5eNaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
ani	ani	k8xC	ani
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Režimy	režim	k1gInPc1	režim
terminálu	terminál	k1gInSc2	terminál
==	==	k?	==
</s>
</p>
<p>
<s>
Terminály	terminála	k1gFnPc1	terminála
mohou	moct	k5eAaImIp3nP	moct
operovat	operovat	k5eAaImF	operovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
režimech	režim	k1gInPc6	režim
<g/>
,	,	kIx,	,
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pošlou	poslat	k5eAaPmIp3nP	poslat
vstup	vstup	k1gInSc4	vstup
zadaný	zadaný	k2eAgInSc4d1	zadaný
uživatelem	uživatel	k1gMnSc7	uživatel
na	na	k7c4	na
klávesnici	klávesnice	k1gFnSc4	klávesnice
do	do	k7c2	do
přijímacího	přijímací	k2eAgInSc2d1	přijímací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
režim	režim	k1gInSc1	režim
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
znakový	znakový	k2eAgInSc1d1	znakový
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
psaný	psaný	k2eAgInSc1d1	psaný
vstup	vstup	k1gInSc1	vstup
okamžitě	okamžitě	k6eAd1	okamžitě
odesílán	odesílat	k5eAaImNgInS	odesílat
do	do	k7c2	do
přijímacího	přijímací	k2eAgInSc2d1	přijímací
systému	systém	k1gInSc2	systém
</s>
</p>
<p>
<s>
Řádkový	řádkový	k2eAgInSc1d1	řádkový
režim	režim	k1gInSc1	režim
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
řádkový	řádkový	k2eAgInSc1d1	řádkový
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
terminál	terminál	k1gInSc1	terminál
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
místní	místní	k2eAgFnSc4d1	místní
řádkovou	řádkový	k2eAgFnSc4d1	řádková
editační	editační	k2eAgFnSc4d1	editační
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
odesílá	odesílat	k5eAaImIp3nS	odesílat
celou	celý	k2eAgFnSc4d1	celá
vstupní	vstupní	k2eAgFnSc4d1	vstupní
řádku	řádka	k1gFnSc4	řádka
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byla	být	k5eAaImAgFnS	být
lokálně	lokálně	k6eAd1	lokálně
editována	editován	k2eAgFnSc1d1	editována
<g/>
,	,	kIx,	,
když	když	k8xS	když
uživatel	uživatel	k1gMnSc1	uživatel
stiskl	stisknout	k5eAaPmAgMnS	stisknout
klávesu	klávesa	k1gFnSc4	klávesa
"	"	kIx"	"
<g/>
návrat	návrat	k1gInSc4	návrat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
return	return	k1gInSc1	return
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
řádkový	řádkový	k2eAgInSc1d1	řádkový
režim	režim	k1gInSc1	režim
terminálu	terminál	k1gInSc2	terminál
<g/>
"	"	kIx"	"
pracuje	pracovat	k5eAaImIp3nS	pracovat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blokový	blokový	k2eAgInSc1d1	blokový
režim	režim	k1gInSc1	režim
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
obrazovkový	obrazovkový	k2eAgInSc1d1	obrazovkový
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
režimu	režim	k1gInSc6	režim
terminál	terminála	k1gFnPc2	terminála
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
lokální	lokální	k2eAgFnSc4d1	lokální
celoobrazovkovou	celoobrazovkový	k2eAgFnSc4d1	celoobrazovková
datovou	datový	k2eAgFnSc4d1	datová
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
vstup	vstup	k1gInSc4	vstup
vložit	vložit	k5eAaPmF	vložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
polí	pole	k1gFnPc2	pole
ve	v	k7c6	v
formuláři	formulář	k1gInSc6	formulář
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
terminálu	terminála	k1gFnSc4	terminála
definován	definovat	k5eAaBmNgInS	definovat
přijímacím	přijímací	k2eAgInSc7d1	přijímací
systémem	systém	k1gInSc7	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posunování	posunování	k1gNnSc1	posunování
kurzoru	kurzor	k1gInSc2	kurzor
po	po	k7c6	po
obrazovce	obrazovka	k1gFnSc6	obrazovka
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
tlačítek	tlačítko	k1gNnPc2	tlačítko
Tabulátor	tabulátor	k1gInSc1	tabulátor
<g/>
,	,	kIx,	,
směrových	směrový	k2eAgFnPc2d1	směrová
šipek	šipka	k1gFnPc2	šipka
klávesnice	klávesnice	k1gFnSc2	klávesnice
a	a	k8xC	a
provádění	provádění	k1gNnSc2	provádění
editačních	editační	k2eAgFnPc2d1	editační
funkcí	funkce	k1gFnPc2	funkce
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
kláves	klávesa	k1gFnPc2	klávesa
Insert	insert	k1gInSc1	insert
<g/>
,	,	kIx,	,
Delete	Dele	k1gNnSc2	Dele
<g/>
,	,	kIx,	,
Backspace	Backspace	k1gFnSc2	Backspace
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Terminál	terminál	k1gInSc1	terminál
odesílá	odesílat	k5eAaImIp3nS	odesílat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
dokončené	dokončený	k2eAgInPc1d1	dokončený
formuláře	formulář	k1gInPc1	formulář
skládajících	skládající	k2eAgMnPc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
vložených	vložený	k2eAgNnPc2d1	vložené
dat	datum	k1gNnPc2	datum
vložených	vložený	k2eAgNnPc2d1	vložené
do	do	k7c2	do
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
do	do	k7c2	do
přijímacího	přijímací	k2eAgInSc2d1	přijímací
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
stiskne	stisknout	k5eAaPmIp3nS	stisknout
klávesové	klávesový	k2eAgNnSc4d1	klávesové
tlačítko	tlačítko	k1gNnSc4	tlačítko
Enter	Entra	k1gFnPc2	Entra
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
klávesami	klávesa	k1gFnPc7	klávesa
Enter	Entra	k1gFnPc2	Entra
a	a	k8xC	a
klávesou	klávesa	k1gFnSc7	klávesa
return	returna	k1gFnPc2	returna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nějakých	nějaký	k3yIgInPc6	nějaký
vícerežimových	vícerežimův	k2eAgInPc6d1	vícerežimův
terminálech	terminál	k1gInPc6	terminál
mohou	moct	k5eAaImIp3nP	moct
tato	tento	k3xDgNnPc1	tento
tlačítka	tlačítko	k1gNnPc1	tlačítko
měnit	měnit	k5eAaImF	měnit
režimy	režim	k1gInPc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Stisknutí	stisknutí	k1gNnSc1	stisknutí
tlačítka	tlačítko	k1gNnSc2	tlačítko
Enter	Entra	k1gFnPc2	Entra
<g/>
,	,	kIx,	,
když	když	k8xS	když
uživatel	uživatel	k1gMnSc1	uživatel
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
blokovém	blokový	k2eAgInSc6d1	blokový
režimu	režim	k1gInSc6	režim
neprovádí	provádět	k5eNaImIp3nS	provádět
stejný	stejný	k2eAgInSc4d1	stejný
příkaz	příkaz	k1gInSc4	příkaz
jako	jako	k8xS	jako
tlačítko	tlačítko	k1gNnSc4	tlačítko
return	returna	k1gFnPc2	returna
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
klávesa	klávesa	k1gFnSc1	klávesa
return	returna	k1gFnPc2	returna
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstupní	vstupní	k2eAgInSc1d1	vstupní
řádek	řádek	k1gInSc1	řádek
bude	být	k5eAaImBp3nS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
střediskového	střediskový	k2eAgInSc2d1	střediskový
počítače	počítač	k1gInSc2	počítač
v	v	k7c6	v
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
řádkovém	řádkový	k2eAgInSc6d1	řádkový
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
tlačítko	tlačítko	k1gNnSc1	tlačítko
enter	entra	k1gFnPc2	entra
spíše	spíše	k9	spíše
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
terminál	terminál	k1gInSc1	terminál
bude	být	k5eAaImBp3nS	být
vysílat	vysílat	k5eAaImF	vysílat
obsah	obsah	k1gInSc1	obsah
řádku	řádek	k1gInSc2	řádek
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zrovna	zrovna	k6eAd1	zrovna
umístěn	umístěn	k2eAgInSc1d1	umístěn
kurzor	kurzor	k1gInSc1	kurzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odlišné	odlišný	k2eAgInPc4d1	odlišný
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
různé	různý	k2eAgInPc4d1	různý
stupně	stupeň	k1gInPc4	stupeň
režimové	režimový	k2eAgFnSc2d1	režimová
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
terminály	terminál	k1gInPc1	terminál
využívány	využívat	k5eAaPmNgInP	využívat
jako	jako	k8xC	jako
počítačové	počítačový	k2eAgInPc1d1	počítačový
terminály	terminál	k1gInPc1	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Terminálové	terminálový	k2eAgNnSc1d1	terminálové
rozhraní	rozhraní	k1gNnSc1	rozhraní
definované	definovaný	k2eAgNnSc1d1	definované
unixovým	unixový	k2eAgInSc7d1	unixový
standardem	standard	k1gInSc7	standard
POSIX	POSIX	kA	POSIX
vůbec	vůbec	k9	vůbec
nepřizpůsobuje	přizpůsobovat	k5eNaImIp3nS	přizpůsobovat
blokové	blokový	k2eAgInPc4d1	blokový
režimy	režim	k1gInPc4	režim
terminálů	terminál	k1gInPc2	terminál
a	a	k8xC	a
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
po	po	k7c6	po
terminálu	terminál	k1gInSc6	terminál
samotném	samotný	k2eAgInSc6d1	samotný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
řádkovém	řádkový	k2eAgInSc6d1	řádkový
režimu	režim	k1gInSc6	režim
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
operační	operační	k2eAgFnSc6d1	operační
systémech	systém	k1gInPc6	systém
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
podpora	podpora	k1gFnSc1	podpora
POSIXu	POSIXus	k1gInSc2	POSIXus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
terminálový	terminálový	k2eAgInSc4d1	terminálový
ovladač	ovladač	k1gInSc4	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
emuluje	emulovat	k5eAaImIp3nS	emulovat
místní	místní	k2eAgFnSc4d1	místní
odezvu	odezva	k1gFnSc4	odezva
v	v	k7c6	v
terminálu	terminál	k1gInSc6	terminál
a	a	k8xC	a
provede	provést	k5eAaPmIp3nS	provést
řádku	řádka	k1gFnSc4	řádka
editační	editační	k2eAgFnSc2d1	editační
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
a	a	k8xC	a
zejména	zejména	k9	zejména
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
počítačového	počítačový	k2eAgInSc2d1	počítačový
terminálu	terminál	k1gInSc2	terminál
podporuje	podporovat	k5eAaImIp3nS	podporovat
režim	režim	k1gInSc1	režim
nekanonického	kanonický	k2eNgInSc2d1	nekanonický
vstupu	vstup	k1gInSc2	vstup
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
terminály	terminál	k1gInPc4	terminál
POSIX	POSIX	kA	POSIX
systému	systém	k1gInSc2	systém
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
okamžitém	okamžitý	k2eAgInSc6d1	okamžitý
znakovém	znakový	k2eAgInSc6d1	znakový
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
terminály	terminála	k1gFnPc4	terminála
IBM	IBM	kA	IBM
3270	[number]	k4	3270
propojené	propojený	k2eAgFnSc6d1	propojená
do	do	k7c2	do
MVS	MVS	kA	MVS
systémů	systém	k1gInPc2	systém
vždy	vždy	k6eAd1	vždy
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
blokový	blokový	k2eAgInSc4d1	blokový
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
terminál	terminála	k1gFnPc2	terminála
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.youtube.com/watch?v=7ND6oLXocR0	[url]	k4	http://www.youtube.com/watch?v=7ND6oLXocR0
–	–	k?	–
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
terminálem	terminál	k1gInSc7	terminál
DEC	DEC	kA	DEC
VT05	VT05	k1gFnSc6	VT05
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
