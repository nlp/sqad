<s>
Původní	původní	k2eAgFnPc1d1	původní
terminály	terminála	k1gFnPc1	terminála
byly	být	k5eAaImAgFnP	být
znakové	znakový	k2eAgFnPc1d1	znaková
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgFnP	sloužit
operátorům	operátor	k1gMnPc3	operátor
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
sálovým	sálový	k2eAgInPc3d1	sálový
počítačům	počítač	k1gInPc3	počítač
pomocí	pomocí	k7c2	pomocí
sériového	sériový	k2eAgInSc2d1	sériový
kabelu	kabel	k1gInSc2	kabel
či	či	k8xC	či
modemu	modem	k1gInSc2	modem
a	a	k8xC	a
telefonní	telefonní	k2eAgFnSc2d1	telefonní
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
