<s>
Mor	mor	k1gInSc1	mor
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
přeneseném	přenesený	k2eAgInSc6d1	přenesený
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
historickém	historický	k2eAgNnSc6d1	historické
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
vážné	vážný	k2eAgNnSc4d1	vážné
infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
už	už	k6eAd1	už
prakticky	prakticky	k6eAd1	prakticky
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
a	a	k8xC	a
raně	raně	k6eAd1	raně
novověkých	novověký	k2eAgFnPc6d1	novověká
kronikách	kronika	k1gFnPc6	kronika
a	a	k8xC	a
dějepisných	dějepisný	k2eAgNnPc6d1	dějepisné
pojednáních	pojednání	k1gNnPc6	pojednání
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
významu	význam	k1gInSc6	význam
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
chorobu	choroba	k1gFnSc4	choroba
způsobovanou	způsobovaný	k2eAgFnSc7d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnPc1	pestis
<g/>
.	.	kIx.	.
</s>
