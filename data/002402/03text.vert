<s>
Mor	mor	k1gInSc1	mor
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
přeneseném	přenesený	k2eAgInSc6d1	přenesený
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
historickém	historický	k2eAgNnSc6d1	historické
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
vážné	vážný	k2eAgNnSc4d1	vážné
infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
už	už	k6eAd1	už
prakticky	prakticky	k6eAd1	prakticky
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
a	a	k8xC	a
raně	raně	k6eAd1	raně
novověkých	novověký	k2eAgFnPc6d1	novověká
kronikách	kronika	k1gFnPc6	kronika
a	a	k8xC	a
dějepisných	dějepisný	k2eAgNnPc6d1	dějepisné
pojednáních	pojednání	k1gNnPc6	pojednání
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
významu	význam	k1gInSc6	význam
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
chorobu	choroba	k1gFnSc4	choroba
způsobovanou	způsobovaný	k2eAgFnSc7d1	způsobovaná
bakterií	bakterie	k1gFnSc7	bakterie
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnPc1	pestis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
dále	daleko	k6eAd2	daleko
zabývat	zabývat	k5eAaImF	zabývat
tímto	tento	k3xDgInSc7	tento
užším	úzký	k2eAgInSc7d2	užší
obsahem	obsah	k1gInSc7	obsah
pojmu	pojem	k1gInSc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
se	se	k3xPyFc4	se
jakožto	jakožto	k8xS	jakožto
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
kromě	kromě	k7c2	kromě
lidí	člověk	k1gMnPc2	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
tyčinkovitá	tyčinkovitý	k2eAgFnSc1d1	tyčinkovitá
bakterie	bakterie	k1gFnSc1	bakterie
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnSc2	pestis
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
dýmějový	dýmějový	k2eAgInSc1d1	dýmějový
mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
bubonická	bubonický	k2eAgFnSc1d1	bubonická
forma	forma	k1gFnSc1	forma
<g/>
)	)	kIx)	)
septický	septický	k2eAgInSc1d1	septický
mor	mor	k1gInSc1	mor
plicní	plicní	k2eAgInSc1d1	plicní
mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
pneumonická	pneumonický	k2eAgFnSc1d1	pneumonický
forma	forma	k1gFnSc1	forma
<g/>
)	)	kIx)	)
Forma	forma	k1gFnSc1	forma
bubonická	bubonický	k2eAgFnSc1d1	bubonická
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
přenášena	přenášen	k2eAgMnSc4d1	přenášen
blechami	blecha	k1gFnPc7	blecha
(	(	kIx(	(
<g/>
druhy	druh	k1gInPc7	druh
Pulex	Pulex	k1gInSc1	Pulex
irritans	irritans	k6eAd1	irritans
<g/>
,	,	kIx,	,
Xenopsylla	Xenopsylla	k1gFnSc1	Xenopsylla
cheopis	cheopis	k1gInSc1	cheopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
infikovaly	infikovat	k5eAaBmAgFnP	infikovat
na	na	k7c6	na
nakaženém	nakažený	k2eAgNnSc6d1	nakažené
hlodavci	hlodavec	k1gMnPc7	hlodavec
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
kryse	krysa	k1gFnSc6	krysa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
příznaky	příznak	k1gInPc1	příznak
netrpí	trpět	k5eNaImIp3nP	trpět
<g/>
:	:	kIx,	:
nemoc	nemoc	k1gFnSc4	nemoc
svého	svůj	k3xOyFgInSc2	svůj
přenašeče	přenašeč	k1gInSc2	přenašeč
nehubí	hubit	k5eNaImIp3nS	hubit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
kousnutí	kousnutí	k1gNnSc4	kousnutí
infikovanou	infikovaný	k2eAgFnSc7d1	infikovaná
blechou	blecha	k1gFnSc7	blecha
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zhnědnutí	zhnědnutí	k1gNnSc3	zhnědnutí
kousance	kousanec	k1gInSc2	kousanec
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
boule	boule	k1gFnSc1	boule
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
plicní	plicní	k2eAgFnSc1d1	plicní
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nebezpečnější	bezpečný	k2eNgNnSc1d2	nebezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
neléčena	léčen	k2eNgFnSc1d1	neléčena
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
vysokou	vysoký	k2eAgFnSc4d1	vysoká
smrtnost	smrtnost	k1gFnSc4	smrtnost
(	(	kIx(	(
<g/>
až	až	k9	až
přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
inkubační	inkubační	k2eAgFnSc6d1	inkubační
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prudkému	prudký	k2eAgNnSc3d1	prudké
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
doprovázenému	doprovázený	k2eAgNnSc3d1	doprovázené
třesavkou	třesavka	k1gFnSc7	třesavka
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
a	a	k8xC	a
únavou	únava	k1gFnSc7	únava
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bubonickou	bubonický	k2eAgFnSc4d1	bubonická
formu	forma	k1gFnSc4	forma
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
hnisavý	hnisavý	k2eAgInSc1d1	hnisavý
zánět	zánět	k1gInSc1	zánět
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
tříslech	tříslo	k1gNnPc6	tříslo
a	a	k8xC	a
podpaží	podpažit	k5eAaPmIp3nP	podpažit
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
hnisavých	hnisavý	k2eAgNnPc2d1	hnisavé
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
celkovou	celkový	k2eAgFnSc4d1	celková
sepsi	sepse	k1gFnSc4	sepse
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgFnSc1d1	plicní
forma	forma	k1gFnSc1	forma
probíhá	probíhat	k5eAaImIp3nS	probíhat
jako	jako	k9	jako
těžký	těžký	k2eAgInSc1d1	těžký
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
smrtností	smrtnost	k1gFnSc7	smrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neléčené	léčený	k2eNgFnSc6d1	neléčená
bubonické	bubonický	k2eAgFnSc6d1	bubonická
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
smrtnost	smrtnost	k1gFnSc1	smrtnost
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
významně	významně	k6eAd1	významně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
jsou	být	k5eAaImIp3nP	být
účinné	účinný	k2eAgInPc1d1	účinný
chloramfenikol	chloramfenikol	k1gInSc1	chloramfenikol
<g/>
,	,	kIx,	,
aminoglykosidy	aminoglykosid	k1gInPc1	aminoglykosid
a	a	k8xC	a
chinolony	chinolon	k1gInPc1	chinolon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
též	též	k9	též
dostupné	dostupný	k2eAgNnSc1d1	dostupné
preventivní	preventivní	k2eAgNnSc1d1	preventivní
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Justiniánský	Justiniánský	k2eAgInSc4d1	Justiniánský
mor	mor	k1gInSc4	mor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doložená	doložený	k2eAgFnSc1d1	doložená
epidemie	epidemie	k1gFnSc1	epidemie
dýmějového	dýmějový	k2eAgInSc2d1	dýmějový
moru	mor	k1gInSc2	mor
propukla	propuknout	k5eAaPmAgFnS	propuknout
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
541	[number]	k4	541
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
největším	veliký	k2eAgNnSc6d3	veliký
evropském	evropský	k2eAgNnSc6d1	Evropské
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
dostala	dostat	k5eAaPmAgFnS	dostat
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
či	či	k8xC	či
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
město	město	k1gNnSc1	město
dováželo	dovážet	k5eAaImAgNnS	dovážet
obilí	obilí	k1gNnSc4	obilí
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejhorších	zlý	k2eAgInPc6d3	Nejhorší
měsících	měsíc	k1gInPc6	měsíc
umíralo	umírat	k5eAaImAgNnS	umírat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
až	až	k9	až
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
zabila	zabít	k5eAaPmAgFnS	zabít
asi	asi	k9	asi
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Evropou	Evropa	k1gFnSc7	Evropa
šířily	šířit	k5eAaImAgFnP	šířit
další	další	k2eAgFnPc1d1	další
vlny	vlna	k1gFnPc1	vlna
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
virulentní	virulentní	k2eAgFnSc1d1	virulentní
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
asi	asi	k9	asi
na	na	k7c4	na
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
zvaná	zvaný	k2eAgFnSc1d1	zvaná
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
nejdříve	dříve	k6eAd3	dříve
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
propukaly	propukat	k5eAaImAgFnP	propukat
pandemie	pandemie	k1gFnPc1	pandemie
jak	jak	k6eAd1	jak
u	u	k7c2	u
místních	místní	k2eAgFnPc2d1	místní
kočovných	kočovný	k2eAgFnPc2d1	kočovná
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
u	u	k7c2	u
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
usazených	usazený	k2eAgFnPc2d1	usazená
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
výboji	výboj	k1gInPc7	výboj
Mongolů	mongol	k1gInPc2	mongol
a	a	k8xC	a
pohyby	pohyb	k1gInPc4	pohyb
jejich	jejich	k3xOp3gNnPc2	jejich
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
moru	mor	k1gInSc2	mor
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
provincii	provincie	k1gFnSc4	provincie
Chu-pej	Chuej	k1gFnSc2	Chu-pej
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
jižních	jižní	k2eAgFnPc2d1	jižní
i	i	k8xC	i
severních	severní	k2eAgFnPc2d1	severní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vojenskými	vojenský	k2eAgInPc7d1	vojenský
přesuny	přesun	k1gInPc7	přesun
a	a	k8xC	a
cestami	cesta	k1gFnPc7	cesta
kupců	kupec	k1gMnPc2	kupec
po	po	k7c6	po
karavanních	karavanní	k2eAgFnPc6d1	karavanní
stezkách	stezka	k1gFnPc6	stezka
šířil	šířit	k5eAaImAgInS	šířit
mor	mor	k1gInSc1	mor
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
asijských	asijský	k2eAgFnPc2d1	asijská
stepí	step	k1gFnPc2	step
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
jednak	jednak	k8xC	jednak
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
byl	být	k5eAaImAgInS	být
mor	mor	k1gInSc1	mor
poprvé	poprvé	k6eAd1	poprvé
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
a	a	k8xC	a
Trapezuntu	Trapezunt	k1gInSc6	Trapezunt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
čilý	čilý	k2eAgInSc4d1	čilý
obchodní	obchodní	k2eAgInSc4d1	obchodní
styk	styk	k1gInSc4	styk
s	s	k7c7	s
asijským	asijský	k2eAgInSc7d1	asijský
východem	východ	k1gInSc7	východ
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
mor	mor	k1gInSc1	mor
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
také	také	k9	také
vojsko	vojsko	k1gNnSc4	vojsko
krymských	krymský	k2eAgInPc2d1	krymský
Tatarů	tatar	k1gInPc2	tatar
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
Benátčany	Benátčan	k1gMnPc7	Benátčan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obléhali	obléhat	k5eAaImAgMnP	obléhat
janovský	janovský	k2eAgInSc4d1	janovský
přístav	přístav	k1gInSc4	přístav
Kaffa	Kaff	k1gMnSc4	Kaff
na	na	k7c6	na
krymském	krymský	k2eAgNnSc6d1	krymské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Kaffy	Kaff	k1gMnPc4	Kaff
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
první	první	k4xOgNnSc4	první
užití	užití	k1gNnPc2	užití
biologické	biologický	k2eAgFnSc2d1	biologická
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obléhající	obléhající	k2eAgMnPc1d1	obléhající
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
města	město	k1gNnSc2	město
házeli	házet	k5eAaImAgMnP	házet
mrtvoly	mrtvola	k1gFnPc4	mrtvola
svých	svůj	k3xOyFgMnPc2	svůj
nakažených	nakažený	k2eAgMnPc2d1	nakažený
spolubojovníků	spolubojovník	k1gMnPc2	spolubojovník
přes	přes	k7c4	přes
hradby	hradba	k1gFnPc4	hradba
dovnitř	dovnitř	k7c2	dovnitř
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Janovská	janovský	k2eAgFnSc1d1	janovská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
flotila	flotila	k1gFnSc1	flotila
s	s	k7c7	s
nakaženými	nakažený	k2eAgMnPc7d1	nakažený
přistála	přistát	k5eAaPmAgFnS	přistát
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
sicilské	sicilský	k2eAgFnSc6d1	sicilská
Messině	Messina	k1gFnSc6	Messina
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
množství	množství	k1gNnSc2	množství
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lodě	loď	k1gFnPc1	loď
převážely	převážet	k5eAaImAgFnP	převážet
i	i	k9	i
nakažené	nakažený	k2eAgMnPc4d1	nakažený
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
hlavními	hlavní	k2eAgInPc7d1	hlavní
přenašeči	přenašeč	k1gInPc7	přenašeč
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Stykem	styk	k1gInSc7	styk
s	s	k7c7	s
janovskými	janovský	k2eAgMnPc7d1	janovský
námořníky	námořník	k1gMnPc7	námořník
(	(	kIx(	(
<g/>
Messiňané	Messiňan	k1gMnPc1	Messiňan
údajně	údajně	k6eAd1	údajně
některé	některý	k3yIgFnPc4	některý
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
lodi	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
posádky	posádka	k1gFnPc1	posádka
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
chodili	chodit	k5eAaImAgMnP	chodit
bezostyšně	bezostyšně	k6eAd1	bezostyšně
rabovat	rabovat	k5eAaImF	rabovat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
dostal	dostat	k5eAaPmAgInS	dostat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Janovu	Janov	k1gInSc3	Janov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
západně	západně	k6eAd1	západně
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
na	na	k7c4	na
ligurské	ligurský	k2eAgNnSc4d1	Ligurské
a	a	k8xC	a
provensálské	provensálský	k2eAgNnSc4d1	provensálské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Janově	Janov	k1gInSc6	Janov
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1347	[number]	k4	1347
<g/>
/	/	kIx~	/
<g/>
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
a	a	k8xC	a
1349	[number]	k4	1349
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
na	na	k7c4	na
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
Podunají	Podunají	k1gNnSc2	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
šířila	šířit	k5eAaImAgFnS	šířit
nákaza	nákaza	k1gFnSc1	nákaza
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
již	již	k9	již
byla	být	k5eAaImAgFnS	být
morem	mor	k1gInSc7	mor
nakažena	nakažen	k2eAgFnSc1d1	nakažena
polovina	polovina	k1gFnSc1	polovina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostala	dostat	k5eAaPmAgFnS	dostat
nákaza	nákaza	k1gFnSc1	nákaza
i	i	k9	i
do	do	k7c2	do
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
nemnohé	mnohý	k2eNgFnPc1d1	nemnohá
oblasti	oblast	k1gFnPc1	oblast
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
moru	mora	k1gFnSc4	mora
ušetřeny	ušetřen	k2eAgFnPc4d1	ušetřena
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
řídkému	řídký	k2eAgNnSc3d1	řídké
osídlení	osídlení	k1gNnSc3	osídlení
nebo	nebo	k8xC	nebo
izolaci	izolace	k1gFnSc3	izolace
od	od	k7c2	od
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Agnolo	Agnola	k1gFnSc5	Agnola
di	di	k?	di
Tura	tur	k1gMnSc4	tur
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Sienské	sienský	k2eAgFnSc2d1	Sienská
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
řádění	řádění	k1gNnSc1	řádění
moru	mor	k1gInSc2	mor
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Siena	Siena	k1gFnSc1	Siena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
popisuje	popisovat	k5eAaImIp3nS	popisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
<g/>
A	a	k8xC	a
lidskému	lidský	k2eAgInSc3d1	lidský
jazyku	jazyk	k1gInSc3	jazyk
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
vylíčit	vylíčit	k5eAaPmF	vylíčit
tu	ten	k3xDgFnSc4	ten
strašnou	strašný	k2eAgFnSc4d1	strašná
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Vskutku	vskutku	k9	vskutku
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nespatří	spatřit	k5eNaPmIp3nS	spatřit
tu	ten	k3xDgFnSc4	ten
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
,	,	kIx,	,
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
zván	zván	k2eAgInSc1d1	zván
blažený	blažený	k2eAgInSc1d1	blažený
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
oběti	oběť	k1gFnPc1	oběť
umíraly	umírat	k5eAaImAgFnP	umírat
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Otékaly	otékat	k5eAaImAgInP	otékat
pod	pod	k7c7	pod
pažemi	paže	k1gFnPc7	paže
a	a	k8xC	a
ve	v	k7c6	v
slabinách	slabina	k1gFnPc6	slabina
a	a	k8xC	a
padaly	padat	k5eAaImAgFnP	padat
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
mrtvy	mrtev	k2eAgFnPc4d1	mrtva
během	během	k7c2	během
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
opouštěl	opouštět	k5eAaImAgMnS	opouštět
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
bratr	bratr	k1gMnSc1	bratr
druhého	druhý	k4xOgMnSc4	druhý
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zachvacovala	zachvacovat	k5eAaImAgFnS	zachvacovat
dechem	dech	k1gInSc7	dech
a	a	k8xC	a
pohledem	pohled	k1gInSc7	pohled
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
...	...	k?	...
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
se	se	k3xPyFc4	se
vykopávaly	vykopávat	k5eAaImAgFnP	vykopávat
velké	velký	k2eAgFnPc1d1	velká
jámy	jáma	k1gFnPc1	jáma
a	a	k8xC	a
plnily	plnit	k5eAaImAgFnP	plnit
množstvím	množství	k1gNnSc7	množství
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
umírali	umírat	k5eAaImAgMnP	umírat
po	po	k7c6	po
stovkách	stovka	k1gFnPc6	stovka
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
vhazováni	vhazovat	k5eAaImNgMnP	vhazovat
do	do	k7c2	do
těch	ten	k3xDgFnPc2	ten
jam	jáma	k1gFnPc2	jáma
a	a	k8xC	a
zahazováni	zahazován	k2eAgMnPc1d1	zahazován
hlínou	hlína	k1gFnSc7	hlína
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
jámy	jáma	k1gFnPc1	jáma
naplnily	naplnit	k5eAaPmAgFnP	naplnit
<g/>
,	,	kIx,	,
vykopávaly	vykopávat	k5eAaImAgFnP	vykopávat
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Agnolo	Agnola	k1gFnSc5	Agnola
z	z	k7c2	z
Tury	tur	k1gMnPc4	tur
...	...	k?	...
jsem	být	k5eAaImIp1nS	být
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
zakopal	zakopat	k5eAaPmAgMnS	zakopat
svých	svůj	k3xOyFgFnPc2	svůj
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
...	...	k?	...
A	a	k9	a
tak	tak	k9	tak
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
umíralo	umírat	k5eAaImAgNnS	umírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
první	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
soudobého	soudobý	k2eAgMnSc2d1	soudobý
kronikáře	kronikář	k1gMnSc2	kronikář
Františka	František	k1gMnSc2	František
Pražského	pražský	k2eAgInSc2d1	pražský
první	první	k4xOgFnSc4	první
vlnu	vlna	k1gFnSc4	vlna
moru	mor	k1gInSc2	mor
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
"	"	kIx"	"
<g/>
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
a	a	k8xC	a
chladné	chladný	k2eAgNnSc1d1	chladné
povětří	povětří	k1gNnSc1	povětří
<g/>
''	''	k?	''
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
suchým	suchý	k2eAgInPc3d1	suchý
rokům	rok	k1gInPc3	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1356	[number]	k4	1356
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
z	z	k7c2	z
nevelkého	velký	k2eNgNnSc2d1	nevelké
ohniska	ohnisko	k1gNnSc2	ohnisko
v	v	k7c6	v
Hessensku	Hessensko	k1gNnSc6	Hessensko
šířit	šířit	k5eAaImF	šířit
nová	nový	k2eAgFnSc1d1	nová
pandemie	pandemie	k1gFnSc1	pandemie
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kulminace	kulminace	k1gFnSc1	kulminace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1357	[number]	k4	1357
<g/>
-	-	kIx~	-
<g/>
1360	[number]	k4	1360
a	a	k8xC	a
1363	[number]	k4	1363
<g/>
-	-	kIx~	-
<g/>
1366	[number]	k4	1366
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
známý	známý	k2eAgInSc4d1	známý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
moru	mor	k1gInSc2	mor
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
Čechám	Čechy	k1gFnPc3	Čechy
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
ji	on	k3xPp3gFnSc4	on
máme	mít	k5eAaImIp1nP	mít
bezpečně	bezpečně	k6eAd1	bezpečně
doloženou	doložená	k1gFnSc4	doložená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1357	[number]	k4	1357
<g/>
-	-	kIx~	-
<g/>
1363	[number]	k4	1363
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlna	vlna	k1gFnSc1	vlna
moru	mora	k1gFnSc4	mora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
rozhodně	rozhodně	k6eAd1	rozhodně
vlnou	vlna	k1gFnSc7	vlna
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
ve	v	k7c6	v
zhruba	zhruba	k6eAd1	zhruba
dvacetiletých	dvacetiletý	k2eAgInPc6d1	dvacetiletý
intervalech	interval	k1gInPc6	interval
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
už	už	k6eAd1	už
ale	ale	k8xC	ale
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
útoky	útok	k1gInPc7	útok
moru	mor	k1gInSc2	mor
na	na	k7c4	na
Evropu	Evropa	k1gFnSc4	Evropa
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
větší	veliký	k2eAgFnPc4d2	veliký
epidemie	epidemie	k1gFnPc4	epidemie
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Velký	velký	k2eAgInSc1d1	velký
londýnský	londýnský	k2eAgInSc1d1	londýnský
mor	mor	k1gInSc1	mor
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
<g/>
-	-	kIx~	-
<g/>
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
mor	mor	k1gInSc1	mor
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
1629	[number]	k4	1629
<g/>
-	-	kIx~	-
<g/>
1631	[number]	k4	1631
nebo	nebo	k8xC	nebo
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1679	[number]	k4	1679
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
i	i	k9	i
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
a	a	k8xC	a
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
významný	významný	k2eAgInSc1d1	významný
výskyt	výskyt	k1gInSc1	výskyt
moru	mor	k1gInSc2	mor
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c4	v
Marseille	Marseille	k1gFnPc4	Marseille
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1720	[number]	k4	1720
<g/>
-	-	kIx~	-
<g/>
1721	[number]	k4	1721
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1713	[number]	k4	1713
<g/>
-	-	kIx~	-
<g/>
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
si	se	k3xPyFc3	se
tato	tento	k3xDgFnSc1	tento
epidemie	epidemie	k1gFnSc1	epidemie
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
se	se	k3xPyFc4	se
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
často	často	k6eAd1	často
šířil	šířit	k5eAaImAgMnS	šířit
z	z	k7c2	z
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
propukla	propuknout	k5eAaPmAgFnS	propuknout
velká	velký	k2eAgFnSc1d1	velká
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
mezi	mezi	k7c7	mezi
ruskými	ruský	k2eAgMnPc7d1	ruský
vojáky	voják	k1gMnPc7	voják
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
rusko-turecké	ruskourecký	k2eAgFnSc2d1	rusko-turecká
války	válka	k1gFnSc2	válka
a	a	k8xC	a
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
až	až	k9	až
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
epidemie	epidemie	k1gFnSc1	epidemie
moru	mor	k1gInSc2	mor
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
půdě	půda	k1gFnSc6	půda
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
tureckého	turecký	k2eAgInSc2d1	turecký
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
morová	morový	k2eAgFnSc1d1	morová
pandemie	pandemie	k1gFnSc1	pandemie
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
miliony	milion	k4xCgInPc4	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
kontinenty	kontinent	k1gInPc4	kontinent
včetně	včetně	k7c2	včetně
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
onemocnění	onemocnění	k1gNnSc2	onemocnění
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
hlášeno	hlásit	k5eAaImNgNnS	hlásit
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
Amerik	Amerika	k1gFnPc2	Amerika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
:	:	kIx,	:
Původce	původce	k1gMnSc1	původce
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
přítomný	přítomný	k2eAgInSc1d1	přítomný
ve	v	k7c6	v
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
populacích	populace	k1gFnPc6	populace
od	od	k7c2	od
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
přes	přes	k7c4	přes
centrální	centrální	k2eAgNnSc4d1	centrální
<g />
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
a	a	k8xC	a
části	část	k1gFnPc1	část
Číny	Čína	k1gFnSc2	Čína
<g/>
;	;	kIx,	;
v	v	k7c6	v
Jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
a	a	k8xC	a
Jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
včetně	včetně	k7c2	včetně
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
Velkým	velký	k2eAgFnPc3d1	velká
pláním	pláň	k1gFnPc3	pláň
a	a	k8xC	a
z	z	k7c2	z
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
až	až	k9	až
po	po	k7c4	po
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
a	a	k8xC	a
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Původce	původce	k1gMnSc1	původce
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ve	v	k7c6	v
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
populacích	populace	k1gFnPc6	populace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgNnSc4d1	rychlé
šíření	šíření	k1gNnSc4	šíření
moru	mor	k1gInSc2	mor
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
historických	historický	k2eAgNnPc6d1	historické
obdobích	období	k1gNnPc6	období
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
některé	některý	k3yIgFnPc4	některý
spekulativní	spekulativní	k2eAgFnPc4d1	spekulativní
a	a	k8xC	a
neúplně	úplně	k6eNd1	úplně
prokázané	prokázaný	k2eAgFnPc4d1	prokázaná
hypotézy	hypotéza	k1gFnPc4	hypotéza
<g/>
:	:	kIx,	:
Některé	některý	k3yIgInPc1	některý
tzv.	tzv.	kA	tzv.
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
nebyly	být	k5eNaImAgFnP	být
bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
podstaty	podstata	k1gFnPc1	podstata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
virové	virový	k2eAgInPc1d1	virový
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gInSc1	Susan
Scottová	Scottová	k1gFnSc1	Scottová
<g/>
,	,	kIx,	,
Christophera	Christophera	k1gFnSc1	Christophera
Duncan	Duncana	k1gFnPc2	Duncana
a	a	k8xC	a
Stephen	Stephno	k1gNnPc2	Stephno
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k1gInSc4	Brien
opírají	opírat	k5eAaImIp3nP	opírat
svou	svůj	k3xOyFgFnSc4	svůj
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c4	o
biologické	biologický	k2eAgInPc4d1	biologický
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
přenášely	přenášet	k5eAaImAgInP	přenášet
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Publicista	publicista	k1gMnSc1	publicista
Darius	Darius	k1gMnSc1	Darius
Nosreti	Nosret	k1gMnPc1	Nosret
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
lokality	lokalita	k1gFnPc4	lokalita
historického	historický	k2eAgInSc2d1	historický
zdroje	zdroj	k1gInSc2	zdroj
infekce	infekce	k1gFnSc2	infekce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
poušť	poušť	k1gFnSc1	poušť
Gobi	Gobi	k1gFnSc1	Gobi
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
figurují	figurovat	k5eAaImIp3nP	figurovat
jako	jako	k8xC	jako
zimoviště	zimoviště	k1gNnPc1	zimoviště
čápů	čáp	k1gMnPc2	čáp
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sekundárním	sekundární	k2eAgMnSc7d1	sekundární
hostitelem	hostitel	k1gMnSc7	hostitel
morové	morový	k2eAgFnSc2d1	morová
blechy	blecha	k1gFnSc2	blecha
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
obdobích	období	k1gNnPc6	období
historie	historie	k1gFnSc2	historie
byl	být	k5eAaImAgMnS	být
nejen	nejen	k6eAd1	nejen
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
reflektujících	reflektující	k2eAgNnPc2d1	reflektující
středověké	středověký	k2eAgFnSc2d1	středověká
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
je	být	k5eAaImIp3nS	být
Dekameron	Dekameron	k1gInSc1	Dekameron
-	-	kIx~	-
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348	[number]	k4	1348
až	až	k9	až
1353	[number]	k4	1353
napsal	napsat	k5eAaBmAgMnS	napsat
italský	italský	k2eAgMnSc1d1	italský
renesanční	renesanční	k2eAgMnSc1d1	renesanční
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
novelista	novelista	k1gMnSc1	novelista
Giovanni	Giovann	k1gMnPc1	Giovann
Boccaccio	Boccaccio	k1gMnSc1	Boccaccio
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
sta	sto	k4xCgNnPc4	sto
novel	novela	k1gFnPc2	novela
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
deseti	deset	k4xCc2	deset
oddílů	oddíl	k1gInPc2	oddíl
po	po	k7c6	po
deseti	deset	k4xCc6	deset
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
po	po	k7c6	po
dni	den	k1gInSc6	den
po	po	k7c4	po
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
deset	deset	k4xCc1	deset
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
utekli	utéct	k5eAaPmAgMnP	utéct
z	z	k7c2	z
města	město	k1gNnSc2	město
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
Dekameronu	Dekameron	k1gInSc2	Dekameron
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovněž	rovněž	k9	rovněž
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
a	a	k8xC	a
nejpodrobnějších	podrobný	k2eAgInPc2d3	nejpodrobnější
popisů	popis	k1gInPc2	popis
moru	mora	k1gFnSc4	mora
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
