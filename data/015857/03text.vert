<s>
Kevin	Kevin	k2eAgInSc1d1
Youkilis	Youkilis	k1gInSc1
</s>
<s>
Kevin	Kevin	k2eAgInSc1d1
Youkilis	Youkilis	k1gInSc1
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
44	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Cincinnati	Cincinnat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
USA	USA	kA
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Číslo	číslo	k1gNnSc1
</s>
<s>
20	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
meta	meta	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
meta	meta	k1gFnSc1
Týmy	tým	k1gInPc1
</s>
<s>
Boston	Boston	k1gInSc1
Red	Red	k1gMnSc2
Sox	Sox	k1gMnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
Chicago	Chicago	k1gNnSc1
White	Whit	k1gInSc5
Sox	Sox	k1gMnSc6
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
New	New	k1gFnPc2
York	York	k1gInSc1
Yankees	Yankees	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Tohoku	Tohok	k1gInSc2
Rakuten	Rakutno	k1gNnPc2
Golden	Goldna	k1gFnPc2
Eagles	Eaglesa	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Zisk	zisk	k1gInSc1
Světové	světový	k2eAgFnSc2d1
série	série	k1gFnSc2
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
(	(	kIx(
<g/>
Boston	Boston	k1gInSc1
Red	Red	k1gMnSc2
Sox	Sox	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kevin	Kevin	k1gMnSc1
Edmund	Edmund	k1gMnSc1
Youkilis	Youkilis	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1979	#num#	k4
<g/>
,	,	kIx,
Cincinnati	Cincinnati	k1gFnSc1
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
baseballista	baseballista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgMnS
americkou	americký	k2eAgFnSc4d1
Major	major	k1gMnSc1
League	Leagu	k1gInSc2
Baseball	baseball	k1gInSc1
za	za	k7c4
týmy	tým	k1gInPc4
Boston	Boston	k1gInSc1
Red	Red	k1gMnPc2
Sox	Sox	k1gFnSc2
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
White	Whit	k1gInSc5
Sox	Sox	k1gMnPc2
a	a	k8xC
New	New	k1gMnPc2
York	York	k1gInSc1
Yankees	Yankeesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
působil	působit	k5eAaImAgMnS
v	v	k7c6
japonské	japonský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
v	v	k7c6
týmu	tým	k1gInSc6
Tohoku	Tohok	k1gInSc2
Rakuten	Rakutno	k1gNnPc2
Golden	Goldna	k1gFnPc2
Eagles	Eaglesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
dvojnásobným	dvojnásobný	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
Světové	světový	k2eAgFnSc2d1
série	série	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
2004	#num#	k4
a	a	k8xC
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kevin	Kevin	k2eAgInSc4d1
Youkilis	Youkilis	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
profil	profil	k1gInSc1
na	na	k7c4
MLB	MLB	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
profil	profil	k1gInSc1
na	na	k7c4
ESPN	ESPN	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2065	#num#	k4
4923	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2011088584	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
171657328	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2011088584	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
