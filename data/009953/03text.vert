<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Urbanus	Urbanus	k1gInSc1	Urbanus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
222	[number]	k4	222
<g/>
–	–	k?	–
<g/>
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
230	[number]	k4	230
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
vinařů	vinař	k1gMnPc2	vinař
a	a	k8xC	a
bednářů	bednář	k1gMnPc2	bednář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
o	o	k7c6	o
sv.	sv.	kA	sv.
Urbanovi	Urban	k1gMnSc6	Urban
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
občasná	občasný	k2eAgFnSc1d1	občasná
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
neznámým	známý	k2eNgMnSc7d1	neznámý
biskupem	biskup	k1gMnSc7	biskup
Urbanem	Urban	k1gMnSc7	Urban
<g/>
.	.	kIx.	.
</s>
<s>
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandra	k1gFnSc1	Alexandra
Severa	Severa	k1gMnSc1	Severa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
obdobím	období	k1gNnSc7	období
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
bez	bez	k7c2	bez
přílišného	přílišný	k2eAgNnSc2d1	přílišné
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnPc2	Pontificalis
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Pontianus	Pontianus	k1gMnSc1	Pontianus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Urbanova	Urbanův	k2eAgInSc2d1	Urbanův
pontifikátu	pontifikát	k1gInSc2	pontifikát
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
spory	spor	k1gInPc1	spor
se	s	k7c7	s
vzdoropapežem	vzdoropapež	k1gMnSc7	vzdoropapež
Hippolytem	Hippolyt	k1gInSc7	Hippolyt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
zastával	zastávat	k5eAaImAgMnS	zastávat
postoje	postoj	k1gInPc4	postoj
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
Zefyrina	Zefyrin	k1gMnSc2	Zefyrin
a	a	k8xC	a
Kalixta	Kalixta	k1gMnSc1	Kalixta
I.	I.	kA	I.
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
také	také	k9	také
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
Valeriána	valeriána	k1gFnSc1	valeriána
<g/>
,	,	kIx,	,
manžela	manžel	k1gMnSc4	manžel
svaté	svatý	k2eAgFnSc2d1	svatá
Cecilie	Cecilie	k1gFnSc2	Cecilie
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
bratra	bratr	k1gMnSc2	bratr
Tiburtia	Tiburtius	k1gMnSc2	Tiburtius
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
shodu	shoda	k1gFnSc4	shoda
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Urban	Urban	k1gMnSc1	Urban
nebyl	být	k5eNaImAgMnS	být
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
230	[number]	k4	230
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
uctívá	uctívat	k5eAaImIp3nS	uctívat
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	on	k3xPp3gInSc2	on
pohřbu	pohřeb	k1gInSc2	pohřeb
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandra	k1gFnSc1	Alexandra
Severa	Severa	k1gMnSc1	Severa
prakticky	prakticky	k6eAd1	prakticky
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
zdají	zdát	k5eAaPmIp3nP	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
legendy	legenda	k1gFnPc4	legenda
o	o	k7c6	o
mučednické	mučednický	k2eAgFnSc6d1	mučednická
smrti	smrt	k1gFnSc6	smrt
Urbana	Urban	k1gMnSc2	Urban
I.	I.	kA	I.
málo	málo	k1gNnSc1	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnSc2	Pontificalis
byl	být	k5eAaImAgMnS	být
Urban	Urban	k1gMnSc1	Urban
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Pretestatově	Pretestatův	k2eAgFnSc3d1	Pretestatův
hřitově	hřitův	k2eAgFnSc3d1	hřitova
(	(	kIx(	(
<g/>
Coemetarium	Coemetarium	k1gNnSc1	Coemetarium
Praetextati	Praetextati	k1gFnPc2	Praetextati
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
hrobka	hrobka	k1gFnSc1	hrobka
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
v	v	k7c6	v
Kalixtových	Kalixtův	k2eAgFnPc6d1	Kalixtův
katakombách	katakomby	k1gFnPc6	katakomby
však	však	k9	však
italský	italský	k2eAgMnSc1d1	italský
archeolog	archeolog	k1gMnSc1	archeolog
Giovanni	Giovanň	k1gMnSc3	Giovanň
dei	dei	k?	dei
Rossi	Ross	k1gMnPc7	Ross
nalezl	nalézt	k5eAaBmAgMnS	nalézt
víko	víko	k1gNnSc4	víko
sarkofágu	sarkofág	k1gInSc2	sarkofág
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
Urban	Urban	k1gMnSc1	Urban
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Urban	Urban	k1gMnSc1	Urban
pohřbený	pohřbený	k2eAgMnSc1d1	pohřbený
na	na	k7c6	na
Pretestatově	Pretestatův	k2eAgMnSc6d1	Pretestatův
hřitově	hřitově	k6eAd1	hřitově
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
jiným	jiný	k2eAgMnSc7d1	jiný
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
Urbana	Urban	k1gMnSc2	Urban
I.	I.	kA	I.
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
seznam	seznam	k1gInSc1	seznam
Sixta	Sixtus	k1gMnSc2	Sixtus
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgNnPc4d1	obsahující
jména	jméno	k1gNnPc4	jméno
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
pohřbených	pohřbený	k2eAgFnPc2d1	pohřbená
v	v	k7c6	v
Kalixtových	Kalixtův	k2eAgFnPc6d1	Kalixtův
katakombách	katakomby	k1gFnPc6	katakomby
neudává	udávat	k5eNaImIp3nS	udávat
Urbana	Urban	k1gMnSc4	Urban
jako	jako	k9	jako
nástupce	nástupce	k1gMnSc1	nástupce
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
cizího	cizí	k2eAgInSc2d1	cizí
biskupa	biskup	k1gInSc2	biskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Urban	Urban	k1gMnSc1	Urban
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Documenta	Documenta	k1gFnSc1	Documenta
Catholica	Catholic	k1gInSc2	Catholic
Omnia	omnium	k1gNnSc2	omnium
-	-	kIx~	-
www.documentacatholicaomnia.eu	www.documentacatholicaomnia.eu	k6eAd1	www.documentacatholicaomnia.eu
latinsky	latinsky	k6eAd1	latinsky
</s>
</p>
