<s>
Plovací	plovací	k2eAgFnPc1d1
blány	blána	k1gFnPc1
mají	mít	k5eAaImIp3nP
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc4
druhy	druh	k1gInPc4
sladkovodních	sladkovodní	k2eAgFnPc2d1
želv	želva	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
několik	několik	k4yIc1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
karetka	karetka	k1gFnSc1
novoguinejská	novoguinejský	k2eAgFnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
místo	místo	k7c2
končetin	končetina	k1gFnPc2
ploutve	ploutev	k1gFnSc2
<g/>
.	.	kIx.
</s>