<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
francouzským	francouzský	k2eAgInSc7d1
názvem	název	k1gInSc7
Arc	Arc	k1gFnSc2
de	de	k?
Triomphe	Triomph	k1gFnSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Étoile	Étoila	k1gFnSc3
nebo	nebo	k8xC
jen	jen	k9
Arc	Arc	k1gFnSc1
de	de	k?
Triomphe	Triomphe	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vítězný	vítězný	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dal	dát	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
Napoleon	napoleon	k1gInSc1
Bonaparte	Bonaparte	k1gInSc1
na	na	k7c4
paměť	paměť	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vítězství	vítězství	k1gNnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>