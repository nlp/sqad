<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
klasicistní	klasicistní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Jean-François	Jean-François	k1gInSc1
Chalgrin	Chalgrin	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1836	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
obvod	obvod	k1gInSc1
<g/>
,	,	kIx,
Quartier	Quartier	k1gMnSc1
des	des	k1gNnSc2
Ternes	Ternes	k1gMnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Ulice	ulice	k1gFnSc1
</s>
<s>
Place	plac	k1gInSc6
Charles-de-Gaulle	Charles-de-Gaulle	k1gFnPc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
PA00088804	PA00088804	k4
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
na	na	k7c6
východním	východní	k2eAgInSc6d1
konci	konec	k1gInSc6
Champs-Élysées	Champs-Élyséesa	k1gFnPc2
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
v	v	k7c6
noci	noc	k1gFnSc6
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
francouzským	francouzský	k2eAgInSc7d1
názvem	název	k1gInSc7
Arc	Arc	k1gFnSc2
de	de	k?
Triomphe	Triomph	k1gFnSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Étoile	Étoila	k1gFnSc3
nebo	nebo	k8xC
jen	jen	k9
Arc	Arc	k1gFnSc1
de	de	k?
Triomphe	Triomphe	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vítězný	vítězný	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dal	dát	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
Napoleon	napoleon	k1gInSc1
Bonaparte	Bonaparte	k1gInSc1
na	na	k7c4
paměť	paměť	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vítězství	vítězství	k1gNnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblouk	oblouk	k1gInSc1
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
v	v	k7c6
letech	léto	k1gNnPc6
1806-1836	1806-1836	k4
a	a	k8xC
stojí	stát	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
náměstí	náměstí	k1gNnSc6
Place	plac	k1gInSc6
Charles-de-Gaulle	Charles-de-Gaulle	k1gFnSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Place	plac	k1gInSc6
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Étoile	Étoila	k1gFnSc6
<g/>
)	)	kIx)
na	na	k7c6
západním	západní	k2eAgInSc6d1
konci	konec	k1gInSc6
slavného	slavný	k2eAgInSc2d1
pařížského	pařížský	k2eAgInSc2d1
bulváru	bulvár	k1gInSc2
Avenue	avenue	k1gFnSc2
des	des	k1gNnPc2
Champs-Élysées	Champs-Élysées	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
stavby	stavba	k1gFnSc2
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
zaměřené	zaměřený	k2eAgNnSc1d1
na	na	k7c6
historii	historie	k1gFnSc6
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
u	u	k7c2
oblouku	oblouk	k1gInSc2
bývá	bývat	k5eAaImIp3nS
ukončen	ukončit	k5eAaPmNgInS
cyklistický	cyklistický	k2eAgInSc1d1
závod	závod	k1gInSc1
Tour	Tour	k1gInSc1
de	de	k?
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
památníku	památník	k1gInSc3
je	být	k5eAaImIp3nS
podzemní	podzemní	k2eAgFnSc7d1
chodbou	chodba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
střeše	střecha	k1gFnSc6
je	být	k5eAaImIp3nS
veřejně	veřejně	k6eAd1
přístupná	přístupný	k2eAgFnSc1d1
vyhlídková	vyhlídkový	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
předlohou	předloha	k1gFnSc7
pro	pro	k7c4
Dianin	Dianin	k2eAgInSc4d1
chrám	chrám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kolorovaná	kolorovaný	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
náměstí	náměstí	k1gNnSc2
s	s	k7c7
Obloukem	oblouk	k1gInSc7
z	z	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
(	(	kIx(
<g/>
pohled	pohled	k1gInSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Stavbu	stavba	k1gFnSc4
nařídil	nařídit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
1806	#num#	k4
po	po	k7c6
vítězné	vítězný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbou	stavba	k1gFnSc7
byli	být	k5eAaImAgMnP
pověřeni	pověřen	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
Jean-François	Jean-François	k1gFnSc2
Chalgrin	Chalgrin	k1gInSc1
a	a	k8xC
Jean-Arnaud	Jean-Arnaud	k1gMnSc1
Raymond	Raymond	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
však	však	k9
nemohli	moct	k5eNaImAgMnP
shodnout	shodnout	k5eAaPmF,k5eAaBmF
na	na	k7c4
koncepci	koncepce	k1gFnSc4
a	a	k8xC
přednost	přednost	k1gFnSc4
dostal	dostat	k5eAaPmAgInS
Chalgrinův	Chalgrinův	k2eAgInSc1d1
projekt	projekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1806	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
vyhloubení	vyhloubení	k1gNnSc4
základů	základ	k1gInPc2
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
2	#num#	k4
roky	rok	k1gInPc4
stavebních	stavební	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
přijel	přijet	k5eAaPmAgMnS
Napoleon	Napoleon	k1gMnSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
nevěstou	nevěsta	k1gFnSc7
arcivévodkyní	arcivévodkyně	k1gFnSc7
Marií	Maria	k1gFnSc7
Luisou	Luisa	k1gFnSc7
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
pilíře	pilíř	k1gInPc1
oblouku	oblouk	k1gInSc2
teprve	teprve	k6eAd1
ve	v	k7c6
výšce	výška	k1gFnSc6
asi	asi	k9
jeden	jeden	k4xCgInSc4
metr	metr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
proto	proto	k6eAd1
postavena	postaven	k2eAgFnSc1d1
dřevěná	dřevěný	k2eAgFnSc1d1
maketa	maketa	k1gFnSc1
kompletního	kompletní	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1811	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
architekt	architekt	k1gMnSc1
Chalgrin	Chalgrin	k1gInSc1
a	a	k8xC
po	po	k7c6
osmi	osm	k4xCc6
dnech	den	k1gInPc6
i	i	k8xC
jeho	jeho	k3xOp3gFnSc4
konkurent	konkurent	k1gMnSc1
Raymond	Raymond	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
pozdější	pozdní	k2eAgInPc4d2
Napoleonovy	Napoleonův	k2eAgInPc4d1
neúspěchy	neúspěch	k1gInPc4
(	(	kIx(
<g/>
ruské	ruský	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
<g/>
,	,	kIx,
koaliční	koaliční	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
1814	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
dokončena	dokončit	k5eAaPmNgFnS
až	až	k9
k	k	k7c3
obloukům	oblouk	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
během	během	k7c2
restaurace	restaurace	k1gFnSc2
Bourbonů	bourbon	k1gInPc2
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
přerušena	přerušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
XVIII	XVIII	kA
<g/>
.	.	kIx.
obnovil	obnovit	k5eAaPmAgInS
stavbu	stavba	k1gFnSc4
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
podíleli	podílet	k5eAaImAgMnP
architekti	architekt	k1gMnPc1
Louis-Robert	Louis-Robert	k1gMnSc1
Goust	Goust	k1gMnSc1
<g/>
,	,	kIx,
Jean-Nicolas	Jean-Nicolas	k1gMnSc1
Huyot	Huyot	k1gMnSc1
a	a	k8xC
Héricart	Héricart	k1gInSc1
de	de	k?
Thury	Thura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
a	a	k8xC
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Adolphe	Adolph	k1gInSc2
Thiers	Thiersa	k1gFnPc2
rozhodli	rozhodnout	k5eAaPmAgMnP
o	o	k7c6
sochařské	sochařský	k2eAgFnSc6d1
výzdobě	výzdoba	k1gFnSc6
oblouku	oblouk	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
zahrnovat	zahrnovat	k5eAaImF
úspěchy	úspěch	k1gInPc4
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1792	#num#	k4
<g/>
–	–	k?
<g/>
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblouk	oblouk	k1gInSc1
byl	být	k5eAaImAgInS
definitivně	definitivně	k6eAd1
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
letech	léto	k1gNnPc6
1832-36	1832-36	k4
a	a	k8xC
slavnostně	slavnostně	k6eAd1
pak	pak	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1836	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
Červencové	červencový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1896	#num#	k4
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
mezi	mezi	k7c4
historické	historický	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
část	část	k1gFnSc1
malé	malý	k2eAgFnSc2d1
arkády	arkáda	k1gFnSc2
</s>
<s>
Autorem	autor	k1gMnSc7
návrhu	návrh	k1gInSc2
byl	být	k5eAaImAgMnS
architekt	architekt	k1gMnSc1
Jean-François	Jean-François	k1gFnSc2
Chalgrin	Chalgrin	k1gInSc1
(	(	kIx(
<g/>
1739	#num#	k4
<g/>
-	-	kIx~
<g/>
1811	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monument	monument	k1gInSc1
je	být	k5eAaImIp3nS
51	#num#	k4
m	m	kA
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
45	#num#	k4
m	m	kA
široký	široký	k2eAgMnSc1d1
a	a	k8xC
22	#num#	k4
m	m	kA
hluboký	hluboký	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výška	výška	k1gFnSc1
hlavních	hlavní	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
je	být	k5eAaImIp3nS
29,19	29,19	k4
m	m	kA
a	a	k8xC
rozpětí	rozpětí	k1gNnSc2
14,62	14,62	k4
m.	m.	k?
Menší	malý	k2eAgInSc1d2
<g/>
,	,	kIx,
boční	boční	k2eAgInPc1d1
oblouky	oblouk	k1gInPc1
mají	mít	k5eAaImIp3nP
výšku	výška	k1gFnSc4
18,68	18,68	k4
m	m	kA
a	a	k8xC
rozpětí	rozpětí	k1gNnSc2
8,44	8,44	k4
m.	m.	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
druhý	druhý	k4xOgInSc4
největší	veliký	k2eAgInSc4d3
vítězný	vítězný	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
zdobená	zdobený	k2eAgFnSc1d1
čtyřmi	čtyři	k4xCgNnPc7
hlavními	hlavní	k2eAgNnPc7d1
sousošími	sousoší	k1gNnPc7
na	na	k7c6
přední	přední	k2eAgFnSc6d1
a	a	k8xC
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
oblouku	oblouk	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Odjezd	odjezd	k1gInSc1
dobrovolníků	dobrovolník	k1gMnPc2
1792	#num#	k4
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
François	François	k1gFnSc2
Rude	Rud	k1gFnSc2
</s>
<s>
Triumf	triumf	k1gInSc1
1810	#num#	k4
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
Cortot	Cortota	k1gFnPc2
</s>
<s>
Odpor	odpor	k1gInSc1
1814	#num#	k4
<g/>
,	,	kIx,
Antoine	Antoin	k1gMnSc5
Étex	Étex	k1gInSc1
</s>
<s>
Mír	mír	k1gInSc1
1815	#num#	k4
<g/>
,	,	kIx,
Antoine	Antoin	k1gMnSc5
Étex	Étex	k1gInSc4
</s>
<s>
Dalších	další	k2eAgInPc2d1
šest	šest	k4xCc4
basreliéfů	basreliéf	k1gInPc2
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
na	na	k7c6
všech	všecek	k3xTgFnPc6
čtyřech	čtyři	k4xCgFnPc6
stranách	strana	k1gFnPc6
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Pohřeb	pohřeb	k1gInSc1
generála	generál	k1gMnSc2
Marceaua	Marceauus	k1gMnSc2
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1796	#num#	k4
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
P.	P.	kA
H.	H.	kA
Lamaire	Lamair	k1gInSc5
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Abúkíru	Abúkír	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1799	#num#	k4
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
Seurre	Seurr	k1gInSc5
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jemappes	Jemappesa	k1gFnPc2
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1792	#num#	k4
<g/>
,	,	kIx,
Carlo	Carlo	k1gNnSc1
Marochetti	Marochetť	k1gFnSc2
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Přechod	přechod	k1gInSc1
přes	přes	k7c4
most	most	k1gInSc4
v	v	k7c6
Arcole	Arcol	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1796	#num#	k4
<g/>
,	,	kIx,
Jean-Jacques	Jean-Jacques	k1gInSc1
Feuchè	Feuchè	k1gInSc5
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Alexandrie	Alexandrie	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1798	#num#	k4
<g/>
,	,	kIx,
John-Étienne	John-Étienn	k1gMnSc5
Chaponniè	Chaponniè	k1gMnSc5
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
<g/>
,	,	kIx,
Jean-François-Théodore	Jean-François-Théodor	k1gInSc5
Gechter	Gechtrum	k1gNnPc2
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Atika	Atika	k1gFnSc1
je	být	k5eAaImIp3nS
zdobená	zdobený	k2eAgFnSc1d1
30	#num#	k4
štíty	štít	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
s	s	k7c7
gladii	gladium	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
štítech	štít	k1gInPc6
jsou	být	k5eAaImIp3nP
vyrytá	vyrytý	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
bitev	bitva	k1gFnPc2
v	v	k7c6
revolučních	revoluční	k2eAgFnPc6d1
a	a	k8xC
napoleonských	napoleonský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
<g/>
:	:	kIx,
Valmy	Valm	k1gMnPc4
<g/>
,	,	kIx,
Jemappes	Jemappes	k1gMnSc1
<g/>
,	,	kIx,
Fleurus	Fleurus	k1gMnSc1
<g/>
,	,	kIx,
Montenotte	Montenott	k1gMnSc5
<g/>
,	,	kIx,
Lodi	loď	k1gFnSc5
<g/>
,	,	kIx,
Castiglione	Castiglion	k1gInSc5
<g/>
,	,	kIx,
Rivoli	Rivole	k1gFnSc6
<g/>
,	,	kIx,
Arcole	Arcole	k1gFnPc1
<g/>
,	,	kIx,
Pyramidy	pyramida	k1gFnPc1
<g/>
,	,	kIx,
Abukir	Abukir	k1gMnSc1
<g/>
,	,	kIx,
Alkmaar	Alkmaar	k1gMnSc1
<g/>
,	,	kIx,
Zurich	Zurich	k1gMnSc1
<g/>
,	,	kIx,
Heliopolis	Heliopolis	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Marengo	marengo	k1gNnSc1
<g/>
,	,	kIx,
Hohenlinden	Hohenlindno	k1gNnPc2
<g/>
,	,	kIx,
Ulm	Ulm	k1gFnSc1
<g/>
,	,	kIx,
Slavkov	Slavkov	k1gInSc1
<g/>
,	,	kIx,
Jena	Jena	k1gFnSc1
<g/>
,	,	kIx,
Friedland	Friedland	k1gInSc1
<g/>
,	,	kIx,
Somosierra	Somosierra	k1gFnSc1
<g/>
,	,	kIx,
Essling	Essling	k1gInSc1
<g/>
,	,	kIx,
Wagram	Wagram	k1gInSc1
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Lützen	Lützen	k2eAgInSc1d1
<g/>
,	,	kIx,
Budyšín	Budyšín	k1gInSc1
<g/>
,	,	kIx,
Drážďany	Drážďany	k1gInPc1
<g/>
,	,	kIx,
Hanau	Hanaa	k1gFnSc4
<g/>
,	,	kIx,
Montmirail	Montmirail	k1gInSc4
<g/>
,	,	kIx,
Montereau	Montereaa	k1gFnSc4
a	a	k8xC
Ligny	Ligna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Basreliéfy	basreliéf	k1gInPc1
na	na	k7c6
vlysu	vlys	k1gInSc6
hlavního	hlavní	k2eAgNnSc2d1
kladí	kladí	k1gNnSc2
podél	podél	k7c2
celé	celý	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
představují	představovat	k5eAaImIp3nP
Odjezd	odjezd	k1gInSc4
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
Brun	Brun	k1gMnSc1
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
Jacquot	Jacquot	k1gMnSc1
a	a	k8xC
Laité	Laita	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
Návrat	návrat	k1gInSc1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
Louis-Denis	Louis-Denis	k1gInSc1
Caillouette	Caillouett	k1gInSc5
<g/>
,	,	kIx,
François	François	k1gFnSc1
Rude	Rude	k1gFnSc1
a	a	k8xC
Bernard	Bernard	k1gMnSc1
Seurre	Seurr	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgFnPc1d1
arkády	arkáda	k1gFnPc1
jsou	být	k5eAaImIp3nP
po	po	k7c6
horních	horní	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
zdobeny	zdoben	k2eAgInPc1d1
alegorickými	alegorický	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
představujícími	představující	k2eAgInPc7d1
postavy	postav	k1gInPc7
z	z	k7c2
římské	římský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
James	James	k1gMnSc1
Pradier	Pradier	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
pilířů	pilíř	k1gInPc2
velkých	velký	k2eAgFnPc2d1
arkád	arkáda	k1gFnPc2
jsou	být	k5eAaImIp3nP
vyrytá	vyrytý	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
významných	významný	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
revolučních	revoluční	k2eAgFnPc2d1
a	a	k8xC
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
arkády	arkáda	k1gFnPc1
jsou	být	k5eAaImIp3nP
zdobeny	zdobit	k5eAaImNgFnP
rovněž	rovněž	k9
alegoriemi	alegorie	k1gFnPc7
představujícími	představující	k2eAgFnPc7d1
pěchotu	pěchota	k1gFnSc4
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
Théophile	Théophila	k1gFnSc3
Bra	Bra	k1gFnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jezdectvo	jezdectvo	k1gNnSc1
(	(	kIx(
<g/>
Achille-Joseph-Étienne	Achille-Joseph-Étienn	k1gMnSc5
Valois	Valois	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dělostřelectvo	dělostřelectvo	k1gNnSc1
(	(	kIx(
<g/>
Jean	Jean	k1gMnSc1
Baptiste	Baptist	k1gMnSc5
Joseph	Joseph	k1gMnSc1
De	De	k?
Bay	Bay	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
námořnictvo	námořnictvo	k1gNnSc1
(	(	kIx(
<g/>
Charles	Charles	k1gMnSc1
Émile	Émil	k1gMnSc5
Seurre	Seurr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vnitřních	vnitřní	k2eAgFnPc6d1
stěnách	stěna	k1gFnPc6
malých	malý	k2eAgFnPc2d1
arkád	arkáda	k1gFnPc2
jsou	být	k5eAaImIp3nP
vyryta	vyryt	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
vojenských	vojenský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
Prvního	první	k4xOgInSc2
císařství	císařství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc1
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
v	v	k7c6
bitvách	bitva	k1gFnPc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
podtržena	podtržen	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
basreliéfy	basreliéf	k1gInPc1
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
nad	nad	k7c7
jmény	jméno	k1gNnPc7
vojenských	vojenský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
symbolizují	symbolizovat	k5eAaImIp3nP
slavné	slavný	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Vítězné	vítězný	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
na	na	k7c6
severu	sever	k1gInSc6
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
François	François	k1gFnSc2
Joseph	Joseph	k1gMnSc1
Bosio	Bosio	k1gMnSc1
<g/>
)	)	kIx)
-	-	kIx~
scény	scéna	k1gFnSc2
z	z	k7c2
bitev	bitva	k1gFnPc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
Jeny	jen	k1gInPc1
<g/>
,	,	kIx,
Friedlandu	Friedland	k1gInSc2
<g/>
,	,	kIx,
Ulmu	Ulmus	k1gInSc2
<g/>
,	,	kIx,
Wagramu	Wagram	k1gInSc2
a	a	k8xC
Jílového	Jílové	k1gNnSc2
</s>
<s>
Vítězné	vítězný	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
na	na	k7c6
jihu	jih	k1gInSc6
(	(	kIx(
<g/>
Gérard	Gérard	k1gMnSc1
<g/>
)	)	kIx)
-	-	kIx~
scény	scéna	k1gFnSc2
z	z	k7c2
bitev	bitva	k1gFnPc2
u	u	k7c2
Marenga	marengo	k1gNnSc2
<g/>
,	,	kIx,
Rivoli	Rivole	k1gFnSc3
<g/>
,	,	kIx,
Arcole	Arcola	k1gFnSc3
a	a	k8xC
Lodi	loď	k1gFnSc3
</s>
<s>
Vítězné	vítězný	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
na	na	k7c6
východě	východ	k1gInSc6
(	(	kIx(
<g/>
Valcher	Valchra	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
scény	scéna	k1gFnSc2
z	z	k7c2
bitev	bitva	k1gFnPc2
u	u	k7c2
Alexandrie	Alexandrie	k1gFnSc2
<g/>
,	,	kIx,
Pyramid	pyramida	k1gFnPc2
<g/>
,	,	kIx,
Abukiru	Abukir	k1gInSc2
a	a	k8xC
Héliopolisu	Héliopolis	k1gInSc2
</s>
<s>
Vítězné	vítězný	k2eAgInPc4d1
odznaky	odznak	k1gInPc4
na	na	k7c6
západě	západ	k1gInSc6
(	(	kIx(
<g/>
Jean-Joseph	Jean-Joseph	k1gInSc1
Espercieux	Espercieux	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
scény	scéna	k1gFnSc2
z	z	k7c2
bitev	bitva	k1gFnPc2
u	u	k7c2
Jemmapes	Jemmapesa	k1gFnPc2
a	a	k8xC
Fleurus	Fleurus	k1gInSc1
</s>
<s>
Pod	pod	k7c7
obloukem	oblouk	k1gInSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
hrob	hrob	k1gInSc1
neznámého	známý	k2eNgMnSc2d1
vojína	vojín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
památník	památník	k1gInSc1
je	být	k5eAaImIp3nS
obehnán	obehnat	k5eAaPmNgInS
stovkou	stovka	k1gFnSc7
sloupků	sloupek	k1gInPc2
symbolizujících	symbolizující	k2eAgInPc2d1
Stodenní	stodenní	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Le	Le	k?
Départ	Départ	k1gInSc4
(	(	kIx(
<g/>
La	la	k1gNnSc4
Marseillaise	Marseillaisa	k1gFnSc3
<g/>
,	,	kIx,
Odjezd	odjezd	k1gInSc1
dobrovolníků	dobrovolník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1792	#num#	k4
</s>
<s>
Le	Le	k?
Triomphe	Triomphe	k1gFnSc1
(	(	kIx(
<g/>
Vítězství	vítězství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1810	#num#	k4
</s>
<s>
La	la	k1gNnSc1
Résistance	Résistanec	k1gInSc2
(	(	kIx(
<g/>
Odpor	odpor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1814	#num#	k4
</s>
<s>
La	la	k1gNnSc1
Paix	Paix	k1gInSc1
(	(	kIx(
<g/>
Mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1815	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Průvodce	průvodce	k1gMnSc1
pokladnicemi	pokladnice	k1gFnPc7
UNESCO	UNESCO	kA
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Vladimír	Vladimír	k1gMnSc1
Tkáč	tkáč	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
80	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
4718	#num#	k4
<g/>
-X	-X	k?
<g/>
:	:	kIx,
Grafis	Grafis	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
274	#num#	k4
s.	s.	k?
S.	S.	kA
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Arc	Arc	k?
de	de	k?
Triomphe	Triomphe	k1gInSc1
du	du	k?
Carrousel	Carrousel	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vítězný	vítězný	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Záznam	záznam	k1gInSc1
v	v	k7c6
evidenci	evidence	k1gFnSc6
historických	historický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Oblouk	oblouk	k1gInSc1
na	na	k7c4
Structurae	Structurae	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4605782-1	4605782-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
93005686	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
136504168	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
93005686	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
