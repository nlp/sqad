<s>
Olovo	olovo	k1gNnSc4	olovo
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
Pb	Pb	k1gFnSc1	Pb
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Plumbum	Plumbum	k1gInSc1	Plumbum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgInSc1d1	těžký
toxický	toxický	k2eAgInSc1d1	toxický
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
