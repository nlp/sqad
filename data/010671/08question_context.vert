<s>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
africana	african	k1gMnSc2	african
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
chobotnatců	chobotnatec	k1gMnPc2	chobotnatec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
největší	veliký	k2eAgMnSc1d3	veliký
suchozemský	suchozemský	k2eAgMnSc1d1	suchozemský
savec	savec	k1gMnSc1	savec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
až	až	k9	až
4	[number]	k4	4
m.	m.	k?	m.
</s>
