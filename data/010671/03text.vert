<p>
<s>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
africana	african	k1gMnSc2	african
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
chobotnatců	chobotnatec	k1gMnPc2	chobotnatec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
největší	veliký	k2eAgMnSc1d3	veliký
suchozemský	suchozemský	k2eAgMnSc1d1	suchozemský
savec	savec	k1gMnSc1	savec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
až	až	k9	až
4	[number]	k4	4
m.	m.	k?	m.
</s>
<s>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
roztroušeně	roztroušeně	k6eAd1	roztroušeně
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Africe	Afrika	k1gFnSc6	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
hojnějším	hojný	k2eAgInSc6d2	hojnější
počtu	počet	k1gInSc6	počet
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
poměrně	poměrně	k6eAd1	poměrně
rozmanité	rozmanitý	k2eAgNnSc4d1	rozmanité
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Slony	slon	k1gMnPc4	slon
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
savanách	savana	k1gFnPc6	savana
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
až	až	k9	až
do	do	k7c2	do
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
do	do	k7c2	do
5000	[number]	k4	5000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Sloni	sloň	k1gFnSc2wR	sloň
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
přizpůsobiví	přizpůsobivý	k2eAgMnPc1d1	přizpůsobivý
-	-	kIx~	-
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
jen	jen	k9	jen
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc1	voda
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc4	rozměr
==	==	k?	==
</s>
</p>
<p>
<s>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
největším	veliký	k2eAgMnSc7d3	veliký
žijícím	žijící	k2eAgMnSc7d1	žijící
suchozemským	suchozemský	k2eAgMnSc7d1	suchozemský
živočichem	živočich	k1gMnSc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tohoto	tento	k3xDgMnSc4	tento
chobotnatce	chobotnatec	k1gMnSc4	chobotnatec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
kolem	kolem	k7c2	kolem
6000	[number]	k4	6000
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
,	,	kIx,	,
rekordní	rekordní	k2eAgMnSc1d1	rekordní
změřený	změřený	k2eAgMnSc1d1	změřený
jedinec	jedinec	k1gMnSc1	jedinec
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
výšky	výška	k1gFnSc2	výška
3,96	[number]	k4	3,96
metru	metr	k1gInSc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
10	[number]	k4	10
400	[number]	k4	400
kg	kg	kA	kg
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
odhad	odhad	k1gInSc1	odhad
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
12	[number]	k4	12
250	[number]	k4	250
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
m	m	kA	m
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
m	m	kA	m
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
6,5	[number]	k4	6,5
<g/>
-	-	kIx~	-
<g/>
8,5	[number]	k4	8,5
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
6-7	[number]	k4	6-7
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc2	samice
4	[number]	k4	4
tuny	tuna	k1gFnSc2	tuna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
běhu	běh	k1gInSc2	běh
<g/>
:	:	kIx,	:
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
65-90	[number]	k4	65-90
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pružnou	pružný	k2eAgFnSc4d1	pružná
kůži	kůže	k1gFnSc4	kůže
šedivého	šedivý	k2eAgNnSc2d1	šedivé
až	až	k8xS	až
šedohnědého	šedohnědý	k2eAgNnSc2d1	šedohnědé
zbarvení	zbarvení	k1gNnSc2	zbarvení
bez	bez	k7c2	bez
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Chlupy	chlup	k1gInPc4	chlup
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
hmatovými	hmatový	k2eAgInPc7d1	hmatový
chloupky	chloupek	k1gInPc7	chloupek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
koncentrované	koncentrovaný	k2eAgFnPc1d1	koncentrovaná
na	na	k7c6	na
chobotu	chobot	k1gInSc6	chobot
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
znaky	znak	k1gInPc4	znak
patří	patřit	k5eAaImIp3nP	patřit
dva	dva	k4xCgInPc4	dva
citlivé	citlivý	k2eAgInPc4d1	citlivý
"	"	kIx"	"
<g/>
prstíky	prstík	k1gInPc4	prstík
<g/>
"	"	kIx"	"
na	na	k7c6	na
konci	konec	k1gInSc6	konec
citlivého	citlivý	k2eAgInSc2d1	citlivý
chobotu	chobot	k1gInSc2	chobot
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
<g/>
,	,	kIx,	,
čichání	čichání	k1gNnSc3	čichání
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc3	pití
<g/>
,	,	kIx,	,
sprchování	sprchování	k1gNnSc3	sprchování
a	a	k8xC	a
podávání	podávání	k1gNnSc3	podávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
obrovské	obrovský	k2eAgInPc1d1	obrovský
silně	silně	k6eAd1	silně
prokrvené	prokrvený	k2eAgInPc4d1	prokrvený
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
velké	velký	k2eAgFnSc6d1	velká
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
mnoho	mnoho	k6eAd1	mnoho
přebytečného	přebytečný	k2eAgNnSc2d1	přebytečné
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
tak	tak	k6eAd1	tak
přehřátí	přehřátí	k1gNnPc4	přehřátí
během	během	k7c2	během
horkých	horký	k2eAgInPc2d1	horký
afrických	africký	k2eAgInPc2d1	africký
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoké	vysoký	k2eAgFnPc4d1	vysoká
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
štíhlou	štíhlý	k2eAgFnSc4d1	štíhlá
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
čelo	čelo	k1gNnSc1	čelo
ubíhající	ubíhající	k2eAgNnSc1d1	ubíhající
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
prohnutý	prohnutý	k2eAgInSc4d1	prohnutý
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
kly	kel	k1gInPc4	kel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
rostou	růst	k5eAaImIp3nP	růst
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
byly	být	k5eAaImAgInP	být
u	u	k7c2	u
slona	slon	k1gMnSc2	slon
zjištěny	zjistit	k5eAaPmNgInP	zjistit
<g/>
,	,	kIx,	,
měřily	měřit	k5eAaImAgInP	měřit
přes	přes	k7c4	přes
3	[number]	k4	3
metry	metr	k1gInPc4	metr
a	a	k8xC	a
vážily	vážit	k5eAaImAgFnP	vážit
přes	přes	k7c4	přes
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sloni	slon	k1gMnPc1	slon
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
jemný	jemný	k2eAgInSc4d1	jemný
čich	čich	k1gInSc4	čich
a	a	k8xC	a
dobrý	dobrý	k2eAgInSc4d1	dobrý
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
i	i	k9	i
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
neslyšitelné	slyšitelný	k2eNgInPc1d1	neslyšitelný
infrazvuky	infrazvuk	k1gInPc1	infrazvuk
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
sluchem	sluch	k1gInSc7	sluch
vnímají	vnímat	k5eAaImIp3nP	vnímat
i	i	k9	i
otřesy	otřes	k1gInPc1	otřes
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
například	například	k6eAd1	například
pohybem	pohyb	k1gInSc7	pohyb
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
změny	změna	k1gFnPc1	změna
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
schopná	schopný	k2eAgNnPc1d1	schopné
běhat	běhat	k5eAaImF	běhat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
spíše	spíše	k9	spíše
chůzi	chůze	k1gFnSc4	chůze
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
rychlou	rychlý	k2eAgFnSc4d1	rychlá
jako	jako	k8xC	jako
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zem	zem	k1gFnSc4	zem
totiž	totiž	k9	totiž
při	při	k7c6	při
pomalejší	pomalý	k2eAgFnSc6d2	pomalejší
chůzi	chůze	k1gFnSc6	chůze
našlapují	našlapovat	k5eAaImIp3nP	našlapovat
měkce	měkko	k6eAd1	měkko
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neslyšně	slyšně	k6eNd1	slyšně
<g/>
.	.	kIx.	.
</s>
<s>
Došlapují	došlapovat	k5eAaImIp3nP	došlapovat
jen	jen	k9	jen
na	na	k7c4	na
konečky	koneček	k1gInPc4	koneček
prstů	prst	k1gInPc2	prst
srostlé	srostlý	k2eAgFnSc2d1	srostlá
rosolovitým	rosolovitý	k2eAgNnSc7d1	rosolovité
vazivem	vazivo	k1gNnSc7	vazivo
do	do	k7c2	do
polštáře	polštář	k1gInSc2	polštář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
celé	celý	k2eAgNnSc1d1	celé
"	"	kIx"	"
<g/>
chodidlo	chodidlo	k1gNnSc1	chodidlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tlumič	tlumič	k1gInSc1	tlumič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Afričtí	africký	k2eAgMnPc1d1	africký
sloni	slon	k1gMnPc1	slon
jsou	být	k5eAaImIp3nP	být
společenská	společenský	k2eAgNnPc4d1	společenské
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
pohromadě	pohromadě	k6eAd1	pohromadě
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
organizovaných	organizovaný	k2eAgFnPc6d1	organizovaná
skupinách	skupina	k1gFnPc6	skupina
tvořených	tvořený	k2eAgFnPc6d1	tvořená
deseti	deset	k4xCc7	deset
až	až	k9	až
dvanácti	dvanáct	k4xCc7	dvanáct
samicemi	samice	k1gFnPc7	samice
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc7	jejich
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
vede	vést	k5eAaImIp3nS	vést
dominantní	dominantní	k2eAgFnSc1d1	dominantní
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
spřízněná	spřízněný	k2eAgFnSc1d1	spřízněná
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
členy	člen	k1gInPc7	člen
stáda	stádo	k1gNnSc2	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rodinné	rodinný	k2eAgFnPc1d1	rodinná
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
dočasně	dočasně	k6eAd1	dočasně
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
do	do	k7c2	do
stád	stádo	k1gNnPc2	stádo
o	o	k7c6	o
několika	několik	k4yIc6	několik
stech	sto	k4xCgNnPc6	sto
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
žijí	žít	k5eAaImIp3nP	žít
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
mládeneckých	mládenecký	k2eAgFnPc6d1	mládenecká
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnPc1d2	starší
jedinci	jedinec	k1gMnPc1	jedinec
pak	pak	k6eAd1	pak
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
Dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
přicházejí	přicházet	k5eAaImIp3nP	přicházet
samci	samec	k1gMnPc1	samec
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
do	do	k7c2	do
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
a	a	k8xC	a
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
spáření	spáření	k1gNnSc2	spáření
se	s	k7c7	s
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
velmi	velmi	k6eAd1	velmi
vyvinuté	vyvinutý	k2eAgNnSc4d1	vyvinuté
sociální	sociální	k2eAgNnSc4d1	sociální
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
stáda	stádo	k1gNnSc2	stádo
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
sloni	slon	k1gMnPc1	slon
truchlí	truchlet	k5eAaImIp3nP	truchlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stádo	stádo	k1gNnSc1	stádo
slonů	slon	k1gMnPc2	slon
dovede	dovést	k5eAaPmIp3nS	dovést
překonávat	překonávat	k5eAaImF	překonávat
značné	značný	k2eAgFnPc4d1	značná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každý	každý	k3xTgMnSc1	každý
slon	slon	k1gMnSc1	slon
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
denně	denně	k6eAd1	denně
pít	pít	k5eAaImF	pít
až	až	k9	až
130-190	[number]	k4	130-190
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
koupe	koupat	k5eAaImIp3nS	koupat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
dost	dost	k6eAd1	dost
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
ponoří	ponořit	k5eAaPmIp3nS	ponořit
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
a	a	k8xC	a
dýchá	dýchat	k5eAaImIp3nS	dýchat
jen	jen	k9	jen
chobotem	chobot	k1gInSc7	chobot
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Šnorchl	šnorchl	k1gInSc4	šnorchl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
koupání	koupání	k1gNnSc6	koupání
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
chobotem	chobot	k1gInSc7	chobot
nastříká	nastříkat	k5eAaPmIp3nS	nastříkat
bahno	bahno	k1gNnSc4	bahno
z	z	k7c2	z
břehu	břeh	k1gInSc2	břeh
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
se	se	k3xPyFc4	se
tak	tak	k9	tak
nejen	nejen	k6eAd1	nejen
před	před	k7c7	před
obtížným	obtížný	k2eAgInSc7d1	obtížný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
proti	proti	k7c3	proti
přehřátí	přehřátí	k1gNnSc3	přehřátí
organismu	organismus	k1gInSc2	organismus
v	v	k7c6	v
horkých	horký	k2eAgInPc6d1	horký
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
===	===	k?	===
</s>
</p>
<p>
<s>
Sloni	slon	k1gMnPc1	slon
se	se	k3xPyFc4	se
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dorozumívat	dorozumívat	k5eAaImF	dorozumívat
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
tak	tak	k6eAd1	tak
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
členové	člen	k1gMnPc1	člen
stáda	stádo	k1gNnSc2	stádo
navzájem	navzájem	k6eAd1	navzájem
nevidí	vidět	k5eNaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Vydávají	vydávat	k5eAaImIp3nP	vydávat
hluboký	hluboký	k2eAgInSc4d1	hluboký
bručící	bručící	k2eAgInSc4d1	bručící
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
kručení	kručení	k1gNnSc4	kručení
v	v	k7c6	v
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
slon	slon	k1gMnSc1	slon
vydává	vydávat	k5eAaPmIp3nS	vydávat
kloktáním	kloktání	k1gNnSc7	kloktání
v	v	k7c6	v
chobotu	chobot	k1gInSc6	chobot
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
slon	slon	k1gMnSc1	slon
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
,	,	kIx,	,
mručí	mručet	k5eAaImIp3nS	mručet
tlumeným	tlumený	k2eAgInSc7d1	tlumený
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
vyleká	vylekat	k5eAaPmIp3nS	vylekat
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
zatroubí	zatroubit	k5eAaPmIp3nS	zatroubit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
samci	samec	k1gMnPc1	samec
se	se	k3xPyFc4	se
při	při	k7c6	při
bojových	bojový	k2eAgFnPc6d1	bojová
hrách	hra	k1gFnPc6	hra
staví	stavit	k5eAaBmIp3nP	stavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
a	a	k8xC	a
zamávají	zamávat	k5eAaPmIp3nP	zamávat
choboty	chobot	k1gInPc1	chobot
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
rozeběhnou	rozeběhnout	k5eAaPmIp3nP	rozeběhnout
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
choboty	chobot	k1gInPc7	chobot
přetlačovat	přetlačovat	k5eAaImF	přetlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
jednomu	jeden	k4xCgNnSc3	jeden
nepodaří	podařit	k5eNaPmIp3nS	podařit
druhého	druhý	k4xOgMnSc4	druhý
přetlačit	přetlačit	k5eAaPmF	přetlačit
<g/>
,	,	kIx,	,
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
souboj	souboj	k1gInSc4	souboj
začnou	začít	k5eAaPmIp3nP	začít
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgMnSc1d1	dominantní
jedinec	jedinec	k1gMnSc1	jedinec
si	se	k3xPyFc3	se
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
zjednává	zjednávat	k5eAaImIp3nS	zjednávat
pořádek	pořádek	k1gInSc4	pořádek
jen	jen	k9	jen
zatočením	zatočení	k1gNnSc7	zatočení
chobotu	chobot	k1gInSc2	chobot
nebo	nebo	k8xC	nebo
zvířením	zvíření	k1gNnSc7	zvíření
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
slyšet	slyšet	k5eAaImF	slyšet
i	i	k8xC	i
sloní	sloní	k2eAgNnSc4d1	sloní
troubení	troubení	k1gNnSc4	troubení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Sloni	slon	k1gMnPc1	slon
jsou	být	k5eAaImIp3nP	být
býložravá	býložravý	k2eAgNnPc4d1	býložravé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
jídelníček	jídelníček	k1gInSc1	jídelníček
závisí	záviset	k5eAaImIp3nS	záviset
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
jedinec	jedinec	k1gMnSc1	jedinec
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ale	ale	k9	ale
musí	muset	k5eAaImIp3nP	muset
denně	denně	k6eAd1	denně
spořádat	spořádat	k5eAaPmF	spořádat
až	až	k9	až
225	[number]	k4	225
kg	kg	kA	kg
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
potravy	potrava	k1gFnSc2	potrava
musí	muset	k5eAaImIp3nS	muset
požírat	požírat	k5eAaImF	požírat
15	[number]	k4	15
až	až	k9	až
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jim	on	k3xPp3gFnPc3	on
zbudou	zbýt	k5eAaPmIp3nP	zbýt
jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
denně	denně	k6eAd1	denně
na	na	k7c4	na
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
navíc	navíc	k6eAd1	navíc
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
dobře	dobře	k6eAd1	dobře
zpracovávaná	zpracovávaný	k2eAgFnSc1d1	zpracovávaná
a	a	k8xC	a
zužitkuje	zužitkovat	k5eAaPmIp3nS	zužitkovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
z	z	k7c2	z
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
slon	slon	k1gMnSc1	slon
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
denně	denně	k6eAd1	denně
až	až	k9	až
180	[number]	k4	180
kg	kg	kA	kg
trusu	trus	k1gInSc2	trus
a	a	k8xC	a
40-60	[number]	k4	40-60
litrů	litr	k1gInPc2	litr
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
slon	slon	k1gMnSc1	slon
podává	podávat	k5eAaImIp3nS	podávat
chobotem	chobot	k1gInSc7	chobot
až	až	k6eAd1	až
do	do	k7c2	do
jícnu	jícen	k1gInSc2	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
málo	málo	k4c1	málo
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
stoličky	stolička	k1gFnPc1	stolička
důležité	důležitý	k2eAgFnPc1d1	důležitá
pro	pro	k7c4	pro
drcení	drcení	k1gNnSc4	drcení
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
jen	jen	k9	jen
rozžvýkat	rozžvýkat	k5eAaPmF	rozžvýkat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
slon	slon	k1gMnSc1	slon
tak	tak	k6eAd1	tak
malou	malý	k2eAgFnSc4d1	malá
účinnost	účinnost	k1gFnSc4	účinnost
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ztratí	ztratit	k5eAaPmIp3nS	ztratit
všechny	všechen	k3xTgInPc4	všechen
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
už	už	k6eAd1	už
v	v	k7c6	v
tlamě	tlama	k1gFnSc6	tlama
moc	moc	k6eAd1	moc
možností	možnost	k1gFnPc2	možnost
potravu	potrava	k1gFnSc4	potrava
rozžvýkat	rozžvýkat	k5eAaPmF	rozžvýkat
a	a	k8xC	a
slon	slon	k1gMnSc1	slon
umírá	umírat	k5eAaImIp3nS	umírat
hlady	hlady	k6eAd1	hlady
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žaludek	žaludek	k1gInSc4	žaludek
nerozžvýkanou	rozžvýkaný	k2eNgFnSc4d1	rozžvýkaný
potravu	potrava	k1gFnSc4	potrava
nerozloží	rozložit	k5eNaPmIp3nS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
70	[number]	k4	70
letech	léto	k1gNnPc6	léto
sloního	sloní	k2eAgInSc2d1	sloní
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
říjné	říjný	k2eAgInPc1d1	říjný
2-6	[number]	k4	2-6
dní	den	k1gInPc2	den
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nP	rodit
zpravidla	zpravidla	k6eAd1	zpravidla
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
po	po	k7c6	po
22	[number]	k4	22
měsíční	měsíční	k2eAgFnSc6d1	měsíční
březosti	březost	k1gFnSc6	březost
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porodu	porod	k1gInSc6	porod
samici	samice	k1gFnSc4	samice
hlídají	hlídat	k5eAaImIp3nP	hlídat
její	její	k3xOp3gInSc4	její
společnice	společnice	k1gFnPc1	společnice
ze	z	k7c2	z
stáda	stádo	k1gNnSc2	stádo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ochránily	ochránit	k5eAaPmAgFnP	ochránit
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Slůně	slůně	k1gNnSc1	slůně
měří	měřit	k5eAaImIp3nS	měřit
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
jen	jen	k9	jen
85	[number]	k4	85
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
110	[number]	k4	110
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
své	svůj	k3xOyFgNnSc4	svůj
mládě	mládě	k1gNnSc4	mládě
kojí	kojit	k5eAaImIp3nS	kojit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
slůně	slůně	k1gNnSc4	slůně
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
již	již	k9	již
dalšího	další	k2eAgMnSc4d1	další
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stará	starat	k5eAaImIp3nS	starat
naráz	naráz	k6eAd1	naráz
i	i	k9	i
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
různě	různě	k6eAd1	různě
stará	starý	k2eAgNnPc4d1	staré
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
dospívají	dospívat	k5eAaImIp3nP	dospívat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
až	až	k9	až
za	za	k7c4	za
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
mládě	mládě	k1gNnSc1	mládě
napadeno	napadnout	k5eAaPmNgNnS	napadnout
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
jej	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
zuřivě	zuřivě	k6eAd1	zuřivě
brání	bránit	k5eAaImIp3nP	bránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
druhu	druh	k1gInSc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
na	na	k7c4	na
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
zmenšujícímu	zmenšující	k2eAgInSc3d1	zmenšující
biotopu	biotop	k1gInSc3	biotop
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgFnSc3d1	zvyšující
lidské	lidský	k2eAgFnSc3d1	lidská
populaci	populace	k1gFnSc3	populace
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
nižší	nízký	k2eAgInPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
také	také	k9	také
snižují	snižovat	k5eAaImIp3nP	snižovat
pytláci	pytlák	k1gMnPc1	pytlák
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
se	se	k3xPyFc4	se
málokdy	málokdy	k6eAd1	málokdy
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
sloni	slon	k1gMnPc1	slon
afričtí	africký	k2eAgMnPc1d1	africký
chráněni	chránit	k5eAaImNgMnP	chránit
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
nezákonný	zákonný	k2eNgInSc1d1	nezákonný
lov	lov	k1gInSc1	lov
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
trestat	trestat	k5eAaImF	trestat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pytlák	pytlák	k1gMnSc1	pytlák
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgMnSc1	tento
slon	slon	k1gMnSc1	slon
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
stavy	stav	k1gInPc4	stav
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
lokalitách	lokalita	k1gFnPc6	lokalita
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
přesný	přesný	k2eAgInSc1d1	přesný
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
20	[number]	k4	20
000	[number]	k4	000
km2	km2	k4	km2
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
mohou	moct	k5eAaImIp3nP	moct
decimovat	decimovat	k5eAaBmF	decimovat
okolní	okolní	k2eAgFnSc4d1	okolní
floru	flora	k1gFnSc4	flora
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
míst	místo	k1gNnPc2	místo
slon	slon	k1gMnSc1	slon
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ani	ani	k9	ani
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
populace	populace	k1gFnSc1	populace
slonů	slon	k1gMnPc2	slon
roste	růst	k5eAaImIp3nS	růst
nebo	nebo	k8xC	nebo
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jedna	jeden	k4xCgFnSc1	jeden
sloní	sloní	k2eAgFnSc1d1	sloní
generace	generace	k1gFnSc1	generace
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
a	a	k8xC	a
obsáhlejší	obsáhlý	k2eAgFnSc1d2	obsáhlejší
analýza	analýza	k1gFnSc1	analýza
by	by	kYmCp3nS	by
trvala	trvat	k5eAaImAgFnS	trvat
také	také	k9	také
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
oblastí	oblast	k1gFnPc2	oblast
počet	počet	k1gInSc1	počet
slonů	slon	k1gMnPc2	slon
oproti	oproti	k7c3	oproti
předchozí	předchozí	k2eAgFnSc3d1	předchozí
generaci	generace	k1gFnSc3	generace
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
chová	chovat	k5eAaImIp3nS	chovat
slony	slon	k1gMnPc4	slon
africké	africký	k2eAgFnSc2d1	africká
ZOO	zoo	k1gFnSc2	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
ZOO	zoo	k1gFnSc2	zoo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byli	být	k5eAaImAgMnP	být
chováni	chován	k2eAgMnPc1d1	chován
i	i	k9	i
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
IMP	IMP	kA	IMP
BV	BV	kA	BV
<g/>
/	/	kIx~	/
<g/>
International	International	k1gFnSc1	International
Masters	Masters	k1gInSc1	Masters
Publishers	Publishers	k1gInSc1	Publishers
-	-	kIx~	-
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
savci-karta	savciarta	k1gFnSc1	savci-karta
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
slonů	slon	k1gMnPc2	slon
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	african	k1gMnSc2	african
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Zoo	zoo	k1gFnPc2	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
slonu	slon	k1gMnSc6	slon
africkém	africký	k2eAgMnSc6d1	africký
</s>
</p>
<p>
<s>
IUCN	IUCN	kA	IUCN
-	-	kIx~	-
červená	červený	k2eAgFnSc1d1	červená
kniha	kniha	k1gFnSc1	kniha
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
Loxodonta	Loxodonta	k1gFnSc1	Loxodonta
africana	africana	k1gFnSc1	africana
<g/>
)	)	kIx)	)
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
SOCHA	Socha	k1gMnSc1	Socha
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
velký	velký	k2eAgMnSc1d1	velký
byl	být	k5eAaImAgMnS	být
největší	veliký	k2eAgMnSc1d3	veliký
známý	známý	k2eAgMnSc1d1	známý
slon	slon	k1gMnSc1	slon
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
OSEL	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
o	o	k7c6	o
největších	veliký	k2eAgMnPc6d3	veliký
slonech	slon	k1gMnPc6	slon
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Meteor	meteor	k1gInSc1	meteor
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
36	[number]	k4	36
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
