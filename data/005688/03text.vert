<s>
Magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
označující	označující	k2eAgFnSc1d1	označující
směs	směs	k1gFnSc1	směs
roztavených	roztavený	k2eAgFnPc2d1	roztavená
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
terestrických	terestrický	k2eAgNnPc2d1	terestrické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
roztavených	roztavený	k2eAgFnPc2d1	roztavená
hornin	hornina	k1gFnPc2	hornina
může	moct	k5eAaImIp3nS	moct
magma	magma	k1gNnSc1	magma
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
krystaly	krystal	k1gInPc1	krystal
různých	různý	k2eAgInPc2d1	různý
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
rozpuštěné	rozpuštěný	k2eAgInPc1d1	rozpuštěný
plyny	plyn	k1gInPc1	plyn
či	či	k8xC	či
bublinky	bublinka	k1gFnPc1	bublinka
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
hlubinný	hlubinný	k2eAgInSc4d1	hlubinný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
magma	magma	k1gNnSc1	magma
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
označení	označení	k1gNnSc4	označení
termín	termín	k1gInSc4	termín
láva	láva	k1gFnSc1	láva
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
zemském	zemský	k2eAgInSc6d1	zemský
plášti	plášť	k1gInSc6	plášť
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
primární	primární	k2eAgNnSc1d1	primární
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgMnSc1d1	sekundární
(	(	kIx(	(
<g/>
žulové	žulový	k2eAgNnSc1d1	žulové
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
horotvornou	horotvorný	k2eAgFnSc7d1	horotvorná
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgNnSc1d1	primární
magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
řidší	řídký	k2eAgFnSc1d2	řidší
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
proniká	pronikat	k5eAaImIp3nS	pronikat
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
výlevné	výlevný	k2eAgFnSc2d1	výlevná
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čedič	čedič	k1gInSc1	čedič
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgMnSc1d1	sekundární
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
a	a	k8xC	a
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
pomalu	pomalu	k6eAd1	pomalu
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
velká	velký	k2eAgNnPc1d1	velké
tělesa	těleso	k1gNnPc1	těleso
(	(	kIx(	(
<g/>
plutony	pluton	k1gInPc1	pluton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
staršími	starý	k2eAgFnPc7d2	starší
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
utuhnutí	utuhnutí	k1gNnSc2	utuhnutí
magmatu	magma	k1gNnSc2	magma
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
horniny	hornina	k1gFnSc2	hornina
na	na	k7c6	na
hlubinné	hlubinný	k2eAgFnSc6d1	hlubinná
<g/>
,	,	kIx,	,
výlevné	výlevný	k2eAgFnSc6d1	výlevná
a	a	k8xC	a
žilné	žilný	k2eAgFnSc6d1	žilná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
magma	magma	k1gNnSc1	magma
nejčastěji	často	k6eAd3	často
křemičitá	křemičitý	k2eAgFnSc1d1	křemičitá
hornina	hornina	k1gFnSc1	hornina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
fluidních	fluidní	k2eAgFnPc2d1	fluidní
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
aluminio-silikátová	aluminioilikátový	k2eAgFnSc1d1	aluminio-silikátový
tavenina	tavenina	k1gFnSc1	tavenina
obsahující	obsahující	k2eAgInPc1d1	obsahující
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
a	a	k8xC	a
fluor	fluor	k1gInSc1	fluor
<g/>
)	)	kIx)	)
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
nebo	nebo	k8xC	nebo
tavením	tavení	k1gNnSc7	tavení
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
spodní	spodní	k2eAgFnSc2d1	spodní
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
různých	různý	k2eAgFnPc2d1	různá
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
většinou	většina	k1gFnSc7	většina
mezi	mezi	k7c7	mezi
650	[number]	k4	650
<g/>
–	–	k?	–
<g/>
1200	[number]	k4	1200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
rozdílu	rozdíl	k1gInSc2	rozdíl
hustoty	hustota	k1gFnSc2	hustota
magmatu	magma	k1gNnSc2	magma
vůči	vůči	k7c3	vůči
okolnímu	okolní	k2eAgNnSc3d1	okolní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
magma	magma	k1gNnSc4	magma
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
prochází	procházet	k5eAaImIp3nS	procházet
skrze	skrze	k?	skrze
kůru	kůra	k1gFnSc4	kůra
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
utváří	utvářit	k5eAaPmIp3nP	utvářit
magmatické	magmatický	k2eAgInPc1d1	magmatický
krby	krb	k1gInPc1	krb
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
hromadění	hromadění	k1gNnPc1	hromadění
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
pak	pak	k6eAd1	pak
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
skrze	skrze	k?	skrze
žíly	žíla	k1gFnSc2	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průniku	průnik	k1gInSc6	průnik
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sopečné	sopečný	k2eAgFnSc3d1	sopečná
činnosti	činnost	k1gFnSc3	činnost
mající	mající	k2eAgFnSc1d1	mající
často	často	k6eAd1	často
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstupu	výstup	k1gInSc2	výstup
dochází	docházet	k5eAaImIp3nS	docházet
taktéž	taktéž	k?	taktéž
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
chladnutí	chladnutí	k1gNnSc3	chladnutí
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
postupné	postupný	k2eAgFnSc3d1	postupná
krystalizaci	krystalizace	k1gFnSc3	krystalizace
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
okolního	okolní	k2eAgInSc2d1	okolní
litostatického	litostatický	k2eAgInSc2d1	litostatický
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
postupné	postupný	k2eAgNnSc1d1	postupné
uvolňování	uvolňování	k1gNnSc1	uvolňování
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
magmatu	magma	k1gNnSc6	magma
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
bublinek	bublinka	k1gFnPc2	bublinka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
plynů	plyn	k1gInPc2	plyn
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
<g/>
,	,	kIx,	,
či	či	k8xC	či
magma	magma	k1gNnSc1	magma
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
potká	potkat	k5eAaPmIp3nS	potkat
zdroj	zdroj	k1gInSc4	zdroj
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
fragmentaci	fragmentace	k1gFnSc3	fragmentace
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
explozivního	explozivní	k2eAgInSc2d1	explozivní
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ale	ale	k8xC	ale
množství	množství	k1gNnSc6	množství
plynů	plyn	k1gInPc2	plyn
malé	malý	k2eAgFnSc3d1	malá
či	či	k8xC	či
plyn	plyn	k1gInSc1	plyn
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
snadno	snadno	k6eAd1	snadno
z	z	k7c2	z
magmatu	magma	k1gNnSc2	magma
unikat	unikat	k5eAaImF	unikat
<g/>
,	,	kIx,	,
magma	magma	k1gNnSc1	magma
se	se	k3xPyFc4	se
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
začne	začít	k5eAaPmIp3nS	začít
vylévat	vylévat	k5eAaImF	vylévat
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
lávové	lávový	k2eAgInPc4d1	lávový
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zemském	zemský	k2eAgInSc6d1	zemský
plášti	plášť	k1gInSc6	plášť
tavením	tavení	k1gNnSc7	tavení
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
(	(	kIx(	(
<g/>
subdukční	subdukční	k2eAgFnSc1d1	subdukční
zóna	zóna	k1gFnSc1	zóna
hlubokomořských	hlubokomořský	k2eAgInPc2d1	hlubokomořský
příkopů	příkop	k1gInPc2	příkop
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
riftové	riftové	k2eAgNnSc2d1	riftové
údolí	údolí	k1gNnSc2	údolí
středooceánských	středooceánský	k2eAgNnPc2d1	středooceánský
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
složení	složení	k1gNnSc4	složení
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Tavení	tavení	k1gNnSc1	tavení
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
způsobené	způsobený	k2eAgFnPc4d1	způsobená
vlivem	vliv	k1gInSc7	vliv
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
magma	magma	k1gNnSc1	magma
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
přídavkem	přídavek	k1gInSc7	přídavek
fluid	fluid	k1gInSc1	fluid
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
)	)	kIx)	)
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
zahřátým	zahřátý	k2eAgFnPc3d1	zahřátá
horninám	hornina	k1gFnPc3	hornina
-	-	kIx~	-
častý	častý	k2eAgInSc1d1	častý
jev	jev	k1gInSc1	jev
v	v	k7c6	v
subdukčních	subdukční	k2eAgFnPc6d1	subdukční
zónách	zóna	k1gFnPc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
snížení	snížení	k1gNnSc4	snížení
teploty	teplota	k1gFnSc2	teplota
tavení	tavení	k1gNnPc2	tavení
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
vznik	vznik	k1gInSc4	vznik
magmatu	magma	k1gNnSc3	magma
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k8xC	i
tavením	tavení	k1gNnSc7	tavení
okolních	okolní	k2eAgFnPc2d1	okolní
hornin	hornina	k1gFnPc2	hornina
vlivem	vliv	k1gInSc7	vliv
už	už	k6eAd1	už
existujícího	existující	k2eAgNnSc2d1	existující
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
vystupujícího	vystupující	k2eAgInSc2d1	vystupující
přes	přes	k7c4	přes
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
tavení	tavení	k1gNnSc2	tavení
těchto	tento	k3xDgFnPc2	tento
hornin	hornina	k1gFnPc2	hornina
nižší	nízký	k2eAgFnSc1d2	nižší
jak	jak	k8xS	jak
teplota	teplota	k1gFnSc1	teplota
vystupujícího	vystupující	k2eAgNnSc2d1	vystupující
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roztavení	roztavení	k1gNnSc6	roztavení
stoupá	stoupat	k5eAaImIp3nS	stoupat
magma	magma	k1gNnSc4	magma
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hustota	hustota	k1gFnSc1	hustota
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Následek	následek	k1gInSc1	následek
stoupání	stoupání	k1gNnSc1	stoupání
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
magmatických	magmatický	k2eAgFnPc2d1	magmatická
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
taveniny	tavenina	k1gFnSc2	tavenina
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
hodnot	hodnota	k1gFnPc2	hodnota
590	[number]	k4	590
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
1	[number]	k4	1
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
krystalizační	krystalizační	k2eAgFnSc1d1	krystalizační
teplota	teplota	k1gFnSc1	teplota
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
magma	magma	k1gNnSc1	magma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nižších	nízký	k2eAgFnPc2d2	nižší
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
obsahu	obsah	k1gInSc2	obsah
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
pohybovat	pohybovat	k5eAaImF	pohybovat
i	i	k9	i
okolo	okolo	k7c2	okolo
1	[number]	k4	1
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
suché	suchý	k2eAgNnSc1d1	suché
magma	magma	k1gNnSc1	magma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
magmatu	magma	k1gNnSc2	magma
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
obsahu	obsah	k1gInSc6	obsah
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
hodnot	hodnota	k1gFnPc2	hodnota
2,2	[number]	k4	2,2
až	až	k9	až
2,8	[number]	k4	2,8
g	g	kA	g
<g/>
·	·	k?	·
<g/>
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Viskozita	viskozita	k1gFnSc1	viskozita
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
volatilními	volatilní	k2eAgFnPc7d1	volatilní
složkami	složka	k1gFnPc7	složka
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Magma	magma	k1gNnSc1	magma
všeobecně	všeobecně	k6eAd1	všeobecně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k6eAd1	jen
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
a	a	k8xC	a
titan	titan	k1gInSc1	titan
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obsažené	obsažený	k2eAgInPc1d1	obsažený
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
nad	nad	k7c7	nad
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
oxid	oxid	k1gInSc4	oxid
křemičitý	křemičitý	k2eAgInSc4d1	křemičitý
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
parciálním	parciální	k2eAgNnSc6d1	parciální
tavení	tavení	k1gNnSc6	tavení
nejprve	nejprve	k6eAd1	nejprve
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
taveniny	tavenina	k1gFnSc2	tavenina
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
baryum	baryum	k1gNnSc1	baryum
<g/>
,	,	kIx,	,
cesium	cesium	k1gNnSc1	cesium
<g/>
,	,	kIx,	,
rubidium	rubidium	k1gNnSc1	rubidium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
přecházejí	přecházet	k5eAaImIp3nP	přecházet
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
<g/>
,	,	kIx,	,
neroztavené	roztavený	k2eNgFnSc6d1	roztavený
části	část	k1gFnSc6	část
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nekompatibilní	kompatibilní	k2eNgInPc1d1	nekompatibilní
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
zirkonium	zirkonium	k1gNnSc1	zirkonium
<g/>
,	,	kIx,	,
hafnium	hafnium	k1gNnSc1	hafnium
<g/>
,	,	kIx,	,
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
stupeň	stupeň	k1gInSc1	stupeň
parciálního	parciální	k2eAgNnSc2d1	parciální
tavení	tavení	k1gNnSc2	tavení
určuje	určovat	k5eAaImIp3nS	určovat
relativní	relativní	k2eAgNnSc1d1	relativní
obohacení	obohacení	k1gNnSc1	obohacení
kompatibilními	kompatibilní	k2eAgInPc7d1	kompatibilní
nebo	nebo	k8xC	nebo
nekompatibilními	kompatibilní	k2eNgInPc7d1	nekompatibilní
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
výslednou	výsledný	k2eAgFnSc4d1	výsledná
horninu	hornina	k1gFnSc4	hornina
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
ztuhnutím	ztuhnutí	k1gNnSc7	ztuhnutí
daného	daný	k2eAgNnSc2d1	dané
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
