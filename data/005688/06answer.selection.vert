<s>
Magma	magma	k1gNnSc1	magma
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
označující	označující	k2eAgFnSc1d1	označující
směs	směs	k1gFnSc1	směs
roztavených	roztavený	k2eAgFnPc2d1	roztavená
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
terestrických	terestrický	k2eAgNnPc2d1	terestrické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
