<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
je	být	k5eAaImIp3nS	být
secesní	secesní	k2eAgFnSc1d1	secesní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hybernské	hybernský	k2eAgFnSc6d1	Hybernská
ulici	ulice	k1gFnSc6	ulice
s	s	k7c7	s
čp.	čp.	k?	čp.
1001	[number]	k4	1001
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
rakouského	rakouský	k2eAgMnSc2d1	rakouský
architekta	architekt	k1gMnSc2	architekt
Friedricha	Friedrich	k1gMnSc2	Friedrich
Ohmanna	Ohmann	k1gMnSc2	Ohmann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
hotelu	hotel	k1gInSc2	hotel
působil	působit	k5eAaImAgInS	působit
populární	populární	k2eAgInSc1d1	populární
kabaret	kabaret	k1gInSc1	kabaret
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
či	či	k8xC	či
Městská	městský	k2eAgNnPc1d1	Městské
divadla	divadlo	k1gNnPc1	divadlo
pražská	pražský	k2eAgFnSc1d1	Pražská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
koupil	koupit	k5eAaPmAgMnS	koupit
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
Quido	Quido	k1gNnSc4	Quido
Bělský	Bělský	k2eAgInSc1d1	Bělský
staveniště	staveniště	k1gNnSc2	staveniště
se	s	k7c7	s
starými	starý	k2eAgFnPc7d1	stará
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc4d2	pozdější
stavební	stavební	k2eAgFnSc4d1	stavební
parcelu	parcela	k1gFnSc4	parcela
pro	pro	k7c4	pro
hotel	hotel	k1gInSc4	hotel
Central	Central	k1gFnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
stavby	stavba	k1gFnSc2	stavba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
rakouský	rakouský	k2eAgMnSc1d1	rakouský
architekt	architekt	k1gMnSc1	architekt
Friedrich	Friedrich	k1gMnSc1	Friedrich
Ohmann	Ohmann	k1gMnSc1	Ohmann
<g/>
.	.	kIx.	.
</s>
<s>
Ohmann	Ohmann	k1gMnSc1	Ohmann
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
za	za	k7c7	za
sebou	se	k3xPyFc7	se
návrh	návrh	k1gInSc4	návrh
první	první	k4xOgFnSc6	první
ryze	ryze	k6eAd1	ryze
secesní	secesní	k2eAgFnPc4d1	secesní
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zbouranou	zbouraný	k2eAgFnSc4d1	zbouraná
budovu	budova	k1gFnSc4	budova
České	český	k2eAgFnSc2d1	Česká
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
banky	banka	k1gFnSc2	banka
s	s	k7c7	s
kavárnou	kavárna	k1gFnSc7	kavárna
Corso	Corsa	k1gFnSc5	Corsa
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhou	druhý	k4xOgFnSc7	druhý
pražskou	pražský	k2eAgFnSc7d1	Pražská
secesní	secesní	k2eAgFnSc7d1	secesní
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Ohmannově	Ohmannův	k2eAgInSc6d1	Ohmannův
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgInS	přijmout
pozici	pozice	k1gFnSc4	pozice
architekta	architekt	k1gMnSc2	architekt
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
Hofburgu	Hofburg	k1gInSc2	Hofburg
<g/>
,	,	kIx,	,
dokončili	dokončit	k5eAaPmAgMnP	dokončit
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
hotelu	hotel	k1gInSc2	hotel
Central	Central	k1gFnSc2	Central
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
žáci	žák	k1gMnPc1	žák
z	z	k7c2	z
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
Alois	Alois	k1gMnSc1	Alois
Dryák	Dryák	k?	Dryák
a	a	k8xC	a
Bedřich	Bedřich	k1gMnSc1	Bedřich
Bendelmayer	Bendelmayer	k1gMnSc1	Bendelmayer
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1	stavební
povolení	povolení	k1gNnSc1	povolení
získal	získat	k5eAaPmAgInS	získat
projekt	projekt	k1gInSc4	projekt
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1901	[number]	k4	1901
se	se	k3xPyFc4	se
zahájil	zahájit	k5eAaPmAgInS	zahájit
provoz	provoz	k1gInSc1	provoz
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
ale	ale	k9	ale
dokončovaly	dokončovat	k5eAaImAgInP	dokončovat
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
záhy	záhy	k6eAd1	záhy
významným	významný	k2eAgNnSc7d1	významné
střediskem	středisko	k1gNnSc7	středisko
společenského	společenský	k2eAgNnSc2d1	společenské
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
hostina	hostina	k1gFnSc1	hostina
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
hotelu	hotel	k1gInSc2	hotel
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
francouzského	francouzský	k2eAgMnSc2d1	francouzský
sochaře	sochař	k1gMnSc2	sochař
Augusta	Augusta	k1gMnSc1	Augusta
Rodina	rodina	k1gFnSc1	rodina
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
hotelu	hotel	k1gInSc2	hotel
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
secesní	secesní	k2eAgInSc1d1	secesní
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1918-1921	[number]	k4	1918-1921
působil	působit	k5eAaImAgInS	působit
populární	populární	k2eAgInSc1d1	populární
kabaret	kabaret	k1gInSc1	kabaret
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
Jiřím	Jiří	k1gMnSc7	Jiří
Červeným	Červený	k1gMnSc7	Červený
a	a	k8xC	a
Eduardem	Eduard	k1gMnSc7	Eduard
Bassem	Bass	k1gMnSc7	Bass
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
kino	kino	k1gNnSc4	kino
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
hotel	hotel	k1gInSc4	hotel
Central	Central	k1gFnSc2	Central
koupila	koupit	k5eAaPmAgFnS	koupit
Nemocenská	mocenský	k2eNgFnSc1d1	mocenský
pokladna	pokladna	k1gFnSc1	pokladna
veřejných	veřejný	k2eAgMnPc2d1	veřejný
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
využívala	využívat	k5eAaImAgFnS	využívat
jej	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
kancelářskou	kancelářský	k2eAgFnSc4d1	kancelářská
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisely	souviset	k5eAaImAgFnP	souviset
také	také	k9	také
přestavby	přestavba	k1gFnPc1	přestavba
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
přistavěny	přistavěn	k2eAgFnPc1d1	přistavěna
prostory	prostora	k1gFnPc1	prostora
pro	pro	k7c4	pro
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Přistavěná	přistavěný	k2eAgFnSc1d1	přistavěná
kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
havarijnímu	havarijní	k2eAgInSc3d1	havarijní
stavu	stav	k1gInSc3	stav
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgFnPc4d3	veliký
přestavby	přestavba	k1gFnPc4	přestavba
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Petra	Petr	k1gMnSc2	Petr
Kropáčka	Kropáček	k1gMnSc2	Kropáček
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
k	k	k7c3	k
potřebám	potřeba	k1gFnPc3	potřeba
biografu	biograf	k1gInSc2	biograf
a	a	k8xC	a
Městského	městský	k2eAgNnSc2d1	Městské
komorního	komorní	k2eAgNnSc2d1	komorní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
proměnou	proměna	k1gFnSc7	proměna
tehdy	tehdy	k6eAd1	tehdy
prošel	projít	k5eAaPmAgInS	projít
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vlivem	vlivem	k7c2	vlivem
přestavby	přestavba	k1gFnSc2	přestavba
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
secesní	secesní	k2eAgInSc4d1	secesní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
získal	získat	k5eAaPmAgInS	získat
sál	sál	k1gInSc1	sál
kapacitu	kapacita	k1gFnSc4	kapacita
přes	přes	k7c4	přes
600	[number]	k4	600
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
zániku	zánik	k1gInSc2	zánik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
účinkovalo	účinkovat	k5eAaImAgNnS	účinkovat
v	v	k7c6	v
divadelním	divadelní	k2eAgInSc6d1	divadelní
sále	sál	k1gInSc6	sál
hotelu	hotel	k1gInSc6	hotel
Central	Central	k1gFnSc2	Central
Městské	městský	k2eAgNnSc1d1	Městské
komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
také	také	k9	také
jen	jen	k9	jen
Komorní	komorní	k2eAgNnSc4d1	komorní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
druhou	druhý	k4xOgFnSc7	druhý
scénou	scéna	k1gFnSc7	scéna
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgNnPc2d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
Komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
špatné	špatný	k2eAgFnSc2d1	špatná
technické	technický	k2eAgFnSc2d1	technická
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
nutné	nutný	k2eAgFnSc2d1	nutná
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
Komorního	komorní	k2eAgNnSc2d1	komorní
divadla	divadlo	k1gNnSc2	divadlo
ale	ale	k8xC	ale
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
byli	být	k5eAaImAgMnP	být
spojováni	spojován	k2eAgMnPc1d1	spojován
lidé	člověk	k1gMnPc1	člověk
nepohodlní	pohodlnět	k5eNaImIp3nP	pohodlnět
pro	pro	k7c4	pro
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
politický	politický	k2eAgInSc4d1	politický
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
objekt	objekt	k1gInSc4	objekt
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgInS	přejít
hotel	hotel	k1gInSc1	hotel
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
nadace	nadace	k1gFnSc2	nadace
Pro	pro	k7c4	pro
Thalia	thalium	k1gNnPc4	thalium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
snažila	snažit	k5eAaImAgFnS	snažit
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
a	a	k8xC	a
opět	opět	k6eAd1	opět
zaktivizovat	zaktivizovat	k5eAaPmF	zaktivizovat
divadelní	divadelní	k2eAgInSc4d1	divadelní
sál	sál	k1gInSc4	sál
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
finanční	finanční	k2eAgFnSc3d1	finanční
náročnosti	náročnost	k1gFnSc3	náročnost
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
musela	muset	k5eAaImAgFnS	muset
ale	ale	k8xC	ale
nadace	nadace	k1gFnSc1	nadace
dům	dům	k1gInSc4	dům
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následujícím	následující	k2eAgMnSc7d1	následující
vlastníkem	vlastník	k1gMnSc7	vlastník
budovy	budova	k1gFnSc2	budova
hotelu	hotel	k1gInSc2	hotel
Central	Central	k1gMnPc2	Central
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zahájila	zahájit	k5eAaPmAgFnS	zahájit
další	další	k2eAgFnSc4d1	další
přestavbu	přestavba	k1gFnSc4	přestavba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
obnovila	obnovit	k5eAaPmAgFnS	obnovit
provoz	provoz	k1gInSc4	provoz
hotelu	hotel	k1gInSc2	hotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gMnSc1	Central
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
pražské	pražský	k2eAgFnSc2d1	Pražská
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
fasáda	fasáda	k1gFnSc1	fasáda
má	mít	k5eAaImIp3nS	mít
nazelenalou	nazelenalý	k2eAgFnSc4d1	nazelenalá
barvu	barva	k1gFnSc4	barva
doplněnou	doplněná	k1gFnSc4	doplněná
plastickým	plastický	k2eAgInSc7d1	plastický
štukovým	štukový	k2eAgInSc7d1	štukový
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
secesním	secesní	k2eAgInSc7d1	secesní
dekorem	dekor	k1gInSc7	dekor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nese	nést	k5eAaImIp3nS	nést
vegetativní	vegetativní	k2eAgInPc4d1	vegetativní
motivy	motiv	k1gInPc4	motiv
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
symboliku	symbolika	k1gFnSc4	symbolika
stromu	strom	k1gInSc2	strom
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dekor	dekor	k1gInSc1	dekor
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
zlaté	zlatý	k2eAgInPc4d1	zlatý
akcenty	akcent	k1gInPc4	akcent
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
Ohmannovu	Ohmannův	k2eAgFnSc4d1	Ohmannův
inspiraci	inspirace	k1gFnSc4	inspirace
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
fasády	fasáda	k1gFnSc2	fasáda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
písmem	písmo	k1gNnSc7	písmo
napsané	napsaný	k2eAgNnSc4d1	napsané
jméno	jméno	k1gNnSc4	jméno
stavitele	stavitel	k1gMnSc2	stavitel
Quida	Quid	k1gMnSc2	Quid
Bělského	Bělský	k1gMnSc2	Bělský
a	a	k8xC	a
architekta	architekt	k1gMnSc2	architekt
Friedricha	Friedrich	k1gMnSc2	Friedrich
Ohmanna	Ohmann	k1gMnSc2	Ohmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
výzdobě	výzdoba	k1gFnSc6	výzdoba
divadelního	divadelní	k2eAgInSc2d1	divadelní
sálu	sál	k1gInSc2	sál
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
malíři	malíř	k1gMnPc1	malíř
Karel	Karel	k1gMnSc1	Karel
Špillar	Špillar	k1gMnSc1	Špillar
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
stěně	stěna	k1gFnSc6	stěna
sálu	sál	k1gInSc2	sál
nad	nad	k7c7	nad
pódiem	pódium	k1gNnSc7	pódium
znázorňovala	znázorňovat	k5eAaImAgFnS	znázorňovat
tanec	tanec	k1gInSc4	tanec
rusalek	rusalka	k1gFnPc2	rusalka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Preisler	Preisler	k1gMnSc1	Preisler
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
malby	malba	k1gFnSc2	malba
Faun	fauna	k1gFnPc2	fauna
svádí	svádět	k5eAaImIp3nS	svádět
dryádu	dryáda	k1gFnSc4	dryáda
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
nad	nad	k7c7	nad
vstupním	vstupní	k2eAgNnSc7d1	vstupní
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
radikální	radikální	k2eAgFnSc6d1	radikální
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929-1930	[number]	k4	1929-1930
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
firma	firma	k1gFnSc1	firma
obnovila	obnovit	k5eAaPmAgFnS	obnovit
provoz	provoz	k1gInSc4	provoz
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
bývalý	bývalý	k2eAgInSc4d1	bývalý
divadelní	divadelní	k2eAgInSc4d1	divadelní
sál	sál	k1gInSc4	sál
k	k	k7c3	k
jídelním	jídelní	k2eAgInPc3d1	jídelní
a	a	k8xC	a
konferenčním	konferenční	k2eAgInPc3d1	konferenční
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnPc1	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
hotelu	hotel	k1gInSc2	hotel
Central	Central	k1gFnSc2	Central
s	s	k7c7	s
čp.	čp.	k?	čp.
1001	[number]	k4	1001
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Hybernské	hybernský	k2eAgFnSc6d1	Hybernská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nedaleko	nedaleko	k7c2	nedaleko
náměstí	náměstí	k1gNnSc2	náměstí
Republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sousední	sousední	k2eAgFnSc1d1	sousední
budova	budova	k1gFnSc1	budova
čp.	čp.	k?	čp.
1000	[number]	k4	1000
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Josefa	Josef	k1gMnSc2	Josef
Blechy	Blecha	k1gMnSc2	Blecha
jako	jako	k9	jako
nájemní	nájemní	k2eAgInPc1d1	nájemní
a	a	k8xC	a
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
čp.	čp.	k?	čp.
1002	[number]	k4	1002
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
neoklasicistní	neoklasicistní	k2eAgFnSc7d1	neoklasicistní
adaptací	adaptace	k1gFnSc7	adaptace
a	a	k8xC	a
nástavbou	nástavba	k1gFnSc7	nástavba
původního	původní	k2eAgInSc2d1	původní
barokního	barokní	k2eAgInSc2d1	barokní
paláce	palác	k1gInSc2	palác
spojovaného	spojovaný	k2eAgMnSc4d1	spojovaný
s	s	k7c7	s
rody	rod	k1gInPc7	rod
Vršovců	Vršovec	k1gMnPc2	Vršovec
<g/>
,	,	kIx,	,
Valdštejnů	Valdštejn	k1gMnPc2	Valdštejn
<g/>
,	,	kIx,	,
Straků	Straka	k1gMnPc2	Straka
a	a	k8xC	a
Vrtbů	Vrtb	k1gMnPc2	Vrtb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAŤKOVÁ	BAŤKOVÁ	kA	BAŤKOVÁ
<g/>
,	,	kIx,	,
Růžena	Růžena	k1gFnSc1	Růžena
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
839	[number]	k4	839
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
627	[number]	k4	627
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
527	[number]	k4	527
<g/>
-	-	kIx~	-
<g/>
529	[number]	k4	529
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
104	[number]	k4	104
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUKEŠ	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
architekti	architekt	k1gMnPc1	architekt
pohledem	pohled	k1gInSc7	pohled
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Lukeše	Lukeš	k1gMnSc2	Lukeš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
244	[number]	k4	244
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
221	[number]	k4	221
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
159	[number]	k4	159
<g/>
,	,	kIx,	,
169	[number]	k4	169
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANOVSKÝ	Janovský	k1gMnSc1	Janovský
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Průvodce	průvodka	k1gFnSc6	průvodka
historickou	historický	k2eAgFnSc7d1	historická
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
144	[number]	k4	144
s.	s.	k?	s.
S.	S.	kA	S.
112	[number]	k4	112
<g/>
,	,	kIx,	,
113	[number]	k4	113
</s>
</p>
<p>
<s>
WITTLICH	WITTLICH	kA	WITTLICH
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
MALÝ	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgFnSc7d1	secesní
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
131	[number]	k4	131
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
847	[number]	k4	847
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
16	[number]	k4	16
<g/>
,	,	kIx,	,
48	[number]	k4	48
<g/>
,	,	kIx,	,
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gMnPc2	Central
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
:	:	kIx,	:
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přivezl	přivézt	k5eAaPmAgMnS	přivézt
secesi	secese	k1gFnSc4	secese
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
architekt	architekt	k1gMnSc1	architekt
Friedrich	Friedrich	k1gMnSc1	Friedrich
Ohmann	Ohmann	k1gMnSc1	Ohmann
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
https://plus.rozhlas.cz/do-prahy-privezl-secesi-kdo-byl-architekt-friedrich-ohmann-7711497	[url]	k4	https://plus.rozhlas.cz/do-prahy-privezl-secesi-kdo-byl-architekt-friedrich-ohmann-7711497
</s>
</p>
<p>
<s>
Odbor	odbor	k1gInSc1	odbor
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
<g/>
:	:	kIx,	:
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
<g/>
;	;	kIx,	;
http://pamatky.praha.eu/jnp/cz/pamatkovy_fond_1/pamatkove_uspechy/pamatkove_uspechy/pamatkove_uspechy-hotel_central_hybernska_10_nove_mesto_praha_1_index.html	[url]	k1gInSc1	http://pamatky.praha.eu/jnp/cz/pamatkovy_fond_1/pamatkove_uspechy/pamatkove_uspechy/pamatkove_uspechy-hotel_central_hybernska_10_nove_mesto_praha_1_index.html
</s>
</p>
<p>
<s>
The	The	k?	The
Guardian	Guardian	k1gInSc1	Guardian
<g/>
:	:	kIx,	:
10	[number]	k4	10
of	of	k?	of
the	the	k?	the
best	best	k1gMnSc1	best
European	European	k1gMnSc1	European
cities	cities	k1gMnSc1	cities
for	forum	k1gNnPc2	forum
art	art	k?	art
nouveau	nouveaus	k1gInSc6	nouveaus
<g/>
;	;	kIx,	;
https://www.theguardian.com/travel/2016/mar/29/10-best-art-nouveau-cities-europe-prague-budapest-glasgow	[url]	k4	https://www.theguardian.com/travel/2016/mar/29/10-best-art-nouveau-cities-europe-prague-budapest-glasgow
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
divadelní	divadelní	k2eAgFnSc1d1	divadelní
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
:	:	kIx,	:
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
<g/>
;	;	kIx,	;
http://encyklopedie.idu.cz/index.php/%C4%8Cerven%C3%A1_sedma	[url]	k1gFnSc1	http://encyklopedie.idu.cz/index.php/%C4%8Cerven%C3%A1_sedma
</s>
</p>
