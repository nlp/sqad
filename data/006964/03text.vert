<s>
Frank	Frank	k1gMnSc1	Frank
Anthony	Anthona	k1gFnSc2	Anthona
Thomas	Thomas	k1gMnSc1	Thomas
Iero	Iero	k1gMnSc1	Iero
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1981	[number]	k4	1981
Belleville	Bellevill	k1gMnSc2	Bellevill
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
člen	člen	k1gMnSc1	člen
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gFnPc2	Chemical
Romance	romance	k1gFnPc4	romance
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Belleville	Bellevilla	k1gFnSc6	Bellevilla
<g/>
,	,	kIx,	,
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
a	a	k8xC	a
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Queen	Quena	k1gFnPc2	Quena
of	of	k?	of
Peace	Peace	k1gMnSc1	Peace
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Arlingtonu	Arlington	k1gInSc6	Arlington
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
Rutgerskou	Rutgerský	k2eAgFnSc4d1	Rutgerský
universitu	universita	k1gFnSc4	universita
<g/>
,	,	kIx,	,
předčasně	předčasně	k6eAd1	předčasně
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
kapele	kapela	k1gFnSc3	kapela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
kytaristou	kytarista	k1gMnSc7	kytarista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zpívá	zpívat	k5eAaImIp3nS	zpívat
zadní	zadní	k2eAgInPc4d1	zadní
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc3	Chemical
Romance	romance	k1gFnSc2	romance
založil	založit	k5eAaPmAgMnS	založit
Frank	Frank	k1gMnSc1	Frank
svou	svůj	k3xOyFgFnSc4	svůj
kapelu	kapela	k1gFnSc4	kapela
frnkiero	frnkiero	k1gNnSc4	frnkiero
andthe	andth	k1gFnSc2	andth
cellabration	cellabration	k1gInSc4	cellabration
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
vydal	vydat	k5eAaPmAgInS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
album	album	k1gNnSc1	album
Stomachaches	Stomachachesa	k1gFnPc2	Stomachachesa
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Gerard	Gerard	k1gMnSc1	Gerard
Way	Way	k1gMnSc1	Way
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
už	už	k6eAd1	už
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc3	Chemical
Romance	romance	k1gFnSc2	romance
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
v	v	k7c6	v
několika	několik	k4yIc6	několik
jiných	jiný	k2eAgFnPc6d1	jiná
kapelách	kapela	k1gFnPc6	kapela
(	(	kIx(	(
<g/>
Pencey	Pencea	k1gFnSc2	Pencea
Prep	Prep	k1gInSc1	Prep
<g/>
,	,	kIx,	,
I	i	k8xC	i
Am	Am	k1gFnSc1	Am
a	a	k8xC	a
Graveyard	Graveyard	k1gInSc1	Graveyard
<g/>
,	,	kIx,	,
Hybrid	hybrid	k1gInSc1	hybrid
<g/>
,	,	kIx,	,
Sector	Sector	k1gInSc1	Sector
12	[number]	k4	12
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Give	Giv	k1gMnSc2	Giv
Up	Up	k1gMnSc2	Up
the	the	k?	the
Ghost	Ghost	k1gInSc1	Ghost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
Epiphone	Epiphon	k1gInSc5	Epiphon
Les	les	k1gInSc4	les
Paul	Paul	k1gMnSc1	Paul
Elite	Elit	k1gInSc5	Elit
Series	Series	k1gMnSc1	Series
a	a	k8xC	a
Zakk	Zakk	k1gMnSc1	Zakk
Wylde	Wyld	k1gInSc5	Wyld
"	"	kIx"	"
<g/>
Grail	Grail	k1gInSc1	Grail
<g/>
"	"	kIx"	"
Gibson	Gibson	k1gInSc1	Gibson
Les	les	k1gInSc1	les
Paul	Paula	k1gFnPc2	Paula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgFnSc2d1	textilní
a	a	k8xC	a
vydavatelské	vydavatelský	k2eAgFnSc2d1	vydavatelská
společnosti	společnost	k1gFnSc2	společnost
Skeleton	skeleton	k1gInSc4	skeleton
Crew	Crew	k1gFnSc2	Crew
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Leathermouth	Leathermoutha	k1gFnPc2	Leathermoutha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
kapely	kapela	k1gFnSc2	kapela
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Deweesem	Dewees	k1gMnSc7	Dewees
zaločil	zaločit	k5eAaPmAgInS	zaločit
elektronicko-hardcorový	elektronickoardcorový	k2eAgInSc1d1	elektronicko-hardcorový
projekt	projekt	k1gInSc1	projekt
Death	Deatha	k1gFnPc2	Deatha
Spells	Spellsa	k1gFnPc2	Spellsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Jamií	Jamie	k1gFnSc7	Jamie
Nestorovou	Nestorův	k2eAgFnSc7d1	Nestorova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgNnP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
Cherry	Cherra	k1gFnSc2	Cherra
a	a	k8xC	a
Lily	lít	k5eAaImAgFnP	lít
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
syn	syn	k1gMnSc1	syn
Miles	Miles	k1gMnSc1	Miles
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
svojí	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Best	Best	k1gInSc1	Best
Dad	Dad	k1gFnSc2	Dad
EVER	EVER	kA	EVER
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
táta	táta	k1gMnSc1	táta
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc3	Chemical
Romance	romance	k1gFnSc2	romance
pracoval	pracovat	k5eAaImAgMnS	pracovat
Frank	Frank	k1gMnSc1	Frank
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
domácím	domácí	k2eAgNnSc6d1	domácí
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
začal	začít	k5eAaPmAgMnS	začít
své	svůj	k3xOyFgMnPc4	svůj
písě	písě	k1gMnPc4	písě
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
akcích	akce	k1gFnPc6	akce
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
demo	demo	k2eAgInSc1d1	demo
"	"	kIx"	"
<g/>
joyriding	joyriding	k1gInSc1	joyriding
<g/>
"	"	kIx"	"
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
novou	nový	k2eAgFnSc7d1	nová
kapelou	kapela	k1gFnSc7	kapela
album	album	k1gNnSc4	album
Stomachaches	Stomachaches	k1gInSc4	Stomachaches
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nové	nový	k2eAgFnSc6d1	nová
kapele	kapela	k1gFnSc6	kapela
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
Jarrod	Jarrod	k1gInSc4	Jarrod
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gFnSc1	Chemical
Romance	romance	k1gFnPc4	romance
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Boba	Bob	k1gMnSc2	Bob
Bryara	Bryar	k1gMnSc2	Bryar
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vydal	vydat	k5eAaPmAgMnS	vydat
video	video	k1gNnSc4	video
debutového	debutový	k2eAgInSc2d1	debutový
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Weighted	Weighted	k1gInSc1	Weighted
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
s	s	k7c7	s
The	The	k1gFnSc7	The
Used	Useda	k1gFnPc2	Useda
a	a	k8xC	a
Taking	Taking	k1gInSc1	Taking
Back	Backa	k1gFnPc2	Backa
Sunday	Sundaa	k1gFnSc2	Sundaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Frank	Frank	k1gMnSc1	Frank
Iero	Iero	k1gMnSc1	Iero
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Frank	Frank	k1gMnSc1	Frank
Iero	Iero	k1gNnSc4	Iero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gFnPc7	Chemical
Romance	romance	k1gFnSc1	romance
official	official	k1gInSc1	official
Website	Websit	k1gInSc5	Websit
Band	banda	k1gFnPc2	banda
biography	biograph	k1gInPc1	biograph
from	from	k6eAd1	from
Alternative	Alternativ	k1gInSc5	Alternativ
Press	Pressa	k1gFnPc2	Pressa
</s>
