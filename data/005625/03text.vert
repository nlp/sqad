<s>
Shih-tzu	Shihza	k1gFnSc4	Shih-tza
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc4	výslovnost
[	[	kIx(	[
<g/>
šícu	šícu	k6eAd1	šícu
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starobylé	starobylý	k2eAgNnSc4d1	starobylé
plemeno	plemeno	k1gNnSc4	plemeno
společenských	společenský	k2eAgMnPc2d1	společenský
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
shih-tzu	shihz	k1gInSc2	shih-tz
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
počátkům	počátek	k1gInPc3	počátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
záhadný	záhadný	k2eAgInSc4d1	záhadný
jako	jako	k8xC	jako
původ	původ	k1gInSc4	původ
většiny	většina	k1gFnSc2	většina
asijských	asijský	k2eAgNnPc2d1	asijské
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
čínských	čínský	k2eAgMnPc2d1	čínský
psů	pes	k1gMnPc2	pes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
plemeno	plemeno	k1gNnSc1	plemeno
shih-tzu	shihz	k1gInSc2	shih-tz
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
a	a	k8xC	a
cílevědomě	cílevědomě	k6eAd1	cílevědomě
kříženo	křížit	k5eAaImNgNnS	křížit
po	po	k7c4	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
původu	původ	k1gInSc2	původ
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
je	být	k5eAaImIp3nS	být
Tibet	Tibet	k1gInSc1	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Předkové	předek	k1gMnPc1	předek
shih-tzu	shihz	k1gInSc2	shih-tz
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
a	a	k8xC	a
klášterech	klášter	k1gInPc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
mezi	mezi	k7c7	mezi
Tibetem	Tibet	k1gInSc7	Tibet
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
propojení	propojení	k1gNnSc3	propojení
<g/>
,	,	kIx,	,
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
chov	chov	k1gInSc4	chov
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
těchto	tento	k3xDgMnPc2	tento
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
posvátnosti	posvátnost	k1gFnSc6	posvátnost
tibetských	tibetský	k2eAgMnPc2d1	tibetský
lvích	lví	k2eAgMnPc2d1	lví
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
údajně	údajně	k6eAd1	údajně
dále	daleko	k6eAd2	daleko
přežívají	přežívat	k5eAaImIp3nP	přežívat
duše	duše	k1gFnPc1	duše
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
velice	velice	k6eAd1	velice
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Posvátnost	posvátnost	k1gFnSc4	posvátnost
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
i	i	k9	i
sochy	socha	k1gFnPc1	socha
se	s	k7c7	s
zřetelnou	zřetelný	k2eAgFnSc7d1	zřetelná
podobou	podoba	k1gFnSc7	podoba
lvího	lví	k2eAgMnSc4d1	lví
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
lvů	lev	k1gInPc2	lev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
z	z	k7c2	z
mědi	měď	k1gFnSc2	měď
nebo	nebo	k8xC	nebo
kamene	kámen	k1gInSc2	kámen
umístěné	umístěný	k2eAgFnSc2d1	umístěná
před	před	k7c7	před
vchody	vchod	k1gInPc7	vchod
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
synagog	synagoga	k1gFnPc2	synagoga
jako	jako	k8xS	jako
strážci	strážce	k1gMnPc7	strážce
či	či	k8xC	či
hlídači	hlídač	k1gMnPc7	hlídač
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
souvisejících	související	k2eAgFnPc2d1	související
se	se	k3xPyFc4	se
lvím	lví	k2eAgMnSc7d1	lví
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
tyto	tento	k3xDgFnPc4	tento
psy	pes	k1gMnPc7	pes
lidé	člověk	k1gMnPc1	člověk
běžně	běžně	k6eAd1	běžně
chovali	chovat	k5eAaImAgMnP	chovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
domovech	domov	k1gInPc6	domov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
Jeho	jeho	k3xOp3gFnSc4	jeho
Svatost	svatost	k1gFnSc4	svatost
Dalajláma	dalajláma	k1gMnSc1	dalajláma
daroval	darovat	k5eAaPmAgMnS	darovat
císařovně	císařovna	k1gFnSc3	císařovna
vdově	vdova	k1gFnSc3	vdova
Cch	Cch	k1gFnSc3	Cch
<g/>
́	́	k?	́
<g/>
-	-	kIx~	-
si	se	k3xPyFc3	se
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
držela	držet	k5eAaImAgFnS	držet
přes	přes	k7c4	přes
stovku	stovka	k1gFnSc4	stovka
pekinézů	pekinéz	k1gInPc2	pekinéz
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
popisováni	popisován	k2eAgMnPc1d1	popisován
jako	jako	k8xC	jako
podobní	podobný	k2eAgMnPc1d1	podobný
"	"	kIx"	"
<g/>
lvím	lví	k2eAgMnPc3d1	lví
psům	pes	k1gMnPc3	pes
<g/>
"	"	kIx"	"
vídaným	vídaný	k2eAgNnSc7d1	vídané
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
je	být	k5eAaImIp3nS	být
nazvala	nazvat	k5eAaPmAgFnS	nazvat
ši-tzu	šiza	k1gFnSc4	ši-tza
kou	kou	k?	kou
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
byli	být	k5eAaImAgMnP	být
psi	pes	k1gMnPc1	pes
rozdáni	rozdán	k2eAgMnPc1d1	rozdán
nejrůznějším	různý	k2eAgMnSc7d3	nejrůznější
zájemcům	zájemce	k1gMnPc3	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
oficiální	oficiální	k2eAgInPc1d1	oficiální
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
importu	import	k1gInSc6	import
lvích	lví	k2eAgMnPc2d1	lví
psů	pes	k1gMnPc2	pes
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
všichni	všechen	k3xTgMnPc1	všechen
bez	bez	k7c2	bez
zanechání	zanechání	k1gNnSc2	zanechání
potomků	potomek	k1gMnPc2	potomek
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgMnPc1d1	další
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
až	až	k9	až
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plemenné	plemenný	k2eAgFnSc6d1	plemenná
knize	kniha	k1gFnSc6	kniha
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
fena	fena	k1gFnSc1	fena
Chanell	Chanell	k1gMnSc1	Chanell
vom	vom	k?	vom
Tadsch	Tadsch	k1gMnSc1	Tadsch
Mahal	Mahal	k1gMnSc1	Mahal
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
dcera	dcera	k1gFnSc1	dcera
Chanell	Chanell	k1gInSc1	Chanell
a	a	k8xC	a
Urmela	Urmela	k1gFnSc1	Urmela
vom	vom	k?	vom
Heydpark	Heydpark	k1gInSc1	Heydpark
<g/>
.	.	kIx.	.
</s>
<s>
Dovezená	dovezený	k2eAgFnSc1d1	dovezená
fena	fena	k1gFnSc1	fena
Andromeda	Andromed	k1gMnSc2	Andromed
di	di	k?	di
Casa	Casus	k1gMnSc2	Casus
Corsini	Corsin	k2eAgMnPc1d1	Corsin
dala	dát	k5eAaPmAgFnS	dát
chovatelské	chovatelský	k2eAgFnSc6d1	chovatelská
stanici	stanice	k1gFnSc6	stanice
Krása	krása	k1gFnSc1	krása
Moravy	Morava	k1gFnSc2	Morava
život	život	k1gInSc1	život
vrhu	vrh	k1gInSc2	vrh
štěňat	štěně	k1gNnPc2	štěně
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgInS	být
E	E	kA	E
<g/>
́	́	k?	́
<g/>
lal	lal	k?	lal
vom	vom	k?	vom
Tschomo	Tschoma	k1gFnSc5	Tschoma
Lungma	Lungmum	k1gNnPc1	Lungmum
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
dovezenými	dovezený	k2eAgMnPc7d1	dovezený
zvířaty	zvíře	k1gNnPc7	zvíře
byla	být	k5eAaImAgFnS	být
fena	fena	k1gFnSc1	fena
Etana	Etan	k1gMnSc2	Etan
Chicatita	Chicatit	k1gMnSc2	Chicatit
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
Berril	Berril	k1gMnSc1	Berril
Le	Le	k1gFnPc2	Le
petit	petit	k1gInSc4	petit
Joujou	Joujá	k1gFnSc4	Joujá
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
NSR	NSR	kA	NSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
psovi	pes	k1gMnSc3	pes
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
vrhy	vrh	k1gInPc1	vrh
v	v	k7c6	v
chovatelských	chovatelský	k2eAgFnPc6d1	chovatelská
stanicích	stanice	k1gFnPc6	stanice
Sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
a	a	k8xC	a
Krása	krása	k1gFnSc1	krása
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dovozy	dovoz	k1gInPc1	dovoz
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
NSR	NSR	kA	NSR
a	a	k8xC	a
NDR	NDR	kA	NDR
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
chovnou	chovný	k2eAgFnSc4d1	chovná
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Chovatelský	chovatelský	k2eAgInSc1d1	chovatelský
klub	klub	k1gInSc1	klub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
