<p>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
ly	ly	k?	ly
či	či	k8xC	či
l.y.	l.y.	k?	l.y.
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
light-year	lightear	k1gInSc1	light-year
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
známé	známý	k2eAgFnSc2d1	známá
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
c	c	k0	c
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
<s>
1	[number]	k4	1
ly	ly	k?	ly
=	=	kIx~	=
a	a	k8xC	a
×	×	k?	×
c	c	k0	c
=	=	kIx~	=
9	[number]	k4	9
460	[number]	k4	460
730	[number]	k4	730
472	[number]	k4	472
580	[number]	k4	580
800	[number]	k4	800
m	m	kA	m
≈	≈	k?	≈
9,46	[number]	k4	9,46
×	×	k?	×
1012	[number]	k4	1012
km	km	kA	km
≈	≈	k?	≈
10	[number]	k4	10
bilionů	bilion	k4xCgInPc2	bilion
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
Světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc4d1	světelný
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
a	a	k8xC	a
odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
)	)	kIx)	)
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jednotky	jednotka	k1gFnPc4	jednotka
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
relativistické	relativistický	k2eAgFnSc6d1	relativistická
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
telekomunikačních	telekomunikační	k2eAgInPc6d1	telekomunikační
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
menší	malý	k2eAgFnPc4d2	menší
jednotky	jednotka	k1gFnPc4	jednotka
obdobné	obdobný	k2eAgFnPc4d1	obdobná
světelnému	světelný	k2eAgInSc3d1	světelný
roku	rok	k1gInSc3	rok
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
světelná	světelný	k2eAgFnSc1d1	světelná
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ls	ls	k?	ls
či	či	k8xC	či
l.	l.	k?	l.
<g/>
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
udávají	udávat	k5eAaImIp3nP	udávat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
světlo	světlo	k1gNnSc1	světlo
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
danou	daný	k2eAgFnSc4d1	daná
menší	malý	k2eAgFnSc4d2	menší
časovou	časový	k2eAgFnSc4d1	časová
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
u	u	k7c2	u
světelné	světelný	k2eAgFnSc2d1	světelná
sekundy	sekunda	k1gFnSc2	sekunda
konkrétně	konkrétně	k6eAd1	konkrétně
za	za	k7c4	za
1	[number]	k4	1
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
i	i	k8xC	i
jednotky	jednotka	k1gFnSc2	jednotka
délky	délka	k1gFnSc2	délka
udávající	udávající	k2eAgFnSc4d1	udávající
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
uraženou	uražený	k2eAgFnSc4d1	uražená
za	za	k7c4	za
dekadické	dekadický	k2eAgInPc4d1	dekadický
díly	díl	k1gInPc4	díl
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
světelná	světelný	k2eAgFnSc1d1	světelná
milisekunda	milisekunda	k1gFnSc1	milisekunda
(	(	kIx(	(
<g/>
≈	≈	k?	≈
<g/>
299,8	[number]	k4	299,8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mikrosekunda	mikrosekunda	k1gFnSc1	mikrosekunda
(	(	kIx(	(
<g/>
≈	≈	k?	≈
<g/>
299,8	[number]	k4	299,8
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
nanosekunda	nanosekunda	k1gFnSc1	nanosekunda
(	(	kIx(	(
<g/>
≈	≈	k?	≈
<g/>
29,98	[number]	k4	29,98
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
astronomické	astronomický	k2eAgFnPc1d1	astronomická
jednotky	jednotka	k1gFnPc1	jednotka
délky	délka	k1gFnSc2	délka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
o	o	k7c6	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgFnSc6d2	veliký
jednotce	jednotka	k1gFnSc6	jednotka
parsek	parsek	k1gInSc1	parsek
(	(	kIx(	(
<g/>
pc	pc	k?	pc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
astronomická	astronomický	k2eAgFnSc1d1	astronomická
jednotka	jednotka	k1gFnSc1	jednotka
úhlový	úhlový	k2eAgInSc4d1	úhlový
rozměr	rozměr	k1gInSc4	rozměr
jedné	jeden	k4xCgFnSc2	jeden
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
pc	pc	k?	pc
≈	≈	k?	≈
3,262	[number]	k4	3,262
ly	ly	k?	ly
≈	≈	k?	≈
30	[number]	k4	30
bilionů	bilion	k4xCgInPc2	bilion
kmNa	kmNa	k6eAd1	kmNa
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
parseku	parsec	k1gInSc2	parsec
běžné	běžný	k2eAgNnSc1d1	běžné
používání	používání	k1gNnSc1	používání
dekadických	dekadický	k2eAgFnPc2d1	dekadická
předpon	předpona	k1gFnPc2	předpona
SI	si	k1gNnSc4	si
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
násobky	násobek	k1gInPc4	násobek
<g/>
:	:	kIx,	:
kiloparsek	kiloparsek	k1gInSc4	kiloparsek
(	(	kIx(	(
<g/>
kpc	kpc	k?	kpc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
megaparsek	megaparsek	k1gMnSc1	megaparsek
(	(	kIx(	(
<g/>
Mpc	Mpc	k1gMnSc1	Mpc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gigaparsek	gigaparsek	k1gMnSc1	gigaparsek
(	(	kIx(	(
<g/>
Gpc	Gpc	k1gMnSc1	Gpc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
kratší	krátký	k2eAgFnPc4d2	kratší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
astronomická	astronomický	k2eAgFnSc1d1	astronomická
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
AU	au	k0	au
=	=	kIx~	=
149	[number]	k4	149
597	[number]	k4	597
870	[number]	k4	870
700	[number]	k4	700
m	m	kA	m
≈	≈	k?	≈
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
</s>
</p>
<p>
<s>
63	[number]	k4	63
241	[number]	k4	241
AU	au	k0	au
≈	≈	k?	≈
1	[number]	k4	1
lySvětelný	lySvětelný	k2eAgInSc4d1	lySvětelný
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
světelná	světelný	k2eAgFnSc1d1	světelná
minuta	minuta	k1gFnSc1	minuta
ani	ani	k8xC	ani
světelná	světelný	k2eAgFnSc1d1	světelná
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
parsek	parsek	k1gInSc1	parsek
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
soustava	soustava	k1gFnSc1	soustava
SI	se	k3xPyFc3	se
připouští	připouštět	k5eAaImIp3nS	připouštět
používat	používat	k5eAaImF	používat
vedle	vedle	k7c2	vedle
soustavných	soustavný	k2eAgFnPc2d1	soustavná
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
metr	metr	k1gInSc4	metr
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
násobky	násobek	k1gInPc4	násobek
a	a	k8xC	a
díly	díl	k1gInPc4	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
používání	používání	k1gNnSc1	používání
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
je	být	k5eAaImIp3nS	být
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
konstanty	konstanta	k1gFnPc1	konstanta
</s>
</p>
<p>
<s>
Foton	foton	k1gInSc1	foton
</s>
</p>
<p>
<s>
Vakuum	vakuum	k1gNnSc1	vakuum
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
</s>
</p>
<p>
<s>
Juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
Předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnPc1	soustava
SI	se	k3xPyFc3	se
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
