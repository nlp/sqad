<s>
Světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
ly	ly	k?	ly
či	či	k8xC	či
l.y.	l.y.	k?	l.y.
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
light-year	lightear	k1gInSc1	light-year
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
