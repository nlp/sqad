<s>
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
závodů	závod	k1gInPc2	závod
formulí	formule	k1gFnPc2	formule
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
královskou	královský	k2eAgFnSc4d1	královská
disciplínu	disciplína	k1gFnSc4	disciplína
automobilového	automobilový	k2eAgInSc2d1	automobilový
sportu	sport	k1gInSc2	sport
pro	pro	k7c4	pro
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
monoposty	monopost	k1gInPc4	monopost
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
federaci	federace	k1gFnSc4	federace
(	(	kIx(	(
<g/>
FIA	FIA	kA	FIA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
organizační	organizační	k2eAgFnSc7d1	organizační
složkou	složka	k1gFnSc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
F1	F1	k1gFnSc2	F1
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
série	série	k1gFnSc2	série
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soutěží	soutěžit	k5eAaImIp3nS	soutěžit
se	se	k3xPyFc4	se
na	na	k7c6	na
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
autodromech	autodrom	k1gInPc6	autodrom
či	či	k8xC	či
tratích	trať	k1gFnPc6	trať
a	a	k8xC	a
městských	městský	k2eAgInPc6d1	městský
okruzích	okruh	k1gInPc6	okruh
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
jsou	být	k5eAaImIp3nP	být
jednomístné	jednomístný	k2eAgInPc1d1	jednomístný
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
speciálně	speciálně	k6eAd1	speciálně
vyrobené	vyrobený	k2eAgFnSc3d1	vyrobená
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
upravují	upravovat	k5eAaImIp3nP	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
pohár	pohár	k1gInSc4	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgNnSc7d1	tradiční
operačním	operační	k2eAgNnSc7d1	operační
centrem	centrum	k1gNnSc7	centrum
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
trhem	trh	k1gInSc7	trh
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplína	disciplína	k1gFnSc1	disciplína
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
a	a	k8xC	a
prosadila	prosadit	k5eAaPmAgFnS	prosadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejnovější	nový	k2eAgMnPc4d3	Nejnovější
pořadatele	pořadatel	k1gMnPc4	pořadatel
se	se	k3xPyFc4	se
zařadili	zařadit	k5eAaPmAgMnP	zařadit
Bahrajn	Bahrajn	k1gMnSc1	Bahrajn
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
novými	nový	k2eAgMnPc7d1	nový
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
jsou	být	k5eAaImIp3nP	být
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Dubaj	Dubaj	k1gFnSc1	Dubaj
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
FIA	FIA	kA	FIA
je	být	k5eAaImIp3nS	být
rozšíření	rozšíření	k1gNnSc1	rozšíření
aktivit	aktivita	k1gFnPc2	aktivita
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
nespočet	nespočet	k1gInSc4	nespočet
Velkých	velký	k2eAgFnPc2d1	velká
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
netěší	těšit	k5eNaImIp3nS	těšit
takové	takový	k3xDgFnSc3	takový
popularitě	popularita	k1gFnSc3	popularita
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Motorismus	motorismus	k1gInSc1	motorismus
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
sérií	série	k1gFnPc2	série
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
politickým	politický	k2eAgInSc7d1	politický
a	a	k8xC	a
komerčním	komerční	k2eAgInSc7d1	komerční
bojem	boj	k1gInSc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
F1	F1	k4	F1
je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
"	"	kIx"	"
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Automobile	automobil	k1gInSc6	automobil
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
federací	federace	k1gFnSc7	federace
-	-	kIx~	-
FIA	FIA	kA	FIA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
Place	plac	k1gInSc6	plac
de	de	k?	de
la	la	k1gNnSc2	la
Concorde	Concord	k1gInSc5	Concord
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Jean	Jean	k1gMnSc1	Jean
Todt	Todt	k1gMnSc1	Todt
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnPc1d1	finanční
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc1d1	obchodní
toky	toka	k1gFnPc1	toka
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
Formula	Formulum	k1gNnSc2	Formulum
One	One	k1gMnSc1	One
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
spadajíci	spadajík	k1gMnPc1	spadajík
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
společnosti	společnost	k1gFnSc2	společnost
SLEC	SLEC	kA	SLEC
Holdings	Holdings	k1gInSc1	Holdings
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prodala	prodat	k5eAaPmAgFnS	prodat
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
část	část	k1gFnSc1	část
akcií	akcie	k1gFnPc2	akcie
CVC	CVC	kA	CVC
Capital	Capital	k1gMnSc1	Capital
Partners	Partners	k1gInSc1	Partners
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
a	a	k8xC	a
televizní	televizní	k2eAgNnPc4d1	televizní
práva	právo	k1gNnPc4	právo
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
Formula	Formula	k1gFnSc1	Formula
One	One	k1gFnSc2	One
Management	management	k1gInSc1	management
a	a	k8xC	a
tu	tu	k6eAd1	tu
ovládá	ovládat	k5eAaImIp3nS	ovládat
Bernie	Bernie	k1gFnSc1	Bernie
Ecclestone	Eccleston	k1gInSc5	Eccleston
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Alpha	Alph	k1gMnSc2	Alph
Prema	Prem	k1gMnSc2	Prem
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
pilot	pilot	k1gMnSc1	pilot
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
je	být	k5eAaImIp3nS	být
rekordmanem	rekordman	k1gMnSc7	rekordman
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
titulů	titul	k1gInPc2	titul
mezi	mezi	k7c7	mezi
jezdci	jezdec	k1gInPc7	jezdec
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ferrari	ferrari	k1gNnSc1	ferrari
získalo	získat	k5eAaPmAgNnS	získat
nejvíce	hodně	k6eAd3	hodně
poháru	pohár	k1gInSc2	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rakušan	Rakušan	k1gMnSc1	Rakušan
Jochen	Jochna	k1gFnPc2	Jochna
Rindt	Rindt	k1gMnSc1	Rindt
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
šampiónem	šampión	k1gMnSc7	šampión
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Sebastian	Sebastian	k1gMnSc1	Sebastian
Vettel	Vettel	k1gMnSc1	Vettel
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
automobilové	automobilový	k2eAgInPc1d1	automobilový
závody	závod	k1gInPc1	závod
byly	být	k5eAaImAgInP	být
pořádány	pořádat	k5eAaImNgInP	pořádat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgInPc2	ten
se	se	k3xPyFc4	se
zúčastňovaly	zúčastňovat	k5eAaImAgInP	zúčastňovat
automobily	automobil	k1gInPc1	automobil
poháněné	poháněný	k2eAgInPc1d1	poháněný
benzínem	benzín	k1gInSc7	benzín
<g/>
,	,	kIx,	,
parou	para	k1gFnSc7	para
i	i	k8xC	i
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začaly	začít	k5eAaPmAgInP	začít
automobily	automobil	k1gInPc1	automobil
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
podle	podle	k7c2	podle
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
což	což	k9	což
byla	být	k5eAaImAgFnS	být
vlastně	vlastně	k9	vlastně
první	první	k4xOgNnPc1	první
technická	technický	k2eAgNnPc1d1	technické
omezení	omezení	k1gNnPc1	omezení
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
)	)	kIx)	)
a	a	k8xC	a
soutěžily	soutěžit	k5eAaImAgFnP	soutěžit
prakticky	prakticky	k6eAd1	prakticky
výhradně	výhradně	k6eAd1	výhradně
automobily	automobil	k1gInPc1	automobil
se	s	k7c7	s
spalovacími	spalovací	k2eAgInPc7d1	spalovací
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Velkých	velký	k2eAgFnPc2d1	velká
cen	cena	k1gFnPc2	cena
byly	být	k5eAaImAgInP	být
závody	závod	k1gInPc1	závod
o	o	k7c4	o
Pohár	pohár	k1gInSc4	pohár
Gordona	Gordon	k1gMnSc2	Gordon
Bennetta	Bennett	k1gMnSc2	Bennett
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vypsal	vypsat	k5eAaPmAgMnS	vypsat
americký	americký	k2eAgMnSc1d1	americký
vydavatel	vydavatel	k1gMnSc1	vydavatel
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
francouzský	francouzský	k2eAgInSc1d1	francouzský
autoklub	autoklub	k1gInSc1	autoklub
(	(	kIx(	(
<g/>
Automobile	automobil	k1gInSc6	automobil
Club	club	k1gInSc4	club
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
ACF	ACF	kA	ACF
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
francouzského	francouzský	k2eAgInSc2d1	francouzský
autoklubu	autoklub	k1gInSc2	autoklub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
době	době	k6eAd1	době
platilo	platit	k5eAaImAgNnS	platit
jediné	jediný	k2eAgNnSc1d1	jediné
omezení	omezení	k1gNnSc1	omezení
<g/>
:	:	kIx,	:
Vůz	vůz	k1gInSc1	vůz
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
1000	[number]	k4	1000
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
přibývalo	přibývat	k5eAaImAgNnS	přibývat
technických	technický	k2eAgNnPc2d1	technické
omezení	omezení	k1gNnPc2	omezení
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
maximální	maximální	k2eAgFnSc1d1	maximální
spotřeba	spotřeba	k1gFnSc1	spotřeba
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
minimální	minimální	k2eAgFnSc1d1	minimální
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
plocha	plocha	k1gFnSc1	plocha
pístů	píst	k1gInPc2	píst
motoru	motor	k1gInSc2	motor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
se	se	k3xPyFc4	se
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
nekonala	konat	k5eNaImAgFnS	konat
pro	pro	k7c4	pro
nezájem	nezájem	k1gInSc4	nezájem
automobilek	automobilka	k1gFnPc2	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Závody	závod	k1gInPc1	závod
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
nákladné	nákladný	k2eAgFnPc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konaly	konat	k5eAaImAgFnP	konat
ještě	ještě	k9	ještě
tři	tři	k4xCgFnPc1	tři
Velké	velký	k2eAgFnPc1d1	velká
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
pravidly	pravidlo	k1gNnPc7	pravidlo
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
série	série	k1gFnSc1	série
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Formule	formule	k1gFnSc1	formule
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
;	;	kIx,	;
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
prvních	první	k4xOgInPc2	první
pět	pět	k4xCc1	pět
ročníků	ročník	k1gInPc2	ročník
evropského	evropský	k2eAgInSc2d1	evropský
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgMnSc1d1	nazván
Grandes	Grandes	k1gMnSc1	Grandes
Epreuves	Epreuves	k1gMnSc1	Epreuves
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
poválečných	poválečný	k2eAgInPc6d1	poválečný
letech	let	k1gInPc6	let
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Formule	formule	k1gFnSc1	formule
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
poválečných	poválečný	k2eAgInPc2d1	poválečný
závodů	závod	k1gInPc2	závod
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
prozatím	prozatím	k6eAd1	prozatím
neměly	mít	k5eNaImAgInP	mít
status	status	k1gInSc4	status
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
závodů	závod	k1gInPc2	závod
bylo	být	k5eAaImAgNnS	být
zorganizováno	zorganizovat	k5eAaPmNgNnS	zorganizovat
již	již	k9	již
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
uspořádat	uspořádat	k5eAaPmF	uspořádat
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
byla	být	k5eAaImAgFnS	být
Formule	formule	k1gFnSc1	formule
A	a	k9	a
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c6	na
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nižší	nízký	k2eAgFnSc1d2	nižší
série	série	k1gFnSc1	série
Formule	formule	k1gFnSc1	formule
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
zástupců	zástupce	k1gMnPc2	zástupce
pořadatelů	pořadatel	k1gMnPc2	pořadatel
vybralo	vybrat	k5eAaPmAgNnS	vybrat
7	[number]	k4	7
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
započítávaných	započítávaný	k2eAgInPc2d1	započítávaný
do	do	k7c2	do
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
závodem	závod	k1gInSc7	závod
nově	nova	k1gFnSc3	nova
založeného	založený	k2eAgNnSc2d1	založené
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
mezi	mezi	k7c4	mezi
piloty	pilota	k1gFnPc4	pilota
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
body	bod	k1gInPc1	bod
i	i	k9	i
mezi	mezi	k7c4	mezi
konstruktéry	konstruktér	k1gMnPc4	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInPc4d1	národní
šampionáty	šampionát	k1gInPc4	šampionát
pro	pro	k7c4	pro
vozy	vůz	k1gInPc4	vůz
F1	F1	k1gMnPc2	F1
se	se	k3xPyFc4	se
pořádaly	pořádat	k5eAaImAgFnP	pořádat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1960	[number]	k4	1960
a	a	k8xC	a
1970	[number]	k4	1970
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátečních	počáteční	k2eAgInPc6d1	počáteční
letech	let	k1gInPc6	let
byla	být	k5eAaImAgFnS	být
organizována	organizován	k2eAgFnSc1d1	organizována
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
nezapočítávaly	započítávat	k5eNaImAgFnP	započítávat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
57	[number]	k4	57
ročníků	ročník	k1gInPc2	ročník
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
768	[number]	k4	768
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
ročníku	ročník	k1gInSc2	ročník
2007	[number]	k4	2007
jejich	jejich	k3xOp3gFnSc4	jejich
počet	počet	k1gInSc1	počet
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
na	na	k7c4	na
785	[number]	k4	785
-	-	kIx~	-
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přidat	přidat	k5eAaPmF	přidat
dalších	další	k2eAgFnPc2d1	další
368	[number]	k4	368
závodů	závod	k1gInPc2	závod
nezapočítávaných	započítávaný	k2eNgInPc2d1	započítávaný
do	do	k7c2	do
klasifikace	klasifikace	k1gFnSc2	klasifikace
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
období	období	k1gNnSc6	období
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nejznámějším	známý	k2eAgInSc7d3	nejznámější
závodem	závod	k1gInSc7	závod
Race	Race	k1gNnSc4	Race
of	of	k?	of
Champions	Champions	k1gInSc1	Champions
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
v	v	k7c4	v
Brands	Brands	k1gInSc4	Brands
Hatch	Hatcha	k1gFnPc2	Hatcha
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
závodů	závod	k1gInPc2	závod
se	se	k3xPyFc4	se
poslední	poslední	k2eAgInSc1d1	poslední
závod	závod	k1gInSc1	závod
nezapočítávaný	započítávaný	k2eNgInSc1d1	započítávaný
do	do	k7c2	do
MS	MS	kA	MS
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1984	[number]	k4	1984
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
uskutečněné	uskutečněný	k2eAgInPc1d1	uskutečněný
závody	závod	k1gInPc1	závod
F1	F1	k1gMnSc4	F1
součástí	součást	k1gFnSc7	součást
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
šampionát	šampionát	k1gInSc1	šampionát
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
nadvlády	nadvláda	k1gFnSc2	nadvláda
vozů	vůz	k1gInPc2	vůz
Alfa	alfa	k1gNnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Giuseppe	Giusepp	k1gInSc5	Giusepp
Farina	Farin	k2eAgNnPc1d1	Farin
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
Alfa	alfa	k1gFnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
158	[number]	k4	158
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
stájovými	stájový	k2eAgMnPc7d1	stájový
kolegy	kolega	k1gMnPc7	kolega
Fangiem	Fangius	k1gMnSc7	Fangius
a	a	k8xC	a
Fagiolim	Fagioli	k1gNnSc7	Fagioli
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
Fangio	Fangio	k1gMnSc1	Fangio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
vylepšeném	vylepšený	k2eAgInSc6d1	vylepšený
modelu	model	k1gInSc6	model
Alfa	alfa	k1gFnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
159	[number]	k4	159
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
tituly	titul	k1gInPc4	titul
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
vozech	vůz	k1gInPc6	vůz
<g/>
:	:	kIx,	:
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
<g/>
,	,	kIx,	,
Maserati	Maserat	k1gMnPc1	Maserat
a	a	k8xC	a
Ferrari	Ferrari	k1gMnPc1	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
nadvládu	nadvláda	k1gFnSc4	nadvláda
přerušil	přerušit	k5eAaPmAgMnS	přerušit
jen	jen	k9	jen
Alberto	Alberta	k1gFnSc5	Alberta
Ascari	Ascari	k1gNnPc7	Ascari
na	na	k7c4	na
Ferrari	Ferrari	k1gMnSc4	Ferrari
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
obhájit	obhájit	k5eAaPmF	obhájit
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prozatím	prozatím	k6eAd1	prozatím
posledním	poslední	k2eAgMnSc7d1	poslední
italským	italský	k2eAgMnSc7d1	italský
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stirling	Stirling	k1gInSc1	Stirling
Moss	Moss	k1gInSc1	Moss
dokázal	dokázat	k5eAaPmAgInS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgMnS	získat
titul	titul	k1gInSc4	titul
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvními	první	k4xOgFnPc7	první
technickými	technický	k2eAgFnPc7d1	technická
inovacemi	inovace	k1gFnPc7	inovace
přišel	přijít	k5eAaPmAgMnS	přijít
Cooper	Cooper	k1gMnSc1	Cooper
znovuzavedením	znovuzavedení	k1gNnSc7	znovuzavedení
středního	střední	k2eAgInSc2d1	střední
zdvihového	zdvihový	k2eAgInSc2d1	zdvihový
objemu	objem	k1gInSc2	objem
válců	válec	k1gInPc2	válec
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pokusu	pokus	k1gInSc2	pokus
konstruktéra	konstruktér	k1gMnSc2	konstruktér
firmy	firma	k1gFnSc2	firma
Auto	auto	k1gNnSc1	auto
Union	union	k1gInSc1	union
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Porsche	Porsche	k1gNnSc2	Porsche
během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přirozené	přirozený	k2eAgFnSc2d1	přirozená
evoluce	evoluce	k1gFnSc2	evoluce
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
projektu	projekt	k1gInSc2	projekt
z	z	k7c2	z
Formule	formule	k1gFnSc2	formule
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k6eAd1	Jack
Brabham	Brabham	k1gInSc1	Brabham
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1959	[number]	k4	1959
a	a	k8xC	a
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
pilotem	pilot	k1gMnSc7	pilot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
triumfovat	triumfovat	k5eAaBmF	triumfovat
i	i	k9	i
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
tak	tak	k6eAd1	tak
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
nové	nový	k2eAgFnSc2d1	nová
koncepce	koncepce	k1gFnSc2	koncepce
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
již	již	k9	již
všechny	všechen	k3xTgInPc1	všechen
vozy	vůz	k1gInPc1	vůz
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
zdvihovým	zdvihový	k2eAgInSc7d1	zdvihový
objemem	objem	k1gInSc7	objem
válců	válec	k1gInPc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
pilotem	pilot	k1gInSc7	pilot
britské	britský	k2eAgFnSc2d1	britská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
Mike	Mike	k1gNnPc2	Mike
Hawthorn	Hawthorna	k1gFnPc2	Hawthorna
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Ferrari	Ferrari	k1gMnSc7	Ferrari
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vozy	vůz	k1gInPc1	vůz
Vanwall	Vanwalla	k1gFnPc2	Vanwalla
získávají	získávat	k5eAaImIp3nP	získávat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
pohár	pohár	k1gInSc4	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Génius	génius	k1gMnSc1	génius
Colin	Colin	k1gMnSc1	Colin
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
,	,	kIx,	,
projektant	projektant	k1gMnSc1	projektant
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
zakladatel	zakladatel	k1gMnSc1	zakladatel
stáje	stáj	k1gFnSc2	stáj
Lotus	Lotus	kA	Lotus
<g/>
,	,	kIx,	,
konstruoval	konstruovat	k5eAaImAgMnS	konstruovat
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dominovaly	dominovat	k5eAaImAgFnP	dominovat
formuli	formule	k1gFnSc4	formule
1	[number]	k4	1
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
takové	takový	k3xDgFnPc4	takový
piloty	pilota	k1gFnPc4	pilota
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
byli	být	k5eAaImAgMnP	být
Jim	on	k3xPp3gMnPc3	on
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnSc1	Jackie
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Surtees	Surtees	k1gMnSc1	Surtees
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Brabham	Brabham	k1gInSc1	Brabham
<g/>
,	,	kIx,	,
Graham	Graham	k1gMnSc1	Graham
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
Denny	Denna	k1gFnPc1	Denna
Hulme	houlit	k5eAaImRp1nP	houlit
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
také	také	k9	také
britští	britský	k2eAgMnPc1d1	britský
piloti	pilot	k1gMnPc1	pilot
získali	získat	k5eAaPmAgMnP	získat
jasnou	jasný	k2eAgFnSc4d1	jasná
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
motoristické	motoristický	k2eAgFnSc6d1	motoristická
disciplíně	disciplína	k1gFnSc6	disciplína
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
12	[number]	k4	12
světových	světový	k2eAgInPc2d1	světový
titulů	titul	k1gInPc2	titul
v	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1962	[number]	k4	1962
a	a	k8xC	a
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
Lotus	Lotus	kA	Lotus
vyprojektoval	vyprojektovat	k5eAaPmAgMnS	vyprojektovat
první	první	k4xOgInSc4	první
vůz	vůz	k1gInSc4	vůz
se	s	k7c7	s
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
skořepinovou	skořepinový	k2eAgFnSc7d1	skořepinová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
namísto	namísto	k7c2	namísto
klasického	klasický	k2eAgInSc2d1	klasický
trubkového	trubkový	k2eAgInSc2d1	trubkový
rámu	rám	k1gInSc2	rám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
přišel	přijít	k5eAaPmAgInS	přijít
Lotus	Lotus	kA	Lotus
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
zbarvením	zbarvení	k1gNnSc7	zbarvení
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgInS	opustit
tradiční	tradiční	k2eAgFnSc4d1	tradiční
zelenou	zelená	k1gFnSc4	zelená
pro	pro	k7c4	pro
britské	britský	k2eAgInPc4d1	britský
vozy	vůz	k1gInPc4	vůz
a	a	k8xC	a
představil	představit	k5eAaPmAgInS	představit
monopost	monopost	k1gInSc1	monopost
vyvedený	vyvedený	k2eAgInSc1d1	vyvedený
v	v	k7c6	v
červeno-zlato-bílých	červenolatoílý	k2eAgFnPc6d1	červeno-zlato-bílý
barvách	barva	k1gFnPc6	barva
svého	své	k1gNnSc2	své
sponzora	sponzor	k1gMnSc2	sponzor
-	-	kIx~	-
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
tak	tak	k9	tak
éra	éra	k1gFnSc1	éra
sponzorství	sponzorství	k1gNnSc2	sponzorství
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
také	také	k9	také
vzhled	vzhled	k1gInSc1	vzhled
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
až	až	k9	až
doposud	doposud	k6eAd1	doposud
byla	být	k5eAaImAgFnS	být
pojmem	pojem	k1gInSc7	pojem
neznámým	známý	k2eNgInSc7d1	neznámý
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
významnosti	významnost	k1gFnSc6	významnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgNnPc4	první
křidélka	křidélko	k1gNnPc4	křidélko
a	a	k8xC	a
aerodynamické	aerodynamický	k2eAgFnPc4d1	aerodynamická
plošky	ploška	k1gFnPc4	ploška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
FISA	FISA	kA	FISA
(	(	kIx(	(
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
du	du	k?	du
Sport	sport	k1gInSc1	sport
Automobile	automobil	k1gInSc5	automobil
<g/>
)	)	kIx)	)
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Jean	Jean	k1gMnSc1	Jean
Marie	Maria	k1gFnSc2	Maria
Balestre	Balestr	k1gInSc5	Balestr
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
byly	být	k5eAaImAgFnP	být
především	především	k9	především
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
táhnoucí	táhnoucí	k2eAgInPc1d1	táhnoucí
spory	spor	k1gInPc1	spor
s	s	k7c7	s
konkurenční	konkurenční	k2eAgFnSc7d1	konkurenční
FOCA	FOCA	kA	FOCA
(	(	kIx(	(
<g/>
Formula	Formula	k1gFnSc1	Formula
One	One	k1gFnSc2	One
Constructors	Constructorsa	k1gFnPc2	Constructorsa
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
řízenou	řízený	k2eAgFnSc7d1	řízená
Bernie	Bernie	k1gFnSc1	Bernie
Ecclestonem	Eccleston	k1gInSc7	Eccleston
<g/>
)	)	kIx)	)
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
tokem	tok	k1gInSc7	tok
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
plynoucích	plynoucí	k2eAgFnPc2d1	plynoucí
především	především	k9	především
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
televizních	televizní	k2eAgNnPc2d1	televizní
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
také	také	k9	také
koncem	koncem	k7c2	koncem
éry	éra	k1gFnSc2	éra
desetiválcových	desetiválcový	k2eAgInPc2d1	desetiválcový
motorů	motor	k1gInPc2	motor
v	v	k7c6	v
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Desetiválcové	desetiválcový	k2eAgInPc1d1	desetiválcový
motory	motor	k1gInPc1	motor
jsou	být	k5eAaImIp3nP	být
nejpoužívanější	používaný	k2eAgFnSc7d3	nejpoužívanější
konfigurací	konfigurace	k1gFnSc7	konfigurace
od	od	k7c2	od
zákazu	zákaz	k1gInSc2	zákaz
motorů	motor	k1gInPc2	motor
s	s	k7c7	s
turbo	turba	k1gFnSc5	turba
přeplňováním	přeplňování	k1gNnPc3	přeplňování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vývoje	vývoj	k1gInSc2	vývoj
motorů	motor	k1gInPc2	motor
často	často	k6eAd1	často
experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
statistiky	statistika	k1gFnPc1	statistika
jsou	být	k5eAaImIp3nP	být
přesné	přesný	k2eAgFnPc1d1	přesná
a	a	k8xC	a
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejúspěšnějšími	úspěšný	k2eAgInPc7d3	nejúspěšnější
motory	motor	k1gInPc7	motor
jsou	být	k5eAaImIp3nP	být
Renault	renault	k1gInSc4	renault
a	a	k8xC	a
Ferrari	Ferrari	k1gMnSc7	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Renault	renault	k1gInSc1	renault
získal	získat	k5eAaPmAgInS	získat
jako	jako	k9	jako
dodavatel	dodavatel	k1gMnSc1	dodavatel
motorů	motor	k1gInPc2	motor
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
stáje	stáj	k1gFnPc4	stáj
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
celkem	celkem	k6eAd1	celkem
šestkrát	šestkrát	k6eAd1	šestkrát
pohár	pohár	k1gInSc4	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
a	a	k8xC	a
pětkrát	pětkrát	k6eAd1	pětkrát
dovezl	dovézt	k5eAaPmAgMnS	dovézt
pilota	pilot	k1gMnSc4	pilot
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
již	již	k6eAd1	již
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
vozem	vůz	k1gInSc7	vůz
získal	získat	k5eAaPmAgInS	získat
jak	jak	k8xS	jak
titul	titul	k1gInSc1	titul
mezi	mezi	k7c7	mezi
jezdci	jezdec	k1gInPc7	jezdec
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
pohár	pohár	k1gInSc4	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Renault	renault	k1gInSc1	renault
šel	jít	k5eAaImAgInS	jít
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
nasadil	nasadit	k5eAaPmAgMnS	nasadit
desetiválcový	desetiválcový	k2eAgInSc4d1	desetiválcový
motor	motor	k1gInSc4	motor
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
rozevření	rozevření	k1gNnPc2	rozevření
111	[number]	k4	111
<g/>
°	°	k?	°
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
snížit	snížit	k5eAaPmF	snížit
těžiště	těžiště	k1gNnSc4	těžiště
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravený	upravený	k2eAgInSc1d1	upravený
motor	motor	k1gInSc1	motor
používal	používat	k5eAaImAgInS	používat
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
R23	R23	k1gFnSc2	R23
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
hlavní	hlavní	k2eAgMnSc1d1	hlavní
soupeř	soupeř	k1gMnSc1	soupeř
Ferrari	Ferrari	k1gMnSc1	Ferrari
byl	být	k5eAaImAgMnS	být
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
s	s	k7c7	s
klasickým	klasický	k2eAgNnSc7d1	klasické
rozevřením	rozevření	k1gNnSc7	rozevření
válců	válec	k1gInPc2	válec
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Renault	renault	k1gInSc1	renault
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
tradičnějšímu	tradiční	k2eAgNnSc3d2	tradičnější
rozevření	rozevření	k1gNnSc3	rozevření
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
72	[number]	k4	72
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Ferrari	ferrari	k1gNnSc1	ferrari
slavilo	slavit	k5eAaImAgNnS	slavit
úspěch	úspěch	k1gInSc4	úspěch
především	především	k9	především
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získalo	získat	k5eAaPmAgNnS	získat
šest	šest	k4xCc1	šest
pohárů	pohár	k1gInPc2	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
a	a	k8xC	a
pět	pět	k4xCc4	pět
titulů	titul	k1gInPc2	titul
mezi	mezi	k7c7	mezi
jezdci	jezdec	k1gInPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2006	[number]	k4	2006
většina	většina	k1gFnSc1	většina
týmů	tým	k1gInPc2	tým
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
s	s	k7c7	s
osmiválcovými	osmiválcový	k2eAgInPc7d1	osmiválcový
motory	motor	k1gInPc7	motor
podle	podle	k7c2	podle
nových	nový	k2eAgFnPc2d1	nová
regulí	regule	k1gFnPc2	regule
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
získal	získat	k5eAaPmAgInS	získat
pouze	pouze	k6eAd1	pouze
tým	tým	k1gInSc1	tým
Minardi	Minard	k1gMnPc1	Minard
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nesehnal	sehnat	k5eNaPmAgInS	sehnat
dostatek	dostatek	k1gInSc1	dostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
pořízení	pořízení	k1gNnSc4	pořízení
motorů	motor	k1gInPc2	motor
V	v	k7c4	v
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mohl	moct	k5eAaImAgInS	moct
používat	používat	k5eAaImF	používat
omezené	omezený	k2eAgInPc4d1	omezený
motory	motor	k1gInPc4	motor
V	V	kA	V
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Honda	honda	k1gFnSc1	honda
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
představila	představit	k5eAaPmAgFnS	představit
jako	jako	k9	jako
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
když	když	k8xS	když
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
akcie	akcie	k1gFnSc1	akcie
stáje	stáj	k1gFnSc2	stáj
BAR	bar	k1gInSc1	bar
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Sauber	Sauber	k1gMnSc1	Sauber
prodal	prodat	k5eAaPmAgMnS	prodat
svou	svůj	k3xOyFgFnSc4	svůj
stáj	stáj	k1gFnSc4	stáj
německému	německý	k2eAgInSc3d1	německý
koncernu	koncern	k1gInSc3	koncern
BMW	BMW	kA	BMW
a	a	k8xC	a
stáj	stáj	k1gFnSc1	stáj
Jordan	Jordan	k1gMnSc1	Jordan
koupil	koupit	k5eAaPmAgMnS	koupit
ruský	ruský	k2eAgMnSc1d1	ruský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Alex	Alex	k1gMnSc1	Alex
Shnaider	Shnaider	k1gMnSc1	Shnaider
a	a	k8xC	a
tým	tým	k1gInSc1	tým
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Midland	Midland	k1gInSc4	Midland
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
tým	tým	k1gInSc1	tým
prodán	prodán	k2eAgInSc1d1	prodán
nizozemské	nizozemský	k2eAgFnSc3d1	nizozemská
firmě	firma	k1gFnSc3	firma
Spyker	Spykra	k1gFnPc2	Spykra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minardi	Minard	k1gMnPc1	Minard
získal	získat	k5eAaPmAgInS	získat
Red	Red	k1gMnSc1	Red
Bull	bulla	k1gFnPc2	bulla
Racing	Racing	k1gInSc1	Racing
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
majitelem	majitel	k1gMnSc7	majitel
dvou	dva	k4xCgInPc6	dva
stájí	stáj	k1gFnPc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Aguri	Aguri	k6eAd1	Aguri
Suzuki	suzuki	k1gNnSc1	suzuki
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
pilot	pilot	k1gMnSc1	pilot
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgMnS	koupit
ve	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
zbytky	zbytek	k1gInPc4	zbytek
stáje	stáj	k1gFnSc2	stáj
Arrows	Arrowsa	k1gFnPc2	Arrowsa
<g/>
,	,	kIx,	,
zkrachovalé	zkrachovalý	k2eAgInPc1d1	zkrachovalý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
stáj	stáj	k1gFnSc4	stáj
Super	super	k2eAgFnSc2d1	super
Aguri	Agur	k1gFnSc2	Agur
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
formace	formace	k1gFnPc1	formace
přinesly	přinést	k5eAaPmAgFnP	přinést
i	i	k9	i
spoustu	spoustu	k6eAd1	spoustu
dohadů	dohad	k1gInPc2	dohad
<g/>
,	,	kIx,	,
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
polemik	polemika	k1gFnPc2	polemika
především	především	k6eAd1	především
okolo	okolo	k7c2	okolo
používání	používání	k1gNnSc2	používání
totožných	totožný	k2eAgNnPc2d1	totožné
šasi	šasi	k1gNnPc2	šasi
dvěma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2007	[number]	k4	2007
přinesla	přinést	k5eAaPmAgFnS	přinést
další	další	k2eAgFnPc4d1	další
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
největší	veliký	k2eAgFnSc7d3	veliký
bylo	být	k5eAaImAgNnS	být
zmrazení	zmrazení	k1gNnSc1	zmrazení
vývoje	vývoj	k1gInSc2	vývoj
motorů	motor	k1gInPc2	motor
-	-	kIx~	-
zákaz	zákaz	k1gInSc1	zákaz
experimentů	experiment	k1gInPc2	experiment
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
parametry	parametr	k1gInPc7	parametr
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
definují	definovat	k5eAaBmIp3nP	definovat
odlitek	odlitek	k1gInSc4	odlitek
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
klikový	klikový	k2eAgInSc1d1	klikový
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
válců	válec	k1gInPc2	válec
<g/>
,	,	kIx,	,
zdvihový	zdvihový	k2eAgInSc1d1	zdvihový
objem	objem	k1gInSc1	objem
<g/>
,	,	kIx,	,
vrtání	vrtání	k1gNnSc1	vrtání
<g/>
,	,	kIx,	,
zdvih	zdvih	k1gInSc1	zdvih
atd.	atd.	kA	atd.
Naopak	naopak	k6eAd1	naopak
připouští	připouštět	k5eAaImIp3nS	připouštět
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
tvaru	tvar	k1gInSc2	tvar
spalovací	spalovací	k2eAgFnSc2d1	spalovací
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
ventilových	ventilový	k2eAgInPc2d1	ventilový
rozvodů	rozvod	k1gInPc2	rozvod
<g/>
,	,	kIx,	,
sání	sání	k1gNnPc2	sání
nebo	nebo	k8xC	nebo
trysek	tryska	k1gFnPc2	tryska
pro	pro	k7c4	pro
vstřikování	vstřikování	k1gNnSc4	vstřikování
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
vozy	vůz	k1gInPc1	vůz
F1	F1	k1gFnSc2	F1
jsou	být	k5eAaImIp3nP	být
monoposty	monopost	k1gInPc4	monopost
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
uloženým	uložený	k2eAgInSc7d1	uložený
uprostřed	uprostřed	k7c2	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Skořepina	skořepina	k1gFnSc1	skořepina
celého	celý	k2eAgInSc2d1	celý
monopostu	monopost	k1gInSc2	monopost
je	být	k5eAaImIp3nS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
z	z	k7c2	z
kompozitu	kompozitum	k1gNnSc3	kompozitum
uhlíkových	uhlíkový	k2eAgNnPc2d1	uhlíkové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
docílilo	docílit	k5eAaPmAgNnS	docílit
nejnižší	nízký	k2eAgFnSc3d3	nejnižší
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vůz	vůz	k1gInSc4	vůz
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
posouváním	posouvání	k1gNnSc7	posouvání
těžiště	těžiště	k1gNnSc2	těžiště
a	a	k8xC	a
vyvažovacích	vyvažovací	k2eAgNnPc2d1	vyvažovací
závaží	závaží	k1gNnPc2	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
a	a	k8xC	a
palivem	palivo	k1gNnSc7	palivo
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
po	po	k7c6	po
závodě	závod	k1gInSc6	závod
méně	málo	k6eAd2	málo
než	než	k8xS	než
620	[number]	k4	620
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
F1	F1	k1gFnSc2	F1
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
projíždět	projíždět	k5eAaImF	projíždět
zatáčky	zatáčka	k1gFnPc4	zatáčka
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
aerodynamickému	aerodynamický	k2eAgInSc3d1	aerodynamický
přítlaku	přítlak	k1gInSc3	přítlak
<g/>
.	.	kIx.	.
</s>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
přítlak	přítlak	k1gInSc1	přítlak
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
obráceného	obrácený	k2eAgNnSc2d1	obrácené
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
používají	používat	k5eAaImIp3nP	používat
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
objevila	objevit	k5eAaPmAgNnP	objevit
křídla	křídlo	k1gNnPc4	křídlo
s	s	k7c7	s
obráceným	obrácený	k2eAgInSc7d1	obrácený
efektem	efekt	k1gInSc7	efekt
než	než	k8xS	než
u	u	k7c2	u
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
obrátí	obrátit	k5eAaPmIp3nS	obrátit
i	i	k9	i
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tlačí	tlačit	k5eAaImIp3nS	tlačit
letadlo	letadlo	k1gNnSc4	letadlo
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
přitlačovat	přitlačovat	k5eAaImF	přitlačovat
auto	auto	k1gNnSc4	auto
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
proudící	proudící	k2eAgInSc1d1	proudící
pod	pod	k7c7	pod
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
než	než	k8xS	než
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proudí	proudit	k5eAaPmIp3nS	proudit
nad	nad	k7c7	nad
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
toho	ten	k3xDgMnSc4	ten
docílilo	docílit	k5eAaPmAgNnS	docílit
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
křídlo	křídlo	k1gNnSc1	křídlo
zkonstruováno	zkonstruovat	k5eAaPmNgNnS	zkonstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
křídla	křídlo	k1gNnSc2	křídlo
tvořila	tvořit	k5eAaImAgFnS	tvořit
větší	veliký	k2eAgFnSc1d2	veliký
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
horní	horní	k2eAgFnSc1d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
a	a	k8xC	a
plynulý	plynulý	k2eAgInSc4d1	plynulý
průjezd	průjezd	k1gInSc4	průjezd
zatáčkou	zatáčka	k1gFnSc7	zatáčka
jsou	být	k5eAaImIp3nP	být
pneumatiky	pneumatika	k1gFnPc1	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
nepoužívaly	používat	k5eNaImAgFnP	používat
hladké	hladký	k2eAgFnPc1d1	hladká
pneumatiky	pneumatika	k1gFnPc1	pneumatika
"	"	kIx"	"
<g/>
slicky	slicka	k1gFnPc1	slicka
<g/>
"	"	kIx"	"
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
automobilových	automobilový	k2eAgFnPc6d1	automobilová
sériích	série	k1gFnPc6	série
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pneumatiky	pneumatika	k1gFnPc1	pneumatika
s	s	k7c7	s
drážkami	drážka	k1gFnPc7	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
2009	[number]	k4	2009
však	však	k8xC	však
pravidla	pravidlo	k1gNnPc4	pravidlo
doznala	doznat	k5eAaPmAgFnS	doznat
mnoho	mnoho	k4c4	mnoho
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc4d1	dovoleno
hladké	hladký	k2eAgFnPc4d1	hladká
pneumatiky	pneumatika	k1gFnPc4	pneumatika
opět	opět	k6eAd1	opět
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
karbonové	karbonový	k2eAgFnPc1d1	karbonová
brzdy	brzda	k1gFnPc1	brzda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jednak	jednak	k8xC	jednak
lehčí	lehčit	k5eAaImIp3nP	lehčit
a	a	k8xC	a
pak	pak	k6eAd1	pak
účinnější	účinný	k2eAgMnSc1d2	účinnější
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
komponenty	komponenta	k1gFnPc1	komponenta
vhodně	vhodně	k6eAd1	vhodně
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jízdní	jízdní	k2eAgFnPc4d1	jízdní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Motory	motor	k1gInPc4	motor
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
sezonách	sezona	k1gFnPc6	sezona
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
jsou	být	k5eAaImIp3nP	být
osmiválcové	osmiválcový	k2eAgInPc1d1	osmiválcový
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
rozevření	rozevření	k1gNnPc2	rozevření
90	[number]	k4	90
<g/>
°	°	k?	°
o	o	k7c6	o
maximálním	maximální	k2eAgInSc6d1	maximální
objemu	objem	k1gInSc6	objem
2,4	[number]	k4	2,4
l.	l.	k?	l.
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
750	[number]	k4	750
koní	kůň	k1gMnPc2	kůň
při	při	k7c6	při
limitovaných	limitovaný	k2eAgInPc6d1	limitovaný
18	[number]	k4	18
000	[number]	k4	000
otáčkách	otáčka	k1gFnPc6	otáčka
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Palivo	palivo	k1gNnSc1	palivo
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
motorech	motor	k1gInPc6	motor
F1	F1	k1gFnSc2	F1
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgNnSc7	svůj
složením	složení	k1gNnSc7	složení
téměř	téměř	k6eAd1	téměř
totožné	totožný	k2eAgNnSc4d1	totožné
jako	jako	k8xC	jako
běžné	běžný	k2eAgNnSc4d1	běžné
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
komerční	komerční	k2eAgNnSc4d1	komerční
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předešlých	předešlý	k2eAgNnPc6d1	předešlé
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používaly	používat	k5eAaImAgFnP	používat
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
směsi	směs	k1gFnPc1	směs
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
metanolu	metanol	k1gInSc2	metanol
nebo	nebo	k8xC	nebo
směs	směs	k1gFnSc4	směs
benzínu	benzín	k1gInSc2	benzín
a	a	k8xC	a
toluenu	toluen	k1gInSc2	toluen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokootáčkovém	vysokootáčkový	k2eAgInSc6d1	vysokootáčkový
motoru	motor	k1gInSc6	motor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgInSc1d1	speciální
olej	olej	k1gInSc1	olej
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
viskozitou	viskozita	k1gFnSc7	viskozita
jakou	jaký	k3yIgFnSc4	jaký
má	můj	k3xOp1gFnSc1	můj
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
používají	používat	k5eAaImIp3nP	používat
šestiválcové	šestiválcový	k2eAgInPc4d1	šestiválcový
motory	motor	k1gInPc4	motor
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
1,6	[number]	k4	1,6
l.	l.	k?	l.
s	s	k7c7	s
turbodmychadlem	turbodmychadlo	k1gNnSc7	turbodmychadlo
a	a	k8xC	a
ERS	ERS	kA	ERS
<g/>
.	.	kIx.	.
</s>
<s>
Převodovka	převodovka	k1gFnSc1	převodovka
je	být	k5eAaImIp3nS	být
připevněna	připevnit	k5eAaPmNgFnS	připevnit
k	k	k7c3	k
zadní	zadní	k2eAgFnSc3d1	zadní
části	část	k1gFnSc3	část
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přenášet	přenášet	k5eAaImF	přenášet
výkon	výkon	k1gInSc4	výkon
motoru	motor	k1gInSc2	motor
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
co	co	k9	co
nejhladším	hladký	k2eAgInSc7d3	nejhladší
a	a	k8xC	a
nejefektivnějším	efektivní	k2eAgInSc7d3	nejefektivnější
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
vozy	vůz	k1gInPc1	vůz
F1	F1	k1gFnSc2	F1
mají	mít	k5eAaImIp3nP	mít
osmirychlostní	osmirychlostní	k2eAgFnPc4d1	osmirychlostní
převodovky	převodovka	k1gFnPc4	převodovka
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byly	být	k5eAaImAgFnP	být
převodovky	převodovka	k1gFnPc1	převodovka
sedmirychlostní	sedmirychlostní	k2eAgFnPc1d1	sedmirychlostní
<g/>
)	)	kIx)	)
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
ovládání	ovládání	k1gNnSc1	ovládání
je	být	k5eAaImIp3nS	být
polosamočinné	polosamočinný	k2eAgNnSc1d1	polosamočinné
pomocí	pomocí	k7c2	pomocí
elektrohydraulického	elektrohydraulický	k2eAgInSc2d1	elektrohydraulický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
samočinné	samočinný	k2eAgFnPc4d1	samočinná
převodovky	převodovka	k1gFnPc4	převodovka
i	i	k8xC	i
rychlé	rychlý	k2eAgInPc4d1	rychlý
systémy	systém	k1gInPc4	systém
dvojspojkových	dvojspojkový	k2eAgFnPc2d1	dvojspojková
převodovek	převodovka	k1gFnPc2	převodovka
DSG	DSG	kA	DSG
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
převodového	převodový	k2eAgInSc2d1	převodový
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
jen	jen	k9	jen
po	po	k7c6	po
přímém	přímý	k2eAgInSc6d1	přímý
pokynu	pokyn	k1gInSc6	pokyn
od	od	k7c2	od
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Revolucí	revoluce	k1gFnSc7	revoluce
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
bezprodlevového	bezprodlevový	k2eAgNnSc2d1	bezprodlevový
řazení	řazení	k1gNnSc2	řazení
nebo	nebo	k8xC	nebo
také	také	k9	také
plynulého	plynulý	k2eAgNnSc2d1	plynulé
řazení	řazení	k1gNnSc2	řazení
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
plynulou	plynulý	k2eAgFnSc4d1	plynulá
změnu	změna	k1gFnSc4	změna
převodových	převodový	k2eAgInPc2d1	převodový
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
převodovek	převodovka	k1gFnPc2	převodovka
s	s	k7c7	s
variátorem	variátor	k1gInSc7	variátor
(	(	kIx(	(
<g/>
VGT	VGT	kA	VGT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
zakázané	zakázaný	k2eAgInPc1d1	zakázaný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
převodovky	převodovka	k1gFnPc4	převodovka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
změnu	změna	k1gFnSc4	změna
rychlostního	rychlostní	k2eAgInSc2d1	rychlostní
stupně	stupeň	k1gInSc2	stupeň
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
spojení	spojení	k1gNnSc2	spojení
motoru	motor	k1gInSc2	motor
s	s	k7c7	s
koly	kolo	k1gNnPc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
řazení	řazení	k1gNnSc1	řazení
má	mít	k5eAaImIp3nS	mít
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
rázy	ráz	k1gInPc4	ráz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zatěžovaly	zatěžovat	k5eAaImAgFnP	zatěžovat
jak	jak	k6eAd1	jak
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
řazení	řazení	k1gNnSc2	řazení
musí	muset	k5eAaImIp3nS	muset
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
namáháním	namáhání	k1gNnSc7	namáhání
a	a	k8xC	a
větším	veliký	k2eAgNnSc7d2	veliký
namáháním	namáhání	k1gNnSc7	namáhání
elektronických	elektronický	k2eAgInPc2d1	elektronický
i	i	k8xC	i
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
zlomku	zlomek	k1gInSc6	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
zařazeny	zařadit	k5eAaPmNgInP	zařadit
dva	dva	k4xCgInPc1	dva
rychlostní	rychlostní	k2eAgInPc1d1	rychlostní
stupně	stupeň	k1gInPc1	stupeň
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
řazení	řazení	k1gNnSc2	řazení
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
osvědčuje	osvědčovat	k5eAaImIp3nS	osvědčovat
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
úsporu	úspora	k1gFnSc4	úspora
až	až	k6eAd1	až
0,3	[number]	k4	0,3
<g/>
s	s	k7c7	s
na	na	k7c6	na
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1	aktuální
monoposty	monopost	k1gInPc1	monopost
F1	F1	k1gFnSc2	F1
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rychlosti	rychlost	k1gFnPc4	rychlost
přes	přes	k7c4	přes
350	[number]	k4	350
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
okruhem	okruh	k1gInSc7	okruh
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
seriálu	seriál	k1gInSc6	seriál
je	být	k5eAaImIp3nS	být
Monza	Monza	k1gFnSc1	Monza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Honda	honda	k1gFnSc1	honda
podrobila	podrobit	k5eAaPmAgFnS	podrobit
svůj	svůj	k3xOyFgInSc4	svůj
monopost	monopost	k1gInSc4	monopost
rychlostnímu	rychlostní	k2eAgMnSc3d1	rychlostní
testu	test	k1gMnSc3	test
v	v	k7c6	v
Bonneville	Bonnevilla	k1gFnSc6	Bonnevilla
Speedway	Speedwaa	k1gFnSc2	Speedwaa
a	a	k8xC	a
v	v	k7c6	v
Poušti	poušť	k1gFnSc6	poušť
Mojave	Mojav	k1gInSc5	Mojav
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
pravidlům	pravidlo	k1gNnPc3	pravidlo
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
překonali	překonat	k5eAaPmAgMnP	překonat
rychlost	rychlost	k1gFnSc4	rychlost
416	[number]	k4	416
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Ovšem	ovšem	k9	ovšem
během	během	k7c2	během
závodů	závod	k1gInPc2	závod
je	být	k5eAaImIp3nS	být
max	max	kA	max
<g/>
.	.	kIx.	.
rychlost	rychlost	k1gFnSc1	rychlost
kolem	kolem	k7c2	kolem
318	[number]	k4	318
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Počet	počet	k1gInSc1	počet
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezónách	sezóna	k1gFnPc6	sezóna
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
od	od	k7c2	od
7	[number]	k4	7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
až	až	k6eAd1	až
po	po	k7c4	po
19	[number]	k4	19
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
až	až	k9	až
22	[number]	k4	22
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
měl	mít	k5eAaImAgMnS	mít
fanoušek	fanoušek	k1gMnSc1	fanoušek
tohoto	tento	k3xDgInSc2	tento
motoristického	motoristický	k2eAgInSc2d1	motoristický
sportu	sport	k1gInSc2	sport
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
příležitostí	příležitost	k1gFnSc7	příležitost
spatřit	spatřit	k5eAaPmF	spatřit
zavodit	zavodit	k5eAaBmF	zavodit
své	svůj	k3xOyFgInPc4	svůj
idoly	idol	k1gInPc4	idol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
7	[number]	k4	7
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statutem	statut	k1gInSc7	statut
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
výsledek	výsledek	k1gInSc4	výsledek
započítávat	započítávat	k5eAaImF	započítávat
do	do	k7c2	do
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
jelo	jet	k5eAaImAgNnS	jet
dalších	další	k2eAgNnPc2d1	další
16	[number]	k4	16
závodů	závod	k1gInPc2	závod
vypsaných	vypsaný	k2eAgInPc2d1	vypsaný
pro	pro	k7c4	pro
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
11	[number]	k4	11
závodů	závod	k1gInPc2	závod
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
série	série	k1gFnSc2	série
Formule	formule	k1gFnSc1	formule
Libre	Libr	k1gMnSc5	Libr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startovali	startovat	k5eAaBmAgMnP	startovat
i	i	k9	i
Alberto	Alberta	k1gFnSc5	Alberta
Ascari	Ascar	k1gFnSc5	Ascar
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Manuel	Manuel	k1gMnSc1	Manuel
Fangio	Fangio	k1gMnSc1	Fangio
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gInSc5	Giusepp
Farina	Farin	k2eAgMnSc4d1	Farin
<g/>
,	,	kIx,	,
Luigi	Luige	k1gFnSc4	Luige
Fagioli	Fagiole	k1gFnSc4	Fagiole
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
až	až	k9	až
s	s	k7c7	s
podivem	podiv	k1gInSc7	podiv
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
10	[number]	k4	10
závodů	závod	k1gInPc2	závod
pro	pro	k7c4	pro
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
jelo	jet	k5eAaImAgNnS	jet
i	i	k9	i
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
34	[number]	k4	34
závodů	závod	k1gInPc2	závod
mimo	mimo	k7c4	mimo
šampionát	šampionát	k1gInSc4	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
počet	počet	k1gInSc4	počet
GP	GP	kA	GP
započítávaných	započítávaný	k2eAgInPc2d1	započítávaný
do	do	k7c2	do
MS	MS	kA	MS
a	a	k8xC	a
opadával	opadávat	k5eAaImAgInS	opadávat
počet	počet	k1gInSc1	počet
závodů	závod	k1gInPc2	závod
bez	bez	k7c2	bez
statutu	statut	k1gInSc2	statut
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ale	ale	k9	ale
mistrovství	mistrovství	k1gNnSc4	mistrovství
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Tasmánský	tasmánský	k2eAgInSc4d1	tasmánský
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jezdilo	jezdit	k5eAaImAgNnS	jezdit
jen	jen	k9	jen
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
se	s	k7c7	s
statutem	statut	k1gInSc7	statut
MS	MS	kA	MS
plus	plus	k1gInSc1	plus
v	v	k7c6	v
závodu	závod	k1gInSc6	závod
šampiónů	šampión	k1gMnPc2	šampión
v	v	k7c4	v
Brands	Brands	k1gInSc4	Brands
Hatch	Hatcha	k1gFnPc2	Hatcha
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
závodů	závod	k1gInPc2	závod
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
mimoevropským	mimoevropský	k2eAgInSc7d1	mimoevropský
závodem	závod	k1gInSc7	závod
bylo	být	k5eAaImAgNnS	být
500	[number]	k4	500
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
Indianapolis	Indianapolis	k1gFnPc6	Indianapolis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
MS	MS	kA	MS
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
mostem	most	k1gInSc7	most
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
naprosto	naprosto	k6eAd1	naprosto
odlišnými	odlišný	k2eAgInPc7d1	odlišný
světy	svět	k1gInPc7	svět
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
mentalitou	mentalita	k1gFnSc7	mentalita
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
odlišným	odlišný	k2eAgNnSc7d1	odlišné
technickým	technický	k2eAgNnSc7d1	technické
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zájem	zájem	k1gInSc1	zájem
amerických	americký	k2eAgMnPc2d1	americký
pilotů	pilot	k1gMnPc2	pilot
o	o	k7c4	o
start	start	k1gInSc4	start
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
byl	být	k5eAaImAgMnS	být
nulový	nulový	k2eAgMnSc1d1	nulový
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
Evropanů	Evropan	k1gMnPc2	Evropan
o	o	k7c4	o
500	[number]	k4	500
mil	míle	k1gFnPc2	míle
byly	být	k5eAaImAgFnP	být
sporadické	sporadický	k2eAgFnPc1d1	sporadická
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
zařazena	zařadit	k5eAaPmNgFnS	zařadit
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
společně	společně	k6eAd1	společně
se	s	k7c7	s
závodem	závod	k1gInSc7	závod
500	[number]	k4	500
mil	míle	k1gFnPc2	míle
v	v	k7c6	v
Indianapolis	Indianapolis	k1gFnSc6	Indianapolis
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
nevyprovokoval	vyprovokovat	k5eNaPmAgInS	vyprovokovat
u	u	k7c2	u
Američanů	Američan	k1gMnPc2	Američan
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
jezdí	jezdit	k5eAaImIp3nS	jezdit
Grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
USA	USA	kA	USA
jen	jen	k9	jen
pro	pro	k7c4	pro
vozy	vůz	k1gInPc4	vůz
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
hostila	hostit	k5eAaImAgFnS	hostit
závody	závod	k1gInPc1	závod
formule	formule	k1gFnSc1	formule
1	[number]	k4	1
na	na	k7c6	na
jihoamerickém	jihoamerický	k2eAgInSc6d1	jihoamerický
kontinentu	kontinent	k1gInSc6	kontinent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
pak	pak	k8xC	pak
první	první	k4xOgInSc4	první
na	na	k7c6	na
africké	africký	k2eAgFnSc6d1	africká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
závodem	závod	k1gInSc7	závod
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
do	do	k7c2	do
Oceánie	Oceánie	k1gFnSc2	Oceánie
zavítal	zavítat	k5eAaPmAgInS	zavítat
kolotoč	kolotoč	k1gInSc1	kolotoč
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
australskou	australský	k2eAgFnSc4d1	australská
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
GP	GP	kA	GP
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
i	i	k8xC	i
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Oceánie	Oceánie	k1gFnSc1	Oceánie
<g/>
,	,	kIx,	,
schází	scházet	k5eAaImIp3nS	scházet
jen	jen	k9	jen
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
může	moct	k5eAaImIp3nS	moct
každá	každý	k3xTgFnSc1	každý
země	zem	k1gFnSc2	zem
uspořádat	uspořádat	k5eAaPmF	uspořádat
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
zemi	zem	k1gFnSc6	zem
koná	konat	k5eAaImIp3nS	konat
více	hodně	k6eAd2	hodně
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
po	po	k7c6	po
dvou	dva	k4xCgFnPc2	dva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
druhá	druhý	k4xOgFnSc1	druhý
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
pojmenována	pojmenován	k2eAgNnPc4d1	pojmenováno
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
v	v	k7c6	v
Monze	Monza	k1gFnSc6	Monza
jezdila	jezdit	k5eAaImAgFnS	jezdit
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
v	v	k7c6	v
Imole	Imol	k1gInSc6	Imol
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
San	San	k1gMnSc1	San
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Monza	Monza	k1gFnSc1	Monza
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
okruhem	okruh	k1gInSc7	okruh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hostí	hostit	k5eAaImIp3nS	hostit
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
chyběl	chybět	k5eAaImAgInS	chybět
jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc7d1	současná
snahou	snaha	k1gFnSc7	snaha
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
pořádaly	pořádat	k5eAaImAgInP	pořádat
jen	jen	k6eAd1	jen
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
střídaly	střídat	k5eAaImAgInP	střídat
různé	různý	k2eAgInPc1d1	různý
okruhy	okruh	k1gInPc1	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
v	v	k7c6	v
Monze	Monza	k1gFnSc6	Monza
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
zmodernizovaném	zmodernizovaný	k2eAgInSc6d1	zmodernizovaný
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
<g/>
.	.	kIx.	.
</s>
<s>
Nürburgring	Nürburgring	k1gInSc1	Nürburgring
a	a	k8xC	a
Hockenheim	Hockenheim	k1gInSc1	Hockenheim
budou	být	k5eAaImBp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
alternovat	alternovat	k5eAaImF	alternovat
v	v	k7c4	v
pořádání	pořádání	k1gNnSc4	pořádání
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nových	nový	k2eAgInPc2d1	nový
závodů	závod	k1gInPc2	závod
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
prvním	první	k4xOgInSc7	první
závodem	závod	k1gInSc7	závod
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Turecka	Turecko	k1gNnSc2	Turecko
prezentuji	prezentovat	k5eAaBmIp1nS	prezentovat
nový	nový	k2eAgInSc4d1	nový
směr	směr	k1gInSc4	směr
evoluce	evoluce	k1gFnSc2	evoluce
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přibyla	přibýt	k5eAaPmAgFnS	přibýt
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pojede	pojet	k5eAaPmIp3nS	pojet
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
,	,	kIx,	,
a	a	k8xC	a
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
i	i	k9	i
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
-Ring	-Ringa	k1gFnPc2	-Ringa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
po	po	k7c6	po
světě	svět	k1gInSc6	svět
konají	konat	k5eAaImIp3nP	konat
následující	následující	k2eAgMnSc1d1	následující
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
masivní	masivní	k2eAgInSc4d1	masivní
vstup	vstup	k1gInSc4	vstup
sponzorů	sponzor	k1gMnPc2	sponzor
do	do	k7c2	do
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
závodily	závodit	k5eAaImAgInP	závodit
v	v	k7c6	v
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
kapotách	kapota	k1gFnPc6	kapota
se	se	k3xPyFc4	se
sponzorské	sponzorský	k2eAgInPc1d1	sponzorský
nápisy	nápis	k1gInPc1	nápis
objevovaly	objevovat	k5eAaImAgInP	objevovat
spíše	spíše	k9	spíše
sporadicky	sporadicky	k6eAd1	sporadicky
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
dodavatele	dodavatel	k1gMnPc4	dodavatel
komponentů	komponent	k1gInPc2	komponent
(	(	kIx(	(
<g/>
pneumatiky	pneumatika	k1gFnPc1	pneumatika
<g/>
,	,	kIx,	,
palivo	palivo	k1gNnSc1	palivo
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
přišel	přijít	k5eAaPmAgMnS	přijít
Lotus	Lotus	kA	Lotus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
tým	tým	k1gInSc1	tým
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
svého	své	k1gNnSc2	své
sponzora	sponzor	k1gMnSc2	sponzor
(	(	kIx(	(
<g/>
červeno	červeno	k1gNnSc1	červeno
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Imperial	Imperial	k1gMnSc1	Imperial
Tobacco	Tobacco	k1gMnSc1	Tobacco
<g/>
"	"	kIx"	"
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
ve	v	k7c4	v
známé	známý	k2eAgNnSc4d1	známé
zlato	zlato	k1gNnSc4	zlato
černé	černá	k1gFnSc2	černá
kombinaci	kombinace	k1gFnSc4	kombinace
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
zbarvení	zbarvení	k1gNnSc1	zbarvení
tak	tak	k9	tak
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
British	British	k1gInSc1	British
racing	racing	k1gInSc1	racing
green	green	k1gInSc1	green
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
jezdily	jezdit	k5eAaImAgInP	jezdit
britské	britský	k2eAgInPc1d1	britský
vozy	vůz	k1gInPc1	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
okruhy	okruh	k1gInPc1	okruh
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
názvy	název	k1gInPc1	název
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
byly	být	k5eAaImAgFnP	být
ovlivněné	ovlivněný	k2eAgMnPc4d1	ovlivněný
mohutným	mohutný	k2eAgInSc7d1	mohutný
nástupem	nástup	k1gInSc7	nástup
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
tabákového	tabákový	k2eAgInSc2d1	tabákový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přijímáním	přijímání	k1gNnSc7	přijímání
legislativy	legislativa	k1gFnSc2	legislativa
zakazující	zakazující	k2eAgFnSc2d1	zakazující
propagaci	propagace	k1gFnSc4	propagace
kouření	kouření	k1gNnSc2	kouření
začínají	začínat	k5eAaImIp3nP	začínat
do	do	k7c2	do
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
proudit	proudit	k5eAaPmF	proudit
prostředky	prostředek	k1gInPc4	prostředek
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
pomalu	pomalu	k6eAd1	pomalu
začínají	začínat	k5eAaImIp3nP	začínat
eliminovat	eliminovat	k5eAaBmF	eliminovat
nápisy	nápis	k1gInPc4	nápis
propagující	propagující	k2eAgInPc4d1	propagující
tabákové	tabákový	k2eAgInPc4d1	tabákový
výrobky	výrobek	k1gInPc4	výrobek
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stáj	stáj	k1gFnSc1	stáj
Williams	Williamsa	k1gFnPc2	Williamsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
následována	následován	k2eAgFnSc1d1	následována
vozy	vůz	k1gInPc4	vůz
McLaren	McLarno	k1gNnPc2	McLarno
a	a	k8xC	a
Ferrari	ferrari	k1gNnPc2	ferrari
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
pilota	pilot	k1gMnSc4	pilot
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
italského	italský	k2eAgMnSc2d1	italský
jezdce	jezdec	k1gMnSc2	jezdec
Andrey	Andrea	k1gFnSc2	Andrea
de	de	k?	de
Adamiche	Adamich	k1gMnSc2	Adamich
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
malá	malý	k2eAgFnSc1d1	malá
keramická	keramický	k2eAgFnSc1d1	keramická
dílna	dílna	k1gFnSc1	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
Ital	Ital	k1gMnSc1	Ital
<g/>
,	,	kIx,	,
Vittorio	Vittorio	k1gMnSc1	Vittorio
Brambilla	Brambilla	k1gMnSc1	Brambilla
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
soukromou	soukromý	k2eAgFnSc7d1	soukromá
firmou	firma	k1gFnSc7	firma
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
rodiště	rodiště	k1gNnSc2	rodiště
<g/>
,	,	kIx,	,
Monzy	Monza	k1gFnSc2	Monza
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Wilson	Wilson	k1gInSc1	Wilson
Fittipaldi	Fittipald	k1gMnPc1	Fittipald
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
sponzorovi	sponzor	k1gMnSc6	sponzor
Copersucar	Copersucar	k1gInSc4	Copersucar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
brazilská	brazilský	k2eAgFnSc1d1	brazilská
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
začátkem	začátkem	k7c2	začátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Niki	Nik	k1gFnSc2	Nik
Lauda	Lauda	k1gMnSc1	Lauda
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
svou	svůj	k3xOyFgFnSc4	svůj
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
značku	značka	k1gFnSc4	značka
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
Mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
jezdců	jezdec	k1gInPc2	jezdec
F1	F1	k1gFnSc2	F1
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
konstruktérů	konstruktér	k1gMnPc2	konstruktér
F1	F1	k1gFnSc2	F1
Seznam	seznam	k1gInSc1	seznam
jezdců	jezdec	k1gMnPc2	jezdec
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Seznam	seznam	k1gInSc4	seznam
jezdců	jezdec	k1gMnPc2	jezdec
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
dle	dle	k7c2	dle
národnosti	národnost	k1gFnSc2	národnost
Seznam	seznam	k1gInSc1	seznam
konstruktérů	konstruktér	k1gMnPc2	konstruktér
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Organizace	organizace	k1gFnSc2	organizace
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
-	-	kIx~	-
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
organizuje	organizovat	k5eAaBmIp3nS	organizovat
závody	závod	k1gInPc4	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Jean	Jean	k1gMnSc1	Jean
Todt	Todt	k1gMnSc1	Todt
<g/>
.	.	kIx.	.
</s>
<s>
Formula	Formula	k1gFnSc1	Formula
One	One	k1gMnSc1	One
Management	management	k1gInSc1	management
-	-	kIx~	-
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Bernie	Bernie	k1gFnSc1	Bernie
Ecclestone	Eccleston	k1gInSc5	Eccleston
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Manufacturers	Manufacturers	k1gInSc1	Manufacturers
Association	Association	k1gInSc1	Association
-	-	kIx~	-
asociace	asociace	k1gFnSc1	asociace
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
World	World	k1gInSc1	World
Championship	Championship	k1gInSc1	Championship
-	-	kIx~	-
alternativní	alternativní	k2eAgNnSc1d1	alternativní
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nezačalo	začít	k5eNaPmAgNnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnSc2d1	americká
série	série	k1gFnSc2	série
IndyCar	IndyCar	k1gMnSc1	IndyCar
Series	Series	k1gMnSc1	Series
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1996	[number]	k4	1996
Indy	Indus	k1gInPc4	Indus
Lights	Lights	k1gInSc1	Lights
-	-	kIx~	-
CART	CART	kA	CART
American	American	k1gInSc1	American
Racing	Racing	k1gInSc1	Racing
Series	Series	k1gInSc1	Series
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CART	CART	kA	CART
Indy	Indus	k1gInPc1	Indus
Lights	Lightsa	k1gFnPc2	Lightsa
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
IRL	IRL	kA	IRL
Pro	pro	k7c4	pro
Series	Series	k1gInSc4	Series
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
IRL	IRL	kA	IRL
Indy	Indus	k1gInPc7	Indus
Pro	pro	k7c4	pro
Series	Series	k1gInSc4	Series
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
IRL	IRL	kA	IRL
Indy	Indus	k1gInPc7	Indus
Lights	Lightsa	k1gFnPc2	Lightsa
od	od	k7c2	od
2008	[number]	k4	2008
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
Champ	Champ	k1gMnSc1	Champ
Car	car	k1gMnSc1	car
-	-	kIx~	-
CART	CART	kA	CART
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Champ	Champ	k1gMnSc1	Champ
Car	car	k1gMnSc1	car
World	World	k1gMnSc1	World
Series	Series	k1gMnSc1	Series
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Předválečné	předválečný	k2eAgInPc1d1	předválečný
závody	závod	k1gInPc1	závod
Formule	formule	k1gFnSc2	formule
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1921	[number]	k4	1921
do	do	k7c2	do
1945	[number]	k4	1945
Formule	formule	k1gFnSc2	formule
Libre	Libr	k1gInSc5	Libr
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1928	[number]	k4	1928
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
Nižší	nízký	k2eAgFnSc1d2	nižší
série	série	k1gFnSc1	série
Formule	formule	k1gFnSc1	formule
2	[number]	k4	2
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1948	[number]	k4	1948
do	do	k7c2	do
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
Formule	formule	k1gFnSc1	formule
3000	[number]	k4	3000
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1985	[number]	k4	1985
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
2004	[number]	k4	2004
GP2	GP2	k1gFnPc2	GP2
-	-	kIx~	-
aktivní	aktivní	k2eAgFnSc1d1	aktivní
od	od	k7c2	od
2005	[number]	k4	2005
GP3	GP3	k1gFnPc2	GP3
-	-	kIx~	-
aktivní	aktivní	k2eAgFnSc1d1	aktivní
od	od	k7c2	od
2010	[number]	k4	2010
A1	A1	k1gFnPc2	A1
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
Pohár	pohár	k1gInSc1	pohár
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
od	od	k7c2	od
2005	[number]	k4	2005
do	do	k7c2	do
2009	[number]	k4	2009
Formule	formule	k1gFnSc1	formule
3	[number]	k4	3
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1950	[number]	k4	1950
Formule	formule	k1gFnSc1	formule
Junior	junior	k1gMnSc1	junior
-	-	kIx~	-
aktivní	aktivní	k2eAgMnSc1d1	aktivní
od	od	k7c2	od
1958	[number]	k4	1958
do	do	k7c2	do
1964	[number]	k4	1964
World	Worlda	k1gFnPc2	Worlda
Series	Seriesa	k1gFnPc2	Seriesa
by	by	k9	by
Renault	renault	k1gInSc1	renault
-	-	kIx~	-
Formule	formule	k1gFnSc1	formule
Nissan	nissan	k1gInSc1	nissan
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
World	World	k1gMnSc1	World
Series	Series	k1gMnSc1	Series
by	by	kYmCp3nS	by
Nissan	nissan	k1gInSc1	nissan
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
World	World	k1gMnSc1	World
Series	Series	k1gMnSc1	Series
by	by	kYmCp3nS	by
Renault	renault	k1gInSc4	renault
od	od	k7c2	od
2005	[number]	k4	2005
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xS	jako
Formule	formule	k1gFnSc1	formule
Renault	renault	k1gInSc1	renault
3.5	[number]	k4	3.5
Formule	formule	k1gFnSc2	formule
Nippon	Nippona	k1gFnPc2	Nippona
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
mistrovství	mistrovství	k1gNnSc1	mistrovství
Japonska	Japonsko	k1gNnSc2	Japonsko
pro	pro	k7c4	pro
vozy	vůz	k1gInPc4	vůz
Formule	formule	k1gFnSc2	formule
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Formule	formule	k1gFnSc1	formule
2	[number]	k4	2
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Formule	formule	k1gFnSc1	formule
3000	[number]	k4	3000
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
od	od	k7c2	od
1996	[number]	k4	1996
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Eurocup	Eurocup	k1gMnSc1	Eurocup
Formule	formule	k1gFnSc2	formule
Renault	renault	k1gInSc1	renault
2.0	[number]	k4	2.0
-	-	kIx~	-
Rencontres	Rencontres	k1gMnSc1	Rencontres
Internationales	Internationales	k1gMnSc1	Internationales
de	de	k?	de
Formule	formule	k1gFnSc1	formule
Renault	renault	k1gInSc1	renault
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eurocup	Eurocup	k1gMnSc1	Eurocup
Formule	formule	k1gFnSc2	formule
Renault	renault	k1gInSc1	renault
2.0	[number]	k4	2.0
od	od	k7c2	od
1993	[number]	k4	1993
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Formula	Formula	k1gFnSc1	Formula
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Současná	současný	k2eAgNnPc4d1	současné
pravidla	pravidlo	k1gNnPc4	pravidlo
na	na	k7c6	na
webu	web	k1gInSc6	web
FIA	FIA	kA	FIA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
grandprix	grandprix	k1gInSc1	grandprix
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
f	f	k?	f
<g/>
1	[number]	k4	1
<g/>
sports	sports	k1gInSc1	sports
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
o	o	k7c4	o
F1	F1	k1gFnSc4	F1
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
gpf	gpf	k?	gpf
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
o	o	k7c4	o
F1	F1	k1gFnSc4	F1
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
