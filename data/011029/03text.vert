<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Akademie	akademie	k1gFnSc1	akademie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
instituce	instituce	k1gFnPc4	instituce
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
která	který	k3yRgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
vysoké	vysoký	k2eAgNnSc4d1	vysoké
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k8xS	jako
Vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
institut	institut	k1gInSc4	institut
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
tři	tři	k4xCgFnPc1	tři
umělecké	umělecký	k2eAgFnPc1d1	umělecká
instituce	instituce	k1gFnPc1	instituce
<g/>
:	:	kIx,	:
Tiranská	tiranský	k2eAgFnSc1d1	Tiranská
státní	státní	k2eAgFnSc1d1	státní
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
herectví	herectví	k1gNnSc1	herectví
Aleksandra	Aleksandr	k1gMnSc2	Aleksandr
Moisiua	Moisiuus	k1gMnSc2	Moisiuus
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
umění	umění	k1gNnSc2	umění
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
využít	využít	k5eAaPmF	využít
ruské	ruský	k2eAgFnSc2d1	ruská
tradice	tradice	k1gFnSc2	tradice
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
muzice	muzika	k1gFnSc6	muzika
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
Albánie	Albánie	k1gFnPc1	Albánie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
období	období	k1gNnSc6	období
komunismu	komunismus	k1gInSc2	komunismus
blízké	blízký	k2eAgInPc1d1	blízký
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
<g/>
Škola	škola	k1gFnSc1	škola
stále	stále	k6eAd1	stále
udržuje	udržovat	k5eAaImIp3nS	udržovat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
školou	škola	k1gFnSc7	škola
baletu	balet	k1gInSc2	balet
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
institut	institut	k1gInSc1	institut
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
stupeň	stupeň	k1gInSc4	stupeň
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
Akademií	akademie	k1gFnSc7	akademie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
jí	jíst	k5eAaImIp3nS	jíst
Bamir	Bamir	k1gMnSc1	Bamir
Topi	Top	k1gFnSc2	Top
prezident	prezident	k1gMnSc1	prezident
Albánské	albánský	k2eAgFnSc2d1	albánská
republiky	republika	k1gFnSc2	republika
udělil	udělit	k5eAaPmAgInS	udělit
"	"	kIx"	"
<g/>
Řád	řád	k1gInSc1	řád
velmistra	velmistr	k1gMnSc2	velmistr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akademici	akademik	k1gMnPc5	akademik
==	==	k?	==
</s>
</p>
<p>
<s>
Akademie	akademie	k1gFnSc1	akademie
umění	umění	k1gNnSc2	umění
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tři	tři	k4xCgFnPc4	tři
fakulty	fakulta	k1gFnPc4	fakulta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
:	:	kIx,	:
muzikologie	muzikologie	k1gFnSc2	muzikologie
<g/>
,	,	kIx,	,
dirigování	dirigování	k1gNnSc2	dirigování
<g/>
,	,	kIx,	,
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
hoboj	hoboj	k1gFnSc1	hoboj
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
,	,	kIx,	,
trombon	trombon	k1gInSc1	trombon
<g/>
,	,	kIx,	,
trumpeta	trumpeta	k1gFnSc1	trumpeta
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc1	fagot
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInSc1d1	lesní
roh	roh	k1gInSc1	roh
<g/>
,	,	kIx,	,
tuba	tuba	k1gFnSc1	tuba
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
múzická	múzický	k2eAgFnSc1d1	múzická
pedagogika	pedagogika	k1gFnSc1	pedagogika
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
akademická	akademický	k2eAgNnPc4d1	akademické
oddělení	oddělení	k1gNnPc4	oddělení
</s>
</p>
<p>
<s>
Muzikologie	muzikologie	k1gFnSc1	muzikologie
<g/>
/	/	kIx~	/
<g/>
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
/	/	kIx~	/
<g/>
dirigování	dirigování	k1gNnSc1	dirigování
</s>
</p>
<p>
<s>
ProdukceFakulta	ProdukceFakulta	k1gFnSc1	ProdukceFakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
:	:	kIx,	:
malba	malba	k1gFnSc1	malba
<g/>
,	,	kIx,	,
grafika	grafika	k1gFnSc1	grafika
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
a	a	k8xC	a
módní	módní	k2eAgNnSc1d1	módní
návrhářství	návrhářství	k1gNnSc1	návrhářství
<g/>
,	,	kIx,	,
multimédia	multimédium	k1gNnPc1	multimédium
<g/>
,	,	kIx,	,
aplikovaný	aplikovaný	k2eAgInSc1d1	aplikovaný
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
akademická	akademický	k2eAgNnPc4d1	akademické
oddělení	oddělení	k1gNnPc4	oddělení
</s>
</p>
<p>
<s>
Malířství	malířství	k1gNnSc1	malířství
</s>
</p>
<p>
<s>
SochařstvíFakulta	SochařstvíFakulta	k1gFnSc1	SochařstvíFakulta
dramatických	dramatický	k2eAgNnPc2d1	dramatické
umění	umění	k1gNnPc2	umění
<g/>
:	:	kIx,	:
televizní	televizní	k2eAgFnSc1d1	televizní
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
scénografie	scénografie	k1gFnSc1	scénografie
<g/>
,	,	kIx,	,
kostýmografie	kostýmografie	k1gFnSc1	kostýmografie
<g/>
,	,	kIx,	,
choreografie	choreografie	k1gFnSc1	choreografie
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
akademická	akademický	k2eAgNnPc4d1	akademické
oddělení	oddělení	k1gNnPc4	oddělení
</s>
</p>
<p>
<s>
Herectví	herectví	k1gNnSc1	herectví
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
/	/	kIx~	/
<g/>
design	design	k1gInSc1	design
<g/>
/	/	kIx~	/
<g/>
choreografie	choreografie	k1gFnSc1	choreografie
<g/>
/	/	kIx~	/
<g/>
umělecká	umělecký	k2eAgFnSc1d1	umělecká
teorie	teorie	k1gFnSc1	teorie
</s>
</p>
<p>
<s>
==	==	k?	==
Ředitelé	ředitel	k1gMnPc1	ředitel
<g/>
/	/	kIx~	/
<g/>
rektoři	rektor	k1gMnPc1	rektor
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
měl	mít	k5eAaImAgInS	mít
institut	institut	k1gInSc1	institut
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
absolventi	absolvent	k1gMnPc1	absolvent
==	==	k?	==
</s>
</p>
<p>
<s>
Inva	Inv	k2eAgFnSc1d1	Inv
Mula	mula	k1gFnSc1	mula
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ermonela	Ermonela	k1gFnSc1	Ermonela
Jaho	Jaho	k1gMnSc1	Jaho
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Altin	Altin	k1gMnSc1	Altin
Kaftira	Kaftira	k1gMnSc1	Kaftira
(	(	kIx(	(
<g/>
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anbeta	Anbet	k1gMnSc4	Anbet
Toromani	Toroman	k1gMnPc1	Toroman
(	(	kIx(	(
<g/>
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kledi	Kled	k1gMnPc1	Kled
Kadiu	Kadium	k1gNnSc6	Kadium
(	(	kIx(	(
<g/>
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ilir	Ilir	k1gInSc1	Ilir
Shaqiri	Shaqir	k1gFnSc2	Shaqir
(	(	kIx(	(
<g/>
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Timo	Timo	k6eAd1	Timo
Flloko	Flloko	k1gNnSc1	Flloko
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ndricim	Ndricim	k1gMnSc1	Ndricim
Xhepa	Xhep	k1gMnSc2	Xhep
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Laert	Laert	k1gInSc1	Laert
Vasili	Vasili	k1gFnSc2	Vasili
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nik	nika	k1gFnPc2	nika
Xhelilaj	Xhelilaj	k1gMnSc1	Xhelilaj
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vénera	Vénera	k1gFnSc1	Vénera
Kastrati	Kastrati	k1gFnSc2	Kastrati
(	(	kIx(	(
<g/>
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odise	Odise	k1gFnSc1	Odise
Lakuriqi	Lakuriq	k1gFnSc2	Lakuriq
(	(	kIx(	(
<g/>
malíř	malíř	k1gMnSc1	malíř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Angjelin	Angjelin	k2eAgInSc1d1	Angjelin
Preljocaj	Preljocaj	k1gInSc1	Preljocaj
(	(	kIx(	(
<g/>
balet	balet	k1gInSc1	balet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eda	Eda	k1gMnSc1	Eda
Zari	Zar	k1gFnSc2	Zar
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
in	in	k?	in
Tirana	Tirana	k1gFnSc1	Tirana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
