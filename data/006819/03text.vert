<s>
Kameňák	Kameňák	k?	Kameňák
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
z	z	k7c2	z
filmové	filmový	k2eAgFnSc2d1	filmová
tetralogie	tetralogie	k1gFnSc2	tetralogie
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2003	[number]	k4	2003
až	až	k6eAd1	až
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
seriálu	seriál	k1gInSc2	seriál
Kameňák	Kameňák	k?	Kameňák
jsou	být	k5eAaImIp3nP	být
Josef	Josef	k1gMnSc1	Josef
"	"	kIx"	"
<g/>
Pepa	Pepa	k1gMnSc1	Pepa
<g/>
"	"	kIx"	"
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Vilma	Vilma	k1gFnSc1	Vilma
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
"	"	kIx"	"
<g/>
Pepíček	Pepíček	k1gMnSc1	Pepíček
<g/>
"	"	kIx"	"
Novák	Novák	k1gMnSc1	Novák
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
Kropáčková	Kropáčková	k1gFnSc1	Kropáčková
a	a	k8xC	a
Uzlíček	uzlíček	k1gInSc1	uzlíček
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
smyšleném	smyšlený	k2eAgNnSc6d1	smyšlené
městečku	městečko	k1gNnSc6	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
číhá	číhat	k5eAaImIp3nS	číhat
jiná	jiný	k2eAgFnSc1d1	jiná
vtipná	vtipný	k2eAgFnSc1d1	vtipná
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
díle	dílo	k1gNnSc6	dílo
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
-	-	kIx~	-
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Pepa	Pepa	k1gMnSc1	Pepa
–	–	k?	–
policista	policista	k1gMnSc1	policista
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Vydra	Vydra	k1gMnSc1	Vydra
<g/>
)	)	kIx)	)
Vilma	Vilma	k1gFnSc1	Vilma
–	–	k?	–
učitelka	učitelka	k1gFnSc1	učitelka
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Paulová	Paulová	k1gFnSc1	Paulová
<g/>
)	)	kIx)	)
Víťa	Víťa	k?	Víťa
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Pěknic	Pěknice	k1gFnPc2	Pěknice
<g/>
)	)	kIx)	)
Rodeo	rodeo	k1gNnSc1	rodeo
–	–	k?	–
voják	voják	k1gMnSc1	voják
(	(	kIx(	(
<g/>
Nikola	Nikola	k1gMnSc1	Nikola
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
)	)	kIx)	)
Leo	Leo	k1gMnSc1	Leo
Kohn	Kohn	k1gMnSc1	Kohn
–	–	k?	–
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Laufer	Laufer	k1gMnSc1	Laufer
<g/>
)	)	kIx)	)
Primář	primář	k1gMnSc1	primář
–	–	k?	–
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
<g />
.	.	kIx.	.
</s>
<s>
Vašut	Vašut	k1gInSc1	Vašut
<g/>
)	)	kIx)	)
Kropáčková	Kropáčková	k1gFnSc1	Kropáčková
-	-	kIx~	-
stará	starý	k2eAgFnSc1d1	stará
babka	babka	k1gFnSc1	babka
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Vejvodová	Vejvodová	k1gFnSc1	Vejvodová
<g/>
)	)	kIx)	)
Pepiček	Pepička	k1gFnPc2	Pepička
–	–	k?	–
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
)	)	kIx)	)
Uzlíček	uzlíček	k1gInSc1	uzlíček
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
)	)	kIx)	)
Major	major	k1gMnSc1	major
–	–	k?	–
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
T.	T.	kA	T.
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
)	)	kIx)	)
Kameňák	Kameňák	k?	Kameňák
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Kameňák	Kameňák	k?	Kameňák
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Kameňák	Kameňák	k?	Kameňák
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
