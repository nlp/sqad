<s>
Podnik	podnik	k1gInSc1	podnik
založil	založit	k5eAaPmAgMnS	založit
Josef	Josef	k1gMnSc1	Josef
Hardtmuth	Hardtmuth	k1gMnSc1	Hardtmuth
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
grafitových	grafitový	k2eAgNnPc2d1	grafitové
jader	jádro	k1gNnPc2	jádro
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
Carlem	Carl	k1gMnSc7	Carl
Hardtmuthem	Hardtmuth	k1gInSc7	Hardtmuth
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
