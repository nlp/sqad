<s>
Název	název	k1gInSc1	název
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
glue	glu	k1gInSc2	glu
–	–	k?	–
lepidlo	lepidlo	k1gNnSc1	lepidlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
gluony	gluon	k1gInPc1	gluon
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
drží	držet	k5eAaImIp3nS	držet
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
pevně	pevně	k6eAd1	pevně
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
