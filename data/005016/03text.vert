<s>
Hanča	Hanča	k1gFnSc1	Hanča
<g/>
,	,	kIx,	,
též	též	k9	též
handža	handža	k6eAd1	handža
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
znaky	znak	k1gInPc7	znak
<g/>
:	:	kIx,	:
漢	漢	k?	漢
<g/>
,	,	kIx,	,
hangul	hangul	k1gInSc1	hangul
<g/>
:	:	kIx,	:
한	한	k?	한
<g/>
,	,	kIx,	,
foneticky	foneticky	k6eAd1	foneticky
/	/	kIx~	/
<g/>
한	한	k?	한
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
han	hana	k1gFnPc2	hana
<g/>
.	.	kIx.	.
<g/>
tɕ	tɕ	k?	tɕ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korejský	korejský	k2eAgInSc4d1	korejský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
čínské	čínský	k2eAgInPc4d1	čínský
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
ty	ten	k3xDgInPc1	ten
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
včleněny	včlenit	k5eAaPmNgInP	včlenit
do	do	k7c2	do
korejského	korejský	k2eAgInSc2d1	korejský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
původní	původní	k2eAgFnSc1d1	původní
"	"	kIx"	"
<g/>
čínská	čínský	k2eAgFnSc1d1	čínská
<g/>
"	"	kIx"	"
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
přizpůsobila	přizpůsobit	k5eAaPmAgFnS	přizpůsobit
korejskému	korejský	k2eAgInSc3d1	korejský
fonologickému	fonologický	k2eAgInSc3d1	fonologický
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
hanča	hanč	k2eAgFnSc1d1	hanč
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
漢	漢	k?	漢
(	(	kIx(	(
<g/>
한	한	k?	한
/	/	kIx~	/
<g/>
han	hana	k1gFnPc2	hana
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
čínský	čínský	k2eAgInSc1d1	čínský
národ	národ	k1gInSc1	národ
Chanů	Chan	k1gMnPc2	Chan
<g/>
,	,	kIx,	,
a	a	k8xC	a
znak	znak	k1gInSc1	znak
字	字	k?	字
(	(	kIx(	(
<g/>
자	자	k?	자
/	/	kIx~	/
<g/>
tɕ	tɕ	k?	tɕ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
název	název	k1gInSc1	název
pro	pro	k7c4	pro
korejskou	korejský	k2eAgFnSc4d1	Korejská
abecedu	abeceda	k1gFnSc4	abeceda
Hangul	Hangula	k1gFnPc2	Hangula
(	(	kIx(	(
<g/>
한	한	k?	한
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složenina	složenina	k1gFnSc1	složenina
dvou	dva	k4xCgFnPc2	dva
slabik	slabika	k1gFnPc2	slabika
<g/>
:	:	kIx,	:
han	hana	k1gFnPc2	hana
(	(	kIx(	(
<g/>
韓	韓	k?	韓
<g/>
/	/	kIx~	/
<g/>
한	한	k?	한
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
korejský	korejský	k2eAgMnSc1d1	korejský
<g/>
,	,	kIx,	,
a	a	k8xC	a
kul	koulit	k5eAaImRp2nS	koulit
(	(	kIx(	(
<g/>
글	글	k?	글
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písmo	písmo	k1gNnSc1	písmo
<g/>
/	/	kIx~	/
<g/>
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Hanča	Hanča	k1gFnSc1	Hanča
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
identické	identický	k2eAgInPc1d1	identický
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
čínskými	čínský	k2eAgInPc7d1	čínský
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Korejskému	korejský	k2eAgInSc3d1	korejský
názvu	název	k1gInSc3	název
hanča	hanča	k6eAd1	hanča
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
název	název	k1gInSc1	název
chan-c	chan	k6eAd1	chan-c
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
pinyin	pinyin	k1gInSc4	pinyin
<g/>
:	:	kIx,	:
hanzi	hanze	k1gFnSc4	hanze
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
název	název	k1gInSc4	název
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kandži	kandž	k1gFnSc6	kandž
či	či	k8xC	či
současných	současný	k2eAgInPc2d1	současný
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zjednodušeny	zjednodušit	k5eAaPmNgInP	zjednodušit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
hanča	hanča	k6eAd1	hanča
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nezměnily	změnit	k5eNaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Hančao	Hančao	k1gNnSc1	Hančao
(	(	kIx(	(
<g/>
한	한	k?	한
<g/>
/	/	kIx~	/
<g/>
漢	漢	k?	漢
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
korejská	korejský	k2eAgNnPc1d1	korejské
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zapsat	zapsat	k5eAaPmF	zapsat
pomocí	pomocí	k7c2	pomocí
znaků	znak	k1gInPc2	znak
hanča	hančum	k1gNnSc2	hančum
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
původní	původní	k2eAgNnPc1d1	původní
korejská	korejský	k2eAgNnPc1d1	korejské
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
morfémy	morfém	k1gInPc1	morfém
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zapsat	zapsat	k5eAaPmF	zapsat
jedině	jedině	k6eAd1	jedině
pomocí	pomocí	k7c2	pomocí
abecedy	abeceda	k1gFnSc2	abeceda
hangul	hangula	k1gFnPc2	hangula
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vytvořením	vytvoření	k1gNnSc7	vytvoření
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1443	[number]	k4	1443
byly	být	k5eAaImAgInP	být
pokusy	pokus	k1gInPc1	pokus
vytvořit	vytvořit	k5eAaPmF	vytvořit
systém	systém	k1gInSc4	systém
písma	písmo	k1gNnSc2	písmo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zjednodušených	zjednodušený	k2eAgInPc2d1	zjednodušený
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
reprezentovaly	reprezentovat	k5eAaImAgInP	reprezentovat
korejské	korejský	k2eAgInPc1d1	korejský
morfémy	morfém	k1gInPc1	morfém
–	–	k?	–
hjangčchal	hjangčchat	k5eAaPmAgInS	hjangčchat
(	(	kIx(	(
<g/>
향	향	k?	향
<g/>
/	/	kIx~	/
<g/>
鄕	鄕	k?	鄕
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kugjol	kugjol	k1gInSc1	kugjol
(	(	kIx(	(
<g/>
구	구	k?	구
<g/>
/	/	kIx~	/
<g/>
口	口	k?	口
<g/>
)	)	kIx)	)
a	a	k8xC	a
idu	ido	k1gNnSc6	ido
(	(	kIx(	(
<g/>
이	이	k?	이
<g/>
/	/	kIx~	/
<g/>
吏	吏	k?	吏
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
těžkopádné	těžkopádný	k2eAgNnSc1d1	těžkopádné
a	a	k8xC	a
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
i	i	k9	i
tak	tak	k6eAd1	tak
znalost	znalost	k1gFnSc1	znalost
komplikovaného	komplikovaný	k2eAgInSc2d1	komplikovaný
systému	systém	k1gInSc2	systém
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
dokumenty	dokument	k1gInPc1	dokument
psaly	psát	k5eAaImAgInP	psát
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
čínštině	čínština	k1gFnSc6	čínština
(	(	kIx(	(
<g/>
podobnou	podobný	k2eAgFnSc4d1	podobná
pozici	pozice	k1gFnSc4	pozice
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
znaků	znak	k1gInPc2	znak
psalo	psát	k5eAaImAgNnS	psát
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
Koreje	Korea	k1gFnSc2	Korea
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
hnutí	hnutí	k1gNnSc1	hnutí
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
používání	používání	k1gNnSc3	používání
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Zavádění	zavádění	k1gNnSc1	zavádění
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
bylo	být	k5eAaImAgNnS	být
postupné	postupný	k2eAgNnSc1d1	postupné
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
používat	používat	k5eAaImF	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
osobních	osobní	k2eAgNnPc2d1	osobní
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ke	k	k7c3	k
dokumentování	dokumentování	k1gNnSc3	dokumentování
důležitých	důležitý	k2eAgFnPc2d1	důležitá
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
chtěl	chtít	k5eAaImAgMnS	chtít
zastávat	zastávat	k5eAaImF	zastávat
veřejný	veřejný	k2eAgInSc4d1	veřejný
post	post	k1gInSc4	post
musel	muset	k5eAaImAgInS	muset
ovládat	ovládat	k5eAaImF	ovládat
klasickou	klasický	k2eAgFnSc4d1	klasická
čínštinu	čínština	k1gFnSc4	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
Korejci	Korejec	k1gMnPc1	Korejec
mohou	moct	k5eAaImIp3nP	moct
svá	svůj	k3xOyFgNnPc4	svůj
jména	jméno	k1gNnPc4	jméno
zapsat	zapsat	k5eAaPmF	zapsat
pomocí	pomocí	k7c2	pomocí
písma	písmo	k1gNnSc2	písmo
hanča	hanč	k1gInSc2	hanč
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
posunem	posun	k1gInSc7	posun
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
znaků	znak	k1gInPc2	znak
bylo	být	k5eAaImAgNnS	být
rozšíření	rozšíření	k1gNnSc1	rozšíření
buddhismu	buddhismus	k1gInSc2	buddhismus
na	na	k7c6	na
Korejském	korejský	k2eAgInSc6d1	korejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
to	ten	k3xDgNnSc1	ten
buddhistické	buddhistický	k2eAgInPc1d1	buddhistický
náboženské	náboženský	k2eAgInPc1d1	náboženský
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
podpořily	podpořit	k5eAaPmAgInP	podpořit
rozšíření	rozšíření	k1gNnSc4	rozšíření
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čínská	čínský	k2eAgFnSc1d1	čínská
čítanka	čítanka	k1gFnSc1	čítanka
Čchien-c	Čchien	k1gFnSc1	Čchien-c
<g/>
'	'	kIx"	'
<g/>
-wen	en	k1gInSc1	-wen
(	(	kIx(	(
<g/>
천	천	k?	천
<g/>
/	/	kIx~	/
<g/>
千	千	k?	千
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
tɕ	tɕ	k?	tɕ
<g/>
.	.	kIx.	.
<g/>
dʑ	dʑ	k?	dʑ
<g/>
.	.	kIx.	.
<g/>
mun	muna	k1gFnPc2	muna
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Text	text	k1gInSc1	text
o	o	k7c4	o
tisíci	tisíc	k4xCgInPc7	tisíc
znacích	znak	k1gInPc6	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc4	znak
korejskou	korejský	k2eAgFnSc7d1	Korejská
abecedou	abeceda	k1gFnSc7	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
plný	plný	k2eAgInSc4d1	plný
rozvoj	rozvoj	k1gInSc4	rozvoj
korejského	korejský	k2eAgInSc2d1	korejský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
abecedy	abeceda	k1gFnSc2	abeceda
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
poloostrova	poloostrov	k1gInSc2	poloostrov
nastala	nastat	k5eAaPmAgFnS	nastat
i	i	k9	i
diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
znaků	znak	k1gInPc2	znak
hanča	hanča	k6eAd1	hanča
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byl	být	k5eAaImAgInS	být
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
smíšeného	smíšený	k2eAgInSc2d1	smíšený
systému	systém	k1gInSc2	systém
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
pomocí	pomocí	k7c2	pomocí
samotné	samotný	k2eAgFnSc2d1	samotná
abecedy	abeceda	k1gFnSc2	abeceda
Hangul	Hangul	k1gInSc1	Hangul
plynulejší	plynulý	k2eAgInSc1d2	plynulejší
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Kim	Kim	k1gFnSc1	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
používat	používat	k5eAaImF	používat
čínské	čínský	k2eAgInPc4d1	čínský
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
horizontální	horizontální	k2eAgInSc4d1	horizontální
levo-pravý	levoravý	k2eAgInSc4d1	levo-pravý
zápis	zápis	k1gInSc4	zápis
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
text	text	k1gInSc1	text
zapisoval	zapisovat	k5eAaImAgInS	zapisovat
vertikálně	vertikálně	k6eAd1	vertikálně
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
základních	základní	k2eAgInPc2d1	základní
2000	[number]	k4	2000
znaků	znak	k1gInPc2	znak
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
poslední	poslední	k2eAgFnPc1d1	poslední
generace	generace	k1gFnPc1	generace
neovládají	ovládat	k5eNaImIp3nP	ovládat
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dost	dost	k6eAd1	dost
lhostejné	lhostejný	k2eAgNnSc1d1	lhostejné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hanča	Hanča	k1gFnSc1	Hanča
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
Kandži	Kandž	k1gFnSc3	Kandž
Korejština	korejština	k1gFnSc1	korejština
</s>
