<s>
Hanča	Hanča	k1gFnSc1	Hanča
<g/>
,	,	kIx,	,
též	též	k9	též
handža	handža	k6eAd1	handža
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
znaky	znak	k1gInPc7	znak
<g/>
:	:	kIx,	:
漢	漢	k?	漢
<g/>
,	,	kIx,	,
hangul	hangul	k1gInSc1	hangul
<g/>
:	:	kIx,	:
한	한	k?	한
<g/>
,	,	kIx,	,
foneticky	foneticky	k6eAd1	foneticky
/	/	kIx~	/
<g/>
한	한	k?	한
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
han	hana	k1gFnPc2	hana
<g/>
.	.	kIx.	.
<g/>
tɕ	tɕ	k?	tɕ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korejský	korejský	k2eAgInSc4d1	korejský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
čínské	čínský	k2eAgInPc4d1	čínský
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
