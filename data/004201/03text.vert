<s>
Horem	horem	k6eAd1	horem
pádem	pád	k1gInSc7	pád
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
režírovaný	režírovaný	k2eAgInSc1d1	režírovaný
Janem	Jan	k1gMnSc7	Jan
Hřebejkem	Hřebejek	k1gInSc7	Hřebejek
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
čtyři	čtyři	k4xCgInPc1	čtyři
České	český	k2eAgInPc1d1	český
lvy	lev	k1gInPc1	lev
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ženská	ženský	k2eAgFnSc1d1	ženská
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c6	na
77	[number]	k4	77
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
předávání	předávání	k1gNnSc2	předávání
Oscarů	Oscar	k1gInPc2	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprošel	projít	k5eNaPmAgInS	projít
nominacemi	nominace	k1gFnPc7	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
o	o	k7c6	o
filmu	film	k1gInSc6	film
Horem	horem	k6eAd1	horem
pádem	pád	k1gInSc7	pád
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
