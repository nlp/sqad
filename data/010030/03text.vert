<p>
<s>
Leobner	Leobnrat	k5eAaPmRp2nS	Leobnrat
Hütte	Hütt	k1gMnSc5	Hütt
je	být	k5eAaImIp3nS	být
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Hochschwab	Hochschwaba	k1gFnPc2	Hochschwaba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakouských	rakouský	k2eAgFnPc6d1	rakouská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Chata	chata	k1gFnSc1	chata
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Rakouského	rakouský	k2eAgInSc2d1	rakouský
Alpenvereinu	Alpenverein	k1gInSc2	Alpenverein
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc2	sekce
Leoben	Leobna	k1gFnPc2	Leobna
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
chaty	chata	k1gFnSc2	chata
činí	činit	k5eAaImIp3nS	činit
21	[number]	k4	21
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Otevřena	otevřít	k5eAaPmNgFnS	otevřít
je	být	k5eAaImIp3nS	být
i	i	k9	i
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Chata	chata	k1gFnSc1	chata
nemá	mít	k5eNaImIp3nS	mít
zimní	zimní	k2eAgInSc4d1	zimní
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Leobner	Leobnrat	k5eAaPmRp2nS	Leobnrat
Hütte	Hütt	k1gMnSc5	Hütt
leží	ležet	k5eAaImIp3nS	ležet
zhruba	zhruba	k6eAd1	zhruba
7,5	[number]	k4	7,5
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Eisenerz	Eisenerza	k1gFnPc2	Eisenerza
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
Hirscheggsattel	Hirscheggsattela	k1gFnPc2	Hirscheggsattela
(	(	kIx(	(
<g/>
1699	[number]	k4	1699
m	m	kA	m
<g/>
)	)	kIx)	)
oddělující	oddělující	k2eAgInPc1d1	oddělující
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
masiv	masiv	k1gInSc1	masiv
Griesmauer	Griesmaura	k1gFnPc2	Griesmaura
a	a	k8xC	a
Polster	Polstra	k1gFnPc2	Polstra
<g/>
.	.	kIx.	.
2,5	[number]	k4	2,5
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
chaty	chata	k1gFnSc2	chata
leží	ležet	k5eAaImIp3nS	ležet
lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
roztroušené	roztroušený	k2eAgNnSc1d1	roztroušené
podle	podle	k7c2	podle
silničního	silniční	k2eAgNnSc2d1	silniční
sedla	sedlo	k1gNnSc2	sedlo
Präbichl	Präbichl	k1gFnSc2	Präbichl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
z	z	k7c2	z
Eisenerz	Eisenerza	k1gFnPc2	Eisenerza
(	(	kIx(	(
<g/>
944	[number]	k4	944
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
2.30	[number]	k4	2.30
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
třicet	třicet	k4xCc4	třicet
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
z	z	k7c2	z
Präbichl	Präbichl	k1gFnSc2	Präbichl
(	(	kIx(	(
<g/>
1232	[number]	k4	1232
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.15	[number]	k4	1.15
</s>
</p>
<p>
<s>
z	z	k7c2	z
Tragöß	Tragöß	k1gFnSc2	Tragöß
(	(	kIx(	(
<g/>
780	[number]	k4	780
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
3.30	[number]	k4	3.30
</s>
</p>
<p>
<s>
z	z	k7c2	z
Vordernberg	Vordernberg	k1gMnSc1	Vordernberg
(	(	kIx(	(
<g/>
839	[number]	k4	839
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
2.30	[number]	k4	2.30
</s>
</p>
<p>
<s>
==	==	k?	==
Možné	možný	k2eAgInPc4d1	možný
cíle	cíl	k1gInPc4	cíl
==	==	k?	==
</s>
</p>
<p>
<s>
Griesmauer	Griesmauer	k1gInSc1	Griesmauer
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.30	[number]	k4	1.30
</s>
</p>
<p>
<s>
TAC	TAC	kA	TAC
Spitze	Spitze	k1gFnSc1	Spitze
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.45	[number]	k4	1.45
</s>
</p>
<p>
<s>
Hochturm	Hochturm	k1gInSc1	Hochturm
(	(	kIx(	(
<g/>
2081	[number]	k4	2081
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.30	[number]	k4	1.30
</s>
</p>
<p>
<s>
Polster	Polster	k1gInSc1	Polster
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.00	[number]	k4	1.00
</s>
</p>
<p>
<s>
Leobner	Leobner	k1gMnSc1	Leobner
Mauer	Mauer	k1gMnSc1	Mauer
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
m	m	kA	m
<g/>
)	)	kIx)	)
čas	čas	k1gInSc1	čas
1.00	[number]	k4	1.00
</s>
</p>
