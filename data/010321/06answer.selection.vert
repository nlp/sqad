<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
obloze	obloha	k1gFnSc6	obloha
velmi	velmi	k6eAd1	velmi
jasné	jasný	k2eAgNnSc1d1	jasné
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
pozorovat	pozorovat	k5eAaImF	pozorovat
nechráněným	chráněný	k2eNgNnSc7d1	nechráněné
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gNnSc4	jeho
delší	dlouhý	k2eAgNnSc4d2	delší
pozorování	pozorování	k1gNnSc4	pozorování
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
