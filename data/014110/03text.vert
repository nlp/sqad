<s>
Pekachjáš	Pekachjat	k5eAaBmIp2nS,k5eAaPmIp2nS,k5eAaImIp2nS
</s>
<s>
Pekachjáš	Pekachjat	k5eAaPmIp2nS,k5eAaImIp2nS,k5eAaBmIp2nS
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Samaří	Samaří	k1gNnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Menachém	Menachý	k2eAgInSc6d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pekachjáš	Pekachjat	k5eAaBmIp2nS,k5eAaPmIp2nS,k5eAaImIp2nS
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
פ	פ	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ק	ק	k?
<g/>
ַ	ַ	k?
<g/>
ח	ח	k?
<g/>
ְ	ְ	k?
<g/>
י	י	k?
<g/>
ָ	ָ	k?
<g/>
ה	ה	k?
<g/>
,	,	kIx,
Pekachja	Pekachja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
českých	český	k2eAgInPc6d1
překladech	překlad	k1gInPc6
Bible	bible	k1gFnSc2
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
jako	jako	k8xC,k8xS
Pekachiáš	Pekachiáš	k1gMnSc1
či	či	k8xC
Pekachia	Pekachius	k1gMnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
sedmnáctým	sedmnáctý	k4xOgMnSc7
králem	král	k1gMnSc7
Severního	severní	k2eAgNnSc2d1
izraelského	izraelský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
se	se	k3xPyFc4
vykládá	vykládat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
Otevřel	otevřít	k5eAaPmAgInS
(	(	kIx(
<g/>
oči	oko	k1gNnPc1
<g/>
)	)	kIx)
Hospodin	Hospodin	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
názoru	názor	k1gInSc2
moderních	moderní	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
a	a	k8xC
archeologů	archeolog	k1gMnPc2
vládl	vládnout	k5eAaImAgInS
asi	asi	k9
v	v	k7c6
letech	léto	k1gNnPc6
737	#num#	k4
až	až	k9
735	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
kroniky	kronika	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Davida	David	k1gMnSc2
Ganse	Gans	k1gMnSc2
by	by	kYmCp3nS
však	však	k9
jeho	jeho	k3xOp3gNnSc1
kralování	kralování	k1gNnSc1
mělo	mít	k5eAaImAgNnS
spadat	spadat	k5eAaImF,k5eAaPmF
do	do	k7c2
let	léto	k1gNnPc2
3165	#num#	k4
<g/>
–	–	k?
<g/>
3167	#num#	k4
od	od	k7c2
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
neboli	neboli	k8xC
do	do	k7c2
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
597	#num#	k4
<g/>
–	–	k?
<g/>
594	#num#	k4
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
2	#num#	k4
letům	let	k1gInPc3
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
v	v	k7c6
Druhé	druhý	k4xOgFnSc6
knize	kniha	k1gFnSc6
králů	král	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pekachjáš	Pekachjat	k5eAaPmIp2nS,k5eAaImIp2nS,k5eAaBmIp2nS
byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
krále	král	k1gMnSc2
Menachéma	Menachém	k1gMnSc2
a	a	k8xC
v	v	k7c6
Samaří	Samaří	k1gNnSc6
začal	začít	k5eAaPmAgInS
vládnout	vládnout	k5eAaImF
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
roce	rok	k1gInSc6
vlády	vláda	k1gFnSc2
judského	judský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Azarjáše	Azarjáš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pekachjáš	Pekachjáš	k1gFnSc1
během	během	k7c2
své	svůj	k3xOyFgFnSc2
krátké	krátký	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
politice	politika	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
spočívala	spočívat	k5eAaImAgFnS
ve	v	k7c6
vazalství	vazalství	k1gNnSc6
k	k	k7c3
asyrskému	asyrský	k2eAgMnSc3d1
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřejmě	zřejmě	k6eAd1
kvůli	kvůli	k7c3
této	tento	k3xDgFnSc3
poplatnické	poplatnický	k2eAgFnSc3d1
politice	politika	k1gFnSc3
se	se	k3xPyFc4
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
spikl	spiknout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
štítonoš	štítonoš	k1gMnSc1
Pekach	Pekach	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Remaljášův	Remaljášův	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ho	on	k3xPp3gMnSc4
ubil	ubít	k5eAaPmAgMnS
v	v	k7c6
paláci	palác	k1gInSc6
královského	královský	k2eAgInSc2d1
domu	dům	k1gInSc2
a	a	k8xC
sám	sám	k3xTgInSc1
místo	místo	k7c2
něj	on	k3xPp3gMnSc4
usedl	usednout	k5eAaPmAgMnS
na	na	k7c4
izraelský	izraelský	k2eAgInSc4d1
královský	královský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Králové	Král	k1gMnPc1
Izraelského	izraelský	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Menachém	Menachý	k2eAgMnSc6d1
</s>
<s>
737	#num#	k4
<g/>
–	–	k?
<g/>
735	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Pekachjáš	Pekachjáš	k1gFnSc2
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Pekach	Pekach	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HELLER	HELLER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
biblických	biblický	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Advent-Orion	Advent-Orion	k1gInSc1
<g/>
/	/	kIx~
<g/>
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7172	#num#	k4
<g/>
-	-	kIx~
<g/>
865	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
725	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
verzí	verze	k1gFnPc2
datace	datace	k1gFnSc2
vlády	vláda	k1gFnSc2
izraelských	izraelský	k2eAgMnPc2d1
a	a	k8xC
judských	judský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
datace	datace	k1gFnSc1
podle	podle	k7c2
Finkelstein	Finkelsteina	k1gFnPc2
–	–	k?
Silberman	Silberman	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
Anchor	Anchora	k1gFnPc2
Bible	bible	k1gFnSc2
Dictionary	Dictionara	k1gFnSc2
a	a	k8xC
Galilovy	Galilův	k2eAgFnSc2d1
práce	práce	k1gFnSc2
The	The	k1gFnSc2
Chronology	chronolog	k1gMnPc4
of	of	k?
the	the	k?
Kings	Kings	k1gInSc1
of	of	k?
Israel	Israel	k1gInSc1
and	and	k?
Judah	Judah	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GANS	GANS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ratolest	ratolest	k1gFnSc1
Davidova	Davidův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2535	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
79	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
2	#num#	k4
<g/>
Kr	Kr	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biblický	biblický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kalich	kalich	k1gInSc1
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Pekachia	Pekachium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
DOUGLAS	DOUGLAS	kA
<g/>
,	,	kIx,
J.	J.	kA
D.	D.	kA
Nový	nový	k2eAgInSc1d1
biblický	biblický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85495	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
PEKACHJÁŠ	PEKACHJÁŠ	kA
<g/>
.	.	kIx.
</s>
<s>
FINKELSTEIN	FINKELSTEIN	kA
<g/>
,	,	kIx,
Israel	Israel	k1gMnSc1
<g/>
;	;	kIx,
SILBERMAN	SILBERMAN	kA
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Asher	Asher	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevování	objevování	k1gNnSc1
Bible	bible	k1gFnSc1
<g/>
:	:	kIx,
Svatá	svatá	k1gFnSc1
Písma	písmo	k1gNnSc2
Izraele	Izrael	k1gInSc2
ve	v	k7c6
světle	světlo	k1gNnSc6
moderní	moderní	k2eAgFnSc2d1
archeologie	archeologie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
869	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
