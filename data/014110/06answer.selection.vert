<s>
Pekachjáš	Pekachjat	k5eAaBmIp2nS,k5eAaPmIp2nS,k5eAaImIp2nS
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
פ	פ	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ק	ק	k?
<g/>
ַ	ַ	k?
<g/>
ח	ח	k?
<g/>
ְ	ְ	k?
<g/>
י	י	k?
<g/>
ָ	ָ	k?
<g/>
ה	ה	k?
<g/>
,	,	kIx,
Pekachja	Pekachja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
českých	český	k2eAgInPc6d1
překladech	překlad	k1gInPc6
Bible	bible	k1gFnSc2
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
jako	jako	k8xC,k8xS
Pekachiáš	Pekachiáš	k1gMnSc1
či	či	k8xC
Pekachia	Pekachius	k1gMnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
sedmnáctým	sedmnáctý	k4xOgMnSc7
králem	král	k1gMnSc7
Severního	severní	k2eAgNnSc2d1
izraelského	izraelský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>