<p>
<s>
William	William	k6eAd1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1773	[number]	k4	1773
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
devátým	devátý	k4xOgMnSc7	devátý
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
<g/>
,	,	kIx,	,
válečným	válečný	k2eAgMnSc7d1	válečný
hrdinou	hrdina	k1gMnSc7	hrdina
a	a	k8xC	a
senátorem	senátor	k1gMnSc7	senátor
státu	stát	k1gInSc2	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
68	[number]	k4	68
letech	let	k1gInPc6	let
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejstarším	starý	k2eAgMnSc7d3	nejstarší
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
posledním	poslední	k2eAgMnSc7d1	poslední
prezidentem	prezident	k1gMnSc7	prezident
narozeným	narozený	k2eAgFnPc3d1	narozená
před	před	k7c7	před
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
virginských	virginský	k2eAgMnPc2d1	virginský
plantážníků	plantážník	k1gMnPc2	plantážník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
Benjamin	Benjamin	k1gMnSc1	Benjamin
Harrison	Harrison	k1gMnSc1	Harrison
V	V	kA	V
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
Otců	otec	k1gMnPc2	otec
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
guvernérem	guvernér	k1gMnSc7	guvernér
teritoria	teritorium	k1gNnSc2	teritorium
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
americkým	americký	k2eAgInSc7d1	americký
jednotkám	jednotka	k1gFnPc3	jednotka
při	při	k7c6	při
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Tecumsehem	Tecumseh	k1gInSc7	Tecumseh
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tippecanoe	Tippecano	k1gInSc2	Tippecano
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
proto	proto	k8xC	proto
přezdívku	přezdívka	k1gFnSc4	přezdívka
Old	Olda	k1gFnPc2	Olda
Tippecanoe	Tippecano	k1gInSc2	Tippecano
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
pověst	pověst	k1gFnSc4	pověst
využil	využít	k5eAaPmAgMnS	využít
při	při	k7c6	při
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
šel	jít	k5eAaImAgMnS	jít
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
nazvanou	nazvaný	k2eAgFnSc7d1	nazvaná
"	"	kIx"	"
<g/>
Tippecanoe	Tippecanoe	k1gFnSc7	Tippecanoe
and	and	k?	and
Tyler	Tyler	k1gMnSc1	Tyler
Too	Too	k1gMnSc1	Too
<g/>
"	"	kIx"	"
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
úřadujícího	úřadující	k2eAgMnSc4d1	úřadující
prezidenta	prezident	k1gMnSc4	prezident
Martina	Martin	k1gInSc2	Martin
Van	van	k1gInSc1	van
Burena	Bureno	k1gNnSc2	Bureno
poměrem	poměr	k1gInSc7	poměr
volitelů	volitel	k1gMnPc2	volitel
234	[number]	k4	234
<g/>
:	:	kIx,	:
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInSc1	jeho
inaugurační	inaugurační	k2eAgInSc1d1	inaugurační
projev	projev	k1gInSc1	projev
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1841	[number]	k4	1841
trval	trvat	k5eAaImAgInS	trvat
jednu	jeden	k4xCgFnSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
nevhodnému	vhodný	k2eNgNnSc3d1	nevhodné
oblečení	oblečení	k1gNnSc3	oblečení
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
projevu	projev	k1gInSc6	projev
(	(	kIx(	(
<g/>
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
zimník	zimník	k1gInSc4	zimník
<g/>
)	)	kIx)	)
a	a	k8xC	a
chladnému	chladný	k2eAgNnSc3d1	chladné
počasí	počasí	k1gNnSc3	počasí
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
realistická	realistický	k2eAgFnSc1d1	realistická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
první	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
objevily	objevit	k5eAaPmAgInP	objevit
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
projevu	projev	k1gInSc6	projev
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
teorií	teorie	k1gFnSc7	teorie
zabývali	zabývat	k5eAaImAgMnP	zabývat
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nepravděpodobnou	pravděpodobný	k2eNgFnSc4d1	nepravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
tyfová	tyfový	k2eAgFnSc1d1	tyfová
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
primitivními	primitivní	k2eAgFnPc7d1	primitivní
hygienickými	hygienický	k2eAgFnPc7d1	hygienická
podmínkami	podmínka	k1gFnPc7	podmínka
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
prezidentském	prezidentský	k2eAgNnSc6d1	prezidentské
sídle	sídlo	k1gNnSc6	sídlo
(	(	kIx(	(
<g/>
kanalizační	kanalizační	k2eAgInSc1d1	kanalizační
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
zřízen	zřídit	k5eAaPmNgInS	zřídit
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vládl	vládnout	k5eAaImAgMnS	vládnout
pouze	pouze	k6eAd1	pouze
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
tedy	tedy	k8xC	tedy
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
strávil	strávit	k5eAaPmAgMnS	strávit
nejkratší	krátký	k2eAgNnSc4d3	nejkratší
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
ústavní	ústavní	k2eAgFnSc3d1	ústavní
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vedlo	vést	k5eAaImAgNnS	vést
to	ten	k3xDgNnSc1	ten
také	také	k9	také
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
k	k	k7c3	k
vyjasnění	vyjasnění	k1gNnSc3	vyjasnění
následnictví	následnictví	k1gNnSc2	následnictví
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
Harrisonovou	Harrisonová	k1gFnSc7	Harrisonová
měli	mít	k5eAaImAgMnP	mít
deset	deset	k4xCc4	deset
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Benjamin	Benjamin	k1gMnSc1	Benjamin
Harrison	Harrison	k1gMnSc1	Harrison
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
v	v	k7c6	v
letech	let	k1gInPc6	let
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
odhalen	odhalit	k5eAaPmNgMnS	odhalit
Harrisonovi	Harrison	k1gMnSc3	Harrison
jezdecký	jezdecký	k2eAgInSc4d1	jezdecký
pomník	pomník	k1gInSc4	pomník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
Williama	William	k1gMnSc2	William
Harrisona	Harrison	k1gMnSc2	Harrison
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
CommonsBiografie	CommonsBiografie	k1gFnSc2	CommonsBiografie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
</p>
