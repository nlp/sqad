<s>
William	William	k6eAd1	William
Henry	Henry	k1gMnSc1	Henry
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1773	[number]	k4	1773
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
devátým	devátý	k4xOgMnSc7	devátý
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
<g/>
,	,	kIx,	,
válečným	válečný	k2eAgMnSc7d1	válečný
hrdinou	hrdina	k1gMnSc7	hrdina
a	a	k8xC	a
senátorem	senátor	k1gMnSc7	senátor
státu	stát	k1gInSc2	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
68	[number]	k4	68
letech	let	k1gInPc6	let
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejstarším	starý	k2eAgMnSc7d3	nejstarší
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
posledním	poslední	k2eAgMnSc7d1	poslední
prezidentem	prezident	k1gMnSc7	prezident
narozeným	narozený	k2eAgFnPc3d1	narozená
před	před	k7c7	před
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>

