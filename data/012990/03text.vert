<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
2013	[number]	k4	2013
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
v	v	k7c6	v
rychlobruslařské	rychlobruslařský	k2eAgFnSc6d1	rychlobruslařská
hale	hala	k1gFnSc6	hala
Adler-Arena	Adler-Areno	k1gNnSc2	Adler-Areno
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
15	[number]	k4	15
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Českou	český	k2eAgFnSc4d1	Česká
výpravu	výprava	k1gFnSc4	výprava
tvořily	tvořit	k5eAaImAgFnP	tvořit
Karolína	Karolína	k1gFnSc1	Karolína
Erbanová	Erbanová	k1gFnSc1	Erbanová
(	(	kIx(	(
<g/>
500	[number]	k4	500
m	m	kA	m
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
m	m	kA	m
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
(	(	kIx(	(
<g/>
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
m	m	kA	m
<g/>
;	;	kIx,	;
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
odhlásil	odhlásit	k5eAaPmAgMnS	odhlásit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muži	muž	k1gMnPc1	muž
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
23	[number]	k4	23
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
14	[number]	k4	14
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
družstev	družstvo	k1gNnPc2	družstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
osm	osm	k4xCc1	osm
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ženy	žena	k1gFnPc1	žena
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
22	[number]	k4	22
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
18	[number]	k4	18
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
16	[number]	k4	16
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
družstev	družstvo	k1gNnPc2	družstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
osm	osm	k4xCc1	osm
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
zemí	zem	k1gFnPc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
2013	[number]	k4	2013
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc5	Wikimedium
Commons	Commons	k1gInSc1	Commons
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInPc1d1	oficiální
výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
isuresults	isuresults	k1gInSc1	isuresults
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
</s>
</p>
