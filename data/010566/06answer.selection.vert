<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
I.	I.	kA	I.
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1769	[number]	k4	1769
Ajaccio	Ajaccio	k1gNnSc4	Ajaccio
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1821	[number]	k4	1821
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
letech	let	k1gInPc6	let
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
a	a	k8xC	a
poté	poté	k6eAd1	poté
sto	sto	k4xCgNnSc1	sto
dní	den	k1gInPc2	den
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
