<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
(	(	kIx(	(
<g/>
civilním	civilní	k2eAgNnSc7d1	civilní
jménem	jméno	k1gNnSc7	jméno
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Romanov	Romanov	k1gInSc1	Romanov
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přezdíván	přezdívat	k5eAaImNgMnS	přezdívat
Nicky	nicka	k1gFnSc2	nicka
nebo	nebo	k8xC	nebo
Niki	Nik	k1gFnSc2	Nik
<g/>
;	;	kIx,	;
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
Jekatěrinburg	Jekatěrinburg	k1gMnSc1	Jekatěrinburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
ruský	ruský	k2eAgMnSc1d1	ruský
imperátor	imperátor	k1gMnSc1	imperátor
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
a	a	k8xC	a
finský	finský	k2eAgMnSc1d1	finský
velkokníže	velkokníže	k1gMnSc1	velkokníže
<g/>
.	.	kIx.	.
</s>
