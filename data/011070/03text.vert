<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
(	(	kIx(	(
<g/>
civilním	civilní	k2eAgNnSc7d1	civilní
jménem	jméno	k1gNnSc7	jméno
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Romanov	Romanov	k1gInSc1	Romanov
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přezdíván	přezdívat	k5eAaImNgMnS	přezdívat
Nicky	nicka	k1gFnSc2	nicka
nebo	nebo	k8xC	nebo
Niki	Nik	k1gFnSc2	Nik
<g/>
;	;	kIx,	;
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
Jekatěrinburg	Jekatěrinburg	k1gMnSc1	Jekatěrinburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
ruský	ruský	k2eAgMnSc1d1	ruský
imperátor	imperátor	k1gMnSc1	imperátor
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
a	a	k8xC	a
finský	finský	k2eAgMnSc1d1	finský
velkokníže	velkokníže	k1gMnSc1	velkokníže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
přinucen	přinutit	k5eAaPmNgInS	přinutit
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svojí	svůj	k3xOyFgFnSc7	svůj
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
rodinou	rodina	k1gFnSc7	rodina
zavražděn	zavražděn	k2eAgInSc1d1	zavražděn
bolševiky	bolševik	k1gMnPc7	bolševik
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
za	za	k7c4	za
svaté	svatá	k1gFnPc4	svatá
a	a	k8xC	a
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
masakru	masakr	k1gInSc3	masakr
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
Chrám	chrám	k1gInSc1	chrám
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
na	na	k7c6	na
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
caru	car	k1gMnSc3	car
Alexandrovi	Alexandr	k1gMnSc3	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
carevně	carevna	k1gFnSc3	carevna
Marii	Maria	k1gFnSc3	Maria
Fjodorovně	Fjodorovna	k1gFnSc3	Fjodorovna
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc3d1	rozená
jako	jako	k8xC	jako
dánská	dánský	k2eAgFnSc1d1	dánská
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ruským	ruský	k2eAgMnSc7d1	ruský
carem	car	k1gMnSc7	car
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Alix	Alix	k1gInSc4	Alix
Hessenskou	Hessenský	k2eAgFnSc7d1	Hessenská
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přijala	přijmout	k5eAaPmAgFnS	přijmout
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
jméno	jméno	k1gNnSc1	jméno
Alexandra	Alexandr	k1gMnSc2	Alexandr
Fjodorovna	Fjodorovna	k1gFnSc1	Fjodorovna
a	a	k8xC	a
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
na	na	k7c6	na
pravoslaví	pravoslaví	k1gNnSc6	pravoslaví
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
carskému	carský	k2eAgInSc3d1	carský
páru	pár	k1gInSc3	pár
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Olga	Olga	k1gFnSc1	Olga
Nikolajevna	Nikolajevna	k1gFnSc1	Nikolajevna
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
dítě	dítě	k1gNnSc1	dítě
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
.	.	kIx.	.
</s>
<s>
Carevna	carevna	k1gFnSc1	carevna
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
přála	přát	k5eAaImAgFnS	přát
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
další	další	k2eAgFnSc1d1	další
dcera	dcera	k1gFnSc1	dcera
–	–	k?	–
velkokněžna	velkokněžna	k1gFnSc1	velkokněžna
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
dcera	dcera	k1gFnSc1	dcera
jménem	jméno	k1gNnSc7	jméno
Anastázie	Anastázie	k1gFnSc2	Anastázie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carevna	carevna	k1gFnSc1	carevna
s	s	k7c7	s
carem	car	k1gMnSc7	car
zoufale	zoufale	k6eAd1	zoufale
toužili	toužit	k5eAaImAgMnP	toužit
mít	mít	k5eAaImF	mít
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
přání	přání	k1gNnSc1	přání
splnilo	splnit	k5eAaPmAgNnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
carevič	carevič	k1gMnSc1	carevič
Alexej	Alexej	k1gMnSc1	Alexej
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Alexeje	Alexej	k1gMnSc2	Alexej
se	se	k3xPyFc4	se
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
projevila	projevit	k5eAaPmAgFnS	projevit
závažná	závažný	k2eAgFnSc1d1	závažná
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
genetická	genetický	k2eAgFnSc1d1	genetická
porucha	porucha	k1gFnSc1	porucha
krevní	krevní	k2eAgFnSc2d1	krevní
srážlivosti	srážlivost	k1gFnSc2	srážlivost
–	–	k?	–
hemofilie	hemofilie	k1gFnSc2	hemofilie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
carevny	carevna	k1gFnSc2	carevna
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
sama	sám	k3xTgFnSc1	sám
carevna	carevna	k1gFnSc1	carevna
touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
netrpěla	trpět	k5eNaImAgFnS	trpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
revoluce	revoluce	k1gFnSc1	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
stále	stále	k6eAd1	stále
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
vzestup	vzestup	k1gInSc1	vzestup
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
půdy	půda	k1gFnSc2	půda
drželi	držet	k5eAaImAgMnP	držet
stále	stále	k6eAd1	stále
velkostatkáři	velkostatkář	k1gMnPc1	velkostatkář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
využívali	využívat	k5eAaImAgMnP	využívat
drobné	drobný	k2eAgMnPc4d1	drobný
rolníky	rolník	k1gMnPc4	rolník
k	k	k7c3	k
levným	levný	k2eAgFnPc3d1	levná
námezdním	námezdní	k2eAgFnPc3d1	námezdní
pracím	práce	k1gFnPc3	práce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
k	k	k7c3	k
selským	selský	k2eAgMnPc3d1	selský
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
probíhala	probíhat	k5eAaImAgFnS	probíhat
rusko-japonská	ruskoaponský	k2eAgFnSc1d1	rusko-japonská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
odhalení	odhalení	k1gNnSc1	odhalení
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
slabosti	slabost	k1gFnSc2	slabost
ruského	ruský	k2eAgNnSc2d1	ruské
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
krvavé	krvavý	k2eAgFnSc3d1	krvavá
neděli	neděle	k1gFnSc3	neděle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
carské	carský	k2eAgInPc1d1	carský
úřady	úřad	k1gInPc1	úřad
nechaly	nechat	k5eAaPmAgInP	nechat
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
davů	dav	k1gInPc2	dav
bouřících	bouřící	k2eAgNnPc2d1	bouřící
se	se	k3xPyFc4	se
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
vzpoura	vzpoura	k1gFnSc1	vzpoura
na	na	k7c6	na
křižníku	křižník	k1gInSc6	křižník
Potěmkin	Potěmkin	k1gMnSc1	Potěmkin
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carská	carský	k2eAgFnSc1d1	carská
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
poklidný	poklidný	k2eAgInSc4d1	poklidný
život	život	k1gInSc4	život
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
než	než	k8xS	než
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jej	on	k3xPp3gMnSc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Pjotr	Pjotr	k1gMnSc1	Pjotr
Durnovo	Durnův	k2eAgNnSc1d1	Durnovo
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
carevna	carevna	k1gFnSc1	carevna
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
sama	sám	k3xTgFnSc1	sám
o	o	k7c4	o
Alexeje	Alexej	k1gMnSc4	Alexej
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dcerami	dcera	k1gFnPc7	dcera
pečovaly	pečovat	k5eAaImAgFnP	pečovat
v	v	k7c6	v
Carské	carský	k2eAgFnSc6d1	carská
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Carském	carský	k2eAgNnSc6d1	carské
Selu	selo	k1gNnSc6	selo
o	o	k7c4	o
raněné	raněný	k2eAgMnPc4d1	raněný
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
asi	asi	k9	asi
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
demonstrace	demonstrace	k1gFnPc1	demonstrace
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
a	a	k8xC	a
přerostly	přerůst	k5eAaPmAgFnP	přerůst
v	v	k7c4	v
Únorovou	únorový	k2eAgFnSc4d1	únorová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
demonstrantům	demonstrant	k1gMnPc3	demonstrant
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
i	i	k9	i
vojsko	vojsko	k1gNnSc1	vojsko
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
dvanáctičlenný	dvanáctičlenný	k2eAgInSc1d1	dvanáctičlenný
prozatímní	prozatímní	k2eAgInSc1d1	prozatímní
výbor	výbor	k1gInSc1	výbor
Státní	státní	k2eAgFnSc2d1	státní
dumy	duma	k1gFnSc2	duma
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
kníže	kníže	k1gMnSc1	kníže
Georgij	Georgij	k1gFnSc2	Georgij
Lvov	Lvov	k1gInSc1	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
moc	moc	k1gFnSc4	moc
však	však	k9	však
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
držel	držet	k5eAaImAgMnS	držet
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
Petrohradský	petrohradský	k2eAgInSc1d1	petrohradský
sovět	sovět	k1gInSc1	sovět
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
menševiků	menševik	k1gMnPc2	menševik
<g/>
,	,	kIx,	,
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
dělnických	dělnický	k2eAgMnPc2d1	dělnický
<g/>
,	,	kIx,	,
rolnických	rolnický	k2eAgMnPc2d1	rolnický
a	a	k8xC	a
vojenských	vojenský	k2eAgMnPc2d1	vojenský
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
ustavovaly	ustavovat	k5eAaImAgInP	ustavovat
Sověty	Sověty	k1gInPc1	Sověty
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
ruských	ruský	k2eAgNnPc6d1	ruské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
car	car	k1gMnSc1	car
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
korunu	koruna	k1gFnSc4	koruna
svému	svůj	k3xOyFgMnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
Michailovi	Michail	k1gMnSc3	Michail
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
určil	určit	k5eAaPmAgInS	určit
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
skončila	skončit	k5eAaPmAgFnS	skončit
vláda	vláda	k1gFnSc1	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Holstein	Holstein	k1gInSc1	Holstein
<g/>
-	-	kIx~	-
<g/>
Gottorp	Gottorp	k1gInSc1	Gottorp
<g/>
-	-	kIx~	-
<g/>
Romanov	Romanov	k1gInSc1	Romanov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
carská	carský	k2eAgFnSc1d1	carská
rodina	rodina	k1gFnSc1	rodina
deportována	deportován	k2eAgFnSc1d1	deportována
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1917	[number]	k4	1917
strávili	strávit	k5eAaPmAgMnP	strávit
v	v	k7c6	v
Tobolsku	Tobolsko	k1gNnSc6	Tobolsko
s	s	k7c7	s
několika	několik	k4yIc7	několik
dvorními	dvorní	k2eAgFnPc7d1	dvorní
dámami	dáma	k1gFnPc7	dáma
a	a	k8xC	a
služebnictvem	služebnictvo	k1gNnSc7	služebnictvo
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
s	s	k7c7	s
carskou	carský	k2eAgFnSc7d1	carská
rodinou	rodina	k1gFnSc7	rodina
odjeli	odjet	k5eAaPmAgMnP	odjet
dobrovolně	dobrovolně	k6eAd1	dobrovolně
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
i	i	k9	i
bolševická	bolševický	k2eAgFnSc1d1	bolševická
říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zavraždění	zavraždění	k1gNnSc4	zavraždění
cara	car	k1gMnSc2	car
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
odvezli	odvézt	k5eAaPmAgMnP	odvézt
bolševici	bolševik	k1gMnPc1	bolševik
nejprve	nejprve	k6eAd1	nejprve
cara	car	k1gMnSc2	car
s	s	k7c7	s
carevnou	carevna	k1gFnSc7	carevna
a	a	k8xC	a
potom	potom	k6eAd1	potom
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
uralského	uralský	k2eAgNnSc2d1	uralský
města	město	k1gNnSc2	město
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejich	jejich	k3xOp3gMnSc1	jejich
poručníkem	poručník	k1gMnSc7	poručník
bolševik	bolševik	k1gMnSc1	bolševik
Jakov	Jakov	k1gInSc4	Jakov
Jurovskij	Jurovskij	k1gFnSc2	Jurovskij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
bývalý	bývalý	k2eAgMnSc1d1	bývalý
panovník	panovník	k1gMnSc1	panovník
stal	stát	k5eAaPmAgMnS	stát
figurkou	figurka	k1gFnSc7	figurka
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
bolševické	bolševický	k2eAgFnSc2d1	bolševická
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
nechvalně	chvalně	k6eNd1	chvalně
proslulé	proslulý	k2eAgFnPc1d1	proslulá
Čeky	čeka	k1gFnPc1	čeka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
provokatér	provokatér	k1gMnSc1	provokatér
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
carské	carský	k2eAgFnSc3d1	carská
rodině	rodina	k1gFnSc3	rodina
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
bolševické	bolševický	k2eAgFnSc2d1	bolševická
internace	internace	k1gFnSc2	internace
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
výrazného	výrazný	k2eAgMnSc2d1	výrazný
člena	člen	k1gMnSc2	člen
monarchistického	monarchistický	k2eAgNnSc2d1	monarchistické
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
mu	on	k3xPp3gMnSc3	on
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
prakticky	prakticky	k6eAd1	prakticky
podepsán	podepsán	k2eAgInSc1d1	podepsán
rozsudek	rozsudek	k1gInSc1	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
středu	střed	k1gInSc6	střed
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
přišlo	přijít	k5eAaPmAgNnS	přijít
popravčí	popravčí	k2eAgNnSc1d1	popravčí
komando	komando	k1gNnSc1	komando
<g/>
,	,	kIx,	,
odvedlo	odvést	k5eAaPmAgNnS	odvést
carskou	carský	k2eAgFnSc4d1	carská
rodinu	rodina	k1gFnSc4	rodina
do	do	k7c2	do
suterénu	suterén	k1gInSc2	suterén
Ipaťjevova	Ipaťjevův	k2eAgInSc2d1	Ipaťjevův
domu	dům	k1gInSc2	dům
a	a	k8xC	a
včetně	včetně	k7c2	včetně
cara	car	k1gMnSc2	car
všechny	všechen	k3xTgMnPc4	všechen
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Vrahem	vrah	k1gMnSc7	vrah
cara	car	k1gMnSc2	car
byl	být	k5eAaImAgMnS	být
čekista	čekista	k1gMnSc1	čekista
Jakov	Jakov	k1gInSc4	Jakov
Jurovskij	Jurovskij	k1gFnSc2	Jurovskij
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poté	poté	k6eAd1	poté
ihned	ihned	k6eAd1	ihned
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
i	i	k9	i
careviče	carevič	k1gMnSc4	carevič
Alexeje	Alexej	k1gMnSc4	Alexej
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Taťánu	Taťána	k1gFnSc4	Taťána
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
však	však	k9	však
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
osvobodily	osvobodit	k5eAaPmAgFnP	osvobodit
Jekatěrinburg	Jekatěrinburg	k1gInSc4	Jekatěrinburg
jednotky	jednotka	k1gFnSc2	jednotka
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
přišly	přijít	k5eAaPmAgFnP	přijít
však	však	k9	však
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
pro	pro	k7c4	pro
cara	car	k1gMnSc4	car
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byli	být	k5eAaImAgMnP	být
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
i	i	k9	i
carův	carův	k2eAgMnSc1d1	carův
bratr	bratr	k1gMnSc1	bratr
Michail	Michail	k1gMnSc1	Michail
a	a	k8xC	a
carevnina	carevnin	k2eAgFnSc1d1	carevnin
sestra	sestra	k1gFnSc1	sestra
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
Ella	Ella	k1gFnSc1	Ella
<g/>
)	)	kIx)	)
–	–	k?	–
žena	žena	k1gFnSc1	žena
carova	carův	k2eAgMnSc2d1	carův
strýce	strýc	k1gMnSc2	strýc
Sergeje	Sergej	k1gMnSc2	Sergej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tituly	titul	k1gInPc1	titul
<g/>
,	,	kIx,	,
oslovení	oslovení	k1gNnSc1	oslovení
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tituly	titul	k1gInPc1	titul
a	a	k8xC	a
oslovení	oslovení	k1gNnSc1	oslovení
===	===	k?	===
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1881	[number]	k4	1881
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
Císařská	císařský	k2eAgFnSc1d1	císařská
Výsost	výsost	k1gFnSc1	výsost
velkokníže	velkokníže	k1gMnSc1	velkokníže
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Ruský	ruský	k2eAgMnSc1d1	ruský
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1881	[number]	k4	1881
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
Císařská	císařský	k2eAgFnSc1d1	císařská
Výsost	výsost	k1gFnSc1	výsost
ruský	ruský	k2eAgInSc4d1	ruský
carevič	carevič	k1gMnSc1	carevič
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gNnSc1	jeho
Císařské	císařský	k2eAgNnSc1d1	císařské
Veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
</s>
</p>
<p>
<s>
===	===	k?	===
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Domácí	domácí	k2eAgMnPc1d1	domácí
====	====	k?	====
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Ondřeje	Ondřej	k1gMnSc2	Ondřej
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgMnSc2d1	bílý
orla	orel	k1gMnSc2	orel
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Stanislava	Stanislav	k1gMnSc2	Stanislav
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Vladimíra	Vladimír	k1gMnSc2	Vladimír
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HERESCHOVÁ	HERESCHOVÁ	kA	HERESCHOVÁ
<g/>
,	,	kIx,	,
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
:	:	kIx,	:
Dialog	dialog	k1gInSc1	dialog
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
487	[number]	k4	487
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IZAKOVIČ	IZAKOVIČ	kA	IZAKOVIČ
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
:	:	kIx,	:
tragédie	tragédie	k1gFnPc1	tragédie
posledního	poslední	k2eAgMnSc2d1	poslední
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
537	[number]	k4	537
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
svět	svět	k1gInSc4	svět
:	:	kIx,	:
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
505	[number]	k4	505
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
793	[number]	k4	793
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deník	deník	k1gInSc1	deník
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Aleš	Aleš	k1gMnSc1	Aleš
Nesý	Nesý	k1gMnSc1	Nesý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Plamja	Plamja	k1gMnSc1	Plamja
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
491	[number]	k4	491
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRECLÍK	preclík	k1gInSc1	preclík
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
legie	legie	k1gFnSc1	legie
<g/>
,	,	kIx,	,
váz	váza	k1gFnPc2	váza
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
219	[number]	k4	219
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paris	Paris	k1gMnSc1	Paris
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
2379	[number]	k4	2379
(	(	kIx(	(
<g/>
734	[number]	k4	734
01	[number]	k4	01
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87173	[number]	k4	87173
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
30	[number]	k4	30
<g/>
,	,	kIx,	,
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
149	[number]	k4	149
</s>
</p>
<p>
<s>
RADZINSKIJ	RADZINSKIJ	kA	RADZINSKIJ
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgMnSc1d1	poslední
car	car	k1gMnSc1	car
:	:	kIx,	:
zavraždění	zavraždění	k1gNnSc1	zavraždění
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-204-0411-2	[number]	k4	80-204-0411-2
</s>
</p>
<p>
<s>
SOKOLOV	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Aleksejevič	Aleksejevič	k1gMnSc1	Aleksejevič
<g/>
.	.	kIx.	.
</s>
<s>
Zavraždění	zavraždění	k1gNnSc1	zavraždění
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Konstantin	Konstantin	k1gMnSc1	Konstantin
Kessler	Kessler	k1gMnSc1	Kessler
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
298	[number]	k4	298
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
