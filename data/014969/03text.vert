<s>
James	James	k1gMnSc1
Bowie	Bowie	k1gFnSc2
</s>
<s>
James	James	k1gMnSc1
Bowie	Bowie	k1gFnSc2
Narození	narození	k1gNnPc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1796	#num#	k4
<g/>
Logan	Logany	k1gInPc2
County	Counta	k1gFnSc2
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1836	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Alamo	Alama	k1gFnSc5
Mission	Mission	k1gInSc4
in	in	k?
San	San	k1gFnSc2
Antonio	Antonio	k1gMnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
zabitý	zabitý	k1gMnSc1
v	v	k7c6
boji	boj	k1gInSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
otrokář	otrokář	k1gMnSc1
<g/>
,	,	kIx,
scientific	scientific	k1gMnSc1
explorer	explorer	k1gMnSc1
a	a	k8xC
botanik	botanik	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Rezin	Rezin	k1gInSc1
Bowie	Bowie	k1gFnSc2
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
Bowie	Bowie	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1796	#num#	k4
Kentucky	Kentucky	k1gFnSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1836	#num#	k4
Alamo	Alama	k1gFnSc5
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
dobrodruh	dobrodruh	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
Texaské	texaský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
farmářské	farmářský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
z	z	k7c2
Kentucky	Kentucka	k1gFnSc2
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Louisiany	Louisiana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
živil	živit	k5eAaImAgInS
se	s	k7c7
převážně	převážně	k6eAd1
pozemkovými	pozemkový	k2eAgFnPc7d1
spekulacemi	spekulace	k1gFnPc7
a	a	k8xC
obchodováním	obchodování	k1gNnSc7
s	s	k7c7
otroky	otrok	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1827	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Vidalii	Vidalie	k1gFnSc6
zúčastnil	zúčastnit	k5eAaPmAgMnS
souboje	souboj	k1gInSc2
zvaného	zvaný	k2eAgInSc2d1
Sandbar	Sandbar	k1gInSc1
Fight	Fight	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
zabil	zabít	k5eAaPmAgMnS
místního	místní	k2eAgMnSc4d1
šerifa	šerif	k1gMnSc4
Norrise	Norris	k1gMnSc4
Wrighta	Wright	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použil	použít	k5eAaPmAgMnS
při	při	k7c6
tom	ten	k3xDgNnSc6
speciální	speciální	k2eAgInSc1d1
dlouhý	dlouhý	k2eAgInSc4d1
soubojový	soubojový	k2eAgInSc4d1
nůž	nůž	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
dostal	dostat	k5eAaPmAgInS
označení	označení	k1gNnSc4
Bowie	Bowie	k1gFnSc2
knife	knife	k?
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
důležitou	důležitý	k2eAgFnSc7d1
výbavou	výbava	k1gFnSc7
amerických	americký	k2eAgMnPc2d1
hraničářů	hraničář	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1830	#num#	k4
žil	žít	k5eAaImAgMnS
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
součástí	součást	k1gFnSc7
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
sloužil	sloužit	k5eAaImAgMnS
u	u	k7c2
Texas	Texas	kA
Rangers	Rangers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
vůdčím	vůdčí	k2eAgFnPc3d1
postavám	postava	k1gFnPc3
anglosaských	anglosaský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1835	#num#	k4
vzbouřili	vzbouřit	k5eAaPmAgMnP
proti	proti	k7c3
centrální	centrální	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
generála	generál	k1gMnSc2
Santa	Sant	k1gMnSc2
Anny	Anna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velel	velet	k5eAaImAgMnS
texaským	texaský	k2eAgFnPc3d1
milicím	milice	k1gFnPc3
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Concepciónu	Concepción	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1835	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
porazily	porazit	k5eAaPmAgFnP
mexickou	mexický	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1836	#num#	k4
se	s	k7c7
skupinou	skupina	k1gFnSc7
povstalců	povstalec	k1gMnPc2
obsadil	obsadit	k5eAaPmAgMnS
misii	misie	k1gFnSc3
Alamo	Alama	k1gFnSc5
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
následně	následně	k6eAd1
obklíčily	obklíčit	k5eAaPmAgFnP
vládní	vládní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
třinácti	třináct	k4xCc2
dnech	den	k1gInPc6
bojů	boj	k1gInPc2
Bowie	Bowie	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
dopoledne	dopoledne	k1gNnSc2
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
a	a	k8xC
misie	misie	k1gFnSc1
byla	být	k5eAaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
Bowie	Bowie	k1gFnSc2
stal	stát	k5eAaPmAgMnS
americkým	americký	k2eAgMnSc7d1
národním	národní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenován	k2eAgNnSc1d1
město	město	k1gNnSc1
Bowie	Bowie	k1gFnSc2
v	v	k7c6
texaském	texaský	k2eAgNnSc6d1
Montague	Montague	k1gNnSc6
County	Counta	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
jeho	jeho	k3xOp3gInPc6
osudech	osud	k1gInPc6
vznikl	vzniknout	k5eAaPmAgInS
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
The	The	k1gMnSc1
Adventures	Adventures	k1gMnSc1
of	of	k?
Jim	on	k3xPp3gMnPc3
Bowie	Bowie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rockový	rockový	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
David	David	k1gMnSc1
Bowie	Bowie	k1gFnSc1
(	(	kIx(
<g/>
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
David	David	k1gMnSc1
Robert	Robert	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
)	)	kIx)
přijal	přijmout	k5eAaPmAgMnS
Bowieho	Bowie	k1gMnSc4
příjmení	příjmení	k1gNnSc2
jako	jako	k8xS,k8xC
umělecký	umělecký	k2eAgInSc4d1
pseudonym	pseudonym	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Bowieho	Bowieze	k6eAd1
nůž	nůž	k1gInSc1
</s>
<s>
Dobové	dobový	k2eAgNnSc1d1
vyobrazení	vyobrazení	k1gNnSc1
bitvy	bitva	k1gFnSc2
o	o	k7c6
Alamo	Alama	k1gFnSc5
</s>
<s>
Bowieho	Bowieze	k6eAd1
pomník	pomník	k1gInSc4
ve	v	k7c6
městě	město	k1gNnSc6
Texarkana	Texarkan	k1gMnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Time	Time	k1gNnSc1
<g/>
4	#num#	k4
<g/>
men	men	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Má	mít	k5eAaImIp3nS
ulice	ulice	k1gFnSc1
nést	nést	k5eAaImF
jméno	jméno	k1gNnSc4
Davida	David	k1gMnSc2
Bowieho	Bowie	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Austin	Austin	k1gInSc1
žije	žít	k5eAaImIp3nS
sporem	spor	k1gInSc7
o	o	k7c4
název	název	k1gInSc4
bulváru	bulvár	k1gInSc2
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Spartacus	Spartacus	k1gMnSc1
Educational	Educational	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
123949572	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8060	#num#	k4
4779	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81078858	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
6257314	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81078858	#num#	k4
</s>
