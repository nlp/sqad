<s>
42	#num#	k4
(	(	kIx(
<g/>
odpověď	odpověď	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
42	#num#	k4
je	být	k5eAaImIp3nS
častý	častý	k2eAgInSc4d1
citát	citát	k1gInSc4
ze	z	k7c2
sci-fi	sci-fi	k1gFnSc2
románu	román	k1gInSc2
Stopařův	stopařův	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
Galaxii	galaxie	k1gFnSc6
od	od	k7c2
britského	britský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Douglase	Douglasa	k1gFnSc6
Adamse	Adams	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
románu	román	k1gInSc6
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
uváděno	uvádět	k5eAaImNgNnS
jako	jako	k8xC,k8xS
odpověď	odpověď	k1gFnSc1
na	na	k7c4
základní	základní	k2eAgFnSc4d1
otázku	otázka	k1gFnSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
vesmíru	vesmír	k1gInSc2
a	a	k8xC
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
Hledání	hledání	k1gNnSc1
odpovědi	odpověď	k1gFnSc2
</s>
<s>
Ve	v	k7c6
světě	svět	k1gInSc6
Stopařova	stopařův	k2eAgMnSc2d1
průvodce	průvodce	k1gMnSc2
po	po	k7c6
galaxii	galaxie	k1gFnSc6
si	se	k3xPyFc3
myši	myš	k1gFnPc4
(	(	kIx(
<g/>
pandimenzionální	pandimenzionální	k2eAgFnPc4d1
to	ten	k3xDgNnSc1
bytosti	bytost	k1gFnPc1
<g/>
)	)	kIx)
položily	položit	k5eAaPmAgFnP
za	za	k7c4
úkol	úkol	k1gInSc4
najít	najít	k5eAaPmF
odpověď	odpověď	k1gFnSc4
na	na	k7c4
otázku	otázka	k1gFnSc4
Života	život	k1gInSc2
<g/>
,	,	kIx,
Vesmíru	vesmír	k1gInSc2
a	a	k8xC
vůbec	vůbec	k9
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
vybraly	vybrat	k5eAaPmAgFnP
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
středu	střed	k1gInSc2
dva	dva	k4xCgInPc1
nejlepší	dobrý	k2eAgInPc1d3
programátory	programátor	k1gInPc1
Lunkvila	Lunkvila	k1gFnSc2
a	a	k8xC
Fuka	Fuk	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
sestrojili	sestrojit	k5eAaPmAgMnP
počítač	počítač	k1gInSc4
nazvaný	nazvaný	k2eAgInSc4d1
„	„	k?
<g/>
Hlubina	hlubina	k1gFnSc1
myšlení	myšlení	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ji	on	k3xPp3gFnSc4
měl	mít	k5eAaImAgMnS
vypočítat	vypočítat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tento	tento	k3xDgInSc1
počítač	počítač	k1gInSc1
jim	on	k3xPp3gMnPc3
skutečně	skutečně	k6eAd1
po	po	k7c6
sedmi	sedm	k4xCc6
a	a	k8xC
půl	půl	k1xP
miliónu	milión	k4xCgInSc2
letech	léto	k1gNnPc6
dal	dát	k5eAaPmAgMnS
jednoduchou	jednoduchý	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
na	na	k7c4
otázku	otázka	k1gFnSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
vesmíru	vesmír	k1gInSc2
a	a	k8xC
vůbec	vůbec	k9
<g/>
:	:	kIx,
„	„	k?
<g/>
42	#num#	k4
<g/>
“	“	k?
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sám	sám	k3xTgMnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
znění	znění	k1gNnSc3
oné	onen	k3xDgFnSc2
základní	základní	k2eAgFnSc2d1
otázky	otázka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hledání	hledání	k1gNnSc1
otázky	otázka	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Naštěstí	naštěstí	k6eAd1
Hlubina	hlubina	k1gFnSc1
myšlení	myšlení	k1gNnSc2
dokázala	dokázat	k5eAaPmAgFnS
navrhnout	navrhnout	k5eAaPmF
největší	veliký	k2eAgInSc4d3
a	a	k8xC
nejmocnější	mocný	k2eAgInSc4d3
počítač	počítač	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
a	a	k8xC
bude	být	k5eAaImBp3nS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
sestrojen	sestrojen	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dokáže	dokázat	k5eAaPmIp3nS
vypočítat	vypočítat	k5eAaPmF
základní	základní	k2eAgFnSc4d1
otázku	otázka	k1gFnSc4
a	a	k8xC
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
Země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
Magrathejskou	Magrathejský	k2eAgFnSc7d1
stavební	stavební	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
Země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
skutečný	skutečný	k2eAgInSc4d1
skvost	skvost	k1gInSc4
<g/>
,	,	kIx,
využívající	využívající	k2eAgInSc4d1
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
výpočtům	výpočet	k1gInPc3
i	i	k8xC
samotné	samotný	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
žijící	žijící	k2eAgFnSc1d1
na	na	k7c6
jejím	její	k3xOp3gInSc6
povrchu	povrch	k1gInSc6
a	a	k8xC
výpočet	výpočet	k1gInSc4
jí	jíst	k5eAaImIp3nS
měl	mít	k5eAaImAgInS
trvat	trvat	k5eAaImF
10	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naneštěstí	naneštěstí	k9
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
při	při	k7c6
stavbě	stavba	k1gFnSc6
hyperprostorové	hyperprostorový	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
Prostetnikem	Prostetnik	k1gMnSc7
Vogonem	Vogon	k1gMnSc7
Jelcem	jelec	k1gMnSc7
těsně	těsně	k6eAd1
před	před	k7c7
dokončením	dokončení	k1gNnSc7
výpočtů	výpočet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
z	z	k7c2
objektů	objekt	k1gInPc2
k	k	k7c3
projektu	projekt	k1gInSc3
použitelných	použitelný	k2eAgInPc2d1
(	(	kIx(
<g/>
pokud	pokud	k8xS
nepočítáme	počítat	k5eNaImIp1nP
tyto	tento	k3xDgInPc1
tři	tři	k4xCgInPc1
další	další	k2eAgInPc1d1
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
nebyly	být	k5eNaImAgInP
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
způsobilé	způsobilý	k2eAgFnPc1d1
<g/>
,	,	kIx,
díky	dík	k1gInPc1
faktu	fakt	k1gInSc2
že	že	k8xS
ji	on	k3xPp3gFnSc4
opustili	opustit	k5eAaPmAgMnP
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
před	před	k7c7
zničením	zničení	k1gNnSc7
<g/>
:	:	kIx,
Trillian	Trillian	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
předčasně	předčasně	k6eAd1
opustila	opustit	k5eAaPmAgFnS
projekt	projekt	k1gInSc4
se	s	k7c7
Zafodem	Zafod	k1gMnSc7
Bíblbroxem	Bíblbrox	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
dvě	dva	k4xCgFnPc1
bílé	bílý	k2eAgFnPc1d1
myši	myš	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
jakožto	jakožto	k8xS
správci	správce	k1gMnSc3
projektu	projekt	k1gInSc2
nemohly	moct	k5eNaImAgFnP
výpočtu	výpočet	k1gInSc2
aktivně	aktivně	k6eAd1
zúčastnit	zúčastnit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
zachránil	zachránit	k5eAaPmAgMnS
ze	z	k7c2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Arthur	Arthur	k1gMnSc1
Dent	Dent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Arthur	Arthur	k1gMnSc1
poté	poté	k6eAd1
postupně	postupně	k6eAd1
vytahoval	vytahovat	k5eAaImAgMnS
písmena	písmeno	k1gNnPc4
namalovaná	namalovaný	k2eAgNnPc4d1
na	na	k7c4
kamínky	kamínek	k1gInPc4
z	z	k7c2
vaku	vak	k1gInSc2
z	z	k7c2
ručníku	ručník	k1gInSc2
a	a	k8xC
skládal	skládat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
otázka	otázka	k1gFnSc1
zní	znět	k5eAaImIp3nS
<g/>
,	,	kIx,
vyšlo	vyjít	k5eAaPmAgNnS
mu	on	k3xPp3gNnSc3
<g/>
:	:	kIx,
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
dostanete	dostat	k5eAaPmIp2nP
<g/>
,	,	kIx,
když	když	k8xS
vynásobíte	vynásobit	k5eAaPmIp2nP
šest	šest	k4xCc4
devíti	devět	k4xCc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledkem	výsledek	k1gInSc7
této	tento	k3xDgFnSc2
operace	operace	k1gFnSc2
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
není	být	k5eNaImIp3nS
42	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fanouškové	Fanoušek	k1gMnPc1
knihy	kniha	k1gFnSc2
posléze	posléze	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
operace	operace	k1gFnSc1
dá	dát	k5eAaPmIp3nS
správný	správný	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
v	v	k7c6
třináctkové	třináctkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
označil	označit	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
shodu	shoda	k1gFnSc4
za	za	k7c4
čistě	čistě	k6eAd1
náhodnou	náhodný	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkaz	odkaz	k1gInSc1
fenoménu	fenomén	k1gInSc2
</s>
<s>
Odpověď	odpověď	k1gFnSc1
na	na	k7c4
otázku	otázka	k1gFnSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
vesmíru	vesmír	k1gInSc2
a	a	k8xC
vůbec	vůbec	k9
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
kulturním	kulturní	k2eAgInSc7d1
fenomenem	fenomen	k1gInSc7
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
někteří	některý	k3yIgMnPc1
chatboti	chatbot	k1gMnPc1
<g/>
,	,	kIx,
vyhledávače	vyhledávač	k1gMnPc4
a	a	k8xC
kalkulátory	kalkulátor	k1gMnPc4
jsou	být	k5eAaImIp3nP
naprogramováni	naprogramovat	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
na	na	k7c4
výše	vysoce	k6eAd2
zmíněnou	zmíněný	k2eAgFnSc4d1
otázku	otázka	k1gFnSc4
odpověděli	odpovědět	k5eAaPmAgMnP
42	#num#	k4
<g/>
,	,	kIx,
například	například	k6eAd1
Google	Google	k1gInSc1
a	a	k8xC
podobně	podobně	k6eAd1
tak	tak	k9
Google	Google	k1gFnSc1
Calculator	Calculator	k1gInSc1
odpoví	odpovědět	k5eAaPmIp3nS
42	#num#	k4
na	na	k7c4
text	text	k1gInSc4
the	the	k?
answer	answer	k1gInSc1
to	ten	k3xDgNnSc4
life	lifat	k5eAaPmIp3nS
<g/>
,	,	kIx,
the	the	k?
universe	universe	k1gFnSc1
and	and	k?
everything	everything	k1gInSc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
tak	tak	k9
Wolfram	wolfram	k1gInSc1
Alpha	Alpha	k1gMnSc1
a	a	k8xC
vyhledávač	vyhledávač	k1gMnSc1
Bing	bingo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
poznámky	poznámka	k1gFnSc2
z	z	k7c2
knihy	kniha	k1gFnSc2
Koule	koule	k1gFnSc2
je	být	k5eAaImIp3nS
Hlubina	hlubina	k1gFnSc1
myšlení	myšlení	k1gNnSc2
(	(	kIx(
<g/>
Deep	Deep	k1gInSc1
Thought	Thought	k1gInSc1
<g/>
)	)	kIx)
narážkou	narážka	k1gFnSc7
na	na	k7c4
významný	významný	k2eAgInSc4d1
pornografický	pornografický	k2eAgInSc4d1
film	film	k1gInSc4
Hluboké	hluboký	k2eAgNnSc1d1
hrdlo	hrdlo	k1gNnSc1
(	(	kIx(
<g/>
Deep	Deep	k1gInSc1
Throat	Throat	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkaz	odkaz	k1gInSc1
k	k	k7c3
odpovědi	odpověď	k1gFnSc3
se	se	k3xPyFc4
také	také	k9
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavný	slavný	k2eAgInSc1d1
britský	britský	k2eAgInSc1d1
seriál	seriál	k1gInSc1
Doctor	Doctor	k1gInSc1
Who	Who	k1gFnSc1
má	mít	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
epizodu	epizoda	k1gFnSc4
nazvanou	nazvaný	k2eAgFnSc4d1
na	na	k7c4
počest	počest	k1gFnSc4
odpovědi	odpověď	k1gFnSc2
„	„	k?
<g/>
42	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
americkém	americký	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Ztraceni	ztracen	k2eAgMnPc1d1
se	se	k3xPyFc4
objevovala	objevovat	k5eAaImAgFnS
řada	řada	k1gFnSc1
čísel	číslo	k1gNnPc2
4	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
,	,	kIx,
42	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Level	level	k1gInSc1
42	#num#	k4
si	se	k3xPyFc3
vybrala	vybrat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
podle	podle	k7c2
Stopařova	stopařův	k2eAgMnSc2d1
průvodce	průvodce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Gothic	Gothic	k1gMnSc1
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
42	#num#	k4
<g/>
"	"	kIx"
kód	kód	k1gInSc1
rušící	rušící	k2eAgInPc1d1
všechny	všechen	k3xTgInPc1
aktivované	aktivovaný	k2eAgInPc1d1
cheaty	cheat	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
zadání	zadání	k1gNnSc6
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
text	text	k1gInSc1
"	"	kIx"
<g/>
What	What	k1gInSc1
was	was	k?
the	the	k?
question	question	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
42	#num#	k4
v	v	k7c6
počtech	počet	k1gInPc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
42	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
dvojkové	dvojkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
101010	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
interpretováno	interpretovat	k5eAaBmNgNnS
jako	jako	k8xC,k8xS
datum	datum	k1gNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
neoficiálnímu	oficiální	k2eNgInSc3d1,k2eAgInSc3d1
svátku	svátek	k1gInSc3
čísla	číslo	k1gNnSc2
42	#num#	k4
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Bible	bible	k1gFnSc2
rovněž	rovněž	k9
počet	počet	k1gInSc1
všech	všecek	k3xTgNnPc2
pokolení	pokolení	k1gNnPc2
mezi	mezi	k7c7
praotcem	praotec	k1gMnSc7
Židů	Žid	k1gMnPc2
Abrahámem	Abrahám	k1gMnSc7
a	a	k8xC
Ježíšem	Ježíš	k1gMnSc7
Kristem	Kristus	k1gMnSc7
(	(	kIx(
<g/>
Matouš	Matouš	k1gMnSc1
1,17	1,17	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopařův	stopařův	k2eAgInSc1d1
průvodce	průvodce	k1gMnSc4
Galaxií	galaxie	k1gFnSc7
2	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
Restaurant	restaurant	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ARGO	ARGO	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
439	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VERNON	VERNON	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc4
on	on	k3xPp3gMnSc1
earth	earth	k1gMnSc1
is	is	k?
42	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-03-07	2008-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tangent	tangenta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Number	Number	k1gInSc1
42	#num#	k4
Explained	Explained	k1gInSc1
by	by	kYmCp3nS
Douglas	Douglas	k1gInSc4
Adams	Adamsa	k1gFnPc2
-	-	kIx~
Also	Also	k6eAd1
Base	basa	k1gFnSc3
13	#num#	k4
Explained	Explained	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
h	h	k?
<g/>
2	#num#	k4
<g/>
g	g	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2005-06-28	2005-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.wolframalpha.com/input/?i=answer+to+life%2C+the+universe%2C+and+everything	http://www.wolframalpha.com/input/?i=answer+to+life%2C+the+universe%2C+and+everything	k1gInSc1
<g/>
↑	↑	k?
http://www.bing.com/search?q=the+answer+to+life+the+universe+and+everything&	http://www.bing.com/search?q=the+answer+to+life+the+universe+and+everything&	k1gInSc1
<g/>
↑	↑	k?
http://stargate.wikia.com/wiki/Quarantine#Notable_quotes	http://stargate.wikia.com/wiki/Quarantine#Notable_quotes	k1gInSc1
<g/>
↑	↑	k?
DARLINGTON	DARLINGTON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Script	Script	k2eAgInSc1d1
Doctors	Doctors	k1gInSc1
<g/>
:	:	kIx,
Chris	Chris	k1gInSc1
Chibnall	Chibnalla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doctor	Doctor	k1gMnSc1
Who	Who	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
#	#	kIx~
<g/>
381	#num#	k4
<g/>
,	,	kIx,
April	April	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Lostpedia	Lostpedium	k1gNnSc2
interview	interview	k1gNnSc2
with	with	k1gMnSc1
David	David	k1gMnSc1
Fury	Fura	k1gFnSc2
<g/>
↑	↑	k?
Mandy	Manda	k1gFnSc2
Carter	Cartra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interview	interview	k1gNnSc1
<g/>
:	:	kIx,
Mark	Mark	k1gMnSc1
King	King	k1gMnSc1
-	-	kIx~
Level	level	k1gInSc1
42	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Official	Official	k1gInSc1
site	site	k1gInSc4
<g/>
:	:	kIx,
Level	level	k1gInSc1
42	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠTRAUCH	ŠTRAUCH	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neděli	neděle	k1gFnSc6
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
svátek	svátek	k1gInSc4
číslo	číslo	k1gNnSc1
42	#num#	k4
<g/>
.	.	kIx.
root	roota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-10-05	2010-10-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deset	deset	k4xCc1
novinek	novinka	k1gFnPc2
v	v	k7c6
Ubuntu	Ubunt	k1gInSc6
10.10	10.10	k4
Maverick	Maverick	k1gMnSc1
Meerkat	Meerkat	k1gMnSc1
<g/>
.	.	kIx.
root	root	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-10-12	2010-10-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Mol	mol	k1gMnSc1
<g/>
.	.	kIx.
42	#num#	k4
-	-	kIx~
The	The	k1gMnSc1
Answer	Answer	k1gMnSc1
to	ten	k3xDgNnSc4
Life	Life	k1gNnSc4
<g/>
,	,	kIx,
The	The	k1gFnSc1
Universe	Universe	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Everything	Everything	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
:	:	kIx,
Maurice	Maurika	k1gFnSc6
Smith	Smitha	k1gFnPc2
<g/>
.	.	kIx.
178	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-0-9557137-0-5	978-0-9557137-0-5	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
42	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
42	#num#	k4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Stopařův	stopařův	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
Galaxii	galaxie	k1gFnSc6
Knihy	kniha	k1gFnSc2
</s>
<s>
Stopařův	stopařův	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
po	po	k7c6
Galaxii	galaxie	k1gFnSc6
</s>
<s>
Restaurant	restaurant	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
vesmíru	vesmír	k1gInSc2
</s>
<s>
Život	život	k1gInSc1
<g/>
,	,	kIx,
vesmír	vesmír	k1gInSc1
a	a	k8xC
vůbec	vůbec	k9
</s>
<s>
Sbohem	sbohem	k0
<g/>
,	,	kIx,
a	a	k8xC
díky	dík	k1gInPc1
za	za	k7c4
ryby	ryba	k1gFnPc4
</s>
<s>
Převážně	převážně	k6eAd1
neškodná	škodný	k2eNgFnSc1d1
</s>
<s>
Mladý	mladý	k2eAgInSc1d1
Zafod	Zafod	k1gInSc1
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
jistotu	jistota	k1gFnSc4
</s>
<s>
Ještě	ještě	k6eAd1
jednou	jeden	k4xCgFnSc7
a	a	k8xC
naposledy	naposledy	k6eAd1
stopem	stop	k1gInSc7
po	po	k7c4
galaxii	galaxie	k1gFnSc4
aneb	aneb	k?
Losos	losos	k1gMnSc1
pochybnosti	pochybnost	k1gFnSc2
Média	médium	k1gNnPc1
</s>
<s>
rozhlasový	rozhlasový	k2eAgInSc1d1
seriál	seriál	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgFnSc1d1
minisérie	minisérie	k1gFnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
film	film	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
produkci	produkce	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
verzemi	verze	k1gFnPc7
Postavy	postava	k1gFnSc2
</s>
<s>
Arthur	Arthur	k1gMnSc1
Dent	Dent	k1gMnSc1
</s>
<s>
Ford	ford	k1gInSc1
Prefect	Prefecta	k1gFnPc2
</s>
<s>
Zafod	Zafod	k1gInSc1
Bíblbrox	Bíblbrox	k1gInSc1
</s>
<s>
Marvin	Marvina	k1gFnPc2
</s>
<s>
Trillian	Trillian	k1gMnSc1
</s>
<s>
Slartibartfast	Slartibartfast	k1gFnSc1
</s>
<s>
Průvodce	průvodce	k1gMnSc1
</s>
<s>
vedlejší	vedlejší	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
rasy	rasa	k1gFnPc1
a	a	k8xC
druhy	druh	k1gInPc1
Ostatní	ostatní	k2eAgInPc1d1
<g/>
:	:	kIx,
</s>
<s>
seznam	seznam	k1gInSc1
pojmů	pojem	k1gInPc2
</s>
<s>
odpověď	odpověď	k1gFnSc1
na	na	k7c4
otázku	otázka	k1gFnSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
vesmíru	vesmír	k1gInSc2
a	a	k8xC
vůbec	vůbec	k9
</s>
<s>
h	h	k?
<g/>
2	#num#	k4
<g/>
g	g	kA
<g/>
2	#num#	k4
</s>
<s>
ručníkový	ručníkový	k2eAgInSc4d1
den	den	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
