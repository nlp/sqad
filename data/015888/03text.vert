<s>
Sulfid	sulfid	k1gInSc1
platiničitý	platiničitý	k2eAgInSc1d1
</s>
<s>
Sulfid	sulfid	k1gInSc1
platiničitý	platiničitý	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Sulfid	sulfid	k1gInSc1
platiničitý	platiničitý	k2eAgInSc1d1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Platinum	Platinum	k1gInSc1
<g/>
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
sulfide	sulfid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Platin	platina	k1gFnPc2
<g/>
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
<g/>
-sulfid	-sulfid	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
PtS	PtS	k?
<g/>
2	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
černá	černý	k2eAgFnSc1d1
pevná	pevný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
12038-21-0	12038-21-0	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
82862	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
S	s	k7c7
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
Pt	Pt	k1gFnSc1
<g/>
]	]	kIx)
<g/>
=	=	kIx~
<g/>
S	s	k7c7
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
Pt	Pt	k1gFnSc1
<g/>
.2	.2	k4
<g/>
S	s	k7c7
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
259,20	259,20	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
7,86	7,86	k4
g.	g.	k?
<g/>
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sulfid	sulfid	k1gInSc1
platiničitý	platiničitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
anorganická	anorganický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
se	s	k7c7
vzorcem	vzorec	k1gInSc7
PtS	PtS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
černá	černá	k1gFnSc1
<g/>
,	,	kIx,
nerozpustná	rozpustný	k2eNgFnSc1d1
<g/>
,	,	kIx,
polovodivá	polovodivý	k2eAgFnSc1d1
pevná	pevný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
krystalovou	krystalový	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
jodidu	jodid	k1gInSc2
kademnatého	kademnatý	k2eAgInSc2d1
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
vrstev	vrstva	k1gFnPc2
oktaedrických	oktaedrický	k2eAgInPc2d1
iontů	ion	k1gInPc2
platiničitých	platiničitý	k2eAgNnPc2d1
a	a	k8xC
pyramidálních	pyramidální	k2eAgNnPc2d1
sulfidových	sulfidový	k2eAgNnPc2d1
center	centrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monokrystaly	monokrystal	k1gInPc7
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
chemickou	chemický	k2eAgFnSc7d1
depozicí	depozice	k1gFnSc7
z	z	k7c2
plynné	plynný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
s	s	k7c7
využitím	využití	k1gNnSc7
fosforu	fosfor	k1gInSc2
jako	jako	k8xS,k8xC
transportního	transportní	k2eAgNnSc2d1
činidla	činidlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
srážením	srážení	k1gNnSc7
roztoků	roztok	k1gInPc2
PtIV	PtIV	k1gFnSc2
plynným	plynný	k2eAgInSc7d1
sulfanem	sulfan	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Platinum_disulfide	Platinum_disulfid	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
SOLED	SOLED	kA
<g/>
,	,	kIx,
S.	S.	kA
<g/>
;	;	kIx,
WOLD	WOLD	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
SYMON	SYMON	kA
<g/>
,	,	kIx,
C.	C.	kA
R.	R.	kA
Platinum	Platinum	k1gNnSc4
Disulfide	Disulfid	k1gInSc5
and	and	k?
Platinum	Platinum	k1gNnSc1
Ditelluride	Ditellurid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Duward	Duwarda	k1gFnPc2
F.	F.	kA
Shriver	Shriver	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hoboken	Hoboken	k1gInSc1
<g/>
,	,	kIx,
NJ	NJ	kA
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
470	#num#	k4
<g/>
-	-	kIx~
<g/>
13250	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4542	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
9780470132500	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
49	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
9780470132500	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GREENWOOD	GREENWOOD	kA
<g/>
,	,	kIx,
NORMAN	Norman	k1gMnSc1
NEILL	NEILL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
1	#num#	k4
<g/>
..	..	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
793	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
1	#num#	k4
příl	příl	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85427	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85427	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
320245801	#num#	k4
S.	S.	kA
1425	#num#	k4
<g/>
-	-	kIx~
<g/>
1426	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sulfidy	sulfid	k1gInPc1
s	s	k7c7
prvkem	prvek	k1gInSc7
v	v	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Sulfid	sulfid	k1gInSc1
americičitý	americičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
AmS	AmS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CeS	ces	k1gNnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
chromičitý	chromičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CrS	CrS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
germaničitý	germaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
GeS	ges	k1gNnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
HfS	HfS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
NpS	NpS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
olovičitý	olovičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PbS	PbS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
manganičitý	manganičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
MnS	MnS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
osmičitý	osmičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
OsS	OsS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
platiničitý	platiničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PtS	PtS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
plutoničitý	plutoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PuS	pusa	k1gFnPc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
protaktiničitý	protaktiničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PaS	pas	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
rutheničitý	rutheničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
RuS	Rus	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
seleničitý	seleničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SeS	SeS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SiS	SiS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
telluričitý	telluričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TeS	tes	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
thoričitý	thoričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ThS	ThS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
cíničitý	cíničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SnS	SnS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
titaničitý	titaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TiS	tis	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
wolframičitý	wolframičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
WS	WS	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
uraničitý	uraničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
US	US	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
vanadičitý	vanadičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
VS	VS	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
zirkoničitý	zirkoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ZrS	ZrS	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Také	také	k9
Sirouhlík	sirouhlík	k1gInSc1
(	(	kIx(
<g/>
CS	CS	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
