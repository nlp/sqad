<s>
Einsteinium	einsteinium	k1gNnSc1
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
11	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
254	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Es	es	kA
</s>
<s>
99	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
ve	v	k7c6
zkumavce	zkumavka	k1gFnSc6
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
<g/>
,	,	kIx,
Es	es	k1gNnSc1
<g/>
,	,	kIx,
99	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Einsteinium	einsteinium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
kovově	kovově	k6eAd1
stříbrné	stříbrný	k2eAgFnPc4d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7429-92-7	7429-92-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
254,088	254,088	k4
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
Es	es	k1gNnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
116	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Es	es	k1gNnSc7
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
98	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Es	es	k1gNnSc7
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
85	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
11	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,80	6,80	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
12,6	12,6	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
21,6	21,6	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
plošně	plošně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
kubická	kubický	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
8,84	8,84	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
860	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
133,15	133,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetické	paramagnetický	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ho	on	k3xPp3gInSc4
<g/>
⋏	⋏	k?
</s>
<s>
Kalifornium	kalifornium	k1gNnSc4
≺	≺	k?
<g/>
Es	es	k1gNnSc2
<g/>
≻	≻	k?
Fermium	fermium	k1gNnSc1
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Es	es	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedenáctý	jedenáctý	k4xOgMnSc1
člen	člen	k1gMnSc1
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
sedmý	sedmý	k4xOgInSc1
transuran	transuran	k1gInSc1
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pojmenován	pojmenovat	k5eAaPmNgMnS
na	na	k7c4
počest	počest	k1gFnSc4
teoretického	teoretický	k2eAgMnSc2d1
fyzika	fyzik	k1gMnSc2
Alberta	Albert	k1gMnSc2
Einsteina	Einstein	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
pozemské	pozemský	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
nikde	nikde	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
z	z	k7c2
řady	řada	k1gFnSc2
transuranů	transuran	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fyzikálně-chemické	Fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
doposud	doposud	k6eAd1
nebyl	být	k5eNaImAgInS
izolován	izolovat	k5eAaBmNgInS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
a	a	k8xC
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
povrchu	povrch	k1gInSc6
zvolna	zvolna	k6eAd1
oxiduje	oxidovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS
α	α	k?
a	a	k8xC
γ	γ	k?
záření	záření	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
silným	silný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
neutronů	neutron	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
za	za	k7c4
dodržování	dodržování	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
radioaktivními	radioaktivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
jeho	jeho	k3xOp3gFnPc6
sloučeninách	sloučenina	k1gFnPc6
a	a	k8xC
jejich	jejich	k3xOp3gNnSc6
chemickém	chemický	k2eAgNnSc6d1
chování	chování	k1gNnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
identifikoval	identifikovat	k5eAaBmAgMnS
einsteinium	einsteinium	k1gNnSc4
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1952	#num#	k4
na	na	k7c6
kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Berkeley	Berkeley	k1gInPc4
a	a	k8xC
současně	současně	k6eAd1
ohlásil	ohlásit	k5eAaPmAgInS
objev	objev	k1gInSc1
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
i	i	k9
G.	G.	kA
R.	R.	kA
Choppin	Choppin	k1gMnSc1
se	s	k7c7
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
z	z	k7c2
národní	národní	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
v	v	k7c4
Los	los	k1gInSc4
Alamos	Alamosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
byl	být	k5eAaImAgInS
výchozím	výchozí	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
spad	spad	k1gInSc1
po	po	k7c6
výbuchu	výbuch	k1gInSc6
termonukleární	termonukleární	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
testované	testovaný	k2eAgFnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
„	„	k?
<g/>
Operace	operace	k1gFnSc2
Ivy	Iva	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
listopadu	listopad	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikovaným	identifikovaný	k2eAgInSc7d1
izotopem	izotop	k1gInSc7
bylo	být	k5eAaImAgNnS
253	#num#	k4
<g/>
Es	es	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
jádra	jádro	k1gNnSc2
238U	238U	k4
postupným	postupný	k2eAgNnSc7d1
pohlcením	pohlcení	k1gNnSc7
15	#num#	k4
neutronů	neutron	k1gInPc2
a	a	k8xC
následnými	následný	k2eAgFnPc7d1
sedmi	sedm	k4xCc2
přeměnami	přeměna	k1gFnPc7
β	β	k?
<g/>
−	−	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
objev	objev	k1gInSc1
byl	být	k5eAaImAgInS
utajován	utajovat	k5eAaImNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
vzhledem	vzhledem	k7c3
k	k	k7c3
probíhající	probíhající	k2eAgFnSc3d1
studené	studený	k2eAgFnSc3d1
válce	válka	k1gFnSc3
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
jadernými	jaderný	k2eAgFnPc7d1
velmocemi	velmoc	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
umělá	umělý	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
einsteinia	einsteinium	k1gNnSc2
byla	být	k5eAaImAgFnS
realizována	realizovat	k5eAaBmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
Oak	Oak	k1gFnSc2
Ridge	Ridg	k1gFnSc2
National	National	k1gFnSc7
Laboratory	Laborator	k1gInPc4
v	v	k7c4
Tennessee	Tennessee	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
z	z	k7c2
přibližně	přibližně	k6eAd1
1	#num#	k4
kg	kg	kA
plutonia	plutonium	k1gNnSc2
připraveny	připravit	k5eAaPmNgInP
asi	asi	k9
3	#num#	k4
mg	mg	kA
einsteinia	einsteinium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Doposud	doposud	k6eAd1
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
19	#num#	k4
izotopů	izotop	k1gInPc2
einsteinia	einsteinium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
jsou	být	k5eAaImIp3nP
nejstabilnější	stabilní	k2eAgFnPc1d3
252	#num#	k4
<g/>
Es	es	k1gNnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
471,7	471,7	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
254	#num#	k4
<g/>
Es	es	k1gNnSc2
s	s	k7c7
poločasem	poločas	k1gInSc7
275,7	275,7	k4
dne	den	k1gInSc2
a	a	k8xC
255	#num#	k4
<g/>
Es	es	k1gNnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
39,8	39,8	k4
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
zbývající	zbývající	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
mají	mít	k5eAaImIp3nP
poločas	poločas	k1gInSc4
přeměny	přeměna	k1gFnSc2
kratší	krátký	k2eAgInSc4d2
než	než	k8xS
3	#num#	k4
dny	den	k1gInPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
viz	vidět	k5eAaImRp2nS
izotopy	izotop	k1gInPc1
einsteinia	einsteinium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Einsteinium	einsteinium	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
sloučeniny	sloučenina	k1gFnPc1
nemají	mít	k5eNaImIp3nP
pro	pro	k7c4
praxi	praxe	k1gFnSc4
žádný	žádný	k3yNgInSc4
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
k	k	k7c3
vědeckým	vědecký	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Einsteinium	einsteinium	k1gNnSc1
255	#num#	k4
<g/>
Es	es	k1gNnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
mendelevia	mendelevium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
einsteinium	einsteinium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
einsteinium	einsteinium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4151415-4	4151415-4	k4
</s>
