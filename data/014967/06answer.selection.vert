<s>
Troyská	troyský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
trojská	trojský	k2eAgFnSc1d1
<g/>
]	]	kIx)
i	i	k9
[	[	kIx(
<g/>
troaská	troaský	k2eAgFnSc1d1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
častěji	často	k6eAd2
psáno	psát	k5eAaImNgNnS
jako	jako	k8xC,k8xS
trojská	trojský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
angličtině	angličtina	k1gFnSc6
troy	troa	k1gMnSc2
weight	weight	k2eAgMnSc1d1
nebo	nebo	k8xC
troy	tro	k2eAgInPc1d1
ounce	ounec	k1gInPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc1d1
váhová	váhový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
31,1	31,1	k4
gramu	gram	k1gInSc2
<g/>
)	)	kIx)
používaná	používaný	k2eAgFnSc1d1
na	na	k7c6
národních	národní	k2eAgInPc6d1
a	a	k8xC
mezinárodních	mezinárodní	k2eAgInPc6d1
trzích	trh	k1gInPc6
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
platiny	platina	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
drahokamů	drahokam	k1gInPc2
<g/>
.	.	kIx.
</s>