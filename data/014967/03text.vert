<s>
Troyská	troyský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
</s>
<s>
1	#num#	k4
troyská	troyský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
zlata	zlato	k1gNnSc2
</s>
<s>
Troyská	troyský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
trojská	trojský	k2eAgFnSc1d1
<g/>
]	]	kIx)
i	i	k9
[	[	kIx(
<g/>
troaská	troaský	k2eAgFnSc1d1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
častěji	často	k6eAd2
psáno	psát	k5eAaImNgNnS
jako	jako	k8xC,k8xS
trojská	trojský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
angličtině	angličtina	k1gFnSc6
troy	troa	k1gMnSc2
weight	weight	k2eAgMnSc1d1
nebo	nebo	k8xC
troy	tro	k2eAgInPc1d1
ounce	ounec	k1gInPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc1d1
váhová	váhový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
31,1	31,1	k4
gramu	gram	k1gInSc2
<g/>
)	)	kIx)
používaná	používaný	k2eAgFnSc1d1
na	na	k7c6
národních	národní	k2eAgInPc6d1
a	a	k8xC
mezinárodních	mezinárodní	k2eAgInPc6d1
trzích	trh	k1gInPc6
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
platiny	platina	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
drahokamů	drahokam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
francouzského	francouzský	k2eAgNnSc2d1
města	město	k1gNnSc2
Troyes	Troyesa	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
poprvé	poprvé	k6eAd1
obchodovalo	obchodovat	k5eAaImAgNnS
již	již	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
troyskou	troyský	k2eAgFnSc4d1
unci	unce	k1gFnSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
1	#num#	k4
oz	oz	k?
t	t	k?
<g/>
,	,	kIx,
případně	případně	k6eAd1
1	#num#	k4
t	t	k?
oz	oz	k?
<g/>
,	,	kIx,
1	#num#	k4
oz	oz	k?
<g/>
.	.	kIx.
tr	tr	k?
<g/>
.	.	kIx.
apod.	apod.	kA
Podle	podle	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
normy	norma	k1gFnSc2
ISO	ISO	kA
4217	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
definuje	definovat	k5eAaBmIp3nS
zkratky	zkratka	k1gFnPc1
pro	pro	k7c4
měny	měna	k1gFnPc4
<g/>
,	,	kIx,
je	on	k3xPp3gFnPc4
pro	pro	k7c4
drahé	drahý	k2eAgInPc4d1
kovy	kov	k1gInPc4
vážené	vážený	k2eAgInPc4d1
v	v	k7c6
troyských	troyský	k2eAgFnPc6d1
uncích	unce	k1gFnPc6
určena	určen	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
tvořená	tvořený	k2eAgFnSc1d1
písmenem	písmeno	k1gNnSc7
X	X	kA
následovaným	následovaný	k2eAgNnSc7d1
dvěma	dva	k4xCgFnPc7
písmeny	písmeno	k1gNnPc7
chemické	chemický	k2eAgFnPc4d1
značky	značka	k1gFnPc4
kovu	kov	k1gInSc2
<g/>
:	:	kIx,
XAG	XAG	kA
–	–	k?
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
XAU	XAU	kA
zlato	zlato	k1gNnSc1
atd.	atd.	kA
</s>
<s>
1	#num#	k4
troyská	troyský	k2eAgFnSc1d1
unce	unce	k1gFnSc1
=	=	kIx~
31,103	31,103	k4
<g/>
4768	#num#	k4
gramů	gram	k1gInPc2
</s>
<s>
1	#num#	k4
troyská	troyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
=	=	kIx~
12	#num#	k4
troyských	troyský	k2eAgFnPc2d1
uncí	unce	k1gFnPc2
=	=	kIx~
373,241	373,241	k4
<g/>
7216	#num#	k4
gramů	gram	k1gInPc2
</s>
<s>
1	#num#	k4
kilogram	kilogram	k1gInSc1
=	=	kIx~
32,15	32,15	k4
troyských	troyský	k2eAgFnPc2d1
uncí	unce	k1gFnPc2
(	(	kIx(
<g/>
zaokrouhleně	zaokrouhleně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
175	#num#	k4
troyských	troyský	k2eAgFnPc2d1
uncí	unce	k1gFnPc2
=	=	kIx~
192	#num#	k4
uncí	unce	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
1998	#num#	k4
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
982	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
↑	↑	k?
KALABIS	KALABIS	kA
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
z	z	k7c2
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaše	váš	k3xOp2gInPc4
peníze	peníz	k1gInPc4
uloží	uložit	k5eAaPmIp3nS
nebo	nebo	k8xC
zhodnotí	zhodnotit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měšec	měšec	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
2017-05-18	2017-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
4414	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Karát	karát	k1gInSc1
(	(	kIx(
<g/>
ryzost	ryzost	k1gFnSc1
<g/>
)	)	kIx)
ryzost	ryzost	k1gFnSc1
zlata	zlato	k1gNnSc2
</s>
<s>
Lot	lot	k1gInSc1
(	(	kIx(
<g/>
ryzost	ryzost	k1gFnSc1
<g/>
)	)	kIx)
ryzost	ryzost	k1gFnSc1
stříbra	stříbro	k1gNnSc2
</s>
<s>
Drahé	drahý	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
