<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
registrováno	registrovat	k5eAaBmNgNnS	registrovat
v	v	k7c4	v
USA	USA	kA	USA
také	také	k9	také
jako	jako	k8xS	jako
Coke	Coke	k1gFnSc1	Coke
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
kola	kolo	k1gNnPc1	kolo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sladký	sladký	k2eAgInSc4d1	sladký
nápoj	nápoj	k1gInSc4	nápoj
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
bezkokainového	bezkokainový	k2eAgInSc2d1	bezkokainový
výtažku	výtažek	k1gInSc2	výtažek
koky	koka	k1gFnSc2	koka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
byl	být	k5eAaImAgInS	být
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
jej	on	k3xPp3gInSc4	on
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
The	The	k1gFnSc1	The
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
Coca-Cola	cocaola	k1gFnSc1	coca-cola
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nealkoholickým	alkoholický	k2eNgInSc7d1	nealkoholický
nápojem	nápoj	k1gInSc7	nápoj
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
víno	víno	k1gNnSc4	víno
ani	ani	k8xC	ani
kokain	kokain	k1gInSc4	kokain
jako	jako	k8xS	jako
původní	původní	k2eAgFnPc4d1	původní
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
je	být	k5eAaImIp3nS	být
sycená	sycený	k2eAgFnSc1d1	sycená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
Albert	Albert	k1gMnSc1	Albert
Niemann	Niemann	k1gMnSc1	Niemann
poprvé	poprvé	k6eAd1	poprvé
izoloval	izolovat	k5eAaBmAgMnS	izolovat
z	z	k7c2	z
listu	list	k1gInSc2	list
koky	koka	k1gFnSc2	koka
kokain	kokain	k1gInSc1	kokain
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
Wilhelm	Wilhelmo	k1gNnPc2	Wilhelmo
Lossen	Lossen	k1gInSc1	Lossen
objevil	objevit	k5eAaPmAgInS	objevit
jeho	on	k3xPp3gInSc4	on
chemický	chemický	k2eAgInSc4d1	chemický
vzorec	vzorec	k1gInSc4	vzorec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
USA	USA	kA	USA
zahájena	zahájen	k2eAgFnSc1d1	zahájena
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
výroba	výroba	k1gFnSc1	výroba
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
korsický	korsický	k2eAgInSc1d1	korsický
chemik	chemik	k1gMnSc1	chemik
Angelo	Angela	k1gFnSc5	Angela
Mariani	Mariaň	k1gFnSc6	Mariaň
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
vin	vina	k1gFnPc2	vina
Mariani	Mariaň	k1gFnSc6	Mariaň
<g/>
,	,	kIx,	,
Marianiho	Mariani	k1gMnSc2	Mariani
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
extraktu	extrakt	k1gInSc2	extrakt
koky	koka	k1gFnSc2	koka
<g/>
.	.	kIx.	.
</s>
<s>
Doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
lahodný	lahodný	k2eAgInSc4d1	lahodný
nápoj	nápoj	k1gInSc4	nápoj
a	a	k8xC	a
medicínské	medicínský	k2eAgNnSc4d1	medicínské
tonikum	tonikum	k1gNnSc4	tonikum
pro	pro	k7c4	pro
tělo	tělo	k1gNnSc4	tělo
i	i	k8xC	i
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
dodá	dodat	k5eAaPmIp3nS	dodat
sílu	síla	k1gFnSc4	síla
přepracovaným	přepracovaný	k2eAgMnPc3d1	přepracovaný
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
zesláblým	zesláblý	k2eAgFnPc3d1	zesláblá
ženám	žena	k1gFnPc3	žena
i	i	k8xC	i
churavým	churavý	k2eAgFnPc3d1	churavá
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
posílí	posílit	k5eAaPmIp3nS	posílit
mozek	mozek	k1gInSc4	mozek
i	i	k8xC	i
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
svalům	sval	k1gInPc3	sval
sílu	síl	k1gInSc2	síl
a	a	k8xC	a
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
zajistí	zajistit	k5eAaPmIp3nS	zajistit
dobré	dobrý	k2eAgNnSc4d1	dobré
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
nápoj	nápoj	k1gInSc4	nápoj
obecně	obecně	k6eAd1	obecně
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
druhy	druh	k1gMnPc4	druh
obtíží	obtíž	k1gFnPc2	obtíž
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
popularita	popularita	k1gFnSc1	popularita
byla	být	k5eAaImAgFnS	být
i	i	k9	i
inspirací	inspirace	k1gFnSc7	inspirace
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Coca-Coly	cocaola	k1gFnSc2	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Coca-Colu	cocaola	k1gFnSc4	coca-cola
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
lékárník	lékárník	k1gMnSc1	lékárník
doktor	doktor	k1gMnSc1	doktor
John	John	k1gMnSc1	John
Stith	Stith	k1gMnSc1	Stith
Pemberton	Pemberton	k1gInSc4	Pemberton
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
grafické	grafický	k2eAgNnSc1d1	grafické
zpracování	zpracování	k1gNnSc1	zpracování
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dodnes	dodnes	k6eAd1	dodnes
používaného	používaný	k2eAgNnSc2d1	používané
loga	logo	k1gNnSc2	logo
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Pembertonův	Pembertonův	k2eAgMnSc1d1	Pembertonův
účetní	účetní	k1gMnSc1	účetní
a	a	k8xC	a
sekretář	sekretář	k1gMnSc1	sekretář
Frank	Frank	k1gMnSc1	Frank
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
nápoj	nápoj	k1gInSc1	nápoj
prodáván	prodávat	k5eAaImNgInS	prodávat
za	za	k7c4	za
5	[number]	k4	5
centů	cent	k1gInPc2	cent
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
Willisem	Willis	k1gInSc7	Willis
Venablem	Venabl	k1gMnSc7	Venabl
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
prodávalo	prodávat	k5eAaImAgNnS	prodávat
9	[number]	k4	9
nápojů	nápoj	k1gInPc2	nápoj
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
prodalo	prodat	k5eAaPmAgNnS	prodat
Coca-Coly	cocaola	k1gFnPc4	coca-cola
za	za	k7c4	za
50	[number]	k4	50
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
ani	ani	k9	ani
nevyrovnalo	vyrovnat	k5eNaPmAgNnS	vyrovnat
nákladům	náklad	k1gInPc3	náklad
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
činily	činit	k5eAaImAgFnP	činit
70	[number]	k4	70
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
ztrátový	ztrátový	k2eAgInSc1d1	ztrátový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Coca-Cola	cocaola	k1gFnSc1	coca-cola
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
dostal	dostat	k5eAaPmAgMnS	dostat
farmaceut	farmaceut	k1gMnSc1	farmaceut
Asa	Asa	k1gMnSc1	Asa
Griggs	Griggsa	k1gFnPc2	Griggsa
Candler	Candler	k1gMnSc1	Candler
<g/>
,	,	kIx,	,
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
během	během	k7c2	během
10	[number]	k4	10
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
prodej	prodej	k1gInSc1	prodej
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
tahounem	tahoun	k1gInSc7	tahoun
úspěchu	úspěch	k1gInSc2	úspěch
byla	být	k5eAaImAgFnS	být
reklama	reklama	k1gFnSc1	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
se	se	k3xPyFc4	se
prodávala	prodávat	k5eAaImAgFnS	prodávat
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
i	i	k8xC	i
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
tvar	tvar	k1gInSc1	tvar
lahve	lahev	k1gFnSc2	lahev
byl	být	k5eAaImAgInS	být
patentován	patentovat	k5eAaBmNgInS	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
lahve	lahev	k1gFnSc2	lahev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
designér	designér	k1gMnSc1	designér
Earl	earl	k1gMnSc1	earl
R.	R.	kA	R.
Dean	Dean	k1gMnSc1	Dean
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
Coca-Colu	cocaola	k1gFnSc4	coca-cola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
tvaru	tvar	k1gInSc2	tvar
kokového	kokový	k2eAgInSc2d1	kokový
listu	list	k1gInSc2	list
i	i	k8xC	i
kolového	kolový	k2eAgInSc2d1	kolový
ořechu	ořech	k1gInSc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc4d1	předchozí
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
lahví	lahev	k1gFnPc2	lahev
měly	mít	k5eAaImAgFnP	mít
klasický	klasický	k2eAgInSc4d1	klasický
válcový	válcový	k2eAgInSc4d1	válcový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
láhev	láhev	k1gFnSc1	láhev
Coca-Coly	cocaola	k1gFnSc2	coca-cola
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
muzea	muzeum	k1gNnSc2	muzeum
mapujícího	mapující	k2eAgNnSc2d1	mapující
historii	historie	k1gFnSc4	historie
firmy	firma	k1gFnPc1	firma
v	v	k7c6	v
nevadském	nevadský	k2eAgInSc6d1	nevadský
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uvedena	uvést	k5eAaPmNgFnS	uvést
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Zero	Zero	k6eAd1	Zero
<g/>
,	,	kIx,	,
propagovaná	propagovaný	k2eAgNnPc1d1	propagované
jako	jako	k8xC	jako
kola	kolo	k1gNnPc1	kolo
s	s	k7c7	s
nulovým	nulový	k2eAgInSc7d1	nulový
obsahem	obsah	k1gInSc7	obsah
cukru	cukr	k1gInSc2	cukr
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Taiwanu	Taiwan	k1gInSc2	Taiwan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
propagován	propagován	k2eAgInSc1d1	propagován
jako	jako	k8xC	jako
nápoj	nápoj	k1gInSc1	nápoj
s	s	k7c7	s
0	[number]	k4	0
kaloriemi	kalorie	k1gFnPc7	kalorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
uvedení	uvedení	k1gNnSc4	uvedení
byla	být	k5eAaImAgFnS	být
nasazena	nasazen	k2eAgFnSc1d1	nasazena
virální	virální	k2eAgFnSc1d1	virální
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
vědomě	vědomě	k6eAd1	vědomě
nepravdivé	pravdivý	k2eNgFnPc4d1	nepravdivá
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
údajné	údajný	k2eAgFnSc6d1	údajná
zvažované	zvažovaný	k2eAgFnSc6d1	zvažovaná
soudní	soudní	k2eAgFnSc6d1	soudní
žalobě	žaloba	k1gFnSc6	žaloba
ohledně	ohledně	k7c2	ohledně
napodobování	napodobování	k1gNnSc2	napodobování
příchuti	příchuť	k1gFnSc2	příchuť
tohoto	tento	k3xDgInSc2	tento
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc1	uvedení
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
propagace	propagace	k1gFnSc1	propagace
tohoto	tento	k3xDgInSc2	tento
produktu	produkt	k1gInSc2	produkt
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
podvrženou	podvržený	k2eAgFnSc7d1	podvržená
předvojovou	předvojový	k2eAgFnSc7d1	předvojová
skupinou	skupina	k1gFnSc7	skupina
<g/>
;	;	kIx,	;
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
grafitti	grafitti	k1gNnSc4	grafitti
a	a	k8xC	a
spamming	spamming	k1gInSc4	spamming
odkazující	odkazující	k2eAgInSc4d1	odkazující
na	na	k7c4	na
blog	blog	k1gInSc4	blog
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
skupina	skupina	k1gFnSc1	skupina
i	i	k8xC	i
www	www	k?	www
stránky	stránka	k1gFnSc2	stránka
se	se	k3xPyFc4	se
falešně	falešně	k6eAd1	falešně
vydávaly	vydávat	k5eAaImAgFnP	vydávat
za	za	k7c4	za
nezávislé	závislý	k2eNgInPc4d1	nezávislý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
byla	být	k5eAaImAgFnS	být
shledána	shledat	k5eAaPmNgFnS	shledat
jako	jako	k9	jako
scestná	scestný	k2eAgFnSc1d1	scestná
<g/>
.	.	kIx.	.
</s>
<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Zero	Zero	k6eAd1	Zero
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Venezuelu	Venezuela	k1gFnSc4	Venezuela
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
další	další	k2eAgFnSc2d1	další
středoamerické	středoamerický	k2eAgFnSc2d1	středoamerická
země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
zakázána	zakázat	k5eAaPmNgFnS	zakázat
pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
problematických	problematický	k2eAgNnPc2d1	problematické
umělých	umělý	k2eAgNnPc2d1	umělé
sladidel	sladidlo	k1gNnPc2	sladidlo
Acesulfamu	Acesulfam	k1gInSc2	Acesulfam
<g/>
,	,	kIx,	,
Aspartamu	aspartam	k1gInSc2	aspartam
a	a	k8xC	a
především	především	k9	především
cyklamátu	cyklamát	k1gInSc2	cyklamát
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
americkým	americký	k2eAgMnSc7d1	americký
FDA	FDA	kA	FDA
zakázán	zakázán	k2eAgInSc1d1	zakázán
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
"	"	kIx"	"
<g/>
rakovinotvorné	rakovinotvorný	k2eAgInPc4d1	rakovinotvorný
tumory	tumor	k1gInPc4	tumor
a	a	k8xC	a
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vady	vada	k1gFnPc4	vada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
byla	být	k5eAaImAgFnS	být
sponzorem	sponzor	k1gMnSc7	sponzor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zákaz	zákaz	k1gInSc4	zákaz
Coca-Coly	cocaola	k1gFnSc2	coca-cola
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
další	další	k2eAgFnSc1d1	další
středoamerická	středoamerický	k2eAgFnSc1d1	středoamerická
země	země	k1gFnSc1	země
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jeho	jeho	k3xOp3gMnSc2	jeho
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
prodejních	prodejní	k2eAgInPc2d1	prodejní
pultů	pult	k1gInPc2	pult
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Coca-cola	cocaola	k1gFnSc1	coca-cola
company	compana	k1gFnSc2	compana
a	a	k8xC	a
Americkou	americký	k2eAgFnSc4d1	americká
asociaci	asociace	k1gFnSc4	asociace
výrobců	výrobce	k1gMnPc2	výrobce
nápojů	nápoj	k1gInPc2	nápoj
podala	podat	k5eAaPmAgFnS	podat
americká	americký	k2eAgFnSc1d1	americká
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
zájmu	zájem	k1gInSc6	zájem
žalobu	žaloba	k1gFnSc4	žaloba
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klamou	klamat	k5eAaImIp3nP	klamat
zákazníky	zákazník	k1gMnPc4	zákazník
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlehčují	zlehčovat	k5eAaImIp3nP	zlehčovat
vliv	vliv	k1gInSc4	vliv
konzumace	konzumace	k1gFnSc2	konzumace
slazených	slazený	k2eAgInPc2d1	slazený
nápojů	nápoj	k1gInPc2	nápoj
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
a	a	k8xC	a
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
obezity	obezita	k1gFnSc2	obezita
a	a	k8xC	a
cukrovky	cukrovka	k1gFnSc2	cukrovka
<g/>
.	.	kIx.	.
</s>
<s>
Kauza	kauza	k1gFnSc1	kauza
má	mít	k5eAaImIp3nS	mít
potenciál	potenciál	k1gInSc4	potenciál
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
do	do	k7c2	do
již	již	k6eAd1	již
rozjeté	rozjetý	k2eAgFnSc2d1	rozjetá
tendence	tendence	k1gFnSc2	tendence
zdanit	zdanit	k5eAaPmF	zdanit
slazené	slazený	k2eAgInPc4d1	slazený
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
tvořený	tvořený	k2eAgInSc4d1	tvořený
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
ingrediencemi	ingredience	k1gFnPc7	ingredience
<g/>
,	,	kIx,	,
výtažky	výtažek	k1gInPc1	výtažek
koky	koka	k1gFnSc2	koka
a	a	k8xC	a
ořechu	ořech	k1gInSc2	ořech
koly	kola	k1gFnSc2	kola
<g/>
,	,	kIx,	,
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kokainu	kokain	k1gInSc2	kokain
a	a	k8xC	a
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
také	také	k9	také
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
však	však	k9	však
Coca-Cola	cocaola	k1gFnSc1	coca-cola
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
nealkoholická	alkoholický	k2eNgFnSc1d1	nealkoholická
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
alkoholové	alkoholový	k2eAgFnSc2d1	alkoholová
prohibice	prohibice	k1gFnSc2	prohibice
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
vyhlášené	vyhlášený	k2eAgFnSc6d1	vyhlášená
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Brzy	brzy	k6eAd1	brzy
z	z	k7c2	z
receptury	receptura	k1gFnSc2	receptura
ubylo	ubýt	k5eAaPmAgNnS	ubýt
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
Coca-Cola	cocaola	k1gFnSc1	coca-cola
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nealkoholickým	alkoholický	k2eNgInSc7d1	nealkoholický
nápojem	nápoj	k1gInSc7	nápoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Pemberton	Pemberton	k1gInSc1	Pemberton
propagoval	propagovat	k5eAaImAgInS	propagovat
jako	jako	k9	jako
posilující	posilující	k2eAgInSc1d1	posilující
prostředek	prostředek	k1gInSc1	prostředek
pro	pro	k7c4	pro
osvěžení	osvěžení	k1gNnSc4	osvěžení
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
bolestem	bolest	k1gFnPc3	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
únavě	únava	k1gFnSc6	únava
a	a	k8xC	a
pro	pro	k7c4	pro
rekonvalescenci	rekonvalescence	k1gFnSc4	rekonvalescence
po	po	k7c6	po
chřipce	chřipka	k1gFnSc6	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
byla	být	k5eAaImAgFnS	být
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
voda	voda	k1gFnSc1	voda
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
sodovkou	sodovka	k1gFnSc7	sodovka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
či	či	k8xC	či
1904	[number]	k4	1904
kvůli	kvůli	k7c3	kvůli
tlaku	tlak	k1gInSc3	tlak
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
kokain	kokain	k1gInSc4	kokain
do	do	k7c2	do
Coca-Coly	cocaola	k1gFnSc2	coca-cola
přidáván	přidáván	k2eAgMnSc1d1	přidáván
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zvýšením	zvýšení	k1gNnSc7	zvýšení
obsahu	obsah	k1gInSc2	obsah
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
kokain	kokain	k1gInSc4	kokain
zakázán	zakázán	k2eAgInSc1d1	zakázán
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Z	z	k7c2	z
chuťových	chuťový	k2eAgInPc2d1	chuťový
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
však	však	k9	však
do	do	k7c2	do
nápoje	nápoj	k1gInSc2	nápoj
přidáván	přidáván	k2eAgInSc4d1	přidáván
výtažek	výtažek	k1gInSc4	výtažek
z	z	k7c2	z
koky	koka	k1gFnSc2	koka
zbavený	zbavený	k2eAgInSc4d1	zbavený
kokainu	kokain	k1gInSc3	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
Coca-Cola	cocaola	k1gFnSc1	coca-cola
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
Coca-Coly	cocaola	k1gFnSc2	coca-cola
ročně	ročně	k6eAd1	ročně
asi	asi	k9	asi
175	[number]	k4	175
tun	tuna	k1gFnPc2	tuna
koky	koka	k1gFnSc2	koka
<g/>
.	.	kIx.	.
</s>
<s>
Výtažek	výtažek	k1gInSc1	výtažek
z	z	k7c2	z
koky	koka	k1gFnSc2	koka
dodává	dodávat	k5eAaImIp3nS	dodávat
společnost	společnost	k1gFnSc1	společnost
Stepan	Stepana	k1gFnPc2	Stepana
Company	Compana	k1gFnSc2	Compana
z	z	k7c2	z
Maywoode	Maywood	k1gMnSc5	Maywood
(	(	kIx(	(
<g/>
New	New	k1gMnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
import	import	k1gInSc4	import
koky	koka	k1gFnSc2	koka
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
extrahovaný	extrahovaný	k2eAgInSc4d1	extrahovaný
kokaín	kokaín	k1gInSc4	kokaín
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
koky	koka	k1gFnSc2	koka
<g/>
)	)	kIx)	)
dodává	dodávat	k5eAaImIp3nS	dodávat
farmaceutické	farmaceutický	k2eAgFnSc3d1	farmaceutická
firmě	firma	k1gFnSc3	firma
Mallinckrodt	Mallinckrodt	k1gMnSc1	Mallinckrodt
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
firma	firma	k1gFnSc1	firma
v	v	k7c6	v
USA	USA	kA	USA
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4	pro
farmaceutické	farmaceutický	k2eAgNnSc4d1	farmaceutické
zpracování	zpracování	k1gNnSc4	zpracování
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
Maywood	Maywood	k1gInSc4	Maywood
Chemical	Chemical	k1gFnSc2	Chemical
Works	Works	kA	Works
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
firmu	firma	k1gFnSc4	firma
koupila	koupit	k5eAaPmAgFnS	koupit
společnost	společnost	k1gFnSc1	společnost
Stepan	Stepan	k1gMnSc1	Stepan
Chemicals	Chemicals	k1gInSc1	Chemicals
<g/>
.	.	kIx.	.
</s>
<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
mínění	mínění	k1gNnSc3	mínění
snažila	snažit	k5eAaImAgFnS	snažit
tajit	tajit	k5eAaImF	tajit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nápoj	nápoj	k1gInSc4	nápoj
má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc1	něco
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
kokou	koka	k1gFnSc7	koka
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
přestat	přestat	k5eAaPmF	přestat
výtažek	výtažek	k1gInSc4	výtažek
z	z	k7c2	z
koky	koka	k1gFnSc2	koka
do	do	k7c2	do
nápoje	nápoj	k1gInSc2	nápoj
přidávat	přidávat	k5eAaImF	přidávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
snížila	snížit	k5eAaPmAgFnS	snížit
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
nápoje	nápoj	k1gInSc2	nápoj
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
k	k	k7c3	k
přidávání	přidávání	k1gNnSc3	přidávání
výtažku	výtažek	k1gInSc2	výtažek
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
8	[number]	k4	8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
americký	americký	k2eAgInSc1d1	americký
server	server	k1gInSc1	server
Thisamericanlife	Thisamericanlif	k1gInSc5	Thisamericanlif
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odhalil	odhalit	k5eAaPmAgMnS	odhalit
tajnou	tajný	k2eAgFnSc4d1	tajná
recepturu	receptura	k1gFnSc4	receptura
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
Coca-Coly	cocaola	k1gFnSc2	coca-cola
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
údajně	údajně	k6eAd1	údajně
nalezl	nalézt	k5eAaBmAgInS	nalézt
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
otevřené	otevřený	k2eAgFnSc2d1	otevřená
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
listu	list	k1gInSc6	list
Atlanta	Atlanta	k1gFnSc1	Atlanta
Journal-Constitution	Journal-Constitution	k1gInSc1	Journal-Constitution
z	z	k7c2	z
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
kopie	kopie	k1gFnSc1	kopie
původního	původní	k2eAgInSc2d1	původní
Pembertonova	Pembertonův	k2eAgInSc2d1	Pembertonův
návodu	návod	k1gInSc2	návod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
návodu	návod	k1gInSc2	návod
měl	mít	k5eAaImAgInS	mít
nápoj	nápoj	k1gInSc1	nápoj
obsahovat	obsahovat	k5eAaImF	obsahovat
tyto	tento	k3xDgFnPc4	tento
ingredience	ingredience	k1gFnPc4	ingredience
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc1	množství
přepočteno	přepočten	k2eAgNnSc1d1	přepočteno
z	z	k7c2	z
uncí	unce	k1gFnPc2	unce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
extrakt	extrakt	k1gInSc1	extrakt
z	z	k7c2	z
koky	koka	k1gFnSc2	koka
–	–	k?	–
6	[number]	k4	6
g	g	kA	g
kyselina	kyselina	k1gFnSc1	kyselina
fosforečná	fosforečný	k2eAgFnSc1d1	fosforečná
–	–	k?	–
84	[number]	k4	84
g	g	kA	g
kofein	kofein	k1gInSc4	kofein
28	[number]	k4	28
g	g	kA	g
cukr	cukr	k1gInSc1	cukr
–	–	k?	–
840	[number]	k4	840
g	g	kA	g
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
původní	původní	k2eAgInSc1d1	původní
údaj	údaj	k1gInSc1	údaj
"	"	kIx"	"
<g/>
30	[number]	k4	30
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
uncích	unce	k1gFnPc6	unce
<g />
.	.	kIx.	.
</s>
<s>
voda	voda	k1gFnSc1	voda
–	–	k?	–
9,36	[number]	k4	9,36
l	l	kA	l
limetková	limetkový	k2eAgFnSc1d1	limetková
šťáva	šťáva	k1gFnSc1	šťáva
–	–	k?	–
0,946	[number]	k4	0,946
l	l	kA	l
vanilka	vanilka	k1gFnSc1	vanilka
–	–	k?	–
28	[number]	k4	28
g	g	kA	g
karamel	karamela	k1gFnPc2	karamela
–	–	k?	–
42	[number]	k4	42
g	g	kA	g
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kvůli	kvůli	k7c3	kvůli
barvě	barva	k1gFnSc3	barva
Tajná	tajný	k2eAgFnSc1d1	tajná
ingredience	ingredience	k1gFnSc1	ingredience
7X	[number]	k4	7X
–	–	k?	–
56	[number]	k4	56
g	g	kA	g
do	do	k7c2	do
18,7	[number]	k4	18,7
l	l	kA	l
sirupu	sirup	k1gInSc2	sirup
alkohol	alkohol	k1gInSc1	alkohol
–	–	k?	–
224	[number]	k4	224
g	g	kA	g
pomerančový	pomerančový	k2eAgInSc4d1	pomerančový
olej	olej	k1gInSc4	olej
–	–	k?	–
20	[number]	k4	20
kapek	kapka	k1gFnPc2	kapka
citronový	citronový	k2eAgInSc4d1	citronový
olej	olej	k1gInSc4	olej
–	–	k?	–
30	[number]	k4	30
kapek	kapka	k1gFnPc2	kapka
muškátový	muškátový	k2eAgInSc1d1	muškátový
oříšek	oříšek	k1gInSc1	oříšek
–	–	k?	–
10	[number]	k4	10
kapek	kapka	k1gFnPc2	kapka
<g />
.	.	kIx.	.
</s>
<s>
koriandr	koriandr	k1gInSc1	koriandr
–	–	k?	–
5	[number]	k4	5
kapek	kapka	k1gFnPc2	kapka
neroli	roli	k6eNd1	roli
(	(	kIx(	(
<g/>
silice	silice	k1gFnSc1	silice
z	z	k7c2	z
květů	květ	k1gInPc2	květ
hořkého	hořký	k2eAgInSc2d1	hořký
pomerančovníku	pomerančovník	k1gInSc2	pomerančovník
<g/>
)	)	kIx)	)
–	–	k?	–
10	[number]	k4	10
kapek	kapka	k1gFnPc2	kapka
skořice	skořice	k1gFnSc2	skořice
–	–	k?	–
10	[number]	k4	10
kapek	kapka	k1gFnPc2	kapka
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
glukózo-fruktózový	glukózoruktózový	k2eAgInSc4d1	glukózo-fruktózový
sirup	sirup	k1gInSc4	sirup
například	například	k6eAd1	například
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
cukr	cukr	k1gInSc4	cukr
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
či	či	k8xC	či
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
