<p>
<s>
Paleoekologie	Paleoekologie	k1gFnSc1	Paleoekologie
používá	používat	k5eAaImIp3nS	používat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
fosilií	fosilie	k1gFnPc2	fosilie
a	a	k8xC	a
subfosilií	subfosilie	k1gFnPc2	subfosilie
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
ekosystémů	ekosystém	k1gInPc2	ekosystém
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obor	obor	k1gInSc1	obor
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
studium	studium	k1gNnSc4	studium
fosilních	fosilní	k2eAgInPc2d1	fosilní
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojených	spojený	k2eAgInPc2d1	spojený
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gInSc2	jejich
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
způsobů	způsob	k1gInPc2	způsob
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
paleoprostředí	paleoprostředit	k5eAaPmIp3nS	paleoprostředit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
záznamy	záznam	k1gInPc1	záznam
byly	být	k5eAaImAgInP	být
studovány	studovat	k5eAaImNgInP	studovat
kvůli	kvůli	k7c3	kvůli
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
objasnění	objasnění	k1gNnSc4	objasnění
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zvířata	zvíře	k1gNnPc4	zvíře
ke	k	k7c3	k
svému	své	k1gNnSc3	své
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
částečně	částečně	k6eAd1	částečně
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
biologické	biologický	k2eAgFnSc2d1	biologická
diverzity	diverzita	k1gFnSc2	diverzita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
taxonomickou	taxonomický	k2eAgFnSc7d1	Taxonomická
a	a	k8xC	a
ekologickou	ekologický	k2eAgFnSc7d1	ekologická
růzností	různost	k1gFnSc7	různost
obratlovců	obratlovec	k1gMnPc2	obratlovec
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
úzká	úzký	k2eAgFnSc1d1	úzká
spojitost	spojitost	k1gFnSc1	spojitost
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
ekologická	ekologický	k2eAgFnSc1d1	ekologická
nika	nika	k1gFnSc1	nika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
a	a	k8xC	a
měření	měření	k1gNnSc1	měření
==	==	k?	==
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
paleoekologie	paleoekologie	k1gFnSc2	paleoekologie
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
stavba	stavba	k1gFnSc1	stavba
co	co	k9	co
nejpřesnějšího	přesný	k2eAgInSc2d3	nejpřesnější
a	a	k8xC	a
nejdetailnějšího	detailní	k2eAgInSc2d3	nejdetailnější
modelu	model	k1gInSc2	model
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
původně	původně	k6eAd1	původně
žily	žít	k5eAaImAgInP	žít
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
jako	jako	k8xC	jako
fosílie	fosílie	k1gFnPc1	fosílie
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
komplexní	komplexní	k2eAgFnSc2d1	komplexní
interakce	interakce	k1gFnSc2	interakce
mezi	mezi	k7c7	mezi
faktory	faktor	k1gInPc7	faktor
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
zásoby	zásoba	k1gFnPc4	zásoba
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
stupeň	stupeň	k1gInSc1	stupeň
slunečního	sluneční	k2eAgNnSc2d1	sluneční
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
informací	informace	k1gFnPc2	informace
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
či	či	k8xC	či
rozloženo	rozložen	k2eAgNnSc1d1	rozloženo
procesem	proces	k1gInSc7	proces
fosilizace	fosilizace	k1gFnSc2	fosilizace
nebo	nebo	k8xC	nebo
diagenezí	diageneze	k1gFnPc2	diageneze
blízkých	blízký	k2eAgInPc2d1	blízký
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dělá	dělat	k5eAaImIp3nS	dělat
interpretaci	interpretace	k1gFnSc4	interpretace
velmi	velmi	k6eAd1	velmi
složitou	složitý	k2eAgFnSc4d1	složitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Faktor	faktor	k1gInSc4	faktor
složitosti	složitost	k1gFnSc2	složitost
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
normálně	normálně	k6eAd1	normálně
složený	složený	k2eAgInSc1d1	složený
ze	z	k7c2	z
statistických	statistický	k2eAgFnPc2d1	statistická
analýz	analýza	k1gFnPc2	analýza
dostupných	dostupný	k2eAgNnPc2d1	dostupné
číselných	číselný	k2eAgNnPc2d1	číselné
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
kvantitivní	kvantitivní	k2eAgFnSc1d1	kvantitivní
paleontologie	paleontologie	k1gFnSc1	paleontologie
nebo	nebo	k8xC	nebo
paleostatistiky	paleostatistika	k1gFnPc1	paleostatistika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
obor	obor	k1gInSc4	obor
studující	studující	k2eAgInPc4d1	studující
posmrtné	posmrtný	k2eAgInPc4d1	posmrtný
procesy	proces	k1gInPc4	proces
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
tafonomie	tafonomie	k1gFnSc1	tafonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čtvrtohory	čtvrtohory	k1gFnPc4	čtvrtohory
==	==	k?	==
</s>
</p>
<p>
<s>
Spousta	spousta	k1gFnSc1	spousta
originálního	originální	k2eAgInSc2d1	originální
paleoekologického	paleoekologický	k2eAgInSc2d1	paleoekologický
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
čtvrtohory	čtvrtohory	k1gFnPc1	čtvrtohory
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
starší	starý	k2eAgNnSc4d2	starší
prostředí	prostředí	k1gNnSc4	prostředí
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
reprezentována	reprezentován	k2eAgFnSc1d1	reprezentována
fosiliemi	fosilie	k1gFnPc7	fosilie
<g/>
.	.	kIx.	.
</s>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
také	také	k9	také
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodně	hodně	k6eAd1	hodně
studií	studie	k1gFnSc7	studie
se	se	k3xPyFc4	se
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
na	na	k7c4	na
holocén	holocén	k1gInSc4	holocén
(	(	kIx(	(
<g/>
posledních	poslední	k2eAgNnPc2d1	poslední
11,000	[number]	k4	11,000
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
dobu	doba	k1gFnSc4	doba
ledovou	ledový	k2eAgFnSc4d1	ledová
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
(	(	kIx(	(
<g/>
období	období	k1gNnSc4	období
před	před	k7c7	před
50,000	[number]	k4	50,000
až	až	k8xS	až
100,000	[number]	k4	100,000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
studia	studio	k1gNnPc1	studio
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
užitečná	užitečný	k2eAgNnPc1d1	užitečné
pro	pro	k7c4	pro
porozumění	porozumění	k1gNnSc4	porozumění
dynamiky	dynamika	k1gFnSc2	dynamika
změn	změna	k1gFnPc2	změna
ekosystémů	ekosystém	k1gInPc2	ekosystém
a	a	k8xC	a
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
ekosystémů	ekosystém	k1gInPc2	ekosystém
před	před	k7c7	před
industrializací	industrializace	k1gFnSc7	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
rozhodovatelů	rozhodovatel	k1gMnPc2	rozhodovatel
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
politice	politika	k1gFnSc6	politika
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
důležitost	důležitost	k1gFnSc4	důležitost
používání	používání	k1gNnSc2	používání
paleoekologických	paleoekologický	k2eAgNnPc2d1	paleoekologický
studií	studio	k1gNnPc2	studio
jako	jako	k8xC	jako
základny	základna	k1gFnSc2	základna
pro	pro	k7c4	pro
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
padnou	padnout	k5eAaImIp3nP	padnout
v	v	k7c6	v
konzervační	konzervační	k2eAgFnSc6d1	konzervační
biologii	biologie	k1gFnSc6	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Paleontologie	paleontologie	k1gFnSc1	paleontologie
</s>
</p>
<p>
<s>
Ekologie	ekologie	k1gFnSc1	ekologie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Paleoekologická	Paleoekologický	k2eAgFnSc1d1	Paleoekologický
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
