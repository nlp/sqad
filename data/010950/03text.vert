<p>
<s>
Chudina	chudina	k1gFnSc1	chudina
(	(	kIx(	(
<g/>
Draba	Draba	k1gFnSc1	Draba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
rod	rod	k1gInSc1	rod
čeledě	čeleď	k1gFnSc2	čeleď
brukvovitých	brukvovitý	k2eAgMnPc2d1	brukvovitý
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gMnSc4	on
přes	přes	k7c4	přes
360	[number]	k4	360
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
planě	planě	k6eAd1	planě
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
chudina	chudina	k1gFnSc1	chudina
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
,	,	kIx,	,
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
i	i	k8xC	i
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
i	i	k8xC	i
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zařazeny	zařadit	k5eAaPmNgInP	zařadit
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgFnPc1d1	odolná
a	a	k8xC	a
nenáročné	náročný	k2eNgFnPc1d1	nenáročná
rostliny	rostlina	k1gFnPc1	rostlina
schopné	schopný	k2eAgFnPc1d1	schopná
odolat	odolat	k5eAaPmF	odolat
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
subalpínských	subalpínský	k2eAgFnPc6d1	subalpínská
a	a	k8xC	a
alpínských	alpínský	k2eAgFnPc6d1	alpínská
jakož	jakož	k8xC	jakož
i	i	k9	i
v	v	k7c6	v
arktickém	arktický	k2eAgNnSc6d1	arktické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rodu	rod	k1gInSc6	rod
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
tyto	tento	k3xDgInPc4	tento
chladnomilné	chladnomilný	k2eAgInPc4d1	chladnomilný
také	také	k9	také
druhy	druh	k1gInPc4	druh
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
teplém	teplý	k2eAgNnSc6d1	teplé
<g/>
,	,	kIx,	,
suchém	suchý	k2eAgNnSc6d1	suché
nebo	nebo	k8xC	nebo
vlhkém	vlhký	k2eAgMnSc6d1	vlhký
<g/>
,	,	kIx,	,
na	na	k7c6	na
plném	plný	k2eAgNnSc6d1	plné
slunci	slunce	k1gNnSc6	slunce
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zastínění	zastínění	k1gNnSc6	zastínění
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdách	půda	k1gFnPc6	půda
zásaditých	zásaditý	k2eAgMnPc2d1	zásaditý
nebo	nebo	k8xC	nebo
kyselých	kyselý	k2eAgInPc2d1	kyselý
<g/>
,	,	kIx,	,
humózních	humózní	k2eAgInPc2d1	humózní
i	i	k8xC	i
chudých	chudý	k2eAgInPc2d1	chudý
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
<g/>
;	;	kIx,	;
jediné	jediný	k2eAgFnPc4d1	jediná
co	co	k8xS	co
jím	on	k3xPp3gNnSc7	on
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
je	on	k3xPp3gNnSc4	on
přílišné	přílišný	k2eAgNnSc4d1	přílišné
vlhko	vlhko	k1gNnSc4	vlhko
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
<g/>
,	,	kIx,	,
dvouleté	dvouletý	k2eAgNnSc1d1	dvouleté
<g/>
,	,	kIx,	,
víceleté	víceletý	k2eAgInPc1d1	víceletý
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
výhradně	výhradně	k6eAd1	výhradně
ale	ale	k8xC	ale
suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
zcela	zcela	k6eAd1	zcela
lysé	lysý	k2eAgInPc1d1	lysý
nebo	nebo	k8xC	nebo
pokryty	pokryt	k2eAgInPc1d1	pokryt
jednobuněčnými	jednobuněčný	k2eAgInPc7d1	jednobuněčný
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
<g/>
,	,	kIx,	,
rozvětvenými	rozvětvený	k2eAgInPc7d1	rozvětvený
nebo	nebo	k8xC	nebo
hvězdicovými	hvězdicový	k2eAgInPc7d1	hvězdicový
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jich	on	k3xPp3gMnPc2	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
současně	současně	k6eAd1	současně
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc1	lodyha
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
přímé	přímý	k2eAgInPc4d1	přímý
nebo	nebo	k8xC	nebo
poléhavé	poléhavý	k2eAgInPc4d1	poléhavý
a	a	k8xC	a
vystoupavé	vystoupavý	k2eAgInPc4d1	vystoupavý
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
nebo	nebo	k8xC	nebo
rozvětvené	rozvětvený	k2eAgInPc4d1	rozvětvený
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
dvojí	dvojí	k4xRgInSc4	dvojí
<g/>
,	,	kIx,	,
bazální	bazální	k2eAgInSc4d1	bazální
v	v	k7c6	v
růžici	růžice	k1gFnSc6	růžice
a	a	k8xC	a
lodyžní	lodyžní	k2eAgNnSc1d1	lodyžní
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
někdy	někdy	k6eAd1	někdy
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
v	v	k7c6	v
růžici	růžice	k1gFnSc6	růžice
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
<g/>
,	,	kIx,	,
celistvé	celistvý	k2eAgInPc1d1	celistvý
<g/>
,	,	kIx,	,
zubaté	zubatý	k2eAgInPc1d1	zubatý
nebo	nebo	k8xC	nebo
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
<g/>
.	.	kIx.	.
</s>
<s>
Lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
listy	list	k1gInPc1	list
bývají	bývat	k5eAaImIp3nP	bývat
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
nebo	nebo	k8xC	nebo
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
,	,	kIx,	,
u	u	k7c2	u
báze	báze	k1gFnSc2	báze
bývají	bývat	k5eAaImIp3nP	bývat
klínovité	klínovitý	k2eAgFnPc1d1	klínovitá
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc1	okraj
mívají	mívat	k5eAaImIp3nP	mívat
celistvé	celistvý	k2eAgInPc1d1	celistvý
nebo	nebo	k8xC	nebo
zubaté	zubatý	k2eAgInPc1d1	zubatý
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
přetrhované	přetrhovaný	k2eAgFnPc1d1	přetrhovaný
<g/>
,	,	kIx,	,
palisty	palist	k1gInPc1	palist
nevyrůstají	vyrůstat	k5eNaImIp3nP	vyrůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chocholíkovitá	Chocholíkovitý	k2eAgNnPc1d1	Chocholíkovitý
květenství	květenství	k1gNnPc1	květenství
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
do	do	k7c2	do
prodlužujících	prodlužující	k2eAgInPc2d1	prodlužující
se	se	k3xPyFc4	se
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
listenů	listen	k1gInPc2	listen
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
cyklické	cyklický	k2eAgInPc1d1	cyklický
květy	květ	k1gInPc1	květ
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
tenkých	tenký	k2eAgFnPc6d1	tenká
vztyčených	vztyčený	k2eAgFnPc6d1	vztyčená
nebo	nebo	k8xC	nebo
rozbíhajících	rozbíhající	k2eAgFnPc6d1	rozbíhající
se	se	k3xPyFc4	se
stopkách	stopka	k1gFnPc6	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
kopinaté	kopinatý	k2eAgInPc1d1	kopinatý
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
eliptické	eliptický	k2eAgFnPc1d1	eliptická
nebo	nebo	k8xC	nebo
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
jsou	být	k5eAaImIp3nP	být
vzpřímené	vzpřímený	k2eAgFnPc1d1	vzpřímená
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
volné	volný	k2eAgInPc1d1	volný
vztyčené	vztyčený	k2eAgInPc1d1	vztyčený
korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
do	do	k7c2	do
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
tvar	tvar	k1gInSc4	tvar
vejčitý	vejčitý	k2eAgInSc4d1	vejčitý
nebo	nebo	k8xC	nebo
podlouhlý	podlouhlý	k2eAgInSc4d1	podlouhlý
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
eliptický	eliptický	k2eAgInSc4d1	eliptický
<g/>
,	,	kIx,	,
vrchol	vrchol	k1gInSc4	vrchol
mají	mít	k5eAaImIp3nP	mít
zakulacený	zakulacený	k2eAgInSc4d1	zakulacený
nebo	nebo	k8xC	nebo
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
zářezem	zářez	k1gInSc7	zářez
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
barvy	barva	k1gFnPc1	barva
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgFnPc1d1	růžová
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
fialové	fialový	k2eAgFnPc1d1	fialová
nebo	nebo	k8xC	nebo
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andreceum	Andreceum	k1gInSc1	Andreceum
tvoří	tvořit	k5eAaImIp3nS	tvořit
šest	šest	k4xCc4	šest
tyčinek	tyčinka	k1gFnPc2	tyčinka
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
)	)	kIx)	)
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
vnější	vnější	k2eAgFnPc4d1	vnější
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgFnPc1d2	kratší
<g/>
,	,	kIx,	,
tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončen	k2eAgInPc4d1	zakončen
vejčitými	vejčitý	k2eAgInPc7d1	vejčitý
nebo	nebo	k8xC	nebo
podlouhlými	podlouhlý	k2eAgInPc7d1	podlouhlý
prašníky	prašník	k1gInPc7	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gNnSc1	Gyneceum
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
přisedlý	přisedlý	k2eAgInSc1d1	přisedlý
dvoupouzdrý	dvoupouzdrý	k2eAgInSc1d1	dvoupouzdrý
semeník	semeník	k1gInSc1	semeník
nese	nést	k5eAaImIp3nS	nést
kratičkou	kratičký	k2eAgFnSc4d1	kratičká
čnělku	čnělka	k1gFnSc4	čnělka
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
někdy	někdy	k6eAd1	někdy
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
)	)	kIx)	)
s	s	k7c7	s
hlavičkovou	hlavičkový	k2eAgFnSc7d1	Hlavičková
bliznou	blizna	k1gFnSc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
báze	báze	k1gFnSc2	báze
tyčinek	tyčinka	k1gFnPc2	tyčinka
jsou	být	k5eAaImIp3nP	být
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
nektarové	nektarový	k2eAgFnSc2d1	Nektarová
žlázky	žlázka	k1gFnSc2	žlázka
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
pukavé	pukavý	k2eAgFnPc4d1	pukavá
šešule	šešule	k1gFnPc4	šešule
nebo	nebo	k8xC	nebo
šešulky	šešulka	k1gFnPc4	šešulka
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
otvírající	otvírající	k2eAgFnSc1d1	otvírající
se	s	k7c7	s
chlopněmi	chlopeň	k1gFnPc7	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
drobná	drobný	k2eAgNnPc1d1	drobné
zploštělá	zploštělý	k2eAgNnPc1d1	zploštělé
obvykle	obvykle	k6eAd1	obvykle
neokřídlená	okřídlený	k2eNgNnPc1d1	okřídlený
semena	semeno	k1gNnPc1	semeno
bez	bez	k7c2	bez
přívěsku	přívěsek	k1gInSc2	přívěsek
a	a	k8xC	a
endospermu	endosperm	k1gInSc2	endosperm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
chudina	chudina	k1gFnSc1	chudina
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
taxonomicky	taxonomicky	k6eAd1	taxonomicky
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
rod	rod	k1gInSc4	rod
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
čeledě	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
taxony	taxon	k1gInPc1	taxon
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
triviálních	triviální	k2eAgInPc2d1	triviální
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
rostoucího	rostoucí	k2eAgInSc2d1	rostoucí
v	v	k7c6	v
tak	tak	k9	tak
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
areálu	areál	k1gInSc6	areál
a	a	k8xC	a
popisovaného	popisovaný	k2eAgMnSc4d1	popisovaný
tolika	tolik	k4xDc7	tolik
rozdílnými	rozdílný	k2eAgMnPc7d1	rozdílný
autory	autor	k1gMnPc7	autor
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
duplicitě	duplicita	k1gFnSc3	duplicita
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
monografie	monografie	k1gFnSc1	monografie
rodu	rod	k1gInSc2	rod
byla	být	k5eAaImAgFnS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
německým	německý	k2eAgMnSc7d1	německý
botanikem	botanik	k1gMnSc7	botanik
O.	O.	kA	O.
E.	E.	kA	E.
Schulzem	Schulz	k1gMnSc7	Schulz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
mnohem	mnohem	k6eAd1	mnohem
přehlednější	přehlední	k2eAgFnSc1d2	přehlední
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
jen	jen	k9	jen
dva	dva	k4xCgMnPc1	dva
sobě	se	k3xPyFc3	se
podobné	podobný	k2eAgInPc4d1	podobný
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chudina	chudina	k1gFnSc1	chudina
hajní	hajní	k2eAgFnSc1d1	hajní
(	(	kIx(	(
<g/>
Draba	Draba	k1gFnSc1	Draba
nemorosa	nemorosa	k1gFnSc1	nemorosa
<g/>
)	)	kIx)	)
L.	L.	kA	L.
</s>
</p>
<p>
<s>
Chudina	chudina	k1gMnSc1	chudina
zední	zední	k1gMnSc1	zední
(	(	kIx(	(
<g/>
Draba	Draba	k1gMnSc1	Draba
muralis	muralis	k1gFnSc2	muralis
<g/>
)	)	kIx)	)
L.	L.	kA	L.
<g/>
Chudina	chudina	k1gFnSc1	chudina
hajní	hajní	k2eAgFnSc1d1	hajní
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
a	a	k8xC	a
chudina	chudina	k1gFnSc1	chudina
zední	zednit	k5eAaImIp3nS	zednit
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chudina	chudina	k1gFnSc1	chudina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Flora	Flora	k1gFnSc1	Flora
of	of	k?	of
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
Draba	Draba	k1gMnSc1	Draba
</s>
</p>
