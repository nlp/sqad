<s>
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
ve	v	k7c4	v
Skalné	skalný	k2eAgNnSc4d1	Skalné
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
fotbalista	fotbalista	k1gMnSc1	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
medailista	medailista	k1gMnSc1	medailista
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
a	a	k8xC	a
Superpoháru	superpohár	k1gInSc2	superpohár
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
ČR	ČR	kA	ČR
a	a	k8xC	a
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
ČR	ČR	kA	ČR
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
ČR	ČR	kA	ČR
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pelé	Pelé	k1gNnSc1	Pelé
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
ho	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
medailí	medaile	k1gFnSc7	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-2009	[number]	k4	2001-2009
byl	být	k5eAaImAgMnS	být
hráčem	hráč	k1gMnSc7	hráč
Juventusu	Juventus	k1gInSc2	Juventus
Turín	Turín	k1gInSc1	Turín
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
v	v	k7c6	v
týmech	tým	k1gInPc6	tým
RH	RH	kA	RH
Cheb	Cheb	k1gInSc1	Cheb
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lazio	Lazio	k1gNnSc1	Lazio
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Laziem	Lazium	k1gNnSc7	Lazium
a	a	k8xC	a
Juventusem	Juventus	k1gInSc7	Juventus
získal	získat	k5eAaPmAgMnS	získat
tzv.	tzv.	kA	tzv.
Scudetto	Scudetto	k1gNnSc1	Scudetto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
italské	italský	k2eAgFnSc2d1	italská
Serie	serie	k1gFnSc2	serie
A.	A.	kA	A.
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
konec	konec	k1gInSc1	konec
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
české	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
až	až	k9	až
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
ale	ale	k9	ale
nezůstal	zůstat	k5eNaPmAgInS	zůstat
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1996	[number]	k4	1996
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vyhlédl	vyhlédnout	k5eAaPmAgMnS	vyhlédnout
trenér	trenér	k1gMnSc1	trenér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
ze	z	k7c2	z
Sparty	Sparta	k1gFnSc2	Sparta
Praha	Praha	k1gFnSc1	Praha
do	do	k7c2	do
Lazia	Lazium	k1gNnSc2	Lazium
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgNnSc7	který
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
italský	italský	k2eAgInSc1d1	italský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
pohár	pohár	k1gInSc1	pohár
a	a	k8xC	a
o	o	k7c4	o
sezónu	sezóna	k1gFnSc4	sezóna
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
poslední	poslední	k2eAgInSc4d1	poslední
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Mallorca	Mallorc	k1gInSc2	Mallorc
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
vítězný	vítězný	k2eAgInSc1d1	vítězný
gól	gól	k1gInSc1	gól
na	na	k7c6	na
konečných	konečný	k2eAgInPc6d1	konečný
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
evropský	evropský	k2eAgInSc1d1	evropský
pohár	pohár	k1gInSc1	pohár
poté	poté	k6eAd1	poté
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2001	[number]	k4	2001
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
za	za	k7c4	za
41,2	[number]	k4	41,2
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
do	do	k7c2	do
Juventusu	Juventus	k1gInSc2	Juventus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
francouzkého	francouzký	k2eAgMnSc4d1	francouzký
záložníka	záložník	k1gMnSc4	záložník
alžírského	alžírský	k2eAgInSc2d1	alžírský
původu	původ	k1gInSc2	původ
Zinedina	Zinedin	k2eAgMnSc4d1	Zinedin
Zidana	Zidan	k1gMnSc4	Zidan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
léto	léto	k1gNnSc4	léto
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
páteří	páteř	k1gFnSc7	páteř
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
italskou	italský	k2eAgFnSc4d1	italská
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
také	také	k9	také
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Juventusu	Juventus	k1gInSc3	Juventus
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
trestu	trest	k1gInSc3	trest
za	za	k7c4	za
žluté	žlutý	k2eAgFnPc4d1	žlutá
karty	karta	k1gFnPc4	karta
po	po	k7c6	po
faulu	faul	k1gInSc6	faul
na	na	k7c4	na
Steva	Steve	k1gMnSc4	Steve
McManamana	McManaman	k1gMnSc4	McManaman
nemohl	moct	k5eNaImAgMnS	moct
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
nastoupit	nastoupit	k5eAaPmF	nastoupit
a	a	k8xC	a
Juventus	Juventus	k1gInSc1	Juventus
prohrál	prohrát	k5eAaPmAgInS	prohrát
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
s	s	k7c7	s
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
po	po	k7c6	po
Josefu	Josef	k1gMnSc6	Josef
Masopustovi	Masopust	k1gMnSc6	Masopust
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
fotbalistu	fotbalista	k1gMnSc4	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
obdržel	obdržet	k5eAaPmAgMnS	obdržet
190	[number]	k4	190
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
tak	tak	k9	tak
druhého	druhý	k4xOgMnSc4	druhý
Thierriho	Thierri	k1gMnSc4	Thierri
Henryho	Henry	k1gMnSc4	Henry
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
128	[number]	k4	128
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetího	třetí	k4xOgMnSc2	třetí
Paola	Paol	k1gMnSc2	Paol
Maldiniho	Maldini	k1gMnSc2	Maldini
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
123	[number]	k4	123
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
ČR	ČR	kA	ČR
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1994	[number]	k4	1994
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Lansdowne	Lansdown	k1gInSc5	Lansdown
Road	Road	k1gMnSc1	Road
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
domácímu	domácí	k2eAgNnSc3d1	domácí
Irsku	Irsko	k1gNnSc3	Irsko
<g/>
,	,	kIx,	,
když	když	k8xS	když
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
v	v	k7c6	v
84	[number]	k4	84
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
místo	místo	k7c2	místo
Jiřího	Jiří	k1gMnSc2	Jiří
Němce	Němec	k1gMnSc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
mezistátní	mezistátní	k2eAgInSc4d1	mezistátní
zápas	zápas	k1gInSc4	zápas
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
dresu	dres	k1gInSc6	dres
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1996	[number]	k4	1996
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
otevíral	otevírat	k5eAaImAgMnS	otevírat
skóre	skóre	k1gNnSc4	skóre
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
české	český	k2eAgNnSc1d1	české
mužstvo	mužstvo	k1gNnSc1	mužstvo
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
Pavla	Pavel	k1gMnSc2	Pavel
Nedvěda	Nedvěd	k1gMnSc2	Nedvěd
na	na	k7c6	na
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
turnajích	turnaj	k1gInPc6	turnaj
<g/>
:	:	kIx,	:
ME	ME	kA	ME
1996	[number]	k4	1996
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
ME	ME	kA	ME
2000	[number]	k4	2000
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
ME	ME	kA	ME
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
stal	stát	k5eAaPmAgMnS	stát
kapitánem	kapitán	k1gMnSc7	kapitán
českého	český	k2eAgNnSc2d1	české
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
mužstva	mužstvo	k1gNnSc2	mužstvo
a	a	k8xC	a
dovedl	dovést	k5eAaPmAgMnS	dovést
ho	on	k3xPp3gMnSc4	on
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
Eura	euro	k1gNnSc2	euro
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápase	zápas	k1gInSc6	zápas
si	se	k3xPyFc3	se
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
zranil	zranit	k5eAaPmAgMnS	zranit
koleno	koleno	k1gNnSc4	koleno
a	a	k8xC	a
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
vystřídán	vystřídán	k2eAgInSc4d1	vystřídán
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Šmicerem	Šmicer	k1gMnSc7	Šmicer
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
reprezentace	reprezentace	k1gFnSc2	reprezentace
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2005	[number]	k4	2005
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
týmu	tým	k1gInSc3	tým
postoupit	postoupit	k5eAaPmF	postoupit
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
přes	přes	k7c4	přes
Norsko	Norsko	k1gNnSc4	Norsko
po	po	k7c6	po
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
odehrál	odehrát	k5eAaPmAgMnS	odehrát
za	za	k7c4	za
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
91	[number]	k4	91
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
53	[number]	k4	53
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
16	[number]	k4	16
remíz	remíza	k1gFnPc2	remíza
<g/>
,	,	kIx,	,
22	[number]	k4	22
proher	prohra	k1gFnPc2	prohra
<g/>
)	)	kIx)	)
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
18	[number]	k4	18
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
evropských	evropský	k2eAgInPc6d1	evropský
šampionátech	šampionát	k1gInPc6	šampionát
a	a	k8xC	a
jednom	jeden	k4xCgNnSc6	jeden
světovém	světový	k2eAgInSc6d1	světový
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc4d1	reprezentační
kariéru	kariéra	k1gFnSc4	kariéra
zakončil	zakončit	k5eAaPmAgMnS	zakončit
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
v	v	k7c6	v
přípravném	přípravný	k2eAgInSc6d1	přípravný
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
prohrál	prohrát	k5eAaPmAgInS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
do	do	k7c2	do
44	[number]	k4	44
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
vystřídán	vystřídán	k2eAgInSc1d1	vystřídán
Davidem	David	k1gMnSc7	David
Jarolímem	Jarolím	k1gMnSc7	Jarolím
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
Pavla	Pavel	k1gMnSc2	Pavel
Nedvěda	Nedvěd	k1gMnSc2	Nedvěd
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
Góly	gól	k1gInPc1	gól
Pavla	Pavel	k1gMnSc2	Pavel
Nedvěda	Nedvěd	k1gMnSc2	Nedvěd
za	za	k7c4	za
A-mužstvo	Aužstvo	k1gNnSc4	A-mužstvo
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
Juventusu	Juventus	k1gInSc2	Juventus
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
