<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
(	(	kIx(	(
<g/>
zóon	zóon	k1gNnSc1	zóon
politikon	politikona	k1gFnPc2	politikona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tedy	tedy	k8xC	tedy
žít	žít	k5eAaImF	žít
sám	sám	k3xTgMnSc1	sám
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soběstačnosti	soběstačnost	k1gFnSc6	soběstačnost
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
částí	část	k1gFnSc7	část
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
buď	buď	k8xC	buď
divoké	divoký	k2eAgNnSc1d1	divoké
zvíře	zvíře	k1gNnSc1	zvíře
nebo	nebo	k8xC	nebo
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pol	pola	k1gFnPc2	pola
1253	[number]	k4	1253
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Aristotelovo	Aristotelův	k2eAgNnSc1d1	Aristotelovo
myšlení	myšlení	k1gNnSc1	myšlení
není	být	k5eNaImIp3nS	být
evoluční	evoluční	k2eAgNnSc1d1	evoluční
ani	ani	k8xC	ani
historické	historický	k2eAgNnSc1d1	historické
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
systematické	systematický	k2eAgNnSc1d1	systematické
<g/>
.	.	kIx.	.
</s>
