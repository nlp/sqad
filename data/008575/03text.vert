<p>
<s>
Kurzor	kurzor	k1gInSc4	kurzor
(	(	kIx(	(
<g/>
karet	kareta	k1gFnPc2	kareta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c6	na
počítačovém	počítačový	k2eAgInSc6d1	počítačový
monitoru	monitor	k1gInSc6	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
bude	být	k5eAaImBp3nS	být
počítač	počítač	k1gInSc4	počítač
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
vypsání	vypsání	k1gNnSc4	vypsání
znaku	znak	k1gInSc2	znak
nebo	nebo	k8xC	nebo
stisk	stisk	k1gInSc1	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
touchpadu	touchpad	k1gInSc2	touchpad
apod.	apod.	kA	apod.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
kurzor	kurzor	k1gInSc1	kurzor
označuje	označovat	k5eAaImIp3nS	označovat
ukazatel	ukazatel	k1gInSc4	ukazatel
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
(	(	kIx(	(
<g/>
touchpadu	touchpad	k1gInSc2	touchpad
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
karet	kareta	k1gFnPc2	kareta
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc1	ukazatel
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
ale	ale	k9	ale
oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
chybně	chybně	k6eAd1	chybně
slučují	slučovat	k5eAaImIp3nP	slučovat
do	do	k7c2	do
slova	slovo	k1gNnSc2	slovo
kurzor	kurzor	k1gInSc1	kurzor
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc1	místo
karetu	kareta	k1gFnSc4	kareta
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
textový	textový	k2eAgInSc1d1	textový
kurzor	kurzor	k1gInSc1	kurzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Karet	kareta	k1gFnPc2	kareta
(	(	kIx(	(
<g/>
při	při	k7c6	při
editaci	editace	k1gFnSc6	editace
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Podoba	podoba	k1gFnSc1	podoba
karetu	kareta	k1gFnSc4	kareta
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
textový	textový	k2eAgInSc4d1	textový
editor	editor	k1gInSc4	editor
ve	v	k7c6	v
vkládacím	vkládací	k2eAgInSc6d1	vkládací
nebo	nebo	k8xC	nebo
přepisovacím	přepisovací	k2eAgInSc6d1	přepisovací
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vkládacím	vkládací	k2eAgInSc6d1	vkládací
režimu	režim	k1gInSc6	režim
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
znak	znak	k1gInSc1	znak
vložen	vložit	k5eAaPmNgInS	vložit
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
karetu	kareta	k1gFnSc4	kareta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přepisovacím	přepisovací	k2eAgInSc6d1	přepisovací
režimu	režim	k1gInSc6	režim
vložený	vložený	k2eAgInSc4d1	vložený
znak	znak	k1gInSc4	znak
přepíše	přepsat	k5eAaPmIp3nS	přepsat
znak	znak	k1gInSc1	znak
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
karetu	kareta	k1gFnSc4	kareta
nebo	nebo	k8xC	nebo
následující	následující	k2eAgInSc4d1	následující
znak	znak	k1gInSc4	znak
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
karet	kareta	k1gFnPc2	kareta
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
textových	textový	k2eAgInPc6d1	textový
editorech	editor	k1gInPc6	editor
typicky	typicky	k6eAd1	typicky
svislou	svislý	k2eAgFnSc4d1	svislá
čárku	čárka	k1gFnSc4	čárka
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
trvale	trvale	k6eAd1	trvale
svítit	svítit	k5eAaImF	svítit
nebo	nebo	k8xC	nebo
blikat	blikat	k5eAaImF	blikat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
první	první	k4xOgMnSc1	první
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Textové	textový	k2eAgNnSc1d1	textové
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
neumožňovalo	umožňovat	k5eNaImAgNnS	umožňovat
zobrazit	zobrazit	k5eAaPmF	zobrazit
další	další	k2eAgInSc4d1	další
znak	znak	k1gInSc4	znak
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
sousedními	sousední	k2eAgInPc7d1	sousední
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
na	na	k7c6	na
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
znak	znak	k1gInSc1	znak
podtržítka	podtržítko	k1gNnSc2	podtržítko
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
druhý	druhý	k4xOgMnSc1	druhý
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obousměrný	obousměrný	k2eAgInSc1d1	obousměrný
text	text	k1gInSc1	text
===	===	k?	===
</s>
</p>
<p>
<s>
Karet	kareta	k1gFnPc2	kareta
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
svislé	svislý	k2eAgFnSc2d1	svislá
čárky	čárka	k1gFnSc2	čárka
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
směr	směr	k1gInSc4	směr
vkládání	vkládání	k1gNnSc2	vkládání
textu	text	k1gInSc2	text
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
tok	tok	k1gInSc1	tok
textu	text	k1gInSc2	text
směrem	směr	k1gInSc7	směr
vpravo	vpravo	k6eAd1	vpravo
i	i	k9	i
směrem	směr	k1gInSc7	směr
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
svislá	svislý	k2eAgFnSc1d1	svislá
čárka	čárka	k1gFnSc1	čárka
doplněna	doplnit	k5eAaPmNgFnS	doplnit
uprostřed	uprostřed	k6eAd1	uprostřed
malou	malý	k2eAgFnSc7d1	malá
šipkou	šipka	k1gFnSc7	šipka
v	v	k7c6	v
aktuálním	aktuální	k2eAgInSc6d1	aktuální
směru	směr	k1gInSc6	směr
vkládání	vkládání	k1gNnSc2	vkládání
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kurzor	kurzor	k1gInSc4	kurzor
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
==	==	k?	==
</s>
</p>
<p>
<s>
Kurzor	kurzor	k1gInSc1	kurzor
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
její	její	k3xOp3gFnSc4	její
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yIgNnSc7	který
místem	místo	k1gNnSc7	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
akce	akce	k1gFnSc1	akce
po	po	k7c6	po
kliknutí	kliknutí	k1gNnSc6	kliknutí
na	na	k7c4	na
tlačítko	tlačítko	k1gNnSc4	tlačítko
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
bod	bod	k1gInSc1	bod
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
hotspot	hotspot	k1gInSc1	hotspot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc4	bod
na	na	k7c6	na
kurzoru	kurzor	k1gInSc6	kurzor
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
při	při	k7c6	při
kliknutí	kliknutí	k1gNnSc6	kliknutí
na	na	k7c4	na
tlačítko	tlačítko	k1gNnSc4	tlačítko
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vrchol	vrchol	k1gInSc4	vrchol
šipky	šipka	k1gFnSc2	šipka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
místo	místo	k1gNnSc4	místo
kurzoru	kurzor	k1gInSc2	kurzor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
přesýpacích	přesýpací	k2eAgFnPc2d1	přesýpací
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gInSc1	jejich
zúžený	zúžený	k2eAgInSc1d1	zúžený
střed	střed	k1gInSc1	střed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
používá	používat	k5eAaImIp3nS	používat
typicky	typicky	k6eAd1	typicky
ukazatel	ukazatel	k1gInSc1	ukazatel
myši	myš	k1gFnSc2	myš
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
šipky	šipka	k1gFnSc2	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Kurzor	kurzor	k1gInSc1	kurzor
myši	myš	k1gFnSc2	myš
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
po	po	k7c6	po
kliknutí	kliknutí	k1gNnSc6	kliknutí
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
okna	okno	k1gNnSc2	okno
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
kurzor	kurzor	k1gInSc1	kurzor
na	na	k7c4	na
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
šipku	šipka	k1gFnSc4	šipka
(	(	kIx(	(
<g/>
svislou	svisla	k1gFnSc7	svisla
<g/>
,	,	kIx,	,
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
nebo	nebo	k8xC	nebo
úhlopříčnou	úhlopříčný	k2eAgFnSc7d1	úhlopříčná
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
tlačítka	tlačítko	k1gNnSc2	tlačítko
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
měnit	měnit	k5eAaImF	měnit
velikost	velikost	k1gFnSc4	velikost
okna	okno	k1gNnSc2	okno
</s>
</p>
<p>
<s>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zaneprázdnění	zaneprázdnění	k1gNnSc2	zaneprázdnění
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
kurzor	kurzor	k1gInSc1	kurzor
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
přesýpací	přesýpací	k2eAgFnPc4d1	přesýpací
hodiny	hodina	k1gFnPc4	hodina
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
MS	MS	kA	MS
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rotující	rotující	k2eAgNnSc1d1	rotující
mezikruží	mezikruží	k1gNnSc1	mezikruží
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc1d1	vista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náramkové	náramkový	k2eAgFnSc2d1	náramková
hodinky	hodinka	k1gFnSc2	hodinka
(	(	kIx(	(
<g/>
prostředí	prostředí	k1gNnSc2	prostředí
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
točící	točící	k2eAgInSc4d1	točící
se	se	k3xPyFc4	se
balónek	balónek	k1gInSc4	balónek
(	(	kIx(	(
<g/>
macOS	macOS	k?	macOS
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
při	při	k7c6	při
najetí	najetí	k1gNnSc6	najetí
na	na	k7c4	na
hypertextový	hypertextový	k2eAgInSc4d1	hypertextový
odkaz	odkaz	k1gInSc4	odkaz
se	se	k3xPyFc4	se
kurzor	kurzor	k1gInSc1	kurzor
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
ručičku	ručička	k1gFnSc4	ručička
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
informativní	informativní	k2eAgInSc4d1	informativní
text	text	k1gInSc4	text
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
webovém	webový	k2eAgInSc6d1	webový
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
atribut	atribut	k1gInSc1	atribut
'	'	kIx"	'
<g/>
title	titla	k1gFnSc6	titla
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
alt	alt	k1gInSc1	alt
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
nestandardní	standardní	k2eNgInSc4d1	nestandardní
'	'	kIx"	'
<g/>
tooltip	tooltip	k1gInSc4	tooltip
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
kurzor	kurzor	k1gInSc1	kurzor
posune	posunout	k5eAaPmIp3nS	posunout
mimo	mimo	k7c4	mimo
odkaz	odkaz	k1gInSc4	odkaz
<g/>
;	;	kIx,	;
pomocný	pomocný	k2eAgInSc4d1	pomocný
text	text	k1gInSc4	text
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
programu	program	k1gInSc6	program
HyperCard	HyperCard	k1gInSc4	HyperCard
pro	pro	k7c4	pro
macOS	macOS	k?	macOS
</s>
</p>
<p>
<s>
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
obrázku	obrázek	k1gInSc2	obrázek
nebo	nebo	k8xC	nebo
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kurzor	kurzor	k1gInSc1	kurzor
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
s	s	k7c7	s
roztaženými	roztažený	k2eAgInPc7d1	roztažený
prsty	prst	k1gInPc7	prst
<g/>
;	;	kIx,	;
po	po	k7c6	po
kliknutí	kliknutí	k1gNnSc6	kliknutí
myši	myš	k1gFnSc2	myš
se	se	k3xPyFc4	se
ruka	ruka	k1gFnSc1	ruka
sevře	sevřít	k5eAaPmIp3nS	sevřít
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dokument	dokument	k1gInSc4	dokument
posunout	posunout	k5eAaPmF	posunout
</s>
</p>
<p>
<s>
v	v	k7c6	v
grafických	grafický	k2eAgInPc6d1	grafický
programech	program	k1gInPc6	program
se	se	k3xPyFc4	se
kurzor	kurzor	k1gInSc1	kurzor
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
nástroje	nástroj	k1gInPc4	nástroj
používané	používaný	k2eAgInPc4d1	používaný
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgFnPc4d1	aktuální
vybranou	vybraný	k2eAgFnSc4d1	vybraná
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
štětec	štětec	k1gInSc1	štětec
<g/>
,	,	kIx,	,
guma	guma	k1gFnSc1	guma
<g/>
,	,	kIx,	,
kyblík	kyblík	k?	kyblík
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
Textové	textový	k2eAgNnSc1d1	textové
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
neumožňovalo	umožňovat	k5eNaImAgNnS	umožňovat
zobrazit	zobrazit	k5eAaPmF	zobrazit
další	další	k2eAgInSc4d1	další
znak	znak	k1gInSc4	znak
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
aktuálního	aktuální	k2eAgInSc2d1	aktuální
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
kurzor	kurzor	k1gInSc4	kurzor
myši	myš	k1gFnSc2	myš
zobrazován	zobrazován	k2eAgInSc4d1	zobrazován
bílým	bílý	k2eAgInSc7d1	bílý
obdélníkem	obdélník	k1gInSc7	obdélník
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
jednoho	jeden	k4xCgInSc2	jeden
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
černobílé	černobílý	k2eAgNnSc1d1	černobílé
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
změnou	změna	k1gFnSc7	změna
pozadí	pozadí	k1gNnSc2	pozadí
znaku	znak	k1gInSc2	znak
na	na	k7c4	na
inverzní	inverzní	k2eAgFnSc4d1	inverzní
(	(	kIx(	(
<g/>
kontrastní	kontrastní	k2eAgFnSc4d1	kontrastní
<g/>
)	)	kIx)	)
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
v	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc4	zobrazení
myši	myš	k1gFnSc2	myš
pomocí	pomocí	k7c2	pomocí
obdélníku	obdélník	k1gInSc2	obdélník
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
na	na	k7c6	na
textové	textový	k2eAgFnSc6d1	textová
konzoli	konzole	k1gFnSc6	konzole
Linuxu	linux	k1gInSc2	linux
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
spuštěný	spuštěný	k2eAgMnSc1d1	spuštěný
démon	démon	k1gMnSc1	démon
gpm	gpm	k?	gpm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systémech	systém	k1gInPc6	systém
DOS	DOS	kA	DOS
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
spustit	spustit	k5eAaPmF	spustit
speciální	speciální	k2eAgInSc4d1	speciální
rezidentní	rezidentní	k2eAgInSc4d1	rezidentní
ovladač	ovladač	k1gInSc4	ovladač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nebyl	být	k5eNaImAgInS	být
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
na	na	k7c6	na
disketě	disketa	k1gFnSc6	disketa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
myší	myš	k1gFnSc7	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kurzor	kurzor	k1gInSc1	kurzor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
