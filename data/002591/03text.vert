<s>
Supercrooo	Supercrooo	k6eAd1	Supercrooo
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
hip-hopová	hipopový	k2eAgFnSc1d1	hip-hopová
/	/	kIx~	/
rapová	rapový	k2eAgFnSc1d1	rapová
dvojice	dvojice	k1gFnSc1	dvojice
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hudebníky	hudebník	k1gMnPc7	hudebník
s	s	k7c7	s
uměleckými	umělecký	k2eAgNnPc7d1	umělecké
jmény	jméno	k1gNnPc7	jméno
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
(	(	kIx(	(
<g/>
také	také	k9	také
Hack	Hack	k1gInSc1	Hack
<g/>
)	)	kIx)	)
a	a	k8xC	a
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
také	také	k9	také
Phat	Phat	k1gInSc1	Phat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hip-hopové	Hipopový	k2eAgNnSc1d1	Hip-hopový
uskupení	uskupení	k1gNnSc1	uskupení
s	s	k7c7	s
názvem	název	k1gInSc7	název
K.O.	K.O.	k1gMnPc1	K.O.
Kru	kra	k1gFnSc4	kra
založili	založit	k5eAaPmAgMnP	založit
Hack	Hack	k1gMnSc1	Hack
s	s	k7c7	s
Phatem	Phat	k1gMnSc7	Phat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Debutovalo	debutovat	k5eAaBmAgNnS	debutovat
vinylovým	vinylový	k2eAgInSc7d1	vinylový
singlem	singl	k1gInSc7	singl
Nádech	nádech	k1gInSc1	nádech
/	/	kIx~	/
Náš	náš	k3xOp1gInSc1	náš
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vydal	vydat	k5eAaPmAgMnS	vydat
P.	P.	kA	P.
<g/>
A.	A.	kA	A.
<g/>
trick	tricka	k1gFnPc2	tricka
records	recordsa	k1gFnPc2	recordsa
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Wichovým	Wichův	k2eAgInSc7d1	Wichův
remixem	remix	k1gInSc7	remix
na	na	k7c6	na
celočeské	celočeský	k2eAgFnSc6d1	celočeský
kompilaci	kompilace	k1gFnSc6	kompilace
Lyrik	lyrika	k1gFnPc2	lyrika
Derby	derby	k1gNnSc1	derby
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Phat	Phat	k1gInSc1	Phat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
začal	začít	k5eAaPmAgMnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
u	u	k7c2	u
P.	P.	kA	P.
<g/>
A.	A.	kA	A.
<g/>
trick	trick	k6eAd1	trick
records	records	k6eAd1	records
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
desky	deska	k1gFnSc2	deska
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sólová	sólový	k2eAgFnSc1d1	sólová
práce	práce	k1gFnSc1	práce
Frekvence	frekvence	k1gFnSc2	frekvence
P.	P.	kA	P.
<g/>
H.A.T.	H.A.T.	k1gFnSc1	H.A.T.
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
PSH	PSH	kA	PSH
-	-	kIx~	-
Terrorist	Terrorist	k1gMnSc1	Terrorist
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
hostovali	hostovat	k5eAaImAgMnP	hostovat
například	například	k6eAd1	například
LA	la	k1gNnSc4	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
a	a	k8xC	a
taky	taky	k6eAd1	taky
Hack	Hacka	k1gFnPc2	Hacka
<g/>
.	.	kIx.	.
</s>
<s>
Produkoval	produkovat	k5eAaImAgMnS	produkovat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
části	část	k1gFnSc2	část
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vypomohli	vypomoct	k5eAaPmAgMnP	vypomoct
pražští	pražský	k2eAgMnPc1d1	pražský
Wich	Wicha	k1gFnPc2	Wicha
<g/>
,	,	kIx,	,
Enemy	Enema	k1gFnSc2	Enema
a	a	k8xC	a
Hack	Hacka	k1gFnPc2	Hacka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
méně	málo	k6eAd2	málo
samplování	samplování	k1gNnSc4	samplování
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
syntetických	syntetický	k2eAgInPc2d1	syntetický
a	a	k8xC	a
elektronických	elektronický	k2eAgInPc2d1	elektronický
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
specifický	specifický	k2eAgInSc1d1	specifický
beat	beat	k1gInSc1	beat
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rap	rap	k1gMnSc1	rap
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
K.O.	K.O.	k?	K.O.
Kru	kra	k1gFnSc4	kra
společně	společně	k6eAd1	společně
po	po	k7c6	po
sólo	sólo	k2eAgFnSc6d1	sólo
pauze	pauza	k1gFnSc6	pauza
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
singl	singl	k1gInSc4	singl
Kéry	Kéra	k1gMnSc2	Kéra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
kompilaci	kompilace	k1gFnSc6	kompilace
From	From	k1gInSc1	From
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
To	to	k9	to
Praha	Praha	k1gFnSc1	Praha
na	na	k7c4	na
labelu	labela	k1gFnSc4	labela
Maddrum	Maddrum	k1gNnSc1	Maddrum
rec	rec	k?	rec
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k8xS	jako
Supercrooo	Supercrooo	k6eAd1	Supercrooo
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
na	na	k7c4	na
kompilaci	kompilace	k1gFnSc4	kompilace
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
Unia	Unium	k1gNnSc2	Unium
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
a	a	k8xC	a
nekompromisní	kompromisní	k2eNgFnSc4d1	nekompromisní
lyriku	lyrika	k1gFnSc4	lyrika
s	s	k7c7	s
volnými	volný	k2eAgFnPc7d1	volná
asociacemi	asociace	k1gFnPc7	asociace
a	a	k8xC	a
paradoxními	paradoxní	k2eAgNnPc7d1	paradoxní
slovními	slovní	k2eAgNnPc7d1	slovní
spojeními	spojení	k1gNnPc7	spojení
<g/>
,	,	kIx,	,
inspirovanou	inspirovaný	k2eAgFnSc4d1	inspirovaná
komiksem	komiks	k1gInSc7	komiks
a	a	k8xC	a
žánrem	žánr	k1gInSc7	žánr
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Toxic	Toxice	k1gFnPc2	Toxice
Funk	funk	k1gInSc4	funk
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
u	u	k7c2	u
Maddrum	Maddrum	k1gNnSc4	Maddrum
records	records	k1gInSc4	records
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgNnSc1d1	plné
nekompromisního	kompromisní	k2eNgInSc2d1	nekompromisní
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
,	,	kIx,	,
perverzní	perverzní	k2eAgFnSc2d1	perverzní
tematiky	tematika	k1gFnSc2	tematika
a	a	k8xC	a
nesamplovaných	samplovaný	k2eNgInPc2d1	samplovaný
<g/>
,	,	kIx,	,
toxických	toxický	k2eAgInPc2d1	toxický
a	a	k8xC	a
elektro	elektro	k6eAd1	elektro
beatů	beat	k1gInPc2	beat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
albem	album	k1gNnSc7	album
Supercrooo	Supercrooo	k6eAd1	Supercrooo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
České	český	k2eAgFnSc3d1	Česká
Kuře	kura	k1gFnSc3	kura
<g/>
:	:	kIx,	:
<g/>
Neurofolk	Neurofolk	k1gInSc1	Neurofolk
vydané	vydaný	k2eAgFnSc2d1	vydaná
u	u	k7c2	u
Nejbr	Nejbra	k1gFnPc2	Nejbra
Beats	Beatsa	k1gFnPc2	Beatsa
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
jsou	být	k5eAaImIp3nP	být
beaty	beat	k1gInPc1	beat
ještě	ještě	k6eAd1	ještě
tvrdší	tvrdý	k2eAgInPc1d2	tvrdší
<g/>
,	,	kIx,	,
agresivnější	agresivní	k2eAgInPc1d2	agresivnější
a	a	k8xC	a
temnější	temný	k2eAgInPc1d2	temnější
než	než	k8xS	než
na	na	k7c6	na
předchozím	předchozí	k2eAgNnSc6d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
propracovanější	propracovaný	k2eAgFnPc4d2	propracovanější
a	a	k8xC	a
rapy	rapa	k1gFnPc1	rapa
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
více	hodně	k6eAd2	hodně
psychedelické	psychedelický	k2eAgFnPc1d1	psychedelická
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vydali	vydat	k5eAaPmAgMnP	vydat
u	u	k7c2	u
vydavetelství	vydavetelství	k1gNnSc2	vydavetelství
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
první	první	k4xOgFnPc1	první
české	český	k2eAgFnPc1d1	Česká
rapové	rapový	k2eAgFnPc1d1	rapová
dvojCD	dvojCD	k?	dvojCD
Dva	dva	k4xCgMnPc1	dva
Nosáči	nosáč	k1gMnPc1	nosáč
Tankujou	tankovat	k5eAaImIp3nP	tankovat
Super	super	k1gInSc4	super
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
shrnují	shrnovat	k5eAaImIp3nP	shrnovat
svou	svůj	k3xOyFgFnSc4	svůj
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
tvorbu	tvorba	k1gFnSc4	tvorba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1996	[number]	k4	1996
až	až	k6eAd1	až
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
známých	známý	k2eAgFnPc2d1	známá
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
nevydané	vydaný	k2eNgFnPc4d1	nevydaná
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
remixů	remix	k1gInPc2	remix
a	a	k8xC	a
nahrávky	nahrávka	k1gFnSc2	nahrávka
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
celého	celý	k2eAgInSc2d1	celý
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
Kariéra	kariéra	k1gFnSc1	kariéra
16	[number]	k4	16
remix	remix	k1gInSc1	remix
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Vec	Vec	k?	Vec
a	a	k8xC	a
H	H	kA	H
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k9	i
videoklip	videoklip	k1gInSc1	videoklip
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Rista	Rist	k1gInSc2	Rist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgMnS	vydat
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc4	Toxxx
své	svůj	k3xOyFgFnSc3	svůj
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
Rok	rok	k1gInSc1	rok
Psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgMnS	vydat
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
společně	společně	k6eAd1	společně
s	s	k7c7	s
Orionem	orion	k1gInSc7	orion
z	z	k7c2	z
PSH	PSH	kA	PSH
album	album	k1gNnSc4	album
Orikoule	Orikoule	k1gFnSc2	Orikoule
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
vydavelství	vydavelství	k1gNnSc2	vydavelství
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vydáním	vydání	k1gNnSc7	vydání
alb	alba	k1gFnPc2	alba
Toxic	Toxice	k1gFnPc2	Toxice
Funk	funk	k1gInSc1	funk
a	a	k8xC	a
České	český	k2eAgNnSc1d1	české
Kuře	kuře	k1gNnSc1	kuře
<g/>
:	:	kIx,	:
<g/>
Neurofolk	Neurofolk	k1gInSc1	Neurofolk
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
experimentální	experimentální	k2eAgInSc1d1	experimentální
elektro	elektro	k6eAd1	elektro
projekt	projekt	k1gInSc1	projekt
Dixxx	Dixxx	k1gInSc1	Dixxx
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
kromě	kromě	k7c2	kromě
Hacka	Hacek	k1gInSc2	Hacek
s	s	k7c7	s
Phatem	Phat	k1gInSc7	Phat
i	i	k8xC	i
Risto	Risto	k1gNnSc1	Risto
s	s	k7c7	s
Lucasem	Lucas	k1gMnSc7	Lucas
Skunkwalkerem	Skunkwalker	k1gMnSc7	Skunkwalker
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
mastering	mastering	k1gInSc4	mastering
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dixxx	Dixxx	k1gInSc4	Dixxx
vydali	vydat	k5eAaPmAgMnP	vydat
desku	deska	k1gFnSc4	deska
DIXXX	DIXXX	kA	DIXXX
na	na	k7c6	na
labelu	label	k1gInSc6	label
Rapsport	Rapsport	k1gInSc1	Rapsport
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
SuperCrooo	SuperCrooo	k6eAd1	SuperCrooo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
500	[number]	k4	500
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydali	vydat	k5eAaPmAgMnP	vydat
i	i	k9	i
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
Dixxx	Dixxx	k1gInSc1	Dixxx
-	-	kIx~	-
Extended	Extended	k1gInSc1	Extended
Edition	Edition	k1gInSc1	Edition
doplněnou	doplněný	k2eAgFnSc4d1	doplněná
o	o	k7c4	o
remixy	remix	k1gInPc4	remix
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
obalem	obal	k1gInSc7	obal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mixtapu	mixtap	k1gInSc6	mixtap
Viktora	Viktor	k1gMnSc2	Viktor
Hazarda	hazard	k1gMnSc2	hazard
Rap	rap	k1gMnSc1	rap
Superstar	superstar	k1gFnSc1	superstar
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
skladba	skladba	k1gFnSc1	skladba
Top	topit	k5eAaImRp2nS	topit
Rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
ke	k	k7c3	k
stažení	stažení	k1gNnSc2	stažení
skladbu	skladba	k1gFnSc4	skladba
Biatches	Biatchesa	k1gFnPc2	Biatchesa
<g/>
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
moderátor	moderátor	k1gMnSc1	moderátor
Poeta	poeta	k1gMnSc1	poeta
spolu	spolu	k6eAd1	spolu
uváděli	uvádět	k5eAaImAgMnP	uvádět
pořad	pořad	k1gInSc4	pořad
o	o	k7c4	o
rapu	rapa	k1gFnSc4	rapa
Rapgame	Rapgam	k1gInSc5	Rapgam
na	na	k7c4	na
Radio	radio	k1gNnSc4	radio
SPIN	spina	k1gFnPc2	spina
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
jednou	jednou	k6eAd1	jednou
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
určitému	určitý	k2eAgNnSc3d1	určité
tématu	téma	k1gNnSc3	téma
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
hosty	host	k1gMnPc7	host
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c6	na
rádiu	rádio	k1gNnSc6	rádio
Spin	spina	k1gFnPc2	spina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Poetu	poeta	k1gMnSc4	poeta
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Willie-mack	Willieack	k1gMnSc1	Willie-mack
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
Dyrtym	Dyrtym	k1gInSc4	Dyrtym
<g/>
.	.	kIx.	.
</s>
<s>
K.O.	K.O.	k?	K.O.
Krů	Krů	k1gFnSc1	Krů
-	-	kIx~	-
Nádech	nádech	k1gInSc1	nádech
<g/>
/	/	kIx~	/
<g/>
Náš	náš	k3xOp1gInSc1	náš
cíl	cíl	k1gInSc1	cíl
(	(	kIx(	(
<g/>
Vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Phat	Phat	k1gInSc1	Phat
-	-	kIx~	-
Frekvence	frekvence	k1gFnSc1	frekvence
P.	P.	kA	P.
<g/>
H.A.T.	H.A.T.	k1gFnSc1	H.A.T.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Phat	Phat	k1gInSc1	Phat
-	-	kIx~	-
Frekvence	frekvence	k1gFnSc1	frekvence
P.	P.	kA	P.
<g/>
H.A.T	H.A.T	k1gFnSc1	H.A.T
<g/>
/	/	kIx~	/
<g/>
Modelky	modelka	k1gFnPc1	modelka
(	(	kIx(	(
<g/>
Vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Supercrooo	Supercrooo	k1gNnSc1	Supercrooo
-	-	kIx~	-
Toxic	Toxice	k1gFnPc2	Toxice
Funk	funk	k1gInSc1	funk
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Dixxx	Dixxx	k1gInSc1	Dixxx
-	-	kIx~	-
Dixxx	Dixxx	k1gInSc1	Dixxx
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Supercrooo	Supercrooo	k1gNnSc1	Supercrooo
-	-	kIx~	-
Baby	baby	k1gNnSc1	baby
(	(	kIx(	(
<g/>
Vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Supercrooo	Supercrooo	k6eAd1	Supercrooo
-	-	kIx~	-
České	český	k2eAgNnSc1d1	české
Kuře	kuře	k1gNnSc1	kuře
<g/>
:	:	kIx,	:
<g/>
Neurofolk	Neurofolk	k1gInSc1	Neurofolk
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Dixxx	Dixxx	k1gInSc1	Dixxx
-	-	kIx~	-
Dixxx	Dixxx	k1gInSc1	Dixxx
Extended	Extended	k1gMnSc1	Extended
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Supercooo	Supercooo	k6eAd1	Supercooo
-	-	kIx~	-
2	[number]	k4	2
Nosáči	nosáč	k1gMnPc1	nosáč
Tankujou	tankovat	k5eAaImIp3nP	tankovat
Super	super	k2eAgMnSc1d1	super
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
-	-	kIx~	-
Rok	rok	k1gInSc1	rok
Psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
James	James	k1gInSc4	James
Cole	cola	k1gFnSc3	cola
a	a	k8xC	a
Orion	orion	k1gInSc1	orion
-	-	kIx~	-
Orikoule	Orikoule	k1gFnSc1	Orikoule
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
Kapitán	kapitán	k1gMnSc1	kapitán
Láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
Cole-	Cole-	k1gMnSc1	Cole-
Halucinace	halucinace	k1gFnSc1	halucinace
ze	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
třetího	třetí	k4xOgNnSc2	třetí
patra	patro	k1gNnSc2	patro
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
-	-	kIx~	-
Legální	legální	k2eAgFnSc2d1	legální
Drogy	droga	k1gFnSc2	droga
+	+	kIx~	+
Illegální	Illegální	k2eAgInPc1d1	Illegální
Kecy	kec	k1gInPc1	kec
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
James	James	k1gInSc4	James
Cole	cola	k1gFnSc3	cola
a	a	k8xC	a
DJ	DJ	kA	DJ
Scarface	Scarface	k1gFnSc1	Scarface
-	-	kIx~	-
Jed	jed	k1gInSc1	jed
na	na	k7c4	na
krysy	krysa	k1gFnPc4	krysa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
-	-	kIx~	-
Bauch	Bauch	k1gInSc1	Bauch
Money	Monea	k1gFnSc2	Monea
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
+	+	kIx~	+
LA4	LA4	k1gFnSc1	LA4
+	+	kIx~	+
Mike	Mike	k1gFnSc1	Mike
<g />
.	.	kIx.	.
</s>
<s>
Trafik	trafika	k1gFnPc2	trafika
-	-	kIx~	-
Nadzemi-EP	Nadzemi-EP	k1gFnSc1	Nadzemi-EP
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
-	-	kIx~	-
Moby	Moby	k1gInPc4	Moby
Dick	Dick	k1gInSc1	Dick
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
-	-	kIx~	-
BMKG	BMKG	kA	BMKG
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
-	-	kIx~	-
Orfeus	Orfeus	k1gMnSc1	Orfeus
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
VA	va	k0wR	va
-	-	kIx~	-
Lyrik	lyrika	k1gFnPc2	lyrika
Derby	derby	k1gNnSc2	derby
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
<g/>
Nádech	nádech	k1gInSc1	nádech
-	-	kIx~	-
Wich	Wich	k1gInSc1	Wich
RMX	RMX	kA	RMX
/	/	kIx~	/
<g/>
SC	SC	kA	SC
ex	ex	k6eAd1	ex
K.O.	K.O.	k1gFnSc1	K.O.
+	+	kIx~	+
Standardní	standardní	k2eAgInSc1d1	standardní
Režim	režim	k1gInSc1	režim
/	/	kIx~	/
<g/>
SC	SC	kA	SC
ex	ex	k6eAd1	ex
K.O.	K.O.	k1gFnSc1	K.O.
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
From	From	k1gInSc1	From
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
To	to	k9	to
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kéry	Kéra	k1gFnSc2	Kéra
/	/	kIx~	/
<g/>
SC	SC	kA	SC
ex	ex	k6eAd1	ex
K.O.	K.O.	k1gFnSc1	K.O.
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
Unia	Uni	k1gInSc2	Uni
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
2005	[number]	k4	2005
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
La	la	k1gNnSc4	la
<g/>
4	[number]	k4	4
-	-	kIx~	-
Elá	Elá	k1gFnSc1	Elá
Pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
Na	na	k7c4	na
Světlo	světlo	k1gNnSc4	světlo
Denní	denní	k2eAgInSc1d1	denní
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Until	Until	k1gMnSc1	Until
That	That	k1gMnSc1	That
Day	Day	k1gMnSc1	Day
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
Supercrooo	Supercrooo	k1gMnSc1	Supercrooo
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
Wich	Wich	k1gInSc1	Wich
-	-	kIx~	-
Time	Time	k1gInSc1	Time
Is	Is	k1gMnSc2	Is
Now	Now	k1gMnSc2	Now
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jádro	jádro	k1gNnSc1	jádro
Kudla	kudla	k1gFnSc1	kudla
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
11	[number]	k4	11
<g/>
]	]	kIx)	]
Gipsy	Gipsa	k1gFnPc1	Gipsa
-	-	kIx~	-
Rýmy	rýma	k1gFnPc1	rýma
a	a	k8xC	a
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sexy	sex	k1gInPc1	sex
<g/>
,	,	kIx,	,
Funky	funk	k1gInPc1	funk
a	a	k8xC	a
Zlej	zlít	k5eAaPmRp2nS	zlít
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
Marpo	Marpa	k1gFnSc5	Marpa
-	-	kIx~	-
Původ	původ	k1gInSc1	původ
Umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Velrybí	velrybí	k2eAgFnSc1d1	velrybí
Bolest	bolest	k1gFnSc1	bolest
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
Orion	orion	k1gInSc1	orion
-	-	kIx~	-
Teritorium	teritorium	k1gNnSc1	teritorium
2	[number]	k4	2
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zápas	zápas	k1gInSc4	zápas
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
Prago	Prago	k6eAd1	Prago
Union	union	k1gInSc1	union
-	-	kIx~	-
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
VAR	var	k1gInSc4	var
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Nejbr	Nejbr	k1gInSc1	Nejbr
Hip	hip	k0	hip
Hop	hop	k0	hop
mix	mix	k1gInSc4	mix
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
nádherný	nádherný	k2eAgInSc1d1	nádherný
/	/	kIx~	/
<g/>
Dixxx	Dixxx	k1gInSc1	Dixxx
+	+	kIx~	+
Party	party	k1gFnSc1	party
Shit	Shit	k1gMnSc1	Shit
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Utkej	utkat	k5eAaPmRp2nS	utkat
se	se	k3xPyFc4	se
v	v	k7c4	v
rapu	rapa	k1gFnSc4	rapa
presents	presents	k6eAd1	presents
CzechoSlovak	CzechoSlovak	k1gInSc1	CzechoSlovak
Beats	Beats	k1gInSc1	Beats
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g/>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
nádherný	nádherný	k2eAgInSc1d1	nádherný
/	/	kIx~	/
<g/>
Dixxx	Dixxx	k1gInSc1	Dixxx
[	[	kIx(	[
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
Viktor	Viktor	k1gMnSc1	Viktor
Hazard	hazard	k1gMnSc1	hazard
-	-	kIx~	-
Rapsuperstar	Rapsuperstar	k1gMnSc1	Rapsuperstar
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Top	topit	k5eAaImRp2nS	topit
Rock	rock	k1gInSc1	rock
/	/	kIx~	/
<g/>
Dixxx	Dixxx	k1gInSc1	Dixxx
[	[	kIx(	[
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
Moja	Moja	k?	Moja
Reč	Reč	k1gFnSc1	Reč
-	-	kIx~	-
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
Show	show	k1gFnSc6	show
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tanker	tanker	k1gInSc4	tanker
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
PSH	PSH	kA	PSH
-	-	kIx~	-
Rap	rap	k1gMnSc1	rap
'	'	kIx"	'
<g/>
N	N	kA	N
<g/>
'	'	kIx"	'
Roll	Roll	k1gMnSc1	Roll
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Wolfův	Wolfův	k2eAgInSc1d1	Wolfův
Revír	revír	k1gInSc1	revír
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
+	+	kIx~	+
Parket	parket	k1gInSc1	parket
RMX	RMX	kA	RMX
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
<g/>
,	,	kIx,	,
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
,	,	kIx,	,
Indy	Indus	k1gInPc1	Indus
[	[	kIx(	[
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Nejbr	Nejbr	k1gInSc1	Nejbr
Hip	hip	k0	hip
Hop	hop	k0	hop
mix	mix	k1gInSc4	mix
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Nejsom	Nejsom	k1gInSc4	Nejsom
Falošný	Falošný	k2eAgInSc4d1	Falošný
/	/	kIx~	/
<g/>
H	H	kA	H
<g/>
16	[number]	k4	16
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
+	+	kIx~	+
Nemala	Nemala	k1gFnSc1	Nemala
By	by	kYmCp3nP	by
Si	se	k3xPyFc3	se
Byť	byť	k8xS	byť
/	/	kIx~	/
<g/>
Zverina	Zverina	k1gMnSc1	Zverina
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Dávej	dávat	k5eAaImRp2nS	dávat
Bacha	Bacha	k?	Bacha
/	/	kIx~	/
<g/>
SC	SC	kA	SC
<g/>
,	,	kIx,	,
Moja	Moja	k?	Moja
Reč	Reč	k1gFnSc1	Reč
<g/>
,	,	kIx,	,
Vec	Vec	k1gFnSc1	Vec
<g/>
,	,	kIx,	,
Katka	Katka	k1gFnSc1	Katka
Winterová	Winterová	k1gFnSc1	Winterová
[	[	kIx(	[
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
Viktor	Viktor	k1gMnSc1	Viktor
Hazard	hazard	k1gMnSc1	hazard
-	-	kIx~	-
Rapsuperstar	Rapsuperstar	k1gMnSc1	Rapsuperstar
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jako	jako	k8xS	jako
Víno	víno	k1gNnSc1	víno
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
DJ	DJ	kA	DJ
Neo	Neo	k1gFnSc1	Neo
-	-	kIx~	-
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
/	/	kIx~	/
<g/>
Orion	orion	k1gInSc4	orion
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
23	[number]	k4	23
<g/>
]	]	kIx)	]
Orion	orion	k1gInSc1	orion
-	-	kIx~	-
Teritorium	teritorium	k1gNnSc1	teritorium
Remixy	Remix	k1gInPc7	Remix
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kam	kam	k6eAd1	kam
Vítr	vítr	k1gInSc1	vítr
Tam	tam	k6eAd1	tam
Plášť	plášť	k1gInSc4	plášť
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Ego	ego	k1gNnSc4	ego
+	+	kIx~	+
Zápas	zápas	k1gInSc1	zápas
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
24	[number]	k4	24
<g/>
]	]	kIx)	]
Marko	Marko	k1gMnSc1	Marko
-	-	kIx~	-
Velmi	velmi	k6eAd1	velmi
Nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
Známosti	známost	k1gFnSc2	známost
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Auta	auto	k1gNnPc4	auto
<g/>
,	,	kIx,	,
Rap	rapa	k1gFnPc2	rapa
<g/>
,	,	kIx,	,
Stejky	Stejka	k1gFnSc2	Stejka
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Verbální	verbální	k2eAgInSc1d1	verbální
Glock	Glock	k1gInSc1	Glock
/	/	kIx~	/
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
+	+	kIx~	+
Boss	boss	k1gMnSc1	boss
/	/	kIx~	/
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
<g />
.	.	kIx.	.
</s>
<s>
518	[number]	k4	518
<g/>
,	,	kIx,	,
La	la	k1gNnSc7	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
[	[	kIx(	[
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
DJ	DJ	kA	DJ
Yanko	Yanko	k1gNnSc1	Yanko
-	-	kIx~	-
Thug	Thug	k1gMnSc1	Thug
Errcitě	Errcita	k1gFnSc3	Errcita
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dřepim	Dřepim	k?	Dřepim
S	s	k7c7	s
Courou	Courou	k?	Courou
/	/	kIx~	/
<g/>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
26	[number]	k4	26
<g/>
]	]	kIx)	]
DJ	DJ	kA	DJ
Kappa	kappa	k1gNnSc1	kappa
-	-	kIx~	-
Hra	hra	k1gFnSc1	hra
Sa	Sa	k1gFnSc2	Sa
Začíná	začínat	k5eAaImIp3nS	začínat
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sweet	Sweet	k1gInSc1	Sweet
Dreams	Dreams	k1gInSc1	Dreams
/	/	kIx~	/
<g/>
SC	SC	kA	SC
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
W518	W518	k4	W518
[	[	kIx(	[
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
Shadow	Shadow	k1gFnSc1	Shadow
Drrop	Drrop	k1gInSc1	Drrop
-	-	kIx~	-
Heavy	Heava	k1gFnSc2	Heava
Metal	metat	k5eAaImAgMnS	metat
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bartender	Bartender	k1gMnSc1	Bartender
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
LA4	LA4	k1gFnSc1	LA4
-	-	kIx~	-
Panoptikum	panoptikum	k1gNnSc1	panoptikum
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mánie	mánie	k1gFnSc1	mánie
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
Haf	haf	k1gInSc1	haf
&	&	k?	&
Beyuz	Beyuz	k1gInSc1	Beyuz
-	-	kIx~	-
V	v	k7c6	v
Živote	život	k1gInSc5	život
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Homotrek	Homotrek	k1gMnSc1	Homotrek
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Zverina	Zverina	k1gFnSc1	Zverina
[	[	kIx(	[
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
Iscream	Iscream	k1gInSc1	Iscream
boyz	boyz	k1gInSc1	boyz
-	-	kIx~	-
Iscream	Iscream	k1gInSc1	Iscream
boyz	boyza	k1gFnPc2	boyza
Mixtape	Mixtap	k1gInSc5	Mixtap
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Problem	Problo	k1gNnSc7	Problo
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
31	[number]	k4	31
<g/>
]	]	kIx)	]
MAAT	MAAT	kA	MAAT
-	-	kIx~	-
Kandidáti	kandidát	k1gMnPc1	kandidát
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Nemužeš	Nemužeš	k1gFnSc7	Nemužeš
se	se	k3xPyFc4	se
probudit	probudit	k5eAaPmF	probudit
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
+	+	kIx~	+
Pimpsleď	Pimpsleď	k1gMnSc1	Pimpsleď
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Nemužeš	Nemužeš	k1gFnPc1	Nemužeš
se	se	k3xPyFc4	se
probudit	probudit	k5eAaPmF	probudit
(	(	kIx(	(
<g/>
JSM	JSM	kA	JSM
Remix	Remix	k1gInSc1	Remix
<g/>
)	)	kIx)	)
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
32	[number]	k4	32
<g/>
]	]	kIx)	]
Berezin	Berezina	k1gFnPc2	Berezina
-	-	kIx~	-
P-13	P-13	k1gFnSc1	P-13
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sterva	Sterva	k1gFnSc1	Sterva
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
[	[	kIx(	[
<g/>
33	[number]	k4	33
<g/>
]	]	kIx)	]
+	+	kIx~	+
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
Pt	Pt	k1gMnSc1	Pt
<g/>
2	[number]	k4	2
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
SC	SC	kA	SC
<g/>
,	,	kIx,	,
LA	la	k1gNnSc2	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
W518	W518	k1gFnSc1	W518
(	(	kIx(	(
<g/>
Bonus	bonus	k1gInSc1	bonus
track	track	k1gInSc1	track
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Grimaso	grimasa	k1gFnSc5	grimasa
-	-	kIx~	-
Kto	Kto	k1gFnSc3	Kto
dá	dát	k5eAaPmIp3nS	dát
viac	viac	k1gFnSc1	viac
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
/	/	kIx~	/
<g/>
SC	SC	kA	SC
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Otecko	otecko	k1gMnSc1	otecko
[	[	kIx(	[
<g/>
34	[number]	k4	34
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Nejbr	Nejbr	k1gInSc1	Nejbr
Hip	hip	k0	hip
Hop	hop	k0	hop
mix	mix	k1gInSc4	mix
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Nemůžeš	moct	k5eNaImIp2nS	moct
se	se	k3xPyFc4	se
probudit	probudit	k5eAaPmF	probudit
/	/	kIx~	/
<g/>
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
MAAT	MAAT	kA	MAAT
<g/>
[	[	kIx(	[
<g/>
35	[number]	k4	35
<g/>
]	]	kIx)	]
Vladimir	Vladimir	k1gInSc1	Vladimir
<g/>
518	[number]	k4	518
-	-	kIx~	-
Gorila	gorila	k1gFnSc1	gorila
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Smíchov	Smíchov	k1gInSc1	Smíchov
-	-	kIx~	-
Újezd	Újezd	k1gInSc1	Újezd
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
+	+	kIx~	+
Vítěz	vítěz	k1gMnSc1	vítěz
sebere	sebrat	k5eAaPmIp3nS	sebrat
všechno	všechen	k3xTgNnSc4	všechen
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
LA4	LA4	k1gFnSc4	LA4
[	[	kIx(	[
<g/>
36	[number]	k4	36
<g/>
]	]	kIx)	]
Cosmic	Cosmic	k1gMnSc1	Cosmic
Crew	Crew	k1gMnSc1	Crew
-	-	kIx~	-
Electric	Electric	k1gMnSc1	Electric
City	City	k1gFnSc2	City
MIXTAPE	MIXTAPE	kA	MIXTAPE
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Transformer	Transformer	k1gMnSc1	Transformer
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
37	[number]	k4	37
<g/>
]	]	kIx)	]
Vec	Vec	k1gFnSc6	Vec
-	-	kIx~	-
Funkčný	Funkčný	k2eAgMnSc1d1	Funkčný
Veterán	veterán	k1gMnSc1	veterán
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ony	onen	k3xDgInPc1	onen
si	se	k3xPyFc3	se
ma	ma	k?	ma
nájdu	nájdu	k6eAd1	nájdu
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Delik	Delik	k1gInSc1	Delik
+	+	kIx~	+
1	[number]	k4	1
<g/>
.	.	kIx.	.
milion	milion	k4xCgInSc1	milion
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
38	[number]	k4	38
<g/>
]	]	kIx)	]
H16	H16	k1gFnSc2	H16
-	-	kIx~	-
Čísla	číslo	k1gNnSc2	číslo
nepustia	nepustium	k1gNnSc2	nepustium
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
S	s	k7c7	s
kým	kdo	k3yQnSc7	kdo
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
taký	taký	k?	taký
si	se	k3xPyFc3	se
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
39	[number]	k4	39
<g/>
]	]	kIx)	]
Iscream	Iscream	k1gInSc1	Iscream
boyz	boyz	k1gInSc1	boyz
-	-	kIx~	-
Mixtape	Mixtap	k1gInSc5	Mixtap
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
Remix	Remix	k1gInSc1	Remix
<g/>
)	)	kIx)	)
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
[	[	kIx(	[
<g/>
40	[number]	k4	40
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
Jižní	jižní	k2eAgNnPc1d1	jižní
Srnkobraní	Srnkobraní	k1gNnPc1	Srnkobraní
Vol	vol	k6eAd1	vol
<g/>
.2	.2	k4	.2
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
Skit	Skit	k2eAgMnSc1d1	Skit
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Pijeme	pít	k5eAaImIp1nP	pít
pivo	pivo	k1gNnSc4	pivo
/	/	kIx~	/
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
+	+	kIx~	+
Problem	Probl	k1gMnSc7	Probl
RMX	RMX	kA	RMX
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc4	Toxxx
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Iscream	Iscream	k1gInSc1	Iscream
boyz	boyz	k1gInSc1	boyz
+	+	kIx~	+
Kdo	kdo	k3yInSc1	kdo
ty	ten	k3xDgMnPc4	ten
jsi	být	k5eAaImIp2nS	být
<g/>
?	?	kIx.	?
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
tě	ty	k3xPp2nSc4	ty
zná	znát	k5eAaImIp3nS	znát
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc4	Toxxx
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Chang	Chang	k1gMnSc1	Chang
<g/>
,	,	kIx,	,
Šejna	Šejna	k1gFnSc1	Šejna
[	[	kIx(	[
<g/>
41	[number]	k4	41
<g/>
]	]	kIx)	]
Monkey	Monkea	k1gFnSc2	Monkea
Bussines	Bussines	k1gInSc1	Bussines
-	-	kIx~	-
Twilight	Twilight	k1gInSc1	Twilight
of	of	k?	of
Jesters	Jesters	k1gInSc1	Jesters
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
Wedding	Wedding	k1gInSc1	Wedding
Song	song	k1gInSc1	song
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
42	[number]	k4	42
<g/>
]	]	kIx)	]
Moja	Moja	k?	Moja
Reč	Reč	k1gMnSc1	Reč
-	-	kIx~	-
Dual	Dual	k1gMnSc1	Dual
Shock	Shock	k1gMnSc1	Shock
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
Na	na	k7c6	na
voľnej	voľnat	k5eAaImRp2nS	voľnat
nohe	nohe	k1gNnSc4	nohe
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Piroman	Piroman	k1gMnSc1	Piroman
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
[	[	kIx(	[
<g/>
43	[number]	k4	43
<g/>
]	]	kIx)	]
DJ	DJ	kA	DJ
Mike	Mike	k1gFnPc2	Mike
Trafik	trafika	k1gFnPc2	trafika
-	-	kIx~	-
H.	H.	kA	H.
<g/>
P.	P.	kA	P.
<g/>
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
Sampler	Sampler	k1gMnSc1	Sampler
vol	vol	k6eAd1	vol
<g/>
.1	.1	k4	.1
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pervitin	pervitin	k1gInSc1	pervitin
<g/>
,	,	kIx,	,
Herák	Herák	k1gMnSc1	Herák
<g/>
,	,	kIx,	,
Toluen	toluen	k1gInSc1	toluen
a	a	k8xC	a
Pivo	pivo	k1gNnSc1	pivo
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Baar	Baar	k1gMnSc1	Baar
+	+	kIx~	+
Gastro	Gastro	k1gNnSc1	Gastro
Tour	Toura	k1gFnPc2	Toura
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Vec	Vec	k1gFnSc1	Vec
<g/>
,	,	kIx,	,
Kardinál	kardinál	k1gMnSc1	kardinál
Korec	Korec	k1gMnSc1	Korec
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Babica	Babica	k1gMnSc1	Babica
+	+	kIx~	+
Děti	dítě	k1gFnPc1	dítě
Prázdnoty	prázdnota	k1gFnSc2	prázdnota
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Indy	Indus	k1gInPc1	Indus
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
Lešek	Lešek	k1gMnSc1	Lešek
Semelka	Semelka	k1gMnSc1	Semelka
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
518	[number]	k4	518
+	+	kIx~	+
Kam	kam	k6eAd1	kam
mám	mít	k5eAaImIp1nS	mít
jít	jít	k5eAaImF	jít
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
+	+	kIx~	+
Rap	rap	k1gMnSc1	rap
Biz	Biz	k1gMnSc1	Biz
(	(	kIx(	(
<g/>
Summer	Summer	k1gMnSc1	Summer
rmx	rmx	k?	rmx
<g/>
)	)	kIx)	)
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
Tede	Ted	k1gMnSc5	Ted
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
+	+	kIx~	+
Proč	proč	k6eAd1	proč
Si	se	k3xPyFc3	se
Proboha	proboha	k0	proboha
Na	na	k7c4	na
Mě	já	k3xPp1nSc4	já
Tak	tak	k6eAd1	tak
Zlá	zlý	k2eAgFnSc1d1	zlá
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Dara	Dara	k1gFnSc1	Dara
Rolins	Rolins	k1gInSc1	Rolins
<g/>
,	,	kIx,	,
Zverina	Zverina	k1gFnSc1	Zverina
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
[	[	kIx(	[
<g/>
44	[number]	k4	44
<g/>
]	]	kIx)	]
VA	va	k0wR	va
-	-	kIx~	-
20ERS	[number]	k4	20ERS
-	-	kIx~	-
20	[number]	k4	20
let	léto	k1gNnPc2	léto
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bike	Bike	k1gFnSc1	Bike
rembok	rembok	k1gInSc1	rembok
adibas	adibas	k1gMnSc1	adibas
/	/	kIx~	/
<g/>
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Kutmasta	Kutmasta	k1gMnSc1	Kutmasta
Kurt	Kurt	k1gMnSc1	Kurt
+	+	kIx~	+
Eins	Eins	k1gInSc1	Eins
<g/>
,	,	kIx,	,
zwei	zwei	k1gNnSc1	zwei
<g/>
,	,	kIx,	,
polizei	polizei	k1gNnSc1	polizei
/	/	kIx~	/
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc4	Toxxx
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Kool	Kool	k1gInSc1	Kool
Keith	Keith	k1gInSc1	Keith
+	+	kIx~	+
Krysy	krysa	k1gFnSc2	krysa
/	/	kIx~	/
<g/>
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Orion	orion	k1gInSc1	orion
+	+	kIx~	+
Nádech	nádech	k1gInSc1	nádech
/	/	kIx~	/
<g/>
K.O.	K.O.	k1gMnSc1	K.O.
Kru	kra	k1gFnSc4	kra
[	[	kIx(	[
<g/>
45	[number]	k4	45
<g/>
]	]	kIx)	]
Supercrooo	Supercrooo	k6eAd1	Supercrooo
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
