<p>
<s>
Sitemap	Sitemap	k1gInSc1	Sitemap
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
napomoci	napomoct	k5eAaPmF	napomoct
internetovým	internetový	k2eAgMnPc3d1	internetový
vyhledávačům	vyhledávač	k1gMnPc3	vyhledávač
vyznat	vyznat	k5eAaBmF	vyznat
se	se	k3xPyFc4	se
na	na	k7c6	na
webové	webový	k2eAgFnSc6d1	webová
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
sitemap	sitemap	k1gInSc1	sitemap
XML	XML	kA	XML
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
nebývá	bývat	k5eNaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
však	však	k9	však
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ho	on	k3xPp3gMnSc4	on
můžete	moct	k5eAaImIp2nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
s	s	k7c7	s
příponami	přípona	k1gFnPc7	přípona
HTML	HTML	kA	HTML
či	či	k8xC	či
PHP	PHP	kA	PHP
<g/>
.	.	kIx.	.
</s>
<s>
Sitemap	Sitemap	k1gMnSc1	Sitemap
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
kořenového	kořenový	k2eAgInSc2d1	kořenový
adresáře	adresář	k1gInSc2	adresář
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Sitemap	Sitemap	k1gInSc1	Sitemap
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vyhledávacím	vyhledávací	k2eAgMnPc3d1	vyhledávací
robotům	robot	k1gMnPc3	robot
procházet	procházet	k5eAaImF	procházet
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
jinak	jinak	k6eAd1	jinak
těžko	těžko	k6eAd1	těžko
dosažitelné	dosažitelný	k2eAgNnSc1d1	dosažitelné
<g/>
.	.	kIx.	.
</s>
<s>
Sitemap	Sitemap	k1gInSc1	Sitemap
je	být	k5eAaImIp3nS	být
doplněk	doplněk	k1gInSc4	doplněk
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
robots	robots	k1gInSc4	robots
<g/>
.	.	kIx.	.
<g/>
txt	txt	k?	txt
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
robots	robots	k1gInSc1	robots
<g/>
.	.	kIx.	.
<g/>
txt	txt	k?	txt
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
vyloučení	vyloučení	k1gNnSc4	vyloučení
některých	některý	k3yIgFnPc2	některý
stránek	stránka	k1gFnPc2	stránka
z	z	k7c2	z
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
sitemap	sitemap	k1gInSc1	sitemap
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
přidávání	přidávání	k1gNnSc4	přidávání
stránek	stránka	k1gFnPc2	stránka
do	do	k7c2	do
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
stránky	stránka	k1gFnPc1	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c4	v
sitemap	sitemap	k1gInSc4	sitemap
nejsou	být	k5eNaImIp3nP	být
uvedeny	uveden	k2eAgMnPc4d1	uveden
vyhledávače	vyhledávač	k1gMnPc4	vyhledávač
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Sitemap	Sitemap	k1gInSc1	Sitemap
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
doplněk	doplněk	k1gInSc1	doplněk
ke	k	k7c3	k
klasickému	klasický	k2eAgNnSc3d1	klasické
procházení	procházení	k1gNnSc3	procházení
webu	web	k1gInSc2	web
vyhledávacím	vyhledávací	k2eAgInSc7d1	vyhledávací
robotem	robot	k1gInSc7	robot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
sitemap	sitemap	k1gInSc1	sitemap
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Elementy	element	k1gInPc4	element
===	===	k?	===
</s>
</p>
<p>
<s>
Elementy	element	k1gInPc4	element
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
http://www.sitemaps.org/protocol.php	[url]	k1gMnSc1	http://www.sitemaps.org/protocol.php
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
www.wall.cz	www.wall.cz	k1gInSc1	www.wall.cz
<g/>
,	,	kIx,	,
Ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
generovat	generovat	k5eAaImF	generovat
soubor	soubor	k1gInSc4	soubor
SITEMAP	SITEMAP	kA	SITEMAP
<g/>
.	.	kIx.	.
<g/>
XML	XML	kA	XML
</s>
</p>
