<p>
<s>
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Marocké	marocký	k2eAgNnSc1d1	marocké
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
,	,	kIx,	,
al-Maghrib	al-Maghrib	k1gInSc1	al-Maghrib
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Maroc	Maroc	k1gFnSc1	Maroc
<g/>
,	,	kIx,	,
berbersky	berbersky	k6eAd1	berbersky
ⵜ	ⵜ	k?	ⵜ
ⵏ	ⵏ	k?	ⵏ
ⵍ	ⵍ	k?	ⵍ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Maghrebu	Maghreb	k1gInSc2	Maghreb
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severozápadě	severozápad	k1gInSc6	severozápad
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Západní	západní	k2eAgFnSc1d1	západní
Sahara	Sahara	k1gFnSc1	Sahara
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dvě	dva	k4xCgFnPc1	dva
malé	malá	k1gFnPc1	malá
španělské	španělský	k2eAgFnPc1d1	španělská
exklávy	exkláva	k1gFnSc2	exkláva
Ceuta	Ceuto	k1gNnSc2	Ceuto
a	a	k8xC	a
Melilla	Melillo	k1gNnSc2	Melillo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prehistorie	prehistorie	k1gFnSc1	prehistorie
a	a	k8xC	a
římská	římský	k2eAgFnSc1d1	římská
nadvláda	nadvláda	k1gFnSc1	nadvláda
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
osídlení	osídlení	k1gNnSc2	osídlení
Maroka	Maroko	k1gNnSc2	Maroko
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
Vysokého	vysoký	k2eAgInSc2d1	vysoký
Atlasu	Atlas	k1gInSc2	Atlas
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
skalní	skalní	k2eAgFnPc1d1	skalní
kresby	kresba	k1gFnPc1	kresba
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
Maroko	Maroko	k1gNnSc1	Maroko
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
a	a	k8xC	a
zalesněnou	zalesněný	k2eAgFnSc7d1	zalesněná
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
tamní	tamní	k2eAgInSc1d1	tamní
lid	lid	k1gInSc1	lid
zabýval	zabývat	k5eAaImAgInS	zabývat
zejména	zejména	k9	zejména
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zakládali	zakládat	k5eAaImAgMnP	zakládat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Maroka	Maroko	k1gNnSc2	Maroko
své	svůj	k3xOyFgFnSc2	svůj
osady	osada	k1gFnSc2	osada
Féničané	Féničan	k1gMnPc1	Féničan
podél	podél	k7c2	podél
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
pádu	pád	k1gInSc6	pád
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
40	[number]	k4	40
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
spojili	spojit	k5eAaPmAgMnP	spojit
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
částí	část	k1gFnSc7	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
tak	tak	k8xS	tak
základ	základ	k1gInSc1	základ
provincii	provincie	k1gFnSc4	provincie
Mauretania	Mauretanium	k1gNnSc2	Mauretanium
Tingitana	Tingitana	k1gFnSc1	Tingitana
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berbeři	Berber	k1gMnPc1	Berber
žili	žít	k5eAaImAgMnP	žít
kočovným	kočovný	k2eAgInSc7d1	kočovný
životem	život	k1gInSc7	život
a	a	k8xC	a
nezakládali	zakládat	k5eNaImAgMnP	zakládat
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nebylo	být	k5eNaImAgNnS	být
co	co	k3yRnSc4	co
dobývat	dobývat	k5eAaImF	dobývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ubránili	ubránit	k5eAaPmAgMnP	ubránit
římské	římský	k2eAgFnSc3d1	římská
nadvládě	nadvláda	k1gFnSc3	nadvláda
a	a	k8xC	a
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
podnikali	podnikat	k5eAaImAgMnP	podnikat
výpady	výpad	k1gInPc4	výpad
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
osady	osada	k1gFnPc4	osada
a	a	k8xC	a
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
specifické	specifický	k2eAgNnSc1d1	specifické
rychlým	rychlý	k2eAgNnSc7d1	rychlé
šířením	šíření	k1gNnSc7	šíření
pouštních	pouštní	k2eAgFnPc2d1	pouštní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Římané	Říman	k1gMnPc1	Říman
bezohledně	bezohledně	k6eAd1	bezohledně
káceli	kácet	k5eAaImAgMnP	kácet
místní	místní	k2eAgInPc4d1	místní
lesy	les	k1gInPc4	les
a	a	k8xC	a
nestarali	starat	k5eNaImAgMnP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
obnovu	obnova	k1gFnSc4	obnova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
tak	tak	k6eAd1	tak
klesala	klesat	k5eAaImAgFnS	klesat
hladina	hladina	k1gFnSc1	hladina
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
pustla	pustnout	k5eAaImAgFnS	pustnout
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využitelná	využitelný	k2eAgFnSc1d1	využitelná
přibližně	přibližně	k6eAd1	přibližně
jen	jen	k9	jen
třetina	třetina	k1gFnSc1	třetina
marockého	marocký	k2eAgNnSc2d1	marocké
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Římská	římský	k2eAgFnSc1d1	římská
nadvláda	nadvláda	k1gFnSc1	nadvláda
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Římany	Říman	k1gMnPc4	Říman
vypudili	vypudit	k5eAaPmAgMnP	vypudit
Vandalové	Vandal	k1gMnPc1	Vandal
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
královskou	královský	k2eAgFnSc4d1	královská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
však	však	k9	však
tvořili	tvořit	k5eAaImAgMnP	tvořit
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
početnou	početný	k2eAgFnSc4d1	početná
horní	horní	k2eAgFnSc4d1	horní
vrstvu	vrstva	k1gFnSc4	vrstva
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
splynuli	splynout	k5eAaPmAgMnP	splynout
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
berberským	berberský	k2eAgNnSc7d1	berberské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
Maroka	Maroko	k1gNnSc2	Maroko
obsazeno	obsazen	k2eAgNnSc4d1	obsazeno
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
(	(	kIx(	(
<g/>
Východořímskou	východořímský	k2eAgFnSc7d1	Východořímská
<g/>
)	)	kIx)	)
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pronikání	pronikání	k1gNnSc2	pronikání
islámu	islám	k1gInSc2	islám
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
640	[number]	k4	640
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
západní	západní	k2eAgFnSc4d1	západní
Afriku	Afrika	k1gFnSc4	Afrika
podrobovat	podrobovat	k5eAaImF	podrobovat
islámští	islámský	k2eAgMnPc1d1	islámský
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
neúspěšnému	úspěšný	k2eNgInSc3d1	neúspěšný
pokusu	pokus	k1gInSc3	pokus
o	o	k7c6	o
dobytí	dobytí	k1gNnSc6	dobytí
Maroka	Maroko	k1gNnSc2	Maroko
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	let	k1gInPc6	let
681	[number]	k4	681
<g/>
–	–	k?	–
<g/>
683	[number]	k4	683
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podrobení	podrobení	k1gNnSc3	podrobení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
705	[number]	k4	705
<g/>
–	–	k?	–
<g/>
708	[number]	k4	708
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
i	i	k9	i
Arabové	Arab	k1gMnPc1	Arab
potýkali	potýkat	k5eAaImAgMnP	potýkat
se	s	k7c7	s
stejnými	stejný	k2eAgInPc7d1	stejný
problémy	problém	k1gInPc7	problém
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
kočovným	kočovný	k2eAgInSc7d1	kočovný
životem	život	k1gInSc7	život
Berberů	Berber	k1gMnPc2	Berber
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berbeři	Berber	k1gMnPc1	Berber
nakonec	nakonec	k6eAd1	nakonec
přijali	přijmout	k5eAaPmAgMnP	přijmout
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
kmenů	kmen	k1gInPc2	kmen
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
politickou	politický	k2eAgFnSc4d1	politická
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
740	[number]	k4	740
dokonce	dokonce	k9	dokonce
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
berberské	berberský	k2eAgNnSc1d1	berberské
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
kalif	kalif	k1gMnSc1	kalif
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
789	[number]	k4	789
založil	založit	k5eAaPmAgMnS	založit
kalify	kalif	k1gMnPc7	kalif
pronásledovaný	pronásledovaný	k2eAgInSc4d1	pronásledovaný
Idrís	Idrís	k1gInSc4	Idrís
I.	I.	kA	I.
dynastií	dynastie	k1gFnSc7	dynastie
Idríssdů	Idríssd	k1gInPc2	Idríssd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
kalifové	kalif	k1gMnPc1	kalif
z	z	k7c2	z
Córdoby	Córdoba	k1gFnSc2	Córdoba
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vypudit	vypudit	k5eAaPmF	vypudit
Fátimovce	Fátimovka	k1gFnSc3	Fátimovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1061	[number]	k4	1061
<g/>
–	–	k?	–
<g/>
1269	[number]	k4	1269
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
Almoravidé	Almoravidý	k2eAgFnSc2d1	Almoravidý
a	a	k8xC	a
Almohadé	Almohadý	k2eAgFnSc2d1	Almohadý
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
k	k	k7c3	k
Maroku	Maroko	k1gNnSc3	Maroko
připojit	připojit	k5eAaPmF	připojit
území	území	k1gNnSc4	území
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1269	[number]	k4	1269
obsadili	obsadit	k5eAaPmAgMnP	obsadit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Marrákeš	Marrákeš	k1gInSc1	Marrákeš
Berbeři	Berber	k1gMnPc1	Berber
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Portugalské	portugalský	k2eAgInPc4d1	portugalský
a	a	k8xC	a
španělské	španělský	k2eAgInPc4d1	španělský
vlivy	vliv	k1gInPc4	vliv
====	====	k?	====
</s>
</p>
<p>
<s>
Léta	léto	k1gNnSc2	léto
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1554	[number]	k4	1554
se	se	k3xPyFc4	se
nesla	nést	k5eAaImAgFnS	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
vlády	vláda	k1gFnSc2	vláda
Wattasidů	Wattasid	k1gMnPc2	Wattasid
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
postupně	postupně	k6eAd1	postupně
usadili	usadit	k5eAaPmAgMnP	usadit
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
Portugalský	portugalský	k2eAgInSc1d1	portugalský
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
patrný	patrný	k2eAgInSc1d1	patrný
zejména	zejména	k9	zejména
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlily	sídlit	k5eAaImAgFnP	sídlit
jejich	jejich	k3xOp3gFnPc1	jejich
faktorie	faktorie	k1gFnPc1	faktorie
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
a	a	k8xC	a
opevňované	opevňovaný	k2eAgFnPc1d1	opevňovaný
během	během	k7c2	během
portugalské	portugalský	k2eAgFnSc2d1	portugalská
námořní	námořní	k2eAgFnSc2d1	námořní
expanze	expanze	k1gFnSc2	expanze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
krále	král	k1gMnSc4	král
Manuela	Manuel	k1gMnSc4	Manuel
I.	I.	kA	I.
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
vládci	vládce	k1gMnPc7	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c6	o
Safi	Saf	k1gFnSc6	Saf
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
o	o	k7c6	o
El	Ela	k1gFnPc2	Ela
Jadidu	Jadid	k1gInSc2	Jadid
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
portugalsky	portugalsky	k6eAd1	portugalsky
Mazagã	Mazagã	k1gFnSc4	Mazagã
<g/>
,	,	kIx,	,
Essauru	Essaura	k1gFnSc4	Essaura
(	(	kIx(	(
<g/>
Essaouira	Essaouira	k1gFnSc1	Essaouira
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Mogador	Mogador	k1gInSc1	Mogador
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tanger	Tanger	k1gInSc4	Tanger
a	a	k8xC	a
Arzilu	Arzila	k1gFnSc4	Arzila
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
byli	být	k5eAaImAgMnP	být
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1578	[number]	k4	1578
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
krále	král	k1gMnSc4	král
Sebastiána	Sebastián	k1gMnSc2	Sebastián
I.	I.	kA	I.
Portugalského	portugalský	k2eAgInSc2d1	portugalský
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Alcácer	Alcácra	k1gFnPc2	Alcácra
Quibiru	Quibir	k1gInSc2	Quibir
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
osad	osada	k1gFnPc2	osada
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
některé	některý	k3yIgFnPc1	některý
pevnosti	pevnost	k1gFnPc1	pevnost
i	i	k9	i
bývalé	bývalý	k2eAgFnSc2d1	bývalá
katedrály	katedrála	k1gFnSc2	katedrála
mají	mít	k5eAaImIp3nP	mít
dodnes	dodnes	k6eAd1	dodnes
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
portuguesa	portuguesa	k1gFnSc1	portuguesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Imperialismus	imperialismus	k1gInSc1	imperialismus
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
samostatnosti	samostatnost	k1gFnSc3	samostatnost
===	===	k?	===
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
výrazně	výrazně	k6eAd1	výrazně
zesílil	zesílit	k5eAaPmAgInS	zesílit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sultáni	sultán	k1gMnPc1	sultán
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
mocenské	mocenský	k2eAgNnSc4d1	mocenské
postavení	postavení	k1gNnSc4	postavení
snažili	snažit	k5eAaImAgMnP	snažit
posílit	posílit	k5eAaPmF	posílit
podepisováním	podepisování	k1gNnSc7	podepisování
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
však	však	k9	však
definitivně	definitivně	k6eAd1	definitivně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
až	až	k9	až
britsko-francouzská	britskorancouzský	k2eAgFnSc1d1	britsko-francouzská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
protestovala	protestovat	k5eAaBmAgFnS	protestovat
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
proti	proti	k7c3	proti
francouzské	francouzský	k2eAgFnSc3d1	francouzská
politice	politika	k1gFnSc3	politika
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
pro	pro	k7c4	pro
výstrahu	výstraha	k1gFnSc4	výstraha
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
Agadiru	Agadir	k1gInSc2	Agadir
dělovou	dělový	k2eAgFnSc4d1	dělová
loď	loď	k1gFnSc4	loď
Panter	panter	k1gMnSc1	panter
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Skok	skok	k1gInSc4	skok
Pantera	panter	k1gMnSc2	panter
do	do	k7c2	do
Agadiru	Agadir	k1gInSc2	Agadir
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyústila	vyústit	k5eAaPmAgFnS	vyústit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Maroko	Maroko	k1gNnSc1	Maroko
stalo	stát	k5eAaPmAgNnS	stát
francouzským	francouzský	k2eAgInSc7d1	francouzský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
,	,	kIx,	,
německé	německý	k2eAgInPc1d1	německý
hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
zájmy	zájem	k1gInPc1	zájem
byly	být	k5eAaImAgInP	být
zabezpečeny	zabezpečit	k5eAaPmNgInP	zabezpečit
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
část	část	k1gFnSc1	část
připadla	připadnout	k5eAaPmAgFnS	připadnout
Španělsku	Španělsko	k1gNnSc3	Španělsko
a	a	k8xC	a
Tanger	Tanger	k1gInSc1	Tanger
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
pásmem	pásmo	k1gNnSc7	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
částech	část	k1gFnPc6	část
držené	držený	k2eAgInPc1d1	držený
Španěly	Španěly	k1gInPc1	Španěly
povstání	povstání	k1gNnSc2	povstání
berberského	berberský	k2eAgInSc2d1	berberský
kmenu	kmen	k1gInSc2	kmen
Rifů	rif	k1gInPc2	rif
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Abd	Abd	k1gFnPc7	Abd
al-Karíma	al-Karí	k2eAgFnPc7d1	al-Karí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
Rífské	Rífský	k2eAgFnSc2d1	Rífský
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
povstání	povstání	k1gNnSc1	povstání
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
a	a	k8xC	a
potlačení	potlačení	k1gNnSc1	potlačení
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
Abd	Abd	k1gMnSc1	Abd
al-Karím	al-Karit	k5eAaBmIp1nS	al-Karit
musel	muset	k5eAaImAgMnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snaha	snaha	k1gFnSc1	snaha
rozdělit	rozdělit	k5eAaPmF	rozdělit
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
uplatňováním	uplatňování	k1gNnSc7	uplatňování
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
politiky	politika	k1gFnSc2	politika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Francie	Francie	k1gFnSc2	Francie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
centry	centr	k1gInPc4	centr
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgNnPc4d1	rostoucí
města	město	k1gNnPc4	město
díky	díky	k7c3	díky
přílivu	příliv	k1gInSc3	příliv
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
protikoloniálního	protikoloniální	k2eAgNnSc2d1	protikoloniální
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
sultán	sultán	k1gMnSc1	sultán
Muhammad	Muhammad	k1gInSc4	Muhammad
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
však	však	k9	však
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
donucen	donutit	k5eAaPmNgMnS	donutit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
uplatňování	uplatňování	k1gNnSc2	uplatňování
své	svůj	k3xOyFgFnSc2	svůj
národnostní	národnostní	k2eAgFnSc2d1	národnostní
politiky	politika	k1gFnSc2	politika
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrat	návrat	k1gInSc4	návrat
si	se	k3xPyFc3	se
vymohlo	vymoct	k5eAaPmAgNnS	vymoct
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
evropské	evropský	k2eAgFnPc1d1	Evropská
koloniální	koloniální	k2eAgFnPc1d1	koloniální
mocnosti	mocnost	k1gFnPc1	mocnost
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
udržet	udržet	k5eAaPmF	udržet
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
francouzský	francouzský	k2eAgInSc1d1	francouzský
protektorát	protektorát	k1gInSc1	protektorát
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
Muhammad	Muhammad	k1gInSc1	Muhammad
V.	V.	kA	V.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
ujal	ujmout	k5eAaPmAgMnS	ujmout
král	král	k1gMnSc1	král
Hasan	Hasan	k1gMnSc1	Hasan
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
monarchistická	monarchistický	k2eAgFnSc1d1	monarchistická
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
Marokem	Maroko	k1gNnSc7	Maroko
<g/>
,	,	kIx,	,
Mauretánií	Mauretánie	k1gFnSc7	Mauretánie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
bývalé	bývalý	k2eAgFnSc2d1	bývalá
španělské	španělský	k2eAgFnSc2d1	španělská
kolonie	kolonie	k1gFnSc2	kolonie
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
však	však	k9	však
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
příslušníci	příslušník	k1gMnPc1	příslušník
hnutí	hnutí	k1gNnSc4	hnutí
Polisario	Polisario	k6eAd1	Polisario
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
navzdory	navzdory	k7c3	navzdory
dohodám	dohoda	k1gFnPc3	dohoda
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Saharskou	saharský	k2eAgFnSc4d1	saharská
arabskou	arabský	k2eAgFnSc4d1	arabská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
Mauretánie	Mauretánie	k1gFnSc1	Mauretánie
vzdala	vzdát	k5eAaPmAgFnS	vzdát
nároku	nárok	k1gInSc3	nárok
na	na	k7c4	na
západosaharské	západosaharský	k2eAgNnSc4d1	západosaharský
území	území	k1gNnSc4	území
a	a	k8xC	a
do	do	k7c2	do
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
vtrhlo	vtrhnout	k5eAaPmAgNnS	vtrhnout
Maroko	Maroko	k1gNnSc1	Maroko
se	s	k7c7	s
stotisícovou	stotisícový	k2eAgFnSc7d1	stotisícová
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snahu	snaha	k1gFnSc4	snaha
OSN	OSN	kA	OSN
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
marocká	marocký	k2eAgFnSc1d1	marocká
vláda	vláda	k1gFnSc1	vláda
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
představitelé	představitel	k1gMnPc1	představitel
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
Polisario	Polisario	k6eAd1	Polisario
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
dodržováno	dodržován	k2eAgNnSc1d1	dodržováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Hassana	Hassan	k1gMnSc2	Hassan
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Muhammed	Muhammed	k1gMnSc1	Muhammed
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
projevu	projev	k1gInSc6	projev
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c6	na
potírání	potírání	k1gNnSc6	potírání
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
korupci	korupce	k1gFnSc4	korupce
a	a	k8xC	a
podpoří	podpořit	k5eAaPmIp3nS	podpořit
vytváření	vytváření	k1gNnSc1	vytváření
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
zlepšení	zlepšení	k1gNnSc3	zlepšení
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
nepokoji	nepokoj	k1gInPc7	nepokoj
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Muhammed	Muhammed	k1gMnSc1	Muhammed
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
změny	změna	k1gFnSc2	změna
ústavy	ústava	k1gFnSc2	ústava
zahrnující	zahrnující	k2eAgNnPc1d1	zahrnující
omezení	omezení	k1gNnPc1	omezení
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
schválila	schválit	k5eAaPmAgFnS	schválit
98,5	[number]	k4	98,5
<g/>
%	%	kIx~	%
většina	většina	k1gFnSc1	většina
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
však	však	k9	však
považovala	považovat	k5eAaImAgFnS	považovat
změny	změna	k1gFnPc4	změna
za	za	k7c4	za
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
dynastií	dynastie	k1gFnPc2	dynastie
===	===	k?	===
</s>
</p>
<p>
<s>
Almorávidé	Almorávidý	k2eAgNnSc1d1	Almorávidé
</s>
</p>
<p>
<s>
Almohadové	Almohadový	k2eAgNnSc1d1	Almohadový
</s>
</p>
<p>
<s>
Merinidé	Merinidý	k2eAgNnSc1d1	Merinidý
</s>
</p>
<p>
<s>
Watasidé	Watasidý	k2eAgNnSc1d1	Watasidý
</s>
</p>
<p>
<s>
Saadové	Saadové	k2eAgFnSc1d1	Saadové
</s>
</p>
<p>
<s>
Alawité	Alawita	k1gMnPc1	Alawita
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavy	hlava	k1gFnSc2	hlava
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
14.8	[number]	k4	14.8
<g/>
.1957	.1957	k4	.1957
<g/>
–	–	k?	–
<g/>
26.2	[number]	k4	26.2
<g/>
.1961	.1961	k4	.1961
–	–	k?	–
Muhammad	Muhammad	k1gInSc1	Muhammad
V.	V.	kA	V.
–	–	k?	–
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
26.2	[number]	k4	26.2
<g/>
.1961	.1961	k4	.1961
<g/>
–	–	k?	–
<g/>
23.7	[number]	k4	23.7
<g/>
.1999	.1999	k4	.1999
–	–	k?	–
Hassan	Hassana	k1gFnPc2	Hassana
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
23.7	[number]	k4	23.7
<g/>
.1999	.1999	k4	.1999
–	–	k?	–
dosud	dosud	k6eAd1	dosud
–	–	k?	–
Muhammad	Muhammad	k1gInSc1	Muhammad
VI	VI	kA	VI
<g/>
.	.	kIx.	.
–	–	k?	–
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Marocká	marocký	k2eAgFnSc1d1	marocká
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
třemi	tři	k4xCgInPc7	tři
aspekty	aspekt	k1gInPc7	aspekt
–	–	k?	–
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
Saharou	Sahara	k1gFnSc7	Sahara
a	a	k8xC	a
atlantským	atlantský	k2eAgNnSc7d1	Atlantské
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
prochází	procházet	k5eAaImIp3nS	procházet
zemí	zem	k1gFnSc7	zem
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
(	(	kIx(	(
<g/>
Antiatlas	Antiatlas	k1gInSc1	Antiatlas
s	s	k7c7	s
vrcholy	vrchol	k1gInPc7	vrchol
přes	přes	k7c4	přes
2	[number]	k4	2
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
–	–	k?	–
Vysoký	vysoký	k2eAgInSc1d1	vysoký
Atlas	Atlas	k1gInSc1	Atlas
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšek	výška	k1gFnPc2	výška
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
–	–	k?	–
až	až	k9	až
k	k	k7c3	k
severu	sever	k1gInSc3	sever
(	(	kIx(	(
<g/>
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Rif	rif	k1gInSc1	rif
s	s	k7c7	s
vrcholy	vrchol	k1gInPc7	vrchol
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
severozápadu	severozápad	k1gInSc2	severozápad
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Střední	střední	k2eAgInSc1d1	střední
Atlas	Atlas	k1gInSc1	Atlas
s	s	k7c7	s
vrcholy	vrchol	k1gInPc7	vrchol
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strmě	strmě	k6eAd1	strmě
padá	padat	k5eAaImIp3nS	padat
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c6	po
pobřeží	pobřeží	k1gNnSc6	pobřeží
do	do	k7c2	do
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vysokém	vysoký	k2eAgInSc6d1	vysoký
Atlasu	Atlas	k1gInSc6	Atlas
rozprostírajícím	rozprostírající	k2eAgInSc6d1	rozprostírající
se	se	k3xPyFc4	se
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Maroku	Maroko	k1gNnSc6	Maroko
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
–	–	k?	–
Džabal	Džabal	k1gInSc4	Džabal
Tubkal	Tubkal	k1gMnSc1	Tubkal
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
4	[number]	k4	4
167	[number]	k4	167
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Atlantické	atlantický	k2eAgNnSc1d1	atlantické
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
ploché	plochý	k2eAgNnSc1d1	ploché
a	a	k8xC	a
stoupá	stoupat	k5eAaImIp3nS	stoupat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
k	k	k7c3	k
tabulovité	tabulovitý	k2eAgFnSc3d1	tabulovitý
krajině	krajina	k1gFnSc3	krajina
marocké	marocký	k2eAgFnSc2d1	marocká
mesety	meseta	k1gFnSc2	meseta
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
800	[number]	k4	800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
přechází	přecházet	k5eAaImIp3nP	přecházet
pohoří	pohoří	k1gNnSc4	pohoří
Atlas	Atlas	k1gInSc1	Atlas
v	v	k7c4	v
alžírskou	alžírský	k2eAgFnSc4d1	alžírská
náhorní	náhorní	k2eAgFnSc4d1	náhorní
plošinu	plošina	k1gFnSc4	plošina
šotů	šot	k1gInPc2	šot
a	a	k8xC	a
v	v	k7c4	v
Saharu	Sahara	k1gFnSc4	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
marockých	marocký	k2eAgFnPc2d1	marocká
řek	řeka	k1gFnPc2	řeka
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Sebú	Sebú	k1gFnPc1	Sebú
<g/>
,	,	kIx,	,
Um	um	k1gInSc1	um
er-Rbia	er-Rbia	k1gFnSc1	er-Rbia
<g/>
,	,	kIx,	,
Tensift	Tensift	k1gInSc1	Tensift
<g/>
,	,	kIx,	,
Souss	Souss	k1gInSc1	Souss
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Mulúja	Mulúj	k1gInSc2	Mulúj
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Režim	režim	k1gInSc1	režim
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vodností	vodnost	k1gFnSc7	vodnost
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
a	a	k8xC	a
nízkými	nízký	k2eAgInPc7d1	nízký
průtoky	průtok	k1gInPc7	průtok
či	či	k8xC	či
vyschnutím	vyschnutí	k1gNnSc7	vyschnutí
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
občasných	občasný	k2eAgInPc2d1	občasný
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vádí	vádí	k1gNnPc1	vádí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
podnebí	podnebí	k1gNnSc6	podnebí
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
hraje	hrát	k5eAaImIp3nS	hrát
Střední	střední	k2eAgInSc1d1	střední
a	a	k8xC	a
Vysoký	vysoký	k2eAgInSc1d1	vysoký
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
výrazným	výrazný	k2eAgNnSc7d1	výrazné
klimatickým	klimatický	k2eAgNnSc7d1	klimatické
rozhraním	rozhraní	k1gNnSc7	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Severozápad	severozápad	k1gInSc1	severozápad
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Středomořím	středomoří	k1gNnSc7	středomoří
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
mírným	mírný	k2eAgNnSc7d1	mírné
deštivým	deštivý	k2eAgNnSc7d1	deštivé
podnebím	podnebí	k1gNnSc7	podnebí
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
16	[number]	k4	16
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
horkými	horký	k2eAgNnPc7d1	horké
suchými	suchý	k2eAgNnPc7d1	suché
léty	léto	k1gNnPc7	léto
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
28	[number]	k4	28
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Atlasu	Atlas	k1gInSc2	Atlas
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
typickým	typický	k2eAgNnSc7d1	typické
vysokohorským	vysokohorský	k2eAgNnSc7d1	vysokohorské
podnebím	podnebí	k1gNnSc7	podnebí
se	se	k3xPyFc4	se
značnými	značný	k2eAgFnPc7d1	značná
teplotním	teplotní	k2eAgInPc3d1	teplotní
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
je	být	k5eAaImIp3nS	být
suché	suchý	k2eAgNnSc1d1	suché
pouštní	pouštní	k2eAgNnSc1d1	pouštní
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
–	–	k?	–
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maxima	maximum	k1gNnSc2	maximum
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
přes	přes	k7c4	přes
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
studené	studený	k2eAgFnSc2d1	studená
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
900	[number]	k4	900
mm	mm	kA	mm
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
200	[number]	k4	200
mm	mm	kA	mm
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadních	severozápadní	k2eAgInPc6d1	severozápadní
svazích	svah	k1gInPc6	svah
hor	hora	k1gFnPc2	hora
bývá	bývat	k5eAaImIp3nS	bývat
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
sněhových	sněhový	k2eAgFnPc2d1	sněhová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
Maroko	Maroko	k1gNnSc1	Maroko
konstituční	konstituční	k2eAgNnSc1d1	konstituční
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc7d1	sociální
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozhodující	rozhodující	k2eAgFnPc4d1	rozhodující
otázky	otázka	k1gFnPc4	otázka
sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
duchovní	duchovní	k2eAgFnSc7d1	duchovní
hlavou	hlava	k1gFnSc7	hlava
muslimského	muslimský	k2eAgNnSc2d1	muslimské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
všechny	všechen	k3xTgInPc1	všechen
zákony	zákon	k1gInPc1	zákon
usnesené	usnesený	k2eAgInPc1d1	usnesený
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
ministry	ministr	k1gMnPc4	ministr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
333	[number]	k4	333
křesel	křeslo	k1gNnPc2	křeslo
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
přímými	přímý	k2eAgFnPc7d1	přímá
volbami	volba	k1gFnPc7	volba
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
delegováni	delegovat	k5eAaBmNgMnP	delegovat
z	z	k7c2	z
obecních	obecní	k2eAgInPc2d1	obecní
parlamentů	parlament	k1gInPc2	parlament
a	a	k8xC	a
stavovských	stavovský	k2eAgFnPc2d1	stavovská
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentu	parlament	k1gInSc2	parlament
předsedá	předsedat	k5eAaImIp3nS	předsedat
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
platí	platit	k5eAaImIp3nS	platit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
povinnost	povinnost	k1gFnSc1	povinnost
na	na	k7c4	na
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
právo	právo	k1gNnSc1	právo
se	se	k3xPyFc4	se
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
z	z	k7c2	z
francouzských	francouzský	k2eAgMnPc2d1	francouzský
a	a	k8xC	a
islámských	islámský	k2eAgMnPc2d1	islámský
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
jeden	jeden	k4xCgInSc4	jeden
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
spadá	spadat	k5eAaImIp3nS	spadat
devět	devět	k4xCc4	devět
pracovních	pracovní	k2eAgInPc2d1	pracovní
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
provinční	provinční	k2eAgInPc4d1	provinční
a	a	k8xC	a
obecní	obecní	k2eAgInPc4d1	obecní
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
sestává	sestávat	k5eAaImIp3nS	sestávat
Maroko	Maroko	k1gNnSc4	Maroko
z	z	k7c2	z
12	[number]	k4	12
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
území	území	k1gNnSc2	území
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
ج	ج	k?	ج
džaha	džaha	k1gFnSc1	džaha
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
ج	ج	k?	ج
džahát	džahát	k1gMnSc1	džahát
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
région	région	k1gInSc1	région
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
régions	régionsa	k1gFnPc2	régionsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
62	[number]	k4	62
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
13	[number]	k4	13
městských	městský	k2eAgFnPc2d1	městská
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
fungujícímu	fungující	k2eAgInSc3d1	fungující
systému	systém	k1gInSc3	systém
nepřímých	přímý	k2eNgFnPc2d1	nepřímá
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
komunálních	komunální	k2eAgFnPc2d1	komunální
rad	rada	k1gFnPc2	rada
je	být	k5eAaImIp3nS	být
reálná	reálný	k2eAgFnSc1d1	reálná
moc	moc	k1gFnSc1	moc
orgánů	orgán	k1gInPc2	orgán
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
waliů	wali	k1gMnPc2	wali
(	(	kIx(	(
<g/>
představitelé	představitel	k1gMnPc1	představitel
regionů	region	k1gInPc2	region
<g/>
)	)	kIx)	)
a	a	k8xC	a
guvernérů	guvernér	k1gMnPc2	guvernér
(	(	kIx(	(
<g/>
představitelé	představitel	k1gMnPc1	představitel
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
)	)	kIx)	)
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
HDP	HDP	kA	HDP
mezi	mezi	k7c7	mezi
13	[number]	k4	13
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sklizni	sklizeň	k1gFnSc6	sklizeň
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
přes	přes	k7c4	přes
rozvoj	rozvoj	k1gInSc4	rozvoj
ostatních	ostatní	k2eAgMnPc2d1	ostatní
odvětví	odvětvit	k5eAaPmIp3nP	odvětvit
významným	významný	k2eAgInSc7d1	významný
článkem	článek	k1gInSc7	článek
ekonomiky	ekonomika	k1gFnSc2	ekonomika
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
%	%	kIx~	%
činného	činný	k2eAgNnSc2d1	činné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
zemědělců	zemědělec	k1gMnPc2	zemědělec
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
50	[number]	k4	50
let	léto	k1gNnPc2	léto
a	a	k8xC	a
cca	cca	kA	cca
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
negramotných	gramotný	k2eNgFnPc2d1	negramotná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
67	[number]	k4	67
%	%	kIx~	%
zemědělců	zemědělec	k1gMnPc2	zemědělec
jsou	být	k5eAaImIp3nP	být
malorolníci	malorolník	k1gMnPc1	malorolník
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
půdy	půda	k1gFnSc2	půda
do	do	k7c2	do
5	[number]	k4	5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
zavlažováno	zavlažovat	k5eAaImNgNnS	zavlažovat
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
pěstovanými	pěstovaný	k2eAgFnPc7d1	pěstovaná
plodinami	plodina	k1gFnPc7	plodina
jsou	být	k5eAaImIp3nP	být
subtropické	subtropický	k2eAgNnSc4d1	subtropické
ovoce	ovoce	k1gNnSc4	ovoce
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
55	[number]	k4	55
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
ovocných	ovocný	k2eAgInPc2d1	ovocný
sadů	sad	k1gInPc2	sad
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
olivovníky	olivovník	k1gInPc4	olivovník
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
datle	datle	k1gFnPc1	datle
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
opuncie	opuncie	k1gFnPc1	opuncie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
obiloviny	obilovina	k1gFnPc1	obilovina
(	(	kIx(	(
<g/>
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
též	též	k9	též
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
a	a	k8xC	a
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
olejniny	olejnina	k1gFnSc2	olejnina
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
oliv	oliva	k1gFnPc2	oliva
ještě	ještě	k9	ještě
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
podzemnice	podzemnice	k1gFnSc1	podzemnice
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgNnSc2d1	jižní
Maroka	Maroko	k1gNnSc2	Maroko
se	se	k3xPyFc4	se
též	též	k9	též
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
argania	arganium	k1gNnPc4	arganium
spinoza	spinoz	k1gMnSc2	spinoz
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
velmi	velmi	k6eAd1	velmi
ceněný	ceněný	k2eAgInSc1d1	ceněný
arganový	arganový	k2eAgInSc1d1	arganový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
využívaný	využívaný	k2eAgInSc1d1	využívaný
v	v	k7c6	v
marocké	marocký	k2eAgFnSc6d1	marocká
kuchyni	kuchyně	k1gFnSc6	kuchyně
a	a	k8xC	a
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
převažuje	převažovat	k5eAaImIp3nS	převažovat
chov	chov	k1gInSc4	chov
suchu	sucho	k1gNnSc3	sucho
odolných	odolný	k2eAgNnPc2d1	odolné
zvířat	zvíře	k1gNnPc2	zvíře
–	–	k?	–
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
produkce	produkce	k1gFnSc1	produkce
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
domácích	domácí	k2eAgNnPc6d1	domácí
stádech	stádo	k1gNnPc6	stádo
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivňován	ovlivňován	k2eAgInSc1d1	ovlivňován
dostupností	dostupnost	k1gFnSc7	dostupnost
píce	píce	k1gFnSc1	píce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
vydatnosti	vydatnost	k1gFnPc4	vydatnost
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
tvoří	tvořit	k5eAaImIp3nS	tvořit
produkce	produkce	k1gFnSc1	produkce
mléka	mléko	k1gNnSc2	mléko
(	(	kIx(	(
<g/>
též	též	k9	též
důležitá	důležitý	k2eAgFnSc1d1	důležitá
součást	součást	k1gFnSc1	součást
marocké	marocký	k2eAgFnSc2d1	marocká
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lesnictví	lesnictví	k1gNnSc2	lesnictví
====	====	k?	====
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
%	%	kIx~	%
území	území	k1gNnSc6	území
Maroka	Maroko	k1gNnSc2	Maroko
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
využitelných	využitelný	k2eAgInPc2d1	využitelný
porostů	porost	k1gInPc2	porost
je	být	k5eAaImIp3nS	být
však	však	k9	však
maximálně	maximálně	k6eAd1	maximálně
5	[number]	k4	5
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
tamních	tamní	k2eAgInPc2d1	tamní
lesů	les	k1gInPc2	les
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
nízkých	nízký	k2eAgInPc2d1	nízký
a	a	k8xC	a
řídkých	řídký	k2eAgInPc2d1	řídký
porostů	porost	k1gInPc2	porost
bez	bez	k7c2	bez
valné	valný	k2eAgFnSc2d1	valná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
marockých	marocký	k2eAgInPc2d1	marocký
lesů	les	k1gInPc2	les
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vysycháním	vysychání	k1gNnSc7	vysychání
celé	celý	k2eAgFnSc2d1	celá
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
marocké	marocký	k2eAgFnSc2d1	marocká
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
pastvy	pastva	k1gFnSc2	pastva
domácích	domácí	k1gMnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
těžby	těžba	k1gFnSc2	těžba
dřeva	dřevo	k1gNnSc2	dřevo
na	na	k7c4	na
otop	otop	k1gInSc4	otop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
lesní	lesní	k2eAgInPc4d1	lesní
celky	celek	k1gInPc4	celek
Maroka	Maroko	k1gNnSc2	Maroko
zároveň	zároveň	k6eAd1	zároveň
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
problémy	problém	k1gInPc1	problém
jsou	být	k5eAaImIp3nP	být
vážné	vážný	k2eAgInPc1d1	vážný
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnými	významný	k2eAgInPc7d1	významný
lesními	lesní	k2eAgInPc7d1	lesní
celky	celek	k1gInPc7	celek
Maroka	Maroko	k1gNnSc2	Maroko
jsou	být	k5eAaImIp3nP	být
les	les	k1gInSc4	les
Mamora	Mamor	k1gMnSc2	Mamor
u	u	k7c2	u
Rabatu	rabat	k1gInSc2	rabat
se	s	k7c7	s
světově	světově	k6eAd1	světově
významnou	významný	k2eAgFnSc7d1	významná
produkční	produkční	k2eAgFnSc7d1	produkční
plochou	plocha	k1gFnSc7	plocha
korkového	korkový	k2eAgInSc2d1	korkový
dubu	dub	k1gInSc2	dub
a	a	k8xC	a
unikátní	unikátní	k2eAgInPc4d1	unikátní
cedrové	cedrový	k2eAgInPc4d1	cedrový
lesy	les	k1gInPc4	les
Středního	střední	k2eAgInSc2d1	střední
Atlasu	Atlas	k1gInSc2	Atlas
a	a	k8xC	a
Rífu	Rífus	k1gInSc2	Rífus
(	(	kIx(	(
<g/>
cedrové	cedrový	k2eAgNnSc1d1	cedrové
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
tříslovina	tříslovina	k1gFnSc1	tříslovina
a	a	k8xC	a
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgNnSc4d1	tradiční
řezbářství	řezbářství	k1gNnSc4	řezbářství
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
berberská	berberský	k2eAgFnSc1d1	berberská
túje	túje	k1gFnSc1	túje
<g/>
"	"	kIx"	"
–	–	k?	–
sandarakovník	sandarakovník	k1gInSc1	sandarakovník
článkovaný	článkovaný	k2eAgInSc1d1	článkovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgInPc6d1	suchý
lesích	les	k1gInPc6	les
kolem	kolem	k7c2	kolem
Agadiru	Agadir	k1gInSc2	Agadir
roste	růst	k5eAaImIp3nS	růst
původní	původní	k2eAgFnSc1d1	původní
argánie	argánie	k1gFnSc1	argánie
trnitá	trnitý	k2eAgFnSc1d1	trnitá
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
plodů	plod	k1gInPc2	plod
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
argánový	argánový	k2eAgInSc1d1	argánový
olej	olej	k1gInSc1	olej
(	(	kIx(	(
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
produkční	produkční	k2eAgFnSc1d1	produkční
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rybolov	rybolov	k1gInSc4	rybolov
====	====	k?	====
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
dlouhému	dlouhý	k2eAgNnSc3d1	dlouhé
pobřeží	pobřeží	k1gNnSc3	pobřeží
(	(	kIx(	(
<g/>
3	[number]	k4	3
500	[number]	k4	500
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
vod	voda	k1gFnPc2	voda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
exportním	exportní	k2eAgInSc7d1	exportní
artiklem	artikl	k1gInSc7	artikl
i	i	k9	i
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
Maroko	Maroko	k1gNnSc1	Maroko
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
900	[number]	k4	900
000	[number]	k4	000
t	t	k?	t
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
ročně	ročně	k6eAd1	ročně
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
až	až	k9	až
300	[number]	k4	300
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rybolovu	rybolov	k1gInSc6	rybolov
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
marocká	marocký	k2eAgFnSc1d1	marocká
flotila	flotila	k1gFnSc1	flotila
má	mít	k5eAaImIp3nS	mít
bezmála	bezmála	k6eAd1	bezmála
20	[number]	k4	20
000	[number]	k4	000
rybářských	rybářský	k2eAgFnPc2d1	rybářská
lodí	loď	k1gFnPc2	loď
včetně	včetně	k7c2	včetně
350	[number]	k4	350
chladicích	chladicí	k2eAgFnPc2d1	chladicí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středomořské	středomořský	k2eAgNnSc1d1	středomořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
se	se	k3xPyFc4	se
na	na	k7c6	na
marockém	marocký	k2eAgInSc6d1	marocký
výlovu	výlov	k1gInSc6	výlov
podílí	podílet	k5eAaImIp3nS	podílet
pouze	pouze	k6eAd1	pouze
4,2	[number]	k4	4,2
%	%	kIx~	%
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
jižní	jižní	k2eAgFnSc6d1	jižní
oblasti	oblast	k1gFnSc6	oblast
atlantického	atlantický	k2eAgNnSc2d1	atlantické
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Al-	Al-	k1gMnSc1	Al-
<g/>
́	́	k?	́
<g/>
Ajún	Ajún	k1gMnSc1	Ajún
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Sahaře	Sahara	k1gFnSc6	Sahara
bývá	bývat	k5eAaImIp3nS	bývat
vyloženo	vyložit	k5eAaPmNgNnS	vyložit
přes	přes	k7c4	přes
40	[number]	k4	40
%	%	kIx~	%
vylovených	vylovený	k2eAgFnPc2d1	vylovená
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
hlavními	hlavní	k2eAgInPc7d1	hlavní
rybářskými	rybářský	k2eAgInPc7d1	rybářský
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Agadir	Agadir	k1gMnSc1	Agadir
<g/>
,	,	kIx,	,
Tantan	Tantan	k1gInSc1	Tantan
<g/>
,	,	kIx,	,
Essaouira	Essaouira	k1gFnSc1	Essaouira
<g/>
,	,	kIx,	,
Safi	Safi	k1gNnSc1	Safi
<g/>
,	,	kIx,	,
El-Jadida	El-Jadida	k1gFnSc1	El-Jadida
<g/>
,	,	kIx,	,
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
Mohamedia	Mohamedium	k1gNnPc1	Mohamedium
<g/>
,	,	kIx,	,
Rabat	rabat	k1gInSc1	rabat
<g/>
,	,	kIx,	,
Kenitra	Kenitra	k1gFnSc1	Kenitra
<g/>
,	,	kIx,	,
Larache	Larache	k1gFnSc1	Larache
<g/>
,	,	kIx,	,
Tanger	Tanger	k1gInSc1	Tanger
<g/>
,	,	kIx,	,
Al-Husejma	Al-Husejma	k1gFnSc1	Al-Husejma
a	a	k8xC	a
Nador	Nador	k1gInSc1	Nador
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marocký	marocký	k2eAgInSc1d1	marocký
rybolov	rybolov	k1gInSc1	rybolov
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
lov	lov	k1gInSc4	lov
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
jejich	jejich	k3xOp3gInPc2	jejich
zdrojů	zdroj	k1gInPc2	zdroj
však	však	k9	však
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
katastroficky	katastroficky	k6eAd1	katastroficky
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
i	i	k9	i
výlov	výlov	k1gInSc1	výlov
těchto	tento	k3xDgMnPc2	tento
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
lov	lov	k1gInSc1	lov
ryb	ryba	k1gFnPc2	ryba
typu	typ	k1gInSc2	typ
sardinek	sardinka	k1gFnPc2	sardinka
<g/>
,	,	kIx,	,
makrel	makrela	k1gFnPc2	makrela
a	a	k8xC	a
ančoviček	ančovička	k1gFnPc2	ančovička
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
bílé	bílý	k2eAgFnPc4d1	bílá
ryby	ryba	k1gFnPc4	ryba
(	(	kIx(	(
<g/>
mečoun	mečoun	k1gMnSc1	mečoun
<g/>
,	,	kIx,	,
treska	treska	k1gFnSc1	treska
<g/>
,	,	kIx,	,
tuňák	tuňák	k1gMnSc1	tuňák
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
měkkýši	měkkýš	k1gMnPc1	měkkýš
a	a	k8xC	a
korýši	korýš	k1gMnPc1	korýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Maroko	Maroko	k1gNnSc4	Maroko
významnou	významný	k2eAgFnSc7d1	významná
exportní	exportní	k2eAgFnSc7d1	exportní
položkou	položka	k1gFnSc7	položka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
tvořil	tvořit	k5eAaImAgInS	tvořit
s	s	k7c7	s
EU	EU	kA	EU
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
13	[number]	k4	13
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obratu	obrat	k1gInSc2	obrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Maroko	Maroko	k1gNnSc4	Maroko
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
relativně	relativně	k6eAd1	relativně
výhodné	výhodný	k2eAgFnPc1d1	výhodná
kvóty	kvóta	k1gFnPc1	kvóta
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rajčat	rajče	k1gNnPc2	rajče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
podílí	podílet	k5eAaImIp3nS	podílet
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
zastoupen	zastoupen	k2eAgInSc4d1	zastoupen
oděvní	oděvní	k2eAgInSc4d1	oděvní
a	a	k8xC	a
pletařský	pletařský	k2eAgInSc4d1	pletařský
<g/>
,	,	kIx,	,
zemědělsko-potravinářský	zemědělskootravinářský	k2eAgInSc4d1	zemědělsko-potravinářský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
hutnický	hutnický	k2eAgInSc4d1	hutnický
a	a	k8xC	a
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
poklesu	pokles	k1gInSc2	pokles
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
marockém	marocký	k2eAgNnSc6d1	marocké
textilním	textilní	k2eAgNnSc6d1	textilní
zboží	zboží	k1gNnSc6	zboží
však	však	k9	však
podíl	podíl	k1gInSc4	podíl
textilního	textilní	k2eAgInSc2d1	textilní
a	a	k8xC	a
kožedělného	kožedělný	k2eAgInSc2d1	kožedělný
průmyslu	průmysl	k1gInSc2	průmysl
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
metalurgie	metalurgie	k1gFnSc2	metalurgie
a	a	k8xC	a
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
dynamicky	dynamicky	k6eAd1	dynamicky
se	se	k3xPyFc4	se
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
odvětví	odvětvit	k5eAaPmIp3nS	odvětvit
dále	daleko	k6eAd2	daleko
vláda	vláda	k1gFnSc1	vláda
preferuje	preferovat	k5eAaImIp3nS	preferovat
průmysl	průmysl	k1gInSc4	průmysl
zpracovávající	zpracovávající	k2eAgFnSc2d1	zpracovávající
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
průmysl	průmysl	k1gInSc4	průmysl
(	(	kIx(	(
<g/>
montáž	montáž	k1gFnSc4	montáž
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
lokality	lokalita	k1gFnPc1	lokalita
mezi	mezi	k7c7	mezi
Casablanckou	Casablancka	k1gFnSc7	Casablancka
a	a	k8xC	a
Mohamedií	Mohamedie	k1gFnSc7	Mohamedie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Casablancou	Casablanca	k1gFnSc7	Casablanca
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Jadidou	Jadida	k1gFnSc7	Jadida
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Rabatem	rabat	k1gInSc7	rabat
a	a	k8xC	a
Kenitrou	Kenitra	k1gFnSc7	Kenitra
<g/>
.	.	kIx.	.
</s>
<s>
Zakládají	zakládat	k5eAaImIp3nP	zakládat
se	se	k3xPyFc4	se
však	však	k9	však
stále	stále	k6eAd1	stále
nové	nový	k2eAgFnSc2d1	nová
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
měst	město	k1gNnPc2	město
Tanger	Tangra	k1gFnPc2	Tangra
a	a	k8xC	a
Udžda	Udždo	k1gNnSc2	Udždo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
fosfátů	fosfát	k1gInPc2	fosfát
(	(	kIx(	(
<g/>
Maroko	Maroko	k1gNnSc1	Maroko
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnPc4d3	veliký
fosfátové	fosfátový	k2eAgFnPc4d1	fosfátová
zásoby	zásoba	k1gFnPc4	zásoba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnSc7d1	tradiční
součástí	součást	k1gFnSc7	součást
průmyslu	průmysl	k1gInSc2	průmysl
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
umělecká	umělecký	k2eAgNnPc4d1	umělecké
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
vysoké	vysoký	k2eAgFnSc3d1	vysoká
úrovni	úroveň	k1gFnSc3	úroveň
tvoří	tvořit	k5eAaImIp3nS	tvořit
tato	tento	k3xDgNnPc1	tento
odvětví	odvětvit	k5eAaPmIp3nP	odvětvit
významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
výrobních	výrobní	k2eAgNnPc2d1	výrobní
družstev	družstvo	k1gNnPc2	družstvo
zaměstnávajících	zaměstnávající	k2eAgNnPc2d1	zaměstnávající
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
000	[number]	k4	000
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
koberce	koberec	k1gInPc4	koberec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
významné	významný	k2eAgNnSc1d1	významné
odvětví	odvětví	k1gNnSc1	odvětví
hospodářství	hospodářství	k1gNnSc2	hospodářství
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
je	být	k5eAaImIp3nS	být
i	i	k9	i
těžba	těžba	k1gFnSc1	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
fosfátu	fosfát	k1gInSc3	fosfát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
největší	veliký	k2eAgFnSc2d3	veliký
světové	světový	k2eAgFnSc2d1	světová
zásoby	zásoba	k1gFnSc2	zásoba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
na	na	k7c6	na
území	území	k1gNnSc6	území
Maroka	Maroko	k1gNnSc2	Maroko
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Marockého	marocký	k2eAgInSc2d1	marocký
tisku	tisk	k1gInSc2	tisk
až	až	k9	až
75	[number]	k4	75
%	%	kIx~	%
celkových	celkový	k2eAgFnPc2d1	celková
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
s	s	k7c7	s
odhadem	odhad	k1gInSc7	odhad
na	na	k7c4	na
60	[number]	k4	60
mld.	mld.	k?	mld.
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
exportu	export	k1gInSc6	export
fosfátů	fosfát	k1gInPc2	fosfát
je	být	k5eAaImIp3nS	být
Maroko	Maroko	k1gNnSc1	Maroko
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
světového	světový	k2eAgInSc2d1	světový
trhu	trh	k1gInSc2	trh
se	s	k7c7	s
surovými	surový	k2eAgInPc7d1	surový
fosfáty	fosfát	k1gInPc7	fosfát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
těžbě	těžba	k1gFnSc6	těžba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
USA	USA	kA	USA
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bohaté	bohatý	k2eAgFnPc1d1	bohatá
zásoby	zásoba	k1gFnPc1	zásoba
nejsou	být	k5eNaImIp3nP	být
zatím	zatím	k6eAd1	zatím
plně	plně	k6eAd1	plně
využívány	využíván	k2eAgMnPc4d1	využíván
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
vytěženo	vytěžit	k5eAaPmNgNnS	vytěžit
25,4	[number]	k4	25,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
fosfátů	fosfát	k1gInPc2	fosfát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
fosfátů	fosfát	k1gInPc2	fosfát
disponuje	disponovat	k5eAaBmIp3nS	disponovat
Maroko	Maroko	k1gNnSc1	Maroko
ještě	ještě	k6eAd1	ještě
nerostnými	nerostný	k2eAgInPc7d1	nerostný
zdroji	zdroj	k1gInPc7	zdroj
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
fluoridu	fluorid	k1gInSc2	fluorid
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
barytu	baryt	k1gInSc2	baryt
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
malá	malý	k2eAgNnPc4d1	malé
naleziště	naleziště	k1gNnPc4	naleziště
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služby	služba	k1gFnPc1	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
především	především	k9	především
sektor	sektor	k1gInSc4	sektor
turistiky	turistika	k1gFnSc2	turistika
a	a	k8xC	a
dopravních	dopravní	k2eAgFnPc2d1	dopravní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
trvalý	trvalý	k2eAgInSc1d1	trvalý
rozvoj	rozvoj	k1gInSc1	rozvoj
po	po	k7c6	po
propadech	propad	k1gInPc6	propad
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
růstem	růst	k1gInSc7	růst
stojí	stát	k5eAaImIp3nS	stát
činnost	činnost	k1gFnSc1	činnost
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
strategii	strategie	k1gFnSc4	strategie
turistiky	turistika	k1gFnSc2	turistika
založeného	založený	k2eAgInSc2d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
Maroka	Maroko	k1gNnSc2	Maroko
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byly	být	k5eAaImAgInP	být
celkové	celkový	k2eAgInPc1d1	celkový
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
6,3	[number]	k4	6,3
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nárůst	nárůst	k1gInSc4	nárůst
o	o	k7c4	o
cca	cca	kA	cca
29,2	[number]	k4	29,2
%	%	kIx~	%
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
a	a	k8xC	a
Maroko	Maroko	k1gNnSc1	Maroko
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
6,58	[number]	k4	6,58
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
3,6	[number]	k4	3,6
milionů	milion	k4xCgInPc2	milion
jich	on	k3xPp3gInPc2	on
přijelo	přijet	k5eAaPmAgNnS	přijet
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
85	[number]	k4	85
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Maghrebu	Maghreb	k1gInSc2	Maghreb
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
Maročané	Maročan	k1gMnPc1	Maročan
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
2,98	[number]	k4	2,98
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
roste	růst	k5eAaImIp3nS	růst
zájem	zájem	k1gInSc1	zájem
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
–	–	k?	–
např.	např.	kA	např.
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Korey	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základními	základní	k2eAgFnPc7d1	základní
turistickými	turistický	k2eAgFnPc7d1	turistická
středisky	středisko	k1gNnPc7	středisko
jsou	být	k5eAaImIp3nP	být
Agadir	Agadir	k1gInSc1	Agadir
a	a	k8xC	a
Marrákeš	Marrákeš	k1gInSc1	Marrákeš
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zavítají	zavítat	k5eAaPmIp3nP	zavítat
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
turistiky	turistika	k1gFnSc2	turistika
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
telekomunikační	telekomunikační	k2eAgFnPc4d1	telekomunikační
obory	obora	k1gFnPc4	obora
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
nárůst	nárůst	k1gInSc1	nárůst
abonentů	abonent	k1gMnPc2	abonent
mobilních	mobilní	k2eAgFnPc2d1	mobilní
sítí	síť	k1gFnPc2	síť
o	o	k7c4	o
29	[number]	k4	29
%	%	kIx~	%
na	na	k7c4	na
celkových	celkový	k2eAgFnPc2d1	celková
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
čítá	čítat	k5eAaImIp3nS	čítat
cca	cca	kA	cca
400	[number]	k4	400
000	[number]	k4	000
abonentů	abonent	k1gMnPc2	abonent
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
o	o	k7c4	o
52,4	[number]	k4	52,4
%	%	kIx~	%
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
mělo	mít	k5eAaImAgNnS	mít
Maroko	Maroko	k1gNnSc1	Maroko
33	[number]	k4	33
848	[number]	k4	848
242	[number]	k4	242
obyvatel	obyvatel	k1gMnPc2	obyvatel
při	při	k7c6	při
hustotě	hustota	k1gFnSc6	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
73	[number]	k4	73
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
rozmístěna	rozmístit	k5eAaPmNgFnS	rozmístit
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
desetině	desetina	k1gFnSc6	desetina
rozlohy	rozloha	k1gFnSc2	rozloha
na	na	k7c6	na
pobřežním	pobřežní	k2eAgInSc6d1	pobřežní
pásu	pás	k1gInSc6	pás
na	na	k7c6	na
severozápadu	severozápad	k1gInSc2	severozápad
země	zem	k1gFnPc1	zem
žijí	žít	k5eAaImIp3nP	žít
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
neustále	neustále	k6eAd1	neustále
migruje	migrovat	k5eAaImIp3nS	migrovat
z	z	k7c2	z
chudých	chudý	k2eAgFnPc2d1	chudá
venkovských	venkovský	k2eAgFnPc2d1	venkovská
oblastí	oblast	k1gFnPc2	oblast
do	do	k7c2	do
hospodářsky	hospodářsky	k6eAd1	hospodářsky
rostoucích	rostoucí	k2eAgNnPc2d1	rostoucí
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgNnPc1d1	historické
královská	královský	k2eAgNnPc1d1	královské
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
Fè	Fè	k1gFnPc1	Fè
<g/>
,	,	kIx,	,
Marrákeš	Marrákeš	k1gInSc1	Marrákeš
<g/>
,	,	kIx,	,
Meknes	Meknes	k1gInSc1	Meknes
<g/>
,	,	kIx,	,
Rabat	rabat	k1gInSc1	rabat
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnPc4d1	moderní
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
centra	centrum	k1gNnPc4	centrum
jsou	být	k5eAaImIp3nP	být
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
Tanger	Tanger	k1gMnSc1	Tanger
<g/>
,	,	kIx,	,
Udžda	Udžda	k1gMnSc1	Udžda
a	a	k8xC	a
Agadir	Agadir	k1gMnSc1	Agadir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Arabové	Arab	k1gMnPc1	Arab
či	či	k8xC	či
poarabštění	poarabštěný	k2eAgMnPc1d1	poarabštěný
Berbeři	Berber	k1gMnPc1	Berber
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
řidčeji	řídce	k6eAd2	řídce
osídlených	osídlený	k2eAgFnPc6d1	osídlená
částech	část	k1gFnPc6	část
Maroka	Maroko	k1gNnSc2	Maroko
žijí	žít	k5eAaImIp3nP	žít
etnické	etnický	k2eAgFnPc1d1	etnická
menšiny	menšina	k1gFnPc1	menšina
–	–	k?	–
zejména	zejména	k6eAd1	zejména
Berbeři	Berber	k1gMnPc1	Berber
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Haratinové	Haratinový	k2eAgFnPc1d1	Haratinový
<g/>
.	.	kIx.	.
</s>
<s>
Berbeři	Berber	k1gMnPc1	Berber
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
především	především	k6eAd1	především
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Rif	rif	k1gInSc1	rif
(	(	kIx(	(
<g/>
Rifové	Rifová	k1gFnPc1	Rifová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Atlas	Atlas	k1gInSc1	Atlas
a	a	k8xC	a
v	v	k7c6	v
oázách	oáza	k1gFnPc6	oáza
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Atlasu	Atlas	k1gInSc2	Atlas
jako	jako	k9	jako
usedlí	usedlý	k2eAgMnPc1d1	usedlý
rolníci	rolník	k1gMnPc1	rolník
v	v	k7c6	v
sídlištích	sídliště	k1gNnPc6	sídliště
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
podobným	podobný	k2eAgNnSc7d1	podobné
opevněním	opevnění	k1gNnSc7	opevnění
(	(	kIx(	(
<g/>
kasbah	kasbah	k1gMnSc1	kasbah
<g/>
)	)	kIx)	)
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Haratinové	Haratin	k1gMnPc1	Haratin
(	(	kIx(	(
<g/>
potomci	potomek	k1gMnPc1	potomek
černých	černá	k1gFnPc2	černá
otroků	otrok	k1gMnPc2	otrok
<g/>
)	)	kIx)	)
sídlí	sídlet	k5eAaImIp3nS	sídlet
především	především	k9	především
v	v	k7c6	v
oázách	oáza	k1gFnPc6	oáza
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
početnou	početný	k2eAgFnSc7d1	početná
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
cizinců	cizinec	k1gMnPc2	cizinec
(	(	kIx(	(
<g/>
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
000	[number]	k4	000
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
220	[number]	k4	220
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ale	ale	k8xC	ale
odešla	odejít	k5eAaPmAgFnS	odejít
v	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
<g/>
Naopak	naopak	k6eAd1	naopak
nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
je	být	k5eAaImIp3nS	být
i	i	k9	i
počet	počet	k1gInSc4	počet
Marokánců	Marokánců	k?	Marokánců
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
–	–	k?	–
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
v	v	k7c6	v
přistěhovaleckých	přistěhovalecký	k2eAgFnPc6d1	přistěhovalecká
komunitách	komunita	k1gFnPc6	komunita
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
země	zem	k1gFnSc2	zem
z	z	k7c2	z
99,5	[number]	k4	99,5
%	%	kIx~	%
Maročané	Maročan	k1gMnPc1	Maročan
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
Mauretánci	Mauretánek	k1gMnPc1	Mauretánek
<g/>
,	,	kIx,	,
Senegalci	Senegalec	k1gMnPc1	Senegalec
a	a	k8xC	a
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Maroka	Maroko	k1gNnSc2	Maroko
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
(	(	kIx(	(
<g/>
asi	asi	k9	asi
98	[number]	k4	98
%	%	kIx~	%
<g/>
)	)	kIx)	)
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
sunnitský	sunnitský	k2eAgInSc4d1	sunnitský
islám	islám	k1gInSc4	islám
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgNnSc1d1	státní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
židovství	židovství	k1gNnSc4	židovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
arabština	arabština	k1gFnSc1	arabština
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
i	i	k8xC	i
berberština	berberština	k1gFnSc1	berberština
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
i	i	k9	i
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marocká	marocký	k2eAgFnSc1d1	marocká
arabština	arabština	k1gFnSc1	arabština
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
arabštiny	arabština	k1gFnSc2	arabština
či	či	k8xC	či
arabštiny	arabština	k1gFnSc2	arabština
moderní	moderní	k2eAgFnSc4d1	moderní
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
státech	stát	k1gInPc6	stát
kolem	kolem	k7c2	kolem
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Maročané	Maročan	k1gMnPc1	Maročan
však	však	k9	však
rozumí	rozumět	k5eAaImIp3nP	rozumět
i	i	k9	i
této	tento	k3xDgFnSc3	tento
arabštině	arabština	k1gFnSc3	arabština
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
té	ten	k3xDgFnSc3	ten
egyptské	egyptský	k2eAgFnSc3d1	egyptská
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
provázanosti	provázanost	k1gFnSc3	provázanost
televizních	televizní	k2eAgInPc2d1	televizní
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berberština	berberština	k1gFnSc1	berberština
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Maroka	Maroko	k1gNnSc2	Maroko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
základních	základní	k2eAgInPc6d1	základní
dialektech	dialekt	k1gInPc6	dialekt
podle	podle	k7c2	podle
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
názvy	název	k1gInPc1	název
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tarifit	Tarifit	k1gInSc1	Tarifit
–	–	k?	–
berberština	berberština	k1gFnSc1	berberština
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pohoří	pohoří	k1gNnSc2	pohoří
Rif	rif	k1gInSc1	rif
a	a	k8xC	a
severního	severní	k2eAgNnSc2d1	severní
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
Rify	rif	k1gInPc4	rif
</s>
</p>
<p>
<s>
Zaian	Zaian	k1gMnSc1	Zaian
<g/>
,	,	kIx,	,
Tamazight	Tamazight	k1gMnSc1	Tamazight
–	–	k?	–
oblast	oblast	k1gFnSc4	oblast
Středního	střední	k2eAgInSc2d1	střední
Atlasu	Atlas	k1gInSc2	Atlas
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
Maroka	Maroko	k1gNnSc2	Maroko
</s>
</p>
<p>
<s>
Tashelhaï	Tashelhaï	k?	Tashelhaï
<g/>
,	,	kIx,	,
Soussi	Sousse	k1gFnSc4	Sousse
<g/>
,	,	kIx,	,
Chleuh	Chleuh	k1gMnSc1	Chleuh
–	–	k?	–
oblast	oblast	k1gFnSc4	oblast
Vysokého	vysoký	k2eAgInSc2d1	vysoký
Atlasu	Atlas	k1gInSc2	Atlas
<g/>
,	,	kIx,	,
Antiatlasu	Antiatlas	k1gInSc2	Antiatlas
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
MarokaPřibližně	MarokaPřibližna	k1gFnSc6	MarokaPřibližna
25	[number]	k4	25
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
některým	některý	k3yIgMnPc3	některý
z	z	k7c2	z
dialektů	dialekt	k1gInPc2	dialekt
berberštiny	berberština	k1gFnSc2	berberština
<g/>
.	.	kIx.	.
</s>
<s>
Berberština	berberština	k1gFnSc1	berberština
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
zavádí	zavádět	k5eAaImIp3nS	zavádět
i	i	k9	i
do	do	k7c2	do
školní	školní	k2eAgFnSc2d1	školní
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
platí	platit	k5eAaImIp3nS	platit
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
snížit	snížit	k5eAaPmF	snížit
podíl	podíl	k1gInSc4	podíl
negramotného	gramotný	k2eNgNnSc2d1	negramotné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
na	na	k7c4	na
odhadovaných	odhadovaný	k2eAgNnPc2d1	odhadované
28	[number]	k4	28
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
dostat	dostat	k5eAaPmF	dostat
školství	školství	k1gNnSc1	školství
do	do	k7c2	do
odlehlých	odlehlý	k2eAgFnPc2d1	odlehlá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
stojí	stát	k5eAaImIp3nS	stát
školy	škola	k1gFnPc4	škola
i	i	k9	i
v	v	k7c6	v
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
údolích	údolí	k1gNnPc6	údolí
Atlasu	Atlas	k1gInSc2	Atlas
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
negramotným	gramotný	k2eNgMnPc3d1	negramotný
rodičům	rodič	k1gMnPc3	rodič
zde	zde	k6eAd1	zde
jejich	jejich	k3xOp3gFnPc4	jejich
děti	dítě	k1gFnPc4	dítě
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
čerství	čerstvý	k2eAgMnPc1d1	čerstvý
absolventi	absolvent	k1gMnPc1	absolvent
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sem	sem	k6eAd1	sem
vláda	vláda	k1gFnSc1	vláda
posílá	posílat	k5eAaImIp3nS	posílat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
první	první	k4xOgFnSc4	první
učitelskou	učitelský	k2eAgFnSc4d1	učitelská
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInPc4d1	trvající
1-4	[number]	k4	1-4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnější	prestižní	k2eAgFnSc1d3	nejprestižnější
je	být	k5eAaImIp3nS	být
islámská	islámský	k2eAgFnSc1d1	islámská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Fè	Fè	k1gFnSc6	Fè
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
859	[number]	k4	859
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Rabatu	rabato	k1gNnSc6	rabato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
sledování	sledování	k1gNnSc1	sledování
zápasů	zápas	k1gInPc2	zápas
marocké	marocký	k2eAgFnSc2d1	marocká
a	a	k8xC	a
španělské	španělský	k2eAgFnSc2d1	španělská
ligy	liga	k1gFnSc2	liga
je	být	k5eAaImIp3nS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
kratochvílí	kratochvíle	k1gFnSc7	kratochvíle
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
marockých	marocký	k2eAgFnPc6d1	marocká
kavárnách	kavárna	k1gFnPc6	kavárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maročtí	marocký	k2eAgMnPc1d1	marocký
atleti	atlet	k1gMnPc1	atlet
tradičně	tradičně	k6eAd1	tradičně
slaví	slavit	k5eAaImIp3nP	slavit
celosvětové	celosvětový	k2eAgInPc4d1	celosvětový
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
běžeckých	běžecký	k2eAgFnPc6d1	běžecká
disciplínách	disciplína	k1gFnPc6	disciplína
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Marocká	marocký	k2eAgFnSc1d1	marocká
Sahara	Sahara	k1gFnSc1	Sahara
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
stává	stávat	k5eAaImIp3nS	stávat
dějištěm	dějiště	k1gNnSc7	dějiště
zřejmě	zřejmě	k6eAd1	zřejmě
nejtěžšího	těžký	k2eAgInSc2d3	nejtěžší
běžeckého	běžecký	k2eAgInSc2d1	běžecký
závodu	závod	k1gInSc2	závod
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ultramaratonu	ultramaraton	k1gInSc2	ultramaraton
zvaného	zvaný	k2eAgInSc2d1	zvaný
Marathon	Marathon	k1gInSc1	Marathon
des	des	k1gNnSc6	des
Sables	Sables	k1gInSc1	Sables
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nejbohatší	bohatý	k2eAgFnSc4d3	nejbohatší
vrstvu	vrstva	k1gFnSc4	vrstva
se	s	k7c7	s
stále	stále	k6eAd1	stále
oblíbenějším	oblíbený	k2eAgMnSc7d2	oblíbenější
stává	stávat	k5eAaImIp3nS	stávat
golf	golf	k1gInSc1	golf
a	a	k8xC	a
golfová	golfový	k2eAgNnPc1d1	golfové
hřiště	hřiště	k1gNnPc1	hřiště
stále	stále	k6eAd1	stále
přibývají	přibývat	k5eAaImIp3nP	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Maroko	Maroko	k1gNnSc1	Maroko
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
ještě	ještě	k6eAd1	ještě
akutnější	akutní	k2eAgFnSc1d2	akutnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ELLINGHAM	ELLINGHAM	kA	ELLINGHAM
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
,	,	kIx,	,
GRISHBROOK	GRISHBROOK	kA	GRISHBROOK
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
,	,	kIx,	,
McVEIGH	McVEIGH	k1gMnSc1	McVEIGH
<g/>
,	,	kIx,	,
Shaun	Shaun	k1gMnSc1	Shaun
<g/>
.	.	kIx.	.
</s>
<s>
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
:	:	kIx,	:
Turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
Rough	Rough	k1gMnSc1	Rough
Guides	Guides	k1gMnSc1	Guides
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
JOTA	jota	k1gNnSc2	jota
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
Universum	universum	k1gNnSc1	universum
5	[number]	k4	5
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
DVD-ROM	DVD-ROM	k1gFnSc1	DVD-ROM
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc4	lexikon
zemí	zem	k1gFnPc2	zem
3	[number]	k4	3
<g/>
:	:	kIx,	:
Severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Arabský	arabský	k2eAgInSc1d1	arabský
poloostrov	poloostrov	k1gInSc1	poloostrov
a	a	k8xC	a
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
GeoCenter	GeoCenter	k1gMnSc1	GeoCenter
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
575	[number]	k4	575
<g/>
-	-	kIx~	-
<g/>
11786	[number]	k4	11786
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HARENBERG	HARENBERG	kA	HARENBERG
<g/>
,	,	kIx,	,
Bodo	Bodo	k6eAd1	Bodo
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
:	:	kIx,	:
Dohoda	dohoda	k1gFnSc1	dohoda
Spojenců	spojenec	k1gMnPc2	spojenec
o	o	k7c4	o
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
Print	Print	k1gInSc1	Print
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7153	[number]	k4	7153
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnPc4	encyklopedie
Diderot	Diderot	k1gMnSc1	Diderot
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Diderot	Diderot	k1gMnSc1	Diderot
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902723	[number]	k4	902723
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DIAS	DIAS	kA	DIAS
<g/>
,	,	kIx,	,
Pedro	Pedro	k1gNnSc1	Pedro
<g/>
.	.	kIx.	.
</s>
<s>
Norte	Norte	k5eAaPmIp2nP	Norte
de	de	k?	de
Áfrika	Áfrika	k1gFnSc1	Áfrika
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
of	of	k?	of
Arte	Arte	k1gInSc1	Arte
de	de	k?	de
Portugal	portugal	k1gInSc1	portugal
no	no	k9	no
mundo	mundo	k1gNnSc1	mundo
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
Arte	Arte	k1gInSc1	Arte
de	de	k?	de
Portugal	portugal	k1gInSc1	portugal
no	no	k9	no
mundo	mundo	k1gNnSc1	mundo
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Lisboa	Lisboa	k1gMnSc1	Lisboa
<g/>
,	,	kIx,	,
<g/>
Público	Público	k1gMnSc1	Público
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maroko	Maroko	k1gNnSc4	Maroko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Maroko	Maroko	k1gNnSc4	Maroko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Maroko	Maroko	k1gNnSc1	Maroko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Moroccan	Moroccan	k1gMnSc1	Moroccan
National	National	k1gMnSc1	National
Tourist	Tourist	k1gMnSc1	Tourist
Office	Office	kA	Office
</s>
</p>
<p>
<s>
Morocco	Morocco	k1gMnSc1	Morocco
<g/>
/	/	kIx~	/
Western	Western	kA	Western
Sahara	Sahara	k1gFnSc1	Sahara
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Morocco	Morocco	k6eAd1	Morocco
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Near	Near	k1gInSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gFnSc2	Not
<g/>
:	:	kIx,	:
Morocco	Morocco	k6eAd1	Morocco
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-20	[number]	k4	2011-04-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Morocco	Morocco	k6eAd1	Morocco
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-13	[number]	k4	2011-07-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006-05-10	[number]	k4	2006-05-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Rabatu	rabat	k1gInSc6	rabat
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Maroko	Maroko	k1gNnSc1	Maroko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-10-23	[number]	k4	2010-10-23
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NEVILL	NEVILL	kA	NEVILL
<g/>
,	,	kIx,	,
Barbour	Barbour	k1gMnSc1	Barbour
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Morocco	Morocco	k1gNnSc1	Morocco
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
