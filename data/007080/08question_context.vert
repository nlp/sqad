<s>
Čedič	čedič	k1gInSc1
neboli	neboli	k8xC
bazalt	bazalt	k1gInSc1
(	(	kIx(
<g/>
starší	starý	k2eAgInPc1d2
<g/>
,	,	kIx,
paleozoické	paleozoický	k2eAgInPc1d1
bazalty	bazalt	k1gInPc1
se	se	k3xPyFc4
nesprávně	správně	k6eNd1
nazývají	nazývat	k5eAaImIp3nP
diabas	diabas	k1gInSc4
nebo	nebo	k8xC
melafyr	melafyr	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
četná	četný	k2eAgFnSc1d1
tmavá	tmavý	k2eAgFnSc1d1
výlevná	výlevný	k2eAgFnSc1d1
vyvřelá	vyvřelý	k2eAgFnSc1d1
hornina	hornina	k1gFnSc1
s	s	k7c7
charakteristickou	charakteristický	k2eAgFnSc7d1
porfyrickou	porfyrický	k2eAgFnSc7d1
nebo	nebo	k8xC
sklovitou	sklovitý	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
s	s	k7c7
vyrostlicemi	vyrostlice	k1gFnPc7
jednotlivých	jednotlivý	k2eAgMnPc2d1
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>