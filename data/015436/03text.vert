<s>
Děkanát	děkanát	k1gInSc1
Zlín	Zlín	k1gInSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
ZlínPiecéze	ZlínPiecéza	k1gFnSc6
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Kamil	Kamil	k1gMnSc1
Obr	obr	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Zlín	Zlín	k1gInSc1
–	–	k?
sv.	sv.	kA
Filip	Filip	k1gMnSc1
a	a	k8xC
Jakub	Jakub	k1gMnSc1
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
prosinci	prosinec	k1gInSc3
2019	#num#	k4
</s>
<s>
Děkanát	děkanát	k1gInSc1
Zlín	Zlín	k1gInSc1
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc2
13	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Kamil	Kamil	k1gMnSc1
Obr	obr	k1gMnSc1
<g/>
,	,	kIx,
místoděkanem	místoděkan	k1gInSc7
byl	být	k5eAaImAgMnS
do	do	k7c2
června	červen	k1gInSc2
2019	#num#	k4
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Zelinka	Zelinka	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
července	červenec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
jím	on	k3xPp3gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šupol	Šupol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
Děkanát	děkanát	k1gInSc1
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Gottwaldov	Gottwaldov	k1gInSc1
<g/>
)	)	kIx)
vznikl	vzniknout	k5eAaPmAgInS
k	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
1952	#num#	k4
sloučením	sloučení	k1gNnSc7
napajedelského	napajedelský	k2eAgInSc2d1
<g/>
,	,	kIx,
pozlovského	pozlovský	k2eAgInSc2d1
a	a	k8xC
vizovického	vizovický	k2eAgInSc2d1
děkanátu	děkanát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
byl	být	k5eAaImAgInS
vizovický	vizovický	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
opět	opět	k6eAd1
oddělen	oddělit	k5eAaPmNgInS
a	a	k8xC
zároveň	zároveň	k6eAd1
byly	být	k5eAaImAgFnP
farnosti	farnost	k1gFnPc4
Luhačovice	Luhačovice	k1gFnPc4
a	a	k8xC
Pozlovice	Pozlovice	k1gFnPc4
přičleněny	přičleněn	k2eAgFnPc1d1
k	k	k7c3
děkanátu	děkanát	k1gInSc6
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
farností	farnost	k1gFnPc2
děkanátu	děkanát	k1gInSc2
Zlín	Zlín	k1gInSc4
</s>
<s>
FarnostFarní	FarnostFarnět	k5eAaPmIp3nS
kostelSprávceWeb	kostelSprávceWba	k1gFnPc2
farnosti	farnost	k1gFnSc2
</s>
<s>
Březnice	Březnice	k1gFnSc1
u	u	k7c2
Zlína	Zlín	k1gInSc2
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Fojtík	Fojtík	k1gMnSc1
</s>
<s>
Halenkovice	Halenkovice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Josefa	Josef	k1gMnSc4
</s>
<s>
excurrendo	excurrendo	k1gNnSc1
Spytihněv	Spytihněv	k1gFnSc2
</s>
<s>
Lhota	Lhota	k1gFnSc1
u	u	k7c2
Malenovic	Malenovice	k1gFnPc2
</s>
<s>
sv.	sv.	kA
Anny	Anna	k1gFnPc1
</s>
<s>
excurrendo	excurrendo	k1gNnSc1
<g/>
:	:	kIx,
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Strnad	Strnad	k1gMnSc1
</s>
<s>
Mysločovice	Mysločovice	k1gFnSc1
</s>
<s>
Nejsv	Nejsv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojice	trojice	k1gFnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Šálek	Šálek	k1gMnSc1
</s>
<s>
Napajedla	Napajedla	k1gNnPc1
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ryszard	Ryszard	k1gMnSc1
Piotr	Piotr	k1gMnSc1
Turko	Turko	k1gNnSc4
</s>
<s>
Otrokovice	Otrokovice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šupol	Šupola	k1gFnPc2
</s>
<s>
Pohořelice	Pohořelice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
et	et	k?
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Jáně	Jáně	k?
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
P.	P.	kA
Marie	Maria	k1gFnSc2
</s>
<s>
R.	R.	kA
D.	D.	kA
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Káňa	Káňa	k1gMnSc1
<g/>
,	,	kIx,
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
(	(	kIx(
<g/>
administrátor	administrátor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tlumačov	Tlumačov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
</s>
<s>
excurrendo	excurrendo	k1gNnSc1
<g/>
:	:	kIx,
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Šálek	Šálek	k1gMnSc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
Ořechov	Ořechov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Antonín	Antonín	k1gMnSc1
Ptáček	Ptáček	k1gMnSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
–	–	k?
Malenovice	Malenovice	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Strnad	Strnad	k1gMnSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
–	–	k?
sv.	sv.	kA
Filip	Filip	k1gMnSc1
a	a	k8xC
Jakub	Jakub	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Filipa	Filip	k1gMnSc4
a	a	k8xC
Jakuba	Jakub	k1gMnSc4
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Kamil	Kamil	k1gMnSc1
Obr	obr	k1gMnSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
–	–	k?
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Pomocnice	pomocnice	k1gFnSc2
křesťanů	křesťan	k1gMnPc2
</s>
<s>
Panny	Panna	k1gFnPc1
Marie	Maria	k1gFnSc2
Pomocnice	pomocnice	k1gFnSc2
křesťanů	křesťan	k1gMnPc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Dan	Dan	k1gMnSc1
Žůrek	Žůrek	k1gMnSc1
<g/>
,	,	kIx,
SDB	SDB	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PALA	Pala	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
:	:	kIx,
Farnost	farnost	k1gFnSc1
sv.	sv.	kA
Filipa	Filip	k1gMnSc2
a	a	k8xC
Jakuba	Jakub	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
:	:	kIx,
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
199	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9788025468128	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
věků	věk	k1gInPc2
–	–	k?
kronika	kronika	k1gFnSc1
nejdůležitějších	důležitý	k2eAgFnPc2d3
událostí	událost	k1gFnPc2
zlínské	zlínský	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
začátku	začátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
35	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČERVINKA	Červinka	k1gMnSc1
<g/>
,	,	kIx,
Arnošt	Arnošt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
zlínské	zlínský	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
a	a	k8xC
farností	farnost	k1gFnPc2
okolních	okolní	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Džavík	Džavík	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
199	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
8090024114	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Náboženský	náboženský	k2eAgInSc4d1
a	a	k8xC
církevní	církevní	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
městě	město	k1gNnSc6
Zlíně	Zlín	k1gInSc6
a	a	k8xC
farnosti	farnost	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
94	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
Údaje	údaj	k1gInPc1
děkanátu	děkanát	k1gInSc2
na	na	k7c6
webu	web	k1gInSc6
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Děkanáty	děkanát	k1gInPc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
</s>
<s>
Holešov	Holešov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Konice	Konice	k1gFnSc2
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Šternberk	Šternberk	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
Zlín	Zlín	k1gInSc1
</s>
<s>
Březnice	Březnice	k1gFnSc1
u	u	k7c2
Zlína	Zlín	k1gInSc2
•	•	k?
Halenkovice	Halenkovice	k1gFnPc4
•	•	k?
Lhota	Lhota	k1gFnSc1
u	u	k7c2
Malenovic	Malenovice	k1gFnPc2
•	•	k?
Mysločovice	Mysločovice	k1gFnSc2
•	•	k?
Napajedla	napajedlo	k1gNnSc2
•	•	k?
Otrokovice	Otrokovice	k1gFnPc4
•	•	k?
Pohořelice	Pohořelice	k1gFnPc4
•	•	k?
Spytihněv	Spytihněv	k1gMnSc1
•	•	k?
Tlumačov	Tlumačov	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Ořechov	Ořechov	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
-	-	kIx~
Malenovice	Malenovice	k1gFnSc1
•	•	k?
Zlín	Zlín	k1gInSc1
-	-	kIx~
sv.	sv.	kA
Filipa	Filip	k1gMnSc2
a	a	k8xC
Jakuba	Jakub	k1gMnSc2
•	•	k?
Zlín	Zlín	k1gInSc1
-	-	kIx~
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Pomocnice	pomocnice	k1gFnSc2
křesťanů	křesťan	k1gMnPc2
</s>
