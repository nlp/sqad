<s>
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
Klíč	klíč	k1gInSc1	klíč
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1841	[number]	k4	1841
Hostinné	hostinný	k2eAgFnSc2d1	hostinná
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1926	[number]	k4	1926
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
grafický	grafický	k2eAgMnSc1d1	grafický
technik	technik	k1gMnSc1	technik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
heliogravuru	heliogravura	k1gFnSc4	heliogravura
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
proces	proces	k1gInSc1	proces
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
první	první	k4xOgFnSc4	první
hlubotiskovou	hlubotiskový	k2eAgFnSc4d1	hlubotisková
rotačku	rotačka	k1gFnSc4	rotačka
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
papírně	papírna	k1gFnSc6	papírna
v	v	k7c6	v
Hostinném	hostinný	k2eAgInSc6d1	hostinný
zvané	zvaný	k2eAgFnSc2d1	zvaná
Labský	labský	k2eAgInSc4d1	labský
mlýn	mlýn	k1gInSc4	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dětství	dětství	k1gNnSc1	dětství
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
úrazem	úraz	k1gInSc7	úraz
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
kulhal	kulhat	k5eAaImAgMnS	kulhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
barikádě	barikáda	k1gFnSc6	barikáda
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
v	v	k7c6	v
neustálé	neustálý	k2eAgFnSc6d1	neustálá
opozici	opozice	k1gFnSc6	opozice
proti	proti	k7c3	proti
rakouskému	rakouský	k2eAgInSc3d1	rakouský
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
malířskou	malířský	k2eAgFnSc4d1	malířská
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
předveden	předvést	k5eAaPmNgInS	předvést
na	na	k7c6	na
policii	policie	k1gFnSc6	policie
za	za	k7c4	za
kresbu	kresba	k1gFnSc4	kresba
hanobící	hanobící	k2eAgFnSc4d1	hanobící
c.k.	c.k.	k?	c.k.
ministra	ministr	k1gMnSc4	ministr
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
tam	tam	k6eAd1	tam
ztropil	ztropit	k5eAaPmAgMnS	ztropit
výtržnost	výtržnost	k1gFnSc4	výtržnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
trest	trest	k1gInSc4	trest
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
z	z	k7c2	z
Akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
vypovězen	vypovědět	k5eAaPmNgMnS	vypovědět
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
malováním	malování	k1gNnSc7	malování
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
Akademii	akademie	k1gFnSc6	akademie
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dostudovat	dostudovat	k5eAaPmF	dostudovat
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
fotografií	fotografia	k1gFnSc7	fotografia
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odjel	odjet	k5eAaPmAgMnS	odjet
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
vítězné	vítězný	k2eAgMnPc4d1	vítězný
Prušáky	Prušák	k1gMnPc4	Prušák
<g/>
.	.	kIx.	.
</s>
<s>
Ateliér	ateliér	k1gInSc1	ateliér
Raffael	Raffael	k1gInSc1	Raffael
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
živnost	živnost	k1gFnSc1	živnost
<g/>
,	,	kIx,	,
bouřlivák	bouřlivák	k1gMnSc1	bouřlivák
Klíč	klíč	k1gInSc1	klíč
však	však	k9	však
navíc	navíc	k6eAd1	navíc
založil	založit	k5eAaPmAgInS	založit
satirický	satirický	k2eAgInSc1d1	satirický
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kreslil	kreslit	k5eAaImAgMnS	kreslit
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
pro	pro	k7c4	pro
maďarský	maďarský	k2eAgInSc4d1	maďarský
časopis	časopis	k1gInSc4	časopis
Kulihrášek	Kulihrášek	k1gMnSc1	Kulihrášek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kreslil	kreslit	k5eAaImAgMnS	kreslit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
úspěšně	úspěšně	k6eAd1	úspěšně
jako	jako	k8xS	jako
koncertní	koncertní	k2eAgMnSc1d1	koncertní
malíř	malíř	k1gMnSc1	malíř
<g/>
:	:	kIx,	:
Kreslil	kreslit	k5eAaImAgInS	kreslit
pravou	pravý	k2eAgFnSc4d1	pravá
i	i	k8xC	i
levou	levý	k2eAgFnSc4d1	levá
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
celou	celá	k1gFnSc4	celá
první	první	k4xOgFnSc4	první
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
kreslil	kreslit	k5eAaImAgInS	kreslit
i	i	k9	i
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
zrovna	zrovna	k6eAd1	zrovna
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejen	nejen	k6eAd1	nejen
bravurním	bravurní	k2eAgMnSc7d1	bravurní
kreslířem	kreslíř	k1gMnSc7	kreslíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
maloval	malovat	k5eAaImAgInS	malovat
–	–	k?	–
např.	např.	kA	např.
portrét	portrét	k1gInSc1	portrét
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Fügnera	Fügner	k1gMnSc2	Fügner
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
kreslil	kreslit	k5eAaImAgMnS	kreslit
Garibaldiho	Garibaldi	k1gMnSc4	Garibaldi
<g/>
,	,	kIx,	,
kněžnu	kněžna	k1gFnSc4	kněžna
Metternichovou	Metternichová	k1gFnSc4	Metternichová
<g/>
,	,	kIx,	,
Palackého	Palackého	k2eAgInSc1d1	Palackého
aj.	aj.	kA	aj.
Byl	být	k5eAaImAgInS	být
i	i	k9	i
zručným	zručný	k2eAgMnSc7d1	zručný
litografem	litograf	k1gMnSc7	litograf
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
ovládal	ovládat	k5eAaImAgInS	ovládat
mědiryt	mědiryt	k1gInSc1	mědiryt
i	i	k8xC	i
lept	lept	k1gInSc1	lept
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rytí	rytí	k1gNnSc6	rytí
dřevorytů	dřevoryt	k1gInPc2	dřevoryt
pro	pro	k7c4	pro
časopisy	časopis	k1gInPc4	časopis
často	často	k6eAd1	často
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
rytecká	rytecký	k2eAgFnSc1d1	rytecká
práce	práce	k1gFnSc1	práce
usnadnit	usnadnit	k5eAaPmF	usnadnit
pomocí	pomocí	k7c2	pomocí
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
reprodukcí	reprodukce	k1gFnSc7	reprodukce
polotónových	polotónový	k2eAgInPc2d1	polotónový
obrazů	obraz	k1gInPc2	obraz
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poznal	poznat	k5eAaPmAgMnS	poznat
obtíže	obtíž	k1gFnPc4	obtíž
tisku	tisk	k1gInSc2	tisk
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
tisk	tisk	k1gInSc4	tisk
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Namáhavé	namáhavý	k2eAgInPc1d1	namáhavý
pokusy	pokus	k1gInPc1	pokus
přinesly	přinést	k5eAaPmAgInP	přinést
ovoce	ovoce	k1gNnSc4	ovoce
o	o	k7c6	o
silvestrovské	silvestrovský	k2eAgFnSc6d1	silvestrovská
noci	noc	k1gFnSc6	noc
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
datem	datum	k1gNnSc7	datum
vynálezu	vynález	k1gInSc2	vynález
heliogravury	heliogravura	k1gFnSc2	heliogravura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
důvěřivý	důvěřivý	k2eAgMnSc1d1	důvěřivý
a	a	k8xC	a
nedal	dát	k5eNaPmAgMnS	dát
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
nápad	nápad	k1gInSc4	nápad
odcizen	odcizen	k2eAgMnSc1d1	odcizen
<g/>
,	,	kIx,	,
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
než	než	k8xS	než
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
pokusy	pokus	k1gInPc7	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
nyní	nyní	k6eAd1	nyní
tisknout	tisknout	k5eAaImF	tisknout
obrázky	obrázek	k1gInPc4	obrázek
strojně	strojně	k6eAd1	strojně
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
špatných	špatný	k2eAgFnPc6d1	špatná
zkušenostech	zkušenost	k1gFnPc6	zkušenost
své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
tajil	tajit	k5eAaImAgMnS	tajit
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1890	[number]	k4	1890
je	být	k5eAaImIp3nS	být
rokem	rok	k1gInSc7	rok
vynálezu	vynález	k1gInSc2	vynález
rotačního	rotační	k2eAgInSc2d1	rotační
stíracího	stírací	k2eAgInSc2d1	stírací
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Lancasteru	Lancaster	k1gInSc6	Lancaster
navštívil	navštívit	k5eAaPmAgMnS	navštívit
firmu	firma	k1gFnSc4	firma
Storey	Storea	k1gFnSc2	Storea
Brothers	Brothers	k1gInSc1	Brothers
&	&	k?	&
Co	co	k3yQnSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgInSc1d1	slavný
grafický	grafický	k2eAgInSc1d1	grafický
závod	závod	k1gInSc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
nového	nový	k2eAgInSc2d1	nový
hlubotiskového	hlubotiskový	k2eAgInSc2d1	hlubotiskový
stroje	stroj	k1gInSc2	stroj
okamžitě	okamžitě	k6eAd1	okamžitě
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
Klíč	klíč	k1gInSc1	klíč
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
hned	hned	k6eAd1	hned
odcestovat	odcestovat	k5eAaPmF	odcestovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
založena	založen	k2eAgFnSc1d1	založena
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
zejména	zejména	k9	zejména
tisku	tisk	k1gInSc6	tisk
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ředitelem	ředitel	k1gMnSc7	ředitel
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
Silvestrem	Silvestr	k1gMnSc7	Silvestr
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
heliogravuru	heliogravura	k1gFnSc4	heliogravura
<g/>
,	,	kIx,	,
prováděl	provádět	k5eAaImAgMnS	provádět
pokusy	pokus	k1gInPc4	pokus
s	s	k7c7	s
měděnou	měděný	k2eAgFnSc7d1	měděná
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařila	dařit	k5eNaImAgFnS	dařit
a	a	k8xC	a
tak	tak	k9	tak
desku	deska	k1gFnSc4	deska
uložil	uložit	k5eAaPmAgInS	uložit
do	do	k7c2	do
zásuvky	zásuvka	k1gFnSc2	zásuvka
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
uložen	uložit	k5eAaPmNgInS	uložit
asfaltový	asfaltový	k2eAgInSc1d1	asfaltový
prášek	prášek	k1gInSc1	prášek
k	k	k7c3	k
naprašování	naprašování	k1gNnSc3	naprašování
desek	deska	k1gFnPc2	deska
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Zvířený	zvířený	k2eAgInSc1d1	zvířený
prášek	prášek	k1gInSc1	prášek
se	se	k3xPyFc4	se
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
rovnoměrné	rovnoměrný	k2eAgFnSc6d1	rovnoměrná
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
nahřál	nahřát	k5eAaPmAgMnS	nahřát
desku	deska	k1gFnSc4	deska
nad	nad	k7c7	nad
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
asfaltový	asfaltový	k2eAgInSc1d1	asfaltový
prášek	prášek	k1gInSc1	prášek
přitavil	přitavit	k5eAaPmAgInS	přitavit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
vrstvy	vrstva	k1gFnSc2	vrstva
vyleptal	vyleptat	k5eAaPmAgInS	vyleptat
obrázek	obrázek	k1gInSc1	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
ráno	ráno	k6eAd1	ráno
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
tiskaře	tiskař	k1gMnSc4	tiskař
Pissaniho	Pissani	k1gMnSc4	Pissani
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
otisky	otisk	k1gInPc7	otisk
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
dokonalé	dokonalý	k2eAgFnPc1d1	dokonalá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zdařilý	zdařilý	k2eAgInSc1d1	zdařilý
obrázek	obrázek	k1gInSc1	obrázek
věnoval	věnovat	k5eAaImAgMnS	věnovat
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
jako	jako	k8xS	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
heliogravura	heliogravura	k1gFnSc1	heliogravura
<g/>
;	;	kIx,	;
použitá	použitý	k2eAgFnSc1d1	použitá
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
však	však	k9	však
hodila	hodit	k5eAaImAgFnS	hodit
jen	jen	k9	jen
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
na	na	k7c6	na
ručním	ruční	k2eAgInSc6d1	ruční
lisu	lis	k1gInSc6	lis
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalení	zdokonalení	k1gNnSc1	zdokonalení
techniky	technika	k1gFnSc2	technika
spočívalo	spočívat	k5eAaImAgNnS	spočívat
v	v	k7c6	v
nahrazení	nahrazení	k1gNnSc6	nahrazení
asfaltového	asfaltový	k2eAgNnSc2d1	asfaltové
zrna	zrno	k1gNnSc2	zrno
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
jemnou	jemný	k2eAgFnSc7d1	jemná
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
k	k	k7c3	k
leptání	leptání	k1gNnSc3	leptání
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozložil	rozložit	k5eAaPmAgMnS	rozložit
na	na	k7c4	na
drobnohledné	drobnohledný	k2eAgInPc4d1	drobnohledný
čtverečky	čtvereček	k1gInPc4	čtvereček
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jakási	jakýsi	k3yIgFnSc1	jakýsi
mřížka	mřížka	k1gFnSc1	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
byly	být	k5eAaImAgFnP	být
různě	různě	k6eAd1	různě
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
,	,	kIx,	,
v	v	k7c6	v
tmavších	tmavý	k2eAgFnPc6d2	tmavší
partiích	partie	k1gFnPc6	partie
obrazu	obraz	k1gInSc2	obraz
bylo	být	k5eAaImAgNnS	být
barvy	barva	k1gFnPc4	barva
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
světlejších	světlý	k2eAgFnPc2d2	světlejší
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
vrstva	vrstva	k1gFnSc1	vrstva
barvy	barva	k1gFnSc2	barva
přenesená	přenesený	k2eAgFnSc1d1	přenesená
na	na	k7c4	na
papír	papír	k1gInSc4	papír
byla	být	k5eAaImAgFnS	být
nestejná	stejný	k2eNgFnSc1d1	nestejná
a	a	k8xC	a
tištěné	tištěný	k2eAgFnSc2d1	tištěná
reprodukce	reprodukce	k1gFnSc2	reprodukce
měly	mít	k5eAaImAgInP	mít
měkké	měkký	k2eAgInPc1d1	měkký
polotónované	polotónovaný	k2eAgInPc1d1	polotónovaný
přechody	přechod	k1gInPc1	přechod
a	a	k8xC	a
hluboké	hluboký	k2eAgInPc1d1	hluboký
stíny	stín	k1gInPc1	stín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vynález	vynález	k1gInSc1	vynález
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
vynálezem	vynález	k1gInSc7	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
J.	J.	kA	J.
Gutenberga	Gutenberg	k1gMnSc2	Gutenberg
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
nové	nový	k2eAgFnSc2d1	nová
polygrafické	polygrafický	k2eAgFnSc2d1	polygrafická
techniky	technika	k1gFnSc2	technika
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
ilustrovaných	ilustrovaný	k2eAgInPc2d1	ilustrovaný
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
obalů	obal	k1gInPc2	obal
<g/>
,	,	kIx,	,
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
apod.	apod.	kA	apod.
v	v	k7c6	v
masovém	masový	k2eAgInSc6d1	masový
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
BLÁHA	Bláha	k1gMnSc1	Bláha
<g/>
,	,	kIx,	,
R.	R.	kA	R.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Klíč	klíč	k1gInSc1	klíč
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
204	[number]	k4	204
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Klíč	klíč	k1gInSc1	klíč
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Klíč	klíč	k1gInSc1	klíč
KDO	kdo	k3yRnSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yQnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Klíč	klíč	k1gInSc1	klíč
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
</s>
