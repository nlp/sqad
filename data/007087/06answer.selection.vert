<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
přeměna	přeměna	k1gFnSc1	přeměna
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
působením	působení	k1gNnSc7	působení
jiného	jiný	k2eAgNnSc2d1	jiné
jádra	jádro	k1gNnSc2	jádro
nebo	nebo	k8xC	nebo
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
fotonu	foton	k1gInSc2	foton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
