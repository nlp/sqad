<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
přeměna	přeměna	k1gFnSc1	přeměna
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
působením	působení	k1gNnSc7	působení
jiného	jiný	k2eAgNnSc2d1	jiné
jádra	jádro	k1gNnSc2	jádro
nebo	nebo	k8xC	nebo
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
fotonu	foton	k1gInSc2	foton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
štěpí	štěpit	k5eAaImIp3nS	štěpit
po	po	k7c6	po
vniknutí	vniknutí	k1gNnSc6	vniknutí
subatomární	subatomární	k2eAgFnSc2d1	subatomární
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
neutronu	neutron	k1gInSc3	neutron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
jádra	jádro	k1gNnSc2	jádro
lehčích	lehký	k2eAgInPc2d2	lehčí
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jaderné	jaderný	k2eAgFnSc6d1	jaderná
reakci	reakce	k1gFnSc6	reakce
dochází	docházet	k5eAaImIp3nS	docházet
jak	jak	k6eAd1	jak
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
struktury	struktura	k1gFnSc2	struktura
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
jejich	jejich	k3xOp3gInSc2	jejich
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
přeměna	přeměna	k1gFnSc1	přeměna
nuklidu	nuklid	k1gInSc2	nuklid
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
nuklid	nuklid	k1gInSc4	nuklid
(	(	kIx(	(
<g/>
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
nedochází	docházet	k5eNaImIp3nS	docházet
při	při	k7c6	při
chemických	chemický	k2eAgFnPc6d1	chemická
reakcích	reakce	k1gFnPc6	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
reakce	reakce	k1gFnPc4	reakce
platí	platit	k5eAaImIp3nP	platit
<g/>
:	:	kIx,	:
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
:	:	kIx,	:
Z	z	k7c2	z
=	=	kIx~	=
Zx	Zx	k1gFnSc2	Zx
+	+	kIx~	+
Za	za	k7c7	za
=	=	kIx~	=
Zy	Zy	k1gMnSc1	Zy
+	+	kIx~	+
Zb	Zb	k1gMnSc1	Zb
=	=	kIx~	=
Z	Z	kA	Z
(	(	kIx(	(
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
'	'	kIx"	'
jsou	být	k5eAaImIp3nP	být
počty	počet	k1gInPc1	počet
elementárních	elementární	k2eAgInPc2d1	elementární
nábojů	náboj	k1gInPc2	náboj
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
částice	částice	k1gFnSc2	částice
před	před	k7c7	před
reakcí	reakce	k1gFnSc7	reakce
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
počtu	počet	k1gInSc2	počet
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
A	A	kA	A
=	=	kIx~	=
Ax	Ax	k1gMnSc1	Ax
+	+	kIx~	+
Aa	Aa	k1gMnSc1	Aa
=	=	kIx~	=
Ay	Ay	k1gMnSc1	Ay
+	+	kIx~	+
Ab	Ab	k1gMnSc1	Ab
=	=	kIx~	=
A	A	kA	A
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
'	'	kIx"	'
jsou	být	k5eAaImIp3nP	být
počty	počet	k1gInPc4	počet
nukleonů	nukleon	k1gInPc2	nukleon
před	před	k7c7	před
reakcí	reakce	k1gFnSc7	reakce
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
reaktantů	reaktant	k1gMnPc2	reaktant
včetně	včetně	k7c2	včetně
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
atakující	atakující	k2eAgFnSc2d1	atakující
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
střely	střela	k1gFnSc2	střela
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
celkové	celkový	k2eAgFnSc3d1	celková
energii	energie	k1gFnSc3	energie
produktu	produkt	k1gInSc2	produkt
včetně	včetně	k7c2	včetně
tepelného	tepelný	k2eAgNnSc2d1	tepelné
zabarvení	zabarvení	k1gNnSc2	zabarvení
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Samovolnost	samovolnost	k1gFnSc1	samovolnost
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
uvolněním	uvolnění	k1gNnSc7	uvolnění
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
(	(	kIx(	(
<g/>
Díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
spočítat	spočítat	k5eAaPmF	spočítat
minimální	minimální	k2eAgFnSc1d1	minimální
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
předchozímu	předchozí	k2eAgInSc3d1	předchozí
zákonu	zákon	k1gInSc3	zákon
možná	možná	k9	možná
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
některých	některý	k3yIgFnPc2	některý
reakcí	reakce	k1gFnPc2	reakce
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc6	zachování
parity	parita	k1gFnSc2	parita
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
reakcí	reakce	k1gFnPc2	reakce
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
izospinu	izospin	k1gInSc2	izospin
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kriterií	kriterium	k1gNnPc2	kriterium
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
reagujících	reagující	k2eAgFnPc2d1	reagující
částic	částice	k1gFnPc2	částice
-	-	kIx~	-
mononukleární	mononukleární	k2eAgFnSc1d1	mononukleární
(	(	kIx(	(
<g/>
samovolná	samovolný	k2eAgFnSc1d1	samovolná
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
,	,	kIx,	,
rozpad	rozpad	k1gInSc1	rozpad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
binukleární	binukleárnit	k5eAaPmIp3nS	binukleárnit
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
Podle	podle	k7c2	podle
účastníků	účastník	k1gMnPc2	účastník
srážky	srážka	k1gFnSc2	srážka
-	-	kIx~	-
fotojaderné	fotojaderný	k2eAgFnPc1d1	fotojaderný
reakce	reakce	k1gFnPc1	reakce
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
reakce	reakce	k1gFnSc1	reakce
těžkých	těžký	k2eAgInPc2d1	těžký
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnPc1	reakce
indukované	indukovaný	k2eAgFnPc1d1	indukovaná
protony	proton	k1gInPc4	proton
<g/>
,	,	kIx,	,
aktivace	aktivace	k1gFnSc1	aktivace
neutrony	neutron	k1gInPc1	neutron
<g/>
...	...	k?	...
Podle	podle	k7c2	podle
průběhu	průběh	k1gInSc2	průběh
-	-	kIx~	-
(	(	kIx(	(
<g/>
štěpná	štěpný	k2eAgFnSc1d1	štěpná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
jaderná	jaderný	k2eAgFnSc1d1	jaderná
syntéza	syntéza	k1gFnSc1	syntéza
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
přenosu	přenos	k1gInSc2	přenos
nukleonu	nukleon	k1gInSc2	nukleon
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
energie	energie	k1gFnSc2	energie
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
exoenergetické	exoenergetický	k2eAgFnSc2d1	exoenergetický
<g/>
,	,	kIx,	,
endoenergetické	endoenergetický	k2eAgFnSc2d1	endoenergetický
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
energie	energie	k1gFnSc2	energie
nalétávajících	nalétávající	k2eAgFnPc2d1	nalétávající
částic	částice	k1gFnPc2	částice
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
nízkoenergetické	nízkoenergetický	k2eAgFnSc2d1	nízkoenergetická
<g/>
,	,	kIx,	,
vysokoenergetické	vysokoenergetický	k2eAgFnSc2d1	vysokoenergetická
<g/>
,	,	kIx,	,
relativistické	relativistický	k2eAgFnSc2d1	relativistická
srážky	srážka	k1gFnSc2	srážka
<g/>
)	)	kIx)	)
Prostou	prostý	k2eAgFnSc7d1	prostá
přeměnou	přeměna	k1gFnSc7	přeměna
vzniká	vznikat	k5eAaImIp3nS	vznikat
většinou	většinou	k6eAd1	většinou
dusík	dusík	k1gInSc4	dusík
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
13	[number]	k4	13
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
B	B	kA	B
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
He	he	k0	he
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Štěpná	štěpný	k2eAgFnSc1d1	štěpná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Štěpení	štěpený	k2eAgMnPc1d1	štěpený
U235	U235	k1gMnPc1	U235
(	(	kIx(	(
<g/>
tepelnými	tepelný	k2eAgInPc7d1	tepelný
neutrony	neutron	k1gInPc7	neutron
<g/>
)	)	kIx)	)
Štěpení	štěpení	k1gNnSc3	štěpení
uranu	uran	k1gInSc2	uran
U235	U235	k1gFnSc2	U235
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
průniku	průnik	k1gInSc6	průnik
pomalého	pomalý	k2eAgInSc2d1	pomalý
(	(	kIx(	(
<g/>
tepelného	tepelný	k2eAgInSc2d1	tepelný
<g/>
)	)	kIx)	)
neutronu	neutron	k1gInSc2	neutron
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
složené	složený	k2eAgNnSc1d1	složené
jádro	jádro	k1gNnSc1	jádro
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
na	na	k7c4	na
2	[number]	k4	2
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
těžké	těžký	k2eAgFnSc6d1	těžká
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
volných	volný	k2eAgInPc2d1	volný
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
postupně	postupně	k6eAd1	postupně
termalizovat	termalizovat	k5eAaBmF	termalizovat
srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
částicemi	částice	k1gFnPc7	částice
a	a	k8xC	a
ztrácet	ztrácet	k5eAaImF	ztrácet
tak	tak	k6eAd1	tak
svoji	svůj	k3xOyFgFnSc4	svůj
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
energii	energie	k1gFnSc4	energie
až	až	k9	až
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
štěpení	štěpení	k1gNnSc4	štěpení
dalšího	další	k2eAgNnSc2d1	další
jádra	jádro	k1gNnSc2	jádro
uranu	uran	k1gInSc2	uran
U	u	k7c2	u
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jader	jádro	k1gNnPc2	jádro
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
štěpením	štěpení	k1gNnSc7	štěpení
uranového	uranový	k2eAgNnSc2d1	uranové
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
lehkých	lehký	k2eAgInPc2d1	lehký
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
Z	z	k7c2	z
=	=	kIx~	=
35	[number]	k4	35
(	(	kIx(	(
<g/>
Br	br	k0	br
<g/>
)	)	kIx)	)
-	-	kIx~	-
45	[number]	k4	45
(	(	kIx(	(
<g/>
Rh	Rh	k1gFnSc2	Rh
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
těžkých	těžký	k2eAgInPc2d1	těžký
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
Z	z	k7c2	z
=	=	kIx~	=
51	[number]	k4	51
(	(	kIx(	(
<g/>
Sb	sb	kA	sb
<g/>
)	)	kIx)	)
-	-	kIx~	-
62	[number]	k4	62
(	(	kIx(	(
<g/>
Sm	Sm	k1gFnSc2	Sm
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
lze	lze	k6eAd1	lze
znázornit	znázornit	k5eAaPmF	znázornit
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
92	[number]	k4	92
:	:	kIx,	:
:	:	kIx,	:
235	[number]	k4	235
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
36	[number]	k4	36
:	:	kIx,	:
:	:	kIx,	:
93	[number]	k4	93
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
r	r	kA	r
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
56	[number]	k4	56
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
140	[number]	k4	140
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
a	a	k8xC	a
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
92	[number]	k4	92
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
235	[number]	k4	235
<g/>
}	}	kIx)	}
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
36	[number]	k4	36
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
93	[number]	k4	93
<g/>
}	}	kIx)	}
<g/>
Kr	Kr	k1gFnSc1	Kr
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
56	[number]	k4	56
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
140	[number]	k4	140
<g/>
}	}	kIx)	}
<g/>
Ba	ba	k9	ba
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
:	:	kIx,	:
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
štěpení	štěpení	k1gNnSc1	štěpení
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
něm.	něm.	k?	něm.
chemiky	chemik	k1gMnPc7	chemik
Otto	Otto	k1gMnSc1	Otto
Hahnem	Hahn	k1gInSc7	Hahn
a	a	k8xC	a
Fritz	Fritz	k1gInSc4	Fritz
Strassmannem	Strassmann	k1gInSc7	Strassmann
u	u	k7c2	u
izotopu	izotop	k1gInSc2	izotop
uranu	uran	k1gInSc2	uran
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tříštivých	tříštivý	k2eAgFnPc6d1	tříštivá
reakcích	reakce	k1gFnPc6	reakce
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc2	uvolnění
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
tříštivá	tříštivý	k2eAgFnSc1d1	tříštivá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dodat	dodat	k5eAaPmF	dodat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
MeV	MeV	k1gMnPc2	MeV
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
33	[number]	k4	33
:	:	kIx,	:
:	:	kIx,	:
75	[number]	k4	75
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
s	s	k7c7	s
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
24	[number]	k4	24
:	:	kIx,	:
:	:	kIx,	:
49	[number]	k4	49
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
r	r	kA	r
+	+	kIx~	+
10	[number]	k4	10
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
18	[number]	k4	18
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
33	[number]	k4	33
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
75	[number]	k4	75
<g/>
}	}	kIx)	}
<g/>
As	as	k1gNnSc2	as
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
24	[number]	k4	24
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
49	[number]	k4	49
<g/>
}	}	kIx)	}
<g/>
Cr	cr	k0	cr
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
p	p	k?	p
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
18	[number]	k4	18
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fúze	fúze	k1gFnSc1	fúze
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
H	H	kA	H
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
e	e	k0	e
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
He	he	k0	he
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
e	e	k0	e
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
He	he	k0	he
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
štěpné	štěpný	k2eAgFnPc4d1	štěpná
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
naděje	naděje	k1gFnPc1	naděje
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
využití	využití	k1gNnSc2	využití
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doposud	doposud	k6eAd1	doposud
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
toho	ten	k3xDgMnSc4	ten
prakticky	prakticky	k6eAd1	prakticky
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Štěpení	štěpení	k1gNnSc1	štěpení
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
průběhu	průběh	k1gInSc2	průběh
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
4	[number]	k4	4
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
Podkritické	Podkritický	k2eAgNnSc1d1	Podkritický
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
neutron	neutron	k1gInSc1	neutron
je	být	k5eAaImIp3nS	být
zachycen	zachycen	k2eAgInSc1d1	zachycen
(	(	kIx(	(
<g/>
přirozený	přirozený	k2eAgInSc1d1	přirozený
rozpad	rozpad	k1gInSc1	rozpad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritické	kritický	k2eAgInPc1d1	kritický
–	–	k?	–
jeden	jeden	k4xCgInSc1	jeden
neutron	neutron	k1gInSc1	neutron
není	být	k5eNaImIp3nS	být
zachycen	zachycen	k2eAgInSc1d1	zachycen
<g/>
,	,	kIx,	,
štěpí	štěpit	k5eAaImIp3nP	štěpit
další	další	k2eAgNnPc1d1	další
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
unikne	uniknout	k5eAaPmIp3nS	uniknout
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
neutron	neutron	k1gInSc1	neutron
(	(	kIx(	(
<g/>
řízená	řízený	k2eAgFnSc1d1	řízená
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadkritické	nadkritický	k2eAgInPc1d1	nadkritický
–	–	k?	–
2	[number]	k4	2
neutrony	neutron	k1gInPc7	neutron
nejsou	být	k5eNaImIp3nP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
<g/>
.	.	kIx.	.
</s>
<s>
Superkritické	Superkritický	k2eAgInPc1d1	Superkritický
–	–	k?	–
neřízená	řízený	k2eNgFnSc1d1	neřízená
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnPc1	reakce
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
následující	následující	k2eAgFnSc1d1	následující
situace	situace	k1gFnSc1	situace
<g/>
:	:	kIx,	:
Neřízená	řízený	k2eNgFnSc1d1	neřízená
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
reagovat	reagovat	k5eAaBmF	reagovat
všechny	všechen	k3xTgInPc4	všechen
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
končí	končit	k5eAaImIp3nS	končit
výbuchem	výbuch	k1gInSc7	výbuch
tzv.	tzv.	kA	tzv.
atomové	atomový	k2eAgFnPc4d1	atomová
bomby	bomba	k1gFnPc4	bomba
<g/>
.	.	kIx.	.
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
izotopy	izotop	k1gInPc1	izotop
235	[number]	k4	235
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
233	[number]	k4	233
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
239	[number]	k4	239
<g/>
Pu	Pu	k1gFnPc2	Pu
<g/>
.	.	kIx.	.
</s>
<s>
Řízená	řízený	k2eAgFnSc1d1	řízená
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
reagovat	reagovat	k5eAaBmF	reagovat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
neutron	neutron	k1gInSc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
využito	využít	k5eAaPmNgNnS	využít
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
řízenou	řízený	k2eAgFnSc4d1	řízená
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
moderátoru	moderátor	k1gInSc2	moderátor
provedl	provést	k5eAaPmAgMnS	provést
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
Enrico	Enrico	k1gMnSc1	Enrico
Fermi	Fer	k1gFnPc7	Fer
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1942	[number]	k4	1942
pod	pod	k7c7	pod
stadionem	stadion	k1gInSc7	stadion
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lepil	lepit	k5eAaImAgMnS	lepit
<g/>
:	:	kIx,	:
Malý	malý	k2eAgInSc4d1	malý
lexikon	lexikon	k1gInSc4	lexikon
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
;	;	kIx,	;
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
;	;	kIx,	;
176	[number]	k4	176
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
