<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Šumava	Šumava	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
ŠumavaIUCN	ŠumavaIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Královský	královský	k2eAgInSc1d1
hvozdZákladní	hvozdZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1963	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
996	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
NP	NP	kA
a	a	k8xC
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc5
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
a	a	k8xC
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
25	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Šumava	Šumava	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
43	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.npsumava.cz	www.npsumava.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Šumava	Šumava	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
Ministerstvem	ministerstvo	k1gNnSc7
školství	školství	k1gNnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
27	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1963	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
168	#num#	k4
654	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
Ministerstvo	ministerstvo	k1gNnSc4
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
upravilo	upravit	k5eAaPmAgNnS
podmínky	podmínka	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
v	v	k7c6
tomto	tento	k3xDgNnSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1991	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
ČR	ČR	kA
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Šumava	Šumava	k1gFnSc1
uvnitř	uvnitř	k7c2
dosavadní	dosavadní	k2eAgFnSc2d1
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vlastně	vlastně	k9
CHKO	CHKO	kA
stala	stát	k5eAaPmAgFnS
ochrannou	ochranný	k2eAgFnSc7d1
zónou	zóna	k1gFnSc7
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
spravuje	spravovat	k5eAaImIp3nS
i	i	k9
další	další	k2eAgFnSc1d1
nejcennější	cenný	k2eAgFnSc1d3
partie	partie	k1gFnSc1
Šumavy	Šumava	k1gFnSc2
–	–	k?
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Královský	královský	k2eAgInSc4d1
hvozd	hvozd	k1gInSc4
s	s	k7c7
Černým	černý	k2eAgMnSc7d1
a	a	k8xC
Čertovým	čertový	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
a	a	k8xC
Boubín	Boubín	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
rozhodnutím	rozhodnutí	k1gNnSc7
politiků	politik	k1gMnPc2
do	do	k7c2
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Šumava	Šumava	k1gFnSc1
nedostaly	dostat	k5eNaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizačně	organizačně	k6eAd1
je	být	k5eAaImIp3nS
správa	správa	k1gFnSc1
CHKO	CHKO	kA
začleněna	začleněn	k2eAgFnSc1d1
do	do	k7c2
správy	správa	k1gFnSc2
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
společnou	společný	k2eAgFnSc4d1
provázanost	provázanost	k1gFnSc4
činnosti	činnost	k1gFnSc2
výhodné	výhodný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivá	jednotlivý	k2eAgFnSc1d1
pracoviště	pracoviště	k1gNnSc1
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
v	v	k7c6
Nýrsku	Nýrsko	k1gNnSc6
<g/>
,	,	kIx,
Kašperských	kašperský	k2eAgFnPc6d1
Horách	hora	k1gFnPc6
<g/>
,	,	kIx,
Vimperku	Vimperk	k1gInSc6
a	a	k8xC
Horní	horní	k2eAgFnSc6d1
Plané	Planá	k1gFnSc6
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
správy	správa	k1gFnSc2
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
Sušice	Sušice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
Bavorský	bavorský	k2eAgInSc1d1
les	les	k1gInSc1
chrání	chránit	k5eAaImIp3nS
kulturně	kulturně	k6eAd1
vysoce	vysoce	k6eAd1
hodnotnou	hodnotný	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
prolíná	prolínat	k5eAaImIp3nS
s	s	k7c7
lidskými	lidský	k2eAgFnPc7d1
aktivitami	aktivita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
CHKO	CHKO	kA
disponuje	disponovat	k5eAaBmIp3nS
většími	veliký	k2eAgFnPc7d2
<g/>
,	,	kIx,
v	v	k7c6
zákonech	zákon	k1gInPc6
zakotvenými	zakotvený	k2eAgFnPc7d1
pravomocemi	pravomoc	k1gFnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Bavorský	bavorský	k2eAgInSc1d1
přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
více	hodně	k6eAd2
na	na	k7c6
základě	základ	k1gInSc6
dobrovolnosti	dobrovolnost	k1gFnSc2
a	a	k8xC
občanského	občanský	k2eAgNnSc2d1
uvědomění	uvědomění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozloha	rozloha	k1gFnSc1
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
nepokrytá	pokrytý	k2eNgFnSc1d1
územím	území	k1gNnSc7
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
99	#num#	k4
624	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
rozloze	rozloha	k1gFnSc6
je	být	k5eAaImIp3nS
zahrnuto	zahrnout	k5eAaPmNgNnS
27,4	27,4	k4
%	%	kIx~
zemědělské	zemědělský	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
27	#num#	k4
297	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
orná	orný	k2eAgFnSc1d1
9	#num#	k4
tisíc	tisíc	k4xCgInPc2
ha	ha	kA
<g/>
,	,	kIx,
louky	louka	k1gFnPc4
a	a	k8xC
pastviny	pastvina	k1gFnPc4
11	#num#	k4
tisíc	tisíc	k4xCgInPc2
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
57,6	57,6	k4
%	%	kIx~
lesní	lesní	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
57	#num#	k4
383	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
a	a	k8xC
0,4	0,4	k4
%	%	kIx~
zastavěné	zastavěný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
399	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pralesovité	pralesovitý	k2eAgInPc4d1
porostní	porostní	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
zaujímají	zaujímat	k5eAaImIp3nP
cca	cca	kA
0,6	0,6	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
CHKO	CHKO	kA
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
a	a	k8xC
sukcesní	sukcesní	k2eAgNnPc4d1
stádia	stádium	k1gNnPc4
zbývajících	zbývající	k2eAgInPc2d1
přibližně	přibližně	k6eAd1
14	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpětí	rozpětí	k1gNnSc1
nadmořských	nadmořský	k2eAgFnPc2d1
výšek	výška	k1gFnPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
498	#num#	k4
m	m	kA
(	(	kIx(
<g/>
Víteň	Víteň	k1gFnSc1
<g/>
)	)	kIx)
do	do	k7c2
1362	#num#	k4
m	m	kA
(	(	kIx(
<g/>
Boubín	Boubín	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
je	být	k5eAaImIp3nS
cca	cca	kA
21	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
tj.	tj.	kA
asi	asi	k9
22	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
/	/	kIx~
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
a	a	k8xC
počet	počet	k1gInSc1
evidovaných	evidovaný	k2eAgMnPc2d1
domů	dům	k1gInPc2
cca	cca	kA
4	#num#	k4
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Zajišťují	zajišťovat	k5eAaImIp3nP
dlouhodobou	dlouhodobý	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
výjimečných	výjimečný	k2eAgInPc2d1
biotopů	biotop	k1gInPc2
a	a	k8xC
garanci	garance	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
monitorování	monitorování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zásadě	zásada	k1gFnSc6
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
je	on	k3xPp3gInPc4
rozlišit	rozlišit	k5eAaPmF
na	na	k7c4
3	#num#	k4
rozdílné	rozdílný	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Pralesy	prales	k1gInPc1
a	a	k8xC
přírodní	přírodní	k2eAgInPc1d1
nepozměněné	pozměněný	k2eNgInPc1d1
mokřady	mokřad	k1gInPc1
–	–	k?
Boubínský	boubínský	k2eAgInSc1d1
prales	prales	k1gInSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
niva	niva	k1gFnSc1
<g/>
,	,	kIx,
Zátoňská	Zátoňský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
sleduje	sledovat	k5eAaImIp3nS
samovolné	samovolný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
přirozených	přirozený	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Samovolně	samovolně	k6eAd1
vzniklé	vzniklý	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
druhotné	druhotný	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
–	–	k?
Chřepice	Chřepice	k1gFnSc2
<g/>
,	,	kIx,
Velké	velký	k2eAgNnSc4d1
Bahno	bahno	k1gNnSc4
<g/>
,	,	kIx,
Pravětínská	Pravětínský	k2eAgNnPc1d1
lada	lado	k1gNnPc1
<g/>
,	,	kIx,
Nebe	nebe	k1gNnSc1
<g/>
,	,	kIx,
určené	určený	k2eAgNnSc1d1
ke	k	k7c3
sledování	sledování	k1gNnSc3
samovolného	samovolný	k2eAgInSc2d1
nástupu	nástup	k1gInSc2
lesa	les	k1gInSc2
a	a	k8xC
prostorové	prostorový	k2eAgFnSc2d1
flexibility	flexibilita	k1gFnSc2
rostlinných	rostlinný	k2eAgInPc2d1
druhů	druh	k1gInPc2
ovlivňovaných	ovlivňovaný	k2eAgFnPc2d1
tímto	tento	k3xDgInSc7
lesem	les	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Lokality	lokalita	k1gFnPc1
ochrany	ochrana	k1gFnSc2
výjimečných	výjimečný	k2eAgInPc2d1
druhů	druh	k1gInPc2
–	–	k?
Blanice	Blanice	k1gFnPc4
<g/>
,	,	kIx,
Pod	pod	k7c7
Popelní	popelný	k2eAgMnPc1d1
horou	hora	k1gFnSc7
<g/>
,	,	kIx,
Vyšný	vyšný	k2eAgInSc4d1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
zajišťující	zajišťující	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
ohrožených	ohrožený	k2eAgInPc2d1
nebo	nebo	k8xC
typických	typický	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
nebo	nebo	k8xC
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
Strž	strž	k1gFnSc1
(	(	kIx(
<g/>
1972	#num#	k4
–	–	k?
79,02	79,02	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Boubínský	boubínský	k2eAgInSc1d1
prales	prales	k1gInSc1
(	(	kIx(
<g/>
1858	#num#	k4
–	–	k?
677,33	677,33	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Černé	Černé	k2eAgNnSc1d1
a	a	k8xC
Čertovo	čertův	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
(	(	kIx(
<g/>
1911	#num#	k4
–	–	k?
174,86	174,86	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Niva	niva	k1gFnSc1
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
120,31	120,31	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Amálino	Amálin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
80,93	80,93	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
20,34	20,34	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Datelovská	Datelovský	k2eAgFnSc1d1
strž	strž	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
5,78	5,78	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Hamižná	hamižný	k2eAgFnSc1d1
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
14,89	14,89	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Chřepice	Chřepice	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
16,89	16,89	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kepelské	Kepelský	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
67,93	67,93	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Kyselovský	Kyselovský	k2eAgInSc1d1
les	les	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
6,79	6,79	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Lakmal	Lakmal	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
41,08	41,08	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Losenice	Losenice	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
–	–	k?
2,70	2,70	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Losenice	Losenice	k1gFnSc1
II	II	kA
(	(	kIx(
<g/>
2008	#num#	k4
–	–	k?
13,27	13,27	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Městišťské	Městišťské	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
168,91	168,91	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Milešický	Milešický	k2eAgInSc1d1
prales	prales	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
8,29	8,29	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
Mokřinách	mokřina	k1gFnPc6
</s>
<s>
Na	na	k7c6
soutoku	soutok	k1gInSc6
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
28,22	28,22	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Nebe	nebe	k1gNnSc1
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
13,89	13,89	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
Otov	Otov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
–	–	k?
6,31	6,31	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Otovský	Otovský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Pateříkova	Pateříkův	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
7,04	7,04	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Prameniště	prameniště	k1gNnSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
335,27	335,27	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pravětínská	Pravětínský	k2eAgFnSc1d1
Lada	Lada	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
49,32	49,32	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Borková	Borková	k1gFnSc1
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
47,48	47,48	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Rožnov	Rožnov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
–	–	k?
28,85	28,85	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Svobodova	Svobodův	k2eAgFnSc1d1
niva	niva	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
–	–	k?
8,61	8,61	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Zátoňská	Zátoňský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
49,29	49,29	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Zelenský	Zelenský	k2eAgInSc1d1
luh	luh	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
16,65	16,65	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Zhůřská	Zhůřský	k2eAgFnSc1d1
pláň	pláň	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
131,94	131,94	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Zhůřský	Zhůřský	k2eAgInSc1d1
lom	lom	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
0,75	0,75	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Blanice	Blanice	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
294,68	294,68	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Házlův	Házlův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
51	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Jasánky	jasánek	k1gInPc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
105,20	105,20	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Jilmová	jilmový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
–	–	k?
8,12	8,12	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Královský	královský	k2eAgInSc1d1
hvozd	hvozd	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
–	–	k?
2020,85	2020,85	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Lipka	lipka	k1gFnSc1
(	(	kIx(
<g/>
1933	#num#	k4
–	–	k?
0,96	0,96	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Křišťanov	Křišťanov	k1gInSc4
–	–	k?
Vyšný	vyšný	k2eAgMnSc1d1
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
4,41	4,41	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Malý	malý	k2eAgInSc1d1
Polec	Polec	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
–	–	k?
11,20	11,20	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Multerberské	Multerberský	k2eAgNnSc1d1
rašeliniště	rašeliniště	k1gNnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
9,12	9,12	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
los	los	k1gInSc1
evropský	evropský	k2eAgInSc1d1
</s>
<s>
Pasecká	Pasecký	k2eAgFnSc1d1
slať	slať	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
89,55	89,55	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pestřice	Pestřice	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
106,58	106,58	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
Popelní	popelný	k2eAgMnPc1d1
horou	hora	k1gFnSc7
(	(	kIx(
<g/>
1985	#num#	k4
–	–	k?
6,10	6,10	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Poušť	poušť	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
47,16	47,16	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Prameniště	prameniště	k1gNnSc1
Hamerského	hamerský	k2eAgInSc2d1
potoka	potok	k1gInSc2
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
54,83	54,83	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Račínská	Račínský	k2eAgNnPc1d1
prameniště	prameniště	k1gNnPc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
123,27	123,27	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Kyselov	Kyselovo	k1gNnPc2
</s>
<s>
Spáleniště	spáleniště	k1gNnSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
53,05	53,05	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Velké	velký	k2eAgNnSc1d1
Bahno	bahno	k1gNnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
85,76	85,76	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Šumava	Šumava	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Správa	správa	k1gFnSc1
NP	NP	kA
a	a	k8xC
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Národní	národní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Šumava	Šumava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc1d1
parky	park	k1gInPc1
</s>
<s>
Novohradské	novohradský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
•	•	k?
Poluška	Poluška	k1gFnSc1
•	•	k?
Soběnovská	Soběnovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Vyšebrodsko	Vyšebrodsko	k1gNnSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
stěna-Luč	stěna-Lučet	k5eAaPmRp2nS
•	•	k?
Vyšenské	Vyšenský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
•	•	k?
Žofínský	žofínský	k2eAgInSc1d1
prales	prales	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Olšina	olšina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bořinka	bořinka	k1gFnSc1
•	•	k?
Český	český	k2eAgInSc1d1
Jílovec	jílovec	k1gInSc1
•	•	k?
Dívčí	dívčí	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Holubovské	Holubovské	k2eAgInSc2d1
hadce	hadec	k1gInSc2
•	•	k?
Jaronínská	Jaronínský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Ježová	Ježová	k1gFnSc1
•	•	k?
Kleť	Kleť	k1gFnSc1
•	•	k?
Kozí	kozí	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kyselovský	Kyselovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Na	na	k7c6
Mokřinách	mokřina	k1gFnPc6
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
II	II	kA
•	•	k?
Olšov	Olšov	k1gInSc1
•	•	k?
Otov	Otov	k1gInSc1
•	•	k?
Otovský	Otovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Pivonické	Pivonický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Pláničský	Pláničský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Pod	pod	k7c7
Borkovou	Borková	k1gFnSc7
•	•	k?
Ptačí	ptačí	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
•	•	k?
Rapotická	Rapotický	k2eAgFnSc1d1
březina	březina	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Borková	Borková	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Kapličky	kaplička	k1gFnSc2
•	•	k?
Rožnov	Rožnov	k1gInSc1
•	•	k?
Světlá	světlat	k5eAaImIp3nS
•	•	k?
Ševcova	Ševcův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Besednické	besednický	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
•	•	k?
Cvičák	cvičák	k1gInSc1
•	•	k?
Házlův	Házlův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
•	•	k?
Hejdlovský	Hejdlovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
luka	luka	k1gNnPc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Malše	Malše	k1gFnSc1
•	•	k?
Jasánky	jasánek	k1gInPc4
•	•	k?
Kalamandra	kalamandra	k1gFnSc1
•	•	k?
Kotlina	kotlina	k1gFnSc1
pod	pod	k7c7
Pláničským	Pláničský	k2eAgInSc7d1
rybníkem	rybník	k1gInSc7
•	•	k?
Meandry	meandr	k1gInPc4
Chvalšinského	Chvalšinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Medvědí	medvědí	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
u	u	k7c2
Borského	Borského	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
•	•	k?
Muckovské	Muckovský	k2eAgInPc4d1
vápencové	vápencový	k2eAgInPc4d1
lomy	lom	k1gInPc4
•	•	k?
Multerberské	Multerberský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Stráži	stráž	k1gFnSc6
•	•	k?
Olšina	olšina	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Novolhotském	Novolhotský	k2eAgInSc6d1
lese	les	k1gInSc6
•	•	k?
Pestřice	Pestřice	k1gFnSc2
•	•	k?
Pohořské	Pohořský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Prameniště	prameniště	k1gNnSc2
Hamerského	hamerský	k2eAgInSc2d1
potoka	potok	k1gInSc2
u	u	k7c2
Zvonkové	zvonkový	k2eAgFnSc2d1
•	•	k?
Prameniště	prameniště	k1gNnSc4
Pohořského	Pohořský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Provázková	provázkový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Račínská	Račínský	k2eAgNnPc4d1
prameniště	prameniště	k1gNnSc4
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Bobovec	Bobovec	k1gMnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Kyselov	Kyselovo	k1gNnPc2
•	•	k?
Slavkovické	Slavkovický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
chlumek	chlumek	k1gInSc1
•	•	k?
Spáleniště	spáleniště	k1gNnSc2
•	•	k?
Stodůlecký	Stodůlecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kříž	Kříž	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
•	•	k?
Šimečkova	Šimečkův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
U	u	k7c2
tří	tři	k4xCgInPc2
můstků	můstek	k1gInPc2
•	•	k?
Uhlířský	uhlířský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Úval	úval	k1gInSc1
Zvonková	zvonkový	k2eAgFnSc1d1
•	•	k?
Velké	velký	k2eAgNnSc4d1
bahno	bahno	k1gNnSc4
•	•	k?
Vltava	Vltava	k1gFnSc1
u	u	k7c2
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Výří	výří	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Žestov	Žestov	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Klatovy	Klatovy	k1gInPc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Šumava	Šumava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc1d1
parky	park	k1gInPc1
</s>
<s>
Buděticko	Buděticko	k6eAd1
•	•	k?
Kochánov	Kochánov	k1gInSc1
•	•	k?
Plánický	Plánický	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
Strž	strž	k1gFnSc1
•	•	k?
Černé	Černá	k1gFnSc2
a	a	k8xC
Čertovo	čertův	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
•	•	k?
Pastviště	pastviště	k1gNnSc1
u	u	k7c2
Fínů	Fín	k1gInPc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Amálino	Amálin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Bělč	Bělč	k1gMnSc1
•	•	k?
Bělyšov	Bělyšov	k1gInSc1
•	•	k?
Borek	borek	k1gInSc1
u	u	k7c2
Velhartic	Velhartice	k1gFnPc2
•	•	k?
Brčálnické	Brčálnický	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
•	•	k?
Čepičná	Čepičný	k2eAgFnSc1d1
•	•	k?
Datelovská	Datelovský	k2eAgFnSc1d1
strž	strž	k1gFnSc1
•	•	k?
Hamižná	hamižný	k2eAgFnSc1d1
•	•	k?
Chřepice	Chřepice	k1gFnSc1
•	•	k?
Jelení	jelení	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kepelské	Kepelský	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
•	•	k?
Kříženecké	kříženecký	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
•	•	k?
Lakmal	Lakmal	k1gInSc1
•	•	k?
Losenice	Losenice	k1gFnSc2
•	•	k?
Losenice	Losenice	k1gFnSc2
II	II	kA
•	•	k?
Luňáky	Luňák	k1gMnPc4
•	•	k?
Městišťské	Městišťský	k2eAgFnSc2d1
rokle	rokle	k1gFnSc2
•	•	k?
Milčice	Milčice	k1gFnSc2
•	•	k?
Na	na	k7c6
Volešku	Volešek	k1gInSc6
•	•	k?
Nebe	nebe	k1gNnSc2
•	•	k?
Onen	onen	k3xDgInSc4
svět	svět	k1gInSc4
•	•	k?
Páteříková	Páteříkový	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Poustka	poustka	k1gFnSc1
•	•	k?
Prácheň	Prácheň	k1gFnSc1
•	•	k?
Prameniště	prameniště	k1gNnSc2
•	•	k?
Pučanka	Pučanka	k1gFnSc1
•	•	k?
Svobodova	Svobodův	k2eAgFnSc1d1
niva	niva	k1gFnSc1
•	•	k?
Úhlavský	Úhlavský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
V	v	k7c6
Morávkách	Morávkách	k?
•	•	k?
Zbynické	Zbynický	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Zelenský	Zelenský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Zhůřská	Zhůřský	k2eAgNnPc4d1
hnízdiště	hnízdiště	k1gNnPc4
•	•	k?
Zhůřská	Zhůřský	k2eAgFnSc1d1
pláň	pláň	k1gFnSc1
•	•	k?
Zhůřský	Zhůřský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Žežulka	žežulka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bejkovna	Bejkovna	k1gFnSc1
•	•	k?
Dolejší	Dolejší	k1gFnSc1
drahy	draha	k1gFnSc2
•	•	k?
Chodská	chodský	k2eAgFnSc1d1
Úhlava	Úhlava	k1gFnSc1
•	•	k?
Chudenická	Chudenický	k2eAgFnSc1d1
bažantnice	bažantnice	k1gFnSc1
•	•	k?
Královský	královský	k2eAgInSc1d1
hvozd	hvozd	k1gInSc1
•	•	k?
Loreta	loreta	k1gFnSc1
•	•	k?
Mrazové	mrazový	k2eAgInPc4d1
srázy	sráz	k1gInPc4
u	u	k7c2
Lazen	Lazna	k1gFnPc2
•	•	k?
Pohorsko	Pohorsko	k1gNnSc4
•	•	k?
Stará	Stará	k1gFnSc1
Úhlava	Úhlava	k1gFnSc1
•	•	k?
Strašínská	Strašínský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Svaté	svatý	k2eAgFnSc3d1
Pole	pola	k1gFnSc3
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Bernard	Bernard	k1gMnSc1
•	•	k?
Tupadelské	Tupadelský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
U	u	k7c2
Radošína	Radošín	k1gInSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Vlkonice	Vlkonice	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Prachatice	Prachatice	k1gFnPc1
Národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
</s>
<s>
Šumava	Šumava	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Boubínský	boubínský	k2eAgInSc1d1
prales	prales	k1gInSc1
•	•	k?
Velká	velký	k2eAgFnSc1d1
Niva	niva	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blanice	Blanice	k1gFnSc1
•	•	k?
Prameniště	prameniště	k1gNnSc1
Blanice	Blanice	k1gFnSc2
•	•	k?
U	u	k7c2
Hajnice	hajnice	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Amálino	Amálin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Čertova	čertův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Hadce	hadec	k1gInSc2
u	u	k7c2
Dobročkova	Dobročkův	k2eAgNnSc2d1
•	•	k?
Hliniště	hliniště	k1gNnSc2
•	•	k?
Hornovltavické	Hornovltavický	k2eAgFnSc2d1
pastviny	pastvina	k1gFnSc2
•	•	k?
Kralovické	Kralovický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Libín	Libín	k1gMnSc1
•	•	k?
Lipka	lipka	k1gFnSc1
I.	I.	kA
•	•	k?
Malý	malý	k2eAgInSc1d1
Polec	Polec	k1gInSc1
•	•	k?
Milešický	Milešický	k2eAgInSc1d1
prales	prales	k1gInSc1
•	•	k?
Miletínky	Miletínka	k1gFnSc2
•	•	k?
Mokrý	mokrý	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Na	na	k7c6
soutoku	soutok	k1gInSc6
•	•	k?
Nad	nad	k7c7
Zavírkou	zavírka	k1gFnSc7
•	•	k?
Najmanka	Najmanka	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Kořenského	Kořenský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Opolenec	Opolenec	k1gMnSc1
•	•	k?
Pod	pod	k7c7
Farským	farský	k2eAgInSc7d1
lesem	les	k1gInSc7
•	•	k?
Pod	pod	k7c7
Popelní	popelný	k2eAgMnPc1d1
horou	hora	k1gFnSc7
•	•	k?
Pravětínská	Pravětínský	k2eAgFnSc1d1
Lada	Lada	k1gFnSc1
•	•	k?
Radost	radost	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
u	u	k7c2
Martinala	Martinala	k1gFnSc2
•	•	k?
Saladínská	Saladínský	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
•	•	k?
Zátoňská	Zátoňský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Zátoňská	Zátoňský	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blanice	Blanice	k1gFnSc1
•	•	k?
Buková	bukový	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Čistá	čistý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Dobročkovské	Dobročkovský	k2eAgInPc1d1
hadce	hadec	k1gInPc1
•	•	k?
Háje	háj	k1gInSc2
•	•	k?
Hrádeček	hrádeček	k1gInSc1
•	•	k?
Irův	Irův	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
•	•	k?
Jaroškov	Jaroškov	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Jezerní	jezerní	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Jezerní	jezerní	k2eAgFnSc4d1
slať	slať	k1gFnSc4
•	•	k?
Jilmová	jilmový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Kotlina	kotlina	k1gFnSc1
Valné	valný	k2eAgFnSc2d1
•	•	k?
Koubovský	Koubovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Lipka	lipka	k1gFnSc1
•	•	k?
Mařský	Mařský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Mastnice	mastnice	k1gFnSc2
•	•	k?
Onšovice	Onšovice	k1gFnSc1
–	–	k?
Mlýny	mlýn	k1gInPc1
•	•	k?
Pančice	Pančice	k1gFnSc2
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
řekách	řeka	k1gFnPc6
•	•	k?
Pasecká	Pasecký	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Ostrohem	Ostroh	k1gInSc7
•	•	k?
Pod	pod	k7c7
Ostrou	ostrý	k2eAgFnSc7d1
horou	hora	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Sviňovicemi	Sviňovice	k1gFnPc7
•	•	k?
Pod	pod	k7c7
Vyhlídkou	vyhlídka	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Vyhlídkou	vyhlídka	k1gFnSc7
II	II	kA
•	•	k?
Podhájí	Podhájí	k1gNnSc2
•	•	k?
Polední	polední	k2eAgFnSc1d1
•	•	k?
Polučí	Polučí	k1gFnSc1
•	•	k?
Poušť	poušť	k1gFnSc1
•	•	k?
Skalka	skalka	k1gFnSc1
•	•	k?
Stádla	Stádla	k1gFnPc2
•	•	k?
Štěrbů	Štěrba	k1gMnPc2
louka	louka	k1gFnSc1
•	•	k?
Tisy	Tisa	k1gFnSc2
u	u	k7c2
Chrobol	Chrobola	k1gFnPc2
•	•	k?
U	u	k7c2
Narovců	Narovec	k1gInPc2
•	•	k?
U	u	k7c2
Piláta	Pilát	k1gMnSc2
•	•	k?
U	u	k7c2
poustevníka	poustevník	k1gMnSc2
•	•	k?
Úbislav	Úbislav	k1gMnSc1
•	•	k?
Upolíny	upolín	k1gInPc1
•	•	k?
V	v	k7c6
polích	pole	k1gNnPc6
•	•	k?
Vraniště	Vraniště	k1gNnSc2
•	•	k?
Vyšný	vyšný	k2eAgInSc1d1
–	–	k?
Křišťanov	Křišťanov	k1gInSc1
•	•	k?
Zábrdská	Zábrdský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Žďárecká	žďárecký	k2eAgFnSc1d1
slať	slať	k1gFnSc1
•	•	k?
Žižkova	Žižkův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
