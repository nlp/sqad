<s>
Deus	Deus	k6eAd1
Ex	ex	k6eAd1
</s>
<s>
Deus	Deus	k6eAd1
ExVývojářIon	ExVývojářIon	k1gMnSc1
StormVydavatelEidos	StormVydavatelEidos	k1gMnSc1
InteractiveRežisérWarren	InteractiveRežisérWarrna	k1gFnPc2
SpectorProducentWarren	SpectorProducentWarrna	k1gFnPc2
SpectoesignérHarvey	SpectoesignérHarvea	k1gFnSc2
SmithProgramátorChris	SmithProgramátorChris	k1gFnSc2
NordenVýtvarníkJay	NordenVýtvarníkJaa	k1gFnSc2
LeeScenáristéSheldon	LeeScenáristéSheldon	k1gMnSc1
PacottiChris	PacottiChris	k1gFnPc2
ToddAustin	ToddAustin	k1gMnSc1
GrossmanSkladateléAlexander	GrossmanSkladateléAlexander	k1gMnSc1
BrandonPan	BrandonPan	k1gMnSc1
GardopéeMichiel	GardopéeMichiel	k1gMnSc1
van	vana	k1gFnPc2
den	den	k1gInSc4
BosHerní	BosHerní	k2eAgMnSc1d1
sérieDeus	sérieDeus	k1gMnSc1
ExEngineUnreal	ExEngineUnreal	k1gMnSc1
EnginePlatformyMicrosoft	EnginePlatformyMicrosoft	k2eAgInSc4d1
WindowsMac	WindowsMac	k1gInSc4
OSPlayStation	OSPlayStation	k1gInSc4
2	#num#	k4
<g/>
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2000	#num#	k4
<g/>
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2000	#num#	k4
světMac	světMac	k1gInSc1
OS	OS	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2000	#num#	k4
SAPlayStation	SAPlayStation	k1gInSc1
226	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
SA	SA	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2002	#num#	k4
EUŽánryFPSakční	EUŽánryFPSakční	k2eAgFnSc1d1
RPGstealthHerní	RPGstealthHerní	k2eAgFnSc1d1
módysingleplayermultiplayerKlasifikacePEGI	módysingleplayermultiplayerKlasifikacePEGI	k?
ESRB	ESRB	kA
Posloupnost	posloupnost	k1gFnSc1
</s>
<s>
Deus	Deus	k6eAd1
Ex	ex	k6eAd1
<g/>
:	:	kIx,
Invisible	Invisible	k1gFnSc1
War	War	k1gFnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Deus	Deus	k1gInSc1
Ex	ex	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
DX	DX	kA
<g/>
,	,	kIx,
s	s	k7c7
výslovností	výslovnost	k1gFnSc7
[	[	kIx(
<g/>
ˌ	ˌ	kIx~
<g/>
.	.	kIx.
<g/>
ə	ə	kA
ˈ	ˈ	kIx~
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kyberpunková	kyberpunková	k1gFnSc1
<g/>
,	,	kIx,
akčně	akčně	k6eAd1
laděná	laděný	k2eAgFnSc1d1
RPG	RPG	kA
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
z	z	k7c2
vlastního	vlastní	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
<g/>
,	,	kIx,
vyvinutá	vyvinutý	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
Ion	ion	k1gInSc1
Storm	Storm	k1gInSc1
a	a	k8xC
vydaná	vydaný	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
proslulá	proslulý	k2eAgNnPc4d1
především	především	k6eAd1
velkou	velký	k2eAgFnSc7d1
variabilitou	variabilita	k1gFnSc7
v	v	k7c6
možnostech	možnost	k1gFnPc6
jak	jak	k8xS,k8xC
zakončit	zakončit	k5eAaPmF
danou	daný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
silným	silný	k2eAgInSc7d1
příběhem	příběh	k1gInSc7
a	a	k8xC
cenami	cena	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
obdržela	obdržet	k5eAaPmAgFnS
(	(	kIx(
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
odbornou	odborný	k2eAgFnSc7d1
kritikou	kritika	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
širší	široký	k2eAgFnSc7d2
hráčskou	hráčský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
patří	patřit	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
„	„	k?
<g/>
nejlepší	dobrý	k2eAgInSc1d3
herní	herní	k2eAgInSc1d1
počin	počin	k1gInSc1
roku	rok	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
také	také	k9
„	„	k?
<g/>
nejlepší	dobrý	k2eAgFnSc1d3
PC	PC	kA
hra	hra	k1gFnSc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
“	“	k?
v	v	k7c6
žebříčku	žebříček	k1gInSc6
podle	podle	k7c2
magazínu	magazín	k1gInSc2
PC	PC	kA
Gamer	Gamer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
designérů	designér	k1gMnPc2
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
Warren	Warrna	k1gFnPc2
Spector	Spector	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
tvůrce	tvůrce	k1gMnSc1
her	hra	k1gFnPc2
(	(	kIx(
<g/>
Ultima	ultimo	k1gNnPc1
Underworld	Underworlda	k1gFnPc2
<g/>
,	,	kIx,
System	Systo	k1gNnSc7
Shock	Shocka	k1gFnPc2
<g/>
,	,	kIx,
Thief	Thief	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Deus	Deus	k6eAd1
Ex	ex	k6eAd1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
z	z	k7c2
blízké	blízký	k2eAgFnSc2d1
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
odehrávající	odehrávající	k2eAgMnSc1d1
se	se	k3xPyFc4
ve	v	k7c6
světě	svět	k1gInSc6
plném	plný	k2eAgInSc6d1
násilí	násilí	k1gNnSc3
<g/>
,	,	kIx,
korupce	korupce	k1gFnSc1
<g/>
,	,	kIx,
vládních	vládní	k2eAgNnPc2d1
spiknutí	spiknutí	k1gNnPc2
<g/>
,	,	kIx,
smrtících	smrtící	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
a	a	k8xC
zločinu	zločin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
vžije	vžít	k5eAaPmIp3nS
do	do	k7c2
postavy	postava	k1gFnSc2
se	s	k7c7
jménem	jméno	k1gNnSc7
JC	JC	kA
Denton	Denton	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
agentem	agent	k1gMnSc7
organizace	organizace	k1gFnSc2
UNATCO	UNATCO	kA
<g/>
,	,	kIx,
mezinárodního	mezinárodní	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
bojujícího	bojující	k2eAgMnSc2d1
proti	proti	k7c3
organizovanému	organizovaný	k2eAgInSc3d1
zločinu	zločin	k1gInSc3
a	a	k8xC
terorismu	terorismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denton	Denton	k1gInSc1
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
nanoimplantáty	nanoimplantát	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
mu	on	k3xPp3gMnSc3
propůjčují	propůjčovat	k5eAaImIp3nP
nadlidské	nadlidský	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
variabilních	variabilní	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
postupu	postup	k1gInSc2
–	–	k?
užití	užití	k1gNnSc2
hrubé	hrubý	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
tiché	tichý	k2eAgNnSc1d1
plížení	plížení	k1gNnSc1
nebo	nebo	k8xC
hackování	hackování	k1gNnSc1
počítačových	počítačový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Herní	herní	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Deus	Deus	k6eAd1
Ex	ex	k6eAd1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2052	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
dystopickém	dystopický	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
inspirována	inspirovat	k5eAaBmNgFnS
populárními	populární	k2eAgFnPc7d1
konspiračními	konspirační	k2eAgFnPc7d1
teoriemi	teorie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Temné	temný	k2eAgNnSc1d1
pozadí	pozadí	k1gNnSc1
je	být	k5eAaImIp3nS
ještě	ještě	k6eAd1
zdůrazněno	zdůraznit	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgFnPc1
epizody	epizoda	k1gFnPc1
hry	hra	k1gFnSc2
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
pomalu	pomalu	k6eAd1
klesá	klesat	k5eAaImIp3nS
do	do	k7c2
chaosu	chaos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populaci	populace	k1gFnSc3
ničí	ničí	k3xOyNgFnSc1
celosvětová	celosvětový	k2eAgFnSc1d1
pandemie	pandemie	k1gFnSc1
tzv.	tzv.	kA
„	„	k?
<g/>
šedé	šedý	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntetická	syntetický	k2eAgFnSc1d1
vakcína	vakcína	k1gFnSc1
Ambrosia	ambrosia	k1gFnSc1
vyráběná	vyráběný	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
VersaLife	VersaLif	k1gInSc5
sice	sice	k8xC
ruší	rušit	k5eAaImIp3nS
účinky	účinek	k1gInPc4
viru	vir	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	on	k3xPp3gMnPc4
jí	jíst	k5eAaImIp3nS
zoufalý	zoufalý	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ambrosia	ambrosia	k1gFnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pouze	pouze	k6eAd1
jedincům	jedinec	k1gMnPc3
„	„	k?
<g/>
nezbytným	zbytný	k2eNgNnSc7d1,k2eAgNnSc7d1
pro	pro	k7c4
udržení	udržení	k1gNnSc4
společenského	společenský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
dostává	dostávat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
především	především	k9
k	k	k7c3
vládním	vládní	k2eAgMnPc3d1
úředníkům	úředník	k1gMnPc3
<g/>
,	,	kIx,
vojákům	voják	k1gMnPc3
<g/>
,	,	kIx,
vědcům	vědec	k1gMnPc3
<g/>
,	,	kIx,
intelektuálním	intelektuální	k2eAgFnPc3d1
elitám	elita	k1gFnPc3
a	a	k8xC
boháčům	boháč	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
nepokoje	nepokoj	k1gInPc1
a	a	k8xC
vzniká	vznikat	k5eAaImIp3nS
mnoho	mnoho	k4c1
teroristických	teroristický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
s	s	k7c7
proklamovaným	proklamovaný	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
pomoci	pomoc	k1gFnSc2
utlačovaným	utlačovaný	k1gMnPc3
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
NSF	NSF	kA
(	(	kIx(
<g/>
National	National	k1gMnSc1
Secessionist	Secessionist	k1gMnSc1
Force	force	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
USA	USA	kA
a	a	k8xC
Silhouette	Silhouett	k1gInSc5
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
boje	boj	k1gInSc2
proti	proti	k7c3
těmto	tento	k3xDgFnPc3
hrozbám	hrozba	k1gFnPc3
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
výrazně	výrazně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
celosvětový	celosvětový	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
UNATCO	UNATCO	kA
(	(	kIx(
<g/>
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Anti-Terrorist	Anti-Terrorist	k1gInSc1
Coalition	Coalition	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
posláním	poslání	k1gNnSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
globální	globální	k2eAgNnSc4d1
zachování	zachování	k1gNnSc4
míru	míra	k1gFnSc4
a	a	k8xC
boj	boj	k1gInSc4
proti	proti	k7c3
teroristickým	teroristický	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNATCO	UNATCO	kA
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
podzemní	podzemní	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
na	na	k7c4
Liberty	Libert	k1gInPc4
Islandu	Island	k1gInSc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
zbytků	zbytek	k1gInPc2
Sochy	Socha	k1gMnSc2
Svobody	Svoboda	k1gMnSc2
zničené	zničený	k2eAgNnSc1d1
teroristickým	teroristický	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
ujímá	ujímat	k5eAaImIp3nS
role	role	k1gFnSc1
JC	JC	kA
Dentona	Dentona	k1gFnSc1
<g/>
,	,	kIx,
nanotechnologicky	nanotechnologicky	k6eAd1
vylepšeného	vylepšený	k2eAgMnSc4d1
agenta	agent	k1gMnSc4
společnosti	společnost	k1gFnSc2
UNATCO	UNATCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
tréninku	trénink	k1gInSc2
jej	on	k3xPp3gMnSc4
ředitel	ředitel	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Manderley	Manderlea	k1gFnSc2
pověřuje	pověřovat	k5eAaImIp3nS
splněním	splnění	k1gNnSc7
několika	několik	k4yIc2
úkolů	úkol	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
cíl	cíl	k1gInSc4
nalezení	nalezení	k1gNnSc2
členů	člen	k1gMnPc2
NSF	NSF	kA
(	(	kIx(
<g/>
National	National	k1gMnSc1
Secessionist	Secessionist	k1gMnSc1
Forces	Forces	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
vystopování	vystopování	k1gNnSc1
odcizené	odcizený	k2eAgFnSc2d1
vakcíny	vakcína	k1gFnSc2
„	„	k?
<g/>
Ambrosia	ambrosia	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
jediného	jediný	k2eAgInSc2d1
léku	lék	k1gInSc2
na	na	k7c4
virus	virus	k1gInSc4
„	„	k?
<g/>
Gray	Graa	k1gFnSc2
Death	Death	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
JC	JC	kA
nachází	nacházet	k5eAaImIp3nS
stopy	stopa	k1gFnPc4
nákladu	náklad	k1gInSc2
s	s	k7c7
lékem	lék	k1gInSc7
u	u	k7c2
soukromého	soukromý	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
letiště	letiště	k1gNnSc2
LaGuardia	LaGuardium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Paulem	Paul	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
také	také	k9
vybaven	vybaven	k2eAgInSc1d1
nanotechnologickým	nanotechnologický	k2eAgNnSc7d1
vylepšením	vylepšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
zde	zde	k6eAd1
dovídá	dovídat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Paul	Paul	k1gMnSc1
přešel	přejít	k5eAaPmAgMnS
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
ke	k	k7c3
skupině	skupina	k1gFnSc3
NSF	NSF	kA
<g/>
)	)	kIx)
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
virus	virus	k1gInSc1
Death	Death	k1gInSc1
Gray	Graa	k1gFnSc2
byl	být	k5eAaImAgInS
uměle	uměle	k6eAd1
vytvořen	vytvořit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNATCO	UNATCO	kA
se	se	k3xPyFc4
díky	díky	k7c3
svému	svůj	k3xOyFgNnSc3
silnému	silný	k2eAgNnSc3d1
postavení	postavení	k1gNnSc3
snaží	snažit	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lék	lék	k1gInSc1
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
rukou	ruka	k1gFnPc2
pouze	pouze	k6eAd1
hrstce	hrstka	k1gFnSc3
vyvolených	vyvolená	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
ústředí	ústředí	k1gNnSc2
centrály	centrála	k1gFnSc2
UNATCO	UNATCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
konfrontován	konfrontovat	k5eAaBmNgInS
ředitelem	ředitel	k1gMnSc7
Manderleyem	Manderley	k1gMnSc7
se	s	k7c7
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc3
bratrovi	bratr	k1gMnSc3
byl	být	k5eAaImAgInS
aktivován	aktivován	k2eAgInSc1d1
24	#num#	k4
<g/>
hodinový	hodinový	k2eAgInSc1d1
kill	kill	k1gInSc1
switch	switcha	k1gFnPc2
za	za	k7c4
jeho	jeho	k3xOp3gFnSc4
zradu	zrada	k1gFnSc4
proti	proti	k7c3
UNATCO	UNATCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manderley	Manderle	k1gMnPc7
přikáže	přikázat	k5eAaPmIp3nS
JC	JC	kA
odletět	odletět	k5eAaPmF
do	do	k7c2
Hongkongu	Hongkong	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zlikvidovat	zlikvidovat	k5eAaPmF
hackera	hacker	k1gMnSc2
Tracera	Tracer	k1gMnSc2
Tonga	Tong	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
Paul	Paul	k1gMnSc1
předtím	předtím	k6eAd1
kontaktoval	kontaktovat	k5eAaImAgMnS
a	a	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vyřadit	vyřadit	k5eAaPmF
z	z	k7c2
činnosti	činnost	k1gFnSc2
kill	kill	k1gMnSc1
switch	switch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
vyhledat	vyhledat	k5eAaPmF
Paula	Paula	k1gFnSc1
v	v	k7c6
bytě	byt	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ukrývá	ukrývat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paul	Paul	k1gMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vysvětlit	vysvětlit	k5eAaPmF
své	svůj	k3xOyFgInPc4
důvody	důvod	k1gInPc4
ke	k	k7c3
zběhnutí	zběhnutí	k1gNnSc3
a	a	k8xC
přesvědčí	přesvědčit	k5eAaPmIp3nS
JC	JC	kA
k	k	k7c3
vyslání	vyslání	k1gNnSc3
nouzového	nouzový	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
varovat	varovat	k5eAaImF
NSF	NSF	kA
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vyslání	vyslání	k1gNnSc6
signálu	signál	k1gInSc2
se	se	k3xPyFc4
JC	JC	kA
dostavá	dostavý	k2eAgNnPc4d1
do	do	k7c2
hledáčku	hledáček	k1gInSc2
UNATCO	UNATCO	kA
a	a	k8xC
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Paulovi	Paul	k1gMnSc3
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
aktivován	aktivovat	k5eAaBmNgInS
kill	kill	k1gInSc1
switch	switcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
aktivaci	aktivace	k1gFnSc6
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
osobně	osobně	k6eAd1
ředitel	ředitel	k1gMnSc1
společnosti	společnost	k1gFnSc2
FEMA	FEMA	kA
(	(	kIx(
<g/>
Federal	Federal	k1gMnSc1
Emergency	Emergenca	k1gFnSc2
Management	management	k1gInSc1
Agency	Agenca	k1gFnSc2
<g/>
)	)	kIx)
Walton	Walton	k1gInSc1
Simons	Simonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
dopadeni	dopaden	k2eAgMnPc1d1
agenty	agent	k1gMnPc7
UNATCO	UNATCO	kA
a	a	k8xC
umístěni	umístěn	k2eAgMnPc1d1
do	do	k7c2
tajného	tajný	k2eAgNnSc2d1
vězení	vězení	k1gNnSc2
vybudovaného	vybudovaný	k2eAgNnSc2d1
pod	pod	k7c7
centrálou	centrála	k1gFnSc7
UNATCO	UNATCO	kA
<g/>
.	.	kIx.
</s>
<s>
Entita	entita	k1gFnSc1
jménem	jméno	k1gNnSc7
Daedalus	Daedalus	k1gInSc4
kontaktuje	kontaktovat	k5eAaImIp3nS
JC	JC	kA
a	a	k8xC
informuje	informovat	k5eAaBmIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
vězení	vězení	k1gNnSc1
je	být	k5eAaImIp3nS
vlastnictvím	vlastnictví	k1gNnSc7
skupiny	skupina	k1gFnSc2
Majestic	Majestice	k1gFnPc2
12	#num#	k4
a	a	k8xC
pomůže	pomoct	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
utéci	utéct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
a	a	k8xC
Paul	Paul	k1gMnSc1
poté	poté	k6eAd1
odlétají	odlétat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
Hongkongu	Hongkong	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jim	on	k3xPp3gMnPc3
Tracer	Tracer	k1gInSc1
Tong	Tong	k1gInSc4
deaktivuje	deaktivovat	k5eAaImIp3nS
kill	kill	k1gMnSc1
switch	switch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
je	být	k5eAaImIp3nS
požádán	požádat	k5eAaPmNgMnS
o	o	k7c4
infiltraci	infiltrace	k1gFnSc4
budovy	budova	k1gFnSc2
společnosti	společnost	k1gFnSc2
VersaLife	VersaLif	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
svého	svůj	k3xOyFgInSc2
průzkumu	průzkum	k1gInSc2
zjišťuje	zjišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
virus	virus	k1gInSc1
Gray	Graa	k1gFnSc2
Death	Deatha	k1gFnPc2
vytvořila	vytvořit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
VersaLife	VersaLif	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukradne	ukradnout	k5eAaPmIp3nS
plány	plán	k1gInPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
viru	vir	k1gInSc2
a	a	k8xC
podaří	podařit	k5eAaPmIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zničit	zničit	k5eAaPmF
zařízení	zařízení	k1gNnSc4
(	(	kIx(
<g/>
UC	UC	kA
–	–	k?
universal	universat	k5eAaImAgMnS,k5eAaPmAgMnS
constructor	constructor	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analýzou	analýza	k1gFnSc7
plánů	plán	k1gInPc2
viru	vir	k1gInSc2
je	být	k5eAaImIp3nS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
vývojem	vývoj	k1gInSc7
stojí	stát	k5eAaImIp3nS
uskupení	uskupení	k1gNnSc1
Iluminátů	iluminát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Tong	Tong	k1gInSc1
přesvědčí	přesvědčit	k5eAaPmIp3nS
JC	JC	kA
k	k	k7c3
návštěvě	návštěva	k1gFnSc3
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
kontaktovat	kontaktovat	k5eAaImF
Ilumináty	iluminát	k1gMnPc4
a	a	k8xC
přesvědčit	přesvědčit	k5eAaPmF
je	být	k5eAaImIp3nS
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Majestic	Majestice	k1gFnPc2
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Paříži	Paříž	k1gFnSc6
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
vůdcem	vůdce	k1gMnSc7
Iluminátů	iluminát	k1gMnPc2
Morgenem	Morgen	k1gInSc7
Everettem	Everett	k1gMnSc7
<g/>
,	,	kIx,
od	od	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
dozvídá	dozvídat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
virus	virus	k1gInSc1
Gray	Graa	k1gFnSc2
Death	Death	k1gMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
původně	původně	k6eAd1
augmentačním	augmentační	k2eAgNnSc7d1
vylepšením	vylepšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bilionář	Bilionář	k1gMnSc1
<g/>
,	,	kIx,
vůdce	vůdce	k1gMnSc1
Majestic	Majestice	k1gFnPc2
12	#num#	k4
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Iluminátů	iluminát	k1gMnPc2
Bob	Bob	k1gMnSc1
Page	Pag	k1gInSc2
tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
ukradl	ukradnout	k5eAaPmAgMnS
a	a	k8xC
pozměnil	pozměnit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
na	na	k7c4
smrtící	smrtící	k2eAgInSc4d1
virus	virus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zničení	zničení	k1gNnSc6
UC	UC	kA
ve	v	k7c6
společnosti	společnost	k1gFnSc6
VersaLife	VersaLif	k1gInSc5
je	být	k5eAaImIp3nS
Everettovi	Everett	k1gMnSc3
jasné	jasný	k2eAgFnSc2d1
<g/>
,	,	kIx,
že	že	k8xS
jedinou	jediný	k2eAgFnSc7d1
šancí	šance	k1gFnSc7
Majestic	Majestice	k1gFnPc2
12	#num#	k4
na	na	k7c4
další	další	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
viru	vir	k1gInSc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc4
Vandenbergovy	Vandenbergův	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
skupina	skupina	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
X-	X-	k1gFnSc1
<g/>
51	#num#	k4
<g/>
,	,	kIx,
složená	složený	k2eAgFnSc1d1
z	z	k7c2
bývalých	bývalý	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
z	z	k7c2
oblasti	oblast	k1gFnSc2
51	#num#	k4
<g/>
,	,	kIx,
postavila	postavit	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
UC	UC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
vůdcem	vůdce	k1gMnSc7
X-51	X-51	k1gFnSc2
Gary	Gara	k1gFnSc2
Savagem	Savag	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
odhalí	odhalit	k5eAaPmIp3nS
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Daedalus	Daedalus	k1gInSc1
je	být	k5eAaImIp3nS
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
programu	program	k1gInSc2
Echelon	Echelon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Everett	Everett	k1gInSc1
ve	v	k7c6
snaze	snaha	k1gFnSc6
získat	získat	k5eAaPmF
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
komunikační	komunikační	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
Majestic	Majestice	k1gFnPc2
12	#num#	k4
vypustí	vypustit	k5eAaPmIp3nS
Daedala	Daedal	k1gMnSc4
do	do	k7c2
vojenské	vojenský	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protiúder	protiúder	k1gInSc1
od	od	k7c2
Boba	Bob	k1gMnSc2
Page	Page	k1gFnSc4
na	na	k7c4
sebe	sebe	k3xPyFc4
nenechává	nechávat	k5eNaImIp3nS
dlouho	dlouho	k6eAd1
čekat	čekat	k5eAaImF
<g/>
:	:	kIx,
Page	Page	k1gInSc1
vypouští	vypouštět	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
umělou	umělý	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
–	–	k?
Icarus	Icarus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daedalus	Daedalus	k1gInSc4
a	a	k8xC
Icarus	Icarus	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
spojují	spojovat	k5eAaImIp3nP
do	do	k7c2
nové	nový	k2eAgFnSc2d1
formy	forma	k1gFnSc2
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
–	–	k?
Helios	Helios	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
kontrolovat	kontrolovat	k5eAaImF
všechny	všechen	k3xTgFnPc4
komunikační	komunikační	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Savage	Savag	k1gInSc2
žádá	žádat	k5eAaImIp3nS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
JC	JC	kA
se	s	k7c7
získáním	získání	k1gNnSc7
plánů	plán	k1gInPc2
k	k	k7c3
rekonstrukci	rekonstrukce	k1gFnSc3
zařízení	zařízení	k1gNnSc2
UC	UC	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
poškozeno	poškodit	k5eAaPmNgNnS
během	během	k7c2
přepadení	přepadení	k1gNnSc2
Vanderbergovy	Vanderbergův	k2eAgFnSc2d1
základny	základna	k1gFnSc2
skupinou	skupina	k1gFnSc7
Majestic	Majestice	k1gFnPc2
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
nachází	nacházet	k5eAaImIp3nS
schémata	schéma	k1gNnPc4
a	a	k8xC
elektronicky	elektronicky	k6eAd1
je	on	k3xPp3gInPc4
posílá	posílat	k5eAaImIp3nS
Savageovi	Savageus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Bob	Bob	k1gMnSc1
Page	Pag	k1gFnSc2
přenos	přenos	k1gInSc1
zachytí	zachytit	k5eAaPmIp3nS
a	a	k8xC
jako	jako	k9
protiúder	protiúder	k1gInSc1
vypouští	vypouštět	k5eAaImIp3nS
jadernou	jaderný	k2eAgFnSc4d1
střelu	střela	k1gFnSc4
na	na	k7c4
Vanderbergovu	Vanderbergův	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničením	zničení	k1gNnSc7
základny	základna	k1gFnSc2
by	by	kYmCp3nS
zajistil	zajistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
oblast	oblast	k1gFnSc1
51	#num#	k4
(	(	kIx(
<g/>
současné	současný	k2eAgNnSc4d1
velitelství	velitelství	k1gNnSc4
Majestic	Majestice	k1gFnPc2
12	#num#	k4
<g/>
)	)	kIx)
bude	být	k5eAaImBp3nS
jediné	jediný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
s	s	k7c7
funkčním	funkční	k2eAgNnSc7d1
UC	UC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
podaří	podařit	k5eAaPmIp3nS
přeprogramovat	přeprogramovat	k5eAaPmF
řízenou	řízený	k2eAgFnSc4d1
střelu	střela	k1gFnSc4
a	a	k8xC
navede	navést	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
na	na	k7c4
oblast	oblast	k1gFnSc4
51	#num#	k4
<g/>
,	,	kIx,
kam	kam	k6eAd1
později	pozdě	k6eAd2
odjíždí	odjíždět	k5eAaImIp3nS
ke	k	k7c3
konfrontaci	konfrontace	k1gFnSc3
s	s	k7c7
Pagem	Pag	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
nachází	nacházet	k5eAaImIp3nS
Page	Page	k1gInSc4
a	a	k8xC
ten	ten	k3xDgInSc4
mu	on	k3xPp3gMnSc3
prozrazuje	prozrazovat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
plán	plán	k1gInSc4
na	na	k7c4
spojení	spojení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
osoby	osoba	k1gFnSc2
s	s	k7c7
umělou	umělý	k2eAgFnSc7d1
inteligenci	inteligence	k1gFnSc3
Helios	Helios	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojením	spojení	k1gNnSc7
by	by	kYmCp3nS
Page	Page	k1gNnSc4
získal	získat	k5eAaPmAgMnS
úplnou	úplný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
všemi	všecek	k3xTgFnPc7
nanotechnologiemi	nanotechnologie	k1gFnPc7
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
doslova	doslova	k6eAd1
bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
JC	JC	kA
je	být	k5eAaImIp3nS
skrze	skrze	k?
infolink	infolink	k1gInSc1
kontaktován	kontaktovat	k5eAaImNgInS
Tongem	Tongo	k1gNnSc7
<g/>
,	,	kIx,
Everettem	Everetto	k1gNnSc7
a	a	k8xC
Heliosem	Helios	k1gInSc7
s	s	k7c7
prosbou	prosba	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Pageovi	Pageus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
JC	JC	kA
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
komu	kdo	k3yQnSc3,k3yInSc3,k3yRnSc3
pomůže	pomoct	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tracer	Tracer	k1gMnSc1
Tong	Tong	k1gMnSc1
žádá	žádat	k5eAaImIp3nS
o	o	k7c6
zničení	zničení	k1gNnSc6
globálního	globální	k2eAgNnSc2d1
komunikačního	komunikační	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
–	–	k?
důsledkem	důsledek	k1gInSc7
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
návrat	návrat	k1gInSc4
světa	svět	k1gInSc2
do	do	k7c2
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morgan	morgan	k1gMnSc1
Everett	Everett	k1gMnSc1
nabízí	nabízet	k5eAaImIp3nP
Dentonovi	Dentonův	k2eAgMnPc1d1
šanci	šance	k1gFnSc4
získat	získat	k5eAaPmF
zpět	zpět	k6eAd1
zašlou	zašlý	k2eAgFnSc4d1
moc	moc	k1gFnSc4
a	a	k8xC
slávu	sláva	k1gFnSc4
Iluminátů	iluminát	k1gMnPc2
pomoci	pomoc	k1gFnSc2
technologií	technologie	k1gFnSc7
oblasti	oblast	k1gFnSc2
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
by	by	kYmCp3nP
Ilumináti	iluminát	k1gMnPc1
mohli	moct	k5eAaImAgMnP
vládnout	vládnout	k5eAaImF
světu	svět	k1gInSc3
neviditelnou	viditelný	k2eNgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helios	Helios	k1gMnSc1
žádá	žádat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
spojit	spojit	k5eAaPmF
s	s	k7c7
Dentonem	Denton	k1gInSc7
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
spojení	spojení	k1gNnSc6
vykonávat	vykonávat	k5eAaImF
vládu	vláda	k1gFnSc4
nad	nad	k7c7
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěr	závěr	k1gInSc1
příběhu	příběh	k1gInSc2
a	a	k8xC
budoucnost	budoucnost	k1gFnSc1
světa	svět	k1gInSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
hráčovu	hráčův	k2eAgFnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Zbraně	zbraň	k1gFnPc1
a	a	k8xC
vybavení	vybavení	k1gNnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
k	k	k7c3
bohatému	bohatý	k2eAgInSc3d1
arzenálu	arzenál	k1gInSc3
střelných	střelný	k2eAgFnPc2d1
<g/>
,	,	kIx,
chladných	chladný	k2eAgFnPc2d1
<g/>
,	,	kIx,
nesmrtících	smrtící	k2eNgFnPc2d1
i	i	k8xC
značně	značně	k6eAd1
kuriózních	kuriózní	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
hasicího	hasicí	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
obsah	obsah	k1gInSc1
může	moct	k5eAaImIp3nS
nastříkat	nastříkat	k5eAaPmF
do	do	k7c2
obličeje	obličej	k1gInSc2
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
meč	meč	k1gInSc4
s	s	k7c7
čepelí	čepel	k1gFnSc7
z	z	k7c2
nanobotů	nanobot	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
získat	získat	k5eAaPmF
mnoho	mnoho	k6eAd1
podpůrného	podpůrný	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
(	(	kIx(
<g/>
lékárničky	lékárnička	k1gFnSc2
<g/>
,	,	kIx,
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
alkohol	alkohol	k1gInSc1
<g/>
,	,	kIx,
baterie	baterie	k1gFnSc1
pro	pro	k7c4
nanoimplantáty	nanoimplantát	k1gInPc4
<g/>
,	,	kIx,
paklíč	paklíč	k1gInSc4
<g/>
,	,	kIx,
nástroje	nástroj	k1gInPc4
na	na	k7c4
vyřazení	vyřazení	k1gNnSc4
elektroniky	elektronika	k1gFnSc2
<g/>
,	,	kIx,
modifikace	modifikace	k1gFnSc2
zbraní	zbraň	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
přečíst	přečíst	k5eAaPmF
literární	literární	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
a	a	k8xC
dokumenty	dokument	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
objasňují	objasňovat	k5eAaImIp3nP
pozadí	pozadí	k1gNnSc4
příběhu	příběh	k1gInSc2
a	a	k8xC
poskytují	poskytovat	k5eAaImIp3nP
informace	informace	k1gFnPc4
usnadňující	usnadňující	k2eAgInSc4d1
další	další	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kapitolu	kapitola	k1gFnSc4
samu	sám	k3xTgFnSc4
o	o	k7c6
sobě	sebe	k3xPyFc6
tvoří	tvořit	k5eAaImIp3nS
nanoimplantáty	nanoimplantát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
získat	získat	k5eAaPmF
formou	forma	k1gFnSc7
odměny	odměna	k1gFnSc2
<g/>
,	,	kIx,
ukrást	ukrást	k5eAaPmF
v	v	k7c6
nepřátelských	přátelský	k2eNgFnPc6d1
základnách	základna	k1gFnPc6
<g/>
,	,	kIx,
anebo	anebo	k8xC
koupit	koupit	k5eAaPmF
od	od	k7c2
překupníků	překupník	k1gMnPc2
na	na	k7c6
černém	černý	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nanoimplantáty	Nanoimplantát	k1gInPc7
jsou	být	k5eAaImIp3nP
instalovány	instalován	k2eAgInPc1d1
pomocí	pomocí	k7c2
lékařských	lékařský	k2eAgInPc2d1
robotů	robot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
robota	robot	k1gMnSc2
funguje	fungovat	k5eAaImIp3nS
i	i	k9
jako	jako	k9
léčebný	léčebný	k2eAgInSc1d1
automat	automat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiný	jiný	k2eAgInSc1d1
typ	typ	k1gInSc1
robota	robot	k1gMnSc2
dočerpává	dočerpávat	k5eAaImIp3nS
energii	energie	k1gFnSc4
do	do	k7c2
implantátů	implantát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nepřátelé	nepřítel	k1gMnPc1
</s>
<s>
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
dočká	dočkat	k5eAaPmIp3nS
bojů	boj	k1gInPc2
s	s	k7c7
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
teroristů	terorista	k1gMnPc2
a	a	k8xC
drobných	drobný	k2eAgMnPc2d1
zlodějíčků	zlodějíček	k1gMnPc2
<g/>
,	,	kIx,
po	po	k7c4
přestřelky	přestřelka	k1gFnPc4
s	s	k7c7
policií	policie	k1gFnSc7
<g/>
,	,	kIx,
armádou	armáda	k1gFnSc7
a	a	k8xC
členy	člen	k1gMnPc7
tajemné	tajemný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Majestic	Majestice	k1gFnPc2
12	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
prošli	projít	k5eAaPmAgMnP
tělesnými	tělesný	k2eAgFnPc7d1
modifikacemi	modifikace	k1gFnPc7
a	a	k8xC
mají	mít	k5eAaImIp3nP
podobné	podobný	k2eAgFnPc4d1
nadlidské	nadlidský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
jako	jako	k8xC,k8xS
JC	JC	kA
Denton	Denton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
mají	mít	k5eAaImIp3nP
také	také	k9
nanoimplantáty	nanoimplantát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
byli	být	k5eAaImAgMnP
posíleni	posílit	k5eAaPmNgMnP
starou	starý	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	on	k3xPp3gFnPc4
zohyzdila	zohyzdit	k5eAaPmAgFnS
množstvím	množství	k1gNnSc7
kovových	kovový	k2eAgInPc2d1
implantátů	implantát	k1gInPc2
<g/>
,	,	kIx,
kabelů	kabel	k1gInPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
viditelných	viditelný	k2eAgFnPc2d1
součástek	součástka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnSc3
z	z	k7c2
Majestic	Majestice	k1gFnPc2
12	#num#	k4
byli	být	k5eAaImAgMnP
posíleni	posílen	k2eAgMnPc1d1
pomocí	pomocí	k7c2
léků	lék	k1gInPc2
a	a	k8xC
jedů	jed	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
u	u	k7c2
nich	on	k3xPp3gInPc2
vyvolaly	vyvolat	k5eAaPmAgFnP
speciální	speciální	k2eAgFnPc4d1
cílené	cílený	k2eAgFnPc4d1
mutace	mutace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
hrozby	hrozba	k1gFnPc4
patří	patřit	k5eAaImIp3nP
bezpečnostní	bezpečnostní	k2eAgMnPc1d1
roboti	robot	k1gMnPc1
<g/>
,	,	kIx,
mutanti	mutant	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
utekli	utéct	k5eAaPmAgMnP
z	z	k7c2
laboratoří	laboratoř	k1gFnPc2
<g/>
,	,	kIx,
automatické	automatický	k2eAgInPc1d1
bezpečnostní	bezpečnostní	k2eAgInPc1d1
systémy	systém	k1gInPc1
a	a	k8xC
pasti	past	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
dovoluje	dovolovat	k5eAaImIp3nS
také	také	k9
hackování	hackování	k1gNnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
aspekt	aspekt	k1gInSc4
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
vítaným	vítaný	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
pro	pro	k7c4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
rádi	rád	k2eAgMnPc1d1
řeší	řešit	k5eAaImIp3nP
situaci	situace	k1gFnSc4
proplížením	proplížení	k1gNnSc7
a	a	k8xC
záškodnictvím	záškodnictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
například	například	k6eAd1
přeprogramovat	přeprogramovat	k5eAaPmF
automatickou	automatický	k2eAgFnSc4d1
kulometnou	kulometný	k2eAgFnSc4d1
věž	věž	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
útočila	útočit	k5eAaImAgFnS
na	na	k7c4
nepřátele	nepřítel	k1gMnPc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
podobně	podobně	k6eAd1
postupovat	postupovat	k5eAaImF
v	v	k7c6
případě	případ	k1gInSc6
bezpečnostních	bezpečnostní	k2eAgInPc2d1
robotů	robot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
uzpůsobena	uzpůsobit	k5eAaPmNgFnS
i	i	k9
pro	pro	k7c4
styl	styl	k1gInSc4
hraní	hraní	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vyhýbá	vyhýbat	k5eAaImIp3nS
jakékoliv	jakýkoliv	k3yIgFnSc3
konfrontaci	konfrontace	k1gFnSc3
s	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
a	a	k8xC
raději	rád	k6eAd2
volí	volit	k5eAaImIp3nS
tichý	tichý	k2eAgInSc1d1
postup	postup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herní	herní	k2eAgInPc1d1
mechanismy	mechanismus	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
například	například	k6eAd1
sebrat	sebrat	k5eAaPmF
bedýnku	bedýnka	k1gFnSc4
a	a	k8xC
nebo	nebo	k8xC
láhev	láhev	k1gFnSc4
a	a	k8xC
hodit	hodit	k5eAaPmF,k5eAaImF
ji	on	k3xPp3gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
chvíli	chvíle	k1gFnSc6
odlákala	odlákat	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc4
stráží	stráž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
použít	použít	k5eAaPmF
nanoimplantáty	nanoimplantát	k1gInPc4
tlumící	tlumící	k2eAgInPc4d1
zvuk	zvuk	k1gInSc4
chůze	chůze	k1gFnSc2
či	či	k8xC
propůjčující	propůjčující	k2eAgFnSc4d1
neviditelnost	neviditelnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
se	se	k3xPyFc4
lze	lze	k6eAd1
vyhnout	vyhnout	k5eAaPmF
mnoha	mnoho	k4c2
konfrontacím	konfrontace	k1gFnPc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
Deus	Deusa	k1gFnPc2
Ex	ex	k6eAd1
dělá	dělat	k5eAaImIp3nS
i	i	k9
obstojnou	obstojný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
žánru	žánr	k1gInSc2
stealth	stealtha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Deus	Deus	k1gInSc4
Ex	ex	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
fanweb	fanwba	k1gFnPc2
-	-	kIx~
Nyní	nyní	k6eAd1
Expirovaná	Expirovaný	k2eAgFnSc1d1
Doména	doména	k1gFnSc1
</s>
<s>
Překlad	překlad	k1gInSc1
textů	text	k1gInPc2
hry	hra	k1gFnSc2
do	do	k7c2
češtiny	čeština	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4663653-5	4663653-5	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
175432380	#num#	k4
</s>
