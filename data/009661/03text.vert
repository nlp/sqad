<p>
<s>
Maniok	maniok	k1gInSc1	maniok
(	(	kIx(	(
<g/>
Manihot	manihot	k1gInSc1	manihot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
pryšcovitých	pryšcovitý	k2eAgMnPc2d1	pryšcovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Manioky	maniok	k1gInPc1	maniok
jsou	být	k5eAaImIp3nP	být
byliny	bylina	k1gFnPc1	bylina
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
a	a	k8xC	a
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
květenstvím	květenství	k1gNnSc7	květenství
lata	lata	k1gFnSc1	lata
nebo	nebo	k8xC	nebo
hrozen	hrozen	k1gInSc1	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
jedlé	jedlý	k2eAgInPc1d1	jedlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
Maniok	maniok	k1gInSc1	maniok
jedlý	jedlý	k2eAgInSc1d1	jedlý
</s>
</p>
<p>
<s>
Manihot	manihot	k1gInSc1	manihot
glaziovi	glazius	k1gMnSc3	glazius
</s>
</p>
<p>
<s>
Manihot	manihot	k1gInSc1	manihot
grahamii	grahamie	k1gFnSc3	grahamie
</s>
</p>
<p>
<s>
Manihot	manihot	k1gInSc1	manihot
palmata	palma	k1gNnPc5	palma
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
maniok	manioka	k1gFnPc2	manioka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
