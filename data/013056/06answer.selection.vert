<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
trojice	trojice	k1gFnSc1	trojice
paleontologů	paleontolog	k1gMnPc2	paleontolog
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
Emanuel	Emanuel	k1gMnSc1	Emanuel
Tschopp	Tschopp	k1gMnSc1	Tschopp
<g/>
,	,	kIx,	,
Octávio	Octávio	k1gMnSc1	Octávio
Mateus	Mateus	k1gMnSc1	Mateus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Benson	Benson	k1gNnSc1	Benson
provedla	provést	k5eAaPmAgFnS	provést
detailní	detailní	k2eAgInPc4d1	detailní
výzkum	výzkum	k1gInSc4	výzkum
anatomických	anatomický	k2eAgInPc2d1	anatomický
znaků	znak	k1gInPc2	znak
mnoha	mnoho	k4c2	mnoho
sauropodů	sauropod	k1gMnPc2	sauropod
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
jejich	jejich	k3xOp3gFnSc2	jejich
analýzy	analýza	k1gFnSc2	analýza
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
odlišný	odlišný	k2eAgInSc1d1	odlišný
a	a	k8xC	a
samostatný	samostatný	k2eAgInSc1d1	samostatný
rod	rod	k1gInSc1	rod
sauropodního	sauropodní	k2eAgMnSc2d1	sauropodní
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
