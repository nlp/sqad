<p>
<s>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
β	β	k?	β
<g/>
,	,	kIx,	,
brontē	brontē	k?	brontē
=	=	kIx~	=
hrom	hrom	k1gInSc1	hrom
+	+	kIx~	+
σ	σ	k?	σ
<g/>
,	,	kIx,	,
sauros	saurosa	k1gFnPc2	saurosa
=	=	kIx~	=
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
hřmotný	hřmotný	k2eAgMnSc1d1	hřmotný
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
hromový	hromový	k2eAgMnSc1d1	hromový
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
velkého	velký	k2eAgMnSc2d1	velký
sauropodního	sauropodní	k2eAgMnSc2d1	sauropodní
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
období	období	k1gNnSc6	období
svrchní	svrchní	k2eAgFnSc2d1	svrchní
jury	jura	k1gFnSc2	jura
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
155	[number]	k4	155
až	až	k8xS	až
152	[number]	k4	152
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vědecky	vědecky	k6eAd1	vědecky
popsán	popsán	k2eAgInSc1d1	popsán
roku	rok	k1gInSc3	rok
1879	[number]	k4	1879
americkým	americký	k2eAgMnSc7d1	americký
paleontologem	paleontolog	k1gMnSc7	paleontolog
O.	O.	kA	O.
C.	C.	kA	C.
Marshem	Marsh	k1gInSc7	Marsh
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosilie	fosilie	k1gFnSc2	fosilie
z	z	k7c2	z
Albany	Albana	k1gFnSc2	Albana
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
Wyomingu	Wyoming	k1gInSc6	Wyoming
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc4	tento
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vědecky	vědecky	k6eAd1	vědecky
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
paleontolog	paleontolog	k1gMnSc1	paleontolog
Elmer	Elmer	k1gMnSc1	Elmer
Riggs	Riggs	k1gInSc4	Riggs
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
druh	druh	k1gMnSc1	druh
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
excelsus	excelsus	k1gMnSc1	excelsus
má	mít	k5eAaImIp3nS	mít
nápadně	nápadně	k6eAd1	nápadně
mnoho	mnoho	k4c1	mnoho
anatomických	anatomický	k2eAgInPc2d1	anatomický
znaků	znak	k1gInPc2	znak
společných	společný	k2eAgInPc2d1	společný
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
Apatosaurus	Apatosaurus	k1gMnSc1	Apatosaurus
ajax	ajax	k1gInSc1	ajax
<g/>
.	.	kIx.	.
</s>
<s>
Fosilie	fosilie	k1gFnSc1	fosilie
B.	B.	kA	B.
excelsus	excelsus	k1gInSc1	excelsus
tedy	tedy	k9	tedy
přeřadil	přeřadit	k5eAaPmAgInS	přeřadit
pod	pod	k7c4	pod
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
rodu	rod	k1gInSc2	rod
Apatosaurus	Apatosaurus	k1gInSc1	Apatosaurus
-	-	kIx~	-
A.	A.	kA	A.
excelsus	excelsus	k1gInSc1	excelsus
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
pravidel	pravidlo	k1gNnPc2	pravidlo
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
názvosloví	názvosloví	k1gNnSc2	názvosloví
totiž	totiž	k9	totiž
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc7	první
řádně	řádně	k6eAd1	řádně
publikované	publikovaný	k2eAgNnSc4d1	publikované
vědecké	vědecký	k2eAgNnSc4d1	vědecké
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
Apatosaurus	Apatosaurus	k1gInSc1	Apatosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
stále	stále	k6eAd1	stále
používalo	používat	k5eAaImAgNnS	používat
již	již	k6eAd1	již
neplatné	platný	k2eNgNnSc4d1	neplatné
vědecké	vědecký	k2eAgNnSc4d1	vědecké
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
paleontologem	paleontolog	k1gMnSc7	paleontolog
R.	R.	kA	R.
T.	T.	kA	T.
Bakkerem	Bakker	k1gInSc7	Bakker
<g/>
)	)	kIx)	)
označováni	označován	k2eAgMnPc1d1	označován
souhrnně	souhrnně	k6eAd1	souhrnně
všichni	všechen	k3xTgMnPc1	všechen
sauropodní	sauropodní	k2eAgMnPc1d1	sauropodní
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
trojice	trojice	k1gFnSc1	trojice
paleontologů	paleontolog	k1gMnPc2	paleontolog
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
Emanuel	Emanuel	k1gMnSc1	Emanuel
Tschopp	Tschopp	k1gMnSc1	Tschopp
<g/>
,	,	kIx,	,
Octávio	Octávio	k1gMnSc1	Octávio
Mateus	Mateus	k1gMnSc1	Mateus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Benson	Benson	k1gNnSc1	Benson
provedla	provést	k5eAaPmAgFnS	provést
detailní	detailní	k2eAgInPc4d1	detailní
výzkum	výzkum	k1gInSc4	výzkum
anatomických	anatomický	k2eAgInPc2d1	anatomický
znaků	znak	k1gInPc2	znak
mnoha	mnoho	k4c2	mnoho
sauropodů	sauropod	k1gMnPc2	sauropod
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
jejich	jejich	k3xOp3gFnSc2	jejich
analýzy	analýza	k1gFnSc2	analýza
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
odlišný	odlišný	k2eAgInSc1d1	odlišný
a	a	k8xC	a
samostatný	samostatný	k2eAgInSc1d1	samostatný
rod	rod	k1gInSc1	rod
sauropodního	sauropodní	k2eAgMnSc2d1	sauropodní
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hřmotný	hřmotný	k2eAgMnSc1d1	hřmotný
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
Marshem	Marsh	k1gInSc7	Marsh
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
mladší	mladý	k2eAgNnSc4d2	mladší
synonymum	synonymum	k1gNnSc4	synonymum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
vědecky	vědecky	k6eAd1	vědecky
neplatné	platný	k2eNgNnSc4d1	neplatné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
nebyla	být	k5eNaImAgFnS	být
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
známa	známo	k1gNnSc2	známo
jeho	jeho	k3xOp3gFnSc1	jeho
lebka	lebka	k1gFnSc1	lebka
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
sauropodů	sauropod	k1gMnPc2	sauropod
poměrně	poměrně	k6eAd1	poměrně
častý	častý	k2eAgInSc1d1	častý
problém	problém	k1gInSc1	problém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zbytek	zbytek	k1gInSc1	zbytek
kostry	kostra	k1gFnSc2	kostra
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
lebka	lebka	k1gFnSc1	lebka
objevena	objeven	k2eAgFnSc1d1	objevena
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgNnP	být
ale	ale	k9	ale
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
lebku	lebka	k1gFnSc4	lebka
tohoto	tento	k3xDgMnSc2	tento
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
restaurován	restaurován	k2eAgInSc1d1	restaurován
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
kamarasaura	kamarasaura	k1gFnSc1	kamarasaura
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
název	název	k1gInSc1	název
Apatosaurus	Apatosaurus	k1gInSc1	Apatosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
klamný	klamný	k2eAgMnSc1d1	klamný
nebo	nebo	k8xC	nebo
bludný	bludný	k2eAgMnSc1d1	bludný
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgNnSc2d1	celé
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
rod	rod	k1gInSc4	rod
lidově	lidově	k6eAd1	lidově
znám	znát	k5eAaImIp1nS	znát
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
opětovně	opětovně	k6eAd1	opětovně
přijala	přijmout	k5eAaPmAgFnS	přijmout
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
natolik	natolik	k6eAd1	natolik
zlidovělo	zlidovět	k5eAaPmAgNnS	zlidovět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
všech	všecek	k3xTgMnPc2	všecek
sauropodních	sauropodní	k2eAgMnPc2d1	sauropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
rodu	rod	k1gInSc2	rod
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
,	,	kIx,	,
B.	B.	kA	B.
excelsus	excelsus	k1gInSc4	excelsus
<g/>
,	,	kIx,	,
B.	B.	kA	B.
parvus	parvus	k1gInSc4	parvus
a	a	k8xC	a
B.	B.	kA	B.
yahnahpin	yahnahpin	k1gInSc4	yahnahpin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrně	průměrně	k6eAd1	průměrně
velké	velký	k2eAgInPc1d1	velký
exempláře	exemplář	k1gInPc1	exemplář
druhu	druh	k1gInSc2	druh
B.	B.	kA	B.
excelsus	excelsus	k1gInSc1	excelsus
dorůstaly	dorůstat	k5eAaImAgFnP	dorůstat
délky	délka	k1gFnPc1	délka
kolem	kolem	k7c2	kolem
22	[number]	k4	22
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
přes	přes	k7c4	přes
15	[number]	k4	15
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
objevy	objev	k1gInPc4	objev
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
obřích	obří	k2eAgInPc2d1	obří
obratlů	obratel	k1gInPc2	obratel
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
apatosaurů	apatosaur	k1gInPc2	apatosaur
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
metrové	metrový	k2eAgFnSc6d1	metrová
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
existenci	existence	k1gFnSc4	existence
ještě	ještě	k6eAd1	ještě
podstatně	podstatně	k6eAd1	podstatně
větších	veliký	k2eAgMnPc2d2	veliký
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
žili	žít	k5eAaImAgMnP	žít
i	i	k9	i
brontosauři	brontosaurus	k1gMnPc1	brontosaurus
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
kolem	kolem	k7c2	kolem
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotností	hmotnost	k1gFnPc2	hmotnost
nad	nad	k7c4	nad
40	[number]	k4	40
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
dokázat	dokázat	k5eAaPmF	dokázat
to	ten	k3xDgNnSc4	ten
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
jen	jen	k9	jen
případné	případný	k2eAgInPc4d1	případný
další	další	k2eAgInPc4d1	další
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
větším	většit	k5eAaImIp1nS	většit
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
blízce	blízce	k6eAd1	blízce
příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
rodem	rod	k1gInSc7	rod
sauropoda	sauropoda	k1gMnSc1	sauropoda
byl	být	k5eAaImAgMnS	být
ostatně	ostatně	k6eAd1	ostatně
Supersaurus	Supersaurus	k1gMnSc1	Supersaurus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paleobiologie	paleobiologie	k1gFnSc2	paleobiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Apatosaurus	Apatosaurus	k1gMnSc1	Apatosaurus
i	i	k8xC	i
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
byli	být	k5eAaImAgMnP	být
pevně	pevně	k6eAd1	pevně
stavění	stavěný	k2eAgMnPc1d1	stavěný
sauropodi	sauropod	k1gMnPc1	sauropod
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nebyli	být	k5eNaImAgMnP	být
tak	tak	k6eAd1	tak
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Diplodocus	Diplodocus	k1gInSc1	Diplodocus
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
čeledi	čeleď	k1gFnSc2	čeleď
Diplodocidae	Diplodocida	k1gFnSc2	Diplodocida
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
neseno	nést	k5eAaImNgNnS	nést
čtyřmi	čtyři	k4xCgFnPc7	čtyři
sloupovitými	sloupovitý	k2eAgFnPc7d1	sloupovitá
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
měl	mít	k5eAaImAgMnS	mít
pět	pět	k4xCc1	pět
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
palce	palec	k1gInPc4	palec
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
nohou	noha	k1gFnPc6	noha
byly	být	k5eAaImAgInP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
drápem	dráp	k1gInSc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
ocas	ocas	k1gInSc1	ocas
byl	být	k5eAaImAgInS	být
bičovitý	bičovitý	k2eAgInSc1d1	bičovitý
<g/>
,	,	kIx,	,
tvořen	tvořit	k5eAaImNgInS	tvořit
82	[number]	k4	82
obratli	obratel	k1gInPc7	obratel
<g/>
,	,	kIx,	,
spojenými	spojený	k2eAgInPc7d1	spojený
mezi	mezi	k7c4	mezi
sebou	se	k3xPyFc7	se
klouby	kloub	k1gInPc1	kloub
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
diplodokův	diplodokův	k2eAgMnSc1d1	diplodokův
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
73	[number]	k4	73
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
paleontologové	paleontolog	k1gMnPc1	paleontolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sauropodi	sauropod	k1gMnPc1	sauropod
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
pomocí	pomocí	k7c2	pomocí
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
způsobem	způsob	k1gInSc7	způsob
komunikovali	komunikovat	k5eAaImAgMnP	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
teorii	teorie	k1gFnSc4	teorie
však	však	k9	však
nelze	lze	k6eNd1	lze
experimentálně	experimentálně	k6eAd1	experimentálně
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Bičovité	bičovitý	k2eAgNnSc1d1	bičovitý
zakončení	zakončení	k1gNnSc1	zakončení
ocasu	ocas	k1gInSc2	ocas
bylo	být	k5eAaImAgNnS	být
nicméně	nicméně	k8xC	nicméně
potenciálně	potenciálně	k6eAd1	potenciálně
dobře	dobře	k6eAd1	dobře
využitelnou	využitelný	k2eAgFnSc7d1	využitelná
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Provedené	provedený	k2eAgInPc1d1	provedený
výpočty	výpočet	k1gInPc1	výpočet
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
brontosauři	brontosaurus	k1gMnPc1	brontosaurus
mohli	moct	k5eAaImAgMnP	moct
při	při	k7c6	při
švihnutí	švihnutí	k1gNnSc6	švihnutí
ocasem	ocas	k1gInSc7	ocas
překonat	překonat	k5eAaPmF	překonat
jeho	jeho	k3xOp3gFnSc7	jeho
špičkou	špička	k1gFnSc7	špička
nadzvukovou	nadzvukový	k2eAgFnSc4d1	nadzvuková
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
vyvolat	vyvolat	k5eAaPmF	vyvolat
zvuk	zvuk	k1gInSc4	zvuk
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
přes	přes	k7c4	přes
200	[number]	k4	200
decibelů	decibel	k1gInPc2	decibel
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
krční	krční	k2eAgInPc4d1	krční
obratle	obratel	k1gInPc4	obratel
těchto	tento	k3xDgMnPc2	tento
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
masivní	masivní	k2eAgFnPc1d1	masivní
a	a	k8xC	a
propůjčovaly	propůjčovat	k5eAaImAgFnP	propůjčovat
krkům	krk	k1gInPc3	krk
těchto	tento	k3xDgInPc2	tento
sauropodů	sauropod	k1gInPc2	sauropod
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
průřez	průřez	k1gInSc4	průřez
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
studia	studio	k1gNnSc2	studio
průřezu	průřez	k1gInSc2	průřez
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
apatosauři	apatosaur	k1gMnPc1	apatosaur
i	i	k8xC	i
brontosauři	brontosaurus	k1gMnPc1	brontosaurus
rostli	růst	k5eAaImAgMnP	růst
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
plné	plný	k2eAgFnSc6d1	plná
velikosti	velikost	k1gFnSc6	velikost
(	(	kIx(	(
<g/>
asi	asi	k9	asi
23	[number]	k4	23
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
30	[number]	k4	30
tun	tuna	k1gFnPc2	tuna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
možná	možná	k9	možná
již	již	k6eAd1	již
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
kolem	kolem	k7c2	kolem
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
malý	malý	k2eAgMnSc1d1	malý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
mládětem	mládě	k1gNnSc7	mládě
druhu	druh	k1gInSc2	druh
B.	B.	kA	B.
parvus	parvus	k1gInSc1	parvus
(	(	kIx(	(
<g/>
CM	cm	kA	cm
566	[number]	k4	566
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otázka	otázka	k1gFnSc1	otázka
živorodosti	živorodost	k1gFnSc2	živorodost
==	==	k?	==
</s>
</p>
<p>
<s>
Paleontolog	paleontolog	k1gMnSc1	paleontolog
Robert	Robert	k1gMnSc1	Robert
T.	T.	kA	T.
Bakker	Bakker	k1gMnSc1	Bakker
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
vědci	vědec	k1gMnPc1	vědec
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
apatosauři	apatosaur	k1gMnPc1	apatosaur
a	a	k8xC	a
brontosauři	brontosaurus	k1gMnPc1	brontosaurus
možná	možná	k9	možná
rodili	rodit	k5eAaImAgMnP	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
(	(	kIx(	(
<g/>
asi	asi	k9	asi
230	[number]	k4	230
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
nekladli	klást	k5eNaImAgMnP	klást
tedy	tedy	k9	tedy
vejce	vejce	k1gNnSc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
myšlenky	myšlenka	k1gFnPc1	myšlenka
však	však	k9	však
byly	být	k5eAaImAgFnP	být
již	již	k9	již
prakticky	prakticky	k6eAd1	prakticky
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
vyvráceny	vyvrácen	k2eAgFnPc4d1	vyvrácena
četnými	četný	k2eAgInPc7d1	četný
paleontologickými	paleontologický	k2eAgInPc7d1	paleontologický
nálezy	nález	k1gInPc7	nález
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Upchurch	Upchurch	k1gMnSc1	Upchurch
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
;	;	kIx,	;
Tomida	Tomid	k1gMnSc2	Tomid
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
<g/>
;	;	kIx,	;
Barrett	Barrett	k1gMnSc1	Barrett
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k9	a
new	new	k?	new
specimen	specimen	k1gInSc1	specimen
of	of	k?	of
Apatosaurus	Apatosaurus	k1gInSc1	Apatosaurus
ajax	ajax	k1gInSc1	ajax
(	(	kIx(	(
<g/>
Sauropoda	Sauropoda	k1gFnSc1	Sauropoda
<g/>
:	:	kIx,	:
Diplodocidae	Diplodocidae	k1gFnSc1	Diplodocidae
<g/>
)	)	kIx)	)
from	from	k1gInSc1	from
the	the	k?	the
Morrison	Morrison	k1gInSc1	Morrison
Formation	Formation	k1gInSc1	Formation
(	(	kIx(	(
<g/>
Upper	Upper	k1gMnSc1	Upper
Jurassic	Jurassic	k1gMnSc1	Jurassic
<g/>
)	)	kIx)	)
of	of	k?	of
Wyoming	Wyoming	k1gInSc1	Wyoming
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaPmAgMnS	Nationat
Science	Scienka	k1gFnSc6	Scienka
Museum	museum	k1gNnSc4	museum
monographs	monographsa	k1gFnPc2	monographsa
26	[number]	k4	26
(	(	kIx(	(
<g/>
118	[number]	k4	118
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
156	[number]	k4	156
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1342	[number]	k4	1342
<g/>
-	-	kIx~	-
<g/>
9574	[number]	k4	9574
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stevens	Stevens	k1gInSc1	Stevens
<g/>
,	,	kIx,	,
Kent	Kent	k1gInSc1	Kent
A.	A.	kA	A.
<g/>
;	;	kIx,	;
Parrish	Parrish	k1gMnSc1	Parrish
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Neck	Neck	k1gInSc1	Neck
Posture	Postur	k1gMnSc5	Postur
and	and	k?	and
Feeding	Feeding	k1gInSc1	Feeding
Habits	Habits	k1gInSc1	Habits
of	of	k?	of
Two	Two	k1gFnSc2	Two
Jurassic	Jurassic	k1gMnSc1	Jurassic
Sauropod	Sauropoda	k1gFnPc2	Sauropoda
Dinosaurs	Dinosaursa	k1gFnPc2	Dinosaursa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Science	Science	k1gFnSc1	Science
<g/>
.	.	kIx.	.
284	[number]	k4	284
(	(	kIx(	(
<g/>
5415	[number]	k4	5415
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
798	[number]	k4	798
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
<g/>
10.112	[number]	k4	10.112
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
science	scienec	k1gInSc2	scienec
<g/>
.284	.284	k4	.284
<g/>
.5415	.5415	k4	.5415
<g/>
.798	.798	k4	.798
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
TSCHOPP	TSCHOPP	kA	TSCHOPP
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
;	;	kIx,	;
MATEUS	MATEUS	kA	MATEUS
<g/>
,	,	kIx,	,
Octávio	Octávio	k1gMnSc1	Octávio
<g/>
;	;	kIx,	;
BENSON	BENSON	kA	BENSON
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
B.J.	B.J.	k1gMnSc1	B.J.
A	a	k8xC	a
specimen-level	specimenevel	k1gMnSc1	specimen-level
phylogenetic	phylogenetice	k1gFnPc2	phylogenetice
analysis	analysis	k1gInSc4	analysis
and	and	k?	and
taxonomic	taxonomice	k1gInPc2	taxonomice
revision	revision	k1gInSc4	revision
of	of	k?	of
Diplodocidae	Diplodocida	k1gInSc2	Diplodocida
(	(	kIx(	(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
,	,	kIx,	,
Sauropoda	Sauropoda	k1gFnSc1	Sauropoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
PeerJ	PeerJ	k?	PeerJ
<g/>
,	,	kIx,	,
2015-04-07	[number]	k4	2015-04-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.771	[number]	k4	10.771
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
peerj	peerj	k1gInSc1	peerj
<g/>
.857	.857	k4	.857
<g/>
.	.	kIx.	.
</s>
<s>
PubMed	PubMed	k1gInSc1	PubMed
<g/>
:	:	kIx,	:
25870766	[number]	k4	25870766
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PeerJ	PeerJ	k?	PeerJ
<g/>
.	.	kIx.	.
</s>
<s>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
is	is	k?	is
back	back	k1gMnSc1	back
<g/>
!	!	kIx.	!
</s>
<s>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
is	is	k?	is
a	a	k8xC	a
unique	uniqu	k1gInSc2	uniqu
genus	genus	k1gMnSc1	genus
after	after	k1gMnSc1	after
all	all	k?	all
<g/>
..	..	k?	..
ScienceDaily	ScienceDaila	k1gFnSc2	ScienceDaila
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2015-04-07	[number]	k4	2015-04-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOCHA	Socha	k1gMnSc1	Socha
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
se	se	k3xPyFc4	se
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
.	.	kIx.	.
</s>
<s>
OSEL	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
