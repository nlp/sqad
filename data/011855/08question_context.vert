<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
pacifickém	pacifický	k2eAgNnSc6d1	pacifické
tažení	tažení	k1gNnSc6	tažení
se	se	k3xPyFc4	se
Spojenci	spojenec	k1gMnPc1	spojenec
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
k	k	k7c3	k
japonským	japonský	k2eAgMnPc3d1	japonský
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mohutně	mohutně	k6eAd1	mohutně
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
na	na	k7c4	na
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
,	,	kIx,	,
Ósaku	Ósaka	k1gFnSc4	Ósaka
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
strategickým	strategický	k2eAgNnSc7d1	strategické
bombardováním	bombardování	k1gNnSc7	bombardování
a	a	k8xC	a
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
atomovými	atomový	k2eAgFnPc7d1	atomová
bombami	bomba	k1gFnPc7	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
bezpodmínečně	bezpodmínečně	k6eAd1	bezpodmínečně
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
