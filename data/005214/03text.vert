<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
hieroglyfické	hieroglyfický	k2eAgNnSc1d1	hieroglyfické
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ἱ	ἱ	k?	ἱ
γ	γ	k?	γ
<g/>
,	,	kIx,	,
hiera	hiera	k1gMnSc1	hiera
glyfé	glyfá	k1gFnSc2	glyfá
–	–	k?	–
"	"	kIx"	"
<g/>
posvátný	posvátný	k2eAgInSc1d1	posvátný
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2900	[number]	k4	2900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
při	při	k7c6	při
sloučení	sloučení	k1gNnSc6	sloučení
Horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
Dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
sami	sám	k3xTgMnPc1	sám
jej	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
písmo	písmo	k1gNnSc1	písmo
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
neboť	neboť	k8xC	neboť
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
a	a	k8xC	a
lidem	lid	k1gInSc7	lid
předal	předat	k5eAaPmAgMnS	předat
bůh	bůh	k1gMnSc1	bůh
Thovt	Thovt	k1gMnSc1	Thovt
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
nepoužívali	používat	k5eNaImAgMnP	používat
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
samohlásky	samohláska	k1gFnSc2	samohláska
a	a	k8xC	a
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neměli	mít	k5eNaImAgMnP	mít
žádný	žádný	k3yNgInSc4	žádný
písemný	písemný	k2eAgInSc4d1	písemný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
vyslovovaly	vyslovovat	k5eAaImAgFnP	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Sbk	Sbk	k1gFnSc1	Sbk
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
Sobek	Sobek	k1gMnSc1	Sobek
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
Egypťané	Egypťan	k1gMnPc1	Egypťan
mu	on	k3xPp3gMnSc3	on
mohli	moct	k5eAaImAgMnP	moct
říkat	říkat	k5eAaImF	říkat
například	například	k6eAd1	například
Sebak	Sebak	k1gInSc4	Sebak
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
ale	ale	k9	ale
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bychom	by	kYmCp1nP	by
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
psané	psaný	k2eAgFnSc2d1	psaná
podoby	podoba	k1gFnSc2	podoba
jména	jméno	k1gNnSc2	jméno
mohli	moct	k5eAaImAgMnP	moct
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfický	hieroglyfický	k2eAgInSc1d1	hieroglyfický
systém	systém	k1gInSc1	systém
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
přes	přes	k7c4	přes
700	[number]	k4	700
různých	různý	k2eAgInPc2d1	různý
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
se	se	k3xPyFc4	se
psaly	psát	k5eAaImAgInP	psát
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
(	(	kIx(	(
<g/>
orientace	orientace	k1gFnSc1	orientace
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
určovala	určovat	k5eAaImAgFnS	určovat
orientací	orientace	k1gFnSc7	orientace
znaků	znak	k1gInPc2	znak
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
takovéhoto	takovýto	k3xDgInSc2	takovýto
textu	text	k1gInSc2	text
pak	pak	k6eAd1	pak
sloupce	sloupec	k1gInPc1	sloupec
znaků	znak	k1gInPc2	znak
bývají	bývat	k5eAaImIp3nP	bývat
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
svislými	svislý	k2eAgFnPc7d1	svislá
čarami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvopočátcích	prvopočátek	k1gInPc6	prvopočátek
písma	písmo	k1gNnSc2	písmo
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
každý	každý	k3xTgInSc1	každý
předmět	předmět	k1gInSc1	předmět
případně	případně	k6eAd1	případně
i	i	k8xC	i
činnost	činnost	k1gFnSc4	činnost
reprezentován	reprezentován	k2eAgMnSc1d1	reprezentován
vlastním	vlastní	k2eAgInSc7d1	vlastní
obrázkem	obrázek	k1gInSc7	obrázek
<g/>
,	,	kIx,	,
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
piktogram	piktogram	k1gInSc4	piktogram
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
<g/>
,	,	kIx,	,
piktogramy	piktogram	k1gInPc1	piktogram
začaly	začít	k5eAaPmAgInP	začít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
nejenom	nejenom	k6eAd1	nejenom
pojmy	pojem	k1gInPc4	pojem
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hlásky	hláska	k1gFnPc4	hláska
či	či	k8xC	či
skupinu	skupina	k1gFnSc4	skupina
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
původní	původní	k2eAgInSc4d1	původní
smysl	smysl	k1gInSc4	smysl
znaků	znak	k1gInPc2	znak
přitom	přitom	k6eAd1	přitom
nevymizel	vymizet	k5eNaPmAgInS	vymizet
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
egyptské	egyptský	k2eAgInPc4d1	egyptský
znaky	znak	k1gInPc4	znak
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
si	se	k3xPyFc3	se
podržely	podržet	k5eAaPmAgFnP	podržet
svoji	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
jednomu	jeden	k4xCgNnSc3	jeden
slovu	slovo	k1gNnSc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
kořeny	kořen	k1gInPc4	kořen
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
projevovaly	projevovat	k5eAaImAgFnP	projevovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nevymizely	vymizet	k5eNaPmAgFnP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Fonogramy	fonogram	k1gInPc1	fonogram
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
skládáním	skládání	k1gNnSc7	skládání
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
buď	buď	k8xC	buď
hlásku	hláska	k1gFnSc4	hláska
či	či	k8xC	či
skupinu	skupina	k1gFnSc4	skupina
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
jednoslabičné	jednoslabičný	k2eAgMnPc4d1	jednoslabičný
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
hláska	hláska	k1gFnSc1	hláska
či	či	k8xC	či
slabika	slabika	k1gFnSc1	slabika
<g/>
)	)	kIx)	)
dvouslabičné	dvouslabičný	k2eAgInPc1d1	dvouslabičný
a	a	k8xC	a
trojslabičné	trojslabičný	k2eAgInPc1d1	trojslabičný
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
sada	sada	k1gFnSc1	sada
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
specifická	specifický	k2eAgFnSc1d1	specifická
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
neměly	mít	k5eNaImAgFnP	mít
žádnou	žádný	k3yNgFnSc4	žádný
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mohly	moct	k5eAaImAgFnP	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
význam	význam	k1gInSc4	význam
i	i	k9	i
výslovnost	výslovnost	k1gFnSc1	výslovnost
zapsaného	zapsaný	k2eAgNnSc2d1	zapsané
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Determinativy	determinativ	k1gInPc1	determinativ
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
ani	ani	k9	ani
interpunkcí	interpunkce	k1gFnSc7	interpunkce
ani	ani	k8xC	ani
diakritikou	diakritika	k1gFnSc7	diakritika
a	a	k8xC	a
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
písmu	písmo	k1gNnSc6	písmo
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
interpunkci	interpunkce	k1gFnSc4	interpunkce
neexistuje	existovat	k5eNaImIp3nS	existovat
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
v	v	k7c6	v
hieroglyfech	hieroglyf	k1gInPc6	hieroglyf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
měly	mít	k5eAaImAgInP	mít
upřesňovací	upřesňovací	k2eAgInPc4d1	upřesňovací
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rozlišovací	rozlišovací	k2eAgFnSc6d1	rozlišovací
funkci	funkce	k1gFnSc6	funkce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
psala	psát	k5eAaImAgNnP	psát
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Determinativ	determinativ	k1gInSc1	determinativ
(	(	kIx(	(
<g/>
rozlišovač	rozlišovač	k1gInSc1	rozlišovač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poslední	poslední	k2eAgInSc1d1	poslední
znak	znak	k1gInSc1	znak
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
upřesňoval	upřesňovat	k5eAaImAgInS	upřesňovat
jeho	jeho	k3xOp3gInSc4	jeho
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k8xC	jako
kdybychom	kdyby	kYmCp1nP	kdyby
si	se	k3xPyFc3	se
za	za	k7c4	za
slovo	slovo	k1gNnSc4	slovo
lup	lupa	k1gFnPc2	lupa
nakreslili	nakreslit	k5eAaPmAgMnP	nakreslit
buď	buď	k8xC	buď
vlas	vlas	k1gInSc4	vlas
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
lupičskou	lupičský	k2eAgFnSc4d1	lupičská
masku	maska	k1gFnSc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Egyptština	egyptština	k1gFnSc1	egyptština
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
jazyků	jazyk	k1gInPc2	jazyk
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písemné	písemný	k2eAgFnSc6d1	písemná
podobě	podoba	k1gFnSc6	podoba
zachycovala	zachycovat	k5eAaImAgFnS	zachycovat
pouze	pouze	k6eAd1	pouze
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
byly	být	k5eAaImAgFnP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ani	ani	k8xC	ani
neměla	mít	k5eNaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
písemný	písemný	k2eAgInSc4d1	písemný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
determinativ	determinativ	k1gInSc1	determinativ
mohl	moct	k5eAaImAgInS	moct
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
hieroglyfického	hieroglyfický	k2eAgNnSc2d1	hieroglyfické
písma	písmo	k1gNnSc2	písmo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
i	i	k9	i
výslovnost	výslovnost	k1gFnSc1	výslovnost
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
shodný	shodný	k2eAgInSc4d1	shodný
zápis	zápis	k1gInSc4	zápis
(	(	kIx(	(
<g/>
shodné	shodný	k2eAgFnPc1d1	shodná
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
rozdílně	rozdílně	k6eAd1	rozdílně
se	se	k3xPyFc4	se
četla	číst	k5eAaImAgFnS	číst
<g/>
/	/	kIx~	/
<g/>
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgFnPc1d1	jiná
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kdybychom	kdyby	kYmCp1nP	kdyby
si	se	k3xPyFc3	se
za	za	k7c2	za
písmena	písmeno	k1gNnSc2	písmeno
v-l-k	v	k6eAd1	v-l-k
nakreslili	nakreslit	k5eAaPmAgMnP	nakreslit
vlčí	vlčí	k2eAgFnSc4d1	vlčí
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
(	(	kIx(	(
<g/>
vlak	vlak	k1gInSc4	vlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyže	lyže	k1gFnSc1	lyže
(	(	kIx(	(
<g/>
vlek	vlek	k1gInSc1	vlek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
meče	meč	k1gInPc1	meč
(	(	kIx(	(
<g/>
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
totožný	totožný	k2eAgInSc4d1	totožný
souhláskový	souhláskový	k2eAgInSc4d1	souhláskový
zápis	zápis	k1gInSc4	zápis
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
dodatečnému	dodatečný	k2eAgMnSc3d1	dodatečný
rozlišovacímu	rozlišovací	k2eAgMnSc3d1	rozlišovací
"	"	kIx"	"
<g/>
znaku	znak	k1gInSc3	znak
<g/>
"	"	kIx"	"
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
i	i	k8xC	i
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
znaků	znak	k1gInPc2	znak
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
pevně	pevně	k6eAd1	pevně
vymezené	vymezený	k2eAgFnPc1d1	vymezená
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
mnohdy	mnohdy	k6eAd1	mnohdy
mohl	moct	k5eAaImAgInS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
situacích	situace	k1gFnPc6	situace
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
fonogram	fonogram	k1gInSc4	fonogram
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
zase	zase	k9	zase
jako	jako	k9	jako
ideogram	ideogram	k1gInSc4	ideogram
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hieratické	hieratický	k2eAgNnSc1d1	hieratické
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
Démotické	démotický	k2eAgNnSc4d1	démotické
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfické	hieroglyfický	k2eAgNnSc1d1	hieroglyfické
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
složité	složitý	k2eAgNnSc1d1	složité
na	na	k7c4	na
zápis	zápis	k1gInSc4	zápis
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
precizně	precizně	k6eAd1	precizně
vykreslen	vykreslen	k2eAgInSc1d1	vykreslen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
objevila	objevit	k5eAaPmAgFnS	objevit
jeho	jeho	k3xOp3gFnSc4	jeho
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
zápis	zápis	k1gInSc4	zápis
na	na	k7c4	na
papyrové	papyrový	k2eAgInPc4d1	papyrový
svitky	svitek	k1gInPc4	svitek
či	či	k8xC	či
na	na	k7c4	na
střepy	střep	k1gInPc4	střep
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
taky	taky	k6eAd1	taky
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
písmo	písmo	k1gNnSc1	písmo
hieratické	hieratický	k2eAgFnPc1d1	hieratická
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kněžské	kněžský	k2eAgNnSc1d1	kněžské
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
varianta	varianta	k1gFnSc1	varianta
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dalším	další	k2eAgNnSc7d1	další
zjednodušováním	zjednodušování	k1gNnSc7	zjednodušování
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
písmo	písmo	k1gNnSc4	písmo
démotické	démotický	k2eAgFnSc2d1	démotický
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
lidové	lidový	k2eAgMnPc4d1	lidový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
(	(	kIx(	(
<g/>
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
gramotnost	gramotnost	k1gFnSc1	gramotnost
byla	být	k5eAaImAgFnS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
dobách	doba	k1gFnPc6	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
značné	značný	k2eAgNnSc4d1	značné
rozšíření	rozšíření	k1gNnSc4	rozšíření
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgInPc3d1	předchozí
systémům	systém	k1gInPc3	systém
<g/>
)	)	kIx)	)
dostalo	dostat	k5eAaPmAgNnS	dostat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
nové	nový	k2eAgInPc4d1	nový
písemné	písemný	k2eAgInPc4d1	písemný
systémy	systém	k1gInPc4	systém
však	však	k9	však
nikterak	nikterak	k6eAd1	nikterak
nevytlačily	vytlačit	k5eNaPmAgInP	vytlačit
předchozí	předchozí	k2eAgNnSc4d1	předchozí
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
nadále	nadále	k6eAd1	nadále
používána	používán	k2eAgNnPc1d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
a	a	k8xC	a
precizní	precizní	k2eAgInPc1d1	precizní
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
k	k	k7c3	k
náboženským	náboženský	k2eAgFnPc3d1	náboženská
textům	text	k1gInPc3	text
vytesaných	vytesaný	k2eAgMnPc2d1	vytesaný
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
liturgické	liturgický	k2eAgInPc4d1	liturgický
texty	text	k1gInPc4	text
na	na	k7c6	na
papyrech	papyr	k1gInPc6	papyr
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
používalo	používat	k5eAaImAgNnS	používat
písmo	písmo	k1gNnSc1	písmo
hieratické	hieratický	k2eAgNnSc1d1	hieratické
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
také	také	k9	také
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
písmo	písmo	k1gNnSc1	písmo
démotické	démotický	k2eAgNnSc1d1	démotické
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
již	již	k6eAd1	již
napovídá	napovídat	k5eAaBmIp3nS	napovídat
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
sloužilo	sloužit	k5eAaImAgNnS	sloužit
i	i	k9	i
k	k	k7c3	k
všedním	všední	k2eAgInPc3d1	všední
zápisům	zápis	k1gInPc3	zápis
a	a	k8xC	a
záznamům	záznam	k1gInPc3	záznam
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
i	i	k9	i
během	během	k7c2	během
perské	perský	k2eAgFnSc2d1	perská
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
přerušené	přerušený	k2eAgFnPc4d1	přerušená
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
Alexandrově	Alexandrův	k2eAgNnSc6d1	Alexandrovo
dobytí	dobytí	k1gNnSc6	dobytí
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
makedonské	makedonský	k2eAgFnSc2d1	makedonská
a	a	k8xC	a
římské	římský	k2eAgFnSc2d1	římská
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
složitost	složitost	k1gFnSc1	složitost
posledních	poslední	k2eAgInPc2d1	poslední
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
změněnou	změněný	k2eAgFnSc7d1	změněná
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
"	"	kIx"	"
<g/>
pravých	pravý	k2eAgMnPc2d1	pravý
Egypťanů	Egypťan	k1gMnPc2	Egypťan
<g/>
"	"	kIx"	"
a	a	k8xC	a
cizích	cizí	k2eAgMnPc2d1	cizí
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
domácích	domácí	k2eAgMnPc2d1	domácí
pomahačů	pomahač	k1gMnPc2	pomahač
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgNnSc7d1	jiné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
přežití	přežití	k1gNnSc2	přežití
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tendence	tendence	k1gFnSc1	tendence
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
římské	římský	k2eAgFnSc2d1	římská
kultury	kultura	k1gFnSc2	kultura
"	"	kIx"	"
<g/>
respektovat	respektovat	k5eAaImF	respektovat
<g/>
"	"	kIx"	"
domácí	domácí	k2eAgFnSc4d1	domácí
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
dovedlo	dovést	k5eAaPmAgNnS	dovést
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
Egypťanů	Egypťan	k1gMnPc2	Egypťan
číst	číst	k5eAaImF	číst
hieroglyfy	hieroglyf	k1gInPc4	hieroglyf
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
užití	užití	k1gNnSc1	užití
zcela	zcela	k6eAd1	zcela
ustává	ustávat	k5eAaImIp3nS	ustávat
po	po	k7c6	po
Theodosiově	Theodosiův	k2eAgInSc6d1	Theodosiův
zákazu	zákaz	k1gInSc6	zákaz
nekřesťanských	křesťanský	k2eNgInPc2d1	nekřesťanský
chrámů	chrám	k1gInPc2	chrám
roku	rok	k1gInSc2	rok
391	[number]	k4	391
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
hieroglyfický	hieroglyfický	k2eAgInSc1d1	hieroglyfický
nápis	nápis	k1gInSc1	nápis
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
394	[number]	k4	394
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfické	hieroglyfický	k2eAgNnSc4d1	hieroglyfické
písmo	písmo	k1gNnSc4	písmo
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
abeceda	abeceda	k1gFnSc1	abeceda
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
alfabéty	alfabéta	k1gFnSc2	alfabéta
a	a	k8xC	a
jen	jen	k9	jen
s	s	k7c7	s
několika	několik	k4yIc7	několik
démotickými	démotický	k2eAgInPc7d1	démotický
znaky	znak	k1gInPc7	znak
pro	pro	k7c4	pro
hlásky	hlásek	k1gInPc4	hlásek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
řečtina	řečtina	k1gFnSc1	řečtina
neznala	znát	k5eNaImAgFnS	znát
<g/>
;	;	kIx,	;
řeč	řeč	k1gFnSc1	řeč
Egypťanů	Egypťan	k1gMnPc2	Egypťan
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
nazývá	nazývat	k5eAaImIp3nS	nazývat
koptština	koptština	k1gFnSc1	koptština
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Arabů	Arab	k1gMnPc2	Arab
koptština	koptština	k1gFnSc1	koptština
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
jazykem	jazyk	k1gInSc7	jazyk
každodenní	každodenní	k2eAgFnSc2d1	každodenní
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
koptský	koptský	k2eAgInSc1d1	koptský
text	text	k1gInSc1	text
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jako	jako	k9	jako
živý	živý	k2eAgInSc4d1	živý
jazyk	jazyk	k1gInSc4	jazyk
koptština	koptština	k1gFnSc1	koptština
vymřela	vymřít	k5eAaPmAgFnS	vymřít
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dodnes	dodnes	k6eAd1	dodnes
přežívá	přežívat	k5eAaImIp3nS	přežívat
jako	jako	k9	jako
liturgický	liturgický	k2eAgInSc1d1	liturgický
jazyk	jazyk	k1gInSc1	jazyk
koptské	koptský	k2eAgFnSc2d1	koptská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
přežily	přežít	k5eAaPmAgInP	přežít
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
podobách	podoba	k1gFnPc6	podoba
<g/>
:	:	kIx,	:
přímo	přímo	k6eAd1	přímo
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
šest	šest	k4xCc1	šest
démotických	démotický	k2eAgInPc2d1	démotický
znaků	znak	k1gInPc2	znak
do	do	k7c2	do
koptské	koptský	k2eAgFnSc2d1	koptská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
nepřímo	přímo	k6eNd1	přímo
pak	pak	k6eAd1	pak
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
měly	mít	k5eAaImAgInP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
semitského	semitský	k2eAgNnSc2d1	semitské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pochází	pocházet	k5eAaImIp3nP	pocházet
ostatní	ostatní	k2eAgNnPc1d1	ostatní
hlásková	hláskový	k2eAgNnPc1d1	hláskové
písma	písmo	k1gNnPc1	písmo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
alfabety	alfabeta	k1gFnSc2	alfabeta
a	a	k8xC	a
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozluštění	rozluštění	k1gNnSc4	rozluštění
egyptských	egyptský	k2eAgInPc2d1	egyptský
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
se	se	k3xPyFc4	se
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
mnoho	mnoho	k4c1	mnoho
učenců	učenec	k1gMnPc2	učenec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
též	též	k9	též
Johannes	Johannes	k1gMnSc1	Johannes
Goropius	Goropius	k1gMnSc1	Goropius
Becanus	Becanus	k1gMnSc1	Becanus
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jezuita	jezuita	k1gMnSc1	jezuita
Athanasius	Athanasius	k1gMnSc1	Athanasius
Kircher	Kirchra	k1gFnPc2	Kirchra
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1680	[number]	k4	1680
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozluštil	rozluštit	k5eAaPmAgMnS	rozluštit
jeden	jeden	k4xCgInSc4	jeden
hieroglyf	hieroglyf	k1gInSc4	hieroglyf
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pokusy	pokus	k1gInPc1	pokus
však	však	k9	však
byly	být	k5eAaImAgInP	být
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
úspěch	úspěch	k1gInSc1	úspěch
pouze	pouze	k6eAd1	pouze
domnělý	domnělý	k2eAgMnSc1d1	domnělý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
úsilí	úsilí	k1gNnSc1	úsilí
opíralo	opírat	k5eAaImAgNnS	opírat
o	o	k7c4	o
imaginativní	imaginativní	k2eAgFnPc4d1	imaginativní
asociace	asociace	k1gFnPc4	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
totiž	totiž	k9	totiž
panovalo	panovat	k5eAaImAgNnS	panovat
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
písmo	písmo	k1gNnSc4	písmo
idiomatické	idiomatický	k2eAgFnSc2d1	idiomatická
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
překlady	překlad	k1gInPc1	překlad
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgInP	soustředit
na	na	k7c4	na
výklad	výklad	k1gInSc4	výklad
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
symboly	symbol	k1gInPc4	symbol
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
skutečnosti	skutečnost	k1gFnPc4	skutečnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
rozluštění	rozluštění	k1gNnSc4	rozluštění
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
mají	mít	k5eAaImIp3nP	mít
Thomas	Thomas	k1gMnSc1	Thomas
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
Jean-François	Jean-François	k1gFnSc1	Jean-François
Champollion	Champollion	k1gInSc1	Champollion
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
při	při	k7c6	při
Napoleonově	Napoleonův	k2eAgNnSc6d1	Napoleonovo
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
byl	být	k5eAaImAgInS	být
učiněn	učiněn	k2eAgInSc1d1	učiněn
nález	nález	k1gInSc1	nález
tzv.	tzv.	kA	tzv.
Rosettské	rosettský	k2eAgFnSc2d1	Rosettská
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
typech	typ	k1gInPc6	typ
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
v	v	k7c6	v
hieroglyfickém	hieroglyfický	k2eAgMnSc6d1	hieroglyfický
<g/>
,	,	kIx,	,
démotickém	démotický	k2eAgMnSc6d1	démotický
a	a	k8xC	a
řeckém	řecký	k2eAgNnSc6d1	řecké
písmu	písmo	k1gNnSc6	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
kritický	kritický	k2eAgInSc4d1	kritický
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Champollionovi	Champollion	k1gMnSc3	Champollion
umožnil	umožnit	k5eAaPmAgMnS	umožnit
proniknout	proniknout	k5eAaPmF	proniknout
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
<g/>
.	.	kIx.	.
</s>
<s>
Rozluštění	rozluštění	k1gNnSc1	rozluštění
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
pak	pak	k6eAd1	pak
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
vyvrátilo	vyvrátit	k5eAaPmAgNnS	vyvrátit
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
písmo	písmo	k1gNnSc4	písmo
idiomatické	idiomatický	k2eAgNnSc4d1	idiomatické
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
obrázkové	obrázkový	k2eAgFnPc1d1	obrázková
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
Champolionových	Champolionový	k2eAgInPc2d1	Champolionový
vývodů	vývod	k1gInPc2	vývod
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
časem	časem	k6eAd1	časem
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
novějšími	nový	k2eAgInPc7d2	novější
poznatky	poznatek	k1gInPc7	poznatek
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
písmu	písmo	k1gNnSc6	písmo
a	a	k8xC	a
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
porozumění	porozumění	k1gNnSc2	porozumění
tomuto	tento	k3xDgNnSc3	tento
písmu	písmo	k1gNnSc3	písmo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
zásluhy	zásluha	k1gFnPc1	zásluha
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nijak	nijak	k6eAd1	nijak
neumenšují	umenšovat	k5eNaImIp3nP	umenšovat
<g/>
.	.	kIx.	.
</s>
