<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
(	(	kIx(
<g/>
Nejdek	Nejdek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
v	v	k7c6
Nejdku	Nejdek	k1gInSc6
Pohled	pohled	k1gInSc4
od	od	k7c2
jihuMísto	jihuMísto	k6eAd1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
Obec	obec	k1gFnSc1
</s>
<s>
Nejdek	Nejdek	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
32,29	32,29	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
0,96	0,96	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
plzeňská	plzeňská	k1gFnSc1
Vikariát	vikariát	k1gInSc1
</s>
<s>
karlovarský	karlovarský	k2eAgMnSc1d1
Farnost	farnost	k1gFnSc1
</s>
<s>
Nejdek	Nejdek	k1gInSc1
Užívání	užívání	k1gNnSc1
</s>
<s>
pravidelné	pravidelný	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1755	#num#	k4
<g/>
–	–	k?
<g/>
1756	#num#	k4
Specifikace	specifikace	k1gFnSc1
Umístění	umístění	k1gNnSc2
oltáře	oltář	k1gInSc2
</s>
<s>
neorientovaný	orientovaný	k2eNgInSc1d1
Stavební	stavební	k2eAgInSc1d1
materiál	materiál	k1gInSc1
</s>
<s>
zdivo	zdivo	k1gNnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Nám.	Nám.	k?
Karla	Karla	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
175	#num#	k4
<g/>
,	,	kIx,
Nejdek	Nejdek	k1gInSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Náměstí	náměstí	k1gNnSc1
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
38718	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
952	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
je	být	k5eAaImIp3nS
římskokatolický	římskokatolický	k2eAgInSc1d1
neorientovaný	orientovaný	k2eNgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
Nejdku	Nejdek	k1gInSc6
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
typickou	typický	k2eAgFnSc4d1
ukázku	ukázka	k1gFnSc4
rustikální	rustikální	k2eAgFnSc2d1
barokní	barokní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
je	být	k5eAaImIp3nS
kostel	kostel	k1gInSc1
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xC,k8xS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
bezprostřední	bezprostřední	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
kostela	kostel	k1gInSc2
stojí	stát	k5eAaImIp3nS
socha	socha	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1710	#num#	k4
a	a	k8xC
barokní	barokní	k2eAgInSc1d1
sloup	sloup	k1gInSc1
se	s	k7c7
sousoším	sousoší	k1gNnSc7
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1717	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgNnPc1
sochařská	sochařský	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
památkově	památkově	k6eAd1
chráněna	chránit	k5eAaImNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Současnému	současný	k2eAgInSc3d1
kostelu	kostel	k1gInSc3
předcházel	předcházet	k5eAaImAgInS
gotický	gotický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
Petr	Petr	k1gMnSc1
Plik	plika	k1gFnPc2
někdy	někdy	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1341	#num#	k4
<g/>
–	–	k?
<g/>
1354	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
velkém	velký	k2eAgInSc6d1
lenním	lenní	k2eAgInSc6d1
listu	list	k1gInSc6
Petra	Petra	k1gFnSc1
Plika	plika	k1gFnSc1
od	od	k7c2
Jana	Jan	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
ze	z	k7c2
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1341	#num#	k4
není	být	k5eNaImIp3nS
ani	ani	k9
přes	přes	k7c4
velmi	velmi	k6eAd1
podrobný	podrobný	k2eAgInSc4d1
výčet	výčet	k1gInSc4
všeho	všecek	k3xTgInSc2
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
žádná	žádný	k3yNgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
kostelu	kostel	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píše	psát	k5eAaImIp3nS
se	se	k3xPyFc4
jen	jen	k9
o	o	k7c6
hradu	hrad	k1gInSc6
Neudek	Neudek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tzv.	tzv.	kA
Erekčních	erekční	k2eAgFnPc6d1
knihách	kniha	k1gFnPc6
(	(	kIx(
<g/>
Libri	Libr	k1gFnSc6
erectionum	erectionum	k1gNnSc4
<g/>
)	)	kIx)
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
z	z	k7c2
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1354	#num#	k4
se	se	k3xPyFc4
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
praví	pravit	k5eAaImIp3nS,k5eAaBmIp3nS
o	o	k7c6
ustavení	ustavení	k1gNnSc6
faráře	farář	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
předpokládá	předpokládat	k5eAaImIp3nS
existenci	existence	k1gFnSc4
farnosti	farnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
tvořil	tvořit	k5eAaImAgInS
hrad	hrad	k1gInSc1
Nejdek	Nejdek	k1gInSc1
a	a	k8xC
okolní	okolní	k2eAgFnSc2d1
osady	osada	k1gFnSc2
<g/>
,	,	kIx,
město	město	k1gNnSc1
Nejdek	Nejdek	k1gInSc1
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
neexistovalo	existovat	k5eNaImAgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
pouhá	pouhý	k2eAgFnSc1d1
hornická	hornický	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
živobytí	živobytí	k1gNnSc4
nejdeckých	nejdecký	k2eAgMnPc2d1
horníků	horník	k1gMnPc2
se	se	k3xPyFc4
staraly	starat	k5eAaImAgFnP
vesnice	vesnice	k1gFnSc1
Suchá	Suchá	k1gFnSc1
a	a	k8xC
Heřmanov	Heřmanov	k1gInSc1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
uvedené	uvedený	k2eAgFnPc1d1
v	v	k7c4
z	z	k7c2
roku	rok	k1gInSc2
1341	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
nejdecký	nejdecký	k2eAgInSc4d1
farní	farní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
již	již	k6eAd1
roku	rok	k1gInSc2
1354	#num#	k4
existoval	existovat	k5eAaImAgInS
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1341	#num#	k4
ještě	ještě	k6eAd1
není	být	k5eNaImIp3nS
zmiňován	zmiňován	k2eAgInSc1d1
<g/>
,	,	kIx,
lze	lze	k6eAd1
dovodit	dovodit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
1341	#num#	k4
<g/>
–	–	k?
<g/>
1354	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostel	kostel	k1gInSc4
později	pozdě	k6eAd2
přestavěli	přestavět	k5eAaPmAgMnP
a	a	k8xC
zvětšili	zvětšit	k5eAaPmAgMnP
Šlikové	Šliek	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
drželi	držet	k5eAaImAgMnP
nejdecké	nejdecký	k2eAgNnSc4d1
panství	panství	k1gNnSc4
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
1446	#num#	k4
<g/>
–	–	k?
<g/>
1602	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejstarších	starý	k2eAgFnPc6d3
dobách	doba	k1gFnPc6
byl	být	k5eAaImAgInS
u	u	k7c2
kostela	kostel	k1gInSc2
hřbitov	hřbitov	k1gInSc4
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
rokem	rok	k1gInSc7
1600	#num#	k4
přemístěn	přemístit	k5eAaPmNgMnS
na	na	k7c4
nové	nový	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
existenci	existence	k1gFnSc6
starého	starý	k2eAgInSc2d1
hřbitova	hřbitov	k1gInSc2
svědčí	svědčit	k5eAaImIp3nS
nález	nález	k1gInSc1
četných	četný	k2eAgInPc2d1
kosterních	kosterní	k2eAgInPc2d1
pozůstatků	pozůstatek	k1gInPc2
při	při	k7c6
pracích	práce	k1gFnPc6
na	na	k7c6
vodovodu	vodovod	k1gInSc6
roku	rok	k1gInSc2
1909	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stará	starý	k2eAgFnSc1d1
listina	listina	k1gFnSc1
faráře	farář	k1gMnSc2
Kirchnera	Kirchner	k1gMnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
roku	rok	k1gInSc2
1513	#num#	k4
byl	být	k5eAaImAgInS
vnitřek	vnitřek	k1gInSc1
kostela	kostel	k1gInSc2
obnoven	obnovit	k5eAaPmNgInS
a	a	k8xC
na	na	k7c4
strop	strop	k1gInSc4
zapsán	zapsán	k2eAgInSc4d1
letopočet	letopočet	k1gInSc4
1513	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církevní	církevní	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
letech	let	k1gInPc6
1562	#num#	k4
<g/>
–	–	k?
<g/>
1624	#num#	k4
vedeny	vést	k5eAaImNgInP
výhradně	výhradně	k6eAd1
evangelickými	evangelický	k2eAgMnPc7d1
pastory	pastor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
byl	být	k5eAaImAgMnS
evangelický	evangelický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
Valentýn	Valentýn	k1gMnSc1
Löw	Löw	k1gMnSc1
z	z	k7c2
Adorfu	Adorf	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1624	#num#	k4
vyhnán	vyhnat	k5eAaPmNgMnS
císařskými	císařský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
křty	křest	k1gInPc4
a	a	k8xC
všechny	všechen	k3xTgInPc1
farní	farní	k2eAgInPc1d1
úkony	úkon	k1gInPc1
po	po	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
vykonávány	vykonáván	k2eAgInPc4d1
střídavě	střídavě	k6eAd1
učitelem	učitel	k1gMnSc7
<g/>
,	,	kIx,
porodní	porodní	k2eAgFnSc7d1
bábou	bába	k1gFnSc7
<g/>
,	,	kIx,
děkanem	děkan	k1gMnSc7
z	z	k7c2
Lokte	loket	k1gInSc2
a	a	k8xC
farářem	farář	k1gMnSc7
z	z	k7c2
Chodova	Chodov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
provizorium	provizorium	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
<g/>
,	,	kIx,
až	až	k8xS
když	když	k8xS
loketský	loketský	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
dosadil	dosadit	k5eAaPmAgMnS
nejprve	nejprve	k6eAd1
faráře	farář	k1gMnSc2
Leonarda	Leonardo	k1gMnSc2
Löfflera	Löffler	k1gMnSc2
a	a	k8xC
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1627	#num#	k4
faráře	farář	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
Brauna	Braun	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
saském	saský	k2eAgInSc6d1
vpádu	vpád	k1gInSc6
roku	rok	k1gInSc2
1631	#num#	k4
musel	muset	k5eAaImAgMnS
farář	farář	k1gMnSc1
Braun	Braun	k1gMnSc1
uprchnout	uprchnout	k5eAaPmF
a	a	k8xC
uvolnit	uvolnit	k5eAaPmF
místo	místo	k6eAd1
evangelickému	evangelický	k2eAgMnSc3d1
pastoru	pastor	k1gMnSc3
Zachariáši	Zachariáš	k1gMnSc3
Adlerovi	Adler	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1632	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
císařské	císařský	k2eAgInPc1d1
pluky	pluk	k1gInPc1
Sasy	Sas	k1gMnPc4
znovu	znovu	k6eAd1
vytlačily	vytlačit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
nato	nato	k6eAd1
však	však	k9
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
na	na	k7c4
mor	mor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zde	zde	k6eAd1
tehdy	tehdy	k6eAd1
řádil	řádit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1641	#num#	k4
až	až	k9
1661	#num#	k4
byl	být	k5eAaImAgInS
farářem	farář	k1gMnSc7
páter	páter	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Lanka	lanko	k1gNnSc2
<g/>
,	,	kIx,
cisterciák	cisterciák	k1gMnSc1
z	z	k7c2
Plas	plasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
zpočátku	zpočátku	k6eAd1
zdržoval	zdržovat	k5eAaImAgInS
jen	jen	k9
občas	občas	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1648	#num#	k4
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
Nejdku	Nejdek	k1gInSc6
trvale	trvale	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
podnět	podnět	k1gInSc4
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1648	#num#	k4
zakoupen	zakoupit	k5eAaPmNgInS
z	z	k7c2
církevních	církevní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
dům	dům	k1gInSc1
nad	nad	k7c7
kostelem	kostel	k1gInSc7
i	i	k8xC
se	s	k7c7
zahradou	zahrada	k1gFnSc7
a	a	k8xC
zřízena	zřízen	k2eAgFnSc1d1
v	v	k7c6
něm	on	k3xPp3gInSc6
církevní	církevní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
působení	působení	k1gNnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1656	#num#	k4
pořízen	pořízen	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
zvon	zvon	k1gInSc1
od	od	k7c2
manželky	manželka	k1gFnSc2
hraběte	hrabě	k1gMnSc2
Humprechta	Humprecht	k1gMnSc2
Černína	Černín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
farářem	farář	k1gMnSc7
od	od	k7c2
roku	rok	k1gInSc2
1708	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1733	#num#	k4
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1709	#num#	k4
škapulířové	škapulířový	k2eAgNnSc4d1
bratrstvo	bratrstvo	k1gNnSc4
a	a	k8xC
postaral	postarat	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
obnovu	obnova	k1gFnSc4
farního	farní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
provedla	provést	k5eAaPmAgFnS
roku	rok	k1gInSc2
1711	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
nově	nově	k6eAd1
zastřešen	zastřešen	k2eAgInSc4d1
šindeli	šindel	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1726	#num#	k4
byla	být	k5eAaImAgFnS
z	z	k7c2
církevních	církevní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
znovu	znovu	k6eAd1
postavena	postaven	k2eAgFnSc1d1
fara	fara	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
hrozila	hrozit	k5eAaImAgFnS
zřícením	zřícení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejdecké	nejdecký	k2eAgFnSc3d1
faře	fara	k1gFnSc3
patřily	patřit	k5eAaImAgFnP
také	také	k9
čtyři	čtyři	k4xCgFnPc1
kaple	kaple	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
byla	být	k5eAaImAgFnS
Křížová	Křížová	k1gFnSc1
kaple	kaple	k1gFnSc2
na	na	k7c6
starém	starý	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
,	,	kIx,
druhou	druhý	k4xOgFnSc7
nemocniční	nemocniční	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
zámecká	zámecký	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
poslední	poslední	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
kaplička	kaplička	k1gFnSc1
na	na	k7c6
Boží	boží	k2eAgFnSc6d1
louce	louka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
Bernovský	Bernovský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
poslední	poslední	k2eAgFnSc6d1
zásadní	zásadní	k2eAgFnSc6d1
přestavbě	přestavba	k1gFnSc6
kostela	kostel	k1gInSc2
do	do	k7c2
současné	současný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
letech	let	k1gInPc6
1755	#num#	k4
<g/>
–	–	k?
<g/>
1756	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc4
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
zvětšen	zvětšen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
půdorys	půdorys	k1gInSc1
i	i	k8xC
orientace	orientace	k1gFnSc1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
oltáře	oltář	k1gInSc2
směřující	směřující	k2eAgFnSc1d1
k	k	k7c3
východu	východ	k1gInSc3
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
na	na	k7c4
severovýchodní	severovýchodní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1
zděný	zděný	k2eAgInSc1d1
barokní	barokní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
obdélného	obdélný	k2eAgInSc2d1
půdorysu	půdorys	k1gInSc2
stojí	stát	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
původního	původní	k2eAgInSc2d1
kamenného	kamenný	k2eAgInSc2d1
gotického	gotický	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
loď	loď	k1gFnSc4
navazuje	navazovat	k5eAaImIp3nS
stejně	stejně	k6eAd1
široký	široký	k2eAgInSc1d1
<g/>
,	,	kIx,
kruhově	kruhově	k6eAd1
uzavřený	uzavřený	k2eAgInSc1d1
presbytář	presbytář	k1gInSc1
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
je	být	k5eAaImIp3nS
připojena	připojen	k2eAgFnSc1d1
sakristie	sakristie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěny	stěna	k1gFnPc4
lodi	loď	k1gFnSc2
člení	členit	k5eAaImIp3nP
pilastry	pilastr	k1gInPc1
s	s	k7c7
kompozitní	kompozitní	k2eAgFnSc7d1
hlavicí	hlavice	k1gFnSc7
<g/>
,	,	kIx,
nesoucí	nesoucí	k2eAgFnSc4d1
průběžnou	průběžný	k2eAgFnSc4d1
zalamovanou	zalamovaný	k2eAgFnSc4d1
římsu	římsa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
prosvětluje	prosvětlovat	k5eAaImIp3nS
pět	pět	k4xCc4
párů	pár	k1gInPc2
obdélníkových	obdélníkový	k2eAgNnPc2d1
oken	okno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgNnPc1
stejná	stejný	k2eAgNnPc1d1
okna	okno	k1gNnPc1
prosvětlují	prosvětlovat	k5eAaImIp3nP
presbytář	presbytář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průčelí	průčelí	k1gNnSc1
patrové	patrový	k2eAgFnSc2d1
sakristie	sakristie	k1gFnSc2
je	být	k5eAaImIp3nS
členěno	členit	k5eAaImNgNnS
lizénovými	lizénový	k2eAgInPc7d1
rámy	rám	k1gInPc7
<g/>
,	,	kIx,
nároží	nároží	k1gNnPc1
jsou	být	k5eAaImIp3nP
zaoblená	zaoblený	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okna	okno	k1gNnPc1
v	v	k7c6
sakristii	sakristie	k1gFnSc6
a	a	k8xC
oratoři	oratoř	k1gFnSc6
jsou	být	k5eAaImIp3nP
obdélníková	obdélníkový	k2eAgFnSc1d1
se	se	k3xPyFc4
šambránami	šambrána	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
má	mít	k5eAaImIp3nS
sedlovou	sedlový	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
závěru	závěr	k1gInSc6
valbovou	valbový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
střecha	střecha	k1gFnSc1
měla	mít	k5eAaImAgFnS
břidlicovou	břidlicový	k2eAgFnSc4d1
krytinu	krytina	k1gFnSc4
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
plechová	plechový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
presbytářem	presbytář	k1gInSc7
se	se	k3xPyFc4
zvedá	zvedat	k5eAaImIp3nS
polygonální	polygonální	k2eAgInSc1d1
sanktusník	sanktusník	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
zakončený	zakončený	k2eAgInSc4d1
bání	bání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInSc4d1
průčelí	průčelí	k1gNnSc1
je	být	k5eAaImIp3nS
uzavřeno	uzavřít	k5eAaPmNgNnS
štítem	štít	k1gInSc7
se	s	k7c7
segmentově	segmentově	k6eAd1
zvednutou	zvednutý	k2eAgFnSc7d1
římsou	římsa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
průčelí	průčelí	k1gNnSc4
kostela	kostel	k1gInSc2
se	s	k7c7
zvednutou	zvednutý	k2eAgFnSc7d1
římsou	římsa	k1gFnSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
členěno	členit	k5eAaImNgNnS
pilastry	pilastr	k1gInPc1
a	a	k8xC
završeno	završen	k2eAgNnSc1d1
lichoběžníkovým	lichoběžníkový	k2eAgInSc7d1
štítem	štít	k1gInSc7
se	s	k7c7
středovou	středový	k2eAgFnSc7d1
nikou	nika	k1gFnSc7
se	s	k7c7
sochou	socha	k1gFnSc7
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Interiér	interiér	k1gInSc1
</s>
<s>
Interiér	interiér	k1gInSc1
kostela	kostel	k1gInSc2
</s>
<s>
Presbytář	presbytář	k1gInSc1
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
zaklenut	zaklenout	k5eAaPmNgInS
plackou	placka	k1gFnSc7
s	s	k7c7
konchou	koncha	k1gFnSc7
<g/>
,	,	kIx,
rovněž	rovněž	k9
v	v	k7c6
kněžišti	kněžiště	k1gNnSc6
je	být	k5eAaImIp3nS
placková	plackový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sakristie	sakristie	k1gFnSc1
je	být	k5eAaImIp3nS
zaklenuta	zaklenout	k5eAaPmNgFnS
valenou	valený	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
radiálními	radiální	k2eAgFnPc7d1
výsečemi	výseč	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
plochostropá	plochostropý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
patrová	patrový	k2eAgFnSc1d1
kruchta	kruchta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
lodi	loď	k1gFnSc2
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
renesanční	renesanční	k2eAgInSc1d1
náhrobník	náhrobník	k1gInSc1
s	s	k7c7
nečitelným	čitelný	k2eNgInSc7d1
nápisem	nápis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
hraběte	hrabě	k1gMnSc4
Kryštofa	Kryštof	k1gMnSc4
Šlika	šlika	k1gFnSc1
<g/>
,	,	kIx,
pohřbeného	pohřbený	k2eAgMnSc2d1
v	v	k7c6
kryptě	krypta	k1gFnSc6
kostela	kostel	k1gInSc2
roku	rok	k1gInSc2
1578	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
kůru	kůr	k1gInSc6
jsou	být	k5eAaImIp3nP
varhany	varhany	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
roku	rok	k1gInSc2
1778	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
byla	být	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
mechanická	mechanický	k2eAgFnSc1d1
část	část	k1gFnSc1
vyměněna	vyměnit	k5eAaPmNgFnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
hlasové	hlasový	k2eAgInPc4d1
rejstříky	rejstřík	k1gInPc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
prospekt	prospekt	k1gInSc4
(	(	kIx(
<g/>
skříň	skříň	k1gFnSc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
použity	použít	k5eAaPmNgFnP
ze	z	k7c2
starých	starý	k2eAgFnPc2d1
varhan	varhany	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
doby	doba	k1gFnSc2
přestavby	přestavba	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1755	#num#	k4
<g/>
–	–	k?
<g/>
1756	#num#	k4
pochází	pocházet	k5eAaImIp3nS
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
vnitřního	vnitřní	k2eAgNnSc2d1
rokokového	rokokový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oltáře	Oltář	k1gInPc1
</s>
<s>
U	u	k7c2
hlavního	hlavní	k2eAgInSc2d1
oltáře	oltář	k1gInSc2
stojí	stát	k5eAaImIp3nP
sochy	socha	k1gFnPc1
Mojžíše	Mojžíš	k1gMnSc2
<g/>
,	,	kIx,
Melchisedecha	Melchisedech	k1gMnSc2
<g/>
,	,	kIx,
Arona	Aron	k1gMnSc2
a	a	k8xC
Davida	David	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
na	na	k7c6
oltáři	oltář	k1gInSc6
představuje	představovat	k5eAaImIp3nS
Abraháma	Abrahám	k1gMnSc4
při	při	k7c6
obětování	obětování	k1gNnSc6
Izáka	Izák	k1gMnSc2
a	a	k8xC
nad	nad	k7c7
ním	on	k3xPp3gNnSc7
byl	být	k5eAaImAgInS
malý	malý	k2eAgInSc4d1
obraz	obraz	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
oltář	oltář	k1gInSc1
byl	být	k5eAaImAgInS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1779	#num#	k4
přivezen	přivézt	k5eAaPmNgInS
z	z	k7c2
Prahy	Praha	k1gFnSc2
ze	z	k7c2
zrušeného	zrušený	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
servitů	servita	k1gMnPc2
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
presbytáři	presbytář	k1gInSc6
byly	být	k5eAaImAgFnP
vedle	vedle	k7c2
hlavního	hlavní	k2eAgInSc2d1
oltáře	oltář	k1gInSc2
dva	dva	k4xCgInPc1
postranní	postranní	k2eAgInPc1d1
oltáře	oltář	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
bývalé	bývalý	k2eAgFnSc2d1
nejdecké	nejdecký	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
vybaven	vybavit	k5eAaPmNgInS
obrazem	obraz	k1gInSc7
Korunování	korunování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
byl	být	k5eAaImAgInS
oltář	oltář	k1gInSc4
Srdce	srdce	k1gNnSc2
Ježíšova	Ježíšův	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
oltáře	oltář	k1gInPc4
již	již	k6eAd1
v	v	k7c6
kostele	kostel	k1gInSc6
nejsou	být	k5eNaImIp3nP
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
odstraněny	odstranit	k5eAaPmNgInP
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
oltář	oltář	k1gInSc1
je	být	k5eAaImIp3nS
zasvěcen	zasvětit	k5eAaPmNgInS
svatému	svatý	k1gMnSc3
Martinovi	Martin	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boční	boční	k2eAgInPc1d1
oltáře	oltář	k1gInPc1
jsou	být	k5eAaImIp3nP
bohatě	bohatě	k6eAd1
zdobené	zdobený	k2eAgInPc1d1
řezbářskými	řezbářský	k2eAgFnPc7d1
pracemi	práce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostela	kostel	k1gInSc2
stojí	stát	k5eAaImIp3nS
dnes	dnes	k6eAd1
boční	boční	k2eAgInSc4d1
oltář	oltář	k1gInSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Bolestné	bolestný	k2eAgFnSc2d1
<g/>
,	,	kIx,
původně	původně	k6eAd1
se	s	k7c7
sochami	socha	k1gFnPc7
svatého	svatý	k2eAgMnSc2d1
Jáchyma	Jáchym	k1gMnSc2
a	a	k8xC
svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
místě	místo	k1gNnSc6
sochy	socha	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Ignáce	Ignác	k1gMnSc2
a	a	k8xC
Františka	František	k1gMnSc2
Xavera	Xaver	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
protější	protější	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
oltář	oltář	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
z	z	k7c2
Nepomuku	Nepomuk	k1gInSc2
se	s	k7c7
sochami	socha	k1gFnPc7
svaté	svatý	k2eAgFnSc2d1
Ludmily	Ludmila	k1gFnSc2
a	a	k8xC
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náhrobky	náhrobek	k1gInPc1
</s>
<s>
V	v	k7c6
kryptě	krypta	k1gFnSc6
je	být	k5eAaImIp3nS
cenný	cenný	k2eAgInSc1d1
renesanční	renesanční	k2eAgInSc1d1
náhrobek	náhrobek	k1gInSc1
hraběte	hrabě	k1gMnSc2
Kryštofa	Kryštof	k1gMnSc2
Šlika	šlika	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
kostele	kostel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hrobkách	hrobka	k1gFnPc6
kostela	kostel	k1gInSc2
byli	být	k5eAaImAgMnP
pohřbeni	pohřben	k2eAgMnPc1d1
následující	následující	k2eAgMnSc1d1
osobnosti	osobnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
1578	#num#	k4
<g/>
:	:	kIx,
hrabě	hrabě	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
Šlik	šlika	k1gFnPc2
</s>
<s>
1583	#num#	k4
<g/>
:	:	kIx,
hrabě	hrabě	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
Šlik	šlika	k1gFnPc2
</s>
<s>
1603	#num#	k4
<g/>
:	:	kIx,
Wolf	Wolf	k1gMnSc1
Kašpar	Kašpar	k1gMnSc1
svobodný	svobodný	k2eAgMnSc1d1
pán	pán	k1gMnSc1
z	z	k7c2
Felsu	Fels	k1gInSc2
</s>
<s>
1608	#num#	k4
<g/>
:	:	kIx,
slečna	slečna	k1gFnSc1
Anna	Anna	k1gFnSc1
Barbara	Barbara	k1gFnSc1
baronka	baronka	k1gFnSc1
z	z	k7c2
Felsu	Fels	k1gInSc2
</s>
<s>
1614	#num#	k4
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Kašpar	Kašpar	k1gMnSc1
Colonna	Colonn	k1gInSc2
svobodný	svobodný	k2eAgMnSc1d1
pán	pán	k1gMnSc1
z	z	k7c2
Felsu	Fels	k1gInSc2
</s>
<s>
1623	#num#	k4
<g/>
:	:	kIx,
Kašpar	Kašpar	k1gMnSc1
Colonna	Colonn	k1gInSc2
svobodný	svobodný	k2eAgMnSc1d1
pán	pán	k1gMnSc1
z	z	k7c2
Felsu	Fels	k1gInSc2
</s>
<s>
1623	#num#	k4
<g/>
:	:	kIx,
paní	paní	k1gFnSc3
Margarete	Margare	k1gNnSc2
Thiesel	Thiesela	k1gFnPc2
z	z	k7c2
Daltitz	Daltitza	k1gFnPc2
</s>
<s>
1628	#num#	k4
<g/>
:	:	kIx,
slečna	slečna	k1gFnSc1
Euphemia	Euphemia	k1gFnSc1
z	z	k7c2
Haßlau	Haßlaus	k1gInSc2
na	na	k7c6
Kirchbergu	Kirchberg	k1gInSc6
</s>
<s>
1654	#num#	k4
<g/>
:	:	kIx,
paní	paní	k1gFnSc1
Anna	Anna	k1gFnSc1
Maria	Maria	k1gFnSc1
Haßlauer	Haßlauer	k1gInSc4
z	z	k7c2
Haßlau	Haßlaus	k1gInSc2
u	u	k7c2
Thierbachu	Thierbach	k1gInSc2
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Salwart	Salwart	k1gInSc4
</s>
<s>
1661	#num#	k4
<g/>
:	:	kIx,
šlechtic	šlechtic	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Haslauer	Haslauer	k1gMnSc1
z	z	k7c2
Haslau	Haslaus	k1gInSc2
u	u	k7c2
Thierbachu	Thierbach	k1gInSc2
</s>
<s>
1667	#num#	k4
<g/>
:	:	kIx,
páter	páter	k1gMnSc1
Anton	Anton	k1gMnSc1
Melem	Melem	k?
</s>
<s>
1720	#num#	k4
<g/>
:	:	kIx,
paní	paní	k1gFnSc1
Regina	Regina	k1gFnSc1
Eckler	Eckler	k1gInSc4
</s>
<s>
1733	#num#	k4
<g/>
:	:	kIx,
páter	páter	k1gMnSc1
Johann	Johann	k1gMnSc1
Jakob	Jakob	k1gMnSc1
Langer	Langer	k1gMnSc1
</s>
<s>
1735	#num#	k4
<g/>
:	:	kIx,
páter	páter	k1gMnSc1
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
Ertl	Ertl	k1gMnSc1
</s>
<s>
1736	#num#	k4
<g/>
:	:	kIx,
Adam	Adam	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Hartigu	Hartig	k1gInSc2
</s>
<s>
1743	#num#	k4
<g/>
:	:	kIx,
kaplan	kaplan	k1gMnSc1
páter	páter	k1gMnSc1
Johann	Johann	k1gMnSc1
Hahn	Hahn	k1gMnSc1
</s>
<s>
1761	#num#	k4
<g/>
:	:	kIx,
páter	páter	k1gMnSc1
Anton	Anton	k1gMnSc1
Ignaz	Ignaz	k1gInSc4
Kirchner	Kirchner	k1gMnSc1
</s>
<s>
1779	#num#	k4
<g/>
:	:	kIx,
páter	páter	k1gMnSc1
Karel	Karel	k1gMnSc1
Eisenkolb	Eisenkolb	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
ovšem	ovšem	k9
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
orientovaný	orientovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
St.	st.	kA
Martin	Martin	k1gMnSc1
(	(	kIx(
<g/>
Nejdek	Nejdek	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martin	k1gMnSc2
<g/>
,	,	kIx,
Nejdek	Nejdek	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diecéze	diecéze	k1gFnSc1
plzeňská	plzeňská	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
sloup	sloup	k1gInSc1
se	s	k7c7
sousoším	sousoší	k1gNnSc7
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
PILZ	PILZ	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
města	město	k1gNnSc2
Nejdku	Nejdek	k1gInSc2
do	do	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Holeček	Holeček	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
<g/>
,	,	kIx,
Kosťová	kosťový	k2eAgFnSc1d1
Ingeborg	Ingeborg	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdek	Nejdek	k1gInSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Nejdek	Nejdek	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
338	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
1979	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Církve	církev	k1gFnSc2
a	a	k8xC
farnosti	farnost	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
218	#num#	k4
<g/>
-	-	kIx~
<g/>
237	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc2
města	město	k1gNnSc2
Nejdek	Nejdek	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Nejdek	Nejdek	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martina	k1gFnSc1
-	-	kIx~
popis	popis	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Nejdek	Nejdek	k1gInSc1
-	-	kIx~
kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnPc1
a	a	k8xC
příroda	příroda	k1gFnSc1
Karlovarska	Karlovarsko	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
hrady	hrad	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kv	kv	k?
<g/>
2013747699	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
296276943	#num#	k4
</s>
