<s>
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
úředně	úředně	k6eAd1	úředně
Íránská	íránský	k2eAgFnSc1d1	íránská
islámská	islámský	k2eAgFnSc1d1	islámská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
persky	persky	k6eAd1	persky
ا	ا	k?	ا
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Árjan	Árjan	k1gInSc1	Árjan
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
země	země	k1gFnSc1	země
Árjů	Árj	k1gMnPc2	Árj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
