<s>
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
úředně	úředně	k6eAd1	úředně
Íránská	íránský	k2eAgFnSc1d1	íránská
islámská	islámský	k2eAgFnSc1d1	islámská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
persky	persky	k6eAd1	persky
ا	ا	k?	ا
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Árjan	Árjan	k1gInSc1	Árjan
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
země	země	k1gFnSc1	země
Árjů	Árj	k1gMnPc2	Árj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
Arménií	Arménie	k1gFnSc7	Arménie
a	a	k8xC	a
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Turkmenistánem	Turkmenistán	k1gInSc7	Turkmenistán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
a	a	k8xC	a
Pákistánem	Pákistán	k1gInSc7	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
je	být	k5eAaImIp3nS	být
omýván	omývat	k5eAaImNgInS	omývat
vodami	voda	k1gFnPc7	voda
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Arabského	arabský	k2eAgNnSc2d1	arabské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
moře	moře	k1gNnSc1	moře
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
77	[number]	k4	77
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozlohou	rozloha	k1gFnSc7	rozloha
cca	cca	kA	cca
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
náleží	náležet	k5eAaImIp3nS	náležet
Írán	Írán	k1gInSc1	Írán
mezi	mezi	k7c4	mezi
20	[number]	k4	20
nejlidnatějších	lidnatý	k2eAgFnPc2d3	nejlidnatější
a	a	k8xC	a
nejrozlehlejších	rozlehlý	k2eAgFnPc2d3	nejrozlehlejší
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
Írán	Írán	k1gInSc1	Írán
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
dříve	dříve	k6eAd2	dříve
označovaném	označovaný	k2eAgNnSc6d1	označované
termínem	termín	k1gInSc7	termín
Persie	Persie	k1gFnPc1	Persie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dominantního	dominantní	k2eAgNnSc2d1	dominantní
etnika	etnikum	k1gNnSc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
poloha	poloha	k1gFnSc1	poloha
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
vymezená	vymezený	k2eAgFnSc1d1	vymezená
Kavkazem	Kavkaz	k1gInSc7	Kavkaz
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Arabským	arabský	k2eAgInSc7d1	arabský
poloostrovem	poloostrov	k1gInSc7	poloostrov
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Mezopotámií	Mezopotámie	k1gFnPc2	Mezopotámie
a	a	k8xC	a
Sýrií	Sýrie	k1gFnPc2	Sýrie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
natolik	natolik	k6eAd1	natolik
exponovaná	exponovaný	k2eAgFnSc1d1	exponovaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
učinila	učinit	k5eAaPmAgFnS	učinit
dějiště	dějiště	k1gNnSc4	dějiště
významných	významný	k2eAgInPc2d1	významný
historických	historický	k2eAgInPc2d1	historický
zvratů	zvrat	k1gInPc2	zvrat
a	a	k8xC	a
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
říše	říš	k1gFnPc4	říš
Médů	Méd	k1gMnPc2	Méd
a	a	k8xC	a
perských	perský	k2eAgMnPc2d1	perský
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
často	často	k6eAd1	často
píší	psát	k5eAaImIp3nP	psát
klasičtí	klasický	k2eAgMnPc1d1	klasický
řečtí	řecký	k2eAgMnPc1d1	řecký
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
teritorium	teritorium	k1gNnSc4	teritorium
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
makedonští	makedonský	k2eAgMnPc1d1	makedonský
Seleukovci	Seleukovec	k1gMnPc1	Seleukovec
<g/>
,	,	kIx,	,
parthští	parthský	k2eAgMnPc1d1	parthský
Arsakovci	Arsakovec	k1gMnPc1	Arsakovec
a	a	k8xC	a
perští	perský	k2eAgMnPc1d1	perský
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
Persii	Persie	k1gFnSc4	Persie
obsadili	obsadit	k5eAaPmAgMnP	obsadit
a	a	k8xC	a
islamizovali	islamizovat	k5eAaBmAgMnP	islamizovat
kočovní	kočovní	k2eAgMnPc1d1	kočovní
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
sjednocení	sjednocení	k1gNnSc1	sjednocení
vystoupením	vystoupení	k1gNnPc3	vystoupení
Mohamedovým	Mohamedův	k2eAgNnPc3d1	Mohamedovo
<g/>
.	.	kIx.	.
</s>
<s>
Arabští	arabský	k2eAgMnPc1d1	arabský
chalífové	chalífa	k1gMnPc1	chalífa
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Umajjovců	Umajjovec	k1gInPc2	Umajjovec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Abbásovců	Abbásovec	k1gInPc2	Abbásovec
začlenili	začlenit	k5eAaPmAgMnP	začlenit
íránské	íránský	k2eAgInPc4d1	íránský
kraje	kraj	k1gInPc4	kraj
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nadnárodní	nadnárodní	k2eAgFnSc2d1	nadnárodní
říše	říš	k1gFnSc2	říš
sahající	sahající	k2eAgMnSc1d1	sahající
od	od	k7c2	od
Maroka	Maroko	k1gNnSc2	Maroko
až	až	k9	až
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
stát	stát	k1gInSc1	stát
začal	začít	k5eAaPmAgInS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
–	–	k?	–
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
zhruba	zhruba	k6eAd1	zhruba
během	během	k7c2	během
sta	sto	k4xCgNnSc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
poté	poté	k6eAd1	poté
vládly	vládnout	k5eAaImAgFnP	vládnout
různé	různý	k2eAgFnPc1d1	různá
dynastie	dynastie	k1gFnPc1	dynastie
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Seldžukové	Seldžukový	k2eAgInPc1d1	Seldžukový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišli	přijít	k5eAaPmAgMnP	přijít
noví	nový	k2eAgMnPc1d1	nový
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
<g/>
:	:	kIx,	:
Mongolové	Mongol	k1gMnPc1	Mongol
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Čingischánem	Čingischán	k1gMnSc7	Čingischán
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
založili	založit	k5eAaPmAgMnP	založit
tzv.	tzv.	kA	tzv.
říši	říš	k1gFnSc3	říš
ílchánů	ílchán	k1gMnPc2	ílchán
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
i	i	k8xC	i
Irák	Irák	k1gInSc1	Irák
s	s	k7c7	s
částí	část	k1gFnSc7	část
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
po	po	k7c6	po
století	století	k1gNnSc6	století
následovalo	následovat	k5eAaImAgNnS	následovat
nové	nový	k2eAgNnSc1d1	nové
období	období	k1gNnSc1	období
partikularismu	partikularismus	k1gInSc2	partikularismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodné	přechodný	k2eAgFnSc6d1	přechodná
nadvládě	nadvláda	k1gFnSc6	nadvláda
Tamerlána	Tamerlán	k2eAgNnPc1d1	Tamerlán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vlastními	vlastní	k2eAgFnPc7d1	vlastní
tvůrci	tvůrce	k1gMnPc7	tvůrce
novodobého	novodobý	k2eAgInSc2d1	novodobý
Íránu	Írán	k1gInSc2	Írán
dynastie	dynastie	k1gFnSc2	dynastie
Safíovců	Safíovec	k1gMnPc2	Safíovec
a	a	k8xC	a
Kádžárovců	Kádžárovec	k1gMnPc2	Kádžárovec
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgMnPc2	jenž
také	také	k9	také
počíná	počínat	k5eAaImIp3nS	počínat
evropské	evropský	k2eAgNnSc4d1	Evropské
pronikání	pronikání	k1gNnSc4	pronikání
na	na	k7c4	na
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
mocností	mocnost	k1gFnPc2	mocnost
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
si	se	k3xPyFc3	se
Persii	Persie	k1gFnSc4	Persie
postupně	postupně	k6eAd1	postupně
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
na	na	k7c4	na
zájmové	zájmový	k2eAgFnPc4d1	zájmová
sféry	sféra	k1gFnPc4	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
ústřední	ústřední	k2eAgFnSc1d1	ústřední
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
nebyla	být	k5eNaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zabránit	zabránit	k5eAaPmF	zabránit
a	a	k8xC	a
ani	ani	k8xC	ani
perská	perský	k2eAgFnSc1d1	perská
revoluce	revoluce	k1gFnSc1	revoluce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemi	zem	k1gFnSc4	zem
zajistila	zajistit	k5eAaPmAgFnS	zajistit
parlamentarismus	parlamentarismus	k1gInSc4	parlamentarismus
<g/>
,	,	kIx,	,
nevedla	vést	k5eNaImAgFnS	vést
k	k	k7c3	k
důkladné	důkladný	k2eAgFnSc3d1	důkladná
reformě	reforma	k1gFnSc3	reforma
mnohdy	mnohdy	k6eAd1	mnohdy
archaických	archaický	k2eAgFnPc2d1	archaická
správních	správní	k2eAgFnPc2d1	správní
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
povstal	povstat	k5eAaPmAgMnS	povstat
proti	proti	k7c3	proti
kádžárovskému	kádžárovský	k2eAgMnSc3d1	kádžárovský
šáhu	šáh	k1gMnSc3	šáh
Ahmadovi	Ahmada	k1gMnSc3	Ahmada
kozácký	kozácký	k2eAgMnSc1d1	kozácký
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Rezá	Rezá	k1gFnSc1	Rezá
Chán	chán	k1gMnSc1	chán
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
soustředil	soustředit	k5eAaPmAgInS	soustředit
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
zvolit	zvolit	k5eAaPmF	zvolit
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
novým	nový	k2eAgMnSc7d1	nový
šáhem	šáh	k1gMnSc7	šáh
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Rezá	Rezá	k1gFnSc1	Rezá
Šáh	šáh	k1gMnSc1	šáh
Pahlaví	Pahlavý	k2eAgMnPc5d1	Pahlavý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
nastala	nastat	k5eAaPmAgFnS	nastat
překotná	překotný	k2eAgFnSc1d1	překotná
a	a	k8xC	a
místy	místy	k6eAd1	místy
neuvážená	uvážený	k2eNgFnSc1d1	neuvážená
modernizace	modernizace	k1gFnSc1	modernizace
země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
evropského	evropský	k2eAgInSc2d1	evropský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
prosazovaná	prosazovaný	k2eAgFnSc1d1	prosazovaná
diktátorskými	diktátorský	k2eAgFnPc7d1	diktátorská
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okupovaly	okupovat	k5eAaBmAgFnP	okupovat
Írán	Írán	k1gInSc1	Írán
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souviselo	souviset	k5eAaImAgNnS	souviset
jednak	jednak	k8xC	jednak
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
strategií	strategie	k1gFnSc7	strategie
Londýna	Londýn	k1gInSc2	Londýn
i	i	k8xC	i
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
s	s	k7c7	s
šáhovou	šáhův	k2eAgFnSc7d1	Šáhova
proněmeckou	proněmecký	k2eAgFnSc7d1	proněmecká
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
velmoci	velmoc	k1gFnPc1	velmoc
také	také	k9	také
dosadily	dosadit	k5eAaPmAgFnP	dosadit
Íráncům	Íránec	k1gMnPc3	Íránec
nového	nový	k2eAgMnSc4d1	nový
vládce	vládce	k1gMnSc4	vládce
–	–	k?	–
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Muhammada	Muhammada	k1gFnSc1	Muhammada
Rezu	rez	k1gInSc2	rez
Pahlavího	Pahlaví	k1gMnSc2	Pahlaví
<g/>
,	,	kIx,	,
absolventa	absolvent	k1gMnSc2	absolvent
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
okupačních	okupační	k2eAgNnPc2d1	okupační
vojsk	vojsko	k1gNnPc2	vojsko
se	se	k3xPyFc4	se
ve	v	k7c6	v
dnech	den	k1gInPc6	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
íránském	íránský	k2eAgNnSc6d1	íránské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Teheránská	teheránský	k2eAgFnSc1d1	Teheránská
konference	konference	k1gFnSc1	konference
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
Churchilla	Churchill	k1gMnSc2	Churchill
a	a	k8xC	a
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
Rezá	Rezá	k1gFnSc1	Rezá
Pahlaví	Pahlavý	k2eAgMnPc1d1	Pahlavý
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
umožnil	umožnit	k5eAaPmAgMnS	umožnit
jisté	jistý	k2eAgNnSc4d1	jisté
uvolnění	uvolnění	k1gNnSc4	uvolnění
politických	politický	k2eAgInPc2d1	politický
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
legální	legální	k2eAgFnSc6d1	legální
bázi	báze	k1gFnSc6	báze
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
vůdce	vůdce	k1gMnSc2	vůdce
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
Muhammad	Muhammad	k1gInSc1	Muhammad
Mosaddek	Mosaddek	k1gInSc1	Mosaddek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
podporu	podpora	k1gFnSc4	podpora
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
islamistů	islamista	k1gMnPc2	islamista
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
znárodnila	znárodnit	k5eAaPmAgFnS	znárodnit
íránský	íránský	k2eAgInSc4d1	íránský
ropný	ropný	k2eAgInSc4d1	ropný
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
kontrolovaný	kontrolovaný	k2eAgInSc4d1	kontrolovaný
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
století	století	k1gNnSc2	století
Brity	Brit	k1gMnPc4	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
krizi	krize	k1gFnSc4	krize
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
íránská	íránský	k2eAgFnSc1d1	íránská
krize	krize	k1gFnSc2	krize
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
ve	v	k7c4	v
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
zorganizovaný	zorganizovaný	k2eAgInSc4d1	zorganizovaný
šáhem	šáh	k1gMnSc7	šáh
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vládl	vládnout	k5eAaImAgInS	vládnout
Muhammad	Muhammad	k1gInSc1	Muhammad
Rezá	Rezá	k1gFnSc1	Rezá
Pahlaví	Pahlavý	k2eAgMnPc1d1	Pahlavý
diktátorsky	diktátorsky	k6eAd1	diktátorsky
pomocí	pomocí	k7c2	pomocí
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
SAVAK	SAVAK	kA	SAVAK
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zahraničněpoliticky	zahraničněpoliticky	k6eAd1	zahraničněpoliticky
se	se	k3xPyFc4	se
orientoval	orientovat	k5eAaBmAgInS	orientovat
na	na	k7c6	na
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
autokratické	autokratický	k2eAgFnPc1d1	autokratická
metody	metoda	k1gFnPc1	metoda
a	a	k8xC	a
přílišné	přílišný	k2eAgNnSc1d1	přílišné
prosazování	prosazování	k1gNnSc1	prosazování
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
monarchii	monarchie	k1gFnSc3	monarchie
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Íránská	íránský	k2eAgFnSc1d1	íránská
islámská	islámský	k2eAgFnSc1d1	islámská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Íránu	Írán	k1gInSc6	Írán
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
protivládní	protivládní	k2eAgFnSc2d1	protivládní
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přerostly	přerůst	k5eAaPmAgFnP	přerůst
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
skončily	skončit	k5eAaPmAgInP	skončit
svržením	svržení	k1gNnSc7	svržení
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Šáh	šáh	k1gMnSc1	šáh
musel	muset	k5eAaImAgMnS	muset
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1979	[number]	k4	1979
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
mocenské	mocenský	k2eAgNnSc4d1	mocenské
vakuum	vakuum	k1gNnSc4	vakuum
vyplnil	vyplnit	k5eAaPmAgMnS	vyplnit
charismatický	charismatický	k2eAgMnSc1d1	charismatický
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
ájatolláh	ájatolláh	k1gMnSc1	ájatolláh
Rúholláh	Rúholláh	k1gMnSc1	Rúholláh
Chomejní	Chomejní	k2eAgMnSc1d1	Chomejní
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
byly	být	k5eAaImAgInP	být
demontovány	demontovat	k5eAaBmNgInP	demontovat
všechny	všechen	k3xTgInPc1	všechen
pilíře	pilíř	k1gInPc1	pilíř
šáhovy	šáhův	k2eAgFnSc2d1	Šáhova
světské	světský	k2eAgFnSc2d1	světská
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
,	,	kIx,	,
referendem	referendum	k1gNnSc7	referendum
schváleno	schválit	k5eAaPmNgNnS	schválit
ustavení	ustavení	k1gNnSc1	ustavení
islámské	islámský	k2eAgFnSc2d1	islámská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
do	do	k7c2	do
ústavy	ústava	k1gFnSc2	ústava
zakomponovány	zakomponován	k2eAgInPc4d1	zakomponován
teokratické	teokratický	k2eAgInPc4d1	teokratický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Chomejní	Chomejní	k2eAgFnSc4d1	Chomejní
postupně	postupně	k6eAd1	postupně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
doktrínu	doktrína	k1gFnSc4	doktrína
"	"	kIx"	"
<g/>
o	o	k7c6	o
nadvládě	nadvláda	k1gFnSc6	nadvláda
teologa	teolog	k1gMnSc4	teolog
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k6eAd1	moc
převzalo	převzít	k5eAaPmAgNnS	převzít
šíitské	šíitský	k2eAgNnSc1d1	šíitské
duchovenstvo	duchovenstvo	k1gNnSc1	duchovenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
sekulární	sekulární	k2eAgMnSc1d1	sekulární
<g/>
,	,	kIx,	,
modernizující	modernizující	k2eAgMnSc1d1	modernizující
se	se	k3xPyFc4	se
Írán	Írán	k1gInSc1	Írán
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Šáha	šáh	k1gMnSc4	šáh
uvržen	uvržen	k2eAgMnSc1d1	uvržen
do	do	k7c2	do
islámské	islámský	k2eAgFnSc2d1	islámská
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
teokratů	teokrat	k1gMnPc2	teokrat
Zahraničněpoliticky	zahraničněpoliticky	k6eAd1	zahraničněpoliticky
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
režim	režim	k1gInSc1	režim
dostal	dostat	k5eAaPmAgInS	dostat
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
USA	USA	kA	USA
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Americká	americký	k2eAgFnSc1d1	americká
rukojmí	rukojmí	k1gMnPc4	rukojmí
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosavadním	dosavadní	k2eAgMnSc7d1	dosavadní
nejužším	úzký	k2eAgMnSc7d3	nejužší
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Irákem	Irák	k1gInSc7	Irák
ovládaným	ovládaný	k2eAgInSc7d1	ovládaný
světskou	světský	k2eAgFnSc7d1	světská
stranou	strana	k1gFnSc7	strana
Baas	Baasa	k1gFnPc2	Baasa
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
překročila	překročit	k5eAaPmAgFnS	překročit
irácká	irácký	k2eAgFnSc1d1	irácká
armáda	armáda	k1gFnSc1	armáda
hranice	hranice	k1gFnSc2	hranice
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
íránsko-irácká	íránskorácký	k2eAgFnSc1d1	íránsko-irácká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
trvala	trvat	k5eAaImAgFnS	trvat
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
zhruba	zhruba	k6eAd1	zhruba
milion	milion	k4xCgInSc4	milion
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgNnSc1d1	vnější
ohrožení	ohrožení	k1gNnSc1	ohrožení
Chomejnímu	Chomejní	k2eAgInSc3d1	Chomejní
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
omezit	omezit	k5eAaPmF	omezit
svobodu	svoboda	k1gFnSc4	svoboda
ve	v	k7c6	v
vnitropolitickém	vnitropolitický	k2eAgInSc6d1	vnitropolitický
smyslu	smysl	k1gInSc6	smysl
–	–	k?	–
popravy	poprava	k1gFnPc1	poprava
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Sestřelení	sestřelení	k1gNnSc1	sestřelení
íránského	íránský	k2eAgNnSc2d1	íránské
civilního	civilní	k2eAgNnSc2d1	civilní
letadla	letadlo	k1gNnSc2	letadlo
Iran	Iran	k1gInSc1	Iran
Air	Air	k1gFnSc4	Air
655	[number]	k4	655
křižníkem	křižník	k1gInSc7	křižník
USS	USS	kA	USS
Vincennes	Vincennes	k1gInSc1	Vincennes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
dále	daleko	k6eAd2	daleko
vyostřilo	vyostřit	k5eAaPmAgNnS	vyostřit
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
Chomejního	Chomejní	k2eAgInSc2d1	Chomejní
smrt	smrt	k1gFnSc4	smrt
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nevedly	vést	k5eNaImAgInP	vést
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
změně	změna	k1gFnSc3	změna
kursu	kurs	k1gInSc2	kurs
islamistického	islamistický	k2eAgNnSc2d1	islamistické
vedení	vedení	k1gNnSc2	vedení
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
dílčí	dílčí	k2eAgFnPc4d1	dílčí
korektury	korektura	k1gFnPc4	korektura
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
však	však	k9	však
přece	přece	k9	přece
jen	jen	k9	jen
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Alího	Alí	k1gMnSc2	Alí
Chameneího	Chameneí	k1gMnSc2	Chameneí
<g/>
,	,	kIx,	,
nového	nový	k2eAgMnSc2d1	nový
vůdce	vůdce	k1gMnSc2	vůdce
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
neměla	mít	k5eNaImAgFnS	mít
zdaleka	zdaleka	k6eAd1	zdaleka
takovou	takový	k3xDgFnSc4	takový
autoritu	autorita	k1gFnSc4	autorita
jako	jako	k8xC	jako
Chomejní	Chomejní	k2eAgInPc4d1	Chomejní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vzrůst	vzrůst	k5eAaPmF	vzrůst
reálné	reálný	k2eAgFnPc4d1	reálná
moci	moc	k1gFnPc4	moc
formální	formální	k2eAgFnPc4d1	formální
hlavy	hlava	k1gFnPc4	hlava
státu	stát	k1gInSc2	stát
–	–	k?	–
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
více	hodně	k6eAd2	hodně
angažovat	angažovat	k5eAaBmF	angažovat
lidé	člověk	k1gMnPc1	člověk
spíše	spíše	k9	spíše
pragmatičtí	pragmatický	k2eAgMnPc1d1	pragmatický
<g/>
,	,	kIx,	,
zaměření	zaměřený	k2eAgMnPc1d1	zaměřený
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
obnovu	obnova	k1gFnSc4	obnova
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
a	a	k8xC	a
sami	sám	k3xTgMnPc1	sám
Íránci	Íránec	k1gMnPc1	Íránec
projevovali	projevovat	k5eAaImAgMnP	projevovat
nyní	nyní	k6eAd1	nyní
mnohem	mnohem	k6eAd1	mnohem
hlasitěji	hlasitě	k6eAd2	hlasitě
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
–	–	k?	–
jejich	jejich	k3xOp3gNnSc2	jejich
mínění	mínění	k1gNnSc2	mínění
musel	muset	k5eAaImAgMnS	muset
brát	brát	k5eAaImF	brát
režim	režim	k1gInSc4	režim
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
umírněný	umírněný	k2eAgMnSc1d1	umírněný
duchovní	duchovní	k1gMnSc1	duchovní
Muhammad	Muhammad	k1gInSc4	Muhammad
Chátamí	Chátamí	k1gNnSc2	Chátamí
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gNnSc7	jehož
vedením	vedení	k1gNnSc7	vedení
započal	započnout	k5eAaPmAgInS	započnout
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
liberalizaci	liberalizace	k1gFnSc6	liberalizace
režimních	režimní	k2eAgFnPc2d1	režimní
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zmírněna	zmírněn	k2eAgFnSc1d1	zmírněna
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
,	,	kIx,	,
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
objevovaly	objevovat	k5eAaImAgInP	objevovat
kritické	kritický	k2eAgInPc1d1	kritický
hlasy	hlas	k1gInPc1	hlas
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
reformní	reformní	k2eAgFnPc1d1	reformní
strany	strana	k1gFnPc1	strana
navíc	navíc	k6eAd1	navíc
triumfovaly	triumfovat	k5eAaBmAgFnP	triumfovat
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgInPc1d1	konzervativní
kruhy	kruh	k1gInPc1	kruh
kladly	klást	k5eAaImAgInP	klást
tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
moc	moc	k1gFnSc1	moc
prezidenta	prezident	k1gMnSc2	prezident
i	i	k8xC	i
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
reálně	reálně	k6eAd1	reálně
docílit	docílit	k5eAaPmF	docílit
jen	jen	k9	jen
málo	málo	k4c4	málo
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
nakonec	nakonec	k6eAd1	nakonec
vládnoucí	vládnoucí	k2eAgMnPc1d1	vládnoucí
konzervativci	konzervativec	k1gMnPc1	konzervativec
eliminovali	eliminovat	k5eAaBmAgMnP	eliminovat
volebními	volební	k2eAgFnPc7d1	volební
manipulacemi	manipulace	k1gFnPc7	manipulace
oponenty	oponent	k1gMnPc7	oponent
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získali	získat	k5eAaPmAgMnP	získat
vítaného	vítaný	k2eAgMnSc4d1	vítaný
spojence	spojenec	k1gMnSc4	spojenec
v	v	k7c6	v
novém	nový	k2eAgMnSc6d1	nový
konzervativním	konzervativní	k2eAgMnSc6d1	konzervativní
prezidentu	prezident	k1gMnSc6	prezident
Mahmúdu	Mahmúd	k1gMnSc6	Mahmúd
Ahmadínežádovi	Ahmadínežád	k1gMnSc6	Ahmadínežád
<g/>
,	,	kIx,	,
známém	známý	k2eAgInSc6d1	známý
svými	svůj	k3xOyFgMnPc7	svůj
výpady	výpad	k1gInPc7	výpad
proti	proti	k7c3	proti
Izraeli	Izrael	k1gMnSc3	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
je	být	k5eAaImIp3nS	být
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
společnosti	společnost	k1gFnSc2	společnost
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
je	on	k3xPp3gMnPc4	on
politicky	politicky	k6eAd1	politicky
mohly	moct	k5eAaImAgInP	moct
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vztahů	vztah	k1gInPc2	vztah
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
napětí	napětí	k1gNnSc2	napětí
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Írán	Írán	k1gInSc1	Írán
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
íránským	íránský	k2eAgMnSc7d1	íránský
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Hasan	Hasan	k1gMnSc1	Hasan
Rúhání	Rúhání	k1gNnSc2	Rúhání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
porazil	porazit	k5eAaPmAgMnS	porazit
Mohammada	Mohammada	k1gFnSc1	Mohammada
Bagher	Baghra	k1gFnPc2	Baghra
Ghalibafa	Ghalibaf	k1gMnSc2	Ghalibaf
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Rúháního	Rúhání	k1gMnSc4	Rúhání
volební	volební	k2eAgNnSc1d1	volební
vítězství	vítězství	k1gNnSc1	vítězství
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
vztahy	vztah	k1gInPc4	vztah
Íránu	Írán	k1gInSc2	Írán
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
je	být	k5eAaImIp3nS	být
šestnáctou	šestnáctý	k4xOgFnSc7	šestnáctý
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
po	po	k7c6	po
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
před	před	k7c7	před
Libyí	Libye	k1gFnSc7	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Sdílí	sdílet	k5eAaImIp3nP	sdílet
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
(	(	kIx(	(
<g/>
432	[number]	k4	432
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arménií	Arménie	k1gFnSc7	Arménie
(	(	kIx(	(
<g/>
35	[number]	k4	35
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
s	s	k7c7	s
Turkmenistánem	Turkmenistán	k1gInSc7	Turkmenistán
(	(	kIx(	(
<g/>
992	[number]	k4	992
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
s	s	k7c7	s
Pákistánem	Pákistán	k1gInSc7	Pákistán
(	(	kIx(	(
<g/>
909	[number]	k4	909
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
(	(	kIx(	(
<g/>
936	[number]	k4	936
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
(	(	kIx(	(
<g/>
499	[number]	k4	499
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Irákem	Irák	k1gInSc7	Irák
(	(	kIx(	(
<g/>
1458	[number]	k4	1458
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hranice	hranice	k1gFnSc1	hranice
dále	daleko	k6eAd2	daleko
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Perský	perský	k2eAgInSc1d1	perský
a	a	k8xC	a
Ománský	ománský	k2eAgInSc1d1	ománský
záliv	záliv	k1gInSc1	záliv
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
Íránu	Írán	k1gInSc2	Írán
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
39	[number]	k4	39
<g/>
°	°	k?	°
<g/>
47	[number]	k4	47
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tak	tak	k9	tak
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k8xS	jako
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gFnSc1	Mallorca
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
25	[number]	k4	25
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloze	poloha	k1gFnSc3	poloha
města	město	k1gNnSc2	město
Dohá	Dohá	k1gFnSc1	Dohá
v	v	k7c6	v
Kataru	katar	k1gInSc6	katar
<g/>
.	.	kIx.	.
</s>
<s>
Íránské	íránský	k2eAgFnSc3d1	íránská
krajině	krajina	k1gFnSc3	krajina
dominují	dominovat	k5eAaImIp3nP	dominovat
rozlehlá	rozlehlý	k2eAgNnPc1d1	rozlehlé
horstva	horstvo	k1gNnPc1	horstvo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
spousta	spousta	k1gFnSc1	spousta
různých	různý	k2eAgFnPc2d1	různá
proláklin	proláklina	k1gFnPc2	proláklina
a	a	k8xC	a
náhorních	náhorní	k2eAgFnPc2d1	náhorní
plošin	plošina	k1gFnPc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Nejhornatější	hornatý	k2eAgFnSc1d3	nejhornatější
je	být	k5eAaImIp3nS	být
zalidněná	zalidněný	k2eAgFnSc1d1	zalidněná
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
s	s	k7c7	s
pohořími	pohoří	k1gNnPc7	pohoří
jako	jako	k8xS	jako
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
Zagros	Zagrosa	k1gFnPc2	Zagrosa
a	a	k8xC	a
Elborz	Elborza	k1gFnPc2	Elborza
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Íránu	Írán	k1gInSc2	Írán
<g/>
:	:	kIx,	:
Damávand	Damávand	k1gInSc1	Damávand
s	s	k7c7	s
5671	[number]	k4	5671
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavně	hlavně	k9	hlavně
neobydlené	obydlený	k2eNgFnPc4d1	neobydlená
pouště	poušť	k1gFnPc4	poušť
jako	jako	k8xS	jako
Dašt-e	Dašt-	k1gMnPc4	Dašt-
Kavír	Kavír	k1gInSc4	Kavír
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
zpestřené	zpestřený	k2eAgNnSc1d1	zpestřené
slaným	slaný	k2eAgNnSc7d1	slané
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
rozlehlejší	rozlehlý	k2eAgFnSc2d2	rozlehlejší
roviny	rovina	k1gFnSc2	rovina
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
je	být	k5eAaImIp3nS	být
především	především	k9	především
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
bezodtokových	bezodtokový	k2eAgNnPc2d1	bezodtokové
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
úplně	úplně	k6eAd1	úplně
vysychají	vysychat	k5eAaImIp3nP	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Írán	Írán	k1gInSc4	Írán
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
subtropické	subtropický	k2eAgNnSc1d1	subtropické
podnebí	podnebí	k1gNnSc1	podnebí
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kontinentalitou	kontinentalita	k1gFnSc7	kontinentalita
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
horké	horký	k2eAgNnSc1d1	horké
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
podnebí	podnebí	k1gNnSc1	podnebí
tropického	tropický	k2eAgInSc2d1	tropický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnSc2	srážka
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
ubývají	ubývat	k5eAaImIp3nP	ubývat
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
jich	on	k3xPp3gMnPc2	on
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
u	u	k7c2	u
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
ročně	ročně	k6eAd1	ročně
až	až	k9	až
2.000	[number]	k4	2.000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsušší	suchý	k2eAgInPc1d3	nejsušší
jsou	být	k5eAaImIp3nP	být
kraje	kraj	k1gInPc1	kraj
u	u	k7c2	u
slaných	slaný	k2eAgNnPc2d1	slané
jezer	jezero	k1gNnPc2	jezero
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
pouštích	poušť	k1gFnPc6	poušť
při	při	k7c6	při
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ojediněle	ojediněle	k6eAd1	ojediněle
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
mlhy	mlha	k1gFnSc2	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
a	a	k8xC	a
náhorních	náhorní	k2eAgFnPc6d1	náhorní
plošinách	plošina	k1gFnPc6	plošina
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
53	[number]	k4	53
%	%	kIx~	%
území	území	k1gNnSc6	území
Íránu	Írán	k1gInSc2	Írán
zabírají	zabírat	k5eAaImIp3nP	zabírat
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
27	[number]	k4	27
%	%	kIx~	%
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
,	,	kIx,	,
9	[number]	k4	9
%	%	kIx~	%
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
75.620	[number]	k4	75.620
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
zavlažováno	zavlažovat	k5eAaImNgNnS	zavlažovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
11	[number]	k4	11
%	%	kIx~	%
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
stromy	strom	k1gInPc7	strom
převažuje	převažovat	k5eAaImIp3nS	převažovat
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
jalovec	jalovec	k1gInSc1	jalovec
<g/>
,	,	kIx,	,
habr	habr	k1gInSc1	habr
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
a	a	k8xC	a
javor	javor	k1gInSc1	javor
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
myrta	myrta	k1gFnSc1	myrta
<g/>
,	,	kIx,	,
mandloň	mandloň	k1gFnSc1	mandloň
a	a	k8xC	a
ořešák	ořešák	k1gInSc1	ořešák
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
antilopa	antilopa	k1gFnSc1	antilopa
středoasijská	středoasijský	k2eAgFnSc1d1	středoasijská
<g/>
,	,	kIx,	,
gazela	gazela	k1gFnSc1	gazela
perská	perský	k2eAgFnSc1d1	perská
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
maral	maral	k1gMnSc1	maral
<g/>
,	,	kIx,	,
rys	rys	k1gMnSc1	rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
muflon	muflon	k1gMnSc1	muflon
<g/>
,	,	kIx,	,
koza	koza	k1gFnSc1	koza
bezoárová	bezoárový	k2eAgFnSc1d1	bezoárová
<g/>
,	,	kIx,	,
sysel	sysel	k1gMnSc1	sysel
<g/>
,	,	kIx,	,
frček	frčka	k1gFnPc2	frčka
<g/>
,	,	kIx,	,
dikobraz	dikobraz	k1gMnSc1	dikobraz
<g/>
,	,	kIx,	,
netopýr	netopýr	k1gMnSc1	netopýr
a	a	k8xC	a
varan	varan	k1gMnSc1	varan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
hadů	had	k1gMnPc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Ochraně	ochrana	k1gFnSc3	ochrana
podléhá	podléhat	k5eAaImIp3nS	podléhat
levhart	levhart	k1gMnSc1	levhart
perský	perský	k2eAgMnSc1d1	perský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
počtech	počet	k1gInPc6	počet
větších	veliký	k2eAgInPc6d2	veliký
než	než	k8xS	než
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
jiném	jiný	k2eAgInSc6d1	jiný
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
byli	být	k5eAaImAgMnP	být
zcela	zcela	k6eAd1	zcela
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
někteří	některý	k3yIgMnPc1	některý
typičtí	typický	k2eAgMnPc1d1	typický
zástupci	zástupce	k1gMnPc1	zástupce
savců	savec	k1gMnPc2	savec
–	–	k?	–
mj.	mj.	kA	mj.
i	i	k8xC	i
perské	perský	k2eAgNnSc1d1	perské
národní	národní	k2eAgNnSc1d1	národní
zvíře	zvíře	k1gNnSc1	zvíře
lev	lev	k1gInSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
podnikají	podnikat	k5eAaImIp3nP	podnikat
opatření	opatření	k1gNnPc1	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
1.649.771	[number]	k4	1.649.771
ha	ha	kA	ha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc1	první
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Rezervací	rezervace	k1gFnSc7	rezervace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
celkem	celkem	k6eAd1	celkem
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
je	být	k5eAaImIp3nS	být
Írán	Írán	k1gInSc4	Írán
islámskou	islámský	k2eAgFnSc7d1	islámská
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
titulární	titulární	k2eAgFnSc7d1	titulární
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
–	–	k?	–
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
Hasan	Hasan	k1gInSc1	Hasan
Rúhání	Rúhání	k1gNnSc1	Rúhání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc4d1	skutečná
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
rahbar	rahbara	k1gFnPc2	rahbara
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
duchovní	duchovní	k1gMnSc1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
volí	volit	k5eAaImIp3nS	volit
tzv.	tzv.	kA	tzv.
Rada	rada	k1gFnSc1	rada
expertů	expert	k1gMnPc2	expert
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
Sajjid	Sajjid	k1gInSc1	Sajjid
Alí	Alí	k1gMnSc1	Alí
Chameneí	Chameneí	k1gMnSc1	Chameneí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgMnSc1d1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
jmenovat	jmenovat	k5eAaBmF	jmenovat
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Radu	rada	k1gFnSc4	rada
dohlížitelů	dohlížitel	k1gMnPc2	dohlížitel
<g/>
,	,	kIx,	,
instituci	instituce	k1gFnSc4	instituce
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
jakýsi	jakýsi	k3yIgInSc4	jakýsi
ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
s	s	k7c7	s
rozšířenými	rozšířený	k2eAgFnPc7d1	rozšířená
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgMnSc1d1	volený
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
duchovní	duchovní	k2eAgMnPc4d1	duchovní
vůdce	vůdce	k1gMnPc4	vůdce
má	mít	k5eAaImIp3nS	mít
právo	práv	k2eAgNnSc1d1	právo
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
Islámské	islámský	k2eAgNnSc1d1	islámské
poradní	poradní	k2eAgNnSc1d1	poradní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Madžlis-e	Madžlis	k1gNnSc1	Madžlis-e
šúrá-je	šúrá	k1gFnSc2	šúrá-j
islámí	islámí	k1gNnSc2	islámí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
290	[number]	k4	290
členů	člen	k1gInPc2	člen
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
let	léto	k1gNnPc2	léto
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vždy	vždy	k6eAd1	vždy
dominovaly	dominovat	k5eAaImAgFnP	dominovat
konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
je	být	k5eAaImIp3nS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c6	na
výkonné	výkonný	k2eAgFnSc6d1	výkonná
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gMnSc4	on
zásahem	zásah	k1gInSc7	zásah
prezidenta	prezident	k1gMnSc2	prezident
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
občané	občan	k1gMnPc1	občan
již	již	k6eAd1	již
od	od	k7c2	od
15	[number]	k4	15
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
ještě	ještě	k9	ještě
86	[number]	k4	86
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
expertů	expert	k1gMnPc2	expert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
systému	systém	k1gInSc6	systém
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
právo	právo	k1gNnSc1	právo
šaría	šarí	k1gInSc2	šarí
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
spousta	spousta	k1gFnSc1	spousta
ohledů	ohled	k1gInPc2	ohled
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
madžlisu	madžlis	k1gInSc6	madžlis
(	(	kIx(	(
<g/>
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
Jako	jako	k8xC	jako
oficiální	oficiální	k2eAgInSc4d1	oficiální
cíl	cíl	k1gInSc4	cíl
íránská	íránský	k2eAgFnSc1d1	íránská
vláda	vláda	k1gFnSc1	vláda
uvádí	uvádět	k5eAaImIp3nS	uvádět
vytvoření	vytvoření	k1gNnSc4	vytvoření
Nového	Nového	k2eAgInSc2d1	Nového
světového	světový	k2eAgInSc2d1	světový
řádu	řád	k1gInSc2	řád
na	na	k7c6	na
základě	základ	k1gInSc6	základ
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
globálního	globální	k2eAgInSc2d1	globální
vojenského	vojenský	k2eAgInSc2d1	vojenský
paktu	pakt	k1gInSc2	pakt
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
31	[number]	k4	31
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
ostán	ostán	k1gInSc1	ostán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každou	každý	k3xTgFnSc4	každý
spravuje	spravovat	k5eAaImIp3nS	spravovat
guvernér	guvernér	k1gMnSc1	guvernér
(	(	kIx(	(
<g/>
ا	ا	k?	ا
<g/>
,	,	kIx,	,
ostándár	ostándár	k1gMnSc1	ostándár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
(	(	kIx(	(
<g/>
šahrestán	šahrestán	k1gInSc4	šahrestán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okrsky	okrsek	k1gInPc1	okrsek
(	(	kIx(	(
<g/>
bachš	bachš	k5eAaPmIp2nS	bachš
<g/>
)	)	kIx)	)
a	a	k8xC	a
podokrsky	podokrsky	k6eAd1	podokrsky
(	(	kIx(	(
<g/>
dehestán	dehestán	k1gInSc1	dehestán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
nejjižnější	jižní	k2eAgInPc4d3	nejjižnější
ostrovy	ostrov	k1gInPc4	ostrov
provincie	provincie	k1gFnSc2	provincie
Hormozgán	Hormozgán	k1gInSc1	Hormozgán
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
budování	budování	k1gNnSc2	budování
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
ztráty	ztráta	k1gFnPc4	ztráta
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
íránsko-irácké	íránskorácký	k2eAgFnSc2d1	íránsko-irácká
války	válka	k1gFnSc2	válka
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
amerických	americký	k2eAgMnPc2d1	americký
expertů	expert	k1gMnPc2	expert
zničil	zničit	k5eAaPmAgInS	zničit
Irák	Irák	k1gInSc1	Irák
během	během	k7c2	během
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
válčení	válčení	k1gNnSc6	válčení
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
íránských	íránský	k2eAgFnPc2d1	íránská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
kapacit	kapacita	k1gFnPc2	kapacita
<g/>
,	,	kIx,	,
technických	technický	k2eAgNnPc2d1	technické
i	i	k8xC	i
lidských	lidský	k2eAgNnPc2d1	lidské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
početního	početní	k2eAgInSc2d1	početní
stavu	stav	k1gInSc2	stav
úderných	úderný	k2eAgFnPc2d1	úderná
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
obnovy	obnova	k1gFnSc2	obnova
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
ukončen	ukončen	k2eAgInSc1d1	ukončen
a	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
týká	týkat	k5eAaImIp3nS	týkat
pozemních	pozemní	k2eAgInPc2d1	pozemní
obranných	obranný	k2eAgInPc2d1	obranný
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
jejich	jejich	k3xOp3gFnSc2	jejich
modernizace	modernizace	k1gFnSc2	modernizace
než	než	k8xS	než
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
íránského	íránský	k2eAgNnSc2d1	íránské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
ztráty	ztráta	k1gFnPc4	ztráta
za	za	k7c2	za
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
značné	značný	k2eAgInPc1d1	značný
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
opačná	opačný	k2eAgFnSc1d1	opačná
–	–	k?	–
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
Írán	Írán	k1gInSc1	Írán
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
regionálnímu	regionální	k2eAgInSc3d1	regionální
významu	význam	k1gInSc3	význam
dosud	dosud	k6eAd1	dosud
vyzbrojen	vyzbrojit	k5eAaPmNgMnS	vyzbrojit
nedostatečně	dostatečně	k6eNd1	dostatečně
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
je	být	k5eAaImIp3nS	být
embargo	embargo	k1gNnSc1	embargo
řady	řada	k1gFnSc2	řada
států	stát	k1gInPc2	stát
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
špičkové	špičkový	k2eAgFnSc2d1	špičková
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vládu	vláda	k1gFnSc4	vláda
nutí	nutit	k5eAaImIp3nP	nutit
nakupovat	nakupovat	k5eAaBmF	nakupovat
zbraně	zbraň	k1gFnPc4	zbraň
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
organizačního	organizační	k2eAgNnSc2d1	organizační
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
vojsko	vojsko	k1gNnSc1	vojsko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
regulérní	regulérní	k2eAgFnSc4d1	regulérní
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
arteš	artat	k5eAaBmIp2nS	artat
<g/>
,	,	kIx,	,
a	a	k8xC	a
revoluční	revoluční	k2eAgFnPc1d1	revoluční
gardy	garda	k1gFnPc1	garda
<g/>
,	,	kIx,	,
pásdárán	pásdárán	k2eAgInSc1d1	pásdárán
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
545	[number]	k4	545
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
ještě	ještě	k9	ještě
kolem	kolem	k7c2	kolem
350	[number]	k4	350
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
islámské	islámský	k2eAgFnSc2d1	islámská
revoluce	revoluce	k1gFnSc2	revoluce
duchovní	duchovní	k1gMnSc1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
a	a	k8xC	a
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
působí	působit	k5eAaImIp3nS	působit
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
grémiem	grémium	k1gNnSc7	grémium
zabývajícím	zabývající	k2eAgNnSc7d1	zabývající
se	se	k3xPyFc4	se
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
problematikou	problematika	k1gFnSc7	problematika
<g/>
;	;	kIx,	;
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
obrany	obrana	k1gFnSc2	obrana
přísluší	příslušet	k5eAaImIp3nS	příslušet
hlavně	hlavně	k9	hlavně
správní	správní	k2eAgFnPc4d1	správní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
prezident	prezident	k1gMnSc1	prezident
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
Ahmadínežád	Ahmadínežáda	k1gFnPc2	Ahmadínežáda
slavnostně	slavnostně	k6eAd1	slavnostně
otevřel	otevřít	k5eAaPmAgMnS	otevřít
vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
kontrolní	kontrolní	k2eAgNnSc4d1	kontrolní
středisko	středisko	k1gNnSc4	středisko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
první	první	k4xOgFnSc4	první
družici	družice	k1gFnSc4	družice
Omid	Omida	k1gFnPc2	Omida
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc4	první
družici	družice	k1gFnSc4	družice
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Írán	Írán	k1gInSc4	Írán
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
ruské	ruský	k2eAgFnPc4d1	ruská
výroby	výroba	k1gFnPc4	výroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Íránské	íránský	k2eAgNnSc1d1	íránské
hospodářství	hospodářství	k1gNnSc1	hospodářství
podléhá	podléhat	k5eAaImIp3nS	podléhat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
státní	státní	k2eAgInSc1d1	státní
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
,	,	kIx,	,
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rukou	ruka	k1gFnPc6	ruka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgInPc1d2	menší
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zpracovávající	zpracovávající	k2eAgNnPc1d1	zpracovávající
bohatá	bohatý	k2eAgNnPc1d1	bohaté
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc1	význam
ještě	ještě	k9	ještě
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
23,68	[number]	k4	23,68
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úroveň	úroveň	k1gFnSc1	úroveň
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
%	%	kIx~	%
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
včetně	včetně	k7c2	včetně
početného	početný	k2eAgInSc2d1	početný
správního	správní	k2eAgInSc2d1	správní
aparátu	aparát	k1gInSc2	aparát
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
45	[number]	k4	45
%	%	kIx~	%
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Íránský	íránský	k2eAgInSc4d1	íránský
jaderný	jaderný	k2eAgInSc4d1	jaderný
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
kapacita	kapacita	k1gFnSc1	kapacita
íránských	íránský	k2eAgFnPc2d1	íránská
elektráren	elektrárna	k1gFnPc2	elektrárna
41	[number]	k4	41
gigawatthodin	gigawatthodina	k1gFnPc2	gigawatthodina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k9	především
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
cenný	cenný	k2eAgInSc1d1	cenný
vývozní	vývozní	k2eAgInSc1d1	vývozní
artikl	artikl	k1gInSc1	artikl
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
výstavba	výstavba	k1gFnSc1	výstavba
cca	cca	kA	cca
20	[number]	k4	20
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
jaderného	jaderný	k2eAgNnSc2d1	jaderné
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prohlášení	prohlášení	k1gNnSc2	prohlášení
nového	nový	k2eAgMnSc2d1	nový
prezidenta	prezident	k1gMnSc2	prezident
Íránské	íránský	k2eAgFnSc2d1	íránská
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
Ferejdúna	Ferejdún	k1gMnSc2	Ferejdún
Abbásího	Abbásí	k1gMnSc2	Abbásí
Daváního	Davání	k1gMnSc2	Davání
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
2011	[number]	k4	2011
postaví	postavit	k5eAaPmIp3nS	postavit
Írán	Írán	k1gInSc1	Írán
v	v	k7c6	v
nejbližších	blízký	k2eAgNnPc6d3	nejbližší
letech	léto	k1gNnPc6	léto
čtyři	čtyři	k4xCgNnPc4	čtyři
až	až	k9	až
pět	pět	k4xCc4	pět
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
reaktorů	reaktor	k1gInPc2	reaktor
(	(	kIx(	(
<g/>
výkon	výkon	k1gInSc4	výkon
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
MW	MW	kA	MW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
spalovat	spalovat	k5eAaImF	spalovat
uran	uran	k1gInSc4	uran
obohacený	obohacený	k2eAgInSc4d1	obohacený
na	na	k7c4	na
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
postavit	postavit	k5eAaPmF	postavit
i	i	k9	i
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
obohacování	obohacování	k1gNnSc4	obohacování
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
těchto	tento	k3xDgNnPc2	tento
jaderných	jaderný	k2eAgNnPc2d1	jaderné
zařízení	zařízení	k1gNnPc2	zařízení
bude	být	k5eAaImBp3nS	být
získat	získat	k5eAaPmF	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Dostatek	dostatek	k1gInSc1	dostatek
uranu	uran	k1gInSc2	uran
pro	pro	k7c4	pro
trvalý	trvalý	k2eAgInSc4d1	trvalý
provoz	provoz	k1gInSc4	provoz
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
reaktoru	reaktor	k1gInSc2	reaktor
TRR	trr	k0	trr
v	v	k7c6	v
Teheránu	Teherán	k1gInSc2	Teherán
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
americká	americký	k2eAgFnSc1d1	americká
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
upravená	upravený	k2eAgFnSc1d1	upravená
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
paliva	palivo	k1gNnSc2	palivo
obohaceného	obohacený	k2eAgInSc2d1	obohacený
na	na	k7c4	na
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
palivových	palivový	k2eAgInPc2d1	palivový
článků	článek	k1gInPc2	článek
v	v	k7c6	v
Isfahánu	Isfahán	k1gInSc6	Isfahán
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
schopna	schopen	k2eAgFnSc1d1	schopna
zahájit	zahájit	k5eAaPmF	zahájit
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
íránském	íránský	k2eAgNnSc6d1	íránské
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
nachází	nacházet	k5eAaImIp3nS	nacházet
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
%	%	kIx~	%
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc4	zem
disponuje	disponovat	k5eAaBmIp3nS	disponovat
zásobami	zásoba	k1gFnPc7	zásoba
černého	černé	k1gNnSc2	černé
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
množství	množství	k1gNnSc6	množství
zhruba	zhruba	k6eAd1	zhruba
18	[number]	k4	18
miliard	miliarda	k4xCgFnPc2	miliarda
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
(	(	kIx(	(
<g/>
27	[number]	k4	27
bilionů	bilion	k4xCgInPc2	bilion
m3	m3	k4	m3
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
dokonce	dokonce	k9	dokonce
940	[number]	k4	940
bilionů	bilion	k4xCgInPc2	bilion
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těžbě	těžba	k1gFnSc6	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
byl	být	k5eAaImAgInS	být
Írán	Írán	k1gInSc1	Írán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
80	[number]	k4	80
miliard	miliarda	k4xCgFnPc2	miliarda
m3	m3	k4	m3
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tamních	tamní	k2eAgFnPc2d1	tamní
ropných	ropný	k2eAgFnPc2d1	ropná
polí	pole	k1gFnPc2	pole
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
a	a	k8xC	a
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
ale	ale	k9	ale
nechtěla	chtít	k5eNaImAgFnS	chtít
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
zisky	zisk	k1gInPc4	zisk
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
rovným	rovný	k2eAgInSc7d1	rovný
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Muhammad	Muhammad	k1gInSc1	Muhammad
Mosaddek	Mosaddek	k1gInSc1	Mosaddek
provedl	provést	k5eAaPmAgInS	provést
znárodnění	znárodnění	k1gNnSc4	znárodnění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgMnS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
převratem	převrat	k1gInSc7	převrat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
plánovaly	plánovat	k5eAaImAgFnP	plánovat
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jakmile	jakmile	k8xS	jakmile
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
ropného	ropný	k2eAgInSc2d1	ropný
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
postupně	postupně	k6eAd1	postupně
ložiska	ložisko	k1gNnPc4	ložisko
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
i	i	k8xC	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
velký	velký	k2eAgInSc4d1	velký
geopolitický	geopolitický	k2eAgInSc4d1	geopolitický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vážném	vážný	k2eAgNnSc6d1	vážné
ochromení	ochromení	k1gNnSc6	ochromení
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
druhého	druhý	k4xOgInSc2	druhý
ropného	ropný	k2eAgInSc2d1	ropný
šoku	šok	k1gInSc2	šok
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
islámské	islámský	k2eAgFnSc6d1	islámská
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
denní	denní	k2eAgFnSc4d1	denní
produkci	produkce	k1gFnSc4	produkce
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3,979	[number]	k4	3,979
milionů	milion	k4xCgInPc2	milion
barelů	barel	k1gInPc2	barel
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
obnáší	obnášet	k5eAaImIp3nS	obnášet
zhruba	zhruba	k6eAd1	zhruba
632,7	[number]	k4	632,7
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
1,425	[number]	k4	1,425
milionu	milion	k4xCgInSc2	milion
barelů	barel	k1gInPc2	barel
(	(	kIx(	(
<g/>
226,6	[number]	k4	226,6
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
či	či	k8xC	či
0,194	[number]	k4	0,194
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
(	(	kIx(	(
<g/>
ročně	ročně	k6eAd1	ročně
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
71	[number]	k4	71
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgInPc2d1	zbylý
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
barelů	barel	k1gInPc2	barel
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
ropy	ropa	k1gFnSc2	ropa
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
ztrojnásobila	ztrojnásobit	k5eAaPmAgFnS	ztrojnásobit
a	a	k8xC	a
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
způsobem	způsob	k1gInSc7	způsob
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
export	export	k1gInSc4	export
–	–	k?	–
výdaje	výdaj	k1gInPc4	výdaj
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
částky	částka	k1gFnSc2	částka
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
jsou	být	k5eAaImIp3nP	být
subvencovány	subvencován	k2eAgFnPc4d1	subvencována
ceny	cena	k1gFnPc4	cena
benzínu	benzín	k1gInSc2	benzín
(	(	kIx(	(
<g/>
litr	litr	k1gInSc1	litr
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
8	[number]	k4	8
eurocentů	eurocent	k1gInPc2	eurocent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
finanční	finanční	k2eAgFnSc3d1	finanční
krizi	krize	k1gFnSc3	krize
již	již	k6eAd1	již
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kiš	kiš	k0	kiš
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Hormozgán	Hormozgána	k1gFnPc2	Hormozgána
otevřena	otevřít	k5eAaPmNgFnS	otevřít
Íránská	íránský	k2eAgFnSc1d1	íránská
ropná	ropný	k2eAgFnSc1d1	ropná
burza	burza	k1gFnSc1	burza
(	(	kIx(	(
<g/>
IOB	IOB	kA	IOB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
petrodolarů	petrodolar	k1gInPc2	petrodolar
začíná	začínat	k5eAaImIp3nS	začínat
obchodovat	obchodovat	k5eAaImF	obchodovat
v	v	k7c6	v
petroeurech	petroeur	k1gInPc6	petroeur
–	–	k?	–
trend	trend	k1gInSc1	trend
<g/>
,	,	kIx,	,
související	související	k2eAgFnSc1d1	související
se	s	k7c7	s
stabilním	stabilní	k2eAgNnSc7d1	stabilní
postavením	postavení	k1gNnSc7	postavení
eura	euro	k1gNnSc2	euro
jako	jako	k8xC	jako
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
platidla	platidlo	k1gNnSc2	platidlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
krizí	krize	k1gFnSc7	krize
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
USA	USA	kA	USA
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
íránská	íránský	k2eAgFnSc1d1	íránská
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaná	využívaný	k2eAgFnSc1d1	využívaná
půda	půda	k1gFnSc1	půda
9	[number]	k4	9
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ovšem	ovšem	k9	ovšem
třetina	třetina	k1gFnSc1	třetina
ploch	plocha	k1gFnPc2	plocha
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
uměle	uměle	k6eAd1	uměle
zavlažována	zavlažován	k2eAgFnSc1d1	zavlažována
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnPc1	ovoce
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
datle	datle	k1gFnSc1	datle
<g/>
,	,	kIx,	,
vlna	vlna	k1gFnSc1	vlna
a	a	k8xC	a
kaviár	kaviár	k1gInSc1	kaviár
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
na	na	k7c6	na
200.000	[number]	k4	200.000
hektarech	hektar	k1gInPc6	hektar
půdy	půda	k1gFnSc2	půda
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
islámskému	islámský	k2eAgInSc3d1	islámský
zákazu	zákaz	k1gInSc3	zákaz
alkoholu	alkohol	k1gInSc2	alkohol
především	především	k9	především
stolní	stolní	k2eAgFnPc4d1	stolní
odrůdy	odrůda	k1gFnPc4	odrůda
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vína	víno	k1gNnSc2	víno
<g/>
;	;	kIx,	;
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
rovněž	rovněž	k9	rovněž
produkce	produkce	k1gFnSc1	produkce
rozinek	rozinka	k1gFnPc2	rozinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Írán	Írán	k1gInSc1	Írán
po	po	k7c6	po
Turecku	Turecko	k1gNnSc6	Turecko
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
exportérem	exportér	k1gMnSc7	exportér
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyvezl	vyvézt	k5eAaPmAgInS	vyvézt
Írán	Írán	k1gInSc1	Írán
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
produkty	produkt	k1gInPc4	produkt
a	a	k8xC	a
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
55,25	[number]	k4	55,25
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
směřovalo	směřovat	k5eAaImAgNnS	směřovat
18,4	[number]	k4	18,4
%	%	kIx~	%
exportu	export	k1gInSc2	export
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
9,7	[number]	k4	9,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
5,8	[number]	k4	5,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
5,4	[number]	k4	5,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
exportními	exportní	k2eAgInPc7d1	exportní
artikly	artikl	k1gInPc7	artikl
převažuje	převažovat	k5eAaImIp3nS	převažovat
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
vysoké	vysoký	k2eAgFnPc1d1	vysoká
ceny	cena	k1gFnPc1	cena
dovolovaly	dovolovat	k5eAaImAgFnP	dovolovat
íránské	íránský	k2eAgFnSc2d1	íránská
vládě	vláda	k1gFnSc3	vláda
subvencovat	subvencovat	k5eAaImF	subvencovat
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
schodek	schodek	k1gInSc4	schodek
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Íránský	íránský	k2eAgInSc1d1	íránský
import	import	k1gInSc1	import
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
částky	částka	k1gFnSc2	částka
zhruba	zhruba	k6eAd1	zhruba
42,5	[number]	k4	42,5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
dodavateli	dodavatel	k1gMnPc7	dodavatel
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
12,8	[number]	k4	12,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
8,3	[number]	k4	8,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
7,7	[number]	k4	7,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
7,2	[number]	k4	7,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
(	(	kIx(	(
<g/>
7,2	[number]	k4	7,2
%	%	kIx~	%
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
5,4	[number]	k4	5,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
íránský	íránský	k2eAgMnSc1d1	íránský
ministr	ministr	k1gMnSc1	ministr
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Sejed	Sejed	k1gMnSc1	Sejed
Šamsedí	Šamsedý	k2eAgMnPc1d1	Šamsedý
Hosejní	Hosejní	k2eAgNnSc1d1	Hosejní
<g/>
,	,	kIx,	,
že	že	k8xS	že
íránská	íránský	k2eAgFnSc1d1	íránská
republika	republika	k1gFnSc1	republika
hodlá	hodlat	k5eAaImIp3nS	hodlat
postupně	postupně	k6eAd1	postupně
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
obchodních	obchodní	k2eAgFnPc2d1	obchodní
transakcí	transakce	k1gFnPc2	transakce
západní	západní	k2eAgFnSc2d1	západní
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
euro	euro	k1gNnSc1	euro
a	a	k8xC	a
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ve	v	k7c6	v
zbavování	zbavování	k1gNnSc6	zbavování
se	se	k3xPyFc4	se
vlivu	vliv	k1gInSc6	vliv
dolaru	dolar	k1gInSc2	dolar
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dlouhodobější	dlouhodobý	k2eAgInSc4d2	dlouhodobější
trend	trend	k1gInSc4	trend
<g/>
.	.	kIx.	.
</s>
<s>
Íránci	Íránec	k1gMnPc1	Íránec
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dožívají	dožívat	k5eAaImIp3nP	dožívat
70,56	[number]	k4	70,56
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
72,07	[number]	k4	72,07
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
69,12	[number]	k4	69,12
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
25	[number]	k4	25
let	léto	k1gNnPc2	léto
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
24	[number]	k4	24
let	léto	k1gNnPc2	léto
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
celkový	celkový	k2eAgInSc1d1	celkový
průměr	průměr	k1gInSc1	průměr
25,8	[number]	k4	25,8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
0,66	[number]	k4	0,66
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
na	na	k7c4	na
rozvojovou	rozvojový	k2eAgFnSc4d1	rozvojová
zemi	zem	k1gFnSc4	zem
nízká	nízký	k2eAgFnSc1d1	nízká
porodnost	porodnost	k1gFnSc1	porodnost
(	(	kIx(	(
<g/>
úhrnná	úhrnný	k2eAgFnSc1d1	úhrnná
plodnost	plodnost	k1gFnSc1	plodnost
kolem	kolem	k7c2	kolem
1,6	[number]	k4	1,6
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ještě	ještě	k9	ještě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
6,5	[number]	k4	6,5
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prudký	prudký	k2eAgInSc1d1	prudký
pokles	pokles	k1gInSc1	pokles
byl	být	k5eAaImAgInS	být
zčásti	zčásti	k6eAd1	zčásti
způsoben	způsobit	k5eAaPmNgInS	způsobit
politikou	politika	k1gFnSc7	politika
íránské	íránský	k2eAgFnSc2d1	íránská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
např.	např.	kA	např.
rozdávala	rozdávat	k5eAaImAgFnS	rozdávat
zdarma	zdarma	k6eAd1	zdarma
kondomy	kondom	k1gInPc4	kondom
<g/>
,	,	kIx,	,
platila	platit	k5eAaImAgFnS	platit
vasektomie	vasektomie	k1gFnSc1	vasektomie
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
novomanželé	novomanžel	k1gMnPc1	novomanžel
museli	muset	k5eAaImAgMnP	muset
povinně	povinně	k6eAd1	povinně
navštívit	navštívit	k5eAaPmF	navštívit
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
obrátila	obrátit	k5eAaPmAgFnS	obrátit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
porodnost	porodnost	k1gFnSc1	porodnost
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
žijí	žít	k5eAaImIp3nP	žít
lidé	člověk	k1gMnPc1	člověk
mnoha	mnoho	k4c2	mnoho
vyznání	vyznání	k1gNnPc2	vyznání
a	a	k8xC	a
etnického	etnický	k2eAgInSc2d1	etnický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
stmelení	stmelení	k1gNnSc2	stmelení
perskou	perský	k2eAgFnSc7d1	perská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
starověké	starověký	k2eAgFnSc2d1	starověká
perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
složkou	složka	k1gFnSc7	složka
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Sedmdesát	sedmdesát	k4xCc1	sedmdesát
procent	procento	k1gNnPc2	procento
Íránců	Íránec	k1gMnPc2	Íránec
je	být	k5eAaImIp3nS	být
potomky	potomek	k1gMnPc7	potomek
árjských	árjský	k1gMnPc2	árjský
(	(	kIx(	(
<g/>
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
<g/>
)	)	kIx)	)
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
začaly	začít	k5eAaPmAgInP	začít
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Íránu	Írán	k1gInSc2	Írán
migrovat	migrovat	k5eAaImF	migrovat
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
hovoří	hovořit	k5eAaImIp3nS	hovořit
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
íránských	íránský	k2eAgInPc2d1	íránský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
perština	perština	k1gFnSc1	perština
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
Peršané	Peršan	k1gMnPc1	Peršan
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ázeři	Ázer	k1gMnPc1	Ázer
(	(	kIx(	(
<g/>
24	[number]	k4	24
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gílánci	Gílánek	k1gMnPc1	Gílánek
a	a	k8xC	a
Mázandaránci	Mázandaránek	k1gMnPc1	Mázandaránek
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kurdové	Kurd	k1gMnPc1	Kurd
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
(	(	kIx(	(
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lúrové	Lúrové	k2eAgFnSc1d1	Lúrové
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Turkmeni	Turkmen	k1gMnPc1	Turkmen
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Íránu	Írán	k1gInSc2	Írán
dramaticky	dramaticky	k6eAd1	dramaticky
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
překročil	překročit	k5eAaPmAgMnS	překročit
hranici	hranice	k1gFnSc4	hranice
77	[number]	k4	77
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
je	být	k5eAaImIp3nS	být
79	[number]	k4	79
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
mluví	mluvit	k5eAaImIp3nS	mluvit
perštinou	perština	k1gFnSc7	perština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
používají	používat	k5eAaImIp3nP	používat
jiné	jiný	k2eAgInPc1d1	jiný
íránské	íránský	k2eAgInPc1d1	íránský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
Indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
jiných	jiný	k2eAgNnPc2d1	jiné
etnik	etnikum	k1gNnPc2	etnikum
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Íránu	Írán	k1gInSc2	Írán
je	být	k5eAaImIp3nS	být
muslimského	muslimský	k2eAgNnSc2d1	muslimské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
90	[number]	k4	90
%	%	kIx~	%
Íránců	Íránec	k1gMnPc2	Íránec
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
šíitské	šíitský	k2eAgFnSc3d1	šíitská
větvi	větev	k1gFnSc3	větev
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
8	[number]	k4	8
%	%	kIx~	%
k	k	k7c3	k
sunnitské	sunnitský	k2eAgFnSc3d1	sunnitská
větvi	větev	k1gFnSc3	větev
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgNnPc4d1	zbylé
dvě	dva	k4xCgNnPc4	dva
procenta	procento	k1gNnPc4	procento
tvoří	tvořit	k5eAaImIp3nP	tvořit
vyznavači	vyznavač	k1gMnPc7	vyznavač
nemuslimských	muslimský	k2eNgNnPc2d1	nemuslimské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
:	:	kIx,	:
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
<g/>
,	,	kIx,	,
mandaeismu	mandaeismus	k1gInSc2	mandaeismus
<g/>
,	,	kIx,	,
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
,	,	kIx,	,
jazídismu	jazídismus	k1gInSc2	jazídismus
<g/>
,	,	kIx,	,
jarsánismu	jarsánismus	k1gInSc2	jarsánismus
<g/>
,	,	kIx,	,
zoroastrismu	zoroastrismus	k1gInSc2	zoroastrismus
<g/>
,	,	kIx,	,
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
tři	tři	k4xCgNnPc1	tři
zmíněná	zmíněný	k2eAgNnPc1d1	zmíněné
náboženství	náboženství	k1gNnPc1	náboženství
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
a	a	k8xC	a
chráněná	chráněný	k2eAgFnSc1d1	chráněná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vyhrazená	vyhrazený	k2eAgNnPc4d1	vyhrazené
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
íránském	íránský	k2eAgInSc6d1	íránský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Íránská	íránský	k2eAgFnSc1d1	íránská
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Izrael	Izrael	k1gInSc4	Izrael
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
navzdory	navzdory	k7c3	navzdory
několika	několik	k4yIc2	několik
vlnám	vlna	k1gFnPc3	vlna
emigrace	emigrace	k1gFnSc2	emigrace
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
šáha	šáh	k1gMnSc2	šáh
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
populace	populace	k1gFnSc1	populace
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
25	[number]	k4	25
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
nepanuje	panovat	k5eNaImIp3nS	panovat
taková	takový	k3xDgFnSc1	takový
nenávist	nenávist	k1gFnSc1	nenávist
vůči	vůči	k7c3	vůči
Židům	Žid	k1gMnPc3	Žid
(	(	kIx(	(
<g/>
i	i	k9	i
přes	přes	k7c4	přes
antisionistickou	antisionistický	k2eAgFnSc4d1	antisionistická
rétoriku	rétorika	k1gFnSc4	rétorika
íránské	íránský	k2eAgFnSc2d1	íránská
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
vláda	vláda	k1gFnSc1	vláda
oficiálně	oficiálně	k6eAd1	oficiálně
neuznává	uznávat	k5eNaImIp3nS	uznávat
existenci	existence	k1gFnSc4	existence
nevěřících	věřící	k2eNgMnPc2d1	nevěřící
Íránců	Íránec	k1gMnPc2	Íránec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
opravdové	opravdový	k2eAgNnSc1d1	opravdové
zastoupení	zastoupení	k1gNnSc1	zastoupení
náboženského	náboženský	k2eAgNnSc2d1	náboženské
rozdělení	rozdělení	k1gNnSc2	rozdělení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
nevěřících	nevěřící	k1gMnPc2	nevěřící
<g/>
,	,	kIx,	,
spiritualistů	spiritualista	k1gMnPc2	spiritualista
<g/>
,	,	kIx,	,
ateistů	ateista	k1gMnPc2	ateista
<g/>
,	,	kIx,	,
agnostiků	agnostik	k1gMnPc2	agnostik
a	a	k8xC	a
odpadlíků	odpadlík	k1gMnPc2	odpadlík
od	od	k7c2	od
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tito	tento	k3xDgMnPc1	tento
zahrnováni	zahrnovat	k5eAaImNgMnP	zahrnovat
do	do	k7c2	do
vládní	vládní	k2eAgFnSc2d1	vládní
statistiky	statistika	k1gFnSc2	statistika
99	[number]	k4	99
<g/>
%	%	kIx~	%
muslimské	muslimský	k2eAgFnSc2d1	muslimská
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
je	být	k5eAaImIp3nS	být
Persie	Persie	k1gFnSc1	Persie
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
díky	díky	k7c3	díky
svým	svůj	k3xOyFgMnPc3	svůj
básníkům	básník	k1gMnPc3	básník
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Firdausího	Firdausí	k1gMnSc4	Firdausí
<g/>
,	,	kIx,	,
Háfize	Háfize	k1gFnSc1	Háfize
či	či	k8xC	či
Sa	Sa	k1gMnSc1	Sa
<g/>
'	'	kIx"	'
<g/>
dího	dího	k1gMnSc1	dího
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pohádky	pohádka	k1gFnPc4	pohádka
Tisíce	tisíc	k4xCgInPc1	tisíc
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
spojuje	spojovat	k5eAaImIp3nS	spojovat
většina	většina	k1gFnSc1	většina
Evropanů	Evropan	k1gMnPc2	Evropan
jednoznačně	jednoznačně	k6eAd1	jednoznačně
s	s	k7c7	s
perským	perský	k2eAgNnSc7d1	perské
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jejich	jejich	k3xOp3gFnSc1	jejich
nejstarší	starý	k2eAgFnSc1d3	nejstarší
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
–	–	k?	–
Peršané	Peršan	k1gMnPc1	Peršan
jen	jen	k6eAd1	jen
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
a	a	k8xC	a
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
perská	perský	k2eAgFnSc1d1	perská
miniatura	miniatura	k1gFnSc1	miniatura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvláště	zvláště	k9	zvláště
za	za	k7c2	za
Safíovců	Safíovec	k1gMnPc2	Safíovec
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
dokonalosti	dokonalost	k1gFnSc3	dokonalost
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
arabských	arabský	k2eAgInPc2d1	arabský
i	i	k8xC	i
perských	perský	k2eAgInPc2d1	perský
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
stylové	stylový	k2eAgInPc4d1	stylový
prvky	prvek	k1gInPc4	prvek
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
až	až	k9	až
po	po	k7c4	po
středoasijské	středoasijský	k2eAgFnPc4d1	středoasijská
republiky	republika	k1gFnPc4	republika
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Perské	perský	k2eAgNnSc1d1	perské
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Perská	perský	k2eAgFnSc1d1	perská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
íránská	íránský	k2eAgFnSc1d1	íránská
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
zapsala	zapsat	k5eAaPmAgFnS	zapsat
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ji	on	k3xPp3gFnSc4	on
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
takové	takový	k3xDgFnPc4	takový
osobnosti	osobnost	k1gFnPc4	osobnost
jako	jako	k8xS	jako
např.	např.	kA	např.
Nímá	Nímá	k1gFnSc1	Nímá
Júšídž	Júšídž	k1gFnSc1	Júšídž
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novátorský	novátorský	k2eAgMnSc1d1	novátorský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
časomíru	časomíra	k1gFnSc4	časomíra
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
volný	volný	k2eAgInSc4d1	volný
verš	verš	k1gInSc4	verš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
u	u	k7c2	u
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
populární	populární	k2eAgFnSc1d1	populární
autorka	autorka	k1gFnSc1	autorka
komiksů	komiks	k1gInPc2	komiks
Marjane	Marjan	k1gMnSc5	Marjan
Satrapiová	Satrapiový	k2eAgNnPc1d1	Satrapiové
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
komiks	komiks	k1gInSc1	komiks
Persepolis	Persepolis	k1gFnSc1	Persepolis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
i	i	k9	i
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
počátky	počátek	k1gInPc1	počátek
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
režiséra	režisér	k1gMnSc2	režisér
Abdulláha	Abdulláh	k1gMnSc2	Abdulláh
Sepanty	Sepanta	k1gFnSc2	Sepanta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
ceněna	cenit	k5eAaImNgFnS	cenit
více	hodně	k6eAd2	hodně
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
než	než	k8xS	než
doma	doma	k6eAd1	doma
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
produkce	produkce	k1gFnSc1	produkce
má	mít	k5eAaImIp3nS	mít
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
filmů	film	k1gInPc2	film
spíše	spíše	k9	spíše
klesající	klesající	k2eAgFnSc1d1	klesající
úroveň	úroveň	k1gFnSc1	úroveň
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
14	[number]	k4	14
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natáčelo	natáčet	k5eAaImAgNnS	natáčet
kolem	kolem	k7c2	kolem
70	[number]	k4	70
filmů	film	k1gInPc2	film
ročně	ročně	k6eAd1	ročně
<g/>
;	;	kIx,	;
kin	kino	k1gNnPc2	kino
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1	islámská
revoluce	revoluce	k1gFnSc1	revoluce
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
žánr	žánr	k1gInSc4	žánr
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
poválečným	poválečný	k2eAgFnPc3d1	poválečná
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
íránského	íránský	k2eAgInSc2d1	íránský
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Ismá	Ismá	k1gFnSc1	Ismá
<g/>
'	'	kIx"	'
<g/>
íl	íl	k?	íl
Kúšán	Kúšán	k1gMnSc1	Kúšán
a	a	k8xC	a
Farroch	Farroch	k1gMnSc1	Farroch
Ghaffárí	Ghaffárí	k1gMnSc1	Ghaffárí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgInS	být
Írán	Írán	k1gInSc1	Írán
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vystavován	vystavovat	k5eAaImNgMnS	vystavovat
západním	západní	k2eAgInPc3d1	západní
vlivům	vliv	k1gInPc3	vliv
–	–	k?	–
první	první	k4xOgFnSc4	první
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hudbu	hudba	k1gFnSc4	hudba
evropského	evropský	k2eAgInSc2d1	evropský
střihu	střih	k1gInSc2	střih
měli	mít	k5eAaImAgMnP	mít
již	již	k6eAd1	již
kádžárovští	kádžárovský	k2eAgMnPc1d1	kádžárovský
šáhové	šáh	k1gMnPc1	šáh
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnSc1d1	moderní
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
až	až	k9	až
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
po	po	k7c6	po
islámské	islámský	k2eAgFnSc6d1	islámská
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
rychle	rychle	k6eAd1	rychle
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
nejznámější	známý	k2eAgFnSc7d3	nejznámější
představitelkou	představitelka	k1gFnSc7	představitelka
byla	být	k5eAaImAgFnS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kúkúš	Kúkúš	k1gMnSc1	Kúkúš
(	(	kIx(	(
<g/>
Gúgúš	Gúgúš	k1gMnSc1	Gúgúš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
populární	populární	k2eAgInSc4d1	populární
koncem	koncem	k7c2	koncem
let	léto	k1gNnPc2	léto
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
působících	působící	k2eAgFnPc2d1	působící
zprvu	zprvu	k6eAd1	zprvu
uvnitř	uvnitř	k7c2	uvnitř
podzemní	podzemní	k2eAgFnSc2d1	podzemní
subkultury	subkultura	k1gFnSc2	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Vládě	Vláďa	k1gFnSc3	Vláďa
nakonec	nakonec	k6eAd1	nakonec
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
se	se	k3xPyFc4	se
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
některých	některý	k3yIgFnPc2	některý
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
smířit	smířit	k5eAaPmF	smířit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xS	jako
např.	např.	kA	např.
127	[number]	k4	127
a	a	k8xC	a
The	The	k1gMnSc1	The
Technicolor	Technicolora	k1gFnPc2	Technicolora
Dream	Dream	k1gInSc1	Dream
již	již	k6eAd1	již
natočily	natočit	k5eAaBmAgFnP	natočit
i	i	k9	i
písně	píseň	k1gFnPc1	píseň
nazpívané	nazpívaný	k2eAgFnPc1d1	nazpívaná
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
íránský	íránský	k2eAgMnSc1d1	íránský
heavy	heava	k1gFnPc4	heava
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
druhem	druh	k1gInSc7	druh
kolektivního	kolektivní	k2eAgInSc2d1	kolektivní
sportu	sport	k1gInSc2	sport
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
však	však	k9	však
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jediný	jediný	k2eAgInSc4d1	jediný
zápas	zápas	k1gInSc4	zápas
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
s	s	k7c7	s
USA	USA	kA	USA
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
Íránci	Íránec	k1gMnPc1	Íránec
dvakrát	dvakrát	k6eAd1	dvakrát
remizovali	remizovat	k5eAaPmAgMnP	remizovat
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
se	s	k7c7	s
Skotskem	Skotsko	k1gNnSc7	Skotsko
a	a	k8xC	a
2006	[number]	k4	2006
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Angolou	Angola	k1gFnSc7	Angola
<g/>
)	)	kIx)	)
a	a	k8xC	a
šestkrát	šestkrát	k6eAd1	šestkrát
prohráli	prohrát	k5eAaPmAgMnP	prohrát
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
a	a	k8xC	a
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
s	s	k7c7	s
Peru	Peru	k1gNnSc7	Peru
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Írán	Írán	k1gInSc1	Írán
mistrovství	mistrovství	k1gNnSc2	mistrovství
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
a	a	k8xC	a
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
disciplín	disciplína	k1gFnPc2	disciplína
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgInPc1d1	populární
zejména	zejména	k9	zejména
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc4	basketbal
a	a	k8xC	a
vodní	vodní	k2eAgNnSc4d1	vodní
pólo	pólo	k1gNnSc4	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volejbalu	volejbal	k1gInSc6	volejbal
se	se	k3xPyFc4	se
Íráncům	Íránec	k1gMnPc3	Íránec
nejnověji	nově	k6eAd3	nově
podařila	podařit	k5eAaPmAgFnS	podařit
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
mužstvo	mužstvo	k1gNnSc1	mužstvo
však	však	k9	však
při	při	k7c6	při
utkáních	utkání	k1gNnPc6	utkání
nezaznamenalo	zaznamenat	k5eNaPmAgNnS	zaznamenat
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
60	[number]	k4	60
%	%	kIx~	%
íránské	íránský	k2eAgFnSc2d1	íránská
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
aglomerace	aglomerace	k1gFnPc1	aglomerace
trpí	trpět	k5eAaImIp3nP	trpět
přelidněností	přelidněnost	k1gFnSc7	přelidněnost
<g/>
,	,	kIx,	,
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
obslužností	obslužnost	k1gFnSc7	obslužnost
a	a	k8xC	a
smogem	smog	k1gInSc7	smog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
metropoli	metropol	k1gFnSc6	metropol
Teheránu	Teherán	k1gInSc2	Teherán
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
svými	svůj	k3xOyFgFnPc7	svůj
téměř	téměř	k6eAd1	téměř
8	[number]	k4	8
miliony	milion	k4xCgInPc7	milion
obyvateli	obyvatel	k1gMnPc7	obyvatel
zdaleka	zdaleka	k6eAd1	zdaleka
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
Teheránu	Teherán	k1gInSc2	Teherán
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
středisky	středisko	k1gNnPc7	středisko
Mašhad	Mašhad	k1gInSc1	Mašhad
<g/>
,	,	kIx,	,
Isfahán	Isfahán	k1gInSc1	Isfahán
<g/>
,	,	kIx,	,
Tabríz	tabríz	k1gInSc1	tabríz
a	a	k8xC	a
Šíráz	Šíráz	k1gInSc1	Šíráz
<g/>
.	.	kIx.	.
</s>
<s>
AXWORTHY	AXWORTHY	kA	AXWORTHY
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788071069942	[number]	k4	9788071069942
<g/>
.	.	kIx.	.
</s>
<s>
BAER	BAER	kA	BAER
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
naložit	naložit	k5eAaPmF	naložit
s	s	k7c7	s
ďáblem	ďábel	k1gMnSc7	ďábel
<g/>
:	:	kIx,	:
íránská	íránský	k2eAgFnSc1d1	íránská
velmoc	velmoc	k1gFnSc1	velmoc
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
,	,	kIx,	,
VOLVOX	VOLVOX	kA	VOLVOX
GLOBATOR	GLOBATOR	kA	GLOBATOR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
CVRKAL	cvrkat	k5eAaImAgMnS	cvrkat
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
337	[number]	k4	337
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
Arijců	Arijce	k1gMnPc2	Arijce
:	:	kIx,	:
nový	nový	k2eAgInSc4d1	nový
Iran	Iran	k1gInSc4	Iran
:	:	kIx,	:
nový	nový	k2eAgInSc4d1	nový
Afghanistan	Afghanistan	k1gInSc4	Afghanistan
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
291	[number]	k4	291
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
Perská	perský	k2eAgFnSc1d1	perská
říše	říše	k1gFnSc1	říše
Chronologie	chronologie	k1gFnSc2	chronologie
starověkých	starověký	k2eAgFnPc2d1	starověká
íránských	íránský	k2eAgFnPc2d1	íránská
dějin	dějiny	k1gFnPc2	dějiny
Ší	Ší	k1gFnSc2	Ší
<g/>
'	'	kIx"	'
<g/>
itský	itský	k2eAgInSc1d1	itský
islám	islám	k1gInSc1	islám
Islám	islám	k1gInSc1	islám
Íránská	íránský	k2eAgFnSc1d1	íránská
ropná	ropný	k2eAgFnSc1d1	ropná
burza	burza	k1gFnSc1	burza
Íránská	íránský	k2eAgFnSc1d1	íránská
islámská	islámský	k2eAgFnSc1d1	islámská
revoluce	revoluce	k1gFnSc1	revoluce
Americká	americký	k2eAgFnSc1d1	americká
rukojmí	rukojmí	k1gNnPc2	rukojmí
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
Irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
Aféra	aféra	k1gFnSc1	aféra
Írán-Contras	Írán-Contras	k1gInSc4	Írán-Contras
Perská	perský	k2eAgFnSc1d1	perská
revoluce	revoluce	k1gFnSc1	revoluce
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Írán	Írán	k1gInSc1	Írán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Írán	Írán	k1gInSc1	Írán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
<g />
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Írán	Írán	k1gInSc1	Írán
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k6eAd1	plus
Velký	velký	k2eAgMnSc1d1	velký
architekt	architekt	k1gMnSc1	architekt
islámské	islámský	k2eAgFnSc2d1	islámská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
podrobných	podrobný	k2eAgFnPc2d1	podrobná
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
Chomejního	Chomejní	k2eAgMnSc4d1	Chomejní
životě	život	k1gInSc6	život
a	a	k8xC	a
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
náboženských	náboženský	k2eAgFnPc6d1	náboženská
i	i	k8xC	i
politických	politický	k2eAgFnPc6d1	politická
koncepcích	koncepce	k1gFnPc6	koncepce
<g/>
,	,	kIx,	,
pořadem	pořad	k1gInSc7	pořad
provází	provázet	k5eAaImIp3nS	provázet
íránistka	íránistka	k1gFnSc1	íránistka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Kříhová	Kříhová	k1gFnSc1	Kříhová
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Menší	malý	k2eAgInSc1d2	menší
geografický	geografický	k2eAgInSc1d1	geografický
přehled	přehled	k1gInSc1	přehled
Stránky	stránka	k1gFnSc2	stránka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
,	,	kIx,	,
persky	persky	k6eAd1	persky
<g/>
)	)	kIx)	)
Národnostně-náboženská	Národnostněáboženský	k2eAgFnSc1d1	Národnostně-náboženský
mapa	mapa	k1gFnSc1	mapa
Íránu	Írán	k1gInSc2	Írán
Petice	petice	k1gFnSc2	petice
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
násilí	násilí	k1gNnSc2	násilí
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Svoboda	svoboda	k1gFnSc1	svoboda
pro	pro	k7c4	pro
Írán	Írán	k1gInSc4	Írán
–	–	k?	–
stránky	stránka	k1gFnSc2	stránka
opozice	opozice	k1gFnSc2	opozice
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Crisis	Crisis	k1gFnSc1	Crisis
Guide	Guid	k1gInSc5	Guid
<g/>
:	:	kIx,	:
<g/>
Iran	Iran	k1gInSc4	Iran
<g/>
,	,	kIx,	,
Council	Council	k1gInSc4	Council
on	on	k3xPp3gMnSc1	on
Foreign	Foreign	k1gMnSc1	Foreign
Relations	Relationsa	k1gFnPc2	Relationsa
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Information	Information	k1gInSc1	Information
Report	report	k1gInSc1	report
-	-	kIx~	-
Iran	Iran	k1gInSc1	Iran
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Border	Border	k1gMnSc1	Border
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Iran	Iran	k1gInSc1	Iran
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Iran	Iran	k1gInSc1	Iran
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Iran	Iran	k1gInSc1	Iran
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
Near	Near	k1gMnSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Iran	Iran	k1gInSc1	Iran
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Iran	Iran	k1gInSc1	Iran
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Iran	Iran	k1gInSc4	Iran
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Írán	Írán	k1gInSc1	Írán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
AFARY	AFARY	kA	AFARY
<g/>
,	,	kIx,	,
Janet	Janet	k1gMnSc1	Janet
<g/>
;	;	kIx,	;
AVERY	AVERY	kA	AVERY
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
William	William	k1gInSc1	William
<g/>
;	;	kIx,	;
MOSTOFI	MOSTOFI	kA	MOSTOFI
<g/>
,	,	kIx,	,
Khosrow	Khosrow	k1gFnSc1	Khosrow
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Iran	Iran	k1gInSc1	Iran
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
