<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc4	svátek
začátku	začátek	k1gInSc2	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
celosvětově	celosvětově	k6eAd1	celosvětově
nejrozšířenějšího	rozšířený	k2eAgInSc2d3	nejrozšířenější
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
svět	svět	k1gInSc1	svět
slaví	slavit	k5eAaImIp3nS	slavit
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k9	i
začátek	začátek	k1gInSc1	začátek
nového	nový	k2eAgInSc2d1	nový
–	–	k?	–
Silvestrovské	silvestrovský	k2eAgFnPc4d1	silvestrovská
oslavy	oslava	k1gFnPc4	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
si	se	k3xPyFc3	se
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnPc1	rodina
i	i	k8xC	i
firmy	firma	k1gFnPc1	firma
posílají	posílat	k5eAaImIp3nP	posílat
blahopřejné	blahopřejný	k2eAgFnPc1d1	blahopřejná
novoročenky	novoročenka	k1gFnPc1	novoročenka
<g/>
,	,	kIx,	,
s	s	k7c7	s
hezkým	hezký	k2eAgInSc7d1	hezký
či	či	k8xC	či
vtipným	vtipný	k2eAgInSc7d1	vtipný
obrázkem	obrázek	k1gInSc7	obrázek
a	a	k8xC	a
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
štěstí	štěstí	k1gNnSc2	štěstí
v	v	k7c6	v
nadcházejícím	nadcházející	k2eAgInSc6d1	nadcházející
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
kalendářů	kalendář	k1gInPc2	kalendář
připadají	připadat	k5eAaPmIp3nP	připadat
oslavy	oslava	k1gFnPc1	oslava
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
dny	den	k1gInPc4	den
<g/>
:	:	kIx,	:
v	v	k7c6	v
juliánském	juliánský	k2eAgInSc6d1	juliánský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
používá	používat	k5eAaImIp3nS	používat
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaImIp3nS	připadat
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
počátek	počátek	k1gInSc1	počátek
cyklu	cyklus	k1gInSc2	cyklus
roku	rok	k1gInSc2	rok
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
od	od	k7c2	od
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
či	či	k8xC	či
slunovratu	slunovrat	k1gInSc2	slunovrat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
počátek	počátek	k1gInSc4	počátek
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
stanovován	stanovovat	k5eAaImNgInS	stanovovat
různě	různě	k6eAd1	různě
(	(	kIx(	(
<g/>
úsvit	úsvit	k1gInSc1	úsvit
<g/>
,	,	kIx,	,
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgFnPc1d1	moderní
oslavy	oslava	k1gFnPc1	oslava
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
==	==	k?	==
</s>
</p>
<p>
<s>
Nejběžnější	běžný	k2eAgNnPc1d3	nejběžnější
data	datum	k1gNnPc1	datum
oslav	oslava	k1gFnPc2	oslava
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
a	a	k8xC	a
seskupená	seskupený	k2eAgFnSc1d1	seskupená
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
konvenčnímu	konvenční	k2eAgInSc3d1	konvenční
západnímu	západní	k2eAgInSc3d1	západní
kalendáři	kalendář	k1gInSc3	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Leden	leden	k1gInSc1	leden
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
:	:	kIx,	:
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
den	den	k1gInSc1	den
roku	rok	k1gInSc2	rok
Gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
počtu	počet	k1gInSc6	počet
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
ortodoxní	ortodoxní	k2eAgFnSc6d1	ortodoxní
církvi	církev	k1gFnSc6	církev
začíná	začínat	k5eAaImIp3nS	začínat
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
14	[number]	k4	14
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
Juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
pravoslaví	pravoslaví	k1gNnSc4	pravoslaví
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
oba	dva	k4xCgInPc1	dva
svátky	svátek	k1gInPc1	svátek
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Gregoriánský	gregoriánský	k2eAgMnSc1d1	gregoriánský
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Juliánský	juliánský	k2eAgMnSc1d1	juliánský
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
civilním	civilní	k2eAgInSc7d1	civilní
svátkem	svátek	k1gInSc7	svátek
zatímco	zatímco	k8xS	zatímco
Juliánský	juliánský	k2eAgInSc1d1	juliánský
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Starý	starý	k2eAgInSc1d1	starý
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
církevním	církevní	k2eAgInSc7d1	církevní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
církevní	církevní	k2eAgInSc1d1	církevní
liturgický	liturgický	k2eAgInSc1d1	liturgický
kalendář	kalendář	k1gInSc1	kalendář
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
zářím	zářit	k5eAaImIp1nS	zářit
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
římský	římský	k2eAgInSc4d1	římský
berní	berní	k2eAgInSc4d1	berní
systém	systém	k1gInSc4	systém
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
indikce	indikce	k1gFnSc2	indikce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
každoročně	každoročně	k6eAd1	každoročně
oslavami	oslava	k1gFnPc7	oslava
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
zmrtvýchvstáním	zmrtvýchvstání	k1gNnSc7	zmrtvýchvstání
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
Pesach	pesach	k1gInSc1	pesach
<g/>
/	/	kIx~	/
<g/>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
nanebevstoupení	nanebevstoupení	k1gNnSc3	nanebevstoupení
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
až	až	k9	až
po	po	k7c6	po
nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Dormition	Dormition	k1gInSc1	Dormition
Theotokos	Theotokos	k1gInSc1	Theotokos
<g/>
/	/	kIx~	/
<g/>
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
<g/>
)	)	kIx)	)
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Osm	osm	k4xCc1	osm
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
největších	veliký	k2eAgFnPc2d3	veliký
východních	východní	k2eAgFnPc2d1	východní
ortodoxních	ortodoxní	k2eAgFnPc2d1	ortodoxní
církví	církev	k1gFnPc2	církev
převzaly	převzít	k5eAaPmAgFnP	převzít
administrativně	administrativně	k6eAd1	administrativně
revidovaný	revidovaný	k2eAgInSc4d1	revidovaný
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
a	a	k8xC	a
sladily	sladit	k5eAaImAgInP	sladit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
civilní	civilní	k2eAgInPc4d1	civilní
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgNnSc1d1	ortodoxní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
slaví	slavit	k5eAaImIp3nS	slavit
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgFnPc1d1	ortodoxní
církve	církev	k1gFnPc1	církev
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Srbska	Srbsko	k1gNnSc2	Srbsko
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
používají	používat	k5eAaImIp3nP	používat
Juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgInSc4d1	čínský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgInSc4d1	známý
jak	jak	k8xS	jak
Lunární	lunární	k2eAgInSc4d1	lunární
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaImIp3nS	připadat
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
nov	nov	k1gInSc4	nov
prvního	první	k4xOgInSc2	první
lunárního	lunární	k2eAgInSc2d1	lunární
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc4	čtyři
až	až	k8xS	až
osm	osm	k4xCc4	osm
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
jarem	jar	k1gInSc7	jar
(	(	kIx(	(
<g/>
Lichun	Lichun	k1gInSc1	Lichun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
může	moct	k5eAaImIp3nS	moct
připadnout	připadnout	k5eAaPmF	připadnout
na	na	k7c4	na
kterýkoliv	kterýkoliv	k3yIgInSc4	kterýkoliv
den	den	k1gInSc4	den
mezi	mezi	k7c4	mezi
21	[number]	k4	21
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
<g/>
)	)	kIx)	)
Gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
lunisolární	lunisolární	k2eAgInSc1d1	lunisolární
čínský	čínský	k2eAgInSc1d1	čínský
kalendář	kalendář	k1gInSc1	kalendář
je	být	k5eAaImIp3nS	být
astronomicky	astronomicky	k6eAd1	astronomicky
definovaný	definovaný	k2eAgInSc1d1	definovaný
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k8xS	jako
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
díky	díky	k7c3	díky
precesi	precese	k1gFnSc3	precese
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
měnit	měnit	k5eAaImF	měnit
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
svou	svůj	k3xOyFgFnSc4	svůj
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
symbolizován	symbolizován	k2eAgInSc1d1	symbolizován
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
12	[number]	k4	12
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
5	[number]	k4	5
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
cyklujících	cyklující	k2eAgInPc2d1	cyklující
každých	každý	k3xTgNnPc2	každý
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
čínský	čínský	k2eAgInSc4d1	čínský
svátek	svátek	k1gInSc4	svátek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vietnamský	vietnamský	k2eAgInSc4d1	vietnamský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
Tế	Tế	k1gMnSc1	Tế
Nguyê	Nguyê	k1gMnSc1	Nguyê
Đán	Đán	k1gMnSc1	Đán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
většinou	většina	k1gFnSc7	většina
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xS	jako
Čínský	čínský	k2eAgInSc4d1	čínský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tibetský	tibetský	k2eAgInSc1d1	tibetský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Losar	Losar	k1gInSc1	Losar
a	a	k8xC	a
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
leden	leden	k1gInSc4	leden
až	až	k8xS	až
březen	březen	k1gInSc4	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Březen	Březno	k1gNnPc2	Březno
===	===	k?	===
</s>
</p>
<p>
<s>
Hola	hola	k1gFnSc1	hola
Mohalla	Mohalla	k1gFnSc1	Mohalla
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
v	v	k7c6	v
sikhském	sikhský	k2eAgInSc6d1	sikhský
Nanakshahi	Nanakshah	k1gInSc6	Nanakshah
kalendáři	kalendář	k1gInSc6	kalendář
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Perský	perský	k2eAgInSc1d1	perský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Nourúz	Nourúz	k1gInSc1	Nourúz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
připadající	připadající	k2eAgInSc4d1	připadající
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
den	den	k1gInSc4	den
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
jarní	jarní	k2eAgNnSc4d1	jarní
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
připadl	připadnout	k5eAaPmAgInS	připadnout
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zoroastrický	Zoroastrický	k2eAgInSc1d1	Zoroastrický
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
íránským	íránský	k2eAgMnSc7d1	íránský
Nourúzem	Nourúz	k1gMnSc7	Nourúz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
komunitou	komunita	k1gFnSc7	komunita
Parsi	Parse	k1gFnSc3	Parse
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Zoroastriány	Zoroastrián	k1gMnPc4	Zoroastrián
a	a	k8xC	a
Peršany	Peršan	k1gMnPc4	Peršan
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
kalendáři	kalendář	k1gInSc3	kalendář
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
slaví	slavit	k5eAaImIp3nS	slavit
v	v	k7c4	v
den	den	k1gInSc4	den
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Naw-Rúz	Naw-Rúz	k1gMnSc1	Naw-Rúz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Saka	sako	k1gNnSc2	sako
(	(	kIx(	(
<g/>
Balijsko-jávský	Balijskoávský	k2eAgInSc1d1	Balijsko-jávský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
)	)	kIx)	)
oslava	oslava	k1gFnSc1	oslava
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
spadá	spadat	k5eAaImIp3nS	spadat
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
Oslava	oslava	k1gFnSc1	oslava
se	se	k3xPyFc4	se
svolává	svolávat	k5eAaImIp3nS	svolávat
na	na	k7c6	na
Nyepi	Nyep	k1gInSc6	Nyep
<g/>
,	,	kIx,	,
balijském	balijský	k2eAgInSc6d1	balijský
hinduistickém	hinduistický	k2eAgInSc6d1	hinduistický
svátku	svátek	k1gInSc6	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Telugijský	Telugijský	k2eAgInSc1d1	Telugijský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
obecně	obecně	k6eAd1	obecně
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
březen	březen	k1gInSc4	březen
nebo	nebo	k8xC	nebo
duben	duben	k1gInSc4	duben
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
slaví	slavit	k5eAaImIp3nS	slavit
příchod	příchod	k1gInSc4	příchod
lunárního	lunární	k2eAgInSc2d1	lunární
roku	rok	k1gInSc2	rok
lidé	člověk	k1gMnPc1	člověk
Andhra	Andhra	k1gFnSc1	Andhra
Pradéše	Pradéše	k1gFnSc1	Pradéše
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Andhra	Andhra	k1gFnSc1	Andhra
Pradéši	Pradéše	k1gFnSc3	Pradéše
jako	jako	k8xC	jako
UGADI	UGADI	kA	UGADI
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
začátku	začátek	k1gInSc2	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
Chaitra	Chaitra	k1gFnSc1	Chaitra
Masam	Masam	k1gInSc1	Masam
<g/>
.	.	kIx.	.
</s>
<s>
Masamem	Masam	k1gInSc7	Masam
je	být	k5eAaImIp3nS	být
míněn	míněn	k2eAgInSc4d1	míněn
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gudi	Gudi	k1gNnSc1	Gudi
Padwa	Padw	k1gInSc2	Padw
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
jako	jako	k9	jako
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
Hindského	hindský	k2eAgInSc2d1	hindský
roku	rok	k1gInSc2	rok
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
Maháráštry	Maháráštrum	k1gNnPc7	Maháráštrum
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
spadá	spadat	k5eAaPmIp3nS	spadat
na	na	k7c4	na
březen	březen	k1gInSc4	březen
nebo	nebo	k8xC	nebo
duben	duben	k1gInSc4	duben
a	a	k8xC	a
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
Ugadi	Ugad	k1gMnPc1	Ugad
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Dekan	dekan	k1gInSc1	dekan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
Kannada	Kannada	k1gFnSc1	Kannada
nebo	nebo	k8xC	nebo
Ugadi	Ugad	k1gMnPc1	Ugad
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgMnS	slavit
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
Karnátaku	Karnátak	k1gInSc2	Karnátak
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
jako	jako	k8xC	jako
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
hindského	hindský	k2eAgInSc2d1	hindský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Chaitra	Chaitra	k1gFnSc1	Chaitra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Duben	duben	k1gInSc4	duben
===	===	k?	===
</s>
</p>
<p>
<s>
Asyrský	asyrský	k2eAgInSc1d1	asyrský
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Rish	Rish	k1gInSc1	Rish
Nissanu	nissan	k1gInSc2	nissan
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
</s>
</p>
<p>
<s>
Pandžábský	Pandžábský	k2eAgInSc1d1	Pandžábský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
Vaisakhi	Vaisakh	k1gFnSc2	Vaisakh
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
oslavována	oslavován	k2eAgFnSc1d1	oslavována
sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepálský	nepálský	k2eAgInSc1d1	nepálský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
měsíčního	měsíční	k2eAgInSc2d1	měsíční
měsíce	měsíc	k1gInSc2	měsíc
Baisakh	Baisakha	k1gFnPc2	Baisakha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
kalendáři	kalendář	k1gInSc6	kalendář
obvykle	obvykle	k6eAd1	obvykle
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thajský	thajský	k2eAgInSc1d1	thajský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
házením	házení	k1gNnSc7	házení
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kambodžský	kambodžský	k2eAgInSc4d1	kambodžský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
a	a	k8xC	a
Laoský	laoský	k2eAgInSc4d1	laoský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
jsou	být	k5eAaImIp3nP	být
slaveny	slavit	k5eAaImNgInP	slavit
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bengálský	bengálský	k2eAgInSc4d1	bengálský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
Pohela	Pohela	k1gFnSc1	Pohela
Baisakh	Baisakh	k1gInSc1	Baisakh
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
způsobem	způsob	k1gInSc7	způsob
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
nebo	nebo	k8xC	nebo
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
Bangladéších	Bangladéš	k1gInPc6	Bangladéš
a	a	k8xC	a
Západním	západní	k2eAgNnSc6d1	západní
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cejlonský	cejlonský	k2eAgInSc1d1	cejlonský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
(	(	kIx(	(
<g/>
měsíce	měsíc	k1gInSc2	měsíc
Bak	bak	k0	bak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
slunce	slunce	k1gNnSc1	slunce
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
z	z	k7c2	z
Meena	Meen	k1gInSc2	Meen
Rashiya	Rashiy	k1gInSc2	Rashiy
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
k	k	k7c3	k
Mesha	Mesh	k1gMnSc4	Mesh
Rashiya	Rashiya	k1gFnSc1	Rashiya
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
skopce	skopec	k1gMnSc2	skopec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Cejlonu	Cejlon	k1gInSc6	Cejlon
slaví	slavit	k5eAaImIp3nP	slavit
národní	národní	k2eAgInSc4d1	národní
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
"	"	kIx"	"
<g/>
Aluth	Aluth	k1gInSc1	Aluth
Avurudhu	Avurudh	k1gInSc2	Avurudh
<g/>
"	"	kIx"	"
v	v	k7c6	v
Sinhálsku	Sinhálsek	k1gInSc6	Sinhálsek
a	a	k8xC	a
"	"	kIx"	"
<g/>
Puththandu	Puththanda	k1gFnSc4	Puththanda
<g/>
"	"	kIx"	"
v	v	k7c6	v
Tamilsku	Tamilsek	k1gInSc6	Tamilsek
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
však	však	k9	však
nezačíná	začínat	k5eNaImIp3nS	začínat
jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
určen	určit	k5eAaPmNgMnS	určit
astrology	astrolog	k1gMnPc7	astrolog
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nejen	nejen	k6eAd1	nejen
začátek	začátek	k1gInSc4	začátek
<g/>
,	,	kIx,	,
i	i	k8xC	i
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
určují	určovat	k5eAaImIp3nP	určovat
astrologové	astrolog	k1gMnPc1	astrolog
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
konec	konec	k1gInSc1	konec
starého	starý	k2eAgNnSc2d1	staré
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
plynule	plynule	k6eAd1	plynule
nenavazuje	navazovat	k5eNaImIp3nS	navazovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
perioda	perioda	k1gFnSc1	perioda
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
nona	non	k2eAgFnSc1d1	non
gathe	gathe	k1gFnSc1	gathe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neutrální	neutrální	k2eAgFnSc1d1	neutrální
perioda	perioda	k1gFnSc1	perioda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudou	být	k5eNaImBp3nP	být
vykonávány	vykonávat	k5eAaImNgFnP	vykonávat
žádné	žádný	k3yNgFnPc1	žádný
práce	práce	k1gFnPc1	práce
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
prováděny	provádět	k5eAaImNgFnP	provádět
pouze	pouze	k6eAd1	pouze
náboženské	náboženský	k2eAgFnPc1d1	náboženská
aktivity	aktivita	k1gFnPc1	aktivita
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Národní	národní	k2eAgInSc1d1	národní
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
však	však	k9	však
nezačíná	začínat	k5eNaImIp3nS	začínat
jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
určen	určit	k5eAaPmNgMnS	určit
astrology	astrolog	k1gMnPc7	astrolog
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vyvolat	vyvolat	k5eAaPmF	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
začátek	začátek	k1gInSc4	začátek
nového	nový	k2eAgInSc2d1	nový
cejlonského	cejlonský	k2eAgInSc2d1	cejlonský
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
určován	určovat	k5eAaImNgInS	určovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pověr	pověra	k1gFnPc2	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
však	však	k9	však
čistě	čistě	k6eAd1	čistě
matematickou	matematický	k2eAgFnSc7d1	matematická
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
ho	on	k3xPp3gMnSc4	on
provádět	provádět	k5eAaImF	provádět
jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
obeznámen	obeznámen	k2eAgMnSc1d1	obeznámen
s	s	k7c7	s
matematikou	matematika	k1gFnSc7	matematika
a	a	k8xC	a
astrologií	astrologie	k1gFnSc7	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Okamžikem	okamžik	k1gInSc7	okamžik
počátku	počátek	k1gInSc2	počátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
chvíle	chvíle	k1gFnPc4	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
střed	střed	k1gInSc1	střed
slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeho	jeho	k3xOp3gFnSc2	jeho
projekce	projekce	k1gFnSc2	projekce
na	na	k7c4	na
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
sféru	sféra	k1gFnSc4	sféra
<g/>
)	)	kIx)	)
křižuje	křižovat	k5eAaImIp3nS	křižovat
linii	linie	k1gFnSc4	linie
oddělující	oddělující	k2eAgFnSc2d1	oddělující
ryby	ryba	k1gFnSc2	ryba
(	(	kIx(	(
<g/>
Meena	Meena	k1gFnSc1	Meena
<g/>
)	)	kIx)	)
od	od	k7c2	od
skopce	skopec	k1gMnSc2	skopec
(	(	kIx(	(
<g/>
Mesha	Mesha	k1gMnSc1	Mesha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
astrologie	astrologie	k1gFnSc2	astrologie
je	být	k5eAaImIp3nS	být
nebeská	nebeský	k2eAgFnSc1d1	nebeská
sféra	sféra	k1gFnSc1	sféra
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
12	[number]	k4	12
stejných	stejný	k2eAgFnPc2d1	stejná
částí	část	k1gFnPc2	část
a	a	k8xC	a
skopec	skopec	k1gMnSc1	skopec
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
tradičně	tradičně	k6eAd1	tradičně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
sféry	sféra	k1gFnSc2	sféra
zatímco	zatímco	k8xS	zatímco
ryby	ryba	k1gFnPc4	ryba
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
díl	díl	k1gInSc1	díl
sféry	sféra	k1gFnSc2	sféra
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
30	[number]	k4	30
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
sféry	sféra	k1gFnSc2	sféra
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
skopce	skopec	k1gMnPc4	skopec
spolu	spolu	k6eAd1	spolu
sousedí	sousedit	k5eAaImIp3nS	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
cestu	cesta	k1gFnSc4	cesta
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
promítneme	promítnout	k5eAaPmIp1nP	promítnout
na	na	k7c4	na
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
sféru	sféra	k1gFnSc4	sféra
<g/>
,	,	kIx,	,
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
)	)	kIx)	)
kříží	křížit	k5eAaImIp3nS	křížit
všech	všecek	k3xTgFnPc2	všecek
12	[number]	k4	12
oblastí	oblast	k1gFnPc2	oblast
zvěrokruhu	zvěrokruh	k1gInSc3	zvěrokruh
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
a	a	k8xC	a
právě	právě	k6eAd1	právě
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
střed	střed	k1gInSc1	střed
slunce	slunce	k1gNnSc2	slunce
křižuje	křižovat	k5eAaImIp3nS	křižovat
linii	linie	k1gFnSc4	linie
oddělující	oddělující	k2eAgNnSc1d1	oddělující
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
skopce	skopec	k1gMnPc4	skopec
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
cejlonského	cejlonský	k2eAgInSc2d1	cejlonský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
chvíle	chvíle	k1gFnSc1	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slunce	slunce	k1gNnSc1	slunce
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
do	do	k7c2	do
skopce	skopec	k1gMnSc2	skopec
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
část	část	k1gFnSc4	část
cejlonského	cejlonský	k2eAgInSc2d1	cejlonský
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
slunce	slunce	k1gNnSc2	slunce
není	být	k5eNaImIp3nS	být
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kruhem	kruh	k1gInSc7	kruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Chvíle	chvíle	k1gFnSc1	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slunce	slunce	k1gNnSc1	slunce
protne	protnout	k5eAaPmIp3nS	protnout
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgNnSc1d1	oddělující
znamení	znamení	k1gNnSc1	znamení
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
skopce	skopec	k1gMnPc4	skopec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
období	období	k1gNnSc2	období
Nonagatha	Nonagath	k1gMnSc2	Nonagath
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
slunce	slunce	k1gNnSc1	slunce
kompletně	kompletně	k6eAd1	kompletně
celou	celý	k2eAgFnSc7d1	celá
plochou	plocha	k1gFnSc7	plocha
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
skopce	skopec	k1gMnSc2	skopec
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nastává	nastávat	k5eAaImIp3nS	nastávat
konec	konec	k1gInSc1	konec
období	období	k1gNnSc2	období
Nonagatha	Nonagatha	k1gFnSc1	Nonagatha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
tamilský	tamilský	k2eAgInSc4d1	tamilský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
a	a	k8xC	a
Vishu	Visha	k1gFnSc4	Visha
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c6	v
jihoindických	jihoindický	k2eAgInPc6d1	jihoindický
státech	stát	k1gInPc6	stát
Tamilnádu	Tamilnáda	k1gFnSc4	Tamilnáda
a	a	k8xC	a
Kérala	Kérala	k1gFnSc1	Kérala
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
svátky	svátek	k1gInPc1	svátek
připadají	připadat	k5eAaImIp3nP	připadat
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
nebo	nebo	k8xC	nebo
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
tamilského	tamilský	k2eAgInSc2d1	tamilský
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
Chithrai	Chithrai	k1gNnSc7	Chithrai
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
Chithrai	Chithra	k1gFnSc2	Chithra
<g/>
,	,	kIx,	,
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Minakši	Minakše	k1gFnSc4	Minakše
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Maduraj	Maduraj	k1gInSc4	Maduraj
<g/>
,	,	kIx,	,
slaven	slaven	k2eAgInSc4d1	slaven
svátek	svátek	k1gInSc4	svátek
Chithrai	Chithra	k1gFnSc2	Chithra
Thiruvizha	Thiruvizha	k1gFnSc1	Thiruvizha
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
ohromný	ohromný	k2eAgInSc1d1	ohromný
trh	trh	k1gInSc1	trh
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Chithrai	Chithrae	k1gFnSc4	Chithrae
Porutkaatchi	Porutkaatch	k1gFnSc2	Porutkaatch
<g/>
.	.	kIx.	.
</s>
<s>
Hindské	hindský	k2eAgInPc1d1	hindský
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
komplikovaně	komplikovaně	k6eAd1	komplikovaně
označovány	označován	k2eAgInPc4d1	označován
kolamy	kolam	k1gInPc4	kolam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podzim	podzim	k1gInSc1	podzim
===	===	k?	===
</s>
</p>
<p>
<s>
Egyptský	egyptský	k2eAgInSc1d1	egyptský
kalendář	kalendář	k1gInSc1	kalendář
začínal	začínat	k5eAaImAgInS	začínat
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
v	v	k7c6	v
juliánském	juliánský	k2eAgInSc6d1	juliánský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
)	)	kIx)	)
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
Augustus	Augustus	k1gInSc4	Augustus
synchronizoval	synchronizovat	k5eAaBmAgMnS	synchronizovat
s	s	k7c7	s
kalendářem	kalendář	k1gInSc7	kalendář
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roš	Roš	k?	Roš
ha-šana	ha-šana	k1gFnSc1	ha-šana
(	(	kIx(	(
<g/>
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
'	'	kIx"	'
<g/>
hlavu	hlava	k1gFnSc4	hlava
roku	rok	k1gInSc2	rok
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
den	den	k1gInSc4	den
následující	následující	k2eAgFnSc2d1	následující
163	[number]	k4	163
dní	den	k1gInPc2	den
po	po	k7c6	po
Pesachu	pesach	k1gInSc6	pesach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nynějším	nynější	k2eAgInSc6d1	nynější
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
pravidlům	pravidlo	k1gNnPc3	pravidlo
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
výpočet	výpočet	k1gInSc4	výpočet
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2089	[number]	k4	2089
způsobí	způsobit	k5eAaPmIp3nS	způsobit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
hebrejským	hebrejský	k2eAgInSc7d1	hebrejský
a	a	k8xC	a
gregoriánským	gregoriánský	k2eAgInSc7d1	gregoriánský
kalendářem	kalendář	k1gInSc7	kalendář
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Roš	Roš	k1gFnSc1	Roš
ha-šana	ha-šana	k1gFnSc1	ha-šana
nebude	být	k5eNaImBp3nS	být
spadat	spadat	k5eAaPmF	spadat
na	na	k7c4	na
dřívější	dřívější	k2eAgInSc4d1	dřívější
den	den	k1gInSc4	den
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
Roš	Roš	k1gMnSc2	Roš
ha-šana	ha-šan	k1gMnSc2	ha-šan
může	moct	k5eAaImIp3nS	moct
připadnout	připadnout	k5eAaPmF	připadnout
nejpozději	pozdě	k6eAd3	pozdě
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
naposledy	naposledy	k6eAd1	naposledy
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
tak	tak	k9	tak
stane	stanout	k5eAaPmIp3nS	stanout
roku	rok	k1gInSc2	rok
2043	[number]	k4	2043
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Koptské	koptský	k2eAgFnSc6d1	koptská
ortodoxní	ortodoxní	k2eAgFnSc6d1	ortodoxní
církvi	církev	k1gFnSc6	církev
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Neyrouz	Neyrouz	k1gInSc1	Neyrouz
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaPmIp3nS	spadat
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1900	[number]	k4	1900
a	a	k8xC	a
2099	[number]	k4	2099
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koptský	koptský	k2eAgInSc1d1	koptský
rok	rok	k1gInSc1	rok
číslo	číslo	k1gNnSc1	číslo
1723	[number]	k4	1723
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
ortodoxní	ortodoxní	k2eAgInSc1d1	ortodoxní
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Enkutataš	Enkutataš	k1gInSc1	Enkutataš
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
datum	datum	k1gNnSc4	datum
jako	jako	k8xS	jako
Neyrouz	Neyrouz	k1gInSc4	Neyrouz
<g/>
;	;	kIx,	;
etiopský	etiopský	k2eAgInSc4d1	etiopský
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
1999	[number]	k4	1999
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
Marwari	Marwar	k1gFnSc2	Marwar
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c4	v
den	den	k1gInSc4	den
svátku	svátek	k1gInSc2	svátek
Diwali	Diwali	k1gFnSc2	Diwali
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gudžarátský	Gudžarátský	k2eAgInSc1d1	Gudžarátský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
slaven	slaven	k2eAgInSc4d1	slaven
den	den	k1gInSc4	den
po	po	k7c6	po
svátku	svátek	k1gInSc6	svátek
Diwali	Diwali	k1gFnSc2	Diwali
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
uprostřed	uprostřed	k7c2	uprostřed
podzimu	podzim	k1gInSc2	podzim
–	–	k?	–
buď	buď	k8xC	buď
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lunárním	lunární	k2eAgInSc6d1	lunární
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gudžarátský	Gudžarátský	k2eAgInSc1d1	Gudžarátský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
synonymní	synonymní	k2eAgMnSc1d1	synonymní
se	se	k3xPyFc4	se
sud	sud	k1gInSc1	sud
ekam	ekam	k6eAd1	ekam
měsíce	měsíc	k1gInPc4	měsíc
Kartik	Kartikum	k1gNnPc2	Kartikum
–	–	k?	–
prvního	první	k4xOgNnSc2	první
dne	den	k1gInSc2	den
prvního	první	k4xOgInSc2	první
měsíce	měsíc	k1gInSc2	měsíc
gudžaratského	gudžaratský	k2eAgInSc2d1	gudžaratský
lunárního	lunární	k2eAgInSc2d1	lunární
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
hinduistů	hinduista	k1gMnPc2	hinduista
slaví	slavit	k5eAaImIp3nS	slavit
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
brzy	brzy	k6eAd1	brzy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
gudžaratské	gudžaratský	k2eAgNnSc1d1	gudžaratský
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
společenství	společenství	k1gNnSc1	společenství
slaví	slavit	k5eAaImIp3nS	slavit
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
po	po	k7c4	po
Diwali	Diwali	k1gFnSc4	Diwali
jako	jako	k8xS	jako
označení	označení	k1gNnSc4	označení
počátku	počátek	k1gInSc2	počátek
nového	nový	k2eAgInSc2d1	nový
fiskálního	fiskální	k2eAgInSc2d1	fiskální
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
novopohané	novopohan	k1gMnPc1	novopohan
slaví	slavit	k5eAaImIp3nP	slavit
Samhain	Samhain	k1gInSc4	Samhain
(	(	kIx(	(
<g/>
slavnost	slavnost	k1gFnSc4	slavnost
starověkých	starověký	k2eAgInPc2d1	starověký
keltů	kelt	k1gInPc2	kelt
<g/>
,	,	kIx,	,
držená	držený	k2eAgFnSc1d1	držená
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
novoroční	novoroční	k2eAgInSc4d1	novoroční
den	den	k1gInSc4	den
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
začátek	začátek	k1gInSc4	začátek
nového	nový	k2eAgInSc2d1	nový
ročního	roční	k2eAgInSc2d1	roční
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nepoužívají	používat	k5eNaImIp3nP	používat
odlišný	odlišný	k2eAgInSc4d1	odlišný
kalendář	kalendář	k1gInSc4	kalendář
začínající	začínající	k2eAgInSc1d1	začínající
tímto	tento	k3xDgInSc7	tento
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Různé	různý	k2eAgInPc1d1	různý
===	===	k?	===
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
1	[number]	k4	1
Muharram	Muharram	k1gInSc4	Muharram
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
muslimský	muslimský	k2eAgInSc1d1	muslimský
kalendář	kalendář	k1gInSc1	kalendář
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
12	[number]	k4	12
lunárních	lunární	k2eAgInPc6d1	lunární
měsících	měsíc	k1gInPc6	měsíc
obsahujících	obsahující	k2eAgInPc2d1	obsahující
okolo	okolo	k7c2	okolo
354	[number]	k4	354
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
gregoriánské	gregoriánský	k2eAgNnSc1d1	gregoriánské
datum	datum	k1gNnSc1	datum
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
o	o	k7c4	o
11	[number]	k4	11
dní	den	k1gInPc2	den
dřívější	dřívější	k2eAgInSc1d1	dřívější
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
spadaly	spadat	k5eAaImAgInP	spadat
dva	dva	k4xCgInPc1	dva
muslimské	muslimský	k2eAgInPc1d1	muslimský
nové	nový	k2eAgInPc1d1	nový
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Telemický	Telemický	k2eAgInSc1d1	Telemický
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
slaven	slaven	k2eAgInSc1d1	slaven
s	s	k7c7	s
vyzýváním	vyzývání	k1gNnSc7	vyzývání
Ra-hoor-Khuita	Raoor-Khuit	k1gMnSc2	Ra-hoor-Khuit
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
začátek	začátek	k1gInSc1	začátek
nové	nový	k2eAgFnSc2d1	nová
éry	éra	k1gFnSc2	éra
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
začátek	začátek	k1gInSc4	začátek
22	[number]	k4	22
dní	den	k1gInPc2	den
telemického	telemický	k2eAgNnSc2d1	telemický
svatého	svatý	k2eAgNnSc2d1	svaté
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgNnPc4d1	historické
data	datum	k1gNnPc4	datum
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
==	==	k?	==
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
římský	římský	k2eAgInSc1d1	římský
kalendář	kalendář	k1gInSc1	kalendář
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
deset	deset	k4xCc4	deset
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
začínal	začínat	k5eAaImAgMnS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
anglických	anglický	k2eAgInPc6d1	anglický
názvech	název	k1gInPc6	název
některých	některý	k3yIgMnPc2	některý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
:	:	kIx,	:
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
september	september	k1gInSc1	september
–	–	k?	–
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
(	(	kIx(	(
<g/>
october	october	k1gInSc1	october
–	–	k?	–
osmý	osmý	k4xOgMnSc1	osmý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
(	(	kIx(	(
<g/>
november	november	k1gInSc1	november
–	–	k?	–
devátý	devátý	k4xOgMnSc1	devátý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prosinec	prosinec	k1gInSc1	prosinec
(	(	kIx(	(
<g/>
december	december	k1gInSc1	december
–	–	k?	–
desátý	desátý	k4xOgMnSc1	desátý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
713	[number]	k4	713
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgFnP	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
králem	král	k1gMnSc7	král
Numou	Numa	k1gMnSc7	Numa
Pompiliem	Pompilium	k1gNnSc7	Pompilium
<g/>
,	,	kIx,	,
k	k	k7c3	k
měsícům	měsíc	k1gInPc3	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
přidány	přidán	k2eAgInPc4d1	přidán
leden	leden	k1gInSc4	leden
(	(	kIx(	(
<g/>
Januarius	Januarius	k1gInSc4	Januarius
<g/>
)	)	kIx)	)
a	a	k8xC	a
únor	únor	k1gInSc4	únor
(	(	kIx(	(
<g/>
Februarius	Februarius	k1gInSc4	Februarius
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
doplňkovým	doplňkový	k2eAgInSc7d1	doplňkový
měsícem	měsíc	k1gInSc7	měsíc
Intercalarisem	Intercalaris	k1gInSc7	Intercalaris
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
použitý	použitý	k2eAgInSc4d1	použitý
v	v	k7c6	v
datování	datování	k1gNnSc6	datování
byl	být	k5eAaImAgInS	být
konzulárním	konzulární	k2eAgInSc7d1	konzulární
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začínal	začínat	k5eAaImAgInS	začínat
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konzulové	konzul	k1gMnPc1	konzul
poprvé	poprvé	k6eAd1	poprvé
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
–	–	k?	–
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
222	[number]	k4	222
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
uzákoněno	uzákoněn	k2eAgNnSc1d1	uzákoněno
na	na	k7c6	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
191	[number]	k4	191
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
zřejmě	zřejmě	k6eAd1	zřejmě
vydáním	vydání	k1gNnSc7	vydání
tzv.	tzv.	kA	tzv.
Aciliova	Aciliův	k2eAgInSc2d1	Aciliův
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Lex	Lex	k1gFnSc1	Lex
Acilia	Acilia	k1gFnSc1	Acilia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zrušil	zrušit	k5eAaPmAgInS	zrušit
cyklus	cyklus	k1gInSc4	cyklus
upravující	upravující	k2eAgInSc1d1	upravující
systém	systém	k1gInSc1	systém
obyčejných	obyčejný	k2eAgNnPc2d1	obyčejné
a	a	k8xC	a
přestupných	přestupný	k2eAgNnPc2d1	přestupné
let	léto	k1gNnPc2	léto
a	a	k8xC	a
úpravu	úprava	k1gFnSc4	úprava
roku	rok	k1gInSc2	rok
nechal	nechat	k5eAaPmAgInS	nechat
na	na	k7c4	na
libovůli	libovůle	k1gFnSc4	libovůle
pontifiků	pontifex	k1gMnPc2	pontifex
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
tedy	tedy	k9	tedy
i	i	k9	i
změnil	změnit	k5eAaPmAgInS	změnit
datum	datum	k1gInSc1	datum
začátku	začátek	k1gInSc2	začátek
římského	římský	k2eAgMnSc2d1	římský
úředního	úřední	k2eAgMnSc2d1	úřední
(	(	kIx(	(
<g/>
konzulského	konzulský	k2eAgInSc2d1	konzulský
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úředním	úřední	k2eAgInSc7d1	úřední
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začínal	začínat	k5eAaImAgInS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
byl	být	k5eAaImAgMnS	být
rok	rok	k1gInSc4	rok
190	[number]	k4	190
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
datovány	datovat	k5eAaImNgFnP	datovat
úřední	úřední	k2eAgFnPc1d1	úřední
písemnosti	písemnost	k1gFnPc1	písemnost
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
občanský	občanský	k2eAgInSc1d1	občanský
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
ponechán	ponechat	k5eAaPmNgInS	ponechat
na	na	k7c6	na
libovůli	libovůle	k1gFnSc6	libovůle
pontifiků	pontifex	k1gMnPc2	pontifex
<g/>
,	,	kIx,	,
získával	získávat	k5eAaImAgInS	získávat
postupně	postupně	k6eAd1	postupně
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
a	a	k8xC	a
zatlačoval	zatlačovat	k5eAaImAgMnS	zatlačovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
1	[number]	k4	1
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
počátek	počátek	k1gInSc4	počátek
občanského	občanský	k2eAgInSc2d1	občanský
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
zavedl	zavést	k5eAaPmAgMnS	zavést
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vypuštěn	vypuštěn	k2eAgInSc4d1	vypuštěn
měsíc	měsíc	k1gInSc4	měsíc
Intercalaris	Intercalaris	k1gFnSc2	Intercalaris
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
však	však	k9	však
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
prvním	první	k4xOgInSc7	první
dnem	den	k1gInSc7	den
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
uznávali	uznávat	k5eAaImAgMnP	uznávat
i	i	k9	i
raní	raný	k2eAgMnPc1d1	raný
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
církve	církev	k1gFnSc2	církev
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
uvedenému	uvedený	k2eAgNnSc3d1	uvedené
datu	datum	k1gNnSc3	datum
výhradu	výhrada	k1gFnSc4	výhrada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemělo	mít	k5eNaImAgNnS	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
den	den	k1gInSc4	den
konaly	konat	k5eAaImAgFnP	konat
bujaré	bujarý	k2eAgFnPc1d1	bujará
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
v	v	k7c6	v
Toursu	Tours	k1gInSc6	Tours
roku	rok	k1gInSc2	rok
567	[number]	k4	567
dokonce	dokonce	k9	dokonce
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
začátek	začátek	k1gInSc4	začátek
roku	rok	k1gInSc2	rok
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
za	za	k7c4	za
starý	starý	k2eAgInSc4d1	starý
omyl	omyl	k1gInSc4	omyl
a	a	k8xC	a
hrozil	hrozit	k5eAaImAgMnS	hrozit
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
budou	být	k5eAaImBp3nP	být
držet	držet	k5eAaImF	držet
exkomunikací	exkomunikace	k1gFnSc7	exkomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
přes	přes	k7c4	přes
několikeré	několikerý	k4xRyIgInPc4	několikerý
zákazy	zákaz	k1gInPc4	zákaz
papežů	papež	k1gMnPc2	papež
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vymýtit	vymýtit	k5eAaPmF	vymýtit
<g/>
,	,	kIx,	,
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
církev	církev	k1gFnSc1	církev
tomuto	tento	k3xDgInSc3	tento
dni	den	k1gInSc3	den
dát	dát	k5eAaPmF	dát
náboženský	náboženský	k2eAgInSc4d1	náboženský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
svátek	svátek	k1gInSc1	svátek
Obřezání	obřezání	k1gNnSc2	obřezání
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
Circumcisio	Circumcisio	k6eAd1	Circumcisio
Domini	Domin	k2eAgMnPc1d1	Domin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uznán	uznán	k2eAgMnSc1d1	uznán
jako	jako	k8xC	jako
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
církví	církev	k1gFnPc2	církev
byl	být	k5eAaImAgMnS	být
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
církevním	církevní	k2eAgNnSc6d1	církevní
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stanovení	stanovení	k1gNnSc2	stanovení
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
papežem	papež	k1gMnSc7	papež
Inocencem	Inocenc	k1gMnSc7	Inocenc
XII	XII	kA	XII
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1691	[number]	k4	1691
byl	být	k5eAaImAgInS	být
však	však	k9	však
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
roku	rok	k1gInSc2	rok
považován	považován	k2eAgInSc1d1	považován
také	také	k9	také
6	[number]	k4	6
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
začátek	začátek	k1gInSc4	začátek
juliánského	juliánský	k2eAgInSc2d1	juliánský
roku	rok	k1gInSc2	rok
používáno	používat	k5eAaImNgNnS	používat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
církevních	církevní	k2eAgInPc2d1	církevní
svátků	svátek	k1gInPc2	svátek
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
U	u	k7c2	u
vánočního	vánoční	k2eAgInSc2d1	vánoční
datovacího	datovací	k2eAgInSc2d1	datovací
systému	systém	k1gInSc2	systém
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
začíná	začínat	k5eAaImIp3nS	začínat
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Anglii	Anglie	k1gFnSc4	Anglie
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
však	však	k9	však
postupně	postupně	k6eAd1	postupně
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
velikonočním	velikonoční	k2eAgInSc7d1	velikonoční
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
franského	franský	k2eAgNnSc2d1	franské
období	období	k1gNnSc2	období
byl	být	k5eAaImAgInS	být
také	také	k9	také
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
římskoněmecké	římskoněmecký	k2eAgFnSc6d1	římskoněmecká
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
datovacím	datovací	k2eAgInSc6d1	datovací
systému	systém	k1gInSc6	systém
od	od	k7c2	od
zvěstování	zvěstování	k1gNnSc2	zvěstování
je	být	k5eAaImIp3nS	být
počátek	počátek	k1gInSc1	počátek
roku	rok	k1gInSc2	rok
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
svátek	svátek	k1gInSc1	svátek
zvěstování	zvěstování	k1gNnSc2	zvěstování
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
zavedený	zavedený	k2eAgInSc4d1	zavedený
Dionysiem	Dionysius	k1gMnSc7	Dionysius
Exiguem	Exigu	k1gMnSc7	Exigu
roku	rok	k1gInSc2	rok
525	[number]	k4	525
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
datovacího	datovací	k2eAgInSc2d1	datovací
systému	systém	k1gInSc2	systém
začíná	začínat	k5eAaImIp3nS	začínat
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
velikonoční	velikonoční	k2eAgFnSc7d1	velikonoční
sobotou	sobota	k1gFnSc7	sobota
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikonoce	velikonoce	k1gFnPc1	velikonoce
jsou	být	k5eAaImIp3nP	být
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stejné	stejný	k2eAgNnSc1d1	stejné
datum	datum	k1gNnSc1	datum
mohlo	moct	k5eAaImAgNnS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
<g/>
;	;	kIx,	;
dva	dva	k4xCgInPc4	dva
výskyty	výskyt	k1gInPc4	výskyt
jednoho	jeden	k4xCgNnSc2	jeden
data	datum	k1gNnSc2	datum
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odlišovaly	odlišovat	k5eAaImAgFnP	odlišovat
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
před	před	k7c7	před
velikonocemi	velikonoce	k1gFnPc7	velikonoce
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
po	po	k7c6	po
velikonocích	velikonoce	k1gFnPc6	velikonoce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
obřízkového	obřízkový	k2eAgInSc2d1	obřízkový
datovacího	datovací	k2eAgInSc2d1	datovací
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
přisouzen	přisouzen	k2eAgInSc1d1	přisouzen
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
svátku	svátek	k1gInSc6	svátek
obřízky	obřízka	k1gFnSc2	obřízka
(	(	kIx(	(
<g/>
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
700	[number]	k4	700
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
slaven	slavit	k5eAaImNgInS	slavit
jako	jako	k8xS	jako
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
:	:	kIx,	:
Natale	Natal	k1gInSc5	Natal
sanctae	sancta	k1gInPc4	sancta
Mariae	Mariae	k1gNnSc7	Mariae
<g/>
.	.	kIx.	.
</s>
<s>
Převzetím	převzetí	k1gNnSc7	převzetí
byzantského	byzantský	k2eAgInSc2d1	byzantský
svátku	svátek	k1gInSc2	svátek
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
ztratil	ztratit	k5eAaPmAgMnS	ztratit
tento	tento	k3xDgInSc4	tento
svátek	svátek	k1gInSc4	svátek
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
opět	opět	k6eAd1	opět
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
vánoční	vánoční	k2eAgInSc1d1	vánoční
oktáv	oktáv	k1gInSc1	oktáv
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
slaven	slavit	k5eAaImNgInS	slavit
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
jako	jako	k8xS	jako
svátek	svátek	k1gInSc1	svátek
obřezání	obřezání	k1gNnSc2	obřezání
Pána	pán	k1gMnSc2	pán
a	a	k8xC	a
vánoční	vánoční	k2eAgInSc4d1	vánoční
oktáv	oktáv	k1gInSc4	oktáv
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
koncil	koncil	k1gInSc1	koncil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
svátek	svátek	k1gInSc1	svátek
obřízky	obřízka	k1gFnSc2	obřízka
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
reforma	reforma	k1gFnSc1	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
Mariánský	mariánský	k2eAgInSc1d1	mariánský
svátek	svátek	k1gInSc1	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
církví	církev	k1gFnSc7	církev
slaven	slaven	k2eAgMnSc1d1	slaven
pouze	pouze	k6eAd1	pouze
svátek	svátek	k1gInSc4	svátek
Boží	boží	k2eAgFnSc2d1	boží
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pojmenování	pojmenování	k1gNnSc1	pojmenování
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současných	současný	k2eAgInPc6d1	současný
kalendářích	kalendář	k1gInPc6	kalendář
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
u	u	k7c2	u
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
zápisy	zápis	k1gInPc7	zápis
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
vánočního	vánoční	k2eAgInSc2d1	vánoční
oktávu	oktáv	k1gInSc2	oktáv
<g/>
,	,	kIx,	,
pojmenování	pojmenování	k1gNnSc4	pojmenování
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
svátek	svátek	k1gInSc4	svátek
boží	boží	k2eAgFnSc2d1	boží
matky	matka	k1gFnSc2	matka
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
<g/>
Historickým	historický	k2eAgInSc7d1	historický
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
svátek	svátek	k1gInSc4	svátek
obřízky	obřízka	k1gFnSc2	obřízka
Pána	pán	k1gMnSc2	pán
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
kapitola	kapitola	k1gFnSc1	kapitola
evangelia	evangelium	k1gNnSc2	evangelium
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
2	[number]	k4	2
<g/>
,	,	kIx,	,
verši	verš	k1gInSc6	verš
21	[number]	k4	21
stojí	stát	k5eAaImIp3nS	stát
<g/>
:	:	kIx,	:
Když	když	k8xS	když
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
čas	čas	k1gInSc4	čas
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
obřízce	obřízka	k1gFnSc3	obřízka
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc4	jméno
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
anděla	anděl	k1gMnSc2	anděl
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
matka	matka	k1gFnSc1	matka
počala	počnout	k5eAaPmAgFnS	počnout
<g/>
.	.	kIx.	.
<g/>
Starověký	starověký	k2eAgInSc1d1	starověký
římský	římský	k2eAgInSc1d1	římský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
Benátské	benátský	k2eAgFnSc6d1	Benátská
republice	republika	k1gFnSc6	republika
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
zničení	zničení	k1gNnSc2	zničení
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
988	[number]	k4	988
do	do	k7c2	do
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
církevní	církevní	k2eAgFnSc7d1	církevní
tradicí	tradice	k1gFnSc7	tradice
posunut	posunout	k5eAaPmNgMnS	posunout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roky	rok	k1gInPc1	rok
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
počítaly	počítat	k5eAaImAgFnP	počítat
od	od	k7c2	od
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
oslavoval	oslavovat	k5eAaImAgInS	oslavovat
po	po	k7c4	po
dvě	dva	k4xCgFnPc4	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
než	než	k8xS	než
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
vládní	vládní	k2eAgNnPc4d1	vládní
nařízení	nařízení	k1gNnPc4	nařízení
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
zavedlo	zavést	k5eAaPmAgNnS	zavést
počítání	počítání	k1gNnSc1	počítání
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
letopočtu	letopočet	k1gInSc2	letopočet
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
roky	rok	k1gInPc7	rok
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
počítaly	počítat	k5eAaImAgFnP	počítat
od	od	k7c2	od
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
za	za	k7c4	za
začátek	začátek	k1gInSc4	začátek
roku	rok	k1gInSc2	rok
stanovil	stanovit	k5eAaPmAgInS	stanovit
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
<s>
Petrovo	Petrův	k2eAgNnSc1d1	Petrovo
nařízení	nařízení	k1gNnSc1	nařízení
mělo	mít	k5eAaImAgNnS	mít
název	název	k1gInSc4	název
<g/>
:	:	kIx,	:
O	o	k7c6	o
psaní	psaní	k1gNnSc6	psaní
napříště	napříště	k6eAd1	napříště
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
1700	[number]	k4	1700
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
listinách	listina	k1gFnPc6	listina
léta	léto	k1gNnSc2	léto
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
od	od	k7c2	od
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
byla	být	k5eAaImAgFnS	být
povolena	povolen	k2eAgFnSc1d1	povolena
výjimka	výjimka	k1gFnSc1	výjimka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zjistit	zjistit	k5eAaPmF	zjistit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dokumentu	dokument	k1gInSc2	dokument
<g/>
:	:	kIx,	:
Ale	ale	k8xC	ale
komu	kdo	k3yRnSc3	kdo
se	se	k3xPyFc4	se
zachce	zachtít	k5eAaPmIp3nS	zachtít
psát	psát	k5eAaImF	psát
obě	dva	k4xCgNnPc1	dva
léta	léto	k1gNnPc1	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
stvoření	stvoření	k1gNnSc6	stvoření
světa	svět	k1gInSc2	svět
i	i	k8xC	i
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
pořadí	pořadí	k1gNnSc1	pořadí
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
roku	rok	k1gInSc2	rok
používal	používat	k5eAaImAgInS	používat
také	také	k9	také
v	v	k7c6	v
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
římskokatolický	římskokatolický	k2eAgInSc4d1	římskokatolický
církevní	církevní	k2eAgInSc4d1	církevní
rok	rok	k1gInSc4	rok
začíná	začínat	k5eAaImIp3nS	začínat
začíná	začínat	k5eAaImIp3nS	začínat
prvním	první	k4xOgInSc7	první
dnem	den	k1gInSc7	den
Adventu	advent	k1gInSc2	advent
<g/>
,	,	kIx,	,
nedělí	neděle	k1gFnSc7	neděle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
dni	den	k1gInSc3	den
sv.	sv.	kA	sv.
Ondřeje	Ondřej	k1gMnSc2	Ondřej
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzimní	podzimní	k2eAgInSc1d1	podzimní
den	den	k1gInSc1	den
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
novoroční	novoroční	k2eAgInSc4d1	novoroční
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
francouzského	francouzský	k2eAgInSc2d1	francouzský
revolučního	revoluční	k2eAgInSc2d1	revoluční
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1793	[number]	k4	1793
a	a	k8xC	a
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
primidi	primid	k1gMnPc1	primid
Vendemiè	Vendemiè	k1gFnPc2	Vendemiè
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
prvního	první	k4xOgInSc2	první
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Datum	datum	k1gNnSc4	datum
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
==	==	k?	==
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Slované	Slovan	k1gMnPc1	Slovan
používali	používat	k5eAaImAgMnP	používat
lunisolární	lunisolární	k2eAgInSc4d1	lunisolární
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
roku	rok	k1gInSc2	rok
určovala	určovat	k5eAaImAgFnS	určovat
podle	podle	k7c2	podle
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
délka	délka	k1gFnSc1	délka
měsíců	měsíc	k1gInPc2	měsíc
podle	podle	k7c2	podle
fází	fáze	k1gFnPc2	fáze
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInSc4d1	měsíční
kruh	kruh	k1gInSc4	kruh
začínali	začínat	k5eAaImAgMnP	začínat
novoluním	novoluní	k1gNnSc7	novoluní
připadajícím	připadající	k2eAgFnPc3d1	připadající
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
dnech	den	k1gInPc6	den
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
misionářů	misionář	k1gMnPc2	misionář
byl	být	k5eAaImAgMnS	být
českou	český	k2eAgFnSc7d1	Česká
panovnickou	panovnický	k2eAgFnSc7d1	panovnická
kanceláří	kancelář	k1gFnSc7	kancelář
používán	používán	k2eAgInSc1d1	používán
vánoční	vánoční	k2eAgInSc1d1	vánoční
datovací	datovací	k2eAgInSc1d1	datovací
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
připadl	připadnout	k5eAaPmAgInS	připadnout
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
..	..	k?	..
Kancelář	kancelář	k1gFnSc1	kancelář
pražských	pražský	k2eAgMnPc2d1	pražský
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
ho	on	k3xPp3gInSc2	on
používala	používat	k5eAaImAgFnS	používat
důsledně	důsledně	k6eAd1	důsledně
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
nazýván	nazývat	k5eAaImNgInS	nazývat
stilus	stilus	k1gInSc1	stilus
Pragensis	Pragensis	k1gFnSc2	Pragensis
<g/>
.	.	kIx.	.
</s>
<s>
Přese	přese	k7c4	přese
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc3	jeho
používání	používání	k1gNnSc3	používání
přežívalo	přežívat	k5eAaImAgNnS	přežívat
až	až	k9	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
poznat	poznat	k5eAaPmF	poznat
z	z	k7c2	z
panovnických	panovnický	k2eAgFnPc2d1	panovnická
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc2d1	soukromá
písemností	písemnost	k1gFnPc2	písemnost
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc4	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
diplomatických	diplomatický	k2eAgInPc6d1	diplomatický
pramenech	pramen	k1gInPc6	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
narativních	narativní	k2eAgFnPc6d1	narativní
(	(	kIx(	(
<g/>
na	na	k7c4	na
příběhy	příběh	k1gInPc4	příběh
orientovaných	orientovaný	k2eAgInPc6d1	orientovaný
<g/>
)	)	kIx)	)
pramenech	pramen	k1gInPc6	pramen
však	však	k9	však
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
počátkem	počátek	k1gInSc7	počátek
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
počítá	počítat	k5eAaImIp3nS	počítat
již	již	k9	již
Kosmas	Kosmas	k1gMnSc1	Kosmas
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgMnPc1d1	následující
kronikáři	kronikář	k1gMnPc1	kronikář
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
však	však	k9	však
většinou	většinou	k6eAd1	většinou
nedrželi	držet	k5eNaImAgMnP	držet
<g/>
.	.	kIx.	.
</s>
<s>
Českými	český	k2eAgFnPc7d1	Česká
zeměmi	zem	k1gFnPc7	zem
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
více	hodně	k6eAd2	hodně
šířit	šířit	k5eAaImF	šířit
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
ve	v	k7c6	v
století	století	k1gNnSc6	století
následujícím	následující	k2eAgNnSc6d1	následující
se	se	k3xPyFc4	se
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
ujal	ujmout	k5eAaPmAgInS	ujmout
nejspíše	nejspíše	k9	nejspíše
vlivem	vliv	k1gInSc7	vliv
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
díky	díky	k7c3	díky
tisku	tisk	k1gInSc3	tisk
kalendářů	kalendář	k1gInPc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
jako	jako	k8xC	jako
církevní	církevní	k2eAgInSc4d1	církevní
svátek	svátek	k1gInSc4	svátek
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
v	v	k7c6	v
římskoněmecké	římskoněmecký	k2eAgFnSc6d1	římskoněmecká
říši	říš	k1gFnSc6	říš
zaveden	zaveden	k2eAgInSc4d1	zaveden
za	za	k7c2	za
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
..	..	k?	..
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
oddělen	oddělen	k2eAgInSc1d1	oddělen
občanský	občanský	k2eAgInSc1d1	občanský
rok	rok	k1gInSc1	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
církevního	církevní	k2eAgInSc2d1	církevní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Drobnosti	drobnost	k1gFnSc6	drobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
Zemi	zem	k1gFnSc6	zem
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
neobydlený	obydlený	k2eNgInSc4d1	neobydlený
atol	atol	k1gInSc4	atol
Caroline	Carolin	k1gInSc5	Carolin
<g/>
,	,	kIx,	,
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
ostrov	ostrov	k1gInSc4	ostrov
státu	stát	k1gInSc2	stát
Kiribati	Kiribati	k1gMnPc2	Kiribati
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgInSc4d1	čínský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
Hogmanay	Hogmanaa	k1gFnPc1	Hogmanaa
</s>
</p>
<p>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
</s>
</p>
<p>
<s>
Muslimský	muslimský	k2eAgInSc4d1	muslimský
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
Thajský	thajský	k2eAgInSc1d1	thajský
nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
S.	S.	kA	S.
I.	I.	kA	I.
Selešnikov	Selešnikov	k1gInSc1	Selešnikov
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
čas	čas	k1gInSc1	čas
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Bláhová	bláhový	k2eAgFnSc1d1	bláhová
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historická	historický	k2eAgFnSc1d1	historická
chronologie	chronologie	k1gFnSc1	chronologie
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Libri	Libri	k6eAd1	Libri
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7277-024-1	[number]	k4	80-7277-024-1
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
New	New	k1gMnSc1	New
Year	Year	k1gMnSc1	Year
celebrations	celebrations	k6eAd1	celebrations
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
New	New	k1gMnSc1	New
Year	Year	k1gMnSc1	Year
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
