<s>
Sněhurka	Sněhurka	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Sněhurka	Sněhurka	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Otto	Otto	k1gMnSc1
Kubel	Kubel	k1gMnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
z	z	k7c2
islandského	islandský	k2eAgInSc2d1
překladu	překlad	k1gInSc2
pohádky	pohádka	k1gFnSc2
</s>
<s>
Sněhurka	Sněhurka	k1gFnSc1
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
dívky	dívka	k1gFnSc2
neobyčejné	obyčejný	k2eNgFnSc2d1
krásy	krása	k1gFnSc2
ze	z	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
Sněhurka	Sněhurka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
známá	známý	k2eAgFnSc1d1
v	v	k7c6
mnoha	mnoho	k4c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autory	autor	k1gMnPc7
nejznámější	známý	k2eAgFnSc2d3
verze	verze	k1gFnSc2
pohádky	pohádka	k1gFnSc2
jsou	být	k5eAaImIp3nP
bratři	bratr	k1gMnPc1
Grimmové	Grimm	k1gMnSc2
<g/>
:	:	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
kouzelné	kouzelný	k2eAgNnSc1d1
zrcadlo	zrcadlo	k1gNnSc1
i	i	k8xC
sedm	sedm	k4xCc1
trpaslíků	trpaslík	k1gMnPc2
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
vlastních	vlastní	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
v	v	k7c6
broadwayské	broadwayský	k2eAgFnSc6d1
hře	hra	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
Sněhurka	Sněhurka	k1gFnSc1
a	a	k8xC
sedm	sedm	k4xCc1
trpaslíků	trpaslík	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
natočilo	natočit	k5eAaBmAgNnS
studio	studio	k1gNnSc1
Walta	Walt	k1gMnSc2
Disneyho	Disney	k1gMnSc2
na	na	k7c4
tento	tento	k3xDgInSc4
motiv	motiv	k1gInSc4
celovečerní	celovečerní	k2eAgInSc4d1
animovaný	animovaný	k2eAgInSc4d1
film	film	k1gInSc4
s	s	k7c7
tímtéž	týž	k3xTgInSc7
názvem	název	k1gInSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
trpaslíci	trpaslík	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
jiná	jiný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
setkáváme	setkávat	k5eAaImIp1nP
s	s	k7c7
názvem	název	k1gInSc7
pohádky	pohádka	k1gFnSc2
pouze	pouze	k6eAd1
Sněhurka	Sněhurka	k1gFnSc1
<g/>
:	:	kIx,
zde	zde	k6eAd1
je	být	k5eAaImIp3nS
namístě	namístě	k?
se	se	k3xPyFc4
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
<g/>
-li	-li	k?
o	o	k7c4
Sněhurku	Sněhurka	k1gFnSc4
a	a	k8xC
sedm	sedm	k4xCc4
trpaslíků	trpaslík	k1gMnPc2
či	či	k8xC
o	o	k7c4
jinou	jiný	k2eAgFnSc4d1
pohádku	pohádka	k1gFnSc4
z	z	k7c2
dílny	dílna	k1gFnSc2
bratří	bratr	k1gMnPc2
Grimmů	Grimm	k1gInPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
o	o	k7c4
Sněhurku	Sněhurka	k1gFnSc4
a	a	k8xC
Růženku	Růženka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Pohádka	pohádka	k1gFnSc1
o	o	k7c6
Sněhurce	Sněhurka	k1gFnSc6
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
starých	starý	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýtus	mýtus	k1gInSc1
o	o	k7c6
zlé	zlý	k2eAgFnSc6d1
čarodějnici	čarodějnice	k1gFnSc6
<g/>
,	,	kIx,
Sněhurčině	Sněhurčin	k2eAgFnSc6d1
maceše	macecha	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
pravděpodobně	pravděpodobně	k6eAd1
poprvé	poprvé	k6eAd1
zaznamenán	zaznamenat	k5eAaPmNgInS
v	v	k7c6
díle	dílo	k1gNnSc6
francouzského	francouzský	k2eAgMnSc2d1
kronikáře	kronikář	k1gMnSc2
Jeana	Jean	k1gMnSc2
Froissarta	Froissart	k1gMnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Froissart	Froissart	k1gInSc1
píše	psát	k5eAaImIp3nS
o	o	k7c6
událostech	událost	k1gFnPc6
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
propadl	propadnout	k5eAaPmAgInS
šílenství	šílenství	k1gNnSc4
a	a	k8xC
prakticky	prakticky	k6eAd1
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgInSc1d1
vládnout	vládnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
svých	svůj	k3xOyFgInPc2
záchvatů	záchvat	k1gInPc2
nepoznával	poznávat	k5eNaImAgInS
svou	svůj	k3xOyFgFnSc4
ženu	žena	k1gFnSc4
Isabelu	Isabela	k1gFnSc4
Bavorskou	bavorský	k2eAgFnSc4d1
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
miluje	milovat	k5eAaImIp3nS
Valentinu	Valentina	k1gFnSc4
Visconti	Visconť	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
žena	žena	k1gFnSc1
Karlova	Karlův	k2eAgMnSc2d1
bratra	bratr	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Orleánského	orleánský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
poddaných	poddaný	k1gMnPc2
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
velmi	velmi	k6eAd1
oblíbený	oblíbený	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
najít	najít	k5eAaPmF
viníka	viník	k1gMnSc4
královy	králův	k2eAgFnSc2d1
duševní	duševní	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
vyprávět	vyprávět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
nápadně	nápadně	k6eAd1
krásná	krásný	k2eAgFnSc1d1
Valentina	Valentina	k1gFnSc1
Visconti	Visconť	k1gFnSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
čarodějnice	čarodějnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tráví	trávit	k5eAaImIp3nS
malé	malý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
jedovatými	jedovatý	k2eAgNnPc7d1
jablky	jablko	k1gNnPc7
a	a	k8xC
vlastní	vlastnit	k5eAaImIp3nS
kouzelné	kouzelný	k2eAgNnSc1d1
zrcadlo	zrcadlo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pohádky	pohádka	k1gFnSc2
o	o	k7c6
Sněhurce	Sněhurka	k1gFnSc6
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c6
dítěti	dítě	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
si	se	k3xPyFc3
bezdětní	bezdětný	k2eAgMnPc1d1
manželé	manžel	k1gMnPc1
uplácají	uplácat	k5eAaPmIp3nP
ze	z	k7c2
sněhu	sníh	k1gInSc2
(	(	kIx(
<g/>
živá	živý	k2eAgFnSc1d1
sněhulačka	sněhulačka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
Sněhurka	Sněhurka	k1gFnSc1
však	však	k9
žije	žít	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
zimě	zima	k1gFnSc6
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
ostatní	ostatní	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
slaví	slavit	k5eAaImIp3nP
jaro	jaro	k6eAd1
skákáním	skákání	k1gNnSc7
přes	přes	k7c4
oheň	oheň	k1gInSc4
<g/>
,	,	kIx,
Sněhurka	Sněhurka	k1gFnSc1
se	se	k3xPyFc4
nad	nad	k7c7
ohněm	oheň	k1gInSc7
vypaří	vypařit	k5eAaPmIp3nP
(	(	kIx(
<g/>
podobný	podobný	k2eAgInSc4d1
motiv	motiv	k1gInSc4
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
v	v	k7c6
pohádkové	pohádkový	k2eAgFnSc6d1
divadelní	divadelní	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Jaroslava	Jaroslav	k1gMnSc2
Kvapila	Kvapil	k1gMnSc2
Princezna	princezna	k1gFnSc1
Pampeliška	pampeliška	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
princezna	princezna	k1gFnSc1
na	na	k7c4
podzim	podzim	k1gInSc4
odkvete	odkvést	k5eAaPmIp3nS
jako	jako	k8xC,k8xS
květy	květ	k1gInPc4
pampelišky	pampeliška	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohádka	pohádka	k1gFnSc1
o	o	k7c6
Sněhurce	Sněhurka	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
jako	jako	k9
hlavní	hlavní	k2eAgInSc4d1
motiv	motiv	k1gInSc4
tv	tv	k?
seriálu	seriál	k1gInSc2
Once	Once	k1gFnSc4
Upon	Upon	k1gNnSc1
a	a	k8xC
Time	Time	k1gNnSc1
z	z	k7c2
produkce	produkce	k1gFnSc2
americké	americký	k2eAgFnSc2d1
ABC	ABC	kA
television	television	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kalendář	kalendář	k1gInSc1
</s>
<s>
Od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
a	a	k8xC
severním	severní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
zaznamenána	zaznamenat	k5eAaPmNgFnS
tradice	tradice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Svátku	svátek	k1gInSc2
Sněhurky	Sněhurka	k1gFnSc2
(	(	kIx(
<g/>
Schneeweissfest	Schneeweissfest	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
slaví	slavit	k5eAaImIp3nS
první	první	k4xOgNnSc4
úterý	úterý	k1gNnSc4
po	po	k7c6
podzimní	podzimní	k2eAgFnSc6d1
rovnodennosti	rovnodennost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátek	svátek	k1gInSc4
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
pravděpodobně	pravděpodobně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
termínem	termín	k1gInSc7
sklizně	sklizeň	k1gFnSc2
některých	některý	k3yIgFnPc2
podzimních	podzimní	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
jablek	jablko	k1gNnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
tradičně	tradičně	k6eAd1
pěstovaných	pěstovaný	k2eAgInPc2d1
(	(	kIx(
<g/>
např.	např.	kA
Grávštýnské	Grávštýnská	k1gFnSc6
červené	červený	k2eAgFnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odlišné	odlišný	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
ruského	ruský	k2eAgInSc2d1
severu	sever	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
Sněhurka	Sněhurka	k1gFnSc1
naopak	naopak	k6eAd1
připomínána	připomínat	k5eAaImNgFnS
v	v	k7c6
březnu	březen	k1gInSc6
až	až	k8xS
dubnu	duben	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
příchodem	příchod	k1gInSc7
jara	jaro	k1gNnSc2
a	a	k8xC
mizením	mizení	k1gNnSc7
sněhové	sněhový	k2eAgFnSc2d1
pokrývky	pokrývka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sněguročka	Sněguročka	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NEJEDLÝ	Nejedlý	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středověký	středověký	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
o	o	k7c6
Meluzíně	Meluzína	k1gFnSc6
a	a	k8xC
rodová	rodový	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
Lucemburků	Lucemburk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgInPc1d1
Břežany	Břežany	k1gInPc1
<g/>
:	:	kIx,
Scriptorium	Scriptorium	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86197	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
68	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.cesky-jazyk.cz/ctenarsky-denik/jaroslav-kvapil/princezna-pampeliska.html	http://www.cesky-jazyk.cz/ctenarsky-denik/jaroslav-kvapil/princezna-pampeliska.html	k1gInSc1
<g/>
↑	↑	k?
FRANKE	Frank	k1gMnSc5
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gMnSc1
Brüder	Brüder	k1gMnSc1
Grimm	Grimm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihr	Ihr	k1gFnSc1
Leben	Lebna	k1gFnPc2
und	und	k?
Wirken	Wirkna	k1gFnPc2
<g/>
,	,	kIx,
in	in	k?
gemeinfasslicher	gemeinfasslichra	k1gFnPc2
Weise	Weise	k1gFnSc1
dargestellt	dargestellt	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dresden	Dresdna	k1gFnPc2
und	und	k?
Leipzig	Leipzig	k1gMnSc1
<g/>
:	:	kIx,
Carl	Carl	k1gMnSc1
Reißner	Reißner	k1gMnSc1
<g/>
,	,	kIx,
1889	#num#	k4
<g/>
.	.	kIx.
176	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PROPP	PROPP	kA
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Märchen	Märchen	k1gInSc1
der	drát	k5eAaImRp2nS
Brüder	Brüder	k1gMnSc1
Grimm	Grimm	k1gMnSc1
im	im	k?
russischen	russischen	k1gInSc1
Norden	Nordna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsches	Deutsches	k1gMnSc1
Jahrbuch	Jahrbuch	k1gMnSc1
für	für	k?
Volkskunde	Volkskund	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Humboldt-Universität	Humboldt-Universität	k1gMnSc1
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
-	-	kIx~
<g/>
122	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sněhurka	Sněhurka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Sněhurka	Sněhurka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4116406-4	4116406-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79063718	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
175786387	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79063718	#num#	k4
</s>
