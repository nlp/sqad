<s desamb="1">
Autory	autor	k1gMnPc7
nejznámější	známý	k2eAgFnSc2d3
verze	verze	k1gFnSc2
pohádky	pohádka	k1gFnSc2
jsou	být	k5eAaImIp3nP
bratři	bratr	k1gMnPc1
Grimmové	Grimm	k1gMnSc2
<g/>
:	:	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
kouzelné	kouzelný	k2eAgNnSc1d1
zrcadlo	zrcadlo	k1gNnSc1
i	i	k8xC
sedm	sedm	k4xCc1
trpaslíků	trpaslík	k1gMnPc2
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
vlastních	vlastní	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
v	v	k7c6
broadwayské	broadwayský	k2eAgFnSc6d1
hře	hra	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
Sněhurka	Sněhurka	k1gFnSc1
a	a	k8xC
sedm	sedm	k4xCc1
trpaslíků	trpaslík	k1gMnPc2
<g/>
.	.	kIx.
</s>