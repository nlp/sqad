<s>
CRISPR	CRISPR	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
wikifikovat	wikifikovat	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
,	,	kIx,
reference	reference	k1gFnSc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
standardně	standardně	k6eAd1
citačními	citační	k2eAgFnPc7d1
šablonami	šablona	k1gFnPc7
</s>
<s>
Princip	princip	k1gInSc1
systému	systém	k1gInSc2
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas	Cas	k1gMnSc2
v	v	k7c6
prokaryotní	prokaryotní	k2eAgFnSc6d1
buňce	buňka	k1gFnSc6
</s>
<s>
CRISPR	CRISPR	kA
jsou	být	k5eAaImIp3nP
segmenty	segment	k1gInPc1
nahromaděných	nahromaděný	k2eAgFnPc2d1
pravidelně	pravidelně	k6eAd1
rozmístěných	rozmístěný	k2eAgFnPc2d1
krátkých	krátká	k1gFnPc2
palindromických	palindromický	k2eAgFnPc2d1
repetic	repetice	k1gFnPc2
(	(	kIx(
<g/>
Clustered	Clustered	k1gMnSc1
Regularly	Regularla	k1gFnSc2
Interspaced	Interspaced	k1gMnSc1
Short	Shorta	k1gFnPc2
Palindromic	Palindromice	k1gFnPc2
Repeats	Repeats	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
úseky	úsek	k1gInPc1
prokaryotické	prokaryotický	k2eAgFnSc2d1
DNA	DNA	kA
obsahující	obsahující	k2eAgFnSc2d1
krátké	krátký	k2eAgFnSc2d1
repetice	repetice	k1gFnSc2
nukleotidů	nukleotid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
repetic	repetice	k1gFnPc2
je	být	k5eAaImIp3nS
následována	následovat	k5eAaImNgFnS
krátkými	krátký	k2eAgInPc7d1
segmenty	segment	k1gInPc7
tzv.	tzv.	kA
spacer	spacero	k1gNnPc2
DNA	DNA	kA
<g/>
,	,	kIx,
získanými	získaný	k2eAgInPc7d1
při	při	k7c6
předchozích	předchozí	k2eAgNnPc6d1
setkáních	setkání	k1gNnPc6
s	s	k7c7
příslušnými	příslušný	k2eAgInPc7d1
fágy	fág	k1gInPc7
nebo	nebo	k8xC
plazmidy	plazmid	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
CRISPR	CRISPR	kA
/	/	kIx~
Cas	Cas	k1gMnSc2
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
prokaryotický	prokaryotický	k2eAgInSc4d1
imunitní	imunitní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
zajišťující	zajišťující	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
vůči	vůči	k7c3
cizím	cizí	k2eAgInPc3d1
genetickým	genetický	k2eAgInPc3d1
elementům	element	k1gInPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
plazmidy	plazmida	k1gFnPc4
nebo	nebo	k8xC
fágy	fág	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
tedy	tedy	k9
formu	forma	k1gFnSc4
získané	získaný	k2eAgFnSc2d1
imunity	imunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spacerová	Spacerový	k2eAgFnSc1d1
DNA	dna	k1gFnSc1
tyto	tento	k3xDgInPc4
exogenní	exogenní	k2eAgInPc4d1
genetické	genetický	k2eAgInPc4d1
elementy	element	k1gInPc4
rozpozná	rozpoznat	k5eAaPmIp3nS
a	a	k8xC
deaktivuje	deaktivovat	k5eAaImIp3nS
způsobem	způsob	k1gInSc7
analogickým	analogický	k2eAgInSc7d1
s	s	k7c7
mechanismem	mechanismus	k1gInSc7
RNA	RNA	kA
interference	interference	k1gFnSc2
v	v	k7c6
eukaryotických	eukaryotický	k2eAgInPc6d1
organismech	organismus	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
CRISPR	CRISPR	kA
lokusy	lokus	k1gInPc1
již	již	k9
byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
u	u	k7c2
přibližně	přibližně	k6eAd1
40	#num#	k4
<g/>
%	%	kIx~
osekvenovaných	osekvenovaný	k2eAgFnPc2d1
bakterií	bakterie	k1gFnPc2
a	a	k8xC
u	u	k7c2
90	#num#	k4
<g/>
%	%	kIx~
archeí	archeí	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technologie	technologie	k1gFnSc1
CRISPR	CRISPR	kA
interference	interference	k1gFnSc1
má	mít	k5eAaImIp3nS
enormní	enormní	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
k	k	k7c3
uplatnění	uplatnění	k1gNnSc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
pozměňování	pozměňování	k1gNnSc2
lidské	lidský	k2eAgFnSc2d1
zárodečné	zárodečný	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
,	,	kIx,
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
i	i	k8xC
dalších	další	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
modifikace	modifikace	k1gFnSc1
genů	gen	k1gInPc2
potravinářských	potravinářský	k2eAgFnPc2d1
plodin	plodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doručením	doručení	k1gNnSc7
proteinu	protein	k1gInSc2
Cas	Cas	k1gFnSc2
<g/>
9	#num#	k4
a	a	k8xC
příslušné	příslušný	k2eAgNnSc4d1
naváděcí	naváděcí	k2eAgNnSc4d1
RNA	RNA	kA
do	do	k7c2
buňky	buňka	k1gFnSc2
lze	lze	k6eAd1
genom	genom	k1gInSc4
cílového	cílový	k2eAgInSc2d1
organismu	organismus	k1gInSc2
rozstřihnout	rozstřihnout	k5eAaPmF
v	v	k7c6
jakémkoli	jakýkoli	k3yIgNnSc6
požadovaném	požadovaný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
CRISPRy	CRISPRa	k1gFnSc2
ve	v	k7c6
spojení	spojení	k1gNnSc6
se	s	k7c7
specifickými	specifický	k2eAgFnPc7d1
endonukleázami	endonukleáza	k1gFnPc7
<g/>
,	,	kIx,
určenými	určený	k2eAgInPc7d1
k	k	k7c3
editování	editování	k1gNnSc3
genomu	genom	k1gInSc2
či	či	k8xC
cílené	cílený	k2eAgFnSc3d1
regulaci	regulace	k1gFnSc3
genů	gen	k1gInPc2
<g/>
,	,	kIx,
již	již	k9
byly	být	k5eAaImAgFnP
otestovány	otestovat	k5eAaPmNgFnP
v	v	k7c6
rozličných	rozličný	k2eAgInPc6d1
organismech	organismus	k1gInPc6
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
etického	etický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
znepokojivá	znepokojivý	k2eAgFnSc1d1
zejména	zejména	k9
možnost	možnost	k1gFnSc1
editace	editace	k1gFnSc2
lidské	lidský	k2eAgFnSc2d1
zárodečné	zárodečný	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
CRISPR	CRISPR	kA
je	být	k5eAaImIp3nS
přirozenou	přirozený	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
bakteriálních	bakteriální	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakterie	bakterie	k1gFnPc1
mají	mít	k5eAaImIp3nP
schopnost	schopnost	k1gFnSc4
začleňovat	začleňovat	k5eAaImF
cizí	cizí	k2eAgNnPc4d1
DNA	dno	k1gNnPc4
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
účelně	účelně	k6eAd1
získávat	získávat	k5eAaImF
poškozenou	poškozený	k2eAgFnSc4d1
DNA	dno	k1gNnPc4
přímo	přímo	k6eAd1
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
okolního	okolní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nahromaděné	nahromaděný	k2eAgFnPc1d1
repetice	repetice	k1gFnPc1
byly	být	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
popsány	popsat	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
u	u	k7c2
bakterie	bakterie	k1gFnSc2
Escherichia	Escherichium	k1gNnSc2
coli	col	k1gFnSc2
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Osace	Osaka	k1gFnSc6
<g/>
,	,	kIx,
vědcem	vědec	k1gMnSc7
Yoshizumim	Yoshizumim	k1gMnSc1
Ishinou	Ishina	k1gMnSc7
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ovšem	ovšem	k9
jejich	jejich	k3xOp3gFnSc2
funkce	funkce	k1gFnSc2
nebyla	být	k5eNaImAgFnS
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Nezávisle	závisle	k6eNd1
na	na	k7c6
tom	ten	k3xDgNnSc6
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
repetice	repetice	k1gFnPc1
pozorovány	pozorován	k2eAgFnPc1d1
také	také	k9
španělským	španělský	k2eAgMnSc7d1
badatelem	badatel	k1gMnSc7
Franciscem	Francisce	k1gMnSc7
Mojicou	Mojica	k1gMnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
krátkými	krátký	k2eAgFnPc7d1
pravidelně	pravidelně	k6eAd1
rozmístěnými	rozmístěný	k2eAgFnPc7d1
repeticemi	repetice	k1gFnPc7
(	(	kIx(
<g/>
SRSR	SRSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byly	být	k5eAaImAgFnP
podobné	podobný	k2eAgFnPc1d1
repetice	repetice	k1gFnPc1
identifikovány	identifikován	k2eAgFnPc1d1
u	u	k7c2
dalších	další	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
bakterií	bakterie	k1gFnPc2
a	a	k8xC
archeí	arche	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Na	na	k7c4
Mojicův	Mojicův	k2eAgInSc4d1
návrh	návrh	k1gInSc4
byly	být	k5eAaImAgFnP
SRSR	SRSR	kA
roku	rok	k1gInSc2
2002	#num#	k4
přejmenovány	přejmenován	k2eAgInPc4d1
na	na	k7c4
CRISPR	CRISPR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Poprvé	poprvé	k6eAd1
vyvstalo	vyvstat	k5eAaPmAgNnS
podezření	podezření	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
CRISPR	CRISPR	kA
lokusy	lokus	k1gInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
zodpovědné	zodpovědný	k2eAgNnSc1d1
za	za	k7c4
zprostředkování	zprostředkování	k1gNnSc4
adaptivní	adaptivní	k2eAgFnSc2d1
imunity	imunita	k1gFnSc2
u	u	k7c2
mikrobů	mikrob	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Práce	práce	k1gFnSc1
zahrhující	zahrhující	k2eAgNnSc1d1
tyto	tento	k3xDgFnPc4
hypotézy	hypotéza	k1gFnPc4
byla	být	k5eAaImAgFnS
prvotně	prvotně	k6eAd1
odmítnuta	odmítnout	k5eAaPmNgFnS
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
vysoce	vysoce	k6eAd1
impaktovaných	impaktovaný	k2eAgInPc2d1
žurnálů	žurnál	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byla	být	k5eAaImAgFnS
objevena	objeven	k2eAgFnSc1d1
řada	řada	k1gFnSc1
genů	gen	k1gInPc2
<g/>
,	,	kIx,
asociovaných	asociovaný	k2eAgInPc2d1
s	s	k7c7
CRISPRovýmy	CRISPRovýmo	k1gNnPc7
repeticemi	repetice	k1gFnPc7
<g/>
,	,	kIx,
pojmenovaných	pojmenovaný	k2eAgFnPc6d1
Cas	Cas	k1gFnPc6
<g/>
,	,	kIx,
neboli	neboli	k8xC
CRISPR-associated	CRISPR-associated	k1gMnSc1
genes	genes	k1gMnSc1
(	(	kIx(
<g/>
s	s	k7c7
CRISPRem-asociované	CRISPRem-asociovaný	k2eAgInPc1d1
geny	gen	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cas	Cas	k1gMnPc1
geny	gen	k1gInPc4
kódují	kódovat	k5eAaBmIp3nP
nukleázy	nukleáza	k1gFnPc1
nebo	nebo	k8xC
helikázy	helikáza	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
enzymy	enzym	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
stříhají	stříhat	k5eAaImIp3nP
(	(	kIx(
<g/>
nukleázy	nukleáza	k1gFnPc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
rozvolňují	rozvolňovat	k5eAaImIp3nP
(	(	kIx(
<g/>
helikázy	helikáza	k1gFnPc4
<g/>
)	)	kIx)
dvoušroubovici	dvoušroubovice	k1gFnSc3
DNA	DNA	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc1
nezávislé	závislý	k2eNgFnPc1d1
výzkumné	výzkumný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
prokázaly	prokázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgNnSc1
DNA	dna	k1gFnSc1
spacery	spacera	k1gFnSc2
z	z	k7c2
CRISPR	CRISPR	kA
lokusu	lokus	k1gInSc6
jsou	být	k5eAaImIp3nP
odvozeny	odvozen	k2eAgFnPc1d1
z	z	k7c2
fágové	fágová	k1gFnSc2
a	a	k8xC
extrachromozomální	extrachromozomální	k2eAgNnPc4d1
DNA	dno	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
plazmidy	plazmida	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Spacery	Spacer	k1gInPc1
byly	být	k5eAaImAgInP
identifikovány	identifikovat	k5eAaBmNgInP
jako	jako	k8xC,k8xS
fragmenty	fragment	k1gInPc1
DNA	dno	k1gNnSc2
pocházející	pocházející	k2eAgNnSc4d1
z	z	k7c2
virů	vir	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
již	již	k6eAd1
dříve	dříve	k6eAd2
napadly	napadnout	k5eAaPmAgInP
danou	daný	k2eAgFnSc4d1
bakteriální	bakteriální	k2eAgFnSc4d1
buňku	buňka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
DNA	dno	k1gNnSc2
spacerů	spacer	k1gMnPc2
byl	být	k5eAaImAgMnS
známkou	známka	k1gFnSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas	Cas	k1gMnSc4
systém	systém	k1gInSc1
mohl	moct	k5eAaImAgInS
hrát	hrát	k5eAaImF
roli	role	k1gFnSc4
v	v	k7c6
adaptivní	adaptivní	k2eAgFnSc6d1
imunitě	imunita	k1gFnSc6
bakterií	bakterie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Koonin	Koonin	k1gInSc1
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
tým	tým	k1gInSc4
navrhli	navrhnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
DNA	dna	k1gFnSc1
spacery	spacera	k1gFnSc2
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
templáty	templát	k1gInPc4
pro	pro	k7c4
transkripci	transkripce	k1gFnSc4
RNA	RNA	kA
molekul	molekula	k1gFnPc2
analogicky	analogicky	k6eAd1
systému	systém	k1gInSc2
zvanému	zvaný	k2eAgInSc3d1
RNA	RNA	kA
interference	interference	k1gFnSc1
uplatňovanému	uplatňovaný	k2eAgInSc3d1
v	v	k7c6
eukaryotických	eukaryotický	k2eAgFnPc6d1
buňkách	buňka	k1gFnPc6
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
Barrangou	Barranga	k1gFnSc7
<g/>
,	,	kIx,
Horvath	Horvath	k1gInSc1
(	(	kIx(
<g/>
odborníci	odborník	k1gMnPc1
potravinového	potravinový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
v	v	k7c6
Daniscu	Daniscus	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
skupina	skupina	k1gFnSc1
Moineau	Moineaus	k1gInSc2
na	na	k7c4
Université	Universitý	k2eAgInPc4d1
Laval	Laval	k1gInSc4
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
ukázali	ukázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
využít	využít	k5eAaPmF
spacerovou	spacerová	k1gFnSc4
DNA	dno	k1gNnSc2
ke	k	k7c3
změně	změna	k1gFnSc3
rezistence	rezistence	k1gFnSc1
Streptococcus	Streptococcus	k1gMnSc1
thermophilus	thermophilus	k1gInSc1
vůči	vůči	k7c3
útoku	útok	k1gInSc3
fágů	fág	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
ukázáno	ukázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
CRISPR	CRISPR	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
využit	využít	k5eAaPmNgInS
jako	jako	k9
nástroj	nástroj	k1gInSc1
genového	genový	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
pro	pro	k7c4
editaci	editace	k1gFnSc4
genů	gen	k1gInPc2
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
buněčné	buněčný	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
již	již	k6eAd1
systém	systém	k1gInSc1
úspěšně	úspěšně	k6eAd1
vyzkoušen	vyzkoušet	k5eAaPmNgInS
v	v	k7c6
široké	široký	k2eAgFnSc6d1
škále	škála	k1gFnSc6
organismů	organismus	k1gInPc2
od	od	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
kvasinek	kvasinka	k1gFnPc2
(	(	kIx(
<g/>
S.	S.	kA
cerevisiae	cerevisia	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Dania	Danium	k1gNnSc2
pruhovaného	pruhovaný	k2eAgNnSc2d1
(	(	kIx(
<g/>
D.	D.	kA
rerio	rerio	k1gNnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
octomilky	octomilka	k1gFnPc4
(	(	kIx(
<g/>
D.	D.	kA
melanogaster	melanogaster	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Axolotl	axolotl	k1gMnSc1
(	(	kIx(
<g/>
A.	A.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
mexicanum	mexicanum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
hlístice	hlístice	k1gFnSc2
(	(	kIx(
<g/>
C.	C.	kA
elegans	elegans	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
myší	myš	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
opic	opice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
až	až	k9
po	po	k7c4
lidská	lidský	k2eAgNnPc4d1
embrya	embryo	k1gNnPc4
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
CRISPR	CRISPR	kA
byl	být	k5eAaImAgInS
modifikován	modifikovat	k5eAaBmNgInS
do	do	k7c2
podoby	podoba	k1gFnSc2
programovatelných	programovatelný	k2eAgInPc2d1
transkripčních	transkripční	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vědcům	vědec	k1gMnPc3
umožňují	umožňovat	k5eAaImIp3nP
přesně	přesně	k6eAd1
zacílit	zacílit	k5eAaPmF
a	a	k8xC
pak	pak	k6eAd1
aktivovat	aktivovat	k5eAaBmF
nebo	nebo	k8xC
umlčet	umlčet	k5eAaPmF
specifické	specifický	k2eAgInPc4d1
geny	gen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
knihovny	knihovna	k1gFnSc2
desítek	desítka	k1gFnPc2
tisíc	tisíc	k4xCgInPc2
ověřených	ověřený	k2eAgInPc2d1
naváděcích	naváděcí	k2eAgInPc2d1
RNA	RNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Soudní	soudní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
však	však	k8xC
roku	rok	k1gInSc2
2018	#num#	k4
metodu	metoda	k1gFnSc4
postavil	postavit	k5eAaPmAgMnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
GMO	GMO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cas	Cas	k?
<g/>
9	#num#	k4
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Doudna	Doudno	k1gNnSc2
a	a	k8xC
Emmanuelle	Emmanuella	k1gFnSc6
Charpentier	Charpentier	k1gInSc1
(	(	kIx(
<g/>
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
<g/>
)	)	kIx)
zkoumaly	zkoumat	k5eAaImAgFnP
s	s	k7c7
CRISPRem-asociované	CRISPRem-asociovaný	k2eAgInPc4d1
proteiny	protein	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
snaze	snaha	k1gFnSc6
lépe	dobře	k6eAd2
porozumět	porozumět	k5eAaPmF
mechanismu	mechanismus	k1gInSc3
využití	využití	k1gNnSc2
DNA	dno	k1gNnSc2
spacerů	spacer	k1gInPc2
v	v	k7c6
imunitní	imunitní	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
bakterií	bakterie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
vědkyně	vědkyně	k1gFnPc1
se	se	k3xPyFc4
zaměřily	zaměřit	k5eAaPmAgFnP
na	na	k7c4
jednodušší	jednoduchý	k2eAgInSc4d2
CRISPR	CRISPR	kA
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
bílkovinu	bílkovina	k1gFnSc4
zvanou	zvaný	k2eAgFnSc7d1
Cas	Cas	k1gFnSc7
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistily	zjistit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
bakterie	bakterie	k1gFnPc1
reagují	reagovat	k5eAaBmIp3nP
na	na	k7c4
napadení	napadení	k1gNnPc4
fágem	fág	k1gInSc7
přepisem	přepis	k1gInSc7
příslušného	příslušný	k2eAgInSc2d1
DNA	DNA	kA
spaceru	spacer	k1gInSc2
a	a	k8xC
přilehlých	přilehlý	k2eAgFnPc2d1
palindromických	palindromický	k2eAgFnPc2d1
sekvencí	sekvence	k1gFnPc2
do	do	k7c2
dlouhé	dlouhý	k2eAgFnSc2d1
molekuly	molekula	k1gFnSc2
RNA	RNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buňka	buňka	k1gFnSc1
pak	pak	k6eAd1
použije	použít	k5eAaPmIp3nS
tracrRNA	tracrRNA	k?
(	(	kIx(
<g/>
trans-activating	trans-activating	k1gInSc1
crRNA	crRNA	k?
<g/>
)	)	kIx)
a	a	k8xC
protein	protein	k1gInSc1
Cas	Cas	k1gFnSc2
<g/>
9	#num#	k4
k	k	k7c3
rozstříhání	rozstříhání	k1gNnSc3
této	tento	k3xDgFnSc2
dlouhé	dlouhý	k2eAgFnSc2d1
RNA	RNA	kA
molekuly	molekula	k1gFnSc2
na	na	k7c4
kratší	krátký	k2eAgInPc4d2
kousky	kousek	k1gInPc4
zvané	zvaný	k2eAgInPc4d1
crRNA	crRNA	k?
(	(	kIx(
<g/>
CRISPR	CRISPR	kA
RNA	RNA	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Cas	Cas	k?
<g/>
9	#num#	k4
je	být	k5eAaImIp3nS
nukleáza	nukleáza	k1gFnSc1
<g/>
,	,	kIx,
enzym	enzym	k1gInSc4
<g/>
,	,	kIx,
specializovaný	specializovaný	k2eAgInSc4d1
pro	pro	k7c4
stříhání	stříhání	k1gNnSc4
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
aktivní	aktivní	k2eAgFnPc4d1
„	„	k?
<g/>
stříhací	stříhací	k2eAgNnSc1d1
<g/>
“	“	k?
místa	místo	k1gNnSc2
(	(	kIx(
<g/>
HNH	HNH	kA
a	a	k8xC
RuvC	RuvC	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
pro	pro	k7c4
každé	každý	k3xTgNnSc4
vlákno	vlákno	k1gNnSc4
dvoušroubovice	dvoušroubovice	k1gFnSc1
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
jedno	jeden	k4xCgNnSc1
či	či	k8xC
obě	dva	k4xCgFnPc1
tato	tento	k3xDgNnPc1
aktivní	aktivní	k2eAgNnPc1d1
místa	místo	k1gNnPc1
deaktivovat	deaktivovat	k5eAaImF
při	při	k7c6
zachování	zachování	k1gNnSc6
schopnosti	schopnost	k1gFnSc2
Cas	Cas	k1gFnSc2
<g/>
9	#num#	k4
vyhledat	vyhledat	k5eAaPmF
a	a	k8xC
navázat	navázat	k5eAaPmF
cílovou	cílový	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
v	v	k7c6
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
Doudna	Doudna	k1gFnSc1
a	a	k8xC
Charpentier	Charpentier	k1gInSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
již	již	k6eAd1
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
vědecké	vědecký	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
)	)	kIx)
zkombinovaly	zkombinovat	k5eAaPmAgFnP
tracrRNA	tracrRNA	k?
a	a	k8xC
crRNA	crRNA	k?
do	do	k7c2
jediné	jediný	k2eAgFnSc2d1
hybridní	hybridní	k2eAgFnSc2d1
„	„	k?
<g/>
guide	guide	k6eAd1
<g/>
“	“	k?
RNA	RNA	kA
molekuly	molekula	k1gFnSc2
(	(	kIx(
<g/>
gRNA	gRNA	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
komplexu	komplex	k1gInSc6
s	s	k7c7
Cas	Cas	k1gFnSc7
<g/>
9	#num#	k4
schopna	schopen	k2eAgFnSc1d1
přesně	přesně	k6eAd1
zacílit	zacílit	k5eAaPmF
a	a	k8xC
rozstřihnout	rozstřihnout	k5eAaPmF
libovolnou	libovolný	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
v	v	k7c6
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
navrhly	navrhnout	k5eAaPmAgFnP
využití	využití	k1gNnPc1
takto	takto	k6eAd1
uměle	uměle	k6eAd1
sestavené	sestavený	k2eAgFnSc2d1
naváděcí	naváděcí	k2eAgFnSc2d1
RNA	RNA	kA
jako	jako	k8xS,k8xC
prostředku	prostředek	k1gInSc2
využitelného	využitelný	k2eAgInSc2d1
k	k	k7c3
editaci	editace	k1gFnSc3
genů	gen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas	Cas	k1gMnSc1
<g/>
9	#num#	k4
systému	systém	k1gInSc2
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
tisíciletí	tisíciletí	k1gNnSc2
<g/>
,	,	kIx,
vědci	vědec	k1gMnPc1
vyvinuli	vyvinout	k5eAaPmAgMnP
tzv.	tzv.	kA
Zinc-finger	Zinc-finger	k1gInSc4
nukleázy	nukleáza	k1gFnSc2
(	(	kIx(
<g/>
nespecifické	specifický	k2eNgFnSc2d1
nukleázy	nukleáza	k1gFnSc2
spojené	spojený	k2eAgFnPc4d1
se	s	k7c7
zinkovými	zinkový	k2eAgInPc7d1
prsty	prst	k1gInPc7
<g/>
,	,	kIx,
syntetickými	syntetický	k2eAgInPc7d1
proteiny	protein	k1gInPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
DNA-vazebné	DNA-vazebný	k2eAgFnPc1d1
domény	doména	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
rozeznávat	rozeznávat	k5eAaImF
specifické	specifický	k2eAgFnPc1d1
sekvence	sekvence	k1gFnPc1
v	v	k7c6
DNA	DNA	kA
<g/>
)	)	kIx)
umožňující	umožňující	k2eAgFnSc1d1
vytvářet	vytvářet	k5eAaImF
dvojřetězcové	dvojřetězcový	k2eAgInPc4d1
zlomy	zlom	k1gInPc4
v	v	k7c6
DNA	DNA	kA
na	na	k7c6
předem	předem	k6eAd1
vybraných	vybraný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
objevem	objev	k1gInSc7
syntetických	syntetický	k2eAgFnPc2d1
nukleáz	nukleáza	k1gFnPc2
zvaných	zvaný	k2eAgFnPc2d1
TALENy	TALENa	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
značnému	značný	k2eAgNnSc3d1
zjednodušení	zjednodušení	k1gNnSc3
a	a	k8xC
zpřesnění	zpřesnění	k1gNnSc3
zacilování	zacilování	k1gNnSc2
na	na	k7c4
konkrétní	konkrétní	k2eAgNnPc4d1
místa	místo	k1gNnPc4
v	v	k7c6
sekvenci	sekvence	k1gFnSc6
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zinc-finger	Zinc-finger	k1gInSc1
nukleázy	nukleáza	k1gFnSc2
i	i	k8xC
TALENy	TALENy	k1gInPc1
vyžadují	vyžadovat	k5eAaImIp3nP
navržení	navržení	k1gNnSc4
a	a	k8xC
syntézu	syntéza	k1gFnSc4
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
dokáží	dokázat	k5eAaPmIp3nP
rozeznat	rozeznat	k5eAaPmF
cílovou	cílový	k2eAgFnSc4d1
sekvenci	sekvence	k1gFnSc4
na	na	k7c6
bázi	báze	k1gFnSc6
interakce	interakce	k1gFnSc2
protein-DNA	protein-DNA	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
finančně	finančně	k6eAd1
i	i	k9
časově	časově	k6eAd1
náročnější	náročný	k2eAgInSc1d2
proces	proces	k1gInSc1
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
při	při	k7c6
využití	využití	k1gNnSc6
komplementárního	komplementární	k2eAgNnSc2d1
párování	párování	k1gNnSc2
RNA-DNA	RNA-DNA	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CRISPR	CRISPR	kA
vyžaduje	vyžadovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
tvorbu	tvorba	k1gFnSc4
krátkých	krátký	k2eAgFnPc2d1
sekvencí	sekvence	k1gFnPc2
RNA	RNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Sawyer	Sawyer	k1gInSc1
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
(	(	kIx(
<g/>
9	#num#	k4
February	Februara	k1gFnSc2
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Editing	Editing	k1gInSc1
Genomes	Genomes	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
Bacterial	Bacterial	k1gMnSc1
Immune	Immun	k1gInSc5
System	Systo	k1gNnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scitable	Scitable	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
6	#num#	k4
April	Aprila	k1gFnPc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marraffini	Marraffin	k1gMnPc1
LA	la	k1gNnSc2
<g/>
,	,	kIx,
Sontheimer	Sontheimer	k1gMnSc1
EJ	ej	k0
(	(	kIx(
<g/>
March	March	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
interference	interference	k1gFnSc1
<g/>
:	:	kIx,
RNA-directed	RNA-directed	k1gInSc1
adaptive	adaptiv	k1gInSc5
immunity	immunit	k1gInPc1
in	in	k?
bacteria	bacterium	k1gNnSc2
and	and	k?
archaea	archaea	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Reviews	Reviews	k1gInSc4
Genetics	Genetics	k1gInSc1
11	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
181	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nrg	nrg	k?
<g/>
2749	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
2928866	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20125085	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Barrangou	Barranga	k1gFnSc7
R	R	kA
<g/>
,	,	kIx,
Fremaux	Fremaux	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
Deveau	Deveaum	k1gNnSc6
H	H	kA
<g/>
,	,	kIx,
Richards	Richards	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
Boyaval	Boyaval	k1gFnSc1
P	P	kA
<g/>
,	,	kIx,
Moineau	Moineaum	k1gNnSc6
S	s	k7c7
<g/>
,	,	kIx,
Romero	Romero	k1gNnSc1
DA	DA	kA
<g/>
,	,	kIx,
Horvath	Horvath	k1gMnSc1
P	P	kA
(	(	kIx(
<g/>
March	March	k1gInSc1
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
provides	provides	k1gMnSc1
acquired	acquired	k1gMnSc1
resistance	resistance	k1gFnSc2
against	against	k1gMnSc1
viruses	viruses	k1gMnSc1
in	in	k?
prokaryotes	prokaryotes	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
315	#num#	k4
(	(	kIx(
<g/>
5819	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1709	#num#	k4
<g/>
–	–	k?
<g/>
1712	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2007	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.315	.315	k4
<g/>
.1709	.1709	k4
<g/>
B.	B.	kA
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1138140	.1138140	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
17379808	#num#	k4
<g/>
↑	↑	k?
Marraffini	Marraffin	k1gMnPc1
LA	la	k1gNnSc2
<g/>
,	,	kIx,
Sontheimer	Sontheimer	k1gMnSc1
EJ	ej	k0
(	(	kIx(
<g/>
December	December	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
interference	interference	k1gFnSc1
limits	limits	k1gInSc1
horizontal	horizontal	k1gMnSc1
gene	gen	k1gInSc5
transfer	transfer	k1gInSc1
in	in	k?
staphylococci	staphylococce	k1gFnSc4
by	by	k9
targeting	targeting	k1gInSc4
DNA	DNA	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
322	#num#	k4
(	(	kIx(
<g/>
5909	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1843	#num#	k4
<g/>
–	–	k?
<g/>
1845	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2008	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.322	.322	k4
<g/>
.1843	.1843	k4
<g/>
M.	M.	kA
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1165771	.1165771	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
2695655	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19095942	#num#	k4
<g/>
↑	↑	k?
Marraffini	Marraffin	k1gMnPc1
LA	la	k1gNnSc2
<g/>
,	,	kIx,
Sontheimer	Sontheimer	k1gMnSc1
EJ	ej	k0
(	(	kIx(
<g/>
March	March	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
interference	interference	k1gFnSc1
<g/>
:	:	kIx,
RNA-directed	RNA-directed	k1gInSc1
adaptive	adaptiv	k1gInSc5
immunity	immunit	k1gInPc1
in	in	k?
bacteria	bacterium	k1gNnSc2
and	and	k?
archaea	archaea	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Reviews	Reviews	k1gInSc4
Genetics	Genetics	k1gInSc1
11	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
181	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nrg	nrg	k?
<g/>
2749	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
2928866	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20125085	#num#	k4
<g/>
↑	↑	k?
Grissa	Grissa	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
Vergnaud	Vergnaud	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
Pourcel	Pourcel	k1gMnSc1
C	C	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
CRISPRdb	CRISPRdb	k1gMnSc1
database	database	k6eAd1
and	and	k?
tools	tools	k6eAd1
to	ten	k3xDgNnSc4
display	displaa	k1gFnPc1
CRISPRs	CRISPRsa	k1gFnPc2
and	and	k?
to	ten	k3xDgNnSc1
generate	generat	k1gInSc5
dictionaries	dictionaries	k1gInSc4
of	of	k?
spacers	spacers	k1gInSc1
and	and	k?
repeats	repeats	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMC	BMC	kA
Bioinformatics	Bioinformatics	k1gInSc4
8	#num#	k4
<g/>
:	:	kIx,
172	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.118	10.118	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1471	#num#	k4
<g/>
-	-	kIx~
<g/>
2105	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
172	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
1892036	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
17521438	#num#	k4
<g/>
↑	↑	k?
Ledford	Ledforda	k1gFnPc2
<g/>
,	,	kIx,
Heidi	Heid	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
June	jun	k1gMnSc5
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
<g/>
,	,	kIx,
the	the	k?
disruptor	disruptor	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Feature	Featur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
522	#num#	k4
(	(	kIx(
<g/>
7554	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Snyder	Snyder	k1gMnSc1
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
August	August	k1gMnSc1
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
New	New	k1gMnSc1
technique	techniqu	k1gFnSc2
accelerates	accelerates	k1gMnSc1
genome	genom	k1gInSc5
editing	editing	k1gInSc4
process	process	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
research	research	k1gInSc1
news	news	k1gInSc1
@	@	kIx~
Vanderbilt	Vanderbilt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nashville	Nashville	k1gFnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
:	:	kIx,
Vanderbilt	Vanderbilt	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hendel	Hendlo	k1gNnPc2
A	A	kA
<g/>
,	,	kIx,
Bak	bak	k0
RO	RO	kA
<g/>
,	,	kIx,
Clark	Clark	k1gInSc1
JT	JT	kA
<g/>
,	,	kIx,
Kennedy	Kenneda	k1gMnSc2
AB	AB	kA
<g/>
,	,	kIx,
Ryan	Ryan	k1gMnSc1
DE	DE	k?
<g/>
,	,	kIx,
Roy	Roy	k1gMnSc1
S	s	k7c7
<g/>
,	,	kIx,
Steinfeld	Steinfeld	k1gInSc4
I	I	kA
<g/>
,	,	kIx,
Lunstad	Lunstad	k1gInSc1
BD	BD	kA
<g/>
,	,	kIx,
Kaiser	Kaiser	k1gMnSc1
RJ	RJ	kA
<g/>
,	,	kIx,
Wilkens	Wilkens	k1gInSc1
AB	AB	kA
<g/>
,	,	kIx,
Bacchetta	Bacchetta	k1gFnSc1
R	R	kA
<g/>
,	,	kIx,
Tsalenko	Tsalenka	k1gFnSc5
A	a	k9
<g/>
,	,	kIx,
Dellinger	Dellinger	k1gMnSc1
D	D	kA
<g/>
,	,	kIx,
Bruhn	Bruhn	k1gNnSc1
L	L	kA
<g/>
,	,	kIx,
Porteus	Porteus	k1gMnSc1
MH	MH	kA
(	(	kIx(
<g/>
June	jun	k1gMnSc5
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Chemically	Chemicalla	k1gMnSc2
modified	modified	k1gInSc1
guide	guide	k6eAd1
RNAs	RNAs	k1gInSc1
enhance	enhance	k1gFnSc2
CRISPR-Cas	CRISPR-Casa	k1gFnPc2
genome	genom	k1gInSc5
editing	editing	k1gInSc4
in	in	k?
human	human	k1gInSc1
primary	primara	k1gFnSc2
cells	cellsa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Biotechnology	biotechnolog	k1gMnPc4
33	#num#	k4
<g/>
:	:	kIx,
985	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nbt	nbt	k?
<g/>
.3290	.3290	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26121415	#num#	k4
<g/>
↑	↑	k?
Mali	Mali	k1gNnSc1
P	P	kA
<g/>
,	,	kIx,
Esvelt	Esvelt	k2eAgMnSc1d1
KM	km	kA
<g/>
,	,	kIx,
Church	Church	k1gMnSc1
GM	GM	kA
(	(	kIx(
<g/>
October	October	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Cas	Cas	k1gFnSc1
<g/>
9	#num#	k4
as	as	k1gNnPc2
a	a	k8xC
versatile	versatile	k6eAd1
tool	toonout	k5eAaPmAgMnS
for	forum	k1gNnPc2
engineering	engineering	k1gInSc1
biology	biolog	k1gMnPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Methods	Methods	k1gInSc1
10	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
957	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nmeth	nmeth	k1gInSc1
<g/>
.2649	.2649	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
4051438	#num#	k4
<g/>
↑	↑	k?
Ledford	Ledforda	k1gFnPc2
<g/>
,	,	kIx,
Heidi	Heid	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
June	jun	k1gMnSc5
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
<g/>
,	,	kIx,
the	the	k?
disruptor	disruptor	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
522	#num#	k4
(	(	kIx(
<g/>
7554	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
20	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
522020	#num#	k4
<g/>
a.	a.	k?
PMID	PMID	kA
26040877	#num#	k4
<g/>
↑	↑	k?
Overballe-Petersen	Overballe-Petersna	k1gFnPc2
S	s	k7c7
<g/>
,	,	kIx,
Harms	Harms	k1gInSc1
K	k	k7c3
<g/>
,	,	kIx,
Orlando	Orlanda	k1gFnSc5
LA	la	k1gNnSc1
<g/>
,	,	kIx,
Mayar	Mayar	k1gInSc1
JV	JV	kA
<g/>
,	,	kIx,
Rasmussen	Rasmussen	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
S	s	k7c7
<g/>
,	,	kIx,
Dahl	dahl	k1gInSc4
TW	TW	kA
<g/>
,	,	kIx,
Rosing	Rosing	k1gInSc1
MT	MT	kA
<g/>
,	,	kIx,
Poole	Poole	k1gNnSc1
AM	AM	kA
<g/>
,	,	kIx,
Sicheritz-Ponten	Sicheritz-Ponten	k2eAgMnSc1d1
T	T	kA
<g/>
,	,	kIx,
Brunak	Brunak	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
Inselmann	Inselmanno	k1gNnPc2
S	s	k7c7
<g/>
,	,	kIx,
de	de	k?
Vries	Vries	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Wackernagel	Wackernagel	k1gInSc1
W	W	kA
<g/>
,	,	kIx,
Pybus	Pybus	k1gInSc1
OG	OG	kA
<g/>
,	,	kIx,
Nielsen	Nielsen	k2eAgMnSc1d1
R	R	kA
<g/>
,	,	kIx,
Johnsen	Johnsen	k2eAgMnSc1d1
PJ	PJ	kA
<g/>
,	,	kIx,
Nielsen	Nielsen	k2eAgMnSc1d1
KM	km	kA
<g/>
,	,	kIx,
Willerslev	Willerslev	k1gMnSc1
E	E	kA
(	(	kIx(
<g/>
December	December	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Bacterial	Bacterial	k1gInSc1
natural	natural	k?
transformation	transformation	k1gInSc4
by	by	kYmCp3nP
highly	highnout	k5eAaPmAgFnP
fragmented	fragmented	k1gInSc4
and	and	k?
damaged	damaged	k1gInSc1
DNA	DNA	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gMnSc1
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
110	#num#	k4
(	(	kIx(
<g/>
49	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
19860	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2013	#num#	k4
<g/>
PNAS	PNAS	kA
<g/>
.	.	kIx.
<g/>
.11019860	.11019860	k4
<g/>
O.	O.	kA
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
pnas	pnas	k6eAd1
<g/>
.1315278110	.1315278110	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24248361	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lay	Lay	k1gFnSc1
summary	summara	k1gFnSc2
–	–	k?
Kurzweil	Kurzweil	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
read	read	k1gInSc1
<g/>
↑	↑	k?
Ishino	Ishino	k1gNnSc1
Y	Y	kA
<g/>
,	,	kIx,
Shinagawa	Shinagawa	k1gFnSc1
H	H	kA
<g/>
,	,	kIx,
Makino	Makino	k1gNnSc1
K	K	kA
<g/>
,	,	kIx,
Amemura	Amemura	k1gFnSc1
M	M	kA
<g/>
,	,	kIx,
Nakata	Nakat	k2eAgFnSc1d1
A	A	kA
(	(	kIx(
<g/>
Dec	Dec	k1gFnSc1
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Nucleotide	Nucleotid	k1gMnSc5
sequence	sequence	k1gFnSc2
of	of	k?
the	the	k?
iap	iap	k?
gene	gen	k1gInSc5
<g/>
,	,	kIx,
responsible	responsible	k6eAd1
for	forum	k1gNnPc2
alkaline	alkalin	k1gInSc5
phosphatase	phosphatas	k1gInSc6
isozyme	isozymat	k5eAaPmIp3nS
conversion	conversion	k1gInSc1
in	in	k?
Escherichia	Escherichia	k1gFnSc1
coli	col	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
identification	identification	k1gInSc1
of	of	k?
the	the	k?
gene	gen	k1gInSc5
product	productum	k1gNnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Bacteriology	Bacteriolog	k1gMnPc4
169	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
5429	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
213968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
3316184	#num#	k4
<g/>
↑	↑	k?
Lander	Landra	k1gFnPc2
ES	ES	kA
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Heroes	Heroes	k1gMnSc1
of	of	k?
CRISPR	CRISPR	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
164	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
18	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2015	.2015	k4
<g/>
.12	.12	k4
<g/>
.041	.041	k4
<g/>
↑	↑	k?
Mojica	Mojic	k1gInSc2
FJ	FJ	kA
<g/>
,	,	kIx,
Díez-Villaseñ	Díez-Villaseñ	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
Soria	Soria	k1gFnSc1
E	E	kA
<g/>
,	,	kIx,
Juez	Juez	k1gMnSc1
G	G	kA
(	(	kIx(
<g/>
Apr	Apr	k1gFnSc1
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Biological	Biological	k1gFnSc1
significance	significance	k1gFnSc2
of	of	k?
a	a	k8xC
family	famila	k1gFnSc2
of	of	k?
regularly	regularla	k1gFnSc2
spaced	spaced	k1gMnSc1
repeats	repeatsa	k1gFnPc2
in	in	k?
the	the	k?
genomes	genomes	k1gInSc1
of	of	k?
Archaea	Archae	k1gInSc2
<g/>
,	,	kIx,
Bacteria	Bacterium	k1gNnSc2
and	and	k?
mitochondria	mitochondrium	k1gNnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Microbiology	Microbiolog	k1gMnPc4
36	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
244	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.104	10.104	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2958.2000	2958.2000	k4
<g/>
.01838	.01838	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
PMID	PMID	kA
10760181	#num#	k4
<g/>
↑	↑	k?
Mojica	Mojic	k1gInSc2
FJ	FJ	kA
<g/>
,	,	kIx,
Díez-Villaseñ	Díez-Villaseñ	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
Soria	Soria	k1gFnSc1
E	E	kA
<g/>
,	,	kIx,
Juez	Juez	k1gMnSc1
G	G	kA
(	(	kIx(
<g/>
Apr	Apr	k1gFnSc1
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Biological	Biological	k1gFnSc1
significance	significance	k1gFnSc2
of	of	k?
a	a	k8xC
family	famila	k1gFnSc2
of	of	k?
regularly	regularla	k1gFnSc2
spaced	spaced	k1gMnSc1
repeats	repeatsa	k1gFnPc2
in	in	k?
the	the	k?
genomes	genomes	k1gInSc1
of	of	k?
Archaea	Archae	k1gInSc2
<g/>
,	,	kIx,
Bacteria	Bacterium	k1gNnSc2
and	and	k?
mitochondria	mitochondrium	k1gNnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Microbiology	Microbiolog	k1gMnPc4
36	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
244	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.104	10.104	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2958.2000	2958.2000	k4
<g/>
.01838	.01838	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
PMID	PMID	kA
10760181	#num#	k4
<g/>
↑	↑	k?
Lander	Landra	k1gFnPc2
ES	ES	kA
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Heroes	Heroes	k1gMnSc1
of	of	k?
CRISPR	CRISPR	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
164	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
18	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2015	.2015	k4
<g/>
.12	.12	k4
<g/>
.041	.041	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26771483	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jansen	Jansen	k1gInSc1
R	R	kA
<g/>
,	,	kIx,
Embden	Embdna	k1gFnPc2
JD	JD	kA
<g/>
,	,	kIx,
Gaastra	Gaastra	k1gFnSc1
W	W	kA
<g/>
,	,	kIx,
Schouls	Schouls	k1gInSc1
LM	LM	kA
(	(	kIx(
<g/>
Mar	Mar	k1gFnSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Identification	Identification	k1gInSc1
of	of	k?
genes	genes	k1gMnSc1
that	that	k1gMnSc1
are	ar	k1gInSc5
associated	associated	k1gInSc4
with	with	k1gInSc1
DNA	DNA	kA
repeats	repeats	k1gInSc1
in	in	k?
prokaryotes	prokaryotes	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Microbiology	Microbiolog	k1gMnPc4
43	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1565	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.104	10.104	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2958.2002	2958.2002	k4
<g/>
.02839	.02839	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
PMID	PMID	kA
11952905	#num#	k4
<g/>
↑	↑	k?
Mojica	Mojic	k1gInSc2
FJ	FJ	kA
<g/>
,	,	kIx,
Díez-Villaseñ	Díez-Villaseñ	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
García-Martínez	García-Martínez	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Soria	Soria	k1gFnSc1
E	E	kA
(	(	kIx(
<g/>
Feb	Feb	k1gFnSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Intervening	Intervening	k1gInSc1
sequences	sequences	k1gInSc1
of	of	k?
regularly	regularla	k1gFnSc2
spaced	spaced	k1gMnSc1
prokaryotic	prokaryotice	k1gFnPc2
repeats	repeats	k1gInSc4
derive	derivat	k5eAaPmIp3nS
from	from	k6eAd1
foreign	foreign	k1gNnSc1
genetic	genetice	k1gFnPc2
elements	elementsa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Molecular	Molecular	k1gInSc1
Evolution	Evolution	k1gInSc1
60	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
174	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15791728	#num#	k4
<g/>
↑	↑	k?
Lander	Landra	k1gFnPc2
ES	ES	kA
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Heroes	Heroes	k1gMnSc1
of	of	k?
CRISPR	CRISPR	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
164	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
18	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2015	.2015	k4
<g/>
.12	.12	k4
<g/>
.041	.041	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26771483	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lander	Lander	k1gInSc1
ES	ES	kA
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Heroes	Heroes	k1gMnSc1
of	of	k?
CRISPR	CRISPR	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
164	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
18	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2015	.2015	k4
<g/>
.12	.12	k4
<g/>
.041	.041	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26771483	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pourcel	Pourcel	k1gFnSc2
C	C	kA
<g/>
,	,	kIx,
Salvignol	Salvignol	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
Vergnaud	Vergnaud	k1gMnSc1
G	G	kA
(	(	kIx(
<g/>
Mar	Mar	k1gFnSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
elements	elements	k6eAd1
in	in	k?
Yersinia	Yersinium	k1gNnPc1
pestis	pestis	k1gInSc1
acquire	acquir	k1gInSc5
new	new	k?
repeats	repeats	k1gInSc1
by	by	kYmCp3nS
preferential	preferentiat	k5eAaImAgInS,k5eAaPmAgInS,k5eAaBmAgInS
uptake	uptake	k1gInSc1
of	of	k?
bacteriophage	bacteriophag	k1gFnSc2
DNA	DNA	kA
<g/>
,	,	kIx,
and	and	k?
provide	provid	k1gMnSc5
additional	additionat	k5eAaImAgInS,k5eAaPmAgInS
tools	tools	k1gInSc1
for	forum	k1gNnPc2
evolutionary	evolutionara	k1gFnSc2
studies	studies	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microbiology	Microbiolog	k1gMnPc4
151	#num#	k4
(	(	kIx(
<g/>
Pt	Pt	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
653	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
mic	mic	k?
<g/>
.0	.0	k4
<g/>
.27437	.27437	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15758212	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bolotin	Bolotina	k1gFnPc2
A	A	kA
<g/>
,	,	kIx,
Quinquis	Quinquis	k1gFnSc1
B	B	kA
<g/>
,	,	kIx,
Sorokin	Sorokin	k2eAgMnSc1d1
A	A	kA
<g/>
,	,	kIx,
Ehrlich	Ehrlich	k1gMnSc1
SD	SD	kA
(	(	kIx(
<g/>
Aug	Aug	k1gFnSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Clustered	Clustered	k1gMnSc1
regularly	regularla	k1gFnSc2
interspaced	interspaced	k1gMnSc1
short	shorta	k1gFnPc2
palindrome	palindrom	k1gInSc5
repeats	repeats	k1gInSc1
(	(	kIx(
<g/>
CRISPRs	CRISPRs	k1gInSc1
<g/>
)	)	kIx)
have	havat	k5eAaPmIp3nS
spacers	spacers	k6eAd1
of	of	k?
extrachromosomal	extrachromosomal	k1gMnSc1
origin	origin	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microbiology	Microbiolog	k1gMnPc4
151	#num#	k4
(	(	kIx(
<g/>
Pt	Pt	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2551	#num#	k4
<g/>
–	–	k?
<g/>
61	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
mic	mic	k?
<g/>
.0	.0	k4
<g/>
.28048	.28048	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16079334	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jansen	Jansen	k1gInSc1
R	R	kA
<g/>
,	,	kIx,
Embden	Embdna	k1gFnPc2
JD	JD	kA
<g/>
,	,	kIx,
Gaastra	Gaastra	k1gFnSc1
W	W	kA
<g/>
,	,	kIx,
Schouls	Schouls	k1gInSc1
LM	LM	kA
(	(	kIx(
<g/>
Mar	Mar	k1gFnSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Identification	Identification	k1gInSc1
of	of	k?
genes	genes	k1gMnSc1
that	that	k1gMnSc1
are	ar	k1gInSc5
associated	associated	k1gInSc4
with	with	k1gInSc1
DNA	DNA	kA
repeats	repeats	k1gInSc1
in	in	k?
prokaryotes	prokaryotes	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Microbiology	Microbiolog	k1gMnPc4
43	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1565	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.104	10.104	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2958.2002	2958.2002	k4
<g/>
.02839	.02839	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
PMID	PMID	kA
11952905	#num#	k4
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	free	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
to	ten	k3xDgNnSc1
read	read	k6eAd1
<g/>
↑	↑	k?
Pourcel	Pourcel	k1gFnPc4
C	C	kA
<g/>
,	,	kIx,
Salvignol	Salvignol	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
Vergnaud	Vergnaud	k1gMnSc1
G	G	kA
(	(	kIx(
<g/>
March	March	k1gInSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
elements	elements	k6eAd1
in	in	k?
Yersinia	Yersinium	k1gNnPc1
pestis	pestis	k1gInSc1
acquire	acquir	k1gInSc5
new	new	k?
repeats	repeats	k1gInSc1
by	by	kYmCp3nS
preferential	preferentiat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
uptake	uptake	k1gInSc1
of	of	k?
bacteriophage	bacteriophag	k1gFnSc2
DNA	DNA	kA
<g/>
,	,	kIx,
and	and	k?
provide	provid	k1gMnSc5
additional	additionat	k5eAaPmAgInS,k5eAaImAgInS
tools	tools	k1gInSc1
for	forum	k1gNnPc2
evolutionary	evolutionara	k1gFnSc2
studies	studies	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microbiology	Microbiolog	k1gMnPc4
151	#num#	k4
(	(	kIx(
<g/>
Pt	Pt	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
653	#num#	k4
<g/>
–	–	k?
<g/>
663	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
mic	mic	k?
<g/>
.0	.0	k4
<g/>
.27437	.27437	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15758212	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mojica	Mojic	k1gInSc2
FJ	FJ	kA
<g/>
,	,	kIx,
Díez-Villaseñ	Díez-Villaseñ	k1gInSc1
C	C	kA
<g/>
,	,	kIx,
García-Martínez	García-Martínez	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Soria	Soria	k1gFnSc1
E	E	kA
(	(	kIx(
<g/>
February	Februara	k1gFnSc2
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Intervening	Intervening	k1gInSc1
sequences	sequences	k1gInSc1
of	of	k?
regularly	regularla	k1gFnSc2
spaced	spaced	k1gMnSc1
prokaryotic	prokaryotice	k1gFnPc2
repeats	repeats	k1gInSc4
derive	derivat	k5eAaPmIp3nS
from	from	k6eAd1
foreign	foreign	k1gNnSc1
genetic	genetice	k1gFnPc2
elements	elementsa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Molecular	Molecular	k1gInSc1
Evolution	Evolution	k1gInSc1
60	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
174	#num#	k4
<g/>
–	–	k?
<g/>
182	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15791728	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bolotin	Bolotina	k1gFnPc2
A	A	kA
<g/>
,	,	kIx,
Quinquis	Quinquis	k1gFnSc1
B	B	kA
<g/>
,	,	kIx,
Sorokin	Sorokin	k2eAgMnSc1d1
A	A	kA
<g/>
,	,	kIx,
Ehrlich	Ehrlich	k1gMnSc1
SD	SD	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Clustered	Clustered	k1gMnSc1
regularly	regularla	k1gFnSc2
interspaced	interspaced	k1gMnSc1
short	shorta	k1gFnPc2
palindrome	palindrom	k1gInSc5
repeats	repeats	k1gInSc1
(	(	kIx(
<g/>
CRISPRs	CRISPRs	k1gInSc1
<g/>
)	)	kIx)
have	havat	k5eAaPmIp3nS
spacers	spacers	k6eAd1
of	of	k?
extrachromosomal	extrachromosomal	k1gMnSc1
origin	origin	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microbiology	Microbiolog	k1gMnPc4
151	#num#	k4
(	(	kIx(
<g/>
Pt	Pt	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2551	#num#	k4
<g/>
–	–	k?
<g/>
2561	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
mic	mic	k?
<g/>
.0	.0	k4
<g/>
.28048	.28048	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16079334	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Horvath	Horvath	k1gInSc1
P	P	kA
<g/>
,	,	kIx,
Barrangou	Barranga	k1gFnSc7
R	R	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas	Cas	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
immune	immun	k1gMnSc5
system	syst	k1gInSc7
of	of	k?
bacteria	bacterium	k1gNnSc2
and	and	k?
archaea	archaea	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
327	#num#	k4
(	(	kIx(
<g/>
5962	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
167	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2010	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.327	.327	k4
<g/>
.	.	kIx.
<g/>
.167	.167	k4
<g/>
H.	H.	kA
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1179555	.1179555	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
20056882	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Morange	Morange	k1gInSc1
<g/>
,	,	kIx,
M	M	kA
(	(	kIx(
<g/>
June	jun	k1gMnSc5
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
What	What	k1gMnSc1
history	histor	k1gInPc4
tells	tells	k6eAd1
us	us	k?
XXXVII	XXXVII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
CRISPR-Cas	CRISPR-Cas	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
discovery	discovera	k1gFnSc2
of	of	k?
an	an	k?
immune	immun	k1gInSc5
system	syst	k1gInSc7
in	in	k?
prokaryotes	prokaryotes	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Biosciences	Biosciences	k1gMnSc1
2	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
221	#num#	k4
<g/>
–	–	k?
<g/>
223	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
12038	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
9532	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
25963251	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pennisi	Pennise	k1gFnSc4
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnPc1
CRISPR	CRISPR	kA
craze	craze	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Focus	Focus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
341	#num#	k4
(	(	kIx(
<g/>
6148	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
833	#num#	k4
<g/>
–	–	k?
<g/>
836	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.341	.341	k4
<g/>
.6148	.6148	k4
<g/>
.833	.833	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23970676	#num#	k4
<g/>
↑	↑	k?
Pennisi	Pennise	k1gFnSc4
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnPc1
CRISPR	CRISPR	kA
craze	craze	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Focus	Focus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
341	#num#	k4
(	(	kIx(
<g/>
6148	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
833	#num#	k4
<g/>
–	–	k?
<g/>
836	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.341	.341	k4
<g/>
.6148	.6148	k4
<g/>
.833	.833	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23970676	#num#	k4
<g/>
↑	↑	k?
Jinek	Jinky	k1gFnPc2
M	M	kA
<g/>
,	,	kIx,
Chylinski	Chylinski	k1gNnSc1
K	K	kA
<g/>
,	,	kIx,
Fonfara	Fonfara	k1gFnSc1
I	i	k9
<g/>
,	,	kIx,
Hauer	Hauer	k1gMnSc1
M	M	kA
<g/>
,	,	kIx,
Doudna	Doudna	k1gFnSc1
JA	JA	k?
<g/>
,	,	kIx,
Charpentier	Charpentier	k1gMnSc1
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
programmable	programmable	k6eAd1
dual-RNA-guided	dual-RNA-guided	k1gMnSc1
DNA	DNA	kA
endonuclease	endonuclease	k6eAd1
in	in	k?
adaptive	adaptiv	k1gInSc5
bacterial	bacterial	k1gInSc4
immunity	immunit	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
337	#num#	k4
(	(	kIx(
<g/>
6096	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
816	#num#	k4
<g/>
–	–	k?
<g/>
821	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2012	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.337	.337	k4
<g/>
.	.	kIx.
<g/>
.816	.816	k4
<g/>
Jdoi	Jdoi	k1gNnPc2
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1225829	.1225829	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
22745249	#num#	k4
<g/>
..	..	k?
<g/>
↑	↑	k?
Connor	Connor	k1gInSc1
S	s	k7c7
(	(	kIx(
<g/>
7	#num#	k4
November	November	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
gene	gen	k1gInSc5
therapy	therapa	k1gFnPc1
<g/>
:	:	kIx,
Scientists	Scientists	k1gInSc1
call	calla	k1gFnPc2
for	forum	k1gNnPc2
more	mor	k1gInSc5
public	publicum	k1gNnPc2
debate	debat	k1gInSc5
around	arounda	k1gFnPc2
breakthrough	breakthrough	k1gInSc1
technique	techniqu	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Scienec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DiCarlo	DiCarlo	k1gNnSc1
JE	být	k5eAaImIp3nS
<g/>
,	,	kIx,
Norville	Norville	k1gInSc1
JE	být	k5eAaImIp3nS
<g/>
,	,	kIx,
Mali	Mali	k1gNnSc1
P	P	kA
<g/>
,	,	kIx,
Rios	Rios	k1gInSc1
X	X	kA
<g/>
,	,	kIx,
Aach	Aach	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Church	Church	k1gMnSc1
GM	GM	kA
(	(	kIx(
<g/>
Apr	Apr	k1gFnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Genome	genom	k1gInSc5
engineering	engineering	k1gInSc4
in	in	k?
Saccharomyces	Saccharomyces	k1gInSc1
cerevisiae	cerevisiae	k1gNnSc1
using	using	k1gInSc1
CRISPR-Cas	CRISPR-Cas	k1gMnSc1
systems	systems	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleic	Nucleice	k1gInPc2
Acids	Acids	k1gInSc4
Research	Research	k1gInSc1
41	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
4336	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
nar	nar	kA
<g/>
/	/	kIx~
<g/>
gkt	gkt	k?
<g/>
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
3627607	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23460208	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hwang	Hwang	k1gInSc1
WY	WY	kA
<g/>
,	,	kIx,
Fu	fu	k0
Y	Y	kA
<g/>
,	,	kIx,
Reyon	Reyon	k1gNnSc1
D	D	kA
<g/>
,	,	kIx,
Maeder	Maeder	k1gInSc1
ML	ml	kA
<g/>
,	,	kIx,
Tsai	Tsai	k1gNnSc1
SQ	SQ	kA
<g/>
,	,	kIx,
Sander	Sandra	k1gFnPc2
JD	JD	kA
<g/>
,	,	kIx,
Peterson	Peterson	k1gNnSc1
RT	RT	kA
<g/>
,	,	kIx,
Yeh	Yeh	k1gFnSc1
JR	JR	kA
<g/>
,	,	kIx,
Joung	Joung	k1gMnSc1
JK	JK	kA
(	(	kIx(
<g/>
Mar	Mar	k1gFnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Efficient	Efficient	k1gInSc1
genome	genom	k1gInSc5
editing	editing	k1gInSc1
in	in	k?
zebrafish	zebrafish	k1gMnSc1
using	using	k1gMnSc1
a	a	k8xC
CRISPR-Cas	CRISPR-Cas	k1gMnSc1
system	syst	k1gMnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Biotechnology	biotechnolog	k1gMnPc4
31	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
227	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nbt	nbt	k?
<g/>
.2501	.2501	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gratz	Gratz	k1gInSc1
SJ	SJ	kA
<g/>
,	,	kIx,
Cummings	Cummings	k1gInSc1
AM	AM	kA
<g/>
,	,	kIx,
Nguyen	Nguyen	k2eAgMnSc1d1
JN	JN	kA
<g/>
,	,	kIx,
Hamm	Hamm	k1gInSc1
DC	DC	kA
<g/>
,	,	kIx,
Donohue	Donohue	k1gNnSc1
LK	LK	kA
<g/>
,	,	kIx,
Harrison	Harrison	k1gMnSc1
MM	mm	kA
<g/>
,	,	kIx,
Wildonger	Wildonger	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
Connor-Giles	Connor-Giles	k1gMnSc1
KM	km	kA
(	(	kIx(
<g/>
Aug	Aug	k1gFnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Genome	genom	k1gInSc5
engineering	engineering	k1gInSc1
of	of	k?
Drosophila	Drosophila	k1gFnSc1
with	with	k1gInSc1
the	the	k?
CRISPR	CRISPR	kA
RNA-guided	RNA-guided	k1gMnSc1
Cas	Cas	k1gMnSc1
<g/>
9	#num#	k4
nuclease	nuclease	k6eAd1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genetics	Genetics	k1gInSc1
194	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1029	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.153	10.153	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
genetics	genetics	k6eAd1
<g/>
.113	.113	k4
<g/>
.152710	.152710	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
3730909	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23709638	#num#	k4
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
read	read	k1gInSc1
<g/>
↑	↑	k?
Flowers	Flowers	k1gInSc1
GP	GP	kA
<g/>
,	,	kIx,
Timberlake	Timberlake	k1gNnSc1
AT	AT	kA
<g/>
,	,	kIx,
McLean	McLean	k1gInSc1
KC	KC	kA
<g/>
,	,	kIx,
Monaghan	Monaghan	k1gInSc1
JR	JR	kA
<g/>
,	,	kIx,
Crews	Crews	k1gInSc1
CM	cm	kA
(	(	kIx(
<g/>
May	May	k1gMnSc1
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Highly	Highly	k1gMnSc1
efficient	efficient	k1gMnSc1
targeted	targeted	k1gMnSc1
mutagenesis	mutagenesis	k1gFnSc2
in	in	k?
axolotl	axolotl	k1gMnSc1
using	using	k1gMnSc1
Cas	Cas	k1gMnSc1
<g/>
9	#num#	k4
RNA-guided	RNA-guided	k1gInSc1
nuclease	nuclease	k6eAd1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Development	Development	k1gInSc1
141	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2165	#num#	k4
<g/>
–	–	k?
<g/>
71	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.124	10.124	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
dev	dev	k?
<g/>
.105072	.105072	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
4011087	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24764077	#num#	k4
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
read	read	k1gInSc1
<g/>
↑	↑	k?
Friedland	Friedland	k1gInSc1
AE	AE	kA
<g/>
,	,	kIx,
Tzur	Tzur	k1gInSc1
YB	YB	kA
<g/>
,	,	kIx,
Esvelt	Esvelt	k2eAgMnSc1d1
KM	km	kA
<g/>
,	,	kIx,
Colaiácovo	Colaiácův	k2eAgNnSc4d1
MP	MP	kA
<g/>
,	,	kIx,
Church	Church	k1gMnSc1
GM	GM	kA
<g/>
,	,	kIx,
Calarco	Calarco	k6eAd1
JA	JA	k?
(	(	kIx(
<g/>
Aug	Aug	k1gFnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Heritable	Heritable	k1gFnSc1
genome	genom	k1gInSc5
editing	editing	k1gInSc4
in	in	k?
C.	C.	kA
elegans	elegans	k6eAd1
via	via	k7c4
a	a	k8xC
CRISPR-Cas	CRISPR-Cas	k1gMnSc1
<g/>
9	#num#	k4
system	systo	k1gNnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Methods	Methods	k1gInSc1
10	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
741	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nmeth	nmeth	k1gInSc1
<g/>
.2532	.2532	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
3822328	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23817069	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiang	Jiang	k1gInSc1
W	W	kA
<g/>
,	,	kIx,
Zhou	Zhoum	k1gNnSc6
H	H	kA
<g/>
,	,	kIx,
Bi	Bi	k1gFnSc1
H	H	kA
<g/>
,	,	kIx,
Fromm	Fromm	k1gMnSc1
M	M	kA
<g/>
,	,	kIx,
Yang	Yang	k1gInSc1
B	B	kA
<g/>
,	,	kIx,
Weeks	Weeks	k1gInSc1
DP	DP	kA
(	(	kIx(
<g/>
Nov	nov	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Demonstration	Demonstration	k1gInSc1
of	of	k?
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas	Cas	k1gFnSc1
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
sgRNA-mediated	sgRNA-mediated	k1gMnSc1
targeted	targeted	k1gMnSc1
gene	gen	k1gInSc5
modification	modification	k1gInSc4
in	in	k?
Arabidopsis	Arabidopsis	k1gInSc1
<g/>
,	,	kIx,
tobacco	tobacco	k1gNnSc1
<g/>
,	,	kIx,
sorghum	sorghum	k1gNnSc1
and	and	k?
rice	ric	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleic	Nucleice	k1gInPc2
Acids	Acids	k1gInSc4
Research	Research	k1gInSc1
41	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
e	e	k0
<g/>
188	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
nar	nar	kA
<g/>
/	/	kIx~
<g/>
gkt	gkt	k?
<g/>
780	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMC	PMC	kA
3814374	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23999092	#num#	k4
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
read	read	k1gInSc1
<g/>
↑	↑	k?
Wang	Wang	k1gInSc1
H	H	kA
<g/>
,	,	kIx,
Yang	Yang	k1gInSc1
H	H	kA
<g/>
,	,	kIx,
Shivalila	Shivalila	k1gFnSc1
CS	CS	kA
<g/>
,	,	kIx,
Dawlaty	Dawle	k1gNnPc7
MM	mm	kA
<g/>
,	,	kIx,
Cheng	Cheng	k1gInSc1
AW	AW	kA
<g/>
,	,	kIx,
Zhang	Zhang	k1gInSc1
F	F	kA
<g/>
,	,	kIx,
Jaenisch	Jaenisch	k1gMnSc1
R	R	kA
(	(	kIx(
<g/>
May	May	k1gMnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
One-step	One-step	k1gInSc1
generation	generation	k1gInSc1
of	of	k?
mice	micat	k5eAaPmIp3nS
carrying	carrying	k1gInSc1
mutations	mutations	k1gInSc1
in	in	k?
multiple	multipl	k1gInSc5
genes	genes	k1gMnSc1
by	by	kYmCp3nS
CRISPR	CRISPR	kA
<g/>
/	/	kIx~
<g/>
Cas-mediated	Cas-mediated	k1gMnSc1
genome	genom	k1gInSc5
engineering	engineering	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
153	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
910	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2013	.2013	k4
<g/>
.04	.04	k4
<g/>
.025	.025	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23643243	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Guo	Guo	k1gFnSc2
X	X	kA
<g/>
,	,	kIx,
Li	li	k8xS
XJ	XJ	kA
(	(	kIx(
<g/>
Jul	Jula	k1gFnPc2
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Targeted	Targeted	k1gInSc1
genome	genom	k1gInSc5
editing	editing	k1gInSc1
in	in	k?
primate	primat	k1gInSc5
embryos	embryosa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
Research	Researcha	k1gFnPc2
25	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
767	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
cr	cr	k0
<g/>
.2015	.2015	k4
<g/>
.64	.64	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
26032266	#num#	k4
<g/>
.	.	kIx.
<g/>
Closed	Closed	k1gInSc1
access	access	k1gInSc1
<g/>
↑	↑	k?
Baltimore	Baltimore	k1gInSc1
D	D	kA
<g/>
,	,	kIx,
Berg	Berg	k1gInSc1
P	P	kA
<g/>
,	,	kIx,
Botchan	Botchan	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
Carroll	Carroll	k1gInSc1
D	D	kA
<g/>
,	,	kIx,
Charo	Charo	k1gNnSc1
RA	ra	k0
<g/>
,	,	kIx,
Church	Church	k1gMnSc1
G	G	kA
<g/>
,	,	kIx,
Corn	Corn	k1gInSc1
JE	být	k5eAaImIp3nS
<g/>
,	,	kIx,
Daley	Dalea	k1gFnPc1
GQ	GQ	kA
<g/>
,	,	kIx,
Doudna	Doudna	k1gFnSc1
JA	JA	k?
<g/>
,	,	kIx,
Fenner	Fenner	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
Greely	Greel	k1gInPc7
HT	HT	kA
<g/>
,	,	kIx,
Jinek	Jinek	k6eAd1
M	M	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
GS	GS	kA
<g/>
,	,	kIx,
Penhoet	Penhoet	k1gInSc1
E	E	kA
<g/>
,	,	kIx,
Puck	Puck	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Sternberg	Sternberg	k1gInSc1
SH	SH	kA
<g/>
,	,	kIx,
Weissman	Weissman	k1gMnSc1
JS	JS	kA
<g/>
,	,	kIx,
Yamamoto	Yamamota	k1gFnSc5
KR	KR	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Biotechnology	biotechnolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
prudent	prudent	k1gMnSc1
path	path	k1gMnSc1
forward	forward	k1gMnSc1
for	forum	k1gNnPc2
genomic	genomice	k1gInPc2
engineering	engineering	k1gInSc4
and	and	k?
germline	germlin	k1gInSc5
gene	gen	k1gInSc5
modification	modification	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
348	#num#	k4
(	(	kIx(
<g/>
6230	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
36	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2015	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.348	.348	k4
<g/>
..	..	k?
<g/>
.36	.36	k4
<g/>
B.	B.	kA
<g/>
↑	↑	k?
Larson	Larson	k1gInSc1
MH	MH	kA
<g/>
,	,	kIx,
Gilbert	Gilbert	k1gMnSc1
LA	la	k1gNnSc2
<g/>
,	,	kIx,
Wang	Wang	k1gInSc1
X	X	kA
<g/>
,	,	kIx,
Lim	Lima	k1gFnPc2
WA	WA	kA
<g/>
,	,	kIx,
Weissman	Weissman	k1gMnSc1
JS	JS	kA
<g/>
,	,	kIx,
Qi	Qi	k1gMnSc1
LS	LS	kA
(	(	kIx(
<g/>
November	November	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
interference	interference	k1gFnSc1
(	(	kIx(
<g/>
CRISPRi	CRISPRi	k1gNnPc2
<g/>
)	)	kIx)
for	forum	k1gNnPc2
sequence-specific	sequence-specifice	k1gFnPc2
control	controla	k1gFnPc2
of	of	k?
gene	gen	k1gInSc5
expression	expression	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Protocols	Protocols	k1gInSc1
8	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2180	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nprot	nprot	k1gInSc1
<g/>
.2013	.2013	k4
<g/>
.132	.132	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24136345	#num#	k4
<g/>
.	.	kIx.
<g/>
open	open	k1gInSc1
access	access	k1gInSc1
publication	publication	k1gInSc1
-	-	kIx~
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
read	read	k6eAd1
<g/>
↑	↑	k?
Pennisi	Pennise	k1gFnSc4
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnPc1
CRISPR	CRISPR	kA
craze	craze	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Focus	Focus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
341	#num#	k4
(	(	kIx(
<g/>
6148	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
833	#num#	k4
<g/>
–	–	k?
<g/>
836	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.341	.341	k4
<g/>
.6148	.6148	k4
<g/>
.833	.833	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23970676	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://naschov.cz/evropsky-soudni-dvur-uprava-genu-je-genovou-modifikaci/	https://naschov.cz/evropsky-soudni-dvur-uprava-genu-je-genovou-modifikaci/	k?
-	-	kIx~
Evropský	evropský	k2eAgInSc1d1
soudní	soudní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
:	:	kIx,
úprava	úprava	k1gFnSc1
genů	gen	k1gInPc2
je	být	k5eAaImIp3nS
genovou	genový	k2eAgFnSc7d1
modifikací	modifikace	k1gFnSc7
<g/>
↑	↑	k?
Pollack	Pollacka	k1gFnPc2
A	A	kA
(	(	kIx(
<g/>
11	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Jennifer	Jennifer	k1gInSc1
Doudna	Doudno	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
Pioneer	Pioneer	kA
Who	Who	k1gMnSc1
Helped	Helped	k1gMnSc1
Simplify	Simplif	k1gInPc7
Genome	genom	k1gInSc5
Editing	Editing	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
12	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pennisi	Pennise	k1gFnSc4
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnPc1
CRISPR	CRISPR	kA
craze	craze	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Focus	Focus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
341	#num#	k4
(	(	kIx(
<g/>
6148	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
833	#num#	k4
<g/>
–	–	k?
<g/>
836	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.341	.341	k4
<g/>
.6148	.6148	k4
<g/>
.833	.833	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23970676	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jinek	Jinek	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
Chylinski	Chylinski	k1gNnSc1
K	K	kA
<g/>
,	,	kIx,
Fonfara	Fonfara	k1gFnSc1
I	i	k9
<g/>
,	,	kIx,
Hauer	Hauer	k1gMnSc1
M	M	kA
<g/>
,	,	kIx,
Doudna	Doudna	k1gFnSc1
JA	JA	k?
<g/>
,	,	kIx,
Charpentier	Charpentier	k1gMnSc1
E	E	kA
(	(	kIx(
<g/>
August	August	k1gMnSc1
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
programmable	programmable	k6eAd1
dual-RNA-guided	dual-RNA-guided	k1gMnSc1
DNA	DNA	kA
endonuclease	endonuclease	k6eAd1
in	in	k?
adaptive	adaptiv	k1gInSc5
bacterial	bacterial	k1gInSc4
immunity	immunit	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
337	#num#	k4
(	(	kIx(
<g/>
6096	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
816	#num#	k4
<g/>
–	–	k?
<g/>
821	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2012	#num#	k4
<g/>
Sci	Sci	k1gFnPc2
<g/>
..	..	k?
<g/>
.337	.337	k4
<g/>
.	.	kIx.
<g/>
.816	.816	k4
<g/>
J.	J.	kA
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1225829	.1225829	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
22745249	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Young	Young	k1gMnSc1
S	s	k7c7
(	(	kIx(
<g/>
11	#num#	k4
February	Februara	k1gFnSc2
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
CRISPR	CRISPR	kA
and	and	k?
Other	Other	k1gInSc1
Genome	genom	k1gInSc5
Editing	Editing	k1gInSc4
Tools	Toolsa	k1gFnPc2
Boost	Boost	k1gMnSc1
Medical	Medical	k1gMnSc1
Research	Research	k1gMnSc1
and	and	k?
Gene	gen	k1gInSc5
Therapy	Therap	k1gInPc7
<g/>
’	’	k?
<g/>
s	s	k7c7
Reach	Reacha	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
MIT	MIT	kA
Technology	technolog	k1gMnPc4
Review	Review	k1gFnPc2
(	(	kIx(
<g/>
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
Massachusetts	Massachusetts	k1gNnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
CRISPR	CRISPR	kA
editace	editace	k1gFnSc1
genomu	genom	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CRISPR	CRISPR	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
CRISPR	CRISPR	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
biologem	biolog	k1gMnSc7
Markem	Marek	k1gMnSc7
Orko	Orko	k1gMnSc1
Váchou	Vácha	k1gMnSc7
o	o	k7c6
možnostech	možnost	k1gFnPc6
využití	využití	k1gNnSc2
CRISPR	CRISPR	kA
<g/>
,	,	kIx,
Ekolist	Ekolist	k1gMnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
</s>
