<s>
CRISPR	CRISPR	kA
jsou	být	k5eAaImIp3nP
segmenty	segment	k1gInPc1
nahromaděných	nahromaděný	k2eAgFnPc2d1
pravidelně	pravidelně	k6eAd1
rozmístěných	rozmístěný	k2eAgFnPc2d1
krátkých	krátká	k1gFnPc2
palindromických	palindromický	k2eAgFnPc2d1
repetic	repetice	k1gFnPc2
(	(	kIx(
<g/>
Clustered	Clustered	k1gMnSc1
Regularly	Regularla	k1gFnSc2
Interspaced	Interspaced	k1gMnSc1
Short	Shorta	k1gFnPc2
Palindromic	Palindromice	k1gFnPc2
Repeats	Repeats	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
úseky	úsek	k1gInPc1
prokaryotické	prokaryotický	k2eAgFnSc2d1
DNA	DNA	kA
obsahující	obsahující	k2eAgFnSc2d1
krátké	krátký	k2eAgFnSc2d1
repetice	repetice	k1gFnSc2
nukleotidů	nukleotid	k1gInPc2
<g/>
.	.	kIx.
</s>