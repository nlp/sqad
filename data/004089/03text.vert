<s>
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1913	[number]	k4	1913
Prostějov	Prostějov	k1gInSc1	Prostějov
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
Stražisko	Stražisko	k1gNnSc1	Stražisko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnSc1d1	pracující
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejíž	jejíž	k3xOyRp3gMnPc4	jejíž
zakladatele	zakladatel	k1gMnPc4	zakladatel
patřil	patřit	k5eAaImAgInS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Proslulý	proslulý	k2eAgInSc1d1	proslulý
je	být	k5eAaImIp3nS	být
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
objevy	objev	k1gInPc7	objev
a	a	k8xC	a
vynálezy	vynález	k1gInPc7	vynález
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
zásadnímu	zásadní	k2eAgNnSc3d1	zásadní
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
a	a	k8xC	a
celosvětovému	celosvětový	k2eAgNnSc3d1	celosvětové
rozšíření	rozšíření	k1gNnSc3	rozšíření
měkkých	měkký	k2eAgFnPc2d1	měkká
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
vycházely	vycházet	k5eAaImAgInP	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
původní	původní	k2eAgFnSc2d1	původní
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hydrogelů	hydrogel	k1gInPc2	hydrogel
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k1gInSc1	Wichterle
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
též	též	k9	též
objevem	objev	k1gInSc7	objev
umělého	umělý	k2eAgNnSc2d1	umělé
polyamidového	polyamidový	k2eAgNnSc2d1	polyamidové
vlákna	vlákno	k1gNnSc2	vlákno
–	–	k?	–
silonu	silon	k1gInSc2	silon
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
rodiny	rodina	k1gFnSc2	rodina
prostějovských	prostějovský	k2eAgMnPc2d1	prostějovský
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Karel	Karel	k1gMnSc1	Karel
Wichterle	Wichterle	k1gInSc4	Wichterle
byl	být	k5eAaImAgMnS	být
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
firmy	firma	k1gFnSc2	firma
Wichterle	Wichterle	k1gFnSc2	Wichterle
a	a	k8xC	a
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
Wikov	Wikov	k1gInSc1	Wikov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Pravoslava	Pravoslava	k1gFnSc1	Pravoslava
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Jana	Jan	k1gMnSc2	Jan
Podivínského	podivínský	k2eAgMnSc2d1	podivínský
<g/>
,	,	kIx,	,
zemského	zemský	k2eAgMnSc2d1	zemský
poslance	poslanec	k1gMnSc2	poslanec
a	a	k8xC	a
majitele	majitel	k1gMnSc2	majitel
velkostatku	velkostatek	k1gInSc2	velkostatek
Kostelec	Kostelec	k1gInSc4	Kostelec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
dědeček	dědeček	k1gMnSc1	dědeček
Tomáš	Tomáš	k1gMnSc1	Tomáš
byl	být	k5eAaImAgMnS	být
také	také	k9	také
poslancem	poslanec	k1gMnSc7	poslanec
a	a	k8xC	a
zakladatelem	zakladatel	k1gMnSc7	zakladatel
cukrovaru	cukrovar	k1gInSc2	cukrovar
ve	v	k7c6	v
Vrbátkách	Vrbátka	k1gFnPc6	Vrbátka
a	a	k8xC	a
sladovny	sladovna	k1gFnSc2	sladovna
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Janem	Jan	k1gMnSc7	Jan
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dětí	dítě	k1gFnPc2	dítě
Karla	Karel	k1gMnSc2	Karel
Wichterleho	Wichterle	k1gMnSc2	Wichterle
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Pravoslavy	Pravoslava	k1gFnSc2	Pravoslava
Podivínské	podivínský	k2eAgFnSc2d1	podivínská
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
mnoho	mnoho	k4c1	mnoho
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
šoku	šok	k1gInSc2	šok
<g/>
,	,	kIx,	,
když	když	k8xS	když
téměř	téměř	k6eAd1	téměř
utonul	utonout	k5eAaPmAgMnS	utonout
v	v	k7c6	v
jezírku	jezírko	k1gNnSc6	jezírko
močůvky	močůvka	k1gFnSc2	močůvka
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
horečkami	horečka	k1gFnPc7	horečka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neustupovaly	ustupovat	k5eNaImAgFnP	ustupovat
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rodinný	rodinný	k2eAgMnSc1d1	rodinný
lékař	lékař	k1gMnSc1	lékař
nařídil	nařídit	k5eAaPmAgMnS	nařídit
klid	klid	k1gInSc4	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
a	a	k8xC	a
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Otto	Otto	k1gMnSc1	Otto
stále	stále	k6eAd1	stále
naživu	naživu	k6eAd1	naživu
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
horečky	horečka	k1gFnSc2	horečka
částečně	částečně	k6eAd1	částečně
polevily	polevit	k5eAaPmAgInP	polevit
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
domů	domů	k6eAd1	domů
docházet	docházet	k5eAaImF	docházet
učitelka	učitelka	k1gFnSc1	učitelka
Vlková	Vlková	k1gFnSc1	Vlková
a	a	k8xC	a
probírala	probírat	k5eAaImAgFnS	probírat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Janem	Jan	k1gMnSc7	Jan
látku	látka	k1gFnSc4	látka
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
stále	stále	k6eAd1	stále
neumíral	umírat	k5eNaImAgMnS	umírat
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
Otto	Otto	k1gMnSc1	Otto
do	do	k7c2	do
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Kollárce	Kollárka	k1gFnSc6	Kollárka
–	–	k?	–
po	po	k7c6	po
přezkoušení	přezkoušení	k1gNnSc6	přezkoušení
ředitelem	ředitel	k1gMnSc7	ředitel
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
nízký	nízký	k2eAgInSc4d1	nízký
věk	věk	k1gInSc4	věk
(	(	kIx(	(
<g/>
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
Státní	státní	k2eAgNnSc4d1	státní
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
byla	být	k5eAaImAgFnS	být
opravdu	opravdu	k6eAd1	opravdu
nezáviděníhodná	nezáviděníhodný	k2eAgFnSc1d1	nezáviděníhodná
–	–	k?	–
byl	být	k5eAaImAgInS	být
daleko	daleko	k6eAd1	daleko
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
(	(	kIx(	(
<g/>
ostatním	ostatní	k2eAgMnSc7d1	ostatní
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
bylo	být	k5eAaImAgNnS	být
zpravidla	zpravidla	k6eAd1	zpravidla
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejmenší	malý	k2eAgMnSc1d3	nejmenší
<g/>
,	,	kIx,	,
a	a	k8xC	a
nesměl	smět	k5eNaImAgMnS	smět
se	se	k3xPyFc4	se
kamarádit	kamarádit	k5eAaImF	kamarádit
s	s	k7c7	s
kluky	kluk	k1gMnPc7	kluk
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
vyrovnalo	vyrovnat	k5eAaPmAgNnS	vyrovnat
a	a	k8xC	a
v	v	k7c6	v
tercii	tercie	k1gFnSc6	tercie
začal	začít	k5eAaPmAgMnS	začít
Otto	Otto	k1gMnSc1	Otto
hrát	hrát	k5eAaImF	hrát
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
juniorského	juniorský	k2eAgNnSc2d1	juniorské
tenisového	tenisový	k2eAgNnSc2d1	tenisové
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
<g/>
.	.	kIx.	.
</s>
<s>
Tenisem	tenis	k1gInSc7	tenis
trávil	trávit	k5eAaImAgMnS	trávit
téměř	téměř	k6eAd1	téměř
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
brzy	brzy	k6eAd1	brzy
projevovat	projevovat	k5eAaImF	projevovat
na	na	k7c6	na
prospěchu	prospěch	k1gInSc6	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oktávy	oktáva	k1gFnSc2	oktáva
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzpamatoval	vzpamatovat	k5eAaPmAgMnS	vzpamatovat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
školních	školní	k2eAgInPc2d1	školní
předmětů	předmět	k1gInPc2	předmět
ho	on	k3xPp3gInSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
bavila	bavit	k5eAaImAgFnS	bavit
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejméně	málo	k6eAd3	málo
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
latině	latina	k1gFnSc3	latina
a	a	k8xC	a
řečtině	řečtina	k1gFnSc3	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
byl	být	k5eAaImAgMnS	být
Otto	Otto	k1gMnSc1	Otto
rozhodnutý	rozhodnutý	k2eAgMnSc1d1	rozhodnutý
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
žádný	žádný	k3yNgInSc4	žádný
vyhraněný	vyhraněný	k2eAgInSc4d1	vyhraněný
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
uplatnit	uplatnit	k5eAaPmF	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
strojírenství	strojírenství	k1gNnSc4	strojírenství
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
brát	brát	k5eAaImF	brát
soukromé	soukromý	k2eAgFnPc4d1	soukromá
hodiny	hodina	k1gFnPc4	hodina
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
žádala	žádat	k5eAaImAgFnS	žádat
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
nasvědčovalo	nasvědčovat	k5eAaImAgNnS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Otto	Otto	k1gMnSc1	Otto
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
dráhu	dráha	k1gFnSc4	dráha
strojního	strojní	k2eAgMnSc2d1	strojní
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
nesměl	smět	k5eNaImAgInS	smět
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
záměry	záměr	k1gInPc7	záměr
svěřit	svěřit	k5eAaPmF	svěřit
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Součkovi	Souček	k1gMnSc3	Souček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
studium	studium	k1gNnSc1	studium
na	na	k7c6	na
strojírenství	strojírenství	k1gNnSc6	strojírenství
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
chmurných	chmurný	k2eAgFnPc6d1	chmurná
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
doporučil	doporučit	k5eAaPmAgMnS	doporučit
mu	on	k3xPp3gMnSc3	on
studium	studium	k1gNnSc4	studium
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
studium	studium	k1gNnSc1	studium
zajímavější	zajímavý	k2eAgNnSc1d2	zajímavější
a	a	k8xC	a
prostředí	prostředí	k1gNnSc1	prostředí
chemické	chemický	k2eAgFnSc2d1	chemická
fakulty	fakulta	k1gFnSc2	fakulta
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
nakloněno	naklonit	k5eAaPmNgNnS	naklonit
samostatné	samostatný	k2eAgFnSc6d1	samostatná
vědecké	vědecký	k2eAgFnSc3d1	vědecká
práci	práce	k1gFnSc3	práce
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Wichterleho	Wichterle	k1gMnSc4	Wichterle
jednoznačně	jednoznačně	k6eAd1	jednoznačně
lákalo	lákat	k5eAaImAgNnS	lákat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
Otto	Otto	k1gMnSc1	Otto
podal	podat	k5eAaPmAgMnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
i	i	k9	i
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
Chemicko-technologického	chemickoechnologický	k2eAgNnSc2d1	chemicko-technologické
inženýrství	inženýrství	k1gNnSc2	inženýrství
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
chemicko-technologická	chemickoechnologický	k2eAgFnSc1d1	chemicko-technologická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
započal	započnout	k5eAaPmAgMnS	započnout
své	svůj	k3xOyFgNnSc4	svůj
studium	studium	k1gNnSc4	studium
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
přivedla	přivést	k5eAaPmAgFnS	přivést
Ottu	Otta	k1gMnSc4	Otta
také	také	k9	také
k	k	k7c3	k
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
problematice	problematika	k1gFnSc3	problematika
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
člen	člen	k1gInSc1	člen
Spolku	spolek	k1gInSc2	spolek
posluchačů	posluchač	k1gMnPc2	posluchač
chemického	chemický	k2eAgNnSc2d1	chemické
inženýrství	inženýrství	k1gNnSc2	inženýrství
(	(	kIx(	(
<g/>
SPICH	SPICH	kA	SPICH
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
udiven	udiven	k2eAgInSc1d1	udiven
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
SPICH	SPICH	kA	SPICH
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c6	o
řadě	řada	k1gFnSc6	řada
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
studijních	studijní	k2eAgFnPc2d1	studijní
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
iniciativy	iniciativa	k1gFnSc2	iniciativa
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
školská	školský	k2eAgFnSc1d1	školská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
ročníku	ročník	k1gInSc6	ročník
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
debatní	debatní	k2eAgFnSc4d1	debatní
schůzi	schůze	k1gFnSc4	schůze
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
Nedostatky	nedostatek	k1gInPc7	nedostatek
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
většina	většina	k1gFnSc1	většina
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
docentů	docent	k1gMnPc2	docent
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vyložil	vyložit	k5eAaPmAgMnS	vyložit
všechny	všechen	k3xTgInPc4	všechen
rozpory	rozpor	k1gInPc4	rozpor
v	v	k7c6	v
osnově	osnova	k1gFnSc6	osnova
(	(	kIx(	(
<g/>
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
cvičení	cvičení	k1gNnSc4	cvičení
začínala	začínat	k5eAaImAgFnS	začínat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
příslušná	příslušný	k2eAgFnSc1d1	příslušná
teorie	teorie	k1gFnSc1	teorie
<g/>
)	)	kIx)	)
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
alternativy	alternativa	k1gFnPc1	alternativa
rozložení	rozložení	k1gNnSc2	rozložení
předmětů	předmět	k1gInPc2	předmět
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
však	však	k9	však
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
přednesenými	přednesený	k2eAgInPc7d1	přednesený
návrhy	návrh	k1gInPc7	návrh
dokonce	dokonce	k9	dokonce
pobouřeni	pobouřit	k5eAaPmNgMnP	pobouřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
znamenaly	znamenat	k5eAaImAgFnP	znamenat
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
počínající	počínající	k2eAgNnSc1d1	počínající
ohrožení	ohrožení	k1gNnSc1	ohrožení
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
také	také	k9	také
ve	v	k7c6	v
SPICHu	SPICHus	k1gInSc6	SPICHus
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
studentských	studentský	k2eAgInPc6d1	studentský
spolcích	spolek	k1gInPc6	spolek
i	i	k9	i
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
neslučitelné	slučitelný	k2eNgInPc1d1	neslučitelný
tábory	tábor	k1gInPc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Neexistoval	existovat	k5eNaImAgInS	existovat
žádný	žádný	k3yNgInSc1	žádný
politický	politický	k2eAgInSc1d1	politický
střed	střed	k1gInSc1	střed
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
pravice	pravice	k1gFnSc2	pravice
<g/>
.	.	kIx.	.
</s>
<s>
Neznamenalo	znamenat	k5eNaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
buď	buď	k8xC	buď
fašisty	fašista	k1gMnPc7	fašista
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
komunisty	komunista	k1gMnPc4	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
organizováni	organizován	k2eAgMnPc1d1	organizován
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
po	po	k7c6	po
málu	málo	k1gNnSc6	málo
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
jasně	jasně	k6eAd1	jasně
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
politickému	politický	k2eAgNnSc3d1	politické
smýšlení	smýšlení	k1gNnSc3	smýšlení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
levice	levice	k1gFnSc1	levice
měla	mít	k5eAaImAgFnS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
výuky	výuka	k1gFnSc2	výuka
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
Wichterle	Wichterle	k1gInSc1	Wichterle
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
byl	být	k5eAaImAgInS	být
levicovým	levicový	k2eAgMnSc7d1	levicový
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
SPICH	SPICH	kA	SPICH
<g/>
.	.	kIx.	.
</s>
<s>
Levice	levice	k1gFnSc1	levice
se	se	k3xPyFc4	se
usnesla	usnést	k5eAaPmAgFnS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
volby	volba	k1gFnPc1	volba
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
<g/>
,	,	kIx,	,
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
pravici	pravice	k1gFnSc3	pravice
poměrné	poměrný	k2eAgNnSc4d1	poměrné
zastoupení	zastoupení	k1gNnSc4	zastoupení
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnPc4	hlasování
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Wichterle	Wichterle	k1gInSc1	Wichterle
rozdílem	rozdíl	k1gInSc7	rozdíl
jediného	jediný	k2eAgInSc2d1	jediný
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
programovém	programový	k2eAgNnSc6d1	programové
prohlášení	prohlášení	k1gNnSc6	prohlášení
pak	pak	k6eAd1	pak
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
pravici	pravice	k1gFnSc4	pravice
dohodnuté	dohodnutý	k2eAgFnSc2d1	dohodnutá
poměrné	poměrný	k2eAgFnSc2d1	poměrná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
téměř	téměř	k6eAd1	téměř
poloviční	poloviční	k2eAgNnSc4d1	poloviční
zastoupení	zastoupení	k1gNnSc4	zastoupení
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
pravice	pravice	k1gFnSc2	pravice
to	ten	k3xDgNnSc1	ten
však	však	k9	však
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
SPICH	SPICH	kA	SPICH
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
levicovým	levicový	k2eAgInSc7d1	levicový
spolkem	spolek	k1gInSc7	spolek
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
všechny	všechen	k3xTgInPc4	všechen
spolky	spolek	k1gInPc4	spolek
zůstaly	zůstat	k5eAaPmAgInP	zůstat
tradičně	tradičně	k6eAd1	tradičně
pravicové	pravicový	k2eAgInPc1d1	pravicový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
tvořil	tvořit	k5eAaImAgInS	tvořit
Wichterle	Wichterle	k1gInSc1	Wichterle
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Emila	Emil	k1gMnSc2	Emil
Votočka	Votočka	k1gFnSc1	Votočka
<g/>
,	,	kIx,	,
významného	významný	k2eAgMnSc4d1	významný
chemika	chemik	k1gMnSc4	chemik
a	a	k8xC	a
učitele	učitel	k1gMnPc4	učitel
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
spolutvůrce	spolutvůrce	k1gMnSc2	spolutvůrce
českého	český	k2eAgNnSc2d1	české
chemického	chemický	k2eAgNnSc2d1	chemické
názvosloví	názvosloví	k1gNnSc2	názvosloví
<g/>
.	.	kIx.	.
</s>
<s>
Prof.	prof.	kA	prof.
Votoček	Votočka	k1gFnPc2	Votočka
také	také	k9	také
Wichterleho	Wichterle	k1gMnSc4	Wichterle
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
svým	svůj	k3xOyFgNnSc7	svůj
nesmlouvavým	smlouvavý	k2eNgNnSc7d1	nesmlouvavé
hájením	hájení	k1gNnSc7	hájení
svobody	svoboda	k1gFnSc2	svoboda
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
státní	státní	k2eAgMnSc1d1	státní
(	(	kIx(	(
<g/>
inženýrské	inženýrský	k2eAgFnPc1d1	inženýrská
<g/>
)	)	kIx)	)
zkoušky	zkouška	k1gFnPc1	zkouška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
pět	pět	k4xCc4	pět
prací	práce	k1gFnPc2	práce
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
především	především	k9	především
na	na	k7c4	na
deriváty	derivát	k1gInPc4	derivát
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
doktorátu	doktorát	k1gInSc2	doktorát
by	by	kYmCp3nP	by
dle	dle	k7c2	dle
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
norem	norma	k1gFnPc2	norma
stačila	stačit	k5eAaBmAgFnS	stačit
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1936	[number]	k4	1936
pak	pak	k6eAd1	pak
promoval	promovat	k5eAaBmAgMnS	promovat
na	na	k7c4	na
doktora	doktor	k1gMnSc4	doktor
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Votoček	Votočka	k1gFnPc2	Votočka
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
místo	místo	k1gNnSc4	místo
docenta	docent	k1gMnSc2	docent
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
laboratoří	laboratoř	k1gFnPc2	laboratoř
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Wichterle	Wichterle	k1gInSc4	Wichterle
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
uzavření	uzavření	k1gNnSc2	uzavření
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Votočka	Votočka	k1gFnSc1	Votočka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
řádného	řádný	k2eAgMnSc2d1	řádný
asistenta	asistent	k1gMnSc2	asistent
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
fakultě	fakulta	k1gFnSc6	fakulta
byla	být	k5eAaImAgFnS	být
mizivá	mizivý	k2eAgFnSc1d1	mizivá
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
Votoček	Votočka	k1gFnPc2	Votočka
doporučil	doporučit	k5eAaPmAgInS	doporučit
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rýsovala	rýsovat	k5eAaImAgFnS	rýsovat
možnost	možnost	k1gFnSc1	možnost
vzniku	vznik	k1gInSc2	vznik
nového	nový	k2eAgInSc2d1	nový
oboru	obor	k1gInSc2	obor
–	–	k?	–
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
větší	veliký	k2eAgFnSc2d2	veliký
námahy	námaha	k1gFnSc2	námaha
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
biologická	biologický	k2eAgNnPc4d1	biologické
cvičení	cvičení	k1gNnSc4	cvičení
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
z	z	k7c2	z
chemie	chemie	k1gFnSc2	chemie
dostal	dostat	k5eAaPmAgMnS	dostat
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
přípravy	příprava	k1gFnSc2	příprava
dostatečnou	dostatečná	k1gFnSc4	dostatečná
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
analytické	analytický	k2eAgFnSc3d1	analytická
chemii	chemie	k1gFnSc3	chemie
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc4d1	odlišný
přístup	přístup	k1gInSc4	přístup
než	než	k8xS	než
zkoušející	zkoušející	k1gFnSc4	zkoušející
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušku	zkouška	k1gFnSc4	zkouška
z	z	k7c2	z
anatomie	anatomie	k1gFnSc2	anatomie
však	však	k9	však
již	již	k6eAd1	již
absolvovat	absolvovat	k5eAaPmF	absolvovat
nestihl	stihnout	k5eNaPmAgMnS	stihnout
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
obě	dva	k4xCgFnPc4	dva
povinné	povinný	k2eAgFnPc4d1	povinná
pitvy	pitva	k1gFnPc4	pitva
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
zkoušce	zkouška	k1gFnSc3	zkouška
připraven	připravit	k5eAaPmNgMnS	připravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
termín	termín	k1gInSc1	termín
zkoušky	zkouška	k1gFnSc2	zkouška
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
uzavření	uzavření	k1gNnSc1	uzavření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
mu	on	k3xPp3gMnSc3	on
zkoušku	zkouška	k1gFnSc4	zkouška
překazilo	překazit	k5eAaPmAgNnS	překazit
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
tedy	tedy	k9	tedy
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
asistentem	asistent	k1gMnSc7	asistent
Votočka	Votočka	k1gFnSc1	Votočka
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
říci	říct	k5eAaPmF	říct
i	i	k9	i
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
získal	získat	k5eAaPmAgMnS	získat
u	u	k7c2	u
Votočka	Votočka	k1gFnSc1	Votočka
první	první	k4xOgFnSc2	první
učitelské	učitelský	k2eAgFnSc2d1	učitelská
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
když	když	k8xS	když
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
přednáškový	přednáškový	k2eAgMnSc1d1	přednáškový
asistent	asistent	k1gMnSc1	asistent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
obnášela	obnášet	k5eAaImAgFnS	obnášet
přípravu	příprava	k1gFnSc4	příprava
demonstračních	demonstrační	k2eAgInPc2d1	demonstrační
pokusů	pokus	k1gInPc2	pokus
a	a	k8xC	a
vzorců	vzorec	k1gInPc2	vzorec
na	na	k7c4	na
tabuli	tabule	k1gFnSc4	tabule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zastupování	zastupování	k1gNnSc2	zastupování
profesora	profesor	k1gMnSc2	profesor
při	při	k7c6	při
nečekané	čekaný	k2eNgFnSc6d1	nečekaná
absenci	absence	k1gFnSc6	absence
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
místo	místo	k7c2	místo
přednášky	přednáška	k1gFnSc2	přednáška
šli	jít	k5eAaImAgMnP	jít
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
neprodyšně	prodyšně	k6eNd1	prodyšně
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
chemické	chemický	k2eAgFnPc4d1	chemická
aparatury	aparatura	k1gFnPc4	aparatura
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
popravách	poprava	k1gFnPc6	poprava
a	a	k8xC	a
deportacích	deportace	k1gFnPc6	deportace
studentů	student	k1gMnPc2	student
následovalo	následovat	k5eAaImAgNnS	následovat
i	i	k9	i
zatýkání	zatýkání	k1gNnSc1	zatýkání
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
především	především	k9	především
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
hned	hned	k6eAd1	hned
nenašli	najít	k5eNaPmAgMnP	najít
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
baťovské	baťovský	k2eAgFnSc2d1	baťovská
laboratoře	laboratoř	k1gFnSc2	laboratoř
nabídly	nabídnout	k5eAaPmAgFnP	nabídnout
Ottovi	Otta	k1gMnSc3	Otta
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Výzkumných	výzkumný	k2eAgFnPc6d1	výzkumná
chemických	chemický	k2eAgFnPc6d1	chemická
dílnách	dílna	k1gFnPc6	dílna
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Wichterle	Wichterle	k1gInSc1	Wichterle
zprvu	zprvu	k6eAd1	zprvu
odmítal	odmítat	k5eAaImAgInS	odmítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wichterleho	Wichterleze	k6eAd1	Wichterleze
manželka	manželka	k1gFnSc1	manželka
Linda	Linda	k1gFnSc1	Linda
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
averzi	averze	k1gFnSc4	averze
vůči	vůči	k7c3	vůči
zlínské	zlínský	k2eAgFnSc3d1	zlínská
materialistické	materialistický	k2eAgFnSc3d1	materialistická
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Otto	Otto	k1gMnSc1	Otto
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
začala	začít	k5eAaPmAgFnS	začít
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
zatýkání	zatýkání	k1gNnSc2	zatýkání
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
sepsat	sepsat	k5eAaPmF	sepsat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
vedoucím	vedoucí	k1gMnSc7	vedoucí
byl	být	k5eAaImAgMnS	být
docent	docent	k1gMnSc1	docent
Stanislav	Stanislav	k1gMnSc1	Stanislav
Landa	Landa	k1gMnSc1	Landa
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k1gFnSc1	Wichterle
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
jako	jako	k9	jako
na	na	k7c4	na
vynikajícího	vynikající	k2eAgMnSc4d1	vynikající
chemika	chemik	k1gMnSc4	chemik
i	i	k8xC	i
skvělého	skvělý	k2eAgMnSc4d1	skvělý
vedoucího	vedoucí	k1gMnSc4	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Landa	Landa	k1gMnSc1	Landa
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tematiku	tematika	k1gFnSc4	tematika
výzkumu	výzkum	k1gInSc2	výzkum
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
Otto	Otto	k1gMnSc1	Otto
určovat	určovat	k5eAaImF	určovat
výhradně	výhradně	k6eAd1	výhradně
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
výsledky	výsledek	k1gInPc4	výsledek
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
Wichterle	Wichterle	k1gInSc1	Wichterle
zabývat	zabývat	k5eAaImF	zabývat
i	i	k9	i
tématy	téma	k1gNnPc7	téma
významnými	významný	k2eAgFnPc7d1	významná
i	i	k9	i
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c6	na
prověření	prověření	k1gNnSc6	prověření
Carothensových	Carothensův	k2eAgInPc2d1	Carothensův
patentů	patent	k1gInPc2	patent
o	o	k7c6	o
Nylonu	nylon	k1gInSc6	nylon
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nylon	nylon	k1gInSc1	nylon
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
jeden	jeden	k4xCgInSc4	jeden
základní	základní	k2eAgInSc4d1	základní
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
nedala	dát	k5eNaPmAgFnS	dát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
spřádat	spřádat	k5eAaImF	spřádat
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Wichterlovi	Wichterlův	k2eAgMnPc1d1	Wichterlův
velmi	velmi	k6eAd1	velmi
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgMnS	moct
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
prací	práce	k1gFnPc2	práce
s	s	k7c7	s
monosacharidy	monosacharid	k1gInPc7	monosacharid
u	u	k7c2	u
Votočka	Votočka	k1gFnSc1	Votočka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
s	s	k7c7	s
polyamidovými	polyamidový	k2eAgNnPc7d1	polyamidové
vlákny	vlákno	k1gNnPc7	vlákno
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
pokusů	pokus	k1gInPc2	pokus
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
makromolekulární	makromolekulární	k2eAgInSc4d1	makromolekulární
polyamid	polyamid	k1gInSc4	polyamid
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tavenina	tavenina	k1gFnSc1	tavenina
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
táhnout	táhnout	k5eAaImF	táhnout
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
pevná	pevný	k2eAgNnPc4d1	pevné
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
úspěchu	úspěch	k1gInSc3	úspěch
byli	být	k5eAaImAgMnP	být
Wichterlovi	Wichterlův	k2eAgMnPc1d1	Wichterlův
přiděleni	přidělen	k2eAgMnPc1d1	přidělen
další	další	k2eAgMnPc1d1	další
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
oddělení	oddělení	k1gNnSc4	oddělení
nových	nový	k2eAgFnPc2d1	nová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
novém	nový	k2eAgNnSc6d1	nové
oddělení	oddělení	k1gNnSc6	oddělení
se	se	k3xPyFc4	se
připravila	připravit	k5eAaPmAgFnS	připravit
metoda	metoda	k1gFnSc1	metoda
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
látky	látka	k1gFnSc2	látka
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
zajistit	zajistit	k5eAaPmF	zajistit
co	co	k9	co
nejlevnější	levný	k2eAgInSc4d3	nejlevnější
způsob	způsob	k1gInSc4	způsob
výroby	výroba	k1gFnSc2	výroba
potřebných	potřebný	k2eAgFnPc2d1	potřebná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kaprolaktamu	kaprolaktam	k1gInSc2	kaprolaktam
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc2d1	základní
látky	látka	k1gFnSc2	látka
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
Nylonu	nylon	k1gInSc2	nylon
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
dalšího	další	k2eAgInSc2d1	další
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
už	už	k6eAd1	už
spřádala	spřádat	k5eAaImAgFnS	spřádat
příze	příze	k1gFnSc1	příze
a	a	k8xC	a
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
jednotkovém	jednotkový	k2eAgNnSc6d1	jednotkové
provozu	provoz	k1gInSc3	provoz
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dokonce	dokonce	k9	dokonce
první	první	k4xOgFnPc1	první
ponožky	ponožka	k1gFnPc1	ponožka
a	a	k8xC	a
dámské	dámský	k2eAgFnPc1d1	dámská
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Punčochářští	punčochářský	k2eAgMnPc1d1	punčochářský
odborníci	odborník	k1gMnPc1	odborník
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
paty	pata	k1gFnPc1	pata
a	a	k8xC	a
špičky	špička	k1gFnPc1	špička
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
zesílit	zesílit	k5eAaPmF	zesílit
bavlnou	bavlna	k1gFnSc7	bavlna
a	a	k8xC	a
syntetickou	syntetický	k2eAgFnSc4d1	syntetická
přízi	příze	k1gFnSc4	příze
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
méně	málo	k6eAd2	málo
hodnotnou	hodnotný	k2eAgFnSc4d1	hodnotná
náhražku	náhražka	k1gFnSc4	náhražka
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půlroce	půlrok	k1gInSc6	půlrok
nošení	nošení	k1gNnSc2	nošení
se	se	k3xPyFc4	se
však	však	k9	však
bavlna	bavlna	k1gFnSc1	bavlna
ze	z	k7c2	z
"	"	kIx"	"
<g/>
zesílených	zesílený	k2eAgFnPc2d1	zesílená
<g/>
"	"	kIx"	"
pat	pata	k1gFnPc2	pata
a	a	k8xC	a
špiček	špička	k1gFnPc2	špička
vydrolila	vydrolit	k5eAaPmAgFnS	vydrolit
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
polyamid	polyamid	k1gInSc1	polyamid
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedotčen	dotknout	k5eNaPmNgInS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
úspěchy	úspěch	k1gInPc4	úspěch
čeští	český	k2eAgMnPc1d1	český
představitelé	představitel	k1gMnPc1	představitel
firmy	firma	k1gFnSc2	firma
naléhali	naléhat	k5eAaBmAgMnP	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
udržel	udržet	k5eAaPmAgInS	udržet
před	před	k7c7	před
Němci	Němec	k1gMnPc7	Němec
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
sbírání	sbírání	k1gNnSc6	sbírání
technologických	technologický	k2eAgInPc2d1	technologický
podkladů	podklad	k1gInPc2	podklad
k	k	k7c3	k
jejich	jejich	k3xOp3gMnPc3	jejich
aplikaci	aplikace	k1gFnSc4	aplikace
hned	hned	k6eAd1	hned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nemohl	moct	k5eNaImAgMnS	moct
proto	proto	k8xC	proto
vzniknout	vzniknout	k5eAaPmF	vzniknout
větší	veliký	k2eAgInSc4d2	veliký
poloprovoz	poloprovoz	k1gInSc4	poloprovoz
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
stísněných	stísněný	k2eAgFnPc6d1	stísněná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nylon	nylon	k1gInSc1	nylon
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábět	vyrábět	k5eAaImF	vyrábět
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Silon	silon	k1gInSc1	silon
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jen	jen	k9	jen
malé	malý	k2eAgInPc4d1	malý
objemy	objem	k1gInPc4	objem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
linka	linka	k1gFnSc1	linka
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
desítky	desítka	k1gFnSc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
nylonu	nylon	k1gInSc2	nylon
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1942	[number]	k4	1942
začalo	začít	k5eAaPmAgNnS	začít
zatýkání	zatýkání	k1gNnSc1	zatýkání
i	i	k9	i
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgNnSc1d1	zdejší
Gestapo	gestapo	k1gNnSc1	gestapo
se	se	k3xPyFc4	se
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
o	o	k7c6	o
likvidaci	likvidace	k1gFnSc6	likvidace
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
účastnil	účastnit	k5eAaImAgMnS	účastnit
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
schůzek	schůzka	k1gFnPc2	schůzka
u	u	k7c2	u
ing.	ing.	kA	ing.
Kouteckého	Koutecký	k2eAgNnSc2d1	Koutecký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poslouchala	poslouchat	k5eAaImAgFnS	poslouchat
vážná	vážný	k2eAgFnSc1d1	vážná
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
sledovaly	sledovat	k5eAaImAgInP	sledovat
pohyby	pohyb	k1gInPc1	pohyb
front	front	k1gInSc1	front
a	a	k8xC	a
zprávy	zpráva	k1gFnPc1	zpráva
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
nemluvilo	mluvit	k5eNaImAgNnS	mluvit
o	o	k7c4	o
organizované	organizovaný	k2eAgFnPc4d1	organizovaná
odbojové	odbojový	k2eAgFnPc4d1	odbojová
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wichterle	Wichterle	k1gInSc1	Wichterle
neměl	mít	k5eNaImAgInS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
komunistické	komunistický	k2eAgFnSc2d1	komunistická
organizace	organizace	k1gFnSc2	organizace
ani	ani	k8xC	ani
když	když	k8xS	když
četl	číst	k5eAaImAgMnS	číst
vypůjčeného	vypůjčený	k2eAgMnSc4d1	vypůjčený
Engelsova	Engelsův	k2eAgMnSc4d1	Engelsův
Anti-Duehringa	Anti-Duehring	k1gMnSc4	Anti-Duehring
<g/>
.	.	kIx.	.
</s>
<s>
Koutecký	Koutecký	k2eAgInSc1d1	Koutecký
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
odbojové	odbojový	k2eAgFnSc2d1	odbojová
buňky	buňka	k1gFnSc2	buňka
<g/>
"	"	kIx"	"
zatčen	zatčen	k2eAgInSc4d1	zatčen
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
podroben	podrobit	k5eAaPmNgInS	podrobit
strašnému	strašný	k2eAgInSc3d1	strašný
Sonderbehandlungu	Sonderbehandlung	k1gInSc3	Sonderbehandlung
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
zvláštnímu	zvláštní	k2eAgInSc3d1	zvláštní
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
nevyzradil	vyzradit	k5eNaPmAgMnS	vyzradit
žádná	žádný	k3yNgNnPc4	žádný
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
dalšímu	další	k2eAgNnSc3d1	další
týrání	týrání	k1gNnSc3	týrání
<g/>
,	,	kIx,	,
oběsil	oběsit	k5eAaPmAgInS	oběsit
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
cele	cela	k1gFnSc6	cela
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc4	gestapo
však	však	k9	však
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
mrtvole	mrtvola	k1gFnSc3	mrtvola
přivedlo	přivést	k5eAaPmAgNnS	přivést
nic	nic	k6eAd1	nic
netušící	tušící	k2eNgFnSc4d1	netušící
manželku	manželka	k1gFnSc4	manželka
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
zhroucení	zhroucení	k1gNnSc2	zhroucení
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
všech	všecek	k3xTgNnPc2	všecek
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgMnS	být
Wichterle	Wichterle	k1gInSc4	Wichterle
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	dík	k1gInPc1	dík
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
korespondenci	korespondence	k1gFnSc4	korespondence
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
chemikem	chemik	k1gMnSc7	chemik
Langenbeckem	Langenbeck	k1gInSc7	Langenbeck
nebyl	být	k5eNaImAgInS	být
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
táboru	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
za	za	k7c7	za
sebou	se	k3xPyFc7	se
nacisté	nacista	k1gMnPc1	nacista
měli	mít	k5eAaImAgMnP	mít
prohry	prohra	k1gFnPc4	prohra
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
Zlínem	Zlín	k1gInSc7	Zlín
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
americké	americký	k2eAgInPc4d1	americký
letouny	letoun	k1gInPc4	letoun
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
skončilo	skončit	k5eAaPmAgNnS	skončit
Wichterlovo	Wichterlův	k2eAgNnSc1d1	Wichterlovo
působení	působení	k1gNnSc1	působení
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
dostali	dostat	k5eAaPmAgMnP	dostat
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Wichterle	Wichterle	k1gInSc1	Wichterle
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vybavení	vybavení	k1gNnSc1	vybavení
laboratoří	laboratoř	k1gFnPc2	laboratoř
sice	sice	k8xC	sice
nebylo	být	k5eNaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
hrozném	hrozný	k2eAgInSc6d1	hrozný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k1gInSc1	Wichterle
byl	být	k5eAaImAgInS	být
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
obnovit	obnovit	k5eAaPmF	obnovit
výuku	výuka	k1gFnSc4	výuka
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
docentem	docent	k1gMnSc7	docent
Lukešem	Lukeš	k1gMnSc7	Lukeš
museli	muset	k5eAaImAgMnP	muset
nejprve	nejprve	k6eAd1	nejprve
najít	najít	k5eAaPmF	najít
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
improvizovaných	improvizovaný	k2eAgFnPc6d1	improvizovaná
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
části	část	k1gFnSc6	část
Ústavu	ústav	k1gInSc2	ústav
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
v	v	k7c6	v
Horské	Horské	k2eAgFnSc6d1	Horské
ulici	ulice	k1gFnSc6	ulice
zřídili	zřídit	k5eAaPmAgMnP	zřídit
laboratoře	laboratoř	k1gFnSc2	laboratoř
posluchačů	posluchač	k1gMnPc2	posluchač
a	a	k8xC	a
už	už	k6eAd1	už
během	během	k7c2	během
prázdnin	prázdniny	k1gFnPc2	prázdniny
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
začaly	začít	k5eAaPmAgInP	začít
střídat	střídat	k5eAaImF	střídat
turnusy	turnus	k1gInPc4	turnus
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Nebyly	být	k5eNaImAgFnP	být
však	však	k9	však
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
žádné	žádný	k3yNgInPc4	žádný
návody	návod	k1gInPc4	návod
ke	k	k7c3	k
cvičení	cvičení	k1gNnSc3	cvičení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Wichterle	Wichterle	k1gInSc1	Wichterle
trávil	trávit	k5eAaImAgInS	trávit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
večerů	večer	k1gInPc2	večer
sepisováním	sepisování	k1gNnSc7	sepisování
návodů	návod	k1gInPc2	návod
pro	pro	k7c4	pro
základní	základní	k2eAgFnPc4d1	základní
preparativní	preparativní	k2eAgFnPc4d1	preparativní
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
tak	tak	k6eAd1	tak
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgNnPc1	první
skripta	skripta	k1gNnPc1	skripta
pro	pro	k7c4	pro
organickou	organický	k2eAgFnSc4d1	organická
i	i	k8xC	i
anorganickou	anorganický	k2eAgFnSc4d1	anorganická
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svým	svůj	k3xOyFgNnSc7	svůj
pojetím	pojetí	k1gNnSc7	pojetí
předbíhala	předbíhat	k5eAaImAgFnS	předbíhat
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaPmAgInS	napsat
dokonce	dokonce	k9	dokonce
německou	německý	k2eAgFnSc4d1	německá
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc4d1	ruská
verzi	verze	k1gFnSc4	verze
těchto	tento	k3xDgFnPc2	tento
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
průběžně	průběžně	k6eAd1	průběžně
vylepšoval	vylepšovat	k5eAaImAgInS	vylepšovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lukešem	Lukeš	k1gMnSc7	Lukeš
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zcela	zcela	k6eAd1	zcela
od	od	k7c2	od
základů	základ	k1gInPc2	základ
změnit	změnit	k5eAaPmF	změnit
výuku	výuka	k1gFnSc4	výuka
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
poválečných	poválečný	k2eAgFnPc6d1	poválečná
přednáškách	přednáška	k1gFnPc6	přednáška
zavedl	zavést	k5eAaPmAgInS	zavést
průběžné	průběžný	k2eAgInPc4d1	průběžný
testy	test	k1gInPc4	test
a	a	k8xC	a
zkoušky	zkouška	k1gFnPc4	zkouška
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jen	jen	k9	jen
formalitou	formalita	k1gFnSc7	formalita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doplňovala	doplňovat	k5eAaImAgFnS	doplňovat
celkové	celkový	k2eAgNnSc4d1	celkové
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
však	však	k9	však
neuspěla	uspět	k5eNaPmAgFnS	uspět
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
konservatismu	konservatismus	k1gInSc3	konservatismus
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
průběžné	průběžný	k2eAgNnSc4d1	průběžné
hodnocení	hodnocení	k1gNnSc4	hodnocení
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
jen	jen	k9	jen
špatně	špatně	k6eAd1	špatně
politicky	politicky	k6eAd1	politicky
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
a	a	k8xC	a
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
správná	správný	k2eAgFnSc1d1	správná
<g/>
"	"	kIx"	"
politická	politický	k2eAgFnSc1d1	politická
příslušnost	příslušnost	k1gFnSc1	příslušnost
neznamená	znamenat	k5eNaImIp3nS	znamenat
dobré	dobrý	k2eAgInPc4d1	dobrý
studijní	studijní	k2eAgInPc4d1	studijní
výsledky	výsledek	k1gInPc4	výsledek
-	-	kIx~	-
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
dosazováním	dosazování	k1gNnSc7	dosazování
členů	člen	k1gMnPc2	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
školy	škola	k1gFnSc2	škola
přibývalo	přibývat	k5eAaImAgNnS	přibývat
Wichterlovi	Wichterlův	k2eAgMnPc1d1	Wichterlův
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
novým	nový	k2eAgMnSc7d1	nový
rektorem	rektor	k1gMnSc7	rektor
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Vladimír	Vladimír	k1gMnSc1	Vladimír
Maděra	Maděra	k1gMnSc1	Maděra
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
politická	politický	k2eAgFnSc1d1	politická
čistka	čistka	k1gFnSc1	čistka
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
i	i	k9	i
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
Otty	Otta	k1gMnSc2	Otta
Wichterla	Wichterl	k1gMnSc2	Wichterl
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
odvolání	odvolání	k1gNnSc4	odvolání
nepozbyla	pozbýt	k5eNaPmAgFnS	pozbýt
výpověď	výpověď	k1gFnSc4	výpověď
platnosti	platnost	k1gFnSc2	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
poté	poté	k6eAd1	poté
získal	získat	k5eAaPmAgMnS	získat
vědecký	vědecký	k2eAgInSc4d1	vědecký
azyl	azyl	k1gInSc4	azyl
na	na	k7c6	na
Československé	československý	k2eAgFnSc6d1	Československá
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládlo	vládnout	k5eAaImAgNnS	vládnout
přece	přece	k9	přece
jen	jen	k9	jen
politicky	politicky	k6eAd1	politicky
liberálnější	liberální	k2eAgNnSc4d2	liberálnější
prostředí	prostředí	k1gNnSc4	prostředí
než	než	k8xS	než
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
Wichterle	Wichterle	k1gInSc1	Wichterle
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
vedoucím	vedoucí	k1gMnSc7	vedoucí
laboratoře	laboratoř	k1gFnSc2	laboratoř
makromolekulárních	makromolekulární	k2eAgFnPc2d1	makromolekulární
látek	látka	k1gFnPc2	látka
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
ČSAV	ČSAV	kA	ČSAV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Ústav	ústav	k1gInSc1	ústav
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
ÚMCH	ÚMCH	kA	ÚMCH
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
na	na	k7c6	na
Petřinách	Petřiny	k1gFnPc6	Petřiny
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
strůjcem	strůjce	k1gMnSc7	strůjce
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gInPc2	jeho
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Wichterle	Wichterl	k1gMnSc2	Wichterl
vždy	vždy	k6eAd1	vždy
hájil	hájit	k5eAaImAgMnS	hájit
nezbytnost	nezbytnost	k1gFnSc4	nezbytnost
širokého	široký	k2eAgInSc2d1	široký
základního	základní	k2eAgInSc2d1	základní
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgInPc1d1	nový
praktické	praktický	k2eAgInPc1d1	praktický
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
zejména	zejména	k9	zejména
z	z	k7c2	z
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
mistrovského	mistrovský	k2eAgNnSc2d1	mistrovské
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
určité	určitý	k2eAgFnSc2d1	určitá
metodiky	metodika	k1gFnSc2	metodika
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
násilně	násilně	k6eAd1	násilně
cílený	cílený	k2eAgInSc1d1	cílený
či	či	k8xC	či
aplikovaný	aplikovaný	k2eAgInSc1d1	aplikovaný
výzkum	výzkum	k1gInSc1	výzkum
tyto	tento	k3xDgInPc4	tento
výsledky	výsledek	k1gInPc4	výsledek
nemívá	mívat	k5eNaImIp3nS	mívat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
poznal	poznat	k5eAaPmAgInS	poznat
již	již	k9	již
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
začátcích	začátek	k1gInPc6	začátek
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
fy	fy	kA	fy
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
po	po	k7c6	po
r.	r.	kA	r.
1948	[number]	k4	1948
odvážně	odvážně	k6eAd1	odvážně
hájil	hájit	k5eAaImAgMnS	hájit
i	i	k9	i
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
jako	jako	k8xC	jako
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
po	po	k7c6	po
r.	r.	kA	r.
1958	[number]	k4	1958
jako	jako	k8xC	jako
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
prof.	prof.	kA	prof.
Wichterle	Wichterle	k1gFnSc2	Wichterle
jako	jako	k8xC	jako
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
ČSAV	ČSAV	kA	ČSAV
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
tržního	tržní	k2eAgNnSc2d1	tržní
zneužívání	zneužívání	k1gNnSc2	zneužívání
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
vynálezem	vynález	k1gInSc7	vynález
Otty	Otta	k1gMnSc2	Otta
Wichterla	Wichterl	k1gMnSc2	Wichterl
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
gelové	gelový	k2eAgFnPc1d1	gelová
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
měkkých	měkký	k2eAgFnPc2d1	měkká
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
náhodný	náhodný	k2eAgInSc1d1	náhodný
rozhovor	rozhovor	k1gInSc1	rozhovor
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Spolucestující	spolucestující	k1gMnSc1	spolucestující
(	(	kIx(	(
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Pur	Pur	k?	Pur
<g/>
)	)	kIx)	)
četl	číst	k5eAaImAgMnS	číst
odborný	odborný	k2eAgInSc4d1	odborný
článek	článek	k1gInSc4	článek
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
chirurgické	chirurgický	k2eAgFnSc2d1	chirurgická
náhrady	náhrada	k1gFnSc2	náhrada
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lepším	dobrý	k2eAgInSc7d2	lepší
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
implantát	implantát	k1gInSc4	implantát
než	než	k8xS	než
ušlechtilé	ušlechtilý	k2eAgInPc1d1	ušlechtilý
kovy	kov	k1gInPc1	kov
by	by	kYmCp3nP	by
byla	být	k5eAaImAgFnS	být
umělá	umělý	k2eAgFnSc1d1	umělá
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
tedy	tedy	k9	tedy
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
hydrofilním	hydrofilní	k2eAgInSc6d1	hydrofilní
polymeru	polymer	k1gInSc6	polymer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
dobře	dobře	k6eAd1	dobře
snesitelný	snesitelný	k2eAgMnSc1d1	snesitelný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvhodnější	vhodný	k2eAgMnSc1d3	nejvhodnější
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgMnS	jevit
HEMA	HEMA	kA	HEMA
gel	gel	k1gInSc1	gel
(	(	kIx(	(
<g/>
poly-hydroxyethyl-methakrylátový	polyydroxyethylethakrylátový	k2eAgInSc1d1	poly-hydroxyethyl-methakrylátový
gel	gel	k1gInSc1	gel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pohlcoval	pohlcovat	k5eAaImAgInS	pohlcovat
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
průhledný	průhledný	k2eAgMnSc1d1	průhledný
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
dobré	dobrý	k2eAgFnPc4d1	dobrá
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc4	problém
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
gel	gel	k1gInSc1	gel
lil	lít	k5eAaImAgInS	lít
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
čočky	čočka	k1gFnSc2	čočka
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
otevírání	otevírání	k1gNnSc6	otevírání
forem	forma	k1gFnPc2	forma
trhaly	trhat	k5eAaImAgInP	trhat
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
okraje	okraj	k1gInPc1	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgInS	přijít
rok	rok	k1gInSc1	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
vyhazov	vyhazov	k1gInSc4	vyhazov
z	z	k7c2	z
VŠCHT	VŠCHT	kA	VŠCHT
a	a	k8xC	a
likvidace	likvidace	k1gFnSc1	likvidace
tamějšího	tamější	k2eAgInSc2d1	tamější
výzkumu	výzkum	k1gInSc2	výzkum
hydrofilních	hydrofilní	k2eAgInPc2d1	hydrofilní
gelů	gel	k1gInPc2	gel
a	a	k8xC	a
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Wichterlem	Wichterl	k1gInSc7	Wichterl
pak	pak	k6eAd1	pak
do	do	k7c2	do
ÚMCH	ÚMCH	kA	ÚMCH
odešlo	odejít	k5eAaPmAgNnS	odejít
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Drahoslav	Drahoslava	k1gFnPc2	Drahoslava
Lím	Lím	k1gMnSc1	Lím
<g/>
,	,	kIx,	,
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
ÚMCH	ÚMCH	kA	ÚMCH
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
ústavu	ústav	k1gInSc2	ústav
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
metoda	metoda	k1gFnSc1	metoda
–	–	k?	–
odlévání	odlévání	k1gNnSc1	odlévání
gelu	gel	k1gInSc2	gel
do	do	k7c2	do
skleněných	skleněný	k2eAgFnPc2d1	skleněná
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
se	se	k3xPyFc4	se
tak	tak	k9	tak
čočka	čočka	k1gFnSc1	čočka
s	s	k7c7	s
přesnou	přesný	k2eAgFnSc7d1	přesná
optikou	optika	k1gFnSc7	optika
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
čočky	čočka	k1gFnPc1	čočka
oko	oko	k1gNnSc4	oko
téměř	téměř	k6eAd1	téměř
nedráždily	dráždit	k5eNaImAgFnP	dráždit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výtěžky	výtěžek	k1gInPc1	výtěžek
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
malé	malý	k2eAgInPc1d1	malý
kvůli	kvůli	k7c3	kvůli
složitému	složitý	k2eAgNnSc3d1	složité
obrušování	obrušování	k1gNnSc3	obrušování
okrajů	okraj	k1gInPc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
čočky	čočka	k1gFnPc1	čočka
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
aplikovány	aplikovat	k5eAaBmNgFnP	aplikovat
i	i	k9	i
na	na	k7c6	na
pacientech	pacient	k1gMnPc6	pacient
2	[number]	k4	2
<g/>
.	.	kIx.	.
oční	oční	k2eAgFnSc2d1	oční
kliniky	klinika	k1gFnSc2	klinika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Dreifusem	Dreifus	k1gMnSc7	Dreifus
<g/>
.	.	kIx.	.
</s>
<s>
Prokázalo	prokázat	k5eAaPmAgNnS	prokázat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
měkké	měkký	k2eAgFnSc2d1	měkká
čočky	čočka	k1gFnSc2	čočka
mohou	moct	k5eAaImIp3nP	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
korekci	korekce	k1gFnSc4	korekce
zraku	zrak	k1gInSc2	zrak
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pacienty	pacient	k1gMnPc4	pacient
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
snášeny	snášen	k2eAgFnPc1d1	snášena
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
však	však	k9	však
výzkum	výzkum	k1gInSc4	výzkum
i	i	k8xC	i
přes	přes	k7c4	přes
Wichterlovo	Wichterlův	k2eAgNnSc4d1	Wichterlovo
naléhání	naléhání	k1gNnSc4	naléhání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
kvůli	kvůli	k7c3	kvůli
malým	malý	k2eAgInPc3d1	malý
výtěžkům	výtěžek	k1gInPc3	výtěžek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Wichterla	Wichterlo	k1gNnSc2	Wichterlo
napadl	napadnout	k5eAaPmAgInS	napadnout
úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
zpracování	zpracování	k1gNnSc2	zpracování
HEMA	HEMA	kA	HEMA
gelu	gel	k1gInSc2	gel
<g/>
,	,	kIx,	,
odlévání	odlévání	k1gNnSc1	odlévání
v	v	k7c6	v
rotujících	rotující	k2eAgFnPc6d1	rotující
otevřených	otevřený	k2eAgFnPc6d1	otevřená
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
možnost	možnost	k1gFnSc1	možnost
pokračování	pokračování	k1gNnPc4	pokračování
v	v	k7c6	v
realizačním	realizační	k2eAgInSc6d1	realizační
vývoji	vývoj	k1gInSc6	vývoj
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
Wichterle	Wichterle	k1gFnSc2	Wichterle
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
zabývat	zabývat	k5eAaImF	zabývat
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
podařilo	podařit	k5eAaPmAgNnS	podařit
podat	podat	k5eAaPmF	podat
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
vyrobit	vyrobit	k5eAaPmF	vyrobit
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
náklady	náklad	k1gInPc7	náklad
metodou	metoda	k1gFnSc7	metoda
monomerního	monomerní	k2eAgNnSc2d1	monomerní
odstředivého	odstředivý	k2eAgNnSc2d1	odstředivé
odlévání	odlévání	k1gNnSc2	odlévání
v	v	k7c6	v
rotujících	rotující	k2eAgFnPc6d1	rotující
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
první	první	k4xOgInSc1	první
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
aparaturu	aparatura	k1gFnSc4	aparatura
sestavenou	sestavený	k2eAgFnSc4d1	sestavená
z	z	k7c2	z
dětské	dětský	k2eAgFnSc2d1	dětská
stavebnice	stavebnice	k1gFnSc2	stavebnice
Merkur	Merkur	k1gInSc1	Merkur
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
dynamem	dynamo	k1gNnSc7	dynamo
z	z	k7c2	z
jízdního	jízdní	k2eAgNnSc2d1	jízdní
kola	kolo	k1gNnSc2	kolo
jako	jako	k8xS	jako
motorkem	motorek	k1gInSc7	motorek
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
s	s	k7c7	s
motorkem	motorek	k1gInSc7	motorek
z	z	k7c2	z
gramofonu	gramofon	k1gInSc2	gramofon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
zkoušce	zkouška	k1gFnSc6	zkouška
těchto	tento	k3xDgFnPc2	tento
čoček	čočka	k1gFnPc2	čočka
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
u	u	k7c2	u
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dreifuse	Dreifuse	k1gFnSc1	Dreifuse
<g/>
)	)	kIx)	)
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Wichterleho	Wichterle	k1gMnSc2	Wichterle
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
službu	služba	k1gFnSc4	služba
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
čoček	čočka	k1gFnPc2	čočka
souvisí	souviset	k5eAaImIp3nP	souviset
velké	velký	k2eAgInPc4d1	velký
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
zájmy	zájem	k1gInPc4	zájem
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
závěrům	závěr	k1gInPc3	závěr
došly	dojít	k5eAaPmAgFnP	dojít
i	i	k9	i
státní	státní	k2eAgFnPc1d1	státní
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
Wichterle	Wichterle	k1gInSc4	Wichterle
vybízen	vybízen	k2eAgInSc4d1	vybízen
k	k	k7c3	k
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
výzkumu	výzkum	k1gInSc2	výzkum
čoček	čočka	k1gFnPc2	čočka
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
ÚMCH	ÚMCH	kA	ÚMCH
<g/>
.	.	kIx.	.
</s>
<s>
Vymohl	vymoct	k5eAaPmAgMnS	vymoct
si	se	k3xPyFc3	se
tedy	tedy	k8xC	tedy
čtyřicet	čtyřicet	k4xCc4	čtyřicet
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
působiště	působiště	k1gNnSc2	působiště
mimo	mimo	k7c4	mimo
ústav	ústav	k1gInSc4	ústav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
nepatří	patřit	k5eNaImIp3nS	patřit
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
metody	metoda	k1gFnPc1	metoda
výroby	výroba	k1gFnSc2	výroba
dovedeny	doveden	k2eAgFnPc1d1	dovedena
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
a	a	k8xC	a
také	také	k9	také
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c4	v
ně	on	k3xPp3gNnSc4	on
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Dostavila	dostavit	k5eAaPmAgFnS	dostavit
se	se	k3xPyFc4	se
i	i	k9	i
odezva	odezva	k1gFnSc1	odezva
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
jednání	jednání	k1gNnSc2	jednání
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1965	[number]	k4	1965
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
podepsána	podepsán	k2eAgFnSc1d1	podepsána
licenční	licenční	k2eAgFnSc1d1	licenční
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Morrisonem	Morrison	k1gMnSc7	Morrison
a	a	k8xC	a
National	National	k1gFnSc1	National
Patent	patent	k1gInSc1	patent
Development	Development	k1gMnSc1	Development
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
NPDC	NPDC	kA	NPDC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastupovanou	zastupovaný	k2eAgFnSc4d1	zastupovaná
Martinem	Martin	k1gInSc7	Martin
Pollakem	Pollak	k1gInSc7	Pollak
a	a	k8xC	a
Jeromem	Jerom	k1gMnSc7	Jerom
Feldmanem	Feldman	k1gMnSc7	Feldman
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
založili	založit	k5eAaPmAgMnP	založit
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
Flexible	Flexible	k1gFnSc2	Flexible
Contact	Contact	k2eAgInSc1d1	Contact
Lens	Lens	k1gInSc1	Lens
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
sublicenčním	sublicenční	k2eAgMnSc7d1	sublicenční
partnerem	partner	k1gMnSc7	partner
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
stala	stát	k5eAaPmAgFnS	stát
firma	firma	k1gFnSc1	firma
Bausch	Bausch	k1gMnSc1	Bausch
&	&	k?	&
Lomb	Lomb	k1gMnSc1	Lomb
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
výrobců	výrobce	k1gMnPc2	výrobce
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1971	[number]	k4	1971
Food	Food	k1gMnSc1	Food
and	and	k?	and
Drug	Drug	k1gInSc1	Drug
Administration	Administration	k1gInSc1	Administration
vydala	vydat	k5eAaPmAgFnS	vydat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
čoček	čočka	k1gFnPc2	čočka
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
akcie	akcie	k1gFnPc4	akcie
firmy	firma	k1gFnSc2	firma
B	B	kA	B
&	&	k?	&
L	L	kA	L
stouply	stoupnout	k5eAaPmAgFnP	stoupnout
tak	tak	k9	tak
prudce	prudko	k6eAd1	prudko
<g/>
,	,	kIx,	,
že	že	k8xS	že
akciový	akciový	k2eAgInSc1d1	akciový
kapitál	kapitál	k1gInSc1	kapitál
firmy	firma	k1gFnSc2	firma
narostl	narůst	k5eAaPmAgInS	narůst
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
o	o	k7c4	o
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnSc3	událost
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
výrazně	výrazně	k6eAd1	výrazně
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
také	také	k9	také
Wichterlův	Wichterlův	k2eAgInSc4d1	Wichterlův
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
pracovníci	pracovník	k1gMnPc1	pracovník
Akademie	akademie	k1gFnSc2	akademie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začali	začít	k5eAaPmAgMnP	začít
scházet	scházet	k5eAaImF	scházet
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
Svaz	svaz	k1gInSc1	svaz
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
Wichterle	Wichterl	k1gMnSc2	Wichterl
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
přípravného	přípravný	k2eAgInSc2d1	přípravný
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wichterle	Wichterle	k6eAd1	Wichterle
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
iniciátorů	iniciátor	k1gMnPc2	iniciátor
manifestu	manifest	k1gInSc2	manifest
2	[number]	k4	2
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Akční	akční	k2eAgInSc1d1	akční
program	program	k1gInSc1	program
KSČ	KSČ	kA	KSČ
byl	být	k5eAaImAgInS	být
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
podněty	podnět	k1gInPc1	podnět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
šly	jít	k5eAaImAgInP	jít
za	za	k7c4	za
rámec	rámec	k1gInSc4	rámec
Akčního	akční	k2eAgInSc2d1	akční
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
aktivizaci	aktivizace	k1gFnSc4	aktivizace
československé	československý	k2eAgFnSc2d1	Československá
veřejnosti	veřejnost	k1gFnSc2	veřejnost
proti	proti	k7c3	proti
stále	stále	k6eAd1	stále
zjevnému	zjevný	k2eAgInSc3d1	zjevný
tlaku	tlak	k1gInSc3	tlak
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vedení	vedení	k1gNnSc2	vedení
proti	proti	k7c3	proti
reformním	reformní	k2eAgFnPc3d1	reformní
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
než	než	k8xS	než
jako	jako	k8xC	jako
signatář	signatář	k1gMnSc1	signatář
2	[number]	k4	2
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
jako	jako	k8xS	jako
poslanec	poslanec	k1gMnSc1	poslanec
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
téměř	téměř	k6eAd1	téměř
jednohlasně	jednohlasně	k6eAd1	jednohlasně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
federalizace	federalizace	k1gFnSc2	federalizace
Československa	Československo	k1gNnSc2	Československo
usedl	usednout	k5eAaPmAgMnS	usednout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
jako	jako	k8xS	jako
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
federálního	federální	k2eAgInSc2d1	federální
parlamentu	parlament	k1gInSc2	parlament
ho	on	k3xPp3gInSc4	on
nominovala	nominovat	k5eAaBmAgFnS	nominovat
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
FS	FS	kA	FS
i	i	k8xC	i
ČNR	ČNR	kA	ČNR
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
odešel	odejít	k5eAaPmAgMnS	odejít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
prodlužováním	prodlužování	k1gNnSc7	prodlužování
mandátu	mandát	k1gInSc2	mandát
(	(	kIx(	(
<g/>
poslanci	poslanec	k1gMnPc1	poslanec
si	se	k3xPyFc3	se
odhlasovali	odhlasovat	k5eAaPmAgMnP	odhlasovat
prodloužení	prodloužení	k1gNnSc3	prodloužení
vlastních	vlastní	k2eAgMnPc2d1	vlastní
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
také	také	k9	také
odmítal	odmítat	k5eAaImAgMnS	odmítat
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
plat	plat	k1gInSc4	plat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
vědcem	vědec	k1gMnSc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1969	[number]	k4	1969
byl	být	k5eAaImAgInS	být
Wichterle	Wichterle	k1gInSc1	Wichterle
jako	jako	k8xS	jako
signatář	signatář	k1gMnSc1	signatář
2	[number]	k4	2
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
zbaven	zbaven	k2eAgMnSc1d1	zbaven
funkce	funkce	k1gFnPc1	funkce
ředitele	ředitel	k1gMnSc2	ředitel
ÚMCH	ÚMCH	kA	ÚMCH
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
tam	tam	k6eAd1	tam
pracoval	pracovat	k5eAaImAgMnS	pracovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
řadový	řadový	k2eAgMnSc1d1	řadový
vědec	vědec	k1gMnSc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgInS	mít
Wichterle	Wichterle	k1gInSc1	Wichterle
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
vynálezy	vynález	k1gInPc4	vynález
spolehlivě	spolehlivě	k6eAd1	spolehlivě
zajištěny	zajištěn	k2eAgInPc4d1	zajištěn
patenty	patent	k1gInPc4	patent
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
firem	firma	k1gFnPc2	firma
napříč	napříč	k7c7	napříč
Amerikou	Amerika	k1gFnSc7	Amerika
tyto	tento	k3xDgInPc4	tento
patenty	patent	k1gInPc4	patent
vědomě	vědomě	k6eAd1	vědomě
porušovalo	porušovat	k5eAaImAgNnS	porušovat
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
čoček	čočka	k1gFnPc2	čočka
soustružením	soustružení	k1gNnPc3	soustružení
z	z	k7c2	z
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
xerogelu	xerogel	k1gInSc2	xerogel
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
bobtnání	bobtnání	k1gNnSc4	bobtnání
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
NPDC	NPDC	kA	NPDC
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
vést	vést	k5eAaImF	vést
mnoho	mnoho	k4c4	mnoho
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
proti	proti	k7c3	proti
velkým	velký	k2eAgFnPc3d1	velká
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patenty	patent	k1gInPc1	patent
porušovaly	porušovat	k5eAaImAgInP	porušovat
<g/>
.	.	kIx.	.
</s>
<s>
Žalované	žalovaný	k2eAgFnPc1d1	žalovaná
firmy	firma	k1gFnPc1	firma
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
podávaly	podávat	k5eAaImAgFnP	podávat
zrušovací	zrušovací	k2eAgFnPc1d1	zrušovací
žaloby	žaloba	k1gFnPc1	žaloba
proti	proti	k7c3	proti
platnosti	platnost	k1gFnSc3	platnost
daných	daný	k2eAgInPc2d1	daný
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
jediná	jediný	k2eAgFnSc1d1	jediná
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uniknout	uniknout	k5eAaPmF	uniknout
těžkému	těžký	k2eAgInSc3d1	těžký
finančnímu	finanční	k2eAgInSc3d1	finanční
postihu	postih	k1gInSc3	postih
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
svědkem	svědek	k1gMnSc7	svědek
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
přirozeně	přirozeně	k6eAd1	přirozeně
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
byla	být	k5eAaImAgFnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nutná	nutný	k2eAgFnSc1d1	nutná
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Wichterlově	Wichterlův	k2eAgFnSc3d1	Wichterlova
přítomnosti	přítomnost	k1gFnSc3	přítomnost
na	na	k7c6	na
černé	černý	k2eAgFnSc6d1	černá
listině	listina	k1gFnSc6	listina
české	český	k2eAgFnSc2d1	Česká
komunistické	komunistický	k2eAgFnSc2d1	komunistická
vlády	vláda	k1gFnSc2	vláda
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
vládě	vláda	k1gFnSc3	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k8xC	i
ona	onen	k3xDgFnSc1	onen
by	by	kYmCp3nS	by
prohrou	prohra	k1gFnSc7	prohra
v	v	k7c6	v
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
patenty	patent	k1gInPc4	patent
ztratila	ztratit	k5eAaPmAgFnS	ztratit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Wichterlovi	Wichterlův	k2eAgMnPc1d1	Wichterlův
konečně	konečně	k6eAd1	konečně
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
vycestovat	vycestovat	k5eAaPmF	vycestovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
federálního	federální	k2eAgInSc2d1	federální
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
až	až	k6eAd1	až
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
dostal	dostat	k5eAaPmAgInS	dostat
<g/>
,	,	kIx,	,
Wichterle	Wichterle	k1gFnPc1	Wichterle
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k4c4	málo
dní	den	k1gInPc2	den
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
veškeré	veškerý	k3xTgFnPc4	veškerý
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
pravosti	pravost	k1gFnSc6	pravost
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1982	[number]	k4	1982
vydal	vydat	k5eAaPmAgInS	vydat
federální	federální	k2eAgInSc1d1	federální
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
instance	instance	k1gFnSc2	instance
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
jejímuž	jejíž	k3xOyRp3gNnSc3	jejíž
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
nebylo	být	k5eNaImAgNnS	být
odvolání	odvolání	k1gNnSc4	odvolání
<g/>
,	,	kIx,	,
rozsudek	rozsudek	k1gInSc4	rozsudek
potvrzující	potvrzující	k2eAgFnSc1d1	potvrzující
platnost	platnost	k1gFnSc1	platnost
Wichterlových	Wichterlův	k2eAgInPc2d1	Wichterlův
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
NPDC	NPDC	kA	NPDC
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
inkasovala	inkasovat	k5eAaBmAgFnS	inkasovat
desítky	desítka	k1gFnPc4	desítka
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nemusela	muset	k5eNaImAgFnS	muset
dělit	dělit	k5eAaImF	dělit
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
Akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čeští	český	k2eAgMnPc1d1	český
mocipáni	mocipán	k1gMnPc1	mocipán
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
vzdali	vzdát	k5eAaPmAgMnP	vzdát
veškerých	veškerý	k3xTgFnPc2	veškerý
licenčních	licenční	k2eAgFnPc2d1	licenční
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
tím	ten	k3xDgNnSc7	ten
zrušit	zrušit	k5eAaPmF	zrušit
účast	účast	k1gFnSc4	účast
Akademie	akademie	k1gFnSc2	akademie
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
sporu	spor	k1gInSc6	spor
a	a	k8xC	a
sprovodit	sprovodit	k5eAaPmF	sprovodit
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
všechny	všechen	k3xTgInPc4	všechen
úspěchy	úspěch	k1gInPc4	úspěch
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterl	k1gMnSc4	Wichterl
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
hospodářství	hospodářství	k1gNnSc1	hospodářství
tak	tak	k6eAd1	tak
přišlo	přijít	k5eAaPmAgNnS	přijít
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
miliardu	miliarda	k4xCgFnSc4	miliarda
devizových	devizový	k2eAgInPc2d1	devizový
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
opadat	opadat	k5eAaBmF	opadat
normalizační	normalizační	k2eAgFnSc4d1	normalizační
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
Wichterlovi	Wichterl	k1gMnSc3	Wichterl
a	a	k8xC	a
opět	opět	k6eAd1	opět
mohl	moct	k5eAaImAgInS	moct
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
cesty	cesta	k1gFnPc4	cesta
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužená	zasloužený	k2eAgFnSc1d1	zasloužená
úcta	úcta	k1gFnSc1	úcta
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
plně	plně	k6eAd1	plně
projevována	projevovat	k5eAaImNgFnS	projevovat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
dvěma	dva	k4xCgFnPc7	dva
americkými	americký	k2eAgFnPc7d1	americká
univeristami	univerista	k1gFnPc7	univerista
<g/>
,	,	kIx,	,
Univeristy	Univerista	k1gMnPc4	Univerista
of	of	k?	of
Illinois	Illinois	k1gFnSc1	Illinois
at	at	k?	at
Chicago	Chicago	k1gNnSc1	Chicago
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
a	a	k8xC	a
Polytechnic	Polytechnice	k1gFnPc2	Polytechnice
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
v	v	k7c4	v
1993	[number]	k4	1993
Univerzitou	univerzita	k1gFnSc7	univerzita
Karlovou	Karlův	k2eAgFnSc7d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
presidentem	president	k1gMnSc7	president
ČSAV	ČSAV	kA	ČSAV
a	a	k8xC	a
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
asteroid	asteroid	k1gInSc4	asteroid
Wichterle	Wichterl	k1gMnSc2	Wichterl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
být	být	k5eAaImF	být
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
chemií	chemie	k1gFnSc7	chemie
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaImF	věnovat
pracím	práce	k1gFnPc3	práce
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
rady	rada	k1gFnPc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gNnSc2	Wichterle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
letním	letní	k2eAgNnSc6d1	letní
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Stražisko	Stražisko	k1gNnSc1	Stražisko
(	(	kIx(	(
<g/>
okr	okr	k1gInSc1	okr
<g/>
.	.	kIx.	.
</s>
<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nedožitých	dožitý	k2eNgNnPc2d1	nedožité
85	[number]	k4	85
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uděluje	udělovat	k5eAaImIp3nS	udělovat
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
mladým	mladý	k1gMnPc3	mladý
<g/>
,	,	kIx,	,
perspektivním	perspektivní	k2eAgMnPc3d1	perspektivní
vědcům	vědec	k1gMnPc3	vědec
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nepřesáhli	přesáhnout	k5eNaPmAgMnP	přesáhnout
35	[number]	k4	35
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
