<s>
Stehenní	stehenní	k2eAgFnSc4d1	stehenní
kost	kost	k1gFnSc4	kost
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
femur	femura	k1gFnPc2	femura
nebo	nebo	k8xC	nebo
os	osa	k1gFnPc2	osa
femoris	femoris	k1gFnSc2	femoris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
kost	kost	k1gFnSc1	kost
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
snese	snést	k5eAaPmIp3nS	snést
velké	velký	k2eAgNnSc4d1	velké
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
párovou	párový	k2eAgFnSc4d1	párová
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
stehno	stehno	k1gNnSc1	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Nasedá	nasedat	k5eAaImIp3nS	nasedat
v	v	k7c6	v
kyčelním	kyčelní	k2eAgInSc6d1	kyčelní
kloubu	kloub	k1gInSc6	kloub
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
čep	čep	k1gInSc4	čep
kosti	kost	k1gFnSc2	kost
stehenní	stehenní	k2eAgNnSc1d1	stehenní
zapadá	zapadat	k5eAaImIp3nS	zapadat
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
kloubu	kloub	k1gInSc2	kloub
kyčelního	kyčelní	k2eAgInSc2d1	kyčelní
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kloub	kloub	k1gInSc4	kloub
kulový	kulový	k2eAgInSc4d1	kulový
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
kolenním	kolenní	k2eAgInSc7d1	kolenní
kloubem	kloub	k1gInSc7	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
každá	každý	k3xTgFnSc1	každý
kost	kost	k1gFnSc1	kost
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pojivem	pojivo	k1gNnSc7	pojivo
-	-	kIx~	-
kostní	kostní	k2eAgFnSc7d1	kostní
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
kostních	kostní	k2eAgFnPc2d1	kostní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
solí	sůl	k1gFnPc2	sůl
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
žlutou	žlutý	k2eAgFnSc7d1	žlutá
dření	dřeň	k1gFnSc7	dřeň
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
rezervou	rezerva	k1gFnSc7	rezerva
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
tělesného	tělesný	k2eAgInSc2d1	tělesný
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
žlutá	žlutý	k2eAgFnSc1d1	žlutá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
červenou	červená	k1gFnSc4	červená
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
posléze	posléze	k6eAd1	posléze
produkuje	produkovat	k5eAaImIp3nS	produkovat
erytrocyty	erytrocyt	k1gInPc4	erytrocyt
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
kosti	kost	k1gFnSc2	kost
<g/>
:	:	kIx,	:
trochanter	trochanter	k1gInSc4	trochanter
minor	minor	k2eAgInSc4d1	minor
trochanter	trochanter	k1gInSc4	trochanter
major	major	k1gMnSc1	major
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stehenní	stehenní	k2eAgFnSc1d1	stehenní
kost	kost	k1gFnSc1	kost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stehenní	stehenní	k2eAgFnSc4d1	stehenní
kost	kost	k1gFnSc4	kost
-	-	kIx~	-
popis	popis	k1gInSc4	popis
</s>
