<s>
Stehenní	stehenní	k2eAgFnSc4d1	stehenní
kost	kost	k1gFnSc4	kost
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
femur	femura	k1gFnPc2	femura
nebo	nebo	k8xC	nebo
os	osa	k1gFnPc2	osa
femoris	femoris	k1gFnSc2	femoris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
kost	kost	k1gFnSc1	kost
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
snese	snést	k5eAaPmIp3nS	snést
velké	velký	k2eAgNnSc4d1	velké
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
.	.	kIx.	.
</s>
