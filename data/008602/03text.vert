<p>
<s>
Zelenobílý	zelenobílý	k2eAgInSc1d1	zelenobílý
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
iniciativa	iniciativa	k1gFnSc1	iniciativa
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
1905	[number]	k4	1905
z	z	k7c2	z
pražských	pražský	k2eAgFnPc2d1	Pražská
Vršovic	Vršovice	k1gFnPc2	Vršovice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
iniciativy	iniciativa	k1gFnSc2	iniciativa
je	být	k5eAaImIp3nS	být
navázání	navázání	k1gNnSc4	navázání
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
fotbalovými	fotbalový	k2eAgInPc7d1	fotbalový
celky	celek	k1gInPc7	celek
se	s	k7c7	s
stejnými	stejný	k2eAgFnPc7d1	stejná
klubovými	klubový	k2eAgFnPc7d1	klubová
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
partnerem	partner	k1gMnSc7	partner
byl	být	k5eAaImAgInS	být
litevský	litevský	k2eAgInSc1d1	litevský
Žalgiris	Žalgiris	k1gInSc1	Žalgiris
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
pro	pro	k7c4	pro
jarní	jarní	k2eAgFnSc4d1	jarní
část	část	k1gFnSc4	část
sezony	sezona	k1gFnSc2	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
dva	dva	k4xCgInPc4	dva
své	svůj	k3xOyFgMnPc4	svůj
hráče	hráč	k1gMnPc4	hráč
-	-	kIx~	-
Mantase	Mantasa	k1gFnSc3	Mantasa
Kuklyse	Kuklysa	k1gFnSc3	Kuklysa
a	a	k8xC	a
Egidijuse	Egidijuse	k1gFnSc1	Egidijuse
Vaitkū	Vaitkū	k1gFnSc1	Vaitkū
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zelenobílé	zelenobílý	k2eAgInPc4d1	zelenobílý
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
==	==	k?	==
</s>
</p>
<p>
<s>
Zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc4	výběr
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc4	ukázka
dresů	dres	k1gInPc2	dres
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zelenobílé	zelenobílý	k2eAgFnSc2d1	zelenobílá
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zelenobílá	zelenobílý	k2eAgFnSc1d1	zelenobílá
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
fotbalových	fotbalový	k2eAgFnPc6d1	fotbalová
soutěžích	soutěž	k1gFnPc6	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zelenobílá	zelenobílý	k2eAgFnSc1d1	zelenobílá
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
sportech	sport	k1gInPc6	sport
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zelenobílá	zelenobílý	k2eAgFnSc1d1	zelenobílá
mimo	mimo	k7c4	mimo
sport	sport	k1gInSc4	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nigérie	Nigérie	k1gFnSc1	Nigérie
Nigérie	Nigérie	k1gFnSc1	Nigérie
-	-	kIx~	-
Nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
Česko	Česko	k1gNnSc1	Česko
-	-	kIx~	-
Strana	strana	k1gFnSc1	strana
svobodných	svobodný	k2eAgMnPc2d1	svobodný
občanů	občan	k1gMnPc2	občan
-	-	kIx~	-
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
Rakousko	Rakousko	k1gNnSc1	Rakousko
-	-	kIx~	-
Braunau	Braunaus	k1gInSc2	Braunaus
am	am	k?	am
Inn	Inn	k1gMnSc1	Inn
-	-	kIx~	-
barvy	barva	k1gFnPc1	barva
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Bohemce	Bohemka	k1gFnSc3	Bohemka
<g/>
"	"	kIx"	"
nejbližší	blízký	k2eAgInSc1d3	Nejbližší
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
irský	irský	k2eAgInSc1d1	irský
Bohemian	bohemian	k1gInSc1	bohemian
FC	FC	kA	FC
(	(	kIx(	(
<g/>
založen	založit	k5eAaPmNgInS	založit
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedenáctinásobný	jedenáctinásobný	k2eAgMnSc1d1	jedenáctinásobný
mistr	mistr	k1gMnSc1	mistr
irské	irský	k2eAgFnSc2d1	irská
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
klubové	klubový	k2eAgFnSc2d1	klubová
barvy	barva	k1gFnSc2	barva
černo-červené	černo-červený	k2eAgFnSc2d1	černo-červená
(	(	kIx(	(
<g/>
venku	venku	k6eAd1	venku
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
černo-zelených	černoelený	k2eAgInPc6d1	černo-zelený
dresech	dres	k1gInPc6	dres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Polka	Polka	k1gFnSc1	Polka
Zelenobílý	zelenobílý	k2eAgMnSc1d1	zelenobílý
</s>
</p>
