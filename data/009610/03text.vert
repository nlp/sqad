<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
myš	myš	k1gFnSc1	myš
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
polohovací	polohovací	k2eAgNnSc4d1	polohovací
zařízení	zařízení	k1gNnSc4	zařízení
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
;	;	kIx,	;
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
po	po	k7c6	po
ploše	plocha	k1gFnSc6	plocha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
desce	deska	k1gFnSc6	deska
stolu	stol	k1gInSc2	stol
<g/>
)	)	kIx)	)
předává	předávat	k5eAaImIp3nS	předávat
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	on	k3xPp3gNnSc4	on
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
kurzoru	kurzor	k1gInSc2	kurzor
po	po	k7c6	po
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
jedno	jeden	k4xCgNnSc4	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
tlačítek	tlačítko	k1gNnPc2	tlačítko
sloužících	sloužící	k2eAgNnPc2d1	sloužící
ke	k	k7c3	k
klikání	klikání	k1gNnSc3	klikání
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
jedno	jeden	k4xCgNnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
koleček	koleček	k1gInSc1	koleček
používaných	používaný	k2eAgMnPc2d1	používaný
pro	pro	k7c4	pro
scrollování	scrollování	k1gNnSc4	scrollování
<g/>
,	,	kIx,	,
zoomovaní	zoomovaný	k2eAgMnPc1d1	zoomovaný
nebo	nebo	k8xC	nebo
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
myši	myš	k1gFnSc2	myš
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
snímač	snímač	k1gInSc1	snímač
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
mechanický	mechanický	k2eAgInSc4d1	mechanický
<g/>
,	,	kIx,	,
optický	optický	k2eAgInSc4d1	optický
nebo	nebo	k8xC	nebo
laserový	laserový	k2eAgInSc4d1	laserový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Myš	myš	k1gFnSc4	myš
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
Douglas	Douglas	k1gInSc1	Douglas
Engelbart	Engelbart	k1gInSc1	Engelbart
ve	v	k7c6	v
Stanfordském	Stanfordský	k2eAgInSc6d1	Stanfordský
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc3	veřejnost
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
představil	představit	k5eAaPmAgMnS	představit
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
získal	získat	k5eAaPmAgInS	získat
patent	patent	k1gInSc4	patent
US3541541	US3541541	k1gFnSc2	US3541541
na	na	k7c4	na
"	"	kIx"	"
<g/>
Indikátor	indikátor	k1gInSc4	indikátor
pozice	pozice	k1gFnSc2	pozice
X-Y	X-Y	k1gFnSc2	X-Y
pro	pro	k7c4	pro
zobrazovací	zobrazovací	k2eAgInSc4d1	zobrazovací
systém	systém	k1gInSc4	systém
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
X-Y	X-Y	k1gFnSc1	X-Y
Position	Position	k1gInSc4	Position
Indicator	Indicator	k1gInSc4	Indicator
For	forum	k1gNnPc2	forum
A	a	k8xC	a
Display	Displaa	k1gFnSc2	Displaa
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
myši	myš	k1gFnPc1	myš
používaly	používat	k5eAaImAgFnP	používat
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
pohybu	pohyb	k1gInSc2	pohyb
dvě	dva	k4xCgFnPc4	dva
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmá	kolmý	k2eAgNnPc1d1	kolmé
kolečka	kolečko	k1gNnPc1	kolečko
<g/>
;	;	kIx,	;
patent	patent	k1gInSc1	patent
na	na	k7c4	na
myš	myš	k1gFnSc4	myš
s	s	k7c7	s
kuličkou	kulička	k1gFnSc7	kulička
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
pohyb	pohyb	k1gInSc4	pohyb
snímala	snímat	k5eAaImAgFnS	snímat
4	[number]	k4	4
kolečka	kolečko	k1gNnSc2	kolečko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
snímání	snímání	k1gNnSc4	snímání
dvojicí	dvojice	k1gFnSc7	dvojice
osiček	osička	k1gFnPc2	osička
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
přítlačnou	přítlačný	k2eAgFnSc7d1	přítlačná
kladkou	kladka	k1gFnSc7	kladka
bylo	být	k5eAaImAgNnS	být
patentováno	patentovat	k5eAaBmNgNnS	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Steve	Steve	k1gMnSc1	Steve
Kirsch	kirsch	k1gInSc4	kirsch
myš	myš	k1gFnSc4	myš
s	s	k7c7	s
optickým	optický	k2eAgNnSc7d1	optické
snímáním	snímání	k1gNnSc7	snímání
pozice	pozice	k1gFnSc2	pozice
pomocí	pomocí	k7c2	pomocí
světla	světlo	k1gNnSc2	světlo
vyzařovaného	vyzařovaný	k2eAgNnSc2d1	vyzařované
dvojicí	dvojice	k1gFnSc7	dvojice
LED	LED	kA	LED
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
myši	myš	k1gFnPc1	myš
s	s	k7c7	s
optickým	optický	k2eAgNnSc7d1	optické
snímáním	snímání	k1gNnSc7	snímání
potřebovaly	potřebovat	k5eAaImAgFnP	potřebovat
speciální	speciální	k2eAgFnSc4d1	speciální
kovovou	kovový	k2eAgFnSc4d1	kovová
podložku	podložka	k1gFnSc4	podložka
s	s	k7c7	s
natištěnou	natištěný	k2eAgFnSc7d1	natištěná
mřížkou	mřížka	k1gFnSc7	mřížka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
komerčním	komerční	k2eAgNnSc6d1	komerční
prostředí	prostředí	k1gNnSc6	prostředí
myš	myš	k1gFnSc1	myš
prosadila	prosadit	k5eAaPmAgFnS	prosadit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
myši	myš	k1gFnPc1	myš
byly	být	k5eAaImAgFnP	být
hranaté	hranatý	k2eAgFnPc1d1	hranatá
a	a	k8xC	a
nepohodlné	pohodlný	k2eNgFnPc1d1	nepohodlná
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
novější	nový	k2eAgInPc1d2	novější
jsou	být	k5eAaImIp3nP	být
ergonomicky	ergonomicky	k6eAd1	ergonomicky
tvarovány	tvarován	k2eAgInPc1d1	tvarován
pro	pro	k7c4	pro
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
držení	držení	k1gNnSc4	držení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
obrana	obrana	k1gFnSc1	obrana
před	před	k7c7	před
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
při	při	k7c6	při
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
používání	používání	k1gNnSc6	používání
myši	myš	k1gFnSc2	myš
hrozí	hrozit	k5eAaImIp3nS	hrozit
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
RSI	RSI	kA	RSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zánět	zánět	k1gInSc4	zánět
šlach	šlacha	k1gFnPc2	šlacha
-	-	kIx~	-
tenditida	tenditida	k1gFnSc1	tenditida
či	či	k8xC	či
tendosynovitida	tendosynovitida	k1gFnSc1	tendosynovitida
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
prevence	prevence	k1gFnSc1	prevence
proti	proti	k7c3	proti
těmto	tento	k3xDgNnPc3	tento
zdravotním	zdravotní	k2eAgNnPc3d1	zdravotní
rizikům	riziko	k1gNnPc3	riziko
nabízejí	nabízet	k5eAaImIp3nP	nabízet
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
i	i	k8xC	i
myši	myš	k1gFnPc1	myš
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
designem	design	k1gInSc7	design
<g/>
,	,	kIx,	,
označovány	označovat	k5eAaImNgInP	označovat
bývají	bývat	k5eAaImIp3nP	bývat
jako	jako	k9	jako
ergonomické	ergonomický	k2eAgFnPc1d1	ergonomická
nebo	nebo	k8xC	nebo
vertikální	vertikální	k2eAgFnPc1d1	vertikální
myši	myš	k1gFnPc1	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
počítačových	počítačový	k2eAgFnPc2d1	počítačová
myší	myš	k1gFnPc2	myš
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
myš	myš	k1gFnSc1	myš
===	===	k?	===
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
English	English	k1gMnSc1	English
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
Engelbartovy	Engelbartův	k2eAgFnSc2d1	Engelbartův
původní	původní	k2eAgFnSc2d1	původní
myši	myš	k1gFnSc2	myš
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
tak	tak	k6eAd1	tak
zvanou	zvaný	k2eAgFnSc4d1	zvaná
kuličkovou	kuličkový	k2eAgFnSc4d1	kuličková
myš	myš	k1gFnSc4	myš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
ve	v	k7c6	v
vývojovém	vývojový	k2eAgNnSc6d1	vývojové
centru	centrum	k1gNnSc6	centrum
Xerox	Xerox	kA	Xerox
PARC	PARC	kA	PARC
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
počítače	počítač	k1gInSc2	počítač
Xerox	Xerox	kA	Xerox
Alto	Alto	k6eAd1	Alto
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
prostředí	prostředí	k1gNnSc2	prostředí
WIMP	WIMP	kA	WIMP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolečka	kolečko	k1gNnSc2	kolečko
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kuličkou	kulička	k1gFnSc7	kulička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pohyb	pohyb	k1gInSc4	pohyb
myši	myš	k1gFnSc2	myš
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc4	pohyb
kuličky	kulička	k1gFnSc2	kulička
snímají	snímat	k5eAaImIp3nP	snímat
dvě	dva	k4xCgFnPc1	dva
navzájem	navzájem	k6eAd1	navzájem
kolmé	kolmý	k2eAgFnPc1d1	kolmá
hřídele	hřídel	k1gFnPc1	hřídel
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
kuličky	kulička	k1gFnPc1	kulička
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Kulička	kulička	k1gFnSc1	kulička
obě	dva	k4xCgFnPc1	dva
hřídele	hřídel	k1gFnPc1	hřídel
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
roztáčí	roztáčet	k5eAaImIp3nS	roztáčet
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
pohyb	pohyb	k1gInSc1	pohyb
na	na	k7c4	na
otočnou	otočný	k2eAgFnSc4d1	otočná
clonku	clonka	k1gFnSc4	clonka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kruhu	kruh	k1gInSc2	kruh
s	s	k7c7	s
okénky	okénko	k1gNnPc7	okénko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
hřídelích	hřídel	k1gInPc6	hřídel
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
clonce	clonka	k1gFnSc6	clonka
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
senzoru	senzor	k1gInSc2	senzor
prosvěcuje	prosvěcovat	k5eAaImIp3nS	prosvěcovat
clonku	clonka	k1gFnSc4	clonka
a	a	k8xC	a
přerušovaný	přerušovaný	k2eAgInSc1d1	přerušovaný
paprsek	paprsek	k1gInSc1	paprsek
je	být	k5eAaImIp3nS	být
snímán	snímán	k2eAgInSc1d1	snímán
optoelektronickým	optoelektronický	k2eAgNnSc7d1	optoelektronické
čidlem	čidlo	k1gNnSc7	čidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jej	on	k3xPp3gMnSc4	on
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
impulzy	impulz	k1gInPc4	impulz
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
otáčení	otáčení	k1gNnSc2	otáčení
je	být	k5eAaImIp3nS	být
rozpoznán	rozpoznat	k5eAaPmNgInS	rozpoznat
pomocí	pomocí	k7c2	pomocí
Grayova	Grayův	k2eAgInSc2d1	Grayův
kódu	kód	k1gInSc2	kód
<g/>
:	:	kIx,	:
Myš	myš	k1gFnSc1	myš
totiž	totiž	k9	totiž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
clonce	clonka	k1gFnSc6	clonka
dva	dva	k4xCgInPc4	dva
snímače	snímač	k1gInPc4	snímač
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gInPc1	jejich
pulzy	pulz	k1gInPc1	pulz
byly	být	k5eAaImAgInP	být
úhlově	úhlově	k6eAd1	úhlově
posunuty	posunut	k2eAgInPc1d1	posunut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
okamžik	okamžik	k1gInSc4	okamžik
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
osvětlen	osvětlit	k5eAaPmNgInS	osvětlit
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
žádný	žádný	k3yNgInSc4	žádný
snímač	snímač	k1gInSc4	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Impulzy	impulz	k1gInPc1	impulz
pohybu	pohyb	k1gInSc2	pohyb
celé	celý	k2eAgFnSc2d1	celá
myši	myš	k1gFnSc2	myš
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
sledy	sled	k1gInPc4	sled
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
detekovány	detekovat	k5eAaImNgInP	detekovat
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Ovladač	ovladač	k1gInSc1	ovladač
myši	myš	k1gFnSc2	myš
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
signály	signál	k1gInPc4	signál
dekóduje	dekódovat	k5eAaBmIp3nS	dekódovat
a	a	k8xC	a
načítává	načítávat	k5eAaImIp3nS	načítávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
převádí	převádět	k5eAaImIp3nS	převádět
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
kurzoru	kurzor	k1gInSc2	kurzor
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
monitoru	monitor	k1gInSc2	monitor
(	(	kIx(	(
<g/>
v	v	k7c6	v
osách	osa	k1gFnPc6	osa
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
tak	tak	k9	tak
rychlost	rychlost	k1gFnSc4	rychlost
i	i	k8xC	i
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdokonalené	zdokonalený	k2eAgFnPc1d1	zdokonalená
počítačové	počítačový	k2eAgFnPc1d1	počítačová
myši	myš	k1gFnPc1	myš
s	s	k7c7	s
kuličkou	kulička	k1gFnSc7	kulička
pocházely	pocházet	k5eAaImAgFnP	pocházet
ze	z	k7c2	z
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
École	École	k1gFnSc1	École
polytechnique	polytechniquat	k5eAaPmIp3nS	polytechniquat
fédérale	fédérale	k6eAd1	fédérale
de	de	k?	de
Lausanne	Lausanne	k1gNnSc1	Lausanne
<g/>
,	,	kIx,	,
EPFL	EPFL	kA	EPFL
<g/>
)	)	kIx)	)
z	z	k7c2	z
inspirace	inspirace	k1gFnSc2	inspirace
profesora	profesor	k1gMnSc4	profesor
Jean-Daniel	Jean-Daniel	k1gMnSc1	Jean-Daniel
Nicouda	Nicouda	k1gMnSc1	Nicouda
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
je	být	k5eAaImIp3nS	být
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
hodinář	hodinář	k1gMnSc1	hodinář
André	André	k1gMnSc1	André
Guignard	Guignard	k1gMnSc1	Guignard
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
design	design	k1gInSc1	design
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
jednu	jeden	k4xCgFnSc4	jeden
kuličku	kulička	k1gFnSc4	kulička
z	z	k7c2	z
hrubé	hrubý	k2eAgFnSc2d1	hrubá
gumy	guma	k1gFnSc2	guma
a	a	k8xC	a
tři	tři	k4xCgNnPc4	tři
tlačítka	tlačítko	k1gNnPc4	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgNnSc1d1	prostřední
tlačítko	tlačítko	k1gNnSc1	tlačítko
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
rolovacím	rolovací	k2eAgNnSc7d1	rolovací
kolečkem	kolečko	k1gNnSc7	kolečko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
a	a	k8xC	a
optomechanická	optomechanický	k2eAgFnSc1d1	optomechanický
myš	myš	k1gFnSc1	myš
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
i	i	k9	i
čistě	čistě	k6eAd1	čistě
mechanické	mechanický	k2eAgFnPc1d1	mechanická
myši	myš	k1gFnPc1	myš
využívající	využívající	k2eAgFnSc4d1	využívající
kuličku	kulička	k1gFnSc4	kulička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
otáčela	otáčet	k5eAaImAgFnS	otáčet
kruhem	kruh	k1gInSc7	kruh
s	s	k7c7	s
kontakty	kontakt	k1gInPc7	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgNnSc1d1	mechanické
snímání	snímání	k1gNnSc1	snímání
pohybu	pohyb	k1gInSc2	pohyb
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
bezkontaktním	bezkontaktní	k2eAgNnSc7d1	bezkontaktní
řešením	řešení	k1gNnSc7	řešení
založeným	založený	k2eAgNnSc7d1	založené
na	na	k7c6	na
výše	vysoce	k6eAd2	vysoce
popsaném	popsaný	k2eAgNnSc6d1	popsané
optickém	optický	k2eAgNnSc6d1	optické
snímání	snímání	k1gNnSc6	snímání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
přesnější	přesný	k2eAgMnSc1d2	přesnější
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgMnSc1d2	spolehlivější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Optická	optický	k2eAgFnSc1d1	optická
myš	myš	k1gFnSc1	myš
===	===	k?	===
</s>
</p>
<p>
<s>
Optická	optický	k2eAgFnSc1d1	optická
myš	myš	k1gFnSc1	myš
využívá	využívat	k5eAaImIp3nS	využívat
LED	led	k1gInSc4	led
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
snímáno	snímat	k5eAaImNgNnS	snímat
fotodiodami	fotodioda	k1gFnPc7	fotodioda
nebo	nebo	k8xC	nebo
dokonalejším	dokonalý	k2eAgInSc7d2	dokonalejší
optickým	optický	k2eAgInSc7d1	optický
snímačem	snímač	k1gInSc7	snímač
(	(	kIx(	(
<g/>
CCD	CCD	kA	CCD
či	či	k8xC	či
CMOS	CMOS	kA	CMOS
prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
maticí	matice	k1gFnSc7	matice
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
optické	optický	k2eAgFnPc1d1	optická
myši	myš	k1gFnPc1	myš
využívaly	využívat	k5eAaImAgFnP	využívat
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
pohybu	pohyb	k1gInSc2	pohyb
speciálně	speciálně	k6eAd1	speciálně
potištěný	potištěný	k2eAgInSc4d1	potištěný
podklad	podklad	k1gInSc4	podklad
(	(	kIx(	(
<g/>
podložku	podložka	k1gFnSc4	podložka
pod	pod	k7c4	pod
myš	myš	k1gFnSc4	myš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
optické	optický	k2eAgFnPc1d1	optická
myši	myš	k1gFnPc1	myš
periodicky	periodicky	k6eAd1	periodicky
snímají	snímat	k5eAaImIp3nP	snímat
obraz	obraz	k1gInSc4	obraz
podkladu	podklad	k1gInSc2	podklad
osvětlený	osvětlený	k2eAgInSc4d1	osvětlený
pomocí	pomoc	k1gFnSc7	pomoc
LED	LED	kA	LED
nebo	nebo	k8xC	nebo
laserové	laserový	k2eAgFnPc1d1	laserová
diody	dioda	k1gFnPc1	dioda
a	a	k8xC	a
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
posuv	posuv	k1gInSc4	posuv
obrazu	obraz	k1gInSc2	obraz
vůči	vůči	k7c3	vůči
předchozímu	předchozí	k2eAgInSc3d1	předchozí
snímku	snímek	k1gInSc3	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
speciální	speciální	k2eAgInPc1d1	speciální
čipy	čip	k1gInPc1	čip
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
obrazu	obraz	k1gInSc2	obraz
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
a	a	k8xC	a
převodu	převod	k1gInSc6	převod
pohybu	pohyb	k1gInSc2	pohyb
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
X	X	kA	X
a	a	k8xC	a
Y.	Y.	kA	Y.
Například	například	k6eAd1	například
optická	optický	k2eAgFnSc1d1	optická
myš	myš	k1gFnSc1	myš
Avago	Avago	k1gMnSc1	Avago
Technologies	Technologies	k1gMnSc1	Technologies
ADNS-2610	ADNS-2610	k1gMnSc1	ADNS-2610
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
1512	[number]	k4	1512
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
FPS	FPS	kA	FPS
<g/>
)	)	kIx)	)
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
18	[number]	k4	18
<g/>
×	×	k?	×
<g/>
18	[number]	k4	18
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgMnSc1	každý
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
64	[number]	k4	64
různých	různý	k2eAgFnPc2d1	různá
úrovní	úroveň	k1gFnPc2	úroveň
šedi	šeď	k1gFnSc2	šeď
<g/>
.	.	kIx.	.
<g/>
Zdrojem	zdroj	k1gInSc7	zdroj
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
LED	LED	kA	LED
dioda	dioda	k1gFnSc1	dioda
svítící	svítící	k2eAgFnSc1d1	svítící
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
podložku	podložka	k1gFnSc4	podložka
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
malým	malý	k2eAgNnSc7d1	malé
zrcátkem	zrcátko	k1gNnSc7	zrcátko
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
optickým	optický	k2eAgInSc7d1	optický
hranolkem	hranolek	k1gInSc7	hranolek
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
svítivosti	svítivost	k1gFnSc3	svítivost
použitých	použitý	k2eAgInPc2d1	použitý
LED	LED	kA	LED
diod	dioda	k1gFnPc2	dioda
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnSc3d1	nízká
výrobní	výrobní	k2eAgFnSc3d1	výrobní
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
kontrastnost	kontrastnost	k1gFnSc4	kontrastnost
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světlo	světlo	k1gNnSc1	světlo
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
odrazí	odrazit	k5eAaPmIp3nS	odrazit
od	od	k7c2	od
vhodné	vhodný	k2eAgFnSc2d1	vhodná
podložky	podložka	k1gFnSc2	podložka
je	být	k5eAaImIp3nS	být
optickým	optický	k2eAgInSc7d1	optický
prvkem	prvek	k1gInSc7	prvek
přivedeno	přivést	k5eAaPmNgNnS	přivést
do	do	k7c2	do
senzoru	senzor	k1gInSc2	senzor
tvořeného	tvořený	k2eAgInSc2d1	tvořený
čipem	čip	k1gInSc7	čip
CCD	CCD	kA	CCD
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
s	s	k7c7	s
maticí	matice	k1gFnSc7	matice
fotoelektricky	fotoelektricky	k6eAd1	fotoelektricky
citlivých	citlivý	k2eAgInPc2d1	citlivý
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
elektronický	elektronický	k2eAgInSc4d1	elektronický
obraz	obraz	k1gInSc4	obraz
podložky	podložka	k1gFnSc2	podložka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zaznamenáván	zaznamenávat	k5eAaImNgInS	zaznamenávat
rychlostí	rychlost	k1gFnSc7	rychlost
řádově	řádově	k6eAd1	řádově
miliony	milion	k4xCgInPc1	milion
pixelů	pixel	k1gInPc2	pixel
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
polohy	poloha	k1gFnSc2	poloha
myši	myš	k1gFnSc2	myš
se	se	k3xPyFc4	se
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
její	její	k3xOp3gInSc1	její
pohyb	pohyb	k1gInSc1	pohyb
pomocí	pomocí	k7c2	pomocí
výkonného	výkonný	k2eAgInSc2d1	výkonný
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
pohybem	pohyb	k1gInSc7	pohyb
kurzoru	kurzor	k1gInSc2	kurzor
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
myši	myš	k1gFnPc1	myš
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
běžnou	běžný	k2eAgFnSc4d1	běžná
podložku	podložka	k1gFnSc4	podložka
desku	deska	k1gFnSc4	deska
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
mohou	moct	k5eAaImIp3nP	moct
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nastat	nastat	k5eAaPmF	nastat
při	při	k7c6	při
snímání	snímání	k1gNnSc6	snímání
průhledné	průhledný	k2eAgFnSc2d1	průhledná
skleněné	skleněný	k2eAgFnSc2d1	skleněná
podložky	podložka	k1gFnSc2	podložka
<g/>
,	,	kIx,	,
podložky	podložka	k1gFnPc4	podložka
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
odrazivostí	odrazivost	k1gFnSc7	odrazivost
<g/>
,	,	kIx,	,
lesklé	lesklý	k2eAgInPc4d1	lesklý
povrchy	povrch	k1gInPc4	povrch
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
červeného	červený	k2eAgNnSc2d1	červené
světla	světlo	k1gNnSc2	světlo
myši	myš	k1gFnSc2	myš
i	i	k8xC	i
podložky	podložka	k1gFnPc4	podložka
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
červeným	červený	k2eAgInSc7d1	červený
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Osvětlení	osvětlení	k1gNnSc1	osvětlení
a	a	k8xC	a
podklad	podklad	k1gInSc1	podklad
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
osvětlení	osvětlení	k1gNnSc4	osvětlení
podkladu	podklad	k1gInSc2	podklad
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
využívají	využívat	k5eAaImIp3nP	využívat
červené	červený	k2eAgInPc4d1	červený
LED	led	k1gInSc4	led
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
optických	optický	k2eAgFnPc2d1	optická
myší	myš	k1gFnPc2	myš
byly	být	k5eAaImAgFnP	být
nejlevnější	levný	k2eAgFnPc1d3	nejlevnější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
barvě	barva	k1gFnSc6	barva
osvětlení	osvětlení	k1gNnSc2	osvětlení
nezáleží	záležet	k5eNaImIp3nS	záležet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
člověku	člověk	k1gMnSc3	člověk
neviditelného	viditelný	k2eNgNnSc2d1	neviditelné
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
světla	světlo	k1gNnSc2	světlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
vyšší	vysoký	k2eAgFnSc3d2	vyšší
přesnosti	přesnost	k1gFnSc3	přesnost
snímání	snímání	k1gNnSc2	snímání
a	a	k8xC	a
nižší	nízký	k2eAgFnSc2d2	nižší
spotřeby	spotřeba	k1gFnSc2	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgFnSc1d1	optická
myš	myš	k1gFnSc1	myš
pracuje	pracovat	k5eAaImIp3nS	pracovat
spolehlivě	spolehlivě	k6eAd1	spolehlivě
na	na	k7c6	na
strukturovaném	strukturovaný	k2eAgInSc6d1	strukturovaný
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
pohyb	pohyb	k1gInSc4	pohyb
podkladu	podklad	k1gInSc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
nevhodným	vhodný	k2eNgInSc7d1	nevhodný
podkladem	podklad	k1gInSc7	podklad
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
povrch	povrch	k1gInSc1	povrch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc4	vznik
falešných	falešný	k2eAgMnPc2d1	falešný
odrazů	odraz	k1gInPc2	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitnější	kvalitní	k2eAgFnPc1d2	kvalitnější
myši	myš	k1gFnPc1	myš
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
více	hodně	k6eAd2	hodně
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
pohyb	pohyb	k1gInSc1	pohyb
myši	myš	k1gFnSc2	myš
přesnější	přesný	k2eAgInSc4d2	přesnější
a	a	k8xC	a
správně	správně	k6eAd1	správně
reagoval	reagovat	k5eAaBmAgMnS	reagovat
i	i	k9	i
na	na	k7c4	na
rychlé	rychlý	k2eAgInPc4d1	rychlý
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
optické	optický	k2eAgFnSc2d1	optická
myši	myš	k1gFnSc2	myš
====	====	k?	====
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
články	článek	k1gInPc4	článek
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
poškození	poškození	k1gNnSc6	poškození
lidského	lidský	k2eAgInSc2d1	lidský
zraku	zrak	k1gInSc2	zrak
optickou	optický	k2eAgFnSc7d1	optická
myší	myš	k1gFnSc7	myš
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
důvod	důvod	k1gInSc1	důvod
se	se	k3xPyFc4	se
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
laserové	laserový	k2eAgNnSc4d1	laserové
zařízení	zařízení	k1gNnSc4	zařízení
třídy	třída	k1gFnSc2	třída
I	i	k9	i
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc4d1	malý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgNnSc1	žádný
poškození	poškození	k1gNnSc1	poškození
nemůže	moct	k5eNaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsoby	způsob	k1gInPc1	způsob
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
myš	myš	k1gFnSc1	myš
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
připojovala	připojovat	k5eAaImAgFnS	připojovat
pomocí	pomocí	k7c2	pomocí
sériového	sériový	k2eAgInSc2d1	sériový
portu	port	k1gInSc2	port
(	(	kIx(	(
<g/>
RS-	RS-	k1gFnSc1	RS-
<g/>
232	[number]	k4	232
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
počítačů	počítač	k1gMnPc2	počítač
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
pomocí	pomocí	k7c2	pomocí
linky	linka	k1gFnSc2	linka
ADB	ADB	kA	ADB
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
konektor	konektor	k1gInSc1	konektor
PS	PS	kA	PS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
USB	USB	kA	USB
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
myši	myš	k1gFnPc1	myš
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
combo	comba	k1gMnSc5	comba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pomocí	pomocí	k7c2	pomocí
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
redukce	redukce	k1gFnSc2	redukce
připojit	připojit	k5eAaPmF	připojit
do	do	k7c2	do
zásuvky	zásuvka	k1gFnSc2	zásuvka
USB	USB	kA	USB
i	i	k8xC	i
PS	PS	kA	PS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bezdrátové	bezdrátový	k2eAgFnPc4d1	bezdrátová
myši	myš	k1gFnPc4	myš
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
IA	ia	k0	ia
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rádiové	rádiový	k2eAgFnSc2d1	rádiová
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Bluetooth	Bluetootha	k1gFnPc2	Bluetootha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samotný	samotný	k2eAgInSc1d1	samotný
vysílač	vysílač	k1gInSc1	vysílač
<g/>
/	/	kIx~	/
<g/>
přijímač	přijímač	k1gInSc1	přijímač
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
pomocí	pomocí	k7c2	pomocí
sériového	sériový	k2eAgNnSc2d1	sériové
rozhraní	rozhraní	k1gNnSc2	rozhraní
PS	PS	kA	PS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
USB	USB	kA	USB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgNnPc1d1	alternativní
zařízení	zařízení	k1gNnPc1	zařízení
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
kurzoru	kurzor	k1gInSc2	kurzor
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
tzv.	tzv.	kA	tzv.
tablet	tablet	k1gInSc1	tablet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podložka	podložka	k1gFnSc1	podložka
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
dotyk	dotyk	k1gInSc4	dotyk
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
přejíždí	přejíždět	k5eAaImIp3nS	přejíždět
perem	pero	k1gNnSc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
také	také	k9	také
tablety	tableta	k1gFnPc1	tableta
citlivé	citlivý	k2eAgFnPc1d1	citlivá
na	na	k7c4	na
přítlak	přítlak	k1gInSc4	přítlak
<g/>
.	.	kIx.	.
</s>
<s>
Tablety	tableta	k1gFnPc1	tableta
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
používány	používat	k5eAaImNgFnP	používat
počítačovými	počítačový	k2eAgMnPc7d1	počítačový
grafiky	grafik	k1gMnPc7	grafik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
přenosných	přenosný	k2eAgNnPc2d1	přenosné
zařízení	zařízení	k1gNnPc2	zařízení
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc4	tři
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
typy	typ	k1gInPc4	typ
náhrady	náhrada	k1gFnSc2	náhrada
myši	myš	k1gFnSc2	myš
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trackball	Trackball	k1gInSc1	Trackball
–	–	k?	–
větší	veliký	k2eAgFnSc1d2	veliký
kulička	kulička	k1gFnSc1	kulička
je	být	k5eAaImIp3nS	být
zabudována	zabudovat	k5eAaPmNgFnS	zabudovat
v	v	k7c6	v
zařízení	zařízení	k1gNnSc6	zařízení
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přímo	přímo	k6eAd1	přímo
prstem	prst	k1gInSc7	prst
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Trackpoint	Trackpoint	k1gInSc1	Trackpoint
–	–	k?	–
tlustší	tlustý	k2eAgFnSc1d2	tlustší
malá	malý	k2eAgFnSc1d1	malá
tyčinka	tyčinka	k1gFnSc1	tyčinka
uprostřed	uprostřed	k7c2	uprostřed
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naklánění	naklánění	k1gNnSc4	naklánění
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
kurzoru	kurzor	k1gInSc2	kurzor
<g/>
,	,	kIx,	,
<g/>
Touchpad	Touchpad	k1gInSc1	Touchpad
–	–	k?	–
destička	destička	k1gFnSc1	destička
měřící	měřící	k2eAgFnSc4d1	měřící
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
posunování	posunování	k1gNnSc1	posunování
prstu	prst	k1gInSc2	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Optická	optický	k2eAgFnSc1d1	optická
myš	myš	k1gFnSc1	myš
</s>
</p>
<p>
<s>
Podložka	podložka	k1gFnSc1	podložka
pod	pod	k7c4	pod
myš	myš	k1gFnSc4	myš
</s>
</p>
<p>
<s>
Číslicové	číslicový	k2eAgNnSc1d1	číslicové
řízení	řízení	k1gNnSc1	řízení
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
počítačová	počítačový	k2eAgFnSc1d1	počítačová
myš	myš	k1gFnSc1	myš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Patent	patent	k1gInSc1	patent
na	na	k7c4	na
myš	myš	k1gFnSc4	myš
na	na	k7c6	na
google	googla	k1gFnSc6	googla
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
/	/	kIx~	/
<g/>
patents	patents	k1gInSc1	patents
</s>
</p>
<p>
<s>
Meet	Meet	k2eAgMnSc1d1	Meet
The	The	k1gMnSc1	The
Inventor	Inventor	k1gMnSc1	Inventor
of	of	k?	of
the	the	k?	the
Mouse	Mouse	k1gFnSc2	Mouse
Wheel	Wheel	k1gMnSc1	Wheel
</s>
</p>
