<p>
<s>
Bukovanský	Bukovanský	k2eAgInSc1d1	Bukovanský
mlýn	mlýn	k1gInSc1	mlýn
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Bukovany	Bukovan	k1gMnPc4	Bukovan
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
přibližně	přibližně	k6eAd1	přibližně
pět	pět	k4xCc4	pět
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
městečka	městečko	k1gNnSc2	městečko
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
větrného	větrný	k2eAgInSc2d1	větrný
mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Lopatky	lopatka	k1gFnPc4	lopatka
mlýna	mlýn	k1gInSc2	mlýn
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
poháněny	poháněn	k2eAgInPc1d1	poháněn
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
je	on	k3xPp3gMnPc4	on
pohání	pohánět	k5eAaImIp3nS	pohánět
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
funkce	funkce	k1gFnSc2	funkce
rozhledny	rozhledna	k1gFnSc2	rozhledna
slouží	sloužit	k5eAaImIp3nS	sloužit
stavba	stavba	k1gFnSc1	stavba
jako	jako	k8xC	jako
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
slovácké	slovácký	k2eAgFnSc2d1	Slovácká
vesnice	vesnice	k1gFnSc2	vesnice
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
mlýna	mlýn	k1gInSc2	mlýn
je	být	k5eAaImIp3nS	být
krbovna	krbovna	k1gFnSc1	krbovna
pronajímaná	pronajímaný	k2eAgFnSc1d1	pronajímaná
pro	pro	k7c4	pro
společenské	společenský	k2eAgFnPc4d1	společenská
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
a	a	k8xC	a
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
investorem	investor	k1gMnSc7	investor
byl	být	k5eAaImAgMnS	být
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
Josef	Josef	k1gMnSc1	Josef
Kouřil	Kouřil	k1gMnSc1	Kouřil
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
325	[number]	k4	325
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
stavby	stavba	k1gFnSc2	stavba
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
výborné	výborný	k2eAgFnPc4d1	výborná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
Chřiby	Chřiby	k1gInPc1	Chřiby
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
a	a	k8xC	a
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
rovněž	rovněž	k9	rovněž
110	[number]	k4	110
km	km	kA	km
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
kopec	kopec	k1gInSc1	kopec
Kobyla	kobyla	k1gFnSc1	kobyla
u	u	k7c2	u
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bukovanský	Bukovanský	k2eAgInSc4d1	Bukovanský
mlýn	mlýn	k1gInSc4	mlýn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Kyjovska	Kyjovsko	k1gNnSc2	Kyjovsko
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
o	o	k7c6	o
vyhlídkových	vyhlídkový	k2eAgInPc6d1	vyhlídkový
objektech	objekt	k1gInPc6	objekt
</s>
</p>
<p>
<s>
rozhledny	rozhledna	k1gFnPc1	rozhledna
<g/>
.	.	kIx.	.
<g/>
wz	wz	k?	wz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
obce	obec	k1gFnSc2	obec
Bukovany	Bukovan	k1gMnPc4	Bukovan
</s>
</p>
