<s>
Bukovanský	Bukovanský	k2eAgInSc1d1	Bukovanský
mlýn	mlýn	k1gInSc1	mlýn
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Bukovany	Bukovan	k1gMnPc4	Bukovan
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
přibližně	přibližně	k6eAd1	přibližně
pět	pět	k4xCc4	pět
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
městečka	městečko	k1gNnSc2	městečko
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
větrného	větrný	k2eAgInSc2d1	větrný
mlýna	mlýn	k1gInSc2	mlýn
<g/>
.	.	kIx.	.
</s>
