<p>
<s>
Synoda	synoda	k1gFnSc1	synoda
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
897	[number]	k4	897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Synodus	Synodus	k1gInSc1	Synodus
horrenda	horrenda	k1gFnSc1	horrenda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
synodu	synoda	k1gFnSc4	synoda
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
papežem	papež	k1gMnSc7	papež
Štěpánem	Štěpán	k1gMnSc7	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
přivlečena	přivlečen	k2eAgFnSc1d1	přivlečena
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
stará	starý	k2eAgFnSc1d1	stará
mrtvola	mrtvola	k1gFnSc1	mrtvola
Štěpánova	Štěpánův	k2eAgMnSc2d1	Štěpánův
předchůdce	předchůdce	k1gMnSc2	předchůdce
papeže	papež	k1gMnSc2	papež
Formosa	Formosa	k1gFnSc1	Formosa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typický	typický	k2eAgInSc4d1	typický
projev	projev	k1gInSc4	projev
úpadku	úpadek	k1gInSc2	úpadek
papežství	papežství	k1gNnPc2	papežství
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
temném	temný	k2eAgNnSc6d1	temné
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
891	[number]	k4	891
<g/>
–	–	k?	–
<g/>
1049	[number]	k4	1049
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předcházející	předcházející	k2eAgFnSc3d1	předcházející
události	událost	k1gFnSc3	událost
==	==	k?	==
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
891	[number]	k4	891
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolit	k5eAaPmNgMnS	zvolit
portský	portský	k2eAgMnSc1d1	portský
biskup	biskup	k1gMnSc1	biskup
Formosus	Formosus	k1gMnSc1	Formosus
s	s	k7c7	s
bohatými	bohatý	k2eAgFnPc7d1	bohatá
zásluhami	zásluha	k1gFnPc7	zásluha
a	a	k8xC	a
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sbíral	sbírat	k5eAaImAgMnS	sbírat
již	již	k6eAd1	již
za	za	k7c2	za
pontifikátu	pontifikát	k1gInSc2	pontifikát
Mikuláše	mikuláš	k1gInSc2	mikuláš
I.	I.	kA	I.
Zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
korunovaci	korunovace	k1gFnSc4	korunovace
Guida	Guida	k1gMnSc1	Guida
ze	z	k7c2	z
Spoleta	Spolet	k2eAgNnPc1d1	Spoleto
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vsadil	vsadit	k5eAaPmAgMnS	vsadit
císařskou	císařský	k2eAgFnSc4d1	císařská
korunu	koruna	k1gFnSc4	koruna
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Lamberta	Lambert	k1gMnSc2	Lambert
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Spoleta	Spoleta	k1gFnSc1	Spoleta
<g/>
.	.	kIx.	.
</s>
<s>
Oddíly	oddíl	k1gInPc1	oddíl
Guidovy	Guidův	k2eAgFnSc2d1	Guidova
vdovy	vdova	k1gFnSc2	vdova
hájily	hájit	k5eAaImAgInP	hájit
Řím	Řím	k1gInSc4	Řím
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
papeži	papež	k1gMnSc3	papež
rozkazovat	rozkazovat	k5eAaImF	rozkazovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
povolal	povolat	k5eAaPmAgMnS	povolat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
východofranského	východofranský	k2eAgMnSc2d1	východofranský
krále	král	k1gMnSc2	král
Arnulfa	Arnulf	k1gMnSc2	Arnulf
z	z	k7c2	z
Korutan	Korutany	k1gInPc2	Korutany
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
roku	rok	k1gInSc2	rok
896	[number]	k4	896
Řím	Řím	k1gInSc1	Řím
dobyl	dobýt	k5eAaPmAgInS	dobýt
a	a	k8xC	a
Formosus	Formosus	k1gInSc4	Formosus
ho	on	k3xPp3gInSc4	on
korunoval	korunovat	k5eAaBmAgInS	korunovat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nemocný	mocný	k2eNgMnSc1d1	nemocný
Arnulf	Arnulf	k1gMnSc1	Arnulf
Korutanský	korutanský	k2eAgInSc4d1	korutanský
Řím	Řím	k1gInSc4	Řím
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
Lambert	Lambert	k1gMnSc1	Lambert
ze	z	k7c2	z
Spoleta	Spoleta	k1gFnSc1	Spoleta
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Formosovi	Formos	k1gMnSc3	Formos
pomstil	pomstit	k5eAaImAgMnS	pomstit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
záhy	záhy	k6eAd1	záhy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Bonifác	Bonifác	k1gMnSc1	Bonifác
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nevládl	vládnout	k5eNaImAgMnS	vládnout
ani	ani	k8xC	ani
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Lambert	Lambert	k1gInSc1	Lambert
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
trůn	trůn	k1gInSc4	trůn
dosadil	dosadit	k5eAaPmAgMnS	dosadit
Štěpána	Štěpán	k1gMnSc4	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svolání	svolání	k1gNnPc1	svolání
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
synody	synoda	k1gFnSc2	synoda
==	==	k?	==
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
896	[number]	k4	896
<g/>
–	–	k?	–
<g/>
897	[number]	k4	897
<g/>
)	)	kIx)	)
nechal	nechat	k5eAaPmAgMnS	nechat
vykopat	vykopat	k5eAaPmF	vykopat
mrtvolu	mrtvola	k1gFnSc4	mrtvola
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Formosa	Formosa	k1gFnSc1	Formosa
(	(	kIx(	(
<g/>
891	[number]	k4	891
<g/>
–	–	k?	–
<g/>
896	[number]	k4	896
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvola	mrtvola	k1gFnSc1	mrtvola
stará	starat	k5eAaImIp3nS	starat
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
byla	být	k5eAaImAgFnS	být
oblečena	obléct	k5eAaPmNgFnS	obléct
do	do	k7c2	do
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
papežského	papežský	k2eAgInSc2d1	papežský
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
posazena	posazen	k2eAgFnSc1d1	posazena
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
takto	takto	k6eAd1	takto
souzena	souzen	k2eAgFnSc1d1	souzena
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
Formosus	Formosus	k1gMnSc1	Formosus
byl	být	k5eAaImAgMnS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
administrativní	administrativní	k2eAgNnSc4d1	administrativní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
i	i	k8xC	i
svěcení	svěcení	k1gNnPc4	svěcení
prohlášena	prohlášen	k2eAgNnPc4d1	prohlášeno
za	za	k7c4	za
neplatná	platný	k2eNgNnPc4d1	neplatné
a	a	k8xC	a
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
žehnal	žehnat	k5eAaImAgMnS	žehnat
<g/>
,	,	kIx,	,
useknuty	useknout	k5eAaPmNgFnP	useknout
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
Formosova	Formosův	k2eAgFnSc1d1	Formosův
mrtvola	mrtvola	k1gFnSc1	mrtvola
smýkána	smýkat	k5eAaImNgFnS	smýkat
ulicemi	ulice	k1gFnPc7	ulice
a	a	k8xC	a
hozena	hodit	k5eAaPmNgFnS	hodit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Tibery	Tibera	k1gFnSc2	Tibera
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Štěpán	Štěpán	k1gMnSc1	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
procesu	proces	k1gInSc2	proces
dlouho	dlouho	k6eAd1	dlouho
netěšil	těšit	k5eNaImAgInS	těšit
<g/>
.	.	kIx.	.
</s>
<s>
Lid	lid	k1gInSc1	lid
v	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
zřícení	zřícení	k1gNnSc6	zřícení
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Lateránu	Laterán	k1gInSc6	Laterán
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
trest	trest	k1gInSc4	trest
za	za	k7c4	za
zohavení	zohavení	k1gNnSc4	zohavení
mrtvoly	mrtvola	k1gFnSc2	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgInSc1d1	zajat
svými	svůj	k3xOyFgMnPc7	svůj
odpůrci	odpůrce	k1gMnPc7	odpůrce
a	a	k8xC	a
uškrcen	uškrcen	k2eAgInSc4d1	uškrcen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následující	následující	k2eAgFnPc1d1	následující
události	událost	k1gFnPc1	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Nenávist	nenávist	k1gFnSc1	nenávist
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
frakcemi	frakce	k1gFnPc7	frakce
byla	být	k5eAaImAgFnS	být
veliká	veliký	k2eAgFnSc1d1	veliká
a	a	k8xC	a
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
ještě	ještě	k9	ještě
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Formosovi	Formosův	k2eAgMnPc1d1	Formosův
stoupenci	stoupenec	k1gMnPc1	stoupenec
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Štěpána	Štěpán	k1gMnSc2	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
prosadili	prosadit	k5eAaPmAgMnP	prosadit
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
stolec	stolec	k1gInSc4	stolec
svého	svůj	k3xOyFgMnSc2	svůj
kandidáta	kandidát	k1gMnSc2	kandidát
Romana	Roman	k1gMnSc2	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
další	další	k2eAgMnSc1d1	další
představitel	představitel	k1gMnSc1	představitel
pro-formosovské	proormosovský	k2eAgFnSc2d1	pro-formosovský
kliky	klika	k1gFnSc2	klika
-	-	kIx~	-
Theodor	Theodor	k1gMnSc1	Theodor
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
/	/	kIx~	/
<g/>
prosinec	prosinec	k1gInSc1	prosinec
897	[number]	k4	897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stihl	stihnout	k5eAaPmAgMnS	stihnout
zrušit	zrušit	k5eAaPmF	zrušit
ustanovení	ustanovení	k1gNnSc4	ustanovení
synody	synoda	k1gFnSc2	synoda
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
údajné	údajný	k2eAgNnSc1d1	údajné
znovunalezené	znovunalezený	k2eAgNnSc1d1	znovunalezené
Formosovo	Formosův	k2eAgNnSc1d1	Formosův
tělo	tělo	k1gNnSc1	tělo
nechal	nechat	k5eAaPmAgInS	nechat
pohřbít	pohřbít	k5eAaPmF	pohřbít
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Theodora	Theodor	k1gMnSc2	Theodor
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
898	[number]	k4	898
papežem	papež	k1gMnSc7	papež
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
synodách	synoda	k1gFnPc6	synoda
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nechal	nechat	k5eAaPmAgMnS	nechat
znovu	znovu	k6eAd1	znovu
rehabilitovat	rehabilitovat	k5eAaBmF	rehabilitovat
Formosa	Formosa	k1gFnSc1	Formosa
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
závěrů	závěr	k1gInPc2	závěr
synody	synoda	k1gFnSc2	synoda
smrti	smrt	k1gFnSc2	smrt
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
hromadné	hromadný	k2eAgInPc1d1	hromadný
omluvy	omluva	k1gFnSc2	omluva
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
synodě	synoda	k1gFnSc6	synoda
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
omluvit	omluvit	k5eAaPmF	omluvit
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
biskup	biskup	k1gMnSc1	biskup
Sergius	Sergius	k1gMnSc1	Sergius
z	z	k7c2	z
Caere	Caer	k1gInSc5	Caer
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Sergius	Sergius	k1gMnSc1	Sergius
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Sergius	Sergius	k1gInSc1	Sergius
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
Formosovým	Formosový	k2eAgMnPc3d1	Formosový
protivníkům	protivník	k1gMnPc3	protivník
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Formosova	Formosův	k2eAgNnSc2d1	Formosův
svěcení	svěcení	k1gNnSc2	svěcení
za	za	k7c4	za
neplatná	platný	k2eNgNnPc4d1	neplatné
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
velký	velký	k2eAgInSc4d1	velký
zmatek	zmatek	k1gInSc4	zmatek
mezi	mezi	k7c7	mezi
duchovními	duchovní	k2eAgInPc7d1	duchovní
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sergius	Sergius	k1gInSc1	Sergius
III	III	kA	III
<g/>
.	.	kIx.	.
de	de	k?	de
facto	facto	k1gNnSc1	facto
provedl	provést	k5eAaPmAgInS	provést
převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
se	se	k3xPyFc4	se
papežského	papežský	k2eAgInSc2d1	papežský
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
příhoda	příhoda	k1gFnSc1	příhoda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
námětem	námět	k1gInSc7	námět
mnoha	mnoho	k4c2	mnoho
beletristických	beletristický	k2eAgNnPc2d1	beletristické
zpracování	zpracování	k1gNnPc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
Mníšek	Mníšek	k1gInSc4	Mníšek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
koncilů	koncil	k1gInPc2	koncil
a	a	k8xC	a
synod	synoda	k1gFnPc2	synoda
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
Životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
326	[number]	k4	326
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
:	:	kIx,	:
Od	od	k7c2	od
Sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
