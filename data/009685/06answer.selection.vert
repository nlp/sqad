<s>
Synoda	synoda	k1gFnSc1	synoda
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
897	[number]	k4	897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Synodus	Synodus	k1gInSc1	Synodus
horrenda	horrenda	k1gFnSc1	horrenda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
synodu	synoda	k1gFnSc4	synoda
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
papežem	papež	k1gMnSc7	papež
Štěpánem	Štěpán	k1gMnSc7	Štěpán
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
přivlečena	přivlečen	k2eAgFnSc1d1	přivlečena
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
stará	starý	k2eAgFnSc1d1	stará
mrtvola	mrtvola	k1gFnSc1	mrtvola
Štěpánova	Štěpánův	k2eAgMnSc2d1	Štěpánův
předchůdce	předchůdce	k1gMnSc2	předchůdce
papeže	papež	k1gMnSc2	papež
Formosa	Formosa	k1gFnSc1	Formosa
<g/>
.	.	kIx.	.
</s>
