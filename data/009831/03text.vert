<p>
<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
též	též	k9	též
letní	letní	k2eAgFnSc1d1	letní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
LOH	LOH	kA	LOH
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnSc2	místo
konání	konání	k1gNnSc2	konání
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
medailí	medaile	k1gFnPc2	medaile
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
po	po	k7c6	po
LOH	LOH	kA	LOH
2016	[number]	k4	2016
-	-	kIx~	-
zdroj	zdroj	k1gInSc1	zdroj
sports-reference	sportseferenec	k1gInSc2	sports-referenec
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Bývalý	bývalý	k2eAgInSc1d1	bývalý
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
==	==	k?	==
Nejúspěšnější	úspěšný	k2eAgFnPc1d3	nejúspěšnější
země	zem	k1gFnPc1	zem
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
olympiádách	olympiáda	k1gFnPc6	olympiáda
podle	podle	k7c2	podle
získaných	získaný	k2eAgFnPc2d1	získaná
medailí	medaile	k1gFnPc2	medaile
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
</s>
</p>
<p>
<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Politika	politika	k1gFnSc1	politika
a	a	k8xC	a
LOH	LOH	kA	LOH
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnSc1	databáze
OH	OH	kA	OH
na	na	k7c6	na
olympic	olympice	k1gFnPc2	olympice
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
