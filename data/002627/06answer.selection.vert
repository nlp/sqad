<s>
Zadečkové	zadečkový	k2eAgFnPc1d1	zadečková
končetiny	končetina	k1gFnPc1	končetina
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ve	v	k7c4	v
snovací	snovací	k2eAgFnPc4d1	snovací
bradavky	bradavka	k1gFnPc4	bradavka
produkující	produkující	k2eAgFnSc1d1	produkující
pavučinová	pavučinový	k2eAgFnSc1d1	pavučinová
vlákna	vlákna	k1gFnSc1	vlákna
až	až	k9	až
z	z	k7c2	z
šesti	šest	k4xCc2	šest
druhů	druh	k1gInPc2	druh
snovacích	snovací	k2eAgFnPc2d1	snovací
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
<g/>
.	.	kIx.	.
</s>
