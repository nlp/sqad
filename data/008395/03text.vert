<p>
<s>
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
je	být	k5eAaImIp3nS	být
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
Václava	Václav	k1gMnSc2	Václav
Hraběte	Hrabě	k1gMnSc2	Hrabě
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
připravili	připravit	k5eAaPmAgMnP	připravit
z	z	k7c2	z
autorovy	autorův	k2eAgFnSc2d1	autorova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
pramenů	pramen	k1gInPc2	pramen
recitátor	recitátor	k1gMnSc1	recitátor
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Hrabětův	Hrabětův	k2eAgMnSc1d1	Hrabětův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Miškovský	Miškovský	k2eAgMnSc1d1	Miškovský
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Blues	blues	k1gFnSc1	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
Prolog	prolog	k1gInSc1	prolog
<g/>
,	,	kIx,	,
Podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
renesanční	renesanční	k2eAgNnSc4d1	renesanční
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
Ospalé	ospalý	k2eAgFnPc4d1	ospalá
něžnosti	něžnost	k1gFnPc4	něžnost
<g/>
,	,	kIx,	,
Krátká	krátký	k2eAgFnSc1d1	krátká
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
Infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
Blues	blues	k1gNnSc1	blues
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Majakovského	Majakovský	k2eAgMnSc2d1	Majakovský
<g/>
,	,	kIx,	,
Ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
Ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
,	,	kIx,	,
Zavři	zavřít	k5eAaPmRp2nS	zavřít
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
Báseň	báseň	k1gFnSc4	báseň
skoro	skoro	k6eAd1	skoro
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Hrabě	Hrabě	k1gMnSc1	Hrabě
připravoval	připravovat	k5eAaImAgMnS	připravovat
pro	pro	k7c4	pro
básnickou	básnický	k2eAgFnSc4d1	básnická
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
však	však	k9	však
sám	sám	k3xTgInSc1	sám
nikdy	nikdy	k6eAd1	nikdy
nevydal	vydat	k5eNaPmAgMnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
básně	báseň	k1gFnPc4	báseň
<g/>
"	"	kIx"	"
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
básní	báseň	k1gFnPc2	báseň
řazených	řazený	k2eAgFnPc2d1	řazená
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
vydání	vydání	k1gNnSc6	vydání
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Blues	blues	k1gNnSc1	blues
<g/>
,	,	kIx,	,
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Blues	blues	k1gNnSc2	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
opět	opět	k6eAd1	opět
u	u	k7c2	u
Labyrintu	labyrint	k1gInSc2	labyrint
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
knihy	kniha	k1gFnSc2	kniha
jich	on	k3xPp3gMnPc2	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Témata	téma	k1gNnPc4	téma
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
básni	báseň	k1gFnSc6	báseň
"	"	kIx"	"
<g/>
Prolog	prolog	k1gInSc1	prolog
<g/>
"	"	kIx"	"
autor	autor	k1gMnSc1	autor
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
hlavně	hlavně	k9	hlavně
obyčejným	obyčejný	k2eAgMnPc3d1	obyčejný
lidem	člověk	k1gMnPc3	člověk
(	(	kIx(	(
<g/>
posílá	posílat	k5eAaImIp3nS	posílat
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
hlavně	hlavně	k6eAd1	hlavně
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
přátelům	přítel	k1gMnPc3	přítel
či	či	k8xC	či
láskám	láska	k1gFnPc3	láska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
hloubavým	hloubavý	k2eAgMnPc3d1	hloubavý
intelektuálům	intelektuál	k1gMnPc3	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
autorova	autorův	k2eAgFnSc1d1	autorova
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
milované	milovaný	k2eAgFnSc3d1	milovaná
dívce	dívka	k1gFnSc3	dívka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
Infekce	infekce	k1gFnSc1	infekce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
HRABĚ	Hrabě	k1gMnSc1	Hrabě
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Miškovský	Miškovský	k2eAgMnSc1d1	Miškovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
257	[number]	k4	257
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRABĚ	Hrabě	k1gMnSc1	Hrabě
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Blues	blues	k1gNnSc1	blues
:	:	kIx,	:
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Miškovský	Miškovský	k2eAgMnSc1d1	Miškovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
142	[number]	k4	142
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901289	[number]	k4	901289
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HRABĚ	Hrabě	k1gMnSc1	Hrabě
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Blues	blues	k1gNnSc1	blues
:	:	kIx,	:
Blues	blues	k1gNnSc1	blues
pro	pro	k7c4	pro
bláznivou	bláznivý	k2eAgFnSc4d1	bláznivá
holku	holka	k1gFnSc4	holka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kovářík	kovářík	k1gMnSc1	kovářík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Miškovský	Miškovský	k2eAgMnSc1d1	Miškovský
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
172	[number]	k4	172
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85935	[number]	k4	85935
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
