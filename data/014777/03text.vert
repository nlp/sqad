<s>
Modrásek	modrásek	k1gMnSc1
měchýřníkový	měchýřníkový	k2eAgMnSc1d1
</s>
<s>
Modrásek	modrásek	k1gMnSc1
měchýřníkový	měchýřníkový	k2eAgInSc1d1
Pohled	pohled	k1gInSc1
na	na	k7c4
šedý	šedý	k2eAgInSc4d1
rub	rub	k1gInSc4
křídel	křídlo	k1gNnPc2
modráska	modrásek	k1gMnSc2
měchýřníkového	měchýřníkový	k2eAgInSc2d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Athropoda	Athropoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
motýli	motýl	k1gMnPc1
(	(	kIx(
<g/>
Lepidoptera	Lepidopter	k1gMnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
modráskovití	modráskovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Lycaenidae	Lycaenidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
Iolana	Iolana	k1gFnSc1
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Iolana	Iolana	k1gFnSc1
iolas	iolas	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Ochsenheimer	Ochsenheimer	k1gMnSc1
<g/>
,	,	kIx,
1816	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Modrásek	modrásek	k1gMnSc1
měchýřníkový	měchýřníkový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Iolana	Iolana	k1gFnSc1
iolas	iolas	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
denního	denní	k2eAgMnSc2d1
motýla	motýl	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
modráskovitých	modráskovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Lycaenidae	Lycaenidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpětí	rozpětí	k1gNnSc2
jeho	jeho	k3xOp3gNnPc2
křídel	křídlo	k1gNnPc2
je	být	k5eAaImIp3nS
36	#num#	k4
až	až	k9
40	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
mají	mít	k5eAaImIp3nP
modrá	modrý	k2eAgNnPc4d1
křídla	křídlo	k1gNnPc4
s	s	k7c7
úzkým	úzký	k2eAgInSc7d1
tmavým	tmavý	k2eAgInSc7d1
lemem	lem	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
modře	modř	k1gFnPc1
zbarveny	zbarven	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
oproti	oproti	k7c3
samcům	samec	k1gMnPc3
mají	mít	k5eAaImIp3nP
široký	široký	k2eAgInSc1d1
tmavý	tmavý	k2eAgInSc1d1
lem	lem	k1gInSc1
a	a	k8xC
nevýrazné	výrazný	k2eNgFnPc1d1
tmavé	tmavý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
podél	podél	k7c2
vnějšího	vnější	k2eAgInSc2d1
okraje	okraj	k1gInSc2
zadních	zadní	k2eAgNnPc2d1
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rub	rub	k1gInSc4
křídel	křídlo	k1gNnPc2
u	u	k7c2
obou	dva	k4xCgNnPc2
pohlaví	pohlaví	k1gNnPc2
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
s	s	k7c7
výraznou	výrazný	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
černých	černý	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
modráska	modrásek	k1gMnSc2
měchýřníkového	měchýřníkový	k2eAgMnSc2d1
</s>
<s>
Motýl	motýl	k1gMnSc1
je	být	k5eAaImIp3nS
rozšířený	rozšířený	k2eAgMnSc1d1
od	od	k7c2
Španělska	Španělsko	k1gNnSc2
přes	přes	k7c4
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc4
a	a	k8xC
Itálii	Itálie	k1gFnSc4
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
(	(	kIx(
<g/>
Balkánský	balkánský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
a	a	k8xC
Turecko	Turecko	k1gNnSc4
<g/>
)	)	kIx)
až	až	k9
po	po	k7c4
Írán	Írán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejseverněji	severně	k6eAd3
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
modráska	modrásek	k1gMnSc2
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obývá	obývat	k5eAaImIp3nS
kamenité	kamenitý	k2eAgInPc1d1
slunné	slunný	k2eAgInPc1d1
svahy	svah	k1gInPc1
a	a	k8xC
křovinaté	křovinatý	k2eAgFnPc1d1
stepi	step	k1gFnPc1
v	v	k7c6
nížinách	nížina	k1gFnPc6
a	a	k8xC
pahorkatinách	pahorkatina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Chování	chování	k1gNnSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Živnou	živný	k2eAgFnSc7d1
rostlinou	rostlina	k1gFnSc7
modráska	modrásek	k1gMnSc2
měchýřníkového	měchýřníkový	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
žanovec	žanovec	k1gInSc1
měchýřník	měchýřník	k1gInSc1
(	(	kIx(
<g/>
Colutea	Colutea	k1gFnSc1
arborescens	arborescensa	k1gFnPc2
<g/>
)	)	kIx)
z	z	k7c2
čeledi	čeleď	k1gFnSc2
bobovitých	bobovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Fabaceae	Fabaceae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
klade	klást	k5eAaImIp3nS
vajíčka	vajíčko	k1gNnPc4
na	na	k7c4
květní	květní	k2eAgInSc4d1
kalich	kalich	k1gInSc4
a	a	k8xC
nezralé	zralý	k2eNgInPc4d1
lusky	lusk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Housenky	housenka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
myrmekofilní	myrmekofilní	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
nezralými	zralý	k2eNgNnPc7d1
semeny	semeno	k1gNnPc7
v	v	k7c6
luscích	lusk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motýl	motýl	k1gMnSc1
je	být	k5eAaImIp3nS
jednogenerační	jednogenerační	k2eAgMnSc1d1
(	(	kIx(
<g/>
monovoltinní	monovoltinný	k2eAgMnPc1d1
<g/>
)	)	kIx)
s	s	k7c7
letovou	letový	k2eAgFnSc7d1
periodou	perioda	k1gFnSc7
od	od	k7c2
konce	konec	k1gInSc2
dubna	duben	k1gInSc2
do	do	k7c2
počátku	počátek	k1gInSc2
července	červenec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
špatných	špatný	k2eAgFnPc6d1
klimatických	klimatický	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
dospělci	dospělec	k1gMnPc1
z	z	k7c2
první	první	k4xOgFnSc2
generece	generece	k1gFnSc2
objevovat	objevovat	k5eAaImF
i	i	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příležitostně	příležitostně	k6eAd1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
motýl	motýl	k1gMnSc1
i	i	k9
částečnou	částečný	k2eAgFnSc4d1
druhou	druhý	k4xOgFnSc4
generaci	generace	k1gFnSc4
(	(	kIx(
<g/>
srpen	srpen	k1gInSc4
a	a	k8xC
září	září	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přezimuje	přezimovat	k5eAaBmIp3nS
kukla	kukla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
LAŠTŮVKA	laštůvka	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
TRAXLER	TRAXLER	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motýli	motýl	k1gMnPc1
a	a	k8xC
housenky	housenka	k1gFnPc1
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Denní	denní	k2eAgMnPc1d1
motýli	motýl	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
539	#num#	k4
s.	s.	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
modrásek	modrásek	k1gMnSc1
měchýřníkový	měchýřníkový	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Entomologie	entomologie	k1gFnSc1
</s>
