<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
(	(	kIx(	(
<g/>
v	v	k7c6	v
hantecu	hantecum	k1gNnSc6	hantecum
Prýgl	Prýgla	k1gFnPc2	Prýgla
nebo	nebo	k8xC	nebo
Prygl	Prygla	k1gFnPc2	Prygla
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Kníničská	kníničský	k2eAgFnSc1d1	Kníničská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
Svratce	Svratka	k1gFnSc6	Svratka
<g/>
.	.	kIx.	.
</s>
