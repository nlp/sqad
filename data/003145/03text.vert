<s>
Mramor	mramor	k1gInSc1	mramor
(	(	kIx(	(
<g/>
krystalický	krystalický	k2eAgInSc1d1	krystalický
vápenec	vápenec	k1gInSc1	vápenec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
95	[number]	k4	95
%	%	kIx~	%
kalcitu	kalcit	k1gInSc2	kalcit
<g/>
.	.	kIx.	.
</s>
<s>
Přimíšeny	přimíšen	k2eAgInPc1d1	přimíšen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jílové	jílový	k2eAgFnPc1d1	jílová
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
nerosty	nerost	k1gInPc1	nerost
(	(	kIx(	(
<g/>
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
limonit	limonit	k1gInSc1	limonit
<g/>
,	,	kIx,	,
hematit	hematit	k1gInSc1	hematit
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
i	i	k8xC	i
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
původně	původně	k6eAd1	původně
bílý	bílý	k2eAgInSc4d1	bílý
mramor	mramor	k1gInSc4	mramor
zabarvují	zabarvovat	k5eAaImIp3nP	zabarvovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
mramor	mramor	k1gInSc1	mramor
označuje	označovat	k5eAaImIp3nS	označovat
každý	každý	k3xTgInSc4	každý
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
leštit	leštit	k5eAaImF	leštit
<g/>
.	.	kIx.	.
</s>
<s>
Mramor	mramor	k1gInSc1	mramor
vzniká	vznikat	k5eAaImIp3nS	vznikat
přeměnou	přeměna	k1gFnSc7	přeměna
(	(	kIx(	(
<g/>
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
<g/>
)	)	kIx)	)
vápenců	vápenec	k1gInPc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
překrystalování	překrystalování	k1gNnSc3	překrystalování
kalcitu	kalcit	k1gInSc2	kalcit
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
-	-	kIx~	-
většinou	většina	k1gFnSc7	většina
organogenního	organogenní	k2eAgInSc2d1	organogenní
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
mramoru	mramor	k1gInSc6	mramor
nenajdeme	najít	k5eNaPmIp1nP	najít
schránky	schránka	k1gFnPc4	schránka
živočichů	živočich	k1gMnPc2	živočich
nebo	nebo	k8xC	nebo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
je	být	k5eAaImIp3nS	být
9,3	[number]	k4	9,3
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
šedého	šedý	k2eAgInSc2d1	šedý
mramoru	mramor	k1gInSc2	mramor
je	být	k5eAaImIp3nS	být
11,6	[number]	k4	11,6
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
modrého	modrý	k2eAgInSc2d1	modrý
mramoru	mramor	k1gInSc2	mramor
je	být	k5eAaImIp3nS	být
9,4	[number]	k4	9,4
<g/>
.	.	kIx.	.
kararský	kararský	k2eAgMnSc1d1	kararský
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
sněhobílou	sněhobílý	k2eAgFnSc7d1	sněhobílá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
dostal	dostat	k5eAaPmAgInS	dostat
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Carrara	Carrar	k1gMnSc2	Carrar
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
šternberský	šternberský	k2eAgMnSc1d1	šternberský
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
)	)	kIx)	)
cetechovický	cetechovický	k2eAgInSc1d1	cetechovický
mramor	mramor	k1gInSc1	mramor
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Cetechovice	Cetechovice	k1gFnSc1	Cetechovice
<g/>
)	)	kIx)	)
pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
mramor	mramor	k1gInSc1	mramor
je	být	k5eAaImIp3nS	být
bilý	bilý	k?	bilý
(	(	kIx(	(
<g/>
Nedvědice	Nedvědice	k1gFnSc2	Nedvědice
<g/>
)	)	kIx)	)
nehodivský	hodivský	k2eNgInSc4d1	hodivský
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
(	(	kIx(	(
<g/>
Nehodiv	hodit	k5eNaPmDgInS	hodit
<g/>
)	)	kIx)	)
lipovský	lipovský	k2eAgInSc4d1	lipovský
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tmavý	tmavý	k2eAgMnSc1d1	tmavý
a	a	k8xC	a
světlý	světlý	k2eAgMnSc1d1	světlý
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnSc1d1	horní
Lipová	lipový	k2eAgFnSc1d1	Lipová
<g/>
)	)	kIx)	)
sněžníkovský	sněžníkovský	k2eAgInSc4d1	sněžníkovský
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světlý	světlý	k2eAgMnSc1d1	světlý
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnSc1d1	horní
Morava	Morava	k1gFnSc1	Morava
<g/>
)	)	kIx)	)
zříceninový	zříceninový	k2eAgMnSc1d1	zříceninový
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vyleštěných	vyleštěný	k2eAgFnPc6d1	vyleštěná
plochách	plocha	k1gFnPc6	plocha
kresby	kresba	k1gFnSc2	kresba
v	v	k7c6	v
hnědých	hnědý	k2eAgInPc6d1	hnědý
tónech	tón	k1gInPc6	tón
(	(	kIx(	(
<g/>
Florencie	Florencie	k1gFnPc1	Florencie
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Sudoměřice	Sudoměřice	k1gInPc1	Sudoměřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vápence	vápenec	k1gInPc1	vápenec
Barrandienu	barrandien	k1gInSc2	barrandien
těžené	těžený	k2eAgInPc1d1	těžený
jako	jako	k8xS	jako
mramory	mramor	k1gInPc1	mramor
pro	pro	k7c4	pro
sochařské	sochařský	k2eAgNnSc4d1	sochařské
a	a	k8xC	a
architektonické	architektonický	k2eAgNnSc4d1	architektonické
užití	užití	k1gNnSc4	užití
<g/>
:	:	kIx,	:
Slivenecký	slivenecký	k2eAgInSc4d1	slivenecký
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
růžový	růžový	k2eAgInSc4d1	růžový
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc4d1	hnědý
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
s	s	k7c7	s
žilkami	žilka	k1gFnPc7	žilka
(	(	kIx(	(
<g/>
Cikánka	cikánka	k1gFnSc1	cikánka
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Kopanina	kopanina	k1gFnSc1	kopanina
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Špičce	špička	k1gFnSc6	špička
<g/>
,	,	kIx,	,
Hvížďalka	Hvížďalka	k1gFnSc1	Hvížďalka
<g/>
)	)	kIx)	)
Karlický	Karlický	k2eAgInSc1d1	Karlický
mramor	mramor	k1gInSc1	mramor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
se	s	k7c7	s
zlatožlutými	zlatožlutý	k2eAgFnPc7d1	zlatožlutá
žilkami	žilka	k1gFnPc7	žilka
(	(	kIx(	(
<g/>
Karlické	Karlický	k2eAgNnSc1d1	Karlické
údolí	údolí	k1gNnSc1	údolí
<g/>
)	)	kIx)	)
Bílý	bílý	k2eAgInSc1d1	bílý
mramor	mramor	k1gInSc1	mramor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
kararský	kararský	k2eAgMnSc1d1	kararský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
ceněným	ceněný	k2eAgInSc7d1	ceněný
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
sochaře	sochař	k1gMnSc4	sochař
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
opracovává	opracovávat	k5eAaImIp3nS	opracovávat
(	(	kIx(	(
<g/>
stupeň	stupeň	k1gInSc1	stupeň
tvrdosti	tvrdost	k1gFnSc2	tvrdost
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
izotropní	izotropní	k2eAgInSc4d1	izotropní
<g/>
,	,	kIx,	,
homogenní	homogenní	k2eAgInSc4d1	homogenní
a	a	k8xC	a
odolný	odolný	k2eAgInSc4d1	odolný
proti	proti	k7c3	proti
otřesům	otřes	k1gInPc3	otřes
při	při	k7c6	při
opracovávání	opracovávání	k1gNnSc6	opracovávání
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
kalcitových	kalcitový	k2eAgInPc6d1	kalcitový
krystalech	krystal	k1gInPc6	krystal
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
"	"	kIx"	"
<g/>
voskový	voskový	k2eAgInSc4d1	voskový
<g/>
"	"	kIx"	"
vzhled	vzhled	k1gInSc4	vzhled
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
sochám	sochat	k5eAaImIp1nS	sochat
dává	dávat	k5eAaImIp3nS	dávat
dojem	dojem	k1gInSc1	dojem
živého	živý	k2eAgNnSc2d1	živé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
starověké	starověký	k2eAgFnPc4d1	starověká
civilizace	civilizace	k1gFnPc4	civilizace
byl	být	k5eAaImAgInS	být
mramor	mramor	k1gInSc1	mramor
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
opracovat	opracovat	k5eAaPmF	opracovat
bronzovými	bronzový	k2eAgInPc7d1	bronzový
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
mnoho	mnoho	k4c1	mnoho
příkladů	příklad	k1gInPc2	příklad
stavitelské	stavitelský	k2eAgFnSc2d1	stavitelská
vyspělosti	vyspělost	k1gFnSc2	vyspělost
jejich	jejich	k3xOp3gMnPc2	jejich
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
stavby	stavba	k1gFnPc4	stavba
slouží	sloužit	k5eAaImIp3nS	sloužit
mramor	mramor	k1gInSc1	mramor
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozmanité	rozmanitý	k2eAgFnSc3d1	rozmanitá
struktuře	struktura	k1gFnSc3	struktura
a	a	k8xC	a
barevnosti	barevnost	k1gFnSc3	barevnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
dekorativním	dekorativní	k2eAgInPc3d1	dekorativní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
ceny	cena	k1gFnSc2	cena
originálu	originál	k1gInSc2	originál
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
napodobován	napodobován	k2eAgInSc1d1	napodobován
(	(	kIx(	(
<g/>
imitace	imitace	k1gFnSc1	imitace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podlahy	podlaha	k1gFnPc4	podlaha
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodný	vhodný	k2eAgInSc1d1	vhodný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
<g/>
.	.	kIx.	.
</s>
<s>
Nařezaný	nařezaný	k2eAgInSc1d1	nařezaný
na	na	k7c4	na
kostky	kostka	k1gFnPc4	kostka
se	se	k3xPyFc4	se
mramor	mramor	k1gInSc1	mramor
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
dláždění	dláždění	k1gNnSc6	dláždění
chodníků	chodník	k1gInPc2	chodník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
různobarevných	různobarevný	k2eAgInPc2d1	různobarevný
druhů	druh	k1gInPc2	druh
složí	složit	k5eAaPmIp3nS	složit
mozaika	mozaika	k1gFnSc1	mozaika
<g/>
.	.	kIx.	.
</s>
<s>
Jemný	jemný	k2eAgInSc1d1	jemný
mramorový	mramorový	k2eAgInSc1d1	mramorový
prach	prach	k1gInSc1	prach
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
brusivo	brusivo	k1gNnSc1	brusivo
v	v	k7c6	v
brusných	brusný	k2eAgFnPc6d1	brusná
pastách	pasta	k1gFnPc6	pasta
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
zubních	zubní	k2eAgFnPc6d1	zubní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plnidlo	plnidlo	k1gNnSc1	plnidlo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lesklého	lesklý	k2eAgInSc2d1	lesklý
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
křídový	křídový	k2eAgInSc1d1	křídový
papír	papír	k1gInSc1	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
"	"	kIx"	"
<g/>
umělého	umělý	k2eAgInSc2d1	umělý
mramoru	mramor	k1gInSc2	mramor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tento	tento	k3xDgInSc1	tento
výčet	výčet	k1gInSc1	výčet
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
.	.	kIx.	.
</s>
<s>
Mramor	mramor	k1gInSc1	mramor
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
materiál	materiál	k1gInSc4	materiál
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
římských	římský	k2eAgMnPc2d1	římský
sochařů	sochař	k1gMnPc2	sochař
a	a	k8xC	a
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgInSc4d1	tradiční
a	a	k8xC	a
vytříbený	vytříbený	k2eAgInSc4d1	vytříbený
vkus	vkus	k1gInSc4	vkus
<g/>
.	.	kIx.	.
</s>
<s>
Sněhově	sněhově	k6eAd1	sněhově
bílý	bílý	k2eAgInSc1d1	bílý
mramor	mramor	k1gInSc1	mramor
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
také	také	k9	také
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
