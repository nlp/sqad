<p>
<s>
Saul	Saul	k1gMnSc1	Saul
Hudson	Hudson	k1gMnSc1	Hudson
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1965	[number]	k4	1965
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Slash	Slasha	k1gFnPc2	Slasha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rockový	rockový	k2eAgMnSc1d1	rockový
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
sólový	sólový	k2eAgMnSc1d1	sólový
kytarista	kytarista	k1gMnSc1	kytarista
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
založil	založit	k5eAaPmAgInS	založit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kapelu	kapela	k1gFnSc4	kapela
Slash	Slasha	k1gFnPc2	Slasha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
slavnější	slavný	k2eAgFnSc4d2	slavnější
a	a	k8xC	a
úspěšnější	úspěšný	k2eAgFnSc4d2	úspěšnější
kapelu	kapela	k1gFnSc4	kapela
Velvet	Velveta	k1gFnPc2	Velveta
Revolver	revolver	k1gInSc1	revolver
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Slash	Slash	k1gMnSc1	Slash
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
rysy	rys	k1gInPc7	rys
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
černé	černý	k2eAgInPc4d1	černý
kudrnaté	kudrnatý	k2eAgInPc4d1	kudrnatý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
nošení	nošení	k1gNnSc4	nošení
cylindru	cylindr	k1gInSc2	cylindr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
unikátní	unikátní	k2eAgNnSc1d1	unikátní
držení	držení	k1gNnSc1	držení
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
sám	sám	k3xTgMnSc1	sám
-	-	kIx~	-
kytaru	kytara	k1gFnSc4	kytara
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízko	nízko	k6eAd1	nízko
pod	pod	k7c7	pod
pasem	pas	k1gInSc7	pas
a	a	k8xC	a
ruku	ruka	k1gFnSc4	ruka
hodně	hodně	k6eAd1	hodně
nataženou	natažený	k2eAgFnSc4d1	natažená
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
býval	bývat	k5eAaImAgInS	bývat
pořád	pořád	k6eAd1	pořád
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
s	s	k7c7	s
cigaretou	cigareta	k1gFnSc7	cigareta
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
nebo	nebo	k8xC	nebo
s	s	k7c7	s
whisky	whisky	k1gFnSc7	whisky
Jack	Jack	k1gMnSc1	Jack
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
mezi	mezi	k7c7	mezi
kytarou	kytara	k1gFnSc7	kytara
a	a	k8xC	a
basou	basa	k1gFnSc7	basa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měla	mít	k5eAaImAgFnS	mít
více	hodně	k6eAd2	hodně
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gInSc1	Slash
byl	být	k5eAaImAgInS	být
také	také	k9	také
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kytaristu	kytarista	k1gMnSc4	kytarista
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odůvodnil	odůvodnit	k5eAaPmAgInS	odůvodnit
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
populární	populární	k2eAgFnSc6d1	populární
a	a	k8xC	a
slavné	slavný	k2eAgFnSc3d1	slavná
kapele	kapela	k1gFnSc3	kapela
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
dostal	dostat	k5eAaPmAgMnS	dostat
údajně	údajně	k6eAd1	údajně
od	od	k7c2	od
babičky	babička	k1gFnSc2	babička
Stevena	Steven	k2eAgMnSc4d1	Steven
Adlera	Adler	k1gMnSc4	Adler
<g/>
,	,	kIx,	,
kamaráda	kamarád	k1gMnSc4	kamarád
a	a	k8xC	a
budoucího	budoucí	k2eAgMnSc4d1	budoucí
člena	člen	k1gMnSc4	člen
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
Duffem	Duff	k1gMnSc7	Duff
McKaganem	McKagan	k1gMnSc7	McKagan
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Aids	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Perlou	perla	k1gFnSc7	perla
Ferrar	Ferrara	k1gFnPc2	Ferrara
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
jsou	být	k5eAaImIp3nP	být
London	London	k1gMnSc1	London
Emilio	Emilio	k1gMnSc1	Emilio
a	a	k8xC	a
Cash	cash	k1gFnSc1	cash
Anthony	Anthona	k1gFnSc2	Anthona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
Slash	Slash	k1gMnSc1	Slash
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
"	"	kIx"	"
<g/>
nepřekonatelných	překonatelný	k2eNgInPc2d1	nepřekonatelný
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
vzal	vzít	k5eAaPmAgMnS	vzít
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Podal	podat	k5eAaPmAgMnS	podat
ji	on	k3xPp3gFnSc4	on
ovšem	ovšem	k9	ovšem
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Meegan	Meegan	k1gMnSc1	Meegan
Hodges	Hodges	k1gMnSc1	Hodges
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
již	již	k6eAd1	již
chodil	chodit	k5eAaImAgMnS	chodit
dříve	dříve	k6eAd2	dříve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
také	také	k9	také
ve	v	k7c6	v
známé	známý	k2eAgFnSc6d1	známá
videohře	videohra	k1gFnSc6	videohra
Guitar	Guitar	k1gMnSc1	Guitar
Hero	Hero	k1gMnSc1	Hero
3	[number]	k4	3
Legends	Legends	k1gInSc1	Legends
of	of	k?	of
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Saul	Saul	k1gMnSc1	Saul
Hudson	Hudson	k1gMnSc1	Hudson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Hampsteadu	Hampstead	k1gInSc6	Hampstead
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Ola	Ola	k1gFnSc1	Ola
Hudson	Hudson	k1gMnSc1	Hudson
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Oliver	Oliver	k1gInSc1	Oliver
<g/>
;	;	kIx,	;
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
afroamerická	afroamerický	k2eAgFnSc1d1	afroamerická
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejíž	jejíž	k3xOyRp3gMnPc4	jejíž
klienty	klient	k1gMnPc4	klient
patřili	patřit	k5eAaImAgMnP	patřit
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Diana	Diana	k1gFnSc1	Diana
Ross	Rossa	k1gFnPc2	Rossa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc1	Anthona
Hudson	Hudson	k1gMnSc1	Hudson
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
obaly	obal	k1gInPc4	obal
alb	alba	k1gFnPc2	alba
pro	pro	k7c4	pro
muzikanty	muzikant	k1gMnPc4	muzikant
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
Joni	Joni	k1gNnSc1	Joni
Mitchell	Mitchella	k1gFnPc2	Mitchella
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
několika	několik	k4yIc7	několik
zprávami	zpráva	k1gFnPc7	zpráva
Slashova	Slashova	k1gFnSc1	Slashova
matka	matka	k1gFnSc1	matka
nebyla	být	k5eNaImAgFnS	být
Nigerijka	Nigerijka	k1gFnSc1	Nigerijka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
nebyl	být	k5eNaImAgMnS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
k	k	k7c3	k
babičce	babička	k1gFnSc3	babička
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
ve	v	k7c4	v
Stoke-on-Trent	Stoken-Trent	k1gInSc4	Stoke-on-Trent
až	až	k9	až
do	do	k7c2	do
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gInSc1	Slash
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
budoucími	budoucí	k2eAgFnPc7d1	budoucí
hvězdami	hvězda	k1gFnPc7	hvězda
jako	jako	k8xC	jako
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gMnSc1	Kravitz
nebo	nebo	k8xC	nebo
Nicolas	Nicolas	k1gMnSc1	Nicolas
Cage	Cag	k1gFnSc2	Cag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
dostal	dostat	k5eAaPmAgMnS	dostat
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
hudební	hudební	k2eAgInPc1d1	hudební
vzory	vzor	k1gInPc1	vzor
byly	být	k5eAaImAgInP	být
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
Rory	Rora	k1gFnSc2	Rora
Gallagher	Gallaghra	k1gFnPc2	Gallaghra
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
a	a	k8xC	a
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
zahrát	zahrát	k5eAaPmF	zahrát
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Smoke	Smoke	k1gFnSc4	Smoke
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Water	Watra	k1gFnPc2	Watra
<g/>
"	"	kIx"	"
od	od	k7c2	od
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc2	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbily	líbit	k5eAaImAgFnP	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
hře	hra	k1gFnSc6	hra
na	na	k7c6	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pochopitelně	pochopitelně	k6eAd1	pochopitelně
mělo	mít	k5eAaImAgNnS	mít
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
studijní	studijní	k2eAgInPc4d1	studijní
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
Slash	Slash	k1gInSc4	Slash
nakonec	nakonec	k6eAd1	nakonec
studia	studio	k1gNnPc4	studio
zanechal	zanechat	k5eAaPmAgInS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
konkurzu	konkurz	k1gInSc3	konkurz
glam	glama	k1gFnPc2	glama
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Poison	Poison	k1gMnSc1	Poison
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
finalistou	finalista	k1gMnSc7	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
potkal	potkat	k5eAaPmAgMnS	potkat
bubeníka	bubeník	k1gMnSc4	bubeník
Stevena	Steven	k2eAgMnSc4d1	Steven
Adlera	Adler	k1gMnSc4	Adler
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
založili	založit	k5eAaPmAgMnP	založit
"	"	kIx"	"
<g/>
skupinu	skupina	k1gFnSc4	skupina
<g/>
"	"	kIx"	"
Road	Road	k1gMnSc1	Road
Crew	Crew	k1gMnSc1	Crew
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
potkali	potkat	k5eAaPmAgMnP	potkat
budoucího	budoucí	k2eAgMnSc4d1	budoucí
kytaristu	kytarista	k1gMnSc4	kytarista
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
Izzy	Izza	k1gFnSc2	Izza
Stradlina	Stradlina	k1gFnSc1	Stradlina
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
přehrál	přehrát	k5eAaPmAgMnS	přehrát
kazetu	kazeta	k1gFnSc4	kazeta
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
zpíval	zpívat	k5eAaImAgMnS	zpívat
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
kytarista	kytarista	k1gMnSc1	kytarista
Tracii	Tracie	k1gFnSc4	Tracie
Guns	Gunsa	k1gFnPc2	Gunsa
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Rob	roba	k1gFnPc2	roba
Gardner	Gardner	k1gInSc4	Gardner
Axlovy	Axlův	k2eAgFnSc2d1	Axlova
nové	nový	k2eAgFnSc2d1	nová
skupiny	skupina	k1gFnSc2	skupina
Hollywood	Hollywood	k1gInSc1	Hollywood
Rose	Rose	k1gMnSc1	Rose
nemohli	moct	k5eNaImAgMnP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
vystoupeních	vystoupení	k1gNnPc6	vystoupení
v	v	k7c6	v
Seattle	Seattle	k1gFnSc2	Seattle
<g/>
,	,	kIx,	,
Slash	Slash	k1gMnSc1	Slash
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
připojili	připojit	k5eAaPmAgMnP	připojit
jako	jako	k9	jako
noví	nový	k2eAgMnPc1d1	nový
členové	člen	k1gMnPc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turné	turné	k1gNnSc6	turné
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
Tour	Toura	k1gFnPc2	Toura
se	se	k3xPyFc4	se
Slash	Slash	k1gMnSc1	Slash
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
občanem	občan	k1gMnSc7	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
působení	působení	k1gNnSc2	působení
v	v	k7c4	v
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
společné	společný	k2eAgNnSc4d1	společné
vystoupení	vystoupení	k1gNnSc4	vystoupení
s	s	k7c7	s
takovými	takový	k3xDgMnPc7	takový
rozličnými	rozličný	k2eAgMnPc7d1	rozličný
umělci	umělec	k1gMnPc7	umělec
jako	jako	k8xC	jako
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gMnSc1	Kravitz
<g/>
,	,	kIx,	,
BLACKstreet	BLACKstreet	k1gMnSc1	BLACKstreet
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
a	a	k8xC	a
Queen	Queen	k2eAgMnSc1d1	Queen
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gInSc1	Slash
je	být	k5eAaImIp3nS	být
neslavně	slavně	k6eNd1	slavně
známý	známý	k2eAgInSc1d1	známý
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
na	na	k7c4	na
American	American	k1gInSc4	American
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
první	první	k4xOgFnSc2	první
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Duff	Duff	k1gMnSc1	Duff
se	se	k3xPyFc4	se
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
dovalili	dovalit	k5eAaPmAgMnP	dovalit
zjevně	zjevně	k6eAd1	zjevně
opilí	opilý	k2eAgMnPc1d1	opilý
<g/>
,	,	kIx,	,
drželi	držet	k5eAaImAgMnP	držet
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
a	a	k8xC	a
kouřili	kouřit	k5eAaImAgMnP	kouřit
cigarety	cigareta	k1gFnPc4	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nejasné	jasný	k2eNgFnSc6d1	nejasná
řeči	řeč	k1gFnSc6	řeč
stihl	stihnout	k5eAaPmAgMnS	stihnout
dvakrát	dvakrát	k6eAd1	dvakrát
zanadávat	zanadávat	k5eAaPmF	zanadávat
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
kamera	kamera	k1gFnSc1	kamera
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
oponu	opona	k1gFnSc4	opona
s	s	k7c7	s
logem	logo	k1gNnSc7	logo
AMA	AMA	kA	AMA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyslovení	vyslovení	k1gNnSc6	vyslovení
první	první	k4xOgFnSc2	první
nadávky	nadávka	k1gFnSc2	nadávka
se	se	k3xPyFc4	se
z	z	k7c2	z
davu	dav	k1gInSc2	dav
ozvalo	ozvat	k5eAaPmAgNnS	ozvat
lapaní	lapaný	k2eAgMnPc1d1	lapaný
po	po	k7c6	po
dechu	dech	k1gInSc6	dech
<g/>
,	,	kIx,	,
nato	nato	k6eAd1	nato
si	se	k3xPyFc3	se
Slash	Slash	k1gMnSc1	Slash
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
ústa	ústa	k1gNnPc4	ústa
a	a	k8xC	a
se	s	k7c7	s
smíchem	smích	k1gInSc7	smích
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
oops	oops	k6eAd1	oops
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gInSc1	Roses
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
28	[number]	k4	28
<g/>
měsíční	měsíční	k2eAgInSc4d1	měsíční
turné	turné	k1gNnSc1	turné
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
Tour	Tour	k1gInSc1	Tour
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
krylo	krýt	k5eAaImAgNnS	krýt
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
jejich	jejich	k3xOp3gFnPc2	jejich
nových	nový	k2eAgFnPc2d1	nová
alb	alba	k1gFnPc2	alba
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc1	Your
Illusion	Illusion	k1gInSc4	Illusion
I	i	k9	i
a	a	k8xC	a
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
od	od	k7c2	od
předešlých	předešlý	k2eAgMnPc2d1	předešlý
lišila	lišit	k5eAaImAgFnS	lišit
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
víc	hodně	k6eAd2	hodně
estetických	estetický	k2eAgFnPc2d1	estetická
a	a	k8xC	a
dramatických	dramatický	k2eAgFnPc2d1	dramatická
písní	píseň	k1gFnPc2	píseň
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
November	November	k1gInSc1	November
Rain	Rain	k1gInSc1	Rain
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Estranged	Estranged	k1gInSc1	Estranged
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
baladami	balada	k1gFnPc7	balada
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Cry	Cry	k1gMnSc1	Cry
<g/>
"	"	kIx"	"
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
napětí	napětí	k1gNnSc3	napětí
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
členů	člen	k1gInPc2	člen
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
toto	tento	k3xDgNnSc4	tento
poté	poté	k6eAd1	poté
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k9	jako
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
složku	složka	k1gFnSc4	složka
jeho	jeho	k3xOp3gFnSc2	jeho
neschopnosti	neschopnost	k1gFnSc2	neschopnost
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
Axlem	Axl	k1gInSc7	Axl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neblaze	blaze	k6eNd1	blaze
známé	známý	k2eAgNnSc1d1	známé
světové	světový	k2eAgNnSc1d1	světové
turné	turné	k1gNnSc1	turné
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
a	a	k8xC	a
Slash	Slash	k1gMnSc1	Slash
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jméno	jméno	k1gNnSc4	jméno
mezi	mezi	k7c7	mezi
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
kytaristy	kytarista	k1gMnPc7	kytarista
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
každé	každý	k3xTgNnSc1	každý
vystoupení	vystoupení	k1gNnSc1	vystoupení
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jeho	jeho	k3xOp3gInSc4	jeho
sólo	sólo	k2eAgInSc4d1	sólo
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc4d1	trvající
5	[number]	k4	5
až	až	k9	až
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
dav	dav	k1gInSc1	dav
ohromoval	ohromovat	k5eAaImAgInS	ohromovat
svým	svůj	k3xOyFgNnSc7	svůj
výjimečným	výjimečný	k2eAgNnSc7d1	výjimečné
nadáním	nadání	k1gNnSc7	nadání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
po	po	k7c6	po
vydaní	vydaný	k2eAgMnPc1d1	vydaný
alba	album	k1gNnPc4	album
The	The	k1gMnPc2	The
Spaghetti	spaghetti	k1gInPc2	spaghetti
Incident	incident	k1gInSc4	incident
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgNnSc1d1	poslední
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
znova	znova	k6eAd1	znova
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
a	a	k8xC	a
1995	[number]	k4	1995
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
projektem	projekt	k1gInSc7	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nazval	nazvat	k5eAaPmAgInS	nazvat
Slash	Slash	k1gInSc4	Slash
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc4	člen
byli	být	k5eAaImAgMnP	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kytarista	kytarista	k1gMnSc1	kytarista
GN	GN	kA	GN
<g/>
'	'	kIx"	'
<g/>
R	R	kA	R
Gilby	Gilba	k1gMnSc2	Gilba
Clarke	Clark	k1gMnSc2	Clark
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Matt	Matt	k2eAgInSc4d1	Matt
Sorum	Sorum	k1gInSc4	Sorum
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydali	vydat	k5eAaPmAgMnP	vydat
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
It	It	k1gFnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Five	Five	k1gFnSc7	Five
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Clock	Clock	k1gInSc4	Clock
Somewhere	Somewher	k1gInSc5	Somewher
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Jacksonem	Jackson	k1gMnSc7	Jackson
na	na	k7c6	na
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Black	Blacka	k1gFnPc2	Blacka
Or	Or	k1gMnSc5	Or
White	Whit	k1gMnSc5	Whit
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
90	[number]	k4	90
<g/>
sekundové	sekundový	k2eAgFnPc1d1	sekundová
sólo	sólo	k2eAgFnPc1d1	sólo
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgNnSc2	který
si	se	k3xPyFc3	se
Jackson	Jackson	k1gMnSc1	Jackson
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
oblek	oblek	k1gInSc4	oblek
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
píseň	píseň	k1gFnSc4	píseň
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
Jacksonem	Jackson	k1gInSc7	Jackson
znovu	znovu	k6eAd1	znovu
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
Jacksonova	Jacksonův	k2eAgFnSc1d1	Jacksonova
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
opustil	opustit	k5eAaPmAgMnS	opustit
GN	GN	kA	GN
<g/>
'	'	kIx"	'
<g/>
R	R	kA	R
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
dál	daleko	k6eAd2	daleko
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
Axlem	Axl	k1gInSc7	Axl
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
neshod	neshoda	k1gFnPc2	neshoda
s	s	k7c7	s
Axlem	Axl	k1gInSc7	Axl
kvůli	kvůli	k7c3	kvůli
hudebnímu	hudební	k2eAgInSc3d1	hudební
směru	směr	k1gInSc3	směr
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
ubírá	ubírat	k5eAaImIp3nS	ubírat
a	a	k8xC	a
také	také	k9	také
za	za	k7c4	za
způsob	způsob	k1gInSc4	způsob
vedení	vedení	k1gNnSc2	vedení
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rozzuřený	rozzuřený	k2eAgInSc1d1	rozzuřený
<g/>
,	,	kIx,	,
když	když	k8xS	když
Axl	Axl	k1gMnSc1	Axl
nahradil	nahradit	k5eAaPmAgMnS	nahradit
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
pasáž	pasáž	k1gFnSc4	pasáž
Gilbyho	Gilby	k1gMnSc2	Gilby
Clarka	Clarek	k1gMnSc2	Clarek
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Sympathy	Sympatha	k1gFnPc1	Sympatha
for	forum	k1gNnPc2	forum
the	the	k?	the
Devil	Devil	k1gMnSc1	Devil
<g/>
"	"	kIx"	"
za	za	k7c4	za
nahrávku	nahrávka	k1gFnSc4	nahrávka
Paula	Paul	k1gMnSc2	Paul
Tobiase	Tobiasa	k1gFnSc6	Tobiasa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
Slash	Slash	k1gInSc4	Slash
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gMnSc7	Snakepit
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgInS	odehrát
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Snakepit	Snakepit	k1gFnSc4	Snakepit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
znova	znova	k6eAd1	znova
se	se	k3xPyFc4	se
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2001	[number]	k4	2001
po	po	k7c6	po
vydaní	vydaný	k2eAgMnPc1d1	vydaný
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
Ain	Ain	k1gFnPc2	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Life	Lif	k1gMnSc4	Lif
Grand	grand	k1gMnSc1	grand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
na	na	k7c6	na
comebackové	comebackový	k2eAgFnSc6d1	comebackový
nahrávce	nahrávka	k1gFnSc6	nahrávka
"	"	kIx"	"
<g/>
Birdland	Birdland	k1gInSc1	Birdland
<g/>
"	"	kIx"	"
skupiny	skupina	k1gFnSc2	skupina
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
nové	nový	k2eAgFnPc4d1	nová
nahrávky	nahrávka	k1gFnPc4	nahrávka
jejich	jejich	k3xOp3gInPc2	jejich
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
a	a	k8xC	a
na	na	k7c6	na
vystoupeních	vystoupení	k1gNnPc6	vystoupení
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
s	s	k7c7	s
bývalými	bývalý	k2eAgMnPc7d1	bývalý
členy	člen	k1gMnPc7	člen
(	(	kIx(	(
<g/>
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Satriani	Satrian	k1gMnPc1	Satrian
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Vai	Vai	k1gMnSc1	Vai
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Lukather	Lukathra	k1gFnPc2	Lukathra
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
"	"	kIx"	"
<g/>
Skunk	skunk	k1gMnSc1	skunk
<g/>
"	"	kIx"	"
Baxter	Baxter	k1gMnSc1	Baxter
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnPc1	Johnna
Rzeznik	Rzeznik	k1gMnSc1	Rzeznik
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Ditchum	Ditchum	k1gInSc1	Ditchum
a	a	k8xC	a
Simon	Simon	k1gMnSc1	Simon
McCarty	McCarta	k1gFnSc2	McCarta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
alba	album	k1gNnSc2	album
Favored	Favored	k1gMnSc1	Favored
Nations	Nations	k1gInSc1	Nations
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
Over	Overa	k1gFnPc2	Overa
<g/>
,	,	kIx,	,
Under	Undra	k1gFnPc2	Undra
<g/>
,	,	kIx,	,
Sideways	Sidewaysa	k1gFnPc2	Sidewaysa
<g/>
,	,	kIx,	,
Down	Downa	k1gFnPc2	Downa
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
nového	nový	k2eAgNnSc2d1	nové
století	století	k1gNnSc2	století
hrával	hrávat	k5eAaImAgMnS	hrávat
s	s	k7c7	s
mnohými	mnohý	k2eAgFnPc7d1	mnohá
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
na	na	k7c6	na
albech	album	k1gNnPc6	album
a	a	k8xC	a
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Hrával	hrávat	k5eAaImAgInS	hrávat
rozličné	rozličný	k2eAgInPc4d1	rozličný
hudební	hudební	k2eAgInPc4d1	hudební
žánry	žánr	k1gInPc4	žánr
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
nebyl	být	k5eNaImAgMnS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Duff	Duff	k1gMnSc1	Duff
McKagan	McKagany	k1gInPc2	McKagany
a	a	k8xC	a
Matt	Matt	k2eAgInSc4d1	Matt
Sorum	Sorum	k1gInSc4	Sorum
znovu	znovu	k6eAd1	znovu
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
rozlučkovém	rozlučkový	k2eAgInSc6d1	rozlučkový
koncertu	koncert	k1gInSc6	koncert
bubeníka	bubeník	k1gMnSc2	bubeník
Randy	rand	k1gInPc4	rand
Castilla	Castill	k1gMnSc2	Castill
<g/>
.	.	kIx.	.
</s>
<s>
Pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
stále	stále	k6eAd1	stále
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
spolu	spolu	k6eAd1	spolu
založit	založit	k5eAaPmF	založit
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velvet	Velveta	k1gFnPc2	Velveta
Revolver	revolver	k1gInSc1	revolver
==	==	k?	==
</s>
</p>
<p>
<s>
Velvet	Velvet	k1gInSc1	Velvet
Revolver	revolver	k1gInSc1	revolver
začínal	začínat	k5eAaImAgInS	začínat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Project	Project	k1gMnSc1	Project
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
Duff	Duff	k1gMnSc1	Duff
a	a	k8xC	a
Matt	Matt	k2eAgMnSc1d1	Matt
zariskovali	zariskovat	k5eAaPmAgMnP	zariskovat
a	a	k8xC	a
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
zpěváka	zpěvák	k1gMnSc4	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
kytaristou	kytarista	k1gMnSc7	kytarista
byl	být	k5eAaImAgInS	být
Izzy	Izza	k1gFnSc2	Izza
Stradlin	Stradlin	k1gInSc1	Stradlin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolupráce	spolupráce	k1gFnSc1	spolupráce
nefungovala	fungovat	k5eNaImAgFnS	fungovat
jak	jak	k8xS	jak
měla	mít	k5eAaImAgFnS	mít
protože	protože	k8xS	protože
Izzy	Izza	k1gFnSc2	Izza
měl	mít	k5eAaImAgInS	mít
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
vyrážení	vyrážení	k1gNnSc3	vyrážení
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
také	také	k9	také
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
kytaristou	kytarista	k1gMnSc7	kytarista
stal	stát	k5eAaPmAgInS	stát
Dave	Dav	k1gInSc5	Dav
Kushner	Kushner	k1gMnSc1	Kushner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předtím	předtím	k6eAd1	předtím
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Duffem	Duff	k1gInSc7	Duff
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
měsíců	měsíc	k1gInPc2	měsíc
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
demo	demo	k2eAgFnPc4d1	demo
nahrávky	nahrávka	k1gFnPc4	nahrávka
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
monotónní	monotónní	k2eAgInSc4d1	monotónní
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zdokumentován	zdokumentován	k2eAgInSc4d1	zdokumentován
stanicí	stanice	k1gFnSc7	stanice
VH	VH	kA	VH
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
připraveni	připravit	k5eAaPmNgMnP	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vykašlat	vykašlat	k5eAaPmF	vykašlat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
skupina	skupina	k1gFnSc1	skupina
Stone	ston	k1gInSc5	ston
Temple	templ	k1gInSc5	templ
Pilots	Pilotsa	k1gFnPc2	Pilotsa
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
přestávku	přestávka	k1gFnSc4	přestávka
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Scott	Scott	k2eAgMnSc1d1	Scott
Weiland	Weiland	k1gInSc4	Weiland
si	se	k3xPyFc3	se
dodal	dodat	k5eAaPmAgMnS	dodat
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
si	se	k3xPyFc3	se
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Project	Project	k1gMnSc1	Project
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gMnSc1	Slash
<g/>
,	,	kIx,	,
Duff	Duff	k1gMnSc1	Duff
<g/>
,	,	kIx,	,
a	a	k8xC	a
Matt	Matt	k1gMnSc1	Matt
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
začali	začít	k5eAaPmAgMnP	začít
okamžitě	okamžitě	k6eAd1	okamžitě
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
Velvet	Velvet	k1gInSc1	Velvet
Revolver	revolver	k1gInSc1	revolver
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
bývalých	bývalý	k2eAgInPc2d1	bývalý
členů	člen	k1gInPc2	člen
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
a	a	k8xC	a
Grungeového	Grungeový	k2eAgMnSc2d1	Grungeový
zpěváka	zpěvák	k1gMnSc2	zpěvák
Scotta	Scott	k1gInSc2	Scott
Weilanda	Weilando	k1gNnSc2	Weilando
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
unikátní	unikátní	k2eAgInSc1d1	unikátní
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
mixující	mixující	k2eAgNnSc1d1	mixující
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
z	z	k7c2	z
hardrocku	hardrock	k1gInSc2	hardrock
a	a	k8xC	a
grunge	grung	k1gFnSc2	grung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Set	set	k1gInSc1	set
Me	Me	k1gMnSc2	Me
Free	Fre	k1gMnSc2	Fre
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Hulk	Hulka	k1gFnPc2	Hulka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Contraband	Contrabanda	k1gFnPc2	Contrabanda
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
19	[number]	k4	19
<g/>
-měsíční	ěsíčnět	k5eAaPmIp3nS	-měsíčnět
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
platinovým	platinový	k2eAgInSc7d1	platinový
a	a	k8xC	a
Slash	Slash	k1gInSc1	Slash
se	se	k3xPyFc4	se
připomenul	připomenout	k5eAaPmAgInS	připomenout
masám	masa	k1gFnPc3	masa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
za	za	k7c4	za
ty	ten	k3xDgInPc4	ten
roky	rok	k1gInPc4	rok
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
zasloužený	zasloužený	k2eAgInSc4d1	zasloužený
oddych	oddych	k1gInSc4	oddych
před	před	k7c7	před
nahráváním	nahrávání	k1gNnSc7	nahrávání
svého	své	k1gNnSc2	své
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
nahrála	nahrát	k5eAaPmAgFnS	nahrát
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Monster	monstrum	k1gNnPc2	monstrum
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Libertad	Libertad	k1gInSc4	Libertad
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
ze	z	k7c2	z
staly	stát	k5eAaPmAgInP	stát
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Builds	Buildsa	k1gFnPc2	Buildsa
Quick	Quick	k1gMnSc1	Quick
Machines	Machines	k1gMnSc1	Machines
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Fight	Fight	k1gMnSc1	Fight
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Get	Get	k1gFnSc1	Get
Out	Out	k1gFnSc2	Out
the	the	k?	the
Door	Door	k1gInSc1	Door
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
začátkem	začátkem	k7c2	začátkem
konce	konec	k1gInSc2	konec
účinkování	účinkování	k1gNnSc2	účinkování
Scotta	Scotto	k1gNnSc2	Scotto
Weilanda	Weilando	k1gNnSc2	Weilando
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
stupňovalo	stupňovat	k5eAaImAgNnS	stupňovat
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
kapela	kapela	k1gFnSc1	kapela
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Scott	Scott	k2eAgInSc1d1	Scott
Weiland	Weiland	k1gInSc1	Weiland
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Velvet	Velveta	k1gFnPc2	Velveta
Revolver	revolver	k1gInSc1	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nedochvilnost	nedochvilnost	k1gFnSc1	nedochvilnost
na	na	k7c4	na
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Velvet	Velvet	k1gMnSc1	Velvet
Revolver	revolver	k1gInSc1	revolver
bez	bez	k7c2	bez
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
nedaří	dařit	k5eNaImIp3nS	dařit
v	v	k7c6	v
konkurzech	konkurz	k1gInPc6	konkurz
najít	najít	k5eAaPmF	najít
toho	ten	k3xDgNnSc2	ten
pravého	pravý	k2eAgNnSc2d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
nerozpadla	rozpadnout	k5eNaPmAgFnS	rozpadnout
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
plánuje	plánovat	k5eAaImIp3nS	plánovat
vydat	vydat	k5eAaPmF	vydat
své	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Sorum	Sorum	k1gInSc1	Sorum
i	i	k8xC	i
Slash	Slash	k1gInSc1	Slash
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
oznámení	oznámení	k1gNnSc3	oznámení
nového	nový	k2eAgMnSc2d1	nový
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Slash	Slash	k1gInSc1	Slash
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednají	jednat	k5eAaImIp3nP	jednat
s	s	k7c7	s
Corey	Corey	k1gInPc7	Corey
Taylorem	Taylor	k1gInSc7	Taylor
(	(	kIx(	(
<g/>
Slipknot	Slipknota	k1gFnPc2	Slipknota
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Sour	Soura	k1gFnPc2	Soura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
učiněno	učinit	k5eAaImNgNnS	učinit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgInS	vydat
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Slash	Slash	k1gInSc1	Slash
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovalo	spolupracovat	k5eAaImAgNnS	spolupracovat
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
interpretů	interpret	k1gMnPc2	interpret
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Ozzy	Ozzy	k1gInPc1	Ozzy
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
Kid	Kid	k1gFnPc1	Kid
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
Lemmy	lemma	k1gFnPc1	lemma
Kilmister	Kilmister	k1gInSc1	Kilmister
<g/>
,	,	kIx,	,
Chris	Chris	k1gInSc1	Chris
Cornell	Cornella	k1gFnPc2	Cornella
nebo	nebo	k8xC	nebo
Andrew	Andrew	k1gFnPc2	Andrew
Stockdale	Stockdala	k1gFnSc3	Stockdala
z	z	k7c2	z
Wolfmother	Wolfmothra	k1gFnPc2	Wolfmothra
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
remix	remix	k1gInSc1	remix
megahitu	megahit	k1gInSc2	megahit
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
,,	,,	k?	,,
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Fergie	Fergie	k1gFnSc1	Fergie
a	a	k8xC	a
rapová	rapový	k2eAgFnSc1d1	rapová
formace	formace	k1gFnSc1	formace
Cypress	Cypressa	k1gFnPc2	Cypressa
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
bonbónkem	bonbónek	k1gInSc7	bonbónek
pro	pro	k7c4	pro
rockové	rockový	k2eAgMnPc4d1	rockový
fanoušky	fanoušek	k1gMnPc4	fanoušek
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
skladba	skladba	k1gFnSc1	skladba
,,	,,	k?	,,
<g/>
Watch	Watch	k1gInSc1	Watch
This	This	k1gInSc1	This
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Slashem	Slash	k1gInSc7	Slash
nahráli	nahrát	k5eAaBmAgMnP	nahrát
Duff	Duff	k1gInSc4	Duff
McKagan	McKagany	k1gInPc2	McKagany
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
frontman	frontman	k1gMnSc1	frontman
Foo	Foo	k1gFnSc2	Foo
Fighters	Fighters	k1gInSc1	Fighters
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnPc2	Grohl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
vyjel	vyjet	k5eAaPmAgInS	vyjet
na	na	k7c4	na
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Slashovu	Slashův	k2eAgFnSc4d1	Slashův
kapelu	kapela	k1gFnSc4	kapela
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
tvořili	tvořit	k5eAaImAgMnP	tvořit
Todd	Todd	k1gMnSc1	Todd
Kerns	Kerns	k1gInSc1	Kerns
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bobby	Bobb	k1gInPc1	Bobb
Schneck	Schneck	k1gInSc1	Schneck
(	(	kIx(	(
<g/>
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brent	Brent	k?	Brent
Fitz	Fitz	k1gInSc1	Fitz
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
vokalistou	vokalista	k1gMnSc7	vokalista
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Myles	Myles	k1gMnSc1	Myles
Kennedy	Kenneda	k1gMnSc2	Kenneda
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Alter	Altra	k1gFnPc2	Altra
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
otextoval	otextovat	k5eAaPmAgInS	otextovat
a	a	k8xC	a
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
na	na	k7c6	na
albu	album	k1gNnSc6	album
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
Starlight	Starlight	k1gInSc1	Starlight
<g/>
,	,	kIx,	,
Back	Back	k1gInSc1	Back
From	Froma	k1gFnPc2	Froma
Cali	Cal	k1gFnSc2	Cal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Mylesem	Myles	k1gInSc7	Myles
Kennedym	Kennedym	k1gInSc1	Kennedym
plánuje	plánovat	k5eAaImIp3nS	plánovat
Slash	Slash	k1gInSc4	Slash
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
i	i	k9	i
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Připravil	připravit	k5eAaPmAgMnS	připravit
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Apocalyptic	Apocalyptice	k1gFnPc2	Apocalyptice
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
zpěvákem	zpěvák	k1gMnSc7	zpěvák
pouze	pouze	k6eAd1	pouze
Myles	Myles	k1gInSc4	Myles
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
Slash	Slash	k1gInSc1	Slash
zastavil	zastavit	k5eAaPmAgInS	zastavit
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgInS	odehrát
vynikající	vynikající	k2eAgInSc1d1	vynikající
koncert	koncert	k1gInSc1	koncert
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
v	v	k7c6	v
Tesla	Tesla	k1gFnSc1	Tesla
aréně	aréna	k1gFnSc6	aréna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
Slashově	Slashův	k2eAgNnSc6d1	Slashův
rodišti	rodiště	k1gNnSc6	rodiště
<g/>
,	,	kIx,	,
Stoke	Stoke	k1gNnSc6	Stoke
On	on	k3xPp3gMnSc1	on
Trent	Trent	k1gMnSc1	Trent
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
oslavily	oslavit	k5eAaPmAgFnP	oslavit
Slashovy	Slashova	k1gFnPc1	Slashova
46	[number]	k4	46
narozeniny	narozeniny	k1gFnPc1	narozeniny
a	a	k8xC	a
natočilo	natočit	k5eAaBmAgNnS	natočit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
live	live	k1gFnSc1	live
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Slash	Slash	k1gInSc1	Slash
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
Album	album	k1gNnSc1	album
of	of	k?	of
The	The	k1gFnSc1	The
Year	Year	k1gInSc1	Year
2010	[number]	k4	2010
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Classic	Classice	k1gInPc2	Classice
Rock	rock	k1gInSc1	rock
Awards	Awards	k1gInSc1	Awards
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Slash	Slash	k1gInSc4	Slash
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mylesem	Myles	k1gInSc7	Myles
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
a	a	k8xC	a
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
The	The	k1gFnSc4	The
Conspirators	Conspiratorsa	k1gFnPc2	Conspiratorsa
<g/>
,	,	kIx,	,
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
albu	album	k1gNnSc3	album
Apocalyptic	Apocalyptice	k1gFnPc2	Apocalyptice
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
hráli	hrát	k5eAaImAgMnP	hrát
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
sále	sál	k1gInSc6	sál
Lucerny	lucerna	k1gFnSc2	lucerna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
==	==	k?	==
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Duffem	Duff	k1gMnSc7	Duff
McKaganem	McKagan	k1gMnSc7	McKagan
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
znovu	znovu	k6eAd1	znovu
členy	člen	k1gMnPc4	člen
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
to	ten	k3xDgNnSc1	ten
takhle	takhle	k6eAd1	takhle
skončí	skončit	k5eAaPmIp3nS	skončit
ale	ale	k8xC	ale
vysvětlovala	vysvětlovat	k5eAaImAgFnS	vysvětlovat
i	i	k9	i
interview	interview	k1gNnSc4	interview
z	z	k7c2	z
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
kde	kde	k6eAd1	kde
Slash	Slash	k1gMnSc1	Slash
pronesl	pronést	k5eAaPmAgMnS	pronést
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
GNR	GNR	kA	GNR
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
neříkej	říkat	k5eNaImRp2nS	říkat
nikdy	nikdy	k6eAd1	nikdy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
pak	pak	k6eAd1	pak
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
zlepšení	zlepšení	k1gNnSc4	zlepšení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
Axlem	Axlem	k1gInSc1	Axlem
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
slova	slovo	k1gNnPc1	slovo
(	(	kIx(	(
<g/>
k	k	k7c3	k
reunionu	reunion	k1gInSc3	reunion
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
chybí	chybit	k5eAaPmIp3nP	chybit
Izzy	Izza	k1gFnPc1	Izza
Stradlin	Stradlina	k1gFnPc2	Stradlina
<g/>
,	,	kIx,	,
Matt	Matt	k2eAgInSc1d1	Matt
Sorum	Sorum	k1gInSc1	Sorum
<g/>
,	,	kIx,	,
Gilby	Gilb	k1gInPc1	Gilb
Clarke	Clarke	k1gInSc1	Clarke
a	a	k8xC	a
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
i	i	k8xC	i
Steven	Steven	k2eAgMnSc1d1	Steven
Adler	Adler	k1gMnSc1	Adler
který	který	k3yRgMnSc1	který
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
občas	občas	k6eAd1	občas
zahraje	zahrát	k5eAaPmIp3nS	zahrát
na	na	k7c6	na
vybraných	vybraný	k2eAgInPc6d1	vybraný
koncertech	koncert	k1gInPc6	koncert
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
že	že	k8xS	že
by	by	kYmCp3nS	by
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
dobrých	dobrý	k2eAgInPc2d1	dobrý
důvodů	důvod	k1gInPc2	důvod
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
fanouškům	fanoušek	k1gMnPc3	fanoušek
šel	jít	k5eAaImAgInS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Troubadour	Troubadoura	k1gFnPc2	Troubadoura
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
ve	v	k7c6	v
staro-nové	staroový	k2eAgFnSc6d1	staro-nová
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
tak	tak	k6eAd1	tak
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
23	[number]	k4	23
let	léto	k1gNnPc2	léto
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
Not	nota	k1gFnPc2	nota
In	In	k1gFnPc2	In
This	This	k1gInSc1	This
Lifetime	Lifetim	k1gInSc5	Lifetim
<g/>
...	...	k?	...
<g/>
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
===	===	k?	===
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
G	G	kA	G
N	N	kA	N
<g/>
'	'	kIx"	'
R	R	kA	R
Lies	Lies	k1gInSc1	Lies
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc1	Your
Illusion	Illusion	k1gInSc4	Illusion
I	i	k9	i
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
II	II	kA	II
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
The	The	k1gMnSc1	The
Spaghetti	spaghetti	k1gInPc2	spaghetti
Incident	incident	k1gInSc4	incident
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
===	===	k?	===
Se	s	k7c7	s
Slash	Slasha	k1gFnPc2	Slasha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Snakepit	Snakepit	k1gFnSc7	Snakepit
===	===	k?	===
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Five	Five	k1gFnSc7	Five
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Clock	Clock	k1gInSc4	Clock
Somewhere	Somewher	k1gInSc5	Somewher
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Ain	Ain	k1gFnSc1	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Life	Life	k1gInSc1	Life
Grand	grand	k1gMnSc1	grand
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
Velvet	Velvet	k1gInSc1	Velvet
Revolver	revolver	k1gInSc4	revolver
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Contraband	Contraband	k1gInSc1	Contraband
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Libertad	Libertad	k1gInSc1	Libertad
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
EP	EP	kA	EP
Melody	Meloda	k1gFnSc2	Meloda
and	and	k?	and
the	the	k?	the
Tyranny	Tyranna	k1gFnSc2	Tyranna
</s>
</p>
<p>
<s>
===	===	k?	===
Sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Slash	Slash	k1gInSc1	Slash
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Apocalyptic	Apocalyptice	k1gFnPc2	Apocalyptice
Love	lov	k1gInSc5	lov
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
World	World	k1gInSc1	World
on	on	k3xPp3gMnSc1	on
Fire	Fire	k1gFnSc5	Fire
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
Living	Living	k1gInSc4	Living
The	The	k1gMnPc2	The
Dream	Dream	k1gInSc1	Dream
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slash	Slasha	k1gFnPc2	Slasha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Slash	Slash	k1gInSc1	Slash
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Blog	Blog	k1gMnSc1	Blog
</s>
</p>
