<s>
Gaius	Gaius	k1gMnSc1	Gaius
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
(	(	kIx(	(
<g/>
soudobá	soudobý	k2eAgFnSc1d1	soudobá
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
kaisar	kaisar	k1gInSc1	kaisar
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
cézar	cézar	k1gInSc1	cézar
<g/>
]	]	kIx)	]
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
či	či	k8xC	či
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
100	[number]	k4	100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
mužů	muž	k1gMnPc2	muž
antické	antický	k2eAgFnSc2d1	antická
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Sehrál	sehrát	k5eAaPmAgInS	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zániku	zánik	k1gInSc2	zánik
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
transformace	transformace	k1gFnSc2	transformace
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
titul	titul	k1gInSc4	titul
caesar	caesara	k1gFnPc2	caesara
a	a	k8xC	a
odvozené	odvozený	k2eAgInPc4d1	odvozený
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
kejser	kejser	k1gInSc1	kejser
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
cesarz	cesarz	k1gMnSc1	cesarz
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
ale	ale	k9	ale
sám	sám	k3xTgMnSc1	sám
Caesar	Caesar	k1gMnSc1	Caesar
nebyl	být	k5eNaImAgMnS	být
<g/>
;	;	kIx,	;
prvním	první	k4xOgMnSc6	první
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
prasynovec	prasynovec	k1gMnSc1	prasynovec
a	a	k8xC	a
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Augustus	Augustus	k1gMnSc1	Augustus
(	(	kIx(	(
<g/>
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
14	[number]	k4	14
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
politické	politický	k2eAgFnSc2d1	politická
frakce	frakce	k1gFnSc2	frakce
populárů	populár	k1gMnPc2	populár
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Marcem	Marce	k1gNnSc7	Marce
Liciniem	Licinium	k1gNnSc7	Licinium
Crassem	Crass	k1gMnSc7	Crass
a	a	k8xC	a
Gnaeem	Gnaeus	k1gMnSc7	Gnaeus
Pompeiem	Pompeius	k1gMnSc7	Pompeius
Magnem	Magn	k1gMnSc7	Magn
tzv.	tzv.	kA	tzv.
první	první	k4xOgInSc4	první
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dominoval	dominovat	k5eAaImAgInS	dominovat
římské	římský	k2eAgFnSc3d1	římská
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovy	Caesarův	k2eAgInPc1d1	Caesarův
výboje	výboj	k1gInPc1	výboj
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
římskou	římska	k1gFnSc7	římska
moc	moc	k6eAd1	moc
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
dále	daleko	k6eAd2	daleko
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Říman	Říman	k1gMnSc1	Říman
pronikl	proniknout	k5eAaPmAgMnS	proniknout
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
Rýn	rýna	k1gFnPc2	rýna
<g/>
.	.	kIx.	.
</s>
<s>
Rozpad	rozpad	k1gInSc1	rozpad
triumvirátu	triumvirát	k1gInSc2	triumvirát
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Crassa	Crass	k1gMnSc2	Crass
přivedl	přivést	k5eAaPmAgMnS	přivést
Caesara	Caesar	k1gMnSc4	Caesar
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Pompeiem	Pompeius	k1gMnSc7	Pompeius
podporovaným	podporovaný	k2eAgMnSc7d1	podporovaný
římským	římský	k2eAgInSc7d1	římský
senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Překročením	překročení	k1gNnSc7	překročení
řeky	řeka	k1gFnSc2	řeka
Rubikonu	Rubikon	k1gInSc2	Rubikon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
49	[number]	k4	49
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Caesar	Caesar	k1gMnSc1	Caesar
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
a	a	k8xC	a
nesporným	sporný	k2eNgMnSc7d1	nesporný
vládcem	vládce	k1gMnSc7	vládce
celého	celý	k2eAgInSc2d1	celý
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uchopení	uchopení	k1gNnSc6	uchopení
moci	moc	k1gFnSc2	moc
zahájil	zahájit	k5eAaPmAgMnS	zahájit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
reformy	reforma	k1gFnPc4	reforma
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
doživotním	doživotní	k2eAgMnSc7d1	doživotní
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
podnítil	podnítit	k5eAaPmAgMnS	podnítit
Marca	Marcus	k1gMnSc4	Marcus
Junia	Junius	k1gMnSc4	Junius
Bruta	Brut	k1gMnSc4	Brut
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
římských	římský	k2eAgMnPc2d1	římský
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
o	o	k7c6	o
březnových	březnový	k2eAgFnPc6d1	březnová
idách	idy	k1gFnPc6	idy
roku	rok	k1gInSc2	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Strůjci	strůjce	k1gMnPc1	strůjce
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
doufali	doufat	k5eAaImAgMnP	doufat
v	v	k7c6	v
obnovení	obnovení	k1gNnSc6	obnovení
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
akce	akce	k1gFnSc1	akce
ale	ale	k9	ale
vedla	vést	k5eAaImAgFnS	vést
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
rozpoutání	rozpoutání	k1gNnSc3	rozpoutání
nového	nový	k2eAgNnSc2d1	nové
kola	kolo	k1gNnSc2	kolo
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vyšel	vyjít	k5eAaPmAgMnS	vyjít
jako	jako	k9	jako
vítěz	vítěz	k1gMnSc1	vítěz
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Octavianus	Octavianus	k1gMnSc1	Octavianus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poté	poté	k6eAd1	poté
nastolil	nastolit	k5eAaPmAgMnS	nastolit
římské	římský	k2eAgNnSc4d1	římské
císařství	císařství	k1gNnSc4	císařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
42	[number]	k4	42
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
římským	římský	k2eAgInSc7d1	římský
senátem	senát	k1gInSc7	senát
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
římských	římský	k2eAgMnPc2d1	římský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
z	z	k7c2	z
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
vlastních	vlastní	k2eAgMnPc2d1	vlastní
Zápisků	zápisek	k1gInPc2	zápisek
(	(	kIx(	(
<g/>
Commentarii	Commentarie	k1gFnSc4	Commentarie
<g/>
)	)	kIx)	)
z	z	k7c2	z
vojenských	vojenský	k2eAgNnPc2d1	vojenské
tažení	tažení	k1gNnPc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
soudobé	soudobý	k2eAgInPc4d1	soudobý
prameny	pramen	k1gInPc4	pramen
náleží	náležet	k5eAaImIp3nP	náležet
také	také	k9	také
dochované	dochovaný	k2eAgInPc1d1	dochovaný
dopisy	dopis	k1gInPc1	dopis
a	a	k8xC	a
projevy	projev	k1gInPc1	projev
Caesarova	Caesarův	k2eAgMnSc2d1	Caesarův
politického	politický	k2eAgMnSc2d1	politický
soupeře	soupeř	k1gMnSc2	soupeř
Cicerona	Cicero	k1gMnSc2	Cicero
<g/>
,	,	kIx,	,
historické	historický	k2eAgNnSc4d1	historické
dílo	dílo	k1gNnSc4	dílo
Sallustia	Sallustia	k1gFnSc1	Sallustia
či	či	k8xC	či
Cattulovy	Cattulův	k2eAgFnPc1d1	Cattulův
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
podrobnosti	podrobnost	k1gFnPc1	podrobnost
o	o	k7c6	o
Caesarově	Caesarův	k2eAgInSc6d1	Caesarův
životě	život	k1gInSc6	život
byly	být	k5eAaImAgInP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
historiky	historik	k1gMnPc7	historik
–	–	k?	–
Appiánem	Appián	k1gInSc7	Appián
<g/>
,	,	kIx,	,
Suetoniem	Suetonium	k1gNnSc7	Suetonium
<g/>
,	,	kIx,	,
Plútarchem	Plútarch	k1gInSc7	Plútarch
<g/>
,	,	kIx,	,
Cassiem	Cassius	k1gMnSc7	Cassius
Dionem	Dion	k1gMnSc7	Dion
a	a	k8xC	a
Strabónem	Strabón	k1gMnSc7	Strabón
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
100	[number]	k4	100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
v	v	k7c6	v
patricijské	patricijský	k2eAgFnSc6d1	patricijská
rodině	rodina	k1gFnSc6	rodina
náležející	náležející	k2eAgFnSc6d1	náležející
k	k	k7c3	k
rodu	rod	k1gInSc3	rod
Juliů	Julius	k1gMnPc2	Julius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
Jula	Jula	k1gFnSc1	Jula
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
trójského	trójský	k2eAgMnSc2d1	trójský
hrdiny	hrdina	k1gMnSc2	hrdina
Aenea	Aeneas	k1gMnSc2	Aeneas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgMnSc3	svůj
aristokratickému	aristokratický	k2eAgMnSc3d1	aristokratický
a	a	k8xC	a
patricijskému	patricijský	k2eAgInSc3d1	patricijský
původu	původ	k1gInSc3	původ
nepatřila	patřit	k5eNaImAgFnS	patřit
caesarská	caesarský	k2eAgFnSc1d1	caesarský
frakce	frakce	k1gFnSc1	frakce
Juliů	Julius	k1gMnPc2	Julius
mezi	mezi	k7c4	mezi
zvláště	zvláště	k6eAd1	zvláště
vlivné	vlivný	k2eAgFnPc4d1	vlivná
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Gaius	Gaius	k1gMnSc1	Gaius
Iulius	Iulius	k1gMnSc1	Iulius
Caesar	Caesar	k1gMnSc1	Caesar
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
pouze	pouze	k6eAd1	pouze
hodnosti	hodnost	k1gFnPc4	hodnost
praetora	praetor	k1gMnSc2	praetor
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jeho	jeho	k3xOp3gMnSc7	jeho
švagrem	švagr	k1gMnSc7	švagr
byl	být	k5eAaImAgMnS	být
Gaius	Gaius	k1gMnSc1	Gaius
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Aurelia	Aurelia	k1gFnSc1	Aurelia
Cotta	Cotta	k1gFnSc1	Cotta
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
mocnější	mocný	k2eAgFnSc2d2	mocnější
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dala	dát	k5eAaPmAgFnS	dát
republice	republika	k1gFnSc6	republika
řadu	řada	k1gFnSc4	řada
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Julia	Julius	k1gMnSc2	Julius
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
dětství	dětství	k1gNnSc1	dětství
toho	ten	k3xDgMnSc4	ten
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
počáteční	počáteční	k2eAgFnPc1d1	počáteční
kapitoly	kapitola	k1gFnPc1	kapitola
jeho	jeho	k3xOp3gInPc2	jeho
životopisů	životopis	k1gInPc2	životopis
od	od	k7c2	od
Suetonia	Suetonium	k1gNnSc2	Suetonium
a	a	k8xC	a
Plútarcha	Plútarcha	k1gFnSc1	Plútarcha
byly	být	k5eAaImAgFnP	být
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
Caesarova	Caesarův	k2eAgNnSc2d1	Caesarovo
vyrůstání	vyrůstání	k1gNnSc2	vyrůstání
byla	být	k5eAaImAgFnS	být
obdobím	období	k1gNnSc7	období
zmatků	zmatek	k1gInPc2	zmatek
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
91	[number]	k4	91
až	až	k9	až
88	[number]	k4	88
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
probíhala	probíhat	k5eAaImAgFnS	probíhat
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
italskými	italský	k2eAgMnPc7d1	italský
spojenci	spojenec	k1gMnPc7	spojenec
žádajícími	žádající	k2eAgFnPc7d1	žádající
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
současně	současně	k6eAd1	současně
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
pontský	pontský	k2eAgMnSc1d1	pontský
král	král	k1gMnSc1	král
Mithridatés	Mithridatésa	k1gFnPc2	Mithridatésa
římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
politická	politický	k2eAgFnSc1d1	politická
scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
rozpolcena	rozpolcen	k2eAgFnSc1d1	rozpolcena
mezi	mezi	k7c4	mezi
znepřátelené	znepřátelený	k2eAgFnPc4d1	znepřátelená
frakce	frakce	k1gFnPc4	frakce
"	"	kIx"	"
<g/>
šlechtických	šlechtický	k2eAgMnPc2d1	šlechtický
<g/>
"	"	kIx"	"
optimátů	optimát	k1gMnPc2	optimát
(	(	kIx(	(
<g/>
optimates	optimates	k1gMnSc1	optimates
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
lidových	lidový	k2eAgMnPc2d1	lidový
<g/>
"	"	kIx"	"
populárů	populár	k1gMnPc2	populár
(	(	kIx(	(
<g/>
populares	populares	k1gMnSc1	populares
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
strýc	strýc	k1gMnSc1	strýc
Marius	Marius	k1gMnSc1	Marius
byl	být	k5eAaImAgMnS	být
vůdcem	vůdce	k1gMnSc7	vůdce
populárů	populár	k1gInPc2	populár
<g/>
.	.	kIx.	.
</s>
<s>
Lucius	Lucius	k1gMnSc1	Lucius
Cornelius	Cornelius	k1gMnSc1	Cornelius
Sulla	Sulla	k1gMnSc1	Sulla
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
Mariův	Mariův	k2eAgMnSc1d1	Mariův
legát	legát	k1gMnSc1	legát
z	z	k7c2	z
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Jugurthovi	Jugurth	k1gMnSc3	Jugurth
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
optimátů	optimát	k1gMnPc2	optimát
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
během	během	k7c2	během
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
války	válka	k1gFnSc2	válka
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
nyní	nyní	k6eAd1	nyní
žádali	žádat	k5eAaImAgMnP	žádat
velení	velení	k1gNnSc4	velení
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Mithridatovi	Mithridat	k1gMnSc3	Mithridat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
přiřčeno	přiřknout	k5eAaPmNgNnS	přiřknout
Sullovi	Sullův	k2eAgMnPc1d1	Sullův
<g/>
,	,	kIx,	,
když	když	k8xS	když
ale	ale	k8xC	ale
opustil	opustit	k5eAaPmAgInS	opustit
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
dosazen	dosazen	k2eAgInSc1d1	dosazen
Marius	Marius	k1gInSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
Sulla	Sulla	k1gMnSc1	Sulla
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
s	s	k7c7	s
legiemi	legie	k1gFnPc7	legie
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Marius	Marius	k1gMnSc1	Marius
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
Sullově	Sullův	k2eAgInSc6d1	Sullův
odchodu	odchod	k1gInSc6	odchod
na	na	k7c4	na
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
spojencem	spojenec	k1gMnSc7	spojenec
Luciem	Lucius	k1gMnSc7	Lucius
Corneliem	Cornelium	k1gNnSc7	Cornelium
Cinnou	Cinna	k1gFnSc7	Cinna
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Sulla	Sulla	k1gMnSc1	Sulla
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
veřejného	veřejný	k2eAgMnSc4d1	veřejný
nepřítele	nepřítel	k1gMnSc4	nepřítel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
byli	být	k5eAaImAgMnP	být
zmasakrováni	zmasakrovat	k5eAaPmNgMnP	zmasakrovat
<g/>
.	.	kIx.	.
</s>
<s>
Marius	Marius	k1gInSc1	Marius
již	již	k9	již
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
86	[number]	k4	86
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
populárové	populár	k1gMnPc1	populár
přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
85	[number]	k4	85
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
otec	otec	k1gMnSc1	otec
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
stal	stát	k5eAaPmAgMnS	stát
hlavou	hlava	k1gFnSc7	hlava
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Cinnovou	Cinnový	k2eAgFnSc7d1	Cinnový
dcerou	dcera	k1gFnSc7	dcera
Cornelií	Cornelie	k1gFnPc2	Cornelie
<g/>
,	,	kIx,	,
když	když	k8xS	když
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
zrušil	zrušit	k5eAaPmAgInS	zrušit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
s	s	k7c7	s
Cossutií	Cossutie	k1gFnSc7	Cossutie
<g/>
,	,	kIx,	,
dívkou	dívka	k1gFnSc7	dívka
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
za	za	k7c4	za
Jovova	Jovův	k2eAgMnSc4d1	Jovův
velekněze	velekněz	k1gMnSc4	velekněz
(	(	kIx(	(
<g/>
flamen	flamen	k2eAgInSc1d1	flamen
Dialis	Dialis	k1gInSc1	Dialis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
předchůdce	předchůdce	k1gMnSc1	předchůdce
padl	padnout	k5eAaImAgMnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
Mariovým	Mariův	k2eAgMnPc3d1	Mariův
stoupencům	stoupenec	k1gMnPc3	stoupenec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Sulla	Sulla	k1gMnSc1	Sulla
přiměl	přimět	k5eAaPmAgMnS	přimět
Mithridata	Mithridat	k1gMnSc4	Mithridat
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
populárům	populár	k1gMnPc3	populár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tažení	tažení	k1gNnSc6	tažení
Itálií	Itálie	k1gFnPc2	Itálie
porazil	porazit	k5eAaPmAgInS	porazit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
82	[number]	k4	82
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mariovce	mariovec	k1gInSc2	mariovec
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Collinské	Collinský	k2eAgFnSc2d1	Collinský
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ukončil	ukončit	k5eAaPmAgMnS	ukončit
jejich	jejich	k3xOp3gFnSc4	jejich
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
obdařil	obdařit	k5eAaPmAgMnS	obdařit
starodávnou	starodávný	k2eAgFnSc7d1	starodávná
funkcí	funkce	k1gFnSc7	funkce
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tradičního	tradiční	k2eAgNnSc2d1	tradiční
šestiměsíčního	šestiměsíční	k2eAgNnSc2d1	šestiměsíční
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
nebylo	být	k5eNaImAgNnS	být
Sullově	Sullův	k2eAgFnSc3d1	Sullova
diktatuře	diktatura	k1gFnSc3	diktatura
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
žádné	žádný	k3yNgNnSc1	žádný
časové	časový	k2eAgNnSc1d1	časové
omezení	omezení	k1gNnSc1	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
krvavé	krvavý	k2eAgFnPc1d1	krvavá
proskripce	proskripce	k1gFnPc1	proskripce
(	(	kIx(	(
<g/>
seznamy	seznam	k1gInPc1	seznam
nepřátel	nepřítel	k1gMnPc2	nepřítel
státu	stát	k1gInSc2	stát
hodných	hodný	k2eAgInPc2d1	hodný
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
namířené	namířený	k2eAgFnSc2d1	namířená
proti	proti	k7c3	proti
Sullovým	Sullův	k2eAgMnPc3d1	Sullův
politickým	politický	k2eAgMnPc3d1	politický
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
překonaly	překonat	k5eAaPmAgFnP	překonat
dokonce	dokonce	k9	dokonce
i	i	k9	i
Mariovy	Mariův	k2eAgFnPc4d1	Mariova
čistky	čistka	k1gFnPc4	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Mariovy	Mariův	k2eAgFnPc1d1	Mariova
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgInP	být
zničeny	zničen	k2eAgInPc4d1	zničen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
vytaženy	vytáhnout	k5eAaPmNgInP	vytáhnout
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
a	a	k8xC	a
vhozeny	vhozen	k2eAgFnPc1d1	vhozena
do	do	k7c2	do
Tiberu	Tiber	k1gInSc2	Tiber
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
Mariův	Mariův	k2eAgMnSc1d1	Mariův
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
zeť	zeť	k1gMnSc1	zeť
Cinny	Cinna	k1gMnSc2	Cinna
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
již	již	k9	již
dříve	dříve	k6eAd2	dříve
během	během	k7c2	během
vzpoury	vzpoura	k1gFnSc2	vzpoura
vlastních	vlastní	k2eAgMnPc2d1	vlastní
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
smrtelném	smrtelný	k2eAgNnSc6d1	smrtelné
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kněžského	kněžský	k2eAgInSc2d1	kněžský
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgMnS	být
zbaven	zbaven	k2eAgMnSc1d1	zbaven
také	také	k9	také
svého	svůj	k3xOyFgNnSc2	svůj
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
věna	věno	k1gNnSc2	věno
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dát	dát	k5eAaPmF	dát
rozvést	rozvést	k5eAaPmF	rozvést
s	s	k7c7	s
Cornelií	Cornelie	k1gFnSc7	Cornelie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
zásah	zásah	k1gInSc1	zásah
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodiny	rodina	k1gFnSc2	rodina
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
Sullu	Sulla	k1gFnSc4	Sulla
<g/>
,	,	kIx,	,
a	a	k8xC	a
panenských	panenský	k2eAgFnPc2d1	panenská
kněžek	kněžka	k1gFnPc2	kněžka
vestálek	vestálka	k1gFnPc2	vestálka
mu	on	k3xPp3gMnSc3	on
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Sulla	Sulla	k6eAd1	Sulla
mu	on	k3xPp3gNnSc3	on
údajně	údajně	k6eAd1	údajně
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
daroval	darovat	k5eAaPmAgMnS	darovat
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
prý	prý	k9	prý
jeho	jeho	k3xOp3gFnSc4	jeho
přímluvce	přímluvce	k1gMnSc1	přímluvce
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
prozíravě	prozíravě	k6eAd1	prozíravě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vždyť	vždyť	k9	vždyť
v	v	k7c6	v
Caesarovi	Caesar	k1gMnSc6	Caesar
je	být	k5eAaImIp3nS	být
skryto	skrýt	k5eAaPmNgNnS	skrýt
mnoho	mnoho	k4c1	mnoho
Mariů	Mario	k1gMnPc2	Mario
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
však	však	k9	však
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
města	město	k1gNnSc2	město
Mytilény	Mytiléna	k1gFnSc2	Mytiléna
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
vyslán	vyslat	k5eAaPmNgMnS	vyslat
do	do	k7c2	do
Bithýnie	Bithýnie	k1gFnSc2	Bithýnie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
požádal	požádat	k5eAaPmAgMnS	požádat
místního	místní	k2eAgMnSc4d1	místní
krále	král	k1gMnSc4	král
Níkoméda	Níkoméd	k1gMnSc4	Níkoméd
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
jeho	jeho	k3xOp3gFnSc2	jeho
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Níkomédově	Níkomédův	k2eAgInSc6d1	Níkomédův
dvoře	dvůr	k1gInSc6	dvůr
strávil	strávit	k5eAaPmAgMnS	strávit
poté	poté	k6eAd1	poté
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zavdal	zavdat	k5eAaPmAgMnS	zavdat
příčinu	příčina	k1gFnSc4	příčina
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
klepům	klep	k1gInPc3	klep
o	o	k7c6	o
homosexuálním	homosexuální	k2eAgNnSc6d1	homosexuální
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc2	on
měly	mít	k5eAaImAgFnP	mít
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
při	při	k7c6	při
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Mytilénu	Mytiléna	k1gFnSc4	Mytiléna
si	se	k3xPyFc3	se
Caesar	Caesar	k1gMnSc1	Caesar
počínal	počínat	k5eAaImAgMnS	počínat
natolik	natolik	k6eAd1	natolik
udatně	udatně	k6eAd1	udatně
a	a	k8xC	a
odvážně	odvážně	k6eAd1	odvážně
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
občanskou	občanský	k2eAgFnSc4d1	občanská
korunu	koruna	k1gFnSc4	koruna
(	(	kIx(	(
<g/>
corona	corona	k1gFnSc1	corona
civica	civica	k1gMnSc1	civica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
náleželo	náležet	k5eAaImAgNnS	náležet
tomu	ten	k3xDgMnSc3	ten
Římanovi	Říman	k1gMnSc3	Říman
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
svému	svůj	k3xOyFgMnSc3	svůj
spoluobčanovi	spoluobčan	k1gMnSc3	spoluobčan
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
rovněž	rovněž	k9	rovněž
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
pirátům	pirát	k1gMnPc3	pirát
v	v	k7c6	v
Kilíkii	Kilíkie	k1gFnSc6	Kilíkie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
samovlády	samovláda	k1gFnSc2	samovláda
se	se	k3xPyFc4	se
Sulla	Sulla	k1gMnSc1	Sulla
vzdal	vzdát	k5eAaPmAgMnS	vzdát
úřadu	úřad	k1gInSc3	úřad
diktátora	diktátor	k1gMnSc2	diktátor
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
republikánské	republikánský	k2eAgNnSc4d1	republikánské
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
propustil	propustit	k5eAaPmAgInS	propustit
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
liktory	liktor	k1gMnPc4	liktor
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
nestřežen	střežen	k2eNgMnSc1d1	nestřežen
se	se	k3xPyFc4	se
procházel	procházet	k5eAaImAgMnS	procházet
po	po	k7c6	po
fóru	fórum	k1gNnSc6	fórum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
vystrojen	vystrojen	k2eAgInSc4d1	vystrojen
státní	státní	k2eAgInSc4d1	státní
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
78	[number]	k4	78
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
o	o	k7c6	o
Sullově	Sullův	k2eAgFnSc6d1	Sullova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
nebrání	bránit	k5eNaImIp3nS	bránit
jeho	jeho	k3xOp3gInSc3	jeho
bezpečnému	bezpečný	k2eAgInSc3d1	bezpečný
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
prostředků	prostředek	k1gInPc2	prostředek
si	se	k3xPyFc3	se
pořídil	pořídit	k5eAaPmAgMnS	pořídit
skromný	skromný	k2eAgInSc4d1	skromný
dům	dům	k1gInSc4	dům
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Subura	Subura	k1gFnSc1	Subura
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
obývali	obývat	k5eAaImAgMnP	obývat
méně	málo	k6eAd2	málo
majetní	majetný	k2eAgMnPc1d1	majetný
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příchod	příchod	k1gInSc1	příchod
se	se	k3xPyFc4	se
časově	časově	k6eAd1	časově
kryl	krýt	k5eAaImAgMnS	krýt
s	s	k7c7	s
pokusem	pokus	k1gInSc7	pokus
Marca	Marcus	k1gMnSc2	Marcus
Aemilia	Aemilius	k1gMnSc2	Aemilius
Lepida	Lepid	k1gMnSc2	Lepid
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
postrádal	postrádat	k5eAaImAgMnS	postrádat
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c4	v
Lepidovo	Lepidův	k2eAgNnSc4d1	Lepidův
vedení	vedení	k1gNnSc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
veřejného	veřejný	k2eAgMnSc2d1	veřejný
obhájce	obhájce	k1gMnSc2	obhájce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vznesl	vznést	k5eAaPmAgMnS	vznést
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3	proti
Sullovu	Sullův	k2eAgMnSc3d1	Sullův
příznivci	příznivec	k1gMnSc3	příznivec
Gnaeu	Gnae	k1gMnSc3	Gnae
Corneliu	Cornelius	k1gMnSc3	Cornelius
Dolabellovi	Dolabell	k1gMnSc3	Dolabell
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
spor	spor	k1gInSc1	spor
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
,	,	kIx,	,
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
řečnické	řečnický	k2eAgFnPc1d1	řečnická
schopnosti	schopnost	k1gFnPc1	schopnost
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
kvůli	kvůli	k7c3	kvůli
nelítostnému	lítostný	k2eNgNnSc3d1	nelítostné
stíhání	stíhání	k1gNnSc3	stíhání
bývalých	bývalý	k2eAgMnPc2d1	bývalý
správců	správce	k1gMnPc2	správce
provincií	provincie	k1gFnPc2	provincie
smutně	smutně	k6eAd1	smutně
proslulých	proslulý	k2eAgInPc2d1	proslulý
svým	svůj	k3xOyFgNnSc7	svůj
vydíráním	vydírání	k1gNnSc7	vydírání
a	a	k8xC	a
zkorumpovaností	zkorumpovanost	k1gFnSc7	zkorumpovanost
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
samotný	samotný	k2eAgMnSc1d1	samotný
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
rétor	rétor	k1gMnSc1	rétor
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Najde	najít	k5eAaPmIp3nS	najít
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ovládá	ovládat	k5eAaImIp3nS	ovládat
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
Caesar	Caesar	k1gMnSc1	Caesar
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Aby	aby	kYmCp3nS	aby
ještě	ještě	k6eAd1	ještě
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
své	svůj	k3xOyFgNnSc4	svůj
řečnické	řečnický	k2eAgNnSc4d1	řečnické
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
Caesar	Caesar	k1gMnSc1	Caesar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
75	[number]	k4	75
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c4	na
Rhodos	Rhodos	k1gInSc4	Rhodos
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
rétoriku	rétorika	k1gFnSc4	rétorika
u	u	k7c2	u
slavného	slavný	k2eAgMnSc2d1	slavný
učitele	učitel	k1gMnSc2	učitel
Apollónia	Apollónium	k1gNnSc2	Apollónium
Molóna	Molón	k1gMnSc2	Molón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dříve	dříve	k6eAd2	dříve
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
i	i	k9	i
Cicerona	Cicero	k1gMnSc4	Cicero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
plavby	plavba	k1gFnSc2	plavba
Egejským	egejský	k2eAgNnSc7d1	Egejské
mořem	moře	k1gNnSc7	moře
byl	být	k5eAaImAgMnS	být
však	však	k8xC	však
Caesar	Caesar	k1gMnSc1	Caesar
unesen	unést	k5eAaPmNgMnS	unést
kilikickými	kilikický	k2eAgMnPc7d1	kilikický
piráty	pirát	k1gMnPc7	pirát
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
jimi	on	k3xPp3gNnPc7	on
držen	držet	k5eAaImNgMnS	držet
jako	jako	k9	jako
vězeň	vězeň	k1gMnSc1	vězeň
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Dodekanéského	Dodekanéský	k2eAgNnSc2d1	Dodekanéský
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Zajetí	zajetí	k1gNnSc1	zajetí
ho	on	k3xPp3gInSc2	on
ale	ale	k9	ale
nijak	nijak	k6eAd1	nijak
nepřipravilo	připravit	k5eNaPmAgNnS	připravit
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
nezdolné	zdolný	k2eNgNnSc1d1	nezdolné
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
za	za	k7c4	za
něho	on	k3xPp3gMnSc4	on
piráti	pirát	k1gMnPc1	pirát
požadovali	požadovat	k5eAaImAgMnP	požadovat
výkupné	výkupné	k1gNnSc4	výkupné
dvacet	dvacet	k4xCc4	dvacet
talentů	talent	k1gInPc2	talent
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
naléhal	naléhat	k5eAaBmAgMnS	naléhat
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
řekli	říct	k5eAaPmAgMnP	říct
o	o	k7c4	o
padesát	padesát	k4xCc4	padesát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
Caesar	Caesar	k1gMnSc1	Caesar
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgInSc7	který
piráty	pirát	k1gMnPc4	pirát
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Správce	správce	k1gMnSc1	správce
provincie	provincie	k1gFnSc2	provincie
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
Pergamu	Pergamos	k1gInSc6	Pergamos
je	být	k5eAaImIp3nS	být
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
popravit	popravit	k5eAaPmF	popravit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Caesar	Caesar	k1gMnSc1	Caesar
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
jejich	jejich	k3xOp3gFnSc3	jejich
prodeji	prodej	k1gFnSc3	prodej
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
ale	ale	k9	ale
pro	pro	k7c4	pro
piráty	pirát	k1gMnPc4	pirát
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
je	on	k3xPp3gMnPc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
ukřižovat	ukřižovat	k5eAaPmF	ukřižovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jim	on	k3xPp3gMnPc3	on
ostatně	ostatně	k6eAd1	ostatně
slíbil	slíbit	k5eAaPmAgMnS	slíbit
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
jeho	jeho	k3xOp3gInSc3	jeho
slibu	slib	k1gInSc3	slib
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
piráti	pirát	k1gMnPc1	pirát
vysmáli	vysmát	k5eAaPmAgMnP	vysmát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c4	na
Rhodos	Rhodos	k1gInSc4	Rhodos
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
však	však	k9	však
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
vojenským	vojenský	k2eAgMnSc7d1	vojenský
tribunem	tribun	k1gMnSc7	tribun
a	a	k8xC	a
učinil	učinit	k5eAaPmAgMnS	učinit
tak	tak	k6eAd1	tak
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
(	(	kIx(	(
<g/>
cursus	cursus	k1gInSc1	cursus
honorum	honorum	k1gInSc1	honorum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zuřila	zuřit	k5eAaImAgFnS	zuřit
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
válka	válka	k1gFnSc1	válka
se	s	k7c7	s
Spartakem	Spartak	k1gInSc7	Spartak
(	(	kIx(	(
<g/>
73	[number]	k4	73
až	až	k9	až
71	[number]	k4	71
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
známo	znám	k2eAgNnSc1d1	známo
jakou	jaký	k3yRgFnSc7	jaký
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
nějakou	nějaký	k3yIgFnSc4	nějaký
<g/>
,	,	kIx,	,
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
Caesar	Caesar	k1gMnSc1	Caesar
sehrál	sehrát	k5eAaPmAgMnS	sehrát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
quaestorem	quaestor	k1gInSc7	quaestor
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
69	[number]	k4	69
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
pronesl	pronést	k5eAaPmAgMnS	pronést
smuteční	smuteční	k2eAgFnSc4d1	smuteční
řeč	řeč	k1gFnSc4	řeč
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
tetu	teta	k1gFnSc4	teta
Julii	Julie	k1gFnSc4	Julie
<g/>
,	,	kIx,	,
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
Mariovi	Mario	k1gMnSc6	Mario
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smutečním	smuteční	k2eAgNnSc6d1	smuteční
procesí	procesí	k1gNnSc6	procesí
byly	být	k5eAaImAgInP	být
neseny	nést	k5eAaImNgInP	nést
zakázané	zakázaný	k2eAgInPc1d1	zakázaný
Mariovy	Mariův	k2eAgInPc1d1	Mariův
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
Sullova	Sullův	k2eAgInSc2d1	Sullův
nástupu	nástup	k1gInSc2	nástup
věc	věc	k1gFnSc1	věc
vskutku	vskutku	k9	vskutku
nevídaná	vídaný	k2eNgFnSc1d1	nevídaná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Cornelia	Cornelia	k1gFnSc1	Cornelia
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
pak	pak	k6eAd1	pak
odešel	odejít	k5eAaPmAgMnS	odejít
sloužit	sloužit	k5eAaImF	sloužit
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Suetonia	Suetonium	k1gNnSc2	Suetonium
spatřil	spatřit	k5eAaPmAgMnS	spatřit
v	v	k7c6	v
Gadesu	Gades	k1gInSc6	Gades
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
Cádiz	Cádiz	k1gInSc4	Cádiz
<g/>
)	)	kIx)	)
sochu	socha	k1gFnSc4	socha
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
prý	prý	k9	prý
posteskl	postesknout	k5eAaPmAgMnS	postesknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
věku	věk	k1gInSc3	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Alexandrovi	Alexandrův	k2eAgMnPc1d1	Alexandrův
už	už	k6eAd1	už
ležel	ležet	k5eAaImAgMnS	ležet
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
dosud	dosud	k6eAd1	dosud
nic	nic	k3yNnSc1	nic
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Hispánie	Hispánie	k1gFnSc2	Hispánie
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Pompeiou	Pompeia	k1gFnSc7	Pompeia
<g/>
,	,	kIx,	,
Sullovou	Sullův	k2eAgFnSc7d1	Sullova
vnučkou	vnučka	k1gFnSc7	vnučka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
aedilem	aedil	k1gInSc7	aedil
nechal	nechat	k5eAaPmAgInS	nechat
obnovit	obnovit	k5eAaPmF	obnovit
pomníky	pomník	k1gInPc4	pomník
Mariových	Mariův	k2eAgFnPc2d1	Mariova
vítězství	vítězství	k1gNnSc4	vítězství
–	–	k?	–
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgInSc4d1	kontroverzní
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
moc	moc	k6eAd1	moc
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
stále	stále	k6eAd1	stále
drželi	držet	k5eAaImAgMnP	držet
někdejší	někdejší	k2eAgMnPc1d1	někdejší
Sullovi	Sullův	k2eAgMnPc1d1	Sullův
straníci	straník	k1gMnPc1	straník
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vznesl	vznést	k5eAaPmAgMnS	vznést
několik	několik	k4yIc4	několik
žalob	žaloba	k1gFnPc2	žaloba
proti	proti	k7c3	proti
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
obohatili	obohatit	k5eAaPmAgMnP	obohatit
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
proskribovaných	proskribovaný	k2eAgFnPc2d1	proskribovaná
a	a	k8xC	a
vynaložil	vynaložit	k5eAaPmAgMnS	vynaložit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
peněz	peníze	k1gInPc2	peníze
půjčených	půjčený	k2eAgInPc2d1	půjčený
převážně	převážně	k6eAd1	převážně
od	od	k7c2	od
Crassa	Crass	k1gMnSc2	Crass
na	na	k7c4	na
veřejné	veřejný	k2eAgFnPc4d1	veřejná
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zastínil	zastínit	k5eAaPmAgMnS	zastínit
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
Marca	Marcus	k1gMnSc2	Marcus
Calpurnia	Calpurnium	k1gNnSc2	Calpurnium
Bibula	Bibul	k1gMnSc2	Bibul
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
63	[number]	k4	63
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Caesara	Caesar	k1gMnSc4	Caesar
rokem	rok	k1gInSc7	rok
plným	plný	k2eAgNnSc7d1	plné
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
tribuna	tribun	k1gMnSc2	tribun
lidu	lid	k1gInSc2	lid
Tita	Titus	k1gMnSc4	Titus
Labiena	Labien	k1gMnSc4	Labien
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obžaloval	obžalovat	k5eAaPmAgInS	obžalovat
Gaia	Gai	k2eAgNnPc4d1	Gai
Rabiria	Rabirium	k1gNnPc4	Rabirium
<g/>
,	,	kIx,	,
senátora	senátor	k1gMnSc4	senátor
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
optimátů	optimát	k1gMnPc2	optimát
<g/>
,	,	kIx,	,
z	z	k7c2	z
politické	politický	k2eAgFnSc2d1	politická
vraždy	vražda	k1gFnSc2	vražda
tribuna	tribun	k1gMnSc2	tribun
Lucia	Lucius	k1gMnSc2	Lucius
Appuleia	Appuleius	k1gMnSc2	Appuleius
Saturnina	Saturnin	k1gMnSc2	Saturnin
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc4	jeho
samotného	samotný	k2eAgMnSc4d1	samotný
určil	určit	k5eAaPmAgInS	určit
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
soudců	soudce	k1gMnPc2	soudce
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Rabiria	Rabirium	k1gNnSc2	Rabirium
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
Cicero	Cicero	k1gMnSc1	Cicero
a	a	k8xC	a
Quintus	Quintus	k1gMnSc1	Quintus
Hortensius	Hortensius	k1gMnSc1	Hortensius
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
z	z	k7c2	z
velezrady	velezrada	k1gFnSc2	velezrada
(	(	kIx(	(
<g/>
perduellio	perduellio	k6eAd1	perduellio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Rabirius	Rabirius	k1gInSc1	Rabirius
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
odvolání	odvolání	k1gNnSc6	odvolání
se	se	k3xPyFc4	se
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
<g/>
,	,	kIx,	,
praetor	praetor	k1gMnSc1	praetor
Quintus	Quintus	k1gMnSc1	Quintus
Caecilius	Caecilius	k1gMnSc1	Caecilius
Metellus	Metellus	k1gMnSc1	Metellus
Celer	celer	k1gInSc4	celer
dal	dát	k5eAaPmAgMnS	dát
přerušit	přerušit	k5eAaPmF	přerušit
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Labienus	Labienus	k1gMnSc1	Labienus
mohl	moct	k5eAaImAgMnS	moct
řízení	řízení	k1gNnSc4	řízení
odročit	odročit	k5eAaPmF	odročit
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
,	,	kIx,	,
neučinil	učinit	k5eNaPmAgMnS	učinit
tak	tak	k6eAd1	tak
a	a	k8xC	a
Caesar	Caesar	k1gMnSc1	Caesar
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Labienus	Labienus	k1gMnSc1	Labienus
zůstal	zůstat	k5eAaPmAgMnS	zůstat
důležitým	důležitý	k2eAgMnSc7d1	důležitý
spojencem	spojenec	k1gMnSc7	spojenec
Caesara	Caesar	k1gMnSc2	Caesar
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
další	další	k2eAgNnSc4d1	další
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
velekněze	velekněz	k1gMnSc2	velekněz
(	(	kIx(	(
<g/>
pontifex	pontifex	k1gMnSc1	pontifex
maximus	maximus	k1gMnSc1	maximus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
přitom	přitom	k6eAd1	přitom
proti	proti	k7c3	proti
dvěma	dva	k4xCgMnPc3	dva
mocným	mocný	k2eAgMnPc3d1	mocný
optimátům	optimát	k1gMnPc3	optimát
a	a	k8xC	a
někdejším	někdejší	k2eAgMnPc3d1	někdejší
konzulům	konzul	k1gMnPc3	konzul
<g/>
:	:	kIx,	:
Quintu	Quint	k1gMnSc3	Quint
Lutatiu	Lutatius	k1gMnSc3	Lutatius
Catulovi	Catul	k1gMnSc3	Catul
a	a	k8xC	a
Publiu	Publius	k1gMnSc3	Publius
Serviliu	Servilius	k1gMnSc3	Servilius
Isauricovi	Isauric	k1gMnSc3	Isauric
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
obviňovaly	obviňovat	k5eAaImAgInP	obviňovat
z	z	k7c2	z
uplácení	uplácení	k1gNnSc2	uplácení
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
v	v	k7c4	v
den	den	k1gInSc4	den
voleb	volba	k1gFnPc2	volba
řekl	říct	k5eAaPmAgMnS	říct
prý	prý	k9	prý
Caesar	Caesar	k1gMnSc1	Caesar
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
vrátí	vrátit	k5eAaPmIp3nS	vrátit
jako	jako	k9	jako
pontifex	pontifex	k1gMnSc1	pontifex
maximus	maximus	k1gMnSc1	maximus
anebo	anebo	k8xC	anebo
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
neúspěchu	neúspěch	k1gInSc2	neúspěch
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
enormním	enormní	k2eAgInPc3d1	enormní
dluhům	dluh	k1gInPc3	dluh
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
bojovat	bojovat	k5eAaImF	bojovat
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
celkem	celkem	k6eAd1	celkem
jasně	jasně	k6eAd1	jasně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
lepší	dobrý	k2eAgNnSc4d2	lepší
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
zkušenosti	zkušenost	k1gFnPc4	zkušenost
svých	svůj	k3xOyFgMnPc2	svůj
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
konzulem	konzul	k1gMnSc7	konzul
<g/>
,	,	kIx,	,
odhalil	odhalit	k5eAaPmAgMnS	odhalit
Catilinovo	Catilinův	k2eAgNnSc4d1	Catilinovo
spiknutí	spiknutí	k1gNnSc4	spiknutí
<g/>
,	,	kIx,	,
Catulus	Catulus	k1gMnSc1	Catulus
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k2eAgMnPc1d1	jiný
nařkli	nařknout	k5eAaPmAgMnP	nařknout
Caesara	Caesar	k1gMnSc4	Caesar
ze	z	k7c2	z
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
komplotu	komplot	k1gInSc2	komplot
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
zvolen	zvolen	k2eAgInSc1d1	zvolen
praetorem	praetor	k1gInSc7	praetor
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
debaty	debata	k1gFnSc2	debata
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
senátoři	senátor	k1gMnPc1	senátor
zabývali	zabývat	k5eAaImAgMnP	zabývat
způsobem	způsob	k1gInSc7	způsob
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	s	k7c7	s
spiklenci	spiklenec	k1gMnPc7	spiklenec
<g/>
.	.	kIx.	.
</s>
<s>
Probíhající	probíhající	k2eAgFnSc4d1	probíhající
diskuzi	diskuze	k1gFnSc4	diskuze
však	však	k9	však
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
četl	číst	k5eAaImAgMnS	číst
milostný	milostný	k2eAgInSc4d1	milostný
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
Catonovy	Catonův	k2eAgFnSc2d1	Catonova
sestry	sestra	k1gFnSc2	sestra
Servilie	Servilie	k1gFnSc2	Servilie
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Porcius	Porcius	k1gMnSc1	Porcius
Cato	Cato	k1gMnSc1	Cato
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Caesarovým	Caesarův	k2eAgMnSc7d1	Caesarův
nesmiřitelným	smiřitelný	k2eNgMnSc7d1	nesmiřitelný
odpůrcem	odpůrce	k1gMnSc7	odpůrce
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
přitom	přitom	k6eAd1	přitom
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
udržování	udržování	k1gNnSc2	udržování
korespondence	korespondence	k1gFnSc2	korespondence
se	s	k7c7	s
spiklenci	spiklenec	k1gMnPc7	spiklenec
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
inkriminované	inkriminovaný	k2eAgInPc1d1	inkriminovaný
dopisy	dopis	k1gInPc1	dopis
byly	být	k5eAaImAgInP	být
nahlas	nahlas	k6eAd1	nahlas
předčítány	předčítán	k2eAgInPc1d1	předčítán
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
snadno	snadno	k6eAd1	snadno
odolal	odolat	k5eAaPmAgMnS	odolat
jeho	jeho	k3xOp3gInPc3	jeho
útokům	útok	k1gInPc3	útok
a	a	k8xC	a
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
spiklence	spiklenec	k1gMnPc4	spiklenec
<g/>
.	.	kIx.	.
</s>
<s>
Catonova	Catonův	k2eAgFnSc1d1	Catonova
řeč	řeč	k1gFnSc1	řeč
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
a	a	k8xC	a
Catilina	Catilina	k1gFnSc1	Catilina
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
praetury	praetura	k1gFnSc2	praetura
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
podporován	podporovat	k5eAaImNgMnS	podporovat
tribunem	tribun	k1gMnSc7	tribun
Caeciliem	Caecilium	k1gNnSc7	Caecilium
Metellem	Metell	k1gMnSc7	Metell
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
však	však	k9	však
předkládali	předkládat	k5eAaImAgMnP	předkládat
natolik	natolik	k6eAd1	natolik
kontroverzní	kontroverzní	k2eAgInPc4d1	kontroverzní
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
až	až	k9	až
byli	být	k5eAaImAgMnP	být
senátem	senát	k1gInSc7	senát
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
srocení	srocení	k1gNnSc1	srocení
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
obou	dva	k4xCgFnPc2	dva
přimělo	přimět	k5eAaPmAgNnS	přimět
senát	senát	k1gInSc1	senát
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
opětnému	opětný	k2eAgNnSc3d1	opětné
dosazení	dosazení	k1gNnSc3	dosazení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
komise	komise	k1gFnSc1	komise
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
Catilinova	Catilinův	k2eAgNnSc2d1	Catilinovo
spiknutí	spiknutí	k1gNnSc2	spiknutí
a	a	k8xC	a
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
předhazována	předhazován	k2eAgFnSc1d1	předhazována
spoluúčast	spoluúčast	k1gFnSc1	spoluúčast
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
podniku	podnik	k1gInSc6	podnik
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
mu	on	k3xPp3gNnSc3	on
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
nic	nic	k6eAd1	nic
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Caesarově	Caesarův	k2eAgInSc6d1	Caesarův
domě	dům	k1gInSc6	dům
konal	konat	k5eAaImAgInS	konat
festival	festival	k1gInSc1	festival
Bona	bona	k1gFnSc1	bona
Dea	Dea	k1gMnSc1	Dea
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dobré	dobrý	k2eAgFnPc1d1	dobrá
bohyně	bohyně	k1gFnPc1	bohyně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
nesměl	smět	k5eNaImAgInS	smět
účastnit	účastnit	k5eAaImF	účastnit
žádný	žádný	k3yNgMnSc1	žádný
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
patricius	patricius	k1gMnSc1	patricius
jménem	jméno	k1gNnSc7	jméno
Publius	Publius	k1gInSc1	Publius
Clodius	Clodius	k1gInSc1	Clodius
Pulcher	Pulchra	k1gFnPc2	Pulchra
sem	sem	k6eAd1	sem
přesto	přesto	k8xC	přesto
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
pronikl	proniknout	k5eAaPmAgInS	proniknout
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
svést	svést	k5eAaPmF	svést
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
manželku	manželka	k1gFnSc4	manželka
Pompeiu	Pompeium	k1gNnSc3	Pompeium
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
chycen	chytit	k5eAaPmNgMnS	chytit
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
během	během	k7c2	během
následného	následný	k2eAgInSc2d1	následný
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
nepředložil	předložit	k5eNaPmAgInS	předložit
jediný	jediný	k2eAgInSc1d1	jediný
důkaz	důkaz	k1gInSc1	důkaz
proti	proti	k7c3	proti
Clodiovi	Clodius	k1gMnSc3	Clodius
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
nechtěl	chtít	k5eNaImAgMnS	chtít
znepřátelit	znepřátelit	k5eAaPmF	znepřátelit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
patricijských	patricijský	k2eAgFnPc2d1	patricijská
rodin	rodina	k1gFnPc2	rodina
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Clodius	Clodius	k1gInSc1	Clodius
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
bezuzdnému	bezuzdný	k2eAgNnSc3d1	bezuzdné
uplácení	uplácení	k1gNnSc3	uplácení
a	a	k8xC	a
zastrašování	zastrašování	k1gNnSc4	zastrašování
svědků	svědek	k1gMnPc2	svědek
i	i	k8xC	i
soudců	soudce	k1gMnPc2	soudce
posléze	posléze	k6eAd1	posléze
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
s	s	k7c7	s
Pompeiou	Pompeia	k1gMnSc7	Pompeia
záhy	záhy	k6eAd1	záhy
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jak	jak	k6eAd1	jak
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
na	na	k7c6	na
mé	můj	k3xOp1gFnSc6	můj
ženě	žena	k1gFnSc6	žena
nesmí	smět	k5eNaImIp3nS	smět
ulpět	ulpět	k5eAaPmF	ulpět
nejmenší	malý	k2eAgNnPc4d3	nejmenší
podezření	podezření	k1gNnPc4	podezření
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
praetury	praetura	k1gFnSc2	praetura
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
správcem	správce	k1gMnSc7	správce
provincie	provincie	k1gFnSc2	provincie
Hispania	Hispanium	k1gNnSc2	Hispanium
Ulterior	Ulteriora	k1gFnPc2	Ulteriora
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
Hispánie	Hispánie	k1gFnSc1	Hispánie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
zadlužený	zadlužený	k2eAgInSc1d1	zadlužený
a	a	k8xC	a
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohl	moct	k5eAaImAgMnS	moct
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
vyhovět	vyhovět	k5eAaPmF	vyhovět
svým	svůj	k3xOyFgMnPc3	svůj
věřitelům	věřitel	k1gMnPc3	věřitel
<g/>
.	.	kIx.	.
</s>
<s>
Obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
na	na	k7c4	na
Marca	Marcum	k1gNnPc4	Marcum
Licinia	Licinium	k1gNnSc2	Licinium
Crassa	Crass	k1gMnSc2	Crass
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
mužů	muž	k1gMnPc2	muž
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
za	za	k7c4	za
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
politickou	politický	k2eAgFnSc4d1	politická
podporu	podpora	k1gFnSc4	podpora
proti	proti	k7c3	proti
Pompeiovi	Pompeius	k1gMnSc3	Pompeius
<g/>
,	,	kIx,	,
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
Crassus	Crassus	k1gMnSc1	Crassus
část	část	k1gFnSc1	část
z	z	k7c2	z
Caesarových	Caesarových	k2eAgInPc2d1	Caesarových
dluhů	dluh	k1gInPc2	dluh
a	a	k8xC	a
za	za	k7c4	za
zbylé	zbylý	k2eAgInPc4d1	zbylý
se	se	k3xPyFc4	se
zaručil	zaručit	k5eAaPmAgMnS	zaručit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
musel	muset	k5eAaImAgMnS	muset
Caesar	Caesar	k1gMnSc1	Caesar
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
provincie	provincie	k1gFnSc2	provincie
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
uplynutím	uplynutí	k1gNnSc7	uplynutí
svého	svůj	k3xOyFgNnSc2	svůj
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gNnSc3	on
hrozila	hrozit	k5eAaImAgFnS	hrozit
žaloba	žaloba	k1gFnSc1	žaloba
pro	pro	k7c4	pro
neplacení	neplacení	k1gNnSc4	neplacení
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
si	se	k3xPyFc3	se
Caesar	Caesar	k1gMnSc1	Caesar
počínal	počínat	k5eAaImAgMnS	počínat
velice	velice	k6eAd1	velice
zdatně	zdatně	k6eAd1	zdatně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
agresivní	agresivní	k2eAgNnPc1d1	agresivní
válečná	válečný	k2eAgNnPc1d1	válečné
tažení	tažení	k1gNnPc1	tažení
proti	proti	k7c3	proti
Iberům	Iber	k1gMnPc3	Iber
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
posílily	posílit	k5eAaPmAgInP	posílit
jeho	jeho	k3xOp3gFnPc4	jeho
renomé	renomé	k1gNnSc2	renomé
schopného	schopný	k2eAgMnSc4d1	schopný
velitele	velitel	k1gMnSc4	velitel
a	a	k8xC	a
získaná	získaný	k2eAgFnSc1d1	získaná
kořist	kořist	k1gFnSc1	kořist
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
uhradit	uhradit	k5eAaPmF	uhradit
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc4	část
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
vysokých	vysoký	k2eAgInPc2d1	vysoký
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Lusitány	Lusitán	k1gMnPc7	Lusitán
a	a	k8xC	a
Callaici	Callaic	k1gMnPc7	Callaic
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc4	voják
pozdraven	pozdraven	k2eAgInSc4d1	pozdraven
jako	jako	k8xS	jako
imperátor	imperátor	k1gMnSc1	imperátor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
mu	on	k3xPp3gNnSc3	on
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
triumfu	triumf	k1gInSc2	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
republikánského	republikánský	k2eAgInSc2d1	republikánský
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
magistratus	magistratus	k1gInSc1	magistratus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
oslavit	oslavit	k5eAaPmF	oslavit
triumf	triumf	k1gInSc4	triumf
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
by	by	kYmCp3nP	by
zůstat	zůstat	k5eAaPmF	zůstat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
až	až	k9	až
do	do	k7c2	do
zahájení	zahájení	k1gNnSc2	zahájení
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
chtěl	chtít	k5eAaImAgMnS	chtít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
by	by	kYmCp3nS	by
složit	složit	k5eAaPmF	složit
velení	velení	k1gNnSc1	velení
a	a	k8xC	a
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	oboj	k1gFnSc7	oboj
současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
neproveditelné	proveditelný	k2eNgNnSc1d1	neproveditelné
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
proto	proto	k8xC	proto
senát	senát	k1gInSc4	senát
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
kandidovat	kandidovat	k5eAaImF	kandidovat
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
(	(	kIx(	(
<g/>
in	in	k?	in
absentia	absentia	k1gFnSc1	absentia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tomu	ten	k3xDgMnSc3	ten
však	však	k9	však
zabránil	zabránit	k5eAaPmAgInS	zabránit
Cato	Cato	k6eAd1	Cato
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
výběru	výběr	k1gInSc2	výběr
mezi	mezi	k7c7	mezi
triumfem	triumf	k1gInSc7	triumf
a	a	k8xC	a
konzulátem	konzulát	k1gInSc7	konzulát
dal	dát	k5eAaPmAgMnS	dát
Caesar	Caesar	k1gMnSc1	Caesar
nakonec	nakonec	k6eAd1	nakonec
přednost	přednost	k1gFnSc4	přednost
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
volba	volba	k1gFnSc1	volba
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
nedůstojných	důstojný	k2eNgFnPc2d1	nedůstojná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Kandidáti	kandidát	k1gMnPc1	kandidát
byli	být	k5eAaImAgMnP	být
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgMnPc1	tři
<g/>
:	:	kIx,	:
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
Lucius	Lucius	k1gMnSc1	Lucius
Lucceius	Lucceius	k1gMnSc1	Lucceius
a	a	k8xC	a
Marcus	Marcus	k1gMnSc1	Marcus
Calpurnius	Calpurnius	k1gMnSc1	Calpurnius
Bibulus	Bibulus	k1gMnSc1	Bibulus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
zastával	zastávat	k5eAaImAgInS	zastávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Caesarem	Caesar	k1gMnSc7	Caesar
úřad	úřad	k1gInSc4	úřad
aedila	aedit	k5eAaBmAgFnS	aedit
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
Cicerona	Cicero	k1gMnSc2	Cicero
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
bohatým	bohatý	k2eAgMnSc7d1	bohatý
Lucceiem	Lucceius	k1gMnSc7	Lucceius
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Cato	Cato	k1gMnSc1	Cato
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
těšil	těšit	k5eAaImAgInS	těšit
reputaci	reputace	k1gFnSc4	reputace
nezkorumpovaného	zkorumpovaný	k2eNgMnSc2d1	nezkorumpovaný
a	a	k8xC	a
poctivého	poctivý	k2eAgMnSc2d1	poctivý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
k	k	k7c3	k
uplácení	uplácení	k1gNnSc3	uplácení
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
konzervativního	konzervativní	k2eAgNnSc2d1	konzervativní
Bibula	Bibulum	k1gNnSc2	Bibulum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
takto	takto	k6eAd1	takto
oboustranně	oboustranně	k6eAd1	oboustranně
zmanipulovaných	zmanipulovaný	k2eAgFnPc2d1	zmanipulovaná
voleb	volba	k1gFnPc2	volba
vzešli	vzejít	k5eAaPmAgMnP	vzejít
jako	jako	k9	jako
konzulové	konzul	k1gMnPc1	konzul
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
59	[number]	k4	59
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Bibulus	Bibulus	k1gMnSc1	Bibulus
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
z	z	k7c2	z
dřívějška	dřívějšek	k1gInSc2	dřívějšek
Crassovým	Crassův	k2eAgMnSc7d1	Crassův
dlužníkem	dlužník	k1gMnSc7	dlužník
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nebránilo	bránit	k5eNaImAgNnS	bránit
ve	v	k7c6	v
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
s	s	k7c7	s
Pompeiem	Pompeius	k1gMnSc7	Pompeius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
neúspěšně	úspěšně	k6eNd1	úspěšně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
schválení	schválení	k1gNnSc4	schválení
jím	jíst	k5eAaImIp1nS	jíst
ustaveného	ustavený	k2eAgNnSc2d1	ustavené
uspořádání	uspořádání	k1gNnSc2	uspořádání
východních	východní	k2eAgFnPc2d1	východní
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
přidělení	přidělení	k1gNnSc4	přidělení
půdy	půda	k1gFnSc2	půda
svým	svůj	k3xOyFgMnPc3	svůj
veteránům	veterán	k1gMnPc3	veterán
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gInSc1	Pompeius
a	a	k8xC	a
Crassus	Crassus	k1gInSc1	Crassus
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
70	[number]	k4	70
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společně	společně	k6eAd1	společně
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dohodne	dohodnout	k5eAaPmIp3nS	dohodnout
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
<g/>
,	,	kIx,	,
ztratí	ztratit	k5eAaPmIp3nS	ztratit
podporu	podpora	k1gFnSc4	podpora
druhého	druhý	k4xOgNnSc2	druhý
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
usmíření	usmíření	k1gNnSc6	usmíření
obou	dva	k4xCgMnPc2	dva
rivalů	rival	k1gMnPc2	rival
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
,	,	kIx,	,
disponovali	disponovat	k5eAaBmAgMnP	disponovat
dostatkem	dostatek	k1gInSc7	dostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
vlivu	vliv	k1gInSc2	vliv
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
úplné	úplný	k2eAgFnSc2d1	úplná
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
neformální	formální	k2eNgFnSc1d1	neformální
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
triumvirát	triumvirát	k1gInSc1	triumvirát
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
tří	tři	k4xCgMnPc2	tři
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
upevněna	upevnit	k5eAaPmNgFnS	upevnit
svatbou	svatba	k1gFnSc7	svatba
Pompeia	Pompeius	k1gMnSc2	Pompeius
s	s	k7c7	s
Caesarovou	Caesarův	k2eAgFnSc7d1	Caesarova
dcerou	dcera	k1gFnSc7	dcera
Julií	Julie	k1gFnPc2	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Calpurnií	Calpurnie	k1gFnSc7	Calpurnie
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Lucia	Lucius	k1gMnSc2	Lucius
Calpurnia	Calpurnium	k1gNnSc2	Calpurnium
Pisona	Pison	k1gMnSc2	Pison
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
konzulem	konzul	k1gMnSc7	konzul
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
triumvirátu	triumvirát	k1gInSc2	triumvirát
<g/>
,	,	kIx,	,
když	když	k8xS	když
Caesar	Caesar	k1gMnSc1	Caesar
podal	podat	k5eAaPmAgMnS	podat
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
přerozdělení	přerozdělení	k1gNnSc4	přerozdělení
půdy	půda	k1gFnSc2	půda
chudým	chudý	k1gMnPc3	chudý
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jak	jak	k8xC	jak
Pompeius	Pompeius	k1gInSc4	Pompeius
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
Crassus	Crassus	k1gInSc4	Crassus
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
návrhem	návrh	k1gInSc7	návrh
souhlas	souhlas	k1gInSc1	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gMnSc1	Pompeius
poté	poté	k6eAd1	poté
vyslal	vyslat	k5eAaPmAgMnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byli	být	k5eAaImAgMnP	být
političtí	politický	k2eAgMnPc1d1	politický
oponenti	oponent	k1gMnPc1	oponent
triumvirů	triumvir	k1gMnPc2	triumvir
dokonale	dokonale	k6eAd1	dokonale
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
a	a	k8xC	a
zastrašeni	zastrašit	k5eAaPmNgMnP	zastrašit
<g/>
.	.	kIx.	.
</s>
<s>
Bibulus	Bibulus	k1gInSc1	Bibulus
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
nato	nato	k6eAd1	nato
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
znamení	znamení	k1gNnSc4	znamení
za	za	k7c4	za
nepříznivá	příznivý	k2eNgNnPc4d1	nepříznivé
a	a	k8xC	a
zákon	zákon	k1gInSc4	zákon
tudíž	tudíž	k8xC	tudíž
za	za	k7c4	za
neplatný	platný	k2eNgInSc4d1	neplatný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
z	z	k7c2	z
fóra	fórum	k1gNnSc2	fórum
Caesarovými	Caesarův	k2eAgMnPc7d1	Caesarův
přívrženci	přívrženec	k1gMnPc7	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Fasces	Fasces	k1gInSc1	Fasces
jeho	jeho	k3xOp3gMnPc2	jeho
liktorů	liktor	k1gMnPc2	liktor
byly	být	k5eAaImAgFnP	být
rozlámány	rozlámán	k2eAgFnPc1d1	rozlámána
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
tribunové	tribun	k1gMnPc1	tribun
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
doprovodu	doprovod	k1gInSc6	doprovod
byli	být	k5eAaImAgMnP	být
zraněni	zranit	k5eAaPmNgMnP	zranit
a	a	k8xC	a
Bibulus	Bibulus	k1gInSc4	Bibulus
sám	sám	k3xTgInSc4	sám
byl	být	k5eAaImAgInS	být
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
hozenými	hozený	k2eAgInPc7d1	hozený
exkrementy	exkrement	k1gInPc7	exkrement
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
strachu	strach	k1gInSc2	strach
o	o	k7c4	o
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
příležitostně	příležitostně	k6eAd1	příležitostně
vydával	vydávat	k5eAaPmAgInS	vydávat
proklamace	proklamace	k1gFnSc2	proklamace
o	o	k7c6	o
špatných	špatný	k2eAgNnPc6d1	špatné
znameních	znamení	k1gNnPc6	znamení
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
Caesarovi	Caesar	k1gMnSc3	Caesar
zabránit	zabránit	k5eAaPmF	zabránit
ve	v	k7c6	v
vykonávání	vykonávání	k1gNnSc6	vykonávání
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obstrukce	obstrukce	k1gFnPc1	obstrukce
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
neúčinné	účinný	k2eNgFnPc4d1	neúčinná
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
satirikové	satirik	k1gMnPc1	satirik
potom	potom	k6eAd1	potom
už	už	k6eAd1	už
navždy	navždy	k6eAd1	navždy
referovali	referovat	k5eAaBmAgMnP	referovat
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
jako	jako	k8xS	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
roce	rok	k1gInSc6	rok
konzulátu	konzulát	k1gInSc2	konzulát
Julia	Julius	k1gMnSc2	Julius
a	a	k8xC	a
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Bibulus	Bibulus	k1gMnSc1	Bibulus
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
<g/>
,	,	kIx,	,
pokusila	pokusit	k5eAaPmAgFnS	pokusit
se	se	k3xPyFc4	se
aristokracie	aristokracie	k1gFnSc1	aristokracie
omezit	omezit	k5eAaPmF	omezit
budoucí	budoucí	k2eAgFnSc4d1	budoucí
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
prokonzul	prokonzul	k1gMnSc1	prokonzul
moc	moc	k1gFnSc1	moc
(	(	kIx(	(
<g/>
následující	následující	k2eAgFnSc1d1	následující
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
místo	místo	k1gNnSc1	místo
správy	správa	k1gFnSc2	správa
nějaké	nějaký	k3yIgFnSc2	nějaký
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
přidělila	přidělit	k5eAaPmAgFnS	přidělit
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
lesy	les	k1gInPc7	les
a	a	k8xC	a
pastvinami	pastvina	k1gFnPc7	pastvina
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Pisona	Pisona	k1gFnSc1	Pisona
a	a	k8xC	a
Pompeia	Pompeia	k1gFnSc1	Pompeia
Caesar	Caesar	k1gMnSc1	Caesar
toto	tento	k3xDgNnSc4	tento
usnesení	usnesení	k1gNnSc4	usnesení
zvrátil	zvrátit	k5eAaPmAgInS	zvrátit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
za	za	k7c4	za
místodržitele	místodržitel	k1gMnPc4	místodržitel
Předalpské	předalpský	k2eAgFnSc2d1	Předalpská
Galie	Galie	k1gFnSc2	Galie
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ilýrie	Ilýrie	k1gFnSc1	Ilýrie
(	(	kIx(	(
<g/>
západní	západní	k2eAgInSc1d1	západní
Balkán	Balkán	k1gInSc1	Balkán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
obdržel	obdržet	k5eAaPmAgMnS	obdržet
také	také	k9	také
správu	správa	k1gFnSc4	správa
Zaalpské	zaalpský	k2eAgFnSc2d1	zaalpská
Galie	Galie	k1gFnSc2	Galie
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získal	získat	k5eAaPmAgMnS	získat
velení	velení	k1gNnSc4	velení
nad	nad	k7c7	nad
čtyřmi	čtyři	k4xCgFnPc7	čtyři
legiemi	legie	k1gFnPc7	legie
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gInSc2	on
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k8xC	i
doba	doba	k1gFnSc1	doba
jeho	jeho	k3xOp3gFnSc2	jeho
imunity	imunita	k1gFnSc2	imunita
před	před	k7c7	před
soudním	soudní	k2eAgNnSc7d1	soudní
stíháním	stíhání	k1gNnSc7	stíhání
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
místo	místo	k7c2	místo
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konzulátu	konzulát	k1gInSc2	konzulát
Caesar	Caesar	k1gMnSc1	Caesar
těsně	těsně	k6eAd1	těsně
unikl	uniknout	k5eAaPmAgMnS	uniknout
obžalobě	obžaloba	k1gFnSc3	obžaloba
kvůli	kvůli	k7c3	kvůli
nezákonnostem	nezákonnost	k1gFnPc3	nezákonnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
děly	dít	k5eAaBmAgFnP	dít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
úřadování	úřadování	k1gNnSc2	úřadování
<g/>
,	,	kIx,	,
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
do	do	k7c2	do
svěřených	svěřený	k2eAgFnPc2d1	svěřená
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Galské	galský	k2eAgFnSc2d1	galská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Prokonzulát	Prokonzulát	k1gInSc1	Prokonzulát
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
Caesara	Caesar	k1gMnSc4	Caesar
vynikající	vynikající	k2eAgFnSc4d1	vynikající
možnost	možnost	k1gFnSc4	možnost
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
mocenskému	mocenský	k2eAgInSc3d1	mocenský
vzestupu	vzestup	k1gInSc3	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
enormně	enormně	k6eAd1	enormně
zadlužený	zadlužený	k2eAgInSc1d1	zadlužený
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
správcovství	správcovství	k1gNnSc1	správcovství
provincie	provincie	k1gFnSc2	provincie
představovalo	představovat	k5eAaImAgNnS	představovat
výtečný	výtečný	k2eAgInSc4d1	výtečný
způsob	způsob	k1gInSc4	způsob
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
domoci	domoct	k5eAaPmF	domoct
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
podléhaly	podléhat	k5eAaImAgFnP	podléhat
čtyři	čtyři	k4xCgFnPc4	čtyři
legie	legie	k1gFnPc4	legie
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
Ilýrie	Ilýrie	k1gFnSc1	Ilýrie
a	a	k8xC	a
Gallia	Gallia	k1gFnSc1	Gallia
Narbonensis	Narbonensis	k1gFnSc1	Narbonensis
<g/>
,	,	kIx,	,
hraničily	hraničit	k5eAaImAgFnP	hraničit
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
nepodrobenými	podrobený	k2eNgNnPc7d1	podrobený
teritorii	teritorium	k1gNnPc7	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
naskýtala	naskýtat	k5eAaImAgFnS	naskýtat
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
neklidnými	klidný	k2eNgInPc7d1	neklidný
a	a	k8xC	a
znesvářenými	znesvářený	k2eAgInPc7d1	znesvářený
kmeny	kmen	k1gInPc7	kmen
probíhaly	probíhat	k5eAaImAgFnP	probíhat
již	již	k9	již
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
se	se	k3xPyFc4	se
Galové	Galové	k2eAgMnPc1d1	Galové
vzájemně	vzájemně	k6eAd1	vzájemně
oslabovali	oslabovat	k5eAaImAgMnP	oslabovat
a	a	k8xC	a
připravovali	připravovat	k5eAaImAgMnP	připravovat
tak	tak	k9	tak
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
podrobení	podrobení	k1gNnSc4	podrobení
cizí	cizí	k2eAgFnSc7d1	cizí
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
napjaté	napjatý	k2eAgFnPc1d1	napjatá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
římští	římský	k2eAgMnPc1d1	římský
spojenci	spojenec	k1gMnPc1	spojenec
Haeduové	Haeduová	k1gFnSc2	Haeduová
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
svým	svůj	k3xOyFgMnPc3	svůj
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
přizvali	přizvat	k5eAaPmAgMnP	přizvat
germánské	germánský	k2eAgInPc4d1	germánský
Svéby	Svéb	k1gInPc4	Svéb
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
náčelníkem	náčelník	k1gMnSc7	náčelník
Ariovistem	Ariovist	k1gMnSc7	Ariovist
<g/>
.	.	kIx.	.
</s>
<s>
Svébové	Svéb	k1gMnPc1	Svéb
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
dobyté	dobytý	k2eAgFnSc6d1	dobytá
zemi	zem	k1gFnSc6	zem
Haeduů	Haedu	k1gInPc2	Haedu
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
také	také	k9	také
Helvétové	Helvét	k1gMnPc1	Helvét
se	se	k3xPyFc4	se
chystali	chystat	k5eAaImAgMnP	chystat
proniknout	proniknout	k5eAaPmF	proniknout
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
nová	nový	k2eAgNnPc4d1	nové
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
okamžitě	okamžitě	k6eAd1	okamžitě
odvedl	odvést	k5eAaPmAgMnS	odvést
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
legie	legie	k1gFnPc4	legie
a	a	k8xC	a
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bibracte	Bibract	k1gInSc5	Bibract
Helvéty	Helvét	k1gMnPc7	Helvét
v	v	k7c6	v
roce	rok	k1gInSc6	rok
58	[number]	k4	58
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
drtivě	drtivě	k6eAd1	drtivě
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc4	jejich
přeživší	přeživší	k2eAgInPc4d1	přeživší
zbytky	zbytek	k1gInPc4	zbytek
odeslal	odeslat	k5eAaPmAgMnS	odeslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
někdejší	někdejší	k2eAgFnSc2d1	někdejší
sídelní	sídelní	k2eAgFnSc2d1	sídelní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
nárazník	nárazník	k1gInSc4	nárazník
před	před	k7c7	před
pronikajícími	pronikající	k2eAgMnPc7d1	pronikající
Germány	Germán	k1gMnPc7	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
vytáhl	vytáhnout	k5eAaPmAgInS	vytáhnout
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Alsaska	Alsasko	k1gNnSc2	Alsasko
proti	proti	k7c3	proti
Ariovistovi	Ariovista	k1gMnSc3	Ariovista
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
Germány	Germán	k1gMnPc7	Germán
vypudil	vypudit	k5eAaPmAgMnS	vypudit
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
Rýn	Rýn	k1gInSc4	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vítězné	vítězný	k2eAgNnSc4d1	vítězné
vojsko	vojsko	k1gNnSc4	vojsko
nechal	nechat	k5eAaPmAgMnS	nechat
přezimovat	přezimovat	k5eAaBmF	přezimovat
na	na	k7c4	na
území	území	k1gNnSc4	území
Sekvánů	Sekván	k1gMnPc2	Sekván
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dal	dát	k5eAaPmAgMnS	dát
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
zásahy	zásah	k1gInPc1	zásah
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
severně	severně	k6eAd1	severně
od	od	k7c2	od
Narbonensis	Narbonensis	k1gFnSc2	Narbonensis
nebyly	být	k5eNaImAgInP	být
pouze	pouze	k6eAd1	pouze
dočasného	dočasný	k2eAgInSc2d1	dočasný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
Caesar	Caesar	k1gMnSc1	Caesar
podrobil	podrobit	k5eAaPmAgMnS	podrobit
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Galii	Galie	k1gFnSc6	Galie
Belgy	Belg	k1gMnPc4	Belg
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nejudatnější	udatný	k2eAgInSc4d3	udatný
lid	lid	k1gInSc4	lid
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
galskými	galský	k2eAgInPc7d1	galský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
56	[number]	k4	56
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porazil	porazit	k5eAaPmAgMnS	porazit
jeho	jeho	k3xOp3gMnSc1	jeho
legát	legát	k1gMnSc1	legát
Decimus	Decimus	k1gMnSc1	Decimus
Junius	Junius	k1gMnSc1	Junius
Brutus	Brutus	k1gMnSc1	Brutus
povstalé	povstalý	k2eAgFnSc2d1	povstalá
Venety	Veneta	k1gFnSc2	Veneta
v	v	k7c6	v
Armorice	Armorika	k1gFnSc6	Armorika
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Bretaň	Bretaň	k1gFnSc1	Bretaň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Caesar	Caesar	k1gMnSc1	Caesar
přivedl	přivést	k5eAaPmAgMnS	přivést
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
západní	západní	k2eAgFnSc2d1	západní
Galie	Galie	k1gFnSc2	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimní	zimní	k2eAgFnSc2d1	zimní
přestávky	přestávka	k1gFnSc2	přestávka
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Ravenně	Ravenně	k1gFnSc6	Ravenně
s	s	k7c7	s
Pompeiem	Pompeius	k1gMnSc7	Pompeius
a	a	k8xC	a
Crassem	Crass	k1gMnSc7	Crass
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jejich	jejich	k3xOp3gNnSc2	jejich
jednání	jednání	k1gNnSc2	jednání
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
prodloužení	prodloužení	k1gNnSc6	prodloužení
Caesarova	Caesarův	k2eAgNnSc2d1	Caesarovo
imperia	imperium	k1gNnSc2	imperium
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Crassus	Crassus	k1gInSc1	Crassus
a	a	k8xC	a
Pompeius	Pompeius	k1gInSc1	Pompeius
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
stát	stát	k5eAaImF	stát
konzuly	konzul	k1gMnPc4	konzul
pro	pro	k7c4	pro
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovi	Caesar	k1gMnSc3	Caesar
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
poskytnut	poskytnut	k2eAgInSc4d1	poskytnut
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
podmanil	podmanit	k5eAaPmAgMnS	podmanit
všechny	všechen	k3xTgInPc4	všechen
dosud	dosud	k6eAd1	dosud
svobodné	svobodný	k2eAgInPc4d1	svobodný
keltské	keltský	k2eAgInPc4d1	keltský
kraje	kraj	k1gInPc4	kraj
rozkládající	rozkládající	k2eAgInPc4d1	rozkládající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Pyrenejemi	Pyreneje	k1gFnPc7	Pyreneje
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
ulehčena	ulehčit	k5eAaPmNgFnS	ulehčit
nejednotností	nejednotnost	k1gFnSc7	nejednotnost
Galů	Gal	k1gMnPc2	Gal
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
leckdy	leckdy	k6eAd1	leckdy
neváhali	váhat	k5eNaImAgMnP	váhat
obrátit	obrátit	k5eAaPmF	obrátit
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
sousedům	soused	k1gMnPc3	soused
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
nestali	stát	k5eNaPmAgMnP	stát
obětí	oběť	k1gFnSc7	oběť
ambiciózního	ambiciózní	k2eAgMnSc2d1	ambiciózní
prokonzula	prokonzul	k1gMnSc2	prokonzul
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vpádu	vpád	k1gInSc3	vpád
germánských	germánský	k2eAgInPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
Usipetů	Usipet	k1gInPc2	Usipet
a	a	k8xC	a
Tenkterů	Tenkter	k1gInPc2	Tenkter
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
považoval	považovat	k5eAaImAgMnS	považovat
Caesar	Caesar	k1gMnSc1	Caesar
za	za	k7c4	za
nezbytné	zbytný	k2eNgNnSc4d1	nezbytné
překročit	překročit	k5eAaPmF	překročit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
55	[number]	k4	55
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Rýn	Rýn	k1gInSc4	Rýn
a	a	k8xC	a
podniknout	podniknout	k5eAaPmF	podniknout
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Germánie	Germánie	k1gFnSc2	Germánie
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
53	[number]	k4	53
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
nezdržel	zdržet	k5eNaPmAgMnS	zdržet
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
jeho	jeho	k3xOp3gFnSc1	jeho
flotila	flotila	k1gFnSc1	flotila
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
bouří	bouř	k1gFnSc7	bouř
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
54	[number]	k4	54
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
provedl	provést	k5eAaPmAgMnS	provést
Caesar	Caesar	k1gMnSc1	Caesar
další	další	k2eAgMnSc1d1	další
vpád	vpád	k1gInSc4	vpád
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
postoupil	postoupit	k5eAaPmAgInS	postoupit
až	až	k6eAd1	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Thamesus	Thamesus	k1gInSc1	Thamesus
(	(	kIx(	(
<g/>
Temže	Temže	k1gFnSc1	Temže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
formálním	formální	k2eAgNnSc6d1	formální
ujištění	ujištění	k1gNnSc6	ujištění
britanských	britanský	k2eAgInPc2d1	britanský
kmenů	kmen	k1gInPc2	kmen
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
poddanství	poddanství	k1gNnSc6	poddanství
a	a	k8xC	a
loajalitě	loajalita	k1gFnSc6	loajalita
vůči	vůči	k7c3	vůči
Římu	Řím	k1gInSc3	Řím
se	se	k3xPyFc4	se
ale	ale	k9	ale
opět	opět	k6eAd1	opět
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
výpravy	výprava	k1gFnPc1	výprava
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
senátu	senát	k1gInSc6	senát
senzaci	senzace	k1gFnSc3	senzace
<g/>
:	:	kIx,	:
Caesar	Caesar	k1gMnSc1	Caesar
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
vedl	vést	k5eAaImAgMnS	vést
vojenské	vojenský	k2eAgFnPc4d1	vojenská
výpravy	výprava	k1gFnPc4	výprava
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
pro	pro	k7c4	pro
Římany	Říman	k1gMnPc4	Říman
dosud	dosud	k6eAd1	dosud
neznámých	známý	k2eNgFnPc2d1	neznámá
končin	končina	k1gFnPc2	končina
<g/>
,	,	kIx,	,
od	od	k7c2	od
trvalého	trvalý	k2eAgNnSc2d1	trvalé
obsazení	obsazení	k1gNnSc2	obsazení
Germánie	Germánie	k1gFnSc2	Germánie
a	a	k8xC	a
Británie	Británie	k1gFnSc2	Británie
však	však	k9	však
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
stanovení	stanovení	k1gNnSc1	stanovení
hranice	hranice	k1gFnSc2	hranice
římského	římský	k2eAgInSc2d1	římský
záboru	zábor	k1gInSc2	zábor
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
Caesarových	Caesarových	k2eAgNnPc2d1	Caesarových
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
evropských	evropský	k2eAgFnPc2d1	Evropská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
šířící	šířící	k2eAgMnSc1d1	šířící
se	se	k3xPyFc4	se
neklid	neklid	k1gInSc1	neklid
mezi	mezi	k7c7	mezi
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
podrobenými	podrobený	k2eAgInPc7d1	podrobený
galskými	galský	k2eAgInPc7d1	galský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
slabé	slabý	k2eAgFnSc3d1	slabá
úrodě	úroda	k1gFnSc3	úroda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
54	[number]	k4	54
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
povstaly	povstat	k5eAaPmAgFnP	povstat
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
kmeny	kmen	k1gInPc7	kmen
Eburonů	Eburon	k1gMnPc2	Eburon
<g/>
,	,	kIx,	,
Treverů	Trever	k1gMnPc2	Trever
a	a	k8xC	a
Nerviů	Nervi	k1gMnPc2	Nervi
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
náčelník	náčelník	k1gMnSc1	náčelník
Ambiorix	Ambiorix	k1gInSc4	Ambiorix
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
sice	sice	k8xC	sice
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
ale	ale	k9	ale
definitivně	definitivně	k6eAd1	definitivně
zbaveny	zbavit	k5eAaPmNgFnP	zbavit
svobody	svoboda	k1gFnPc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Větším	veliký	k2eAgNnSc7d2	veliký
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
římskou	římský	k2eAgFnSc4d1	římská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
povstání	povstání	k1gNnSc1	povstání
Vercingetoriga	Vercingetorig	k1gMnSc2	Vercingetorig
<g/>
,	,	kIx,	,
náčelníka	náčelník	k1gMnSc2	náčelník
kmene	kmen	k1gInSc2	kmen
Arvernů	Arvern	k1gInPc2	Arvern
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
52	[number]	k4	52
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
prvnímu	první	k4xOgNnSc3	první
podařilo	podařit	k5eAaPmAgNnS	podařit
sjednotit	sjednotit	k5eAaPmF	sjednotit
všechny	všechen	k3xTgMnPc4	všechen
Galy	Gal	k1gMnPc4	Gal
a	a	k8xC	a
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
objevil	objevit	k5eAaPmAgMnS	objevit
první	první	k4xOgMnSc1	první
skutečně	skutečně	k6eAd1	skutečně
rovnocenný	rovnocenný	k2eAgMnSc1d1	rovnocenný
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
taktika	taktika	k1gFnSc1	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
přivodila	přivodit	k5eAaBmAgFnS	přivodit
Caesarovým	Caesarův	k2eAgFnPc3d1	Caesarova
legiím	legie	k1gFnPc3	legie
vážné	vážná	k1gFnSc2	vážná
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vercingetorix	Vercingetorix	k1gInSc4	Vercingetorix
nejprve	nejprve	k6eAd1	nejprve
odřízl	odříznout	k5eAaPmAgMnS	odříznout
Caesara	Caesar	k1gMnSc2	Caesar
od	od	k7c2	od
jeho	jeho	k3xOp3gFnPc2	jeho
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
odrazil	odrazit	k5eAaPmAgMnS	odrazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gergovie	Gergovie	k1gFnSc2	Gergovie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
ale	ale	k8xC	ale
Vercingentorix	Vercingentorix	k1gInSc1	Vercingentorix
nerozvážně	rozvážně	k6eNd1	rozvážně
upustil	upustit	k5eAaPmAgInS	upustit
od	od	k7c2	od
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
opatrné	opatrný	k2eAgFnSc2d1	opatrná
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
taktiky	taktika	k1gFnSc2	taktika
a	a	k8xC	a
pln	pln	k2eAgMnSc1d1	pln
sebedůvěry	sebedůvěra	k1gFnPc4	sebedůvěra
napadl	napadnout	k5eAaPmAgMnS	napadnout
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nedostatečně	dostatečně	k6eNd1	dostatečně
vycvičená	vycvičený	k2eAgFnSc1d1	vycvičená
jízda	jízda	k1gFnSc1	jízda
byla	být	k5eAaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
Vercingetorix	Vercingetorix	k1gInSc1	Vercingetorix
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
do	do	k7c2	do
Alesie	Alesie	k1gFnSc2	Alesie
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
začal	začít	k5eAaPmAgMnS	začít
toto	tento	k3xDgNnSc4	tento
oppidum	oppidum	k1gNnSc4	oppidum
bez	bez	k7c2	bez
otálení	otálení	k1gNnSc2	otálení
obklopovat	obklopovat	k5eAaImF	obklopovat
třicet	třicet	k4xCc4	třicet
pět	pět	k4xCc4	pět
kilometrů	kilometr	k1gInPc2	kilometr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
obléhací	obléhací	k2eAgFnSc4d1	obléhací
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
Galie	Galie	k1gFnSc1	Galie
vydalo	vydat	k5eAaPmAgNnS	vydat
Vercingetorigovi	Vercingetorig	k1gMnSc3	Vercingetorig
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
ohromné	ohromný	k2eAgNnSc1d1	ohromné
osvobozovací	osvobozovací	k2eAgNnSc1d1	osvobozovací
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Alesii	Alesie	k1gFnSc6	Alesie
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Germánské	germánský	k2eAgFnSc2d1	germánská
jízdy	jízda	k1gFnSc2	jízda
dokázali	dokázat	k5eAaPmAgMnP	dokázat
navzdory	navzdory	k7c3	navzdory
nesmírné	smírný	k2eNgFnSc3d1	nesmírná
převaze	převaha	k1gFnSc3	převaha
Galů	Gal	k1gMnPc2	Gal
ubránit	ubránit	k5eAaPmF	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Vercingetoriga	Vercingetorig	k1gMnSc2	Vercingetorig
byl	být	k5eAaImAgInS	být
galský	galský	k2eAgInSc1d1	galský
odpor	odpor	k1gInSc1	odpor
definitivně	definitivně	k6eAd1	definitivně
zlomen	zlomit	k5eAaPmNgInS	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
46	[number]	k4	46
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
triumfu	triumf	k1gInSc2	triumf
Vercingetorix	Vercingetorix	k1gInSc1	Vercingetorix
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
musel	muset	k5eAaImAgMnS	muset
Caesar	Caesar	k1gMnSc1	Caesar
čelit	čelit	k5eAaImF	čelit
ještě	ještě	k6eAd1	ještě
ojedinělým	ojedinělý	k2eAgFnPc3d1	ojedinělá
lokálním	lokální	k2eAgFnPc3d1	lokální
vzpourám	vzpoura	k1gFnPc3	vzpoura
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
potlačil	potlačit	k5eAaPmAgInS	potlačit
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
brutalitou	brutalita	k1gFnSc7	brutalita
<g/>
.	.	kIx.	.
</s>
<s>
Nezměrnou	změrný	k2eNgFnSc4d1	nezměrná
válečnou	válečný	k2eAgFnSc4d1	válečná
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
tribut	tribut	k1gInSc4	tribut
placený	placený	k2eAgInSc1d1	placený
poraženými	poražený	k2eAgInPc7d1	poražený
kmeny	kmen	k1gInPc7	kmen
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
financování	financování	k1gNnSc3	financování
vlastní	vlastní	k2eAgFnSc2d1	vlastní
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
čítala	čítat	k5eAaImAgFnS	čítat
třináct	třináct	k4xCc4	třináct
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
se	se	k3xPyFc4	se
již	již	k9	již
Caesar	Caesar	k1gMnSc1	Caesar
nemusel	muset	k5eNaImAgMnS	muset
obávat	obávat	k5eAaImF	obávat
věřitelů	věřitel	k1gMnPc2	věřitel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c4	v
Galii	Galie	k1gFnSc4	Galie
nabyl	nabýt	k5eAaPmAgInS	nabýt
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
uloupil	uloupit	k5eAaPmAgMnS	uloupit
<g/>
)	)	kIx)	)
ohromné	ohromný	k2eAgNnSc1d1	ohromné
bohatství	bohatství	k1gNnSc1	bohatství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
učinilo	učinit	k5eAaPmAgNnS	učinit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejzámožnějších	zámožný	k2eAgMnPc2d3	nejzámožnější
římských	římský	k2eAgMnPc2d1	římský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Caesarem	Caesar	k1gMnSc7	Caesar
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
jeho	jeho	k3xOp3gMnPc2	jeho
legátů	legát	k1gMnPc2	legát
jeho	jeho	k3xOp3gFnSc4	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Lucius	Lucius	k1gMnSc1	Lucius
Iulius	Iulius	k1gMnSc1	Iulius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
<g/>
,	,	kIx,	,
Titus	Titus	k1gMnSc1	Titus
Labienus	Labienus	k1gMnSc1	Labienus
<g/>
,	,	kIx,	,
Quintus	Quintus	k1gMnSc1	Quintus
Tullius	Tullius	k1gMnSc1	Tullius
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Marca	Marcum	k1gNnSc2	Marcum
Tullia	Tullius	k1gMnSc2	Tullius
Cicerona	Cicero	k1gMnSc2	Cicero
<g/>
,	,	kIx,	,
a	a	k8xC	a
Crassův	Crassův	k2eAgMnSc1d1	Crassův
stejnojmenný	stejnojmenný	k2eAgMnSc1d1	stejnojmenný
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Publius	Publius	k1gMnSc1	Publius
Licinius	Licinius	k1gMnSc1	Licinius
Crassus	Crassus	k1gMnSc1	Crassus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Plútarcha	Plútarch	k1gMnSc2	Plútarch
bylo	být	k5eAaImAgNnS	být
výsledkem	výsledek	k1gInSc7	výsledek
tažení	tažení	k1gNnPc2	tažení
800	[number]	k4	800
podmaněných	podmaněný	k2eAgNnPc2d1	podmaněné
galských	galský	k2eAgNnPc2d1	galské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
300	[number]	k4	300
podrobených	podrobený	k2eAgInPc2d1	podrobený
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
poražených	poražený	k2eAgMnPc2d1	poražený
Galů	Gal	k1gMnPc2	Gal
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
taktiky	taktika	k1gFnSc2	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Antičtí	antický	k2eAgMnPc1d1	antický
historikové	historik	k1gMnPc1	historik
měli	mít	k5eAaImAgMnP	mít
obvykle	obvykle	k6eAd1	obvykle
tendenci	tendence	k1gFnSc4	tendence
zveličovat	zveličovat	k5eAaImF	zveličovat
všechny	všechen	k3xTgInPc4	všechen
početní	početní	k2eAgInPc4d1	početní
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Caesarovy	Caesarův	k2eAgInPc4d1	Caesarův
výboje	výboj	k1gInPc4	výboj
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
byly	být	k5eAaImAgFnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
největší	veliký	k2eAgFnSc7d3	veliký
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
operací	operace	k1gFnSc7	operace
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Alexandrova	Alexandrův	k2eAgNnSc2d1	Alexandrovo
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
důsledky	důsledek	k1gInPc1	důsledek
navíc	navíc	k6eAd1	navíc
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
podstatně	podstatně	k6eAd1	podstatně
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
:	:	kIx,	:
Galie	Galie	k1gFnSc1	Galie
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nezískala	získat	k5eNaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
věrně	věrně	k6eAd1	věrně
setrvávala	setrvávat	k5eAaImAgFnS	setrvávat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Říma	Řím	k1gInSc2	Řím
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
poznatky	poznatek	k1gInPc4	poznatek
z	z	k7c2	z
tažení	tažení	k1gNnSc2	tažení
zachytil	zachytit	k5eAaPmAgMnS	zachytit
Caesar	Caesar	k1gMnSc1	Caesar
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Commentarii	Commentarie	k1gFnSc3	Commentarie
de	de	k?	de
bello	bello	k1gNnSc1	bello
Gallico	Gallico	k1gMnSc1	Gallico
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zápisky	zápiska	k1gFnPc1	zápiska
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgNnSc1d1	galské
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
písemnost	písemnost	k1gFnSc1	písemnost
kromě	kromě	k7c2	kromě
průběhu	průběh	k1gInSc2	průběh
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
líčí	líčit	k5eAaImIp3nS	líčit
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
detailů	detail	k1gInPc2	detail
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
zvycích	zvyk	k1gInPc6	zvyk
Galů	Gal	k1gMnPc2	Gal
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
především	především	k6eAd1	především
ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
Caesarových	Caesarových	k2eAgInPc2d1	Caesarových
agresivních	agresivní	k2eAgInPc2d1	agresivní
a	a	k8xC	a
dobyvačných	dobyvačný	k2eAgInPc2d1	dobyvačný
výbojů	výboj	k1gInPc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
Galy	Gal	k1gMnPc7	Gal
a	a	k8xC	a
Germány	Germán	k1gMnPc7	Germán
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
je	být	k5eAaImIp3nS	být
odlišil	odlišit	k5eAaPmAgMnS	odlišit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
kmenů	kmen	k1gInPc2	kmen
stanovil	stanovit	k5eAaPmAgInS	stanovit
Rýn	Rýn	k1gInSc1	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Germáni	Germán	k1gMnPc1	Germán
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokládáni	pokládán	k2eAgMnPc1d1	pokládán
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
keltských	keltský	k2eAgInPc2d1	keltský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
úspěchy	úspěch	k1gInPc4	úspěch
a	a	k8xC	a
přínos	přínos	k1gInSc4	přínos
Římu	Řím	k1gInSc3	Řím
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
Caesar	Caesar	k1gMnSc1	Caesar
nadále	nadále	k6eAd1	nadále
značně	značně	k6eAd1	značně
nepopulární	populární	k2eNgMnSc1d1	nepopulární
mezi	mezi	k7c7	mezi
konzervativními	konzervativní	k2eAgMnPc7d1	konzervativní
senátory	senátor	k1gMnPc7	senátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
podezřívali	podezřívat	k5eAaImAgMnP	podezřívat
z	z	k7c2	z
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
královské	královský	k2eAgFnSc6d1	královská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
55	[number]	k4	55
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byli	být	k5eAaImAgMnP	být
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
spojenci	spojenec	k1gMnPc1	spojenec
Pompeius	Pompeius	k1gMnSc1	Pompeius
a	a	k8xC	a
Crassus	Crassus	k1gMnSc1	Crassus
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
za	za	k7c7	za
konzuly	konzul	k1gMnPc7	konzul
a	a	k8xC	a
Caesarovi	Caesar	k1gMnSc6	Caesar
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ujednáním	ujednání	k1gNnSc7	ujednání
triumvirů	triumvir	k1gMnPc2	triumvir
prodloužen	prodloužen	k2eAgInSc4d1	prodloužen
prokonzulát	prokonzulát	k1gInSc4	prokonzulát
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byl	být	k5eAaImAgInS	být
fakticky	fakticky	k6eAd1	fakticky
poslední	poslední	k2eAgInSc1d1	poslední
moment	moment	k1gInSc1	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
triumvirové	triumvir	k1gMnPc1	triumvir
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
souladu	soulad	k1gInSc6	soulad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
54	[number]	k4	54
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zemřela	zemřít	k5eAaPmAgFnS	zemřít
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
dcera	dcera	k1gFnSc1	dcera
Julia	Julius	k1gMnSc2	Julius
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
Pompeiovou	Pompeiový	k2eAgFnSc7d1	Pompeiový
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
příbuzenský	příbuzenský	k2eAgInSc1d1	příbuzenský
svazek	svazek	k1gInSc1	svazek
mezi	mezi	k7c7	mezi
Pompeiem	Pompeius	k1gMnSc7	Pompeius
a	a	k8xC	a
Caesarem	Caesar	k1gMnSc7	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
Crassus	Crassus	k1gInSc1	Crassus
zabit	zabít	k5eAaPmNgInS	zabít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Karrh	Karrha	k1gFnPc2	Karrha
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gMnSc1	Pompeius
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
přízeň	přízeň	k1gFnSc4	přízeň
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
optimátů	optimát	k1gMnPc2	optimát
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
plně	plně	k6eAd1	plně
zaměstnaný	zaměstnaný	k2eAgInSc1d1	zaměstnaný
záležitostmi	záležitost	k1gFnPc7	záležitost
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
udržet	udržet	k5eAaPmF	udržet
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Pompeiem	Pompeius	k1gMnSc7	Pompeius
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
ruku	ruka	k1gFnSc4	ruka
jedné	jeden	k4xCgFnSc3	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
praneteří	praneteř	k1gFnPc2	praneteř
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Pompeius	Pompeius	k1gMnSc1	Pompeius
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Cornelií	Cornelie	k1gFnSc7	Cornelie
Metellou	Metella	k1gFnSc7	Metella
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Metella	Metell	k1gMnSc2	Metell
Scipiona	Scipion	k1gMnSc2	Scipion
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejúhlavnějších	úhlavní	k2eAgMnPc2d3	nejúhlavnější
Caesarových	Caesarových	k2eAgMnPc2d1	Caesarových
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Pompeiem	Pompeius	k1gMnSc7	Pompeius
nařídil	nařídit	k5eAaPmAgInS	nařídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Caesarovi	Caesar	k1gMnSc3	Caesar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
svoji	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gNnSc4	jeho
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
jako	jako	k8xS	jako
prokonzula	prokonzul	k1gMnSc4	prokonzul
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
navíc	navíc	k6eAd1	navíc
Caesarovi	Caesar	k1gMnSc3	Caesar
znovu	znovu	k6eAd1	znovu
zakázal	zakázat	k5eAaPmAgMnS	zakázat
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
in	in	k?	in
absentia	absentium	k1gNnSc2	absentium
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
konzulát	konzulát	k1gInSc4	konzulát
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
mu	on	k3xPp3gMnSc3	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
přidělit	přidělit	k5eAaPmF	přidělit
novou	nový	k2eAgFnSc4d1	nová
provincii	provincie	k1gFnSc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
vzdá	vzdát	k5eAaPmIp3nS	vzdát
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
okamžitě	okamžitě	k6eAd1	okamžitě
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Pompeius	Pompeius	k1gMnSc1	Pompeius
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Caesara	Caesar	k1gMnSc4	Caesar
z	z	k7c2	z
porušení	porušení	k1gNnSc2	porušení
kázně	kázeň	k1gFnSc2	kázeň
a	a	k8xC	a
zrady	zrada	k1gFnSc2	zrada
<g/>
,	,	kIx,	,
překročil	překročit	k5eAaPmAgMnS	překročit
Caesar	Caesar	k1gMnSc1	Caesar
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
49	[number]	k4	49
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
legií	legie	k1gFnSc7	legie
řeku	řeka	k1gFnSc4	řeka
Rubikon	Rubikon	k1gInSc1	Rubikon
(	(	kIx(	(
<g/>
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
hranice	hranice	k1gFnSc1	hranice
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tak	tak	k6eAd1	tak
další	další	k2eAgFnSc4d1	další
fázi	fáze	k1gFnSc4	fáze
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
této	tento	k3xDgFnSc2	tento
řeky	řeka	k1gFnSc2	řeka
měl	mít	k5eAaImAgMnS	mít
Caesar	Caesar	k1gMnSc1	Caesar
pronést	pronést	k5eAaPmF	pronést
slavný	slavný	k2eAgInSc4d1	slavný
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
alea	ale	k2eAgFnSc1d1	alea
iacta	iacta	k1gFnSc1	iacta
est	est	k?	est
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kostky	kostka	k1gFnPc1	kostka
jsou	být	k5eAaImIp3nP	být
vrženy	vržen	k2eAgFnPc1d1	vržena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Optimáti	optimát	k1gMnPc1	optimát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Catona	Caton	k1gMnSc2	Caton
mladšího	mladý	k2eAgMnSc2d2	mladší
a	a	k8xC	a
Metella	Metell	k1gMnSc2	Metell
Scipiona	Scipion	k1gMnSc2	Scipion
<g/>
,	,	kIx,	,
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
postrádajíce	postrádat	k5eAaImSgMnP	postrádat
důvěry	důvěra	k1gFnPc4	důvěra
v	v	k7c6	v
nově	nova	k1gFnSc6	nova
odvedené	odvedený	k2eAgNnSc1d1	odvedené
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
když	když	k8xS	když
se	se	k3xPyFc4	se
města	město	k1gNnPc1	město
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
ochotně	ochotně	k6eAd1	ochotně
vydávala	vydávat	k5eAaPmAgFnS	vydávat
do	do	k7c2	do
Caesarových	Caesarových	k2eAgFnPc2d1	Caesarových
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
senátní	senátní	k2eAgNnSc1d1	senátní
vojsko	vojsko	k1gNnSc1	vojsko
početně	početně	k6eAd1	početně
vysoce	vysoce	k6eAd1	vysoce
převyšovalo	převyšovat	k5eAaImAgNnS	převyšovat
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stále	stále	k6eAd1	stále
disponoval	disponovat	k5eAaBmAgInS	disponovat
pouze	pouze	k6eAd1	pouze
svojí	svojit	k5eAaImIp3nS	svojit
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
Pompeius	Pompeius	k1gMnSc1	Pompeius
se	se	k3xPyFc4	se
neodvážil	odvážit	k5eNaPmAgMnS	odvážit
svést	svést	k5eAaPmF	svést
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
proto	proto	k8xC	proto
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
Pompeia	Pompeius	k1gMnSc4	Pompeius
do	do	k7c2	do
Brundisia	Brundisium	k1gNnSc2	Brundisium
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pompeia	Pompeia	k1gFnSc1	Pompeia
a	a	k8xC	a
senátory	senátor	k1gMnPc4	senátor
polapí	polapit	k5eAaPmIp3nP	polapit
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
legiemi	legie	k1gFnPc7	legie
uniknou	uniknout	k5eAaPmIp3nP	uniknout
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
Caesar	Caesar	k1gMnSc1	Caesar
konečně	konečně	k6eAd1	konečně
obsadil	obsadit	k5eAaPmAgMnS	obsadit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
již	již	k9	již
Pompeius	Pompeius	k1gInSc1	Pompeius
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
vojska	vojsko	k1gNnSc2	vojsko
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
Pompeia	Pompeium	k1gNnSc2	Pompeium
mu	on	k3xPp3gMnSc3	on
chybělo	chybět	k5eAaImAgNnS	chybět
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
proti	proti	k7c3	proti
Pompeiovým	Pompeiový	k2eAgFnPc3d1	Pompeiový
legiím	legie	k1gFnPc3	legie
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jak	jak	k6eAd1	jak
pravil	pravit	k5eAaBmAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejprve	nejprve	k6eAd1	nejprve
budu	být	k5eAaImBp1nS	být
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
vojsku	vojsko	k1gNnSc3	vojsko
bez	bez	k7c2	bez
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
obrátím	obrátit	k5eAaPmIp1nS	obrátit
proti	proti	k7c3	proti
vojevůdci	vojevůdce	k1gMnSc3	vojevůdce
bez	bez	k7c2	bez
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Marca	Marc	k2eAgMnSc4d1	Marc
Aemilia	Aemilius	k1gMnSc4	Aemilius
Lepida	Lepid	k1gMnSc4	Lepid
ponechal	ponechat	k5eAaPmAgMnS	ponechat
jako	jako	k8xS	jako
prefekta	prefekt	k1gMnSc2	prefekt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
správu	správa	k1gFnSc4	správa
zbytku	zbytek	k1gInSc2	zbytek
Itálie	Itálie	k1gFnSc2	Itálie
svěřil	svěřit	k5eAaPmAgInS	svěřit
Marcu	Marc	k1gMnSc3	Marc
Antoniovi	Antonio	k1gMnSc3	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozoruhodného	pozoruhodný	k2eAgNnSc2d1	pozoruhodné
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgMnPc2d1	pouhý
dvacet	dvacet	k4xCc1	dvacet
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
trvajícího	trvající	k2eAgInSc2d1	trvající
pochodu	pochod	k1gInSc2	pochod
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
město	město	k1gNnSc4	město
Massilii	Massilie	k1gFnSc4	Massilie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Galii	Galie	k1gFnSc6	Galie
a	a	k8xC	a
odsud	odsud	k6eAd1	odsud
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
pompeiovským	pompeiovský	k2eAgNnSc7d1	pompeiovský
vojskem	vojsko	k1gNnSc7	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Ilerdy	Ilerda	k1gFnSc2	Ilerda
a	a	k8xC	a
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
ho	on	k3xPp3gMnSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
ustaven	ustavit	k5eAaPmNgMnS	ustavit
diktátorem	diktátor	k1gMnSc7	diktátor
s	s	k7c7	s
Marcem	Marce	k1gMnSc7	Marce
Antoniem	Antonio	k1gMnSc7	Antonio
jako	jako	k8xS	jako
velitelem	velitel	k1gMnSc7	velitel
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
dnech	den	k1gInPc6	den
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
podruhé	podruhé	k6eAd1	podruhé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
konzulem	konzul	k1gMnSc7	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
s	s	k7c7	s
15	[number]	k4	15
000	[number]	k4	000
muži	muž	k1gMnPc7	muž
překročil	překročit	k5eAaPmAgMnS	překročit
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
vypravil	vypravit	k5eAaPmAgMnS	vypravit
se	se	k3xPyFc4	se
vypudit	vypudit	k5eAaPmF	vypudit
Pompeia	Pompeia	k1gFnSc1	Pompeia
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Dyrrhachia	Dyrrhachius	k1gMnSc2	Dyrrhachius
se	se	k3xPyFc4	se
jen	jen	k9	jen
se	s	k7c7	s
štěstím	štěstí	k1gNnSc7	štěstí
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
katastrofální	katastrofální	k2eAgFnSc3d1	katastrofální
porážce	porážka	k1gFnSc3	porážka
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pompeiovcům	pompeiovec	k1gMnPc3	pompeiovec
podařilo	podařit	k5eAaPmAgNnS	podařit
prolomit	prolomit	k5eAaPmF	prolomit
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
Thesálie	Thesálie	k1gFnSc2	Thesálie
a	a	k8xC	a
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
částmi	část	k1gFnPc7	část
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zatím	zatím	k6eAd1	zatím
dorazily	dorazit	k5eAaPmAgInP	dorazit
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gMnSc1	Pompeius
a	a	k8xC	a
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Farsálu	Farsál	k1gInSc2	Farsál
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgInS	být
Pompeius	Pompeius	k1gInSc1	Pompeius
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
měl	mít	k5eAaImAgMnS	mít
nad	nad	k7c7	nad
Caesarem	Caesar	k1gMnSc7	Caesar
převahu	převah	k1gInSc2	převah
v	v	k7c6	v
pěchotě	pěchota	k1gFnSc6	pěchota
i	i	k8xC	i
jízdě	jízda	k1gFnSc6	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Pompeiových	Pompeiový	k2eAgMnPc2d1	Pompeiový
stoupenců	stoupenec	k1gMnPc2	stoupenec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
se	se	k3xPyFc4	se
rozprchlo	rozprchnout	k5eAaPmAgNnS	rozprchnout
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
koutů	kout	k1gInPc2	kout
římského	římský	k2eAgInSc2d1	římský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
podněcovali	podněcovat	k5eAaImAgMnP	podněcovat
další	další	k2eAgInSc4d1	další
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
Caesarovi	Caesar	k1gMnSc3	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
od	od	k7c2	od
Caesara	Caesar	k1gMnSc2	Caesar
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gMnSc1	Pompeius
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
porážce	porážka	k1gFnSc6	porážka
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc2	král
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
zákeřně	zákeřně	k6eAd1	zákeřně
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
Pompeia	Pompeius	k1gMnSc4	Pompeius
do	do	k7c2	do
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
pouze	pouze	k6eAd1	pouze
uťatá	uťatý	k2eAgFnSc1d1	uťatá
hlava	hlava	k1gFnSc1	hlava
jeho	jeho	k3xOp3gMnSc2	jeho
rivala	rival	k1gMnSc2	rival
a	a	k8xC	a
někdejšího	někdejší	k2eAgMnSc4d1	někdejší
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nechal	nechat	k5eAaPmAgMnS	nechat
Caesar	Caesar	k1gMnSc1	Caesar
přepravit	přepravit	k5eAaPmF	přepravit
a	a	k8xC	a
pohřbít	pohřbít	k5eAaPmF	pohřbít
Pompeiovy	Pompeiův	k2eAgInPc4d1	Pompeiův
ostatky	ostatek	k1gInPc4	ostatek
s	s	k7c7	s
veškerými	veškerý	k3xTgFnPc7	veškerý
poctami	pocta	k1gFnPc7	pocta
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
svoji	svůj	k3xOyFgFnSc4	svůj
mírnost	mírnost	k1gFnSc4	mírnost
k	k	k7c3	k
mrtvému	mrtvý	k2eAgMnSc3d1	mrtvý
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
zapletl	zaplést	k5eAaPmAgMnS	zaplést
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Ptolemaiem	Ptolemaios	k1gMnSc7	Ptolemaios
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
spoluvládkyní	spoluvládkyně	k1gFnSc7	spoluvládkyně
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
kvůli	kvůli	k7c3	kvůli
Ptolemaiově	Ptolemaiův	k2eAgFnSc3d1	Ptolemaiova
roli	role	k1gFnSc3	role
v	v	k7c6	v
Pompeiově	Pompeiův	k2eAgNnSc6d1	Pompeiovo
zavraždění	zavraždění	k1gNnSc6	zavraždění
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
přidal	přidat	k5eAaPmAgMnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Kleopatry	Kleopatra	k1gFnSc2	Kleopatra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
"	"	kIx"	"
<g/>
alexandrijské	alexandrijský	k2eAgFnSc6d1	Alexandrijská
válce	válka	k1gFnSc6	válka
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
nejprve	nejprve	k6eAd1	nejprve
zatlačen	zatlačit	k5eAaPmNgMnS	zatlačit
do	do	k7c2	do
defenzívy	defenzíva	k1gFnSc2	defenzíva
a	a	k8xC	a
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obležení	obležení	k1gNnSc2	obležení
lehla	lehnout	k5eAaPmAgFnS	lehnout
popelem	popel	k1gInSc7	popel
slavná	slavný	k2eAgFnSc1d1	slavná
alexandrijská	alexandrijský	k2eAgFnSc1d1	Alexandrijská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
posil	posít	k5eAaPmAgMnS	posít
Caesar	Caesar	k1gMnSc1	Caesar
snadno	snadno	k6eAd1	snadno
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
královou	králův	k2eAgFnSc7d1	králova
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
utonul	utonout	k5eAaPmAgMnS	utonout
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
dosadil	dosadit	k5eAaPmAgMnS	dosadit
Kleopatru	Kleopatra	k1gFnSc4	Kleopatra
na	na	k7c4	na
egyptský	egyptský	k2eAgInSc4d1	egyptský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
otcem	otec	k1gMnSc7	otec
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Caesara	Caesar	k1gMnSc4	Caesar
známého	známý	k1gMnSc4	známý
jako	jako	k8xS	jako
Kaisarion	Kaisarion	k1gInSc4	Kaisarion
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
strávených	strávený	k2eAgInPc2d1	strávený
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
u	u	k7c2	u
Kleopatry	Kleopatra	k1gFnSc2	Kleopatra
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
47	[number]	k4	47
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obrátil	obrátit	k5eAaPmAgMnS	obrátit
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
proti	proti	k7c3	proti
pontskému	pontský	k2eAgMnSc3d1	pontský
králi	král	k1gMnSc3	král
Farnakovi	Farnak	k1gMnSc3	Farnak
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přepadl	přepadnout	k5eAaPmAgInS	přepadnout
tamější	tamější	k2eAgFnPc4d1	tamější
římské	římský	k2eAgFnPc4d1	římská
provincie	provincie	k1gFnPc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
<g/>
,	,	kIx,	,
pětidenním	pětidenní	k2eAgNnSc6d1	pětidenní
tažení	tažení	k1gNnSc6	tažení
byl	být	k5eAaImAgInS	být
Farnakés	Farnakés	k1gInSc1	Farnakés
poražen	porazit	k5eAaPmNgInS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zely	zet	k5eAaImAgInP	zet
<g/>
.	.	kIx.	.
</s>
<s>
Senátu	senát	k1gInSc2	senát
poslal	poslat	k5eAaPmAgMnS	poslat
poté	poté	k6eAd1	poté
Caesar	Caesar	k1gMnSc1	Caesar
stručný	stručný	k2eAgInSc4d1	stručný
komentář	komentář	k1gInSc4	komentář
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Veni	Veni	k1gNnSc1	Veni
<g/>
,	,	kIx,	,
vidi	vidi	kA	vidi
<g/>
,	,	kIx,	,
vici	vici	k1gNnSc1	vici
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Přišel	přijít	k5eAaPmAgMnS	přijít
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgMnS	vidět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgInS	vypořádat
se	s	k7c7	s
zbývajícími	zbývající	k2eAgMnPc7d1	zbývající
Pompeiovými	Pompeiový	k2eAgMnPc7d1	Pompeiový
straníky	straník	k1gMnPc7	straník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Thapsu	Thaps	k1gInSc2	Thaps
v	v	k7c6	v
roce	rok	k1gInSc6	rok
46	[number]	k4	46
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rychle	rychle	k6eAd1	rychle
přemohl	přemoct	k5eAaPmAgMnS	přemoct
republikánské	republikánský	k2eAgNnSc1d1	republikánské
vojsko	vojsko	k1gNnSc1	vojsko
vedené	vedený	k2eAgNnSc1d1	vedené
Metellem	Metell	k1gMnSc7	Metell
Scipionem	Scipion	k1gInSc7	Scipion
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaPmAgMnS	padnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
Catonem	Caton	k1gMnSc7	Caton
mladším	mladý	k2eAgMnSc7d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
vojenské	vojenský	k2eAgFnSc6d1	vojenská
katastrofě	katastrofa	k1gFnSc6	katastrofa
spáchal	spáchat	k5eAaPmAgInS	spáchat
Cato	Cato	k6eAd1	Cato
v	v	k7c6	v
Utice	Utika	k1gFnSc6	Utika
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
Numidie	Numidie	k1gFnSc2	Numidie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
podporovalo	podporovat	k5eAaImAgNnS	podporovat
pompeiovce	pompeiovec	k1gInPc4	pompeiovec
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Caesarem	Caesar	k1gMnSc7	Caesar
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
v	v	k7c4	v
římskou	římský	k2eAgFnSc4d1	římská
provincii	provincie	k1gFnSc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Pompeiovi	Pompeiův	k2eAgMnPc1d1	Pompeiův
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Gnaeus	Gnaeus	k1gMnSc1	Gnaeus
Pompeius	Pompeius	k1gMnSc1	Pompeius
a	a	k8xC	a
Sextus	Sextus	k1gMnSc1	Sextus
Pompeius	Pompeius	k1gMnSc1	Pompeius
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Titem	Tit	k1gMnSc7	Tit
Labienem	Labien	k1gMnSc7	Labien
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
Caesarovým	Caesarův	k2eAgMnSc7d1	Caesarův
legátem	legát	k1gMnSc7	legát
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
galských	galský	k2eAgFnPc2d1	galská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
unikli	uniknout	k5eAaPmAgMnP	uniknout
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
Caesar	Caesar	k1gMnSc1	Caesar
vydal	vydat	k5eAaPmAgMnS	vydat
stíhat	stíhat	k5eAaImF	stíhat
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgInS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mundy	Munda	k1gFnSc2	Munda
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
resp.	resp.	kA	resp.
pompeiovci	pompeiovec	k1gMnPc1	pompeiovec
byli	být	k5eAaImAgMnP	být
definitivně	definitivně	k6eAd1	definitivně
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
republika	republika	k1gFnSc1	republika
fakticky	fakticky	k6eAd1	fakticky
dospěla	dochvít	k5eAaPmAgFnS	dochvít
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
Lepidem	Lepid	k1gInSc7	Lepid
potřetí	potřetí	k4xO	potřetí
a	a	k8xC	a
poté	poté	k6eAd1	poté
bez	bez	k7c2	bez
kolegy	kolega	k1gMnSc2	kolega
počtvrté	počtvrté	k4xO	počtvrté
konzulem	konzul	k1gMnSc7	konzul
(	(	kIx(	(
<g/>
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
desíti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
sepsání	sepsání	k1gNnSc3	sepsání
závěti	závěť	k1gFnSc2	závěť
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Gaia	Gaia	k1gMnSc1	Gaia
Octavia	octavia	k1gFnSc1	octavia
svým	svůj	k1gMnSc7	svůj
dědicem	dědic	k1gMnSc7	dědic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
očekáváním	očekávání	k1gNnSc7	očekávání
nepřistoupil	přistoupit	k5eNaPmAgMnS	přistoupit
Caesar	Caesar	k1gMnSc1	Caesar
k	k	k7c3	k
proskripcím	proskripce	k1gFnPc3	proskripce
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
udělil	udělit	k5eAaPmAgMnS	udělit
milost	milost	k1gFnSc4	milost
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Caesarových	Caesarových	k2eAgNnPc2d1	Caesarových
velkolepých	velkolepý	k2eAgNnPc2d1	velkolepé
vítězství	vítězství	k1gNnPc2	vítězství
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
honosné	honosný	k2eAgFnPc1d1	honosná
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovi	Caesar	k1gMnSc3	Caesar
bylo	být	k5eAaImAgNnS	být
přiznáno	přiznán	k2eAgNnSc1d1	přiznáno
právo	právo	k1gNnSc1	právo
nosit	nosit	k5eAaImF	nosit
při	při	k7c6	při
všech	všecek	k3xTgFnPc6	všecek
veřejných	veřejný	k2eAgFnPc6d1	veřejná
příležitostech	příležitost	k1gFnPc6	příležitost
roucho	roucho	k1gNnSc1	roucho
triumfátora	triumfátor	k1gMnSc2	triumfátor
včetně	včetně	k7c2	včetně
purpurové	purpurový	k2eAgFnSc2d1	purpurová
roby	roba	k1gFnSc2	roba
(	(	kIx(	(
<g/>
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
odívali	odívat	k5eAaImAgMnP	odívat
římští	římský	k2eAgMnPc1d1	římský
králové	král	k1gMnPc1	král
<g/>
)	)	kIx)	)
a	a	k8xC	a
vavřínového	vavřínový	k2eAgInSc2d1	vavřínový
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
zakrýt	zakrýt	k5eAaPmF	zakrýt
svoji	svůj	k3xOyFgFnSc4	svůj
pleš	pleš	k1gFnSc4	pleš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
státní	státní	k2eAgInPc4d1	státní
náklady	náklad	k1gInPc4	náklad
bylo	být	k5eAaImAgNnS	být
Caesarovi	Caesar	k1gMnSc3	Caesar
postaveno	postaven	k2eAgNnSc1d1	postaveno
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nádherné	nádherný	k2eAgNnSc1d1	nádherné
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
mu	on	k3xPp3gMnSc3	on
dále	daleko	k6eAd2	daleko
přiznal	přiznat	k5eAaPmAgMnS	přiznat
titul	titul	k1gInSc4	titul
doživotního	doživotní	k2eAgMnSc2d1	doživotní
diktátora	diktátor	k1gMnSc2	diktátor
(	(	kIx(	(
<g/>
dictator	dictator	k1gMnSc1	dictator
perpetuus	perpetuus	k1gMnSc1	perpetuus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jakýchkoli	jakýkoli	k3yIgNnPc6	jakýkoli
náboženských	náboženský	k2eAgNnPc6d1	náboženské
procesích	procesí	k1gNnPc6	procesí
byla	být	k5eAaImAgFnS	být
nesena	nesen	k2eAgFnSc1d1	nesena
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
socha	socha	k1gFnSc1	socha
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
socha	socha	k1gFnSc1	socha
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
neporazitelnému	porazitelný	k2eNgMnSc3d1	neporazitelný
bohu	bůh	k1gMnSc3	bůh
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Quirinově	Quirinův	k2eAgInSc6d1	Quirinův
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
nejen	nejen	k6eAd1	nejen
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
římským	římský	k2eAgMnPc3d1	římský
králům	král	k1gMnPc3	král
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Quirinovi	Quirin	k1gMnSc6	Quirin
byl	být	k5eAaImAgMnS	být
uctíván	uctíván	k2eAgMnSc1d1	uctíván
zakladatel	zakladatel	k1gMnSc1	zakladatel
města	město	k1gNnSc2	město
–	–	k?	–
Romulus	Romulus	k1gMnSc1	Romulus
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčit	k5eAaPmNgFnS	vztyčit
na	na	k7c6	na
Kapitolu	Kapitol	k1gInSc6	Kapitol
vedle	vedle	k7c2	vedle
soch	socha	k1gFnPc2	socha
sedmi	sedm	k4xCc2	sedm
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
a	a	k8xC	a
Lucia	Lucius	k1gMnSc4	Lucius
Junia	Junius	k1gMnSc4	Junius
Bruta	Brut	k1gMnSc4	Brut
<g/>
,	,	kIx,	,
Římana	Říman	k1gMnSc4	Říman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
vyhnat	vyhnat	k5eAaPmF	vyhnat
Tarquinia	Tarquinium	k1gNnPc1	Tarquinium
Superba	Superba	k1gMnSc1	Superba
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgInSc2d1	poslední
z	z	k7c2	z
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tak	tak	k9	tak
neslýchané	slýchaný	k2eNgNnSc4d1	neslýchané
jednání	jednání	k1gNnSc4	jednání
završil	završit	k5eAaPmAgMnS	završit
Caesar	Caesar	k1gMnSc1	Caesar
ražením	ražení	k1gNnSc7	ražení
peněz	peníze	k1gInPc2	peníze
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
portrétem	portrét	k1gInSc7	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
žijící	žijící	k2eAgMnSc1d1	žijící
Říman	Říman	k1gMnSc1	Říman
vyobrazován	vyobrazován	k2eAgMnSc1d1	vyobrazován
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
konzulátu	konzulát	k1gInSc2	konzulát
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
sebe	sebe	k3xPyFc4	sebe
určil	určit	k5eAaPmAgMnS	určit
za	za	k7c7	za
konzuly	konzul	k1gMnPc7	konzul
Quinta	Quinta	k1gFnSc1	Quinta
Fabia	fabia	k1gFnSc1	fabia
Maxima	Maxima	k1gFnSc1	Maxima
a	a	k8xC	a
Gaia	Gaia	k1gFnSc1	Gaia
Trebonia	Trebonium	k1gNnSc2	Trebonium
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
popudil	popudit	k5eAaPmAgInS	popudit
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tak	tak	k6eAd1	tak
úmyslně	úmyslně	k6eAd1	úmyslně
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
republikánský	republikánský	k2eAgInSc4d1	republikánský
systém	systém	k1gInSc4	systém
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
senátoři	senátor	k1gMnPc1	senátor
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
vzdávání	vzdávání	k1gNnSc6	vzdávání
poct	pocta	k1gFnPc2	pocta
Caesarovi	Caesarovi	k1gRnPc1	Caesarovi
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
chrámu	chrám	k1gInSc2	chrám
bohyně	bohyně	k1gFnSc2	bohyně
Libertas	Libertasa	k1gFnPc2	Libertasa
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Caesar	Caesar	k1gMnSc1	Caesar
titul	titul	k1gInSc4	titul
liberator	liberator	k1gInSc4	liberator
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
Říman	Říman	k1gMnSc1	Říman
bezprecedentní	bezprecedentní	k2eAgNnSc4d1	bezprecedentní
právo	právo	k1gNnSc4	právo
vlastního	vlastní	k2eAgNnSc2d1	vlastní
imperia	imperium	k1gNnSc2	imperium
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
jakoukoli	jakýkoli	k3yIgFnSc7	jakýkoli
žalobou	žaloba	k1gFnSc7	žaloba
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
stál	stát	k5eAaImAgMnS	stát
nad	nad	k7c7	nad
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
přiznáno	přiznán	k2eAgNnSc4d1	přiznáno
právo	právo	k1gNnSc4	právo
dosazovat	dosazovat	k5eAaImF	dosazovat
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgInPc2	všecek
magistrátů	magistrát	k1gInPc2	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
Quintilis	Quintilis	k1gFnSc2	Quintilis
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c6	na
Julius	Julius	k1gMnSc1	Julius
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
bylo	být	k5eAaImAgNnS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
tribue	tribue	k1gFnSc1	tribue
lidového	lidový	k2eAgNnSc2d1	lidové
shromáždění	shromáždění	k1gNnSc2	shromáždění
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
něm.	něm.	k?	něm.
Kromě	kromě	k7c2	kromě
přijímání	přijímání	k1gNnSc2	přijímání
různých	různý	k2eAgInPc2d1	různý
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
poct	pocta	k1gFnPc2	pocta
však	však	k9	však
Caesar	Caesar	k1gMnSc1	Caesar
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
srdci	srdce	k1gNnSc6	srdce
rovněž	rovněž	k9	rovněž
prospěch	prospěch	k1gInSc4	prospěch
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
sice	sice	k8xC	sice
strávil	strávit	k5eAaPmAgMnS	strávit
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
času	čas	k1gInSc2	čas
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
podařilo	podařit	k5eAaPmAgNnS	podařit
zavést	zavést	k5eAaPmF	zavést
množství	množství	k1gNnSc1	množství
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
vítězným	vítězný	k2eAgNnSc7d1	vítězné
završením	završení	k1gNnSc7	završení
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
Caesar	Caesar	k1gMnSc1	Caesar
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Kodifikací	kodifikace	k1gFnSc7	kodifikace
a	a	k8xC	a
přepracováním	přepracování	k1gNnSc7	přepracování
zákonů	zákon	k1gInPc2	zákon
plánoval	plánovat	k5eAaImAgMnS	plánovat
zásadně	zásadně	k6eAd1	zásadně
reformovat	reformovat	k5eAaBmF	reformovat
římský	římský	k2eAgInSc1d1	římský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušil	zjednodušit	k5eAaPmAgInS	zjednodušit
možnost	možnost	k1gFnSc4	možnost
nabytí	nabytí	k1gNnSc2	nabytí
římského	římský	k2eAgNnSc2d1	římské
občanství	občanství	k1gNnSc2	občanství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
hojně	hojně	k6eAd1	hojně
uděloval	udělovat	k5eAaImAgMnS	udělovat
galským	galský	k2eAgMnSc7d1	galský
<g/>
,	,	kIx,	,
hispánským	hispánský	k2eAgMnSc7d1	hispánský
a	a	k8xC	a
africkým	africký	k2eAgMnPc3d1	africký
provinciálům	provinciál	k1gMnPc3	provinciál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
započal	započnout	k5eAaPmAgMnS	započnout
romanizaci	romanizace	k1gFnSc3	romanizace
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
nechal	nechat	k5eAaPmAgInS	nechat
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc1	počet
senátorů	senátor	k1gMnPc2	senátor
ze	z	k7c2	z
600	[number]	k4	600
na	na	k7c4	na
900	[number]	k4	900
a	a	k8xC	a
vřadil	vřadit	k5eAaPmAgInS	vřadit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
dokonce	dokonce	k9	dokonce
některé	některý	k3yIgInPc4	některý
galské	galský	k2eAgInPc4d1	galský
náčelníky	náčelník	k1gInPc4	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
navýšil	navýšit	k5eAaPmAgMnS	navýšit
počty	počet	k1gInPc4	počet
většiny	většina	k1gFnSc2	většina
magistrátů	magistrát	k1gInPc2	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
založit	založit	k5eAaPmF	založit
dvacet	dvacet	k4xCc4	dvacet
nových	nový	k2eAgFnPc2d1	nová
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
přelidněného	přelidněný	k2eAgInSc2d1	přelidněný
Říma	Řím	k1gInSc2	Řím
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
veterány	veterán	k1gMnPc7	veterán
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
zaopatřil	zaopatřit	k5eAaPmAgMnS	zaopatřit
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
a	a	k8xC	a
Korint	Korint	k1gInSc1	Korint
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
zničená	zničený	k2eAgNnPc1d1	zničené
Římany	Říman	k1gMnPc4	Říman
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
hodlal	hodlat	k5eAaImAgMnS	hodlat
vysušit	vysušit	k5eAaPmF	vysušit
Pomptinské	Pomptinský	k2eAgInPc4d1	Pomptinský
močály	močál	k1gInPc4	močál
rozkládající	rozkládající	k2eAgInPc4d1	rozkládající
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
stavět	stavět	k5eAaImF	stavět
silnice	silnice	k1gFnPc4	silnice
a	a	k8xC	a
prokopat	prokopat	k5eAaPmF	prokopat
Korintskou	korintský	k2eAgFnSc4d1	Korintská
šíji	šíje	k1gFnSc4	šíje
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
dosud	dosud	k6eAd1	dosud
vcelku	vcelku	k6eAd1	vcelku
prostým	prostý	k2eAgNnSc7d1	prosté
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
ambiciózní	ambiciózní	k2eAgFnPc1d1	ambiciózní
veřejné	veřejný	k2eAgFnPc1d1	veřejná
stavby	stavba	k1gFnPc1	stavba
podle	podle	k7c2	podle
helénistického	helénistický	k2eAgInSc2d1	helénistický
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
senátu	senát	k1gInSc2	senát
Curia	curium	k1gNnSc2	curium
Hostilia	Hostilium	k1gNnSc2	Hostilium
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
byla	být	k5eAaImAgNnP	být
tudíž	tudíž	k8xC	tudíž
nazývána	nazýván	k2eAgNnPc1d1	nazýváno
Curia	curium	k1gNnPc1	curium
Iulia	Iulius	k1gMnSc2	Iulius
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
fórum	fórum	k1gNnSc1	fórum
s	s	k7c7	s
chrámem	chrám	k1gInSc7	chrám
Venuše	Venuše	k1gFnSc2	Venuše
Venetrix	Venetrix	k1gInSc1	Venetrix
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Venuše	Venuše	k1gFnSc2	Venuše
rodičky	rodička	k1gFnSc2	rodička
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomerium	Pomerium	k1gNnSc4	Pomerium
<g/>
,	,	kIx,	,
posvátné	posvátný	k2eAgFnPc4d1	posvátná
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dalšího	další	k2eAgInSc2d1	další
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
růstu	růst	k1gInSc2	růst
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
reformní	reformní	k2eAgFnSc6d1	reformní
činnosti	činnost	k1gFnSc6	činnost
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k9	i
řešení	řešení	k1gNnSc4	řešení
rozličných	rozličný	k2eAgInPc2d1	rozličný
společenských	společenský	k2eAgInPc2d1	společenský
neduhů	neduh	k1gInPc2	neduh
<g/>
.	.	kIx.	.
</s>
<s>
Schválil	schválit	k5eAaPmAgInS	schválit
zákon	zákon	k1gInSc4	zákon
zakazující	zakazující	k2eAgInSc4d1	zakazující
římským	římský	k2eAgMnSc7d1	římský
občanům	občan	k1gMnPc3	občan
opustit	opustit	k5eAaPmF	opustit
Itálii	Itálie	k1gFnSc4	Itálie
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
prosadil	prosadit	k5eAaPmAgInS	prosadit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušník	příslušník	k1gMnSc1	příslušník
společenské	společenský	k2eAgFnSc2d1	společenská
elity	elita	k1gFnSc2	elita
zranil	zranit	k5eAaPmAgMnS	zranit
nebo	nebo	k8xC	nebo
zabil	zabít	k5eAaPmAgInS	zabít
člena	člen	k1gMnSc4	člen
nižší	nízký	k2eAgFnSc2d2	nižší
společenské	společenský	k2eAgFnSc2d1	společenská
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgNnSc4	veškerý
bohatství	bohatství	k1gNnSc4	bohatství
pachatele	pachatel	k1gMnSc2	pachatel
bylo	být	k5eAaImAgNnS	být
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
dal	dát	k5eAaPmAgMnS	dát
zrušit	zrušit	k5eAaPmF	zrušit
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
všech	všecek	k3xTgInPc2	všecek
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
upevnil	upevnit	k5eAaPmAgInS	upevnit
svoji	svůj	k3xOyFgFnSc4	svůj
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pontifex	pontifex	k1gMnSc1	pontifex
maximus	maximus	k1gInSc4	maximus
měl	mít	k5eAaImAgMnS	mít
Caesar	Caesar	k1gMnSc1	Caesar
spravovat	spravovat	k5eAaImF	spravovat
záležitosti	záležitost	k1gFnPc4	záležitost
počítání	počítání	k1gNnSc2	počítání
času	čas	k1gInSc2	čas
a	a	k8xC	a
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
astronoma	astronom	k1gMnSc2	astronom
Sósigena	Sósigen	k1gMnSc2	Sósigen
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
46	[number]	k4	46
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nový	nový	k2eAgInSc4d1	nový
kalendář	kalendář	k1gInSc4	kalendář
s	s	k7c7	s
365	[number]	k4	365
dny	den	k1gInPc7	den
a	a	k8xC	a
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
rokem	rok	k1gInSc7	rok
přestupným	přestupný	k2eAgInSc7d1	přestupný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
papežem	papež	k1gMnSc7	papež
Řehořem	Řehoř	k1gMnSc7	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
moderní	moderní	k2eAgInSc4d1	moderní
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Antičtí	antický	k2eAgMnPc1d1	antický
historikové	historik	k1gMnPc1	historik
popisují	popisovat	k5eAaImIp3nP	popisovat
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Caesarem	Caesar	k1gMnSc7	Caesar
a	a	k8xC	a
senátem	senát	k1gInSc7	senát
pramenící	pramenící	k2eAgFnSc2d1	pramenící
z	z	k7c2	z
Caesarových	Caesarových	k2eAgInPc2d1	Caesarových
možných	možný	k2eAgInPc2d1	možný
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Caesar	Caesar	k1gMnSc1	Caesar
skutečně	skutečně	k6eAd1	skutečně
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
spokojil	spokojit	k5eAaPmAgMnS	spokojit
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
historiky	historik	k1gMnPc4	historik
prakticky	prakticky	k6eAd1	prakticky
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
postavení	postavení	k1gNnSc1	postavení
bylo	být	k5eAaImAgNnS	být
vskutku	vskutku	k9	vskutku
královské	královský	k2eAgNnSc1d1	královské
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přimět	přimět	k5eAaPmF	přimět
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
smířili	smířit	k5eAaPmAgMnP	smířit
s	s	k7c7	s
nastolením	nastolení	k1gNnSc7	nastolení
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
konzervativními	konzervativní	k2eAgMnPc7d1	konzervativní
optimáty	optimát	k1gMnPc7	optimát
a	a	k8xC	a
stoupenci	stoupenec	k1gMnPc7	stoupenec
republiky	republika	k1gFnSc2	republika
narůstal	narůstat	k5eAaImAgInS	narůstat
neklid	neklid	k1gInSc4	neklid
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
Caesarovi	Caesar	k1gMnSc3	Caesar
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
pohnutky	pohnutka	k1gFnPc4	pohnutka
a	a	k8xC	a
motivy	motiv	k1gInPc4	motiv
pro	pro	k7c4	pro
Caesarovo	Caesarův	k2eAgNnSc4d1	Caesarovo
pozdější	pozdní	k2eAgNnSc4d2	pozdější
zavraždění	zavraždění	k1gNnSc4	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gMnSc1	Suetonius
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Caesarově	Caesarův	k2eAgInSc6d1	Caesarův
návratu	návrat	k1gInSc6	návrat
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
davu	dav	k1gInSc2	dav
vložili	vložit	k5eAaPmAgMnP	vložit
na	na	k7c4	na
nedalekou	daleký	k2eNgFnSc4d1	nedaleká
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
sochu	socha	k1gFnSc4	socha
vavřínový	vavřínový	k2eAgInSc4d1	vavřínový
věnec	věnec	k1gInSc4	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Tribunové	tribun	k1gMnPc1	tribun
lidu	lid	k1gInSc2	lid
Gaius	Gaius	k1gInSc1	Gaius
Epidius	Epidius	k1gMnSc1	Epidius
Marcellus	Marcellus	k1gMnSc1	Marcellus
a	a	k8xC	a
Lucius	Lucius	k1gMnSc1	Lucius
Caesetius	Caesetius	k1gMnSc1	Caesetius
Flavius	Flavius	k1gMnSc1	Flavius
nařídili	nařídit	k5eAaPmAgMnP	nařídit
věnec	věnec	k1gInSc4	věnec
odstranit	odstranit	k5eAaPmF	odstranit
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
boha	bůh	k1gMnSc2	bůh
Jova	Jova	k1gMnSc1	Jova
a	a	k8xC	a
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
Caesar	Caesar	k1gMnSc1	Caesar
následně	následně	k6eAd1	následně
oba	dva	k4xCgMnPc4	dva
tribuny	tribun	k1gMnPc4	tribun
zbavil	zbavit	k5eAaPmAgMnS	zbavit
jejich	jejich	k3xOp3gInSc2	jejich
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
momentu	moment	k1gInSc2	moment
se	se	k3xPyFc4	se
Caesar	Caesar	k1gMnSc1	Caesar
prý	prý	k9	prý
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
oprostit	oprostit	k5eAaPmF	oprostit
od	od	k7c2	od
podezření	podezření	k1gNnSc2	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
královládu	královláda	k1gFnSc4	královláda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
přiživila	přiživit	k5eAaPmAgFnS	přiživit
příhoda	příhoda	k1gFnSc1	příhoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gInSc4	on
dav	dav	k1gInSc4	dav
tituloval	titulovat	k5eAaImAgMnS	titulovat
jako	jako	k9	jako
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Caesar	Caesar	k1gMnSc1	Caesar
nepříliš	příliš	k6eNd1	příliš
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejsem	být	k5eNaImIp1nS	být
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
slavnosti	slavnost	k1gFnSc6	slavnost
luperkálií	luperkálie	k1gFnPc2	luperkálie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Caesar	Caesar	k1gMnSc1	Caesar
pronášel	pronášet	k5eAaImAgMnS	pronášet
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
Marcus	Marcus	k1gInSc1	Marcus
Antonius	Antonius	k1gInSc1	Antonius
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
opakovaně	opakovaně	k6eAd1	opakovaně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vložit	vložit	k5eAaPmF	vložit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
diadém	diadém	k1gInSc1	diadém
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gInSc1	Suetonius
dále	daleko	k6eAd2	daleko
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lucius	Lucius	k1gMnSc1	Lucius
Cotta	Cotta	k1gMnSc1	Cotta
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
senát	senát	k1gInSc4	senát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
učinil	učinit	k5eAaPmAgMnS	učinit
Caesara	Caesar	k1gMnSc4	Caesar
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
proroctví	proroctví	k1gNnPc2	proroctví
Sibyliných	Sibylin	k2eAgFnPc2d1	Sibylina
knih	kniha	k1gFnPc2	kniha
mohl	moct	k5eAaImAgMnS	moct
pouze	pouze	k6eAd1	pouze
král	král	k1gMnSc1	král
porazit	porazit	k5eAaPmF	porazit
Parthy	Parth	k1gInPc4	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
udělili	udělit	k5eAaPmAgMnP	udělit
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
titul	titul	k1gInSc4	titul
pater	patro	k1gNnPc2	patro
patriae	patriae	k6eAd1	patriae
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
vlasti	vlast	k1gFnSc2	vlast
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gMnSc4	on
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
diktátorem	diktátor	k1gMnSc7	diktátor
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
(	(	kIx(	(
<g/>
dictator	dictator	k1gMnSc1	dictator
perpetuus	perpetuus	k1gMnSc1	perpetuus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Cassia	Cassius	k1gMnSc2	Cassius
Diona	Dion	k1gMnSc2	Dion
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přišla	přijít	k5eAaPmAgFnS	přijít
k	k	k7c3	k
Caesarovi	Caesar	k1gMnSc3	Caesar
ze	z	k7c2	z
senátu	senát	k1gInSc2	senát
delegace	delegace	k1gFnSc2	delegace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
těchto	tento	k3xDgFnPc2	tento
poct	pocta	k1gFnPc2	pocta
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
přijal	přijmout	k5eAaPmAgMnS	přijmout
vsedě	vsedě	k6eAd1	vsedě
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
se	se	k3xPyFc4	se
obtěžoval	obtěžovat	k5eAaImAgMnS	obtěžovat
vstát	vstát	k5eAaPmF	vstát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
prý	prý	k9	prý
poslední	poslední	k2eAgFnSc1d1	poslední
kapka	kapka	k1gFnSc1	kapka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
uražené	uražený	k2eAgMnPc4d1	uražený
senátory	senátor	k1gMnPc4	senátor
přiměla	přimět	k5eAaPmAgFnS	přimět
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zavraždění	zavraždění	k1gNnSc6	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovi	Caesarův	k2eAgMnPc1d1	Caesarův
příznivci	příznivec	k1gMnPc1	příznivec
měli	mít	k5eAaImAgMnP	mít
jeho	jeho	k3xOp3gNnSc4	jeho
selhání	selhání	k1gNnSc4	selhání
omlouvat	omlouvat	k5eAaImF	omlouvat
záchvatem	záchvat	k1gInSc7	záchvat
průjmu	průjem	k1gInSc2	průjem
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vykládali	vykládat	k5eAaImAgMnP	vykládat
jako	jako	k8xC	jako
projev	projev	k1gInSc4	projev
neúcty	neúcta	k1gFnSc2	neúcta
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovy	Caesarův	k2eAgFnPc1d1	Caesarova
plné	plný	k2eAgFnPc1d1	plná
moci	moc	k1gFnPc1	moc
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
senátorech	senátor	k1gMnPc6	senátor
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
,	,	kIx,	,
závist	závist	k1gFnSc4	závist
a	a	k8xC	a
především	především	k9	především
strach	strach	k1gInSc4	strach
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Caesar	Caesar	k1gMnSc1	Caesar
nastolí	nastolit	k5eAaPmIp3nS	nastolit
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
tyranii	tyranie	k1gFnSc4	tyranie
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Junius	Junius	k1gMnSc1	Junius
Brutus	Brutus	k1gMnSc1	Brutus
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
švagrem	švagr	k1gMnSc7	švagr
Gaiem	Gai	k1gMnSc7	Gai
Cassiem	Cassius	k1gMnSc7	Cassius
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
nazývajícími	nazývající	k2eAgMnPc7d1	nazývající
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
osvoboditeli	osvoboditel	k1gMnSc3	osvoboditel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
liberatores	liberatores	k1gInSc1	liberatores
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
proto	proto	k6eAd1	proto
připravovat	připravovat	k5eAaImF	připravovat
Caesarovo	Caesarův	k2eAgNnSc4d1	Caesarovo
zavraždění	zavraždění	k1gNnSc4	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
spiklence	spiklenec	k1gMnPc4	spiklenec
náleželi	náležet	k5eAaImAgMnP	náležet
kromě	kromě	k7c2	kromě
optimátů	optimát	k1gMnPc2	optimát
také	také	k9	také
populárové	populár	k1gMnPc1	populár
<g/>
,	,	kIx,	,
nepřátelé	nepřítel	k1gMnPc1	nepřítel
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
přátelé	přítel	k1gMnPc1	přítel
Caesara	Caesar	k1gMnSc4	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
zapojených	zapojený	k2eAgFnPc2d1	zapojená
do	do	k7c2	do
komplotu	komplot	k1gInSc2	komplot
čítal	čítat	k5eAaImAgInS	čítat
zřejmě	zřejmě	k6eAd1	zřejmě
kolem	kolem	k7c2	kolem
šedesáti	šedesát	k4xCc2	šedesát
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
činem	čin	k1gInSc7	čin
se	se	k3xPyFc4	se
Cassius	Cassius	k1gMnSc1	Cassius
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jim	on	k3xPp3gMnPc3	on
doporučil	doporučit	k5eAaPmAgInS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
odhalení	odhalení	k1gNnSc2	odhalení
obrátili	obrátit	k5eAaPmAgMnP	obrátit
své	své	k1gNnSc4	své
nože	nůž	k1gInSc2	nůž
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
březnové	březnový	k2eAgFnPc4d1	březnová
idy	idy	k1gFnPc4	idy
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pozvala	pozvat	k5eAaPmAgFnS	pozvat
skupina	skupina	k1gFnSc1	skupina
senátorů	senátor	k1gMnPc2	senátor
Caesara	Caesar	k1gMnSc2	Caesar
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
Pompeiově	Pompeiův	k2eAgNnSc6d1	Pompeiovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
vyslechnout	vyslechnout	k5eAaPmF	vyslechnout
žádost	žádost	k1gFnSc4	žádost
dotazující	dotazující	k2eAgFnSc4d1	dotazující
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
jenom	jenom	k6eAd1	jenom
záminkou	záminka	k1gFnSc7	záminka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vylákání	vylákání	k1gNnSc3	vylákání
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
prý	prý	k9	prý
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
velitele	velitel	k1gMnSc2	velitel
jízdy	jízda	k1gFnSc2	jízda
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
postihla	postihnout	k5eAaPmAgFnS	postihnout
Caesara	Caesar	k1gMnSc4	Caesar
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
odložit	odložit	k5eAaPmF	odložit
zasedání	zasedání	k1gNnSc4	zasedání
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Calpurnii	Calpurnie	k1gFnSc3	Calpurnie
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
zdálo	zdát	k5eAaImAgNnS	zdát
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zavraždění	zavraždění	k1gNnSc6	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
haruspex	haruspex	k1gMnSc1	haruspex
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
z	z	k7c2	z
věštby	věštba	k1gFnSc2	věštba
velmi	velmi	k6eAd1	velmi
neblahá	blahý	k2eNgNnPc4d1	neblahé
znamení	znamení	k1gNnPc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Decimus	Decimus	k1gMnSc1	Decimus
Junius	Junius	k1gMnSc1	Junius
Brutus	Brutus	k1gMnSc1	Brutus
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
zapletený	zapletený	k2eAgMnSc1d1	zapletený
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vydal	vydat	k5eAaPmAgInS	vydat
k	k	k7c3	k
váhajícímu	váhající	k2eAgMnSc3d1	váhající
diktátorovi	diktátor	k1gMnSc3	diktátor
a	a	k8xC	a
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jednání	jednání	k1gNnSc4	jednání
neodkládal	odkládat	k5eNaImAgMnS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Decimus	Decimus	k1gMnSc1	Decimus
Brutus	Brutus	k1gMnSc1	Brutus
odebrali	odebrat	k5eAaPmAgMnP	odebrat
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Caesar	Caesar	k1gMnSc1	Caesar
dopis	dopis	k1gInSc4	dopis
s	s	k7c7	s
varováním	varování	k1gNnSc7	varování
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
odložil	odložit	k5eAaPmAgMnS	odložit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
přečte	přečíst	k5eAaPmIp3nS	přečíst
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
nejasné	jasný	k2eNgNnSc4d1	nejasné
tušení	tušení	k1gNnSc4	tušení
o	o	k7c6	o
komplotu	komplot	k1gInSc6	komplot
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
nabyl	nabýt	k5eAaPmAgInS	nabýt
předchozí	předchozí	k2eAgFnPc4d1	předchozí
noci	noc	k1gFnPc4	noc
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
rozrušeným	rozrušený	k2eAgNnSc7d1	rozrušené
Serviliem	Servilium	k1gNnSc7	Servilium
Cascou	Casca	k1gMnSc7	Casca
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgInS	obávat
nejhoršího	zlý	k2eAgMnSc2d3	nejhorší
a	a	k8xC	a
chystal	chystat	k5eAaImAgMnS	chystat
se	se	k3xPyFc4	se
Caesara	Caesar	k1gMnSc2	Caesar
zadržet	zadržet	k5eAaPmF	zadržet
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
před	před	k7c7	před
Pompeiovým	Pompeiový	k2eAgNnSc7d1	Pompeiový
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
spiklenců	spiklenec	k1gMnPc2	spiklenec
zde	zde	k6eAd1	zde
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
pod	pod	k7c7	pod
jakousi	jakýsi	k3yIgFnSc7	jakýsi
záminkou	záminka	k1gFnSc7	záminka
zapředl	zapříst	k5eAaPmAgMnS	zapříst
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátu	senát	k1gInSc6	senát
se	se	k3xPyFc4	se
Lucius	Lucius	k1gMnSc1	Lucius
Tillius	Tillius	k1gMnSc1	Tillius
Cimber	Cimber	k1gMnSc1	Cimber
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
Caesara	Caesar	k1gMnSc4	Caesar
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k8xC	ale
diktátor	diktátor	k1gMnSc1	diktátor
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Cimber	Cimber	k1gInSc1	Cimber
začal	začít	k5eAaPmAgInS	začít
strhávat	strhávat	k5eAaImF	strhávat
z	z	k7c2	z
Caesara	Caesar	k1gMnSc2	Caesar
jeho	jeho	k3xOp3gNnSc1	jeho
togu	togu	k5eAaPmIp1nS	togu
<g/>
.	.	kIx.	.
</s>
<s>
Spiklenci	spiklenec	k1gMnPc1	spiklenec
se	se	k3xPyFc4	se
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
smluveném	smluvený	k2eAgNnSc6d1	smluvené
znamení	znamení	k1gNnSc6	znamení
shlukli	shluknout	k5eAaPmAgMnP	shluknout
kolem	kolem	k7c2	kolem
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
ho	on	k3xPp3gMnSc4	on
Casca	Casca	k1gMnSc1	Casca
říznul	říznout	k5eAaPmAgMnS	říznout
svojí	svůj	k3xOyFgFnSc7	svůj
dýkou	dýka	k1gFnSc7	dýka
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
Cascovi	Casec	k1gMnSc3	Casec
a	a	k8xC	a
obořil	obořit	k5eAaPmAgInS	obořit
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Praničemný	praničemný	k2eAgMnSc1d1	praničemný
Casco	Casco	k1gMnSc1	Casco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
to	ten	k3xDgNnSc4	ten
děláš	dělat	k5eAaImIp2nS	dělat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Vystrašený	vystrašený	k2eAgMnSc1d1	vystrašený
Casca	Casca	k1gMnSc1	Casca
zvolal	zvolat	k5eAaPmAgMnS	zvolat
řecky	řecky	k6eAd1	řecky
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
druhům	druh	k1gInPc3	druh
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
spiklenci	spiklenec	k1gMnPc1	spiklenec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Bruta	Brut	k1gInSc2	Brut
<g/>
,	,	kIx,	,
vrhli	vrhnout	k5eAaImAgMnP	vrhnout
s	s	k7c7	s
obnaženými	obnažený	k2eAgInPc7d1	obnažený
noži	nůž	k1gInPc7	nůž
na	na	k7c4	na
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sevřen	sevřen	k2eAgInSc1d1	sevřen
nepřáteli	nepřítel	k1gMnPc7	nepřítel
a	a	k8xC	a
oslepen	oslepit	k5eAaPmNgMnS	oslepit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
krví	krev	k1gFnSc7	krev
zakopl	zakopnout	k5eAaPmAgMnS	zakopnout
a	a	k8xC	a
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Spiklenci	spiklenec	k1gMnPc1	spiklenec
mu	on	k3xPp3gMnSc3	on
zasadili	zasadit	k5eAaPmAgMnP	zasadit
údajně	údajně	k6eAd1	údajně
třiadvacet	třiadvacet	k4xCc4	třiadvacet
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
Caesar	Caesar	k1gMnSc1	Caesar
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Caesarova	Caesarův	k2eAgNnPc1d1	Caesarovo
poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
nejsou	být	k5eNaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
zcela	zcela	k6eAd1	zcela
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
badatelů	badatel	k1gMnPc2	badatel
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Shakespearově	Shakespearův	k2eAgNnSc6d1	Shakespearovo
dramatu	drama	k1gNnSc6	drama
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
znějí	znět	k5eAaImIp3nP	znět
jeho	jeho	k3xOp3gNnPc1	jeho
poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
Et	Et	k1gFnSc2	Et
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
Brute	Brut	k1gMnSc5	Brut
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
I	i	k9	i
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
Brute	Brut	k1gMnSc5	Brut
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
Shakespearovu	Shakespearův	k2eAgFnSc4d1	Shakespearova
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
invenci	invence	k1gFnSc4	invence
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Suetonia	Suetonium	k1gNnSc2	Suetonium
pravil	pravit	k5eAaBmAgMnS	pravit
řecky	řecky	k6eAd1	řecky
Kai	Kai	k1gMnSc1	Kai
sí	sí	k?	sí
<g/>
,	,	kIx,	,
teknón	teknón	k1gInSc1	teknón
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
I	i	k9	i
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
můj	můj	k1gMnSc5	můj
synu	syn	k1gMnSc5	syn
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Caesar	Caesar	k1gMnSc1	Caesar
neřekl	říct	k5eNaPmAgMnS	říct
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
když	když	k8xS	když
spatřil	spatřit	k5eAaPmAgMnS	spatřit
mezi	mezi	k7c7	mezi
spiklenci	spiklenec	k1gMnPc7	spiklenec
Bruta	Bruto	k1gNnSc2	Bruto
<g/>
,	,	kIx,	,
přehodil	přehodit	k5eAaPmAgInS	přehodit
si	se	k3xPyFc3	se
tógu	tóga	k1gFnSc4	tóga
přes	přes	k7c4	přes
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
činu	čin	k1gInSc2	čin
se	se	k3xPyFc4	se
Brutus	Brutus	k1gMnSc1	Brutus
pokusil	pokusit	k5eAaPmAgMnS	pokusit
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyděšení	vyděšený	k2eAgMnPc1d1	vyděšený
senátoři	senátor	k1gMnPc1	senátor
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
rozutekli	rozutéct	k5eAaPmAgMnP	rozutéct
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
zakrvácené	zakrvácený	k2eAgNnSc1d1	zakrvácené
tělo	tělo	k1gNnSc1	tělo
leželo	ležet	k5eAaImAgNnS	ležet
u	u	k7c2	u
podstavce	podstavec	k1gInSc2	podstavec
Pompeiovy	Pompeiův	k2eAgFnSc2d1	Pompeiova
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Spiklenci	spiklenec	k1gMnPc1	spiklenec
volající	volající	k2eAgMnSc1d1	volající
<g/>
,	,	kIx,	,
že	že	k8xS	že
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
vyběhli	vyběhnout	k5eAaPmAgMnP	vyběhnout
na	na	k7c4	na
fórum	fórum	k1gNnSc4	fórum
a	a	k8xC	a
ukazovali	ukazovat	k5eAaImAgMnP	ukazovat
svoje	svůj	k3xOyFgFnPc4	svůj
zkrvavené	zkrvavený	k2eAgFnPc4d1	zkrvavená
ruce	ruka	k1gFnPc4	ruka
ohromenému	ohromený	k2eAgInSc3d1	ohromený
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Antonius	Antonius	k1gMnSc1	Antonius
<g/>
,	,	kIx,	,
Lepidus	Lepidus	k1gMnSc1	Lepidus
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
senátoři	senátor	k1gMnPc1	senátor
se	se	k3xPyFc4	se
v	v	k7c6	v
panice	panika	k1gFnSc6	panika
rozprchli	rozprchnout	k5eAaPmAgMnP	rozprchnout
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
triumfující	triumfující	k2eAgMnPc1d1	triumfující
"	"	kIx"	"
<g/>
osvoboditelé	osvoboditel	k1gMnPc1	osvoboditel
<g/>
"	"	kIx"	"
neměli	mít	k5eNaImAgMnP	mít
žádný	žádný	k3yNgInSc4	žádný
plán	plán	k1gInSc4	plán
jak	jak	k6eAd1	jak
postupovat	postupovat	k5eAaImF	postupovat
po	po	k7c6	po
Caesarově	Caesarův	k2eAgFnSc6d1	Caesarova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Znejistělí	znejistělý	k2eAgMnPc1d1	znejistělý
nedostatkem	nedostatek	k1gInSc7	nedostatek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
podpory	podpora	k1gFnSc2	podpora
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
ve	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
Jova	Jova	k1gMnSc1	Jova
Nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
a	a	k8xC	a
Největšího	veliký	k2eAgMnSc2d3	veliký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyčkávali	vyčkávat	k5eAaImAgMnP	vyčkávat
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
očekávané	očekávaný	k2eAgFnSc2d1	očekávaná
svobody	svoboda	k1gFnSc2	svoboda
se	se	k3xPyFc4	se
Římanům	Říman	k1gMnPc3	Říman
dostalo	dostat	k5eAaPmAgNnS	dostat
dalších	další	k2eAgNnPc2d1	další
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brutovo	Brutův	k2eAgNnSc4d1	Brutovo
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
neodstranit	odstranit	k5eNaPmF	odstranit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Caesarem	Caesar	k1gMnSc7	Caesar
také	také	k9	také
Marca	Marcum	k1gNnSc2	Marcum
Antonia	Antonio	k1gMnSc2	Antonio
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
jako	jako	k8xC	jako
osudový	osudový	k2eAgInSc1d1	osudový
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
podrobil	podrobit	k5eAaPmAgMnS	podrobit
kritice	kritika	k1gFnSc3	kritika
i	i	k8xC	i
Cicero	cicero	k1gNnSc4	cicero
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
byl	být	k5eAaImAgMnS	být
nyní	nyní	k6eAd1	nyní
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
konzul	konzul	k1gMnSc1	konzul
formálně	formálně	k6eAd1	formálně
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
získal	získat	k5eAaPmAgInS	získat
podporu	podpora	k1gFnSc4	podpora
Lepida	Lepid	k1gMnSc2	Lepid
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
jako	jako	k9	jako
veliteli	velitel	k1gMnPc7	velitel
jízdy	jízda	k1gFnSc2	jízda
podléhala	podléhat	k5eAaImAgNnP	podléhat
Caesarova	Caesarův	k2eAgNnPc1d1	Caesarovo
vojska	vojsko	k1gNnPc1	vojsko
mimo	mimo	k7c4	mimo
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc1	slunce
si	se	k3xPyFc3	se
od	od	k7c2	od
Calpurnie	Calpurnie	k1gFnSc2	Calpurnie
vymohl	vymoct	k5eAaPmAgMnS	vymoct
Caesarovy	Caesarův	k2eAgFnPc4d1	Caesarova
písemnosti	písemnost	k1gFnPc4	písemnost
a	a	k8xC	a
pokladnici	pokladnice	k1gFnSc4	pokladnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
takto	takto	k6eAd1	takto
zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
legie	legie	k1gFnPc4	legie
a	a	k8xC	a
finance	finance	k1gFnPc4	finance
<g/>
,	,	kIx,	,
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
zasedání	zasedání	k1gNnSc2	zasedání
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnPc2	jeho
jednání	jednání	k1gNnPc2	jednání
se	se	k3xPyFc4	se
domluvil	domluvit	k5eAaPmAgInS	domluvit
s	s	k7c7	s
Caesarovými	Caesarův	k2eAgMnPc7d1	Caesarův
vrahy	vrah	k1gMnPc7	vrah
na	na	k7c6	na
kompromisu	kompromis	k1gInSc6	kompromis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
amnestii	amnestie	k1gFnSc4	amnestie
zavázali	zavázat	k5eAaPmAgMnP	zavázat
respektovat	respektovat	k5eAaImF	respektovat
Caesarovy	Caesarův	k2eAgInPc4d1	Caesarův
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Piso	Pisa	k1gFnSc5	Pisa
závěť	závěť	k1gFnSc1	závěť
svého	svůj	k3xOyFgMnSc2	svůj
zetě	zeť	k1gMnSc2	zeť
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Caesar	Caesar	k1gMnSc1	Caesar
odkázal	odkázat	k5eAaPmAgMnS	odkázat
své	svůj	k3xOyFgFnPc4	svůj
zahrady	zahrada	k1gFnPc4	zahrada
římským	římský	k2eAgMnPc3d1	římský
občanům	občan	k1gMnPc3	občan
a	a	k8xC	a
každému	každý	k3xTgMnSc3	každý
obyvateli	obyvatel	k1gMnSc3	obyvatel
města	město	k1gNnSc2	město
zanechal	zanechat	k5eAaPmAgMnS	zanechat
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgMnPc2	svůj
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
na	na	k7c6	na
Foru	forum	k1gNnSc6	forum
Romanu	Romana	k1gFnSc4	Romana
konal	konat	k5eAaImAgInS	konat
Caesarův	Caesarův	k2eAgInSc1d1	Caesarův
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
pronesl	pronést	k5eAaPmAgMnS	pronést
Antonius	Antonius	k1gMnSc1	Antonius
krátkou	krátký	k2eAgFnSc4d1	krátká
smuteční	smuteční	k2eAgFnSc4d1	smuteční
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
vzbouřil	vzbouřit	k5eAaPmAgInS	vzbouřit
přítomný	přítomný	k2eAgInSc1d1	přítomný
lid	lid	k1gInSc1	lid
roznícený	roznícený	k2eAgInSc1d1	roznícený
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
zkrvavenou	zkrvavený	k2eAgFnSc4d1	zkrvavená
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
tógu	tóga	k1gFnSc4	tóga
<g/>
.	.	kIx.	.
</s>
<s>
Leckteří	leckterý	k3yIgMnPc1	leckterý
vrazi	vrah	k1gMnPc1	vrah
padli	padnout	k5eAaImAgMnP	padnout
té	ten	k3xDgFnSc6	ten
noci	noc	k1gFnSc6	noc
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
rozvášněnému	rozvášněný	k2eAgInSc3d1	rozvášněný
davu	dav	k1gInSc3	dav
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
unikli	uniknout	k5eAaPmAgMnP	uniknout
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
chtěli	chtít	k5eAaImAgMnP	chtít
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
tělo	tělo	k1gNnSc1	tělo
spáleno	spálit	k5eAaPmNgNnS	spálit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
prasynovec	prasynovec	k1gMnSc1	prasynovec
Gaius	Gaius	k1gMnSc1	Gaius
Octavius	Octavius	k1gMnSc1	Octavius
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
Caesar	Caesar	k1gMnSc1	Caesar
ustavil	ustavit	k5eAaPmAgMnS	ustavit
svým	svůj	k3xOyFgMnSc7	svůj
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dědicem	dědic	k1gMnSc7	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnohem	mnohem	k6eAd1	mnohem
závažnější	závažný	k2eAgFnSc1d2	závažnější
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Caesar	Caesar	k1gMnSc1	Caesar
Octavia	octavia	k1gFnSc1	octavia
také	také	k6eAd1	také
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
Octavius	Octavius	k1gMnSc1	Octavius
přijal	přijmout	k5eAaPmAgMnS	přijmout
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
oprávněný	oprávněný	k2eAgMnSc1d1	oprávněný
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Antonius	Antonius	k1gMnSc1	Antonius
zřejmě	zřejmě	k6eAd1	zřejmě
sám	sám	k3xTgMnSc1	sám
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
tohoto	tento	k3xDgMnSc4	tento
osmnáctiletého	osmnáctiletý	k2eAgMnSc4d1	osmnáctiletý
chlapce	chlapec	k1gMnSc4	chlapec
si	se	k3xPyFc3	se
vzápětí	vzápětí	k6eAd1	vzápětí
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
jeho	jeho	k3xOp3gNnSc4	jeho
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
po	po	k7c6	po
Caesarově	Caesarův	k2eAgFnSc6d1	Caesarova
smrti	smrt	k1gFnSc6	smrt
přiřkli	přiřknout	k5eAaPmAgMnP	přiřknout
senátoři	senátor	k1gMnPc1	senátor
Brutovi	Brutův	k2eAgMnPc1d1	Brutův
a	a	k8xC	a
Cassiovi	Cassiův	k2eAgMnPc1d1	Cassiův
správcovství	správcovství	k1gNnPc1	správcovství
velkých	velký	k2eAgFnPc2d1	velká
východních	východní	k2eAgFnPc2d1	východní
provincií	provincie	k1gFnPc2	provincie
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
oplývaly	oplývat	k5eAaImAgFnP	oplývat
četnými	četný	k2eAgFnPc7d1	četná
legiemi	legie	k1gFnPc7	legie
umístěnými	umístěný	k2eAgFnPc7d1	umístěná
zde	zde	k6eAd1	zde
kvůli	kvůli	k7c3	kvůli
chystanému	chystaný	k2eAgNnSc3d1	chystané
tažení	tažení	k1gNnSc3	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Antonius	Antonius	k1gInSc1	Antonius
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
pokusil	pokusit	k5eAaPmAgMnS	pokusit
intrikami	intrika	k1gFnPc7	intrika
zbavit	zbavit	k5eAaPmF	zbavit
těchto	tento	k3xDgFnPc2	tento
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
vrahů	vrah	k1gMnPc2	vrah
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
stala	stát	k5eAaPmAgFnS	stát
natolik	natolik	k6eAd1	natolik
neudržitelnou	udržitelný	k2eNgFnSc4d1	neudržitelná
<g/>
,	,	kIx,	,
že	že	k8xS	že
raději	rád	k6eAd2	rád
opustili	opustit	k5eAaPmAgMnP	opustit
město	město	k1gNnSc4	město
a	a	k8xC	a
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
se	se	k3xPyFc4	se
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
43	[number]	k4	43
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
senát	senát	k1gInSc4	senát
Antonia	Antonio	k1gMnSc2	Antonio
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
Cicerona	Cicero	k1gMnSc2	Cicero
za	za	k7c2	za
nepřítele	nepřítel	k1gMnSc2	nepřítel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Antonius	Antonius	k1gMnSc1	Antonius
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Předalpské	předalpský	k2eAgFnSc2d1	Předalpská
Galie	Galie	k1gFnSc2	Galie
proti	proti	k7c3	proti
tamějšímu	tamější	k2eAgMnSc3d1	tamější
místodržiteli	místodržitel	k1gMnSc3	místodržitel
Decimu	decima	k1gFnSc4	decima
Brutovi	Brut	k1gMnSc3	Brut
<g/>
.	.	kIx.	.
</s>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
několik	několik	k4yIc1	několik
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
senátory	senátor	k1gMnPc7	senátor
vedenými	vedený	k2eAgMnPc7d1	vedený
Ciceronem	Cicero	k1gMnSc7	Cicero
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
s	s	k7c7	s
Antoniem	Antonio	k1gMnSc7	Antonio
srazil	srazit	k5eAaPmAgMnS	srazit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Mutiny	Mutina	k1gFnSc2	Mutina
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Modena	Modena	k1gFnSc1	Modena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
se	se	k3xPyFc4	se
však	však	k9	však
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
senátu	senát	k1gInSc3	senát
<g/>
,	,	kIx,	,
vpadl	vpadnout	k5eAaPmAgInS	vpadnout
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
vojensky	vojensky	k6eAd1	vojensky
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Antoniův	Antoniův	k2eAgInSc4d1	Antoniův
kompromis	kompromis	k1gInSc4	kompromis
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
nelegální	legální	k2eNgMnPc4d1	nelegální
a	a	k8xC	a
Caesarovy	Caesarův	k2eAgMnPc4d1	Caesarův
vrahy	vrah	k1gMnPc4	vrah
postavil	postavit	k5eAaPmAgMnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
neočekávaně	očekávaně	k6eNd1	očekávaně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
uzavřít	uzavřít	k5eAaPmF	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Antoniem	Antonio	k1gMnSc7	Antonio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Hispánií	Hispánie	k1gFnSc7	Hispánie
a	a	k8xC	a
Galií	Galie	k1gFnSc7	Galie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
43	[number]	k4	43
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Octavianus	Octavianus	k1gInSc1	Octavianus
<g/>
,	,	kIx,	,
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
a	a	k8xC	a
Lepidus	Lepidus	k1gMnSc1	Lepidus
setkali	setkat	k5eAaPmAgMnP	setkat
u	u	k7c2	u
Bononie	Bononie	k1gFnSc2	Bononie
(	(	kIx(	(
<g/>
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
zde	zde	k6eAd1	zde
druhý	druhý	k4xOgInSc4	druhý
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
.	.	kIx.	.
</s>
<s>
Shodným	shodný	k2eAgInSc7d1	shodný
cílem	cíl	k1gInSc7	cíl
triumvirů	triumvir	k1gMnPc2	triumvir
byla	být	k5eAaImAgFnS	být
pomsta	pomsta	k1gFnSc1	pomsta
Caesara	Caesar	k1gMnSc2	Caesar
a	a	k8xC	a
potrestání	potrestání	k1gNnSc1	potrestání
jeho	jeho	k3xOp3gMnPc2	jeho
vrahů	vrah	k1gMnPc2	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zbavili	zbavit	k5eAaPmAgMnP	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
proskripční	proskripční	k2eAgFnPc4d1	proskripční
listiny	listina	k1gFnPc4	listina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Sully	Sulla	k1gFnSc2	Sulla
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
teror	teror	k1gInSc4	teror
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
ztratily	ztratit	k5eAaPmAgFnP	ztratit
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
stovky	stovka	k1gFnSc2	stovka
možná	možná	k9	možná
tisíce	tisíc	k4xCgInPc1	tisíc
předních	přední	k2eAgMnPc2d1	přední
republikánsky	republikánsky	k6eAd1	republikánsky
smýšlejících	smýšlející	k2eAgMnPc2d1	smýšlející
občanů	občan	k1gMnPc2	občan
včetně	včetně	k7c2	včetně
Cicerona	Cicero	k1gMnSc2	Cicero
<g/>
.	.	kIx.	.
</s>
<s>
Krutost	krutost	k1gFnSc1	krutost
triumvirů	triumvir	k1gMnPc2	triumvir
ostře	ostro	k6eAd1	ostro
kontrastovala	kontrastovat	k5eAaImAgFnS	kontrastovat
s	s	k7c7	s
Caesarovou	Caesarův	k2eAgFnSc7d1	Caesarova
vlastní	vlastní	k2eAgFnSc7d1	vlastní
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
byl	být	k5eAaImAgMnS	být
nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
senátem	senát	k1gInSc7	senát
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
boha	bůh	k1gMnSc4	bůh
jako	jako	k9	jako
Divus	Divus	k1gMnSc1	Divus
Iulius	Iulius	k1gMnSc1	Iulius
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
božský	božský	k2eAgMnSc1d1	božský
Julius	Julius	k1gMnSc1	Julius
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
42	[number]	k4	42
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Antonius	Antonius	k1gInSc1	Antonius
a	a	k8xC	a
Octavianus	Octavianus	k1gInSc1	Octavianus
doprovázeni	doprovázet	k5eAaImNgMnP	doprovázet
mocným	mocný	k2eAgInSc7d1	mocný
vojskem	vojsko	k1gNnSc7	vojsko
přeplavili	přeplavit	k5eAaPmAgMnP	přeplavit
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
utkali	utkat	k5eAaPmAgMnP	utkat
s	s	k7c7	s
početně	početně	k6eAd1	početně
slabší	slabý	k2eAgFnSc7d2	slabší
armádou	armáda	k1gFnSc7	armáda
Cassia	Cassium	k1gNnSc2	Cassium
a	a	k8xC	a
Bruta	Bruto	k1gNnSc2	Bruto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozhodujícímu	rozhodující	k2eAgInSc3d1	rozhodující
střetu	střet	k1gInSc3	střet
došlo	dojít	k5eAaPmAgNnS	dojít
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
opět	opět	k6eAd1	opět
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
42	[number]	k4	42
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Filipp	Filippy	k1gInPc2	Filippy
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
boji	boj	k1gInSc6	boj
měli	mít	k5eAaImAgMnP	mít
mírně	mírně	k6eAd1	mírně
navrch	navrch	k6eAd1	navrch
republikáni	republikán	k1gMnPc1	republikán
<g/>
,	,	kIx,	,
Cassius	Cassius	k1gMnSc1	Cassius
špatně	špatně	k6eAd1	špatně
vyhodnotil	vyhodnotit	k5eAaPmAgMnS	vyhodnotit
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Brutus	Brutus	k1gMnSc1	Brutus
poté	poté	k6eAd1	poté
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
spojeným	spojený	k2eAgInPc3d1	spojený
silám	síla	k1gFnPc3	síla
triumvirů	triumvir	k1gMnPc2	triumvir
a	a	k8xC	a
následoval	následovat	k5eAaImAgMnS	následovat
Cassiova	Cassiův	k2eAgInSc2d1	Cassiův
příkladu	příklad	k1gInSc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzaly	vzít	k5eAaPmAgFnP	vzít
zasvé	zasvé	k6eAd1	zasvé
poslední	poslední	k2eAgFnPc1d1	poslední
naděje	naděje	k1gFnPc1	naděje
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
přeživší	přeživší	k2eAgMnPc1d1	přeživší
vrazi	vrah	k1gMnPc1	vrah
brzy	brzy	k6eAd1	brzy
dopadeni	dopadnout	k5eAaPmNgMnP	dopadnout
a	a	k8xC	a
popraveni	popravit	k5eAaPmNgMnP	popravit
triumviry	triumvir	k1gMnPc7	triumvir
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
římským	římský	k2eAgInSc7d1	římský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
společného	společný	k2eAgInSc2d1	společný
cíle	cíl	k1gInSc2	cíl
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
rychle	rychle	k6eAd1	rychle
znesvářili	znesvářit	k5eAaPmAgMnP	znesvářit
<g/>
.	.	kIx.	.
</s>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
nejprve	nejprve	k6eAd1	nejprve
eliminoval	eliminovat	k5eAaBmAgMnS	eliminovat
Lepida	Lepid	k1gMnSc4	Lepid
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
odňal	odnít	k5eAaPmAgInS	odnít
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
31	[number]	k4	31
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Actia	Actius	k1gMnSc2	Actius
Marca	Marcus	k1gMnSc2	Marcus
Antonia	Antonio	k1gMnSc2	Antonio
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Caesarovou	Caesarová	k1gFnSc7	Caesarová
milenkou	milenka	k1gFnSc7	milenka
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Octavianus	Octavianus	k1gMnSc1	Octavianus
samovlády	samovláda	k1gFnSc2	samovláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Caesara	Caesar	k1gMnSc2	Caesar
se	se	k3xPyFc4	se
Octavianus	Octavianus	k1gMnSc1	Octavianus
obezřetně	obezřetně	k6eAd1	obezřetně
vyvaroval	vyvarovat	k5eAaPmAgMnS	vyvarovat
byť	byť	k8xS	byť
i	i	k9	i
jen	jen	k9	jen
zdání	zdání	k1gNnSc1	zdání
vytvoření	vytvoření	k1gNnSc2	vytvoření
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
obnovení	obnovení	k1gNnSc4	obnovení
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
a	a	k8xC	a
skromně	skromně	k6eAd1	skromně
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc7	první
občanem	občan	k1gMnSc7	občan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
princeps	princeps	k1gInSc1	princeps
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
pevně	pevně	k6eAd1	pevně
třímal	třímat	k5eAaImAgInS	třímat
veškerou	veškerý	k3xTgFnSc4	veškerý
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
nezměrnou	nezměrný	k2eAgFnSc4d1	nezměrná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc4	senát
mu	on	k3xPp3gMnSc3	on
dovolil	dovolit	k5eAaPmAgMnS	dovolit
vykonávat	vykonávat	k5eAaImF	vykonávat
pravomoci	pravomoc	k1gFnPc4	pravomoc
různých	různý	k2eAgInPc2d1	různý
magistrátů	magistrát	k1gInPc2	magistrát
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
tyto	tento	k3xDgInPc4	tento
úřady	úřad	k1gInPc4	úřad
nezastával	zastávat	k5eNaImAgMnS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přijal	přijmout	k5eAaPmAgMnS	přijmout
Octavianus	Octavianus	k1gMnSc1	Octavianus
čestné	čestný	k2eAgNnSc4d1	čestné
jméno	jméno	k1gNnSc4	jméno
Augustus	Augustus	k1gInSc4	Augustus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
udělili	udělit	k5eAaPmAgMnP	udělit
senátoři	senátor	k1gMnPc1	senátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
skutečným	skutečný	k2eAgMnSc7d1	skutečný
zakladatelem	zakladatel	k1gMnSc7	zakladatel
římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
pohřbil	pohřbít	k5eAaPmAgInS	pohřbít
římskou	římský	k2eAgFnSc4d1	římská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
zlatý	zlatý	k2eAgInSc1d1	zlatý
nebo	nebo	k8xC	nebo
augustovský	augustovský	k2eAgInSc1d1	augustovský
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
zažila	zažít	k5eAaPmAgFnS	zažít
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
skutečný	skutečný	k2eAgInSc1d1	skutečný
rozkvět	rozkvět	k1gInSc1	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
římských	římský	k2eAgMnPc2d1	římský
řečníků	řečník	k1gMnPc2	řečník
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
–	–	k?	–
dokonce	dokonce	k9	dokonce
i	i	k9	i
Cicero	Cicero	k1gMnSc1	Cicero
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
pochvalně	pochvalně	k6eAd1	pochvalně
o	o	k7c6	o
Caesarových	Caesarových	k2eAgFnPc6d1	Caesarových
rétorských	rétorský	k2eAgFnPc6d1	rétorská
schopnostech	schopnost	k1gFnPc6	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejslavnější	slavný	k2eAgNnPc1d3	nejslavnější
díla	dílo	k1gNnPc1	dílo
náleží	náležet	k5eAaImIp3nP	náležet
smuteční	smuteční	k2eAgFnSc4d1	smuteční
řeč	řeč	k1gFnSc4	řeč
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
tetu	teta	k1gFnSc4	teta
Julii	Julie	k1gFnSc4	Julie
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
Anticato	Anticat	k2eAgNnSc1d1	Anticat
<g/>
,	,	kIx,	,
polemika	polemika	k1gFnSc1	polemika
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
očernění	očernění	k1gNnSc3	očernění
Catonovy	Catonův	k2eAgFnSc2d1	Catonova
reputace	reputace	k1gFnSc2	reputace
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
reagoval	reagovat	k5eAaBmAgInS	reagovat
na	na	k7c4	na
Ciceronův	Ciceronův	k2eAgInSc4d1	Ciceronův
oslavný	oslavný	k2eAgInSc4d1	oslavný
spis	spis	k1gInSc4	spis
Cato	Cato	k6eAd1	Cato
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
napsal	napsat	k5eAaPmAgMnS	napsat
báseň	báseň	k1gFnSc4	báseň
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
nezachovaly	zachovat	k5eNaPmAgInP	zachovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
opisování	opisování	k1gNnSc6	opisování
antických	antický	k2eAgInPc2d1	antický
rukopisů	rukopis	k1gInPc2	rukopis
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nedůležité	důležitý	k2eNgNnSc4d1	nedůležité
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
ztracená	ztracený	k2eAgNnPc1d1	ztracené
jiná	jiný	k2eAgNnPc1d1	jiné
Caesarova	Caesarův	k2eAgNnPc1d1	Caesarovo
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
tragédie	tragédie	k1gFnSc1	tragédie
Oidipus	Oidipus	k1gMnSc1	Oidipus
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
rčení	rčení	k1gNnSc1	rčení
a	a	k8xC	a
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
byl	být	k5eAaImAgInS	být
Héraklés	Héraklés	k1gInSc1	Héraklés
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
tito	tento	k3xDgMnPc1	tento
mýtičtí	mýtický	k2eAgMnPc1d1	mýtický
hrdinové	hrdina	k1gMnPc1	hrdina
trpěli	trpět	k5eAaImAgMnP	trpět
za	za	k7c4	za
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vykonali	vykonat	k5eAaPmAgMnP	vykonat
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
Caesarův	Caesarův	k2eAgInSc4d1	Caesarův
skeptický	skeptický	k2eAgInSc4d1	skeptický
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
velekněz	velekněz	k1gMnSc1	velekněz
římského	římský	k2eAgInSc2d1	římský
státního	státní	k2eAgInSc2d1	státní
kultu	kult	k1gInSc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc4d1	jediný
Caesarovy	Caesarův	k2eAgInPc4d1	Caesarův
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
fascinující	fascinující	k2eAgInPc1d1	fascinující
Zápisky	zápisek	k1gInPc1	zápisek
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgFnSc6d1	galská
(	(	kIx(	(
<g/>
Commentarii	Commentarie	k1gFnSc3	Commentarie
de	de	k?	de
bello	bello	k1gNnSc1	bello
Gallico	Gallico	k1gMnSc1	Gallico
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zápisky	zápiska	k1gFnSc2	zápiska
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
občanské	občanský	k2eAgFnSc6d1	občanská
(	(	kIx(	(
<g/>
Commentarii	Commentarie	k1gFnSc3	Commentarie
de	de	k?	de
bello	bello	k1gNnSc4	bello
civili	civít	k5eAaPmAgMnP	civít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tvořilo	tvořit	k5eAaImAgNnS	tvořit
podklad	podklad	k1gInSc4	podklad
pro	pro	k7c4	pro
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
boje	boj	k1gInSc2	boj
posílané	posílaný	k2eAgFnSc2d1	posílaná
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Caesar	Caesar	k1gMnSc1	Caesar
svá	svůj	k3xOyFgNnPc4	svůj
vojenská	vojenský	k2eAgNnPc4d1	vojenské
tažení	tažení	k1gNnPc4	tažení
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc4	osm
knih	kniha	k1gFnPc2	kniha
pojednávajících	pojednávající	k2eAgFnPc2d1	pojednávající
o	o	k7c6	o
letech	léto	k1gNnPc6	léto
58	[number]	k4	58
až	až	k9	až
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
autorem	autor	k1gMnSc7	autor
poslední	poslední	k2eAgFnSc2d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
Caesarův	Caesarův	k2eAgMnSc1d1	Caesarův
poddůstojník	poddůstojník	k1gMnSc1	poddůstojník
Aulus	Aulus	k1gMnSc1	Aulus
Hirtius	Hirtius	k1gMnSc1	Hirtius
<g/>
.	.	kIx.	.
</s>
<s>
Zápisky	zápiska	k1gFnPc1	zápiska
patří	patřit	k5eAaImIp3nP	patřit
dodnes	dodnes	k6eAd1	dodnes
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
srozumitelnou	srozumitelný	k2eAgFnSc4d1	srozumitelná
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
jen	jen	k9	jen
asi	asi	k9	asi
1300	[number]	k4	1300
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
k	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
užívané	užívaný	k2eAgFnSc3d1	užívaná
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
detailně	detailně	k6eAd1	detailně
popsal	popsat	k5eAaPmAgMnS	popsat
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
válečných	válečný	k2eAgFnPc2d1	válečná
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgMnPc6	jenž
jeho	jeho	k3xOp3gMnSc3	jeho
vojsko	vojsko	k1gNnSc1	vojsko
zdecimovalo	zdecimovat	k5eAaPmAgNnS	zdecimovat
galské	galský	k2eAgInPc4d1	galský
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
řadu	řada	k1gFnSc4	řada
postřehů	postřeh	k1gInPc2	postřeh
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
životních	životní	k2eAgInPc2d1	životní
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
Galů	Gal	k1gMnPc2	Gal
<g/>
,	,	kIx,	,
Germanů	German	k1gMnPc2	German
a	a	k8xC	a
Britanů	Britan	k1gInPc2	Britan
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovení	zhotovení	k1gNnSc1	zhotovení
a	a	k8xC	a
zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
Zápisků	zápisek	k1gInPc2	zápisek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
nebyly	být	k5eNaImAgInP	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
prokonzulátu	prokonzulát	k1gInSc2	prokonzulát
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
jako	jako	k8xC	jako
celistvé	celistvý	k2eAgNnSc4d1	celistvé
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
sloužilo	sloužit	k5eAaImAgNnS	sloužit
především	především	k9	především
k	k	k7c3	k
ospravedlnění	ospravedlnění	k1gNnSc3	ospravedlnění
Caesarových	Caesarových	k2eAgFnPc2d1	Caesarových
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
obhajobě	obhajoba	k1gFnSc3	obhajoba
jeho	jeho	k3xOp3gFnPc2	jeho
bezohledných	bezohledný	k2eAgFnPc2d1	bezohledná
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
silné	silný	k2eAgFnSc3d1	silná
kritice	kritika	k1gFnSc3	kritika
leckterých	leckterý	k3yIgMnPc2	leckterý
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
knihy	kniha	k1gFnPc1	kniha
o	o	k7c6	o
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
zachycující	zachycující	k2eAgFnSc2d1	zachycující
události	událost	k1gFnSc2	událost
z	z	k7c2	z
let	léto	k1gNnPc2	léto
49	[number]	k4	49
a	a	k8xC	a
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jsou	být	k5eAaImIp3nP	být
nedokončené	dokončený	k2eNgFnPc1d1	nedokončená
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
jako	jako	k9	jako
nedobrovolný	dobrovolný	k2eNgMnSc1d1	nedobrovolný
bojovník	bojovník	k1gMnSc1	bojovník
v	v	k7c6	v
nevyhnutelném	vyhnutelný	k2eNgInSc6d1	nevyhnutelný
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tažení	tažení	k1gNnSc6	tažení
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
47	[number]	k4	47
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyhotovil	vyhotovit	k5eAaPmAgInS	vyhotovit
Hirtius	Hirtius	k1gInSc1	Hirtius
spis	spis	k1gInSc1	spis
Bellum	Bellum	k1gInSc1	Bellum
Alexandrinum	Alexandrinum	k1gInSc1	Alexandrinum
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
Caesarovi	Caesar	k1gMnSc3	Caesar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
napsal	napsat	k5eAaBmAgMnS	napsat
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
:	:	kIx,	:
Bellum	Bellum	k1gInSc1	Bellum
Africanum	Africanum	k1gNnSc1	Africanum
a	a	k8xC	a
Bellum	Bellum	k1gNnSc1	Bellum
Hispaniense	Hispaniense	k1gFnSc2	Hispaniense
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
líčí	líčit	k5eAaImIp3nP	líčit
závěrečná	závěrečný	k2eAgNnPc1d1	závěrečné
střetnutí	střetnutí	k1gNnPc1	střetnutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
46	[number]	k4	46
a	a	k8xC	a
45	[number]	k4	45
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
historikové	historik	k1gMnPc1	historik
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
Caesara	Caesar	k1gMnSc2	Caesar
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejzdatnějších	zdatný	k2eAgMnPc2d3	nejzdatnější
a	a	k8xC	a
nejschopnějších	schopný	k2eAgMnPc2d3	nejschopnější
stratégů	stratég	k1gMnPc2	stratég
a	a	k8xC	a
taktiků	taktik	k1gMnPc2	taktik
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
vojenství	vojenství	k1gNnSc2	vojenství
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
bok	bok	k1gInSc4	bok
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
,	,	kIx,	,
Čingischán	Čingischán	k1gMnSc1	Čingischán
nebo	nebo	k8xC	nebo
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
porážky	porážka	k1gFnPc4	porážka
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gergovie	Gergovie	k1gFnSc2	Gergovie
během	během	k7c2	během
galských	galský	k2eAgFnPc2d1	galská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Dyrrhachia	Dyrrhachium	k1gNnSc2	Dyrrhachium
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
prokázal	prokázat	k5eAaPmAgMnS	prokázat
taktickou	taktický	k2eAgFnSc4d1	taktická
genialitu	genialita	k1gFnSc4	genialita
svými	svůj	k3xOyFgInPc7	svůj
obdivuhodnými	obdivuhodný	k2eAgInPc7d1	obdivuhodný
výkony	výkon	k1gInPc7	výkon
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Alesie	Alesie	k1gFnSc2	Alesie
<g/>
,	,	kIx,	,
obrácením	obrácení	k1gNnSc7	obrácení
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
Pompeiova	Pompeiův	k2eAgNnSc2d1	Pompeiovo
početně	početně	k6eAd1	početně
silnějšího	silný	k2eAgNnSc2d2	silnější
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
Farsálu	Farsál	k1gInSc2	Farsál
nebo	nebo	k8xC	nebo
úplným	úplný	k2eAgNnSc7d1	úplné
zničením	zničení	k1gNnSc7	zničení
Farnakova	Farnakův	k2eAgNnSc2d1	Farnakův
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
Zely	zet	k5eAaImAgFnP	zet
<g/>
.	.	kIx.	.
</s>
<s>
Caesarova	Caesarův	k2eAgNnPc1d1	Caesarovo
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
tažení	tažení	k1gNnPc1	tažení
v	v	k7c6	v
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
za	za	k7c2	za
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
lze	lze	k6eAd1	lze
přičíst	přičíst	k5eAaPmF	přičíst
jeho	jeho	k3xOp3gFnSc3	jeho
přísné	přísný	k2eAgFnSc3d1	přísná
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnSc3d1	tvrdá
kázni	kázeň	k1gFnSc3	kázeň
vyžadované	vyžadovaný	k2eAgFnSc2d1	vyžadovaná
jím	jíst	k5eAaImIp1nS	jíst
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
legionářích	legionář	k1gMnPc6	legionář
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
obdiv	obdiv	k1gInSc1	obdiv
<g/>
,	,	kIx,	,
oddanost	oddanost	k1gFnSc1	oddanost
a	a	k8xC	a
úcta	úcta	k1gFnSc1	úcta
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
pověstná	pověstný	k2eAgFnSc1d1	pověstná
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svým	svůj	k3xOyFgNnSc7	svůj
spravedlivým	spravedlivý	k2eAgNnSc7d1	spravedlivé
zacházením	zacházení	k1gNnSc7	zacházení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neváhal	váhat	k5eNaImAgMnS	váhat
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
vojákům	voják	k1gMnPc3	voják
zkušeným	zkušený	k2eAgNnSc7d1	zkušené
před	před	k7c7	před
urozenými	urozený	k2eAgMnPc7d1	urozený
<g/>
.	.	kIx.	.
</s>
<s>
Vskutku	vskutku	k9	vskutku
prvotřídní	prvotřídní	k2eAgFnSc4d1	prvotřídní
Caesarovu	Caesarův	k2eAgFnSc4d1	Caesarova
pěchotu	pěchota	k1gFnSc4	pěchota
a	a	k8xC	a
jízdu	jízda	k1gFnSc4	jízda
doplňovala	doplňovat	k5eAaImAgFnS	doplňovat
hrůzu	hrůza	k1gFnSc4	hrůza
budící	budící	k2eAgFnSc1d1	budící
těžká	těžký	k2eAgFnSc1d1	těžká
artilerie	artilerie	k1gFnSc1	artilerie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
činily	činit	k5eAaImAgInP	činit
jeho	jeho	k3xOp3gNnSc4	jeho
vojsko	vojsko	k1gNnSc4	vojsko
tak	tak	k9	tak
vysoce	vysoce	k6eAd1	vysoce
efektivním	efektivní	k2eAgFnPc3d1	efektivní
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
takřka	takřka	k6eAd1	takřka
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
kastrametační	kastrametační	k2eAgFnSc2d1	kastrametační
a	a	k8xC	a
ženistické	ženistický	k2eAgFnSc2d1	ženistický
schopnosti	schopnost	k1gFnSc2	schopnost
jeho	jeho	k3xOp3gMnPc2	jeho
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
legendární	legendární	k2eAgFnSc1d1	legendární
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
přesouvat	přesouvat	k5eAaImF	přesouvat
a	a	k8xC	a
manévrovat	manévrovat	k5eAaImF	manévrovat
<g/>
.	.	kIx.	.
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
pochodu	pochod	k1gInSc6	pochod
schopná	schopný	k2eAgFnSc1d1	schopná
urazit	urazit	k5eAaPmF	urazit
i	i	k9	i
čtyřicet	čtyřicet	k4xCc4	čtyřicet
mil	míle	k1gFnPc2	míle
denně	denně	k6eAd1	denně
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
rychlostí	rychlost	k1gFnSc7	rychlost
akcí	akce	k1gFnPc2	akce
ohromovala	ohromovat	k5eAaImAgFnS	ohromovat
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
čítalo	čítat	k5eAaImAgNnS	čítat
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
000	[number]	k4	000
legionářů	legionář	k1gMnPc2	legionář
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
jezdci	jezdec	k1gMnPc7	jezdec
a	a	k8xC	a
specializovanými	specializovaný	k2eAgFnPc7d1	specializovaná
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
ženisté	ženista	k1gMnPc1	ženista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zápiscích	zápisek	k1gInPc6	zápisek
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgFnSc2d1	galská
Caesar	Caesar	k1gMnSc1	Caesar
popsal	popsat	k5eAaPmAgMnS	popsat
obléhání	obléhání	k1gNnSc1	obléhání
jednoho	jeden	k4xCgNnSc2	jeden
galského	galský	k2eAgNnSc2d1	galské
oppida	oppidum	k1gNnSc2	oppidum
umístěného	umístěný	k2eAgNnSc2d1	umístěné
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc3d1	vysoká
a	a	k8xC	a
příkré	příkrý	k2eAgFnSc3d1	příkrá
náhorní	náhorní	k2eAgFnSc3d1	náhorní
plošině	plošina	k1gFnSc3	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
ženisté	ženista	k1gMnPc1	ženista
prorazili	prorazit	k5eAaPmAgMnP	prorazit
tunel	tunel	k1gInSc4	tunel
v	v	k7c6	v
ohromné	ohromný	k2eAgFnSc6d1	ohromná
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odklonili	odklonit	k5eAaPmAgMnP	odklonit
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zásoboval	zásobovat	k5eAaImAgMnS	zásobovat
galské	galský	k2eAgNnSc4d1	galské
město	město	k1gNnSc4	město
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
odříznuté	odříznutý	k2eAgNnSc1d1	odříznuté
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
zdroje	zdroj	k1gInSc2	zdroj
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
římského	římský	k2eAgMnSc2d1	římský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starší	k1gMnSc2	starší
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
"	"	kIx"	"
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
caedare	caedar	k1gMnSc5	caedar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vyříznout	vyříznout	k5eAaPmF	vyříznout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
caesus	caesus	k1gInSc1	caesus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
řez	řez	k1gInSc1	řez
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
Juliů	Julius	k1gMnPc2	Julius
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nesla	nést	k5eAaImAgFnS	nést
cognomen	cognomen	k2eAgInSc4d1	cognomen
Caesar	Caesar	k1gMnSc1	Caesar
měla	mít	k5eAaImAgFnS	mít
prý	prý	k9	prý
pocházet	pocházet	k5eAaImF	pocházet
od	od	k7c2	od
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
.	.	kIx.	.
</s>
<s>
Historia	Historium	k1gNnSc2	Historium
Augusta	August	k1gMnSc4	August
podává	podávat	k5eAaImIp3nS	podávat
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
alternativní	alternativní	k2eAgFnPc4d1	alternativní
možnosti	možnost	k1gFnPc4	možnost
vzniku	vznik	k1gInSc2	vznik
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
první	první	k4xOgMnSc1	první
Caesar	Caesar	k1gMnSc1	Caesar
buď	buď	k8xC	buď
zabil	zabít	k5eAaPmAgMnS	zabít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
během	během	k7c2	během
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
slona	slon	k1gMnSc2	slon
(	(	kIx(	(
<g/>
caesai	caesai	k6eAd1	caesai
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
oplýval	oplývat	k5eAaImAgInS	oplývat
bujnou	bujný	k2eAgFnSc7d1	bujná
kšticí	kštice	k1gFnSc7	kštice
(	(	kIx(	(
<g/>
caesaris	caesaris	k1gFnSc7	caesaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
měl	mít	k5eAaImAgInS	mít
jasně	jasně	k6eAd1	jasně
šedé	šedý	k2eAgNnSc4d1	šedé
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
oculis	oculis	k1gFnSc1	oculis
caesiis	caesiis	k1gFnSc2	caesiis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc3	první
variantě	varianta	k1gFnSc3	varianta
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
klonil	klonit	k5eAaImAgMnS	klonit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
nechal	nechat	k5eAaPmAgMnS	nechat
razit	razit	k5eAaImF	razit
mince	mince	k1gFnPc4	mince
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
slona	slon	k1gMnSc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
latině	latina	k1gFnSc6	latina
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
života	život	k1gInSc2	život
bylo	být	k5eAaImAgNnS	být
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
K	k	k7c3	k
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
spřežka	spřežka	k1gFnSc1	spřežka
"	"	kIx"	"
<g/>
ae	ae	k?	ae
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ai	ai	k?	ai
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nikoli	nikoli	k9	nikoli
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
é	é	k0	é
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
následující	následující	k2eAgFnSc1d1	následující
latinská	latinský	k2eAgFnSc1d1	Latinská
výslovnost	výslovnost	k1gFnSc1	výslovnost
slova	slovo	k1gNnSc2	slovo
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
:	:	kIx,	:
kaisar	kaisar	k1gMnSc1	kaisar
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
také	také	k9	také
kaizar	kaizar	k1gMnSc1	kaizar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
pozdní	pozdní	k2eAgFnSc2d1	pozdní
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
historických	historický	k2eAgInPc2d1	historický
spisů	spis	k1gInPc2	spis
vyhotovena	vyhotoven	k2eAgFnSc1d1	vyhotovena
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc4	jazyk
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
a	a	k8xC	a
urozených	urozený	k2eAgFnPc2d1	urozená
římských	římský	k2eAgFnPc2d1	římská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
psalo	psát	k5eAaImAgNnS	psát
Κ	Κ	k?	Κ
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
kaísar	kaísar	k1gInSc1	kaísar
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
výslovnosti	výslovnost	k1gFnPc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
klasické	klasický	k2eAgFnSc2d1	klasická
výslovnosti	výslovnost	k1gFnSc2	výslovnost
Caesarova	Caesarův	k2eAgNnSc2d1	Caesarovo
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
německý	německý	k2eAgInSc1d1	německý
titul	titul	k1gInSc1	titul
Kaiser	Kaisra	k1gFnPc2	Kaisra
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
kajzr	kajzr	k1gInSc1	kajzr
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
sízr	sízr	k1gInSc1	sízr
<g/>
]	]	kIx)	]
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
česká	český	k2eAgFnSc1d1	Česká
[	[	kIx(	[
<g/>
cézar	cézar	k1gMnSc1	cézar
<g/>
]	]	kIx)	]
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Caesarovo	Caesarův	k2eAgNnSc1d1	Caesarovo
jméno	jméno	k1gNnSc1	jméno
znělo	znět	k5eAaImAgNnS	znět
[	[	kIx(	[
<g/>
čésar	čésar	k1gInSc1	čésar
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Caesar	Caesar	k1gMnSc1	Caesar
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Augusta	August	k1gMnSc4	August
součástí	součást	k1gFnSc7	součást
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
titulatury	titulatura	k1gFnSc2	titulatura
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Hadriana	Hadriana	k1gFnSc1	Hadriana
náležel	náležet	k5eAaImAgInS	náležet
titul	titul	k1gInSc1	titul
caesar	caesar	k1gInSc4	caesar
císařovu	císařův	k2eAgMnSc3d1	císařův
designovanému	designovaný	k2eAgMnSc3d1	designovaný
nástupci	nástupce	k1gMnSc3	nástupce
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
tetrarchie	tetrarchie	k1gFnSc2	tetrarchie
níže	nízce	k6eAd2	nízce
postaveným	postavený	k2eAgMnPc3d1	postavený
spolucísařům	spolucísař	k1gMnPc3	spolucísař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepatrně	nepatrně	k6eAd1	nepatrně
obměněné	obměněný	k2eAgFnSc6d1	obměněná
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
Caesar	Caesar	k1gMnSc1	Caesar
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xC	jako
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vládce	vládce	k1gMnPc4	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
"	"	kIx"	"
<g/>
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
"	"	kIx"	"
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
"	"	kIx"	"
<g/>
car	car	k1gMnSc1	car
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
Caesarova	Caesarův	k2eAgNnSc2d1	Caesarovo
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Caesarově	Caesarův	k2eAgInSc6d1	Caesarův
životě	život	k1gInSc6	život
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
nejobšírněji	obšírně	k6eAd3	obšírně
Gaius	Gaius	k1gMnSc1	Gaius
Suetonius	Suetonius	k1gMnSc1	Suetonius
Tranquillus	Tranquillus	k1gMnSc1	Tranquillus
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Životopisy	životopis	k1gInPc1	životopis
dvanácti	dvanáct	k4xCc2	dvanáct
císařů	císař	k1gMnPc2	císař
(	(	kIx(	(
<g/>
De	De	k?	De
Vita	vit	k2eAgFnSc1d1	Vita
Caesarum	Caesarum	k?	Caesarum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
spisovatel	spisovatel	k1gMnSc1	spisovatel
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
císařské	císařský	k2eAgInPc4d1	císařský
archivy	archiv	k1gInPc4	archiv
za	za	k7c4	za
Hadriana	Hadrian	k1gMnSc4	Hadrian
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gInSc1	Suetonius
měl	mít	k5eAaImAgInS	mít
tudíž	tudíž	k8xC	tudíž
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
výtečnému	výtečný	k2eAgInSc3d1	výtečný
zdroji	zdroj	k1gInSc3	zdroj
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
ovšem	ovšem	k9	ovšem
zacházel	zacházet	k5eAaImAgMnS	zacházet
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
zdrženlivostí	zdrženlivost	k1gFnSc7	zdrženlivost
a	a	k8xC	a
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
mnohých	mnohý	k2eAgNnPc6d1	mnohé
tvrzeních	tvrzení	k1gNnPc6	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gMnSc1	Suetonius
se	se	k3xPyFc4	se
nesnažil	snažit	k5eNaImAgMnS	snažit
sepsat	sepsat	k5eAaPmF	sepsat
prosté	prostý	k2eAgInPc1d1	prostý
životopisy	životopis	k1gInPc1	životopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
usiloval	usilovat	k5eAaImAgInS	usilovat
i	i	k9	i
o	o	k7c4	o
vystižení	vystižení	k1gNnSc4	vystižení
určitých	určitý	k2eAgFnPc2d1	určitá
mravních	mravní	k2eAgFnPc2d1	mravní
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
určitý	určitý	k2eAgMnSc1d1	určitý
člověk	člověk	k1gMnSc1	člověk
disponoval	disponovat	k5eAaBmAgMnS	disponovat
absolutní	absolutní	k2eAgFnSc7d1	absolutní
svobodou	svoboda	k1gFnSc7	svoboda
a	a	k8xC	a
mocí	moc	k1gFnSc7	moc
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jedince	jedinec	k1gMnPc4	jedinec
vskutku	vskutku	k9	vskutku
pevného	pevný	k2eAgInSc2d1	pevný
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
udržet	udržet	k5eAaPmF	udržet
svoji	svůj	k3xOyFgFnSc4	svůj
čestnost	čestnost	k1gFnSc4	čestnost
a	a	k8xC	a
bezúhonnost	bezúhonnost	k1gFnSc4	bezúhonnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
biografie	biografie	k1gFnPc1	biografie
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
krutost	krutost	k1gFnSc1	krutost
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
sexuální	sexuální	k2eAgFnPc1d1	sexuální
úchylky	úchylka	k1gFnPc1	úchylka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
přirozeně	přirozeně	k6eAd1	přirozeně
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
antických	antický	k2eAgMnPc2d1	antický
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
portréty	portrét	k1gInPc1	portrét
císařů	císař	k1gMnPc2	císař
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
temné	temný	k2eAgInPc4d1	temný
a	a	k8xC	a
negativní	negativní	k2eAgInPc4d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
autor	autor	k1gMnSc1	autor
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
z	z	k7c2	z
Chairóneie	Chairóneie	k1gFnSc2	Chairóneie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
Suetonius	Suetonius	k1gInSc4	Suetonius
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
zabýval	zabývat	k5eAaImAgInS	zabývat
Caesarem	Caesar	k1gMnSc7	Caesar
a	a	k8xC	a
v	v	k7c6	v
Paralelních	paralelní	k2eAgFnPc6d1	paralelní
biografiích	biografie	k1gFnPc6	biografie
(	(	kIx(	(
<g/>
Bioi	Bioi	k1gNnPc1	Bioi
parallè	parallè	k?	parallè
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
porovnal	porovnat	k5eAaPmAgMnS	porovnat
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srovnání	srovnání	k1gNnSc2	srovnání
nejen	nejen	k6eAd1	nejen
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
měli	mít	k5eAaImAgMnP	mít
více	hodně	k6eAd2	hodně
společného	společný	k2eAgMnSc4d1	společný
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
připustit	připustit	k5eAaPmF	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Suetoniova	Suetoniův	k2eAgFnSc1d1	Suetoniova
i	i	k8xC	i
Plútarchova	Plútarchův	k2eAgFnSc1d1	Plútarchova
biografie	biografie	k1gFnSc1	biografie
představují	představovat	k5eAaImIp3nP	představovat
pouhý	pouhý	k2eAgInSc4d1	pouhý
náčrt	náčrt	k1gInSc4	náčrt
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nám	my	k3xPp1nPc3	my
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
zařadit	zařadit	k5eAaPmF	zařadit
informace	informace	k1gFnPc4	informace
ostatních	ostatní	k2eAgMnPc2d1	ostatní
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
důležité	důležitý	k2eAgNnSc4d1	důležité
postavení	postavení	k1gNnSc4	postavení
náleží	náležet	k5eAaImIp3nS	náležet
samotnému	samotný	k2eAgMnSc3d1	samotný
Caesarovi	Caesar	k1gMnSc3	Caesar
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nelze	lze	k6eNd1	lze
pominout	pominout	k5eAaPmF	pominout
Ciceronovu	Ciceronův	k2eAgFnSc4d1	Ciceronova
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
Listy	list	k1gInPc1	list
Atticovi	Atticovi	k1gRnPc1	Atticovi
(	(	kIx(	(
<g/>
Epistulae	Epistulae	k1gFnSc1	Epistulae
ad	ad	k7c4	ad
Atticum	Atticum	k1gInSc4	Atticum
<g/>
)	)	kIx)	)
nám	my	k3xPp1nPc3	my
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
pochopit	pochopit	k5eAaPmF	pochopit
politický	politický	k2eAgInSc4d1	politický
život	život	k1gInSc4	život
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tyto	tento	k3xDgInPc4	tento
dopisy	dopis	k1gInPc4	dopis
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
objeveny	objevit	k5eAaPmNgInP	objevit
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc4	císař
Nerona	Nero	k1gMnSc4	Nero
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohé	mnohý	k2eAgNnSc1d1	mnohé
nepatřičně	patřičně	k6eNd1	patřičně
se	se	k3xPyFc4	se
vyjadřující	vyjadřující	k2eAgMnSc1d1	vyjadřující
o	o	k7c6	o
Caesarovi	Caesar	k1gMnSc3	Caesar
nebyly	být	k5eNaImAgInP	být
vůbec	vůbec	k9	vůbec
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgFnSc3d1	stejná
selekci	selekce	k1gFnSc3	selekce
byly	být	k5eAaImAgFnP	být
podrobeny	podroben	k2eAgInPc1d1	podroben
také	také	k9	také
soubory	soubor	k1gInPc4	soubor
Ciceronových	Ciceronův	k2eAgInPc2d1	Ciceronův
Listů	list	k1gInPc2	list
přátelům	přítel	k1gMnPc3	přítel
(	(	kIx(	(
<g/>
Epistulae	Epistulae	k1gFnSc1	Epistulae
ad	ad	k7c4	ad
Familiares	Familiares	k1gInSc4	Familiares
<g/>
)	)	kIx)	)
a	a	k8xC	a
Listů	list	k1gInPc2	list
Brutovi	Brutovi	k1gRnPc1	Brutovi
(	(	kIx(	(
<g/>
Epistulae	Epistulae	k1gFnSc1	Epistulae
ad	ad	k7c4	ad
Brutum	Brutum	k1gNnSc4	Brutum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
údajů	údaj	k1gInPc2	údaj
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
projevech	projev	k1gInPc6	projev
Cicerona	Cicero	k1gMnSc2	Cicero
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
řečech	řeč	k1gFnPc6	řeč
za	za	k7c4	za
Marcella	Marcello	k1gNnPc4	Marcello
(	(	kIx(	(
<g/>
Pro	pro	k7c4	pro
Marcello	Marcello	k1gNnSc4	Marcello
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
Ligaria	Ligarium	k1gNnPc4	Ligarium
(	(	kIx(	(
<g/>
Pro	pro	k7c4	pro
Ligario	Ligario	k1gNnSc4	Ligario
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Filipikách	filipika	k1gFnPc6	filipika
(	(	kIx(	(
<g/>
Philippicae	Philippicae	k1gNnSc6	Philippicae
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Marcu	Marc	k1gMnSc3	Marc
Antoniovi	Antonio	k1gMnSc3	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
pramenem	pramen	k1gInSc7	pramen
o	o	k7c6	o
Caesarově	Caesarův	k2eAgNnSc6d1	Caesarovo
vystupování	vystupování	k1gNnSc6	vystupování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
63	[number]	k4	63
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
Catilinovo	Catilinův	k2eAgNnSc1d1	Catilinovo
spiknutí	spiknutí	k1gNnSc1	spiknutí
(	(	kIx(	(
<g/>
Bellum	Bellum	k1gInSc1	Bellum
Catilinae	Catilina	k1gInSc2	Catilina
<g/>
)	)	kIx)	)
od	od	k7c2	od
Caesarova	Caesarův	k2eAgMnSc2d1	Caesarův
přívržence	přívrženec	k1gMnSc2	přívrženec
Gaia	Gaius	k1gMnSc2	Gaius
Sallustia	Sallustius	k1gMnSc2	Sallustius
Crispa	Crisp	k1gMnSc2	Crisp
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
autorem	autor	k1gMnSc7	autor
Listů	list	k1gInPc2	list
Caesarovi	Caesarovi	k1gRnPc1	Caesarovi
(	(	kIx(	(
<g/>
Epistolae	Epistolae	k1gFnSc1	Epistolae
ad	ad	k7c4	ad
Caesarem	Caesar	k1gMnSc7	Caesar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
Caesarovi	Caesar	k1gMnSc6	Caesar
od	od	k7c2	od
Augustova	Augustův	k2eAgMnSc2d1	Augustův
současníka	současník	k1gMnSc2	současník
<g/>
,	,	kIx,	,
historika	historik	k1gMnSc2	historik
Tita	Titus	k1gMnSc2	Titus
Livia	Livius	k1gMnSc2	Livius
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
fragmenty	fragment	k1gInPc4	fragment
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
při	pře	k1gFnSc4	pře
psaní	psaní	k1gNnSc2	psaní
svého	svůj	k3xOyFgInSc2	svůj
Caesarova	Caesarův	k2eAgInSc2d1	Caesarův
životopisu	životopis	k1gInSc2	životopis
čerpal	čerpat	k5eAaImAgInS	čerpat
právě	právě	k9	právě
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
podrobností	podrobnost	k1gFnPc2	podrobnost
o	o	k7c6	o
Caesarovi	Caesar	k1gMnSc6	Caesar
je	být	k5eAaImIp3nS	být
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
i	i	k9	i
v	v	k7c6	v
Plútarchových	Plútarchův	k2eAgFnPc6d1	Plútarchova
biografiích	biografie	k1gFnPc6	biografie
Catona	Catona	k1gFnSc1	Catona
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
,	,	kIx,	,
Cicerona	Cicero	k1gMnSc2	Cicero
<g/>
,	,	kIx,	,
Crassa	Crass	k1gMnSc2	Crass
<g/>
,	,	kIx,	,
Marca	Marcus	k1gMnSc2	Marcus
Antonia	Antonio	k1gMnSc2	Antonio
a	a	k8xC	a
Pompeia	Pompeius	k1gMnSc2	Pompeius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působil	působit	k5eAaImAgMnS	působit
řecký	řecký	k2eAgMnSc1d1	řecký
dějepisec	dějepisec	k1gMnSc1	dějepisec
Appiános	Appiános	k1gMnSc1	Appiános
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
popisující	popisující	k2eAgInSc1d1	popisující
zánik	zánik	k1gInSc1	zánik
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Livia	Livius	k1gMnSc2	Livius
vycházel	vycházet	k5eAaImAgMnS	vycházet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
důležitým	důležitý	k2eAgInSc7d1	důležitý
pramenem	pramen	k1gInSc7	pramen
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
Caesarovo	Caesarův	k2eAgNnSc4d1	Caesarovo
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
