<s>
Snímková	snímkový	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
</s>
<s>
Snímková	snímkový	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
je	být	k5eAaImIp3nS
frekvence	frekvence	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
jakou	jaký	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
zobrazovací	zobrazovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
jednotlivé	jednotlivý	k2eAgInPc4d1
unikátní	unikátní	k2eAgInPc4d1
snímky	snímek	k1gInPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
záznamové	záznamový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
zachycuje	zachycovat	k5eAaImIp3nS
snímky	snímek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snímková	snímkový	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
udává	udávat	k5eAaImIp3nS
v	v	k7c6
jednotkách	jednotka	k1gFnPc6
fps	fps	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
frames	frames	k1gInSc1
per	pero	k1gNnPc2
second	second	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
prostě	prostě	k9
v	v	k7c6
hertzích	hertz	k1gInPc6
–	–	k?
v	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
jednotka	jednotka	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
jednomu	jeden	k4xCgNnSc3
snímku	snímek	k1gInSc6
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
snímkové	snímkový	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
pohyblivým	pohyblivý	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
grafice	grafika	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
zpracování	zpracování	k1gNnSc6
videa	video	k1gNnSc2
<g/>
,	,	kIx,
filmovém	filmový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
či	či	k8xC
televizním	televizní	k2eAgNnSc6d1
vysílání	vysílání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Snímkové	snímkový	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
ve	v	k7c6
filmu	film	k1gInSc6
a	a	k8xC
televizi	televize	k1gFnSc6
</s>
<s>
Ve	v	k7c6
filmu	film	k1gInSc6
<g/>
,	,	kIx,
televizi	televize	k1gFnSc6
a	a	k8xC
při	při	k7c6
zpracování	zpracování	k1gNnSc6
videa	video	k1gNnSc2
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
běžně	běžně	k6eAd1
používaných	používaný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
snímkové	snímkový	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
<g/>
:	:	kIx,
24	#num#	k4
a	a	k8xC
nově	nově	k6eAd1
48	#num#	k4
Hz	Hz	kA
pro	pro	k7c4
film	film	k1gInSc4
a	a	k8xC
pro	pro	k7c4
televizi	televize	k1gFnSc4
25	#num#	k4
a	a	k8xC
30	#num#	k4
Hz	Hz	kA
pro	pro	k7c4
neprokládaný	prokládaný	k2eNgInSc4d1
(	(	kIx(
<g/>
progresivní	progresivní	k2eAgInSc4d1
<g/>
)	)	kIx)
režim	režim	k1gInSc4
a	a	k8xC
50	#num#	k4
a	a	k8xC
60	#num#	k4
Hz	Hz	kA
pro	pro	k7c4
režim	režim	k1gInSc4
prokládaný	prokládaný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
23,976	23,976	k4
Hz	Hz	kA
(	(	kIx(
<g/>
24	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
snímkovací	snímkovací	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
nejčastěji	často	k6eAd3
používaná	používaný	k2eAgFnSc1d1
při	při	k7c6
záznamu	záznam	k1gInSc6
na	na	k7c4
filmový	filmový	k2eAgInSc4d1
pás	pás	k1gInSc4
kombinovaný	kombinovaný	k2eAgInSc4d1
se	se	k3xPyFc4
zvukovou	zvukový	k2eAgFnSc7d1
stopou	stopa	k1gFnSc7
(	(	kIx(
<g/>
němý	němý	k2eAgInSc1d1
film	film	k1gInSc1
ještě	ještě	k9
umožňoval	umožňovat	k5eAaImAgInS
ruční	ruční	k2eAgInSc1d1
posun	posun	k1gInSc1
s	s	k7c7
nestandardizovanou	standardizovaný	k2eNgFnSc7d1
frekvencí	frekvence	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
převodu	převod	k1gInSc6
filmu	film	k1gInSc2
na	na	k7c4
televizní	televizní	k2eAgInSc4d1
signál	signál	k1gInSc4
se	se	k3xPyFc4
u	u	k7c2
režimu	režim	k1gInSc2
PAL	pal	k1gInSc1
snímky	snímka	k1gFnSc2
převádí	převádět	k5eAaImIp3nS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
i	i	k8xC
za	za	k7c4
cenu	cena	k1gFnSc4
zrychlení	zrychlení	k1gNnSc3
o	o	k7c4
4,096	4,096	k4
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
diváci	divák	k1gMnPc1
zpravidla	zpravidla	k6eAd1
nepoznají	poznat	k5eNaPmIp3nP
<g/>
)	)	kIx)
<g/>
;	;	kIx,
u	u	k7c2
převodu	převod	k1gInSc2
na	na	k7c4
NTSC	NTSC	kA
s	s	k7c7
30	#num#	k4
fps	fps	k?
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pulldown	pulldowno	k1gNnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
dále	daleko	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
formát	formát	k1gInSc1
PAL	pal	k1gInSc1
má	mít	k5eAaImIp3nS
50	#num#	k4
půlsnímků	půlsnímek	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
25	#num#	k4
celých	celý	k2eAgInPc2d1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
(	(	kIx(
<g/>
50	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
formát	formát	k1gInSc1
NTSC	NTSC	kA
má	mít	k5eAaImIp3nS
59,94	59,94	k4
půlsnímků	půlsnímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
(	(	kIx(
<g/>
přesná	přesný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
je	být	k5eAaImIp3nS
60	#num#	k4
Hz	Hz	kA
<g/>
/	/	kIx~
<g/>
1,001	1,001	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celých	celá	k1gFnPc2
60	#num#	k4
snímků	snímek	k1gInPc2
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
každých	každý	k3xTgInPc2
500	#num#	k4
půlsnímků	půlsnímek	k1gInPc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
(	(	kIx(
<g/>
plánovanému	plánovaný	k2eAgMnSc3d1
<g/>
)	)	kIx)
vynechání	vynechání	k1gNnSc3
jednoho	jeden	k4xCgInSc2
půlsnímku	půlsnímek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
trhu	trh	k1gInSc6
existují	existovat	k5eAaImIp3nP
televize	televize	k1gFnPc1
s	s	k7c7
označením	označení	k1gNnSc7
„	„	k?
<g/>
100	#num#	k4
Hz	Hz	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hodnota	hodnota	k1gFnSc1
neznamená	znamenat	k5eNaImIp3nS
počet	počet	k1gInSc1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
ale	ale	k8xC
počet	počet	k1gInSc4
impulzů	impulz	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
vydá	vydat	k5eAaPmIp3nS
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
<g/>
-li	-li	k?
tedy	tedy	k9
v	v	k7c4
PAL	pal	k1gInSc4
(	(	kIx(
<g/>
50	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
půlsnímek	půlsnímek	k1gInSc1
vyšle	vyslat	k5eAaPmIp3nS
dvakrát	dvakrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledným	výsledný	k2eAgInSc7d1
efektem	efekt	k1gInSc7
je	být	k5eAaImIp3nS
dojem	dojem	k1gInSc1
menšího	malý	k2eAgNnSc2d2
blikání	blikání	k1gNnSc2
obrazu	obraz	k1gInSc2
než	než	k8xS
u	u	k7c2
50	#num#	k4
<g/>
Hz	Hz	kA
televizoru	televizor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1
některých	některý	k3yIgFnPc2
(	(	kIx(
<g/>
ploškových	plošková	k1gFnPc2
<g/>
,	,	kIx,
loutkových	loutkový	k2eAgInPc2d1
<g/>
,	,	kIx,
kreslených	kreslený	k2eAgInPc2d1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
animovaných	animovaný	k2eAgInPc2d1
filmů	film	k1gInPc2
občas	občas	k6eAd1
přistupují	přistupovat	k5eAaImIp3nP
k	k	k7c3
praktice	praktika	k1gFnSc3
točit	točit	k5eAaImF
jeden	jeden	k4xCgInSc4
snímek	snímek	k1gInSc4
na	na	k7c4
místo	místo	k1gNnSc4
dvou	dva	k4xCgMnPc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
s	s	k7c7
poloviční	poloviční	k2eAgFnSc7d1
snímkovou	snímkový	k2eAgFnSc7d1
frekvencí	frekvence	k1gFnSc7
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
12	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volí	volit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
pro	pro	k7c4
i	i	k9
tak	tak	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
náročnost	náročnost	k1gFnSc4
animování	animování	k1gNnSc2
(	(	kIx(
<g/>
přípravy	příprava	k1gFnSc2
pro	pro	k7c4
desetitisíce	desetitisíce	k1gInPc4
snímků	snímek	k1gInPc2
za	za	k7c4
každou	každý	k3xTgFnSc4
minutu	minuta	k1gFnSc4
výsledného	výsledný	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
výsledný	výsledný	k2eAgInSc1d1
film	film	k1gInSc1
ve	v	k7c6
správné	správný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
snímek	snímek	k1gInSc1
se	se	k3xPyFc4
promítne	promítnout	k5eAaPmIp3nS
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
ve	v	k7c6
formě	forma	k1gFnSc6
videosignálu	videosignál	k1gInSc2
se	se	k3xPyFc4
převádí	převádět	k5eAaImIp3nS
ve	v	k7c6
střihovém	střihový	k2eAgInSc6d1
nebo	nebo	k8xC
konverzním	konverzní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
formě	forma	k1gFnSc6
filmu	film	k1gInSc2
se	se	k3xPyFc4
zduplikuje	zduplikovat	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
filmové	filmový	k2eAgFnSc6d1
kopírce	kopírka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Převody	převod	k1gInPc1
snímkovacích	snímkovací	k2eAgFnPc2d1
frekvencí	frekvence	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
součástí	součást	k1gFnSc7
např.	např.	kA
procesu	proces	k1gInSc2
telecine	telecinout	k5eAaPmIp3nS
při	při	k7c6
úpravě	úprava	k1gFnSc6
filmového	filmový	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
pro	pro	k7c4
televizní	televizní	k2eAgInPc4d1
formáty	formát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
technikou	technika	k1gFnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pulldown	pulldown	k1gInSc1
–	–	k?
pro	pro	k7c4
převod	převod	k1gInSc4
PAL	pal	k1gInSc1
→	→	k?
NTSC	NTSC	kA
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
zaznamenané	zaznamenaný	k2eAgNnSc1d1
zpomalení	zpomalení	k1gNnSc1
času	čas	k1gInSc2
je	být	k5eAaImIp3nS
záběr	záběr	k1gInSc1
světla	světlo	k1gNnSc2
pronikající	pronikající	k2eAgFnSc7d1
plastovou	plastový	k2eAgFnSc7d1
lahví	lahev	k1gFnSc7
Coca-Coly	coca-cola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pořízen	pořídit	k5eAaPmNgMnS
na	na	k7c6
půdě	půda	k1gFnSc6
Massachusettského	massachusettský	k2eAgInSc2d1
technologického	technologický	k2eAgInSc2d1
institutu	institut	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
frekvenci	frekvence	k1gFnSc4
1	#num#	k4
Tfps	Tfpsa	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
bilión	bilión	k4xCgInSc1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
tj.	tj.	kA
1,000,000,000,000	1,000,000,000,000	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tera	Tera	k1gMnSc1
=	=	kIx~
1,000,000,000,000	1,000,000,000,000	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
tedy	tedy	k9
dostatečně	dostatečně	k6eAd1
zpomalený	zpomalený	k2eAgMnSc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
viděl	vidět	k5eAaImAgMnS
samotné	samotný	k2eAgNnSc4d1
šíření	šíření	k1gNnSc4
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záběr	záběr	k1gInSc4
ale	ale	k8xC
nebyl	být	k5eNaImAgInS
pořízen	pořídit	k5eAaPmNgInS
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
ale	ale	k8xC
postupně	postupně	k6eAd1
„	„	k?
<g/>
po	po	k7c6
fotonech	foton	k1gInPc6
<g/>
“	“	k?
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
vystřelovaných	vystřelovaný	k2eAgMnPc2d1
z	z	k7c2
emitoru	emitor	k1gInSc2
a	a	k8xC
zaznamenávaných	zaznamenávaný	k2eAgFnPc2d1
každého	každý	k3xTgMnSc2
zvlášť	zvlášť	k6eAd1
a	a	k9
každého	každý	k3xTgNnSc2
s	s	k7c7
rostoucím	rostoucí	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iluze	iluze	k1gFnSc1
pohybu	pohyb	k1gInSc2
světla	světlo	k1gNnSc2
vznikla	vzniknout	k5eAaPmAgFnS
složením	složení	k1gNnSc7
všech	všecek	k3xTgInPc2
obrázků	obrázek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
</s>
<s>
Hodnotou	hodnota	k1gFnSc7
snímkové	snímkový	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
se	se	k3xPyFc4
např.	např.	kA
hodnotí	hodnotit	k5eAaImIp3nS
obrazová	obrazový	k2eAgFnSc1d1
odezva	odezva	k1gFnSc1
a	a	k8xC
plynulost	plynulost	k1gFnSc1
grafiky	grafika	k1gFnSc2
u	u	k7c2
počítačových	počítačový	k2eAgFnPc2d1
her	hra	k1gFnPc2
na	na	k7c6
konkrétním	konkrétní	k2eAgInSc6d1
počítači	počítač	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
minimální	minimální	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
pro	pro	k7c4
plynulou	plynulý	k2eAgFnSc4d1
grafiku	grafika	k1gFnSc4
(	(	kIx(
<g/>
při	při	k7c6
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
lidské	lidský	k2eAgNnSc1d1
oko	oko	k1gNnSc1
nerozezná	rozeznat	k5eNaPmIp3nS
jednotlivé	jednotlivý	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
<g/>
)	)	kIx)
se	se	k3xPyFc4
bere	brát	k5eAaImIp3nS
24	#num#	k4
fps	fps	k?
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
60	#num#	k4
fps	fps	k?
se	se	k3xPyFc4
bere	brát	k5eAaImIp3nS
jako	jako	k9
naprosto	naprosto	k6eAd1
dostatečné	dostatečný	k2eAgInPc1d1
pro	pro	k7c4
bezproblémový	bezproblémový	k2eAgInSc4d1
chod	chod	k1gInSc4
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Avšak	avšak	k8xC
u	u	k7c2
náročnějších	náročný	k2eAgFnPc2d2
her	hra	k1gFnPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vidět	vidět	k5eAaImF
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c4
60	#num#	k4
<g/>
,	,	kIx,
120	#num#	k4
a	a	k8xC
240	#num#	k4
fps	fps	k?
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
zpravidla	zpravidla	k6eAd1
poznáte	poznat	k5eAaPmIp2nP
zpomaleným	zpomalený	k2eAgInSc7d1
pohybem	pohyb	k1gInSc7
myši	myš	k1gFnSc2
na	na	k7c6
obrazovce	obrazovka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
zvýšení	zvýšení	k1gNnSc2
fps	fps	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
zhoršení	zhoršení	k1gNnSc4
detailů	detail	k1gInPc2
apod	apod	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
snímky	snímek	k1gInPc7
za	za	k7c4
sekundu	sekunda	k1gFnSc4
zvýší	zvýšit	k5eAaPmIp3nP
a	a	k8xC
obraz	obraz	k1gInSc4
bude	být	k5eAaImBp3nS
pevnější	pevný	k2eAgMnSc1d2
a	a	k8xC
plynulejší	plynulý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Snímková	snímkový	k2eAgFnSc1d1
</s>
<s>
frekvence	frekvence	k1gFnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
také	také	k9
definovat	definovat	k5eAaBmF
kolikrát	kolikrát	k6eAd1
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
obraz	obraz	k1gInSc1
za	za	k7c4
1	#num#	k4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS
aktualizace	aktualizace	k1gFnSc1
obrazu	obraz	k1gInSc2
na	na	k7c6
monitoru	monitor	k1gInSc6
x	x	k?
krát	krát	k6eAd1
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
x	x	k?
je	být	k5eAaImIp3nS
Právě	právě	k9
</s>
<s>
daný	daný	k2eAgInSc1d1
počet	počet	k1gInSc1
snímků	snímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Složené	složený	k2eAgNnSc1d1
oko	oko	k1gNnSc1
</s>
<s>
Pohyb	pohyb	k1gInSc1
složeného	složený	k2eAgNnSc2d1
oka	oko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
mají	mít	k5eAaImIp3nP
někteří	některý	k3yIgMnPc1
korýši	korýš	k1gMnPc1
a	a	k8xC
dospělá	dospělý	k2eAgNnPc4d1
stádia	stádium	k1gNnPc4
hmyzu	hmyz	k1gInSc2
s	s	k7c7
nedokonalou	dokonalý	k2eNgFnSc7d1
proměnou	proměna	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vnímán	vnímat	k5eAaImNgInS
jako	jako	k9
přechod	přechod	k1gInSc1
z	z	k7c2
jednoho	jeden	k4xCgNnSc2
očka	očko	k1gNnSc2
(	(	kIx(
<g/>
ommatidia	ommatidium	k1gNnSc2
<g/>
)	)	kIx)
do	do	k7c2
druhého	druhý	k4xOgNnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
pak	pak	k9
živočich	živočich	k1gMnSc1
nevidí	vidět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vnímat	vnímat	k5eAaImF
více	hodně	k6eAd2
impulzů	impulz	k1gInPc2
a	a	k8xC
lépe	dobře	k6eAd2
a	a	k8xC
včas	včas	k6eAd1
na	na	k7c4
pohyb	pohyb	k1gInSc4
reagovat	reagovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
impulzů	impulz	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
je	být	k5eAaImIp3nS
druhově	druhově	k6eAd1
závislý	závislý	k2eAgInSc1d1
a	a	k8xC
pohybuje	pohybovat	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c7
8	#num#	k4
a	a	k8xC
200	#num#	k4
fps	fps	k?
(	(	kIx(
<g/>
např.	např.	kA
u	u	k7c2
včel	včela	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejběžnější	běžný	k2eAgFnSc1d3
hodnota	hodnota	k1gFnSc1
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
60	#num#	k4
a	a	k8xC
120	#num#	k4
fps	fps	k?
(	(	kIx(
<g/>
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
u	u	k7c2
člověka	člověk	k1gMnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
kolem	kolem	k7c2
60	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmyzu	hmyz	k1gInSc2
tak	tak	k9
pohyb	pohyb	k1gInSc1
většinově	většinově	k6eAd1
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
pomalejší	pomalý	k2eAgMnSc1d2
než	než	k8xS
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hodnota	hodnota	k1gFnSc1
udává	udávat	k5eAaImIp3nS
<g/>
,	,	kIx,
při	při	k7c6
kolika	kolik	k4yQc6,k4yIc6,k4yRc6
impulzech	impulz	k1gInPc6
za	za	k7c4
sekundu	sekunda	k1gFnSc4
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
vnímán	vnímat	k5eAaImNgInS
jako	jako	k8xS,k8xC
plynulý	plynulý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.dailymail.co.uk/sciencetech/article-2191451/The-camera-TRILLION-pictures-second--making-fast-watch-beams-light-travelling-slow-motion.html	http://www.dailymail.co.uk/sciencetech/article-2191451/The-camera-TRILLION-pictures-second--making-fast-watch-beams-light-travelling-slow-motion.html	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Časosběr	Časosběr	k1gMnSc1
</s>
<s>
Rychloběžná	rychloběžný	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
