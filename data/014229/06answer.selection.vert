<s desamb="1">
Snímková	snímkový	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
udává	udávat	k5eAaImIp3nS
v	v	k7c6
jednotkách	jednotka	k1gFnPc6
fps	fps	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
frames	frames	k1gInSc1
per	pero	k1gNnPc2
second	second	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
prostě	prostě	k9
v	v	k7c6
hertzích	hertz	k1gInPc6
–	–	k?
v	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
jednotka	jednotka	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
jednomu	jeden	k4xCgNnSc3
snímku	snímek	k1gInSc6
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>