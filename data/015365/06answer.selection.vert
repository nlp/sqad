<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
(	(	kIx(
<g/>
též	též	k9
síran	síran	k1gInSc1
hlinito-draselný	hlinito-draselný	k2eAgInSc1d1
<g/>
,	,	kIx,
kamenec	kamenec	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
jen	jen	k9
kamenec	kamenec	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podvojná	podvojný	k2eAgFnSc1d1
sůl	sůl	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
s	s	k7c7
chemickým	chemický	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
KAl	kal	k1gInSc1
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>