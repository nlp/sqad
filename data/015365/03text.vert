<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
</s>
<s>
Kamenec	Kamenec	k1gInSc1
</s>
<s>
Síran	síran	k1gInSc1
hlinito-draselný	hlinito-draselný	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
</s>
<s>
Triviální	triviální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
kamenec	kamenec	k1gInSc1
draselno-hlinitýkamenec	draselno-hlinitýkamenec	k1gInSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Aluminium	aluminium	k1gNnSc1
potassium	potassium	k1gNnSc1
sulfate	sulfat	k1gMnSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Aluminiumkaliumsulfat-Dodecahydrat	Aluminiumkaliumsulfat-Dodecahydrat	k1gMnSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
AlK	AlK	k?
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
nebo	nebo	k8xC
krystalky	krystalka	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
10043-67-1	10043-67-1	k4
</s>
<s>
7784-24-9	7784-24-9	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
258,206	258,206	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
312,252	312,252	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
trihydrát	trihydrát	k1gInSc1
<g/>
)	)	kIx)
<g/>
474,390	474,390	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
92,5	92,5	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
dehydratace	dehydratace	k1gFnSc2
</s>
<s>
dodekahydrát	dodekahydrát	k1gInSc1
64,5	64,5	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
-	-	kIx~
<g/>
9	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
200	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
-	-	kIx~
<g/>
12	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,757	1,757	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
dodekahydrát	dodekahydrát	k1gInSc1
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
nP	nP	k?
<g/>
=	=	kIx~
1,456	1,456	k4
45	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
dodekahydrát	dodekahydrát	k1gInSc1
(	(	kIx(
<g/>
β	β	k?
<g/>
)	)	kIx)
nPř	nPř	k?
<g/>
=	=	kIx~
1,429	1,429	k4
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
nPm	nPm	k?
<g/>
=	=	kIx~
1,456	1,456	k4
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
3,0	3,0	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
0	#num#	k4
C	C	kA
<g/>
)	)	kIx)
<g/>
4,0	4,0	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
10	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
5,9	5,9	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
7,23	7,23	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
8,39	8,39	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
30	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
11,7	11,7	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
40	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
17,0	17,0	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
50	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
24,75	24,75	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
60	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
40,0	40,0	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
70	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
71	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
80	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
109	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
90	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
119	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
92,5	92,5	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
dodekahydrát	dodekahydrát	k1gInSc1
5,92	5,92	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
11,27	11,27	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
23,61	23,61	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
40	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
35,61	35,61	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
50	#num#	k4
°	°	k?
<g/>
C	C	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
55,85	55,85	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
60	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
173,18	173,18	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
80	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
229,1	229,1	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
(	(	kIx(
<g/>
90	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
ε	ε	k1gFnPc2
</s>
<s>
6,67	6,67	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
susceptibilita	susceptibilita	k1gFnSc1
</s>
<s>
−	−	k?
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Krystalová	krystalový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
krychlová	krychlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
,	,	kIx,
α	α	k?
<g/>
)	)	kIx)
šesterečná	šesterečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
,	,	kIx,
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Hrana	hrana	k1gFnSc1
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
</s>
<s>
dodekahydrát	dodekahydrát	k1gInSc1
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
473,7	473,7	k4
pm	pm	k?
dodekahydrát	dodekahydrát	k1gInSc1
(	(	kIx(
<g/>
β	β	k?
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
689,2	689,2	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
1	#num#	k4
732	#num#	k4
pm	pm	k?
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
470	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
−	−	k?
381	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
trihydrát	trihydrát	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
0	#num#	k4
<g/>
61,8	61,8	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Entalpie	entalpie	k1gFnSc1
tání	tání	k1gNnSc3
Δ	Δ	k5eAaPmF
</s>
<s>
59,1	59,1	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
205	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
314	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
trihydrát	trihydrát	k1gInSc1
<g/>
)	)	kIx)
687,4	687,4	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
Gibbsova	Gibbsův	k2eAgFnSc1d1
energie	energie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
240	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
−	−	k?
975	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
trihydrát	trihydrát	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
141,7	141,7	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Izobarické	izobarický	k2eAgNnSc1d1
měrné	měrný	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
cp	cp	k?
</s>
<s>
0,747	0,747	k4
3	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
1,372	1,372	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
dodekahydrát	dodekahydrát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
žádné	žádný	k3yNgInPc1
nejsou	být	k5eNaImIp3nP
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
žádné	žádný	k3yNgInPc1
nejsou	být	k5eNaImIp3nP
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
(	(	kIx(
<g/>
též	též	k9
síran	síran	k1gInSc1
hlinito-draselný	hlinito-draselný	k2eAgInSc1d1
<g/>
,	,	kIx,
kamenec	kamenec	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
jen	jen	k9
kamenec	kamenec	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podvojná	podvojný	k2eAgFnSc1d1
sůl	sůl	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
s	s	k7c7
chemickým	chemický	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
KAl	kal	k1gInSc1
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
dodekahydrát	dodekahydrát	k1gInSc1
KAl	kal	k1gInSc1
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
·	·	k?
<g/>
12	#num#	k4
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
při	při	k7c6
čištění	čištění	k1gNnSc6
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
činění	činění	k1gNnSc2
koží	kož	k1gFnPc2
a	a	k8xC
střívek	střívko	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
ohnivzdorném	ohnivzdorný	k2eAgInSc6d1
textilu	textil	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
kypřicích	kypřicí	k2eAgInPc6d1
prášcích	prášek	k1gInPc6
a	a	k8xC
jako	jako	k9
antiperspirant	antiperspirant	k1gMnSc1
a	a	k8xC
adstringens	adstringens	k1gNnSc1
po	po	k7c6
holení	holení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
krystalizuje	krystalizovat	k5eAaImIp3nS
v	v	k7c6
osmistěnech	osmistěn	k1gInPc6
se	s	k7c7
zploštělými	zploštělý	k2eAgInPc7d1
vrcholy	vrchol	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
rozpustný	rozpustný	k2eAgMnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roztok	Roztoky	k1gInPc2
způsobuje	způsobovat	k5eAaImIp3nS
zčervenání	zčervenání	k1gNnSc1
lakmusu	lakmus	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
kyselý	kyselý	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
adstringenční	adstringenční	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zahřívání	zahřívání	k1gNnSc6
do	do	k7c2
téměř	téměř	k6eAd1
červeného	červený	k2eAgInSc2d1
žáru	žár	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
porézní	porézní	k2eAgFnSc4d1
<g/>
,	,	kIx,
drobivou	drobivý	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
známou	známý	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
pálený	pálený	k2eAgInSc4d1
kamenec	kamenec	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
92	#num#	k4
°	°	k?
<g/>
C	C	kA
taje	tát	k5eAaImIp3nS
(	(	kIx(
<g/>
respektive	respektive	k9
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
<g/>
)	)	kIx)
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
krystalové	krystalový	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
Neutrální	neutrální	k2eAgInSc1d1
kamenec	kamenec	k1gInSc1
<g/>
“	“	k?
vznikne	vzniknout	k5eAaPmIp3nS
přidáním	přidání	k1gNnSc7
takového	takový	k3xDgNnSc2
množství	množství	k1gNnSc2
uhličitanu	uhličitan	k1gInSc2
sodného	sodný	k2eAgInSc2d1
do	do	k7c2
roztoku	roztok	k1gInSc2
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
začne	začít	k5eAaPmIp3nS
vylučovat	vylučovat	k5eAaImF
oxid	oxid	k1gInSc4
hlinitý	hlinitý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
nachází	nacházet	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
jako	jako	k8xC,k8xS
mořidlo	mořidlo	k1gNnSc4
<g/>
,	,	kIx,
při	při	k7c6
přípravě	příprava	k1gFnSc6
lázně	lázeň	k1gFnSc2
pro	pro	k7c4
ručně	ručně	k6eAd1
vyráběný	vyráběný	k2eAgInSc4d1
papír	papír	k1gInSc4
a	a	k8xC
pro	pro	k7c4
čiření	čiření	k1gNnSc4
zakalených	zakalený	k2eAgFnPc2d1
kapalin	kapalina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Minerální	minerální	k2eAgFnSc1d1
forma	forma	k1gFnSc1
a	a	k8xC
výskyt	výskyt	k1gInSc1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgMnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
typicky	typicky	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
enkrustace	enkrustace	k1gFnPc1
na	na	k7c6
skalách	skála	k1gFnPc6
v	v	k7c6
místech	místo	k1gNnPc6
zvětrávání	zvětrávání	k1gNnSc2
a	a	k8xC
oxidace	oxidace	k1gFnSc2
sulfidových	sulfidový	k2eAgInPc2d1
a	a	k8xC
draselných	draselný	k2eAgInPc2d1
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
se	se	k3xPyFc4
získával	získávat	k5eAaImAgMnS
z	z	k7c2
alunitu	alunit	k1gInSc2
<g/>
,	,	kIx,
nerostu	nerost	k1gInSc2
těženého	těžený	k2eAgInSc2d1
ze	z	k7c2
sirných	sirný	k2eAgInPc2d1
sopečných	sopečný	k2eAgInPc2d1
sedimentů	sediment	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Alunit	Alunit	k1gInSc1
se	se	k3xPyFc4
často	často	k6eAd1
přidružuje	přidružovat	k5eAaImIp3nS
ke	k	k7c3
zdrojům	zdroj	k1gInPc3
draslíku	draslík	k1gInSc2
a	a	k8xC
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
hlášen	hlásit	k5eAaImNgInS
na	na	k7c6
Vesuvu	Vesuv	k1gInSc6
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
Springsure	Springsur	k1gMnSc5
(	(	kIx(
<g/>
Queensland	Queenslando	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c4
Alum	Alum	k1gInSc4
Cave	Cav	k1gFnSc2
(	(	kIx(
<g/>
Tennessee	Tennessee	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alum	Alum	k1gMnSc1
Gulch	Gulch	k1gMnSc1
(	(	kIx(
<g/>
Arizona	Arizona	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
USA	USA	kA
a	a	k8xC
na	na	k7c6
ostrově	ostrov	k1gInSc6
Cebu	Cebus	k1gInSc2
(	(	kIx(
<g/>
Filipíny	Filipíny	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
tawas	tawas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbuznými	příbuzný	k2eAgInPc7d1
minerály	minerál	k1gInPc7
jsou	být	k5eAaImIp3nP
kalunit	kalunit	k1gInSc4
(	(	kIx(
<g/>
kamenná	kamenný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
kalinit	kalinit	k5eAaPmF
<g/>
,	,	kIx,
vláknitý	vláknitý	k2eAgInSc1d1
minerál	minerál	k1gInSc1
se	s	k7c7
vzorcem	vzorec	k1gInSc7
KAl	kal	k1gInSc1
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
·	·	k?
<g/>
11	#num#	k4
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
má	mít	k5eAaImIp3nS
adstringenční	adstringenční	k2eAgInSc1d1
a	a	k8xC
antiseptické	antiseptický	k2eAgInPc1d1
účinky	účinek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
ho	on	k3xPp3gInSc4
lze	lze	k6eAd1
používat	používat	k5eAaImF
jako	jako	k9
přírodní	přírodní	k2eAgInSc4d1
deodorant	deodorant	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
inhibuje	inhibovat	k5eAaBmIp3nS
růst	růst	k1gInSc1
bakterií	bakterie	k1gFnPc2
odpovědných	odpovědný	k2eAgInPc2d1
za	za	k7c4
tělesný	tělesný	k2eAgInSc4d1
pach	pach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
použití	použití	k1gNnSc1
nezabraňuje	zabraňovat	k5eNaImIp3nS
pocení	pocení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adstrigenční	Adstrigenční	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
se	se	k3xPyFc4
často	často	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
po	po	k7c6
holení	holení	k1gNnSc6
a	a	k8xC
proti	proti	k7c3
krvácení	krvácení	k1gNnSc3
z	z	k7c2
malých	malý	k2eAgFnPc2d1
ran	rána	k1gFnPc2
a	a	k8xC
odřenin	odřenina	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
krvácení	krvácení	k1gNnSc6
z	z	k7c2
nosu	nos	k1gInSc2
a	a	k8xC
z	z	k7c2
hemoroidů	hemoroidy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
zevně	zevně	k6eAd1
i	i	k9
vnitřně	vnitřně	k6eAd1
v	v	k7c6
tradičních	tradiční	k2eAgNnPc6d1
lékařstvích	lékařství	k1gNnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
ájurvédském	ájurvédský	k2eAgMnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
phitkarí	phitkarí	k2eAgFnSc1d1
nebo	nebo	k8xC
sauraštrí	sauraštrí	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
říká	říkat	k5eAaImIp3nS
明	明	k?
míngfán	míngfat	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
pro	pro	k7c4
vytvrzování	vytvrzování	k1gNnSc4
fotografických	fotografický	k2eAgFnPc2d1
emulzí	emulze	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c6
filmech	film	k1gInPc6
a	a	k8xC
papírech	papír	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
jako	jako	k9
součást	součást	k1gFnSc1
ustalovače	ustalovač	k1gInSc2
<g/>
;	;	kIx,
moderní	moderní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
jsou	být	k5eAaImIp3nP
však	však	k9
již	již	k6eAd1
z	z	k7c2
výroby	výroba	k1gFnSc2
dostatečně	dostatečně	k6eAd1
vytvrzeny	vytvrzen	k2eAgFnPc1d1
a	a	k8xC
proto	proto	k8xC
tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
již	již	k6eAd1
není	být	k5eNaImIp3nS
běžný	běžný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
slabě	slabě	k6eAd1
dráždivý	dráždivý	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
kamenec	kamenec	k1gInSc1
</s>
<s>
jirchářské	jirchářský	k2eAgNnSc1d1
činění	činění	k1gNnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Potassium	Potassium	k1gNnSc4
alum	aluma	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Bottomley	Bottomlea	k1gFnPc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
p.	p.	k?
35	#num#	k4
<g/>
↑	↑	k?
http://webmineral.com/data/Potassium-alum.shtml	http://webmineral.com/data/Potassium-alum.shtml	k1gMnSc1
Potassium	Potassium	k1gNnSc1
Alum	Alum	k1gMnSc1
<g/>
:	:	kIx,
Mineral	Mineral	k1gMnSc1
Data	datum	k1gNnSc2
<g/>
↑	↑	k?
http://www.mindat.org/show.php?id=3267	http://www.mindat.org/show.php?id=3267	k4
Mindat	Mindat	k1gFnSc2
<g/>
↑	↑	k?
http://webmineral.com/data/Kalinite.shtml	http://webmineral.com/data/Kalinite.shtml	k1gMnSc1
Webmineral	Webmineral	k1gMnSc1
<g/>
↑	↑	k?
http://tcm.health-info.org/Herbology.Materia.Medica/mingfan-properties.htm	http://tcm.health-info.org/Herbology.Materia.Medica/mingfan-properties.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Uses	Usesa	k1gFnPc2
of	of	k?
Alum	Alum	k1gMnSc1
in	in	k?
Traditional	Traditional	k1gMnSc1
Chinese	Chinese	k1gFnSc2
Medicine	Medicin	k1gInSc5
<g/>
↑	↑	k?
Gallego	Gallego	k1gNnSc1
H	H	kA
<g/>
,	,	kIx,
Lewis	Lewis	k1gFnSc1
EJ	ej	k0
<g/>
,	,	kIx,
Crutchfield	Crutchfield	k1gMnSc1
CE	CE	kA
3	#num#	k4
<g/>
rd	rd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crystal	Crystal	k1gFnSc1
deodorant	deodorant	k1gInSc1
dermatitis	dermatitis	k1gFnSc1
<g/>
:	:	kIx,
irritant	irritant	k1gMnSc1
dermatitis	dermatitis	k1gFnSc1
to	ten	k3xDgNnSc1
alum-containing	alum-containing	k1gInSc1
deodorant	deodorant	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cutis	Cutis	k1gInSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10431678	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bottomley	Bottomley	k1gInPc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
Bottomley	Bottomle	k1gMnPc4
<g/>
,	,	kIx,
L.A.	L.A.	k1gMnPc4
School	Schoola	k1gFnPc2
of	of	k?
Chemistry	Chemistr	k1gMnPc4
&	&	k?
Bichemistry	Bichemistr	k1gMnPc7
<g/>
,	,	kIx,
Georgia	Georgia	k1gFnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
,	,	kIx,
Chemistry	Chemistr	k1gMnPc7
1310	#num#	k4
<g/>
:	:	kIx,
Laboratory	Laborator	k1gMnPc4
Manual	Manual	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plymouth	Plymoutha	k1gFnPc2
<g/>
,	,	kIx,
MI	já	k3xPp1nSc3
<g/>
:	:	kIx,
Hayden-McNeil	Hayden-McNeila	k1gFnPc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7380	#num#	k4
<g/>
-	-	kIx~
<g/>
3819	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
JIŘÍ	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
KAREL	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
ALOIS	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Potassium	Potassium	k1gNnSc1
Alum	Aluma	k1gFnPc2
<g/>
:	:	kIx,
Mineral	Mineral	k1gFnSc1
Data	datum	k1gNnSc2
</s>
<s>
Mindat	Mindat	k5eAaImF,k5eAaPmF
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Anorganické	anorganický	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
draselné	draselný	k2eAgFnSc2d1
Halogenidy	Halogenida	k1gFnSc2
a	a	k8xC
pseudohalogenidy	pseudohalogenida	k1gFnSc2
</s>
<s>
Hydrogendifluorid	Hydrogendifluorid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KHF	KHF	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fluorid	fluorid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KF	KF	kA
<g/>
)	)	kIx)
•	•	k?
Bromid	bromid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBr	KBr	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chlorid	chlorid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KCl	KCl	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jodid	jodid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KI	KI	kA
<g/>
)	)	kIx)
•	•	k?
Kyanid	kyanid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KCN	KCN	kA
<g/>
)	)	kIx)
•	•	k?
Kyanatan	Kyanatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KOCN	KOCN	kA
<g/>
)	)	kIx)
•	•	k?
Isokyanát	Isokyanát	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KNCO	KNCO	kA
<g/>
)	)	kIx)
•	•	k?
Fulminát	fulminát	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KCNO	KCNO	kA
<g/>
)	)	kIx)
•	•	k?
Thiokyanatan	Thiokyanatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KSCN	KSCN	kA
<g/>
)	)	kIx)
•	•	k?
Isothiokyanatan	Isothiokyanatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KNCS	KNCS	kA
<g/>
)	)	kIx)
Soli	sůl	k1gFnPc1
kyslíkatých	kyslíkatý	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
neuvedeny	uveden	k2eNgFnPc4d1
soli	sůl	k1gFnPc4
</s>
<s>
se	s	k7c7
záměnou	záměna	k1gFnSc7
kyslíku	kyslík	k1gInSc2
za	za	k7c4
jiný	jiný	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
složené	složený	k2eAgInPc1d1
a	a	k8xC
„	„	k?
<g/>
zásadité	zásaditý	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Chlornan	chlornan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KOCl	KOCl	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Chloritan	Chloritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KClO	KClO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chlorečnan	chlorečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KClO	KClO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chloristan	chloristan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KClO	KClO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bromnan	Bromnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KOBr	KOBr	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bromitan	Bromitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBrO	KBrO	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bromičnan	bromičnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBrO	KBrO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jodičnan	Jodičnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
KIO	KIO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogendijodičnan	Hydrogendijodičnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
KHI	KHI	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
Jodistan	Jodistan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KIO	KIO	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Siřičitan	siřičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
SO	So	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogensiřičitan	Hydrogensiřičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KHSO	KHSO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Disiřičitan	Disiřičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dithioničitan	Dithioničitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
O	o	k7c4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dithionan	Dithionan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Síran	síran	k1gInSc1
draselno-hlinitý	draselno-hlinitý	k2eAgInSc1d1
(	(	kIx(
<g/>
KAl	kal	k1gInSc1
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Síran	síran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogensíran	Hydrogensíran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KHSO	KHSO	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Disíran	Disíran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peroxosíran	Peroxosíran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
SO	So	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hydrogenperoxosíran	Hydrogenperoxosíran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KHSO	KHSO	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peroxodisíran	Peroxodisíran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Seleničitan	Seleničitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
SeO	SeO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Selenan	Selenan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
SeO	SeO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Telluričitan	Telluričitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
TeO	Tea	k1gFnSc5
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Metatelluran	Metatelluran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
TeO	Tea	k1gFnSc5
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tetrahydrogenorthotelluran	Tetrahydrogenorthotelluran	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
TeO	Tea	k1gFnSc5
<g/>
6	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Dusitan	dusitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KNO	KNO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dusičnan	dusičnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KNO	KNO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Orthodusičnan	Orthodusičnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
NO	no	k9
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peroxydusitan	Peroxydusitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KOONO	KOONO	kA
<g/>
)	)	kIx)
•	•	k?
Fosfornan	Fosfornan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
KPO	KPO	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dihydrogenfosforitan	Dihydrogenfosforitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
PO	po	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogenfosforitan	Hydrogenfosforitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
HPO	HPO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dihydrogenfosforečnan	Dihydrogenfosforečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KH	kh	k0
<g/>
2	#num#	k4
<g/>
PO	po	k7c4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hydrogenfosforečnan	Hydrogenfosforečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
HPO	HPO	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fosforečnan	fosforečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
PO	po	k7c4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dihydrogendifosforečnan	Dihydrogendifosforečnan	k1gMnSc1
didraselný	didraselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
P	P	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hydrogendifosforečnan	Hydrogendifosforečnany	k1gInPc2
tridraselný	tridraselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
HP	HP	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Difosforečnan	Difosforečnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
4	#num#	k4
<g/>
P	P	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trifosforečnan	Trifosforečnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
5	#num#	k4
<g/>
P	P	kA
<g/>
3	#num#	k4
<g/>
O	o	k7c6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Metafosforečnan	metafosforečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KPO	KPO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cyklotrifosforečnan	Cyklotrifosforečnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
P	P	kA
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cyklohexafosforečnan	Cyklohexafosforečnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
6	#num#	k4
<g/>
P	P	kA
<g/>
6	#num#	k4
<g/>
O	o	k7c4
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Arsenitan	Arsenitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KAsO	kasa	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dihydrogenarseničnan	Dihydrogenarseničnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KH	kh	k0
<g/>
2	#num#	k4
<g/>
AsO	AsO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogenarseničnan	Hydrogenarseničnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
HAsO	HAsO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arseničnan	Arseničnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
3	#num#	k4
<g/>
AsO	AsO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Hexahydrogenantimoničnan	Hexahydrogenantimoničnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KSb	KSb	k1gFnSc1
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Antimonitan	Antimonitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KSbO	KSbO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bismutičnan	Bismutičnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBiO	KBiO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Uhličitan	uhličitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
CO	co	k8xS
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Hydrogenuhličitan	Hydrogenuhličitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KHCO	KHCO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šťavelan	šťavelan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
CO	co	k6eAd1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogenšťavelan	Hydrogenšťavelan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KH	kh	k0
<g/>
(	(	kIx(
<g/>
CO	co	k9
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Orthokřemičitan	Orthokřemičitan	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
4	#num#	k4
<g/>
SiO	SiO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Metakřemičitan	metakřemičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
SiO	SiO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
~	~	kIx~
<g/>
)	)	kIx)
•	•	k?
Dikřemičitan	Dikřemičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trikřemičitan	Trikřemičitan	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cínatan	cínatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
SnO	SnO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cíničitan	Cíničitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
SnO	SnO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Metaboritan	Metaboritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBO	KBO	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tetraboritan	tetraboritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
B	B	kA
<g/>
4	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
•	•	k?
<g/>
8	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
•	•	k?
Oktaboritan	Oktaboritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
B	B	kA
<g/>
8	#num#	k4
<g/>
O	o	k7c4
<g/>
13	#num#	k4
<g/>
•	•	k?
<g/>
4	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
•	•	k?
Perboritan	perboritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
B	B	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hlinitan	hlinitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tetrahydroxidozinečnatan	Tetrahydroxidozinečnatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
Zn	zn	kA
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zinečnatan	zinečnatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
ZnO	ZnO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Železičitan	Železičitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
FeO	FeO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Železan	Železan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
FeO	FeO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Osman	Osman	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
OsO	osa	k1gFnSc5
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manganitan	Manganitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
KMnO	KMnO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
~	~	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Manganečnan	Manganečnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
3	#num#	k4
<g/>
MnO	MnO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manganan	Manganan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
MnO	MnO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manganistan	manganistan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KMnO	KMnO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Technecistan	Technecistan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KTcO	KTcO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Rhenistan	Rhenistan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KReO	KReO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chromitan	chromitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
•	•	k?
Chroman	chroman	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
CrO	CrO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dichroman	Dichroman	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Molybdenan	molybdenan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
MoO	MoO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wolframan	wolframan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
WO	WO	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Metavanadičnan	Metavanadičnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
KVO	KVO	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Orthovanadičnan	Orthovanadičnan	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
3	#num#	k4
<g/>
VO	VO	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trititaničitan	Trititaničitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zirkoničitan	Zirkoničitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
ZrO	ZrO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Berylnatan	Berylnatan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g/>
BeO	BeO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Soli	sůl	k1gFnPc1
tvořené	tvořený	k2eAgFnPc1d1
záměnou	záměna	k1gFnSc7
vodíkuze	vodíkuze	k1gFnSc2
sloučenin	sloučenina	k1gFnPc2
typu	typ	k1gInSc2
prvekx	prvekx	k1gInSc4
–	–	k?
vodíky	vodík	k1gInPc4
</s>
<s>
Hydrid	hydrid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KH	kh	k0
<g/>
)	)	kIx)
•	•	k?
Hydroxid	hydroxid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KOH	KOH	kA
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
•	•	k?
Peroxid	peroxid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Superoxid	superoxid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KO	KO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Ozonid	ozonid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KO	KO	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hydrogensulfid	Hydrogensulfid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KSH	KSH	kA
<g/>
)	)	kIx)
•	•	k?
Sulfid	sulfid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
•	•	k?
Selenid	Selenid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
Se	s	k7c7
<g/>
)	)	kIx)
•	•	k?
Tellurid	tellurid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	k	k7c3
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Te	Te	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Amid	amid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KNH	KNH	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Imid	imid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
2	#num#	k4
<g/>
NH	NH	kA
<g/>
)	)	kIx)
•	•	k?
Nitrid	nitrid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
N	N	kA
<g/>
)	)	kIx)
•	•	k?
Azid	azid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KN	KN	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fosfid	Fosfid	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
K	K	kA
<g/>
3	#num#	k4
<g/>
P	P	kA
<g/>
)	)	kIx)
•	•	k?
Tetrahydridoboritan	Tetrahydridoboritan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KBH	KBH	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tetrahydridohlinitan	Tetrahydridohlinitan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
(	(	kIx(
<g/>
KAlH	KAlH	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
|	|	kIx~
Fotografie	fotografie	k1gFnSc1
</s>
