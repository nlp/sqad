<s>
István	István	k2eAgInSc1d1
Bartakovics	Bartakovics	k1gInSc1
</s>
<s>
Ing.	ing.	kA
István	István	k2eAgInSc4d1
Bartakovics	Bartakovics	k1gInSc4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1990	#num#	k4
–	–	k?
1992	#num#	k4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
(	(	kIx(
<g/>
SL	SL	kA
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1992	#num#	k4
–	–	k?
1992	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
MKDH	MKDH	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1939	#num#	k4
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
István	István	k2eAgInSc1d1
Bartakovics	Bartakovics	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Štefan	Štefan	k1gMnSc1
Bartakovics	Bartakovics	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1939	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
slovenský	slovenský	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
politik	politik	k1gMnSc1
maďarské	maďarský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
poslanec	poslanec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
Sněmovny	sněmovna	k1gFnSc2
lidu	lid	k1gInSc2
za	za	k7c4
hnutí	hnutí	k1gNnSc4
Együttélés	Együttélésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c2
MKDH	MKDH	kA
do	do	k7c2
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1992	#num#	k4
zasedl	zasednout	k5eAaPmAgMnS
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
lidu	lid	k1gInSc2
za	za	k7c4
Maďarské	maďarský	k2eAgNnSc4d1
křesťanskodemokratické	křesťanskodemokratický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
kandidovalo	kandidovat	k5eAaImAgNnS
společně	společně	k6eAd1
s	s	k7c7
hnutím	hnutí	k1gNnSc7
Együttélés	Együttélésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Federálním	federální	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
setrval	setrvat	k5eAaPmAgMnS
do	do	k7c2
zániku	zánik	k1gInSc2
Československa	Československo	k1gNnSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
István	István	k2eAgInSc4d1
Bartakovics	Bartakovics	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1992	#num#	k4
<g/>
.	.	kIx.
június	június	k1gInSc1
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
foruminst	foruminst	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
István	István	k2eAgInSc1d1
Bartakovics	Bartakovics	k1gInSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
1778153775043961550001	#num#	k4
</s>
