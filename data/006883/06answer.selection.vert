<s>
Kefalonia	Kefalonium	k1gNnPc1	Kefalonium
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Jónském	jónský	k2eAgNnSc6d1	Jónské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
šestý	šestý	k4xOgInSc1	šestý
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
patřící	patřící	k2eAgInSc1d1	patřící
Řecku	Řecko	k1gNnSc3	Řecko
<g/>
.	.	kIx.	.
</s>
