<s>
Kefalonia	Kefalonium	k1gNnPc1	Kefalonium
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Jónském	jónský	k2eAgNnSc6d1	Jónské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
šestý	šestý	k4xOgInSc1	šestý
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
patřící	patřící	k2eAgInSc1d1	patřící
Řecku	Řecko	k1gNnSc3	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
780	[number]	k4	780
km2	km2	k4	km2
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zdroji	zdroj	k1gInSc6	zdroj
730	[number]	k4	730
km2	km2	k4	km2
až	až	k9	až
800	[number]	k4	800
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hora	hora	k1gFnSc1	hora
Ainos	Ainosa	k1gFnPc2	Ainosa
(	(	kIx(	(
<g/>
1628	[number]	k4	1628
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
fíky	fík	k1gInPc1	fík
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
rybářství	rybářství	k1gNnSc4	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
regionální	regionální	k2eAgFnSc7d1	regionální
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Jónské	jónský	k2eAgInPc4d1	jónský
ostrovy	ostrov	k1gInPc4	ostrov
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Argostolion	Argostolion	k1gInSc1	Argostolion
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
regionální	regionální	k2eAgFnSc3d1	regionální
jednotce	jednotka	k1gFnSc3	jednotka
náleží	náležet	k5eAaImIp3nS	náležet
také	také	k9	také
několik	několik	k4yIc4	několik
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
regionální	regionální	k2eAgFnSc2d1	regionální
jednotky	jednotka	k1gFnSc2	jednotka
je	být	k5eAaImIp3nS	být
904	[number]	k4	904
km2	km2	k4	km2
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
42	[number]	k4	42
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
italských	italský	k2eAgMnPc2d1	italský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
po	po	k7c6	po
italské	italský	k2eAgFnSc6d1	italská
kapitulaci	kapitulace	k1gFnSc6	kapitulace
po	po	k7c6	po
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Popravu	poprava	k1gFnSc4	poprava
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
masakrů	masakr	k1gInPc2	masakr
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kefalonia	Kefalonium	k1gNnSc2	Kefalonium
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kefalonia	Kefalonium	k1gNnSc2	Kefalonium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
