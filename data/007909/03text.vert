<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Moderně	moderně	k6eAd1	moderně
je	být	k5eAaImIp3nS	být
vykládáno	vykládat	k5eAaImNgNnS	vykládat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
"	"	kIx"	"
<g/>
mírumilovný	mírumilovný	k2eAgInSc4d1	mírumilovný
<g/>
,	,	kIx,	,
proslavený	proslavený	k2eAgInSc4d1	proslavený
mírem	mír	k1gInSc7	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
"	"	kIx"	"
<g/>
oslavovatel	oslavovatel	k1gMnSc1	oslavovatel
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
ze	z	k7c2	z
staročeského	staročeský	k2eAgInSc2d1	staročeský
slav	slavit	k5eAaImRp2nS	slavit
a	a	k8xC	a
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staročeštině	staročeština	k1gFnSc6	staročeština
slovo	slovo	k1gNnSc1	slovo
znamenalo	znamenat	k5eAaImAgNnS	znamenat
svět	svět	k1gInSc4	svět
i	i	k8xC	i
absenci	absence	k1gFnSc4	absence
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
češtině	čeština	k1gFnSc6	čeština
pouze	pouze	k6eAd1	pouze
absenci	absence	k1gFnSc4	absence
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
domáckých	domácký	k2eAgFnPc2d1	domácká
a	a	k8xC	a
zdrobnělých	zdrobnělý	k2eAgFnPc2d1	zdrobnělá
podob	podoba	k1gFnPc2	podoba
<g/>
:	:	kIx,	:
Míra	Míra	k1gFnSc1	Míra
<g/>
,	,	kIx,	,
Mirda	Mirda	k1gMnSc1	Mirda
<g/>
,	,	kIx,	,
Mireček	Mireček	k1gMnSc1	Mireček
<g/>
.	.	kIx.	.
</s>
<s>
Varianta	varianta	k1gFnSc1	varianta
Mirek	Mirka	k1gFnPc2	Mirka
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
posuzována	posuzovat	k5eAaImNgFnS	posuzovat
jako	jako	k9	jako
domácká	domácký	k2eAgFnSc1d1	domácká
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
je	být	k5eAaImIp3nS	být
matrikami	matrika	k1gFnPc7	matrika
zapsána	zapsat	k5eAaPmNgNnP	zapsat
přibližně	přibližně	k6eAd1	přibližně
desítce	desítka	k1gFnSc6	desítka
novorozenců	novorozenec	k1gMnPc2	novorozenec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
emancipace	emancipace	k1gFnSc1	emancipace
varianty	varianta	k1gFnSc2	varianta
Mirek	Mirka	k1gFnPc2	Mirka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k6eAd1	již
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jmen	jméno	k1gNnPc2	jméno
Radoslav	Radoslav	k1gMnSc1	Radoslav
a	a	k8xC	a
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
protějšek	protějšek	k1gInSc1	protějšek
je	být	k5eAaImIp3nS	být
Miroslava	Miroslav	k1gMnSc4	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
jména	jméno	k1gNnPc1	jméno
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
<g/>
,	,	kIx,	,
Irenej	Irenej	k1gMnSc1	Irenej
<g/>
,	,	kIx,	,
Ireneus	Ireneus	k1gMnSc1	Ireneus
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
,	,	kIx,	,
Slavomír	Slavomír	k1gMnSc1	Slavomír
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
<g/>
,	,	kIx,	,
Míra	Míra	k1gMnSc1	Míra
<g/>
,	,	kIx,	,
Mirda	Mirda	k1gMnSc1	Mirda
<g/>
,	,	kIx,	,
Mireček	Mireček	k1gMnSc1	Mireček
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
různých	různý	k2eAgInPc2d1	různý
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
−	−	k?	−
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
30	[number]	k4	30
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
–	–	k?	–
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
,	,	kIx,	,
slovinština	slovinština	k1gFnSc1	slovinština
<g/>
,	,	kIx,	,
srbocharvátština	srbocharvátština	k1gFnSc1	srbocharvátština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
Mirosław	Mirosław	k1gFnSc1	Mirosław
–	–	k?	–
polština	polština	k1gFnSc1	polština
Miroslaus	Miroslaus	k1gInSc1	Miroslaus
–	–	k?	–
latina	latina	k1gFnSc1	latina
Miroslavo	Miroslava	k1gFnSc5	Miroslava
–	–	k?	–
španělština	španělština	k1gFnSc1	španělština
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
–	–	k?	–
populární	populární	k2eAgMnSc1d1	populární
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Holeňák	Holeňák	k1gMnSc1	Holeňák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Homola	Homola	k1gMnSc1	Homola
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
–	–	k?	–
český	český	k2eAgInSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
teoretik	teoretik	k1gMnSc1	teoretik
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
Miroslav	Miroslav	k1gMnSc1	Miroslav
Klose	Klose	k1gFnSc2	Klose
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Krejčí	Krejčí	k1gMnSc1	Krejčí
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macháček	Macháček	k1gMnSc1	Macháček
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Matušovič	Matušovič	k1gMnSc1	Matušovič
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Plzák	Plzák	k1gMnSc1	Plzák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sládek	Sládek	k1gMnSc1	Sládek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
krajně	krajně	k6eAd1	krajně
pravicový	pravicový	k2eAgInSc1d1	pravicový
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
Miroslav	Miroslav	k1gMnSc1	Miroslav
Slepička	Slepička	k1gMnSc1	Slepička
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šatan	Šatan	k1gMnSc1	Šatan
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejista	hokejista	k1gMnSc1	hokejista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šmajda	Šmajda	k?	Šmajda
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
finalista	finalista	k1gMnSc1	finalista
ČeskoSlovenské	československý	k2eAgFnSc2d1	Československá
Superstar	superstar	k1gFnSc2	superstar
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šuta	šuta	k1gFnSc1	šuta
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyrš	Tyrš	k1gMnSc1	Tyrš
–	–	k?	–
český	český	k2eAgMnSc1d1	český
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vitouš	Vitouš	k1gMnSc1	Vitouš
–	–	k?	–
český	český	k2eAgMnSc1d1	český
jazzový	jazzový	k2eAgMnSc1d1	jazzový
hudebník	hudebník	k1gMnSc1	hudebník
Miroslav	Miroslav	k1gMnSc1	Miroslav
Wanek	Wanek	k1gMnSc1	Wanek
–	–	k?	–
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zikmund	Zikmund	k1gMnSc1	Zikmund
–	–	k?	–
český	český	k2eAgMnSc1d1	český
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Miroslav	Miroslav	k1gMnSc1	Miroslav
Žbirka	Žbirka	k1gMnSc1	Žbirka
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
zpěvák	zpěvák	k1gMnSc1	zpěvák
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Miroslav	Miroslav	k1gMnSc1	Miroslav
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
