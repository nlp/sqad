<p>
<s>
Trubadúři	trubadúr	k1gMnPc1	trubadúr
neboli	neboli	k8xC	neboli
trobadoři	trobador	k1gMnPc1	trobador
byli	být	k5eAaImAgMnP	být
básníci	básník	k1gMnPc1	básník
<g/>
,	,	kIx,	,
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
muzikanti	muzikant	k1gMnPc1	muzikant
a	a	k8xC	a
zpěváci	zpěvák	k1gMnPc1	zpěvák
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
zejména	zejména	k9	zejména
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
–	–	k?	–
Okcitánii	Okcitánie	k1gFnSc6	Okcitánie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
řečí	řeč	k1gFnSc7	řeč
byla	být	k5eAaImAgFnS	být
okcitánština	okcitánština	k1gFnSc1	okcitánština
<g/>
,	,	kIx,	,
románský	románský	k2eAgInSc1d1	románský
jazyk	jazyk	k1gInSc1	jazyk
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
francouzštině	francouzština	k1gFnSc3	francouzština
a	a	k8xC	a
katalánštině	katalánština	k1gFnSc3	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
okcitánštiny	okcitánština	k1gFnSc2	okcitánština
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
pojmenování	pojmenování	k1gNnSc1	pojmenování
trobadorů	trobador	k1gMnPc2	trobador
<g/>
:	:	kIx,	:
Trobar	Trobar	k1gMnSc1	Trobar
v	v	k7c6	v
okcitánštině	okcitánština	k1gFnSc6	okcitánština
znamená	znamenat	k5eAaImIp3nS	znamenat
tvořit	tvořit	k5eAaImF	tvořit
a	a	k8xC	a
trobador	trobador	k1gMnSc1	trobador
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
trobador	trobador	k1gMnSc1	trobador
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
dílo	dílo	k1gNnSc4	dílo
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Vilém	Vilém	k1gMnSc1	Vilém
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ovšem	ovšem	k9	ovšem
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
znaky	znak	k1gInPc4	znak
vyspělosti	vyspělost	k1gFnSc2	vyspělost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavěl	stavět	k5eAaImAgMnS	stavět
už	už	k6eAd1	už
na	na	k7c6	na
předchozí	předchozí	k2eAgFnSc6d1	předchozí
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Styl	styl	k1gInSc4	styl
trubadúrů	trubadúr	k1gMnPc2	trubadúr
přebíraly	přebírat	k5eAaImAgFnP	přebírat
i	i	k9	i
okolní	okolní	k2eAgFnPc1d1	okolní
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
truvéry	truvér	k1gMnPc4	truvér
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
i	i	k9	i
pro	pro	k7c4	pro
německé	německý	k2eAgMnPc4d1	německý
minnesängry	minnesänger	k1gMnPc4	minnesänger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trubadúr	trubadúr	k1gMnSc1	trubadúr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
trubadúr	trubadúr	k1gMnSc1	trubadúr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
