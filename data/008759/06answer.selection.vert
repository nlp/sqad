<s>
Trubadúři	trubadúr	k1gMnPc1	trubadúr
neboli	neboli	k8xC	neboli
trobadoři	trobador	k1gMnPc1	trobador
byli	být	k5eAaImAgMnP	být
básníci	básník	k1gMnPc1	básník
<g/>
,	,	kIx,	,
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
muzikanti	muzikant	k1gMnPc1	muzikant
a	a	k8xC	a
zpěváci	zpěvák	k1gMnPc1	zpěvák
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
zejména	zejména	k9	zejména
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
–	–	k?	–
Okcitánii	Okcitánie	k1gFnSc6	Okcitánie
<g/>
.	.	kIx.	.
</s>
