<p>
<s>
Microdigital	Microdigital	k1gMnSc1	Microdigital
TK	TK	kA	TK
95	[number]	k4	95
je	být	k5eAaImIp3nS	být
brazilská	brazilský	k2eAgFnSc1d1	brazilská
verze	verze	k1gFnSc1	verze
počítače	počítač	k1gInSc2	počítač
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
společností	společnost	k1gFnPc2	společnost
Microdigital	Microdigital	k1gMnSc1	Microdigital
Eletrônica	Eletrônica	k1gMnSc1	Eletrônica
Ltda	Ltda	k1gMnSc1	Ltda
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
následník	následník	k1gMnSc1	následník
počítače	počítač	k1gInSc2	počítač
TK	TK	kA	TK
90	[number]	k4	90
<g/>
X	X	kA	X
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
počítači	počítač	k1gInSc3	počítač
Commodore	Commodor	k1gInSc5	Commodor
Plus	plus	k1gInSc1	plus
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Počítač	počítač	k1gInSc1	počítač
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
ROM	ROM	kA	ROM
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
provedené	provedený	k2eAgFnPc4d1	provedená
úpravy	úprava	k1gFnPc4	úprava
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
.	.	kIx.	.
</s>
<s>
Přidané	přidaný	k2eAgInPc1d1	přidaný
příkazy	příkaz	k1gInPc1	příkaz
TRACE	TRACE	kA	TRACE
a	a	k8xC	a
UDG	UDG	kA	UDG
ovšem	ovšem	k9	ovšem
zůstaly	zůstat	k5eAaPmAgInP	zůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
periférie	periférie	k1gFnPc1	periférie
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
pro	pro	k7c4	pro
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
byly	být	k5eAaImAgInP	být
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
s	s	k7c7	s
TK	TK	kA	TK
95	[number]	k4	95
<g/>
,	,	kIx,	,
Microdigital	Microdigital	k1gMnSc1	Microdigital
Eletrônica	Eletrônic	k1gInSc2	Eletrônic
však	však	k9	však
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
periférií	periférie	k1gFnPc2	periférie
<g/>
:	:	kIx,	:
světelné	světelný	k2eAgNnSc1d1	světelné
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
interface	interface	k1gInSc1	interface
RS-232	RS-232	k1gFnSc2	RS-232
a	a	k8xC	a
modemy	modem	k1gInPc4	modem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počítač	počítač	k1gInSc1	počítač
nebyl	být	k5eNaImAgInS	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
příliš	příliš	k6eAd1	příliš
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnPc2	jeho
uvedení	uvedení	k1gNnPc2	uvedení
se	se	k3xPyFc4	se
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
prosazovaly	prosazovat	k5eAaImAgInP	prosazovat
počítače	počítač	k1gInPc1	počítač
MSX	MSX	kA	MSX
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgFnPc1d1	technická
informace	informace	k1gFnPc1	informace
==	==	k?	==
</s>
</p>
<p>
<s>
procesor	procesor	k1gInSc1	procesor
<g/>
:	:	kIx,	:
Z	z	k7c2	z
<g/>
80	[number]	k4	80
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
3,58	[number]	k4	3,58
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
paměť	paměť	k1gFnSc1	paměť
RAM	RAM	kA	RAM
<g/>
:	:	kIx,	:
48	[number]	k4	48
KiB	KiB	k1gFnPc2	KiB
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
paměť	paměť	k1gFnSc1	paměť
ROM	ROM	kA	ROM
<g/>
:	:	kIx,	:
16	[number]	k4	16
KiB	KiB	k1gFnPc2	KiB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Microdigital	Microdigital	k1gMnSc1	Microdigital
TK	TK	kA	TK
95	[number]	k4	95
na	na	k7c4	na
Sinclair	Sinclair	k1gInSc4	Sinclair
Nostalgia	Nostalgium	k1gNnSc2	Nostalgium
Products	Productsa	k1gFnPc2	Productsa
</s>
</p>
