<s>
Optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
přenosových	přenosový	k2eAgFnPc6d1	přenosová
rychlostech	rychlost	k1gFnPc6	rychlost
dat	datum	k1gNnPc2	datum
než	než	k8xS	než
jiné	jiný	k2eAgFnSc2d1	jiná
formy	forma	k1gFnSc2	forma
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
