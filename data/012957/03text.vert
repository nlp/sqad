<p>
<s>
Optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
skleněné	skleněný	k2eAgNnSc1d1	skleněné
nebo	nebo	k8xC	nebo
plastové	plastový	k2eAgNnSc1d1	plastové
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
světla	světlo	k1gNnSc2	světlo
přenáší	přenášet	k5eAaImIp3nS	přenášet
signály	signál	k1gInPc4	signál
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
své	svůj	k3xOyFgFnSc2	svůj
podélné	podélný	k2eAgFnSc2d1	podélná
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
aplikace	aplikace	k1gFnSc2	aplikace
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
v	v	k7c6	v
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
přenosových	přenosový	k2eAgFnPc6d1	přenosová
rychlostech	rychlost	k1gFnPc6	rychlost
dat	datum	k1gNnPc2	datum
než	než	k8xS	než
jiné	jiný	k2eAgFnSc2d1	jiná
formy	forma	k1gFnSc2	forma
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
místo	místo	k7c2	místo
kovových	kovový	k2eAgInPc2d1	kovový
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
signály	signál	k1gInPc1	signál
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
ztrátou	ztráta	k1gFnSc7	ztráta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
vlákna	vlákno	k1gNnPc1	vlákno
imunní	imunní	k2eAgNnPc1d1	imunní
vůči	vůči	k7c3	vůči
elektromagnetickému	elektromagnetický	k2eAgNnSc3d1	elektromagnetické
rušení	rušení	k1gNnSc3	rušení
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
pro	pro	k7c4	pro
osvětlení	osvětlení	k1gNnSc4	osvětlení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
balena	balen	k2eAgFnSc1d1	balena
ve	v	k7c6	v
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zobrazení	zobrazení	k1gNnSc4	zobrazení
v	v	k7c6	v
těsných	těsný	k2eAgFnPc6d1	těsná
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
konstruovaná	konstruovaný	k2eAgNnPc1d1	konstruované
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
snímače	snímač	k1gInSc2	snímač
a	a	k8xC	a
vláknového	vláknový	k2eAgInSc2d1	vláknový
laseru	laser	k1gInSc2	laser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
==	==	k?	==
</s>
</p>
<p>
<s>
Optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použita	použit	k2eAgNnPc1d1	použito
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
ohebná	ohebný	k2eAgNnPc1d1	ohebné
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
svázána	svázat	k5eAaPmNgFnS	svázat
do	do	k7c2	do
svazků	svazek	k1gInPc2	svazek
jako	jako	k8xC	jako
kabely	kabela	k1gFnSc2	kabela
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
výhodná	výhodný	k2eAgFnSc1d1	výhodná
zejména	zejména	k9	zejména
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
světlo	světlo	k1gNnSc1	světlo
prochází	procházet	k5eAaImIp3nS	procházet
přes	přes	k7c4	přes
vlákno	vlákno	k1gNnSc4	vlákno
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
útlumem	útlum	k1gInSc7	útlum
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
elektrickými	elektrický	k2eAgInPc7d1	elektrický
kabely	kabel	k1gInPc7	kabel
s	s	k7c7	s
kovovými	kovový	k2eAgMnPc7d1	kovový
vodiči	vodič	k1gMnPc7	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
můžeme	moct	k5eAaImIp1nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
přenosu	přenos	k1gInSc2	přenos
desítky	desítka	k1gFnSc2	desítka
Terabitů	Terabit	k1gInPc2	Terabit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
aplikovaných	aplikovaný	k2eAgInPc6d1	aplikovaný
systémech	systém	k1gInPc6	systém
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
rychlosti	rychlost	k1gFnPc1	rychlost
10	[number]	k4	10
nebo	nebo	k8xC	nebo
40	[number]	k4	40
Gb	Gb	k1gFnPc2	Gb
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Každé	každý	k3xTgNnSc1	každý
vlákno	vlákno	k1gNnSc1	vlákno
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
mnoho	mnoho	k4c4	mnoho
nezávislých	závislý	k2eNgInPc2d1	nezávislý
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
jiné	jiný	k2eAgFnSc2d1	jiná
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Vytváření	vytváření	k1gNnSc1	vytváření
sítí	síť	k1gFnPc2	síť
na	na	k7c4	na
krátké	krátký	k2eAgFnPc4d1	krátká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
pomocí	pomocí	k7c2	pomocí
optických	optický	k2eAgInPc2d1	optický
kabelů	kabel	k1gInPc2	kabel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
šetří	šetřit	k5eAaImIp3nS	šetřit
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
kabelovém	kabelový	k2eAgNnSc6d1	kabelové
vedení	vedení	k1gNnSc6	vedení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jediné	jediný	k2eAgNnSc1d1	jediné
vlákno	vlákno	k1gNnSc1	vlákno
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
dat	datum	k1gNnPc2	datum
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
elektrický	elektrický	k2eAgInSc4d1	elektrický
kabel	kabel	k1gInSc4	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
imunní	imunní	k2eAgFnSc1d1	imunní
vůči	vůči	k7c3	vůči
elektrickému	elektrický	k2eAgNnSc3d1	elektrické
rušení	rušení	k1gNnSc3	rušení
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgInPc1d1	optický
kabely	kabel	k1gInPc1	kabel
nejsou	být	k5eNaImIp3nP	být
elektricky	elektricky	k6eAd1	elektricky
vodivé	vodivý	k2eAgFnPc1d1	vodivá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
řešení	řešení	k1gNnSc1	řešení
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
komunikačních	komunikační	k2eAgNnPc2d1	komunikační
zařízení	zařízení	k1gNnPc2	zařízení
umístěných	umístěný	k2eAgNnPc2d1	umístěné
na	na	k7c6	na
přenosové	přenosový	k2eAgFnSc6d1	přenosová
soustavě	soustava	k1gFnSc6	soustava
vysokého	vysoký	k2eAgNnSc2d1	vysoké
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
kovových	kovový	k2eAgFnPc6d1	kovová
konstrukcích	konstrukce	k1gFnPc6	konstrukce
náchylných	náchylný	k2eAgInPc2d1	náchylný
na	na	k7c4	na
úder	úder	k1gInSc4	úder
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
výbušné	výbušný	k2eAgInPc1d1	výbušný
výpary	výpar	k1gInPc1	výpar
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
vznícení	vznícení	k1gNnSc2	vznícení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
vlákna	vlákno	k1gNnPc1	vlákno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
průhledného	průhledný	k2eAgInSc2d1	průhledný
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kombinace	kombinace	k1gFnPc1	kombinace
obou	dva	k4xCgFnPc2	dva
<g/>
,	,	kIx,	,
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
u	u	k7c2	u
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
aplikací	aplikace	k1gFnPc2	aplikace
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
použita	použit	k2eAgNnPc1d1	použito
vlákna	vlákno	k1gNnPc1	vlákno
skleněná	skleněný	k2eAgNnPc1d1	skleněné
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nižších	nízký	k2eAgInPc2d2	nižší
optických	optický	k2eAgInPc2d1	optický
útlumů	útlum	k1gInPc2	útlum
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
mnohavidová	mnohavidový	k2eAgFnSc1d1	mnohavidová
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jednovidová	jednovidový	k2eAgNnPc1d1	jednovidové
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohavidové	mnohavidový	k2eAgNnSc1d1	mnohavidový
vlákno	vlákno	k1gNnSc1	vlákno
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
do	do	k7c2	do
550	[number]	k4	550
m	m	kA	m
(	(	kIx(	(
<g/>
600	[number]	k4	600
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednovidové	jednovidový	k2eAgNnSc1d1	jednovidové
vlákno	vlákno	k1gNnSc1	vlákno
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnPc4d2	delší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
funkce	funkce	k1gFnSc2	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
válečkový	válečkový	k2eAgInSc4d1	válečkový
dielektrický	dielektrický	k2eAgInSc4d1	dielektrický
vlnovod	vlnovod	k1gInSc4	vlnovod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
světlo	světlo	k1gNnSc1	světlo
či	či	k8xC	či
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
osy	osa	k1gFnSc2	osa
vlákna	vlákno	k1gNnSc2	vlákno
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
principu	princip	k1gInSc2	princip
totálního	totální	k2eAgInSc2d1	totální
odrazu	odraz	k1gInSc2	odraz
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
prostředí	prostředí	k1gNnSc2	prostředí
s	s	k7c7	s
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
vlákna	vlákno	k1gNnSc2	vlákno
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
primární	primární	k2eAgFnSc1d1	primární
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
optického	optický	k2eAgInSc2d1	optický
signálu	signál	k1gInSc2	signál
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
jádra	jádro	k1gNnSc2	jádro
vyšší	vysoký	k2eAgInPc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
používaných	používaný	k2eAgNnPc2d1	používané
v	v	k7c6	v
datových	datový	k2eAgFnPc6d1	datová
sítích	síť	k1gFnPc6	síť
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
průměr	průměr	k1gInSc1	průměr
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
pláště	plášť	k1gInSc2	plášť
v	v	k7c6	v
mikrometrech	mikrometr	k1gInPc6	mikrometr
<g/>
,	,	kIx,	,
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
mnohavidová	mnohavidový	k2eAgNnPc1d1	mnohavidový
vlákna	vlákno	k1gNnPc1	vlákno
(	(	kIx(	(
<g/>
MM	mm	kA	mm
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměrech	průměr	k1gInPc6	průměr
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
μ	μ	k?	μ
(	(	kIx(	(
<g/>
standardizováno	standardizován	k2eAgNnSc1d1	standardizováno
ITU-T	ITU-T	k1gFnPc4	ITU-T
podle	podle	k7c2	podle
G	G	kA	G
<g/>
.651	.651	k4	.651
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
62,5	[number]	k4	62,5
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
μ	μ	k?	μ
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
výhradně	výhradně	k6eAd1	výhradně
používají	používat	k5eAaImIp3nP	používat
jednovidová	jednovidový	k2eAgNnPc1d1	jednovidové
vlákna	vlákno	k1gNnPc1	vlákno
(	(	kIx(	(
<g/>
SM	SM	kA	SM
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
standardy	standard	k1gInPc4	standard
G	G	kA	G
<g/>
.652	.652	k4	.652
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
653	[number]	k4	653
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
.655	.655	k4	.655
a	a	k8xC	a
G.	G.	kA	G.
657	[number]	k4	657
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
===	===	k?	===
</s>
</p>
<p>
<s>
Index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
změnu	změna	k1gFnSc4	změna
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
prostředími	prostředí	k1gNnPc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nejrychleji	rychle	k6eAd3	rychle
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
vydělením	vydělení	k1gNnSc7	vydělení
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
rychlostí	rychlost	k1gFnPc2	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
hmotném	hmotný	k2eAgNnSc6d1	hmotné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
hodnota	hodnota	k1gFnSc1	hodnota
indexu	index	k1gInSc2	index
pláště	plášť	k1gInSc2	plášť
optického	optický	k2eAgNnSc2d1	optické
vlákna	vlákno	k1gNnSc2	vlákno
je	být	k5eAaImIp3nS	být
1,46	[number]	k4	1,46
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
hodnota	hodnota	k1gFnSc1	hodnota
pro	pro	k7c4	pro
jádro	jádro	k1gNnSc4	jádro
je	být	k5eAaImIp3nS	být
1,48	[number]	k4	1,48
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
pomaleji	pomale	k6eAd2	pomale
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
daném	daný	k2eAgNnSc6d1	dané
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Totální	totální	k2eAgInSc1d1	totální
odraz	odraz	k1gInSc1	odraz
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
(	(	kIx(	(
<g/>
těžko	těžko	k6eAd1	těžko
proniknutelném	proniknutelný	k2eAgNnSc6d1	proniknutelné
<g/>
)	)	kIx)	)
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
pod	pod	k7c7	pod
šikmým	šikmý	k2eAgInSc7d1	šikmý
úhlem	úhel	k1gInSc7	úhel
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
mezní	mezní	k2eAgInSc4d1	mezní
úhel	úhel	k1gInSc4	úhel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
bude	být	k5eAaImBp3nS	být
kompletně	kompletně	k6eAd1	kompletně
odraženo	odražen	k2eAgNnSc1d1	odraženo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
v	v	k7c6	v
optických	optický	k2eAgNnPc6d1	optické
vláknech	vlákno	k1gNnPc6	vlákno
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
kolem	kolem	k7c2	kolem
vlákna	vlákno	k1gNnSc2	vlákno
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
musí	muset	k5eAaImIp3nS	muset
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
odrazovou	odrazový	k2eAgFnSc4d1	odrazová
plochu	plocha	k1gFnSc4	plocha
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
kritický	kritický	k2eAgInSc4d1	kritický
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
vlákna	vlákno	k1gNnSc2	vlákno
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozsahu	rozsah	k1gInSc6	rozsah
úhlu	úhel	k1gInSc2	úhel
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
bez	bez	k7c2	bez
propuštění	propuštění	k1gNnSc2	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozsah	rozsah	k1gInSc1	rozsah
úhlů	úhel	k1gInPc2	úhel
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
vstupní	vstupní	k2eAgInSc1d1	vstupní
kužel	kužel	k1gInSc1	kužel
<g/>
"	"	kIx"	"
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgInSc2	tento
vstupního	vstupní	k2eAgInSc2d1	vstupní
kuželu	kužel	k1gInSc2	kužel
je	být	k5eAaImIp3nS	být
funkcí	funkce	k1gFnSc7	funkce
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
vlákna	vlákno	k1gNnSc2	vlákno
a	a	k8xC	a
obložením	obložení	k1gNnSc7	obložení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
vláken	vlákna	k1gFnPc2	vlákna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mnohavidové	mnohavidový	k2eAgNnSc1d1	mnohavidový
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
===	===	k?	===
</s>
</p>
<p>
<s>
Vícevidové	Vícevidový	k2eAgNnSc1d1	Vícevidový
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MM	mm	kA	mm
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
multimode	multimod	k1gMnSc5	multimod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
optického	optický	k2eAgNnSc2d1	optické
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
uvnitř	uvnitř	k7c2	uvnitř
budovy	budova	k1gFnSc2	budova
nebo	nebo	k8xC	nebo
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
přenosu	přenos	k1gInSc2	přenos
u	u	k7c2	u
vícevidových	vícevidový	k2eAgFnPc2d1	vícevidová
linek	linka	k1gFnPc2	linka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
Mbit	Mbitum	k1gNnPc2	Mbitum
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
až	až	k9	až
10	[number]	k4	10
Gbit	Gbitum	k1gNnPc2	Gbitum
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
na	na	k7c4	na
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
do	do	k7c2	do
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dostačující	dostačující	k2eAgInSc1d1	dostačující
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednovidové	jednovidový	k2eAgNnSc1d1	jednovidové
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
===	===	k?	===
</s>
</p>
<p>
<s>
Jednovidové	jednovidový	k2eAgNnSc1d1	jednovidové
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SM	SM	kA	SM
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
single	singl	k1gInSc5	singl
mode	modus	k1gInSc5	modus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
optického	optický	k2eAgNnSc2d1	optické
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
kontinenty	kontinent	k1gInPc1	kontinent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
našla	najít	k5eAaPmAgFnS	najít
optická	optický	k2eAgFnSc1d1	optická
vlákna	vlákna	k1gFnSc1	vlákna
uplatnění	uplatnění	k1gNnSc2	uplatnění
v	v	k7c6	v
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
a	a	k8xC	a
pro	pro	k7c4	pro
vysokorychlostní	vysokorychlostní	k2eAgInPc4d1	vysokorychlostní
přenosy	přenos	k1gInPc4	přenos
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
levnější	levný	k2eAgFnSc1d2	levnější
vícevidová	vícevidový	k2eAgFnSc1d1	vícevidová
nebo	nebo	k8xC	nebo
gradientní	gradientní	k2eAgFnSc1d1	gradientní
optická	optický	k2eAgFnSc1d1	optická
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlákna	vlákno	k1gNnPc4	vlákno
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
účely	účel	k1gInPc4	účel
===	===	k?	===
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
účely	účel	k1gInPc4	účel
jsou	být	k5eAaImIp3nP	být
konstruována	konstruován	k2eAgNnPc1d1	konstruováno
s	s	k7c7	s
ne	ne	k9	ne
válečkovitým	válečkovitý	k2eAgNnSc7d1	válečkovitý
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
nebo	nebo	k8xC	nebo
obkládací	obkládací	k2eAgFnSc7d1	obkládací
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
elipsovitým	elipsovitý	k2eAgInSc7d1	elipsovitý
nebo	nebo	k8xC	nebo
obdélníkovým	obdélníkový	k2eAgInSc7d1	obdélníkový
příčným	příčný	k2eAgInSc7d1	příčný
řezem	řez	k1gInSc7	řez
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
polarizaci	polarizace	k1gFnSc4	polarizace
určující	určující	k2eAgNnSc1d1	určující
vlákno	vlákno	k1gNnSc1	vlákno
a	a	k8xC	a
vlákno	vlákno	k1gNnSc1	vlákno
navržené	navržený	k2eAgNnSc1d1	navržené
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fotonické	Fotonický	k2eAgNnSc1d1	Fotonický
krystalové	krystalový	k2eAgNnSc1d1	krystalové
vlákno	vlákno	k1gNnSc1	vlákno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
normálním	normální	k2eAgInSc7d1	normální
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
zadržení	zadržení	k1gNnSc3	zadržení
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
vlákna	vlákno	k1gNnSc2	vlákno
efekt	efekt	k1gInSc1	efekt
ohybů	ohyb	k1gInPc2	ohyb
namísto	namísto	k7c2	namísto
totální	totální	k2eAgFnSc2d1	totální
reflexe	reflexe	k1gFnSc2	reflexe
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vláken	vlákna	k1gFnPc2	vlákna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přizpůsobeny	přizpůsoben	k2eAgInPc1d1	přizpůsoben
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
upraveny	upravit	k5eAaPmNgInP	upravit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
)	)	kIx)	)
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Útlum	útlum	k1gInSc4	útlum
===	===	k?	===
</s>
</p>
<p>
<s>
Útlum	útlum	k1gInSc1	útlum
je	být	k5eAaImIp3nS	být
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
rozdíl	rozdíl	k1gInSc4	rozdíl
síly	síla	k1gFnSc2	síla
signálu	signál	k1gInSc2	signál
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
vedení	vedení	k1gNnSc2	vedení
(	(	kIx(	(
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
kabelu	kabel	k1gInSc3	kabel
<g/>
)	)	kIx)	)
oproti	oproti	k7c3	oproti
druhému	druhý	k4xOgInSc3	druhý
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
nižší	nízký	k2eAgMnSc1d2	nižší
bude	být	k5eAaImBp3nS	být
útlum	útlum	k1gInSc1	útlum
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
a	a	k8xC	a
přesnější	přesný	k2eAgInPc1d2	přesnější
bude	být	k5eAaImBp3nS	být
přenos	přenos	k1gInSc1	přenos
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
útlumu	útlum	k1gInSc2	útlum
u	u	k7c2	u
křemenných	křemenný	k2eAgNnPc2d1	křemenné
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
řádově	řádově	k6eAd1	řádově
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
desetinách	desetina	k1gFnPc6	desetina
decibelu	decibel	k1gInSc2	decibel
na	na	k7c4	na
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Plastová	plastový	k2eAgNnPc1d1	plastové
vlákna	vlákno	k1gNnPc1	vlákno
mají	mít	k5eAaImIp3nP	mít
útlum	útlum	k1gInSc4	útlum
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
dB	db	kA	db
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Útlum	útlum	k1gInSc1	útlum
křemenných	křemenný	k2eAgNnPc2d1	křemenné
vláken	vlákno	k1gNnPc2	vlákno
je	být	k5eAaImIp3nS	být
složen	složen	k2eAgInSc1d1	složen
z	z	k7c2	z
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
materiálová	materiálový	k2eAgFnSc1d1	materiálová
absorpce	absorpce	k1gFnSc1	absorpce
–	–	k?	–
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
interakci	interakce	k1gFnSc4	interakce
fotonu	foton	k1gInSc2	foton
s	s	k7c7	s
atomární	atomární	k2eAgFnSc7d1	atomární
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
teplo	teplo	k6eAd1	teplo
</s>
</p>
<p>
<s>
vlastní	vlastní	k2eAgInSc4d1	vlastní
–	–	k?	–
v	v	k7c6	v
SiO	SiO	k1gFnSc6	SiO
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
ultrafialová	ultrafialový	k2eAgFnSc1d1	ultrafialová
–	–	k?	–
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
</s>
</p>
<p>
<s>
infračervená	infračervený	k2eAgFnSc1d1	infračervená
–	–	k?	–
roste	růst	k5eAaImIp3nS	růst
se	se	k3xPyFc4	se
zvětšující	zvětšující	k2eAgMnSc1d1	zvětšující
se	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
omezuje	omezovat	k5eAaImIp3nS	omezovat
použití	použití	k1gNnSc1	použití
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
pro	pro	k7c4	pro
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
nad	nad	k7c7	nad
1700	[number]	k4	1700
nm	nm	k?	nm
</s>
</p>
<p>
<s>
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
–	–	k?	–
na	na	k7c6	na
atomech	atom	k1gInPc6	atom
příměsí	příměs	k1gFnPc2	příměs
</s>
</p>
<p>
<s>
materiálový	materiálový	k2eAgInSc1d1	materiálový
rozptyl	rozptyl	k1gInSc1	rozptyl
</s>
</p>
<p>
<s>
lineární	lineární	k2eAgInSc4d1	lineární
–	–	k?	–
rozptýlené	rozptýlený	k2eAgNnSc1d1	rozptýlené
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
</s>
</p>
<p>
<s>
Rayleighův	Rayleighův	k2eAgInSc1d1	Rayleighův
rozptyl	rozptyl	k1gInSc1	rozptyl
–	–	k?	–
rozptyl	rozptyl	k1gInSc1	rozptyl
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
mikrozměnách	mikrozměna	k1gFnPc6	mikrozměna
ve	v	k7c6	v
vlákně	vlákno	k1gNnSc6	vlákno
menších	malý	k2eAgInPc2d2	menší
než	než	k8xS	než
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
;	;	kIx,	;
nepřímo	přímo	k6eNd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
4	[number]	k4	4
<g/>
.	.	kIx.	.
mocnině	mocnina	k1gFnSc3	mocnina
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
<g/>
;	;	kIx,	;
nelze	lze	k6eNd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
konstrukci	konstrukce	k1gFnSc4	konstrukce
zesilovačů	zesilovač	k1gInPc2	zesilovač
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Ramanovské	Ramanovský	k2eAgInPc4d1	Ramanovský
zesilovače	zesilovač	k1gInPc4	zesilovač
pro	pro	k7c4	pro
telekomunikace	telekomunikace	k1gFnPc4	telekomunikace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mieův	Mieův	k2eAgInSc1d1	Mieův
rozptyl	rozptyl	k1gInSc1	rozptyl
–	–	k?	–
způsoben	způsobit	k5eAaPmNgInS	způsobit
změnami	změna	k1gFnPc7	změna
srovnatelnými	srovnatelný	k2eAgFnPc7d1	srovnatelná
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
</s>
</p>
<p>
<s>
nelineární	lineární	k2eNgInSc1d1	nelineární
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
,	,	kIx,	,
rozptýlené	rozptýlený	k2eAgNnSc1d1	rozptýlené
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
</s>
</p>
<p>
<s>
Brillouinův	Brillouinův	k2eAgInSc1d1	Brillouinův
rozptyl	rozptyl	k1gInSc1	rozptyl
–	–	k?	–
interakce	interakce	k1gFnSc2	interakce
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
vlny	vlna	k1gFnSc2	vlna
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
akustickým	akustický	k2eAgInSc7d1	akustický
vlněním	vlnění	k1gNnSc7	vlnění
</s>
</p>
<p>
<s>
Ramanův	Ramanův	k2eAgInSc1d1	Ramanův
jev	jev	k1gInSc1	jev
–	–	k?	–
posun	posun	k1gInSc1	posun
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
</s>
</p>
<p>
<s>
ztráta	ztráta	k1gFnSc1	ztráta
ohybem	ohyb	k1gInSc7	ohyb
</s>
</p>
<p>
<s>
mikroskopický	mikroskopický	k2eAgInSc1d1	mikroskopický
ohyb	ohyb	k1gInSc1	ohyb
–	–	k?	–
deformace	deformace	k1gFnSc1	deformace
pláště	plášť	k1gInSc2	plášť
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
milimetru	milimetr	k1gInSc2	milimetr
</s>
</p>
<p>
<s>
makroskopický	makroskopický	k2eAgInSc1d1	makroskopický
ohyb	ohyb	k1gInSc1	ohyb
–	–	k?	–
projevují	projevovat	k5eAaImIp3nP	projevovat
se	s	k7c7	s
především	především	k9	především
útlumem	útlum	k1gInSc7	útlum
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
</s>
</p>
<p>
<s>
ztráty	ztráta	k1gFnPc1	ztráta
při	při	k7c6	při
spojování	spojování	k1gNnSc6	spojování
a	a	k8xC	a
na	na	k7c6	na
konektorech	konektor	k1gInPc6	konektor
</s>
</p>
<p>
<s>
===	===	k?	===
Okna	okno	k1gNnPc1	okno
===	===	k?	===
</s>
</p>
<p>
<s>
Útlumová	útlumový	k2eAgFnSc1d1	útlumová
charakteristika	charakteristika	k1gFnSc1	charakteristika
křemenných	křemenný	k2eAgNnPc2d1	křemenné
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
několik	několik	k4yIc1	několik
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
jsou	být	k5eAaImIp3nP	být
úseky	úsek	k1gInPc1	úsek
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
útlumem	útlum	k1gInSc7	útlum
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ITU-T	ITU-T	k1gFnSc2	ITU-T
jsou	být	k5eAaImIp3nP	být
definována	definovat	k5eAaBmNgFnS	definovat
tato	tento	k3xDgNnPc4	tento
okna	okno	k1gNnPc4	okno
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jednovidové	jednovidový	k2eAgNnSc4d1	jednovidové
vlákno	vlákno	k1gNnSc4	vlákno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
O	o	k7c6	o
(	(	kIx(	(
<g/>
Original	Original	k1gFnSc6	Original
<g/>
)	)	kIx)	)
1260	[number]	k4	1260
<g/>
–	–	k?	–
<g/>
1360	[number]	k4	1360
nm	nm	k?	nm
</s>
</p>
<p>
<s>
E	E	kA	E
(	(	kIx(	(
<g/>
Extended	Extended	k1gMnSc1	Extended
<g/>
)	)	kIx)	)
1360	[number]	k4	1360
<g/>
–	–	k?	–
<g/>
1460	[number]	k4	1460
nm	nm	k?	nm
</s>
</p>
<p>
<s>
S	s	k7c7	s
(	(	kIx(	(
<g/>
Short	Short	k1gInSc1	Short
wavelength	wavelength	k1gInSc1	wavelength
<g/>
)	)	kIx)	)
1460	[number]	k4	1460
<g/>
–	–	k?	–
<g/>
1530	[number]	k4	1530
nm	nm	k?	nm
</s>
</p>
<p>
<s>
C	C	kA	C
(	(	kIx(	(
<g/>
Conventional	Conventional	k1gMnSc1	Conventional
<g/>
)	)	kIx)	)
1530	[number]	k4	1530
<g/>
–	–	k?	–
<g/>
1565	[number]	k4	1565
nm	nm	k?	nm
</s>
</p>
<p>
<s>
L	L	kA	L
(	(	kIx(	(
<g/>
Long	Long	k1gMnSc1	Long
wavelength	wavelength	k1gMnSc1	wavelength
<g/>
)	)	kIx)	)
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1625	[number]	k4	1625
nm	nm	k?	nm
</s>
</p>
<p>
<s>
U	u	k7c2	u
(	(	kIx(	(
<g/>
Ultra	ultra	k2eAgInPc2d1	ultra
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
1625	[number]	k4	1625
nm	nm	k?	nm
</s>
</p>
<p>
<s>
===	===	k?	===
Disperze	disperze	k1gFnSc2	disperze
===	===	k?	===
</s>
</p>
<p>
<s>
Disperze	disperze	k1gFnSc1	disperze
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
zkreslení	zkreslení	k1gNnSc2	zkreslení
přenášeného	přenášený	k2eAgInSc2d1	přenášený
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zpožďování	zpožďování	k1gNnSc3	zpožďování
impulsů	impuls	k1gInPc2	impuls
a	a	k8xC	a
změně	změna	k1gFnSc6	změna
jejich	jejich	k3xOp3gInSc2	jejich
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
různým	různý	k2eAgInSc7d1	různý
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
disperze	disperze	k1gFnSc2	disperze
v	v	k7c6	v
optických	optický	k2eAgNnPc6d1	optické
vláknech	vlákno	k1gNnPc6	vlákno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
materiálová	materiálový	k2eAgFnSc1d1	materiálová
disperze	disperze	k1gFnSc1	disperze
</s>
</p>
<p>
<s>
vlnovodná	vlnovodný	k2eAgFnSc1d1	vlnovodný
disperze	disperze	k1gFnSc1	disperze
</s>
</p>
<p>
<s>
vidová	vidový	k2eAgFnSc1d1	vidová
disperze	disperze	k1gFnSc1	disperze
–	–	k?	–
disperze	disperze	k1gFnSc1	disperze
způsobená	způsobený	k2eAgFnSc1d1	způsobená
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
lomu	lom	k1gInSc6	lom
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nepatrné	nepatrný	k2eAgFnSc3d1	nepatrná
odchylce	odchylka	k1gFnSc3	odchylka
dráhy	dráha	k1gFnSc2	dráha
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Vidová	vidový	k2eAgFnSc1d1	vidová
disperze	disperze	k1gFnSc1	disperze
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
vidy	vid	k1gInPc7	vid
v	v	k7c6	v
mnohovidových	mnohovidový	k2eAgNnPc6d1	mnohovidové
vláknech	vlákno	k1gNnPc6	vlákno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
chromatická	chromatický	k2eAgFnSc1d1	chromatická
disperze	disperze	k1gFnSc1	disperze
–	–	k?	–
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Numerická	numerický	k2eAgFnSc1d1	numerická
apertura	apertura	k1gFnSc1	apertura
(	(	kIx(	(
<g/>
NA	na	k7c4	na
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
GI	GI	kA	GI
vláken	vlákna	k1gFnPc2	vlákna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
schopnost	schopnost	k1gFnSc4	schopnost
optického	optický	k2eAgNnSc2d1	optické
vlákna	vlákno	k1gNnSc2	vlákno
navázat	navázat	k5eAaPmF	navázat
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
jádra	jádro	k1gNnSc2	jádro
optický	optický	k2eAgInSc4d1	optický
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
NA	na	k7c6	na
roste	růst	k5eAaImIp3nS	růst
tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesnější	přesný	k2eAgFnSc1d2	přesnější
definice	definice	k1gFnSc1	definice
může	moct	k5eAaImIp3nS	moct
znít	znít	k5eAaImF	znít
<g/>
:	:	kIx,	:
sinus	sinus	k1gInSc1	sinus
polovičního	poloviční	k2eAgInSc2d1	poloviční
úhlu	úhel	k1gInSc2	úhel
dopadu	dopad	k1gInSc2	dopad
svazku	svazek	k1gInSc2	svazek
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ještě	ještě	k6eAd1	ještě
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgMnSc1d1	maximální
NA	na	k7c6	na
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nedosahovanou	dosahovaný	k2eNgFnSc7d1	dosahovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ztráty	ztráta	k1gFnSc2	ztráta
numerickou	numerický	k2eAgFnSc7d1	numerická
aperturou	apertura	k1gFnSc7	apertura
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
ztráty	ztráta	k1gFnSc2	ztráta
numerickou	numerický	k2eAgFnSc7d1	numerická
aperturou	apertura	k1gFnSc7	apertura
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
spojů	spoj	k1gInPc2	spoj
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
vlákny	vlákno	k1gNnPc7	vlákno
a	a	k8xC	a
mezi	mezi	k7c7	mezi
vláknem	vlákno	k1gNnSc7	vlákno
a	a	k8xC	a
zdrojem	zdroj	k1gInSc7	zdroj
světelného	světelný	k2eAgInSc2d1	světelný
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
vlákny	vlákno	k1gNnPc7	vlákno
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc1d2	nižší
NA	na	k7c4	na
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
maximální	maximální	k2eAgInSc1d1	maximální
úhel	úhel	k1gInSc1	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgInSc7	který
mohou	moct	k5eAaImIp3nP	moct
paprsky	paprsek	k1gInPc1	paprsek
na	na	k7c4	na
vlákno	vlákno	k1gNnSc4	vlákno
dopadat	dopadat	k5eAaImF	dopadat
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
vycházející	vycházející	k2eAgInPc1d1	vycházející
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
vlákna	vlákno	k1gNnSc2	vlákno
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
větším	veliký	k2eAgInSc7d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
NA	na	k7c4	na
přijímajícího	přijímající	k2eAgNnSc2d1	přijímající
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
přenesena	přenesen	k2eAgNnPc1d1	přeneseno
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gNnSc4	on
zahrnout	zahrnout	k5eAaPmF	zahrnout
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
vyzařovací	vyzařovací	k2eAgInSc1d1	vyzařovací
úhel	úhel	k1gInSc1	úhel
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
NA	na	k7c6	na
optického	optický	k2eAgNnSc2d1	optické
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
do	do	k7c2	do
přenosových	přenosový	k2eAgFnPc2d1	přenosová
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
použití	použití	k1gNnSc2	použití
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přenosu	přenos	k1gInSc2	přenos
signálu	signál	k1gInSc2	signál
mají	mít	k5eAaImIp3nP	mít
optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
následující	následující	k2eAgFnSc2d1	následující
výhody	výhoda	k1gFnSc2	výhoda
oproti	oproti	k7c3	oproti
metalickým	metalický	k2eAgInPc3d1	metalický
vodičům	vodič	k1gInPc3	vodič
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
velká	velký	k2eAgFnSc1d1	velká
šířka	šířka	k1gFnSc1	šířka
pásma	pásmo	k1gNnSc2	pásmo
</s>
</p>
<p>
<s>
nízký	nízký	k2eAgInSc1d1	nízký
útlum	útlum	k1gInSc1	útlum
(	(	kIx(	(
<g/>
delší	dlouhý	k2eAgInPc1d2	delší
opakovací	opakovací	k2eAgInPc1d1	opakovací
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
zesilovačů	zesilovač	k1gInPc2	zesilovač
na	na	k7c6	na
optické	optický	k2eAgFnSc6d1	optická
trase	trasa	k1gFnSc6	trasa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
elektromagnetické	elektromagnetický	k2eAgFnSc3d1	elektromagnetická
interferenci	interference	k1gFnSc3	interference
a	a	k8xC	a
přeslechům	přeslech	k1gInPc3	přeslech
</s>
</p>
<p>
<s>
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
signál	signál	k1gInSc1	signál
nelze	lze	k6eNd1	lze
jednoduše	jednoduše	k6eAd1	jednoduše
vyvázat	vyvázat	k5eAaPmF	vyvázat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
izolace	izolace	k1gFnSc1	izolace
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Materiály	materiál	k1gInPc4	materiál
===	===	k?	===
</s>
</p>
<p>
<s>
Skleněná	skleněný	k2eAgNnPc1d1	skleněné
optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
chalkogenní	chalkogenní	k2eAgMnPc1d1	chalkogenní
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
skla	sklo	k1gNnPc1	sklo
<g/>
,	,	kIx,	,
i	i	k8xC	i
tato	tento	k3xDgNnPc1	tento
skla	sklo	k1gNnPc1	sklo
mají	mít	k5eAaImIp3nP	mít
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
asi	asi	k9	asi
1,5	[number]	k4	1,5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plastová	plastový	k2eAgFnSc1d1	plastová
optická	optický	k2eAgFnSc1d1	optická
vlákna	vlákna	k1gFnSc1	vlákna
(	(	kIx(	(
<g/>
POF	POF	kA	POF
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
vlákna	vlákno	k1gNnPc1	vlákno
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
0,5	[number]	k4	0,5
mm	mm	kA	mm
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
POF	POF	kA	POF
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgInSc1d2	vyšší
útlum	útlum	k1gInSc1	útlum
než	než	k8xS	než
skleněná	skleněný	k2eAgNnPc1d1	skleněné
vlákna	vlákno	k1gNnPc1	vlákno
(	(	kIx(	(
<g/>
1	[number]	k4	1
dB	db	kA	db
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc7d2	vyšší
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
vysoký	vysoký	k2eAgInSc1d1	vysoký
útlum	útlum	k1gInSc1	útlum
omezuje	omezovat	k5eAaImIp3nS	omezovat
rozsah	rozsah	k1gInSc4	rozsah
pro	pro	k7c4	pro
systémy	systém	k1gInPc4	systém
založené	založený	k2eAgInPc4d1	založený
na	na	k7c4	na
POF	POF	kA	POF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Proces	proces	k1gInSc1	proces
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
standardních	standardní	k2eAgNnPc2d1	standardní
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
preforma	preforma	k1gFnSc1	preforma
(	(	kIx(	(
<g/>
skleněný	skleněný	k2eAgInSc1d1	skleněný
válec	válec	k1gInSc1	válec
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
několika	několik	k4yIc7	několik
cm	cm	kA	cm
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
až	až	k9	až
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
s	s	k7c7	s
pečlivě	pečlivě	k6eAd1	pečlivě
kontrolovaným	kontrolovaný	k2eAgInSc7d1	kontrolovaný
profilem	profil	k1gInSc7	profil
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Natavením	natavení	k1gNnSc7	natavení
a	a	k8xC	a
tažením	tažení	k1gNnSc7	tažení
konce	konec	k1gInSc2	konec
preformy	preforma	k1gFnSc2	preforma
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgNnSc1d1	tenké
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Preformy	Preforma	k1gFnPc1	Preforma
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
třemi	tři	k4xCgInPc7	tři
druhy	druh	k1gInPc7	druh
chemických	chemický	k2eAgInPc2d1	chemický
procesů	proces	k1gInPc2	proces
<g/>
:	:	kIx,	:
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
uložení	uložení	k1gNnSc4	uložení
páry	pára	k1gFnSc2	pára
(	(	kIx(	(
<g/>
MCVD	MCVD	kA	MCVD
-	-	kIx~	-
Modified	Modified	k1gMnSc1	Modified
Chemical	Chemical	k1gFnSc2	Chemical
vapor	vapor	k1gMnSc1	vapor
deposition	deposition	k1gInSc1	deposition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgNnSc1d1	vnější
uložení	uložení	k1gNnSc1	uložení
páry	pára	k1gFnSc2	pára
(	(	kIx(	(
<g/>
OVD	OVD	kA	OVD
<g/>
)	)	kIx)	)
a	a	k8xC	a
axiální	axiální	k2eAgNnSc1d1	axiální
uložení	uložení	k1gNnSc1	uložení
páry	pára	k1gFnSc2	pára
(	(	kIx(	(
<g/>
AVD	AVD	kA	AVD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výpočty	výpočet	k1gInPc1	výpočet
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
optickými	optický	k2eAgNnPc7d1	optické
vlákny	vlákno	k1gNnPc7	vlákno
==	==	k?	==
</s>
</p>
<p>
<s>
Optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
n	n	k0	n
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
pláště	plášť	k1gInPc4	plášť
</s>
</p>
<p>
<s>
n	n	k0	n
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
<s>
n	n	k0	n
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
paprsek	paprsek	k1gInSc1	paprsek
do	do	k7c2	do
materiálu	materiál	k1gInSc2	materiál
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
0	[number]	k4	0
=	=	kIx~	=
1	[number]	k4	1
pro	pro	k7c4	pro
vakuum	vakuum	k1gNnSc4	vakuum
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
pro	pro	k7c4	pro
vzduch	vzduch	k1gInSc4	vzduch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
α	α	k?	α
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
paprsek	paprsek	k1gInSc1	paprsek
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
materiálu	materiál	k1gInSc2	materiál
jádra	jádro	k1gNnSc2	jádro
–	–	k?	–
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
optické	optický	k2eAgFnSc3d1	optická
ose	osa	k1gFnSc3	osa
</s>
</p>
<p>
<s>
α	α	k?	α
<g/>
́	́	k?	́
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
paprsek	paprsek	k1gInSc1	paprsek
lomí	lomit	k5eAaImIp3nS	lomit
materiálem	materiál	k1gInSc7	materiál
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
<s>
β	β	k?	β
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
paprsek	paprsek	k1gInSc1	paprsek
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
optické	optický	k2eAgNnSc4d1	optické
rozhraní	rozhraní	k1gNnSc4	rozhraní
</s>
</p>
<p>
<s>
===	===	k?	===
Výpočet	výpočet	k1gInSc1	výpočet
numerické	numerický	k2eAgFnSc2d1	numerická
apertury	apertura	k1gFnSc2	apertura
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Numerické	numerický	k2eAgFnSc2d1	numerická
apertury	apertura	k1gFnSc2	apertura
–	–	k?	–
A	A	kA	A
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Numerické	numerický	k2eAgFnSc2d1	numerická
apertury	apertura	k1gFnSc2	apertura
v	v	k7c6	v
kolmém	kolmý	k2eAgInSc6d1	kolmý
směru	směr	k1gInSc6	směr
–	–	k?	–
A0	A0	k1gFnSc2	A0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-n_	_	k?	-n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Úhlu	úhel	k1gInSc2	úhel
lomeného	lomený	k2eAgInSc2d1	lomený
paprsku	paprsek	k1gInSc2	paprsek
–	–	k?	–
α	α	k?	α
<g/>
́	́	k?	́
<g/>
:	:	kIx,	:
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
Snellova	Snellův	k2eAgInSc2d1	Snellův
zákona	zákon	k1gInSc2	zákon
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
'	'	kIx"	'
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
arcsin	arcsin	k1gMnSc1	arcsin
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
arcsin	arcsin	k1gMnSc1	arcsin
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Úhlu	úhel	k1gInSc3	úhel
dopadu	dopad	k1gInSc2	dopad
paprsku	paprsek	k1gInSc2	paprsek
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
–	–	k?	–
β	β	k?	β
<g/>
:	:	kIx,	:
užijeme	užít	k5eAaPmIp1nP	užít
principu	princip	k1gInSc3	princip
totálního	totální	k2eAgInSc2d1	totální
odrazu	odraz	k1gInSc2	odraz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
90	[number]	k4	90
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
90	[number]	k4	90
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}}	}}	k?	}}
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
90	[number]	k4	90
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
90	[number]	k4	90
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
arcsin	arcsin	k1gMnSc1	arcsin
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
arcsin	arcsin	k1gMnSc1	arcsin
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výpočet	výpočet	k1gInSc1	výpočet
počtu	počet	k1gInSc2	počet
odrazů	odraz	k1gInPc2	odraz
paprsku	paprsek	k1gInSc2	paprsek
vláknem	vlákno	k1gNnSc7	vlákno
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
–	–	k?	–
počet	počet	k1gInSc1	počet
odrazů	odraz	k1gInPc2	odraz
paprsku	paprsek	k1gInSc2	paprsek
v	v	k7c6	v
optickém	optický	k2eAgNnSc6d1	optické
vlákně	vlákno	k1gNnSc6	vlákno
</s>
</p>
<p>
<s>
l	l	kA	l
–	–	k?	–
délka	délka	k1gFnSc1	délka
vlákna	vlákno	k1gNnSc2	vlákno
</s>
</p>
<p>
<s>
d	d	k?	d
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
<s>
s	s	k7c7	s
–	–	k?	–
délka	délka	k1gFnSc1	délka
paprsku	paprsek	k1gInSc2	paprsek
</s>
</p>
<p>
<s>
α	α	k?	α
–	–	k?	–
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
paprsek	paprsek	k1gInSc1	paprsek
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
materiálu	materiál	k1gInSc2	materiál
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
<s>
α	α	k?	α
<g/>
́	́	k?	́
–	–	k?	–
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
paprsek	paprsek	k1gInSc1	paprsek
lomí	lomit	k5eAaImIp3nS	lomit
materiálem	materiál	k1gInSc7	materiál
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
tg	tg	kA	tg
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
tg	tg	kA	tg
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
tg	tg	kA	tg
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výpočet	výpočet	k1gInSc1	výpočet
délky	délka	k1gFnSc2	délka
paprsku	paprsek	k1gInSc2	paprsek
ve	v	k7c6	v
vláknu	vlákno	k1gNnSc6	vlákno
===	===	k?	===
</s>
</p>
<p>
<s>
s	s	k7c7	s
–	–	k?	–
Délka	délka	k1gFnSc1	délka
paprsku	paprsek	k1gInSc2	paprsek
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
'	'	kIx"	'
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
l	l	kA	l
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Doleček	doleček	k1gInSc1	doleček
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Moderní	moderní	k2eAgFnSc1d1	moderní
učebnice	učebnice	k1gFnSc1	učebnice
elektroniky	elektronika	k1gFnSc2	elektronika
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
-	-	kIx~	-
Optoelektronika	optoelektronika	k1gFnSc1	optoelektronika
-	-	kIx~	-
optoelektronické	optoelektronický	k2eAgInPc1d1	optoelektronický
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
optická	optický	k2eAgNnPc1d1	optické
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
ISBN	ISBN	kA	ISBN
80-7300-184-5	[number]	k4	80-7300-184-5
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
optické	optický	k2eAgFnSc2d1	optická
vlákno	vlákno	k1gNnSc4	vlákno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jiráček	Jiráček	k1gMnSc1	Jiráček
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
optoelektroniky	optoelektronika	k1gFnSc2	optoelektronika
<g/>
,	,	kIx,	,
Ediční	ediční	k2eAgNnSc1d1	ediční
středisko	středisko	k1gNnSc1	středisko
RUP	rup	k0	rup
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Ďado	Ďado	k1gMnSc1	Ďado
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Kreidel	Kreidlo	k1gNnPc2	Kreidlo
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Senzory	senzor	k1gInPc1	senzor
a	a	k8xC	a
měřící	měřící	k2eAgInPc1d1	měřící
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Saleh	Saleh	k1gMnSc1	Saleh
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Teich	Teich	k1gMnSc1	Teich
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
fotoniky	fotonika	k1gFnSc2	fotonika
2	[number]	k4	2
<g/>
,	,	kIx,	,
Matfyzpress	Matfyzpress	k1gInSc1	Matfyzpress
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
www.comtel.cz	www.comtel.cz	k1gInSc1	www.comtel.cz
–	–	k?	–
předmět	předmět	k1gInSc4	předmět
Optické	optický	k2eAgFnSc2d1	optická
komunikace	komunikace	k1gFnSc2	komunikace
</s>
</p>
<p>
<s>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
příručka	příručka	k1gFnSc1	příručka
o	o	k7c6	o
optických	optický	k2eAgNnPc6d1	optické
vláknech	vlákno	k1gNnPc6	vlákno
(	(	kIx(	(
<g/>
pdf	pdf	k?	pdf
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.rp-photonics.com/fibers.html	[url]	k1gMnSc1	http://www.rp-photonics.com/fibers.html
</s>
</p>
<p>
<s>
https://web.archive.org/web/20190205063932/http://www.fiber-optics.info/	[url]	k4	https://web.archive.org/web/20190205063932/http://www.fiber-optics.info/
</s>
</p>
<p>
<s>
Oddělení	oddělení	k1gNnSc4	oddělení
technologie	technologie	k1gFnSc2	technologie
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
ÚFE	ÚFE	kA	ÚFE
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.v.i.	v.v.i.	k?	v.v.i.
</s>
</p>
