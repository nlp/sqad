<p>
<s>
Amorfní	amorfní	k2eAgFnPc1d1	amorfní
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
(	(	kIx(	(
<g/>
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
<g/>
)	)	kIx)	)
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
látkách	látka	k1gFnPc6	látka
náhodné	náhodný	k2eAgFnPc1d1	náhodná
<g/>
,	,	kIx,	,
určité	určitý	k2eAgFnPc1d1	určitá
zákonitosti	zákonitost	k1gFnPc1	zákonitost
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
polohách	poloha	k1gFnPc6	poloha
navzájem	navzájem	k6eAd1	navzájem
sousedících	sousedící	k2eAgInPc2d1	sousedící
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
amorfní	amorfní	k2eAgMnSc1d1	amorfní
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
beztvarý	beztvarý	k2eAgMnSc1d1	beztvarý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
beztvaré	beztvarý	k2eAgFnPc1d1	beztvará
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Amorfní	amorfní	k2eAgFnPc1d1	amorfní
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
izotropní	izotropní	k2eAgNnSc4d1	izotropní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
stejné	stejný	k2eAgFnSc2d1	stejná
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
vlastnosti	vlastnost	k1gFnSc2	vlastnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mechanické	mechanický	k2eAgFnPc1d1	mechanická
<g/>
,	,	kIx,	,
tepelné	tepelný	k2eAgFnPc1d1	tepelná
<g/>
,	,	kIx,	,
optické	optický	k2eAgNnSc1d1	optické
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
amorfní	amorfní	k2eAgFnPc4d1	amorfní
látky	látka	k1gFnPc4	látka
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
kapaliny	kapalina	k1gFnPc4	kapalina
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
viskozitou	viskozita	k1gFnSc7	viskozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
energetického	energetický	k2eAgNnSc2d1	energetické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
krystalické	krystalický	k2eAgNnSc1d1	krystalické
uspořádání	uspořádání	k1gNnSc1	uspořádání
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
než	než	k8xS	než
amorfní	amorfní	k2eAgNnSc1d1	amorfní
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
přirozené	přirozený	k2eAgFnPc1d1	přirozená
<g/>
.	.	kIx.	.
</s>
<s>
Amorfní	amorfní	k2eAgFnPc1d1	amorfní
látky	látka	k1gFnPc1	látka
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
při	při	k7c6	při
rychlém	rychlý	k2eAgNnSc6d1	rychlé
ochlazení	ochlazení	k1gNnSc6	ochlazení
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
částice	částice	k1gFnPc1	částice
nemají	mít	k5eNaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
krystalu	krystal	k1gInSc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
amorfní	amorfní	k2eAgFnSc2d1	amorfní
látky	látka	k1gFnSc2	látka
postupně	postupně	k6eAd1	postupně
měknou	měknout	k5eAaImIp3nP	měknout
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
tudíž	tudíž	k8xC	tudíž
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
pomocí	pomocí	k7c2	pomocí
oblasti	oblast	k1gFnSc2	oblast
měknutí	měknutí	k1gNnSc2	měknutí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
teplotní	teplotní	k2eAgInSc4d1	teplotní
interval	interval	k1gInSc4	interval
mezi	mezi	k7c7	mezi
pevnou	pevný	k2eAgFnSc7d1	pevná
a	a	k8xC	a
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
fází	fáze	k1gFnSc7	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
amorfní	amorfní	k2eAgFnPc4d1	amorfní
látky	látka	k1gFnPc4	látka
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
asfalt	asfalt	k1gInSc1	asfalt
<g/>
,	,	kIx,	,
vosk	vosk	k1gInSc1	vosk
nebo	nebo	k8xC	nebo
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
amorfní	amorfní	k2eAgFnPc4d1	amorfní
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
patří	patřit	k5eAaImIp3nP	patřit
polymery	polymer	k1gInPc1	polymer
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
až	až	k9	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
polymery	polymer	k1gInPc7	polymer
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
plastické	plastický	k2eAgFnPc1d1	plastická
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
PVC	PVC	kA	PVC
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
</s>
</p>
<p>
<s>
Krystal	krystal	k1gInSc1	krystal
</s>
</p>
