<s>
Vasilij	Vasilít	k5eAaPmRp2nS	Vasilít
III	III	kA	III
<g/>
.	.	kIx.	.
Ivanovič	Ivanovič	k1gInSc1	Ivanovič
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
III	III	kA	III
И	И	k?	И
<g/>
,	,	kIx,	,
1479	[number]	k4	1479
<g/>
-	-	kIx~	-
<g/>
1533	[number]	k4	1533
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
a	a	k8xC	a
byzantské	byzantský	k2eAgFnSc2d1	byzantská
princezny	princezna	k1gFnSc2	princezna
Sofie	Sofia	k1gFnSc2	Sofia
Palaiologovny	Palaiologovna	k1gFnSc2	Palaiologovna
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
jako	jako	k8xS	jako
veliký	veliký	k2eAgMnSc1d1	veliký
kníže	kníže	k1gMnSc1	kníže
moskevský	moskevský	k2eAgMnSc1d1	moskevský
.	.	kIx.	.
</s>
<s>
Anexemi	anexe	k1gFnPc7	anexe
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
knížectví	knížectví	k1gNnSc2	knížectví
(	(	kIx(	(
<g/>
1510	[number]	k4	1510
Pskov	Pskovo	k1gNnPc2	Pskovo
<g/>
,	,	kIx,	,
1521	[number]	k4	1521
Rjazaň	Rjazaň	k1gFnSc1	Rjazaň
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobytím	dobytí	k1gNnSc7	dobytí
Smolenska	Smolensko	k1gNnSc2	Smolensko
na	na	k7c6	na
litevském	litevský	k2eAgNnSc6d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc6	velkoknížectví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1514	[number]	k4	1514
završil	završit	k5eAaPmAgMnS	završit
sjednocení	sjednocení	k1gNnSc4	sjednocení
ruského	ruský	k2eAgNnSc2d1	ruské
území	území	k1gNnSc2	území
kolem	kolem	k7c2	kolem
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Velkokníže	velkokníže	k1gMnSc1	velkokníže
Vasilij	Vasilij	k1gMnSc1	Vasilij
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
adresátů	adresát	k1gMnPc2	adresát
prorockých	prorocký	k2eAgInPc2d1	prorocký
listů	list	k1gInPc2	list
pskovského	pskovský	k1gMnSc2	pskovský
mnicha	mnich	k1gMnSc2	mnich
Filofeje	Filofej	k1gMnSc2	Filofej
(	(	kIx(	(
<g/>
1465	[number]	k4	1465
<g/>
-	-	kIx~	-
<g/>
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Д	Д	k?	Д
Р	Р	k?	Р
п	п	k?	п
<g/>
,	,	kIx,	,
а	а	k?	а
т	т	k?	т
с	с	k?	с
<g/>
,	,	kIx,	,
а	а	k?	а
ч	ч	k?	ч
н	н	k?	н
б	б	k?	б
<g/>
"	"	kIx"	"
-	-	kIx~	-
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Dva	dva	k4xCgInPc4	dva
Římy	Řím	k1gInPc1	Řím
padly	padnout	k5eAaPmAgInP	padnout
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnPc1	třetí
stojí	stát	k5eAaImIp3nP	stát
a	a	k8xC	a
čtvrtého	čtvrtý	k4xOgMnSc4	čtvrtý
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
teze	teze	k1gFnSc1	teze
o	o	k7c6	o
Moskvě	Moskva	k1gFnSc6	Moskva
jako	jako	k8xS	jako
třetím	třetí	k4xOgMnSc6	třetí
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
PICKOVÁ	Picková	k1gFnSc1	Picková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Dynastický	dynastický	k2eAgInSc1d1	dynastický
svár	svár	k1gInSc1	svár
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
108	[number]	k4	108
<g/>
-	-	kIx~	-
<g/>
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
