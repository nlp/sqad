<s>
Vasilij	Vasilít	k5eAaPmRp2nS	Vasilít
III	III	kA	III
<g/>
.	.	kIx.	.
Ivanovič	Ivanovič	k1gInSc1	Ivanovič
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
III	III	kA	III
И	И	k?	И
<g/>
,	,	kIx,	,
1479	[number]	k4	1479
<g/>
-	-	kIx~	-
<g/>
1533	[number]	k4	1533
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
a	a	k8xC	a
byzantské	byzantský	k2eAgFnSc2d1	byzantská
princezny	princezna	k1gFnSc2	princezna
Sofie	Sofia	k1gFnSc2	Sofia
Palaiologovny	Palaiologovna	k1gFnSc2	Palaiologovna
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
jako	jako	k8xC	jako
veliký	veliký	k2eAgMnSc1d1	veliký
kníže	kníže	k1gMnSc1	kníže
moskevský	moskevský	k2eAgMnSc1d1	moskevský
.	.	kIx.	.
</s>
