<p>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
Grangerová	Grangerová	k1gFnSc1	Grangerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Hermiona	Hermion	k1gMnSc2	Hermion
Jean	Jean	k1gMnSc1	Jean
Grangerová	Grangerová	k1gFnSc1	Grangerová
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
série	série	k1gFnSc2	série
knih	kniha	k1gFnPc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
studentkou	studentka	k1gFnSc7	studentka
nebelvírské	belvírský	k2eNgFnSc2d1	nebelvírská
koleje	kolej	k1gFnSc2	kolej
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
studuje	studovat	k5eAaImIp3nS	studovat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
ročníku	ročník	k1gInSc6	ročník
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
Weasley	Weaslea	k1gFnSc2	Weaslea
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
nerozlučnou	rozlučný	k2eNgFnSc4d1	nerozlučná
přátelskou	přátelský	k2eAgFnSc4d1	přátelská
trojici	trojice	k1gFnSc4	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
sérii	série	k1gFnSc6	série
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
hraje	hrát	k5eAaImIp3nS	hrát
Hermionu	Hermion	k1gInSc6	Hermion
Emma	Emma	k1gFnSc1	Emma
Watson	Watson	k1gInSc1	Watson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
a	a	k8xC	a
původ	původ	k1gInSc4	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
mudlovského	mudlovský	k2eAgInSc2d1	mudlovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
její	její	k3xOp3gFnSc7	její
rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
zubaři	zubař	k1gMnPc1	zubař
<g/>
,	,	kIx,	,
sourozence	sourozenec	k1gMnPc4	sourozenec
žádné	žádný	k3yNgNnSc1	žádný
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc1	Rowling
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánovala	plánovat	k5eAaImAgFnS	plánovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
Hermioně	Hermioň	k1gFnPc4	Hermioň
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Lenu	Lena	k1gFnSc4	Lena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
studentkou	studentka	k1gFnSc7	studentka
ročníku	ročník	k1gInSc2	ročník
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
schopnou	schopný	k2eAgFnSc7d1	schopná
čarodějkou	čarodějka	k1gFnSc7	čarodějka
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
i	i	k8xC	i
jí	jíst	k5eAaImIp3nS	jíst
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
hned	hned	k6eAd1	hned
napoprvé	napoprvé	k6eAd1	napoprvé
nedaří	dařit	k5eNaImIp3nS	dařit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jevit	jevit	k5eAaImF	jevit
-	-	kIx~	-
létání	létání	k1gNnSc4	létání
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
<g/>
,	,	kIx,	,
vyčarovat	vyčarovat	k5eAaPmF	vyčarovat
patrona	patron	k1gMnSc4	patron
<g/>
,	,	kIx,	,
věštby	věštba	k1gFnPc4	věštba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
bystrá	bystrý	k2eAgFnSc1d1	bystrá
mysl	mysl	k1gFnSc1	mysl
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
knihám	kniha	k1gFnPc3	kniha
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
poznání	poznání	k1gNnSc6	poznání
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
aktivita	aktivita	k1gFnSc1	aktivita
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
jí	on	k3xPp3gFnSc3	on
u	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
vysloužily	vysloužit	k5eAaPmAgInP	vysloužit
pověst	pověst	k1gFnSc4	pověst
šprta	šprt	k1gMnSc2	šprt
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
projevuje	projevovat	k5eAaImIp3nS	projevovat
občas	občas	k6eAd1	občas
poněkud	poněkud	k6eAd1	poněkud
panovačně	panovačně	k6eAd1	panovačně
a	a	k8xC	a
umí	umět	k5eAaImIp3nS	umět
dát	dát	k5eAaPmF	dát
bez	bez	k7c2	bez
obalu	obal	k1gInSc2	obal
najevo	najevo	k6eAd1	najevo
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
profesorům	profesor	k1gMnPc3	profesor
-	-	kIx~	-
například	například	k6eAd1	například
profesorce	profesorka	k1gFnSc3	profesorka
Trelawneyové	Trelawneyová	k1gFnSc2	Trelawneyová
uraženě	uraženě	k6eAd1	uraženě
odejde	odejít	k5eAaPmIp3nS	odejít
uprostřed	uprostřed	k7c2	uprostřed
hodiny	hodina	k1gFnSc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
velmi	velmi	k6eAd1	velmi
kamarádská	kamarádský	k2eAgFnSc1d1	kamarádská
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc4	ten
však	však	k9	však
několikrát	několikrát	k6eAd1	několikrát
porušuje	porušovat	k5eAaImIp3nS	porušovat
-	-	kIx~	-
např.	např.	kA	např.
zneužitím	zneužití	k1gNnSc7	zneužití
zvětšovacího	zvětšovací	k2eAgNnSc2d1	zvětšovací
kouzla	kouzlo	k1gNnSc2	kouzlo
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
kabelku	kabelka	k1gFnSc4	kabelka
<g/>
;	;	kIx,	;
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
podobné	podobný	k2eAgFnSc2d1	podobná
praktiky	praktika	k1gFnSc2	praktika
striktně	striktně	k6eAd1	striktně
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
zastává	zastávat	k5eAaImIp3nS	zastávat
se	se	k3xPyFc4	se
utlačovaných	utlačovaný	k1gMnPc2	utlačovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
nutnost	nutnost	k1gFnSc1	nutnost
nebo	nebo	k8xC	nebo
loajalita	loajalita	k1gFnSc1	loajalita
k	k	k7c3	k
přátelům	přítel	k1gMnPc3	přítel
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
riskovat	riskovat	k5eAaBmF	riskovat
i	i	k8xC	i
hodně	hodně	k6eAd1	hodně
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
příliš	příliš	k6eAd1	příliš
neukazovat	ukazovat	k5eNaImF	ukazovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
ročníku	ročník	k1gInSc6	ročník
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
její	její	k3xOp3gInSc1	její
největší	veliký	k2eAgInSc1d3	veliký
strach	strach	k1gInSc1	strach
při	při	k7c6	při
hodině	hodina	k1gFnSc6	hodina
Obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
-	-	kIx~	-
bubák	bubák	k1gInSc1	bubák
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c6	v
profesoru	profesor	k1gMnSc3	profesor
McGonagallovou	McGonagallová	k1gFnSc4	McGonagallová
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zkouškách	zkouška	k1gFnPc6	zkouška
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
<g />
.	.	kIx.	.
</s>
<s>
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
je	být	k5eAaImIp3nS	být
Hermiona	Hermiona	k1gFnSc1	Hermiona
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xS	jako
nepříliš	příliš	k6eNd1	příliš
půvabná	půvabný	k2eAgFnSc1d1	půvabná
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
střapaté	střapatý	k2eAgInPc1d1	střapatý
hnědé	hnědý	k2eAgInPc1d1	hnědý
vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
dost	dost	k6eAd1	dost
velké	velký	k2eAgInPc1d1	velký
přední	přední	k2eAgInPc1d1	přední
zuby	zub	k1gInPc1	zub
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
v	v	k7c4	v
přitažlivou	přitažlivý	k2eAgFnSc4d1	přitažlivá
mladou	mladý	k2eAgFnSc4d1	mladá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
začínají	začínat	k5eAaImIp3nP	začínat
chlapci	chlapec	k1gMnPc1	chlapec
jevit	jevit	k5eAaImF	jevit
zájem	zájem	k1gInSc4	zájem
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
její	její	k3xOp3gFnPc4	její
encyklopedické	encyklopedický	k2eAgFnPc4d1	encyklopedická
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
domácím	domácí	k2eAgNnSc7d1	domácí
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
Křivonožka	křivonožka	k1gMnSc1	křivonožka
<g/>
,	,	kIx,	,
kříženec	kříženec	k1gMnSc1	kříženec
kočky	kočka	k1gFnSc2	kočka
s	s	k7c7	s
maguárem	maguár	k1gInSc7	maguár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
===	===	k?	===
</s>
</p>
<p>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
Spěšném	spěšný	k2eAgInSc6d1	spěšný
vlaku	vlak	k1gInSc6	vlak
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
žabákovi	žabák	k1gMnSc6	žabák
Nevilla	Nevilla	k1gFnSc1	Nevilla
Longbottoma	Longbottoma	k1gFnSc1	Longbottoma
Trevorovi	Trevorův	k2eAgMnPc1d1	Trevorův
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kupé	kupé	k1gNnSc2	kupé
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Ron	ron	k1gInSc4	ron
a	a	k8xC	a
Harry	Harra	k1gFnPc4	Harra
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
a	a	k8xC	a
Ronovi	Ron	k1gMnSc3	Ron
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
nelíbí	líbit	k5eNaImIp3nS	líbit
její	její	k3xOp3gFnSc2	její
arogance	arogance	k1gFnSc2	arogance
a	a	k8xC	a
všeználkovství	všeználkovství	k1gNnSc2	všeználkovství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k9	co
Hermionu	Hermion	k1gInSc2	Hermion
napadne	napadnout	k5eAaPmIp3nS	napadnout
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
svatých	svatá	k1gFnPc6	svatá
troll	troll	k1gMnSc1	troll
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
stanou	stanout	k5eAaPmIp3nP	stanout
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
logické	logický	k2eAgNnSc1d1	logické
myšlení	myšlení	k1gNnSc1	myšlení
a	a	k8xC	a
vědomosti	vědomost	k1gFnSc3	vědomost
trojici	trojice	k1gFnSc3	trojice
výrazně	výrazně	k6eAd1	výrazně
pomohou	pomoct	k5eAaPmIp3nP	pomoct
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
ke	k	k7c3	k
Kameni	kámen	k1gInSc3	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
se	se	k3xPyFc4	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
nového	nový	k2eAgMnSc2d1	nový
učitele	učitel	k1gMnSc2	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Zlatoslava	Zlatoslav	k1gMnSc2	Zlatoslav
Lockharta	Lockhart	k1gMnSc2	Lockhart
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
později	pozdě	k6eAd2	pozdě
také	také	k9	také
využije	využít	k5eAaPmIp3nS	využít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
přístupem	přístup	k1gInSc7	přístup
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Lektváry	Lektvár	k1gMnPc4	Lektvár
nejmocnější	mocný	k2eAgMnSc1d3	nejmocnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
pak	pak	k6eAd1	pak
podaří	podařit	k5eAaPmIp3nS	podařit
excelovat	excelovat	k5eAaImF	excelovat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g />
.	.	kIx.	.
</s>
<s>
mnoholičného	mnoholičný	k2eAgInSc2d1	mnoholičný
lektvaru	lektvar	k1gInSc2	lektvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Ron	ron	k1gInSc4	ron
použijí	použít	k5eAaPmIp3nP	použít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jako	jako	k9	jako
Crabbe	Crabb	k1gInSc5	Crabb
a	a	k8xC	a
Goyle	Goyl	k1gInSc6	Goyl
vyslechli	vyslechnout	k5eAaPmAgMnP	vyslechnout
Draca	Draca	k1gFnSc1	Draca
ohledně	ohledně	k7c2	ohledně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
Zmijozelův	Zmijozelův	k2eAgMnSc1d1	Zmijozelův
dědic	dědic	k1gMnSc1	dědic
(	(	kIx(	(
<g/>
přísady	přísada	k1gFnPc1	přísada
<g/>
:	:	kIx,	:
křídla	křídlo	k1gNnPc4	křídlo
ze	z	k7c2	z
zlatoočka	zlatoočka	k1gFnSc1	zlatoočka
<g/>
,	,	kIx,	,
pijavice	pijavice	k1gFnSc1	pijavice
<g/>
,	,	kIx,	,
prášek	prášek	k1gInSc1	prášek
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
dvojrožce	dvojrožec	k1gMnSc2	dvojrožec
<g/>
,	,	kIx,	,
truskavec	truskavec	k1gInSc1	truskavec
<g/>
,	,	kIx,	,
úhorník	úhorník	k1gInSc1	úhorník
mnohodílný	mnohodílný	k2eAgInSc1d1	mnohodílný
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
bojgy	bojga	k1gFnSc2	bojga
africké	africký	k2eAgFnSc2d1	africká
a	a	k8xC	a
-	-	kIx~	-
část	část	k1gFnSc1	část
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
chcete	chtít	k5eAaImIp2nP	chtít
proměnit	proměnit	k5eAaPmF	proměnit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vlas	vlas	k1gInSc1	vlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
si	se	k3xPyFc3	se
omylem	omylem	k6eAd1	omylem
místo	místo	k1gNnSc4	místo
vlasu	vlas	k1gInSc2	vlas
Millicent	Millicent	k1gInSc1	Millicent
Bullstrodeové	Bullstrodeová	k1gFnSc2	Bullstrodeová
vezme	vzít	k5eAaPmIp3nS	vzít
chlup	chlup	k1gInSc4	chlup
její	její	k3xOp3gFnSc2	její
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
lektvar	lektvar	k1gInSc1	lektvar
nemá	mít	k5eNaImIp3nS	mít
používat	používat	k5eAaImF	používat
na	na	k7c4	na
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
na	na	k7c4	na
ošetřovnu	ošetřovna	k1gFnSc4	ošetřovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
díle	dílo	k1gNnSc6	dílo
Hermiona	Hermiona	k1gFnSc1	Hermiona
také	také	k6eAd1	také
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
šprtkou	šprtka	k1gFnSc7	šprtka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
i	i	k9	i
porušit	porušit	k5eAaPmF	porušit
školní	školní	k2eAgInSc4d1	školní
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
když	když	k8xS	když
potřeby	potřeba	k1gFnPc1	potřeba
k	k	k7c3	k
lektvaru	lektvar	k1gInSc3	lektvar
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
ze	z	k7c2	z
Snapeova	Snapeův	k2eAgInSc2d1	Snapeův
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Hermiona	Hermiona	k1gFnSc1	Hermiona
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
netvor	netvor	k1gMnSc1	netvor
v	v	k7c6	v
Tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
je	být	k5eAaImIp3nS	být
bazilišek	bazilišek	k1gMnSc1	bazilišek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
napadne	napadnout	k5eAaPmIp3nS	napadnout
právě	právě	k6eAd1	právě
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
chce	chtít	k5eAaImIp3nS	chtít
oznámit	oznámit	k5eAaPmF	oznámit
ostatním	ostatní	k1gNnSc7	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
ale	ale	k9	ale
použila	použít	k5eAaPmAgFnS	použít
zrcátko	zrcátko	k1gNnSc4	zrcátko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
zkameněla	zkamenět	k5eAaPmAgFnS	zkamenět
<g/>
.	.	kIx.	.
</s>
<s>
Nešla	jít	k5eNaImAgFnS	jít
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
a	a	k8xC	a
Ronem	ron	k1gInSc7	ron
do	do	k7c2	do
Tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
madame	madame	k1gFnSc1	madame
Pomfreyová	Pomfreyová	k1gFnSc1	Pomfreyová
vyléčila	vyléčit	k5eAaPmAgFnS	vyléčit
ze	z	k7c2	z
zkamenění	zkamenění	k1gNnSc2	zkamenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
si	se	k3xPyFc3	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
koupí	koupit	k5eAaPmIp3nS	koupit
kocoura	kocour	k1gMnSc4	kocour
Křivonožku	křivonožka	k1gMnSc4	křivonožka
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
nemůže	moct	k5eNaImIp3nS	moct
Ron	Ron	k1gMnSc1	Ron
snést	snést	k5eAaPmF	snést
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neustále	neustále	k6eAd1	neustále
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
krysu	krysa	k1gFnSc4	krysa
Prašivku	prašivka	k1gFnSc4	prašivka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
zvolila	zvolit	k5eAaPmAgFnS	zvolit
více	hodně	k6eAd2	hodně
volitelných	volitelný	k2eAgInPc2d1	volitelný
předmětů	předmět	k1gInPc2	předmět
než	než	k8xS	než
ostatní	ostatní	k2eAgNnSc1d1	ostatní
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
obraceč	obraceč	k1gInSc1	obraceč
času	čas	k1gInSc2	čas
od	od	k7c2	od
profesorky	profesorka	k1gFnSc2	profesorka
McGonagallové	McGonagallová	k1gFnSc2	McGonagallová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
zvládat	zvládat	k5eAaImF	zvládat
vyučování	vyučování	k1gNnSc4	vyučování
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
ostatním	ostatní	k2eAgMnPc3d1	ostatní
spolužákům	spolužák	k1gMnPc3	spolužák
celý	celý	k2eAgInSc4d1	celý
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
nejde	jít	k5eNaImIp3nS	jít
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgFnPc4	některý
její	její	k3xOp3gFnPc4	její
hodiny	hodina	k1gFnPc4	hodina
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
na	na	k7c6	na
všech	všecek	k3xTgFnPc2	všecek
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
je	být	k5eAaImIp3nS	být
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jim	on	k3xPp3gMnPc3	on
Snape	Snap	k1gInSc5	Snap
zadá	zadat	k5eAaPmIp3nS	zadat
domácí	domácí	k2eAgInSc4d1	domácí
úkol	úkol	k1gInSc4	úkol
o	o	k7c6	o
vlkodlacích	vlkodlak	k1gMnPc6	vlkodlak
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgNnP	rozhodnout
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zachovat	zachovat	k5eAaPmF	zachovat
jeho	jeho	k3xOp3gNnSc4	jeho
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
jde	jít	k5eAaImIp3nS	jít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
do	do	k7c2	do
Chroptící	chroptící	k2eAgFnSc2d1	chroptící
chýše	chýš	k1gFnSc2	chýš
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
zavedl	zavést	k5eAaPmAgMnS	zavést
Rona	Ron	k1gMnSc4	Ron
a	a	k8xC	a
Prašivku	prašivka	k1gFnSc4	prašivka
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
tam	tam	k6eAd1	tam
odhalí	odhalit	k5eAaPmIp3nS	odhalit
skutečný	skutečný	k2eAgInSc1d1	skutečný
příběh	příběh	k1gInSc1	příběh
Potterových	Potterův	k2eAgFnPc2d1	Potterova
<g/>
,	,	kIx,	,
Siriuse	Siriuse	k1gFnPc1	Siriuse
a	a	k8xC	a
Pettigrewa	Pettigrewum	k1gNnPc1	Pettigrewum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Siriusově	Siriusův	k2eAgNnSc6d1	Siriusovo
uvěznění	uvěznění	k1gNnSc6	uvěznění
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Hermionin	Hermionin	k2eAgInSc1d1	Hermionin
obraceč	obraceč	k1gInSc1	obraceč
času	čas	k1gInSc2	čas
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
zachránili	zachránit	k5eAaPmAgMnP	zachránit
Siriuse	Siriuse	k1gFnPc4	Siriuse
od	od	k7c2	od
mozkomorova	mozkomorův	k2eAgInSc2d1	mozkomorův
polibku	polibek	k1gInSc2	polibek
a	a	k8xC	a
Klofana	Klofana	k1gFnSc1	Klofana
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
===	===	k?	===
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Weasleyovými	Weasleyová	k1gFnPc7	Weasleyová
a	a	k8xC	a
Harrym	Harrym	k1gInSc1	Harrym
jde	jít	k5eAaImIp3nS	jít
Hermiona	Hermiona	k1gFnSc1	Hermiona
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
poprvé	poprvé	k6eAd1	poprvé
uvidí	uvidět	k5eAaPmIp3nP	uvidět
chytače	chytač	k1gInPc1	chytač
bulharského	bulharský	k2eAgInSc2d1	bulharský
týmu	tým	k1gInSc2	tým
Viktora	Viktor	k1gMnSc2	Viktor
Kruma	Krum	k1gMnSc2	Krum
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
k	k	k7c3	k
Ronově	Ronův	k2eAgFnSc3d1	Ronova
nelibosti	nelibost	k1gFnSc3	nelibost
na	na	k7c4	na
Vánoční	vánoční	k2eAgInSc4d1	vánoční
ples	ples	k1gInSc4	ples
Turnaje	turnaj	k1gInSc2	turnaj
tří	tři	k4xCgFnPc2	tři
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Založí	založit	k5eAaPmIp3nS	založit
také	také	k9	také
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
občanské	občanský	k2eAgFnSc2d1	občanská
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
skřítků	skřítek	k1gMnPc2	skřítek
(	(	kIx(	(
<g/>
SPOŽÚS	SPOŽÚS	kA	SPOŽÚS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
práva	právo	k1gNnPc4	právo
domácích	domácí	k2eAgMnPc2d1	domácí
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
také	také	k9	také
obětí	oběť	k1gFnSc7	oběť
reportérky	reportérka	k1gFnSc2	reportérka
Rity	Rita	k1gFnSc2	Rita
Holoubkové	Holoubková	k1gFnSc2	Holoubková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
obviní	obvinit	k5eAaPmIp3nS	obvinit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlomila	zlomit	k5eAaPmAgFnS	zlomit
Harrymu	Harrym	k1gInSc2	Harrym
srdce	srdce	k1gNnSc4	srdce
kvůli	kvůli	k7c3	kvůli
Krumovi	Krum	k1gMnSc3	Krum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dílu	díl	k1gInSc2	díl
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Holoubková	Holoubková	k1gFnSc1	Holoubková
zvěromág	zvěromága	k1gFnPc2	zvěromága
a	a	k8xC	a
zakáže	zakázat	k5eAaPmIp3nS	zakázat
jí	on	k3xPp3gFnSc3	on
dále	daleko	k6eAd2	daleko
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
prozradila	prozradit	k5eAaPmAgFnS	prozradit
její	její	k3xOp3gNnPc4	její
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Prázdniny	prázdniny	k1gFnPc4	prázdniny
tráví	trávit	k5eAaImIp3nS	trávit
Hermiona	Hermiona	k1gFnSc1	Hermiona
na	na	k7c6	na
Grimmauldově	Grimmauldův	k2eAgNnSc6d1	Grimmauldovo
náměstí	náměstí	k1gNnSc6	náměstí
12	[number]	k4	12
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
nebelvírskou	belvírský	k2eNgFnSc7d1	nebelvírská
prefektkou	prefektka	k1gFnSc7	prefektka
<g/>
,	,	kIx,	,
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
Lenkou	Lenka	k1gFnSc7	Lenka
Láskorádovou	Láskorádový	k2eAgFnSc7d1	Láskorádová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ritou	Rita	k1gFnSc7	Rita
Holoubkovou	Holoubková	k1gFnSc7	Holoubková
pomůže	pomoct	k5eAaPmIp3nS	pomoct
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
má	mít	k5eAaImIp3nS	mít
Harry	Harra	k1gMnSc2	Harra
prezentovat	prezentovat	k5eAaBmF	prezentovat
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Voldemortově	Voldemortův	k2eAgInSc6d1	Voldemortův
vzestupu	vzestup	k1gInSc6	vzestup
v	v	k7c6	v
Jinotaji	jinotaj	k1gInSc6	jinotaj
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
také	také	k9	také
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
Brumbálovu	brumbálův	k2eAgFnSc4d1	Brumbálova
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
studenty	student	k1gMnPc4	student
naučit	naučit	k5eAaPmF	naučit
obraně	obraně	k6eAd1	obraně
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nová	nový	k2eAgFnSc1d1	nová
učitelka	učitelka	k1gFnSc1	učitelka
Dolores	Doloresa	k1gFnPc2	Doloresa
Umbridgeová	Umbridgeová	k1gFnSc1	Umbridgeová
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brumbálově	brumbálův	k2eAgFnSc6d1	Brumbálova
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
vykouzlit	vykouzlit	k5eAaPmF	vykouzlit
svého	svůj	k3xOyFgMnSc4	svůj
patrona	patron	k1gMnSc4	patron
-	-	kIx~	-
vydru	vydra	k1gFnSc4	vydra
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejoblíbenější	oblíbený	k2eAgNnSc4d3	nejoblíbenější
zvíře	zvíře	k1gNnSc4	zvíře
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc1	Rowling
<g/>
;	;	kIx,	;
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
žije	žít	k5eAaImIp3nS	žít
u	u	k7c2	u
Weasleyů	Weasley	k1gInPc2	Weasley
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
vztah	vztah	k1gInSc4	vztah
Hermiony	Hermion	k1gInPc7	Hermion
a	a	k8xC	a
Rona	Ron	k1gMnSc2	Ron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Umbridgeové	Umbridgeová	k1gFnPc1	Umbridgeová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
stala	stát	k5eAaPmAgFnS	stát
ředitelkou	ředitelka	k1gFnSc7	ředitelka
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
zbaví	zbavit	k5eAaPmIp3nP	zbavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
chytne	chytnout	k5eAaPmIp3nS	chytnout
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
kabinetě	kabinet	k1gInSc6	kabinet
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kentaurům	kentaur	k1gMnPc3	kentaur
v	v	k7c6	v
Zapovězeném	zapovězený	k2eAgInSc6d1	zapovězený
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ji	on	k3xPp3gFnSc4	on
dovedou	dovést	k5eAaPmIp3nP	dovést
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
odboru	odbor	k1gInSc6	odbor
záhad	záhada	k1gFnPc2	záhada
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
vážně	vážně	k6eAd1	vážně
zraněná	zraněný	k2eAgFnSc1d1	zraněná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
===	===	k?	===
</s>
</p>
<p>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
členkou	členka	k1gFnSc7	členka
Křikova	Křikův	k2eAgInSc2d1	Křikův
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ji	on	k3xPp3gFnSc4	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
Horacio	Horacio	k6eAd1	Horacio
Křiklan	Křiklan	k1gInSc1	Křiklan
díky	díky	k7c3	díky
jejím	její	k3xOp3gFnPc3	její
vědomostem	vědomost	k1gFnPc3	vědomost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lektvarech	lektvar	k1gInPc6	lektvar
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
hvězdou	hvězda	k1gFnSc7	hvězda
k	k	k7c3	k
Hermionině	Hermionin	k2eAgFnSc3d1	Hermionina
nelibosti	nelibost	k1gFnSc3	nelibost
Harry	Harra	k1gFnSc2	Harra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
starou	starý	k2eAgFnSc4d1	stará
učebnici	učebnice	k1gFnSc4	učebnice
Severuse	Severuse	k1gFnSc2	Severuse
Snapea	Snape	k1gInSc2	Snape
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
nese	nést	k5eAaImIp3nS	nést
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
fair-play	fairlaa	k1gFnPc4	fair-plaa
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc2	on
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnSc2d1	důležitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
také	také	k9	také
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
brankářem	brankář	k1gMnSc7	brankář
nebelvírského	belvírský	k2eNgInSc2d1	nebelvírský
famfrpálového	famfrpálový	k2eAgInSc2d1	famfrpálový
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
když	když	k8xS	když
sešle	seslat	k5eAaPmIp3nS	seslat
matoucí	matoucí	k2eAgNnSc4d1	matoucí
kouzlo	kouzlo	k1gNnSc4	kouzlo
na	na	k7c4	na
Cormaca	Cormac	k2eAgMnSc4d1	Cormac
McLaggena	McLaggen	k1gMnSc4	McLaggen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
dílu	díl	k1gInSc2	díl
také	také	k9	také
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
najevo	najevo	k6eAd1	najevo
city	cit	k1gInPc1	cit
mezi	mezi	k7c7	mezi
Hermionou	Hermiona	k1gFnSc7	Hermiona
a	a	k8xC	a
Ronem	ron	k1gInSc7	ron
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
jí	on	k3xPp3gFnSc3	on
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
Krumem	Krum	k1gInSc7	Krum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
začne	začít	k5eAaPmIp3nS	začít
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Levandulí	levandule	k1gFnSc7	levandule
Brownovou	Brownová	k1gFnSc7	Brownová
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
zase	zase	k9	zase
ze	z	k7c2	z
vzteku	vztek	k1gInSc2	vztek
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
vánoční	vánoční	k2eAgInSc4d1	vánoční
večírek	večírek	k1gInSc4	večírek
s	s	k7c7	s
Cormacem	Cormaec	k1gInSc7	Cormaec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
Zachariáši	Zachariáš	k1gMnSc6	Zachariáš
Smithovi	Smith	k1gMnSc6	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
usmíří	usmířit	k5eAaPmIp3nS	usmířit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Ron	ron	k1gInSc1	ron
otráven	otráven	k2eAgInSc1d1	otráven
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
bitvy	bitva	k1gFnPc1	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
proti	proti	k7c3	proti
smrtijedům	smrtijed	k1gMnPc3	smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Brumbálově	brumbálův	k2eAgInSc6d1	brumbálův
pohřbu	pohřeb	k1gInSc6	pohřeb
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
doprovázet	doprovázet	k5eAaImF	doprovázet
Harryho	Harry	k1gMnSc4	Harry
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
za	za	k7c4	za
viteály	viteál	k1gMnPc4	viteál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
Harryho	Harry	k1gMnSc2	Harry
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
viteálech	viteál	k1gInPc6	viteál
a	a	k8xC	a
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
smrtijedy	smrtijed	k1gMnPc7	smrtijed
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
jim	on	k3xPp3gMnPc3	on
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
průšvihů	průšvih	k1gInPc2	průšvih
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gNnSc4	on
chytnou	chytnout	k5eAaPmIp3nP	chytnout
lapkové	lapka	k1gMnPc1	lapka
<g/>
,	,	kIx,	,
představí	představit	k5eAaPmIp3nS	představit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Penelopa	Penelopa	k1gFnSc1	Penelopa
Clearwaterová	Clearwaterový	k2eAgFnSc1d1	Clearwaterový
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k8xC	ale
neunikne	uniknout	k5eNaPmIp3nS	uniknout
výslechu	výslech	k1gInSc2	výslech
a	a	k8xC	a
kletbě	kletba	k1gFnSc6	kletba
Cruciatus	Cruciatus	k1gInSc1	Cruciatus
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
uvězněni	uvěznit	k5eAaPmNgMnP	uvěznit
na	na	k7c6	na
Malfoyově	Malfoyov	k1gInSc6	Malfoyov
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
neprozradí	prozradit	k5eNaPmIp3nS	prozradit
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
zachráněni	zachráněn	k2eAgMnPc1d1	zachráněn
Dobbym	Dobbym	k1gInSc4	Dobbym
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c4	za
Belatrix	Belatrix	k1gInSc4	Belatrix
Lestrangeovou	Lestrangeův	k2eAgFnSc7d1	Lestrangeův
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
u	u	k7c2	u
Gringottových	Gringottová	k1gFnPc2	Gringottová
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
pro	pro	k7c4	pro
pohárek	pohárek	k1gInSc4	pohárek
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Bradavice	bradavice	k1gFnPc4	bradavice
bojuje	bojovat	k5eAaImIp3nS	bojovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ginny	Ginn	k1gMnPc7	Ginn
a	a	k8xC	a
Lenkou	Lenka	k1gFnSc7	Lenka
proti	proti	k7c3	proti
Belatrix	Belatrix	k1gInSc4	Belatrix
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
ji	on	k3xPp3gFnSc4	on
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Belatrixina	Belatrixin	k2eAgFnSc1d1	Belatrixin
smrtící	smrtící	k2eAgFnSc1d1	smrtící
kletba	kletba	k1gFnSc1	kletba
jen	jen	k9	jen
tak	tak	k6eAd1	tak
tak	tak	k6eAd1	tak
mine	minout	k5eAaImIp3nS	minout
Ginny	Ginn	k1gInPc4	Ginn
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
Molly	Molla	k1gFnPc4	Molla
Weasleyová	Weasleyová	k1gFnSc1	Weasleyová
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
Belatrix	Belatrix	k1gInSc4	Belatrix
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
chce	chtít	k5eAaImIp3nS	chtít
zničit	zničit	k5eAaPmF	zničit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
viteálů	viteál	k1gInPc2	viteál
v	v	k7c6	v
Komnatě	komnata	k1gFnSc6	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komnatě	komnata	k1gFnSc6	komnata
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
láska	láska	k1gFnSc1	láska
mezi	mezi	k7c7	mezi
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
Hermionou	Hermiona	k1gFnSc7	Hermiona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Epilog	epilog	k1gInSc1	epilog
====	====	k?	====
</s>
</p>
<p>
<s>
Devatenáct	devatenáct	k4xCc1	devatenáct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Voldemortově	Voldemortův	k2eAgInSc6d1	Voldemortův
pádu	pád	k1gInSc6	pád
je	být	k5eAaImIp3nS	být
Hermiona	Hermiona	k1gFnSc1	Hermiona
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c2	za
Rona	Ron	k1gMnSc2	Ron
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
Rose	Ros	k1gMnSc2	Ros
a	a	k8xC	a
Huga	Hugo	k1gMnSc2	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
škole	škola	k1gFnSc6	škola
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
odboru	odbor	k1gInSc6	odbor
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
kouzelnými	kouzelný	k2eAgMnPc7d1	kouzelný
tvory	tvor	k1gMnPc7	tvor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zasadí	zasadit	k5eAaPmIp3nP	zasadit
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
postavení	postavení	k1gNnSc2	postavení
domácích	domácí	k2eAgMnPc2d1	domácí
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
odbor	odbor	k1gInSc4	odbor
pro	pro	k7c4	pro
uplatňování	uplatňování	k1gNnSc4	uplatňování
kouzelnických	kouzelnický	k2eAgInPc2d1	kouzelnický
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
Kingsleyho	Kingsley	k1gMnSc4	Kingsley
Pastorka	Pastorek	k1gMnSc4	Pastorek
na	na	k7c6	na
postu	post	k1gInSc6	post
ministra	ministr	k1gMnSc2	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křivonožka	křivonožka	k1gMnSc1	křivonožka
==	==	k?	==
</s>
</p>
<p>
<s>
Křivonožka	křivonožka	k1gMnSc1	křivonožka
je	být	k5eAaImIp3nS	být
Hermionin	Hermionin	k2eAgMnSc1d1	Hermionin
kocour	kocour	k1gMnSc1	kocour
<g/>
,	,	kIx,	,
kříženec	kříženec	k1gMnSc1	kříženec
kočky	kočka	k1gFnSc2	kočka
a	a	k8xC	a
maguára	maguár	k1gMnSc2	maguár
<g/>
.	.	kIx.	.
</s>
<s>
Koupila	koupit	k5eAaPmAgFnS	koupit
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
na	na	k7c6	na
Příčné	příčný	k2eAgFnSc6d1	příčná
ulici	ulice	k1gFnSc6	ulice
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Křivonožka	křivonožka	k1gMnSc1	křivonožka
začal	začít	k5eAaPmAgMnS	začít
okamžitě	okamžitě	k6eAd1	okamžitě
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
Prašivku	prašivka	k1gFnSc4	prašivka
<g/>
,	,	kIx,	,
Ronovu	Ronův	k2eAgFnSc4d1	Ronova
krysu	krysa	k1gFnSc4	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
přestali	přestat	k5eAaPmAgMnP	přestat
Ron	Ron	k1gMnSc1	Ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
bavit	bavit	k5eAaImF	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Křivonožka	křivonožka	k1gMnSc1	křivonožka
ale	ale	k8xC	ale
v	v	k7c6	v
Prašivce	prašivka	k1gFnSc6	prašivka
odhalil	odhalit	k5eAaPmAgMnS	odhalit
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
zvěromág	zvěromág	k1gMnSc1	zvěromág
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
odhalen	odhalen	k2eAgInSc1d1	odhalen
příběh	příběh	k1gInSc1	příběh
Siriuse	Siriuse	k1gFnSc2	Siriuse
Blacka	Blacko	k1gNnSc2	Blacko
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
během	během	k7c2	během
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
Křivonožka	křivonožka	k1gMnSc1	křivonožka
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
mu	on	k3xPp3gMnSc3	on
donést	donést	k5eAaPmF	donést
Prašivku	prašivka	k1gFnSc4	prašivka
a	a	k8xC	a
donesl	donést	k5eAaPmAgMnS	donést
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
hesla	heslo	k1gNnSc2	heslo
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
nebelvírské	belvírský	k2eNgFnSc2d1	nebelvírská
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
ležet	ležet	k5eAaImF	ležet
Neville	Neville	k1gNnSc4	Neville
Longbottom	Longbottom	k1gInSc4	Longbottom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
nehrál	hrát	k5eNaImAgMnS	hrát
Křivonožka	křivonožka	k1gMnSc1	křivonožka
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
