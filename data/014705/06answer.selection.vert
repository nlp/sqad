<s>
Oční	oční	k2eAgInPc4d1
stíny	stín	k1gInPc4
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
kosmetického	kosmetický	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nanáší	nanášet	k5eAaImIp3nS
do	do	k7c2
okolí	okolí	k1gNnSc2
očí	oko	k1gNnPc2
za	za	k7c7
účelem	účel	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
zvýraznění	zvýraznění	k1gNnSc2
a	a	k8xC
zkrášlení	zkrášlení	k1gNnSc2
<g/>
.	.	kIx.
</s>