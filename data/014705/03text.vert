<s>
Oční	oční	k2eAgInPc1d1
stíny	stín	k1gInPc1
</s>
<s>
Nalíčené	nalíčený	k2eAgNnSc1d1
oko	oko	k1gNnSc1
</s>
<s>
Hnědá	hnědý	k2eAgFnSc1d1
škála	škála	k1gFnSc1
očních	oční	k2eAgInPc2d1
stínů	stín	k1gInPc2
</s>
<s>
Oční	oční	k2eAgInPc4d1
stíny	stín	k1gInPc4
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
kosmetického	kosmetický	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nanáší	nanášet	k5eAaImIp3nS
do	do	k7c2
okolí	okolí	k1gNnSc2
očí	oko	k1gNnPc2
za	za	k7c7
účelem	účel	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
zvýraznění	zvýraznění	k1gNnSc2
a	a	k8xC
zkrášlení	zkrášlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
barevné	barevný	k2eAgInPc4d1
prášky	prášek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
nanášeny	nanášen	k2eAgMnPc4d1
malým	malý	k2eAgInSc7d1
polštářkem	polštářek	k1gInSc7
na	na	k7c4
kůži	kůže	k1gFnSc4
okolo	okolo	k7c2
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c4
oční	oční	k2eAgNnSc4d1
víčko	víčko	k1gNnSc4
a	a	k8xC
lícní	lícní	k2eAgFnPc4d1
kosti	kost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnSc2
těchto	tento	k3xDgInPc2
prášků	prášek	k1gInPc2
nejsou	být	k5eNaImIp3nP
nikterak	nikterak	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
a	a	k8xC
jsou	být	k5eAaImIp3nP
vyráběny	vyrábět	k5eAaImNgInP
v	v	k7c6
celé	celý	k2eAgFnSc6d1
škále	škála	k1gFnSc6
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Oční	oční	k2eAgInPc1d1
stíny	stín	k1gInPc1
se	se	k3xPyFc4
často	často	k6eAd1
doplňují	doplňovat	k5eAaImIp3nP
s	s	k7c7
používáním	používání	k1gNnSc7
řasenky	řasenka	k1gFnSc2
pro	pro	k7c4
zvýraznění	zvýraznění	k1gNnSc4
řas	řasa	k1gFnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
rtěnkou	rtěnka	k1gFnSc7
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
celkového	celkový	k2eAgInSc2d1
makeupu	makeup	k1gInSc2
obličeje	obličej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
odstraňování	odstraňování	k1gNnSc4
líčidla	líčidlo	k1gNnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
odličovací	odličovací	k2eAgInSc1d1
polštářek	polštářek	k1gInSc1
a	a	k8xC
krém	krém	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
stírá	stírat	k5eAaImIp3nS
nanesená	nanesený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
z	z	k7c2
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oční	oční	k2eAgInPc1d1
stíny	stín	k1gInPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
z	z	k7c2
pevných	pevný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
jako	jako	k8xS,k8xC
pudru	pudr	k1gInSc2
<g/>
,	,	kIx,
rozdrcené	rozdrcený	k2eAgFnSc2d1
slídy	slída	k1gFnSc2
či	či	k8xC
grafitu	grafit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
používání	používání	k1gNnSc1
je	být	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
rozšířeno	rozšířit	k5eAaPmNgNnS
mezi	mezi	k7c7
oběma	dva	k4xCgNnPc7
pohlavími	pohlaví	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
je	být	k5eAaImIp3nS
ale	ale	k9
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
ženami	žena	k1gFnPc7
a	a	k8xC
to	ten	k3xDgNnSc4
jak	jak	k6eAd1
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
tak	tak	k6eAd1
i	i	k9
při	při	k7c6
slavnostních	slavnostní	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
produkty	produkt	k1gInPc4
nabízí	nabízet	k5eAaImIp3nS
mnoho	mnoho	k4c1
světových	světový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
(	(	kIx(
Maybeline	Maybelin	k1gInSc5
<g/>
,	,	kIx,
Catrice	Catrika	k1gFnSc6
<g/>
,	,	kIx,
Urban	Urban	k1gMnSc1
Decay	Decaa	k1gFnSc2
<g/>
,	,	kIx,
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
