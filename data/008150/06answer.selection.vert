<s>
Miliarda	miliarda	k4xCgFnSc1	miliarda
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
číslovka	číslovka	k1gFnSc1	číslovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
číslo	číslo	k1gNnSc4	číslo
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
109	[number]	k4	109
(	(	kIx(	(
<g/>
jednička	jednička	k1gFnSc1	jednička
a	a	k8xC	a
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
devět	devět	k4xCc4	devět
nul	nula	k1gFnPc2	nula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgInPc7d1	jiný
slovy	slovo	k1gNnPc7	slovo
tisíc	tisíc	k4xCgInSc4	tisíc
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
