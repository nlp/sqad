<s>
Miliarda	miliarda	k4xCgFnSc1	miliarda
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
číslovka	číslovka	k1gFnSc1	číslovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
číslo	číslo	k1gNnSc4	číslo
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
109	[number]	k4	109
(	(	kIx(	(
<g/>
jednička	jednička	k1gFnSc1	jednička
a	a	k8xC	a
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
devět	devět	k4xCc4	devět
nul	nula	k1gFnPc2	nula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgInPc7d1	jiný
slovy	slovo	k1gNnPc7	slovo
tisíc	tisíc	k4xCgInSc4	tisíc
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
předpona	předpona	k1gFnSc1	předpona
giga-	giga-	k?	giga-
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
milliard	milliarda	k1gFnPc2	milliarda
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
škále	škála	k1gFnSc6	škála
se	se	k3xPyFc4	se
číslo	číslo	k1gNnSc1	číslo
109	[number]	k4	109
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
bilion	bilion	k4xCgInSc4	bilion
(	(	kIx(	(
<g/>
billion	billion	k1gInSc4	billion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
označuje	označovat	k5eAaImIp3nS	označovat
číslo	číslo	k1gNnSc1	číslo
1012	[number]	k4	1012
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jednoznačnosti	jednoznačnost	k1gFnSc3	jednoznačnost
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
miliarda	miliarda	k4xCgFnSc1	miliarda
někdy	někdy	k6eAd1	někdy
opisuje	opisovat	k5eAaImIp3nS	opisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
tisíc	tisíc	k4xCgInSc4	tisíc
milionů	milion	k4xCgInPc2	milion
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
miliarda	miliarda	k4xCgFnSc1	miliarda
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
mille	mille	k1gNnSc2	mille
(	(	kIx(	(
<g/>
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozmnožující	rozmnožující	k2eAgFnSc7d1	rozmnožující
koncovkou	koncovka	k1gFnSc7	koncovka
-ard	rda	k1gFnPc2	-arda
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
škála	škála	k1gFnSc1	škála
Seznam	seznam	k1gInSc1	seznam
čísel	číslo	k1gNnPc2	číslo
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
miliarda	miliarda	k4xCgFnSc1	miliarda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
