<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
Michli	Michli	k1gFnPc2	Michli
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Čtrnácti	čtrnáct	k4xCc2	čtrnáct
svatých	svatá	k1gFnPc2	svatá
pomocníků	pomocník	k1gMnPc2	pomocník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1724	[number]	k4	1724
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
michelské	michelský	k2eAgFnSc2d1	Michelská
farnosti	farnost	k1gFnSc2	farnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
duchovně	duchovně	k6eAd1	duchovně
spravován	spravován	k2eAgInSc1d1	spravován
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
nemovitá	movitý	k2eNgFnSc1d1	nemovitá
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
fundační	fundační	k2eAgFnSc2d1	fundační
a	a	k8xC	a
donační	donační	k2eAgFnSc2d1	donační
listiny	listina	k1gFnSc2	listina
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1724	[number]	k4	1724
nechal	nechat	k5eAaPmAgMnS	nechat
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
práv	práv	k2eAgMnSc1d1	práv
Václav	Václav	k1gMnSc1	Václav
Neumann	Neumann	k1gMnSc1	Neumann
z	z	k7c2	z
Puchholtze	Puchholtze	k1gFnSc2	Puchholtze
zřídit	zřídit	k5eAaPmF	zřídit
kapli	kaple	k1gFnSc4	kaple
"	"	kIx"	"
<g/>
ku	k	k7c3	k
cti	čest	k1gFnSc3	čest
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Čtrnácti	čtrnáct	k4xCc2	čtrnáct
sv.	sv.	kA	sv.
pomocníků	pomocník	k1gMnPc2	pomocník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yRgFnSc7	který
držela	držet	k5eAaImAgFnS	držet
patronát	patronát	k1gInSc4	patronát
právnická	právnický	k2eAgFnSc1d1	právnická
a	a	k8xC	a
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
nejsou	být	k5eNaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
žádné	žádný	k3yNgFnPc1	žádný
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1728	[number]	k4	1728
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
Josef	Josef	k1gMnSc1	Josef
Daniel	Daniel	k1gMnSc1	Daniel
Mayer	Mayer	k1gMnSc1	Mayer
z	z	k7c2	z
Mayernu	Mayerna	k1gFnSc4	Mayerna
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
dva	dva	k4xCgInPc4	dva
přenosné	přenosný	k2eAgInPc4d1	přenosný
postranní	postranní	k2eAgInPc4d1	postranní
oltáře	oltář	k1gInPc4	oltář
a	a	k8xC	a
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
relikvie	relikvie	k1gFnPc1	relikvie
sv.	sv.	kA	sv.
Inocence	Inocenc	k1gMnSc4	Inocenc
a	a	k8xC	a
sv.	sv.	kA	sv.
Viktora	Viktor	k1gMnSc2	Viktor
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
knězem	kněz	k1gMnSc7	kněz
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	let	k1gInPc6	let
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
Šebesta	Šebesta	k1gMnSc1	Šebesta
<g/>
.	.	kIx.	.
</s>
<s>
Jmění	jmění	k1gNnSc1	jmění
kostela	kostel	k1gInSc2	kostel
činilo	činit	k5eAaImAgNnS	činit
700	[number]	k4	700
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
,	,	kIx,	,
křty	křest	k1gInPc4	křest
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
misionářsky	misionářsky	k6eAd1	misionářsky
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
Vršovic	Vršovice	k1gFnPc2	Vršovice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byly	být	k5eAaImAgFnP	být
události	událost	k1gFnPc1	událost
michelského	michelský	k2eAgInSc2d1	michelský
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
zaznamenávány	zaznamenáván	k2eAgInPc4d1	zaznamenáván
v	v	k7c6	v
pamětních	pamětní	k2eAgFnPc6d1	pamětní
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kniha	kniha	k1gFnSc1	kniha
křtů	křest	k1gInPc2	křest
<g/>
,	,	kIx,	,
sňatků	sňatek	k1gInPc2	sňatek
a	a	k8xC	a
pohřbů	pohřeb	k1gInPc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
kostele	kostel	k1gInSc6	kostel
zřízena	zřídit	k5eAaPmNgFnS	zřídit
samostatná	samostatný	k2eAgFnSc1d1	samostatná
michelská	michelský	k2eAgFnSc1d1	Michelská
farnost	farnost	k1gFnSc1	farnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
kněžím	kněz	k1gMnPc3	kněz
patřil	patřit	k5eAaImAgInS	patřit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
pozdější	pozdní	k2eAgMnSc1d2	pozdější
kardinál	kardinál	k1gMnSc1	kardinál
a	a	k8xC	a
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
Josef	Josef	k1gMnSc1	Josef
Beran	Beran	k1gMnSc1	Beran
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pastorační	pastorační	k2eAgFnSc2d1	pastorační
činnosti	činnost	k1gFnSc2	činnost
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k8xC	i
Útulku	útulek	k1gInSc2	útulek
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
hluchoněmé	hluchoněmá	k1gFnPc4	hluchoněmá
v	v	k7c6	v
Krči	Krč	k1gFnSc6	Krč
<g/>
.	.	kIx.	.
<g/>
Kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
osmiúhelníku	osmiúhelník	k1gInSc2	osmiúhelník
s	s	k7c7	s
presbytářem	presbytář	k1gInSc7	presbytář
a	a	k8xC	a
předsíní	předsíň	k1gFnSc7	předsíň
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dvou	dva	k4xCgInPc2	dva
obdélných	obdélný	k2eAgInPc2d1	obdélný
přístavků	přístavek	k1gInPc2	přístavek
má	mít	k5eAaImIp3nS	mít
neobyčejně	obyčejně	k6eNd1	obyčejně
silné	silný	k2eAgFnPc4d1	silná
zdi	zeď	k1gFnPc4	zeď
<g/>
,	,	kIx,	,
sanktusovou	sanktusový	k2eAgFnSc4d1	sanktusová
věžičku	věžička	k1gFnSc4	věžička
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
barokní	barokní	k2eAgInPc1d1	barokní
štíty	štít	k1gInPc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
oltářem	oltář	k1gInSc7	oltář
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
průčelí	průčelí	k1gNnSc6	průčelí
je	být	k5eAaImIp3nS	být
vytesán	vytesán	k2eAgInSc1d1	vytesán
erb	erb	k1gInSc1	erb
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
,	,	kIx,	,
vsazený	vsazený	k2eAgInSc4d1	vsazený
do	do	k7c2	do
římsy	římsa	k1gFnSc2	římsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hřbitov	hřbitov	k1gInSc4	hřbitov
===	===	k?	===
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
již	již	k6eAd1	již
nedostačoval	dostačovat	k5eNaImAgMnS	dostačovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
obcí	obec	k1gFnPc2	obec
Michle	Michl	k1gMnSc5	Michl
založen	založit	k5eAaPmNgInS	založit
hřbitov	hřbitov	k1gInSc1	hřbitov
nový	nový	k2eAgInSc1d1	nový
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Krč	Krč	k1gFnSc1	Krč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
hřbitovu	hřbitov	k1gInSc3	hřbitov
původně	původně	k6eAd1	původně
také	také	k9	také
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interiér	interiér	k1gInSc1	interiér
==	==	k?	==
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
různorodé	různorodý	k2eAgNnSc1d1	různorodé
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
mariánského	mariánský	k2eAgInSc2d1	mariánský
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
olejomalby	olejomalba	k1gFnPc1	olejomalba
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oltářní	oltářní	k2eAgFnSc6d1	oltářní
přepážce	přepážka	k1gFnSc6	přepážka
jsou	být	k5eAaImIp3nP	být
vztyčeny	vztyčen	k2eAgFnPc1d1	vztyčena
dvě	dva	k4xCgFnPc1	dva
barokní	barokní	k2eAgFnPc1d1	barokní
sochy	socha	k1gFnPc1	socha
českých	český	k2eAgMnPc2d1	český
patronů	patron	k1gMnPc2	patron
<g/>
:	:	kIx,	:
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
ve	v	k7c6	v
zbroji	zbroj	k1gFnSc6	zbroj
a	a	k8xC	a
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
,	,	kIx,	,
polychromované	polychromovaný	k2eAgFnPc1d1	polychromovaná
dřevořezby	dřevořezba	k1gFnPc1	dřevořezba
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgInP	být
přeneseny	přenést	k5eAaPmNgInP	přenést
z	z	k7c2	z
postranních	postranní	k2eAgInPc2d1	postranní
oltářů	oltář	k1gInPc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
postranním	postranní	k2eAgInSc6d1	postranní
oltáři	oltář	k1gInSc6	oltář
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
rámu	rám	k1gInSc6	rám
s	s	k7c7	s
andílčími	andílčí	k2eAgFnPc7d1	andílčí
hlavičkami	hlavička	k1gFnPc7	hlavička
stojí	stát	k5eAaImIp3nS	stát
kopie	kopie	k1gFnSc1	kopie
sochy	socha	k1gFnSc2	socha
Michelské	michelský	k2eAgFnSc2d1	Michelská
madony	madona	k1gFnSc2	madona
<g/>
,	,	kIx,	,
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Michelská	michelský	k2eAgFnSc1d1	Michelská
madona	madona	k1gFnSc1	madona
===	===	k?	===
</s>
</p>
<p>
<s>
Nejméně	málo	k6eAd3	málo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kostele	kostel	k1gInSc6	kostel
uctívána	uctíván	k2eAgFnSc1d1	uctívána
milostná	milostný	k2eAgFnSc1d1	milostná
soška	soška	k1gFnSc1	soška
Michelské	michelský	k2eAgFnSc2d1	Michelská
madony	madona	k1gFnSc2	madona
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
děl	dělo	k1gNnPc2	dělo
českého	český	k2eAgNnSc2d1	české
vrcholně	vrcholně	k6eAd1	vrcholně
gotického	gotický	k2eAgNnSc2d1	gotické
sochařství	sochařství	k1gNnSc2	sochařství
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
(	(	kIx(	(
<g/>
jižním	jižní	k2eAgInSc6d1	jižní
<g/>
)	)	kIx)	)
bočním	boční	k2eAgInSc6d1	boční
oltáři	oltář	k1gInSc6	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
originálu	originál	k1gInSc3	originál
je	být	k5eAaImIp3nS	být
pozlacená	pozlacený	k2eAgFnSc1d1	pozlacená
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
obě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
insignie	insignie	k1gFnPc4	insignie
Královny	královna	k1gFnSc2	královna
andělů	anděl	k1gMnPc2	anděl
(	(	kIx(	(
<g/>
korunku	korunka	k1gFnSc4	korunka
a	a	k8xC	a
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
kopie	kopie	k1gFnSc1	kopie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevořezba	dřevořezba	k1gFnSc1	dřevořezba
stojící	stojící	k2eAgFnSc2d1	stojící
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Královny	královna	k1gFnSc2	královna
s	s	k7c7	s
oblečeným	oblečený	k2eAgInSc7d1	oblečený
Ježíškem	ježíšek	k1gInSc7	ježíšek
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
paži	paže	k1gFnSc6	paže
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
zbytek	zbytek	k1gInSc4	zbytek
kruhového	kruhový	k2eAgNnSc2d1	kruhové
dýnka	dýnko	k1gNnSc2	dýnko
po	po	k7c6	po
královské	královský	k2eAgFnSc6d1	královská
koruně	koruna	k1gFnSc6	koruna
s	s	k7c7	s
dekorativně	dekorativně	k6eAd1	dekorativně
řezanými	řezaný	k2eAgFnPc7d1	řezaná
loknami	lokna	k1gFnPc7	lokna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
měla	mít	k5eAaImAgFnS	mít
ona	onen	k3xDgFnSc1	onen
i	i	k8xC	i
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
vysokou	vysoký	k2eAgFnSc4d1	vysoká
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
korunku	korunka	k1gFnSc4	korunka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
madona	madona	k1gFnSc1	madona
držela	držet	k5eAaImAgFnS	držet
královské	královský	k2eAgNnSc4d1	královské
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
zdobí	zdobit	k5eAaImIp3nS	zdobit
přemíra	přemíra	k1gFnSc1	přemíra
schematických	schematický	k2eAgFnPc2d1	schematická
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
řezaných	řezaný	k2eAgInPc2d1	řezaný
záhybů	záhyb	k1gInPc2	záhyb
oděvu	oděv	k1gInSc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
vyřezána	vyřezat	k5eAaPmNgFnS	vyřezat
z	z	k7c2	z
hruškového	hruškový	k2eAgNnSc2d1	hruškové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k2eAgInSc4d1	komorní
formát	formát	k1gInSc4	formát
120	[number]	k4	120
cm	cm	kA	cm
výšky	výška	k1gFnSc2	výška
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
uvnitř	uvnitř	k7c2	uvnitř
oltářního	oltářní	k2eAgInSc2d1	oltářní
výklenku	výklenek	k1gInSc2	výklenek
nebo	nebo	k8xC	nebo
oltářní	oltářní	k2eAgFnSc2d1	oltářní
archy	archa	k1gFnSc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Polychromie	polychromie	k1gFnSc1	polychromie
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gInSc1	originál
sošky	soška	k1gFnSc2	soška
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
zakoupen	zakoupit	k5eAaPmNgInS	zakoupit
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
inventárním	inventární	k2eAgNnSc7d1	inventární
číslem	číslo	k1gNnSc7	číslo
P	P	kA	P
701	[number]	k4	701
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
v	v	k7c6	v
Anežském	anežský	k2eAgInSc6d1	anežský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
michelského	michelský	k2eAgInSc2d1	michelský
kostela	kostel	k1gInSc2	kostel
byla	být	k5eAaImAgFnS	být
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
kopie	kopie	k1gFnSc1	kopie
této	tento	k3xDgFnSc2	tento
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
takzvaného	takzvaný	k2eAgMnSc2d1	takzvaný
Mistra	mistr	k1gMnSc2	mistr
Michelské	michelský	k2eAgFnSc2d1	Michelská
madony	madona	k1gFnSc2	madona
či	či	k8xC	či
jeho	on	k3xPp3gInSc2	on
vlivu	vliv	k1gInSc2	vliv
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
asi	asi	k9	asi
deset	deset	k4xCc1	deset
dřevořezeb	dřevořezba	k1gFnPc2	dřevořezba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1320	[number]	k4	1320
<g/>
–	–	k?	–
<g/>
1360	[number]	k4	1360
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc4	Florián
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
augustiniánů	augustinián	k1gMnPc2	augustinián
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Sankt	Sankt	k1gInSc1	Sankt
Florianu	Florian	k1gMnSc3	Florian
<g/>
,	,	kIx,	,
soška	soška	k1gFnSc1	soška
Broumovské	broumovský	k2eAgFnSc2d1	Broumovská
madony	madona	k1gFnSc2	madona
<g/>
,	,	kIx,	,
madonka	madonka	k1gFnSc1	madonka
z	z	k7c2	z
Dýšiny	Dýšina	k1gFnSc2	Dýšina
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
madona	madona	k1gFnSc1	madona
z	z	k7c2	z
Hrabové	hrabový	k2eAgFnSc2d1	Hrabová
<g/>
,	,	kIx,	,
Prostějovská	prostějovský	k2eAgFnSc1d1	prostějovská
madona	madona	k1gFnSc1	madona
<g/>
,	,	kIx,	,
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
madona	madona	k1gFnSc1	madona
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
korpusy	korpus	k1gInPc4	korpus
Krista	Krista	k1gFnSc1	Krista
–	–	k?	–
od	od	k7c2	od
karmelitek	karmelitka	k1gFnPc2	karmelitka
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
a	a	k8xC	a
z	z	k7c2	z
Ostřice	ostřice	k1gFnSc2	ostřice
<g/>
;	;	kIx,	;
snad	snad	k9	snad
také	také	k9	také
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
objevená	objevený	k2eAgFnSc1d1	objevená
madona	madona	k1gFnSc1	madona
na	na	k7c6	na
lvu	lev	k1gInSc6	lev
z	z	k7c2	z
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
,	,	kIx,	,
zakoupená	zakoupený	k2eAgFnSc1d1	zakoupená
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
působišti	působiště	k1gNnSc3	působiště
Mistra	mistr	k1gMnSc2	mistr
Michelské	michelský	k2eAgFnSc2d1	Michelská
madony	madona	k1gFnSc2	madona
historikové	historik	k1gMnPc1	historik
umění	umění	k1gNnSc2	umění
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přichází	přicházet	k5eAaImIp3nS	přicházet
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
nebo	nebo	k8xC	nebo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varhany	varhany	k1gInPc4	varhany
==	==	k?	==
</s>
</p>
<p>
<s>
Varhany	varhany	k1gFnPc1	varhany
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
zábradlí	zábradlí	k1gNnSc6	zábradlí
kůru	kůr	k1gInSc2	kůr
<g/>
.	.	kIx.	.
</s>
<s>
Hrací	hrací	k2eAgInSc1d1	hrací
stůl	stůl	k1gInSc1	stůl
je	být	k5eAaImIp3nS	být
vestavěn	vestavět	k5eAaPmNgInS	vestavět
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
skříně	skříň	k1gFnSc2	skříň
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gInPc4	varhany
postavila	postavit	k5eAaPmAgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
pražská	pražský	k2eAgFnSc1d1	Pražská
varhanářská	varhanářský	k2eAgFnSc1d1	varhanářská
firma	firma	k1gFnSc1	firma
Josef	Josef	k1gMnSc1	Josef
Rejna	Rejn	k1gInSc2	Rejn
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Varhany	varhany	k1gInPc1	varhany
jsou	být	k5eAaImIp3nP	být
jednomanuálové	jednomanuálový	k2eAgFnPc4d1	jednomanuálový
s	s	k7c7	s
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
trakturou	traktura	k1gFnSc7	traktura
a	a	k8xC	a
zásuvkovou	zásuvkový	k2eAgFnSc7d1	zásuvková
vzdušnicí	vzdušnice	k1gFnSc7	vzdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgFnP	mít
6	[number]	k4	6
rejstříků	rejstřík	k1gInPc2	rejstřík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
rejstříky	rejstřík	k1gInPc4	rejstřík
na	na	k7c6	na
kuželkové	kuželkový	k2eAgFnSc6d1	Kuželková
vzdušnici	vzdušnice	k1gFnSc6	vzdušnice
<g/>
,	,	kIx,	,
napojené	napojený	k2eAgInPc4d1	napojený
na	na	k7c4	na
stávající	stávající	k2eAgInSc4d1	stávající
nástroj	nástroj	k1gInSc4	nástroj
pneumatickou	pneumatický	k2eAgFnSc7d1	pneumatická
trakturou	traktura	k1gFnSc7	traktura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
varhanům	varhany	k1gInPc3	varhany
byla	být	k5eAaImAgFnS	být
také	také	k9	také
přidána	přidán	k2eAgFnSc1d1	přidána
pedálová	pedálový	k2eAgFnSc1d1	pedálová
spojka	spojka	k1gFnSc1	spojka
a	a	k8xC	a
tremolo	tremolo	k1gNnSc1	tremolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dispozice	dispozice	k1gFnSc1	dispozice
varhanManuál	varhanManuál	k1gInSc1	varhanManuál
C-f	C	k1gFnSc1	C-f
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
54	[number]	k4	54
tónů	tón	k1gInPc2	tón
</s>
</p>
<p>
<s>
Principál	principál	k1gInSc1	principál
8	[number]	k4	8
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
Kryt	kryt	k1gInSc1	kryt
8	[number]	k4	8
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
Salicionál	salicionál	k1gInSc1	salicionál
8	[number]	k4	8
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
Flétna	flétna	k1gFnSc1	flétna
4	[number]	k4	4
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
Oktáva	oktáva	k1gFnSc1	oktáva
4	[number]	k4	4
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
Flétna	flétna	k1gFnSc1	flétna
šustivá	šustivý	k2eAgFnSc1d1	šustivá
2	[number]	k4	2
<g/>
́	́	k?	́
<g/>
(	(	kIx(	(
<g/>
přidaná	přidaná	k1gFnSc1	přidaná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mixtura	mixtura	k1gFnSc1	mixtura
1	[number]	k4	1
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
́	́	k?	́
3	[number]	k4	3
<g/>
×	×	k?	×
(	(	kIx(	(
<g/>
přidaná	přidaná	k1gFnSc1	přidaná
<g/>
)	)	kIx)	)
<g/>
Pedál	pedál	k1gInSc1	pedál
C-c	C	k1gFnSc2	C-c
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
25	[number]	k4	25
tónů	tón	k1gInPc2	tón
</s>
</p>
<p>
<s>
Subbass	Subbass	k6eAd1	Subbass
16	[number]	k4	16
<g/>
́	́	k?	́
</s>
</p>
<p>
<s>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
P	P	kA	P
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
tvoří	tvořit	k5eAaImIp3nS	tvořit
farnost	farnost	k1gFnSc4	farnost
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc3	Assis
v	v	k7c6	v
Krči	Krč	k1gFnSc6	Krč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
ji	on	k3xPp3gFnSc4	on
spravují	spravovat	k5eAaImIp3nP	spravovat
Misionáři	misionář	k1gMnPc1	misionář
Identes	Identes	k1gInSc4	Identes
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
administrátorem	administrátor	k1gMnSc7	administrátor
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Antonín	Antonín	k1gMnSc1	Antonín
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
MId	MId	k1gMnSc1	MId
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
kněz	kněz	k1gMnSc1	kněz
Alberto	Alberta	k1gFnSc5	Alberta
Giralda	Girald	k1gMnSc4	Girald
Cid	Cid	k1gMnSc4	Cid
<g/>
,	,	kIx,	,
MId	MId	k1gMnSc4	MId
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
obklopen	obklopit	k5eAaPmNgInS	obklopit
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
kvůli	kvůli	k7c3	kvůli
budovanému	budovaný	k2eAgNnSc3d1	budované
BB	BB	kA	BB
Centru	centr	k1gInSc2	centr
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Brumlovky	Brumlovka	k1gFnSc2	Brumlovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provoz	provoz	k1gInSc1	provoz
po	po	k7c6	po
nedokonale	dokonale	k6eNd1	dokonale
řešené	řešený	k2eAgFnSc6d1	řešená
silnici	silnice	k1gFnSc6	silnice
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
způsobil	způsobit	k5eAaPmAgInS	způsobit
poškození	poškození	k1gNnSc4	poškození
obvodových	obvodový	k2eAgFnPc2d1	obvodová
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
této	tento	k3xDgFnSc3	tento
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
a	a	k8xC	a
upravení	upravení	k1gNnSc3	upravení
ulice	ulice	k1gFnSc2	ulice
Baarovy	Baarův	k2eAgFnSc2d1	Baarova
však	však	k9	však
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
prasklinami	prasklina	k1gFnPc7	prasklina
ve	v	k7c6	v
zdích	zeď	k1gFnPc6	zeď
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PROFOUS	PROFOUS	kA	PROFOUS
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
;	;	kIx,	;
Místní	místní	k2eAgNnPc1d1	místní
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
písmena	písmeno	k1gNnPc1	písmeno
M-Ř	M-Ř	k1gFnSc1	M-Ř
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTAL	kutat	k5eAaImAgMnS	kutat
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
;	;	kIx,	;
České	český	k2eAgNnSc1d1	české
gotické	gotický	k2eAgNnSc1d1	gotické
sochařství	sochařství	k1gNnSc1	sochařství
1350	[number]	k4	1350
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
HLOBIL	HLOBIL	kA	HLOBIL
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
;	;	kIx,	;
Mistr	mistr	k1gMnSc1	mistr
Michelské	michelský	k2eAgFnSc2d1	Michelská
madony	madona	k1gFnSc2	madona
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Král	Král	k1gMnSc1	Král
který	který	k3yIgMnSc1	který
létal	létat	k5eAaImAgMnS	létat
<g/>
.	.	kIx.	.
</s>
<s>
Moravsko-slezské	moravskolezský	k2eAgNnSc4d1	moravsko-slezské
pomezí	pomezí	k1gNnSc4	pomezí
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
prostoru	prostor	k1gInSc2	prostor
doby	doba	k1gFnSc2	doba
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Majer	Majer	k1gMnSc1	Majer
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
s.	s.	k?	s.
433	[number]	k4	433
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnPc1d1	pamětní
knihy	kniha	k1gFnPc1	kniha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
rukopisy	rukopis	k1gInPc1	rukopis
latinské	latinský	k2eAgInPc1d1	latinský
<g/>
,	,	kIx,	,
německé	německý	k2eAgInPc1d1	německý
a	a	k8xC	a
české	český	k2eAgInPc1d1	český
<g/>
;	;	kIx,	;
Archiv	archiv	k1gInSc1	archiv
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc3	Assis
(	(	kIx(	(
<g/>
Krč	Krč	k1gFnSc1	Krč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Praha-Michle	Praha-Michle	k1gFnSc2	Praha-Michle
</s>
</p>
<p>
<s>
Narození	narození	k1gNnSc1	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc6	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Michle	Michl	k1gMnSc5	Michl
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Bývalé	bývalý	k2eAgFnPc1d1	bývalá
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
