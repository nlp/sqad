<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Západočeského	západočeský	k2eAgInSc2d1	západočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
západě	západ	k1gInSc6	západ
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejzápadnějším	západní	k2eAgInSc7d3	nejzápadnější
krajem	kraj	k1gInSc7	kraj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Ústeckým	ústecký	k2eAgInSc7d1	ústecký
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Plzeňským	plzeňský	k2eAgInSc7d1	plzeňský
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Sasko	Sasko	k1gNnSc4	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
jako	jako	k8xC	jako
vyšší	vysoký	k2eAgInSc1d2	vyšší
územní	územní	k2eAgInSc1d1	územní
samosprávný	samosprávný	k2eAgInSc1d1	samosprávný
celek	celek	k1gInSc1	celek
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c6	na
území	území	k1gNnSc6	území
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Západočeského	západočeský	k2eAgInSc2d1	západočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
kraje	kraj	k1gInSc2	kraj
jsou	být	k5eAaImIp3nP	být
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
obcí	obec	k1gFnPc2	obec
i	i	k8xC	i
obyvatel	obyvatel	k1gMnPc2	obyvatel
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
malé	malý	k2eAgInPc4d1	malý
kraje	kraj	k1gInPc4	kraj
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
(	(	kIx(	(
<g/>
3	[number]	k4	3
314	[number]	k4	314
km2	km2	k4	km2
<g/>
)	)	kIx)	)
zabírá	zabírat	k5eAaImIp3nS	zabírat
4,25	[number]	k4	4,25
%	%	kIx~	%
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
třetí	třetí	k4xOgMnSc1	třetí
nejmenší	malý	k2eAgInSc1d3	nejmenší
kraj	kraj	k1gInSc1	kraj
hned	hned	k6eAd1	hned
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Karlovarský	karlovarský	k2eAgInSc4d1	karlovarský
kraj	kraj	k1gInSc4	kraj
nejmenším	malý	k2eAgInSc7d3	nejmenší
krajem	kraj	k1gInSc7	kraj
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
kraji	kraj	k1gInPc7	kraj
na	na	k7c6	na
základě	základ	k1gInSc6	základ
článku	článek	k1gInSc2	článek
99	[number]	k4	99
a	a	k8xC	a
následujících	následující	k2eAgInPc2d1	následující
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
347	[number]	k4	347
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
vyšších	vysoký	k2eAgInPc2d2	vyšší
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
názvy	název	k1gInPc4	název
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vymezení	vymezení	k1gNnSc4	vymezení
výčtem	výčet	k1gInSc7	výčet
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
území	území	k1gNnSc1	území
okresů	okres	k1gInPc2	okres
definuje	definovat	k5eAaBmIp3nS	definovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
564	[number]	k4	564
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgInPc4d2	vyšší
územní	územní	k2eAgInPc4d1	územní
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
stanoví	stanovit	k5eAaPmIp3nS	stanovit
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
kraje	kraj	k1gInPc1	kraj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
definitivně	definitivně	k6eAd1	definitivně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
kompetence	kompetence	k1gFnSc2	kompetence
získaly	získat	k5eAaPmAgFnP	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
129	[number]	k4	129
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
krajích	kraj	k1gInPc6	kraj
(	(	kIx(	(
<g/>
krajské	krajský	k2eAgNnSc4d1	krajské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
jejich	jejich	k3xOp3gNnPc2	jejich
nově	nově	k6eAd1	nově
zřízených	zřízený	k2eAgNnPc2d1	zřízené
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
krajské	krajský	k2eAgNnSc1d1	krajské
členění	členění	k1gNnSc1	členění
je	být	k5eAaImIp3nS	být
obdobné	obdobný	k2eAgNnSc1d1	obdobné
krajům	kraj	k1gInPc3	kraj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zřízených	zřízený	k2eAgFnPc2d1	zřízená
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
280	[number]	k4	280
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Převedení	převedení	k1gNnSc1	převedení
části	část	k1gFnSc2	část
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
obce	obec	k1gFnSc2	obec
Valeč	Valeč	k1gInSc1	Valeč
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
ležícího	ležící	k2eAgMnSc2d1	ležící
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
újezdu	újezd	k1gInSc6	újezd
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
Karlovarského	karlovarský	k2eAgMnSc2d1	karlovarský
do	do	k7c2	do
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
se	s	k7c7	s
zásadním	zásadní	k2eAgInSc7d1	zásadní
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
této	tento	k3xDgFnSc2	tento
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
záměr	záměr	k1gInSc1	záměr
převést	převést	k5eAaPmF	převést
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
ankety	anketa	k1gFnSc2	anketa
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
na	na	k7c4	na
Ústecké	ústecký	k2eAgNnSc4d1	ústecké
území	území	k1gNnSc4	území
o	o	k7c6	o
7	[number]	k4	7
hektarech	hektar	k1gInPc6	hektar
se	s	k7c7	s
3	[number]	k4	3
osadami	osada	k1gFnPc7	osada
(	(	kIx(	(
<g/>
Bukovina	Bukovina	k1gFnSc1	Bukovina
<g/>
,	,	kIx,	,
Střelnice	střelnice	k1gFnSc1	střelnice
a	a	k8xC	a
Ořkov	Ořkov	k1gInSc1	Ořkov
<g/>
)	)	kIx)	)
a	a	k8xC	a
20	[number]	k4	20
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
spádově	spádově	k6eAd1	spádově
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
Podbořanům	Podbořany	k1gInPc3	Podbořany
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
po	po	k7c6	po
počátečním	počáteční	k2eAgInSc6d1	počáteční
nesouhlasu	nesouhlas	k1gInSc6	nesouhlas
nakonec	nakonec	k6eAd1	nakonec
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
43,1	[number]	k4	43,1
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
oblast	oblast	k1gFnSc1	oblast
náleží	náležet	k5eAaImIp3nS	náležet
ke	k	k7c3	k
Krušnohorské	krušnohorský	k2eAgFnSc3d1	Krušnohorská
soustavě	soustava	k1gFnSc3	soustava
provincie	provincie	k1gFnSc2	provincie
České	český	k2eAgFnSc2d1	Česká
vysočiny	vysočina	k1gFnSc2	vysočina
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Klínovecká	klínovecký	k2eAgFnSc1d1	Klínovecká
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smrčiny	Smrčiny	k1gFnPc1	Smrčiny
(	(	kIx(	(
<g/>
Ašská	ašský	k2eAgFnSc1d1	Ašská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
Hazlovská	Hazlovský	k2eAgFnSc1d1	Hazlovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
a	a	k8xC	a
Chebská	chebský	k2eAgFnSc1d1	Chebská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chebská	chebský	k2eAgFnSc1d1	Chebská
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
Doupovské	Doupovský	k2eAgFnPc1d1	Doupovská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Karlovarská	karlovarský	k2eAgFnSc1d1	Karlovarská
vrchovina	vrchovina	k1gFnSc1	vrchovina
(	(	kIx(	(
<g/>
Slavkovský	slavkovský	k2eAgInSc1d1	slavkovský
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Tepelská	tepelský	k2eAgFnSc1d1	Tepelská
vrchovina	vrchovina	k1gFnSc1	vrchovina
a	a	k8xC	a
Bezdružická	Bezdružický	k2eAgFnSc1d1	Bezdružická
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
protéká	protékat	k5eAaImIp3nS	protékat
krajem	krajem	k6eAd1	krajem
řeka	řeka	k1gFnSc1	řeka
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odvádí	odvádět	k5eAaImIp3nS	odvádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
Klínovce	Klínovec	k1gInSc2	Klínovec
1	[number]	k4	1
244	[number]	k4	244
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgNnPc3d3	nejnižší
je	on	k3xPp3gNnPc4	on
hladina	hladina	k1gFnSc1	hladina
Ohře	Ohře	k1gFnSc2	Ohře
320	[number]	k4	320
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
v	v	k7c4	v
Boči	boča	k1gMnPc4	boča
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
mírně	mírně	k6eAd1	mírně
teplá	teplý	k2eAgFnSc1d1	teplá
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
chladná	chladný	k2eAgFnSc1d1	chladná
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
Krušných	krušný	k2eAgMnPc2d1	krušný
hor.	hor.	k?	hor.
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
kraj	kraj	k1gInSc1	kraj
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
okresy	okres	k1gInPc4	okres
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
okresními	okresní	k2eAgInPc7d1	okresní
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
okres	okres	k1gInSc4	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
nejmenším	malý	k2eAgInSc7d3	nejmenší
okresem	okres	k1gInSc7	okres
je	být	k5eAaImIp3nS	být
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
.	.	kIx.	.
</s>
<s>
Okresy	okres	k1gInPc1	okres
jsou	být	k5eAaImIp3nP	být
územními	územní	k2eAgInPc7d1	územní
obvody	obvod	k1gInPc7	obvod
některých	některý	k3yIgFnPc2	některý
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgInPc1d1	okresní
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přenesené	přenesený	k2eAgFnSc2d1	přenesená
působnosti	působnost	k1gFnSc2	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Aš	Aš	k1gInSc1	Aš
Cheb	Cheb	k1gInSc1	Cheb
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
Lázně	lázeň	k1gFnSc2	lázeň
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
Ostrov	ostrov	k1gInSc4	ostrov
Kraslice	kraslice	k1gFnSc1	kraslice
Sokolov	Sokolov	k1gInSc1	Sokolov
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
299	[number]	k4	299
293	[number]	k4	293
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
okresem	okres	k1gInSc7	okres
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
žije	žít	k5eAaImIp3nS	žít
117	[number]	k4	117
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
zbývajících	zbývající	k2eAgInPc6d1	zbývající
okresech	okres	k1gInPc6	okres
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hodnota	hodnota	k1gFnSc1	hodnota
120	[number]	k4	120
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
77	[number]	k4	77
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
osídlení	osídlení	k1gNnSc2	osídlení
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
tzv.	tzv.	kA	tzv.
středisková	střediskový	k2eAgFnSc1d1	středisková
sídelní	sídelní	k2eAgFnSc1d1	sídelní
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
uplatňování	uplatňování	k1gNnSc1	uplatňování
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
vylidnění	vylidnění	k1gNnSc3	vylidnění
venkovského	venkovský	k2eAgInSc2d1	venkovský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
k	k	k7c3	k
destrukci	destrukce	k1gFnSc3	destrukce
ekonomického	ekonomický	k2eAgMnSc2d1	ekonomický
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
prostředí	prostředí	k1gNnSc2	prostředí
venkovských	venkovský	k2eAgNnPc2d1	venkovské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Chebské	chebský	k2eAgFnSc2d1	Chebská
a	a	k8xC	a
Sokolovské	sokolovský	k2eAgFnSc2d1	Sokolovská
pánve	pánev	k1gFnSc2	pánev
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
urbanizovaná	urbanizovaný	k2eAgFnSc1d1	urbanizovaná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
koncentrovány	koncentrován	k2eAgFnPc1d1	koncentrována
výrobní	výrobní	k2eAgFnPc1d1	výrobní
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgFnPc1d1	okrajová
horské	horský	k2eAgFnPc1d1	horská
polohy	poloha	k1gFnPc1	poloha
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
osídleny	osídlit	k5eAaPmNgFnP	osídlit
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
rekreační	rekreační	k2eAgFnSc2d1	rekreační
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
3	[number]	k4	3
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
a	a	k8xC	a
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současného	současný	k2eAgNnSc2d1	současné
administrativního	administrativní	k2eAgNnSc2d1	administrativní
členění	členění	k1gNnSc2	členění
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
celkem	celkem	k6eAd1	celkem
132	[number]	k4	132
obcí	obec	k1gFnPc2	obec
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgFnPc2d1	tvořená
jednou	jeden	k4xCgFnSc7	jeden
až	až	k9	až
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
dělá	dělat	k5eAaImIp3nS	dělat
kraj	kraj	k1gInSc1	kraj
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
počtem	počet	k1gInSc7	počet
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rozloha	rozloha	k1gFnSc1	rozloha
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
25,1	[number]	k4	25,1
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
žilo	žít	k5eAaImAgNnS	žít
2306	[number]	k4	2306
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc7d3	veliký
obcí	obec	k1gFnSc7	obec
kraje	kraj	k1gInSc2	kraj
jsou	být	k5eAaImIp3nP	být
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
17	[number]	k4	17
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
499	[number]	k4	499
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
47	[number]	k4	47
%	%	kIx~	%
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
cca	cca	kA	cca
5,5	[number]	k4	5,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
44	[number]	k4	44
obcích	obec	k1gFnPc6	obec
od	od	k7c2	od
500	[number]	k4	500
do	do	k7c2	do
1999	[number]	k4	1999
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
14	[number]	k4	14
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
26	[number]	k4	26
obcích	obec	k1gFnPc6	obec
od	od	k7c2	od
2000	[number]	k4	2000
do	do	k7c2	do
9999	[number]	k4	9999
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
do	do	k7c2	do
10	[number]	k4	10
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
cca	cca	kA	cca
44	[number]	k4	44
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
roku	rok	k1gInSc3	rok
1998	[number]	k4	1998
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
13	[number]	k4	13
procentních	procentní	k2eAgInPc2d1	procentní
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
mírně	mírně	k6eAd1	mírně
klesající	klesající	k2eAgInSc1d1	klesající
trend	trend	k1gInSc1	trend
úbytku	úbytek	k1gInSc2	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
304	[number]	k4	304
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
necelá	celý	k2eNgFnSc1d1	necelá
3	[number]	k4	3
%	%	kIx~	%
z	z	k7c2	z
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Stárnutí	stárnutí	k1gNnSc3	stárnutí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
projevilo	projevit	k5eAaPmAgNnS	projevit
přírůstkem	přírůstek	k1gInSc7	přírůstek
2261	[number]	k4	2261
seniorů	senior	k1gMnPc2	senior
(	(	kIx(	(
<g/>
65	[number]	k4	65
<g/>
letých	letý	k2eAgMnPc2d1	letý
a	a	k8xC	a
starších	starý	k2eAgMnPc2d2	starší
<g/>
)	)	kIx)	)
a	a	k8xC	a
úbytkem	úbytek	k1gInSc7	úbytek
5121	[number]	k4	5121
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
9	[number]	k4	9
%	%	kIx~	%
juniorů	junior	k1gMnPc2	junior
(	(	kIx(	(
<g/>
mladších	mladý	k2eAgNnPc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
koeficient	koeficient	k1gInSc1	koeficient
stáří	stáří	k1gNnSc2	stáří
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
seniorů	senior	k1gMnPc2	senior
k	k	k7c3	k
počtu	počet	k1gInSc2	počet
juniorů	junior	k1gMnPc2	junior
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
roku	rok	k1gInSc3	rok
1996	[number]	k4	1996
o	o	k7c4	o
10,3	[number]	k4	10,3
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
na	na	k7c4	na
70,6	[number]	k4	70,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rozvodů	rozvod	k1gInPc2	rozvod
na	na	k7c4	na
100	[number]	k4	100
sňatků	sňatek	k1gInPc2	sňatek
řadí	řadit	k5eAaImIp3nS	řadit
kraj	kraj	k1gInSc1	kraj
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
před	před	k7c4	před
kraj	kraj	k1gInSc4	kraj
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
a	a	k8xC	a
Ústecký	ústecký	k2eAgInSc4d1	ústecký
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
relativní	relativní	k2eAgInSc4d1	relativní
počet	počet	k1gInSc4	počet
potratů	potrat	k1gInPc2	potrat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
republikový	republikový	k2eAgInSc1d1	republikový
průměr	průměr	k1gInSc1	průměr
o	o	k7c4	o
2	[number]	k4	2
potraty	potrat	k1gInPc4	potrat
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
hodnotou	hodnota	k1gFnSc7	hodnota
5,2	[number]	k4	5,2
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
republikový	republikový	k2eAgInSc1d1	republikový
průměr	průměr	k1gInSc1	průměr
činí	činit	k5eAaImIp3nS	činit
4,1	[number]	k4	4,1
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
je	být	k5eAaImIp3nS	být
i	i	k9	i
relativní	relativní	k2eAgInSc1d1	relativní
počet	počet	k1gInSc1	počet
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
po	po	k7c6	po
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
mortalita	mortalita	k1gFnSc1	mortalita
(	(	kIx(	(
<g/>
o	o	k7c4	o
6,6	[number]	k4	6,6
%	%	kIx~	%
nižší	nízký	k2eAgInSc1d2	nižší
proti	proti	k7c3	proti
průměru	průměr	k1gInSc3	průměr
za	za	k7c4	za
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předběžných	předběžný	k2eAgInPc2d1	předběžný
výsledků	výsledek	k1gInPc2	výsledek
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
306	[number]	k4	306
799	[number]	k4	799
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
ženy	žena	k1gFnPc4	žena
připadá	připadat	k5eAaPmIp3nS	připadat
50,9	[number]	k4	50,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
národnostní	národnostní	k2eAgFnPc1d1	národnostní
struktury	struktura	k1gFnPc1	struktura
se	se	k3xPyFc4	se
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
hlásilo	hlásit	k5eAaImAgNnS	hlásit
87,2	[number]	k4	87,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
moravské	moravský	k2eAgFnSc2d1	Moravská
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
slovenské	slovenský	k2eAgFnSc2d1	slovenská
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
německé	německý	k2eAgNnSc4d1	německé
2,8	[number]	k4	2,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
k	k	k7c3	k
polské	polský	k2eAgFnSc2d1	polská
0,1	[number]	k4	0,1
%	%	kIx~	%
a	a	k8xC	a
k	k	k7c3	k
romské	romský	k2eAgNnSc4d1	romské
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
doznal	doznat	k5eAaPmAgInS	doznat
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
nárůst	nárůst	k1gInSc1	nárůst
o	o	k7c6	o
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
národnostní	národnostní	k2eAgFnSc6d1	národnostní
struktuře	struktura	k1gFnSc6	struktura
pak	pak	k6eAd1	pak
výrazně	výrazně	k6eAd1	výrazně
ubyl	ubýt	k5eAaPmAgInS	ubýt
podíl	podíl	k1gInSc1	podíl
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
hlásících	hlásící	k2eAgNnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
slovenské	slovenský	k2eAgFnSc2d1	slovenská
(	(	kIx(	(
<g/>
o	o	k7c4	o
4,3	[number]	k4	4,3
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnPc1d1	německá
(	(	kIx(	(
<g/>
-	-	kIx~	-
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravské	moravský	k2eAgFnPc1d1	Moravská
(	(	kIx(	(
<g/>
-	-	kIx~	-
0,6	[number]	k4	0,6
<g/>
)	)	kIx)	)
a	a	k8xC	a
romské	romský	k2eAgMnPc4d1	romský
(	(	kIx(	(
<g/>
-	-	kIx~	-
0,4	[number]	k4	0,4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nárůst	nárůst	k1gInSc1	nárůst
o	o	k7c4	o
3	[number]	k4	3
procentní	procentní	k2eAgInPc4d1	procentní
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
vyznání	vyznání	k1gNnSc3	vyznání
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
20,1	[number]	k4	20,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
75,9	[number]	k4	75,9
%	%	kIx~	%
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
roku	rok	k1gInSc3	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
věřících	věřící	k1gMnPc2	věřící
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c6	o
11,9	[number]	k4	11,9
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podstatnému	podstatný	k2eAgInSc3d1	podstatný
odlivu	odliv	k1gInSc3	odliv
hlásících	hlásící	k2eAgNnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
(	(	kIx(	(
<g/>
o	o	k7c4	o
8,7	[number]	k4	8,7
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
lázeňství	lázeňství	k1gNnSc1	lázeňství
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
významným	významný	k2eAgNnSc7d1	významné
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c6	na
Sokolovsku	Sokolovsko	k1gNnSc6	Sokolovsko
a	a	k8xC	a
kaolinu	kaolin	k1gInSc6	kaolin
na	na	k7c6	na
Karlovarsku	Karlovarsko	k1gNnSc6	Karlovarsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
dopravního	dopravní	k2eAgNnSc2d1	dopravní
hlediska	hledisko	k1gNnSc2	hledisko
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Otevřením	otevření	k1gNnSc7	otevření
hranic	hranice	k1gFnPc2	hranice
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
zejména	zejména	k9	zejména
silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zejména	zejména	k9	zejména
vozů	vůz	k1gInPc2	vůz
nákladních	nákladní	k2eAgInPc2d1	nákladní
s	s	k7c7	s
nepříznivými	příznivý	k2eNgInPc7d1	nepříznivý
dopady	dopad	k1gInPc7	dopad
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
potřebám	potřeba	k1gFnPc3	potřeba
rozvoje	rozvoj	k1gInPc4	rozvoj
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
pokryt	pokryt	k2eAgInSc1d1	pokryt
relativně	relativně	k6eAd1	relativně
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
na	na	k7c6	na
konci	konec	k1gInSc6	konec
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebyla	být	k5eNaImAgFnS	být
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
modernizována	modernizován	k2eAgFnSc1d1	modernizována
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
nádražní	nádražní	k2eAgInPc1d1	nádražní
a	a	k8xC	a
provozní	provozní	k2eAgInPc1d1	provozní
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavebně	stavebně	k6eAd1	stavebně
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavních	hlavní	k2eAgInPc6d1	hlavní
tazích	tag	k1gInPc6	tag
jsou	být	k5eAaImIp3nP	být
prováděny	prováděn	k2eAgFnPc1d1	prováděna
rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
některých	některý	k3yIgInPc2	některý
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
sítě	síť	k1gFnSc2	síť
byla	být	k5eAaImAgFnS	být
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
tratě	trať	k1gFnPc1	trať
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sokolov	Sokolov	k1gInSc1	Sokolov
-	-	kIx~	-
Kraslice	kraslice	k1gFnSc1	kraslice
-	-	kIx~	-
Klingenthal	Klingenthal	k1gMnSc1	Klingenthal
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
-	-	kIx~	-
Potůčky	potůček	k1gInPc1	potůček
-	-	kIx~	-
Johangeorgenstadt	Johangeorgenstadt	k1gInSc1	Johangeorgenstadt
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
-	-	kIx~	-
Schirnding	Schirnding	k1gInSc1	Schirnding
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
(	(	kIx(	(
<g/>
Schirnding-	Schirnding-	k1gFnSc1	Schirnding-
<g/>
)	)	kIx)	)
Cheb	Cheb	k1gInSc1	Cheb
-	-	kIx~	-
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
(	(	kIx(	(
<g/>
-Plzeň	-Plzeň	k1gFnSc1	-Plzeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
III	III	kA	III
<g/>
.	.	kIx.	.
železničního	železniční	k2eAgInSc2d1	železniční
koridoru	koridor	k1gInSc2	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvojkolejnou	dvojkolejný	k2eAgFnSc7d1	dvojkolejná
tratí	trať	k1gFnSc7	trať
Chomutov	Chomutov	k1gInSc1	Chomutov
-	-	kIx~	-
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
napříč	napříč	k7c7	napříč
celým	celý	k2eAgNnSc7d1	celé
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
pak	pak	k6eAd1	pak
odbočují	odbočovat	k5eAaImIp3nP	odbočovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
regionální	regionální	k2eAgFnPc4d1	regionální
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
tato	tento	k3xDgFnSc1	tento
trať	trať	k1gFnSc1	trať
prochází	procházet	k5eAaImIp3nS	procházet
také	také	k9	také
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
nepěkná	pěkný	k2eNgFnSc1d1	nepěkná
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
bude	být	k5eAaImBp3nS	být
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
stržena	strhnout	k5eAaPmNgFnS	strhnout
a	a	k8xC	a
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc7d1	nová
moderní	moderní	k2eAgFnSc7d1	moderní
budovou	budova	k1gFnSc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
tratích	trať	k1gFnPc6	trať
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
ČR	ČR	kA	ČR
značně	značně	k6eAd1	značně
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
provozu	provoz	k1gInSc2	provoz
ještě	ještě	k6eAd1	ještě
citelnější	citelný	k2eAgFnSc1d2	citelnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
útlumu	útlum	k1gInSc3	útlum
těžby	těžba	k1gFnSc2	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
přepravované	přepravovaný	k2eAgFnSc2d1	přepravovaná
zátěže	zátěž	k1gFnSc2	zátěž
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
silnic	silnice	k1gFnPc2	silnice
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poloze	poloha	k1gFnSc3	poloha
kraje	kraj	k1gInSc2	kraj
mají	mít	k5eAaImIp3nP	mít
silnice	silnice	k1gFnPc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
nadregionální	nadregionální	k2eAgInSc4d1	nadregionální
význam	význam	k1gInSc4	význam
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
spoji	spoj	k1gInPc7	spoj
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
i	i	k9	i
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
komunikací	komunikace	k1gFnSc7	komunikace
pro	pro	k7c4	pro
kraj	kraj	k1gInSc4	kraj
je	být	k5eAaImIp3nS	být
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
spojí	spojit	k5eAaPmIp3nS	spojit
kraj	kraj	k1gInSc1	kraj
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
orientuje	orientovat	k5eAaBmIp3nS	orientovat
svoji	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
spíše	spíše	k9	spíše
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
většími	veliký	k2eAgNnPc7d2	veliký
městy	město	k1gNnPc7	město
v	v	k7c6	v
regionu	region	k1gInSc6	region
a	a	k8xC	a
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Aš	Aš	k1gInSc1	Aš
<g/>
,	,	kIx,	,
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místa	místo	k1gNnSc2	místo
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gFnPc4	jejich
spojnice	spojnice	k1gFnPc4	spojnice
a	a	k8xC	a
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
řidším	řídký	k2eAgNnSc7d2	řidší
osídlením	osídlení	k1gNnSc7	osídlení
jsou	být	k5eAaImIp3nP	být
obsluhována	obsluhovat	k5eAaImNgNnP	obsluhovat
pouze	pouze	k6eAd1	pouze
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
lokalitách	lokalita	k1gFnPc6	lokalita
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
obyvatelé	obyvatel	k1gMnPc1	obyvatel
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
zčásti	zčásti	k6eAd1	zčásti
k	k	k7c3	k
migraci	migrace	k1gFnSc3	migrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgNnPc4	tři
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
a	a	k8xC	a
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarské	karlovarský	k2eAgNnSc1d1	Karlovarské
letiště	letiště	k1gNnSc1	letiště
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
letiště	letiště	k1gNnSc2	letiště
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
a	a	k8xC	a
jako	jako	k9	jako
na	na	k7c6	na
jediném	jediné	k1gNnSc6	jediné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
provozují	provozovat	k5eAaImIp3nP	provozovat
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
ruských	ruský	k2eAgNnPc2d1	ruské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
charterové	charterový	k2eAgNnSc1d1	charterové
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
turecké	turecký	k2eAgFnSc2d1	turecká
Antalye	Antaly	k1gFnSc2	Antaly
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
letiště	letiště	k1gNnSc1	letiště
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
největším	veliký	k2eAgNnSc7d3	veliký
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
investovány	investován	k2eAgInPc4d1	investován
značné	značný	k2eAgInPc4d1	značný
prostředky	prostředek	k1gInPc4	prostředek
do	do	k7c2	do
zlepšení	zlepšení	k1gNnSc2	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
cestujících	cestující	k1gMnPc2	cestující
i	i	k8xC	i
do	do	k7c2	do
letecké	letecký	k2eAgFnSc2d1	letecká
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
a	a	k8xC	a
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
jsou	být	k5eAaImIp3nP	být
využívána	využívat	k5eAaPmNgNnP	využívat
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInPc4d1	sportovní
a	a	k8xC	a
rekreační	rekreační	k2eAgNnSc4d1	rekreační
létání	létání	k1gNnSc4	létání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
pěstování	pěstování	k1gNnSc4	pěstování
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
řepky	řepka	k1gFnSc2	řepka
olejky	olejka	k1gFnSc2	olejka
a	a	k8xC	a
obilnin	obilnina	k1gFnPc2	obilnina
-	-	kIx~	-
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
ječmene	ječmen	k1gInSc2	ječmen
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
pak	pak	k6eAd1	pak
i	i	k9	i
lnu	lnout	k5eAaImIp1nS	lnout
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
sekce	sekce	k1gFnSc1	sekce
zemědělství	zemědělství	k1gNnSc2	zemědělství
je	být	k5eAaImIp3nS	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
chovem	chov	k1gInSc7	chov
mléčného	mléčný	k2eAgInSc2d1	mléčný
i	i	k8xC	i
masného	masný	k2eAgInSc2d1	masný
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
půd	půda	k1gFnPc2	půda
převažují	převažovat	k5eAaImIp3nP	převažovat
hnědé	hnědý	k2eAgFnPc1d1	hnědá
půdy	půda	k1gFnPc1	půda
nížin	nížina	k1gFnPc2	nížina
a	a	k8xC	a
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
(	(	kIx(	(
<g/>
hnědozemě	hnědozem	k1gFnSc2	hnědozem
<g/>
)	)	kIx)	)
a	a	k8xC	a
hnědé	hnědý	k2eAgFnSc2d1	hnědá
lesní	lesní	k2eAgFnSc2d1	lesní
půdy	půda	k1gFnSc2	půda
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
kambizemě	kambizemě	k6eAd1	kambizemě
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
stáčírny	stáčírna	k1gFnPc1	stáčírna
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kyselka	kyselka	k1gFnSc1	kyselka
-	-	kIx~	-
Mattoni	Matton	k1gMnPc1	Matton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
výskytem	výskyt	k1gInSc7	výskyt
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sokolovském	sokolovský	k2eAgInSc6d1	sokolovský
hnědouhelném	hnědouhelný	k2eAgInSc6d1	hnědouhelný
revíru	revír	k1gInSc6	revír
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
závody	závod	k1gInPc1	závod
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Sokolově	Sokolův	k2eAgFnSc6d1	Sokolova
a	a	k8xC	a
Vřesové	vřesový	k2eAgFnSc6d1	Vřesová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
známé	známý	k2eAgInPc1d1	známý
sklářské	sklářský	k2eAgInPc1d1	sklářský
podniky	podnik	k1gInPc1	podnik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
značka	značka	k1gFnSc1	značka
Moser	Moser	k1gMnSc1	Moser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Keramický	keramický	k2eAgInSc1d1	keramický
průmysl	průmysl	k1gInSc1	průmysl
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
naleziště	naleziště	k1gNnSc4	naleziště
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
kaolínu	kaolín	k1gInSc2	kaolín
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
Kraslicích	Kraslice	k1gFnPc6	Kraslice
(	(	kIx(	(
<g/>
dechové	dechový	k2eAgInPc1d1	dechový
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lubech	lub	k1gInPc6	lub
(	(	kIx(	(
<g/>
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
strunné	strunný	k2eAgInPc1d1	strunný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Energetický	energetický	k2eAgInSc1d1	energetický
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
dvěma	dva	k4xCgFnPc7	dva
elektrárnami	elektrárna	k1gFnPc7	elektrárna
v	v	k7c6	v
sokolovské	sokolovský	k2eAgFnSc6d1	Sokolovská
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
Vřesová	vřesový	k2eAgFnSc1d1	Vřesová
<g/>
,	,	kIx,	,
Tisová	tisový	k2eAgFnSc1d1	Tisová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několika	několik	k4yIc7	několik
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
(	(	kIx(	(
<g/>
Březová	březový	k2eAgFnSc1d1	Březová
<g/>
,	,	kIx,	,
Stanovice	Stanovice	k1gFnSc1	Stanovice
<g/>
,	,	kIx,	,
Jesenice	Jesenice	k1gFnSc1	Jesenice
<g/>
)	)	kIx)	)
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
,	,	kIx,	,
alternativními	alternativní	k2eAgInPc7d1	alternativní
zdroji	zdroj	k1gInPc7	zdroj
(	(	kIx(	(
<g/>
větrná	větrný	k2eAgFnSc1d1	větrná
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
Božím	boží	k2eAgInSc6d1	boží
Daru	dar	k1gInSc6	dar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
různě	různě	k6eAd1	různě
přírodní	přírodní	k2eAgInPc4d1	přírodní
útvary	útvar	k1gInPc4	útvar
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc6	svůj
geologické	geologický	k2eAgFnSc6d1	geologická
a	a	k8xC	a
geomorfologické	geomorfologický	k2eAgFnSc6d1	geomorfologická
rozmanitosti	rozmanitost	k1gFnSc6	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
lákadlo	lákadlo	k1gNnSc1	lákadlo
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
turisty	turist	k1gMnPc4	turist
i	i	k8xC	i
lázeňské	lázeňský	k2eAgMnPc4d1	lázeňský
hosty	host	k1gMnPc4	host
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
císařská	císařský	k2eAgFnSc1d1	císařská
falc	falc	k1gFnSc1	falc
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Lokti	loket	k1gInSc6	loket
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Bečově	Bečov	k1gInSc6	Bečov
s	s	k7c7	s
relikviářem	relikviář	k1gInSc7	relikviář
sv.	sv.	kA	sv.
Maura	Maur	k1gMnSc2	Maur
<g/>
.	.	kIx.	.
</s>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
Odrava	Odrava	k1gFnSc1	Odrava
Ohře	Ohře	k1gFnSc1	Ohře
Rolava	Rolava	k1gFnSc1	Rolava
Svatava	Svatava	k1gFnSc1	Svatava
Teplá	teplat	k5eAaImIp3nS	teplat
Chodovský	chodovský	k2eAgInSc4d1	chodovský
potok	potok	k1gInSc4	potok
Libocký	libocký	k2eAgInSc4d1	libocký
potok	potok	k1gInSc4	potok
Plesná	plesný	k2eAgFnSc1d1	Plesná
Sázek	sázka	k1gFnPc2	sázka
Blatenský	blatenský	k2eAgInSc1d1	blatenský
vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
stoka	stoka	k1gFnSc1	stoka
Boč	bočit	k5eAaImRp2nS	bočit
-	-	kIx~	-
čedičová	čedičový	k2eAgFnSc1d1	čedičová
žíla	žíla	k1gFnSc1	žíla
Božídarské	božídarský	k2eAgNnSc1d1	Božídarské
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
Goethova	Goethova	k1gFnSc1	Goethova
skalka	skalka	k1gFnSc1	skalka
Jesenice	Jesenice	k1gFnSc1	Jesenice
-	-	kIx~	-
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Kladská	kladský	k2eAgFnSc1d1	Kladská
-	-	kIx~	-
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
Klínovec	Klínovec	k1gInSc1	Klínovec
-	-	kIx~	-
hora	hora	k1gFnSc1	hora
Komorní	komorní	k2eAgFnSc1d1	komorní
hůrka	hůrka	k1gFnSc1	hůrka
Oceán-	Oceán-	k1gFnSc1	Oceán-
(	(	kIx(	(
<g/>
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
<g/>
)	)	kIx)	)
Přebuz	Přebuz	k1gInSc1	Přebuz
-	-	kIx~	-
vřesoviště	vřesoviště	k1gNnSc1	vřesoviště
Rotava	Rotava	k1gFnSc1	Rotava
-	-	kIx~	-
čedičový	čedičový	k2eAgInSc1d1	čedičový
lom	lom	k1gInSc1	lom
<g />
.	.	kIx.	.
</s>
<s>
Skalky	skalka	k1gFnPc1	skalka
skřítků	skřítek	k1gMnPc2	skřítek
Soos	Soosa	k1gFnPc2	Soosa
-	-	kIx~	-
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
Svatošské	Svatošský	k2eAgFnSc2d1	Svatošská
skály	skála	k1gFnSc2	skála
Tatrovice	Tatrovice	k1gFnSc2	Tatrovice
-	-	kIx~	-
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Údolí	údolí	k1gNnSc2	údolí
Ohře	Ohře	k1gFnSc2	Ohře
Velké	velký	k2eAgNnSc1d1	velké
Jeřábí	jeřábí	k2eAgNnSc1d1	jeřábí
jezero	jezero	k1gNnSc1	jezero
-	-	kIx~	-
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
Vladař	vladař	k1gMnSc1	vladař
-	-	kIx~	-
(	(	kIx(	(
<g/>
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Skalka	skalka	k1gFnSc1	skalka
-	-	kIx~	-
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vlčí	vlčí	k2eAgFnSc2d1	vlčí
jámy	jáma	k1gFnSc2	jáma
-	-	kIx~	-
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
těžbě	těžba	k1gFnSc6	těžba
cínu	cín	k1gInSc2	cín
Vysoký	vysoký	k2eAgInSc1d1	vysoký
kámen	kámen	k1gInSc1	kámen
Železná	železný	k2eAgFnSc1d1	železná
hůrka	hůrka	k1gFnSc1	hůrka
-	-	kIx~	-
Nejmladší	mladý	k2eAgFnSc1d3	nejmladší
sopka	sopka	k1gFnSc1	sopka
v	v	k7c6	v
ČR	ČR	kA	ČR
Děpoltovice-	Děpoltovice-	k1gFnPc2	Děpoltovice-
Pegasova	Pegasův	k2eAgFnSc1d1	Pegasova
stezka	stezka	k1gFnSc1	stezka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
JK	JK	kA	JK
Pegas	Pegas	k1gMnSc1	Pegas
Děpoltovice	Děpoltovice	k1gFnSc2	Děpoltovice
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Štorkán	Štorkán	k1gMnSc1	Štorkán
Andělská	andělský	k2eAgFnSc1d1	Andělská
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Bečov	Bečov	k1gInSc1	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Bečov	Bečov	k1gInSc1	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Doubrava	Doubrava	k1gFnSc1	Doubrava
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Favorit	favorit	k1gInSc1	favorit
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Hartenberg	Hartenberg	k1gInSc1	Hartenberg
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
zámku	zámek	k1gInSc2	zámek
Hartenštejn	Hartenštejna	k1gFnPc2	Hartenštejna
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Hauenštejn	Hauenštejn	k1gMnSc1	Hauenštejn
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Hazlov	Hazlov	k1gInSc1	Hazlov
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Himlštejn	Himlštejn	k1gMnSc1	Himlštejn
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Hungerberg	Hungerberg	k1gMnSc1	Hungerberg
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
<g />
.	.	kIx.	.
</s>
<s>
Chebský	chebský	k2eAgInSc1d1	chebský
hrad	hrad	k1gInSc1	hrad
-	-	kIx~	-
falc	falc	k1gFnSc1	falc
Chyše	Chyše	k?	Chyše
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Jindřichovice	Jindřichovice	k1gFnSc2	Jindřichovice
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Dvůr	Dvůr	k1gInSc1	Dvůr
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Libá	libý	k2eAgFnSc1d1	libá
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Loket	loket	k1gInSc1	loket
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Mostov	Mostov	k1gInSc1	Mostov
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Neuberg	Neuberg	k1gInSc1	Neuberg
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Nevděk	nevděk	k1gInSc1	nevděk
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Pomezná	pomezný	k2eAgFnSc1d1	Pomezná
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Seeberg	Seeberg	k1gInSc1	Seeberg
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Sokolov	Sokolov	k1gInSc1	Sokolov
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Staré	staré	k1gNnSc1	staré
Sedlo	sedlo	k1gNnSc1	sedlo
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Starý	Starý	k1gMnSc1	Starý
<g />
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Štědrý	štědrý	k2eAgInSc4d1	štědrý
hrádek	hrádek	k1gInSc4	hrádek
-	-	kIx~	-
zřícenina	zřícenina	k1gFnSc1	zřícenina
Valeč	Valeč	k1gMnSc1	Valeč
-	-	kIx~	-
zámek	zámek	k1gInSc1	zámek
Vildštejn	Vildštejn	k1gInSc1	Vildštejn
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Blatenský	blatenský	k2eAgInSc1d1	blatenský
vrch	vrch	k1gInSc1	vrch
-	-	kIx~	-
rozhledna	rozhledna	k1gFnSc1	rozhledna
Bublava	Bublava	k1gFnSc1	Bublava
Háj	háj	k1gInSc4	háj
u	u	k7c2	u
Aše	Aš	k1gInSc2	Aš
-	-	kIx~	-
rozhledna	rozhledna	k1gFnSc1	rozhledna
Horní	horní	k2eAgFnSc1d1	horní
Blatná	blatný	k2eAgFnSc1d1	Blatná
-	-	kIx~	-
muzeum	muzeum	k1gNnSc1	muzeum
Chlum	chlumit	k5eAaImRp2nS	chlumit
sv.	sv.	kA	sv.
Máří	Máří	k?	Máří
-	-	kIx~	-
poutní	poutní	k2eAgFnSc1d1	poutní
místo	místo	k1gNnSc4	místo
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
-	-	kIx~	-
mincovna	mincovna	k1gFnSc1	mincovna
Jelení	jelení	k2eAgNnSc1d1	jelení
Krásno	krásno	k1gNnSc1	krásno
-	-	kIx~	-
rozhledna	rozhledna	k1gFnSc1	rozhledna
Nejdek	Nejdek	k1gInSc1	Nejdek
-	-	kIx~	-
muzeum	muzeum	k1gNnSc1	muzeum
Pernink	Pernink	k1gInSc1	Pernink
Starý	Starý	k1gMnSc1	Starý
Hrozňatov	Hrozňatov	k1gInSc1	Hrozňatov
-	-	kIx~	-
poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
Teplá	teplat	k5eAaImIp3nS	teplat
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
klášter	klášter	k1gInSc4	klášter
Tisový	tisový	k2eAgInSc4d1	tisový
vrch	vrch	k1gInSc4	vrch
-	-	kIx~	-
rozhledna	rozhledna	k1gFnSc1	rozhledna
Žlutice	žlutice	k1gFnSc2	žlutice
-	-	kIx~	-
muzeum	muzeum	k1gNnSc4	muzeum
Ašský	ašský	k2eAgInSc1d1	ašský
výběžek	výběžek	k1gInSc1	výběžek
Hbané	Hbané	k2eAgInSc1d1	Hbané
Chebsko	Chebsko	k1gNnSc4	Chebsko
Loketsko	Loketska	k1gFnSc5	Loketska
Mikroregiony	mikroregion	k1gInPc1	mikroregion
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
Sedličané	Sedličan	k1gMnPc1	Sedličan
Říšská	říšský	k2eAgFnSc1d1	říšská
župa	župa	k1gFnSc1	župa
Sudety	Sudety	k1gFnPc1	Sudety
Sudety	Sudety	k1gFnPc1	Sudety
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karlovarský	karlovarský	k2eAgInSc4d1	karlovarský
kraj	kraj	k1gInSc4	kraj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
Památky	památka	k1gFnSc2	památka
a	a	k8xC	a
příroda	příroda	k1gFnSc1	příroda
Karlovarska	Karlovarsko	k1gNnSc2	Karlovarsko
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
<g/>
,	,	kIx,	,
památky	památka	k1gFnPc4	památka
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
</s>
