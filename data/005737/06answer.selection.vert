<s>
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
hominidů	hominid	k1gMnPc2	hominid
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc6	pleistocén
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
4,2	[number]	k4	4,2
-	-	kIx~	-
1,3	[number]	k4	1,3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
