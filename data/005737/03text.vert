<s>
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
hominidů	hominid	k1gMnPc2	hominid
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc6	pleistocén
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
4,2	[number]	k4	4,2
-	-	kIx~	-
1,3	[number]	k4	1,3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
zástupci	zástupce	k1gMnPc1	zástupce
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vývojové	vývojový	k2eAgFnSc3d1	vývojová
linii	linie	k1gFnSc3	linie
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéek	k1gMnPc1	Australopitéek
byli	být	k5eAaImAgMnP	být
dlouho	dlouho	k6eAd1	dlouho
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nejstarší	starý	k2eAgMnPc4d3	nejstarší
známé	známý	k1gMnPc4	známý
homininy	hominin	k2eAgMnPc4d1	hominin
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
geologickým	geologický	k2eAgFnPc3d1	geologická
stářím	stář	k1gFnPc3	stář
nepřekonaly	překonat	k5eNaPmAgInP	překonat
rody	rod	k1gInPc1	rod
Sahelanthropus	Sahelanthropus	k1gInSc1	Sahelanthropus
<g/>
,	,	kIx,	,
Orrorin	Orrorin	k1gInSc1	Orrorin
a	a	k8xC	a
Ardipithecus	Ardipithecus	k1gInSc1	Ardipithecus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přibývajícím	přibývající	k2eAgInPc3d1	přibývající
nálezům	nález	k1gInPc3	nález
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
15	[number]	k4	15
-	-	kIx~	-
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
rodu	rod	k1gInSc6	rod
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nP	měnit
a	a	k8xC	a
upřesňují	upřesňovat	k5eAaImIp3nP	upřesňovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
poznanými	poznaný	k2eAgMnPc7d1	poznaný
představiteli	představitel	k1gMnPc7	představitel
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc1	druh
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc7	afarensis
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
africanus	africanus	k1gMnSc1	africanus
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
doloženi	doložen	k2eAgMnPc1d1	doložen
značným	značný	k2eAgNnSc7d1	značné
množstvím	množství	k1gNnSc7	množství
ostatků	ostatek	k1gInPc2	ostatek
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
modelem	model	k1gInSc7	model
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
druhy	druh	k1gMnPc4	druh
<g/>
,	,	kIx,	,
známé	známý	k1gMnPc4	známý
často	často	k6eAd1	často
spíše	spíše	k9	spíše
torzovitě	torzovitě	k6eAd1	torzovitě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
ostatky	ostatek	k1gInPc4	ostatek
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Raymond	Raymond	k1gInSc1	Raymond
A.	A.	kA	A.
Dart	Dart	k1gInSc1	Dart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nálezech	nález	k1gInPc6	nález
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
z	z	k7c2	z
vápencových	vápencový	k2eAgInPc2d1	vápencový
lomů	lom	k1gInPc2	lom
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Taung	Taunga	k1gFnPc2	Taunga
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Taungs	Taungs	k1gInSc1	Taungs
<g/>
)	)	kIx)	)
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Bečuánsku	Bečuánsko	k1gNnSc6	Bečuánsko
objevil	objevit	k5eAaPmAgInS	objevit
taungské	taungský	k2eAgNnSc4d1	taungský
dítě	dítě	k1gNnSc4	dítě
-	-	kIx~	-
obličejovou	obličejový	k2eAgFnSc4d1	obličejová
část	část	k1gFnSc4	část
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
přesně	přesně	k6eAd1	přesně
zapadal	zapadat	k5eAaPmAgInS	zapadat
detailně	detailně	k6eAd1	detailně
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
přirozený	přirozený	k2eAgInSc1d1	přirozený
výlitek	výlitek	k1gInSc1	výlitek
mozkovny	mozkovna	k1gFnSc2	mozkovna
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
africanus	africanus	k1gMnSc1	africanus
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
opice	opice	k1gFnSc2	opice
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
málo	málo	k6eAd1	málo
očekávaného	očekávaný	k2eAgNnSc2d1	očekávané
místa	místo	k1gNnSc2	místo
nálezu	nález	k1gInSc2	nález
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
totiž	totiž	k9	totiž
byly	být	k5eAaImAgInP	být
doklady	doklad	k1gInPc1	doklad
pliocénních	pliocénní	k2eAgMnPc2d1	pliocénní
a	a	k8xC	a
pleistocénních	pleistocénní	k2eAgMnPc2d1	pleistocénní
hominidů	hominid	k1gMnPc2	hominid
prakticky	prakticky	k6eAd1	prakticky
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
badatelů	badatel	k1gMnPc2	badatel
hledalo	hledat	k5eAaImAgNnS	hledat
kolébku	kolébka	k1gFnSc4	kolébka
lidstva	lidstvo	k1gNnSc2	lidstvo
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
právě	právě	k6eAd1	právě
odhaleny	odhalen	k2eAgInPc1d1	odhalen
ostatky	ostatek	k1gInPc1	ostatek
starších	starý	k2eAgFnPc2d2	starší
forem	forma	k1gFnPc2	forma
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
podvrženého	podvržený	k2eAgMnSc2d1	podvržený
Piltdownského	Piltdownský	k1gMnSc2	Piltdownský
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
upravené	upravený	k2eAgFnSc2d1	upravená
lebky	lebka	k1gFnSc2	lebka
středověkého	středověký	k2eAgMnSc2d1	středověký
člověka	člověk	k1gMnSc2	člověk
spojené	spojený	k2eAgInPc4d1	spojený
se	se	k3xPyFc4	se
spodní	spodní	k2eAgFnSc7d1	spodní
čelistí	čelist	k1gFnSc7	čelist
orangutana	orangutan	k1gMnSc2	orangutan
-	-	kIx~	-
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
pátralo	pátrat	k5eAaImAgNnS	pátrat
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
mozkem	mozek	k1gInSc7	mozek
a	a	k8xC	a
opičími	opičí	k2eAgInPc7d1	opičí
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Taungské	Taungský	k2eAgNnSc1d1	Taungský
dítě	dítě	k1gNnSc1	dítě
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
přesně	přesně	k6eAd1	přesně
opačné	opačný	k2eAgNnSc1d1	opačné
-	-	kIx~	-
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
lidskými	lidský	k2eAgInPc7d1	lidský
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
"	"	kIx"	"
<g/>
zaostalé	zaostalý	k2eAgFnSc2d1	zaostalá
<g/>
"	"	kIx"	"
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
lebka	lebka	k1gFnSc1	lebka
mláděte	mládě	k1gNnSc2	mládě
i	i	k9	i
přes	přes	k7c4	přes
dobrou	dobrý	k2eAgFnSc4d1	dobrá
zachovalost	zachovalost	k1gFnSc4	zachovalost
nemohla	moct	k5eNaImAgFnS	moct
vypovídat	vypovídat	k5eAaPmF	vypovídat
o	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
"	"	kIx"	"
<g/>
lidských	lidský	k2eAgInPc2d1	lidský
<g/>
"	"	kIx"	"
znaků	znak	k1gInPc2	znak
tudíž	tudíž	k8xC	tudíž
byla	být	k5eAaImAgFnS	být
přičítána	přičítán	k2eAgFnSc1d1	přičítána
právě	právě	k6eAd1	právě
nedospělosti	nedospělost	k1gFnSc3	nedospělost
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
nálezu	nález	k1gInSc6	nález
tak	tak	k6eAd1	tak
strhla	strhnout	k5eAaPmAgFnS	strhnout
velkou	velký	k2eAgFnSc4d1	velká
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
odborná	odborný	k2eAgFnSc1d1	odborná
veřejnost	veřejnost	k1gFnSc1	veřejnost
taungské	taungská	k1gFnSc2	taungská
dítě	dítě	k1gNnSc1	dítě
coby	coby	k?	coby
lidského	lidský	k2eAgMnSc2d1	lidský
předka	předek	k1gMnSc2	předek
ostře	ostro	k6eAd1	ostro
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
názory	názor	k1gInPc1	názor
R.	R.	kA	R.
A.	A.	kA	A.
Darta	Darta	k1gMnSc1	Darta
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
Robert	Robert	k1gMnSc1	Robert
Broom	Broom	k1gInSc4	Broom
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Transvaalském	Transvaalský	k2eAgNnSc6d1	Transvaalský
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
sám	sám	k3xTgMnSc1	sám
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
pátrání	pátrání	k1gNnSc2	pátrání
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
ostatcích	ostatek	k1gInPc6	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
tak	tak	k6eAd1	tak
ve	v	k7c6	v
vápencovém	vápencový	k2eAgInSc6d1	vápencový
lomu	lom	k1gInSc6	lom
u	u	k7c2	u
Sterkfontein	Sterkfonteina	k1gFnPc2	Sterkfonteina
zachytil	zachytit	k5eAaPmAgMnS	zachytit
ostatky	ostatek	k1gInPc4	ostatek
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
popsal	popsat	k5eAaPmAgMnS	popsat
podle	podle	k7c2	podle
nálezu	nález	k1gInSc2	nález
z	z	k7c2	z
Kromdraai	Kromdraa	k1gFnSc2	Kromdraa
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
robustus	robustus	k1gMnSc1	robustus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgInP	být
doklady	doklad	k1gInPc1	doklad
australopitéků	australopitéek	k1gMnPc2	australopitéek
zjištěny	zjištěn	k2eAgInPc1d1	zjištěn
také	také	k9	také
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
jeskyních	jeskyně	k1gFnPc6	jeskyně
Swartkrans	Swartkransa	k1gFnPc2	Swartkransa
a	a	k8xC	a
Makapansgat	Makapansgat	k1gInSc1	Makapansgat
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc4	význam
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
evoluci	evoluce	k1gFnSc6	evoluce
již	již	k9	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Vlna	vlna	k1gFnSc1	vlna
ostrého	ostrý	k2eAgInSc2d1	ostrý
odporu	odpor	k1gInSc2	odpor
opadla	opadnout	k5eAaPmAgFnS	opadnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
let	léto	k1gNnPc2	léto
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
již	již	k9	již
bylo	být	k5eAaImAgNnS	být
dokladů	doklad	k1gInPc2	doklad
tolik	tolik	k4xDc1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
odborná	odborný	k2eAgFnSc1d1	odborná
veřejnost	veřejnost	k1gFnSc1	veřejnost
konečně	konečně	k6eAd1	konečně
začala	začít	k5eAaPmAgFnS	začít
Dartovy	Dartův	k2eAgInPc4d1	Dartův
i	i	k8xC	i
Broomovy	Broomův	k2eAgInPc4d1	Broomův
objevy	objev	k1gInPc4	objev
brát	brát	k5eAaImF	brát
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
i	i	k9	i
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
jihoafrických	jihoafrický	k2eAgFnPc6d1	Jihoafrická
krasových	krasový	k2eAgFnPc6d1	krasová
jeskyních	jeskyně	k1gFnPc6	jeskyně
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
systematické	systematický	k2eAgInPc1d1	systematický
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
vydávající	vydávající	k2eAgInPc1d1	vydávající
tisíce	tisíc	k4xCgInPc1	tisíc
kostí	kost	k1gFnPc2	kost
vyhynulých	vyhynulý	k2eAgNnPc2d1	vyhynulé
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
stovky	stovka	k1gFnPc1	stovka
nálezů	nález	k1gInPc2	nález
homininů	hominin	k1gInPc2	hominin
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
zastíněn	zastínit	k5eAaPmNgInS	zastínit
překvapivými	překvapivý	k2eAgInPc7d1	překvapivý
objevy	objev	k1gInPc7	objev
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
australopitéků	australopiték	k1gInPc2	australopiték
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Afriky	Afrika	k1gFnSc2	Afrika
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
již	již	k6eAd1	již
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
však	však	k9	však
zpočátku	zpočátku	k6eAd1	zpočátku
správně	správně	k6eAd1	správně
rozeznány	rozeznat	k5eAaPmNgFnP	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přinesly	přinést	k5eAaPmAgInP	přinést
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
podle	podle	k7c2	podle
dobře	dobře	k6eAd1	dobře
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
lebky	lebka	k1gFnSc2	lebka
z	z	k7c2	z
Olduvajské	Olduvajský	k2eAgFnSc2d1	Olduvajská
rokle	rokle	k1gFnSc2	rokle
popsán	popsán	k2eAgInSc1d1	popsán
druh	druh	k1gInSc1	druh
Paranthropus	Paranthropus	k1gInSc4	Paranthropus
boisei	boise	k1gFnSc2	boise
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
nálezů	nález	k1gInPc2	nález
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
získáno	získat	k5eAaPmNgNnS	získat
také	také	k9	také
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
Turkana	Turkan	k1gMnSc2	Turkan
-	-	kIx~	-
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
Omo	Omo	k1gFnSc2	Omo
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Koobi	Koob	k1gFnSc2	Koob
Fora	forum	k1gNnSc2	forum
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
téměř	téměř	k6eAd1	téměř
současně	současně	k6eAd1	současně
odhaleny	odhalen	k2eAgFnPc4d1	odhalena
a	a	k8xC	a
zkoumány	zkoumán	k2eAgFnPc4d1	zkoumána
dvě	dva	k4xCgFnPc4	dva
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc4d1	významná
lokality	lokalita	k1gFnPc4	lokalita
–	–	k?	–
Hadar	Hadar	k1gMnSc1	Hadar
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
a	a	k8xC	a
Laetoli	Laetole	k1gFnSc6	Laetole
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
vydaly	vydat	k5eAaPmAgFnP	vydat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
ostatků	ostatek	k1gInPc2	ostatek
<g/>
,	,	kIx,	,
popsané	popsaný	k2eAgInPc1d1	popsaný
jako	jako	k8xS	jako
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
kolenního	kolenní	k2eAgInSc2d1	kolenní
kloubu	kloub	k1gInSc2	kloub
v	v	k7c6	v
Hadaru	Hadar	k1gInSc6	Hadar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
objev	objev	k1gInSc1	objev
otisků	otisk	k1gInPc2	otisk
stop	stopa	k1gFnPc2	stopa
v	v	k7c6	v
Laetoli	Laetole	k1gFnSc6	Laetole
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nejistou	jistý	k2eNgFnSc4d1	nejistá
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
pliocénní	pliocénní	k2eAgNnSc4d1	pliocénní
homininé	homininé	k1gNnSc4	homininé
již	již	k6eAd1	již
chodili	chodit	k5eAaImAgMnP	chodit
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
končetinách	končetina	k1gFnPc6	končetina
jako	jako	k8xS	jako
moderní	moderní	k2eAgMnPc1d1	moderní
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
slavný	slavný	k2eAgInSc1d1	slavný
je	být	k5eAaImIp3nS	být
nález	nález	k1gInSc1	nález
neúplné	úplný	k2eNgFnSc2d1	neúplná
kostry	kostra	k1gFnSc2	kostra
samice	samice	k1gFnSc1	samice
A.	A.	kA	A.
afarensis	afarensis	k1gFnSc1	afarensis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Lucy	Luca	k1gFnSc2	Luca
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezvykle	zvykle	k6eNd1	zvykle
dobré	dobrý	k2eAgNnSc1d1	dobré
dochování	dochování	k1gNnSc1	dochování
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
celkový	celkový	k2eAgInSc4d1	celkový
vzhled	vzhled	k1gInSc4	vzhled
i	i	k8xC	i
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
časných	časný	k2eAgInPc2d1	časný
homininů	hominin	k1gInPc2	hominin
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
později	pozdě	k6eAd2	pozdě
podobných	podobný	k2eAgInPc2d1	podobný
nálezů	nález	k1gInPc2	nález
částečných	částečný	k2eAgFnPc2d1	částečná
koster	kostra	k1gFnPc2	kostra
přibylo	přibýt	k5eAaPmAgNnS	přibýt
(	(	kIx(	(
<g/>
jen	jen	k9	jen
pro	pro	k7c4	pro
druh	druh	k1gInSc4	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ostatky	ostatek	k1gInPc1	ostatek
velkého	velký	k2eAgMnSc2d1	velký
samce	samec	k1gMnSc2	samec
Kadanuumuu	Kadanuumuus	k1gInSc2	Kadanuumuus
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
tříletého	tříletý	k2eAgNnSc2d1	tříleté
mláděte	mládě	k1gNnSc2	mládě
Selam	Selam	k1gInSc1	Selam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lucy	Luca	k1gFnPc4	Luca
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
vzorový	vzorový	k2eAgMnSc1d1	vzorový
zástupce	zástupce	k1gMnSc1	zástupce
australopitéků	australopitéek	k1gMnPc2	australopitéek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
užívána	užívat	k5eAaImNgFnS	užívat
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
srovnávacím	srovnávací	k2eAgFnPc3d1	srovnávací
analýzám	analýza	k1gFnPc3	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
Lomekwi	Lomekw	k1gFnSc2	Lomekw
při	při	k7c6	při
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Turkana	Turkan	k1gMnSc2	Turkan
zachycen	zachytit	k5eAaPmNgInS	zachytit
také	také	k9	také
další	další	k2eAgMnSc1d1	další
zástupce	zástupce	k1gMnSc1	zástupce
robustních	robustní	k2eAgInPc2d1	robustní
druhů	druh	k1gInPc2	druh
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
aethiopicus	aethiopicus	k1gMnSc1	aethiopicus
<g/>
.	.	kIx.	.
</s>
<s>
Přelom	přelom	k1gInSc1	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přinesl	přinést	k5eAaPmAgMnS	přinést
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
i	i	k8xC	i
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
množství	množství	k1gNnSc1	množství
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
překvapivých	překvapivý	k2eAgInPc2d1	překvapivý
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
měrou	míra	k1gFnSc7wR	míra
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
známý	známý	k2eAgInSc1d1	známý
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
druhu	druh	k1gInSc2	druh
P.	P.	kA	P.
boisei	boisei	k6eAd1	boisei
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
zachyceny	zachycen	k2eAgInPc1d1	zachycen
na	na	k7c6	na
etiopské	etiopský	k2eAgFnSc6d1	etiopská
lokalitě	lokalita	k1gFnSc6	lokalita
Konso	Konsa	k1gFnSc5	Konsa
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
Malema	Malemum	k1gNnSc2	Malemum
až	až	k9	až
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Malawi	Malawi	k1gNnSc2	Malawi
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
druhu	druh	k1gInSc2	druh
P.	P.	kA	P.
aethiopicus	aethiopicus	k1gInSc1	aethiopicus
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
významně	významně	k6eAd1	významně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
objevům	objev	k1gInPc3	objev
v	v	k7c6	v
tanzanském	tanzanský	k2eAgNnSc6d1	tanzanské
Laetoli	Laetole	k1gFnSc6	Laetole
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
nově	nově	k6eAd1	nově
rozeznaných	rozeznaný	k2eAgInPc2d1	rozeznaný
druhů	druh	k1gInPc2	druh
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
homininů	hominin	k1gInPc2	hominin
byl	být	k5eAaImAgInS	být
složitější	složitý	k2eAgInSc1d2	složitější
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
hominina	hominin	k2eAgMnSc4d1	hominin
a	a	k8xC	a
předka	předek	k1gMnSc4	předek
všech	všecek	k3xTgInPc2	všecek
mladších	mladý	k2eAgInPc2d2	mladší
druhů	druh	k1gInPc2	druh
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
nálezech	nález	k1gInPc6	nález
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
keňského	keňský	k2eAgNnSc2d1	keňské
jezera	jezero	k1gNnSc2	jezero
Turkana	Turkan	k1gMnSc2	Turkan
a	a	k8xC	a
z	z	k7c2	z
etiopské	etiopský	k2eAgFnSc2d1	etiopská
oblasti	oblast	k1gFnSc2	oblast
Střední	střední	k2eAgInSc4d1	střední
Awaš	Awaš	k1gInSc4	Awaš
identifikován	identifikován	k2eAgInSc4d1	identifikován
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgInSc4d2	starší
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
anamensis	anamensis	k1gFnSc2	anamensis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
až	až	k9	až
ve	v	k7c6	v
středoafrickém	středoafrický	k2eAgInSc6d1	středoafrický
Čadu	Čad	k1gInSc6	Čad
objevena	objevit	k5eAaPmNgFnS	objevit
spodní	spodní	k2eAgFnSc1d1	spodní
čelist	čelist	k1gFnSc1	čelist
druhu	druh	k1gInSc2	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
prvního	první	k4xOgMnSc4	první
australopitéka	australopitéek	k1gMnSc4	australopitéek
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
východně	východně	k6eAd1	východně
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
musel	muset	k5eAaImAgInS	muset
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obývat	obývat	k5eAaImF	obývat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996-1997	[number]	k4	1996-1997
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Bouri	Bour	k1gFnSc2	Bour
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Střední	střední	k2eAgInSc4d1	střední
Awaš	Awaš	k1gInSc4	Awaš
odhaleny	odhalen	k2eAgInPc1d1	odhalen
doklady	doklad	k1gInPc1	doklad
dalšího	další	k2eAgMnSc2d1	další
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
málo	málo	k1gNnSc1	málo
známého	známý	k2eAgInSc2d1	známý
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
garhi	garh	k1gInSc6	garh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
plné	plný	k2eAgNnSc4d1	plné
uznání	uznání	k1gNnSc4	uznání
ještě	ještě	k6eAd1	ještě
čekají	čekat	k5eAaImIp3nP	čekat
málo	málo	k6eAd1	málo
početné	početný	k2eAgInPc1d1	početný
ostatky	ostatek	k1gInPc1	ostatek
<g/>
,	,	kIx,	,
zachycené	zachycený	k2eAgNnSc1d1	zachycené
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
v	v	k7c6	v
etiopské	etiopský	k2eAgFnSc6d1	etiopská
oblasti	oblast	k1gFnSc6	oblast
Woranso-Mile	Woranso-Mila	k1gFnSc6	Woranso-Mila
a	a	k8xC	a
Burtele	Burtela	k1gFnSc6	Burtela
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
navržen	navržen	k2eAgInSc1d1	navržen
název	název	k1gInSc1	název
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
deyiremeda	deyiremeda	k1gMnSc1	deyiremeda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevům	objev	k1gInPc3	objev
nových	nový	k2eAgInPc2d1	nový
jeskynních	jeskynní	k2eAgInPc2d1	jeskynní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
vydávajících	vydávající	k2eAgInPc2d1	vydávající
ostatky	ostatek	k1gInPc7	ostatek
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
robustus	robustus	k1gMnSc1	robustus
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
zachycen	zachytit	k5eAaPmNgInS	zachytit
především	především	k6eAd1	především
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
v	v	k7c4	v
Drimolen	Drimolen	k2eAgInSc4d1	Drimolen
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
lebek	lebka	k1gFnPc2	lebka
australopitéka	australopitéka	k6eAd1	australopitéka
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnPc1d1	zásadní
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nové	nový	k2eAgInPc4d1	nový
objevy	objev	k1gInPc4	objev
ve	v	k7c4	v
Sterkfontein	Sterkfontein	k1gInSc4	Sterkfontein
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
zejména	zejména	k9	zejména
kostra	kostra	k1gFnSc1	kostra
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Little	Little	k1gFnSc1	Little
Foot	Foot	k1gInSc1	Foot
<g/>
,	,	kIx,	,
zachycená	zachycený	k2eAgFnSc1d1	zachycená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
však	však	k9	však
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tato	tento	k3xDgFnSc1	tento
téměř	téměř	k6eAd1	téměř
úplná	úplný	k2eAgFnSc1d1	úplná
kostra	kostra	k1gFnSc1	kostra
patří	patřit	k5eAaImIp3nS	patřit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
Sterkfontein	Sterkfontein	k1gInSc1	Sterkfontein
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
africanus	africanus	k1gMnSc1	africanus
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgInPc1d1	významný
objevy	objev	k1gInPc1	objev
byly	být	k5eAaImAgInP	být
učiněny	učinit	k5eAaImNgInP	učinit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
i	i	k8xC	i
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Malapa	Malap	k1gMnSc2	Malap
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
zachyceny	zachycen	k2eAgFnPc1d1	zachycena
nezvykle	zvykle	k6eNd1	zvykle
početné	početný	k2eAgFnPc1d1	početná
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
kompletní	kompletní	k2eAgInPc1d1	kompletní
ostatky	ostatek	k1gInPc1	ostatek
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
sediba	sedib	k1gMnSc4	sedib
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
po	po	k7c6	po
důkladném	důkladný	k2eAgNnSc6d1	důkladné
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
jistě	jistě	k9	jistě
umožní	umožnit	k5eAaPmIp3nS	umožnit
poznat	poznat	k5eAaPmF	poznat
další	další	k2eAgFnPc4d1	další
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
životě	život	k1gInSc6	život
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgFnPc1d1	datovaná
do	do	k7c2	do
spodního	spodní	k2eAgInSc2d1	spodní
pleistocénu	pleistocén	k1gInSc2	pleistocén
(	(	kIx(	(
<g/>
před	před	k7c7	před
1	[number]	k4	1
-	-	kIx~	-
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavdaly	zavdat	k5eAaPmAgFnP	zavdat
příčinu	příčina	k1gFnSc4	příčina
k	k	k7c3	k
úvahám	úvaha	k1gFnPc3	úvaha
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
australopitéků	australopiték	k1gInPc2	australopiték
i	i	k9	i
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nález	nález	k1gInSc4	nález
čtyř	čtyři	k4xCgFnPc2	čtyři
nápadně	nápadně	k6eAd1	nápadně
masivních	masivní	k2eAgFnPc2d1	masivní
spodních	spodní	k2eAgFnPc2d1	spodní
čelistí	čelist	k1gFnPc2	čelist
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
částečně	částečně	k6eAd1	částečně
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
lebky	lebka	k1gFnPc4	lebka
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
Sangiran	Sangirana	k1gFnPc2	Sangirana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
čelistí	čelist	k1gFnPc2	čelist
zachytil	zachytit	k5eAaPmAgInS	zachytit
svými	svůj	k3xOyFgInPc7	svůj
výzkumy	výzkum	k1gInPc7	výzkum
již	již	k6eAd1	již
Ralph	Ralph	k1gInSc1	Ralph
von	von	k1gInSc1	von
Koenigswald	Koenigswald	k1gInSc1	Koenigswald
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
Meganthropus	Meganthropus	k1gMnSc1	Meganthropus
palaeojavanicus	palaeojavanicus	k1gMnSc1	palaeojavanicus
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
mezi	mezi	k7c7	mezi
australopitéky	australopiték	k1gInPc7	australopiték
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
John	John	k1gMnSc1	John
T.	T.	kA	T.
Robinson	Robinson	k1gMnSc1	Robinson
na	na	k7c6	na
základě	základ	k1gInSc6	základ
utváření	utváření	k1gNnPc2	utváření
třenových	třenový	k2eAgInPc2d1	třenový
zubů	zub	k1gInPc2	zub
-	-	kIx~	-
rod	rod	k1gInSc1	rod
Meganthropus	Meganthropus	k1gMnSc1	Meganthropus
tak	tak	k8xS	tak
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
asijskou	asijský	k2eAgFnSc4d1	asijská
obdobu	obdoba	k1gFnSc4	obdoba
paranthropů	paranthrop	k1gMnPc2	paranthrop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
ještě	ještě	k6eAd1	ještě
mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
předka	předek	k1gMnSc4	předek
australopitéků	australopitéek	k1gMnPc2	australopitéek
rod	rod	k1gInSc1	rod
Ramapithecus	Ramapithecus	k1gInSc1	Ramapithecus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
australopitékové	australopitéek	k1gMnPc1	australopitéek
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
představ	představa	k1gFnPc2	představa
mohli	moct	k5eAaImAgMnP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
vycestovat	vycestovat	k5eAaPmF	vycestovat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
výzkumy	výzkum	k1gInPc1	výzkum
však	však	k9	však
dostatečně	dostatečně	k6eAd1	dostatečně
prokázaly	prokázat	k5eAaPmAgInP	prokázat
africké	africký	k2eAgInPc1d1	africký
kořeny	kořen	k1gInPc1	kořen
australopitéků	australopitéek	k1gMnPc2	australopitéek
a	a	k8xC	a
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
jednohlasně	jednohlasně	k6eAd1	jednohlasně
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
homininé	homininé	k2eAgMnPc1d1	homininé
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
nevyskytovali	vyskytovat	k5eNaImAgMnP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
nápadně	nápadně	k6eAd1	nápadně
robustní	robustní	k2eAgFnSc4d1	robustní
stavbu	stavba	k1gFnSc4	stavba
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnPc1d1	připomínající
africké	africký	k2eAgFnPc1d1	africká
paranthropy	paranthropa	k1gFnPc1	paranthropa
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Jávy	Jáva	k1gFnSc2	Jáva
menší	malý	k2eAgFnSc2d2	menší
stoličky	stolička	k1gFnSc2	stolička
i	i	k8xC	i
třenové	třenový	k2eAgInPc4d1	třenový
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
patří	patřit	k5eAaImIp3nP	patřit
tak	tak	k9	tak
spíše	spíše	k9	spíše
druhu	druh	k1gInSc2	druh
Homo	Homo	k6eAd1	Homo
erectus	erectus	k1gInSc1	erectus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
jihoasijská	jihoasijský	k2eAgFnSc1d1	jihoasijská
forma	forma	k1gFnSc1	forma
si	se	k3xPyFc3	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
velmi	velmi	k6eAd1	velmi
robustní	robustní	k2eAgInSc4d1	robustní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
početné	početný	k2eAgInPc4d1	početný
primitivní	primitivní	k2eAgInPc4d1	primitivní
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
jeskyně	jeskyně	k1gFnSc2	jeskyně
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
Kolébka	kolébka	k1gFnSc1	kolébka
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
součástí	součást	k1gFnSc7	součást
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Východoafrické	východoafrický	k2eAgFnPc1d1	východoafrická
oblasti	oblast	k1gFnPc1	oblast
Omo	Omo	k1gFnPc2	Omo
a	a	k8xC	a
Hadar	Hadara	k1gFnPc2	Hadara
s	s	k7c7	s
hojným	hojný	k2eAgInSc7d1	hojný
výskytem	výskyt	k1gInSc7	výskyt
ostatků	ostatek	k1gInPc2	ostatek
časných	časný	k2eAgInPc2d1	časný
homininů	hominin	k1gInPc2	hominin
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědicví	dědicví	k1gNnSc2	dědicví
připsány	připsat	k5eAaPmNgInP	připsat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
i	i	k8xC	i
pleistocénu	pleistocén	k1gInSc6	pleistocén
aktivní	aktivní	k2eAgFnSc1d1	aktivní
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
ukládáním	ukládání	k1gNnSc7	ukládání
četných	četný	k2eAgFnPc2d1	četná
vrstev	vrstva	k1gFnPc2	vrstva
sopečného	sopečný	k2eAgInSc2d1	sopečný
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
mnohé	mnohý	k2eAgInPc1d1	mnohý
ze	z	k7c2	z
zachycených	zachycený	k2eAgInPc2d1	zachycený
ostatků	ostatek	k1gInPc2	ostatek
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
datovat	datovat	k5eAaImF	datovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
radiometrických	radiometrický	k2eAgFnPc2d1	radiometrická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
datace	datace	k1gFnSc1	datace
jihoafrických	jihoafrický	k2eAgInPc2d1	jihoafrický
nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
brekcie	brekcie	k1gFnSc2	brekcie
bývá	bývat	k5eAaImIp3nS	bývat
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Využívat	využívat	k5eAaImF	využívat
lze	lze	k6eAd1	lze
často	často	k6eAd1	často
jen	jen	k9	jen
biostratigrafii	biostratigrafie	k1gFnSc4	biostratigrafie
-	-	kIx~	-
srovnávání	srovnávání	k1gNnSc4	srovnávání
spektra	spektrum	k1gNnSc2	spektrum
nalézaných	nalézaný	k2eAgInPc2d1	nalézaný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
datovanými	datovaný	k2eAgFnPc7d1	datovaná
lokalitami	lokalita	k1gFnPc7	lokalita
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
technický	technický	k2eAgInSc1d1	technický
rozvoj	rozvoj	k1gInSc1	rozvoj
umožnil	umožnit	k5eAaPmAgInS	umožnit
uplatnění	uplatnění	k1gNnSc4	uplatnění
absolutních	absolutní	k2eAgFnPc2d1	absolutní
datovacích	datovací	k2eAgFnPc2d1	datovací
metod	metoda	k1gFnPc2	metoda
i	i	k9	i
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zpřesnily	zpřesnit	k5eAaPmAgInP	zpřesnit
starší	starý	k2eAgInPc1d2	starší
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnPc1d1	představující
často	často	k6eAd1	často
značně	značně	k6eAd1	značně
široké	široký	k2eAgInPc4d1	široký
časové	časový	k2eAgInPc4d1	časový
intervaly	interval	k1gInPc4	interval
<g/>
.	.	kIx.	.
</s>
<s>
Využíván	využíván	k2eAgInSc1d1	využíván
je	být	k5eAaImIp3nS	být
paleomagnetismus	paleomagnetismus	k1gInSc1	paleomagnetismus
<g/>
,	,	kIx,	,
luminiscence	luminiscence	k1gFnSc1	luminiscence
nebo	nebo	k8xC	nebo
metoda	metoda	k1gFnSc1	metoda
uran-olovo	uranlův	k2eAgNnSc1d1	uran-olovo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
australopitéka	australopitéek	k1gMnSc4	australopitéek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
považován	považován	k2eAgMnSc1d1	považován
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
anamensis	anamensis	k1gFnSc2	anamensis
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
3,9	[number]	k4	3,9
<g/>
–	–	k?	–
<g/>
4,2	[number]	k4	4,2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
3,8	[number]	k4	3,8
<g/>
–	–	k?	–
<g/>
2,9	[number]	k4	2,9
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
před	před	k7c7	před
3,5	[number]	k4	3,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
žili	žít	k5eAaImAgMnP	žít
homininé	homininé	k2eAgMnPc1d1	homininé
<g/>
,	,	kIx,	,
řazení	řazený	k2eAgMnPc1d1	řazený
do	do	k7c2	do
druhů	druh	k1gInPc2	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
a	a	k8xC	a
Kenyanthropus	Kenyanthropus	k1gInSc4	Kenyanthropus
platyops	platyopsa	k1gFnPc2	platyopsa
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nepřesná	přesný	k2eNgFnSc1d1	nepřesná
je	být	k5eAaImIp3nS	být
datace	datace	k1gFnSc1	datace
druhu	druh	k1gInSc3	druh
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
africanus	africanus	k1gMnSc1	africanus
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
kladen	klást	k5eAaImNgInS	klást
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
nálezy	nález	k1gInPc1	nález
ze	z	k7c2	z
Sterkfontein	Sterkfonteina	k1gFnPc2	Sterkfonteina
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
kostra	kostra	k1gFnSc1	kostra
Little	Little	k1gFnSc2	Little
Foot	Foota	k1gFnPc2	Foota
<g/>
)	)	kIx)	)
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
stáří	stáří	k1gNnSc4	stáří
až	až	k6eAd1	až
3,67	[number]	k4	3,67
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
před	před	k7c7	před
2,5	[number]	k4	2,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
první	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
žily	žít	k5eAaImAgFnP	žít
i	i	k9	i
mladší	mladý	k2eAgFnPc4d2	mladší
formy	forma	k1gFnPc4	forma
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zástupci	zástupce	k1gMnPc1	zástupce
robustních	robustní	k2eAgInPc2d1	robustní
druhů	druh	k1gInPc2	druh
-	-	kIx~	-
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
aethiopicus	aethiopicus	k1gMnSc1	aethiopicus
(	(	kIx(	(
<g/>
před	před	k7c7	před
2,7	[number]	k4	2,7
<g/>
–	–	k?	–
<g/>
2,3	[number]	k4	2,3
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
boisei	boise	k1gFnSc2	boise
(	(	kIx(	(
<g/>
před	před	k7c7	před
2,3	[number]	k4	2,3
<g/>
–	–	k?	–
<g/>
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
robustus	robustus	k1gMnSc1	robustus
(	(	kIx(	(
<g/>
před	před	k7c7	před
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
datován	datován	k2eAgInSc1d1	datován
i	i	k8xC	i
východoafrický	východoafrický	k2eAgInSc1d1	východoafrický
druh	druh	k1gInSc1	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
garhi	garh	k1gFnSc2	garh
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
P.	P.	kA	P.
robustus	robustus	k1gInSc1	robustus
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
před	před	k7c7	před
1,98	[number]	k4	1,98
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
sediba	sediba	k1gMnSc1	sediba
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéek	k1gMnPc1	Australopitéek
byli	být	k5eAaImAgMnP	být
vcelku	vcelku	k6eAd1	vcelku
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
skupinou	skupina	k1gFnSc7	skupina
homininů	hominin	k1gInPc2	hominin
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc2d1	obývající
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
oblasti	oblast	k1gFnSc2	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
proto	proto	k8xC	proto
dále	daleko	k6eAd2	daleko
členěno	členit	k5eAaImNgNnS	členit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgNnSc7d1	typické
místem	místo	k1gNnSc7	místo
nálezů	nález	k1gInPc2	nález
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejbohatší	bohatý	k2eAgNnPc1d3	nejbohatší
naleziště	naleziště	k1gNnPc1	naleziště
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
,	,	kIx,	,
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
a	a	k8xC	a
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
z	z	k7c2	z
Malawi	Malawi	k1gNnSc2	Malawi
a	a	k8xC	a
středoafrického	středoafrický	k2eAgInSc2d1	středoafrický
Čadu	Čad	k1gInSc2	Čad
<g/>
.	.	kIx.	.
jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
-	-	kIx~	-
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
sediba	sediba	k1gFnSc1	sediba
<g/>
,	,	kIx,	,
A.	A.	kA	A.
robustus	robustus	k1gInSc1	robustus
střední	střední	k2eAgFnSc1d1	střední
Afrika	Afrika	k1gFnSc1	Afrika
-	-	kIx~	-
A.	A.	kA	A.
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
východní	východní	k2eAgFnSc4d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
-	-	kIx~	-
A.	A.	kA	A.
anamensis	anamensis	k1gInSc1	anamensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
garhi	garhi	k1gNnSc1	garhi
<g/>
,	,	kIx,	,
A.	A.	kA	A.
aethiopicus	aethiopicus	k1gInSc1	aethiopicus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
boisei	boisei	k6eAd1	boisei
Dalším	další	k2eAgNnSc7d1	další
hlediskem	hledisko	k1gNnSc7	hledisko
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
lze	lze	k6eAd1	lze
zástupce	zástupce	k1gMnSc1	zástupce
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
členit	členit	k5eAaImF	členit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
nálezů	nález	k1gInPc2	nález
se	se	k3xPyFc4	se
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
nápadné	nápadný	k2eAgInPc1d1	nápadný
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
lehce	lehko	k6eAd1	lehko
stavěnými	stavěný	k2eAgMnPc7d1	stavěný
(	(	kIx(	(
<g/>
gracilními	gracilní	k2eAgMnPc7d1	gracilní
<g/>
)	)	kIx)	)
a	a	k8xC	a
robustními	robustní	k2eAgFnPc7d1	robustní
formami	forma	k1gFnPc7	forma
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
především	především	k9	především
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
řadí	řadit	k5eAaImIp3nP	řadit
robustní	robustní	k2eAgFnPc4d1	robustní
australopitéky	australopitéka	k1gFnPc4	australopitéka
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
rodu	rod	k1gInSc2	rod
Paranthropus	Paranthropus	k1gInSc1	Paranthropus
<g/>
.	.	kIx.	.
gracilní	gracilní	k2eAgMnSc1d1	gracilní
-	-	kIx~	-
A.	A.	kA	A.
anamensis	anamensis	k1gInSc1	anamensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
<g/>
,	,	kIx,	,
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
sediba	sediba	k1gFnSc1	sediba
<g/>
,	,	kIx,	,
A.	A.	kA	A.
garhi	garhi	k6eAd1	garhi
robustní	robustní	k2eAgFnSc7d1	robustní
-	-	kIx~	-
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
aethiopicus	aethiopicus	k1gMnSc1	aethiopicus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
boisei	boisei	k1gNnSc1	boisei
<g/>
,	,	kIx,	,
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
robustus	robustus	k1gInSc1	robustus
Rod	rod	k1gInSc1	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
se	se	k3xPyFc4	se
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
existence	existence	k1gFnSc2	existence
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
utváření	utváření	k1gNnSc1	utváření
svébytných	svébytný	k2eAgInPc2d1	svébytný
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
postupné	postupný	k2eAgNnSc1d1	postupné
odlišování	odlišování	k1gNnSc1	odlišování
se	se	k3xPyFc4	se
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
typu	typ	k1gInSc6	typ
dělení	dělení	k1gNnSc2	dělení
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
robustní	robustní	k2eAgInPc4d1	robustní
druhy	druh	k1gInPc4	druh
samostatné	samostatný	k2eAgNnSc4d1	samostatné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
archaické	archaický	k2eAgInPc1d1	archaický
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
vydělováni	vydělovat	k5eAaImNgMnP	vydělovat
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Praeanthropus	Praeanthropus	k1gInSc1	Praeanthropus
<g/>
.	.	kIx.	.
archaičtí	archaický	k2eAgMnPc1d1	archaický
-	-	kIx~	-
A.	A.	kA	A.
anamensis	anamensis	k1gInSc1	anamensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
pokročilí	pokročilý	k1gMnPc1	pokročilý
-	-	kIx~	-
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
sediba	sediba	k1gFnSc1	sediba
<g/>
,	,	kIx,	,
A.	A.	kA	A.
garhi	garhi	k6eAd1	garhi
robustní	robustní	k2eAgFnSc7d1	robustní
-	-	kIx~	-
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
aethiopicus	aethiopicus	k1gMnSc1	aethiopicus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
boisei	boisei	k1gNnSc1	boisei
<g/>
,	,	kIx,	,
A.	A.	kA	A.
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
robustus	robustus	k1gInSc1	robustus
Australopitékové	Australopitéková	k1gFnSc2	Australopitéková
tvoří	tvořit	k5eAaImIp3nS	tvořit
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgFnSc4d1	variabilní
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gFnSc4	jejich
celkovou	celkový	k2eAgFnSc4d1	celková
charakteristiku	charakteristika	k1gFnSc4	charakteristika
lze	lze	k6eAd1	lze
podat	podat	k5eAaPmF	podat
jen	jen	k9	jen
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
podobala	podobat	k5eAaImAgFnS	podobat
spíše	spíše	k9	spíše
lidoopům	lidoop	k1gMnPc3	lidoop
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
již	již	k6eAd1	již
neslo	nést	k5eAaImAgNnS	nést
početné	početný	k2eAgNnSc1d1	početné
lidské	lidský	k2eAgInPc1d1	lidský
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dožívali	dožívat	k5eAaImAgMnP	dožívat
asi	asi	k9	asi
40	[number]	k4	40
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
moderními	moderní	k2eAgMnPc7d1	moderní
lidmi	člověk	k1gMnPc7	člověk
byli	být	k5eAaImAgMnP	být
poměrně	poměrně	k6eAd1	poměrně
malí	malý	k1gMnPc1	malý
-	-	kIx~	-
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
100	[number]	k4	100
-	-	kIx~	-
160	[number]	k4	160
cm	cm	kA	cm
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
30	[number]	k4	30
-	-	kIx~	-
50	[number]	k4	50
kg	kg	kA	kg
se	se	k3xPyFc4	se
podobali	podobat	k5eAaImAgMnP	podobat
spíše	spíše	k9	spíše
dnešním	dnešní	k2eAgMnSc7d1	dnešní
šimpanzům	šimpanz	k1gMnPc3	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
nalézaných	nalézaný	k2eAgInPc2d1	nalézaný
ostatků	ostatek	k1gInPc2	ostatek
ale	ale	k8xC	ale
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
výrazný	výrazný	k2eAgInSc4d1	výrazný
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samci	samec	k1gMnPc1	samec
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
proti	proti	k7c3	proti
samicím	samice	k1gFnPc3	samice
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
větší	veliký	k2eAgMnSc1d2	veliký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
jako	jako	k9	jako
u	u	k7c2	u
dnešních	dnešní	k2eAgFnPc2d1	dnešní
goril	gorila	k1gFnPc2	gorila
nebo	nebo	k8xC	nebo
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
však	však	k9	však
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
výrazných	výrazný	k2eAgInPc2d1	výrazný
rozdílů	rozdíl	k1gInPc2	rozdíl
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
samic	samice	k1gFnPc2	samice
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
a	a	k8xC	a
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
jen	jen	k9	jen
minimální	minimální	k2eAgFnPc1d1	minimální
odlišnosti	odlišnost	k1gFnPc1	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dojem	dojem	k1gInSc4	dojem
velkého	velký	k2eAgInSc2d1	velký
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
důsledkem	důsledek	k1gInSc7	důsledek
srovnávání	srovnávání	k1gNnSc2	srovnávání
nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
geografického	geografický	k2eAgNnSc2d1	geografické
i	i	k8xC	i
časového	časový	k2eAgNnSc2d1	časové
rozpětí	rozpětí	k1gNnSc2	rozpětí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
australopitéky	australopitéek	k1gMnPc4	australopitéek
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
typický	typický	k2eAgInSc1d1	typický
ještě	ještě	k9	ještě
vcelku	vcelku	k6eAd1	vcelku
malý	malý	k2eAgInSc1d1	malý
mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
při	při	k7c6	při
horní	horní	k2eAgFnSc6d1	horní
hranici	hranice	k1gFnSc6	hranice
rozpětí	rozpětí	k1gNnSc1	rozpětí
současných	současný	k2eAgMnPc2d1	současný
lidoopů	lidoop	k1gMnPc2	lidoop
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
450	[number]	k4	450
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
duševní	duševní	k2eAgFnPc4d1	duševní
schopnosti	schopnost	k1gFnPc4	schopnost
těchto	tento	k3xDgInPc2	tento
časných	časný	k2eAgInPc2d1	časný
homininů	hominin	k1gInPc2	hominin
výrazně	výrazně	k6eAd1	výrazně
přesahovaly	přesahovat	k5eAaImAgFnP	přesahovat
možnosti	možnost	k1gFnPc1	možnost
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
,	,	kIx,	,
goril	gorila	k1gFnPc2	gorila
nebo	nebo	k8xC	nebo
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
mozku	mozek	k1gInSc2	mozek
i	i	k9	i
k	k	k7c3	k
průběhu	průběh	k1gInSc2	průběh
prořezávání	prořezávání	k1gNnSc2	prořezávání
zubů	zub	k1gInPc2	zub
lze	lze	k6eAd1	lze
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
příliš	příliš	k6eAd1	příliš
nelišila	lišit	k5eNaImAgFnS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
prvních	první	k4xOgInPc6	první
náznacích	náznak	k1gInPc6	náznak
reorganizace	reorganizace	k1gFnSc2	reorganizace
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
tak	tak	k9	tak
předcházela	předcházet	k5eAaImAgFnS	předcházet
výraznějšímu	výrazný	k2eAgNnSc3d2	výraznější
zvětšování	zvětšování	k1gNnSc3	zvětšování
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgFnPc1d1	dostupná
lebeční	lebeční	k2eAgFnPc1d1	lebeční
kosti	kost	k1gFnPc1	kost
i	i	k8xC	i
přirozené	přirozený	k2eAgInPc1d1	přirozený
výlitky	výlitek	k1gInPc1	výlitek
mozkoven	mozkovna	k1gFnPc2	mozkovna
mohou	moct	k5eAaImIp3nP	moct
nasvědčovat	nasvědčovat	k5eAaImF	nasvědčovat
mírnému	mírný	k2eAgNnSc3d1	mírné
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
temenního	temenní	k2eAgInSc2d1	temenní
a	a	k8xC	a
čelního	čelní	k2eAgInSc2d1	čelní
laloku	lalok	k1gInSc2	lalok
i	i	k8xC	i
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
spánkového	spánkový	k2eAgInSc2d1	spánkový
laloku	lalok	k1gInSc2	lalok
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
týlních	týlní	k2eAgFnPc2d1	týlní
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
Měsíčitá	Měsíčitý	k2eAgFnSc1d1	Měsíčitý
rýha	rýha	k1gFnSc1	rýha
(	(	kIx(	(
<g/>
sulcus	sulcus	k1gMnSc1	sulcus
lunatus	lunatus	k1gMnSc1	lunatus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
posouvá	posouvat	k5eAaImIp3nS	posouvat
směrem	směr	k1gInSc7	směr
vzad	vzad	k6eAd1	vzad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
sediba	sedib	k1gMnSc2	sedib
je	být	k5eAaImIp3nS	být
pravá	pravý	k2eAgFnSc1d1	pravá
hemisféra	hemisféra	k1gFnSc1	hemisféra
mírně	mírně	k6eAd1	mírně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
levá	levý	k2eAgFnSc1d1	levá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
preferencí	preference	k1gFnSc7	preference
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
stavba	stavba	k1gFnSc1	stavba
mozku	mozek	k1gInSc2	mozek
australopitéků	australopitéek	k1gMnPc2	australopitéek
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
blíží	blížit	k5eAaImIp3nS	blížit
spíše	spíše	k9	spíše
lidoopům	lidoop	k1gMnPc3	lidoop
než	než	k8xS	než
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
rysy	rys	k1gInPc1	rys
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
předmětem	předmět	k1gInSc7	předmět
živých	živý	k2eAgFnPc2d1	živá
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dochovaných	dochovaný	k2eAgInPc6d1	dochovaný
ostatcích	ostatek	k1gInPc6	ostatek
nejednoznačný	jednoznačný	k2eNgInSc1d1	nejednoznačný
nebo	nebo	k8xC	nebo
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
.	.	kIx.	.
</s>
<s>
Mozkovnu	mozkovna	k1gFnSc4	mozkovna
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
nápadné	nápadný	k2eAgInPc1d1	nápadný
úpony	úpon	k1gInPc1	úpon
dobře	dobře	k6eAd1	dobře
vyvinutých	vyvinutý	k2eAgInPc2d1	vyvinutý
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
u	u	k7c2	u
robustních	robustní	k2eAgInPc2d1	robustní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
šíjový	šíjový	k2eAgInSc4d1	šíjový
hřeben	hřeben	k1gInSc4	hřeben
crista	crist	k1gMnSc2	crist
nuchae	nucha	k1gInSc2	nucha
pro	pro	k7c4	pro
šíjové	šíjový	k2eAgInPc4d1	šíjový
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
vyvažující	vyvažující	k2eAgFnSc4d1	vyvažující
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
masivními	masivní	k2eAgFnPc7d1	masivní
čelistmi	čelist	k1gFnPc7	čelist
a	a	k8xC	a
u	u	k7c2	u
větších	veliký	k2eAgMnPc2d2	veliký
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
samců	samec	k1gInPc2	samec
<g/>
)	)	kIx)	)
i	i	k9	i
o	o	k7c4	o
šípový	šípový	k2eAgInSc4d1	šípový
hřeben	hřeben	k1gInSc4	hřeben
crista	crist	k1gMnSc2	crist
sagittalis	sagittalis	k1gFnSc2	sagittalis
pro	pro	k7c4	pro
spánkový	spánkový	k2eAgInSc4d1	spánkový
sval	sval	k1gInSc4	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gInSc1	musculus
temporalis	temporalis	k1gFnSc2	temporalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
silnou	silný	k2eAgFnSc4d1	silná
práci	práce	k1gFnSc4	práce
předních	přední	k2eAgMnPc2d1	přední
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
výrazné	výrazný	k2eAgNnSc1d1	výrazné
zúžení	zúžení	k1gNnSc1	zúžení
lebky	lebka	k1gFnSc2	lebka
za	za	k7c7	za
očnicemi	očnice	k1gFnPc7	očnice
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dostatek	dostatek	k1gInSc1	dostatek
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
zvětšený	zvětšený	k2eAgInSc4d1	zvětšený
spánkový	spánkový	k2eAgInSc4d1	spánkový
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jihoafrických	jihoafrický	k2eAgInPc2d1	jihoafrický
druhů	druh	k1gInPc2	druh
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
a	a	k8xC	a
P.	P.	kA	P.
robustus	robustus	k1gInSc1	robustus
navíc	navíc	k6eAd1	navíc
obličej	obličej	k1gInSc4	obličej
zpevňují	zpevňovat	k5eAaImIp3nP	zpevňovat
masivní	masivní	k2eAgInPc1d1	masivní
kostěné	kostěný	k2eAgInPc1d1	kostěný
sloupky	sloupek	k1gInPc1	sloupek
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
nosního	nosní	k2eAgInSc2d1	nosní
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
přední	přední	k2eAgInPc4d1	přední
pilíře	pilíř	k1gInPc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
miskovitě	miskovitě	k6eAd1	miskovitě
prohloubený	prohloubený	k2eAgInSc4d1	prohloubený
obličej	obličej	k1gInSc4	obličej
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
nosem	nos	k1gInSc7	nos
a	a	k8xC	a
výraznými	výrazný	k2eAgFnPc7d1	výrazná
lícními	lícní	k2eAgFnPc7d1	lícní
kostmi	kost	k1gFnPc7	kost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svým	svůj	k3xOyFgNnSc7	svůj
vyklenutím	vyklenutí	k1gNnSc7	vyklenutí
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
zevní	zevní	k2eAgInSc4d1	zevní
žvýkací	žvýkací	k2eAgInSc4d1	žvýkací
sval	sval	k1gInSc4	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gMnSc1	musculus
masseter	masseter	k1gMnSc1	masseter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
vzad	vzad	k6eAd1	vzad
ubíhající	ubíhající	k2eAgNnSc4d1	ubíhající
čelo	čelo	k1gNnSc4	čelo
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
výrazné	výrazný	k2eAgInPc1d1	výrazný
nadočnicové	nadočnicový	k2eAgInPc1d1	nadočnicový
oblouky	oblouk	k1gInPc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
australopitéky	australopiték	k1gInPc4	australopiték
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
také	také	k9	také
vystupující	vystupující	k2eAgInPc1d1	vystupující
<g/>
,	,	kIx,	,
masivně	masivně	k6eAd1	masivně
stavěné	stavěný	k2eAgFnPc1d1	stavěná
čelisti	čelist	k1gFnPc1	čelist
bez	bez	k7c2	bez
brady	brada	k1gFnSc2	brada
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc4	zub
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
hrbolky	hrbolek	k1gInPc7	hrbolek
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
sklovinou	sklovina	k1gFnSc7	sklovina
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
zvětšené	zvětšený	k2eAgFnPc1d1	zvětšená
stoličky	stolička	k1gFnPc1	stolička
a	a	k8xC	a
třenové	třenový	k2eAgInPc1d1	třenový
zuby	zub	k1gInPc1	zub
(	(	kIx(	(
<g/>
megadoncie	megadoncie	k1gFnSc1	megadoncie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zmenšené	zmenšený	k2eAgInPc1d1	zmenšený
řezáky	řezák	k1gInPc1	řezák
a	a	k8xC	a
špičáky	špičák	k1gInPc1	špičák
téměř	téměř	k6eAd1	téměř
zarovnané	zarovnaný	k2eAgInPc1d1	zarovnaný
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
dentice	dentice	k1gFnSc1	dentice
uložena	uložit	k5eAaPmNgFnS	uložit
ještě	ještě	k9	ještě
v	v	k7c6	v
paralelních	paralelní	k2eAgFnPc6d1	paralelní
řadách	řada	k1gFnPc6	řada
<g/>
,	,	kIx,	,
u	u	k7c2	u
mladších	mladý	k2eAgMnPc2d2	mladší
zástupců	zástupce	k1gMnPc2	zástupce
(	(	kIx(	(
<g/>
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
sediba	sediba	k1gFnSc1	sediba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
utváří	utvářit	k5eAaPmIp3nP	utvářit
parabolický	parabolický	k2eAgInSc4d1	parabolický
zubní	zubní	k2eAgInSc4d1	zubní
oblouk	oblouk	k1gInSc4	oblouk
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
trupu	trup	k1gInSc2	trup
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
nese	nést	k5eAaImIp3nS	nést
množství	množství	k1gNnSc1	množství
archaických	archaický	k2eAgInPc2d1	archaický
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vzhledu	vzhled	k1gInSc2	vzhled
moderních	moderní	k2eAgMnPc2d1	moderní
lidí	člověk	k1gMnPc2	člověk
blíží	blížit	k5eAaImIp3nS	blížit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
lebka	lebka	k1gFnSc1	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
má	mít	k5eAaImIp3nS	mít
spíš	spíš	k9	spíš
kónický	kónický	k2eAgInSc4d1	kónický
tvar	tvar	k1gInSc4	tvar
jako	jako	k8xS	jako
u	u	k7c2	u
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
soudkovitý	soudkovitý	k2eAgMnSc1d1	soudkovitý
jako	jako	k8xC	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
esovitě	esovitě	k6eAd1	esovitě
prohnutá	prohnutý	k2eAgFnSc1d1	prohnutá
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
nálezy	nález	k1gInPc1	nález
bederních	bederní	k2eAgInPc2d1	bederní
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
australopitékové	australopitéek	k1gMnPc1	australopitéek
měli	mít	k5eAaImAgMnP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
značně	značně	k6eAd1	značně
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
bederní	bederní	k2eAgFnSc4d1	bederní
část	část	k1gFnSc4	část
páteře	páteř	k1gFnSc2	páteř
se	s	k7c7	s
šesti	šest	k4xCc7	šest
obratli	obratel	k1gInPc7	obratel
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
bederních	bederní	k2eAgInPc2d1	bederní
obratlů	obratel	k1gInPc2	obratel
jen	jen	k9	jen
pět	pět	k4xCc4	pět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
se	se	k3xPyFc4	se
však	však	k9	však
zakládá	zakládat	k5eAaImIp3nS	zakládat
především	především	k6eAd1	především
na	na	k7c6	na
rozboru	rozbor	k1gInSc6	rozbor
jihoafrických	jihoafrický	k2eAgInPc2d1	jihoafrický
nálezů	nález	k1gInPc2	nález
ze	z	k7c2	z
Sterkfontein	Sterkfonteina	k1gFnPc2	Sterkfonteina
a	a	k8xC	a
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
jedinců	jedinec	k1gMnPc2	jedinec
nebyl	být	k5eNaImAgInS	být
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
anomálii	anomálie	k1gFnSc4	anomálie
nebo	nebo	k8xC	nebo
o	o	k7c4	o
typický	typický	k2eAgInSc4d1	typický
znak	znak	k1gInSc4	znak
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Pánev	pánev	k1gFnSc1	pánev
australopitéků	australopitéek	k1gMnPc2	australopitéek
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
všechny	všechen	k3xTgInPc4	všechen
zásadní	zásadní	k2eAgInPc4d1	zásadní
znaky	znak	k1gInPc4	znak
lidské	lidský	k2eAgFnSc2d1	lidská
bipedie	bipedie	k1gFnSc2	bipedie
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
stabilními	stabilní	k2eAgInPc7d1	stabilní
klouby	kloub	k1gInPc7	kloub
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
oporu	opora	k1gFnSc4	opora
plně	plně	k6eAd1	plně
vzpřímenému	vzpřímený	k2eAgNnSc3d1	vzpřímené
tělu	tělo	k1gNnSc3	tělo
a	a	k8xC	a
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
při	při	k7c6	při
dvojnohé	dvojnohý	k2eAgFnSc6d1	dvojnohá
chůzi	chůze	k1gFnSc6	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc4d1	stabilní
chůzi	chůze	k1gFnSc4	chůze
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
i	i	k9	i
posunutí	posunutí	k1gNnSc1	posunutí
kolen	koleno	k1gNnPc2	koleno
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
mělká	mělký	k2eAgFnSc1d1	mělká
jamka	jamka	k1gFnSc1	jamka
kyčelního	kyčelní	k2eAgInSc2d1	kyčelní
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc3d1	odpovídající
malé	malý	k2eAgFnSc3d1	malá
hlavici	hlavice	k1gFnSc3	hlavice
stehenní	stehenní	k2eAgFnSc6d1	stehenní
kosti	kost	k1gFnSc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
končetiny	končetina	k1gFnPc1	končetina
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
pažemi	paže	k1gFnPc7	paže
ještě	ještě	k6eAd1	ještě
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
prsty	prst	k1gInPc1	prst
nohou	noha	k1gFnPc2	noha
byly	být	k5eAaImAgInP	být
lehce	lehko	k6eAd1	lehko
zakřivené	zakřivený	k2eAgInPc1d1	zakřivený
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
palec	palec	k1gInSc1	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
přitažený	přitažený	k2eAgInSc1d1	přitažený
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
prstům	prst	k1gInPc3	prst
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zachovával	zachovávat	k5eAaImAgInS	zachovávat
větší	veliký	k2eAgInSc4d2	veliký
rozsah	rozsah	k1gInSc4	rozsah
pohybu	pohyb	k1gInSc2	pohyb
než	než	k8xS	než
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Chodidlo	chodidlo	k1gNnSc1	chodidlo
mělo	mít	k5eAaImAgNnS	mít
oproti	oproti	k7c3	oproti
lidem	člověk	k1gMnPc3	člověk
mírně	mírně	k6eAd1	mírně
pohyblivější	pohyblivý	k2eAgNnSc4d2	pohyblivější
zánártí	zánártí	k1gNnSc4	zánártí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
příčnou	příčný	k2eAgFnSc4d1	příčná
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
nízkou	nízký	k2eAgFnSc7d1	nízká
podélnou	podélný	k2eAgFnSc7d1	podélná
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
horní	horní	k2eAgFnSc4d1	horní
končetinu	končetina	k1gFnSc4	končetina
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
primitivně	primitivně	k6eAd1	primitivně
prodloužené	prodloužený	k2eAgNnSc1d1	prodloužené
a	a	k8xC	a
svalnaté	svalnatý	k2eAgNnSc1d1	svalnaté
předloktí	předloktí	k1gNnSc1	předloktí
i	i	k8xC	i
větší	veliký	k2eAgInSc1d2	veliký
rozsah	rozsah	k1gInSc1	rozsah
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
ramenním	ramenní	k2eAgInSc6d1	ramenní
kloubu	kloub	k1gInSc6	kloub
a	a	k8xC	a
v	v	k7c6	v
zápěstí	zápěstí	k1gNnSc6	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ruky	ruka	k1gFnSc2	ruka
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
nelišila	lišit	k5eNaImAgFnS	lišit
od	od	k7c2	od
lidských	lidský	k2eAgFnPc2d1	lidská
dlaní	dlaň	k1gFnPc2	dlaň
<g/>
,	,	kIx,	,
jen	jen	k9	jen
prsty	prst	k1gInPc1	prst
byly	být	k5eAaImAgInP	být
mírně	mírně	k6eAd1	mírně
prohnuté	prohnutý	k2eAgInPc1d1	prohnutý
<g/>
.	.	kIx.	.
</s>
<s>
Nebyly	být	k5eNaImAgInP	být
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
palcem	palec	k1gInSc7	palec
tak	tak	k6eAd1	tak
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
jako	jako	k8xS	jako
u	u	k7c2	u
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
palec	palec	k1gInSc1	palec
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
moderním	moderní	k2eAgMnPc3d1	moderní
šimpanzům	šimpanz	k1gMnPc3	šimpanz
a	a	k8xC	a
gorilám	gorila	k1gFnPc3	gorila
lépe	dobře	k6eAd2	dobře
osvalený	osvalený	k2eAgInSc4d1	osvalený
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
pohyblivější	pohyblivý	k2eAgFnPc1d2	pohyblivější
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéek	k1gMnPc1	Australopitéek
obývali	obývat	k5eAaImAgMnP	obývat
převážně	převážně	k6eAd1	převážně
tropický	tropický	k2eAgInSc4d1	tropický
podnebný	podnebný	k2eAgInSc4d1	podnebný
pás	pás	k1gInSc4	pás
a	a	k8xC	a
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
i	i	k9	i
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
(	(	kIx(	(
<g/>
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
sediba	sediba	k1gFnSc1	sediba
<g/>
,	,	kIx,	,
P.	P.	kA	P.
robustus	robustus	k1gInSc1	robustus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
až	až	k9	až
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gNnSc1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
australopitéků	australopitéek	k1gMnPc2	australopitéek
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
z	z	k7c2	z
širšího	široký	k2eAgNnSc2d2	širší
spektra	spektrum	k1gNnSc2	spektrum
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
s	s	k7c7	s
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
přežíváním	přežívání	k1gNnSc7	přežívání
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
postupnými	postupný	k2eAgFnPc7d1	postupná
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
však	však	k9	však
s	s	k7c7	s
přibývajícími	přibývající	k2eAgInPc7d1	přibývající
nálezy	nález	k1gInPc7	nález
neustále	neustále	k6eAd1	neustále
potvrzována	potvrzovat	k5eAaImNgFnS	potvrzovat
značná	značný	k2eAgFnSc1d1	značná
ekologická	ekologický	k2eAgFnSc1d1	ekologická
tolerance	tolerance	k1gFnSc1	tolerance
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
najít	najít	k5eAaPmF	najít
obživu	obživa	k1gFnSc4	obživa
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
prostředí	prostředí	k1gNnPc2	prostředí
od	od	k7c2	od
sušší	suchý	k2eAgFnSc2d2	sušší
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgFnSc2d1	otevřená
savany	savana	k1gFnSc2	savana
přes	přes	k7c4	přes
krasové	krasový	k2eAgFnPc4d1	krasová
oblasti	oblast	k1gFnPc4	oblast
až	až	k9	až
po	po	k7c4	po
prosvětlené	prosvětlený	k2eAgInPc4d1	prosvětlený
či	či	k8xC	či
hustší	hustý	k2eAgInPc4d2	hustší
vlhké	vlhký	k2eAgInPc4d1	vlhký
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
spojované	spojovaný	k2eAgInPc1d1	spojovaný
nejčastěji	často	k6eAd3	často
se	s	k7c7	s
záplavovým	záplavový	k2eAgNnSc7d1	záplavové
územím	území	k1gNnSc7	území
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgInPc1d1	detailní
výzkumy	výzkum	k1gInPc1	výzkum
tedy	tedy	k9	tedy
příliš	příliš	k6eAd1	příliš
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
dřívější	dřívější	k2eAgInPc1d1	dřívější
předpoklady	předpoklad	k1gInPc1	předpoklad
o	o	k7c6	o
výhradním	výhradní	k2eAgNnSc6d1	výhradní
sepětí	sepětí	k1gNnSc6	sepětí
australopitéků	australopiték	k1gInPc2	australopiték
se	s	k7c7	s
suchými	suchý	k2eAgFnPc7d1	suchá
travnatými	travnatý	k2eAgFnPc7d1	travnatá
pláněmi	pláň	k1gFnPc7	pláň
bez	bez	k7c2	bez
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
přednost	přednost	k1gFnSc4	přednost
dávali	dávat	k5eAaImAgMnP	dávat
spíše	spíše	k9	spíše
zalesněnému	zalesněný	k2eAgNnSc3d1	zalesněné
prostředí	prostředí	k1gNnSc3	prostředí
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
vláhy	vláha	k1gFnSc2	vláha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
postupně	postupně	k6eAd1	postupně
pronikali	pronikat	k5eAaImAgMnP	pronikat
i	i	k9	i
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
starší	starý	k2eAgMnPc4d2	starší
druhy	druh	k1gMnPc4	druh
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mnoha	mnoho	k4c2	mnoho
přírodovědných	přírodovědný	k2eAgFnPc2d1	přírodovědná
analýz	analýza	k1gFnPc2	analýza
(	(	kIx(	(
<g/>
nálezy	nález	k1gInPc1	nález
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
půdy	půda	k1gFnSc2	půda
nebo	nebo	k8xC	nebo
zubní	zubní	k2eAgFnSc2d1	zubní
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
)	)	kIx)	)
nejčastěji	často	k6eAd3	často
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
pestré	pestrý	k2eAgFnSc6d1	pestrá
mozaikovité	mozaikovitý	k2eAgFnSc6d1	mozaikovitá
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mohly	moct	k5eAaImAgFnP	moct
převládat	převládat	k5eAaImF	převládat
otevřené	otevřený	k2eAgFnPc4d1	otevřená
travnaté	travnatý	k2eAgFnPc4d1	travnatá
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
i	i	k9	i
dostatek	dostatek	k1gInSc1	dostatek
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
mokřin	mokřina	k1gFnPc2	mokřina
a	a	k8xC	a
menších	malý	k2eAgInPc2d2	menší
či	či	k8xC	či
větších	veliký	k2eAgInPc2d2	veliký
úseků	úsek	k1gInPc2	úsek
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
a	a	k8xC	a
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgFnPc2d1	poskytující
potravu	potrava	k1gFnSc4	potrava
i	i	k9	i
úkryt	úkryt	k1gInSc4	úkryt
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
patrně	patrně	k6eAd1	patrně
již	již	k9	již
před	před	k7c7	před
3,6	[number]	k4	3,6
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
obýval	obývat	k5eAaImAgMnS	obývat
výrazně	výrazně	k6eAd1	výrazně
otevřenější	otevřený	k2eAgInSc4d2	otevřenější
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
stromové	stromový	k2eAgInPc1d1	stromový
porosty	porost	k1gInPc1	porost
mohly	moct	k5eAaImAgInP	moct
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
malé	malý	k2eAgInPc4d1	malý
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgInPc1d2	mladší
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
robustní	robustní	k2eAgFnPc1d1	robustní
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
pronikaly	pronikat	k5eAaImAgInP	pronikat
do	do	k7c2	do
sušších	suchý	k2eAgFnPc2d2	sušší
nezalesněných	zalesněný	k2eNgFnPc2d1	nezalesněná
prostor	prostora	k1gFnPc2	prostora
savany	savana	k1gFnSc2	savana
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
soudobých	soudobý	k2eAgMnPc2d1	soudobý
zástupců	zástupce	k1gMnPc2	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
stále	stále	k6eAd1	stále
drželi	držet	k5eAaImAgMnP	držet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mokřin	mokřina	k1gFnPc2	mokřina
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
vlhčích	vlhký	k2eAgNnPc2d2	vlhčí
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k6eAd1	ještě
mohly	moct	k5eAaImAgFnP	moct
přežívat	přežívat	k5eAaImF	přežívat
i	i	k9	i
řídké	řídký	k2eAgInPc4d1	řídký
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Stravě	strava	k1gFnSc3	strava
australopitéků	australopiték	k1gInPc2	australopiték
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
nálezů	nález	k1gInPc2	nález
věnována	věnován	k2eAgFnSc1d1	věnována
značná	značný	k2eAgFnSc1d1	značná
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
díky	díky	k7c3	díky
jedinečným	jedinečný	k2eAgFnPc3d1	jedinečná
adaptacím	adaptace	k1gFnPc3	adaptace
chrupu	chrup	k1gInSc2	chrup
a	a	k8xC	a
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
u	u	k7c2	u
nejstarších	starý	k2eAgMnPc2d3	nejstarší
zástupců	zástupce	k1gMnPc2	zástupce
(	(	kIx(	(
<g/>
A.	A.	kA	A.
anamensis	anamensis	k1gInSc1	anamensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
afarensis	afarensis	k1gFnSc1	afarensis
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
patrná	patrný	k2eAgNnPc1d1	patrné
zesílení	zesílení	k1gNnPc1	zesílení
žvýkacího	žvýkací	k2eAgInSc2d1	žvýkací
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
určité	určitý	k2eAgFnPc1d1	určitá
změny	změna	k1gFnPc1	změna
potravních	potravní	k2eAgInPc2d1	potravní
návyků	návyk	k1gInPc2	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgFnPc1d1	masivní
čelisti	čelist	k1gFnPc1	čelist
a	a	k8xC	a
enormně	enormně	k6eAd1	enormně
velké	velký	k2eAgFnPc1d1	velká
stoličky	stolička	k1gFnPc1	stolička
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
sklovinou	sklovina	k1gFnSc7	sklovina
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přispívaly	přispívat	k5eAaImAgFnP	přispívat
k	k	k7c3	k
předpokladu	předpoklad	k1gInSc3	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
australopitékové	australopitéek	k1gMnPc1	australopitéek
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předků	předek	k1gInPc2	předek
moderních	moderní	k2eAgMnPc2d1	moderní
lidoopů	lidoop	k1gMnPc2	lidoop
přešli	přejít	k5eAaPmAgMnP	přejít
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
tvrdé	tvrdá	k1gFnSc2	tvrdá
nebo	nebo	k8xC	nebo
silně	silně	k6eAd1	silně
abrazivní	abrazivní	k2eAgFnSc2d1	abrazivní
stravy	strava	k1gFnSc2	strava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
či	či	k8xC	či
kořínky	kořínek	k1gInPc1	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
změnami	změna	k1gFnPc7	změna
-	-	kIx~	-
vysušováním	vysušování	k1gNnSc7	vysušování
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
ústupem	ústup	k1gInSc7	ústup
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savany	savana	k1gFnSc2	savana
nacházel	nacházet	k5eAaImAgInS	nacházet
dostatek	dostatek	k1gInSc1	dostatek
měkkých	měkký	k2eAgInPc2d1	měkký
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Úzká	úzký	k2eAgFnSc1d1	úzká
specializace	specializace	k1gFnSc1	specializace
na	na	k7c4	na
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
a	a	k8xC	a
tuhou	tuhý	k2eAgFnSc4d1	tuhá
stravu	strava	k1gFnSc4	strava
se	se	k3xPyFc4	se
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
zejména	zejména	k9	zejména
u	u	k7c2	u
robustních	robustní	k2eAgInPc2d1	robustní
australopitéků	australopiték	k1gInPc2	australopiték
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
masivní	masivní	k2eAgFnSc7d1	masivní
stavbou	stavba	k1gFnSc7	stavba
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
čelistí	čelist	k1gFnPc2	čelist
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
pokládáni	pokládán	k2eAgMnPc1d1	pokládán
za	za	k7c4	za
výhradní	výhradní	k2eAgMnPc4d1	výhradní
býložravce	býložravec	k1gMnPc4	býložravec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc4d1	ostatní
australopitékové	australopitéková	k1gFnPc4	australopitéková
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
předkové	předek	k1gMnPc1	předek
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
všežravci	všežravec	k1gMnPc1	všežravec
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
<g/>
,	,	kIx,	,
odvozované	odvozovaný	k2eAgInPc1d1	odvozovaný
z	z	k7c2	z
morfologie	morfologie	k1gFnSc2	morfologie
chrupu	chrup	k1gInSc2	chrup
však	však	k9	však
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
významně	významně	k6eAd1	významně
mění	měnit	k5eAaImIp3nS	měnit
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
počet	počet	k1gInSc4	počet
detailních	detailní	k2eAgFnPc2d1	detailní
analýz	analýza	k1gFnPc2	analýza
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
mikroskopického	mikroskopický	k2eAgNnSc2d1	mikroskopické
poškození	poškození	k1gNnSc2	poškození
zubní	zubní	k2eAgFnSc2d1	zubní
skloviny	sklovina	k1gFnSc2	sklovina
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podíl	podíl	k1gInSc1	podíl
tvrdé	tvrdá	k1gFnSc2	tvrdá
<g/>
,	,	kIx,	,
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
náročné	náročný	k2eAgFnSc2d1	náročná
stravy	strava	k1gFnSc2	strava
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
nijak	nijak	k6eAd1	nijak
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stopy	stopa	k1gFnSc2	stopa
zanechané	zanechaný	k2eAgFnSc2d1	zanechaná
konzumovaným	konzumovaný	k2eAgNnSc7d1	konzumované
jídlem	jídlo	k1gNnSc7	jídlo
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
spíše	spíše	k9	spíše
měkčích	měkký	k2eAgFnPc2d2	měkčí
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Analýzy	analýza	k1gFnPc1	analýza
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
skloviny	sklovina	k1gFnSc2	sklovina
sice	sice	k8xC	sice
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
příjem	příjem	k1gInSc4	příjem
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
bezlesých	bezlesý	k2eAgFnPc2d1	bezlesá
prostor	prostora	k1gFnPc2	prostora
(	(	kIx(	(
<g/>
traviny	travina	k1gFnPc1	travina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
kořínky	kořínek	k1gInPc1	kořínek
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
a	a	k8xC	a
zásobní	zásobní	k2eAgInPc1d1	zásobní
orgány	orgán	k1gInPc1	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vždy	vždy	k6eAd1	vždy
výrazně	výrazně	k6eAd1	výrazně
doplňovaný	doplňovaný	k2eAgInSc1d1	doplňovaný
měkčími	měkký	k2eAgInPc7d2	měkčí
plody	plod	k1gInPc7	plod
z	z	k7c2	z
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
jen	jen	k6eAd1	jen
částečnému	částečný	k2eAgInSc3d1	částečný
a	a	k8xC	a
postupnému	postupný	k2eAgNnSc3d1	postupné
pronikání	pronikání	k1gNnSc3	pronikání
do	do	k7c2	do
otevřených	otevřený	k2eAgInPc2d1	otevřený
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
jídelníčku	jídelníček	k1gInSc2	jídelníček
došlo	dojít	k5eAaPmAgNnS	dojít
spíše	spíše	k9	spíše
rozšířením	rozšíření	k1gNnSc7	rozšíření
spektra	spektrum	k1gNnSc2	spektrum
přijímané	přijímaný	k2eAgFnSc2d1	přijímaná
potravy	potrava	k1gFnSc2	potrava
než	než	k8xS	než
specializací	specializace	k1gFnSc7	specializace
a	a	k8xC	a
přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
typu	typ	k1gInSc2	typ
k	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéek	k1gMnPc1	Australopitéek
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
robustních	robustní	k2eAgInPc2d1	robustní
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
dokázali	dokázat	k5eAaPmAgMnP	dokázat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
využít	využít	k5eAaPmF	využít
širší	široký	k2eAgNnSc4d2	širší
spektrum	spektrum	k1gNnSc4	spektrum
biotopů	biotop	k1gInPc2	biotop
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
najít	najít	k5eAaPmF	najít
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
v	v	k7c4	v
dynamicky	dynamicky	k6eAd1	dynamicky
se	se	k3xPyFc4	se
měnícím	měnící	k2eAgMnSc7d1	měnící
<g/>
,	,	kIx,	,
nestabilním	stabilní	k2eNgInSc7d1	nestabilní
prostředí	prostředí	k1gNnSc4	prostředí
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
sezónností	sezónnost	k1gFnSc7	sezónnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
určit	určit	k5eAaPmF	určit
převládající	převládající	k2eAgFnSc4d1	převládající
složku	složka	k1gFnSc4	složka
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Preferováno	preferovat	k5eAaImNgNnS	preferovat
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
ovoce	ovoce	k1gNnPc4	ovoce
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
měkké	měkký	k2eAgFnPc4d1	měkká
části	část	k1gFnPc4	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
nahrazované	nahrazovaný	k2eAgInPc4d1	nahrazovaný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
nedostatku	nedostatek	k1gInSc2	nedostatek
jinými	jiný	k2eAgInPc7d1	jiný
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
robustního	robustní	k2eAgInSc2d1	robustní
žvýkacího	žvýkací	k2eAgInSc2d1	žvýkací
aparátu	aparát	k1gInSc2	aparát
již	již	k6eAd1	již
u	u	k7c2	u
nejstarších	starý	k2eAgMnPc2d3	nejstarší
australopitéků	australopitéek	k1gMnPc2	australopitéek
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
spíše	spíše	k9	spíše
než	než	k8xS	než
denní	denní	k2eAgFnSc1d1	denní
rutina	rutina	k1gFnSc1	rutina
přispět	přispět	k5eAaPmF	přispět
jen	jen	k9	jen
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kriticky	kriticky	k6eAd1	kriticky
významná	významný	k2eAgFnSc1d1	významná
konzumace	konzumace	k1gFnSc1	konzumace
méně	málo	k6eAd2	málo
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tuhá	tuhý	k2eAgFnSc1d1	tuhá
a	a	k8xC	a
abrazivní	abrazivní	k2eAgFnSc1d1	abrazivní
potrava	potrava	k1gFnSc1	potrava
mohla	moct	k5eAaImAgFnS	moct
patřit	patřit	k5eAaImF	patřit
k	k	k7c3	k
důležitým	důležitý	k2eAgInPc3d1	důležitý
záložním	záložní	k2eAgInPc3d1	záložní
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
,	,	kIx,	,
využívaným	využívaný	k2eAgMnSc7d1	využívaný
v	v	k7c6	v
méně	málo	k6eAd2	málo
příznivých	příznivý	k2eAgFnPc6d1	příznivá
částech	část	k1gFnPc6	část
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nemusela	muset	k5eNaImAgFnS	muset
být	být	k5eAaImF	být
nutričně	nutričně	k6eAd1	nutričně
nejvýhodnější	výhodný	k2eAgFnSc1d3	nejvýhodnější
ani	ani	k8xC	ani
snadno	snadno	k6eAd1	snadno
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
jí	jíst	k5eAaImIp3nS	jíst
dostatek	dostatek	k1gInSc4	dostatek
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
běžné	běžný	k2eAgFnPc1d1	běžná
složky	složka	k1gFnPc1	složka
potravy	potrava	k1gFnSc2	potrava
selhávaly	selhávat	k5eAaImAgFnP	selhávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
krátkých	krátký	k2eAgNnPc6d1	krátké
obdobích	období	k1gNnPc6	období
mohla	moct	k5eAaImAgFnS	moct
tvořit	tvořit	k5eAaImF	tvořit
až	až	k9	až
100	[number]	k4	100
<g/>
%	%	kIx~	%
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
na	na	k7c6	na
zubech	zub	k1gInPc6	zub
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
krátkodobému	krátkodobý	k2eAgNnSc3d1	krátkodobé
využití	využití	k1gNnSc3	využití
<g/>
)	)	kIx)	)
zanechala	zanechat	k5eAaPmAgFnS	zanechat
výraznější	výrazný	k2eAgFnPc4d2	výraznější
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
pravděpodobný	pravděpodobný	k2eAgInSc4d1	pravděpodobný
záložní	záložní	k2eAgInSc4d1	záložní
zdroj	zdroj	k1gInSc4	zdroj
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgFnPc1d1	považována
různé	různý	k2eAgFnPc1d1	různá
podzemní	podzemní	k2eAgFnPc1d1	podzemní
zásobní	zásobní	k2eAgFnPc1d1	zásobní
orgány	orgány	k1gFnPc1	orgány
travin	travina	k1gFnPc2	travina
nebo	nebo	k8xC	nebo
šáchorovitých	šáchorovitý	k2eAgInPc2d1	šáchorovitý
(	(	kIx(	(
<g/>
hlízy	hlíza	k1gFnPc1	hlíza
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnPc1	cibule
<g/>
,	,	kIx,	,
oddenky	oddenek	k1gInPc1	oddenek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
orgány	orgán	k1gInPc1	orgán
zadržují	zadržovat	k5eAaImIp3nP	zadržovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nadzemních	nadzemní	k2eAgFnPc2d1	nadzemní
částí	část	k1gFnPc2	část
rostliny	rostlina	k1gFnSc2	rostlina
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
i	i	k8xC	i
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
v	v	k7c6	v
savaně	savana	k1gFnSc6	savana
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všudypřítomné	všudypřítomný	k2eAgFnPc1d1	všudypřítomná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
živočichy	živočich	k1gMnPc4	živočich
obtížně	obtížně	k6eAd1	obtížně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gInPc4	on
vyhrabat	vyhrabat	k5eAaPmF	vyhrabat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
potravní	potravní	k2eAgFnSc1d1	potravní
konkurence	konkurence	k1gFnSc1	konkurence
ostatních	ostatní	k2eAgMnPc2d1	ostatní
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
vlhkém	vlhký	k2eAgInSc6d1	vlhký
lese	les	k1gInSc6	les
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
využití	využití	k1gNnSc1	využití
tudíž	tudíž	k8xC	tudíž
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
australopitéků	australopiték	k1gInPc2	australopiték
do	do	k7c2	do
nezalesněných	zalesněný	k2eNgFnPc2d1	nezalesněná
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
různých	různý	k2eAgFnPc2d1	různá
mokřin	mokřina	k1gFnPc2	mokřina
i	i	k9	i
v	v	k7c6	v
záplavových	záplavový	k2eAgFnPc6d1	záplavová
oblastech	oblast	k1gFnPc6	oblast
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pozorované	pozorovaný	k2eAgFnSc3d1	pozorovaná
vazbě	vazba	k1gFnSc3	vazba
na	na	k7c4	na
vlhčí	vlhký	k2eAgNnPc4d2	vlhčí
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
vyloženě	vyloženě	k6eAd1	vyloženě
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tuhé	tuhý	k2eAgFnPc1d1	tuhá
a	a	k8xC	a
houževnaté	houževnatý	k2eAgFnPc1d1	houževnatá
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
poměrně	poměrně	k6eAd1	poměrně
důkladné	důkladný	k2eAgNnSc4d1	důkladné
rozžvýkání	rozžvýkání	k1gNnSc4	rozžvýkání
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
zeminy	zemina	k1gFnSc2	zemina
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
podzemních	podzemní	k2eAgFnPc2d1	podzemní
částí	část	k1gFnPc2	část
rostlin	rostlina	k1gFnPc2	rostlina
mohly	moct	k5eAaImAgFnP	moct
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
sklovině	sklovina	k1gFnSc6	sklovina
způsobit	způsobit	k5eAaPmF	způsobit
škrábance	škrábanec	k1gInPc4	škrábanec
a	a	k8xC	a
rýhy	rýha	k1gFnPc4	rýha
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc4d1	podobný
stopám	stopa	k1gFnPc3	stopa
na	na	k7c6	na
zubech	zub	k1gInPc6	zub
listožravých	listožravý	k2eAgMnPc2d1	listožravý
primátů	primát	k1gMnPc2	primát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stopy	stopa	k1gFnPc1	stopa
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
přes	přes	k7c4	přes
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
podobnost	podobnost	k1gFnSc4	podobnost
naznačovat	naznačovat	k5eAaImF	naznačovat
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
různorodost	různorodost	k1gFnSc1	různorodost
australopitéků	australopitéek	k1gMnPc2	australopitéek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
využívání	využívání	k1gNnSc2	využívání
různých	různý	k2eAgInPc2d1	různý
záložních	záložní	k2eAgInPc2d1	záložní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c4	v
utváření	utváření	k1gNnSc4	utváření
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
čelistí	čelist	k1gFnPc2	čelist
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dány	dát	k5eAaPmNgFnP	dát
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
mechanickými	mechanický	k2eAgMnPc7d1	mechanický
nároky	nárok	k1gInPc4	nárok
podzemních	podzemní	k2eAgInPc2d1	podzemní
zásobních	zásobní	k2eAgInPc2d1	zásobní
orgánů	orgán	k1gInPc2	orgán
–	–	k?	–
gracilní	gracilní	k2eAgMnPc1d1	gracilní
australopitékové	australopitéek	k1gMnPc1	australopitéek
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
soustředit	soustředit	k5eAaPmF	soustředit
spíše	spíše	k9	spíše
na	na	k7c4	na
měkčí	měkký	k2eAgFnPc4d2	měkčí
<g/>
,	,	kIx,	,
tuhé	tuhý	k2eAgFnPc4d1	tuhá
cibulky	cibulka	k1gFnPc4	cibulka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
robustní	robustní	k2eAgInPc1d1	robustní
druhy	druh	k1gInPc1	druh
byly	být	k5eAaImAgInP	být
schopné	schopný	k2eAgInPc1d1	schopný
konzumovat	konzumovat	k5eAaBmF	konzumovat
i	i	k9	i
tvrdší	tvrdý	k2eAgFnPc4d2	tvrdší
hlízy	hlíza	k1gFnPc4	hlíza
a	a	k8xC	a
oddenky	oddenek	k1gInPc4	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
pak	pak	k6eAd1	pak
mohli	moct	k5eAaImAgMnP	moct
místo	místo	k7c2	místo
tuhých	tuhý	k2eAgFnPc2d1	tuhá
hlíz	hlíza	k1gFnPc2	hlíza
zvolit	zvolit	k5eAaPmF	zvolit
jako	jako	k9	jako
záložní	záložní	k2eAgInSc1d1	záložní
zdroj	zdroj	k1gInSc1	zdroj
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
morek	morek	k1gInSc4	morek
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
dostat	dostat	k5eAaPmF	dostat
pomocí	pomocí	k7c2	pomocí
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
téměř	téměř	k6eAd1	téměř
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
australopitékové	australopitéek	k1gMnPc1	australopitéek
byli	být	k5eAaImAgMnP	být
převážně	převážně	k6eAd1	převážně
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
ne	ne	k9	ne
výhradně	výhradně	k6eAd1	výhradně
<g/>
)	)	kIx)	)
býložraví	býložravý	k2eAgMnPc1d1	býložravý
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
spíše	spíše	k9	spíše
o	o	k7c6	o
masité	masitý	k2eAgFnSc6d1	masitá
stravě	strava	k1gFnSc6	strava
i	i	k9	i
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
aktivního	aktivní	k2eAgInSc2d1	aktivní
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývajícími	přibývající	k2eAgInPc7d1	přibývající
podklady	podklad	k1gInPc7	podklad
se	se	k3xPyFc4	se
však	však	k8xC	však
postupně	postupně	k6eAd1	postupně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
předpoklad	předpoklad	k1gInSc4	předpoklad
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
upřednostňování	upřednostňování	k1gNnSc1	upřednostňování
podporuje	podporovat	k5eAaImIp3nS	podporovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
vcelku	vcelku	k6eAd1	vcelku
mohutný	mohutný	k2eAgInSc4d1	mohutný
hrudník	hrudník	k1gInSc4	hrudník
a	a	k8xC	a
trup	trup	k1gInSc4	trup
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgInSc4d1	poskytující
dostatek	dostatek	k1gInSc4	dostatek
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
objemný	objemný	k2eAgInSc4d1	objemný
trávicí	trávicí	k2eAgInSc4d1	trávicí
trakt	trakt	k1gInSc4	trakt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ploché	plochý	k2eAgFnPc4d1	plochá
tupé	tupý	k2eAgFnPc4d1	tupá
stoličky	stolička	k1gFnPc4	stolička
<g/>
,	,	kIx,	,
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
k	k	k7c3	k
důkladnému	důkladný	k2eAgNnSc3d1	důkladné
zpracování	zpracování	k1gNnSc3	zpracování
syrového	syrový	k2eAgNnSc2d1	syrové
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
potravním	potravní	k2eAgFnPc3d1	potravní
zvyklostem	zvyklost	k1gFnPc3	zvyklost
moderních	moderní	k2eAgMnPc2d1	moderní
lidoopů	lidoop	k1gMnPc2	lidoop
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
předpokádat	předpokádat	k5eAaImF	předpokádat
příležitostnou	příležitostný	k2eAgFnSc4d1	příležitostná
konzumaci	konzumace	k1gFnSc4	konzumace
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
termitů	termit	k1gMnPc2	termit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drobných	drobný	k2eAgMnPc2d1	drobný
obratlovců	obratlovec	k1gMnPc2	obratlovec
nebo	nebo	k8xC	nebo
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tendence	tendence	k1gFnPc4	tendence
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
živočišné	živočišný	k2eAgFnSc2d1	živočišná
potravy	potrava	k1gFnSc2	potrava
mohou	moct	k5eAaImIp3nP	moct
ukazovat	ukazovat	k5eAaImF	ukazovat
i	i	k9	i
nálezy	nález	k1gInPc1	nález
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
stopy	stopa	k1gFnSc2	stopa
po	po	k7c6	po
úderech	úder	k1gInPc6	úder
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Dikika	Dikikum	k1gNnSc2	Dikikum
i	i	k9	i
v	v	k7c6	v
Bouri	Bour	k1gFnSc6	Bour
a	a	k8xC	a
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
snahu	snaha	k1gFnSc4	snaha
o	o	k7c6	o
rozbití	rozbití	k1gNnSc6	rozbití
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
získání	získání	k1gNnSc2	získání
morku	morek	k1gInSc2	morek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
původce	původce	k1gMnSc4	původce
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
přisuzovány	přisuzován	k2eAgInPc1d1	přisuzován
druhům	druh	k1gMnPc3	druh
A.	A.	kA	A.
afarensis	afarensis	k1gInSc4	afarensis
a	a	k8xC	a
A.	A.	kA	A.
garhi	garhi	k6eAd1	garhi
<g/>
.	.	kIx.	.
</s>
<s>
Občasnou	občasný	k2eAgFnSc4d1	občasná
konzumaci	konzumace	k1gFnSc4	konzumace
masité	masitý	k2eAgFnSc2d1	masitá
stravy	strava	k1gFnSc2	strava
lze	lze	k6eAd1	lze
patrně	patrně	k6eAd1	patrně
odvozovat	odvozovat	k5eAaImF	odvozovat
i	i	k9	i
ze	z	k7c2	z
stop	stopa	k1gFnPc2	stopa
nákazy	nákaza	k1gFnSc2	nákaza
brucelózou	brucelóza	k1gFnSc7	brucelóza
na	na	k7c6	na
obratlích	obratel	k1gInPc6	obratel
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
ze	z	k7c2	z
Sterkfontein	Sterkfonteina	k1gFnPc2	Sterkfonteina
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgInPc1d1	početný
morfologické	morfologický	k2eAgInPc1d1	morfologický
znaky	znak	k1gInPc1	znak
kostry	kostra	k1gFnSc2	kostra
<g/>
,	,	kIx,	,
především	především	k9	především
téměř	téměř	k6eAd1	téměř
lidské	lidský	k2eAgNnSc4d1	lidské
utváření	utváření	k1gNnSc4	utváření
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
kyčelního	kyčelní	k2eAgInSc2d1	kyčelní
<g/>
,	,	kIx,	,
kolenního	kolenní	k2eAgInSc2d1	kolenní
i	i	k8xC	i
hlezenního	hlezenní	k2eAgInSc2d1	hlezenní
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
obdobné	obdobný	k2eAgNnSc4d1	obdobné
rozložení	rozložení	k1gNnSc4	rozložení
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
vazů	vaz	k1gInPc2	vaz
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hýždí	hýždě	k1gFnPc2	hýždě
a	a	k8xC	a
stehen	stehno	k1gNnPc2	stehno
nebo	nebo	k8xC	nebo
palec	palec	k1gInSc4	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
hybností	hybnost	k1gFnSc7	hybnost
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
běžně	běžně	k6eAd1	běžně
užívané	užívaný	k2eAgFnSc6d1	užívaná
bipedii	bipedie	k1gFnSc6	bipedie
-	-	kIx~	-
vzpřímené	vzpřímený	k2eAgFnSc3d1	vzpřímená
dvojnohé	dvojnohý	k2eAgFnSc3d1	dvojnohá
chůzi	chůze	k1gFnSc3	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
pohybu	pohyb	k1gInSc2	pohyb
australopitéků	australopiték	k1gInPc2	australopiték
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
odvozována	odvozovat	k5eAaImNgFnS	odvozovat
z	z	k7c2	z
početných	početný	k2eAgInPc2d1	početný
nálezů	nález	k1gInPc2	nález
druhu	druh	k1gInSc2	druh
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc1	afarensis
a	a	k8xC	a
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
africanus	africanus	k1gMnSc1	africanus
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
ovšem	ovšem	k9	ovšem
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
způsob	způsob	k1gInSc1	způsob
a	a	k8xC	a
efektivita	efektivita	k1gFnSc1	efektivita
chůze	chůze	k1gFnSc1	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
badatelů	badatel	k1gMnPc2	badatel
nepochybuje	pochybovat	k5eNaImIp3nS	pochybovat
o	o	k7c6	o
plně	plně	k6eAd1	plně
bipedním	bipední	k2eAgInSc6d1	bipední
pohybu	pohyb	k1gInSc6	pohyb
lidského	lidský	k2eAgInSc2d1	lidský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
s	s	k7c7	s
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
specifickými	specifický	k2eAgInPc7d1	specifický
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
část	část	k1gFnSc1	část
autorů	autor	k1gMnPc2	autor
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
chůzi	chůze	k1gFnSc4	chůze
s	s	k7c7	s
pokrčenými	pokrčený	k2eAgNnPc7d1	pokrčené
koleny	koleno	k1gNnPc7	koleno
a	a	k8xC	a
kyčlemi	kyčel	k1gFnPc7	kyčel
<g/>
.	.	kIx.	.
</s>
<s>
Odlišný	odlišný	k2eAgInSc1d1	odlišný
poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
končetin	končetina	k1gFnPc2	končetina
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
moderními	moderní	k2eAgMnPc7d1	moderní
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgFnSc2d1	krátká
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
ruce	ruka	k1gFnPc4	ruka
<g/>
)	)	kIx)	)
i	i	k9	i
odlišnosti	odlišnost	k1gFnPc4	odlišnost
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
stabilizace	stabilizace	k1gFnSc2	stabilizace
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
trupu	trup	k1gInSc2	trup
mohly	moct	k5eAaImAgFnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohyb	pohyb	k1gInSc1	pohyb
možná	možná	k9	možná
připomínal	připomínat	k5eAaImAgInS	připomínat
nedokonalou	dokonalý	k2eNgFnSc4d1	nedokonalá
bipedii	bipedie	k1gFnSc4	bipedie
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Chůze	chůze	k1gFnSc1	chůze
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
energeticky	energeticky	k6eAd1	energeticky
náročnější	náročný	k2eAgFnSc1d2	náročnější
<g/>
,	,	kIx,	,
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
pro	pro	k7c4	pro
přesuny	přesun	k1gInPc4	přesun
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vnímat	vnímat	k5eAaImF	vnímat
tento	tento	k3xDgInSc4	tento
pohyb	pohyb	k1gInSc4	pohyb
jako	jako	k8xS	jako
nedokonalý	dokonalý	k2eNgInSc1d1	nedokonalý
přechodný	přechodný	k2eAgInSc1d1	přechodný
stupeň	stupeň	k1gInSc1	stupeň
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
lépe	dobře	k6eAd2	dobře
vyvinutému	vyvinutý	k2eAgMnSc3d1	vyvinutý
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéek	k1gMnPc1	Australopitéek
byli	být	k5eAaImAgMnP	být
dobře	dobře	k6eAd1	dobře
přizpůsobení	přizpůsobený	k2eAgMnPc1d1	přizpůsobený
svému	svůj	k3xOyFgNnSc3	svůj
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
obývali	obývat	k5eAaImAgMnP	obývat
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
části	část	k1gFnSc2	část
odborníků	odborník	k1gMnPc2	odborník
však	však	k9	však
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
rod	rod	k1gInSc4	rod
Homo	Homo	k6eAd1	Homo
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
rychlému	rychlý	k2eAgInSc3d1	rychlý
pohybu	pohyb	k1gInSc3	pohyb
a	a	k8xC	a
běhu	běh	k1gInSc6	běh
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
savaně	savana	k1gFnSc6	savana
<g/>
,	,	kIx,	,
australopitékové	australopitékové	k2eAgFnSc4d1	australopitékové
pomalejší	pomalý	k2eAgFnSc4d2	pomalejší
pozemní	pozemní	k2eAgFnSc4d1	pozemní
chůzi	chůze	k1gFnSc4	chůze
alespoň	alespoň	k9	alespoň
zčásti	zčásti	k6eAd1	zčásti
kombinovali	kombinovat	k5eAaImAgMnP	kombinovat
se	s	k7c7	s
šplháním	šplhání	k1gNnSc7	šplhání
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patrně	patrně	k6eAd1	patrně
hledali	hledat	k5eAaImAgMnP	hledat
potravu	potrava	k1gFnSc4	potrava
i	i	k8xC	i
úkryt	úkryt	k1gInSc4	úkryt
před	před	k7c7	před
dravci	dravec	k1gMnPc7	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
některé	některý	k3yIgInPc4	některý
přetrvávající	přetrvávající	k2eAgInPc4d1	přetrvávající
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
zděděné	zděděný	k2eAgFnPc4d1	zděděná
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
-	-	kIx~	-
robustní	robustní	k2eAgFnPc4d1	robustní
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
osvalená	osvalený	k2eAgFnSc1d1	osvalená
pažní	pažní	k2eAgFnSc1d1	pažní
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
svalnaté	svalnatý	k2eAgNnSc4d1	svalnaté
předloktí	předloktí	k1gNnSc4	předloktí
<g/>
,	,	kIx,	,
pohyblivá	pohyblivý	k2eAgNnPc1d1	pohyblivé
ramena	rameno	k1gNnPc1	rameno
i	i	k8xC	i
zápěstí	zápěstí	k1gNnPc1	zápěstí
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
prohnuté	prohnutý	k2eAgInPc1d1	prohnutý
prsty	prst	k1gInPc1	prst
schopné	schopný	k2eAgInPc1d1	schopný
pevného	pevný	k2eAgInSc2d1	pevný
úchopu	úchop	k1gInSc2	úchop
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
odtažitelný	odtažitelný	k2eAgInSc1d1	odtažitelný
palec	palec	k1gInSc1	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nemohou	moct	k5eNaImIp3nP	moct
shodnout	shodnout	k5eAaBmF	shodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
pozorované	pozorovaný	k2eAgInPc4d1	pozorovaný
znaky	znak	k1gInPc4	znak
skutečně	skutečně	k6eAd1	skutečně
odrazem	odraz	k1gInSc7	odraz
částečně	částečně	k6eAd1	částečně
stromového	stromový	k2eAgInSc2d1	stromový
života	život	k1gInSc2	život
a	a	k8xC	a
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
nepoužívané	používaný	k2eNgInPc4d1	nepoužívaný
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ještě	ještě	k6eAd1	ještě
nestihly	stihnout	k5eNaPmAgInP	stihnout
zcela	zcela	k6eAd1	zcela
vymizet	vymizet	k5eAaPmF	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
četnost	četnost	k1gFnSc4	četnost
užívání	užívání	k1gNnSc2	užívání
stromového	stromový	k2eAgNnSc2d1	stromové
patra	patro	k1gNnSc2	patro
u	u	k7c2	u
australopitéků	australopiték	k1gInPc2	australopiték
tak	tak	k9	tak
dosud	dosud	k6eAd1	dosud
neexistuje	existovat	k5eNaImIp3nS	existovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
samci	samec	k1gMnPc7	samec
a	a	k8xC	a
samicemi	samice	k1gFnPc7	samice
i	i	k8xC	i
různými	různý	k2eAgFnPc7d1	různá
věkovými	věkový	k2eAgFnPc7d1	věková
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nešlo	jít	k5eNaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
šplhání	šplhání	k1gNnSc4	šplhání
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
lezení	lezení	k1gNnSc4	lezení
po	po	k7c6	po
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
útesech	útes	k1gInPc6	útes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
již	již	k6eAd1	již
šplhání	šplhání	k1gNnSc6	šplhání
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
oproti	oproti	k7c3	oproti
starším	starý	k2eAgMnPc3d2	starší
hominidům	hominid	k1gMnPc3	hominid
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
výhodné	výhodný	k2eAgNnSc1d1	výhodné
pro	pro	k7c4	pro
život	život	k1gInSc4	život
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
byly	být	k5eAaImAgFnP	být
obětovány	obětován	k2eAgFnPc1d1	obětována
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
bipední	bipední	k2eAgFnSc2d1	bipední
chůze	chůze	k1gFnSc2	chůze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
protistojný	protistojný	k2eAgInSc1d1	protistojný
palec	palec	k1gInSc1	palec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Australopiték	Australopiték	k6eAd1	Australopiték
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
musel	muset	k5eAaImAgInS	muset
pohybovat	pohybovat	k5eAaImF	pohybovat
svébytným	svébytný	k2eAgInSc7d1	svébytný
způsobem	způsob	k1gInSc7	způsob
-	-	kIx~	-
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
lidoopi	lidoop	k1gMnPc1	lidoop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
nálezy	nález	k1gInPc1	nález
navíc	navíc	k6eAd1	navíc
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
adaptace	adaptace	k1gFnSc1	adaptace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
pro	pro	k7c4	pro
bipedii	bipedie	k1gFnSc4	bipedie
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
mírně	mírně	k6eAd1	mírně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc6	pleistocén
existovalo	existovat	k5eAaImAgNnS	existovat
souběžně	souběžně	k6eAd1	souběžně
více	hodně	k6eAd2	hodně
druhů	druh	k1gMnPc2	druh
dvojnohé	dvojnohý	k2eAgFnSc2d1	dvojnohá
chůze	chůze	k1gFnSc2	chůze
<g/>
,	,	kIx,	,
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
kombinované	kombinovaný	k2eAgInPc1d1	kombinovaný
se	s	k7c7	s
šplháním	šplhání	k1gNnSc7	šplhání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
linií	linie	k1gFnPc2	linie
pokračovala	pokračovat	k5eAaImAgNnP	pokračovat
ke	k	k7c3	k
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Adaptace	adaptace	k1gFnSc1	adaptace
k	k	k7c3	k
bipedii	bipedie	k1gFnSc3	bipedie
však	však	k9	však
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
i	i	k9	i
možnosti	možnost	k1gFnPc1	možnost
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Platypeloidní	Platypeloidní	k2eAgInSc1d1	Platypeloidní
tvar	tvar	k1gInSc1	tvar
pánve	pánev	k1gFnSc2	pánev
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
moderních	moderní	k2eAgMnPc2d1	moderní
lidí	člověk	k1gMnPc2	člověk
velmi	velmi	k6eAd1	velmi
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
pro	pro	k7c4	pro
porod	porod	k1gInSc4	porod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
bývá	bývat	k5eAaImIp3nS	bývat
značně	značně	k6eAd1	značně
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
až	až	k8xS	až
nemožný	možný	k2eNgInSc4d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samice	samice	k1gFnPc4	samice
australopitéků	australopitéek	k1gMnPc2	australopitéek
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
relativně	relativně	k6eAd1	relativně
bezproblémový	bezproblémový	k2eAgMnSc1d1	bezproblémový
díky	díky	k7c3	díky
malému	malý	k2eAgInSc3d1	malý
objemu	objem	k1gInSc3	objem
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
malé	malý	k2eAgFnSc3d1	malá
hlavičce	hlavička	k1gFnSc3	hlavička
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
již	již	k9	již
plod	plod	k1gInSc4	plod
zřejmě	zřejmě	k6eAd1	zřejmě
nemohl	moct	k5eNaImAgMnS	moct
procházet	procházet	k5eAaImF	procházet
porodními	porodní	k2eAgFnPc7d1	porodní
cestami	cesta	k1gFnPc7	cesta
přímo	přímo	k6eAd1	přímo
jako	jako	k8xC	jako
u	u	k7c2	u
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
donucen	donutit	k5eAaPmNgInS	donutit
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
rotaci	rotace	k1gFnSc3	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nemohl	moct	k5eNaImAgInS	moct
v	v	k7c6	v
předozadně	předozadně	k6eAd1	předozadně
zploštělé	zploštělý	k2eAgFnSc6d1	zploštělá
pánvi	pánev	k1gFnSc6	pánev
otáčet	otáčet	k5eAaImF	otáčet
jako	jako	k9	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
hlavička	hlavička	k1gFnSc1	hlavička
tak	tak	k6eAd1	tak
nejspíše	nejspíše	k9	nejspíše
procházela	procházet	k5eAaImAgFnS	procházet
pánví	pánev	k1gFnSc7	pánev
příčně	příčně	k6eAd1	příčně
<g/>
,	,	kIx,	,
čelně	čelně	k6eAd1	čelně
orientována	orientován	k2eAgFnSc1d1	orientována
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
kyčelních	kyčelní	k2eAgMnPc2d1	kyčelní
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
autoři	autor	k1gMnPc1	autor
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komplexnější	komplexní	k2eAgInSc1d2	komplexnější
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
lidský	lidský	k2eAgInSc1d1	lidský
<g/>
"	"	kIx"	"
porod	porod	k1gInSc1	porod
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
rotací	rotace	k1gFnSc7	rotace
plodu	plod	k1gInSc2	plod
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejsložitějších	složitý	k2eAgMnPc2d3	nejsložitější
ve	v	k7c4	v
zvířecí	zvířecí	k2eAgFnSc4d1	zvířecí
říši	říše	k1gFnSc4	říše
-	-	kIx~	-
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
až	až	k9	až
během	během	k7c2	během
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
zvyšováním	zvyšování	k1gNnSc7	zvyšování
lebeční	lebeční	k2eAgFnSc2d1	lebeční
kapacity	kapacita	k1gFnSc2	kapacita
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
odporuje	odporovat	k5eAaImIp3nS	odporovat
nález	nález	k1gInSc1	nález
pánve	pánev	k1gFnSc2	pánev
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
sediba	sedib	k1gMnSc2	sedib
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
předozadním	předozadní	k2eAgInSc6d1	předozadní
směru	směr	k1gInSc6	směr
širší	široký	k2eAgInSc4d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
této	tento	k3xDgFnSc2	tento
pánve	pánev	k1gFnSc2	pánev
přitom	přitom	k6eAd1	přitom
nemůže	moct	k5eNaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
se	s	k7c7	s
zvětšováním	zvětšování	k1gNnSc7	zvětšování
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mláďata	mládě	k1gNnPc1	mládě
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
sediba	sediba	k1gMnSc1	sediba
se	se	k3xPyFc4	se
velikostí	velikost	k1gFnSc7	velikost
mozkovny	mozkovna	k1gFnSc2	mozkovna
výrazně	výrazně	k6eAd1	výrazně
nelišila	lišit	k5eNaImAgFnS	lišit
od	od	k7c2	od
moderních	moderní	k2eAgMnPc2d1	moderní
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
pánve	pánev	k1gFnSc2	pánev
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
spíše	spíše	k9	spíše
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
bipedie	bipedie	k1gFnSc2	bipedie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
změny	změna	k1gFnPc1	změna
umožnily	umožnit	k5eAaPmAgFnP	umožnit
efektivnější	efektivní	k2eAgFnSc4d2	efektivnější
práci	práce	k1gFnSc4	práce
hýžďových	hýžďový	k2eAgInPc2d1	hýžďový
a	a	k8xC	a
stehenních	stehenní	k2eAgInPc2d1	stehenní
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
energeticky	energeticky	k6eAd1	energeticky
méně	málo	k6eAd2	málo
náročnou	náročný	k2eAgFnSc4d1	náročná
chůzi	chůze	k1gFnSc4	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
australopitéci	australopitéct	k5eAaPmF	australopitéct
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
vykročili	vykročit	k5eAaPmAgMnP	vykročit
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
modernímu	moderní	k2eAgMnSc3d1	moderní
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
i	i	k9	i
společenskou	společenský	k2eAgFnSc7d1	společenská
organizací	organizace	k1gFnSc7	organizace
zůstali	zůstat	k5eAaPmAgMnP	zůstat
spíše	spíše	k9	spíše
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Nápadný	nápadný	k2eAgInSc1d1	nápadný
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
netvořili	tvořit	k5eNaImAgMnP	tvořit
monogamní	monogamní	k2eAgInPc4d1	monogamní
páry	pár	k1gInPc4	pár
a	a	k8xC	a
žili	žít	k5eAaImAgMnP	žít
patrně	patrně	k6eAd1	patrně
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
skupinách	skupina	k1gFnPc6	skupina
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
moderní	moderní	k2eAgMnPc1d1	moderní
šimpanzi	šimpanz	k1gMnPc1	šimpanz
či	či	k8xC	či
gorily	gorila	k1gFnPc1	gorila
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tělesných	tělesný	k2eAgInPc2d1	tělesný
rozměrů	rozměr	k1gInPc2	rozměr
však	však	k9	však
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c7	mezi
samci	samec	k1gInPc7	samec
a	a	k8xC	a
samicemi	samice	k1gFnPc7	samice
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
špičáků	špičák	k1gInPc2	špičák
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
zuby	zub	k1gInPc1	zub
již	již	k9	již
oproti	oproti	k7c3	oproti
lidoopům	lidoop	k1gMnPc3	lidoop
neměly	mít	k5eNaImAgFnP	mít
tak	tak	k9	tak
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
nebo	nebo	k8xC	nebo
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nijak	nijak	k6eAd1	nijak
závratný	závratný	k2eAgMnSc1d1	závratný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
množství	množství	k1gNnSc6	množství
zvířat	zvíře	k1gNnPc2	zvíře
mohli	moct	k5eAaImAgMnP	moct
australopitékové	australopitékové	k2eAgMnPc1d1	australopitékové
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
zlomek	zlomek	k1gInSc4	zlomek
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
vytváření	vytváření	k1gNnSc4	vytváření
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
základen	základna	k1gFnPc2	základna
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
obydlených	obydlený	k2eAgNnPc2d1	obydlené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
ostatků	ostatek	k1gInPc2	ostatek
homininů	hominin	k1gInPc2	hominin
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
jihoafrických	jihoafrický	k2eAgFnPc6d1	Jihoafrická
jeskyních	jeskyně	k1gFnPc6	jeskyně
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
aktivitou	aktivita	k1gFnSc7	aktivita
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
osídlením	osídlení	k1gNnSc7	osídlení
australopitéky	australopitéka	k1gFnSc2	australopitéka
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
hltanu	hltan	k1gInSc2	hltan
a	a	k8xC	a
hrtanu	hrtan	k1gInSc2	hrtan
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
podle	podle	k7c2	podle
utváření	utváření	k1gNnSc2	utváření
spodiny	spodina	k1gFnSc2	spodina
lebeční	lebeční	k2eAgFnSc1d1	lebeční
obdobná	obdobný	k2eAgFnSc1d1	obdobná
jako	jako	k8xC	jako
u	u	k7c2	u
moderních	moderní	k2eAgMnPc2d1	moderní
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc3	možnost
rozvoje	rozvoj	k1gInSc2	rozvoj
artikulované	artikulovaný	k2eAgFnSc2d1	artikulovaná
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
tak	tak	k9	tak
mohla	moct	k5eAaImAgFnS	moct
probíhat	probíhat	k5eAaImF	probíhat
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
goril	gorila	k1gFnPc2	gorila
nebo	nebo	k8xC	nebo
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
A.	A.	kA	A.
Dart	Dart	k1gMnSc1	Dart
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
australopitékové	australopitéek	k1gMnPc1	australopitéek
(	(	kIx(	(
<g/>
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
)	)	kIx)	)
využívali	využívat	k5eAaPmAgMnP	využívat
tzv.	tzv.	kA	tzv.
osteodontokeratickou	osteodontokeratický	k2eAgFnSc4d1	osteodontokeratický
kulturu	kultura	k1gFnSc4	kultura
-	-	kIx~	-
nástroje	nástroj	k1gInPc4	nástroj
ze	z	k7c2	z
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
rohů	roh	k1gInPc2	roh
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
studie	studie	k1gFnPc1	studie
však	však	k9	však
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
kosti	kost	k1gFnPc1	kost
byly	být	k5eAaImAgFnP	být
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
a	a	k8xC	a
nashromážděny	nashromáždit	k5eAaPmNgFnP	nashromáždit
aktivitou	aktivita	k1gFnSc7	aktivita
predátorů	predátor	k1gMnPc2	predátor
stojících	stojící	k2eAgMnPc2d1	stojící
i	i	k8xC	i
za	za	k7c7	za
smrtí	smrt	k1gFnSc7	smrt
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalostí	znalost	k1gFnPc2	znalost
nástrojové	nástrojový	k2eAgFnSc2d1	nástrojová
činnosti	činnost	k1gFnSc2	činnost
moderních	moderní	k2eAgMnPc2d1	moderní
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
vydry	vydra	k1gFnPc1	vydra
<g/>
,	,	kIx,	,
makakové	makakové	k?	makakové
<g/>
)	)	kIx)	)
všeobecně	všeobecně	k6eAd1	všeobecně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
australopitékové	australopitéek	k1gMnPc1	australopitéek
mohli	moct	k5eAaImAgMnP	moct
využívat	využívat	k5eAaImF	využívat
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
nástroje	nástroj	k1gInPc4	nástroj
-	-	kIx~	-
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
klacíky	klacík	k1gInPc4	klacík
nebo	nebo	k8xC	nebo
neupravené	upravený	k2eNgInPc4d1	neupravený
valouny	valoun	k1gInPc4	valoun
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
lze	lze	k6eAd1	lze
však	však	k9	však
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
případné	případný	k2eAgInPc1d1	případný
nástroje	nástroj	k1gInPc1	nástroj
prakticky	prakticky	k6eAd1	prakticky
nelze	lze	k6eNd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácené	zkrácený	k2eAgInPc1d1	zkrácený
prsty	prst	k1gInPc1	prst
i	i	k8xC	i
silný	silný	k2eAgInSc1d1	silný
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
palec	palec	k1gInSc1	palec
již	již	k6eAd1	již
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
manipulačních	manipulační	k2eAgFnPc2d1	manipulační
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgFnPc3d1	přetrvávající
adaptacím	adaptace	k1gFnPc3	adaptace
pro	pro	k7c4	pro
šplhání	šplhání	k1gNnSc4	šplhání
ještě	ještě	k9	ještě
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
tak	tak	k9	tak
jemné	jemný	k2eAgFnSc2d1	jemná
manipulace	manipulace	k1gFnSc2	manipulace
jako	jako	k8xS	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
australopitékové	australopitéek	k1gMnPc1	australopitéek
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
tzv.	tzv.	kA	tzv.
precizního	precizní	k2eAgInSc2d1	precizní
gripu	grip	k1gInSc2	grip
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pevně	pevně	k6eAd1	pevně
spojit	spojit	k5eAaPmF	spojit
ukazovák	ukazovák	k1gInSc4	ukazovák
a	a	k8xC	a
palec	palec	k1gInSc4	palec
bříšky	bříšek	k1gInPc1	bříšek
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
zručnost	zručnost	k1gFnSc4	zručnost
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
australopitéky	australopitéky	k6eAd1	australopitéky
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
několik	několik	k4yIc1	několik
náznaků	náznak	k1gInPc2	náznak
užívání	užívání	k1gNnPc2	užívání
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
:	:	kIx,	:
Dikika	Dikika	k1gFnSc1	Dikika
-	-	kIx~	-
zářezy	zářez	k1gInPc1	zářez
na	na	k7c6	na
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kostech	kost	k1gFnPc6	kost
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Bouri	Bouri	k1gNnPc1	Bouri
-	-	kIx~	-
zářezy	zářez	k1gInPc1	zářez
na	na	k7c6	na
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kostech	kost	k1gFnPc6	kost
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
A.	A.	kA	A.
garhi	garhi	k6eAd1	garhi
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Swartkrans	Swartkrans	k1gInSc4	Swartkrans
-	-	kIx~	-
užíváním	užívání	k1gNnSc7	užívání
opotřebené	opotřebený	k2eAgFnSc2d1	opotřebená
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
A.	A.	kA	A.
robustus	robustus	k1gInSc1	robustus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Drimolen	Drimolen	k2eAgMnSc1d1	Drimolen
-	-	kIx~	-
užíváním	užívání	k1gNnSc7	užívání
opotřebené	opotřebený	k2eAgFnSc2d1	opotřebená
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
A.	A.	kA	A.
robustus	robustus	k1gInSc1	robustus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgInPc4d1	možný
tyto	tento	k3xDgInPc4	tento
nálezy	nález	k1gInPc4	nález
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přiřadit	přiřadit	k5eAaPmF	přiřadit
konkrétnímu	konkrétní	k2eAgInSc3d1	konkrétní
druhu	druh	k1gInSc3	druh
homininů	hominin	k1gInPc2	hominin
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
původcem	původce	k1gMnSc7	původce
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
soudobí	soudobý	k2eAgMnPc1d1	soudobý
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gNnSc1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Zářezy	zářez	k1gInPc1	zářez
na	na	k7c6	na
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kostech	kost	k1gFnPc6	kost
představují	představovat	k5eAaImIp3nP	představovat
první	první	k4xOgInPc4	první
doklady	doklad	k1gInPc4	doklad
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
živočišné	živočišný	k2eAgFnSc3d1	živočišná
potravě	potrava	k1gFnSc3	potrava
a	a	k8xC	a
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
možný	možný	k2eAgInSc4d1	možný
zájem	zájem	k1gInSc4	zájem
australopitéků	australopitéek	k1gMnPc2	australopitéek
o	o	k7c4	o
masitou	masitý	k2eAgFnSc4d1	masitá
potravu	potrava	k1gFnSc4	potrava
(	(	kIx(	(
<g/>
morek	morek	k1gInSc4	morek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
získávání	získávání	k1gNnSc4	získávání
kameny	kámen	k1gInPc4	kámen
nebo	nebo	k8xC	nebo
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
neuvažuje	uvažovat	k5eNaImIp3nS	uvažovat
o	o	k7c6	o
aktivním	aktivní	k2eAgInSc6d1	aktivní
lovu	lov	k1gInSc6	lov
velkých	velký	k2eAgNnPc2d1	velké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
využívání	využívání	k1gNnSc4	využívání
kořisti	kořist	k1gFnSc2	kořist
jiných	jiný	k2eAgMnPc2d1	jiný
masožravců	masožravec	k1gMnPc2	masožravec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c6	o
oškrábání	oškrábání	k1gNnSc6	oškrábání
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
masa	maso	k1gNnSc2	maso
nebo	nebo	k8xC	nebo
rozbití	rozbití	k1gNnSc2	rozbití
kosti	kost	k1gFnSc2	kost
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
získání	získání	k1gNnSc1	získání
morku	morek	k1gInSc2	morek
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
však	však	k9	však
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
použit	použit	k2eAgMnSc1d1	použit
předem	předem	k6eAd1	předem
připravený	připravený	k2eAgMnSc1d1	připravený
<g/>
,	,	kIx,	,
záměrně	záměrně	k6eAd1	záměrně
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
nástroj	nástroj	k1gInSc4	nástroj
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
vhodný	vhodný	k2eAgInSc1d1	vhodný
neopracovaný	opracovaný	k2eNgInSc1d1	neopracovaný
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
nejvýše	vysoce	k6eAd3	vysoce
přirozeně	přirozeně	k6eAd1	přirozeně
zaostřenou	zaostřený	k2eAgFnSc4d1	zaostřená
hranu	hrana	k1gFnSc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéková	k1gFnPc1	Australopitéková
tvoří	tvořit	k5eAaImIp3nP	tvořit
značně	značně	k6eAd1	značně
různorodou	různorodý	k2eAgFnSc4d1	různorodá
<g/>
,	,	kIx,	,
geograficky	geograficky	k6eAd1	geograficky
i	i	k9	i
časově	časově	k6eAd1	časově
širokou	široký	k2eAgFnSc4d1	široká
skupinu	skupina	k1gFnSc4	skupina
hominidů	hominid	k1gMnPc2	hominid
<g/>
,	,	kIx,	,
vazby	vazba	k1gFnPc4	vazba
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gNnPc7	on
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
zatím	zatím	k6eAd1	zatím
mnohdy	mnohdy	k6eAd1	mnohdy
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mínění	mínění	k1gNnSc2	mínění
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
badatelů	badatel	k1gMnPc2	badatel
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
spíše	spíše	k9	spíše
parafyletický	parafyletický	k2eAgMnSc1d1	parafyletický
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
australopitéků	australopiték	k1gInPc2	australopiték
do	do	k7c2	do
menších	malý	k2eAgFnPc2d2	menší
skupin	skupina	k1gFnPc2	skupina
-	-	kIx~	-
samostatných	samostatný	k2eAgInPc2d1	samostatný
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
,	,	kIx,	,
Praeanthropus	Praeanthropus	k1gMnSc1	Praeanthropus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
však	však	k9	však
žádné	žádný	k3yNgNnSc1	žádný
z	z	k7c2	z
nastíněných	nastíněný	k2eAgNnPc2d1	nastíněné
řešení	řešení	k1gNnPc2	řešení
nebylo	být	k5eNaImAgNnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
neúplně	úplně	k6eNd1	úplně
dochovaným	dochovaný	k2eAgInPc3d1	dochovaný
nálezům	nález	k1gInPc3	nález
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
homininů	hominin	k1gInPc2	hominin
nebyl	být	k5eNaImAgInS	být
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
provázelo	provázet	k5eAaImAgNnS	provázet
množství	množství	k1gNnSc1	množství
homoplasií	homoplasie	k1gFnPc2	homoplasie
a	a	k8xC	a
že	že	k8xS	že
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzácných	vzácný	k2eAgInPc2d1	vzácný
<g/>
,	,	kIx,	,
neúplných	úplný	k2eNgInPc2d1	neúplný
nálezů	nález	k1gInPc2	nález
je	být	k5eAaImIp3nS	být
však	však	k9	však
lze	lze	k6eAd1	lze
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Australopitékové	Australopitéková	k1gFnPc1	Australopitéková
(	(	kIx(	(
<g/>
A.	A.	kA	A.
anamensis	anamensis	k1gFnSc2	anamensis
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
ardipitéky	ardipitéka	k1gFnPc4	ardipitéka
a	a	k8xC	a
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
3	[number]	k4	3
-	-	kIx~	-
3,5	[number]	k4	3,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
ovšem	ovšem	k9	ovšem
prudce	prudko	k6eAd1	prudko
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
množství	množství	k1gNnSc1	množství
současně	současně	k6eAd1	současně
žijících	žijící	k2eAgInPc2d1	žijící
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
radiace	radiace	k1gFnSc1	radiace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
poznání	poznání	k1gNnSc4	poznání
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
A.	A.	kA	A.
afarensis	afarensis	k1gFnSc7	afarensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
<g/>
,	,	kIx,	,
A.	A.	kA	A.
deyiremeda	deyiremeda	k1gMnSc1	deyiremeda
i	i	k8xC	i
Kenyanthropus	Kenyanthropus	k1gMnSc1	Kenyanthropus
platyops	platyopsa	k1gFnPc2	platyopsa
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
A.	A.	kA	A.
africanus	africanus	k1gInSc1	africanus
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
ještě	ještě	k9	ještě
robustní	robustní	k2eAgFnPc1d1	robustní
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
první	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gNnSc1	Homo
a	a	k8xC	a
nejstarší	starý	k2eAgInPc1d3	nejstarší
kamenné	kamenný	k2eAgInPc1d1	kamenný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
homininů	hominin	k1gInPc2	hominin
zvolila	zvolit	k5eAaPmAgFnS	zvolit
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
strategii	strategie	k1gFnSc4	strategie
velkých	velký	k2eAgInPc2d1	velký
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
silných	silný	k2eAgFnPc2d1	silná
čelistí	čelist	k1gFnPc2	čelist
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Paranthropus	Paranthropus	k1gInSc1	Paranthropus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k1gMnPc1	jiný
upřednostnili	upřednostnit	k5eAaPmAgMnP	upřednostnit
větší	veliký	k2eAgInSc4d2	veliký
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Homo	Homo	k1gNnSc1	Homo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
málo	málo	k6eAd1	málo
poznané	poznaný	k2eAgInPc1d1	poznaný
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc1	vztah
východoafrických	východoafrický	k2eAgInPc2d1	východoafrický
a	a	k8xC	a
jihoafrických	jihoafrický	k2eAgInPc2d1	jihoafrický
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
jsou	být	k5eAaImIp3nP	být
oddělené	oddělený	k2eAgFnSc2d1	oddělená
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
neprozkoumané	prozkoumaný	k2eNgNnSc1d1	neprozkoumané
a	a	k8xC	a
bez	bez	k7c2	bez
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
datování	datování	k1gNnSc1	datování
jihoafrických	jihoafrický	k2eAgFnPc2d1	Jihoafrická
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
jižní	jižní	k2eAgFnSc7d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
Afrikou	Afrika	k1gFnSc7	Afrika
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
intenzivním	intenzivní	k2eAgInPc3d1	intenzivní
přesunům	přesun	k1gInPc3	přesun
a	a	k8xC	a
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
fauny	fauna	k1gFnSc2	fauna
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
hominidů	hominid	k1gMnPc2	hominid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zatím	zatím	k6eAd1	zatím
těžké	těžký	k2eAgNnSc1d1	těžké
vysledovat	vysledovat	k5eAaImF	vysledovat
<g/>
.	.	kIx.	.
</s>
<s>
Jasná	jasný	k2eAgFnSc1d1	jasná
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gMnSc1	Homo
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
vzešli	vzejít	k5eAaPmAgMnP	vzejít
z	z	k7c2	z
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
vyřešeno	vyřešit	k5eAaPmNgNnS	vyřešit
na	na	k7c4	na
který	který	k3yRgInSc4	který
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
<g/>
)	)	kIx)	)
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
lze	lze	k6eAd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
jen	jen	k9	jen
paranthropy	paranthrop	k1gInPc4	paranthrop
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
linií	linie	k1gFnPc2	linie
mohla	moct	k5eAaImAgFnS	moct
svébytným	svébytný	k2eAgInSc7d1	svébytný
způsobem	způsob	k1gInSc7	způsob
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
typicky	typicky	k6eAd1	typicky
lidské	lidský	k2eAgInPc4d1	lidský
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
reorganizace	reorganizace	k1gFnSc1	reorganizace
a	a	k8xC	a
růst	růst	k1gInSc1	růst
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
zubech	zub	k1gInPc6	zub
<g/>
,	,	kIx,	,
zlepšování	zlepšování	k1gNnSc1	zlepšování
bipedie	bipedie	k1gFnSc2	bipedie
<g/>
,	,	kIx,	,
opouštění	opouštění	k1gNnSc1	opouštění
stromového	stromový	k2eAgNnSc2d1	stromové
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
užívání	užívání	k1gNnSc1	užívání
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tyto	tento	k3xDgFnPc1	tento
kroky	krok	k1gInPc1	krok
dokončila	dokončit	k5eAaPmAgFnS	dokončit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Časné	časný	k2eAgFnPc1d1	časná
formy	forma	k1gFnPc1	forma
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
mladších	mladý	k2eAgMnPc2d2	mladší
zástupců	zástupce	k1gMnPc2	zástupce
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
navíc	navíc	k6eAd1	navíc
žily	žít	k5eAaImAgInP	žít
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
2,8	[number]	k4	2,8
–	–	k?	–
1,3	[number]	k4	1,3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
souběžně	souběžně	k6eAd1	souběžně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
australopitékové	australopitéek	k1gMnPc1	australopitéek
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
známých	známý	k2eAgFnPc6d1	známá
lokalitách	lokalita	k1gFnPc6	lokalita
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
hojnější	hojný	k2eAgFnPc1d2	hojnější
než	než	k8xS	než
současně	současně	k6eAd1	současně
žijící	žijící	k2eAgMnPc1d1	žijící
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
potravními	potravní	k2eAgMnPc7d1	potravní
generalisty	generalista	k1gMnPc7	generalista
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
mělo	mít	k5eAaImAgNnS	mít
poskytovat	poskytovat	k5eAaImF	poskytovat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
šance	šance	k1gFnPc4	šance
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
nepříznivých	příznivý	k2eNgNnPc2d1	nepříznivé
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
úbytku	úbytek	k1gInSc2	úbytek
vlhkých	vlhký	k2eAgNnPc2d1	vlhké
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
záložních	záložní	k2eAgInPc2d1	záložní
zdrojů	zdroj	k1gInPc2	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
australopitékové	australopitéek	k1gMnPc1	australopitéek
překlenovali	překlenovat	k5eAaImAgMnP	překlenovat
suchá	suchý	k2eAgNnPc4d1	suché
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
neschopnost	neschopnost	k1gFnSc4	neschopnost
konkurovat	konkurovat	k5eAaImF	konkurovat
prasatům	prase	k1gNnPc3	prase
<g/>
,	,	kIx,	,
opicím	opice	k1gFnPc3	opice
či	či	k8xC	či
mladším	mladý	k2eAgFnPc3d2	mladší
formám	forma	k1gFnPc3	forma
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gNnSc1	Homo
(	(	kIx(	(
<g/>
především	především	k9	především
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívajícím	využívající	k2eAgMnSc7d1	využívající
stejné	stejný	k2eAgInPc4d1	stejný
potravní	potravní	k2eAgInPc4d1	potravní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
například	například	k6eAd1	například
mohl	moct	k5eAaImAgMnS	moct
díky	díky	k7c3	díky
lepší	dobrý	k2eAgFnSc3d2	lepší
schopnosti	schopnost	k1gFnSc3	schopnost
plánování	plánování	k1gNnSc2	plánování
záměrně	záměrně	k6eAd1	záměrně
stopovat	stopovat	k5eAaImF	stopovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
nespoléhat	spoléhat	k5eNaImF	spoléhat
jen	jen	k9	jen
na	na	k7c4	na
náhodné	náhodný	k2eAgInPc4d1	náhodný
nálezy	nález	k1gInPc4	nález
nebo	nebo	k8xC	nebo
dokázal	dokázat	k5eAaPmAgInS	dokázat
díky	díky	k7c3	díky
kamenným	kamenný	k2eAgInPc3d1	kamenný
nástrojům	nástroj	k1gInPc3	nástroj
rozbít	rozbít	k5eAaPmF	rozbít
zvířecí	zvířecí	k2eAgFnPc4d1	zvířecí
kosti	kost	k1gFnPc4	kost
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
morek	morek	k1gInSc4	morek
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
zbytky	zbytek	k1gInPc1	zbytek
masa	maso	k1gNnSc2	maso
zanechané	zanechaný	k2eAgFnSc2d1	zanechaná
šelmami	šelma	k1gFnPc7	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
hypotézy	hypotéza	k1gFnPc1	hypotéza
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nelze	lze	k6eNd1	lze
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
Australopitheacus	Australopitheacus	k1gInSc1	Australopitheacus
anamensis	anamensis	k1gFnSc2	anamensis
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
australopitékem	australopitéek	k1gInSc7	australopitéek
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Etiopie	Etiopie	k1gFnSc1	Etiopie
a	a	k8xC	a
Keňa	Keňa	k1gFnSc1	Keňa
asi	asi	k9	asi
před	před	k7c7	před
4,2	[number]	k4	4,2
-	-	kIx~	-
3,9	[number]	k4	3,9
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obýval	obývat	k5eAaImAgInS	obývat
pestrou	pestrý	k2eAgFnSc4d1	pestrá
mozaikovitou	mozaikovitý	k2eAgFnSc4d1	mozaikovitá
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
převážně	převážně	k6eAd1	převážně
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
druhu	druh	k1gInSc2	druh
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
předkem	předek	k1gInSc7	předek
všech	všecek	k3xTgInPc2	všecek
mladších	mladý	k2eAgInPc2d2	mladší
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
3,8	[number]	k4	3,8
až	až	k9	až
2,9	[number]	k4	2,9
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
početným	početný	k2eAgInPc3d1	početný
nálezům	nález	k1gInPc3	nález
z	z	k7c2	z
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
Keni	Keňa	k1gFnSc2	Keňa
a	a	k8xC	a
Etiopie	Etiopie	k1gFnSc2	Etiopie
(	(	kIx(	(
<g/>
na	na	k7c4	na
500	[number]	k4	500
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
poznaných	poznaný	k2eAgInPc2d1	poznaný
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Obýval	obývat	k5eAaImAgMnS	obývat
širší	široký	k2eAgNnSc4d2	širší
spektrum	spektrum	k1gNnSc4	spektrum
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využíval	využívat	k5eAaPmAgInS	využívat
různorodé	různorodý	k2eAgInPc4d1	různorodý
potravní	potravní	k2eAgInPc4d1	potravní
zdroje	zdroj	k1gInPc4	zdroj
převážně	převážně	k6eAd1	převážně
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předkem	předek	k1gInSc7	předek
rodu	rod	k1gInSc2	rod
Homo	Homo	k6eAd1	Homo
i	i	k9	i
robustních	robustní	k2eAgInPc2d1	robustní
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhu	druh	k1gInSc2	druh
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnSc2	afarensis
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
řadí	řadit	k5eAaImIp3nP	řadit
i	i	k9	i
ostatky	ostatek	k1gInPc7	ostatek
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgFnPc7d1	jiná
vyčleňované	vyčleňovaný	k2eAgInPc4d1	vyčleňovaný
do	do	k7c2	do
samostatných	samostatný	k2eAgInPc2d1	samostatný
druhů	druh	k1gInPc2	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
bahrelghazali	bahrelghazat	k5eAaPmAgMnP	bahrelghazat
a	a	k8xC	a
Kenyanthropus	Kenyanthropus	k1gInSc4	Kenyanthropus
platyops	platyopsa	k1gFnPc2	platyopsa
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
africanus	africanus	k1gMnSc1	africanus
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
doložen	doložit	k5eAaPmNgInS	doložit
hojnými	hojný	k2eAgInPc7d1	hojný
nálezy	nález	k1gInPc7	nález
z	z	k7c2	z
tamních	tamní	k2eAgFnPc2d1	tamní
krasových	krasový	k2eAgFnPc2d1	krasová
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
předchůdce	předchůdce	k1gMnSc4	předchůdce
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slepou	slepý	k2eAgFnSc4d1	slepá
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
východoafrickým	východoafrický	k2eAgMnPc3d1	východoafrický
australopitékům	australopitéek	k1gMnPc3	australopitéek
byl	být	k5eAaImAgInS	být
lépe	dobře	k6eAd2	dobře
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
lesnatému	lesnatý	k2eAgNnSc3d1	lesnaté
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
obživu	obživa	k1gFnSc4	obživa
však	však	k9	však
získával	získávat	k5eAaImAgMnS	získávat
i	i	k9	i
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
savaně	savana	k1gFnSc6	savana
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
garhi	garhi	k6eAd1	garhi
žil	žít	k5eAaImAgMnS	žít
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
2,5	[number]	k4	2,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
mělkého	mělký	k2eAgNnSc2d1	mělké
jezera	jezero	k1gNnSc2	jezero
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
Awaš	Awaš	k1gInSc1	Awaš
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
poznaný	poznaný	k2eAgInSc1d1	poznaný
australopiték	australopiték	k1gInSc1	australopiték
měl	mít	k5eAaImAgInS	mít
patrně	patrně	k6eAd1	patrně
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnPc4d2	delší
dolní	dolní	k2eAgFnPc4d1	dolní
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ostatků	ostatek	k1gInPc2	ostatek
hominidů	hominid	k1gMnPc2	hominid
byly	být	k5eAaImAgInP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
i	i	k8xC	i
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnPc4d1	stará
kosti	kost	k1gFnPc4	kost
turovitých	turovití	k1gMnPc2	turovití
a	a	k8xC	a
koní	koní	k2eAgInSc1d1	koní
Hipparion	Hipparion	k1gInSc1	Hipparion
s	s	k7c7	s
rýhami	rýha	k1gFnPc7	rýha
a	a	k8xC	a
stopami	stopa	k1gFnPc7	stopa
úderů	úder	k1gInPc2	úder
kamenem	kámen	k1gInSc7	kámen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
poškození	poškození	k1gNnSc4	poškození
způsobili	způsobit	k5eAaPmAgMnP	způsobit
zástupci	zástupce	k1gMnPc7	zástupce
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
garhi	garh	k1gFnPc1	garh
<g/>
,	,	kIx,	,
souvislost	souvislost	k1gFnSc1	souvislost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
sediba	sediba	k1gMnSc1	sediba
žil	žít	k5eAaImAgMnS	žít
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
australopitéků	australopitéek	k1gMnPc2	australopitéek
před	před	k7c7	před
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jeskyně	jeskyně	k1gFnSc2	jeskyně
Malapa	Malapa	k1gFnSc1	Malapa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
zachyceny	zachycen	k2eAgFnPc1d1	zachycena
unikátně	unikátně	k6eAd1	unikátně
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
kostry	kostra	k1gFnPc1	kostra
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnPc1d1	představující
jedince	jedinko	k6eAd1	jedinko
dobře	dobře	k6eAd1	dobře
přizpůsobené	přizpůsobený	k2eAgFnPc1d1	přizpůsobená
ke	k	k7c3	k
šplhání	šplhání	k1gNnSc3	šplhání
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
moderně	moderně	k6eAd1	moderně
utvářenou	utvářený	k2eAgFnSc7d1	utvářená
pánví	pánev	k1gFnSc7	pánev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
běžně	běžně	k6eAd1	běžně
užívanou	užívaný	k2eAgFnSc4d1	užívaná
bipedii	bipedie	k1gFnSc4	bipedie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
již	již	k6eAd1	již
nese	nést	k5eAaImIp3nS	nést
překapivě	překapivě	k6eAd1	překapivě
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
typických	typický	k2eAgInPc2d1	typický
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
bahrelghazali	bahrelghazat	k5eAaImAgMnP	bahrelghazat
byl	být	k5eAaImAgInS	být
zachycen	zachytit	k5eAaPmNgInS	zachytit
v	v	k7c6	v
Čadu	Čad	k1gInSc6	Čad
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgInSc7d1	jediný
australopitékem	australopitéek	k1gInSc7	australopitéek
<g/>
,	,	kIx,	,
nalezeným	nalezený	k2eAgInSc7d1	nalezený
východně	východně	k6eAd1	východně
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
3	[number]	k4	3
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
obýval	obývat	k5eAaImAgInS	obývat
nezvykle	zvykle	k6eNd1	zvykle
otevřený	otevřený	k2eAgInSc1d1	otevřený
ekosystém	ekosystém	k1gInSc1	ekosystém
bez	bez	k7c2	bez
výraznějších	výrazný	k2eAgInPc2d2	výraznější
stromových	stromový	k2eAgInPc2d1	stromový
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
deyiremeda	deyiremeda	k1gMnSc1	deyiremeda
byl	být	k5eAaImAgMnS	být
současníkem	současník	k1gMnSc7	současník
druhu	druh	k1gInSc2	druh
Australopithecus	Australopithecus	k1gInSc4	Australopithecus
afarensis	afarensis	k1gInSc1	afarensis
-	-	kIx~	-
žil	žít	k5eAaImAgInS	žít
před	před	k7c7	před
3,3	[number]	k4	3,3
až	až	k9	až
3,5	[number]	k4	3,5
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jen	jen	k9	jen
z	z	k7c2	z
několika	několik	k4yIc2	několik
zlomků	zlomek	k1gInPc2	zlomek
čelistí	čelist	k1gFnPc2	čelist
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
utváření	utváření	k1gNnSc1	utváření
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
blíží	blížit	k5eAaImIp3nS	blížit
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
Kenyanthropus	Kenyanthropus	k1gInSc4	Kenyanthropus
platyops	platyops	k6eAd1	platyops
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
aethiopicus	aethiopicus	k1gMnSc1	aethiopicus
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
prokazatelným	prokazatelný	k2eAgMnSc7d1	prokazatelný
zástupcem	zástupce	k1gMnSc7	zástupce
robustních	robustní	k2eAgInPc2d1	robustní
australopitéků	australopiték	k1gInPc2	australopiték
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
2,3	[number]	k4	2,3
až	až	k9	až
2,7	[number]	k4	2,7
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Keni	Keňa	k1gFnSc2	Keňa
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
patrně	patrně	k6eAd1	patrně
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
druhem	druh	k1gInSc7	druh
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
afarensis	afarensis	k1gFnPc2	afarensis
a	a	k8xC	a
mladšími	mladý	k2eAgMnPc7d2	mladší
paranthropy	paranthrop	k1gMnPc7	paranthrop
(	(	kIx(	(
<g/>
A.	A.	kA	A.
robustus	robustus	k1gInSc1	robustus
<g/>
,	,	kIx,	,
A.	A.	kA	A.
boisei	boise	k1gFnSc2	boise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obýval	obývat	k5eAaImAgMnS	obývat
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
zalesněné	zalesněný	k2eAgFnPc4d1	zalesněná
prostory	prostora	k1gFnPc4	prostora
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
již	již	k6eAd1	již
ale	ale	k8xC	ale
pronikal	pronikat	k5eAaImAgMnS	pronikat
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nacházel	nacházet	k5eAaImAgMnS	nacházet
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gMnSc1	Paranthropus
<g/>
)	)	kIx)	)
robustus	robustus	k1gMnSc1	robustus
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
jihoafických	jihoafický	k2eAgFnPc2d1	jihoafický
krasových	krasový	k2eAgFnPc2d1	krasová
jeskyní	jeskyně	k1gFnPc2	jeskyně
v	v	k7c6	v
Kolébce	kolébka	k1gFnSc6	kolébka
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
okolí	okolí	k1gNnSc4	okolí
obýval	obývat	k5eAaImAgMnS	obývat
před	před	k7c7	před
1,5	[number]	k4	1,5
až	až	k8xS	až
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stával	stávat	k5eAaImAgInS	stávat
kořistí	kořist	k1gFnSc7	kořist
masožravých	masožravý	k2eAgFnPc2d1	masožravá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc1d1	tvořená
převážně	převážně	k6eAd1	převážně
travnatými	travnatý	k2eAgFnPc7d1	travnatá
pláněmi	pláň	k1gFnPc7	pláň
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
občasnými	občasný	k2eAgFnPc7d1	občasná
skupinkami	skupinka	k1gFnPc7	skupinka
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
strava	strava	k1gFnSc1	strava
byla	být	k5eAaImAgFnS	být
patrně	patrně	k6eAd1	patrně
výrazně	výrazně	k6eAd1	výrazně
variabilní	variabilní	k2eAgFnSc1d1	variabilní
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
aktuálně	aktuálně	k6eAd1	aktuálně
dostupných	dostupný	k2eAgInPc2d1	dostupný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
(	(	kIx(	(
<g/>
Paranthropus	Paranthropus	k1gInSc1	Paranthropus
<g/>
)	)	kIx)	)
boisei	boisei	k6eAd1	boisei
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
australopiték	australopiték	k1gMnSc1	australopiték
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
1,3	[number]	k4	1,3
až	až	k9	až
2,3	[number]	k4	2,3
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejodvozenějším	odvozený	k2eAgMnSc7d3	odvozený
druhem	druh	k1gMnSc7	druh
paranthropů	paranthrop	k1gMnPc2	paranthrop
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
lebka	lebka	k1gFnSc1	lebka
i	i	k8xC	i
čelisti	čelist	k1gFnPc1	čelist
jsou	být	k5eAaImIp3nP	být
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhledával	vyhledávat	k5eAaImAgInS	vyhledávat
vlhká	vlhký	k2eAgNnPc4d1	vlhké
místa	místo	k1gNnPc4	místo
při	při	k7c6	při
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
živit	živit	k5eAaImF	živit
oddenky	oddenek	k1gInPc4	oddenek
šáchorovitých	šáchorovitý	k2eAgFnPc2d1	šáchorovitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
