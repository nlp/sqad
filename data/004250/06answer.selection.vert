<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
několik	několik	k4yIc4	několik
ulic	ulice	k1gFnPc2	ulice
<g/>
:	:	kIx,	:
ulice	ulice	k1gFnSc1	ulice
Járy	Jára	k1gMnSc2	Jára
da	da	k?	da
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Járy	Jára	k1gMnSc2	Jára
da	da	k?	da
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Roztokách	roztoka	k1gFnPc6	roztoka
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
ulička	ulička	k1gFnSc1	ulička
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Chodníček	chodníček	k1gInSc1	chodníček
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
"	"	kIx"	"
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
<g/>
,	,	kIx,	,
Okružní	okružní	k2eAgFnSc1d1	okružní
třída	třída	k1gFnSc1	třída
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Huti	huť	k1gFnSc6	huť
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kunice	Kunice	k1gFnSc2	Kunice
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Homole	homole	k1gFnSc2	homole
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Horšovském	horšovský	k2eAgInSc6d1	horšovský
Týně	Týn	k1gInSc6	Týn
a	a	k8xC	a
kuriózní	kuriózní	k2eAgNnSc1d1	kuriózní
nábřeží	nábřeží	k1gNnSc1	nábřeží
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Lipníku	Lipník	k1gInSc6	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vůbec	vůbec	k9	vůbec
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
