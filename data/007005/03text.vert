<s>
Malé	Malé	k2eAgFnPc4d1	Malé
Antily	Antily	k1gFnPc4	Antily
je	být	k5eAaImIp3nS	být
pás	pás	k1gInSc1	pás
ostrovů	ostrov	k1gInPc2	ostrov
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
;	;	kIx,	;
součást	součást	k1gFnSc1	součást
souostroví	souostroví	k1gNnSc2	souostroví
Antily	Antily	k1gFnPc4	Antily
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sopečné	sopečný	k2eAgInPc1d1	sopečný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
<g/>
:	:	kIx,	:
Závětrné	závětrný	k2eAgInPc4d1	závětrný
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Anguilla	Anguill	k1gMnSc4	Anguill
<g/>
,	,	kIx,	,
Antigua	Antiguus	k1gMnSc4	Antiguus
<g/>
,	,	kIx,	,
Barbuda	Barbud	k1gMnSc4	Barbud
<g/>
,	,	kIx,	,
Dominika	Dominik	k1gMnSc4	Dominik
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gMnSc4	Guadeloupe
<g/>
,	,	kIx,	,
Montserrat	Montserrat	k1gMnSc1	Montserrat
<g/>
,	,	kIx,	,
Nevis	viset	k5eNaImRp2nS	viset
<g/>
,	,	kIx,	,
Panenské	panenský	k2eAgInPc4d1	panenský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Saba	Saba	k1gMnSc1	Saba
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Eustach	Eustach	k1gMnSc1	Eustach
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
Návětrné	návětrný	k2eAgInPc4d1	návětrný
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Grenadiny	grenadina	k1gFnPc1	grenadina
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gInSc1	Martinik
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
<g/>
,	,	kIx,	,
Tobago	Tobago	k1gMnSc1	Tobago
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
Závětrné	závětrný	k2eAgFnPc1d1	závětrná
Antily	Antily	k1gFnPc1	Antily
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Aruba	Aruba	k1gMnSc1	Aruba
<g/>
,	,	kIx,	,
Bonaire	Bonair	k1gMnSc5	Bonair
<g/>
,	,	kIx,	,
Curaçao	curaçao	k1gNnPc1	curaçao
<g/>
,	,	kIx,	,
Isla	Isl	k2eAgFnSc1d1	Isla
de	de	k?	de
Margarita	Margarita	k1gFnSc1	Margarita
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
sopky	sopka	k1gFnSc2	sopka
Soufriè	Soufriè	k1gFnSc2	Soufriè
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guadeloupe	Guadeloupe	k1gFnSc2	Guadeloupe
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
dělením	dělení	k1gNnSc7	dělení
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
západoevropských	západoevropský	k2eAgInPc6d1	západoevropský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
Barbados	Barbadosa	k1gFnPc2	Barbadosa
Dominika	Dominik	k1gMnSc2	Dominik
Grenada	Grenada	k1gFnSc1	Grenada
Svatá	svatá	k1gFnSc1	svatá
Lucie	Lucie	k1gFnSc2	Lucie
Svatý	svatý	k1gMnSc1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
Svatý	svatý	k1gMnSc1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnSc2	grenadina
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k6eAd1	Tobago
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
-	-	kIx~	-
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
americké	americký	k2eAgFnPc1d1	americká
državy	država	k1gFnPc1	država
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
"	"	kIx"	"
<g/>
Zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Anguilla	Anguilla	k1gFnSc1	Anguilla
Britské	britský	k2eAgNnSc1d1	Britské
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
Montserrat	Montserrat	k1gInSc4	Montserrat
Nizozemské	nizozemský	k2eAgNnSc1d1	Nizozemské
království	království	k1gNnSc1	království
<g />
.	.	kIx.	.
</s>
<s>
Aruba	Aruba	k1gFnSc1	Aruba
Curaçao	curaçao	k1gNnSc2	curaçao
Bonaire	Bonair	k1gInSc5	Bonair
Saba	Sabum	k1gNnSc2	Sabum
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
Svatý	svatý	k2eAgMnSc1d1	svatý
Eustach	Eustach	k1gMnSc1	Eustach
Francie	Francie	k1gFnSc2	Francie
Guadeloupe	Guadeloupe	k1gMnSc1	Guadeloupe
-	-	kIx~	-
zámořský	zámořský	k2eAgInSc1d1	zámořský
departement	departement	k1gInSc1	departement
a	a	k8xC	a
region	region	k1gInSc1	region
DOM-ROM	DOM-ROM	k1gFnPc2	DOM-ROM
Martinik	Martinik	k1gInSc1	Martinik
-	-	kIx~	-
zámořský	zámořský	k2eAgInSc1d1	zámořský
departement	departement	k1gInSc1	departement
a	a	k8xC	a
region	region	k1gInSc1	region
DOM-ROM	DOM-ROM	k1gFnPc2	DOM-ROM
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
-	-	kIx~	-
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
COM	COM	kA	COM
Svatý	svatý	k2eAgMnSc1d1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
-	-	kIx~	-
zámořské	zámořský	k2eAgNnSc1d1	zámořské
společenství	společenství	k1gNnSc1	společenství
COM	COM	kA	COM
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
-	-	kIx~	-
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
v	v	k7c6	v
karibské	karibský	k2eAgFnSc6d1	karibská
oblasti	oblast	k1gFnSc6	oblast
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
ostrovní	ostrovní	k2eAgNnSc1d1	ostrovní
území	území	k1gNnSc1	území
USA	USA	kA	USA
<g/>
:	:	kIx,	:
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
území	území	k1gNnPc1	území
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
