<s>
Ribonukleová	ribonukleový	k2eAgFnSc1d1	ribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
dříve	dříve	k6eAd2	dříve
RNK	RNK	kA	RNK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
vláknem	vlákno	k1gNnSc7	vlákno
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cukr	cukr	k1gInSc4	cukr
ribózu	ribóza	k1gFnSc4	ribóza
a	a	k8xC	a
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
<g/>
,	,	kIx,	,
cytosin	cytosin	k1gInSc1	cytosin
a	a	k8xC	a
uracil	uracil	k1gInSc1	uracil
<g/>
.	.	kIx.	.
</s>
