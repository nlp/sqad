<s>
Pragmatika	pragmatika	k1gFnSc1
</s>
<s>
Pragmatika	pragmatika	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
pragmatická	pragmatický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
pragma	pragmum	k1gNnSc2
<g/>
,	,	kIx,
skutek	skutek	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
na	na	k7c6
pomezí	pomezí	k1gNnSc6
lingvistiky	lingvistika	k1gFnSc2
a	a	k8xC
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
řečovými	řečový	k2eAgInPc7d1
akty	akt	k1gInPc7
(	(	kIx(
<g/>
promluvami	promluva	k1gFnPc7
<g/>
,	,	kIx,
výpověďmi	výpověď	k1gFnPc7
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
účelnou	účelný	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
řeči	řeč	k1gFnSc2
<g/>
:	:	kIx,
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
něco	něco	k3yInSc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
obvykle	obvykle	k6eAd1
tím	ten	k3xDgNnSc7
sleduje	sledovat	k5eAaImIp3nS
i	i	k9
nějaký	nějaký	k3yIgInSc4
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
sémantiky	sémantika	k1gFnSc2
se	se	k3xPyFc4
pragmatika	pragmatika	k1gFnSc1
nespokojuje	spokojovat	k5eNaImIp3nS
s	s	k7c7
ideálním	ideální	k2eAgInSc7d1
(	(	kIx(
<g/>
slovníkovým	slovníkový	k2eAgInSc7d1
<g/>
)	)	kIx)
významem	význam	k1gInSc7
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
všímá	všímat	k5eAaImIp3nS
si	se	k3xPyFc3
záměru	záměra	k1gFnSc4
i	i	k8xC
strategie	strategie	k1gFnPc4
mluvčího	mluvčí	k1gMnSc2
<g/>
,	,	kIx,
situace	situace	k1gFnSc2
a	a	k8xC
kontextu	kontext	k1gInSc2
výpovědí	výpověď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
významu	význam	k1gInSc2
výpovědi	výpověď	k1gFnSc2
se	se	k3xPyFc4
zajímá	zajímat	k5eAaImIp3nS
i	i	k9
o	o	k7c6
její	její	k3xOp3gFnSc6
–	–	k?
často	často	k6eAd1
mimo-řečové	mimo-řečové	k2eAgMnSc1d1
–	–	k?
účinky	účinek	k1gInPc4
a	a	k8xC
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pragmatika	pragmatika	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
název	název	k1gInSc1
subdisciplíny	subdisciplína	k1gFnSc2
sémiologie	sémiologie	k1gFnSc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
pouze	pouze	k6eAd1
vztahem	vztah	k1gInSc7
jazykových	jazykový	k2eAgInPc2d1
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
slov	slovo	k1gNnPc2
<g/>
)	)	kIx)
k	k	k7c3
uživateli	uživatel	k1gMnSc3
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k9
ne	ne	k9
např.	např.	kA
popisem	popis	k1gInSc7
užití	užití	k1gNnSc2
pragmaticky	pragmaticky	k6eAd1
aktivních	aktivní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Když	když	k8xS
diplomat	diplomat	k1gMnSc1
řekne	říct	k5eAaPmIp3nS
ano	ano	k9
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nP
tím	ten	k3xDgInSc7
„	„	k?
<g/>
možná	možná	k9
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
když	když	k8xS
řekne	říct	k5eAaPmIp3nS
možná	možná	k9
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nS
„	„	k?
<g/>
ne	ne	k9
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
když	když	k8xS
řekne	říct	k5eAaPmIp3nS
ne	ne	k9
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
diplomat	diplomat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
dáma	dáma	k1gFnSc1
řekne	říct	k5eAaPmIp3nS
ne	ne	k9
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nP
tím	ten	k3xDgInSc7
„	„	k?
<g/>
možná	možná	k9
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
když	když	k8xS
řekne	říct	k5eAaPmIp3nS
možná	možná	k9
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nS
„	„	k?
<g/>
ano	ano	k9
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
když	když	k8xS
řekne	říct	k5eAaPmIp3nS
ano	ano	k9
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
dáma	dáma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Voltaire	Voltair	k1gMnSc5
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
</s>
<s>
Starší	starý	k2eAgFnSc1d2
lingvistika	lingvistika	k1gFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
především	především	k9
jazykem	jazyk	k1gInSc7
(	(	kIx(
<g/>
anebo	anebo	k8xC
různými	různý	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
systémem	systém	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gNnPc7
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
všímala	všímat	k5eAaImAgFnS
vět	věta	k1gFnPc2
a	a	k8xC
výpovědí	výpověď	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
obvykle	obvykle	k6eAd1
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
ptáme	ptat	k5eAaImIp1nP
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
pravdivá	pravdivý	k2eAgNnPc1d1
nebo	nebo	k8xC
nepravdivá	pravdivý	k2eNgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
prvních	první	k4xOgMnPc2
indických	indický	k2eAgMnPc2d1
jazykovědců	jazykovědec	k1gMnPc2
a	a	k8xC
od	od	k7c2
Aristotela	Aristoteles	k1gMnSc2
měla	mít	k5eAaImAgFnS
tak	tak	k6eAd1
velmi	velmi	k6eAd1
blízko	blízko	k6eAd1
k	k	k7c3
logice	logika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazykové	jazykový	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
typů	typ	k1gInPc2
výpovědí	výpověď	k1gFnPc2
si	se	k3xPyFc3
všiml	všimnout	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
Augustinus	Augustinus	k1gMnSc1
ve	v	k7c6
spise	spis	k1gInSc6
„	„	k?
<g/>
O	o	k7c6
učiteli	učitel	k1gMnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozebírá	rozebírat	k5eAaImIp3nS
situaci	situace	k1gFnSc4
a	a	k8xC
postup	postup	k1gInSc4
rozhovoru	rozhovor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatel	zakladatel	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
de	de	k?
Saussure	Saussur	k1gMnSc5
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
-	-	kIx~
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
zavedl	zavést	k5eAaPmAgMnS
důležité	důležitý	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
mezi	mezi	k7c7
jazykem	jazyk	k1gInSc7
a	a	k8xC
řečí	řeč	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Jazyk	jazyk	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
langue	langue	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vícevrstevný	vícevrstevný	k2eAgInSc4d1
systém	systém	k1gInSc4
fonetiky	fonetika	k1gFnSc2
<g/>
,	,	kIx,
lexika	lexikon	k1gNnSc2
a	a	k8xC
gramatiky	gramatika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
představuje	představovat	k5eAaImIp3nS
souhrn	souhrn	k1gInSc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
má	mít	k5eAaImIp3nS
mluvčí	mluvčí	k1gMnPc4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americký	americký	k2eAgMnSc1d1
lingvista	lingvista	k1gMnSc1
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
jazykové	jazykový	k2eAgFnSc6d1
kompetenci	kompetence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Řeč	řeč	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
parole	parole	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
souhrn	souhrn	k1gInSc4
aktuálních	aktuální	k2eAgFnPc2d1
promluv	promluva	k1gFnPc2
čili	čili	k8xC
praktických	praktický	k2eAgNnPc2d1
použití	použití	k1gNnPc2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
novější	nový	k2eAgFnSc6d2
terminologii	terminologie	k1gFnSc6
řečových	řečový	k2eAgInPc2d1
aktů	akt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Obě	dva	k4xCgFnPc1
stránky	stránka	k1gFnPc1
přitom	přitom	k6eAd1
spolu	spolu	k6eAd1
těsně	těsně	k6eAd1
souvisejí	souviset	k5eAaImIp3nP
<g/>
:	:	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
ovládat	ovládat	k5eAaImF
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
naučil	naučit	k5eAaPmAgMnS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
řeči	řeč	k1gFnSc6
obě	dva	k4xCgNnPc1
slova	slovo	k1gNnPc1
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
synonyma	synonymum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Začátky	začátek	k1gInPc1
pragmatiky	pragmatika	k1gFnSc2
</s>
<s>
Různé	různý	k2eAgFnPc1d1
významové	významový	k2eAgFnPc1d1
situace	situace	k1gFnPc1
a	a	k8xC
praktické	praktický	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnSc2
řeči	řeč	k1gFnSc2
studoval	studovat	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
-	-	kIx~
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
pozdním	pozdní	k2eAgInSc6d1
díle	díl	k1gInSc6
„	„	k?
<g/>
Filosofická	filosofický	k2eAgNnPc4d1
zkoumání	zkoumání	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
věnováno	věnovat	k5eAaImNgNnS,k5eAaPmNgNnS
zkoumání	zkoumání	k1gNnSc1
„	„	k?
<g/>
řečových	řečový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Sprachspiele	Sprachspiel	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
není	být	k5eNaImIp3nS
jen	jen	k9
výměnou	výměna	k1gFnSc7
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
účastníci	účastník	k1gMnPc1
v	v	k7c6
něm	on	k3xPp3gInSc6
často	často	k6eAd1
sledují	sledovat	k5eAaImIp3nP
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
dosti	dosti	k6eAd1
vzdálené	vzdálený	k2eAgNnSc1d1
přímému	přímý	k2eAgInSc3d1
obsahu	obsah	k1gInSc3
řeči	řeč	k1gFnSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Filosofická	filosofický	k2eAgNnPc4d1
zkoumání	zkoumání	k1gNnSc4
<g/>
“	“	k?
lze	lze	k6eAd1
tedy	tedy	k9
pokládat	pokládat	k5eAaImF
za	za	k7c4
zakladatelské	zakladatelský	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
pragmatiky	pragmatika	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgFnPc4
navázali	navázat	k5eAaPmAgMnP
zejména	zejména	k9
anglicky	anglicky	k6eAd1
mluvící	mluvící	k2eAgMnPc1d1
badatelé	badatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Austin	Austin	k1gMnSc1
a	a	k8xC
Grice	Grice	k1gMnSc1
</s>
<s>
Britský	britský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
John	John	k1gMnSc1
Langshaw	Langshaw	k1gMnSc1
Austin	Austin	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
si	se	k3xPyFc3
všiml	všimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vyslovení	vyslovení	k1gNnSc1
určitých	určitý	k2eAgFnPc2d1
vět	věta	k1gFnPc2
může	moct	k5eAaImIp3nS
přímo	přímo	k6eAd1
způsobit	způsobit	k5eAaPmF
nějakou	nějaký	k3yIgFnSc4
společenskou	společenský	k2eAgFnSc4d1
skutečnost	skutečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
řekne	říct	k5eAaPmIp3nS
„	„	k?
<g/>
Přijdu	přijít	k5eAaPmIp1nS
zítra	zítra	k6eAd1
<g/>
“	“	k?
si	se	k3xPyFc3
tím	ten	k3xDgNnSc7
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
závazek	závazek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
předtím	předtím	k6eAd1
neměl	mít	k5eNaImAgInS
<g/>
;	;	kIx,
když	když	k8xS
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
řeknou	říct	k5eAaPmIp3nP
„	„	k?
<g/>
Ano	ano	k9
<g/>
“	“	k?
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
z	z	k7c2
nich	on	k3xPp3gMnPc6
manželé	manžel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
posmrtně	posmrtně	k6eAd1
vydané	vydaný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
„	„	k?
<g/>
Jak	jak	k6eAd1
udělat	udělat	k5eAaPmF
něco	něco	k6eAd1
slovy	slovo	k1gNnPc7
<g/>
“	“	k?
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
navázal	navázat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
žák	žák	k1gMnSc1
John	John	k1gMnSc1
Searle	Searle	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1932	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
rozlišil	rozlišit	k5eAaPmAgMnS
pět	pět	k4xCc4
typů	typ	k1gInPc2
ilokučních	ilokuční	k2eAgInPc2d1
(	(	kIx(
<g/>
„	„	k?
<g/>
oslovovacích	oslovovací	k2eAgInPc2d1
<g/>
“	“	k?
<g/>
)	)	kIx)
aktů	akt	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Reprezentativní	reprezentativní	k2eAgFnSc1d1
čili	čili	k8xC
asertivní	asertivní	k2eAgFnSc1d1
(	(	kIx(
<g/>
ujišťující	ujišťující	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
mluvčí	mluvčí	k1gFnSc1
něco	něco	k3yInSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
například	například	k6eAd1
„	„	k?
<g/>
Prší	pršet	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Direktivní	direktivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
chce	chtít	k5eAaImIp3nS
mluvčí	mluvčí	k1gMnSc1
osloveného	oslovený	k2eAgNnSc2d1
pohnout	pohnout	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
něco	něco	k3yInSc4
udělal	udělat	k5eAaPmAgMnS
<g/>
,	,	kIx,
například	například	k6eAd1
„	„	k?
<g/>
Zavři	zavřít	k5eAaPmRp2nS
dveře	dveře	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Zavazující	zavazující	k2eAgFnSc1d1
čili	čili	k8xC
komissivní	komissivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
mluvčí	mluvčí	k1gMnSc1
k	k	k7c3
něčemu	něco	k3yInSc3
zavazuje	zavazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
například	například	k6eAd1
„	„	k?
<g/>
Zítra	zítra	k6eAd1
to	ten	k3xDgNnSc1
udělám	udělat	k5eAaPmIp1nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Expresivní	expresivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
mluvčí	mluvčí	k1gMnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
pocity	pocit	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
„	„	k?
<g/>
Jsem	být	k5eAaImIp1nS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
prší	pršet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Deklarativní	deklarativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
mluvčí	mluvčí	k1gMnSc1
způsobí	způsobit	k5eAaPmIp3nS
nějakou	nějaký	k3yIgFnSc4
změnu	změna	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
„	„	k?
<g/>
Odsuzuji	odsuzovat	k5eAaImIp1nS
vás	vy	k3xPp2nPc4
na	na	k7c4
týden	týden	k1gInSc4
do	do	k7c2
vězení	vězení	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Britsko-americký	britsko-americký	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
Paul	Paul	k1gMnSc1
Grice	Grice	k1gMnSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
-	-	kIx~
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
zdůraznil	zdůraznit	k5eAaPmAgMnS
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
doslovným	doslovný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
promluvy	promluva	k1gFnSc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
mluvčí	mluvčí	k1gMnSc1
zamýšlí	zamýšlet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
příjemci	příjemce	k1gMnSc3
dává	dávat	k5eAaImIp3nS
najevo	najevo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozvání	pozvání	k1gNnSc1
na	na	k7c6
večeři	večeře	k1gFnSc6
může	moct	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
odmítnout	odmítnout	k5eAaPmF
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
Už	už	k6eAd1
něco	něco	k3yInSc4
mám	mít	k5eAaImIp1nS
<g/>
“	“	k?
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gMnPc6
o	o	k7c6
večeři	večeře	k1gFnSc6
vůbec	vůbec	k9
nezmíní	zmínit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6
hrají	hrát	k5eAaImIp3nP
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
sdílené	sdílený	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
a	a	k8xC
významy	význam	k1gInPc4
<g/>
,	,	kIx,
Grice	Grice	k1gFnPc4
jim	on	k3xPp3gMnPc3
říká	říkat	k5eAaImIp3nS
konverzační	konverzační	k2eAgFnSc4d1
implikatury	implikatura	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
se	se	k3xPyFc4
ptá	ptat	k5eAaImIp3nS
B	B	kA
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
společný	společný	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
C	C	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
bance	banka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
odpovídá	odpovídat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Dobře	dobře	k6eAd1
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
ho	on	k3xPp3gMnSc4
nezavřeli	zavřít	k5eNaPmAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
Říká	říkat	k5eAaImIp3nS
sice	sice	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
dává	dávat	k5eAaImIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
C	C	kA
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
zaplést	zaplést	k5eAaPmF
do	do	k7c2
nějaké	nějaký	k3yIgFnSc2
aféry	aféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
kromě	kromě	k7c2
toho	ten	k3xDgInSc2
předpokládá	předpokládat	k5eAaImIp3nS
u	u	k7c2
všech	všecek	k3xTgMnPc2
účastníků	účastník	k1gMnPc2
jisté	jistý	k2eAgFnSc2d1
„	„	k?
<g/>
kooperativní	kooperativní	k2eAgInPc1d1
principy	princip	k1gInPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
o	o	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
sice	sice	k8xC
nemluví	mluvit	k5eNaImIp3nS
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgFnPc4
se	se	k3xPyFc4
ale	ale	k9
všichni	všechen	k3xTgMnPc1
spoléhají	spoléhat	k5eAaImIp3nP
–	–	k?
například	například	k6eAd1
neříkat	říkat	k5eNaImF
<g/>
,	,	kIx,
o	o	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
mluvit	mluvit	k5eAaImF
jasně	jasně	k6eAd1
a	a	k8xC
stručně	stručně	k6eAd1
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
...	...	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Karlík	Karlík	k1gMnSc1
...	...	k?
Jarmila	Jarmila	k1gFnSc1
Bachmannová	Bachmannová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedický	encyklopedický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakl	Nakl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
Noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
710	#num#	k4
<g/>
-	-	kIx~
<g/>
6484	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Pragmalingvistika	Pragmalingvistika	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
332	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cit	cit	k1gInSc1
<g/>
.	.	kIx.
ve	v	k7c4
Stanford	Stanford	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosoph	k1gInPc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Pragmatics	Pragmaticsa	k1gFnPc2
.	.	kIx.
<g/>
↑	↑	k?
Heslo	heslo	k1gNnSc4
Pragmatics	Pragmatics	k1gInSc4
ve	v	k7c4
Stanford	Stanford	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosopha	k1gFnSc2
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
J.	J.	kA
L.	L.	kA
Austin	Austina	k1gFnPc2
<g/>
,	,	kIx,
Jak	jak	k8xC,k8xS
udělat	udělat	k5eAaPmF
něco	něco	k3yInSc4
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
2000	#num#	k4
-	-	kIx~
172	#num#	k4
str	str	kA
<g/>
.	.	kIx.
ISBN	ISBN	kA
80-7007-133-8	80-7007-133-8	k4
</s>
<s>
D.	D.	kA
Bolinger	Bolinger	k1gInSc1
<g/>
,	,	kIx,
Jazyk	jazyk	k1gInSc1
jako	jako	k8xS,k8xC
nabitá	nabitý	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užívání	užívání	k1gNnSc1
a	a	k8xC
zneužívání	zneužívání	k1gNnSc1
jazyka	jazyk	k1gInSc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Zima	Zima	k1gMnSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-254-0313-6	978-80-254-0313-6	k4
</s>
<s>
M.	M.	kA
Hirschová	Hirschová	k1gFnSc1
<g/>
,	,	kIx,
Pragmatika	pragmatika	k1gFnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
<g/>
,	,	kIx,
2006	#num#	k4
-	-	kIx~
243	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-244-1283-7	80-244-1283-7	k4
</s>
<s>
S.	S.	kA
Machová	Machová	k1gFnSc1
-	-	kIx~
M.	M.	kA
Švehlová	Švehlová	k1gFnSc1
<g/>
,	,	kIx,
Sémantika	sémantika	k1gFnSc1
&	&	k?
pragmatická	pragmatický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
2001	#num#	k4
-	-	kIx~
159	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-7290-061-7	80-7290-061-7	k4
</s>
<s>
J.	J.	kA
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Člověk	člověk	k1gMnSc1
jako	jako	k8xS,k8xC
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7178-627-6	80-7178-627-6	k4
Kap	kapa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeč	řeč	k1gFnSc1
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
P.	P.	kA
Watzlawick	Watzlawick	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Pragmatika	pragmatika	k1gFnSc1
lidské	lidský	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
:	:	kIx,
interakční	interakční	k2eAgInPc1d1
vzorce	vzorec	k1gInPc1
<g/>
,	,	kIx,
patologie	patologie	k1gFnPc1
a	a	k8xC
paradoxy	paradox	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Konfrontace	konfrontace	k1gFnSc1
1999	#num#	k4
-	-	kIx~
243	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-86088-04-9	80-86088-04-9	k4
</s>
<s>
L.	L.	kA
Wittgenstein	Wittgenstein	k1gInSc1
<g/>
,	,	kIx,
Filosofická	filosofický	k2eAgNnPc1d1
zkoumání	zkoumání	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
1998	#num#	k4
ISBN	ISBN	kA
80-7007-103-6	80-7007-103-6	k4
</s>
<s>
MAŠÍN	MAŠÍN	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragmalingvistika	Pragmalingvistika	k1gFnSc1
a	a	k8xC
osvojování	osvojování	k1gNnSc1
češtiny	čeština	k1gFnSc2
jako	jako	k8xS,k8xC
cizího	cizí	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
183	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978-80-7308-555-1	978-80-7308-555-1	k4
(	(	kIx(
<g/>
print	print	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7308-634-3	978-80-7308-634-3	k4
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
pdf	pdf	k?
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Filosofie	filosofie	k1gFnSc1
jazyka	jazyk	k1gInSc2
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Řečové	řečový	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pragmatika	pragmatik	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Heslo	heslo	k1gNnSc1
Pragmatics	Pragmaticsa	k1gFnPc2
ve	v	k7c4
Stanford	Stanford	k1gInSc4
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
philosophy	philosoph	k1gInPc1
</s>
<s>
Liu	Liu	k?
<g/>
,	,	kIx,
Shaozhong	Shaozhong	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
What	What	k1gInSc1
is	is	k?
Pragmatics	Pragmatics	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
</s>
<s>
Dan	Dan	k1gMnSc1
Sperber	Sperber	k1gMnSc1
discusses	discusses	k1gMnSc1
Pragmatics	Pragmatics	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lingvistika	lingvistika	k1gFnSc1
Teoretická	teoretický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Fonologie	fonologie	k1gFnSc1
•	•	k?
Generativní	generativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Intersubjektivita	intersubjektivita	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Kvantitativní	kvantitativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Lexikologie	lexikologie	k1gFnSc2
•	•	k?
Morfologie	morfologie	k1gFnSc2
•	•	k?
Pragmatika	pragmatika	k1gFnSc1
•	•	k?
Sémantika	sémantika	k1gFnSc1
•	•	k?
Syntax	syntax	k1gFnSc1
Deskriptivní	deskriptivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Etymologie	etymologie	k1gFnSc1
•	•	k?
Fonetika	fonetika	k1gFnSc1
•	•	k?
Historická	historický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Komparativní	komparativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Sociolingvistika	sociolingvistika	k1gFnSc1
Aplikovaná	aplikovaný	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Forenzní	forenzní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Jazyková	jazykový	k2eAgFnSc1d1
akvizice	akvizice	k1gFnSc1
•	•	k?
Počítačová	počítačový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Korpusová	korpusový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Neurolingvistika	Neurolingvistika	k1gFnSc1
•	•	k?
Preskriptivní	Preskriptivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Psycholingvistika	psycholingvistika	k1gFnSc1
•	•	k?
Stylistika	stylistika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4076315-8	4076315-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6671	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85106058	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85106058	#num#	k4
</s>
