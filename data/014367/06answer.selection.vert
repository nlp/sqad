<s>
Pragmatika	pragmatika	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
pragmatická	pragmatický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
pragma	pragmum	k1gNnSc2
<g/>
,	,	kIx,
skutek	skutek	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
na	na	k7c6
pomezí	pomezí	k1gNnSc6
lingvistiky	lingvistika	k1gFnSc2
a	a	k8xC
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
řečovými	řečový	k2eAgInPc7d1
akty	akt	k1gInPc7
(	(	kIx(
<g/>
promluvami	promluva	k1gFnPc7
<g/>
,	,	kIx,
výpověďmi	výpověď	k1gFnPc7
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
účelnou	účelný	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
řeči	řeč	k1gFnSc2
<g/>
:	:	kIx,
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
něco	něco	k3yInSc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
obvykle	obvykle	k6eAd1
tím	ten	k3xDgNnSc7
sleduje	sledovat	k5eAaImIp3nS
i	i	k9
nějaký	nějaký	k3yIgInSc4
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
</s>