<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
navíc	navíc	k6eAd1	navíc
porodila	porodit	k5eAaPmAgFnS	porodit
sestra	sestra	k1gFnSc1	sestra
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
dítě	dítě	k1gNnSc1	dítě
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1744	[number]	k4	1744
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1745	[number]	k4	1745
přivedla	přivést	k5eAaPmAgFnS	přivést
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dalšího	další	k2eAgMnSc2d1	další
syna	syn	k1gMnSc2	syn
–	–	k?	–
Karla	Karel	k1gMnSc2	Karel
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
