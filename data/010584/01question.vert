<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
britský	britský	k2eAgMnSc1d1	britský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
kryptoanalytik	kryptoanalytik	k1gMnSc1	kryptoanalytik
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
?	?	kIx.	?
</s>
