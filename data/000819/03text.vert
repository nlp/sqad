<s>
Obec	obec	k1gFnSc1	obec
Bošín	Bošína	k1gFnPc2	Bošína
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Boschin	Boschin	k2eAgInSc1d1	Boschin
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Pardubický	pardubický	k2eAgInSc1d1	pardubický
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
91	[number]	k4	91
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1371	[number]	k4	1371
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Filomény	Filoména	k1gFnSc2	Filoména
Smírčí	smírčí	k2eAgInSc1d1	smírčí
kříž	kříž	k1gInSc1	kříž
Cyrilometodějský	cyrilometodějský	k2eAgInSc1d1	cyrilometodějský
Bošínská	Bošínský	k2eAgFnSc1d1	Bošínská
obora	obora	k1gFnSc1	obora
-	-	kIx~	-
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
bezprostředně	bezprostředně	k6eAd1	bezprostředně
navazující	navazující	k2eAgInPc1d1	navazující
na	na	k7c4	na
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bošín	Bošína	k1gFnPc2	Bošína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
