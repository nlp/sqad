<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
změny	změna	k1gFnSc2	změna
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
česká	český	k2eAgFnSc1d1	Česká
liberální	liberální	k2eAgFnSc1d1	liberální
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
sekretariát	sekretariát	k1gInSc4	sekretariát
vedl	vést	k5eAaImAgMnS	vést
Josef	Josef	k1gMnSc1	Josef
Brož	Brož	k1gMnSc1	Brož
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
iniciativy	iniciativa	k1gFnSc2	iniciativa
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
odejděte	odejít	k5eAaPmRp2nP	odejít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
před	před	k7c7	před
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
založením	založení	k1gNnSc7	založení
strany	strana	k1gFnSc2	strana
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c4	mezi
organizátory	organizátor	k1gInPc4	organizátor
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
odchod	odchod	k1gInSc4	odchod
lidí	člověk	k1gMnPc2	člověk
kolem	kolem	k7c2	kolem
Moniky	Monika	k1gFnSc2	Monika
Pajerové	Pajerová	k1gFnSc2	Pajerová
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
stranu	strana	k1gFnSc4	strana
Naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
neuspěla	uspět	k5eNaPmAgFnS	uspět
–	–	k?	–
získala	získat	k5eAaPmAgFnS	získat
0,27	[number]	k4	0,27
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Martin	Martin	k1gMnSc1	Martin
Mejstřík	Mejstřík	k1gMnSc1	Mejstřík
kandidující	kandidující	k2eAgInSc4d1	kandidující
za	za	k7c4	za
Cestu	cesta	k1gFnSc4	cesta
změny	změna	k1gFnSc2	změna
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
CZ	CZ	kA	CZ
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
5	[number]	k4	5
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
postavila	postavit	k5eAaPmAgFnS	postavit
CZ	CZ	kA	CZ
společnou	společný	k2eAgFnSc4d1	společná
kandidátku	kandidátka	k1gFnSc4	kandidátka
s	s	k7c7	s
US-DEU	US-DEU	k1gFnSc7	US-DEU
<g/>
,	,	kIx,	,
Liberální	liberální	k2eAgFnSc7d1	liberální
reformní	reformní	k2eAgFnSc7d1	reformní
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
ODA	ODA	kA	ODA
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Unie	unie	k1gFnSc2	unie
liberálních	liberální	k2eAgMnPc2d1	liberální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
CZ	CZ	kA	CZ
z	z	k7c2	z
32	[number]	k4	32
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
24	[number]	k4	24
poslaneckých	poslanecký	k2eAgNnPc2d1	poslanecké
míst	místo	k1gNnPc2	místo
nominovala	nominovat	k5eAaBmAgFnS	nominovat
6	[number]	k4	6
(	(	kIx(	(
<g/>
nestranice	nestranice	k1gFnSc1	nestranice
Tereza	Tereza	k1gFnSc1	Tereza
Brdečková	Brdečková	k1gFnSc1	Brdečková
na	na	k7c6	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
ULD	ULD	kA	ULD
získala	získat	k5eAaPmAgFnS	získat
1,69	[number]	k4	1,69
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
do	do	k7c2	do
EP	EP	kA	EP
se	se	k3xPyFc4	se
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
CZ	CZ	kA	CZ
nominovala	nominovat	k5eAaBmAgFnS	nominovat
2	[number]	k4	2
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
;	;	kIx,	;
neuspěl	uspět	k5eNaPmAgMnS	uspět
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ani	ani	k9	ani
Daniel	Daniela	k1gFnPc2	Daniela
Kroupa	Kroupa	k1gMnSc1	Kroupa
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
obhajující	obhajující	k2eAgInSc4d1	obhajující
mandát	mandát	k1gInSc4	mandát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
ODA	ODA	kA	ODA
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
CZ	CZ	kA	CZ
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
podporu	podpora	k1gFnSc4	podpora
několika	několik	k4yIc3	několik
dalším	další	k2eAgMnPc3d1	další
kandidátům	kandidát	k1gMnPc3	kandidát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
(	(	kIx(	(
<g/>
člen	člen	k1gMnSc1	člen
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
navrhující	navrhující	k2eAgFnSc1d1	navrhující
strana	strana	k1gFnSc1	strana
US-DEU	US-DEU	k1gFnSc2	US-DEU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
změny	změna	k1gFnSc2	změna
byla	být	k5eAaImAgFnS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Democratic	Democratice	k1gFnPc2	Democratice
Party	parta	k1gFnSc2	parta
-	-	kIx~	-
EDP	EDP	kA	EDP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
liberálně	liberálně	k6eAd1	liberálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
a	a	k8xC	a
reformní	reformní	k2eAgFnSc7d1	reformní
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
European	European	k1gMnSc1	European
Liberal	Liberal	k1gMnSc1	Liberal
Democrat	Democrat	k1gMnSc1	Democrat
and	and	k?	and
Reform	Reform	k1gInSc1	Reform
Party	party	k1gFnSc1	party
-	-	kIx~	-
ELDR	ELDR	kA	ELDR
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
Alianci	aliance	k1gFnSc4	aliance
liberálů	liberál	k1gMnPc2	liberál
a	a	k8xC	a
demokratů	demokrat	k1gMnPc2	demokrat
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
Alliance	Alliance	k1gFnSc1	Alliance
of	of	k?	of
Liberals	Liberals	k1gInSc1	Liberals
and	and	k?	and
Democrats	Democrats	k1gInSc1	Democrats
for	forum	k1gNnPc2	forum
Europe	Europ	k1gInSc5	Europ
-	-	kIx~	-
ALDE	ALDE	kA	ALDE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Republikový	republikový	k2eAgInSc1d1	republikový
sněm	sněm	k1gInSc1	sněm
strany	strana	k1gFnSc2	strana
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
dobrovolném	dobrovolný	k2eAgNnSc6d1	dobrovolné
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
s	s	k7c7	s
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
cestazmeny	cestazmen	k1gInPc1	cestazmen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
stránek	stránka	k1gFnPc2	stránka
<g/>
:	:	kIx,	:
24.8	[number]	k4	24.8
<g/>
.2006	.2006	k4	.2006
<g/>
a	a	k8xC	a
–	–	k?	–
archiv	archiv	k1gInSc1	archiv
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
na	na	k7c4	na
web.archive.org	web.archive.org	k1gInSc4	web.archive.org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
cestazmeny	cestazmen	k2eAgInPc4d1	cestazmen
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
archiv	archiv	k1gInSc1	archiv
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
na	na	k7c4	na
vjrott	vjrott	k1gInSc4	vjrott
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Analýza	analýza	k1gFnSc1	analýza
<g/>
:	:	kIx,	:
CESTA	cesta	k1gFnSc1	cesta
ZMĚNY	změna	k1gFnSc2	změna
na	na	k7c6	na
kruhovém	kruhový	k2eAgInSc6d1	kruhový
objezdu	objezd	k1gInSc6	objezd
–	–	k?	–
nepodepsaný	podepsaný	k2eNgInSc1d1	nepodepsaný
článek	článek	k1gInSc1	článek
v	v	k7c6	v
Britských	britský	k2eAgInPc6d1	britský
listech	list	k1gInPc6	list
z	z	k7c2	z
14.11	[number]	k4	14.11
<g/>
.2001	.2001	k4	.2001
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
alde	alde	k1gFnSc1	alde
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
–	–	k?	–
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
ALDE	ALDE	kA	ALDE
</s>
</p>
<p>
<s>
martinmejstrik	martinmejstrik	k1gMnSc1	martinmejstrik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
Martina	Martin	k1gMnSc2	Martin
Mejstříka	Mejstřík	k1gMnSc2	Mejstřík
</s>
</p>
<p>
<s>
karelschwarzenberg	karelschwarzenberg	k1gInSc1	karelschwarzenberg
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
</s>
</p>
