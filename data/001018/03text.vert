<s>
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Groß	Groß	k1gMnSc1	Groß
Raigern	Raigern	k1gMnSc1	Raigern
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
3500	[number]	k4	3500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnSc1d1	viniční
trať	trať	k1gFnSc1	trať
Hájiska	Hájiska	k1gFnSc1	Hájiska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
klášterního	klášterní	k2eAgInSc2d1	klášterní
areálu	areál	k1gInSc2	areál
dříve	dříve	k6eAd2	dříve
existovalo	existovat	k5eAaImAgNnS	existovat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
opevněné	opevněný	k2eAgNnSc1d1	opevněné
velkomoravské	velkomoravský	k2eAgNnSc1d1	velkomoravské
hradisko	hradisko	k1gNnSc1	hradisko
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
založeno	založit	k5eAaPmNgNnS	založit
okolo	okolo	k6eAd1	okolo
950	[number]	k4	950
<g/>
,	,	kIx,	,
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
nejpozději	pozdě	k6eAd3	pozdě
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1045	[number]	k4	1045
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
listinách	listina	k1gFnPc6	listina
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1234	[number]	k4	1234
získal	získat	k5eAaPmAgInS	získat
Rajhrad	Rajhrad	k1gInSc4	Rajhrad
trhové	trhový	k2eAgNnSc4d1	trhové
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
městečkem	městečko	k1gNnSc7	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
město	město	k1gNnSc4	město
byl	být	k5eAaImAgMnS	být
povýšen	povýšen	k2eAgMnSc1d1	povýšen
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
existuje	existovat	k5eAaImIp3nS	existovat
fungující	fungující	k2eAgInSc4d1	fungující
mužský	mužský	k2eAgInSc4d1	mužský
klášter	klášter	k1gInSc4	klášter
tvořící	tvořící	k2eAgNnSc1d1	tvořící
centrum	centrum	k1gNnSc1	centrum
Benediktinského	benediktinský	k2eAgNnSc2d1	benediktinské
opatství	opatství	k1gNnSc2	opatství
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
a	a	k8xC	a
ženský	ženský	k2eAgInSc1d1	ženský
klášter	klášter	k1gInSc1	klášter
sester	sestra	k1gFnPc2	sestra
Těšitelek	těšitelka	k1gFnPc2	těšitelka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
funguje	fungovat	k5eAaImIp3nS	fungovat
Dům	dům	k1gInSc1	dům
léčby	léčba	k1gFnSc2	léčba
bolesti	bolest	k1gFnSc2	bolest
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
hospic	hospic	k1gInSc1	hospic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
budov	budova	k1gFnPc2	budova
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
sídlí	sídlet	k5eAaImIp3nS	sídlet
Státní	státní	k2eAgInSc1d1	státní
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
a	a	k8xC	a
Památník	památník	k1gInSc1	památník
písemnictví	písemnictví	k1gNnSc2	písemnictví
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
Muzeum	muzeum	k1gNnSc4	muzeum
Brněnska	Brněnsko	k1gNnSc2	Brněnsko
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
rajhradské	rajhradský	k2eAgFnSc2d1	Rajhradská
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Aleji	alej	k1gFnSc6	alej
<g/>
,	,	kIx,	,
a	a	k8xC	a
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
rajhradského	rajhradský	k2eAgInSc2d1	rajhradský
kláštera	klášter	k1gInSc2	klášter
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
vedlejšími	vedlejší	k2eAgNnPc7d1	vedlejší
rameny	rameno	k1gNnPc7	rameno
Svratky	Svratka	k1gFnSc2	Svratka
nacházela	nacházet	k5eAaImAgFnS	nacházet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
malá	malý	k2eAgFnSc1d1	malá
obec	obec	k1gFnSc1	obec
Čeladice	Čeladice	k1gFnSc2	Čeladice
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
museli	muset	k5eAaImAgMnP	muset
chránit	chránit	k5eAaImF	chránit
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
velice	velice	k6eAd1	velice
malý	malý	k2eAgInSc4d1	malý
katastr	katastr	k1gInSc4	katastr
<g/>
,	,	kIx,	,
neměly	mít	k5eNaImAgFnP	mít
možnost	možnost	k1gFnSc4	možnost
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
nedostatek	nedostatek	k1gInSc4	nedostatek
polí	pole	k1gFnPc2	pole
k	k	k7c3	k
obživě	obživa	k1gFnSc3	obživa
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ničivé	ničivý	k2eAgFnSc6d1	ničivá
povodni	povodeň	k1gFnSc6	povodeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
usadili	usadit	k5eAaPmAgMnP	usadit
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
byly	být	k5eAaImAgFnP	být
Čeladice	Čeladice	k1gFnPc1	Čeladice
definitivně	definitivně	k6eAd1	definitivně
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
Rajhradu	Rajhrad	k1gInSc3	Rajhrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
fungují	fungovat	k5eAaImIp3nP	fungovat
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
uprostřed	uprostřed	k7c2	uprostřed
ulice	ulice	k1gFnSc2	ulice
Masarykova	Masarykův	k2eAgMnSc2d1	Masarykův
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
sídlišti	sídliště	k1gNnSc3	sídliště
Klášterní	klášterní	k2eAgInSc4d1	klášterní
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Horní	horní	k2eAgFnSc1d1	horní
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
v	v	k7c6	v
klidné	klidný	k2eAgFnSc6d1	klidná
čtvrti	čtvrt	k1gFnSc6	čtvrt
sídlišť	sídliště	k1gNnPc2	sídliště
a	a	k8xC	a
malých	malý	k2eAgInPc2d1	malý
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
s	s	k7c7	s
neoficiálním	oficiální	k2eNgInSc7d1	neoficiální
názvem	název	k1gInSc7	název
Matlaška	Matlašek	k1gInSc2	Matlašek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
také	také	k9	také
funguje	fungovat	k5eAaImIp3nS	fungovat
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zahradnická	zahradnická	k1gFnSc1	zahradnická
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
zvané	zvaný	k2eAgNnSc4d1	zvané
místními	místní	k2eAgInPc7d1	místní
Rafka	Rafko	k1gNnPc4	Rafko
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
klubu	klub	k1gInSc2	klub
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
<g/>
F.	F.	kA	F.
<g/>
K.	K.	kA	K.
Rajhradský	rajhradský	k2eAgInSc1d1	rajhradský
Atleticko-Fotbalový	atletickootbalový	k2eAgInSc1d1	atleticko-fotbalový
Klub	klub	k1gInSc1	klub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
RAFK	RAFK	kA	RAFK
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
ulici	ulice	k1gFnSc6	ulice
stojí	stát	k5eAaImIp3nS	stát
sokolovna	sokolovna	k1gFnSc1	sokolovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
místní	místní	k2eAgMnSc1d1	místní
Sokol	Sokol	k1gMnSc1	Sokol
vede	vést	k5eAaImIp3nS	vést
několik	několik	k4yIc4	několik
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
věrnou	věrný	k2eAgFnSc4d1	věrná
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejstarší	starý	k2eAgFnPc1d3	nejstarší
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
tennisový	tennisový	k2eAgInSc1d1	tennisový
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
volejbalu	volejbal	k1gInSc2	volejbal
<g/>
,	,	kIx,	,
šachový	šachový	k2eAgInSc1d1	šachový
oddíl	oddíl	k1gInSc1	oddíl
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
Moravská	moravský	k2eAgFnSc1d1	Moravská
šermířská	šermířský	k2eAgFnSc1d1	šermířská
skupina	skupina	k1gFnSc1	skupina
Asmodeus	Asmodeus	k1gInSc1	Asmodeus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
Šermířskou	šermířský	k2eAgFnSc4d1	šermířská
show	show	k1gFnSc4	show
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
bitva	bitva	k1gFnSc1	bitva
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
předchází	předcházet	k5eAaImIp3nP	předcházet
večerní	večerní	k2eAgInSc4d1	večerní
průvod	průvod	k1gInSc4	průvod
rakouských	rakouský	k2eAgNnPc2d1	rakouské
a	a	k8xC	a
švédských	švédský	k2eAgNnPc2d1	švédské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
historicky	historicky	k6eAd1	historicky
podloženého	podložený	k2eAgInSc2d1	podložený
Altblau	Altblaus	k1gInSc2	Altblaus
regimentu	regiment	k1gInSc2	regiment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgInPc2d2	menší
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
rybářský	rybářský	k2eAgInSc1d1	rybářský
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
skautky	skautka	k1gFnPc1	skautka
<g/>
,	,	kIx,	,
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
i	i	k9	i
vodácký	vodácký	k2eAgInSc1d1	vodácký
oddíl	oddíl	k1gInSc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
<g/>
.	.	kIx.	.
</s>
<s>
Benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
Památníkem	památník	k1gInSc7	památník
písemnictví	písemnictví	k1gNnSc2	písemnictví
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Kostel	kostel	k1gInSc4	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1765	[number]	k4	1765
<g/>
-	-	kIx~	-
<g/>
1766	[number]	k4	1766
Pitrův	Pitrův	k2eAgInSc1d1	Pitrův
most	most	k1gInSc1	most
-	-	kIx~	-
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Štěpán	Štěpán	k1gMnSc1	Štěpán
Adler	Adler	k1gMnSc1	Adler
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
odbojář	odbojář	k1gMnSc1	odbojář
Beda	Beda	k1gMnSc1	Beda
Dudík	Dudík	k1gMnSc1	Dudík
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
mnich	mnich	k1gMnSc1	mnich
Maurus	Maurus	k1gMnSc1	Maurus
Haberhauer	Haberhauer	k1gMnSc1	Haberhauer
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
-	-	kIx~	-
<g/>
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
mnich	mnich	k1gMnSc1	mnich
Karel	Karel	k1gMnSc1	Karel
Horálek	Horálek	k1gMnSc1	Horálek
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slavista	slavista	k1gMnSc1	slavista
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
korespondent	korespondent	k1gMnSc1	korespondent
ČSAV	ČSAV	kA	ČSAV
František	František	k1gMnSc1	František
Jedlička	Jedlička	k1gMnSc1	Jedlička
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Metoděj	Metoděj	k1gMnSc1	Metoděj
Antonín	Antonín	k1gMnSc1	Antonín
Jedlička	Jedlička	k1gMnSc1	Jedlička
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
Alois	Alois	k1gMnSc1	Alois
<g />
.	.	kIx.	.
</s>
<s>
Kotyza	Kotyza	k1gFnSc1	Kotyza
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
rajhradského	rajhradský	k2eAgInSc2d1	rajhradský
kláštera	klášter	k1gInSc2	klášter
Josef	Josef	k1gMnSc1	Josef
Bonaventura	Bonaventura	k1gFnSc1	Bonaventura
Piter	Piter	k1gMnSc1	Piter
(	(	kIx(	(
<g/>
1708	[number]	k4	1708
<g/>
-	-	kIx~	-
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
mnich	mnich	k1gMnSc1	mnich
Matouš	Matouš	k1gMnSc1	Matouš
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Sobek	Sobek	k1gMnSc1	Sobek
z	z	k7c2	z
Bílenberka	Bílenberka	k1gFnSc1	Bílenberka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Gregor	Gregor	k1gMnSc1	Gregor
Wolný	Wolný	k1gMnSc1	Wolný
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
mnich	mnich	k1gMnSc1	mnich
Josef	Josef	k1gMnSc1	Josef
Zelený	Zelený	k1gMnSc1	Zelený
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
RAFK	RAFK	kA	RAFK
Rajhrad	Rajhrad	k1gInSc4	Rajhrad
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rajhrad	Rajhrad	k1gInSc4	Rajhrad
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
SOŠ	SOŠ	kA	SOŠ
a	a	k8xC	a
SOU	sou	k1gInSc2	sou
Rajhrad	Rajhrad	k1gInSc4	Rajhrad
Dům	dům	k1gInSc1	dům
léčby	léčba	k1gFnSc2	léčba
bolesti	bolest	k1gFnSc2	bolest
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
-	-	kIx~	-
hospic	hospic	k1gInSc1	hospic
</s>
