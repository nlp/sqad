<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
další	další	k2eAgFnSc7d1	další
výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Václavem	Václav	k1gMnSc7	Václav
"	"	kIx"	"
<g/>
Vašem	váš	k3xOp2gInSc6	váš
<g/>
"	"	kIx"	"
Patejdlem	Patejdlo	k1gNnSc7	Patejdlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
tvořil	tvořit	k5eAaImAgInS	tvořit
s	s	k7c7	s
Rážem	Ráž	k1gMnSc7	Ráž
neodmyslitelnou	odmyslitelný	k2eNgFnSc4d1	neodmyslitelná
dvojici	dvojice	k1gFnSc4	dvojice
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1985	[number]	k4	1985
Patejdl	Patejdl	k1gFnSc2	Patejdl
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
soukromou	soukromý	k2eAgFnSc4d1	soukromá
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
o	o	k7c6	o
11	[number]	k4	11
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Elánu	elán	k1gInSc2	elán
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
