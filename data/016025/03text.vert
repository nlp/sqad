<s>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
</s>
<s>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
na	na	k7c4
na	na	k7c4
akci	akce	k1gFnSc4
Bahna	bahno	k1gNnSc2
2018	#num#	k4
<g/>
Typ	typa	k1gFnPc2
vozidla	vozidlo	k1gNnSc2
</s>
<s>
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Historie	historie	k1gFnSc1
Výrobce	výrobce	k1gMnSc2
</s>
<s>
ČKD	ČKD	kA
Návrh	návrh	k1gInSc1
</s>
<s>
1938	#num#	k4
Období	období	k1gNnSc1
výroby	výroba	k1gFnSc2
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
1	#num#	k4
414	#num#	k4
Základní	základní	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
Posádka	posádka	k1gFnSc1
</s>
<s>
4	#num#	k4
Délka	délka	k1gFnSc1
</s>
<s>
4,54	4,54	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
2,13	2,13	k4
m	m	kA
Výška	výška	k1gFnSc1
</s>
<s>
2,31	2,31	k4
m	m	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
9,7	9,7	k4
t	t	k?
Pancéřování	pancéřování	k1gNnSc4
a	a	k8xC
výzbroj	výzbroj	k1gFnSc4
Pancéřování	pancéřování	k1gNnSc2
</s>
<s>
15	#num#	k4
-	-	kIx~
20	#num#	k4
mm	mm	kA
Hlavní	hlavní	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
</s>
<s>
37	#num#	k4
<g/>
mmkanón	mmkanón	k1gMnSc1
Škoda	Škoda	k1gMnSc1
A7	A7	k1gMnSc1
Sekundární	sekundární	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
7,92	7,92	k4
<g/>
mmkulomet	mmkulomet	k1gInSc4
ZB-37	ZB-37	k1gFnSc2
Pohon	pohon	k1gInSc1
a	a	k8xC
pohyb	pohyb	k1gInSc1
Pohon	pohon	k1gInSc1
</s>
<s>
zážehový	zážehový	k2eAgInSc1d1
vodou	voda	k1gFnSc7
chlazený	chlazený	k2eAgInSc4d1
řadový	řadový	k2eAgInSc4d1
6	#num#	k4
<g/>
válec	válec	k1gInSc4
Praga	Prag	k1gMnSc4
EPA112	EPA112	k1gFnSc2
kW	kW	kA
(	(	kIx(
<g/>
150	#num#	k4
hp	hp	k?
<g/>
)	)	kIx)
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
rychlost	rychlost	k1gFnSc1
</s>
<s>
42	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
Poměr	poměr	k1gInSc1
výkon	výkon	k1gInSc1
<g/>
/	/	kIx~
<g/>
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
15,5	15,5	k4
hp	hp	k?
<g/>
/	/	kIx~
<g/>
tunu	tuna	k1gFnSc4
Dojezd	dojezd	k1gInSc1
</s>
<s>
200	#num#	k4
km	km	kA
</s>
<s>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
TNHP	TNHP	kA
<g/>
,	,	kIx,
německy	německy	k6eAd1
PzKpfw	PzKpfw	k1gFnPc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
či	či	k8xC
SdKfz	SdKfz	k1gInSc1
140	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
československé	československý	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
(	(	kIx(
<g/>
vyroben	vyrobit	k5eAaPmNgInS
společně	společně	k6eAd1
ČKD	ČKD	kA
a	a	k8xC
Škodou	škoda	k1gFnSc7
<g/>
)	)	kIx)
převzatý	převzatý	k2eAgInSc1d1
Wehrmachtem	wehrmacht	k1gInSc7
po	po	k7c4
obsazení	obsazení	k1gNnSc4
Československa	Československo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
a	a	k8xC
používaný	používaný	k2eAgInSc1d1
jím	on	k3xPp3gInSc7
během	běh	k1gInSc7
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
lehkých	lehký	k2eAgInPc2d1
tanků	tank	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
rychlému	rychlý	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
obrněné	obrněný	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
během	během	k7c2
válečných	válečný	k2eAgNnPc2d1
let	léto	k1gNnPc2
však	však	k9
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
zastaral	zastarat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německu	Německo	k1gNnSc6
se	se	k3xPyFc4
osvědčil	osvědčit	k5eAaPmAgInS
jak	jak	k6eAd1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
a	a	k8xC
na	na	k7c6
Balkáně	Balkán	k1gInSc6
na	na	k7c6
jaře	jaro	k1gNnSc6
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpád	vpád	k1gInSc1
do	do	k7c2
SSSR	SSSR	kA
v	v	k7c6
červnu	červen	k1gInSc6
1941	#num#	k4
a	a	k8xC
střety	střet	k1gInPc1
se	s	k7c7
stále	stále	k6eAd1
narůstajícími	narůstající	k2eAgInPc7d1
počty	počet	k1gInPc7
tanků	tank	k1gInPc2
T-34	T-34	k1gMnSc1
ale	ale	k9
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
lehký	lehký	k2eAgInSc1d1
tank	tank	k1gInSc1
má	mít	k5eAaImIp3nS
pro	pro	k7c4
rok	rok	k1gInSc4
1942	#num#	k4
jen	jen	k6eAd1
omezené	omezený	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
vyrobilo	vyrobit	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1939-1941	1939-1941	k4
několik	několik	k4yIc4
sérií	série	k1gFnPc2
s	s	k7c7
vylepšeným	vylepšený	k2eAgInSc7d1
pancířem	pancíř	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
výrobu	výroba	k1gFnSc4
tanku	tank	k1gInSc2
ukončilo	ukončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
bylo	být	k5eAaImAgNnS
nově	nově	k6eAd1
vyrobenými	vyrobený	k2eAgInPc7d1
tanky	tank	k1gInPc7
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
22	#num#	k4
<g/>
.	.	kIx.
tanková	tankový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
zničena	zničen	k2eAgFnSc1d1
u	u	k7c2
Stalingradu	Stalingrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
Wehrmacht	wehrmacht	k1gFnSc1
ztratil	ztratit	k5eAaPmAgInS
trvale	trvale	k6eAd1
429	#num#	k4
tanků	tank	k1gInPc2
PzKpwf	PzKpwf	k1gInSc1
<g/>
.38	.38	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
výrobu	výroba	k1gFnSc4
tanků	tank	k1gInPc2
ukončilo	ukončit	k5eAaPmAgNnS
<g/>
,	,	kIx,
ponechalo	ponechat	k5eAaPmAgNnS
ve	v	k7c6
výrobě	výroba	k1gFnSc6
však	však	k9
jeho	jeho	k3xOp3gFnSc4
korbu	korba	k1gFnSc4
a	a	k8xC
podvozek	podvozek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
základem	základ	k1gInSc7
pro	pro	k7c4
mnoho	mnoho	k4c4
odvozených	odvozený	k2eAgInPc2d1
a	a	k8xC
přestavovaných	přestavovaný	k2eAgInPc2d1
typů	typ	k1gInPc2
zbraní	zbraň	k1gFnPc2
–	–	k?
zejména	zejména	k9
samohybných	samohybný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
stíhačů	stíhač	k1gMnPc2
tanků	tank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
nasazení	nasazení	k1gNnSc1
</s>
<s>
Tank	tank	k1gInSc1
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
z	z	k7c2
tanku	tank	k1gInSc2
TNH	TNH	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
pokusná	pokusný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
ve	v	k7c6
vylepšených	vylepšený	k2eAgFnPc6d1
modifikacích	modifikace	k1gFnPc6
prodávala	prodávat	k5eAaImAgFnS
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
nového	nový	k2eAgInSc2d1
tanku	tank	k1gInSc2
bylo	být	k5eAaImAgNnS
nahradit	nahradit	k5eAaPmF
model	model	k1gInSc4
LT	LT	kA
vz.	vz.	k?
35	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
složitý	složitý	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
trpěl	trpět	k5eAaImAgMnS
značnou	značný	k2eAgFnSc7d1
poruchovostí	poruchovost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
nového	nový	k2eAgInSc2d1
lehkého	lehký	k2eAgInSc2d1
tanku	tank	k1gInSc2
byl	být	k5eAaImAgInS
zadán	zadat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
firma	firma	k1gFnSc1
Škoda	škoda	k1gFnSc1
nabídla	nabídnout	k5eAaPmAgFnS
vylepšenou	vylepšený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
typu	typ	k1gInSc2
LT	LT	kA
vz.	vz.	k?
35	#num#	k4
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
přesně	přesně	k6eAd1
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
armáda	armáda	k1gFnSc1
nechtěla	chtít	k5eNaImAgFnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
firma	firma	k1gFnSc1
ČKD	ČKD	kA
přišla	přijít	k5eAaPmAgFnS
s	s	k7c7
vývozním	vývozní	k2eAgInSc7d1
tankem	tank	k1gInSc7
TNH-S	TNH-S	k1gFnSc2
a	a	k8xC
po	po	k7c6
drobných	drobný	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
jej	on	k3xPp3gMnSc4
nabídla	nabídnout	k5eAaPmAgFnS
jako	jako	k8xS,k8xC
TNHP	TNHP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
armádní	armádní	k2eAgMnPc4d1
představitele	představitel	k1gMnPc4
udělal	udělat	k5eAaPmAgInS
tank	tank	k1gInSc1
TNHP	TNHP	kA
dobrý	dobrý	k2eAgInSc1d1
dojem	dojem	k1gInSc1
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
kosmetických	kosmetický	k2eAgFnPc6d1
změnách	změna	k1gFnPc6
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
do	do	k7c2
výzbroje	výzbroj	k1gFnSc2
pod	pod	k7c7
názvem	název	k1gInSc7
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
vzor	vzor	k1gInSc1
38	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
objednávka	objednávka	k1gFnSc1
činila	činit	k5eAaImAgFnS
150	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejímu	její	k3xOp3gNnSc3
předání	předání	k1gNnSc4
československé	československý	k2eAgFnSc2d1
armádě	armáda	k1gFnSc3
však	však	k8xC
nikdy	nikdy	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
si	se	k3xPyFc3
všech	všecek	k3xTgInPc2
150	#num#	k4
strojů	stroj	k1gInPc2
„	„	k?
<g/>
vyzvedli	vyzvednout	k5eAaPmAgMnP
<g/>
“	“	k?
přímo	přímo	k6eAd1
v	v	k7c6
závodech	závod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
okupaci	okupace	k1gFnSc6
byla	být	k5eAaImAgFnS
nejdříve	dříve	k6eAd3
výroba	výroba	k1gFnSc1
zastavena	zastaven	k2eAgFnSc1d1
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
ji	on	k3xPp3gFnSc4
ale	ale	k9
ČKD	ČKD	kA
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
přejmenované	přejmenovaný	k2eAgNnSc1d1
na	na	k7c6
BMM	BMM	kA
–	–	k?
Böhmisch-Mährische	Böhmisch-Mährische	k1gNnSc2
Maschinenfabrik	Maschinenfabrika	k1gFnPc2
AG	AG	kA
<g/>
)	)	kIx)
znovu	znovu	k6eAd1
rozběhly	rozběhnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
Německo	Německo	k1gNnSc1
projevilo	projevit	k5eAaPmAgNnS
o	o	k7c6
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
vysoce	vysoce	k6eAd1
kvalitní	kvalitní	k2eAgInSc4d1
tank	tank	k1gInSc4
veliký	veliký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
si	se	k3xPyFc3
objednalo	objednat	k5eAaPmAgNnS
dalších	další	k2eAgInPc2d1
1	#num#	k4
396	#num#	k4
tanků	tank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostalo	dostat	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
označení	označení	k1gNnSc2
Panzerkampfwagen	Panzerkampfwagen	k1gInSc4
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
resp.	resp.	kA
SdKfz	SdKfz	k1gInSc4
140	#num#	k4
a	a	k8xC
generálu	generál	k1gMnSc3
Guderianovi	Guderian	k1gMnSc3
pomohly	pomoct	k5eAaPmAgInP
v	v	k7c6
porážce	porážka	k1gFnSc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc2
<g/>
,	,	kIx,
Nizozemska	Nizozemsko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
se	se	k3xPyFc4
účastnily	účastnit	k5eAaImAgFnP
útoku	útok	k1gInSc3
na	na	k7c6
SSSR	SSSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroje	stroj	k1gInSc2
byly	být	k5eAaImAgFnP
též	též	k9
exportovány	exportovat	k5eAaBmNgFnP
do	do	k7c2
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
satelity	satelit	k1gInPc4
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
74	#num#	k4
kusů	kus	k1gInPc2
označovaných	označovaný	k2eAgInPc2d1
jako	jako	k8xS,k8xC
LT-	LT-	k1gFnSc7
<g/>
38	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
odebral	odebrat	k5eAaPmAgInS
samostatný	samostatný	k2eAgInSc1d1
Slovenský	slovenský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
tanky	tank	k1gInPc1
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
do	do	k7c2
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
Maďarska	Maďarsko	k1gNnSc2
atd.	atd.	kA
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
sloužilo	sloužit	k5eAaImAgNnS
31	#num#	k4
tanků	tank	k1gInPc2
pod	pod	k7c7
označením	označení	k1gNnSc7
LT	LT	kA
38	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
československé	československý	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Lt	Lt	k?
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
v	v	k7c6
slovenské	slovenský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
</s>
<s>
Jelikož	jelikož	k8xS
v	v	k7c6
období	období	k1gNnSc6
vzniku	vznik	k1gInSc2
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
se	se	k3xPyFc4
výrobu	výroba	k1gFnSc4
typu	typ	k1gInSc2
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
jen	jen	k9
rozjížděla	rozjíždět	k5eAaImAgFnS
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
vzniku	vznik	k1gInSc6
tvořily	tvořit	k5eAaImAgInP
základ	základ	k1gInSc4
slovenské	slovenský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
tanky	tank	k1gInPc1
typu	typ	k1gInSc2
LT-	LT-	k1gFnSc2
<g/>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
války	válka	k1gFnSc2
si	se	k3xPyFc3
Slovensko	Slovensko	k1gNnSc1
objednalo	objednat	k5eAaPmAgNnS
37	#num#	k4
tanků	tank	k1gInPc2
LT	LT	kA
vz	vz	k?
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
dostaly	dostat	k5eAaPmAgInP
v	v	k7c6
slovenské	slovenský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
označení	označení	k1gNnSc2
LT-	LT-	k1gFnSc2
<g/>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
tanky	tank	k1gInPc1
byly	být	k5eAaImAgFnP
vojensky	vojensky	k6eAd1
nasazeny	nasadit	k5eAaPmNgFnP
při	při	k7c6
invazi	invaze	k1gFnSc6
do	do	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1943	#num#	k4
až	až	k9
1944	#num#	k4
odkoupilo	odkoupit	k5eAaPmAgNnS
Slovensko	Slovensko	k1gNnSc1
dalších	další	k2eAgInPc2d1
37	#num#	k4
tanků	tank	k1gInPc2
vyřazených	vyřazený	k2eAgInPc2d1
z	z	k7c2
německé	německý	k2eAgFnSc2d1
výzbroje	výzbroj	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Tanky	tank	k1gInPc1
LT-38	LT-38	k1gFnSc2
sehrály	sehrát	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
v	v	k7c6
Slovenském	slovenský	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstalci	povstalec	k1gMnPc1
disponovali	disponovat	k5eAaBmAgMnP
13	#num#	k4
tanky	tank	k1gInPc4
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
tanků	tank	k1gInPc2
přišli	přijít	k5eAaPmAgMnP
v	v	k7c6
bojích	boj	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dochované	dochovaný	k2eAgInPc1d1
tanky	tank	k1gInPc1
</s>
<s>
Funkční	funkční	k2eAgInSc4d1
tank	tank	k1gInSc4
Lt	Lt	k1gFnSc2
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
inventáři	inventář	k1gInSc6
Vojenského	vojenský	k2eAgNnSc2d1
historického	historický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Lešany	Lešana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
opatřen	opatřit	k5eAaPmNgInS
československou	československý	k2eAgFnSc7d1
kamufláží	kamufláž	k1gFnSc7
a	a	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
vidět	vidět	k5eAaImF
pravidelně	pravidelně	k6eAd1
na	na	k7c6
akcích	akce	k1gFnPc6
věnovaných	věnovaný	k2eAgFnPc6d1
vojenské	vojenský	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
jsou	být	k5eAaImIp3nP
zachovány	zachovat	k5eAaPmNgInP
dva	dva	k4xCgInPc1
tanky	tank	k1gInPc1
Lt	Lt	k1gFnPc2
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompletní	kompletní	k2eAgInSc1d1
tank	tank	k1gInSc1
v	v	k7c6
kamufláži	kamufláž	k1gFnSc6
povstalecké	povstalecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
je	být	k5eAaImIp3nS
vystaven	vystavit	k5eAaPmNgInS
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
v	v	k7c6
Banské	banský	k2eAgFnSc6d1
Bystrici	Bystrica	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
k	k	k7c3
exponátem	exponát	k1gInSc7
Muzea	muzeum	k1gNnSc2
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
přibylo	přibýt	k5eAaPmAgNnS
torzo	torzo	k1gNnSc1
povstaleckého	povstalecký	k2eAgInSc2d1
tanku	tank	k1gInSc2
313	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
od	od	k7c2
války	válka	k1gFnSc2
chátral	chátrat	k5eAaImAgInS
opuštěný	opuštěný	k2eAgMnSc1d1
v	v	k7c6
lesích	les	k1gInPc6
nad	nad	k7c7
Istebným	Istebný	k2eAgNnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojízdný	pojízdný	k2eAgInSc1d1
tank	tank	k1gInSc1
Lt	Lt	k1gFnSc1
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
Vojenského	vojenský	k2eAgNnSc2d1
technického	technický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Lešanech	Lešan	k1gInPc6
</s>
<s>
Detail	detail	k1gInSc1
věže	věž	k1gFnSc2
tanku	tank	k1gInSc2
Lt	Lt	k1gFnSc2
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
Vojenského	vojenský	k2eAgNnSc2d1
technického	technický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Lešanech	Lešan	k1gInPc6
</s>
<s>
Tank	tank	k1gInSc1
Lt	Lt	k1gFnSc2
<g/>
.	.	kIx.
vz.	vz.	k?
38	#num#	k4
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
v	v	k7c6
Banské	banský	k2eAgFnSc6d1
Bystrici	Bystrica	k1gFnSc6
</s>
<s>
Německý	německý	k2eAgInSc1d1
Panzer	Panzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
Ausf	Ausf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
v	v	k7c6
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Munsteru	Munster	k1gInSc6
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Sériové	sériový	k2eAgInPc1d1
typy	typ	k1gInPc1
tanku	tank	k1gInSc2
</s>
<s>
Původní	původní	k2eAgFnSc1d1
(	(	kIx(
<g/>
lehká	lehký	k2eAgFnSc1d1
<g/>
)	)	kIx)
verze	verze	k1gFnSc1
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
A	A	kA
(	(	kIx(
<g/>
150	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
B	B	kA
(	(	kIx(
<g/>
110	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
C	C	kA
(	(	kIx(
<g/>
110	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
D	D	kA
(	(	kIx(
<g/>
105	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Těžké	těžký	k2eAgFnSc2d1
<g/>
“	“	k?
verze	verze	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
TNHP-S	TNHP-S	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Verze	verze	k1gFnSc1
s	s	k7c7
posíleným	posílený	k2eAgInSc7d1
pancířem	pancíř	k1gInSc7
(	(	kIx(
<g/>
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
E	E	kA
(	(	kIx(
<g/>
304	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
F	F	kA
(	(	kIx(
<g/>
250	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Pz-	Pz-	k?
<g/>
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
G	G	kA
(	(	kIx(
<g/>
306	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Významné	významný	k2eAgFnPc1d1
přestavby	přestavba	k1gFnPc1
tanku	tank	k1gInSc2
</s>
<s>
Prototyp	prototyp	k1gInSc1
československého	československý	k2eAgInSc2d1
tanku	tank	k1gInSc2
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
</s>
<s>
Aufklärungspanzer	Aufklärungspanzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
70	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Flakpanzer	Flakpanzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
též	též	k9
Gepard	gepard	k1gMnSc1
<g/>
,	,	kIx,
140	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Panzerbefehlswagen	Panzerbefehlswagen	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přestavba	přestavba	k1gFnSc1
na	na	k7c4
velitelský	velitelský	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
,	,	kIx,
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Bergerpanzer	Bergerpanzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
–	–	k?
vyprošťovací	vyprošťovací	k2eAgNnSc4d1
a	a	k8xC
servisní	servisní	k2eAgNnSc4d1
vozidlo	vozidlo	k1gNnSc4
</s>
<s>
Marder	Marder	k1gMnSc1
III	III	kA
(	(	kIx(
<g/>
stíhač	stíhač	k1gMnSc1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
940	#num#	k4
kusů	kus	k1gInPc2
s	s	k7c7
kanónem	kanón	k1gInSc7
7,5	7,5	k4
cm	cm	kA
+	+	kIx~
350	#num#	k4
s	s	k7c7
kanónem	kanón	k1gInSc7
76,2	76,2	k4
mm	mm	kA
<g/>
)	)	kIx)
</s>
<s>
Grille	Grille	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
Bison	Bison	k1gNnSc1
<g/>
,	,	kIx,
samohybné	samohybný	k2eAgNnSc1d1
dělo	dělo	k1gNnSc1
<g/>
,	,	kIx,
500	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Hetzer	Hetzer	k1gMnSc1
(	(	kIx(
<g/>
stíhač	stíhač	k1gMnSc1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
2	#num#	k4
600	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
FRANCEV	FRANCEV	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
KLIMENT	Kliment	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
K.	K.	kA
Praga	Praga	k1gFnSc1
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
MBI	MBI	kA
–	–	k?
Miroslav	Miroslav	k1gMnSc1
Bílý	bílý	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086524019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MIČIANIK	MIČIANIK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenská	slovenský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
ťažení	ťažení	k1gNnSc6
proti	proti	k7c3
Sovietskemu	Sovietskem	k1gInSc3
zväzu	zväz	k1gInSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
I.	I.	kA
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
:	:	kIx,
Dali-BB	Dali-BB	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
89090	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovenština	slovenština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
MINÁR	MINÁR	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
LT	LT	kA
38	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.utocna-vozba.estranky.cz	www.utocna-vozba.estranky.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
–	–	k?
blokované	blokovaný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
-	-	kIx~
PzKpfw	PzKpfw	k1gFnSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
|	|	kIx~
Fronta	fronta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.fronta.cz	www.fronta.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AKTUALITY	aktualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
SK	Sk	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aj	aj	kA
Nemcom	Nemcom	k1gInSc1
bolo	bola	k1gFnSc5
v	v	k7c6
povstaní	povstaný	k2eAgMnPc1d1
horúco	horúco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napríklad	Napríklad	k1gInSc1
vďaka	vďaka	k6eAd1
malým	malý	k2eAgInSc7d1
obrnencom	obrnencom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuality	aktualita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VRAŽDA	vražda	k1gFnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachránili	zachránit	k5eAaPmAgMnP
najlepší	najlepší	k2eAgInSc4d1
ľahký	ľahký	k2eAgInSc4d1
tank	tank	k1gInSc4
z	z	k7c2
druhej	druhat	k5eAaBmRp2nS,k5eAaImRp2nS,k5eAaPmRp2nS
svetovej	svetovat	k5eAaPmRp2nS,k5eAaImRp2nS
vojny	vojna	k1gFnSc2
<g/>
,	,	kIx,
bojoval	bojovat	k5eAaImAgMnS
za	za	k7c4
nacistov	nacistov	k1gInSc4
aj	aj	kA
za	za	k7c4
partizánov	partizánov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denník	denník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-04-26	2016-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Panzer	Panzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
modifikace	modifikace	k1gFnSc2
<g/>
,	,	kIx,
fotografie	fotografia	k1gFnSc2
a	a	k8xC
videa	video	k1gNnSc2
na	na	k7c4
www.panzernet.net	www.panzernet.net	k1gInSc4
</s>
<s>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
na	na	k7c4
www.fronta.cz	www.fronta.cz	k1gInSc4
</s>
<s>
Útočná	útočný	k2eAgFnSc1d1
vozba	vozba	k1gFnSc1
–	–	k?
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Německá	německý	k2eAgNnPc4d1
obrněná	obrněný	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
tanky	tank	k1gInPc1
</s>
<s>
Panzer	Panzer	k1gMnSc1
I	i	k9
•	•	k?
Panzer	Panzra	k1gFnPc2
II	II	kA
•	•	k?
Panzer	Panzer	k1gMnSc1
III	III	kA
•	•	k?
Panzer	Panzer	k1gMnSc1
IV	IV	kA
•	•	k?
Panther	Panthra	k1gFnPc2
•	•	k?
Tiger	Tiger	k1gMnSc1
I	i	k8xC
•	•	k?
Tiger	Tigero	k1gNnPc2
II	II	kA
•	•	k?
Panzer	Panzer	k1gInSc1
35	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
•	•	k?
Panzer	Panzer	k1gInSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
samohybné	samohybný	k2eAgFnPc1d1
houfnice	houfnice	k1gFnPc1
</s>
<s>
Sturmpanzer	Sturmpanzer	k1gMnSc1
II	II	kA
•	•	k?
Wespe	Wesp	k1gInSc5
•	•	k?
Hummel	Hummel	k1gInSc1
•	•	k?
Grille	Grille	k1gInSc1
•	•	k?
Panzerwerfer	Panzerwerfer	k1gInSc1
•	•	k?
sIG	sIG	k?
33	#num#	k4
•	•	k?
Wurfrahmen	Wurfrahmen	k1gInSc1
40	#num#	k4
•	•	k?
Karl-Gerät	Karl-Geräta	k1gFnPc2
útočná	útočný	k2eAgFnSc1d1
děla	dělo	k1gNnPc1
</s>
<s>
StuG	StuG	k?
III	III	kA
•	•	k?
StuG	StuG	k1gMnSc1
IV	IV	kA
•	•	k?
StuH	stuha	k1gFnPc2
42	#num#	k4
•	•	k?
Brummbär	Brummbär	k1gMnSc1
•	•	k?
Sturmtiger	Sturmtiger	k1gMnSc1
stíhače	stíhač	k1gMnSc2
tanků	tank	k1gInPc2
</s>
<s>
Panzerjäger	Panzerjäger	k1gMnSc1
I	i	k9
•	•	k?
Marder	Marder	k1gInSc1
I	i	k8xC
•	•	k?
Marder	Mardero	k1gNnPc2
II	II	kA
•	•	k?
Marder	Marder	k1gMnSc1
III	III	kA
•	•	k?
Hetzer	Hetzer	k1gMnSc1
•	•	k?
Jagdpanzer	Jagdpanzer	k1gMnSc1
IV	IV	kA
•	•	k?
Jagdpanther	Jagdpanthra	k1gFnPc2
•	•	k?
Nashorn	Nashorn	k1gMnSc1
•	•	k?
Jagdtiger	Jagdtiger	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
RSO	RSO	kA
polopásy	polopása	k1gFnSc2
</s>
<s>
SdKfz	SdKfz	k1gInSc1
2	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
250	#num#	k4
•	•	k?
251	#num#	k4
•	•	k?
252	#num#	k4
•	•	k?
253	#num#	k4
•	•	k?
254	#num#	k4
obrněné	obrněný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
</s>
<s>
SdKfz	SdKfz	k1gInSc1
221	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
SdKfz	SdKfz	k1gInSc1
231	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
ADGZ	ADGZ	kA
protiletadlové	protiletadlový	k2eAgInPc1d1
tanky	tank	k1gInPc1
</s>
<s>
Flakpanzer	Flakpanzer	k1gMnSc1
I	i	k9
<g/>
,	,	kIx,
Flakpanzer	Flakpanzer	k1gMnSc1
IV	IV	kA
<g/>
:	:	kIx,
Möbelwagen	Möbelwagen	k1gInSc1
<g/>
,	,	kIx,
Wirbelwind	Wirbelwind	k1gMnSc1
<g/>
,	,	kIx,
Ostwind	Ostwind	k1gMnSc1
<g/>
,	,	kIx,
Kugelblitz	Kugelblitz	k1gMnSc1
•	•	k?
Flakpanzer	Flakpanzer	k1gMnSc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
demoliční	demoliční	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
</s>
<s>
Leichter	Leichter	k1gMnSc1
Ladungsträger	Ladungsträger	k1gMnSc1
•	•	k?
Mittlerer	Mittlerer	k1gMnSc1
Ladungsträger	Ladungsträger	k1gMnSc1
•	•	k?
Schwerer	Schwerer	k1gMnSc1
Ladungsträger	Ladungsträger	k1gMnSc1
prototypy	prototyp	k1gInPc4
</s>
<s>
Maus	Maus	k6eAd1
•	•	k?
E-Serie	E-Serie	k1gFnSc1
•	•	k?
Panther	Panthra	k1gFnPc2
II	II	kA
•	•	k?
Waffenträger	Waffenträger	k1gMnSc1
•	•	k?
Neubaufahrzeug	Neubaufahrzeug	k1gMnSc1
•	•	k?
Sturer	Sturer	k1gMnSc1
Emil	Emil	k1gMnSc1
návrhy	návrh	k1gInPc7
</s>
<s>
Panzer	Panzer	k1gMnSc1
VII	VII	kA
Löwe	Löw	k1gFnPc1
•	•	k?
Panzer	Panzer	k1gInSc1
IX	IX	kA
<g/>
,	,	kIx,
Panzer	Panzer	k1gMnSc1
X	X	kA
•	•	k?
Ratte	Ratt	k1gInSc5
•	•	k?
Monster	monstrum	k1gNnPc2
•	•	k?
VK	VK	kA
1602	#num#	k4
Leopard	leopard	k1gMnSc1
</s>
<s>
Československá	československý	k2eAgNnPc1d1
meziválečná	meziválečný	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
Tanky	tank	k1gInPc4
</s>
<s>
KH-60	KH-60	k4
•	•	k?
Tančík	tančík	k1gInSc1
vz.	vz.	k?
33	#num#	k4
•	•	k?
LT	LT	kA
vz.	vz.	k?
34	#num#	k4
•	•	k?
LT	LT	kA
vz.	vz.	k?
35	#num#	k4
•	•	k?
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
Dělostřelecké	dělostřelecký	k2eAgInPc4d1
tahače	tahač	k1gInPc4
</s>
<s>
Praga	Praga	k1gFnSc1
T-3	T-3	k1gMnSc2
•	•	k?
Praga	Prag	k1gMnSc2
T-4	T-4	k1gMnSc2
•	•	k?
Praga	Prag	k1gMnSc2
T-6	T-6	k1gMnSc2
•	•	k?
Praga	Prag	k1gMnSc2
T-7	T-7	k1gMnSc2
•	•	k?
Praga	Prag	k1gMnSc2
T-8	T-8	k1gMnSc2
•	•	k?
Praga	Prag	k1gMnSc4
T-9	T-9	k1gFnSc2
•	•	k?
Škoda	škoda	k1gFnSc1
MTH	MTH	kA
Obrněné	obrněný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
</s>
<s>
OA	OA	kA
vz.	vz.	k?
23	#num#	k4
•	•	k?
OA	OA	kA
vz.	vz.	k?
27	#num#	k4
•	•	k?
OA	OA	kA
vz.	vz.	k?
30	#num#	k4
Exportní	exportní	k2eAgInSc4d1
vozidla	vozidlo	k1gNnPc1
</s>
<s>
AH-IV	AH-IV	k?
•	•	k?
Praga	Praga	k1gFnSc1
LTH	LTH	kA
•	•	k?
Praga	Praga	k1gFnSc1
LTP	LTP	kA
•	•	k?
Praga	Praga	k1gFnSc1
TNH	TNH	kA
•	•	k?
LT-40	LT-40	k1gFnSc1
Prototypy	prototyp	k1gInPc4
</s>
<s>
Škoda	Škoda	k1gMnSc1
Š-I-j	Š-I-j	k1gMnSc1
•	•	k?
Škoda	Škoda	k1gMnSc1
Š-II	Š-II	k1gMnSc1
•	•	k?
Škoda	Škoda	k1gMnSc1
Š-III	Š-III	k1gMnSc1
•	•	k?
Škoda	Škoda	k1gMnSc1
T-21	T-21	k1gMnSc1
•	•	k?
Praga	Praga	k1gFnSc1
V-8-H	V-8-H	k1gFnSc1
•	•	k?
Tatra	Tatra	k1gFnSc1
T-III	T-III	k1gFnSc1
</s>
