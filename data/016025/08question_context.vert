<s>
LT	LT	kA
vz.	vz.	k?
38	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
TNHP	TNHP	kA
<g/>
,	,	kIx,
německy	německy	k6eAd1
PzKpfw	PzKpfw	k1gFnPc1
38	#num#	k4
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
či	či	k8xC
SdKfz	SdKfz	k1gInSc1
140	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
československé	československý	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
(	(	kIx(
<g/>
vyroben	vyrobit	k5eAaPmNgInS
společně	společně	k6eAd1
ČKD	ČKD	kA
a	a	k8xC
Škodou	škoda	k1gFnSc7
<g/>
)	)	kIx)
převzatý	převzatý	k2eAgInSc1d1
Wehrmachtem	wehrmacht	k1gInSc7
po	po	k7c4
obsazení	obsazení	k1gNnSc4
Československa	Československo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
a	a	k8xC
používaný	používaný	k2eAgInSc1d1
jím	on	k3xPp3gInSc7
během	běh	k1gInSc7
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
lehkých	lehký	k2eAgInPc2d1
tanků	tank	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>