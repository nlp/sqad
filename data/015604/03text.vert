<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
ג	ג	k?
ח	ח	k?
<g/>
"	"	kIx"
<g/>
ן	ן	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
34	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
65	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc4
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Drom	Drom	k1gMnSc1
ha-Šaron	ha-Šaron	k1gMnSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1,3	1,3	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
373	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
286,9	286,9	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1933	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Židé	Žid	k1gMnPc1
z	z	k7c2
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ג	ג	k?
<g/>
ִ	ִ	k?
<g/>
ּ	ּ	k?
<g/>
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ע	ע	k?
<g/>
ַ	ַ	k?
<g/>
ת	ת	k?
ח	ח	k?
<g/>
ֵ	ֵ	k?
<g/>
"	"	kIx"
<g/>
ן	ן	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Giv	Giv	k1gFnSc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Hen	hena	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
typu	typ	k1gInSc2
mošav	mošava	k1gFnPc2
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Centrálním	centrální	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Drom	Drom	k1gMnSc1
ha-Šaron	ha-Šaron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
65	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
hustě	hustě	k6eAd1
osídlené	osídlený	k2eAgFnSc6d1
a	a	k8xC
zemědělsky	zemědělsky	k6eAd1
intenzivně	intenzivně	k6eAd1
využívané	využívaný	k2eAgFnSc6d1
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
<g/>
,	,	kIx,
respektive	respektive	k9
Šaronské	Šaronský	k2eAgFnSc3d1
planině	planina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
7	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
břehu	břeh	k1gInSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
cca	cca	kA
14	#num#	k4
kilometrů	kilometr	k1gInPc2
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
a	a	k8xC
cca	cca	kA
71	#num#	k4
kilometrů	kilometr	k1gInPc2
jihojihozápadně	jihojihozápadně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Haify	Haifa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
silně	silně	k6eAd1
urbanizované	urbanizovaný	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
plynule	plynule	k6eAd1
navazují	navazovat	k5eAaImIp3nP
okolní	okolní	k2eAgNnPc4d1
města	město	k1gNnPc4
Kfar	Kfar	k1gInSc1
Saba	Sab	k1gInSc2
<g/>
,	,	kIx,
Ra	ra	k0
<g/>
'	'	kIx"
<g/>
anana	anan	k1gMnSc4
<g/>
,	,	kIx,
Herzlija	Herzlijus	k1gMnSc4
a	a	k8xC
Hod	hod	k1gInSc4
ha-Šaron	ha-Šaron	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pak	pak	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
součást	součást	k1gFnSc4
aglomerace	aglomerace	k1gFnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
(	(	kIx(
<g/>
takzvaný	takzvaný	k2eAgMnSc1d1
Guš	Guš	k1gMnSc1
Dan	Dan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mošav	Mošav	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
jakousi	jakýsi	k3yIgFnSc4
enklávu	enkláva	k1gFnSc4
na	na	k7c6
pomezí	pomezí	k1gNnSc6
všech	všecek	k3xTgFnPc2
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
s	s	k7c7
torzovitě	torzovitě	k6eAd1
zachovanou	zachovaný	k2eAgFnSc7d1
zemědělskou	zemědělský	k2eAgFnSc7d1
krajinou	krajina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giv	Giv	k1gFnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc4
obývají	obývat	k5eAaImIp3nP
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
etnicky	etnicky	k6eAd1
převážně	převážně	k6eAd1
židovské	židovský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojen	napojen	k2eAgMnSc1d1
pomocí	pomocí	k7c2
místních	místní	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
okolní	okolní	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
název	název	k1gInSc1
je	být	k5eAaImIp3nS
akronymem	akronym	k1gInSc7
jména	jméno	k1gNnSc2
hebrejského	hebrejský	k2eAgMnSc4d1
básníka	básník	k1gMnSc4
Chajima	Chajim	k1gMnSc4
Nachmana	Nachman	k1gMnSc4
Bialika	Bialik	k1gMnSc4
-	-	kIx~
Chajim	Chajim	k1gMnSc1
Nachman	Nachman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladateli	zakladatel	k1gMnPc7
mošavu	mošava	k1gFnSc4
byla	být	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
Židů	Žid	k1gMnPc2
z	z	k7c2
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mošav	Mošav	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
širšího	široký	k2eAgInSc2d2
programu	program	k1gInSc2
Hitjašvut	Hitjašvut	k1gMnSc1
ha-Elef	ha-Elef	k1gMnSc1
(	(	kIx(
<g/>
ה	ה	k?
ה	ה	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
za	za	k7c4
cíl	cíl	k1gInSc4
urychlit	urychlit	k5eAaPmF
zřizování	zřizování	k1gNnSc4
menších	malý	k2eAgFnPc2d2
zemědělských	zemědělský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
pomohly	pomoct	k5eAaPmAgFnP
utvořit	utvořit	k5eAaPmF
územně	územně	k6eAd1
kompaktní	kompaktní	k2eAgInPc4d1
bloky	blok	k1gInPc4
židovského	židovský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
mandátní	mandátní	k2eAgFnSc6d1
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
usadilo	usadit	k5eAaPmAgNnS
41	#num#	k4
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
prvních	první	k4xOgNnPc2
osadníků	osadník	k1gMnPc2
ale	ale	k8xC
brzy	brzy	k6eAd1
vesnici	vesnice	k1gFnSc4
opustilo	opustit	k5eAaPmAgNnS
kvůli	kvůli	k7c3
těžkým	těžký	k2eAgInPc3d1
hospodářským	hospodářský	k2eAgInPc3d1
poměrům	poměr	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračoval	pokračovat	k5eAaImAgMnS
ale	ale	k9
příliv	příliv	k1gInSc4
nových	nový	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
židovských	židovský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Litvy	Litva	k1gFnSc2
a	a	k8xC
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
sem	sem	k6eAd1
dorazili	dorazit	k5eAaPmAgMnP
i	i	k9
Židé	Žid	k1gMnPc1
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
arabského	arabský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
byli	být	k5eAaImAgMnP
obyvatelé	obyvatel	k1gMnPc1
nuceni	nutit	k5eAaImNgMnP
svou	svůj	k3xOyFgFnSc4
vesnici	vesnice	k1gFnSc4
trvale	trvale	k6eAd1
hlídat	hlídat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
rokem	rok	k1gInSc7
1949	#num#	k4
měl	mít	k5eAaImAgMnS
Giv	Giv	k1gFnSc4
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gMnSc1
rozlohu	rozloha	k1gFnSc4
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
684	#num#	k4
dunamů	dunam	k1gInPc2
(	(	kIx(
<g/>
0,684	0,684	k4
kilometru	kilometr	k1gInSc2
čtverečního	čtvereční	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Správní	správní	k2eAgNnSc1d1
území	území	k1gNnSc1
obce	obec	k1gFnSc2
v	v	k7c6
současnosti	současnost	k1gFnSc6
dosahuje	dosahovat	k5eAaImIp3nS
1300	#num#	k4
dunamů	dunam	k1gInPc2
(	(	kIx(
<g/>
1,300	1,300	k4
kilometru	kilometr	k1gInSc2
čtverečního	čtvereční	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
zčásti	zčásti	k6eAd1
založena	založit	k5eAaPmNgFnS
na	na	k7c6
zemědělství	zemědělství	k1gNnSc6
(	(	kIx(
<g/>
pěstování	pěstování	k1gNnSc1
citrusů	citrus	k1gInPc2
a	a	k8xC
květin	květina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
Giv	Giv	k1gFnSc6
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chena	k1gFnPc2
Židé	Žid	k1gMnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
statistické	statistický	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
"	"	kIx"
<g/>
ostatní	ostatní	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
nearabské	arabský	k2eNgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
ale	ale	k8xC
bez	bez	k7c2
formální	formální	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
židovskému	židovský	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
menší	malý	k2eAgNnSc4d2
sídlo	sídlo	k1gNnSc4
vesnického	vesnický	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
dlouhodobě	dlouhodobě	k6eAd1
mírně	mírně	k6eAd1
rostoucí	rostoucí	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
373	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
populace	populace	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
o	o	k7c4
2,1	2,1	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Giv	Giv	k1gFnSc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1948	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
228256272256301320333336338346358379355362384389381373	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ג	ג	k?
ח	ח	k?
<g/>
"	"	kIx"
<g/>
ן	ן	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
drom-hasharon	drom-hasharon	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
<g/>
/	/	kIx~
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
י	י	k?
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
י	י	k?
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Localities	Localities	k1gMnSc1
of	of	k?
Eretz	Eretz	k1gMnSc1
Israel	Israel	k1gMnSc1
<g/>
:	:	kIx,
Towns	Towns	k1gInSc1
<g/>
,	,	kIx,
Kibbutzim	Kibbutzim	k1gInSc1
<g/>
,	,	kIx,
Moshavim	Moshavim	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Israel	Israel	k1gInSc1
Der	drát	k5eAaImRp2nS
Juden-Staat	Juden-Staat	k1gInSc1
<g/>
:	:	kIx,
Das	Das	k1gMnSc1
Jahr	Jahr	k1gMnSc1
Der	drát	k5eAaImRp2nS
Zionisten	Zionisten	k2eAgInSc1d1
<g/>
,	,	kIx,
Ullman-Verlag	Ullman-Verlag	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ר	ר	k?
ה	ה	k?
<g/>
,	,	kIx,
מ	מ	k?
ג	ג	k?
ו	ו	k?
1948,1961	1948,1961	k4
<g/>
,1972	,1972	k4
<g/>
,1983	,1983	k4
<g/>
,	,	kIx,
1995	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ש	ש	k?
י	י	k?
א	א	k?
a	a	k8xC
další	další	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
demografického	demografický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
sídel	sídlo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
mošav	mošav	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Oblastní	oblastní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Drom	Drom	k1gMnSc1
ha-Šaron	ha-Šaron	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Drom	Drom	k1gMnSc1
ha-Šaron	ha-Šaron	k1gMnSc1
Kibucy	kibuc	k1gInPc7
</s>
<s>
Ejal	Ejal	k1gInSc1
·	·	k?
Ejnat	Ejnat	k1gInSc1
·	·	k?
Giv	Giv	k1gFnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
ha-Šloša	ha-Šloša	k1gMnSc1
·	·	k?
Chorešim	Chorešim	k1gMnSc1
·	·	k?
Nachšonim	Nachšonim	k1gMnSc1
·	·	k?
Nir	Nir	k1gMnSc1
Elijahu	Elijah	k1gInSc2
·	·	k?
Ramat	Ramat	k1gInSc1
ha-Koveš	ha-Kovat	k5eAaPmIp2nS
Mošavy	Mošava	k1gFnPc4
</s>
<s>
Adanim	Adanim	k1gMnSc1
·	·	k?
Cofit	Cofit	k1gMnSc1
·	·	k?
Cur	Cur	k1gMnSc1
Natan	Natan	k1gMnSc1
·	·	k?
Elišama	Elišamum	k1gNnSc2
·	·	k?
Gan	Gan	k1gMnSc1
Chajim	Chajim	k1gMnSc1
·	·	k?
Ganej	Ganej	k1gInSc1
Am	Am	k1gMnSc1
·	·	k?
Gat	Gat	k1gMnSc1
Rimon	Rimon	k1gMnSc1
<g/>
·	·	k?
Giv	Giv	k1gMnSc1
<g/>
'	'	kIx"
<g/>
at	at	k?
Chen	Chen	k1gMnSc1
·	·	k?
Chagor	Chagor	k1gMnSc1
·	·	k?
Jarchiv	Jarchivo	k1gNnPc2
·	·	k?
Jarkona	Jarkon	k1gMnSc2
·	·	k?
Kfar	Kfar	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
as	as	k1gInSc1
·	·	k?
Kfar	Kfar	k1gInSc1
Malal	Malal	k1gMnSc1
·	·	k?
Kfar	Kfar	k1gMnSc1
Sirkin	Sirkin	k1gMnSc1
·	·	k?
Magšimim	Magšimim	k1gMnSc1
·	·	k?
Neve	Neve	k1gInSc1
Jamin	Jamin	k1gInSc1
·	·	k?
Neve	Neve	k1gInSc1
Jarak	Jarak	k1gMnSc1
·	·	k?
Sde	Sde	k1gMnSc1
Warburg	Warburg	k1gMnSc1
·	·	k?
Sdej	sdát	k5eAaPmRp2nS
Chemed	Chemed	k1gInSc4
Společné	společný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
</s>
<s>
Cur	Cur	k?
Jicchak	Jicchak	k1gMnSc1
·	·	k?
Matan	Matan	k1gMnSc1
·	·	k?
Nirit	Nirit	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
sídla	sídlo	k1gNnSc2
</s>
<s>
Bejt	Bejt	k?
Berl	Berl	k1gMnSc1
·	·	k?
Ramot	Ramot	k1gMnSc1
ha-Šavim	ha-Šavim	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
