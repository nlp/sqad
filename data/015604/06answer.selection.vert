<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
65	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
hustě	hustě	k6eAd1
osídlené	osídlený	k2eAgFnSc6d1
a	a	k8xC
zemědělsky	zemědělsky	k6eAd1
intenzivně	intenzivně	k6eAd1
využívané	využívaný	k2eAgFnSc6d1
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
<g/>
,	,	kIx,
respektive	respektive	k9
Šaronské	Šaronský	k2eAgFnSc3d1
planině	planina	k1gFnSc3
<g/>
.	.	kIx.
</s>