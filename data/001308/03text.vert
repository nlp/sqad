<s>
Germanium	germanium	k1gNnSc1	germanium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ge	Ge	k1gFnSc2	Ge
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Germanium	germanium	k1gNnSc4	germanium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
šedobílý	šedobílý	k2eAgInSc4d1	šedobílý
polokovový	polokovový	k2eAgInSc4d1	polokovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nalézající	nalézající	k2eAgNnSc4d1	nalézající
největší	veliký	k2eAgNnSc4d3	veliký
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
polovodičovém	polovodičový	k2eAgInSc6d1	polovodičový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
polokov	polokov	k1gInSc1	polokov
<g/>
,	,	kIx,	,
nalézající	nalézající	k2eAgMnSc1d1	nalézající
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Germanium	germanium	k1gNnSc1	germanium
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
sloučeniny	sloučenina	k1gFnPc4	sloučenina
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
<g/>
:	:	kIx,	:
Ge-	Ge-	k1gFnSc1	Ge-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Ge	Ge	k1gFnSc1	Ge
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
Ge	Ge	k1gMnSc1	Ge
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Clemens	Clemensa	k1gFnPc2	Clemensa
A.	A.	kA	A.
Winkler	Winkler	k1gMnSc1	Winkler
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jej	on	k3xPp3gMnSc4	on
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
předpovězena	předpovězet	k5eAaImNgFnS	předpovězet
tvůrcem	tvůrce	k1gMnSc7	tvůrce
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
ruským	ruský	k2eAgMnSc7d1	ruský
chemikem	chemik	k1gMnSc7	chemik
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
Ivanovičem	Ivanovič	k1gMnSc7	Ivanovič
Mendělejevem	Mendělejev	k1gInSc7	Mendělejev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
nazýval	nazývat	k5eAaImAgInS	nazývat
eka-silicium	ekailicium	k1gNnSc4	eka-silicium
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
určil	určit	k5eAaPmAgInS	určit
základní	základní	k2eAgFnPc4d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc4d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
tohoto	tento	k3xDgMnSc4	tento
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
neznámého	známý	k2eNgInSc2d1	neznámý
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
se	se	k3xPyFc4	se
germanium	germanium	k1gNnSc1	germanium
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
polovodič	polovodič	k1gInSc1	polovodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
tak	tak	k8xC	tak
v	v	k7c6	v
amorfní	amorfní	k2eAgFnSc6d1	amorfní
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
je	být	k5eAaImIp3nS	být
germanium	germanium	k1gNnSc1	germanium
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
např.	např.	kA	např.
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Germanium	germanium	k1gNnSc1	germanium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
značně	značně	k6eAd1	značně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,07	[number]	k4	0,07
mikrogramu	mikrogram	k1gInSc2	mikrogram
germania	germanium	k1gNnSc2	germanium
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
germania	germanium	k1gNnSc2	germanium
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
jako	jako	k8xC	jako
stopová	stopový	k2eAgFnSc1d1	stopová
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ložiscích	ložisko	k1gNnPc6	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
setkáme	setkat	k5eAaPmIp1nP	setkat
se	s	k7c7	s
směsným	směsný	k2eAgInSc7d1	směsný
sulfidem	sulfid	k1gInSc7	sulfid
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
germania	germanium	k1gNnSc2	germanium
argyroditem	argyrodit	k1gMnSc7	argyrodit
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
Ag	Ag	k1gFnSc2	Ag
<g/>
8	[number]	k4	8
<g/>
GeS	ges	k1gNnPc2	ges
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Germanium	germanium	k1gNnSc1	germanium
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
získává	získávat	k5eAaImIp3nS	získávat
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c4	po
zpracování	zpracování	k1gNnSc4	zpracování
zinkových	zinkový	k2eAgFnPc2d1	zinková
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
z	z	k7c2	z
popele	popel	k1gInSc2	popel
po	po	k7c6	po
spalování	spalování	k1gNnSc6	spalování
uhlí	uhlí	k1gNnSc2	uhlí
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyredukování	vyredukování	k1gNnSc6	vyredukování
kovu	kov	k1gInSc2	kov
s	s	k7c7	s
čistotou	čistota	k1gFnSc7	čistota
přibližně	přibližně	k6eAd1	přibližně
99	[number]	k4	99
%	%	kIx~	%
se	se	k3xPyFc4	se
germanium	germanium	k1gNnSc4	germanium
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
čistotě	čistota	k1gFnSc6	čistota
připravuje	připravovat	k5eAaImIp3nS	připravovat
metodou	metoda	k1gFnSc7	metoda
zonálního	zonální	k2eAgNnSc2d1	zonální
tavení	tavení	k1gNnSc2	tavení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
křemík	křemík	k1gInSc1	křemík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
získávání	získávání	k1gNnSc2	získávání
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgNnSc2d1	čisté
germania	germanium	k1gNnSc2	germanium
je	být	k5eAaImIp3nS	být
frakční	frakční	k2eAgFnSc1d1	frakční
destilace	destilace	k1gFnSc1	destilace
těkavého	těkavý	k2eAgInSc2d1	těkavý
chloridu	chlorid	k1gInSc2	chlorid
germaničitého	germaničitý	k2eAgInSc2d1	germaničitý
GeCl	GeCl	k1gInSc1	GeCl
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
germanium	germanium	k1gNnSc4	germanium
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
první	první	k4xOgInPc1	první
tranzistory	tranzistor	k1gInPc1	tranzistor
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
elektronické	elektronický	k2eAgFnPc1d1	elektronická
součástky	součástka	k1gFnPc1	součástka
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgNnSc2d1	čisté
germania	germanium	k1gNnSc2	germanium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
desetiletí	desetiletí	k1gNnPc2	desetiletí
bylo	být	k5eAaImAgNnS	být
germanium	germanium	k1gNnSc1	germanium
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
křemíkem	křemík	k1gInSc7	křemík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
v	v	k7c6	v
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
vyvinout	vyvinout	k5eAaPmF	vyvinout
postupy	postup	k1gInPc4	postup
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
čistotě	čistota	k1gFnSc6	čistota
minimálně	minimálně	k6eAd1	minimálně
99,999	[number]	k4	99,999
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Avšak	avšak	k8xC	avšak
germanium	germanium	k1gNnSc1	germanium
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
polovodičové	polovodičový	k2eAgFnPc4d1	polovodičová
diody	dioda	k1gFnPc4	dioda
I	i	k9	i
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
germanium	germanium	k1gNnSc1	germanium
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
polovodičů	polovodič	k1gInPc2	polovodič
jako	jako	k8xC	jako
germanid	germanid	k1gInSc1	germanid
křemíku	křemík	k1gInSc2	křemík
(	(	kIx(	(
<g/>
SiGe	SiGe	k1gInSc1	SiGe
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
přenosu	přenos	k1gInSc2	přenos
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
vlnění	vlnění	k1gNnSc4	vlnění
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
radarové	radarový	k2eAgFnSc6d1	radarová
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
tato	tento	k3xDgNnPc4	tento
použití	použití	k1gNnPc4	použití
poněkud	poněkud	k6eAd1	poněkud
klesá	klesat	k5eAaImIp3nS	klesat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
uplatnění	uplatnění	k1gNnSc1	uplatnění
má	mít	k5eAaImIp3nS	mít
germanium	germanium	k1gNnSc4	germanium
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
světlovodné	světlovodný	k2eAgFnSc2d1	světlovodná
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
i	i	k9	i
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
optických	optický	k2eAgFnPc2d1	optická
součástek	součástka	k1gFnPc2	součástka
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
čočky	čočka	k1gFnSc2	čočka
pro	pro	k7c4	pro
kamery	kamera	k1gFnPc4	kamera
s	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
úhlem	úhel	k1gInSc7	úhel
záběru	záběr	k1gInSc2	záběr
nebo	nebo	k8xC	nebo
optika	optika	k1gFnSc1	optika
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
signálu	signál	k1gInSc2	signál
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
přístrojích	přístroj	k1gInPc6	přístroj
pro	pro	k7c4	pro
noční	noční	k2eAgNnSc4d1	noční
vidění	vidění	k1gNnSc4	vidění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
světelný	světelný	k2eAgInSc1d1	světelný
lom	lom	k1gInSc1	lom
dodává	dodávat	k5eAaImIp3nS	dodávat
optickému	optický	k2eAgNnSc3d1	optické
sklu	sklo	k1gNnSc3	sklo
také	také	k9	také
oxid	oxid	k1gInSc4	oxid
germaničitý	germaničitý	k2eAgInSc4d1	germaničitý
GeO	GeO	k1gFnSc7	GeO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
polymerů	polymer	k1gInPc2	polymer
(	(	kIx(	(
<g/>
plastů	plast	k1gInPc2	plast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
nahradit	nahradit	k5eAaPmF	nahradit
titanem	titan	k1gInSc7	titan
<g/>
.	.	kIx.	.
</s>
<s>
Germaniové	germaniový	k2eAgInPc1d1	germaniový
generátory	generátor	k1gInPc1	generátor
mění	měnit	k5eAaImIp3nP	měnit
teplo	teplo	k1gNnSc4	teplo
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
germania	germanium	k1gNnSc2	germanium
mají	mít	k5eAaImIp3nP	mít
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
-	-	kIx~	-
slitina	slitina	k1gFnSc1	slitina
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klenotnická	klenotnický	k2eAgFnSc1d1	klenotnická
pájka	pájka	k1gFnSc1	pájka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
při	při	k7c6	při
chladnutí	chladnutí	k1gNnSc6	chladnutí
roztahuje	roztahovat	k5eAaImIp3nS	roztahovat
<g/>
,	,	kIx,	,
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
zlatem	zlato	k1gNnSc7	zlato
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Gadoliniovo-germaniové	Gadoliniovoermaniový	k2eAgFnPc1d1	Gadoliniovo-germaniový
granátoidy	granátoida	k1gFnPc1	granátoida
(	(	kIx(	(
<g/>
GGG	GGG	kA	GGG
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
laserové	laserový	k2eAgFnSc6d1	laserová
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
germanium	germanium	k1gNnSc4	germanium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
germanium	germanium	k1gNnSc4	germanium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
