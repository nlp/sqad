<p>
<s>
Petrobras	Petrobras	k1gInSc1	Petrobras
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
Petróleo	Petróleo	k6eAd1	Petróleo
Brasileiro	Brasileiro	k1gNnSc1	Brasileiro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
brazilská	brazilský	k2eAgFnSc1d1	brazilská
energetická	energetický	k2eAgFnSc1d1	energetická
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
korporace	korporace	k1gFnSc1	korporace
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
obratu	obrat	k1gInSc2	obrat
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
firmu	firma	k1gFnSc4	firma
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
úsilí	úsilí	k1gNnSc3	úsilí
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
brazilského	brazilský	k2eAgMnSc2d1	brazilský
prezidenta	prezident	k1gMnSc2	prezident
Getúlia	Getúlius	k1gMnSc2	Getúlius
Vargase	Vargasa	k1gFnSc6	Vargasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
sice	sice	k8xC	sice
firma	firma	k1gFnSc1	firma
ztratila	ztratit	k5eAaPmAgFnS	ztratit
zákonem	zákon	k1gInSc7	zákon
daný	daný	k2eAgInSc4d1	daný
monopol	monopol	k1gInSc4	monopol
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
denní	denní	k2eAgFnSc7d1	denní
produkcí	produkce	k1gFnSc7	produkce
2	[number]	k4	2
miliónů	milión	k4xCgInPc2	milión
barelů	barel	k1gInPc2	barel
denně	denně	k6eAd1	denně
významným	významný	k2eAgMnSc7d1	významný
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těžby	těžba	k1gFnSc2	těžba
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
také	také	k9	také
tankery	tanker	k1gInPc4	tanker
a	a	k8xC	a
rafinérie	rafinérie	k1gFnPc4	rafinérie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petrobras	Petrobrasa	k1gFnPc2	Petrobrasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
