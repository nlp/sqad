<p>
<s>
Shannon	Shannon	k1gNnSc1	Shannon
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Sionainn	Sionainn	k1gNnSc1	Sionainn
nebo	nebo	k8xC	nebo
Sionna	Sionna	k1gFnSc1	Sionna
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
River	River	k1gMnSc1	River
Shannon	Shannon	k1gMnSc1	Shannon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
územím	území	k1gNnSc7	území
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Leinster	Leinster	k1gMnSc1	Leinster
<g/>
,	,	kIx,	,
Connacht	Connacht	k1gMnSc1	Connacht
<g/>
,	,	kIx,	,
Munster	Munster	k1gMnSc1	Munster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
368	[number]	k4	368
km	km	kA	km
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
356	[number]	k4	356
km	km	kA	km
<g/>
)	)	kIx)	)
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
15	[number]	k4	15
700	[number]	k4	700
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
Centrální	centrální	k2eAgFnSc7d1	centrální
rovinou	rovina	k1gFnSc7	rovina
přes	přes	k7c4	přes
řetězec	řetězec	k1gInSc4	řetězec
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Loch	loch	k1gInSc1	loch
Allen	allen	k1gInSc1	allen
<g/>
,	,	kIx,	,
Loch	loch	k1gInSc1	loch
Ree	Rea	k1gFnSc3	Rea
<g/>
,	,	kIx,	,
Loch	loch	k1gInSc1	loch
Derg	Derg	k1gInSc1	Derg
(	(	kIx(	(
<g/>
Munster	Munster	k1gInSc1	Munster
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
estuár	estuár	k1gInSc1	estuár
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInPc1d1	vodní
stavy	stav	k1gInPc1	stav
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
nízké	nízký	k2eAgNnSc1d1	nízké
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
Limerick	Limericka	k1gFnPc2	Limericka
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
přílivu	příliv	k1gInSc6	příliv
dostupná	dostupný	k2eAgFnSc1d1	dostupná
i	i	k8xC	i
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnPc4d1	námořní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
kanály	kanál	k1gInPc7	kanál
Grand	grand	k1gMnSc1	grand
a	a	k8xC	a
Royal	Royal	k1gMnSc1	Royal
s	s	k7c7	s
Dublinem	Dublin	k1gInSc7	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
města	město	k1gNnSc2	město
Carrick	Carrick	k1gInSc4	Carrick
na	na	k7c4	na
Shannonu	Shannona	k1gFnSc4	Shannona
<g/>
,	,	kIx,	,
Athlone	Athlon	k1gInSc5	Athlon
<g/>
,	,	kIx,	,
Killaloe	Killaloe	k1gFnSc5	Killaloe
<g/>
,	,	kIx,	,
Limerick	Limerick	k1gMnSc1	Limerick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Ш	Ш	k?	Ш
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Shannon	Shannona	k1gFnPc2	Shannona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Shannon	Shannona	k1gFnPc2	Shannona
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
