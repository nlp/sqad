<s>
Shannon	Shannon	k1gNnSc1	Shannon
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Sionainn	Sionainn	k1gNnSc1	Sionainn
nebo	nebo	k8xC	nebo
Sionna	Sionna	k1gFnSc1	Sionna
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
River	River	k1gMnSc1	River
Shannon	Shannon	k1gMnSc1	Shannon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
územím	území	k1gNnSc7	území
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Leinster	Leinster	k1gMnSc1	Leinster
<g/>
,	,	kIx,	,
Connacht	Connacht	k1gMnSc1	Connacht
<g/>
,	,	kIx,	,
Munster	Munster	k1gMnSc1	Munster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
