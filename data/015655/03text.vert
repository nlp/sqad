<s>
Socket	Socket	k1gInSc1
775	#num#	k4
</s>
<s>
LGA	LGA	kA
775	#num#	k4
</s>
<s>
Specifikace	specifikace	k1gFnSc1
Typ	typ	k1gInSc1
patice	patice	k1gFnSc1
</s>
<s>
LGA	LGA	kA
Pouzdro	pouzdro	k1gNnSc1
</s>
<s>
Flip-chip	Flip-chip	k1gInSc1
pin	pin	k1gInSc1
grid	grida	k1gFnPc2
array	arraa	k1gFnSc2
Kontaktů	kontakt	k1gInPc2
</s>
<s>
775	#num#	k4
Sběrnice	sběrnice	k1gFnSc1
</s>
<s>
FSB	FSB	kA
(	(	kIx(
<g/>
AGTL	AGTL	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
Propustnost	propustnost	k1gFnSc1
</s>
<s>
133	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
533	#num#	k4
MT	MT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
200	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
800	#num#	k4
MT	MT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
266	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
1066	#num#	k4
MT	MT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
333	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
1333	#num#	k4
MT	MT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
400	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
1600	#num#	k4
MT	MT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
Procesory	procesor	k1gInPc1
</s>
<s>
Pentium	Pentium	kA
4	#num#	k4
(	(	kIx(
<g/>
2.6	2.6	k4
<g/>
—	—	k?
<g/>
3.8	3.8	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Celeron	Celeron	k1gMnSc1
D	D	kA
(	(	kIx(
<g/>
2.53	2.53	k4
<g/>
—	—	k?
<g/>
3.6	3.6	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pentium	Pentium	kA
4	#num#	k4
Extreme	Extrem	k1gInSc5
(	(	kIx(
<g/>
3.2	3.2	k4
<g/>
—	—	k?
<g/>
3.73	3.73	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pentium	Pentium	kA
D	D	kA
(	(	kIx(
<g/>
2.66	2.66	k4
<g/>
—	—	k?
<g/>
3.6	3.6	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pentium	Pentium	kA
Extreme	Extrem	k1gInSc5
(	(	kIx(
<g/>
3.2	3.2	k4
<g/>
—	—	k?
<g/>
3.73	3.73	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pentium	Pentium	kA
Dual-Core	Dual-Cor	k1gInSc5
(	(	kIx(
<g/>
1.4	1.4	k4
<g/>
—	—	k?
<g/>
3.33	3.33	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Core	Core	k6eAd1
2	#num#	k4
Duo	duo	k1gNnSc1
(	(	kIx(
<g/>
1.6	1.6	k4
<g/>
—	—	k?
<g/>
3.33	3.33	k4
GHz	GHz	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Core	Core	k6eAd1
2	#num#	k4
Extreme	Extrem	k1gInSc5
(	(	kIx(
<g/>
2.66	2.66	k4
<g/>
—	—	k?
<g/>
3.2	3.2	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Core	Core	k6eAd1
2	#num#	k4
Quad	Quada	k1gFnPc2
(	(	kIx(
<g/>
2.33	2.33	k4
<g/>
—	—	k?
<g/>
3	#num#	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Xeon	Xeon	k1gNnSc1
(	(	kIx(
<g/>
1.86	1.86	k4
<g/>
—	—	k?
<g/>
3.4	3.4	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Celeron	Celeron	k1gInSc1
(	(	kIx(
<g/>
1.6	1.6	k4
<g/>
—	—	k?
<g/>
2.4	2.4	k4
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Socket	Socket	k1gInSc4
478	#num#	k4
Nástupce	nástupce	k1gMnSc1
</s>
<s>
LGA	LGA	kA
1156	#num#	k4
a	a	k8xC
LGA	LGA	kA
1366	#num#	k4
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
LGA	LGA	kA
775	#num#	k4
(	(	kIx(
<g/>
socket	socket	k1gInSc1
T	T	kA
<g/>
,	,	kIx,
socket	socket	k1gInSc1
775	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
typ	typ	k1gInSc4
patice	patice	k1gFnSc2
procesoru	procesor	k1gInSc2
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
na	na	k7c6
řadě	řada	k1gFnSc6
po	po	k7c6
socketu	socket	k1gInSc6
478	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
inovací	inovace	k1gFnSc7
uchycení	uchycení	k1gNnSc2
procesoru	procesor	k1gInSc2
k	k	k7c3
patici	patice	k1gFnSc3
(	(	kIx(
<g/>
LGA	LGA	kA
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
Land	Landa	k1gFnPc2
Grid	Grido	k1gNnPc2
Array	Arra	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
podle	podle	k7c2
Intelu	Intel	k1gInSc2
je	být	k5eAaImIp3nS
důležitá	důležitý	k2eAgFnSc1d1
pro	pro	k7c4
vyšší	vysoký	k2eAgInPc4d2
nároky	nárok	k1gInPc4
napájení	napájení	k1gNnSc2
procesoru	procesor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
řadě	řada	k1gFnSc6
už	už	k6eAd1
na	na	k7c6
procesoru	procesor	k1gInSc6
nenajdete	najít	k5eNaPmIp2nP
piny	pin	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
kontaktní	kontaktní	k2eAgFnPc1d1
plošky	ploška	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piny	pin	k1gInPc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
tentokrát	tentokrát	k6eAd1
na	na	k7c4
patici	patice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
se	se	k3xPyFc4
příznivě	příznivě	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
ceně	cena	k1gFnSc6
procesorů	procesor	k1gInPc2
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
patici	patice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Výrazných	výrazný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
uchycení	uchycení	k1gNnSc3
chladičů	chladič	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
teď	teď	k6eAd1
upevněny	upevněn	k2eAgFnPc1d1
přímo	přímo	k6eAd1
k	k	k7c3
základní	základní	k2eAgFnSc3d1
desce	deska	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
má	mít	k5eAaImIp3nS
několik	několik	k4yIc4
výhod	výhoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
montáži	montáž	k1gFnSc6
lze	lze	k6eAd1
použít	použít	k5eAaPmF
menší	malý	k2eAgFnPc4d2
síly	síla	k1gFnPc4
a	a	k8xC
navíc	navíc	k6eAd1
použitím	použití	k1gNnSc7
„	„	k?
<g/>
boxovaných	boxovaný	k2eAgInPc2d1
<g/>
“	“	k?
chladičů	chladič	k1gInPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
chlazení	chlazení	k1gNnSc3
i	i	k9
okolních	okolní	k2eAgFnPc2d1
součástek	součástka	k1gFnPc2
na	na	k7c6
základní	základní	k2eAgFnSc6d1
desce	deska	k1gFnSc6
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
napájecí	napájecí	k2eAgInPc4d1
obvody	obvod	k1gInPc4
procesoru	procesor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
podpora	podpora	k1gFnSc1
DDR2	DDR2	k1gFnSc2
(	(	kIx(
<g/>
také	také	k9
DDR	DDR	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
možnost	možnost	k1gFnSc4
použít	použít	k5eAaPmF
vícejádrové	vícejádrové	k2eAgInPc7d1
procesory	procesor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporované	podporovaný	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
sběrnice	sběrnice	k1gFnSc2
jsou	být	k5eAaImIp3nP
533	#num#	k4
<g/>
,	,	kIx,
800	#num#	k4
<g/>
,	,	kIx,
1066	#num#	k4
<g/>
,	,	kIx,
1333	#num#	k4
<g/>
,	,	kIx,
1600	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Socket	Socket	k1gInSc1
775	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
