<p>
<s>
Emoce	emoce	k1gFnSc1	emoce
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat	lata	k1gFnPc2	lata
<g/>
.	.	kIx.	.
emovere	emovrat	k5eAaPmIp3nS	emovrat
<g/>
-	-	kIx~	-
vzrušovat	vzrušovat	k5eAaImF	vzrušovat
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
psychicky	psychicky	k6eAd1	psychicky
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
konstruované	konstruovaný	k2eAgInPc4d1	konstruovaný
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
subjektivní	subjektivní	k2eAgInPc4d1	subjektivní
zážitky	zážitek	k1gInPc4	zážitek
libosti	libost	k1gFnSc2	libost
a	a	k8xC	a
nelibosti	nelibost	k1gFnSc2	nelibost
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgInPc1d1	provázený
fyziologickými	fyziologický	k2eAgFnPc7d1	fyziologická
změnami	změna	k1gFnPc7	změna
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
dýchání	dýchání	k1gNnSc2	dýchání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
motorickými	motorický	k2eAgInPc7d1	motorický
projevy	projev	k1gInPc7	projev
(	(	kIx(	(
<g/>
mimika	mimika	k1gFnSc1	mimika
<g/>
,	,	kIx,	,
gestikulace	gestikulace	k1gFnSc1	gestikulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnami	změna	k1gFnPc7	změna
pohotovosti	pohotovost	k1gFnSc2	pohotovost
a	a	k8xC	a
zaměřenosti	zaměřenost	k1gFnSc2	zaměřenost
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
činností	činnost	k1gFnPc2	činnost
podle	podle	k7c2	podle
subjektivního	subjektivní	k2eAgInSc2d1	subjektivní
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
hodnocenému	hodnocený	k2eAgInSc3d1	hodnocený
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
zaujetí	zaujetí	k1gNnSc3	zaujetí
postoje	postoj	k1gInSc2	postoj
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
metaanalýzy	metaanalýza	k1gFnSc2	metaanalýza
však	však	k9	však
fyziologické	fyziologický	k2eAgFnPc1d1	fyziologická
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
dané	daný	k2eAgFnPc4d1	daná
emoce	emoce	k1gFnPc4	emoce
nejsou	být	k5eNaImIp3nP	být
jednotné	jednotný	k2eAgInPc1d1	jednotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sociologické	sociologický	k2eAgNnSc1d1	sociologické
pojetí	pojetí	k1gNnSc1	pojetí
emocí	emoce	k1gFnPc2	emoce
==	==	k?	==
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
předpokladem	předpoklad	k1gInSc7	předpoklad
sociologického	sociologický	k2eAgNnSc2d1	sociologické
pojetí	pojetí	k1gNnSc2	pojetí
emocí	emoce	k1gFnPc2	emoce
(	(	kIx(	(
<g/>
v	v	k7c4	v
angl	angl	k1gInSc4	angl
<g/>
.	.	kIx.	.
emotions	emotions	k1gInSc1	emotions
<g/>
,	,	kIx,	,
sentiment	sentiment	k1gInSc1	sentiment
<g/>
,	,	kIx,	,
feelings	feelings	k1gInSc1	feelings
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
sociální	sociální	k2eAgFnSc4d1	sociální
konstruovanost	konstruovanost	k1gFnSc4	konstruovanost
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
nejsou	být	k5eNaImIp3nP	být
prožívány	prožívat	k5eAaImNgFnP	prožívat
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
jedince	jedinec	k1gMnSc2	jedinec
samovolně	samovolně	k6eAd1	samovolně
<g/>
,	,	kIx,	,
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
vztaženy	vztažen	k2eAgFnPc1d1	vztažena
k	k	k7c3	k
určitým	určitý	k2eAgInPc3d1	určitý
objektům	objekt	k1gInPc3	objekt
<g/>
,	,	kIx,	,
jevům	jev	k1gInPc3	jev
nebo	nebo	k8xC	nebo
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Sociologická	sociologický	k2eAgFnSc1d1	sociologická
konceptualizace	konceptualizace	k1gFnSc1	konceptualizace
tedy	tedy	k9	tedy
neredukuje	redukovat	k5eNaBmIp3nS	redukovat
emoce	emoce	k1gFnSc1	emoce
na	na	k7c4	na
následky	následek	k1gInPc4	následek
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
a	a	k8xC	a
psychologických	psychologický	k2eAgInPc2d1	psychologický
procesů	proces	k1gInPc2	proces
či	či	k8xC	či
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnSc1	emoce
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
formulace	formulace	k1gFnSc1	formulace
afektu	afekt	k1gInSc2	afekt
(	(	kIx(	(
<g/>
v	v	k7c4	v
angl	angl	k1gInSc4	angl
<g/>
.	.	kIx.	.
affects	affects	k6eAd1	affects
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
lidé	člověk	k1gMnPc1	člověk
učí	učit	k5eAaImIp3nP	učit
během	během	k7c2	během
socializace	socializace	k1gFnSc2	socializace
reflektovat	reflektovat	k5eAaImF	reflektovat
a	a	k8xC	a
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
identifikovat	identifikovat	k5eAaBmF	identifikovat
a	a	k8xC	a
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
psychologický	psychologický	k2eAgInSc4d1	psychologický
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
afekt	afekt	k1gInSc1	afekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
pochopitelný	pochopitelný	k2eAgInSc1d1	pochopitelný
jak	jak	k8xS	jak
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
zároveň	zároveň	k6eAd1	zároveň
internalizují	internalizovat	k5eAaImIp3nP	internalizovat
určité	určitý	k2eAgFnPc4d1	určitá
kulturní	kulturní	k2eAgFnPc4d1	kulturní
normy	norma	k1gFnPc4	norma
a	a	k8xC	a
návody	návod	k1gInPc4	návod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgMnPc4d1	vhodný
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
typizovaných	typizovaný	k2eAgFnPc6d1	typizovaná
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
cítit	cítit	k5eAaImF	cítit
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
smutek	smutek	k1gInSc1	smutek
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
lidských	lidský	k2eAgFnPc2d1	lidská
emocí	emoce	k1gFnPc2	emoce
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
symbolickým	symbolický	k2eAgInSc7d1	symbolický
interakcionismem	interakcionismus	k1gInSc7	interakcionismus
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
aktér	aktér	k1gMnSc1	aktér
či	či	k8xC	či
aktéři	aktér	k1gMnPc1	aktér
danou	daný	k2eAgFnSc4d1	daná
situaci	situace	k1gFnSc4	situace
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nějaká	nějaký	k3yIgFnSc1	nějaký
emoce	emoce	k1gFnSc1	emoce
či	či	k8xC	či
psychický	psychický	k2eAgInSc1d1	psychický
stav	stav	k1gInSc1	stav
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
strach	strach	k1gInSc1	strach
či	či	k8xC	či
stud	stud	k1gInSc1	stud
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
fyziologicky	fyziologicky	k6eAd1	fyziologicky
stimulován	stimulován	k2eAgInSc1d1	stimulován
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
aktéry	aktér	k1gMnPc4	aktér
dané	daný	k2eAgFnSc2d1	daná
situace	situace	k1gFnSc2	situace
studem	stud	k1gInSc7	stud
či	či	k8xC	či
strachem	strach	k1gInSc7	strach
a	a	k8xC	a
aktéři	aktér	k1gMnPc1	aktér
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
jednají	jednat	k5eAaImIp3nP	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cítíme	cítit	k5eAaImIp1nP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
porušuje	porušovat	k5eAaImIp3nS	porušovat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
sociální	sociální	k2eAgFnSc1d1	sociální
norma	norma	k1gFnSc1	norma
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
jednání	jednání	k1gNnSc4	jednání
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgMnS	mít
by	by	k9	by
ses	ses	k?	ses
stydět	stydět	k5eAaImF	stydět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nP	muset
aktéři	aktér	k1gMnPc1	aktér
situace	situace	k1gFnSc2	situace
stud	stud	k1gInSc1	stud
pociťovat	pociťovat	k5eAaImF	pociťovat
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dostaví	dostavit	k5eAaPmIp3nS	dostavit
až	až	k9	až
ex	ex	k6eAd1	ex
post	post	k1gInSc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Popřípadě	popřípadě	k6eAd1	popřípadě
<g/>
,	,	kIx,	,
když	když	k8xS	když
cítíme	cítit	k5eAaImIp1nP	cítit
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
pocit	pocit	k1gInSc4	pocit
ohledně	ohledně	k7c2	ohledně
potenciálně	potenciálně	k6eAd1	potenciálně
nepředvídatelné	předvídatelný	k2eNgFnSc2d1	nepředvídatelná
a	a	k8xC	a
náročné	náročný	k2eAgFnSc2d1	náročná
budoucí	budoucí	k2eAgFnSc2d1	budoucí
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nám	my	k3xPp1nPc3	my
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
sděleno	sdělen	k2eAgNnSc1d1	sděleno
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Státnic	státnice	k1gFnPc2	státnice
se	se	k3xPyFc4	se
bát	bát	k5eAaImF	bát
nemusíš	muset	k5eNaImIp2nS	muset
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pěkné	pěkný	k2eAgNnSc1d1	pěkné
povídání	povídání	k1gNnSc1	povídání
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc1	pocit
identifikován	identifikován	k2eAgInSc1d1	identifikován
jako	jako	k8xS	jako
strach	strach	k1gInSc1	strach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tedy	tedy	k9	tedy
pojmenování	pojmenování	k1gNnSc1	pojmenování
a	a	k8xC	a
identifikace	identifikace	k1gFnSc1	identifikace
emoce	emoce	k1gFnSc2	emoce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
statického	statický	k2eAgMnSc2d1	statický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neustálým	neustálý	k2eAgInSc7d1	neustálý
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
proces	proces	k1gInSc1	proces
definice	definice	k1gFnSc2	definice
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
také	také	k9	také
není	být	k5eNaImIp3nS	být
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
reflexe	reflexe	k1gFnPc1	reflexe
vlastního	vlastní	k2eAgInSc2d1	vlastní
strachu	strach	k1gInSc2	strach
může	moct	k5eAaImIp3nS	moct
vézt	vézt	k5eAaImF	vézt
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
strach	strach	k1gInSc1	strach
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
subjektu	subjekt	k1gInSc2	subjekt
jako	jako	k8xC	jako
iracionální	iracionální	k2eAgNnPc1d1	iracionální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
význam	význam	k1gInSc1	význam
prvotního	prvotní	k2eAgInSc2d1	prvotní
fyziologického	fyziologický	k2eAgInSc2d1	fyziologický
stimulu	stimul	k1gInSc2	stimul
či	či	k8xC	či
impulsu	impuls	k1gInSc2	impuls
nelze	lze	k6eNd1	lze
opomíjet	opomíjet	k5eAaImF	opomíjet
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
či	či	k8xC	či
pojmenování	pojmenování	k1gNnSc4	pojmenování
emoce	emoce	k1gFnSc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
bez	bez	k7c2	bez
určitého	určitý	k2eAgInSc2d1	určitý
biologického	biologický	k2eAgInSc2d1	biologický
impulsu	impuls	k1gInSc2	impuls
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
<g/>
"	"	kIx"	"
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
naučené	naučený	k2eAgFnSc2d1	naučená
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
schopnosti	schopnost	k1gFnSc2	schopnost
definovat	definovat	k5eAaBmF	definovat
a	a	k8xC	a
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
by	by	kYmCp3nP	by
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
žádné	žádný	k3yNgFnPc1	žádný
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
jako	jako	k9	jako
emoce	emoce	k1gFnPc1	emoce
vnímány	vnímán	k2eAgFnPc1d1	vnímána
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
vidíme	vidět	k5eAaImIp1nP	vidět
lva	lev	k1gMnSc4	lev
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
může	moct	k5eAaImIp3nS	moct
cítit	cítit	k5eAaImF	cítit
jisté	jistý	k2eAgNnSc1d1	jisté
zneklidnění	zneklidnění	k1gNnSc1	zneklidnění
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
ale	ale	k9	ale
neměli	mít	k5eNaImAgMnP	mít
sociálně	sociálně	k6eAd1	sociálně
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
naučeno	naučen	k2eAgNnSc1d1	naučeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
potenciálně	potenciálně	k6eAd1	potenciálně
nebezpečného	bezpečný	k2eNgMnSc4d1	nebezpečný
tvora	tvor	k1gMnSc4	tvor
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
by	by	kYmCp3nP	by
nemuseli	muset	k5eNaImAgMnP	muset
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
provézt	provézt	k5eAaPmF	provézt
identifikaci	identifikace	k1gFnSc3	identifikace
a	a	k8xC	a
"	"	kIx"	"
<g/>
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
"	"	kIx"	"
emoce	emoce	k1gFnPc1	emoce
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
zažívat	zažívat	k5eAaImF	zažívat
"	"	kIx"	"
<g/>
strach	strach	k1gInSc1	strach
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
o	o	k7c4	o
sociologii	sociologie	k1gFnSc4	sociologie
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
společenský	společenský	k2eAgInSc1d1	společenský
řád	řád	k1gInSc1	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
autorek	autorka	k1gFnPc2	autorka
představují	představovat	k5eAaImIp3nP	představovat
emoce	emoce	k1gFnPc1	emoce
ať	ať	k9	ať
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
zásadní	zásadní	k2eAgInSc4d1	zásadní
způsob	způsob	k1gInSc4	způsob
a	a	k8xC	a
prostředek	prostředek	k1gInSc4	prostředek
dosahování	dosahování	k1gNnSc2	dosahování
sociální	sociální	k2eAgFnSc2d1	sociální
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
sociálního	sociální	k2eAgInSc2d1	sociální
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klasických	klasický	k2eAgMnPc2d1	klasický
autorů	autor	k1gMnPc2	autor
Émile	Émile	k1gFnSc2	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
určité	určitý	k2eAgFnPc1d1	určitá
emoce	emoce	k1gFnPc1	emoce
(	(	kIx(	(
<g/>
úcta	úcta	k1gFnSc1	úcta
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
povinnosti	povinnost	k1gFnSc2	povinnost
či	či	k8xC	či
závazku	závazek	k1gInSc2	závazek
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
stabilnější	stabilní	k2eAgNnPc1d2	stabilnější
sociální	sociální	k2eAgNnPc1d1	sociální
pouta	pouto	k1gNnPc1	pouto
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
fungování	fungování	k1gNnSc4	fungování
společnosti	společnost	k1gFnSc2	společnost
jako	jako	k8xC	jako
koherentního	koherentní	k2eAgInSc2d1	koherentní
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Randalla	Randall	k1gMnSc4	Randall
Collinse	Collins	k1gMnSc4	Collins
představují	představovat	k5eAaImIp3nP	představovat
emoce	emoce	k1gFnPc1	emoce
a	a	k8xC	a
sdílení	sdílení	k1gNnSc1	sdílení
nálad	nálada	k1gFnPc2	nálada
mezi	mezi	k7c7	mezi
aktéry	aktér	k1gMnPc7	aktér
komunikace	komunikace	k1gFnSc2	komunikace
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
interakčního	interakční	k2eAgInSc2d1	interakční
rituálu	rituál	k1gInSc2	rituál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
komplexní	komplexní	k2eAgFnSc4d1	komplexní
jednotku	jednotka	k1gFnSc4	jednotka
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
a	a	k8xC	a
popisující	popisující	k2eAgNnSc1d1	popisující
fungování	fungování	k1gNnSc1	fungování
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Susan	Susan	k1gInSc1	Susan
Shott	Shott	k1gInSc1	Shott
emoce	emoce	k1gFnSc2	emoce
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
rolové	rolový	k2eAgMnPc4d1	rolový
(	(	kIx(	(
<g/>
role-taking	roleaking	k1gInSc1	role-taking
feelings	feelings	k1gInSc1	feelings
<g/>
)	)	kIx)	)
a	a	k8xC	a
nerolové	rolový	k2eNgMnPc4d1	rolový
–	–	k?	–
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
smutek	smutek	k1gInSc1	smutek
<g/>
,	,	kIx,	,
vztek	vztek	k1gInSc1	vztek
(	(	kIx(	(
<g/>
non-role	nonole	k1gFnSc1	non-role
taking	taking	k1gInSc1	taking
feelings	feelings	k1gInSc1	feelings
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
konstitutivního	konstitutivní	k2eAgNnSc2d1	konstitutivní
pro	pro	k7c4	pro
společenské	společenský	k2eAgFnPc4d1	společenská
vazby	vazba	k1gFnPc4	vazba
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
emoce	emoce	k1gFnSc2	emoce
rolové	rolový	k2eAgFnSc2d1	rolová
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
také	také	k9	také
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
reflexivní	reflexivní	k2eAgInSc4d1	reflexivní
(	(	kIx(	(
<g/>
vina	vina	k1gFnSc1	vina
<g/>
,	,	kIx,	,
stud	stud	k1gInSc1	stud
<g/>
,	,	kIx,	,
pýcha	pýcha	k1gFnSc1	pýcha
<g/>
,	,	kIx,	,
hrdost	hrdost	k1gFnSc1	hrdost
<g/>
,	,	kIx,	,
ješitnost	ješitnost	k1gFnSc1	ješitnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
empatické	empatický	k2eAgFnPc4d1	empatická
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vcítíme	vcítit	k5eAaPmIp1nP	vcítit
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Rolové	rolový	k2eAgFnPc1d1	rolová
emoce	emoce	k1gFnPc1	emoce
mají	mít	k5eAaImIp3nP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
morální	morální	k2eAgInSc4d1	morální
a	a	k8xC	a
normativní	normativní	k2eAgInSc4d1	normativní
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
jedince	jedinec	k1gMnPc4	jedinec
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
jednání	jednání	k1gNnSc3	jednání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
sankce	sankce	k1gFnPc1	sankce
pro	pro	k7c4	pro
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
považovaným	považovaný	k2eAgInSc7d1	považovaný
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
emoce	emoce	k1gFnPc1	emoce
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
či	či	k8xC	či
nepřímé	přímý	k2eNgFnSc6d1	nepřímá
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
aktéry	aktér	k1gMnPc7	aktér
<g/>
,	,	kIx,	,
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
samotný	samotný	k2eAgMnSc1d1	samotný
aktér	aktér	k1gMnSc1	aktér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
emoci	emoce	k1gFnSc4	emoce
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
tak	tak	k6eAd1	tak
sebe-kontrolu	sebeontrola	k1gFnSc4	sebe-kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
výkonu	výkon	k1gInSc2	výkon
"	"	kIx"	"
<g/>
neviditelné	viditelný	k2eNgFnSc3d1	neviditelná
<g/>
"	"	kIx"	"
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
Foucault	Foucault	k2eAgMnSc1d1	Foucault
<g/>
,	,	kIx,	,
když	když	k8xS	když
využívá	využívat	k5eAaImIp3nS	využívat
Benthamův	Benthamův	k2eAgInSc4d1	Benthamův
popis	popis	k1gInSc4	popis
Panoptikonu	Panoptikon	k1gInSc2	Panoptikon
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
sleduje	sledovat	k5eAaImIp3nS	sledovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
neustálé	neustálý	k2eAgNnSc4d1	neustálé
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kulturní	kulturní	k2eAgFnSc2d1	kulturní
či	či	k8xC	či
sociální	sociální	k2eAgFnSc2d1	sociální
normy	norma	k1gFnSc2	norma
a	a	k8xC	a
případně	případně	k6eAd1	případně
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
jeho	jeho	k3xOp3gInSc4	jeho
výkon	výkon	k1gInSc4	výkon
při	při	k7c6	při
podřízení	podřízení	k1gNnSc6	podřízení
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
vzdorování	vzdorování	k1gNnSc2	vzdorování
normě	norma	k1gFnSc3	norma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
sociální	sociální	k2eAgFnSc2d1	sociální
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
motivace	motivace	k1gFnSc1	motivace
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
činu	čin	k1gInSc3	čin
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
empatické	empatický	k2eAgFnPc1d1	empatická
rolové	rolový	k2eAgFnPc1d1	rolová
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
často	často	k6eAd1	často
motivují	motivovat	k5eAaBmIp3nP	motivovat
altruistické	altruistický	k2eAgNnSc4d1	altruistické
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Management	management	k1gInSc1	management
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
kontext	kontext	k1gInSc1	kontext
===	===	k?	===
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
jakým	jaký	k3yQgInSc7	jaký
probíhá	probíhat	k5eAaImIp3nS	probíhat
reflexe	reflexe	k1gFnSc1	reflexe
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
pocitů	pocit	k1gInPc2	pocit
se	se	k3xPyFc4	se
ustavuje	ustavovat	k5eAaImIp3nS	ustavovat
během	během	k7c2	během
socializace	socializace	k1gFnSc2	socializace
aktéra	aktér	k1gMnSc2	aktér
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
kulturním	kulturní	k2eAgInSc7d1	kulturní
kontextem	kontext	k1gInSc7	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
kulturách	kultura	k1gFnPc6	kultura
defavorizovány	defavorizován	k2eAgInPc1d1	defavorizován
a	a	k8xC	a
jedinci	jedinec	k1gMnSc3	jedinec
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
tak	tak	k8xC	tak
snaží	snažit	k5eAaImIp3nS	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ventilovat	ventilovat	k5eAaImF	ventilovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rituálu	rituál	k1gInSc2	rituál
či	či	k8xC	či
transponovat	transponovat	k5eAaBmF	transponovat
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
emoce	emoce	k1gFnSc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
kulturní	kulturní	k2eAgInPc1d1	kulturní
okruhy	okruh	k1gInPc1	okruh
nejenom	nejenom	k6eAd1	nejenom
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
specifické	specifický	k2eAgInPc1d1	specifický
"	"	kIx"	"
<g/>
slovníky	slovník	k1gInPc1	slovník
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
repertoáry	repertoár	k1gInPc4	repertoár
<g/>
"	"	kIx"	"
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
učí	učit	k5eAaImIp3nP	učit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
prožitky	prožitek	k1gInPc7	prožitek
manipulovat	manipulovat	k5eAaImF	manipulovat
a	a	k8xC	a
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
řídit	řídit	k5eAaImF	řídit
podle	podle	k7c2	podle
daných	daný	k2eAgNnPc2d1	dané
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
norem	norma	k1gFnPc2	norma
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
feeling	feeling	k1gInSc4	feeling
rules	rulesa	k1gFnPc2	rulesa
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
často	často	k6eAd1	často
podvědomě	podvědomě	k6eAd1	podvědomě
snaží	snažit	k5eAaImIp3nP	snažit
cítit	cítit	k5eAaImF	cítit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tak	tak	k6eAd1	tak
cítit	cítit	k5eAaImF	cítit
i	i	k9	i
k	k	k7c3	k
aktéru	aktér	k1gMnSc3	aktér
s	s	k7c7	s
nimž	jenž	k3xRgMnPc3	jenž
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
emotion	emotion	k1gInSc4	emotion
work	worka	k1gFnPc2	worka
<g/>
.	.	kIx.	.
</s>
<s>
Manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
emocemi	emoce	k1gFnPc7	emoce
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
managementu	management	k1gInSc2	management
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
kulturně	kulturně	k6eAd1	kulturně
specifický	specifický	k2eAgInSc1d1	specifický
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jejich	jejich	k3xOp3gInPc1	jejich
fyziologicko-biologické	fyziologickoiologický	k2eAgInPc1d1	fyziologicko-biologický
stimuly	stimul	k1gInPc1	stimul
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
univerzální	univerzální	k2eAgInPc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emotion	Emotion	k1gInSc1	Emotion
work	work	k1gInSc1	work
a	a	k8xC	a
management	management	k1gInSc1	management
emocí	emoce	k1gFnPc2	emoce
se	se	k3xPyFc4	se
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
euro-atlantického	eurotlantický	k2eAgInSc2d1	euro-atlantický
kulturního	kulturní	k2eAgInSc2d1	kulturní
okruhu	okruh	k1gInSc2	okruh
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
kapitalistických	kapitalistický	k2eAgFnPc6d1	kapitalistická
společnostech	společnost	k1gFnPc6	společnost
projevuje	projevovat	k5eAaImIp3nS	projevovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
emocí	emoce	k1gFnPc2	emoce
stává	stávat	k5eAaImIp3nS	stávat
komodita	komodita	k1gFnSc1	komodita
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyžadovány	vyžadovat	k5eAaImNgFnP	vyžadovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výkonu	výkon	k1gInSc2	výkon
pracovních	pracovní	k2eAgFnPc2d1	pracovní
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
u	u	k7c2	u
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
aktéry	aktér	k1gMnPc7	aktér
například	například	k6eAd1	například
letuška	letuška	k1gFnSc1	letuška
<g/>
,	,	kIx,	,
číšník	číšník	k1gMnSc1	číšník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
během	během	k7c2	během
obchodního	obchodní	k2eAgNnSc2d1	obchodní
jednání	jednání	k1gNnSc2	jednání
mezi	mezi	k7c4	mezi
CEOs	CEOs	k1gInSc4	CEOs
obchodních	obchodní	k2eAgFnPc2d1	obchodní
korporací	korporace	k1gFnPc2	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Zvládání	zvládání	k1gNnSc1	zvládání
emocí	emoce	k1gFnPc2	emoce
tak	tak	k9	tak
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
dimenzi	dimenze	k1gFnSc4	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Goffmana	Goffman	k1gMnSc4	Goffman
představuje	představovat	k5eAaImIp3nS	představovat
management	management	k1gInSc1	management
emocí	emoce	k1gFnPc2	emoce
přeneseně	přeneseně	k6eAd1	přeneseně
faktor	faktor	k1gInSc1	faktor
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
schopnost	schopnost	k1gFnSc1	schopnost
aktéra	aktér	k1gMnSc2	aktér
být	být	k5eAaImF	být
mobilní	mobilní	k2eAgInPc1d1	mobilní
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sociální	sociální	k2eAgFnSc2d1	sociální
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Respektive	respektive	k9	respektive
jednat	jednat	k5eAaImF	jednat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
aktéři	aktér	k1gMnPc1	aktér
budou	být	k5eAaImBp3nP	být
prokazovat	prokazovat	k5eAaImF	prokazovat
úctu	úcta	k1gFnSc4	úcta
(	(	kIx(	(
<g/>
v	v	k7c4	v
angl	angl	k1gInSc4	angl
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
demeanor	demeanor	k1gInSc1	demeanor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
lépe	dobře	k6eAd2	dobře
zvládnutá	zvládnutý	k2eAgFnSc1d1	zvládnutá
schopnost	schopnost	k1gFnSc1	schopnost
managementu	management	k1gInSc2	management
vlastních	vlastní	k2eAgFnPc2d1	vlastní
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
rostou	růst	k5eAaImIp3nP	růst
jeho	jeho	k3xOp3gFnPc4	jeho
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
i	i	k9	i
lepší	dobrý	k2eAgNnSc4d2	lepší
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sociální	sociální	k2eAgFnSc2d1	sociální
hierarchie	hierarchie	k1gFnSc2	hierarchie
společenské	společenský	k2eAgFnSc2d1	společenská
či	či	k8xC	či
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
dimenze	dimenze	k1gFnSc1	dimenze
problematiky	problematika	k1gFnSc2	problematika
emocí	emoce	k1gFnPc2	emoce
===	===	k?	===
</s>
</p>
<p>
<s>
Pocit	pocit	k1gInSc1	pocit
studu	stud	k1gInSc2	stud
nebo	nebo	k8xC	nebo
hanby	hanba	k1gFnSc2	hanba
přispívá	přispívat	k5eAaImIp3nS	přispívat
kromě	kromě	k7c2	kromě
stabilizace	stabilizace	k1gFnSc2	stabilizace
struktur	struktura	k1gFnPc2	struktura
sociálních	sociální	k2eAgFnPc2d1	sociální
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
sítí	síť	k1gFnPc2	síť
také	také	k9	také
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
sociálních	sociální	k2eAgFnPc2d1	sociální
nerovností	nerovnost	k1gFnPc2	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušníci	příslušník	k1gMnPc1	příslušník
sociální	sociální	k2eAgFnSc2d1	sociální
vrstvy	vrstva	k1gFnSc2	vrstva
či	či	k8xC	či
třídy	třída	k1gFnSc2	třída
často	často	k6eAd1	často
diferencují	diferencovat	k5eAaImIp3nP	diferencovat
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
jako	jako	k9	jako
individuum	individuum	k1gNnSc1	individuum
i	i	k8xC	i
jako	jako	k9	jako
člena	člen	k1gMnSc4	člen
určité	určitý	k2eAgFnSc2d1	určitá
referenční	referenční	k2eAgFnSc2d1	referenční
skupiny	skupina	k1gFnSc2	skupina
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
morální	morální	k2eAgFnPc4d1	morální
hranice	hranice	k1gFnPc4	hranice
vůči	vůči	k7c3	vůči
členům	člen	k1gInPc3	člen
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
vrstev	vrstva	k1gFnPc2	vrstva
či	či	k8xC	či
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
tedy	tedy	k9	tedy
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
prožívané	prožívaný	k2eAgInPc1d1	prožívaný
stavy	stav	k1gInPc1	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
evaluaci	evaluace	k1gFnSc3	evaluace
či	či	k8xC	či
hodnocení	hodnocení	k1gNnSc6	hodnocení
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
či	či	k8xC	či
jiných	jiný	k2eAgMnPc2d1	jiný
aktérů	aktér	k1gMnPc2	aktér
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
dalšími	další	k2eAgMnPc7d1	další
aktéry	aktér	k1gMnPc7	aktér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
často	často	k6eAd1	často
aktéři	aktér	k1gMnPc1	aktér
považují	považovat	k5eAaImIp3nP	považovat
normu	norma	k1gFnSc4	norma
platící	platící	k2eAgFnSc4d1	platící
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
sociální	sociální	k2eAgInSc4d1	sociální
či	či	k8xC	či
kulturní	kulturní	k2eAgInSc4d1	kulturní
okruh	okruh	k1gInSc4	okruh
za	za	k7c4	za
univerzálně	univerzálně	k6eAd1	univerzálně
platnou	platný	k2eAgFnSc4d1	platná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
mající	mající	k2eAgFnSc4d1	mající
legitimitu	legitimita	k1gFnSc4	legitimita
být	být	k5eAaImF	být
univerzálně	univerzálně	k6eAd1	univerzálně
platnou	platný	k2eAgFnSc4d1	platná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
sociální	sociální	k2eAgFnPc4d1	sociální
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
jiného	jiný	k2eAgMnSc2d1	jiný
aktéra	aktér	k1gMnSc2	aktér
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnPc2	jeho
jednání	jednání	k1gNnPc2	jednání
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
nazvat	nazvat	k5eAaPmF	nazvat
emoční	emoční	k2eAgInSc4d1	emoční
habitus	habitus	k1gInSc4	habitus
<g/>
.	.	kIx.	.
</s>
<s>
Feeling	Feeling	k1gInSc4	Feeling
rules	rulesa	k1gFnPc2	rulesa
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
nejen	nejen	k6eAd1	nejen
normativního	normativní	k2eAgInSc2d1	normativní
kódu	kód	k1gInSc2	kód
určité	určitý	k2eAgFnSc2d1	určitá
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
diferencované	diferencovaný	k2eAgInPc1d1	diferencovaný
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
sociální	sociální	k2eAgFnPc4d1	sociální
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
moderních	moderní	k2eAgFnPc2d1	moderní
a	a	k8xC	a
pozdně	pozdně	k6eAd1	pozdně
moderních	moderní	k2eAgMnPc2d1	moderní
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
stále	stále	k6eAd1	stále
významnější	významný	k2eAgFnSc4d2	významnější
schopnost	schopnost	k1gFnSc4	schopnost
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
emocemi	emoce	k1gFnPc7	emoce
vlastními	vlastní	k2eAgFnPc7d1	vlastní
nebo	nebo	k8xC	nebo
s	s	k7c7	s
emocemi	emoce	k1gFnPc7	emoce
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
potenciální	potenciální	k2eAgFnSc4d1	potenciální
příležitostí	příležitost	k1gFnSc7	příležitost
pro	pro	k7c4	pro
sociální	sociální	k2eAgFnSc4d1	sociální
mobilitu	mobilita	k1gFnSc4	mobilita
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
kumulace	kumulace	k1gFnSc2	kumulace
úcty	úcta	k1gFnSc2	úcta
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
či	či	k8xC	či
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
"	"	kIx"	"
<g/>
řízeným	řízený	k2eAgNnSc7d1	řízené
<g/>
"	"	kIx"	"
prožíváním	prožívání	k1gNnSc7	prožívání
emocí	emoce	k1gFnPc2	emoce
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
sportovní	sportovní	k2eAgFnPc4d1	sportovní
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
popkulturu	popkulturu	k?	popkulturu
či	či	k8xC	či
kulturní	kulturní	k2eAgInSc1d1	kulturní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
nazvat	nazvat	k5eAaPmF	nazvat
citovou	citový	k2eAgFnSc7d1	citová
vyprahlostí	vyprahlost	k1gFnSc7	vyprahlost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgFnPc4d1	lidská
schopnosti	schopnost	k1gFnPc4	schopnost
konstruovat	konstruovat	k5eAaImF	konstruovat
city	cit	k1gInPc4	cit
a	a	k8xC	a
komunikovat	komunikovat	k5eAaImF	komunikovat
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
omezeny	omezit	k5eAaPmNgInP	omezit
a	a	k8xC	a
ohrožovány	ohrožovat	k5eAaImNgInP	ohrožovat
především	především	k6eAd1	především
blazeovaným	blazeovaný	k2eAgInSc7d1	blazeovaný
postojem	postoj	k1gInSc7	postoj
<g/>
,	,	kIx,	,
přemírou	přemíra	k1gFnSc7	přemíra
banálních	banální	k2eAgInPc2d1	banální
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
jistým	jistý	k2eAgInSc7d1	jistý
nihilismem	nihilismus	k1gInSc7	nihilismus
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
chápat	chápat	k5eAaImF	chápat
fenomén	fenomén	k1gInSc4	fenomén
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
prožívány	prožívat	k5eAaImNgInP	prožívat
a	a	k8xC	a
kontextualizovány	kontextualizovat	k5eAaImNgInP	kontextualizovat
odlišně	odlišně	k6eAd1	odlišně
než	než	k8xS	než
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Zygmunt	Zygmunt	k1gMnSc1	Zygmunt
Bauman	Bauman	k1gMnSc1	Bauman
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
společnosti	společnost	k1gFnSc6	společnost
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
lidské	lidský	k2eAgInPc1d1	lidský
vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
emoce	emoce	k1gFnSc1	emoce
předmětem	předmět	k1gInSc7	předmět
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
kulturního	kulturní	k2eAgInSc2d1	kulturní
významu	význam	k1gInSc2	význam
emocí	emoce	k1gFnPc2	emoce
lze	lze	k6eAd1	lze
připomenout	připomenout	k5eAaPmF	připomenout
dílo	dílo	k1gNnSc4	dílo
Norberta	Norbert	k1gMnSc2	Norbert
Eliase	Eliasa	k1gFnSc6	Eliasa
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
O	o	k7c6	o
procesu	proces	k1gInSc6	proces
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
psychogeneze	psychogeneze	k1gFnSc1	psychogeneze
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
členů	člen	k1gInPc2	člen
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
osobnosti	osobnost	k1gFnSc2	osobnost
se	se	k3xPyFc4	se
aktéři	aktér	k1gMnPc1	aktér
učí	učit	k5eAaImIp3nP	učit
zvládání	zvládání	k1gNnSc4	zvládání
projevů	projev	k1gInPc2	projev
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
spontánních	spontánní	k2eAgInPc2d1	spontánní
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
akceptování	akceptování	k1gNnSc3	akceptování
disciplinace	disciplinace	k1gFnSc2	disciplinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
je	být	k5eAaImIp3nS	být
důvěra	důvěra	k1gFnSc1	důvěra
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
i	i	k9	i
komplexní	komplexní	k2eAgInSc4d1	komplexní
systém	systém	k1gInSc4	systém
finančního	finanční	k2eAgInSc2d1	finanční
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
a	a	k8xC	a
globální	globální	k2eAgFnSc4d1	globální
směnu	směna	k1gFnSc4	směna
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
současné	současný	k2eAgFnSc2d1	současná
dluhové	dluhový	k2eAgFnSc2d1	dluhová
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
ztráta	ztráta	k1gFnSc1	ztráta
důvěry	důvěra	k1gFnSc2	důvěra
mnohdy	mnohdy	k6eAd1	mnohdy
pro	pro	k7c4	pro
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
subjekty	subjekt	k1gInPc4	subjekt
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
státy	stát	k1gInPc1	stát
<g/>
)	)	kIx)	)
zničující	zničující	k2eAgMnSc1d1	zničující
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
skomírající	skomírající	k2eAgInPc4d1	skomírající
fundamentální	fundamentální	k2eAgInPc4d1	fundamentální
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
ukazatele	ukazatel	k1gInPc4	ukazatel
a	a	k8xC	a
výkony	výkon	k1gInPc4	výkon
reálné	reálný	k2eAgFnSc2d1	reálná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Důvěra	důvěra	k1gFnSc1	důvěra
mnoha	mnoho	k4c2	mnoho
aktérů	aktér	k1gMnPc2	aktér
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
chodu	chod	k1gInSc6	chod
nejen	nejen	k6eAd1	nejen
trhy	trh	k1gInPc1	trh
s	s	k7c7	s
dluhopisy	dluhopis	k1gInPc7	dluhopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
burzy	burza	k1gFnPc4	burza
s	s	k7c7	s
cennými	cenný	k2eAgInPc7d1	cenný
papíry	papír	k1gInPc7	papír
a	a	k8xC	a
finančními	finanční	k2eAgInPc7d1	finanční
produkty	produkt	k1gInPc7	produkt
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
systémy	systém	k1gInPc1	systém
důchodových	důchodový	k2eAgNnPc2d1	důchodové
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
dokonce	dokonce	k9	dokonce
emotivní	emotivní	k2eAgFnPc1d1	emotivní
a	a	k8xC	a
racionálně	racionálně	k6eAd1	racionálně
nepodložené	podložený	k2eNgFnPc4d1	nepodložená
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
likviditě	likvidita	k1gFnSc6	likvidita
bank	banka	k1gFnPc2	banka
spustit	spustit	k5eAaPmF	spustit
hromadné	hromadný	k2eAgInPc4d1	hromadný
výběry	výběr	k1gInPc4	výběr
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
tak	tak	k6eAd1	tak
faktickou	faktický	k2eAgFnSc4d1	faktická
neschopnost	neschopnost	k1gFnSc4	neschopnost
dostát	dostát	k5eAaPmF	dostát
závazkům	závazek	k1gInPc3	závazek
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
banka	banka	k1gFnSc1	banka
solventní	solventní	k2eAgFnSc1d1	solventní
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc4	emoce
tak	tak	k9	tak
můžou	můžou	k?	můžou
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
spouštěčů	spouštěč	k1gInPc2	spouštěč
sebenaplňujících	sebenaplňující	k2eAgInPc2d1	sebenaplňující
se	se	k3xPyFc4	se
proroctví	proroctví	k1gNnSc2	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
základě	základ	k1gInSc6	základ
racionalismu	racionalismus	k1gInSc2	racionalismus
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
koherentní	koherentní	k2eAgFnSc4d1	koherentní
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc4d1	stabilní
funkci	funkce	k1gFnSc4	funkce
sociálního	sociální	k2eAgInSc2d1	sociální
i	i	k8xC	i
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
systému	systém	k1gInSc2	systém
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
managementu	management	k1gInSc2	management
emocí	emoce	k1gFnPc2	emoce
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
===	===	k?	===
</s>
</p>
<p>
<s>
Ilustrativním	ilustrativní	k2eAgInSc7d1	ilustrativní
příkladem	příklad	k1gInSc7	příklad
managementu	management	k1gInSc2	management
emocí	emoce	k1gFnPc2	emoce
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
a	a	k8xC	a
studentky	studentka	k1gFnPc1	studentka
medicíny	medicína	k1gFnSc2	medicína
učí	učit	k5eAaImIp3nP	učit
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
se	se	k3xPyFc4	se
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
své	svůj	k3xOyFgFnPc4	svůj
emoce	emoce	k1gFnPc4	emoce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zakoušejí	zakoušet	k5eAaImIp3nP	zakoušet
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
pacienty	pacient	k1gMnPc7	pacient
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
praxe	praxe	k1gFnSc2	praxe
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
pro	pro	k7c4	pro
euroatlantický	euroatlantický	k2eAgInSc4d1	euroatlantický
kulturní	kulturní	k2eAgInSc4d1	kulturní
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
kulturní	kulturní	k2eAgFnSc7d1	kulturní
normou	norma	k1gFnSc7	norma
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
pacienta	pacient	k1gMnSc2	pacient
emoční	emoční	k2eAgFnSc1d1	emoční
neutralita	neutralita	k1gFnSc1	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgInSc1d1	profesionální
přístup	přístup	k1gInSc1	přístup
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
neosobní	osobní	k2eNgInSc1d1	neosobní
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
specifické	specifický	k2eAgInPc4d1	specifický
problémy	problém	k1gInPc4	problém
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
neosobní	osobní	k2eNgInSc1d1	neosobní
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
stejného	stejný	k2eAgInSc2d1	stejný
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
pacientům	pacient	k1gMnPc3	pacient
a	a	k8xC	a
určitého	určitý	k2eAgInSc2d1	určitý
standardu	standard	k1gInSc2	standard
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
studenti	student	k1gMnPc1	student
a	a	k8xC	a
studentky	studentka	k1gFnPc1	studentka
medicíny	medicína	k1gFnSc2	medicína
mají	mít	k5eAaImIp3nP	mít
naučené	naučený	k2eAgFnPc1d1	naučená
a	a	k8xC	a
socializované	socializovaný	k2eAgFnPc1d1	socializovaná
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
objevují	objevovat	k5eAaImIp3nP	objevovat
během	během	k7c2	během
určitých	určitý	k2eAgFnPc2d1	určitá
životních	životní	k2eAgFnPc2d1	životní
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pocit	pocit	k1gInSc1	pocit
studu	stud	k1gInSc2	stud
během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
intimních	intimní	k2eAgNnPc2d1	intimní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc1	odpor
či	či	k8xC	či
znechucení	znechucení	k1gNnSc1	znechucení
během	během	k7c2	během
pitvy	pitva	k1gFnSc2	pitva
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
operací	operace	k1gFnPc2	operace
s	s	k7c7	s
lidským	lidský	k2eAgNnSc7d1	lidské
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
částmi	část	k1gFnPc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
jednání	jednání	k1gNnPc1	jednání
v	v	k7c6	v
roli	role	k1gFnSc6	role
lékaře	lékař	k1gMnSc2	lékař
často	často	k6eAd1	často
porušuje	porušovat	k5eAaImIp3nS	porušovat
určité	určitý	k2eAgNnSc1d1	určité
jinak	jinak	k6eAd1	jinak
obecně	obecně	k6eAd1	obecně
platné	platný	k2eAgFnPc4d1	platná
sociální	sociální	k2eAgFnPc4d1	sociální
konvence	konvence	k1gFnPc4	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
pitvy	pitva	k1gFnSc2	pitva
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
mrtvým	mrtvý	k1gMnPc3	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyšetření	vyšetření	k1gNnPc2	vyšetření
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
fyzický	fyzický	k2eAgInSc1d1	fyzický
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
cizím	cizí	k2eAgMnSc7d1	cizí
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Negativní	negativní	k2eAgFnPc1d1	negativní
či	či	k8xC	či
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
norem	norma	k1gFnPc2	norma
lékařské	lékařský	k2eAgFnSc2d1	lékařská
profese	profes	k1gFnSc2	profes
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
a	a	k8xC	a
studenti	student	k1gMnPc1	student
a	a	k8xC	a
studentky	studentka	k1gFnPc1	studentka
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
tak	tak	k8xS	tak
snaží	snažit	k5eAaImIp3nS	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
proto	proto	k8xC	proto
různé	různý	k2eAgFnPc1d1	různá
strategie	strategie	k1gFnPc1	strategie
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
emoce	emoce	k1gFnPc1	emoce
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
nebo	nebo	k8xC	nebo
ventilovány	ventilován	k2eAgFnPc1d1	ventilována
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
strategie	strategie	k1gFnPc1	strategie
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
individuální	individuální	k2eAgInPc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
odosobnění	odosobnění	k1gNnSc1	odosobnění
pacienta	pacient	k1gMnSc2	pacient
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
předmět	předmět	k1gInSc1	předmět
odborného	odborný	k2eAgInSc2d1	odborný
zájmu	zájem	k1gInSc2	zájem
<g/>
;	;	kIx,	;
mechanizace	mechanizace	k1gFnSc1	mechanizace
a	a	k8xC	a
rutinizace	rutinizace	k1gFnSc1	rutinizace
vyšetření	vyšetření	k1gNnSc2	vyšetření
či	či	k8xC	či
prohlídky	prohlídka	k1gFnSc2	prohlídka
<g/>
;	;	kIx,	;
akcentování	akcentování	k1gNnSc1	akcentování
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
stránek	stránka	k1gFnPc2	stránka
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
profesního	profesní	k2eAgInSc2d1	profesní
růstu	růst	k1gInSc2	růst
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
užívání	užívání	k1gNnSc1	užívání
humoru	humor	k1gInSc2	humor
<g/>
;	;	kIx,	;
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
přímému	přímý	k2eAgInSc3d1	přímý
kontaktu	kontakt	k1gInSc3	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výkonu	výkon	k1gInSc3	výkon
povolání	povolání	k1gNnSc2	povolání
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
přílišný	přílišný	k2eAgInSc4d1	přílišný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
management	management	k1gInSc4	management
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
vnější	vnější	k2eAgInSc4d1	vnější
tlak	tlak	k1gInSc4	tlak
za	za	k7c4	za
následek	následek	k1gInSc4	následek
syndrom	syndrom	k1gInSc4	syndrom
vyhoření	vyhoření	k1gNnPc2	vyhoření
<g/>
.	.	kIx.	.
</s>
<s>
Management	management	k1gInSc1	management
emocí	emoce	k1gFnPc2	emoce
jedince	jedinko	k6eAd1	jedinko
také	také	k9	také
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
sociální	sociální	k2eAgFnSc1d1	sociální
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
ideál	ideál	k1gInSc4	ideál
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
jedincovu	jedincův	k2eAgFnSc4d1	jedincova
snahu	snaha	k1gFnSc4	snaha
a	a	k8xC	a
aspiraci	aspirace	k1gFnSc4	aspirace
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
ideál	ideál	k1gInSc4	ideál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Psychologické	psychologický	k2eAgNnSc1d1	psychologické
pojetí	pojetí	k1gNnSc1	pojetí
emocí	emoce	k1gFnPc2	emoce
==	==	k?	==
</s>
</p>
<p>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnPc1d1	krátká
epizody	epizoda	k1gFnPc1	epizoda
vzájemně	vzájemně	k6eAd1	vzájemně
souvisejících	související	k2eAgFnPc2d1	související
koordinovaných	koordinovaný	k2eAgFnPc2d1	koordinovaná
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
několika	několik	k4yIc6	několik
komponentách	komponenta	k1gFnPc6	komponenta
(	(	kIx(	(
<g/>
neurofyziologické	neurofyziologický	k2eAgFnSc6d1	neurofyziologická
aktivaci	aktivace	k1gFnSc6	aktivace
<g/>
,	,	kIx,	,
behaviorálním	behaviorální	k2eAgInSc6d1	behaviorální
projevu	projev	k1gInSc6	projev
<g/>
,	,	kIx,	,
subjektivním	subjektivní	k2eAgInSc6d1	subjektivní
prožitku	prožitek	k1gInSc6	prožitek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tendenci	tendence	k1gFnSc6	tendence
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
a	a	k8xC	a
kognitivních	kognitivní	k2eAgInPc6d1	kognitivní
procesech	proces	k1gInPc6	proces
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
<g/>
)	)	kIx)	)
vnější	vnější	k2eAgFnPc4d1	vnější
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
celistvou	celistvý	k2eAgFnSc7d1	celistvá
odpovědí	odpověď	k1gFnSc7	odpověď
jedince	jedinko	k6eAd1	jedinko
na	na	k7c4	na
osobně	osobně	k6eAd1	osobně
relevantní	relevantní	k2eAgMnSc1d1	relevantní
a	a	k8xC	a
motivačně	motivačně	k6eAd1	motivačně
významné	významný	k2eAgFnPc4d1	významná
události	událost	k1gFnPc4	událost
(	(	kIx(	(
<g/>
Frijda	Frijda	k1gMnSc1	Frijda
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
;	;	kIx,	;
Levenson	Levenson	k1gNnSc1	Levenson
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
netrvalou	trvalý	k2eNgFnSc4d1	netrvalá
a	a	k8xC	a
časově	časově	k6eAd1	časově
relativně	relativně	k6eAd1	relativně
ohraničenou	ohraničený	k2eAgFnSc7d1	ohraničená
<g/>
)	)	kIx)	)
bio-psycho-sociální	biosychoociální	k2eAgFnSc7d1	bio-psycho-sociální
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
naši	náš	k3xOp1gFnSc4	náš
celkovou	celkový	k2eAgFnSc4d1	celková
pohodu	pohoda	k1gFnSc4	pohoda
a	a	k8xC	a
které	který	k3yRgFnPc4	který
potenciálně	potenciálně	k6eAd1	potenciálně
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
metaforou	metafora	k1gFnSc7	metafora
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
<g/>
,	,	kIx,	,
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
<g/>
,	,	kIx,	,
organizované	organizovaný	k2eAgNnSc4d1	organizované
<g/>
,	,	kIx,	,
ekologické	ekologický	k2eAgNnSc4d1	ekologické
a	a	k8xC	a
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
zpracovávání	zpracovávání	k1gNnSc4	zpracovávání
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nám	my	k3xPp1nPc3	my
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
a	a	k8xC	a
jednat	jednat	k5eAaImF	jednat
bez	bez	k7c2	bez
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
racionálního	racionální	k2eAgNnSc2d1	racionální
zvažování	zvažování	k1gNnSc2	zvažování
(	(	kIx(	(
<g/>
Tooby	Tooba	k1gFnPc1	Tooba
<g/>
,	,	kIx,	,
Cosmides	Cosmides	k1gInSc1	Cosmides
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
se	se	k3xPyFc4	se
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
a	a	k8xC	a
souběžně	souběžně	k6eAd1	souběžně
probíhající	probíhající	k2eAgNnSc1d1	probíhající
zhodnocení	zhodnocení	k1gNnSc1	zhodnocení
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
situací	situace	k1gFnPc2	situace
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
nebo	nebo	k8xC	nebo
negativní	negativní	k2eAgFnPc4d1	negativní
implikace	implikace	k1gFnPc4	implikace
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zájmů	zájem	k1gInPc2	zájem
či	či	k8xC	či
cílů	cíl	k1gInPc2	cíl
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
.	.	kIx.	.
<g/>
g	g	kA	g
<g/>
.	.	kIx.	.
</s>
<s>
Ortony	Ortona	k1gFnPc1	Ortona
<g/>
,	,	kIx,	,
Clore	Clor	k1gMnSc5	Clor
<g/>
,	,	kIx,	,
Collins	Collinsa	k1gFnPc2	Collinsa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
;	;	kIx,	;
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Ellsworth	Ellsworth	k1gMnSc1	Ellsworth
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
detekovatelný	detekovatelný	k2eAgInSc4d1	detekovatelný
spouštěč	spouštěč	k1gInSc4	spouštěč
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc4d1	rychlý
nástup	nástup	k1gInSc4	nástup
a	a	k8xC	a
omezenou	omezený	k2eAgFnSc4d1	omezená
délku	délka	k1gFnSc4	délka
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
který	který	k3yRgMnSc1	který
dále	daleko	k6eAd2	daleko
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
reakci	reakce	k1gFnSc4	reakce
(	(	kIx(	(
<g/>
Frijda	Frijda	k1gMnSc1	Frijda
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Poláčková	Poláčková	k1gFnSc1	Poláčková
Šolcová	Šolcová	k1gFnSc1	Šolcová
<g/>
,	,	kIx,	,
Trnka	Trnka	k1gMnSc1	Trnka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
evolučně	evolučně	k6eAd1	evolučně
starší	starý	k2eAgNnSc4d2	starší
než	než	k8xS	než
rozumové	rozumový	k2eAgNnSc4d1	rozumové
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnSc1	emoce
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
ostatní	ostatní	k1gNnSc4	ostatní
(	(	kIx(	(
<g/>
panika	panika	k1gFnSc1	panika
<g/>
,	,	kIx,	,
pláč	pláč	k1gInSc1	pláč
na	na	k7c6	na
pohřbech	pohřeb	k1gInPc6	pohřeb
<g/>
,	,	kIx,	,
neutišitelný	utišitelný	k2eNgInSc1d1	neutišitelný
smích	smích	k1gInSc1	smích
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Silné	silný	k2eAgFnPc1d1	silná
emoce	emoce	k1gFnPc1	emoce
mohou	moct	k5eAaImIp3nP	moct
poškodit	poškodit	k5eAaPmF	poškodit
zdraví	zdraví	k1gNnSc4	zdraví
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
přivodit	přivodit	k5eAaPmF	přivodit
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
působení	působení	k1gNnSc1	působení
určité	určitý	k2eAgFnSc2d1	určitá
emoce	emoce	k1gFnSc2	emoce
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cit	cit	k1gInSc1	cit
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
užší	úzký	k2eAgInPc4d2	užší
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
pocitový	pocitový	k2eAgInSc1d1	pocitový
zážitek	zážitek	k1gInSc1	zážitek
<g/>
.	.	kIx.	.
<g/>
Užší	úzký	k2eAgFnSc1d2	užší
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
především	především	k9	především
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
emoce	emoce	k1gFnSc1	emoce
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
více	hodně	k6eAd2	hodně
kritérií	kritérion	k1gNnPc2	kritérion
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
daný	daný	k2eAgInSc4d1	daný
stav	stav	k1gInSc4	stav
mohli	moct	k5eAaImAgMnP	moct
označit	označit	k5eAaPmF	označit
emocí	emoce	k1gFnSc7	emoce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
subjektivní	subjektivní	k2eAgInSc1d1	subjektivní
prožitek	prožitek	k1gInSc1	prožitek
<g/>
,	,	kIx,	,
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
projev	projev	k1gInSc1	projev
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
tendence	tendence	k1gFnSc1	tendence
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
<g/>
,	,	kIx,	,
regulace	regulace	k1gFnSc1	regulace
emoce	emoce	k1gFnSc1	emoce
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc1	pocit
obecnějším	obecní	k2eAgInSc7d2	obecní
<g/>
,	,	kIx,	,
širším	široký	k2eAgInSc7d2	širší
pojmem	pojem	k1gInSc7	pojem
než	než	k8xS	než
emoce	emoce	k1gFnSc1	emoce
<g/>
:	:	kIx,	:
stále	stále	k6eAd1	stále
něco	něco	k3yInSc4	něco
cítíme	cítit	k5eAaImIp1nP	cítit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
prožíváme	prožívat	k5eAaImIp1nP	prožívat
nějakou	nějaký	k3yIgFnSc4	nějaký
emoci	emoce	k1gFnSc4	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Pocit	pocit	k1gInSc1	pocit
je	být	k5eAaImIp3nS	být
neustálý	neustálý	k2eAgInSc1d1	neustálý
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
reflektovaný	reflektovaný	k2eAgInSc1d1	reflektovaný
a	a	k8xC	a
uvědomovaný	uvědomovaný	k2eAgInSc1d1	uvědomovaný
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
prostou	prostý	k2eAgFnSc7d1	prostá
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
cítíš	cítit	k5eAaImIp2nS	cítit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pocit	pocit	k1gInSc1	pocit
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
synonymum	synonymum	k1gNnSc4	synonymum
k	k	k7c3	k
afektu	afekt	k1gInSc3	afekt
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgInPc2	všecek
afektivních	afektivní	k2eAgInPc2d1	afektivní
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
nálad	nálada	k1gFnPc2	nálada
<g/>
,	,	kIx,	,
emočních	emoční	k2eAgFnPc2d1	emoční
epizod	epizoda	k1gFnPc2	epizoda
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vlastnosti	vlastnost	k1gFnSc6	vlastnost
emocí	emoce	k1gFnPc2	emoce
===	===	k?	===
</s>
</p>
<p>
<s>
subjektivita	subjektivita	k1gFnSc1	subjektivita
–	–	k?	–
na	na	k7c4	na
stejné	stejný	k2eAgFnPc4d1	stejná
situace	situace	k1gFnPc4	situace
mohou	moct	k5eAaImIp3nP	moct
různí	různý	k2eAgMnPc1d1	různý
jedinci	jedinec	k1gMnPc1	jedinec
odpovídat	odpovídat	k5eAaImF	odpovídat
různými	různý	k2eAgFnPc7d1	různá
a	a	k8xC	a
různě	různě	k6eAd1	různě
intenzivními	intenzivní	k2eAgFnPc7d1	intenzivní
emocemi	emoce	k1gFnPc7	emoce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hněv	hněv	k1gInSc1	hněv
nebo	nebo	k8xC	nebo
strach	strach	k1gInSc1	strach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
reaguje	reagovat	k5eAaBmIp3nS	reagovat
výrazně	výrazně	k6eAd1	výrazně
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
emocí	emoce	k1gFnSc7	emoce
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
intoxikace	intoxikace	k1gFnSc2	intoxikace
(	(	kIx(	(
<g/>
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc1	droga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc1	poškození
mozku	mozek	k1gInSc2	mozek
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgFnSc2d1	duševní
choroby	choroba	k1gFnSc2	choroba
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
někoho	někdo	k3yInSc2	někdo
nedostavují	dostavovat	k5eNaImIp3nP	dostavovat
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
apatii	apatie	k1gFnSc6	apatie
<g/>
,	,	kIx,	,
lhostejnosti	lhostejnost	k1gFnSc3	lhostejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
předmětnost	předmětnost	k1gFnSc4	předmětnost
–	–	k?	–
emoce	emoce	k1gFnPc1	emoce
jsou	být	k5eAaImIp3nP	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
podnět	podnět	k1gInSc4	podnět
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
afektu	afekt	k1gInSc2	afekt
a	a	k8xC	a
od	od	k7c2	od
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
nezacílená	zacílený	k2eNgFnSc1d1	nezacílená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
aktuálnost	aktuálnost	k1gFnSc1	aktuálnost
–	–	k?	–
emoce	emoce	k1gFnSc2	emoce
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
polarita	polarita	k1gFnSc1	polarita
–	–	k?	–
emoce	emoce	k1gFnSc1	emoce
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
umístit	umístit	k5eAaPmF	umístit
na	na	k7c6	na
dimenzi	dimenze	k1gFnSc6	dimenze
libost	libost	k1gFnSc4	libost
<g/>
–	–	k?	–
<g/>
nelibost	nelibost	k1gFnSc1	nelibost
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
sociální	sociální	k2eAgInSc4d1	sociální
kontext	kontext	k1gInSc4	kontext
je	být	k5eAaImIp3nS	být
však	však	k9	však
řada	řada	k1gFnSc1	řada
emocí	emoce	k1gFnPc2	emoce
umístitelná	umístitelný	k2eAgFnSc1d1	umístitelná
ambivalentně	ambivalentně	k6eAd1	ambivalentně
(	(	kIx(	(
<g/>
příjemný	příjemný	k2eAgInSc1d1	příjemný
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
loučení	loučení	k1gNnSc1	loučení
<g/>
,	,	kIx,	,
lítost	lítost	k1gFnSc1	lítost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
–	–	k?	–
emoční	emoční	k2eAgInPc4d1	emoční
stavy	stav	k1gInPc4	stav
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
paměťové	paměťový	k2eAgInPc4d1	paměťový
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc1	nějaký
údaj	údaj	k1gInSc1	údaj
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
emocí	emoce	k1gFnSc7	emoce
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
i	i	k8xC	i
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedinec	jedinec	k1gMnSc1	jedinec
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
snáze	snadno	k6eAd2	snadno
zapamatuje	zapamatovat	k5eAaPmIp3nS	zapamatovat
<g/>
;	;	kIx,	;
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
emoce	emoce	k1gFnSc1	emoce
může	moct	k5eAaImIp3nS	moct
schopnost	schopnost	k1gFnSc4	schopnost
zapamatování	zapamatování	k1gNnSc2	zapamatování
naopak	naopak	k6eAd1	naopak
snížit	snížit	k5eAaPmF	snížit
(	(	kIx(	(
<g/>
úlek	úlek	k1gInSc1	úlek
<g/>
,	,	kIx,	,
vztek	vztek	k1gInSc1	vztek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Funkce	funkce	k1gFnSc1	funkce
emocí	emoce	k1gFnPc2	emoce
===	===	k?	===
</s>
</p>
<p>
<s>
Funkcí	funkce	k1gFnSc7	funkce
emocí	emoce	k1gFnPc2	emoce
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
jedince	jedinko	k6eAd1	jedinko
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
emoce	emoce	k1gFnPc1	emoce
facilitují	facilitovat	k5eAaBmIp3nP	facilitovat
vštípení	vštípení	k1gNnSc4	vštípení
zážitku	zážitek	k1gInSc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
strach	strach	k1gInSc1	strach
má	mít	k5eAaImIp3nS	mít
jedince	jedinec	k1gMnSc4	jedinec
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
a	a	k8xC	a
vtisknout	vtisknout	k5eAaPmF	vtisknout
danou	daný	k2eAgFnSc4d1	daná
situaci	situace	k1gFnSc4	situace
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
jako	jako	k8xC	jako
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnPc1	emoce
tak	tak	k9	tak
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vymezení	vymezení	k1gNnSc3	vymezení
a	a	k8xC	a
hierarchizaci	hierarchizace	k1gFnSc3	hierarchizace
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc4	vytvoření
schopnosti	schopnost	k1gFnSc2	schopnost
seberegulace	seberegulace	k1gFnSc2	seberegulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Emoční	emoční	k2eAgFnSc2d1	emoční
inteligence	inteligence	k1gFnSc2	inteligence
===	===	k?	===
</s>
</p>
<p>
<s>
Emoční	emoční	k2eAgFnSc1d1	emoční
inteligence	inteligence	k1gFnSc1	inteligence
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
zvládat	zvládat	k5eAaImF	zvládat
své	svůj	k3xOyFgFnPc4	svůj
emoce	emoce	k1gFnPc4	emoce
a	a	k8xC	a
vcítit	vcítit	k5eAaPmF	vcítit
se	se	k3xPyFc4	se
do	do	k7c2	do
emocí	emoce	k1gFnPc2	emoce
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
intrapersonální	intrapersonální	k2eAgFnSc1d1	intrapersonální
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
znalost	znalost	k1gFnSc1	znalost
vlastních	vlastní	k2eAgFnPc2d1	vlastní
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ovládání	ovládání	k1gNnSc1	ovládání
vlastních	vlastní	k2eAgFnPc2d1	vlastní
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
schopnost	schopnost	k1gFnSc1	schopnost
sebemotivace	sebemotivace	k1gFnSc1	sebemotivace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
interpersonální	interpersonální	k2eAgFnSc1d1	interpersonální
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
sociální	sociální	k2eAgFnSc1d1	sociální
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
<g/>
empatie	empatie	k1gFnSc1	empatie
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
vcítění	vcítění	k1gNnSc2	vcítění
se	se	k3xPyFc4	se
do	do	k7c2	do
emocí	emoce	k1gFnPc2	emoce
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
schopnost	schopnost	k1gFnSc1	schopnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
na	na	k7c6	na
základě	základ	k1gInSc6	základ
emocí	emoce	k1gFnPc2	emoce
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
afektivních	afektivní	k2eAgInPc2d1	afektivní
jevů	jev	k1gInPc2	jev
podle	podle	k7c2	podle
intenzity	intenzita	k1gFnSc2	intenzita
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
trvání	trvání	k1gNnSc2	trvání
===	===	k?	===
</s>
</p>
<p>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
afektivních	afektivní	k2eAgInPc2d1	afektivní
procesů	proces	k1gInPc2	proces
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
především	především	k9	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
délky	délka	k1gFnSc2	délka
trvání	trvání	k1gNnSc4	trvání
jevu	jev	k1gInSc2	jev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Frijda	Frijda	k1gMnSc1	Frijda
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
;	;	kIx,	;
Levenson	Levenson	k1gNnSc1	Levenson
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
;	;	kIx,	;
Stein	Stein	k1gMnSc1	Stein
<g/>
,	,	kIx,	,
Trabasso	Trabassa	k1gFnSc5	Trabassa
<g/>
,	,	kIx,	,
Liwag	Liwag	k1gMnSc1	Liwag
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
paradoxně	paradoxně	k6eAd1	paradoxně
"	"	kIx"	"
<g/>
změřit	změřit	k5eAaPmF	změřit
<g/>
"	"	kIx"	"
délku	délka	k1gFnSc4	délka
afektivního	afektivní	k2eAgInSc2d1	afektivní
<g />
.	.	kIx.	.
</s>
<s>
jevu	jev	k1gInSc3	jev
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
emoční	emoční	k2eAgFnSc1d1	emoční
epizoda	epizoda	k1gFnSc1	epizoda
či	či	k8xC	či
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
trvání	trvání	k1gNnSc4	trvání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různé	různý	k2eAgInPc1d1	různý
(	(	kIx(	(
<g/>
Scherer	Scherer	k1gInSc1	Scherer
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
zdánlivých	zdánlivý	k2eAgInPc2d1	zdánlivý
konců	konec	k1gInPc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
afektivními	afektivní	k2eAgInPc7d1	afektivní
jevy	jev	k1gInPc7	jev
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
afektivní	afektivní	k2eAgInPc1d1	afektivní
postoje	postoj	k1gInPc1	postoj
jakožto	jakožto	k8xS	jakožto
relativně	relativně	k6eAd1	relativně
trvalé	trvalý	k2eAgFnPc1d1	trvalá
a	a	k8xC	a
citově	citově	k6eAd1	citově
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
preference	preference	k1gFnSc1	preference
a	a	k8xC	a
predisponované	predisponovaný	k2eAgNnSc1d1	predisponované
chování	chování	k1gNnSc1	chování
vůči	vůči	k7c3	vůči
objektům	objekt	k1gInPc3	objekt
<g/>
,	,	kIx,	,
jevům	jev	k1gInPc3	jev
a	a	k8xC	a
osobám	osoba	k1gFnPc3	osoba
(	(	kIx(	(
<g/>
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ničení	ničení	k1gNnSc4	ničení
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
rád	rád	k6eAd1	rád
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
interpersonální	interpersonální	k2eAgInPc1d1	interpersonální
afektivní	afektivní	k2eAgInPc1d1	afektivní
postoje	postoj	k1gInPc1	postoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
</s>
</p>
<p>
<s>
charakterizuji	charakterizovat	k5eAaBmIp1nS	charakterizovat
afektivní	afektivní	k2eAgNnSc1d1	afektivní
(	(	kIx(	(
<g/>
citový	citový	k2eAgInSc4d1	citový
<g/>
)	)	kIx)	)
vztah	vztah	k1gInSc4	vztah
v	v	k7c6	v
diskrétních	diskrétní	k2eAgFnPc6d1	diskrétní
interpersonálních	interpersonální	k2eAgFnPc6d1	interpersonální
interakcích	interakce	k1gFnPc6	interakce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vřelost	vřelost	k1gFnSc1	vřelost
<g/>
,	,	kIx,	,
chlad	chlad	k1gInSc1	chlad
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
osobnostní	osobnostní	k2eAgInPc1d1	osobnostní
rysy	rys	k1gInPc1	rys
jako	jako	k8xC	jako
temperamentové	temperamentový	k2eAgFnPc1d1	temperamentová
dispozice	dispozice	k1gFnPc1	dispozice
a	a	k8xC	a
tendence	tendence	k1gFnPc1	tendence
k	k	k7c3	k
chování	chování	k1gNnSc3	chování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
(	(	kIx(	(
<g/>
nervozita	nervozita	k1gFnSc1	nervozita
<g/>
,	,	kIx,	,
hostilita	hostilita	k1gFnSc1	hostilita
<g/>
,	,	kIx,	,
závistivost	závistivost	k1gFnSc1	závistivost
<g/>
,	,	kIx,	,
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vášně	vášeň	k1gFnPc4	vášeň
jako	jako	k8xC	jako
vysoce	vysoce	k6eAd1	vysoce
hodnocené	hodnocený	k2eAgInPc1d1	hodnocený
individuální	individuální	k2eAgInPc1d1	individuální
cíle	cíl	k1gInPc1	cíl
s	s	k7c7	s
emočně	emočně	k6eAd1	emočně
důležitými	důležitý	k2eAgInPc7d1	důležitý
výsledky	výsledek	k1gInPc7	výsledek
snažení	snažení	k1gNnSc2	snažení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
psychologické	psychologický	k2eAgFnSc6d1	psychologická
literatuře	literatura	k1gFnSc6	literatura
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
termín	termín	k1gInSc1	termín
sentiment	sentiment	k1gInSc1	sentiment
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
afektivní	afektivní	k2eAgFnSc1d1	afektivní
dispozice	dispozice	k1gFnSc1	dispozice
reagovat	reagovat	k5eAaBmF	reagovat
emočně	emočně	k6eAd1	emočně
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc4	objekt
či	či	k8xC	či
děje	děj	k1gInPc4	děj
(	(	kIx(	(
<g/>
Frijda	Frijda	k1gFnSc1	Frijda
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
Oatley	Oatlea	k1gFnSc2	Oatlea
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
afekt	afekt	k1gInSc1	afekt
–	–	k?	–
Afekt	afekt	k1gInSc1	afekt
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgInPc2	všecek
afektivních	afektivní	k2eAgInPc2d1	afektivní
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
jedem	jed	k1gInSc7	jed
bod	bod	k1gInSc1	bod
kontinua	kontinuum	k1gNnSc2	kontinuum
prožívání	prožívání	k1gNnSc2	prožívání
<g/>
,	,	kIx,	,
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
afektu	afekt	k1gInSc2	afekt
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
evaluační	evaluační	k2eAgNnSc1d1	evaluační
<g/>
.	.	kIx.	.
</s>
<s>
Afekt	afekt	k1gInSc1	afekt
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
emocí	emoce	k1gFnSc7	emoce
<g/>
)	)	kIx)	)
signální	signální	k2eAgInSc1d1	signální
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgInSc1d1	přítomný
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
interpretován	interpretovat	k5eAaBmNgMnS	interpretovat
ani	ani	k8xC	ani
registrován	registrovat	k5eAaBmNgMnS	registrovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nálada	nálada	k1gFnSc1	nálada
–	–	k?	–
<g/>
relativně	relativně	k6eAd1	relativně
ohraničený	ohraničený	k2eAgInSc4d1	ohraničený
úsek	úsek	k1gInSc4	úsek
afektivního	afektivní	k2eAgInSc2d1	afektivní
proudu	proud	k1gInSc2	proud
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
valenční	valenční	k2eAgFnSc7d1	valenční
kvalitou	kvalita	k1gFnSc7	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Nálada	nálada	k1gFnSc1	nálada
je	být	k5eAaImIp3nS	být
determinována	determinovat	k5eAaBmNgFnS	determinovat
implicitním	implicitní	k2eAgNnSc7d1	implicitní
afektivním	afektivní	k2eAgNnSc7d1	afektivní
vyladěním	vyladění	k1gNnSc7	vyladění
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
tvarována	tvarovat	k5eAaImNgFnS	tvarovat
aktuálními	aktuální	k2eAgInPc7d1	aktuální
duševními	duševní	k2eAgInPc7d1	duševní
a	a	k8xC	a
tělesnými	tělesný	k2eAgInPc7d1	tělesný
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
<g/>
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
kontext	kontext	k1gInSc4	kontext
<g/>
,	,	kIx,	,
sociokulturní	sociokulturní	k2eAgNnSc4d1	sociokulturní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
kognitivní	kognitivní	k2eAgInSc4d1	kognitivní
styl	styl	k1gInSc4	styl
apod.	apod.	kA	apod.
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
emoce	emoce	k1gFnSc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Náladu	nálada	k1gFnSc4	nálada
lze	lze	k6eAd1	lze
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
pojímat	pojímat	k5eAaImF	pojímat
jako	jako	k9	jako
aktuální	aktuální	k2eAgFnSc4d1	aktuální
a	a	k8xC	a
časově	časově	k6eAd1	časově
relativně	relativně	k6eAd1	relativně
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
dispozici	dispozice	k1gFnSc4	dispozice
vnímat	vnímat	k5eAaImF	vnímat
<g/>
,	,	kIx,	,
interpretovat	interpretovat	k5eAaBmF	interpretovat
a	a	k8xC	a
vztahovat	vztahovat	k5eAaImF	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
vnějšímu	vnější	k2eAgNnSc3d1	vnější
prostředí	prostředí	k1gNnSc3	prostředí
i	i	k8xC	i
sám	sám	k3xTgInSc1	sám
k	k	k7c3	k
sobě	se	k3xPyFc3	se
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
pozitivně	pozitivně	k6eAd1	pozitivně
–	–	k?	–
negativně	negativně	k6eAd1	negativně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
aktuální	aktuální	k2eAgFnSc1d1	aktuální
dispozice	dispozice	k1gFnSc1	dispozice
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
momentální	momentální	k2eAgInPc4d1	momentální
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
zdroje	zdroj	k1gInPc4	zdroj
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
Poláčková	Poláčková	k1gFnSc1	Poláčková
Šolcová	Šolcová	k1gFnSc1	Šolcová
<g/>
,	,	kIx,	,
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vášeň	vášeň	k1gFnSc1	vášeň
–	–	k?	–
vysoce	vysoce	k6eAd1	vysoce
hodnocené	hodnocený	k2eAgInPc1d1	hodnocený
individuální	individuální	k2eAgInPc1d1	individuální
cíle	cíl	k1gInPc1	cíl
s	s	k7c7	s
emočně	emočně	k6eAd1	emočně
důležitými	důležitý	k2eAgInPc7d1	důležitý
výsledky	výsledek	k1gInPc7	výsledek
snažení	snažení	k1gNnSc2	snažení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
emocí	emoce	k1gFnPc2	emoce
podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
===	===	k?	===
</s>
</p>
<p>
<s>
Emoce	emoce	k1gFnSc1	emoce
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
evolučního	evoluční	k2eAgNnSc2d1	evoluční
stáří	stáří	k1gNnSc2	stáří
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc6d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
emoce	emoce	k1gFnPc1	emoce
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
i	i	k9	i
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgNnSc1d1	související
jednání	jednání	k1gNnSc1	jednání
a	a	k8xC	a
mimika	mimika	k1gFnSc1	mimika
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
vrozené	vrozený	k2eAgInPc1d1	vrozený
(	(	kIx(	(
<g/>
smích	smích	k1gInSc1	smích
<g/>
,	,	kIx,	,
pláč	pláč	k1gInSc1	pláč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
vymezení	vymezení	k1gNnSc4	vymezení
základních	základní	k2eAgFnPc2d1	základní
emocí	emoce	k1gFnPc2	emoce
se	se	k3xPyFc4	se
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Základní	základní	k2eAgFnPc1d1	základní
emoce	emoce	k1gFnPc1	emoce
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
emoce	emoce	k1gFnPc1	emoce
====	====	k?	====
</s>
</p>
<p>
<s>
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
–	–	k?	–
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
překonávání	překonávání	k1gNnPc4	překonávání
překážek	překážka	k1gFnPc2	překážka
a	a	k8xC	a
problémů	problém	k1gInPc2	problém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
hodnotu	hodnota	k1gFnSc4	hodnota
pravdy	pravda	k1gFnSc2	pravda
</s>
</p>
<p>
<s>
zvědavost	zvědavost	k1gFnSc4	zvědavost
</s>
</p>
<p>
<s>
frustrace	frustrace	k1gFnSc1	frustrace
z	z	k7c2	z
nepochopení	nepochopení	k1gNnSc2	nepochopení
</s>
</p>
<p>
<s>
morální	morální	k2eAgInSc1d1	morální
–	–	k?	–
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
morálce	morálka	k1gFnSc3	morálka
společnosti	společnost	k1gFnSc2	společnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stud	stud	k1gInSc1	stud
</s>
</p>
<p>
<s>
altruismus	altruismus	k1gInSc1	altruismus
</s>
</p>
<p>
<s>
empatie	empatie	k1gFnSc1	empatie
</s>
</p>
<p>
<s>
estetické	estetický	k2eAgInPc1d1	estetický
–	–	k?	–
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
a	a	k8xC	a
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
vkus	vkus	k1gInSc4	vkus
člověka	člověk	k1gMnSc4	člověk
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Citové	citový	k2eAgInPc1d1	citový
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
určitých	určitý	k2eAgFnPc2d1	určitá
emocí	emoce	k1gFnPc2	emoce
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
složitější	složitý	k2eAgFnPc1d2	složitější
vazby	vazba	k1gFnPc1	vazba
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
citové	citový	k2eAgInPc1d1	citový
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
láska	láska	k1gFnSc1	láska
</s>
</p>
<p>
<s>
přátelství	přátelství	k1gNnSc1	přátelství
</s>
</p>
<p>
<s>
nenávist	nenávist	k1gFnSc1	nenávist
</s>
</p>
<p>
<s>
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
</s>
</p>
<p>
<s>
závist	závist	k1gFnSc1	závist
</s>
</p>
<p>
<s>
žárlivost	žárlivost	k1gFnSc1	žárlivost
</s>
</p>
<p>
<s>
pohrdání	pohrdání	k1gNnSc1	pohrdání
a	a	k8xC	a
opovržení	opovržení	k1gNnSc1	opovržení
</s>
</p>
<p>
<s>
odpor	odpor	k1gInSc1	odpor
a	a	k8xC	a
štítivost	štítivost	k1gFnSc1	štítivost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Tělesné	tělesný	k2eAgInPc1d1	tělesný
pocity	pocit	k1gInPc1	pocit
===	===	k?	===
</s>
</p>
<p>
<s>
Tělesné	tělesný	k2eAgInPc1d1	tělesný
pocity	pocit	k1gInPc1	pocit
nejsou	být	k5eNaImIp3nP	být
emocemi	emoce	k1gFnPc7	emoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
úzký	úzký	k2eAgInSc1d1	úzký
vztah	vztah	k1gInSc1	vztah
–	–	k?	–
určitý	určitý	k2eAgInSc1d1	určitý
tělesný	tělesný	k2eAgInSc1d1	tělesný
pocit	pocit	k1gInSc1	pocit
zpravidla	zpravidla	k6eAd1	zpravidla
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
některých	některý	k3yIgFnPc2	některý
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
tělesné	tělesný	k2eAgInPc4d1	tělesný
pocity	pocit	k1gInPc4	pocit
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
bolest	bolest	k1gFnSc1	bolest
</s>
</p>
<p>
<s>
únava	únava	k1gFnSc1	únava
</s>
</p>
<p>
<s>
hlad	hlad	k1gInSc1	hlad
</s>
</p>
<p>
<s>
žízeň	žízeň	k1gFnSc1	žízeň
</s>
</p>
<p>
<s>
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
pud	pud	k1gInSc1	pud
(	(	kIx(	(
<g/>
žádostivost	žádostivost	k1gFnSc1	žádostivost
<g/>
,	,	kIx,	,
orgasmus	orgasmus	k1gInSc1	orgasmus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAEHR	BAEHR	kA	BAEHR
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
REVIEW	REVIEW	kA	REVIEW
FORUM	forum	k1gNnSc1	forum
THE	THE	kA	THE
SOCIOLOGY	sociolog	k1gMnPc4	sociolog
OF	OF	kA	OF
ALMOST	ALMOST	kA	ALMOST
EVERYTHING	EVERYTHING	kA	EVERYTHING
<g/>
.	.	kIx.	.
</s>
<s>
FOUR	FOUR	kA	FOUR
QUESTIONS	QUESTIONS	kA	QUESTIONS
TO	to	k9	to
RANDAL	RANDAL	kA	RANDAL
COLLINS	COLLINS	kA	COLLINS
ABOUT	ABOUT	kA	ABOUT
INERACTION	INERACTION	kA	INERACTION
RITUAL	RITUAL	kA	RITUAL
CHAINS	CHAINS	kA	CHAINS
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Canadian	Canadian	k1gMnSc1	Canadian
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Sociology	sociolog	k1gMnPc4	sociolog
Online	Onlin	k1gMnSc5	Onlin
<g/>
.	.	kIx.	.
</s>
<s>
January-February	January-Februar	k1gInPc1	January-Februar
2005.	[number]	k4	2005.
</s>
</p>
<p>
<s>
BAUMAN	BAUMAN	kA	BAUMAN
<g/>
,	,	kIx,	,
Zygmunt	Zygmunt	k1gInSc1	Zygmunt
<g/>
.	.	kIx.	.
</s>
<s>
Individualizovaná	individualizovaný	k2eAgFnSc1d1	individualizovaná
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
2004.	[number]	k4	2004.
290	[number]	k4	290
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-204-1195-X	[number]	k4	80-204-1195-X
</s>
</p>
<p>
<s>
BLUMER	BLUMER	kA	BLUMER
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Sociological	Sociologicat	k5eAaPmAgInS	Sociologicat
Implications	Implications	k1gInSc1	Implications
of	of	k?	of
the	the	k?	the
Thought	Thought	k1gMnSc1	Thought
of	of	k?	of
George	Georg	k1gMnSc4	Georg
Herbert	Herbert	k1gMnSc1	Herbert
Mead	Mead	k1gMnSc1	Mead
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Sociology	sociolog	k1gMnPc7	sociolog
<g/>
.	.	kIx.	.
1966.	[number]	k4	1966.
71	[number]	k4	71
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
535	[number]	k4	535
<g/>
–	–	k?	–
<g/>
544	[number]	k4	544
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLUMER	BLUMER	kA	BLUMER
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Symbolic	Symbolice	k1gFnPc2	Symbolice
Interactionism	Interactionisma	k1gFnPc2	Interactionisma
<g/>
.	.	kIx.	.
</s>
<s>
Englewood	Englewood	k1gInSc1	Englewood
Cliffs	Cliffs	k1gInSc1	Cliffs
<g/>
,	,	kIx,	,
N.J.	N.J.	k1gMnSc1	N.J.
<g/>
:	:	kIx,	:
Prentice-Hall	Prentice-Hall	k1gMnSc1	Prentice-Hall
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELIAS	ELIAS	kA	ELIAS
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
procesu	proces	k1gInSc6	proces
civilizace	civilizace	k1gFnSc2	civilizace
:	:	kIx,	:
sociogenetické	sociogenetický	k2eAgFnSc2d1	sociogenetický
a	a	k8xC	a
psychogenetické	psychogenetický	k2eAgFnSc2d1	psychogenetický
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
chování	chování	k1gNnSc2	chování
světských	světský	k2eAgFnPc2d1	světská
horních	horní	k2eAgFnPc2d1	horní
vrstev	vrstva	k1gFnPc2	vrstva
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Argo	Argo	k1gNnSc1	Argo
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7203-838-9	[number]	k4	80-7203-838-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FISHER	FISHER	kA	FISHER
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
<g/>
.	.	kIx.	.
</s>
<s>
CHON	CHON	kA	CHON
<g/>
,	,	kIx,	,
Kyum	Kyum	k1gMnSc1	Kyum
Koo	Koo	k1gMnSc1	Koo
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Durkheim	Durkheim	k1gInSc1	Durkheim
and	and	k?	and
the	the	k?	the
Social	Social	k1gInSc1	Social
Construction	Construction	k1gInSc1	Construction
of	of	k?	of
Emotions	Emotions	k1gInSc1	Emotions
<g/>
.	.	kIx.	.
</s>
<s>
Social	Sociat	k5eAaImAgMnS	Sociat
Psychology	psycholog	k1gMnPc4	psycholog
Quarterly	Quarterla	k1gFnSc2	Quarterla
<g/>
.	.	kIx.	.
52	[number]	k4	52
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1-9	[number]	k4	1-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOUCAULT	FOUCAULT	kA	FOUCAULT
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
<g/>
.	.	kIx.	.
</s>
<s>
Dohlížet	dohlížet	k5eAaImF	dohlížet
a	a	k8xC	a
trestat	trestat	k5eAaImF	trestat
<g/>
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
zrodu	zrod	k1gInSc6	zrod
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dauphin	dauphin	k1gMnSc1	dauphin
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
427	[number]	k4	427
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-86019-96-9	[number]	k4	80-86019-96-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GOFFMAN	GOFFMAN	kA	GOFFMAN
<g/>
,	,	kIx,	,
Erving	Erving	k1gInSc1	Erving
<g/>
.	.	kIx.	.
</s>
<s>
Interaction	Interaction	k1gInSc1	Interaction
Ritual	Ritual	k1gInSc1	Ritual
<g/>
:	:	kIx,	:
Essays	Essays	k1gInSc1	Essays
in	in	k?	in
Face-to-face	Faceoace	k1gFnSc2	Face-to-face
Behavior	Behaviora	k1gFnPc2	Behaviora
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Pantheon	Pantheon	k1gInSc1	Pantheon
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
270	[number]	k4	270
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
0-394-70631-5	[number]	k4	0-394-70631-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOCHSCHILD	HOCHSCHILD	kA	HOCHSCHILD
<g/>
,	,	kIx,	,
Arlie	Arlie	k1gFnSc1	Arlie
Russell	Russell	k1gMnSc1	Russell
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Sociology	sociolog	k1gMnPc7	sociolog
of	of	k?	of
Feeling	Feeling	k1gInSc1	Feeling
and	and	k?	and
Emotion	Emotion	k1gInSc1	Emotion
<g/>
.	.	kIx.	.
</s>
<s>
Another	Anothra	k1gFnPc2	Anothra
Voice	Voice	k1gFnSc2	Voice
<g/>
.	.	kIx.	.
280	[number]	k4	280
<g/>
–	–	k?	–
<g/>
307	[number]	k4	307
</s>
</p>
<p>
<s>
HOCHSCHILD	HOCHSCHILD	kA	HOCHSCHILD
<g/>
,	,	kIx,	,
Arlie	Arlie	k1gFnSc1	Arlie
Russell	Russell	k1gMnSc1	Russell
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Emotion	Emotion	k1gInSc1	Emotion
Work	Work	k1gInSc1	Work
<g/>
,	,	kIx,	,
Feeling	Feeling	k1gInSc1	Feeling
Rules	Rules	k1gInSc1	Rules
<g/>
,	,	kIx,	,
and	and	k?	and
Social	Social	k1gInSc1	Social
Structure	Structur	k1gInSc5	Structur
<g/>
.	.	kIx.	.
</s>
<s>
Americal	Americat	k5eAaPmAgMnS	Americat
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Sociology	sociolog	k1gMnPc7	sociolog
<g/>
.	.	kIx.	.
85	[number]	k4	85
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
551	[number]	k4	551
<g/>
–	–	k?	–
<g/>
575	[number]	k4	575
</s>
</p>
<p>
<s>
MEAD	MEAD	kA	MEAD
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Mind	Mind	k1gMnSc1	Mind
<g/>
,	,	kIx,	,
Self	Self	k1gMnSc1	Self
<g/>
,	,	kIx,	,
and	and	k?	and
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAKONEČNÝ	NAKONEČNÝ	kA	NAKONEČNÝ
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
emoce	emoce	k1gFnPc1	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-200-0763-6	[number]	k4	80-200-0763-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETRUSEK	PETRUSEK	kA	PETRUSEK
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86429-63-6	[number]	k4	80-86429-63-6
</s>
</p>
<p>
<s>
POLÁČKOVÁ	Poláčková	k1gFnSc1	Poláčková
ŠOLCOVÁ	Šolcová	k1gFnSc1	Šolcová
<g/>
,	,	kIx,	,
Iva	Iva	k1gFnSc1	Iva
<g/>
,	,	kIx,	,
TRNKA	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
afektivních	afektivní	k2eAgInPc2d1	afektivní
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
Psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
59	[number]	k4	59
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
298-314	[number]	k4	298-314
</s>
</p>
<p>
<s>
PIXLEY	PIXLEY	kA	PIXLEY
<g/>
,	,	kIx,	,
Jocelyn	Jocelyn	k1gMnSc1	Jocelyn
<g/>
.	.	kIx.	.
2009.	[number]	k4	2009.
'	'	kIx"	'
<g/>
The	The	k1gMnPc2	The
Current	Current	k1gMnSc1	Current
Crisis	Crisis	k1gFnSc2	Crisis
of	of	k?	of
Capitalism	Capitalism	k1gMnSc1	Capitalism
<g/>
'	'	kIx"	'
<g/>
:	:	kIx,	:
What	What	k1gMnSc1	What
Sort	sorta	k1gFnPc2	sorta
of	of	k?	of
Crisis	Crisis	k1gFnPc2	Crisis
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
The	The	k?	The
Economic	Economic	k1gMnSc1	Economic
and	and	k?	and
Labour	Labour	k1gMnSc1	Labour
Relations	Relationsa	k1gFnPc2	Relationsa
Review	Review	k1gMnSc1	Review
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
59-68	[number]	k4	59-68
</s>
</p>
<p>
<s>
SAYER	SAYER	kA	SAYER
<g/>
,	,	kIx,	,
A.	A.	kA	A.
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Class	Class	k1gInSc1	Class
<g/>
,	,	kIx,	,
Moral	Moral	k1gInSc1	Moral
Worth	Worth	k1gInSc1	Worth
and	and	k?	and
Recognition	Recognition	k1gInSc1	Recognition
<g/>
.	.	kIx.	.
</s>
<s>
Sociology	sociolog	k1gMnPc4	sociolog
39	[number]	k4	39
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
947	[number]	k4	947
<g/>
–	–	k?	–
<g/>
963	[number]	k4	963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHIMANK	SCHIMANK	kA	SCHIMANK
<g/>
,	,	kIx,	,
UWE	UWE	kA	UWE
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Against	Against	k1gFnSc1	Against
all	all	k?	all
odds	odds	k1gInSc1	odds
<g/>
:	:	kIx,	:
the	the	k?	the
"	"	kIx"	"
<g/>
loyalty	loyalt	k1gInPc4	loyalt
<g/>
"	"	kIx"	"
<g/>
of	of	k?	of
small	smalnout	k5eAaPmAgMnS	smalnout
investors	investors	k1gInSc4	investors
<g/>
.	.	kIx.	.
</s>
<s>
Socio-Economic	Socio-Economic	k1gMnSc1	Socio-Economic
Review	Review	k1gMnSc1	Review
<g/>
.	.	kIx.	.
9.	[number]	k4	9.
107	[number]	k4	107
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SHOTT	SHOTT	kA	SHOTT
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Emotion	Emotion	k1gInSc1	Emotion
and	and	k?	and
Social	Social	k1gInSc1	Social
Life	Life	k1gInSc1	Life
:	:	kIx,	:
A	a	k9	a
Symbolic	Symbolice	k1gFnPc2	Symbolice
Interactionist	Interactionist	k1gMnSc1	Interactionist
Analysis	Analysis	k1gFnSc2	Analysis
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Americal	Americal	k1gMnSc1	Americal
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Sociology	sociolog	k1gMnPc7	sociolog
<g/>
.	.	kIx.	.
84	[number]	k4	84
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1317	[number]	k4	1317
<g/>
–	–	k?	–
<g/>
1334	[number]	k4	1334
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SMITH	SMITH	kA	SMITH
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
C.	C.	kA	C.
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
KLEINMAN	KLEINMAN	kA	KLEINMAN
<g/>
,	,	kIx,	,
Sherryl	Sherryl	k1gInSc1	Sherryl
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Managing	Managing	k1gInSc1	Managing
Emotions	Emotions	k1gInSc1	Emotions
in	in	k?	in
Medical	Medical	k1gFnSc1	Medical
School	School	k1gInSc1	School
<g/>
:	:	kIx,	:
Students	Students	k1gInSc1	Students
<g/>
'	'	kIx"	'
Contacts	Contacts	k1gInSc1	Contacts
with	with	k1gInSc1	with
the	the	k?	the
Living	Living	k1gInSc1	Living
and	and	k?	and
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
<g/>
.	.	kIx.	.
</s>
<s>
Social	Sociat	k5eAaPmAgMnS	Sociat
Psychology	psycholog	k1gMnPc4	psycholog
Quaterly	Quaterla	k1gFnSc2	Quaterla
<g/>
.	.	kIx.	.
52	[number]	k4	52
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
56-69	[number]	k4	56-69
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgInSc1d1	sociální
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
</s>
</p>
<p>
<s>
Symbolický	symbolický	k2eAgInSc4d1	symbolický
interakcionismus	interakcionismus	k1gInSc4	interakcionismus
</s>
</p>
<p>
<s>
Poruchy	porucha	k1gFnPc1	porucha
emotivity	emotivita	k1gFnSc2	emotivita
</s>
</p>
<p>
<s>
Emoční	emoční	k2eAgInSc1d1	emoční
kvocient	kvocient	k1gInSc1	kvocient
</s>
</p>
<p>
<s>
Nálada	nálada	k1gFnSc1	nálada
</s>
</p>
<p>
<s>
Emotikon	Emotikon	k1gMnSc1	Emotikon
</s>
</p>
<p>
<s>
Alexitymie	Alexitymie	k1gFnSc1	Alexitymie
</s>
</p>
<p>
<s>
Projevy	projev	k1gInPc1	projev
emocí	emoce	k1gFnPc2	emoce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
emoce	emoce	k1gFnSc2	emoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cit	cit	k1gInSc1	cit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Emoce	emoce	k1gFnSc2	emoce
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
