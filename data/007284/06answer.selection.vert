<s>
Emoce	emoce	k1gFnSc1	emoce
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat	lata	k1gFnPc2	lata
<g/>
.	.	kIx.	.
emovere	emovrat	k5eAaPmIp3nS	emovrat
<g/>
-	-	kIx~	-
vzrušovat	vzrušovat	k5eAaImF	vzrušovat
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
psychicky	psychicky	k6eAd1	psychicky
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
konstruované	konstruovaný	k2eAgInPc4d1	konstruovaný
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
subjektivní	subjektivní	k2eAgInPc4d1	subjektivní
zážitky	zážitek	k1gInPc4	zážitek
libosti	libost	k1gFnSc2	libost
a	a	k8xC	a
nelibosti	nelibost	k1gFnSc2	nelibost
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgInPc1d1	provázený
fyziologickými	fyziologický	k2eAgFnPc7d1	fyziologická
změnami	změna	k1gFnPc7	změna
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
dýchání	dýchání	k1gNnSc2	dýchání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
motorickými	motorický	k2eAgInPc7d1	motorický
projevy	projev	k1gInPc7	projev
(	(	kIx(	(
<g/>
mimika	mimika	k1gFnSc1	mimika
<g/>
,	,	kIx,	,
gestikulace	gestikulace	k1gFnSc1	gestikulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnami	změna	k1gFnPc7	změna
pohotovosti	pohotovost	k1gFnSc2	pohotovost
a	a	k8xC	a
zaměřenosti	zaměřenost	k1gFnSc2	zaměřenost
<g/>
.	.	kIx.	.
</s>
