<p>
<s>
John	John	k1gMnSc1	John
Winston	Winston	k1gInSc4	Winston
Ono	onen	k3xDgNnSc1	onen
Lennon	Lennon	k1gNnSc1	Lennon
<g/>
,	,	kIx,	,
MBE	MBE	kA	MBE
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
John	John	k1gMnSc1	John
Winston	Winston	k1gInSc4	Winston
Lennon	Lennon	k1gNnSc4	Lennon
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
Liverpool	Liverpool	k1gInSc1	Liverpool
–	–	k?	–
8.	[number]	k4	8.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgInS	prosadit
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc6	mládí
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Winston	Winston	k1gInSc4	Winston
Lennon	Lennon	k1gInSc1	Lennon
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
liverpoolské	liverpoolský	k2eAgFnSc6d1	liverpoolská
porodnici	porodnice	k1gFnSc6	porodnice
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
roku	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Julia	Julius	k1gMnSc2	Julius
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Stanley	Stanlea	k1gFnPc1	Stanlea
<g/>
)	)	kIx)	)
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
námořník	námořník	k1gMnSc1	námořník
s	s	k7c7	s
irskými	irský	k2eAgInPc7d1	irský
kořeny	kořen	k1gInPc7	kořen
Alfred	Alfred	k1gMnSc1	Alfred
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
otci	otec	k1gMnSc3	otec
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
jméno	jméno	k1gNnSc4	jméno
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
premiérovi	premiér	k1gMnSc6	premiér
Anglie	Anglie	k1gFnSc2	Anglie
Winstonu	Winston	k1gInSc2	Winston
Churchillovi	Churchill	k1gMnSc3	Churchill
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
bez	bez	k7c2	bez
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stále	stále	k6eAd1	stále
plul	plout	k5eAaImAgInS	plout
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
nejdříve	dříve	k6eAd3	dříve
posílal	posílat	k5eAaImAgMnS	posílat
část	část	k1gFnSc4	část
výplaty	výplata	k1gFnSc2	výplata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
zběhl	zběhnout	k5eAaPmAgMnS	zběhnout
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
přerušil	přerušit	k5eAaPmAgMnS	přerušit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
kontakt	kontakt	k1gInSc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Alfred	Alfred	k1gMnSc1	Alfred
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Julia	Julius	k1gMnSc2	Julius
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
těhotná	těhotná	k1gFnSc1	těhotná
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
Mimi	Mimi	k1gFnPc2	Mimi
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1946	[number]	k4	1946
Alfred	Alfred	k1gMnSc1	Alfred
unesl	unést	k5eAaPmAgMnS	unést
Johna	John	k1gMnSc4	John
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
ujet	ujet	k5eAaPmF	ujet
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Julia	Julius	k1gMnSc4	Julius
ho	on	k3xPp3gMnSc4	on
našla	najít	k5eAaPmAgFnS	najít
a	a	k8xC	a
plán	plán	k1gInSc4	plán
mu	on	k3xPp3gMnSc3	on
uťala	utít	k5eAaPmAgFnS	utít
<g/>
.	.	kIx.	.
</s>
<s>
Pětiletý	pětiletý	k2eAgMnSc1d1	pětiletý
John	John	k1gMnSc1	John
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
před	před	k7c4	před
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
chce	chtít	k5eAaImIp3nS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
či	či	k8xC	či
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgInS	vybrat
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
zvolil	zvolit	k5eAaPmAgMnS	zvolit
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Otce	otec	k1gMnPc4	otec
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
uviděl	uvidět	k5eAaPmAgInS	uvidět
až	až	k9	až
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
volbu	volba	k1gFnSc4	volba
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
dětství	dětství	k1gNnSc2	dětství
strávil	strávit	k5eAaPmAgInS	strávit
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
tety	teta	k1gFnSc2	teta
ve	v	k7c6	v
Wooltonu	Woolton	k1gInSc6	Woolton
<g/>
,	,	kIx,	,
předměstí	předměstí	k1gNnSc6	předměstí
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
George	Georg	k1gInSc2	Georg
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
John	John	k1gMnSc1	John
žil	žít	k5eAaImAgMnS	žít
jen	jen	k9	jen
s	s	k7c7	s
tetou	teta	k1gFnSc7	teta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
jako	jako	k9	jako
anglikán	anglikán	k1gMnSc1	anglikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
dětství	dětství	k1gNnSc4	dětství
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Dovedale	Dovedala	k1gFnSc3	Dovedala
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Quarry	Quarra	k1gFnSc2	Quarra
Bank	bank	k1gInSc1	bank
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
mu	on	k3xPp3gMnSc3	on
matka	matka	k1gFnSc1	matka
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
značky	značka	k1gFnSc2	značka
Gallotone	Galloton	k1gInSc5	Galloton
Champion	Champion	k1gInSc4	Champion
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15.	[number]	k4	15.
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Johnovi	John	k1gMnSc3	John
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
srazilo	srazit	k5eAaPmAgNnS	srazit
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
auto	auto	k1gNnSc1	auto
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
svým	svůj	k3xOyFgNnSc7	svůj
zraněním	zranění	k1gNnSc7	zranění
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
žádnou	žádný	k3yNgFnSc4	žádný
školní	školní	k2eAgFnSc4d1	školní
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zkoušku	zkouška	k1gFnSc4	zkouška
a	a	k8xC	a
jen	jen	k9	jen
díky	díky	k7c3	díky
přičinění	přičinění	k1gNnSc3	přičinění
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
Liverpool	Liverpool	k1gInSc1	Liverpool
College	Colleg	k1gMnSc2	Colleg
of	of	k?	of
Art	Art	k1gMnSc2	Art
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rebelem	rebel	k1gMnSc7	rebel
<g/>
,	,	kIx,	,
oblékal	oblékat	k5eAaImAgMnS	oblékat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Teddy	Tedda	k1gFnSc2	Tedda
Boys	boy	k1gMnPc2	boy
a	a	k8xC	a
přicházel	přicházet	k5eAaImAgMnS	přicházet
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
autoritami	autorita	k1gFnPc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
pomoc	pomoc	k1gFnSc4	pomoc
své	svůj	k3xOyFgFnSc2	svůj
spolužačky	spolužačka	k1gFnSc2	spolužačka
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc2d1	budoucí
první	první	k4xOgFnSc2	první
manželky	manželka	k1gFnSc2	manželka
Cynthie	Cynthie	k1gFnSc2	Cynthie
Powell	Powell	k1gMnSc1	Powell
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
roční	roční	k2eAgFnPc4d1	roční
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
The	The	k1gMnSc2	The
Quarrymen	Quarrymen	k1gInSc4	Quarrymen
k	k	k7c3	k
The	The	k1gFnSc3	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnSc1	Beatles
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
první	první	k4xOgFnSc2	první
skifflové	skifflová	k1gFnSc2	skifflová
a	a	k8xC	a
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollové	rollový	k2eAgFnSc2d1	rollová
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnPc2	The
Quarrymen	Quarrymen	k1gInSc1	Quarrymen
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
září	září	k1gNnSc6	září
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
docházel	docházet	k5eAaImAgInS	docházet
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
Quarry	Quarra	k1gFnSc2	Quarra
Bank	bank	k1gInSc1	bank
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
vystoupení	vystoupení	k1gNnSc2	vystoupení
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
potkal	potkat	k5eAaPmAgMnS	potkat
Paula	Paul	k1gMnSc4	Paul
McCartneyho	McCartney	k1gMnSc4	McCartney
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
okamžikem	okamžik	k1gInSc7	okamžik
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
hudebních	hudební	k2eAgInPc2d1	hudební
spojení	spojení	k1gNnSc1	spojení
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
–	–	k?	–
Lennon	Lennon	k1gNnSc4	Lennon
<g/>
/	/	kIx~	/
<g/>
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
první	první	k4xOgInSc1	první
song	song	k1gInSc1	song
napsal	napsat	k5eAaPmAgInS	napsat
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
Little	Little	k1gFnSc2	Little
Girl	girl	k1gFnSc2	girl
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc4	The
Fourmost	Fourmost	k1gFnSc4	Fourmost
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
s	s	k7c7	s
přičiněním	přičinění	k1gNnSc7	přičinění
McCartneyho	McCartney	k1gMnSc2	McCartney
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
přibyl	přibýt	k5eAaPmAgInS	přibýt
tehdy	tehdy	k6eAd1	tehdy
čtrnáctiletý	čtrnáctiletý	k2eAgMnSc1d1	čtrnáctiletý
kytarista	kytarista	k1gMnSc1	kytarista
George	George	k1gFnPc2	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dalšího	další	k1gNnSc2	další
přivedl	přivést	k5eAaPmAgMnS	přivést
Lennon	Lennon	k1gMnSc1	Lennon
basistu	basista	k1gMnSc4	basista
Stuarta	Stuart	k1gMnSc4	Stuart
Sutcliffa	Sutcliff	k1gMnSc4	Sutcliff
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Lennon	Lennon	k1gInSc1	Lennon
přibral	přibrat	k5eAaPmAgInS	přibrat
bubeníka	bubeník	k1gMnSc4	bubeník
Petea	Peteus	k1gMnSc4	Peteus
Besta	Best	k1gMnSc4	Best
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
srpnem	srpen	k1gInSc7	srpen
1960	[number]	k4	1960
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
1962	[number]	k4	1962
skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnSc7	jejich
manažerem	manažer	k1gMnSc7	manažer
stal	stát	k5eAaPmAgMnS	stát
nováček	nováček	k1gMnSc1	nováček
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
Brian	Brian	k1gMnSc1	Brian
Epstein	Epstein	k1gMnSc1	Epstein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
image	image	k1gFnSc4	image
i	i	k8xC	i
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náročné	náročný	k2eAgFnSc6d1	náročná
šňůře	šňůra	k1gFnSc6	šňůra
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
Sutcliff	Sutcliff	k1gMnSc1	Sutcliff
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
roli	role	k1gFnSc4	role
zastal	zastat	k5eAaPmAgMnS	zastat
McCartney	McCartne	k1gMnPc4	McCartne
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc4	bubeník
Besta	Best	k1gMnSc4	Best
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zkompletovala	zkompletovat	k5eAaPmAgFnS	zkompletovat
sestava	sestava	k1gFnSc1	sestava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1962	[number]	k4	1962
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gFnPc6	Me
Do	do	k7c2	do
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
17.	[number]	k4	17.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
singlu	singl	k1gInSc2	singl
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
během	během	k7c2	během
deseti	deset	k4xCc2	deset
hodin	hodina	k1gFnPc2	hodina
dne	den	k1gInSc2	den
11.	[number]	k4	11.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
nahráli	nahrát	k5eAaBmAgMnP	nahrát
své	svůj	k3xOyFgNnSc1	svůj
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Please	Pleasa	k1gFnSc3	Pleasa
Please	Pleasa	k1gFnSc3	Pleasa
Me	Me	k1gFnSc3	Me
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
své	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
jako	jako	k8xC	jako
Lennon	Lennon	k1gInSc4	Lennon
<g/>
/	/	kIx~	/
<g/>
McCartney	McCartney	k1gInPc1	McCartney
a	a	k8xC	a
skutečné	skutečný	k2eAgNnSc1d1	skutečné
autorství	autorství	k1gNnSc1	autorství
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
až	až	k9	až
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Duo	duo	k1gNnSc1	duo
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
osm	osm	k4xCc4	osm
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
ke	k	k7c3	k
skladbám	skladba	k1gFnPc3	skladba
"	"	kIx"	"
<g/>
Ask	Ask	k1gFnSc1	Ask
Me	Me	k1gFnSc1	Me
Why	Why	k1gFnSc1	Why
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Do	do	k7c2	do
You	You	k1gFnSc2	You
Want	Wanta	k1gFnPc2	Wanta
to	ten	k3xDgNnSc1	ten
Know	Know	k1gMnSc1	Know
a	a	k8xC	a
Secret	Secret	k1gMnSc1	Secret
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Please	Pleas	k1gMnSc5	Pleas
Please	Pleas	k1gMnSc5	Pleas
Me	Me	k1gMnSc5	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
k	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
zcela	zcela	k6eAd1	zcela
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	alba	k1gFnSc1	alba
propukla	propuknout	k5eAaPmAgFnS	propuknout
známá	známý	k2eAgFnSc1d1	známá
beatlemanie	beatlemanie	k1gFnSc1	beatlemanie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
přibylo	přibýt	k5eAaPmAgNnS	přibýt
ještě	ještě	k6eAd1	ještě
úspěšnější	úspěšný	k2eAgNnSc1d2	úspěšnější
album	album	k1gNnSc1	album
With	Witha	k1gFnPc2	Witha
the	the	k?	the
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
Lennonovy	Lennonův	k2eAgFnPc4d1	Lennonova
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
to	ten	k3xDgNnSc1	ten
Do	do	k7c2	do
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
a	a	k8xC	a
Second	Seconda	k1gFnPc2	Seconda
Time	Tim	k1gFnSc2	Tim
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1964	[number]	k4	1964
skupina	skupina	k1gFnSc1	skupina
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
estrádě	estráda	k1gFnSc6	estráda
The	The	k1gMnSc1	The
Ed	Ed	k1gMnSc1	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
poté	poté	k6eAd1	poté
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
A	a	k8xC	a
Hard	Hard	k1gInSc4	Hard
Day	Day	k1gFnSc2	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Night	k1gInSc1	Night
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nejdříve	dříve	k6eAd3	dříve
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
napsal	napsat	k5eAaPmAgMnS	napsat
tyto	tento	k3xDgFnPc4	tento
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
Hard	Hard	k1gMnSc1	Hard
Day	Day	k1gMnSc1	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Nighta	k1gFnPc2	Nighta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Any	Any	k1gFnSc1	Any
Time	Tim	k1gFnSc2	Tim
at	at	k?	at
All	All	k1gFnSc2	All
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k8xC	i
Should	Should	k1gMnSc1	Should
Have	Hav	k1gFnSc2	Hav
Known	Known	k1gMnSc1	Known
Better	Better	k1gMnSc1	Better
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
If	If	k1gMnSc1	If
I	i	k8xC	i
Fell	Fell	k1gMnSc1	Fell
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc1	Be
Back	Back	k1gMnSc1	Back
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Cry	Cry	k1gFnSc1	Cry
Instead	Instead	k1gInSc1	Instead
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Happy	Happa	k1gFnSc2	Happa
Just	just	k6eAd1	just
to	ten	k3xDgNnSc1	ten
Dance	Danka	k1gFnSc3	Danka
with	with	k1gMnSc1	with
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tell	Tell	k1gMnSc1	Tell
Me	Me	k1gMnSc1	Me
Why	Why	k1gMnSc1	Why
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
When	When	k1gMnSc1	When
I	i	k8xC	i
Get	Get	k1gMnSc1	Get
Home	Hom	k1gFnSc2	Hom
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Do	do	k7c2	do
That	Thata	k1gFnPc2	Thata
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Albem	album	k1gNnSc7	album
si	se	k3xPyFc3	se
definitivně	definitivně	k6eAd1	definitivně
získali	získat	k5eAaPmAgMnP	získat
i	i	k8xC	i
fanoušky	fanoušek	k1gMnPc7	fanoušek
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
prosince	prosinec	k1gInSc2	prosinec
1964	[number]	k4	1964
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
Beatles	beatles	k1gMnSc2	beatles
for	forum	k1gNnPc2	forum
Sale	Sale	k1gNnPc2	Sale
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
písním	píseň	k1gFnPc3	píseň
"	"	kIx"	"
<g/>
I	i	k9	i
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Want	Want	k1gInSc1	Want
to	ten	k3xDgNnSc4	ten
Spoil	Spoil	k1gMnSc1	Spoil
the	the	k?	the
Party	parta	k1gFnSc2	parta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
Loser	Loser	k1gInSc1	Loser
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
No	no	k9	no
Reply	Repla	k1gFnSc2	Repla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
získali	získat	k5eAaPmAgMnP	získat
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
Beatles	Beatles	k1gFnSc2	Beatles
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
(	(	kIx(	(
<g/>
MBE	MBE	kA	MBE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
beatlemanie	beatlemanie	k1gFnSc1	beatlemanie
po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
škodí	škodit	k5eAaImIp3nP	škodit
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Řev	řev	k1gInSc1	řev
a	a	k8xC	a
pištění	pištění	k1gNnSc1	pištění
fanynek	fanynka	k1gFnPc2	fanynka
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
přehlušovaly	přehlušovat	k5eAaImAgFnP	přehlušovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
a	a	k8xC	a
Lennona	Lennona	k1gFnSc1	Lennona
takové	takový	k3xDgInPc4	takový
výstupy	výstup	k1gInPc4	výstup
nebavily	bavit	k5eNaImAgFnP	bavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1965	[number]	k4	1965
proto	proto	k8xC	proto
vychází	vycházet	k5eAaImIp3nS	vycházet
album	album	k1gNnSc4	album
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
volá	volat	k5eAaImIp3nS	volat
Lennon	Lennon	k1gMnSc1	Lennon
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
byl	být	k5eAaImAgMnS	být
vyčerpán	vyčerpat	k5eAaPmNgMnS	vyčerpat
a	a	k8xC	a
znechucen	znechutit	k5eAaPmNgMnS	znechutit
z	z	k7c2	z
života	život	k1gInSc2	život
ve	v	k7c6	v
slávě	sláva	k1gFnSc6	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
naplno	naplno	k6eAd1	naplno
propadl	propadnout	k5eAaPmAgMnS	propadnout
drogám	droga	k1gFnPc3	droga
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
z	z	k7c2	z
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
tyto	tento	k3xDgFnPc4	tento
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Only	Only	k1gInPc7	Only
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ticket	Ticket	k1gMnSc1	Ticket
to	ten	k3xDgNnSc4	ten
Ride	Ride	k1gNnSc4	Ride
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Going	Going	k1gInSc1	Going
to	ten	k3xDgNnSc1	ten
Lose	los	k1gInSc6	los
That	That	k1gMnSc1	That
Girl	girl	k1gFnSc2	girl
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
to	ten	k3xDgNnSc1	ten
Hide	Hide	k1gNnSc1	Hide
Your	Youra	k1gFnPc2	Youra
Love	lov	k1gInSc5	lov
Away	Awa	k2eAgInPc1d1	Awa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1965	[number]	k4	1965
nahráli	nahrát	k5eAaBmAgMnP	nahrát
již	již	k6eAd1	již
šestou	šestý	k4xOgFnSc4	šestý
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rubber	Rubber	k1gMnSc1	Rubber
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
prozatím	prozatím	k6eAd1	prozatím
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
Girl	girl	k1gFnSc1	girl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Norwegian	Norwegian	k1gMnSc1	Norwegian
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
This	This	k1gInSc1	This
Bird	Birda	k1gFnPc2	Birda
Has	hasit	k5eAaImRp2nS	hasit
Flown	Flown	k1gNnSc4	Flown
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nowhere	Nowher	k1gInSc5	Nowher
Man	mana	k1gFnPc2	mana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Run	Runa	k1gFnPc2	Runa
for	forum	k1gNnPc2	forum
Your	Your	k1gMnSc1	Your
Life	Lif	k1gFnSc2	Lif
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
Word	Word	kA	Word
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4.	[number]	k4	4.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
interview	interview	k1gNnSc6	interview
pronesl	pronést	k5eAaPmAgMnS	pronést
poznámku	poznámka	k1gFnSc4	poznámka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Beatles	Beatles	k1gFnPc1	Beatles
jsou	být	k5eAaImIp3nP	být
populárnější	populární	k2eAgMnSc1d2	populárnější
než	než	k8xS	než
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
vlna	vlna	k1gFnSc1	vlna
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
USA	USA	kA	USA
bojkotovaly	bojkotovat	k5eAaImAgFnP	bojkotovat
vysílání	vysílání	k1gNnSc4	vysílání
hudby	hudba	k1gFnSc2	hudba
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
svolávaly	svolávat	k5eAaImAgInP	svolávat
davy	dav	k1gInPc1	dav
k	k	k7c3	k
pálení	pálení	k1gNnSc3	pálení
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
byl	být	k5eAaImAgMnS	být
donucen	donucen	k2eAgMnSc1d1	donucen
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1966	[number]	k4	1966
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
omluvit	omluvit	k5eAaPmF	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Deprimovaní	deprimovaný	k2eAgMnPc1d1	deprimovaný
z	z	k7c2	z
opakujících	opakující	k2eAgInPc2d1	opakující
se	se	k3xPyFc4	se
nezáživných	záživný	k2eNgInPc2d1	nezáživný
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
vyčerpávajících	vyčerpávající	k2eAgNnPc2d1	vyčerpávající
turné	turné	k1gNnPc2	turné
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1966	[number]	k4	1966
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
George	George	k1gNnSc4	George
Harrison	Harrison	k1gMnSc1	Harrison
a	a	k8xC	a
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
opustit	opustit	k5eAaPmF	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
řekl	říct	k5eAaPmAgMnS	říct
jen	jen	k9	jen
manažerovi	manažer	k1gMnSc3	manažer
a	a	k8xC	a
McCartneymu	McCartneym	k1gInSc2	McCartneym
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Harrisona	Harrison	k1gMnSc4	Harrison
zase	zase	k9	zase
uchlácholili	uchlácholit	k5eAaPmAgMnP	uchlácholit
příslibem	příslib	k1gInSc7	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
žádné	žádný	k3yNgNnSc4	žádný
turné	turné	k1gNnSc4	turné
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
jejich	jejich	k3xOp3gNnSc1	jejich
další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
Revolver	revolver	k1gInSc1	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znovu	znovu	k6eAd1	znovu
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
Lennon	Lennona	k1gFnPc2	Lennona
přispěl	přispět	k5eAaPmAgInS	přispět
písněmi	píseň	k1gFnPc7	píseň
"	"	kIx"	"
<g/>
And	Anda	k1gFnPc2	Anda
Your	Your	k1gInSc1	Your
Bird	Bird	k1gMnSc1	Bird
Can	Can	k1gMnSc1	Can
Sing	Sing	k1gMnSc1	Sing
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Doctor	Doctor	k1gMnSc1	Doctor
Robert	Robert	k1gMnSc1	Robert
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Only	Only	k1gInPc1	Only
Sleeping	Sleeping	k1gInSc1	Sleeping
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Said	Said	k1gMnSc1	Said
She	She	k1gMnSc1	She
Said	Said	k1gMnSc1	Said
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tomorrow	Tomorrow	k1gFnSc1	Tomorrow
Never	Never	k1gMnSc1	Never
Knows	Knows	k1gInSc1	Knows
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nahráli	nahrát	k5eAaPmAgMnP	nahrát
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
album	album	k1gNnSc1	album
Sgt	Sgt	k1gFnSc2	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc4	club
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
album	album	k1gNnSc4	album
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgFnSc1	třetí
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
Lennon	Lennon	k1gMnSc1	Lennon
napsal	napsat	k5eAaBmAgMnS	napsat
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
A	a	k9	a
Day	Day	k1gFnSc1	Day
in	in	k?	in
the	the	k?	the
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Being	Beinga	k1gFnPc2	Beinga
for	forum	k1gNnPc2	forum
the	the	k?	the
Benefit	Benefit	k1gMnSc1	Benefit
of	of	k?	of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Kite	kit	k1gInSc5	kit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lucy	Luca	k1gFnSc2	Luca
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
with	with	k1gMnSc1	with
Diamonds	Diamonds	k1gInSc1	Diamonds
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollového	rollový	k2eAgNnSc2d1	rollové
období	období	k1gNnSc2	období
konce	konec	k1gInSc2	konec
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
údajně	údajně	k6eAd1	údajně
nechtěně	chtěně	k6eNd1	chtěně
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
názvu	název	k1gInSc2	název
dává	dávat	k5eAaImIp3nS	dávat
tehdy	tehdy	k6eAd1	tehdy
módní	módní	k2eAgFnSc4d1	módní
drogu	droga	k1gFnSc4	droga
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1967	[number]	k4	1967
skupinu	skupina	k1gFnSc4	skupina
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
guru	guru	k1gMnSc3	guru
Mahariši	Mahariš	k1gMnSc3	Mahariš
Maheš	Maheš	k1gMnSc3	Maheš
Jógi	Jóg	k1gMnSc3	Jóg
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
transcendentální	transcendentální	k2eAgFnSc1d1	transcendentální
meditace	meditace	k1gFnSc1	meditace
<g/>
,	,	kIx,	,
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
seminářů	seminář	k1gInPc2	seminář
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
o	o	k7c4	o
předávkování	předávkování	k1gNnSc4	předávkování
jejich	jejich	k3xOp3gMnSc2	jejich
manažera	manažer	k1gMnSc2	manažer
Epsteine	Epstein	k1gMnSc5	Epstein
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
skupinu	skupina	k1gFnSc4	skupina
definitivně	definitivně	k6eAd1	definitivně
rozložila	rozložit	k5eAaPmAgFnS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Maharaši	Maharaše	k1gFnSc4	Maharaše
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Magical	Magical	k1gFnSc2	Magical
Mystery	Myster	k1gInPc1	Myster
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
představilo	představit	k5eAaPmAgNnS	představit
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
více	hodně	k6eAd2	hodně
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
a	a	k8xC	a
undergroundový	undergroundový	k2eAgInSc4d1	undergroundový
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc4	Lennon
přispěl	přispět	k5eAaPmAgMnS	přispět
písněmi	píseň	k1gFnPc7	píseň
"	"	kIx"	"
<g/>
All	All	k1gMnPc1	All
You	You	k1gMnSc1	You
Need	Need	k1gMnSc1	Need
Is	Is	k1gMnSc1	Is
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k8xC	i
Am	Am	k1gMnSc1	Am
the	the	k?	the
Walrus	Walrus	k1gMnSc1	Walrus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Strawberry	Strawberr	k1gInPc1	Strawberr
Fields	Fieldsa	k1gFnPc2	Fieldsa
Forever	Forevra	k1gFnPc2	Forevra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rozložená	rozložený	k2eAgFnSc1d1	rozložená
skupina	skupina	k1gFnSc1	skupina
nově	nově	k6eAd1	nově
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
McCartneyho	McCartney	k1gMnSc2	McCartney
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
Maharašiho	Maharaši	k1gMnSc2	Maharaši
ášramu	ášram	k1gInSc2	ášram
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zkomponovala	zkomponovat	k5eAaPmAgFnS	zkomponovat
většinu	většina	k1gFnSc4	většina
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Bílé	bílý	k2eAgNnSc1d1	bílé
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
deziluzi	deziluze	k1gFnSc3	deziluze
z	z	k7c2	z
Maharašiho	Maharaši	k1gMnSc2	Maharaši
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
naplno	naplno	k6eAd1	naplno
začalo	začít	k5eAaPmAgNnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
Lennonovo	Lennonův	k2eAgNnSc1d1	Lennonovo
manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
sbližovat	sbližovat	k5eAaImF	sbližovat
s	s	k7c7	s
avantgardní	avantgardní	k2eAgFnSc7d1	avantgardní
japonskou	japonský	k2eAgFnSc7d1	japonská
umělkyní	umělkyně	k1gFnSc7	umělkyně
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
začal	začít	k5eAaPmAgInS	začít
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
také	také	k6eAd1	také
Beatles	beatles	k1gMnPc1	beatles
založili	založit	k5eAaPmAgMnP	založit
firmu	firma	k1gFnSc4	firma
Apple	Apple	kA	Apple
Corps	corps	k1gInSc4	corps
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
divizi	divize	k1gFnSc4	divize
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
McCartney	McCartney	k1gInPc1	McCartney
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
snažil	snažit	k5eAaImAgMnS	snažit
udržet	udržet	k5eAaPmF	udržet
skupinu	skupina	k1gFnSc4	skupina
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
všichni	všechen	k3xTgMnPc1	všechen
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
začal	začít	k5eAaPmAgMnS	začít
vodit	vodit	k5eAaImF	vodit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
porušil	porušit	k5eAaPmAgMnS	porušit
nepsané	nepsaný	k2eAgNnSc1d1	nepsaný
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Beatles	Beatles	k1gFnPc1	Beatles
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
počinem	počin	k1gInSc7	počin
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
napsal	napsat	k5eAaBmAgMnS	napsat
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Glass	Glass	k1gInSc1	Glass
Onion	Onion	k1gInSc1	Onion
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Good	Good	k1gMnSc1	Good
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Happiness	Happiness	k1gInSc1	Happiness
Is	Is	k1gFnSc2	Is
a	a	k8xC	a
Warm	Warm	k1gMnSc1	Warm
Gun	Gun	k1gMnSc1	Gun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
So	So	kA	So
Tired	Tired	k1gMnSc1	Tired
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Revolution	Revolution	k1gInSc1	Revolution
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Revolution	Revolution	k1gInSc1	Revolution
9	[number]	k4	9
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sexy	sex	k1gInPc1	sex
Sadie	Sadie	k1gFnSc2	Sadie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Continuing	Continuing	k1gInSc1	Continuing
Story	story	k1gFnSc2	story
of	of	k?	of
Bungalow	Bungalow	k1gMnSc1	Bungalow
Bill	Bill	k1gMnSc1	Bill
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Yer	Yer	k1gFnSc1	Yer
Blues	blues	k1gFnSc2	blues
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Julie	Julie	k1gFnSc1	Julie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nepřímo	přímo	k6eNd1	přímo
věnována	věnovat	k5eAaImNgFnS	věnovat
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
kvapně	kvapně	k6eAd1	kvapně
blížil	blížit	k5eAaImAgInS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
nové	nový	k2eAgFnSc3d1	nová
skupině	skupina	k1gFnSc3	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc4	band
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
koncertu	koncert	k1gInSc2	koncert
pochází	pocházet	k5eAaImIp3nS	pocházet
album	album	k1gNnSc1	album
Live	Liv	k1gInSc2	Liv
Peace	Peaka	k1gFnSc3	Peaka
in	in	k?	in
Toronto	Toronto	k1gNnSc1	Toronto
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
vydal	vydat	k5eAaPmAgInS	vydat
tři	tři	k4xCgNnPc4	tři
experimentální	experimentální	k2eAgNnPc4d1	experimentální
avantgardní	avantgardní	k2eAgNnPc4d1	avantgardní
alba	album	k1gNnPc4	album
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnPc2	Two
Virgins	Virgins	k1gInSc1	Virgins
<g/>
,	,	kIx,	,
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Life	Lif	k1gInSc2	Lif
with	with	k1gInSc1	with
the	the	k?	the
Lions	Lions	k1gInSc1	Lions
a	a	k8xC	a
Wedding	Wedding	k1gInSc1	Wedding
Album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
proslulé	proslulý	k2eAgNnSc1d1	proslulé
poslední	poslední	k2eAgNnSc1d1	poslední
vystoupení	vystoupení	k1gNnSc1	vystoupení
Beatles	Beatles	k1gFnPc2	Beatles
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
budovy	budova	k1gFnSc2	budova
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
v	v	k7c6	v
Saville	Savilla	k1gFnSc6	Savilla
Row	Row	k1gFnSc2	Row
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
Lennon	Lennon	k1gMnSc1	Lennon
přivedl	přivést	k5eAaPmAgMnS	přivést
Allena	Allen	k1gMnSc4	Allen
Kleina	Klein	k1gMnSc4	Klein
coby	coby	k?	coby
nového	nový	k2eAgMnSc4d1	nový
manažera	manažer	k1gMnSc4	manažer
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
přijali	přijmout	k5eAaPmAgMnP	přijmout
Harrison	Harrison	k1gMnSc1	Harrison
i	i	k8xC	i
Starr	Starr	k1gMnSc1	Starr
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
McCartney	McCartnea	k1gFnSc2	McCartnea
chtěl	chtít	k5eAaImAgMnS	chtít
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Eastmana	Eastman	k1gMnSc4	Eastman
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc4	bratr
své	svůj	k3xOyFgFnPc4	svůj
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Lindy	Linda	k1gFnSc2	Linda
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
alba	album	k1gNnPc4	album
Abbey	Abbea	k1gFnSc2	Abbea
Road	Road	k1gInSc1	Road
a	a	k8xC	a
Let	let	k1gInSc1	let
It	It	k1gMnSc2	It
Be	Be	k1gMnSc2	Be
už	už	k9	už
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
utrpením	utrpení	k1gNnSc7	utrpení
a	a	k8xC	a
připomínalo	připomínat	k5eAaImAgNnS	připomínat
spíše	spíše	k9	spíše
kompilační	kompilační	k2eAgFnSc4d1	kompilační
spolupráci	spolupráce	k1gFnSc4	spolupráce
čtyř	čtyři	k4xCgMnPc2	čtyři
samostatných	samostatný	k2eAgMnPc2d1	samostatný
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1969	[number]	k4	1969
oznámil	oznámit	k5eAaPmAgInS	oznámit
McCartneymu	McCartneym	k1gInSc2	McCartneym
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gMnSc4	on
poprosil	poprosit	k5eAaPmAgMnS	poprosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
prozatím	prozatím	k6eAd1	prozatím
nezveřejňoval	zveřejňovat	k5eNaImAgInS	zveřejňovat
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc1	Lennon
poslechl	poslechnout	k5eAaPmAgInS	poslechnout
a	a	k8xC	a
skutečný	skutečný	k2eAgInSc1d1	skutečný
rozpad	rozpad	k1gInSc1	rozpad
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
McCartney	McCartnea	k1gFnSc2	McCartnea
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
využil	využít	k5eAaPmAgMnS	využít
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
svého	svůj	k3xOyFgNnSc2	svůj
nového	nový	k2eAgNnSc2d1	nové
sólového	sólový	k2eAgNnSc2d1	sólové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sólová	sólový	k2eAgFnSc1d1	sólová
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
propadli	propadnout	k5eAaPmAgMnP	propadnout
primární	primární	k2eAgFnSc4d1	primární
terapii	terapie	k1gFnSc4	terapie
dr.	dr.	kA	dr.
Arthura	Arthura	k1gFnSc1	Arthura
Janova	Janov	k1gInSc2	Janov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
Lennon	Lennon	k1gMnSc1	Lennon
doslova	doslova	k6eAd1	doslova
vykřičet	vykřičet	k5eAaPmF	vykřičet
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
psychických	psychický	k2eAgInPc2d1	psychický
problémů	problém	k1gInPc2	problém
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
této	tento	k3xDgFnSc2	tento
terapie	terapie	k1gFnSc2	terapie
nahrál	nahrát	k5eAaBmAgMnS	nahrát
mezi	mezi	k7c7	mezi
zářím	září	k1gNnSc7	září
a	a	k8xC	a
říjnem	říjen	k1gInSc7	říjen
1970	[number]	k4	1970
své	své	k1gNnSc4	své
debutové	debutový	k2eAgNnSc4d1	debutové
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
/	/	kIx~	/
<g/>
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc4	band
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
například	například	k6eAd1	například
na	na	k7c6	na
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Mother	Mothra	k1gFnPc2	Mothra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
představuje	představovat	k5eAaImIp3nS	představovat
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tížilo	tížit	k5eAaImAgNnS	tížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Mother	Mothra	k1gFnPc2	Mothra
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
dětstvím	dětství	k1gNnSc7	dětství
<g/>
,	,	kIx,	,
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
God	God	k1gFnSc1	God
<g/>
"	"	kIx"	"
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
konci	konec	k1gInSc6	konec
Beatles	Beatles	k1gFnSc2	Beatles
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
mýtu	mýtus	k1gInSc2	mýtus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Working	Working	k1gInSc1	Working
Class	Classa	k1gFnPc2	Classa
Hero	Hero	k6eAd1	Hero
<g/>
"	"	kIx"	"
zase	zase	k9	zase
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gFnSc4	její
kritiku	kritika	k1gFnSc4	kritika
předvedl	předvést	k5eAaPmAgMnS	předvést
i	i	k9	i
v	v	k7c6	v
singlu	singl	k1gInSc6	singl
"	"	kIx"	"
<g/>
Power	Power	k1gMnSc1	Power
to	ten	k3xDgNnSc4	ten
the	the	k?	the
People	People	k1gFnPc6	People
<g/>
"	"	kIx"	"
z	z	k7c2	z
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1971.	[number]	k4	1971.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1971	[number]	k4	1971
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Imagine	Imagin	k1gMnSc5	Imagin
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
oceněno	ocenit	k5eAaPmNgNnS	ocenit
jak	jak	k8xC	jak
kritiky	kritika	k1gFnPc4	kritika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
posluchači	posluchač	k1gMnPc7	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
pochází	pocházet	k5eAaImIp3nS	pocházet
proslulá	proslulý	k2eAgFnSc1d1	proslulá
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Imagine	Imagin	k1gMnSc5	Imagin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
Lennon	Lennon	k1gMnSc1	Lennon
nabádá	nabádat	k5eAaImIp3nS	nabádat
k	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
míru	mír	k1gInSc6	mír
a	a	k8xC	a
bratrství	bratrství	k1gNnSc6	bratrství
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
států	stát	k1gInPc2	stát
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
milionář	milionář	k1gMnSc1	milionář
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
komunistických	komunistický	k2eAgInPc6d1	komunistický
ideálech	ideál	k1gInPc6	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
Lennon	Lennon	k1gInSc1	Lennon
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
z	z	k7c2	z
Tittenhurst	Tittenhurst	k1gFnSc4	Tittenhurst
Parku	park	k1gInSc2	park
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
natrvalo	natrvalo	k6eAd1	natrvalo
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
Lennon	Lennon	k1gMnSc1	Lennon
věnoval	věnovat	k5eAaPmAgMnS	věnovat
více	hodně	k6eAd2	hodně
avantgardnímu	avantgardní	k2eAgNnSc3d1	avantgardní
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc3	pití
a	a	k8xC	a
politice	politika	k1gFnSc3	politika
než	než	k8xS	než
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydal	vydat	k5eAaPmAgInS	vydat
politické	politický	k2eAgNnSc4d1	politické
album	album	k1gNnSc4	album
Some	Som	k1gFnSc2	Som
Time	Time	k1gNnSc2	Time
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
album	album	k1gNnSc1	album
Mind	Minda	k1gFnPc2	Minda
Games	Gamesa	k1gFnPc2	Gamesa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
Walls	Walls	k1gInSc1	Walls
and	and	k?	and
Bridges	Bridgesa	k1gFnPc2	Bridgesa
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
album	album	k1gNnSc4	album
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Rolla	k1gFnPc2	Rolla
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Sean	Sean	k1gMnSc1	Sean
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
se	se	k3xPyFc4	se
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
comeback	comeback	k1gInSc1	comeback
spustil	spustit	k5eAaPmAgInS	spustit
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
album	album	k1gNnSc1	album
Double	double	k2eAgInPc7d1	double
Fantasy	fantas	k1gInPc7	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
8.	[number]	k4	8.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
postřelen	postřelen	k2eAgMnSc1d1	postřelen
a	a	k8xC	a
následně	následně	k6eAd1	následně
zraněním	zranění	k1gNnSc7	zranění
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Just	just	k6eAd1	just
Like	Like	k1gNnSc1	Like
<g/>
)	)	kIx)	)
Starting	Starting	k1gInSc1	Starting
Over	Over	k1gInSc1	Over
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Woman	Woman	k1gInSc1	Woman
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Watching	Watching	k1gInSc1	Watching
the	the	k?	the
Wheels	Wheels	k1gInSc1	Wheels
<g/>
"	"	kIx"	"
poté	poté	k6eAd1	poté
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
několika	několik	k4yIc2	několik
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
vydáno	vydat	k5eAaPmNgNnS	vydat
album	album	k1gNnSc1	album
Milk	Milk	k1gInSc1	Milk
and	and	k?	and
Honey	Honea	k1gFnPc1	Honea
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
nedodělané	dodělaný	k2eNgFnPc4d1	nedodělaná
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980.	[number]	k4	1980.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8.	[number]	k4	8.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Lennon	Lennon	k1gMnSc1	Lennon
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
z	z	k7c2	z
autogramiády	autogramiáda	k1gFnSc2	autogramiáda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
branou	brána	k1gFnSc7	brána
jeho	on	k3xPp3gInSc2	on
newyorského	newyorský	k2eAgInSc2d1	newyorský
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
the	the	k?	the
Dakota	Dakota	k1gFnSc1	Dakota
ho	on	k3xPp3gNnSc4	on
těžce	těžce	k6eAd1	těžce
postřelil	postřelit	k5eAaPmAgMnS	postřelit
duševně	duševně	k6eAd1	duševně
nemocný	mocný	k2eNgMnSc1d1	nemocný
Mark	Mark	k1gMnSc1	Mark
David	David	k1gMnSc1	David
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
předtím	předtím	k6eAd1	předtím
podepsal	podepsat	k5eAaPmAgMnS	podepsat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Double	double	k2eAgInPc4d1	double
Fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
asi	asi	k9	asi
ve	v	k7c4	v
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
z	z	k7c2	z
limuzíny	limuzína	k1gFnSc2	limuzína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gFnPc4	on
přivezla	přivézt	k5eAaPmAgFnS	přivézt
z	z	k7c2	z
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
prošli	projít	k5eAaPmAgMnP	projít
vstupní	vstupní	k2eAgFnSc7d1	vstupní
bránou	brána	k1gFnSc7	brána
domu	dům	k1gInSc2	dům
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
zavolal	zavolat	k5eAaPmAgMnS	zavolat
Chapman	Chapman	k1gMnSc1	Chapman
zezadu	zezadu	k6eAd1	zezadu
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
Lennone	Lennon	k1gMnSc5	Lennon
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
stihl	stihnout	k5eAaPmAgMnS	stihnout
sotva	sotva	k6eAd1	sotva
otočit	otočit	k5eAaPmF	otočit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
Chapman	Chapman	k1gMnSc1	Chapman
pětkrát	pětkrát	k6eAd1	pětkrát
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
ještě	ještě	k9	ještě
stačil	stačit	k5eAaBmAgMnS	stačit
vyjít	vyjít	k5eAaPmF	vyjít
několik	několik	k4yIc4	několik
schodů	schod	k1gInPc2	schod
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
byli	být	k5eAaImAgMnP	být
velice	velice	k6eAd1	velice
záhy	záhy	k6eAd1	záhy
dva	dva	k4xCgMnPc1	dva
policisté	policista	k1gMnPc1	policista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vážnému	vážný	k2eAgInSc3d1	vážný
stavu	stav	k1gInSc3	stav
zraněného	zraněný	k2eAgMnSc4d1	zraněný
nečekat	čekat	k5eNaImF	čekat
na	na	k7c4	na
sanitku	sanitka	k1gFnSc4	sanitka
a	a	k8xC	a
odvézt	odvézt	k5eAaPmF	odvézt
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
policejním	policejní	k2eAgInSc7d1	policejní
autem	aut	k1gInSc7	aut
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
do	do	k7c2	do
Rooseveltovy	Rooseveltův	k2eAgFnSc2d1	Rooseveltova
nemocnice	nemocnice	k1gFnSc2	nemocnice
museli	muset	k5eAaImAgMnP	muset
ujet	ujet	k5eAaPmF	ujet
asi	asi	k9	asi
jednu	jeden	k4xCgFnSc4	jeden
míli	míle	k1gFnSc4	míle
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
převozu	převoz	k1gInSc2	převoz
Lennon	Lennon	k1gMnSc1	Lennon
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
doktora	doktor	k1gMnSc2	doktor
Stephena	Stephen	k1gMnSc2	Stephen
Lynna	Lynn	k1gMnSc2	Lynn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
té	ten	k3xDgFnSc6	ten
noci	noc	k1gFnSc6	noc
sloužil	sloužit	k5eAaImAgMnS	sloužit
pohotovost	pohotovost	k1gFnSc4	pohotovost
v	v	k7c6	v
Rooseveltově	Rooseveltův	k2eAgFnSc6d1	Rooseveltova
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Lennon	Lennon	k1gMnSc1	Lennon
mrtvý	mrtvý	k1gMnSc1	mrtvý
již	již	k6eAd1	již
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
přivezli	přivézt	k5eAaPmAgMnP	přivézt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
23.	[number]	k4	23.
hodinou	hodina	k1gFnSc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
učinil	učinit	k5eAaPmAgInS	učinit
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc4	všechen
byly	být	k5eAaImAgInP	být
marné	marný	k2eAgInPc1d1	marný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohočetné	mnohočetný	k2eAgFnPc1d1	mnohočetná
střelné	střelný	k2eAgFnPc1d1	střelná
rány	rána	k1gFnPc1	rána
samozřejmě	samozřejmě	k6eAd1	samozřejmě
způsobily	způsobit	k5eAaPmAgFnP	způsobit
vícečetná	vícečetný	k2eAgNnPc1d1	vícečetné
zranění	zranění	k1gNnPc1	zranění
včetně	včetně	k7c2	včetně
roztříštěné	roztříštěný	k2eAgFnSc2d1	roztříštěná
levé	levý	k2eAgFnSc2d1	levá
lopatky	lopatka	k1gFnSc2	lopatka
a	a	k8xC	a
průstřelu	průstřel	k1gInSc2	průstřel
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
fatálním	fatální	k2eAgNnSc7d1	fatální
zraněním	zranění	k1gNnSc7	zranění
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
přetnutí	přetnutí	k1gNnSc1	přetnutí
krční	krční	k2eAgFnSc2d1	krční
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
ztráty	ztráta	k1gFnSc2	ztráta
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
Lennon	Lennon	k1gMnSc1	Lennon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
hemoragický	hemoragický	k2eAgInSc4d1	hemoragický
šok	šok	k1gInSc4	šok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
oznámena	oznámit	k5eAaPmNgFnS	oznámit
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nemocnici	nemocnice	k1gFnSc4	nemocnice
opustila	opustit	k5eAaPmAgFnS	opustit
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
doktor	doktor	k1gMnSc1	doktor
Lynn	Lynn	k1gMnSc1	Lynn
popisoval	popisovat	k5eAaImAgMnS	popisovat
událost	událost	k1gFnSc4	událost
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zpráva	zpráva	k1gFnSc1	zpráva
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
i	i	k8xC	i
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
prakticky	prakticky	k6eAd1	prakticky
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
shromážděný	shromážděný	k2eAgInSc1d1	shromážděný
dav	dav	k1gInSc1	dav
lidí	člověk	k1gMnPc2	člověk
před	před	k7c7	před
domem	dům	k1gInSc7	dům
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívali	zpívat	k5eAaImAgMnP	zpívat
Lennonovy	Lennonův	k2eAgFnPc4d1	Lennonova
písně	píseň	k1gFnPc4	píseň
jak	jak	k8xS	jak
z	z	k7c2	z
období	období	k1gNnSc2	období
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jeho	jeho	k3xOp3gFnPc1	jeho
sólové	sólový	k2eAgFnPc1d1	sólová
kariéry	kariéra	k1gFnPc1	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
následujících	následující	k2eAgInPc2d1	následující
dní	den	k1gInPc2	den
většina	většina	k1gFnSc1	většina
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
i	i	k8xC	i
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
zrušila	zrušit	k5eAaPmAgFnS	zrušit
svoje	svůj	k3xOyFgNnSc4	svůj
programové	programový	k2eAgNnSc4d1	programové
schéma	schéma	k1gNnSc4	schéma
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc4d1	hudební
složku	složka	k1gFnSc4	složka
vysílání	vysílání	k1gNnSc2	vysílání
tvořily	tvořit	k5eAaImAgFnP	tvořit
převážně	převážně	k6eAd1	převážně
nahrávky	nahrávka	k1gFnPc1	nahrávka
Beatles	beatles	k1gMnSc2	beatles
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
pak	pak	k6eAd1	pak
zařadily	zařadit	k5eAaPmAgFnP	zařadit
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
filmů	film	k1gInPc2	film
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
rozproudila	rozproudit	k5eAaPmAgFnS	rozproudit
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
držení	držení	k1gNnSc6	držení
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
vyrojila	vyrojit	k5eAaPmAgFnS	vyrojit
spousta	spousta	k1gFnSc1	spousta
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
fingované	fingovaný	k2eAgFnSc6d1	fingovaná
vraždě	vražda	k1gFnSc6	vražda
a	a	k8xC	a
Lennonovi	Lennon	k1gMnSc6	Lennon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
někde	někde	k6eAd1	někde
v	v	k7c6	v
tichomoří	tichomoří	k1gNnSc6	tichomoří
na	na	k7c6	na
zapadlém	zapadlý	k2eAgInSc6d1	zapadlý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Dakota	Dakota	k1gFnSc1	Dakota
House	house	k1gNnSc1	house
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Partnerské	partnerský	k2eAgInPc1d1	partnerský
vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
23.	[number]	k4	23.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
spolužačkou	spolužačka	k1gFnSc7	spolužačka
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
Cynthií	Cynthie	k1gFnPc2	Cynthie
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
8.	[number]	k4	8.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
John	John	k1gMnSc1	John
Charles	Charles	k1gMnSc1	Charles
Julian	Julian	k1gMnSc1	Julian
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc1	Lennon
ji	on	k3xPp3gFnSc4	on
mnohokrát	mnohokrát	k6eAd1	mnohokrát
podvedl	podvést	k5eAaPmAgInS	podvést
s	s	k7c7	s
fanynkami	fanynka	k1gFnPc7	fanynka
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
list	list	k1gInSc1	list
svých	svůj	k3xOyFgFnPc2	svůj
milenek	milenka	k1gFnPc2	milenka
jí	jíst	k5eAaImIp3nS	jíst
předal	předat	k5eAaPmAgMnS	předat
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začali	začít	k5eAaPmAgMnP	začít
spát	spát	k5eAaImF	spát
odděleně	odděleně	k6eAd1	odděleně
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
manželství	manželství	k1gNnPc2	manželství
se	se	k3xPyFc4	se
blížilo	blížit	k5eAaImAgNnS	blížit
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Konečným	konečný	k2eAgInSc7d1	konečný
důvodem	důvod	k1gInSc7	důvod
jejich	jejich	k3xOp3gInSc2	jejich
rozchodu	rozchod	k1gInSc2	rozchod
byla	být	k5eAaImAgFnS	být
japonská	japonský	k2eAgFnSc1d1	japonská
umělkyně	umělkyně	k1gFnSc1	umělkyně
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
Lennon	Lennon	k1gInSc1	Lennon
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgInS	setkat
9.	[number]	k4	9.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
galerie	galerie	k1gFnSc2	galerie
Indica	Indica	k1gMnSc1	Indica
John	John	k1gMnSc1	John
Dunbar	Dunbar	k1gMnSc1	Dunbar
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
vernisáž	vernisáž	k1gFnSc4	vernisáž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
předala	předat	k5eAaPmAgFnS	předat
Yoko	Yoko	k6eAd1	Yoko
Lennonovi	Lennon	k1gMnSc6	Lennon
kartičku	kartička	k1gFnSc4	kartička
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Dýchej	dýchat	k5eAaImRp2nS	dýchat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
zasílala	zasílat	k5eAaImAgFnS	zasílat
po	po	k7c4	po
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
až	až	k9	až
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
ji	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgMnS	začít
financovat	financovat	k5eAaBmF	financovat
výstavu	výstava	k1gFnSc4	výstava
"	"	kIx"	"
<g/>
Half	halfa	k1gFnPc2	halfa
a	a	k8xC	a
Wind	Winda	k1gFnPc2	Winda
<g/>
"	"	kIx"	"
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Lisson	Lissona	k1gFnPc2	Lissona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
pravidelně	pravidelně	k6eAd1	pravidelně
setkávat	setkávat	k5eAaImF	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lennonem	Lennon	k1gMnSc7	Lennon
sdílela	sdílet	k5eAaImAgFnS	sdílet
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
LSD	LSD	kA	LSD
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
i	i	k8xC	i
ve	v	k7c6	v
šňupání	šňupání	k1gNnSc6	šňupání
heroinu	heroin	k1gInSc2	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odjela	odjet	k5eAaPmAgFnS	odjet
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
domu	dům	k1gInSc2	dům
pozval	pozvat	k5eAaPmAgMnS	pozvat
Yoko	Yoko	k1gMnSc1	Yoko
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
i	i	k8xC	i
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
album	album	k1gNnSc1	album
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnSc1	Two
Virgins	Virginsa	k1gFnPc2	Virginsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
je	být	k5eAaImIp3nS	být
Cynthie	Cynthie	k1gFnSc1	Cynthie
nalezla	nalézt	k5eAaBmAgFnS	nalézt
oba	dva	k4xCgInPc4	dva
spolu	spolu	k6eAd1	spolu
spokojené	spokojený	k2eAgInPc4d1	spokojený
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
jejich	jejich	k3xOp3gInSc2	jejich
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
manželství	manželství	k1gNnSc2	manželství
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
podala	podat	k5eAaPmAgFnS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgFnS	být
sama	sám	k3xTgFnSc1	sám
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
Anthonyho	Anthony	k1gMnSc4	Anthony
Coxe	Cox	k1gMnSc4	Cox
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
dceru	dcera	k1gFnSc4	dcera
Kyoko	Kyoko	k1gNnSc1	Kyoko
(	(	kIx(	(
<g/>
Kjóko-čan	Kjóko-čan	k1gMnSc1	Kjóko-čan
Cox	Cox	k1gMnSc1	Cox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
zachovali	zachovat	k5eAaPmAgMnP	zachovat
takt	takt	k1gInSc4	takt
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
se	se	k3xPyFc4	se
setkávali	setkávat	k5eAaImAgMnP	setkávat
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
Ringa	Ring	k1gMnSc2	Ring
Starra	Starr	k1gMnSc2	Starr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
společně	společně	k6eAd1	společně
předvedli	předvést	k5eAaPmAgMnP	předvést
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
na	na	k7c6	na
premiéře	premiéra	k1gFnSc6	premiéra
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
knihy	kniha	k1gFnSc2	kniha
Písání	Písání	k1gNnSc2	Písání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
podal	podat	k5eAaPmAgMnS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Cynthia	Cynthia	k1gFnSc1	Cynthia
se	se	k3xPyFc4	se
však	však	k9	však
nechtěla	chtít	k5eNaImAgFnS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
soudila	soudit	k5eAaImAgFnS	soudit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
z	z	k7c2	z
Lennona	Lennon	k1gMnSc2	Lennon
vysoudila	vysoudit	k5eAaPmAgFnS	vysoudit
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
výživné	výživné	k1gNnSc4	výživné
na	na	k7c4	na
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Rozvod	rozvod	k1gInSc1	rozvod
Lennona	Lennon	k1gMnSc2	Lennon
byl	být	k5eAaImAgInS	být
stvrzen	stvrdit	k5eAaPmNgInS	stvrdit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Rozvod	rozvod	k1gInSc1	rozvod
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
až	až	k9	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lennon	Lennon	k1gInSc1	Lennon
vyplatit	vyplatit	k5eAaPmF	vyplatit
Coxe	Coxe	k1gFnSc4	Coxe
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
následně	následně	k6eAd1	následně
i	i	k9	i
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Kyoko	Kyoko	k1gNnSc4	Kyoko
zmizel	zmizet	k5eAaPmAgInS	zmizet
neznámo	neznámo	k6eAd1	neznámo
kam	kam	k6eAd1	kam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
často	často	k6eAd1	často
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
závislá	závislý	k2eAgFnSc1d1	závislá
Yoko	Yoko	k6eAd1	Yoko
třikrát	třikrát	k6eAd1	třikrát
potratila	potratit	k5eAaPmAgFnS	potratit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1969	[number]	k4	1969
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1971.	[number]	k4	1971.
<g/>
Dne	den	k1gInSc2	den
26.	[number]	k4	26.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Lennon	Lennon	k1gMnSc1	Lennon
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
<g/>
,	,	kIx,	,
svědkem	svědek	k1gMnSc7	svědek
skromného	skromný	k2eAgInSc2d1	skromný
obřadu	obřad	k1gInSc2	obřad
byl	být	k5eAaImAgMnS	být
Peter	Peter	k1gMnSc1	Peter
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odletěli	odletět	k5eAaPmAgMnP	odletět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
odletěli	odletět	k5eAaPmAgMnP	odletět
na	na	k7c4	na
líbánky	líbánky	k1gInPc4	líbánky
do	do	k7c2	do
Amsterodamu	Amsterodam	k1gInSc2	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
bed-in	bedn	k1gInSc4	bed-in
<g/>
,	,	kIx,	,
když	když	k8xS	když
novinářům	novinář	k1gMnPc3	novinář
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánují	plánovat	k5eAaImIp3nP	plánovat
strávit	strávit	k5eAaPmF	strávit
týden	týden	k1gInSc4	týden
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
před	před	k7c7	před
zraky	zrak	k1gInPc7	zrak
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
nakonec	nakonec	k6eAd1	nakonec
využili	využít	k5eAaPmAgMnP	využít
pro	pro	k7c4	pro
happening	happening	k1gInSc4	happening
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
námětem	námět	k1gInSc7	námět
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
John	John	k1gMnSc1	John
and	and	k?	and
Yoko	Yoko	k1gMnSc1	Yoko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
rádiích	rádius	k1gInPc6	rádius
bojkotována	bojkotován	k2eAgFnSc1d1	bojkotována
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
různých	různý	k2eAgInPc2d1	různý
názorů	názor	k1gInPc2	názor
byl	být	k5eAaImAgInS	být
Lennon	Lennon	k1gInSc1	Lennon
fixován	fixovat	k5eAaImNgInS	fixovat
na	na	k7c4	na
Yoko	Yoko	k1gNnSc4	Yoko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
mateřské	mateřský	k2eAgFnPc4d1	mateřská
vazby	vazba	k1gFnPc4	vazba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1969	[number]	k4	1969
pořádal	pořádat	k5eAaImAgMnS	pořádat
Lennon	Lennon	k1gMnSc1	Lennon
výlet	výlet	k1gInSc4	výlet
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
dcerou	dcera	k1gFnSc7	dcera
Kyoko	Kyoko	k1gNnSc1	Kyoko
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Julianem	Julian	k1gMnSc7	Julian
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výletu	výlet	k1gInSc2	výlet
měli	mít	k5eAaImAgMnP	mít
středně	středně	k6eAd1	středně
vážnou	vážný	k2eAgFnSc4d1	vážná
autonehodu	autonehoda	k1gFnSc4	autonehoda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
viníka	viník	k1gMnSc2	viník
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
Lennon	Lennon	k1gInSc1	Lennon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řídil	řídit	k5eAaImAgInS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthona	k1gFnPc1	Anthona
Cox	Cox	k1gFnSc2	Cox
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
již	jenž	k3xRgFnSc4	jenž
páru	pára	k1gFnSc4	pára
nikdy	nikdy	k6eAd1	nikdy
nesvěřil	svěřit	k5eNaPmAgMnS	svěřit
Kyoko	Kyoko	k1gNnSc4	Kyoko
samotnou	samotný	k2eAgFnSc4d1	samotná
a	a	k8xC	a
raději	rád	k6eAd2	rád
pokaždé	pokaždé	k6eAd1	pokaždé
zmizel	zmizet	k5eAaPmAgMnS	zmizet
neznámo	neznámo	k6eAd1	neznámo
kam	kam	k6eAd1	kam
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Lennon	Lennon	k1gMnSc1	Lennon
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Kyoko	Kyoko	k1gNnSc4	Kyoko
unést	unést	k5eAaPmF	unést
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
<g/>
.	.	kIx.	.
</s>
<s>
Kyoko	Kyoko	k6eAd1	Kyoko
odvezli	odvézt	k5eAaPmAgMnP	odvézt
z	z	k7c2	z
dětského	dětský	k2eAgNnSc2d1	dětské
hřiště	hřiště	k1gNnSc2	hřiště
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gFnPc4	on
zadržela	zadržet	k5eAaPmAgFnS	zadržet
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
mimosoudně	mimosoudně	k6eAd1	mimosoudně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
Dakota	Dakota	k1gFnSc1	Dakota
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
zažilo	zažít	k5eAaPmAgNnS	zažít
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
najal	najmout	k5eAaPmAgMnS	najmout
jako	jako	k9	jako
služebnou	služebná	k1gFnSc4	služebná
a	a	k8xC	a
asistentku	asistentka	k1gFnSc4	asistentka
mladou	mladý	k2eAgFnSc4d1	mladá
americko-čínskou	americko-čínský	k2eAgFnSc4d1	americko-čínská
dívku	dívka	k1gFnSc4	dívka
jménem	jméno	k1gNnSc7	jméno
May	May	k1gMnSc1	May
Pang	Pang	k1gMnSc1	Pang
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
odjel	odjet	k5eAaPmAgMnS	odjet
odpočinout	odpočinout	k5eAaPmF	odpočinout
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
tam	tam	k6eAd1	tam
sebou	se	k3xPyFc7	se
i	i	k9	i
May	May	k1gMnSc1	May
Pang	Pang	k1gMnSc1	Pang
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
poslechl	poslechnout	k5eAaPmAgMnS	poslechnout
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
září	září	k1gNnSc6	září
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
pití	pití	k1gNnSc4	pití
a	a	k8xC	a
milenkám	milenka	k1gFnPc3	milenka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
May	May	k1gMnSc1	May
Pang	Pang	k1gInSc1	Pang
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc4	všechen
tolerovala	tolerovat	k5eAaImAgFnS	tolerovat
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
několik	několik	k4yIc4	několik
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
zažil	zažít	k5eAaPmAgMnS	zažít
v	v	k7c6	v
LA	la	k1gNnSc6	la
svůj	svůj	k3xOyFgInSc4	svůj
proslulý	proslulý	k2eAgInSc4d1	proslulý
"	"	kIx"	"
<g/>
ztracený	ztracený	k2eAgInSc4d1	ztracený
víkend	víkend	k1gInSc4	víkend
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
prohýřený	prohýřený	k2eAgInSc4d1	prohýřený
rok	rok	k1gInSc4	rok
rockového	rockový	k2eAgMnSc2d1	rockový
muzikanta	muzikant	k1gMnSc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
i	i	k9	i
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrál	nahrát	k5eAaBmAgInS	nahrát
album	album	k1gNnSc4	album
Walls	Wallsa	k1gFnPc2	Wallsa
and	and	k?	and
Bridges	Bridgesa	k1gFnPc2	Bridgesa
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
propitého	propitý	k2eAgNnSc2d1	propité
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
až	až	k9	až
na	na	k7c6	na
albu	album	k1gNnSc6	album
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Roll	k1gInSc1	Roll
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
ho	on	k3xPp3gNnSc4	on
Yoko	Yoko	k1gNnSc4	Yoko
vzala	vzít	k5eAaPmAgFnS	vzít
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9.	[number]	k4	9.
října	říjen	k1gInSc2	říjen
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
na	na	k7c4	na
Lennonovy	Lennonův	k2eAgFnPc4d1	Lennonova
35.	[number]	k4	35.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
)	)	kIx)	)
přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jediné	jediný	k2eAgNnSc1d1	jediné
společné	společný	k2eAgNnSc1d1	společné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Seana	Seaen	k2eAgFnSc1d1	Seana
Ono	onen	k3xDgNnSc4	onen
Taro	Taro	k1gNnSc4	Taro
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
narození	narození	k1gNnSc4	narození
syna	syn	k1gMnSc2	syn
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
dal	dát	k5eAaPmAgMnS	dát
pauzu	pauza	k1gFnSc4	pauza
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	se	k3xPyFc4	se
před	před	k7c7	před
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dítěti	dítě	k1gNnSc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
styk	styk	k1gInSc4	styk
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
díky	díky	k7c3	díky
jejím	její	k3xOp3gFnPc3	její
chytrým	chytrý	k2eAgFnPc3d1	chytrá
investicím	investice	k1gFnPc3	investice
pár	pár	k4xCyI	pár
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
disponoval	disponovat	k5eAaBmAgMnS	disponovat
majetkem	majetek	k1gInSc7	majetek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
odcestovali	odcestovat	k5eAaPmAgMnP	odcestovat
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
John	John	k1gMnSc1	John
poznal	poznat	k5eAaPmAgMnS	poznat
rodinu	rodina	k1gFnSc4	rodina
Yoko	Yoko	k1gMnSc1	Yoko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sám	sám	k3xTgMnSc1	sám
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Hong	Honga	k1gFnPc2	Honga
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
Singapuru	Singapur	k1gInSc6	Singapur
a	a	k8xC	a
Kapského	kapský	k2eAgNnSc2d1	Kapské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Yoko	Yoko	k1gNnSc4	Yoko
složil	složit	k5eAaPmAgMnS	složit
mnoho	mnoho	k4c4	mnoho
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
I	i	k9	i
Want	Want	k1gMnSc1	Want
You	You	k1gMnSc1	You
(	(	kIx(	(
<g/>
She	She	k1gMnSc1	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
So	So	kA	So
Heavy	Heav	k1gInPc4	Heav
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Oh	oh	k0	oh
Yoko	Yoka	k1gMnSc5	Yoka
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
viděl	vidět	k5eAaImAgInS	vidět
naposledy	naposledy	k6eAd1	naposledy
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
1974.	[number]	k4	1974.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Drogová	drogový	k2eAgFnSc1d1	drogová
závislost	závislost	k1gFnSc1	závislost
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
závislost	závislost	k1gFnSc4	závislost
na	na	k7c4	na
povzbuzující	povzbuzující	k2eAgFnPc4d1	povzbuzující
látky	látka	k1gFnPc4	látka
fenmetrazin	fenmetrazina	k1gFnPc2	fenmetrazina
a	a	k8xC	a
amfetamin	amfetamin	k1gInSc1	amfetamin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
cigarety	cigareta	k1gFnPc4	cigareta
a	a	k8xC	a
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
začal	začít	k5eAaPmAgMnS	začít
užívat	užívat	k5eAaImF	užívat
LSD	LSD	kA	LSD
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
na	na	k7c6	na
tripech	trip	k1gInPc6	trip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
šňupání	šňupání	k1gNnSc4	šňupání
heroinu	heroin	k1gInSc2	heroin
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vyléčili	vyléčit	k5eAaPmAgMnP	vyléčit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znovu	znovu	k6eAd1	znovu
spadli	spadnout	k5eAaPmAgMnP	spadnout
do	do	k7c2	do
závislosti	závislost	k1gFnSc2	závislost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
heroinu	heroin	k1gInSc6	heroin
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
metadon	metadon	k1gInSc4	metadon
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
užívali	užívat	k5eAaImAgMnP	užívat
i	i	k8xC	i
hašiš	hašiš	k1gInSc4	hašiš
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
"	"	kIx"	"
<g/>
ztraceného	ztracený	k2eAgInSc2d1	ztracený
víkendu	víkend	k1gInSc2	víkend
<g/>
"	"	kIx"	"
v	v	k7c4	v
LA	la	k1gNnSc4	la
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1973	[number]	k4	1973
a	a	k8xC	a
1974	[number]	k4	1974
Lennon	Lennona	k1gFnPc2	Lennona
vydatně	vydatně	k6eAd1	vydatně
pil	pít	k5eAaImAgMnS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
Seana	Sean	k1gMnSc2	Sean
již	již	k6eAd1	již
užíval	užívat	k5eAaImAgInS	užívat
jen	jen	k9	jen
hašiš	hašiš	k1gInSc4	hašiš
a	a	k8xC	a
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
byl	být	k5eAaImAgMnS	být
Lennon	Lennon	k1gMnSc1	Lennon
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
držení	držení	k1gNnSc4	držení
hašiše	hašiš	k1gInSc2	hašiš
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
pokutě	pokuta	k1gFnSc3	pokuta
150	[number]	k4	150
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zatčení	zatčení	k1gNnSc1	zatčení
později	pozdě	k6eAd2	pozdě
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
USA	USA	kA	USA
k	k	k7c3	k
potížím	potíž	k1gFnPc3	potíž
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
drogách	droga	k1gFnPc6	droga
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Tomorrow	Tomorrow	k1gFnSc1	Tomorrow
Never	Never	k1gMnSc1	Never
Knows	Knows	k1gInSc1	Knows
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Happiness	Happiness	k1gInSc1	Happiness
is	is	k?	is
a	a	k8xC	a
Warm	Warm	k1gMnSc1	Warm
Gun	Gun	k1gMnSc1	Gun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Doctor	Doctor	k1gMnSc1	Doctor
Robert	Robert	k1gMnSc1	Robert
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
A	a	k8xC	a
Day	Day	k1gFnSc1	Day
in	in	k?	in
the	the	k?	the
Life	Life	k1gInSc1	Life
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cold	Cold	k1gInSc1	Cold
Turkey	Turkea	k1gFnSc2	Turkea
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
některými	některý	k3yIgFnPc7	některý
rádii	rádio	k1gNnPc7	rádio
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Lucy	Luca	k1gFnSc2	Luca
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
with	with	k1gMnSc1	with
Diamonds	Diamonds	k1gInSc1	Diamonds
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
drogy	droga	k1gFnPc4	droga
propagující	propagující	k2eAgNnSc4d1	propagující
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
psychedelická	psychedelický	k2eAgNnPc1d1	psychedelické
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
dávají	dávat	k5eAaImIp3nP	dávat
dohromady	dohromady	k6eAd1	dohromady
zkratku	zkratka	k1gFnSc4	zkratka
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Psaní	psaní	k1gNnSc1	psaní
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
básničky	básnička	k1gFnPc4	básnička
a	a	k8xC	a
kratičké	kratičký	k2eAgInPc4d1	kratičký
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
vlastními	vlastní	k2eAgFnPc7d1	vlastní
kresbami	kresba	k1gFnPc7	kresba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
náruživým	náruživý	k2eAgMnSc7d1	náruživý
čtenářem	čtenář	k1gMnSc7	čtenář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
vydal	vydat	k5eAaPmAgMnS	vydat
Lennon	Lennon	k1gMnSc1	Lennon
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
In	In	k1gMnPc1	In
His	his	k1gNnSc2	his
Own	Own	k1gMnSc5	Own
Write	Writ	k1gMnSc5	Writ
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
další	další	k2eAgFnSc4d1	další
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
A	a	k9	a
Spaniard	Spaniard	k1gMnSc1	Spaniard
in	in	k?	in
the	the	k?	the
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Skywriting	Skywriting	k1gInSc1	Skywriting
by	by	kYmCp3nS	by
Word	Word	kA	Word
of	of	k?	of
Mouth	Mouth	k1gInSc1	Mouth
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
aktivismus	aktivismus	k1gInSc1	aktivismus
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Lennonův	Lennonův	k2eAgInSc1d1	Lennonův
politický	politický	k2eAgInSc1d1	politický
aktivismus	aktivismus	k1gInSc1	aktivismus
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
k	k	k7c3	k
omluvě	omluva	k1gFnSc3	omluva
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
o	o	k7c6	o
Ježíšovi	Ježíš	k1gMnSc6	Ježíš
a	a	k8xC	a
Beatles	Beatles	k1gFnSc6	Beatles
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyjádření	vyjádření	k1gNnSc1	vyjádření
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
s	s	k7c7	s
kontextem	kontext	k1gInSc7	kontext
vyšumělo	vyšumět	k5eAaPmAgNnS	vyšumět
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k9	již
o	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Beatles	Beatles	k1gFnSc3	Beatles
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
oficiálně	oficiálně	k6eAd1	oficiálně
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
aktivismus	aktivismus	k1gInSc1	aktivismus
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgInS	stýkat
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
15.	[number]	k4	15.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
společně	společně	k6eAd1	společně
Lennon	Lennon	k1gNnSc1	Lennon
a	a	k8xC	a
Ono	onen	k3xDgNnSc4	onen
symbolicky	symbolicky	k6eAd1	symbolicky
zasadili	zasadit	k5eAaPmAgMnP	zasadit
žaludy	žalud	k1gInPc4	žalud
za	za	k7c4	za
mír	mír	k1gInSc4	mír
u	u	k7c2	u
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
také	také	k9	také
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
natočil	natočit	k5eAaBmAgMnS	natočit
LP	LP	kA	LP
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnPc2	Two
Virgins	Virgins	k1gInSc1	Virgins
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
obalu	obal	k1gInSc6	obal
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
gramofonová	gramofonový	k2eAgFnSc1d1	gramofonová
deska	deska	k1gFnSc1	deska
musela	muset	k5eAaImAgFnS	muset
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
hnědém	hnědý	k2eAgInSc6d1	hnědý
papírovém	papírový	k2eAgInSc6d1	papírový
sáčku	sáček	k1gInSc6	sáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
verdiktu	verdikt	k1gInSc2	verdikt
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Newarku	Newark	k1gInSc6	Newark
zabaveno	zabaven	k2eAgNnSc1d1	zabaveno
30 000	[number]	k4	30 000
kopií	kopie	k1gFnSc7	kopie
alba	album	k1gNnSc2	album
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
pornografii	pornografie	k1gFnSc3	pornografie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
26.	[number]	k4	26.
března	březen	k1gInSc2	březen
vzali	vzít	k5eAaPmAgMnP	vzít
odjeli	odjet	k5eAaPmAgMnP	odjet
pořádat	pořádat	k5eAaImF	pořádat
happening	happening	k1gInSc4	happening
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podnikli	podniknout	k5eAaPmAgMnP	podniknout
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
Bed-in	Bedn	k1gInSc4	Bed-in
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc4	týden
leželi	ležet	k5eAaImAgMnP	ležet
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
před	před	k7c4	před
zraky	zrak	k1gInPc4	zrak
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
visely	viset	k5eAaImAgFnP	viset
nápisy	nápis	k1gInPc4	nápis
"	"	kIx"	"
<g/>
Bed	Beda	k1gFnPc2	Beda
Peace	Peace	k1gFnSc2	Peace
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hair	Hair	k1gInSc1	Hair
Peace	Peaec	k1gInSc2	Peaec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
Bed-in	Bedn	k1gInSc4	Bed-in
poté	poté	k6eAd1	poté
chtěli	chtít	k5eAaImAgMnP	chtít
zopakovat	zopakovat	k5eAaPmF	zopakovat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lennon	Lennon	k1gMnSc1	Lennon
nezískal	získat	k5eNaPmAgMnS	získat
vízum	vízum	k1gNnSc4	vízum
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
odsouzení	odsouzení	k1gNnSc3	odsouzení
za	za	k7c4	za
držení	držení	k1gNnSc4	držení
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
odpluli	odplout	k5eAaPmAgMnP	odplout
na	na	k7c4	na
Bahamy	Bahamy	k1gFnPc4	Bahamy
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Hilton	Hilton	k1gInSc1	Hilton
svůj	svůj	k3xOyFgInSc4	svůj
manifest	manifest	k1gInSc4	manifest
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
Lennonovi	Lennonův	k2eAgMnPc1d1	Lennonův
strávili	strávit	k5eAaPmAgMnP	strávit
8	[number]	k4	8
dní	den	k1gInPc2	den
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
vzývali	vzývat	k5eAaImAgMnP	vzývat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
pořádali	pořádat	k5eAaImAgMnP	pořádat
mírové	mírový	k2eAgInPc4d1	mírový
dýchánky	dýchánek	k1gInPc4	dýchánek
a	a	k8xC	a
posledního	poslední	k2eAgInSc2d1	poslední
večera	večer	k1gInSc2	večer
společně	společně	k6eAd1	společně
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Timothy	Timotha	k1gMnSc2	Timotha
Leary	Leara	k1gMnSc2	Leara
<g/>
)	)	kIx)	)
nahráli	nahrát	k5eAaPmAgMnP	nahrát
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
kultovní	kultovní	k2eAgFnSc4d1	kultovní
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Give	Give	k1gFnSc4	Give
Peace	Peaec	k1gInSc2	Peaec
a	a	k8xC	a
Chance	Chanec	k1gInSc2	Chanec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Yoko	Yoko	k1gMnSc1	Yoko
tam	tam	k6eAd1	tam
také	také	k9	také
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc4	band
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
politicky	politicky	k6eAd1	politicky
angažovat	angažovat	k5eAaBmF	angažovat
a	a	k8xC	a
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
válkám	válka	k1gFnPc3	válka
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
bed-in	bedn	k1gMnSc1	bed-in
happeningy	happening	k1gInPc4	happening
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
hotelu	hotel	k1gInSc2	hotel
Sacher	Sachra	k1gFnPc2	Sachra
happening	happening	k1gInSc1	happening
"	"	kIx"	"
<g/>
bagismus	bagismus	k1gInSc1	bagismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
zraky	zrak	k1gInPc7	zrak
novinářů	novinář	k1gMnPc2	novinář
skryti	skrýt	k5eAaPmNgMnP	skrýt
pod	pod	k7c7	pod
bílým	bílý	k2eAgNnSc7d1	bílé
prostěradlem	prostěradlo	k1gNnSc7	prostěradlo
a	a	k8xC	a
odmítali	odmítat	k5eAaImAgMnP	odmítat
vylézt	vylézt	k5eAaPmF	vylézt
<g/>
.	.	kIx.	.
</s>
<s>
Říkali	říkat	k5eAaImAgMnP	říkat
tomu	ten	k3xDgMnSc3	ten
totální	totální	k2eAgFnSc4d1	totální
komunikace	komunikace	k1gFnSc1	komunikace
bez	bez	k7c2	bez
předsudků	předsudek	k1gInPc2	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
nápisy	nápis	k1gInPc4	nápis
"	"	kIx"	"
<g/>
Bed	Beda	k1gFnPc2	Beda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Peace	Peace	k1gFnSc1	Peace
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hair	Hair	k1gInSc1	Hair
Peace	Peaec	k1gInSc2	Peaec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
rozesílali	rozesílat	k5eAaImAgMnP	rozesílat
světovým	světový	k2eAgMnPc3d1	světový
politikům	politik	k1gMnPc3	politik
žaludy	žalud	k1gInPc4	žalud
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
zasadili	zasadit	k5eAaPmAgMnP	zasadit
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22.	[number]	k4	22.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
změnil	změnit	k5eAaPmAgMnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
jméno	jméno	k1gNnSc4	jméno
z	z	k7c2	z
Winston	Winston	k1gInSc4	Winston
na	na	k7c4	na
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nechtěl	chtít	k5eNaImAgMnS	chtít
být	být	k5eAaImF	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
válečném	válečný	k2eAgNnSc6d1	válečné
premiérovi	premiér	k1gMnSc3	premiér
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1969	[number]	k4	1969
podpořili	podpořit	k5eAaPmAgMnP	podpořit
stranu	strana	k1gFnSc4	strana
Černých	Černých	k2eAgMnPc2d1	Černých
panterů	panter	k1gMnPc2	panter
<g/>
,	,	kIx,	,
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
veřejně	veřejně	k6eAd1	veřejně
předali	předat	k5eAaPmAgMnP	předat
odstřižky	odstřižek	k1gInPc4	odstřižek
svých	svůj	k3xOyFgInPc2	svůj
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
vydražili	vydražit	k5eAaPmAgMnP	vydražit
a	a	k8xC	a
použili	použít	k5eAaPmAgMnP	použít
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Panteři	panter	k1gMnPc1	panter
jim	on	k3xPp3gMnPc3	on
zase	zase	k9	zase
darovali	darovat	k5eAaPmAgMnP	darovat
boxerské	boxerský	k2eAgFnPc4d1	boxerská
rukavice	rukavice	k1gFnPc4	rukavice
Muhammada	Muhammada	k1gFnSc1	Muhammada
Aliho	Ali	k1gMnSc2	Ali
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1969	[number]	k4	1969
Lennon	Lennon	k1gInSc1	Lennon
demonstrativně	demonstrativně	k6eAd1	demonstrativně
vrátil	vrátit	k5eAaPmAgInS	vrátit
řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
(	(	kIx(	(
<g/>
MBE	MBE	kA	MBE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
podporou	podpora	k1gFnSc7	podpora
zásahu	zásah	k1gInSc2	zásah
nigerijských	nigerijský	k2eAgNnPc2d1	nigerijské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Biafře	Biafra	k1gFnSc6	Biafra
<g/>
,	,	kIx,	,
v	v	k7c6	v
nigerijsko-biaferské	nigerijskoiaferský	k2eAgFnSc6d1	nigerijsko-biaferský
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Američanů	Američan	k1gMnPc2	Američan
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
cenzurou	cenzura	k1gFnSc7	cenzura
jeho	jeho	k3xOp3gFnSc2	jeho
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Cold	Cold	k1gInSc1	Cold
Turkey	Turkea	k1gFnSc2	Turkea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1969	[number]	k4	1969
ještě	ještě	k6eAd1	ještě
natočili	natočit	k5eAaBmAgMnP	natočit
film	film	k1gInSc4	film
o	o	k7c6	o
kontroverzním	kontroverzní	k2eAgInSc6d1	kontroverzní
případu	případ	k1gInSc6	případ
Jamese	Jamese	k1gFnSc2	Jamese
Hanrattyho	Hanratty	k1gMnSc2	Hanratty
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mnozí	mnohý	k2eAgMnPc1d1	mnohý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
nevinný	vinný	k2eNgMnSc1d1	nevinný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1971	[number]	k4	1971
nahrál	nahrát	k5eAaBmAgMnS	nahrát
revolucionářskou	revolucionářský	k2eAgFnSc4d1	revolucionářská
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Power	Power	k1gMnSc1	Power
to	ten	k3xDgNnSc4	ten
the	the	k?	the
People	People	k1gMnPc7	People
<g/>
"	"	kIx"	"
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podporovat	podporovat	k5eAaImF	podporovat
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
inklinovat	inklinovat	k5eAaImF	inklinovat
k	k	k7c3	k
maoismu	maoismus	k1gInSc3	maoismus
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Imagine	Imagin	k1gInSc5	Imagin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
intervenci	intervence	k1gFnSc3	intervence
Britů	Brit	k1gMnPc2	Brit
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
v	v	k7c6	v
Ulsteru	Ulster	k1gInSc6	Ulster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1971	[number]	k4	1971
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Ann	Ann	k1gMnSc1	Ann
Arbor	Arbor	k1gMnSc1	Arbor
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vůdce	vůdce	k1gMnSc2	vůdce
Bílých	bílý	k2eAgMnPc2d1	bílý
panterů	panter	k1gMnPc2	panter
Johna	John	k1gMnSc2	John
Sinclaira	Sinclair	k1gMnSc2	Sinclair
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
držení	držení	k1gNnSc4	držení
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Sinclair	Sinclair	k1gMnSc1	Sinclair
byl	být	k5eAaImAgMnS	být
omilostněn	omilostnit	k5eAaPmNgMnS	omilostnit
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
Lennon	Lennon	k1gInSc1	Lennon
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Apollo	Apollo	k1gNnSc1	Apollo
v	v	k7c4	v
Harlemu	Harlema	k1gFnSc4	Harlema
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
obětí	oběť	k1gFnPc2	oběť
vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
vzpoury	vzpoura	k1gFnSc2	vzpoura
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
Attica	Attic	k1gInSc2	Attic
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
demonstroval	demonstrovat	k5eAaBmAgInS	demonstrovat
s	s	k7c7	s
odbory	odbor	k1gInPc1	odbor
proti	proti	k7c3	proti
exportu	export	k1gInSc3	export
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
také	také	k9	také
znovu	znovu	k6eAd1	znovu
proti	proti	k7c3	proti
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
prezidentu	prezident	k1gMnSc3	prezident
Richardu	Richard	k1gMnSc3	Richard
Nixonovi	Nixon	k1gMnSc3	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
Yippies	Yippies	k1gMnSc1	Yippies
(	(	kIx(	(
<g/>
Youth	Youth	k1gMnSc1	Youth
International	International	k1gFnSc2	International
Party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
YIP	YIP	kA	YIP
<g/>
)	)	kIx)	)
Jerry	Jerra	k1gMnSc2	Jerra
Rubinem	Rubino	k1gNnSc7	Rubino
plánovali	plánovat	k5eAaImAgMnP	plánovat
putovní	putovní	k2eAgInSc4d1	putovní
festival	festival	k1gInSc4	festival
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
Nixonovu	Nixonův	k2eAgNnSc3d1	Nixonovo
znovuzvolení	znovuzvolení	k1gNnSc3	znovuzvolení
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
stopoval	stopovat	k5eAaImAgMnS	stopovat
Nixonovu	Nixonův	k2eAgFnSc4d1	Nixonova
předvolební	předvolební	k2eAgFnSc4d1	předvolební
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plánu	plán	k1gInSc2	plán
nakonec	nakonec	k6eAd1	nakonec
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1972	[number]	k4	1972
poté	poté	k6eAd1	poté
Lennon	Lennon	k1gMnSc1	Lennon
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
politické	politický	k2eAgNnSc4d1	politické
album	album	k1gNnSc4	album
Some	Som	k1gFnSc2	Som
Time	Time	k1gNnSc2	Time
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jeho	on	k3xPp3gInSc4	on
telefon	telefon	k1gInSc4	telefon
odposloucháván	odposloucháván	k2eAgInSc4d1	odposloucháván
FBI	FBI	kA	FBI
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Lennonovi	Lennon	k1gMnSc3	Lennon
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
prodloužení	prodloužení	k1gNnSc1	prodloužení
povolení	povolení	k1gNnPc2	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
soudně	soudně	k6eAd1	soudně
odvolával	odvolávat	k5eAaImAgMnS	odvolávat
po	po	k7c4	po
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
soud	soud	k1gInSc4	soud
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
vydána	vydán	k2eAgFnSc1d1	vydána
zelená	zelená	k1gFnSc1	zelená
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
abdikaci	abdikace	k1gFnSc4	abdikace
Nixona	Nixon	k1gMnSc2	Nixon
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
Seana	Sean	k1gMnSc2	Sean
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
politice	politika	k1gFnSc3	politika
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jen	jen	k9	jen
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
oslavy	oslava	k1gFnSc2	oslava
inaugurace	inaugurace	k1gFnSc2	inaugurace
prezidenta	prezident	k1gMnSc2	prezident
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Cartera	Carter	k1gMnSc2	Carter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Lennonovy	Lennonův	k2eAgFnPc1d1	Lennonova
písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Imagine	Imagin	k1gInSc5	Imagin
<g/>
,	,	kIx,	,
Strawberry	Strawberr	k1gInPc1	Strawberr
Fields	Fieldsa	k1gFnPc2	Fieldsa
Forever	Forevra	k1gFnPc2	Forevra
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
mezi	mezi	k7c7	mezi
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
písně	píseň	k1gFnPc4	píseň
hudební	hudební	k2eAgFnSc2d1	hudební
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
britská	britský	k2eAgFnSc1d1	britská
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
BBC	BBC	kA	BBC
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c4	o
100	[number]	k4	100
největších	veliký	k2eAgMnPc2d3	veliký
Britů	Brit	k1gMnPc2	Brit
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
8.	[number]	k4	8.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
liverpoolské	liverpoolský	k2eAgNnSc1d1	Liverpoolské
letiště	letiště	k1gNnSc1	letiště
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
Liverpool	Liverpool	k1gInSc1	Liverpool
(	(	kIx(	(
<g/>
Liverpool	Liverpool	k1gInSc1	Liverpool
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
Airport	Airport	k1gInSc1	Airport
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgNnSc7	první
letištěm	letiště	k1gNnSc7	letiště
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
pojmenovaném	pojmenovaný	k2eAgNnSc6d1	pojmenované
po	po	k7c6	po
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Používané	používaný	k2eAgInPc1d1	používaný
nástroje	nástroj	k1gInPc1	nástroj
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Rickenbacker	Rickenbacker	k1gInSc1	Rickenbacker
325	[number]	k4	325
C58	C58	k1gFnSc2	C58
–	–	k?	–
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
přírodní	přírodní	k2eAgInSc1d1	přírodní
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc4	model
který	který	k3yRgMnSc1	který
Lennon	Lennon	k1gMnSc1	Lennon
používal	používat	k5eAaImAgMnS	používat
od	od	k7c2	od
Hamburku	Hamburk	k1gInSc2	Hamburk
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
do	do	k7c2	do
Ed	Ed	k1gFnSc2	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
<g/>
:	:	kIx,	:
Gibson	Gibson	k1gMnSc1	Gibson
EJ-160	EJ-160	k1gMnSc1	EJ-160
–	–	k?	–
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
černo-hnědo-žlutý	černonědo-žlutý	k2eAgInSc1d1	černo-hnědo-žlutý
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byla	být	k5eAaImAgFnS	být
pomalován	pomalován	k2eAgInSc1d1	pomalován
na	na	k7c4	na
modro-růžovou	modroůžový	k2eAgFnSc4d1	modro-růžová
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c4	na
přírodní	přírodní	k2eAgInPc4d1	přírodní
<g/>
.	.	kIx.	.
</s>
<s>
Elektro-akustická	Elektrokustický	k2eAgFnSc1d1	Elektro-akustický
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
používal	používat	k5eAaImAgMnS	používat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
Rickenbacker	Rickenbacker	k1gInSc1	Rickenbacker
325	[number]	k4	325
C64	C64	k1gFnSc2	C64
–	–	k?	–
Černý	černý	k2eAgInSc1d1	černý
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostal	dostat	k5eAaPmAgInS	dostat
Lennon	Lennon	k1gInSc4	Lennon
darem	dar	k1gInSc7	dar
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Rickenbacker	Rickenbackra	k1gFnPc2	Rickenbackra
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
kytaře	kytara	k1gFnSc3	kytara
později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgMnS	dostat
12strunný	12strunný	k2eAgInSc4d1	12strunný
černý	černý	k2eAgInSc4d1	černý
model	model	k1gInSc4	model
Rickenbacker	Rickenbackra	k1gFnPc2	Rickenbackra
325/12	[number]	k4	325/12
C64	C64	k1gFnPc2	C64
a	a	k8xC	a
6strunný	6strunný	k2eAgInSc4d1	6strunný
červený	červený	k2eAgInSc4d1	červený
model	model	k1gInSc4	model
Rickenbacker	Rickenbackra	k1gFnPc2	Rickenbackra
325	[number]	k4	325
C96	C96	k1gFnPc2	C96
s	s	k7c7	s
F	F	kA	F
<g/>
-	-	kIx~	-
otvorem	otvor	k1gInSc7	otvor
jako	jako	k8xS	jako
rezervu	rezerva	k1gFnSc4	rezerva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
Epiphone	Epiphon	k1gMnSc5	Epiphon
E230	E230	k1gMnSc5	E230
TD	TD	kA	TD
Casino	Casino	k1gNnSc1	Casino
–	–	k?	–
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
černo-hnědo-žlutý	černonědo-žlutý	k2eAgInSc1d1	černo-hnědo-žlutý
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
tuto	tento	k3xDgFnSc4	tento
kytaru	kytara	k1gFnSc4	kytara
měl	mít	k5eAaImAgInS	mít
nejraději	rád	k6eAd3	rád
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
používal	používat	k5eAaImAgMnS	používat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
/	/	kIx~	/
<g/>
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	banda	k1gFnPc2	banda
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Imagine	Imagin	k1gMnSc5	Imagin
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
Sometime	Sometim	k1gMnSc5	Sometim
In	In	k1gMnSc5	In
New	New	k1gMnSc5	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Mind	Mind	k1gMnSc1	Mind
Games	Games	k1gMnSc1	Games
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
:	:	kIx,	:
Walls	Walls	k1gInSc1	Walls
and	and	k?	and
Bridges	Bridges	k1gInSc1	Bridges
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Roll	k1gInSc4	Roll
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
Double	double	k2eAgInPc1d1	double
Fantasy	fantas	k1gInPc1	fantas
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Milk	Milk	k1gInSc1	Milk
and	and	k?	and
Honey	Honea	k1gFnSc2	Honea
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Experimentální	experimentální	k2eAgNnPc4d1	experimentální
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnSc1	Two
Virgins	Virginsa	k1gFnPc2	Virginsa
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Life	Lif	k1gInSc2	Lif
with	with	k1gInSc1	with
the	the	k?	the
Lions	Lions	k1gInSc1	Lions
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Wedding	Wedding	k1gInSc1	Wedding
Album	album	k1gNnSc1	album
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Živé	živý	k2eAgFnPc1d1	živá
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
Peace	Peace	k1gFnSc1	Peace
in	in	k?	in
Toronto	Toronto	k1gNnSc1	Toronto
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnPc1	filmografie
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Beatles	Beatles	k1gFnSc4	Beatles
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
How	How	k?	How
I	i	k9	i
Won	won	k1gInSc4	won
the	the	k?	the
War	War	k1gFnSc2	War
(	(	kIx(	(
<g/>
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
válku	válka	k1gFnSc4	válka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
U.S.	U.S.	k1gMnSc1	U.S.
vs	vs	k?	vs
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
In	In	k?	In
His	his	k1gNnSc1	his
Own	Own	k1gMnSc5	Own
Write	Writ	k1gMnSc5	Writ
(	(	kIx(	(
<g/>
Písání	Písání	k1gNnSc2	Písání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spaniard	Spaniard	k1gInSc1	Spaniard
in	in	k?	in
the	the	k?	the
Works	Works	kA	Works
(	(	kIx(	(
<g/>
Španěl	Španěl	k1gMnSc1	Španěl
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skywriting	Skywriting	k1gInSc1	Skywriting
By	by	k9	by
Word	Word	kA	Word
of	of	k?	of
Mouth	Mouth	k1gInSc1	Mouth
(	(	kIx(	(
<g/>
Nanebepění	Nanebepění	k1gNnSc1	Nanebepění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CRAMPTON	CRAMPTON	kA	CRAMPTON
<g/>
,	,	kIx,	,
Luke	Luke	k1gNnSc2	Luke
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
192	[number]	k4	192
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-3-8365-1758-4	[number]	k4	978-3-8365-1758-4
</s>
</p>
<p>
<s>
MANFERTO	MANFERTO	kA	MANFERTO
<g/>
,	,	kIx,	,
Valeria	Valerium	k1gNnSc2	Valerium
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
268	[number]	k4	268
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-204-2256-9	[number]	k4	978-80-204-2256-9
</s>
</p>
<p>
<s>
THOMAS	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
Gareth	Gareth	k1gMnSc1	Gareth
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gMnSc1	Svojtka
<g/>
,	,	kIx,	,
2011.	[number]	k4	2011.
223	[number]	k4	223
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-256-0704-6	[number]	k4	80-256-0704-6
</s>
</p>
<p>
<s>
WENNER	WENNER	kA	WENNER
<g/>
,	,	kIx,	,
Jann	Jann	k1gMnSc1	Jann
S.	S.	kA	S.
Lennon	Lennon	k1gMnSc1	Lennon
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2002.	[number]	k4	2002.
147	[number]	k4	147
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7209-371-1	[number]	k4	80-7209-371-1
</s>
</p>
<p>
<s>
WOODALL	WOODALL	kA	WOODALL
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2000.	[number]	k4	2000.
100	[number]	k4	100
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7207-348-6	[number]	k4	80-7207-348-6
</s>
</p>
<p>
<s>
Bairdová	Bairdová	k1gFnSc1	Bairdová
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
<g/>
;	;	kIx,	;
Giuliano	Giuliana	k1gFnSc5	Giuliana
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
–	–	k?	–
můj	můj	k1gMnSc1	můj
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7004-072-6	[number]	k4	80-7004-072-6
</s>
</p>
<p>
<s>
Matzner	Matzner	k1gMnSc1	Matzner
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
:	:	kIx,	:
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
výpověď	výpověď	k1gFnSc1	výpověď
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
generaci	generace	k1gFnSc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
85618421	[number]	k4	85618421
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
39407088	[number]	k4	39407088
<g/>
;	;	kIx,	;
2.	[number]	k4	2.
dopl	doplum	k1gNnPc2	doplum
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-204-0581-X	[number]	k4	80-204-0581-X
</s>
</p>
<p>
<s>
Černá	Černá	k1gFnSc1	Černá
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
;	;	kIx,	;
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Poplach	poplach	k1gInSc1	poplach
kolem	kolem	k7c2	kolem
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
PANTON	PANTON	kA	PANTON
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
42161054	[number]	k4	42161054
</s>
</p>
<p>
<s>
Clifford	Clifford	k1gInSc1	Clifford
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
a	a	k8xC	a
spol.	spol.	k?	spol.
<g/>
:	:	kIx,	:
Album	album	k1gNnSc1	album
Rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladá	k1gFnSc2	mladá
letá	letý	k2eAgFnSc1d1	letá
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-06-00394-7	[number]	k4	80-06-00394-7
</s>
</p>
<p>
<s>
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Hvězdy	hvězda	k1gFnPc1	hvězda
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
PANTON	PANTON	kA	PANTON
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
ISBN	ISBN	kA	ISBN
OCLC	OCLC	kA	OCLC
24413638	[number]	k4	24413638
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
</s>
</p>
<p>
<s>
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
</s>
</p>
<p>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
</s>
</p>
<p>
<s>
Julian	Julian	k1gMnSc1	Julian
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
<s>
Sean	Sean	k1gMnSc1	Sean
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Lennon	Lennon	k1gNnSc4	Lennon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
brouci.com	brouci.com	k1gInSc4	brouci.com
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Beatles	Beatles	k1gFnPc2	Beatles
https://www.beatles-komplet.cz	[url]	k1gFnPc2	https://www.beatles-komplet.cz
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
potřebuješ	potřebovat	k5eAaImIp2nS	potřebovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc1	peníz
pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podrobné	podrobný	k2eAgFnPc4d1	podrobná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
názorech	názor	k1gInPc6	názor
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc2	Lennon
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
politickou	politický	k2eAgFnSc7d1	politická
orientací	orientace	k1gFnSc7	orientace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
