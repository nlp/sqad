<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
chemii	chemie	k1gFnSc4	chemie
dle	dle	k7c2	dle
PČP	PČP	kA	PČP
etanol	etanol	k1gInSc1	etanol
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ethylalkohol	ethylalkohol	k1gInSc4	ethylalkohol
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
špiritus	špiritus	k?	špiritus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejnižší	nízký	k2eAgInSc1d3	nejnižší
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
kapalinu	kapalina	k1gFnSc4	kapalina
ostré	ostrý	k2eAgFnSc3d1	ostrá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
zředění	zředění	k1gNnSc6	zředění
příjemné	příjemný	k2eAgFnSc2d1	příjemná
alkoholické	alkoholický	k2eAgFnSc2d1	alkoholická
vůně	vůně	k1gFnSc2	vůně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
zápalný	zápalný	k2eAgInSc1d1	zápalný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xC	jako
hořlavina	hořlavina	k1gFnSc1	hořlavina
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
tisíce	tisíc	k4xCgInPc4	tisíc
let	let	k1gInSc4	let
konzumován	konzumovat	k5eAaBmNgInS	konzumovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
ethanolu	ethanol	k1gInSc2	ethanol
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
zvyklý	zvyklý	k2eAgInSc1d1	zvyklý
ho	on	k3xPp3gMnSc4	on
přijímat	přijímat	k5eAaImF	přijímat
a	a	k8xC	a
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
tělesná	tělesný	k2eAgFnSc1d1	tělesná
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
dávkování	dávkování	k1gNnSc4	dávkování
relativní	relativní	k2eAgNnSc4d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
dávkách	dávka	k1gFnPc6	dávka
ethanol	ethanol	k1gInSc1	ethanol
krátkodobě	krátkodobě	k6eAd1	krátkodobě
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
euforii	euforie	k1gFnSc4	euforie
a	a	k8xC	a
pocit	pocit	k1gInSc4	pocit
uvolnění	uvolnění	k1gNnSc2	uvolnění
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
ztrátu	ztráta	k1gFnSc4	ztráta
koordinace	koordinace	k1gFnSc2	koordinace
pohybů	pohyb	k1gInPc2	pohyb
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
působením	působení	k1gNnPc3	působení
na	na	k7c4	na
mozeček	mozeček	k1gInSc4	mozeček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sníženou	snížený	k2eAgFnSc4d1	snížená
vnímavost	vnímavost	k1gFnSc4	vnímavost
<g/>
,	,	kIx,	,
sníženou	snížený	k2eAgFnSc4d1	snížená
reaktivitu	reaktivita	k1gFnSc4	reaktivita
<g/>
,	,	kIx,	,
útlum	útlum	k1gInSc4	útlum
rozumových	rozumový	k2eAgFnPc2d1	rozumová
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
agresivitu	agresivita	k1gFnSc4	agresivita
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
mechanismus	mechanismus	k1gInSc4	mechanismus
účinku	účinek	k1gInSc2	účinek
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
depresiva	depresivum	k1gNnPc4	depresivum
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k9	třeba
heroin	heroin	k1gInSc1	heroin
<g/>
,	,	kIx,	,
opium	opium	k1gNnSc1	opium
<g/>
,	,	kIx,	,
kodein	kodein	k1gInSc1	kodein
<g/>
,	,	kIx,	,
fentanyl	fentanyl	k1gInSc1	fentanyl
nebo	nebo	k8xC	nebo
GHB	GHB	kA	GHB
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
euforie	euforie	k1gFnSc1	euforie
i	i	k8xC	i
útlum	útlum	k1gInSc1	útlum
organismu	organismus	k1gInSc2	organismus
u	u	k7c2	u
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
látek	látka	k1gFnPc2	látka
bývá	bývat	k5eAaImIp3nS	bývat
intenzivnější	intenzivní	k2eAgFnSc1d2	intenzivnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intoxikovaný	intoxikovaný	k2eAgMnSc1d1	intoxikovaný
jedinec	jedinec	k1gMnSc1	jedinec
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
úbytek	úbytek	k1gInSc4	úbytek
myšlenek	myšlenka	k1gFnPc2	myšlenka
na	na	k7c4	na
stres	stres	k1gInSc4	stres
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
opomíjet	opomíjet	k5eAaImF	opomíjet
každodenních	každodenní	k2eAgFnPc2d1	každodenní
starost	starost	k1gFnSc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
antagonista	antagonista	k1gMnSc1	antagonista
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
blokátor	blokátor	k1gInSc1	blokátor
<g/>
"	"	kIx"	"
receptorů	receptor	k1gMnPc2	receptor
GABA	GABA	kA	GABA
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
příčinou	příčina	k1gFnSc7	příčina
"	"	kIx"	"
<g/>
opilosti	opilost	k1gFnSc2	opilost
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
euforie	euforie	k1gFnPc4	euforie
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
bezstarostnosti	bezstarostnost	k1gFnSc2	bezstarostnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgInSc4d2	významnější
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
psychiku	psychika	k1gFnSc4	psychika
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
jeho	jeho	k3xOp3gInSc4	jeho
metabolit	metabolit	k1gInSc4	metabolit
acetaldehyd	acetaldehyd	k1gInSc1	acetaldehyd
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
ethanová	ethanový	k2eAgFnSc1d1	ethanový
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
octová	octový	k2eAgFnSc1d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dle	dle	k7c2	dle
Českého	český	k2eAgInSc2d1	český
lékopisu	lékopis	k1gInSc2	lékopis
===	===	k?	===
</s>
</p>
<p>
<s>
Ethanolum	Ethanolum	k1gInSc1	Ethanolum
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
(	(	kIx(	(
<g/>
spiritus	spiritus	k1gInSc1	spiritus
dilutus	dilutus	k1gInSc1	dilutus
<g/>
,	,	kIx,	,
spiritus	spiritus	k1gInSc1	spiritus
vini	vini	k1gNnSc1	vini
dilutus	dilutus	k1gInSc1	dilutus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ethanolum	Ethanolum	k1gInSc1	Ethanolum
85	[number]	k4	85
<g/>
%	%	kIx~	%
<g/>
(	(	kIx(	(
<g/>
spiritus	spiritus	k1gInSc1	spiritus
concentratus	concentratus	k1gInSc1	concentratus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ethanolum	Ethanolum	k1gInSc1	Ethanolum
96	[number]	k4	96
<g/>
%	%	kIx~	%
<g/>
(	(	kIx(	(
<g/>
spiritus	spiritus	k1gInSc1	spiritus
96	[number]	k4	96
<g/>
%	%	kIx~	%
,	,	kIx,	,
líh	líh	k1gInSc1	líh
96	[number]	k4	96
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ethanolum	Ethanolum	k1gInSc1	Ethanolum
anhydricum	anhydricum	k1gInSc1	anhydricum
(	(	kIx(	(
<g/>
spiritus	spiritus	k1gInSc1	spiritus
absolutus	absolutus	k1gInSc1	absolutus
<g/>
,	,	kIx,	,
bezvodý	bezvodý	k2eAgInSc1d1	bezvodý
líh	líh	k1gInSc1	líh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
===	===	k?	===
</s>
</p>
<p>
<s>
líh	líh	k1gInSc1	líh
koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
–	–	k?	–
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
%	%	kIx~	%
</s>
</p>
<p>
<s>
líh	líh	k1gInSc1	líh
absolutní	absolutní	k2eAgInSc1d1	absolutní
–	–	k?	–
99,4	[number]	k4	99,4
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
produkce	produkce	k1gFnSc2	produkce
ethanolu	ethanol	k1gInSc2	ethanol
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
sacharidů	sacharid	k1gInPc2	sacharid
(	(	kIx(	(
<g/>
cukrů	cukr	k1gInPc2	cukr
<g/>
)	)	kIx)	)
alkoholovým	alkoholový	k2eAgNnSc7d1	alkoholové
kvašením	kvašení	k1gNnSc7	kvašení
působením	působení	k1gNnSc7	působení
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
různých	různý	k2eAgInPc2d1	různý
šlechtěných	šlechtěný	k2eAgInPc2d1	šlechtěný
kmenů	kmen	k1gInPc2	kmen
druhu	druh	k1gInSc2	druh
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisia	k1gFnSc2	cerevisia
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jak	jak	k6eAd1	jak
cukerného	cukerný	k2eAgInSc2d1	cukerný
roztoku	roztok	k1gInSc2	roztok
(	(	kIx(	(
<g/>
o	o	k7c6	o
maximální	maximální	k2eAgFnSc6d1	maximální
koncentraci	koncentrace	k1gFnSc6	koncentrace
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
přímo	přímo	k6eAd1	přímo
přírodních	přírodní	k2eAgFnPc2d1	přírodní
surovin	surovina	k1gFnPc2	surovina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
sacharidy	sacharid	k1gInPc4	sacharid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvasný	kvasný	k2eAgInSc1d1	kvasný
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
sumární	sumární	k2eAgFnSc2d1	sumární
rovnice	rovnice	k1gFnSc2	rovnice
</s>
</p>
<p>
<s>
C6H12O6	C6H12O6	k4	C6H12O6
→	→	k?	→
2	[number]	k4	2
C2H5OH	C2H5OH	k1gFnPc2	C2H5OH
+	+	kIx~	+
2	[number]	k4	2
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Kvalita	kvalita	k1gFnSc1	kvalita
takto	takto	k6eAd1	takto
získaného	získaný	k2eAgInSc2d1	získaný
ethanolu	ethanol	k1gInSc2	ethanol
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
výchozí	výchozí	k2eAgFnSc6d1	výchozí
surovině	surovina	k1gFnSc6	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Kvašením	kvašení	k1gNnSc7	kvašení
vzniká	vznikat	k5eAaImIp3nS	vznikat
zápara	zápara	k1gFnSc1	zápara
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
velmi	velmi	k6eAd1	velmi
zředěný	zředěný	k2eAgInSc1d1	zředěný
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
ethanolu	ethanol	k1gInSc2	ethanol
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vždy	vždy	k6eAd1	vždy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nežádoucí	žádoucí	k2eNgFnSc3d1	nežádoucí
příměsi	příměs	k1gFnSc3	příměs
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
přiboudliny	přiboudlina	k1gFnPc1	přiboudlina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vyšší	vysoký	k2eAgInPc1d2	vyšší
alkoholy	alkohol	k1gInPc1	alkohol
(	(	kIx(	(
<g/>
propan-	propan-	k?	propan-
<g/>
1	[number]	k4	1
<g/>
-ol	l	k?	-ol
a	a	k8xC	a
isopropylalkohol	isopropylalkohol	k1gInSc1	isopropylalkohol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vícesytné	vícesytný	k2eAgInPc1d1	vícesytný
alkoholy	alkohol	k1gInPc1	alkohol
(	(	kIx(	(
<g/>
glycerol	glycerol	k1gInSc1	glycerol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ketony	keton	k1gInPc1	keton
(	(	kIx(	(
<g/>
aceton	aceton	k1gInSc1	aceton
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Čištění	čištění	k1gNnSc1	čištění
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
na	na	k7c6	na
výkonných	výkonný	k2eAgFnPc6d1	výkonná
destilačních	destilační	k2eAgFnPc6d1	destilační
kolonách	kolona	k1gFnPc6	kolona
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
absolutní	absolutní	k2eAgInSc4d1	absolutní
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
95,57	[number]	k4	95,57
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
4,43	[number]	k4	4,43
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
vody	voda	k1gFnSc2	voda
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
destilací	destilace	k1gFnSc7	destilace
s	s	k7c7	s
bezvodým	bezvodý	k2eAgInSc7d1	bezvodý
síranem	síran	k1gInSc7	síran
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
nebo	nebo	k8xC	nebo
oxidem	oxid	k1gInSc7	oxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vodu	voda	k1gFnSc4	voda
vážou	vázat	k5eAaImIp3nP	vázat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
působením	působení	k1gNnSc7	působení
hygroskopických	hygroskopický	k2eAgFnPc2d1	hygroskopická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
bezvodého	bezvodý	k2eAgInSc2d1	bezvodý
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
(	(	kIx(	(
<g/>
potaše	potaš	k1gInSc2	potaš
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bezvodého	bezvodý	k2eAgInSc2d1	bezvodý
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
(	(	kIx(	(
<g/>
modré	modrý	k2eAgFnSc2d1	modrá
skalice	skalice	k1gFnSc2	skalice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
postupy	postup	k1gInPc7	postup
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
ethanol	ethanol	k1gInSc4	ethanol
o	o	k7c6	o
čistotě	čistota	k1gFnSc6	čistota
až	až	k9	až
99,9	[number]	k4	99,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
metodou	metoda	k1gFnSc7	metoda
získávání	získávání	k1gNnSc2	získávání
co	co	k9	co
nejčistšího	čistý	k2eAgInSc2d3	nejčistší
ethanolu	ethanol	k1gInSc2	ethanol
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
azeotropická	azeotropický	k2eAgFnSc1d1	azeotropický
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
destilaci	destilace	k1gFnSc6	destilace
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
benzínu	benzín	k1gInSc2	benzín
nebo	nebo	k8xC	nebo
benzenu	benzen	k1gInSc2	benzen
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
produkt	produkt	k1gInSc4	produkt
o	o	k7c6	o
čistotě	čistota	k1gFnSc6	čistota
až	až	k9	až
99,7	[number]	k4	99,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synteticky	synteticky	k6eAd1	synteticky
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
připravuje	připravovat	k5eAaImIp3nS	připravovat
katalytickou	katalytický	k2eAgFnSc7d1	katalytická
hydratací	hydratace	k1gFnSc7	hydratace
ethenu	ethen	k1gInSc2	ethen
(	(	kIx(	(
<g/>
etylenu	etylen	k1gInSc2	etylen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
OHJako	OHJako	k1gNnSc1	OHJako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kyselina	kyselina	k1gFnSc1	kyselina
trihydrogenfosforečná	trihydrogenfosforečný	k2eAgFnSc1d1	trihydrogenfosforečná
na	na	k7c6	na
oxidu	oxid	k1gInSc6	oxid
křemičitém	křemičitý	k2eAgInSc6d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
připravený	připravený	k2eAgInSc1d1	připravený
ethanol	ethanol	k1gInSc1	ethanol
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
nečistot	nečistota	k1gFnPc2	nečistota
než	než	k8xS	než
kvasný	kvasný	k2eAgInSc1d1	kvasný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kvalitnější	kvalitní	k2eAgNnSc1d2	kvalitnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
syntetické	syntetický	k2eAgFnSc2d1	syntetická
přípravy	příprava	k1gFnSc2	příprava
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
katalytické	katalytický	k2eAgFnSc6d1	katalytická
hydrogenaci	hydrogenace	k1gFnSc6	hydrogenace
acetaldehydu	acetaldehyd	k1gInSc2	acetaldehyd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
hydratací	hydratace	k1gFnSc7	hydratace
acetylenu	acetylen	k1gInSc2	acetylen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
HC	HC	kA	HC
<g/>
≡	≡	k?	≡
<g/>
CH	Ch	kA	Ch
+	+	kIx~	+
H2	H2	k1gMnSc1	H2
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
CHO	cho	k0	cho
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
CHO	cho	k0	cho
+	+	kIx~	+
H2	H2	k1gMnSc3	H2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OH	OH	kA	OH
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Prostorovou	prostorový	k2eAgFnSc4d1	prostorová
geometrii	geometrie	k1gFnSc4	geometrie
molekuly	molekula	k1gFnSc2	molekula
ethanolu	ethanol	k1gInSc2	ethanol
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
obrázek	obrázek	k1gInSc1	obrázek
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
zachována	zachován	k2eAgFnSc1d1	zachována
volná	volný	k2eAgFnSc1d1	volná
otáčivost	otáčivost	k1gFnSc1	otáčivost
podle	podle	k7c2	podle
σ	σ	k?	σ
mezi	mezi	k7c7	mezi
uhlíkovými	uhlíkový	k2eAgInPc7d1	uhlíkový
atomy	atom	k1gInPc7	atom
i	i	k9	i
mezi	mezi	k7c7	mezi
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
konformaci	konformace	k1gFnSc4	konformace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
nejsou	být	k5eNaImIp3nP	být
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
na	na	k7c6	na
sousedních	sousední	k2eAgInPc6d1	sousední
atomech	atom	k1gInPc6	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kyslíku	kyslík	k1gInSc2	kyslík
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
hydroxylové	hydroxylový	k2eAgFnSc2d1	hydroxylová
skupiny	skupina	k1gFnSc2	skupina
OH	OH	kA	OH
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
atom	atom	k1gInSc4	atom
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
vodíkovou	vodíkový	k2eAgFnSc4d1	vodíková
vazbu	vazba	k1gFnSc4	vazba
s	s	k7c7	s
kyslíkovým	kyslíkový	k2eAgInSc7d1	kyslíkový
atomem	atom	k1gInSc7	atom
jiné	jiný	k2eAgFnSc2d1	jiná
molekuly	molekula	k1gFnSc2	molekula
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc4	bod
varu	var	k1gInSc2	var
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
očekávat	očekávat	k5eAaImF	očekávat
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
molekulové	molekulový	k2eAgFnSc3d1	molekulová
hmotnosti	hmotnost	k1gFnSc3	hmotnost
(	(	kIx(	(
<g/>
78,3	[number]	k4	78,3
°	°	k?	°
<g/>
C	C	kA	C
místo	místo	k1gNnSc1	místo
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
u	u	k7c2	u
nepolárních	polární	k2eNgFnPc2d1	nepolární
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
hmotný	hmotný	k2eAgInSc4d1	hmotný
propan	propan	k1gInSc4	propan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
u	u	k7c2	u
polárních	polární	k2eAgFnPc2d1	polární
látek	látka	k1gFnPc2	látka
bez	bez	k7c2	bez
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
dimethylether	dimethylethra	k1gFnPc2	dimethylethra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
vazby	vazba	k1gFnPc1	vazba
C	C	kA	C
<g/>
–	–	k?	–
<g/>
O	o	k7c4	o
i	i	k8xC	i
O	o	k7c4	o
<g/>
–	–	k?	–
<g/>
H	H	kA	H
jsou	být	k5eAaImIp3nP	být
polární	polární	k2eAgFnSc4d1	polární
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
molekula	molekula	k1gFnSc1	molekula
ethanolu	ethanol	k1gInSc2	ethanol
polární	polární	k2eAgFnSc1d1	polární
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
jen	jen	k9	jen
špatně	špatně	k6eAd1	špatně
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
;	;	kIx,	;
s	s	k7c7	s
polárními	polární	k2eAgNnPc7d1	polární
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
mísí	mísit	k5eAaImIp3nP	mísit
neomezeně	omezeně	k6eNd1	omezeně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tzv.	tzv.	kA	tzv.
azeotropickou	azeotropický	k2eAgFnSc4d1	azeotropický
směs	směs	k1gFnSc4	směs
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
95,57	[number]	k4	95,57
hmotnostních	hmotnostní	k2eAgNnPc2d1	hmotnostní
procent	procento	k1gNnPc2	procento
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
která	který	k3yIgFnSc1	který
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
1013	[number]	k4	1013
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
vře	vřít	k5eAaImIp3nS	vřít
při	při	k7c6	při
78,1	[number]	k4	78,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
složení	složení	k1gNnSc1	složení
plynné	plynný	k2eAgFnSc2d1	plynná
a	a	k8xC	a
kapalné	kapalný	k2eAgFnSc2d1	kapalná
fáze	fáze	k1gFnSc2	fáze
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
směs	směs	k1gFnSc1	směs
tohoto	tento	k3xDgNnSc2	tento
složení	složení	k1gNnSc2	složení
již	již	k9	již
nedá	dát	k5eNaPmIp3nS	dát
další	další	k2eAgFnSc7d1	další
destilací	destilace	k1gFnSc7	destilace
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
ethanolu	ethanol	k1gInSc2	ethanol
===	===	k?	===
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
poměrně	poměrně	k6eAd1	poměrně
reaktivní	reaktivní	k2eAgFnSc1d1	reaktivní
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
bouřlivě	bouřlivě	k6eAd1	bouřlivě
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
kovy	kov	k1gInPc7	kov
(	(	kIx(	(
<g/>
sodíkem	sodík	k1gInSc7	sodík
nebo	nebo	k8xC	nebo
draslíkem	draslík	k1gInSc7	draslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
schéma	schéma	k1gNnSc1	schéma
reakce	reakce	k1gFnSc2	reakce
ethanolu	ethanol	k1gInSc2	ethanol
s	s	k7c7	s
kovem	kov	k1gInSc7	kov
vypadá	vypadat	k5eAaImIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ethanol	ethanol	k1gInSc1	ethanol
+	+	kIx~	+
kov	kov	k1gInSc1	kov
→	→	k?	→
ethoxid	ethoxid	k1gInSc1	ethoxid
(	(	kIx(	(
<g/>
ethanolát	ethanolát	k1gInSc1	ethanolát
<g/>
)	)	kIx)	)
+	+	kIx~	+
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
<g/>
Reakce	reakce	k1gFnSc1	reakce
ethanolu	ethanol	k1gInSc2	ethanol
se	s	k7c7	s
sodíkem	sodík	k1gInSc7	sodík
probíhá	probíhat	k5eAaImIp3nS	probíhat
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
C2H5OH	C2H5OH	k1gFnSc1	C2H5OH
+	+	kIx~	+
2	[number]	k4	2
Na	na	k7c6	na
→	→	k?	→
2	[number]	k4	2
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
ONa	onen	k3xDgFnSc1	onen
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
unikající	unikající	k2eAgInSc4d1	unikající
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
ethoxid	ethoxid	k1gInSc4	ethoxid
sodný	sodný	k2eAgInSc4d1	sodný
(	(	kIx(	(
<g/>
ethanolát	ethanolát	k1gInSc4	ethanolát
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
draslíkem	draslík	k1gInSc7	draslík
by	by	kYmCp3nS	by
analogicky	analogicky	k6eAd1	analogicky
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ethoxid	ethoxid	k1gInSc1	ethoxid
draselný	draselný	k2eAgInSc1d1	draselný
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Ethoxidy	Ethoxida	k1gFnPc1	Ethoxida
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
bazické	bazický	k2eAgFnPc1d1	bazická
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
a	a	k8xC	a
hydroxid	hydroxid	k1gInSc4	hydroxid
příslušného	příslušný	k2eAgInSc2d1	příslušný
alkalického	alkalický	k2eAgInSc2d1	alkalický
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
ONa	onen	k3xDgFnSc1	onen
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
C2H5OH	C2H5OH	k1gMnSc1	C2H5OH
+	+	kIx~	+
NaOH	NaOH	k1gMnSc1	NaOH
<g/>
.	.	kIx.	.
<g/>
Působením	působení	k1gNnSc7	působení
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
manganistanu	manganistan	k1gInSc2	manganistan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
</s>
</p>
<p>
<s>
2	[number]	k4	2
CH3CH2OH	CH3CH2OH	k1gFnSc1	CH3CH2OH
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
O	O	kA	O
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
Odalší	Odalší	k2eAgFnSc7d1	Odalší
šetrnou	šetrný	k2eAgFnSc7d1	šetrná
oxidací	oxidace	k1gFnSc7	oxidace
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
až	až	k9	až
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
O	O	kA	O
+	+	kIx~	+
O	o	k7c6	o
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COOH	COOH	kA	COOH
<g/>
.	.	kIx.	.
<g/>
Přeměna	přeměna	k1gFnSc1	přeměna
ethanolu	ethanol	k1gInSc2	ethanol
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc7d1	octová
probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
působením	působení	k1gNnSc7	působení
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
rodu	rod	k1gInSc2	rod
Acetobacter	Acetobacter	k1gInSc1	Acetobacter
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
octové	octový	k2eAgNnSc1d1	octové
kvašení	kvašení	k1gNnSc1	kvašení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
octa	ocet	k1gInSc2	ocet
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupná	postupný	k2eAgFnSc1d1	postupná
oxidace	oxidace	k1gFnSc1	oxidace
ethanolu	ethanol	k1gInSc2	ethanol
je	být	k5eAaImIp3nS	být
také	také	k9	také
podstatou	podstata	k1gFnSc7	podstata
jeho	jeho	k3xOp3gNnSc2	jeho
odbourávání	odbourávání	k1gNnSc2	odbourávání
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
<g/>
;	;	kIx,	;
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
acetaldehyd	acetaldehyd	k1gInSc1	acetaldehyd
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
kocoviny	kocovina	k1gFnSc2	kocovina
po	po	k7c6	po
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
požití	požití	k1gNnSc6	požití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
také	také	k9	také
bouřlivě	bouřlivě	k6eAd1	bouřlivě
jako	jako	k8xC	jako
hoření	hoření	k1gNnSc6	hoření
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
až	až	k9	až
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CH3CH2OH	CH3CH2OH	k4	CH3CH2OH
+	+	kIx~	+
3	[number]	k4	3
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
3	[number]	k4	3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Působením	působení	k1gNnSc7	působení
halogenovodíků	halogenovodík	k1gInPc2	halogenovodík
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
náhradě	náhrada	k1gFnSc3	náhrada
hydroxylové	hydroxylový	k2eAgFnSc2d1	hydroxylová
skupiny	skupina	k1gFnSc2	skupina
příslušným	příslušný	k2eAgInSc7d1	příslušný
halogenovým	halogenový	k2eAgInSc7d1	halogenový
atomem	atom	k1gInSc7	atom
<g/>
;	;	kIx,	;
např.	např.	kA	např.
účinkem	účinek	k1gInSc7	účinek
bromovodíku	bromovodík	k1gInSc2	bromovodík
vzniká	vznikat	k5eAaImIp3nS	vznikat
bromethan	bromethan	k1gInSc1	bromethan
(	(	kIx(	(
<g/>
etyhylbromid	etyhylbromid	k1gInSc1	etyhylbromid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
</s>
</p>
<p>
<s>
C2H5OH	C2H5OH	k4	C2H5OH
+	+	kIx~	+
HBr	HBr	k1gFnSc1	HBr
→	→	k?	→
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
Br	br	k0	br
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
směsi	směs	k1gFnSc2	směs
bromidu	bromid	k1gInSc2	bromid
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
</s>
</p>
<p>
<s>
C2H5OH	C2H5OH	k4	C2H5OH
+	+	kIx~	+
KBr	KBr	k1gFnSc1	KBr
+	+	kIx~	+
H2SO4	H2SO4	k1gFnSc1	H2SO4
→	→	k?	→
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
Br	br	k0	br
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
KHSO	KHSO	kA	KHSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
reakce	reakce	k1gFnSc1	reakce
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
končit	končit	k5eAaImF	končit
vznikem	vznik	k1gInSc7	vznik
ethylbromidu	ethylbromid	k1gInSc2	ethylbromid
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ten	ten	k3xDgInSc4	ten
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
molekulou	molekula	k1gFnSc7	molekula
ethanolu	ethanol	k1gInSc2	ethanol
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
diethyletheru	diethylether	k1gInSc2	diethylether
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
Br	br	k0	br
+	+	kIx~	+
C2H5OH	C2H5OH	k1gMnSc3	C2H5OH
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
O	o	k7c4	o
<g/>
–	–	k?	–
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
+	+	kIx~	+
HBr	HBr	k1gFnSc2	HBr
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
vzniká	vznikat	k5eAaImIp3nS	vznikat
diethylether	diethylethra	k1gFnPc2	diethylethra
i	i	k8xC	i
působením	působení	k1gNnSc7	působení
jiných	jiný	k2eAgFnPc2d1	jiná
silných	silný	k2eAgFnPc2d1	silná
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
nebo	nebo	k8xC	nebo
některých	některý	k3yIgFnPc2	některý
hydrogensolí	hydrogensole	k1gFnPc2	hydrogensole
<g/>
,	,	kIx,	,
jako	jako	k9	jako
hydrogensíranu	hydrogensíran	k1gInSc2	hydrogensíran
draselného	draselný	k2eAgInSc2d1	draselný
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Působením	působení	k1gNnSc7	působení
halogenidů	halogenid	k1gInPc2	halogenid
karboxylových	karboxylový	k2eAgFnPc2d1	karboxylová
kyselin	kyselina	k1gFnPc2	kyselina
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
vznikají	vznikat	k5eAaImIp3nP	vznikat
estery	ester	k1gInPc4	ester
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
příslušné	příslušný	k2eAgFnSc2d1	příslušná
organické	organický	k2eAgFnSc2d1	organická
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
acetylchloridem	acetylchlorid	k1gInSc7	acetylchlorid
vzniká	vznikat	k5eAaImIp3nS	vznikat
ethylacetát	ethylacetát	k1gInSc1	ethylacetát
</s>
</p>
<p>
<s>
C2H5OH	C2H5OH	k4	C2H5OH
+	+	kIx~	+
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COCl	COCl	k1gInSc1	COCl
→	→	k?	→
CH3COOCH2CH3	CH3COOCH2CH3	k1gFnSc1	CH3COOCH2CH3
+	+	kIx~	+
HCl	HCl	k1gFnSc1	HCl
<g/>
.	.	kIx.	.
<g/>
Estery	ester	k1gInPc1	ester
ethanolu	ethanol	k1gInSc2	ethanol
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
přímou	přímý	k2eAgFnSc7d1	přímá
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
karboxylovými	karboxylový	k2eAgFnPc7d1	karboxylová
kyselinami	kyselina	k1gFnPc7	kyselina
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
váže	vázat	k5eAaImIp3nS	vázat
vznikající	vznikající	k2eAgFnSc4d1	vznikající
vodu	voda	k1gFnSc4	voda
</s>
</p>
<p>
<s>
C2H5OH	C2H5OH	k4	C2H5OH
+	+	kIx~	+
CH3COOH	CH3COOH	k1gMnSc1	CH3COOH
→	→	k?	→
CH3COOCH2CH3	CH3COOCH2CH3	k1gMnSc1	CH3COOCH2CH3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
niklových	niklový	k2eAgInPc2d1	niklový
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
220	[number]	k4	220
°	°	k?	°
<g/>
C	C	kA	C
vzniká	vznikat	k5eAaImIp3nS	vznikat
směs	směs	k1gFnSc1	směs
ethylaminů	ethylamin	k1gInPc2	ethylamin
</s>
</p>
<p>
<s>
CH3CH2OH	CH3CH2OH	k4	CH3CH2OH
+	+	kIx~	+
NH3	NH3	k1gMnSc1	NH3
→	→	k?	→
CH3CH2NH2	CH3CH2NH2	k1gMnSc1	CH3CH2NH2
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
,	,	kIx,	,
</s>
</p>
<p>
<s>
CH3CH2OH	CH3CH2OH	k4	CH3CH2OH
+	+	kIx~	+
CH3CH2NH2	CH3CH2NH2	k1gFnSc1	CH3CH2NH2
→	→	k?	→
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
NH	NH	kA	NH
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
CH3CH2OH	CH3CH2OH	k4	CH3CH2OH
+	+	kIx~	+
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
NH	NH	kA	NH
→	→	k?	→
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
3N	[number]	k4	3N
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Katalytickým	katalytický	k2eAgNnPc3d1	katalytické
odštěpením	odštěpení	k1gNnPc3	odštěpení
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
ethanolu	ethanol	k1gInSc2	ethanol
vzniká	vznikat	k5eAaImIp3nS	vznikat
ethen	ethen	k2eAgMnSc1d1	ethen
<g/>
,	,	kIx,	,
nejjednodušší	jednoduchý	k2eAgMnSc1d3	nejjednodušší
zástupce	zástupce	k1gMnSc1	zástupce
alkenů	alken	k1gMnPc2	alken
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OH	OH	kA	OH
→	→	k?	→
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
použitím	použití	k1gNnSc7	použití
ethanolu	ethanol	k1gInSc2	ethanol
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
výkonu	výkon	k1gInSc2	výkon
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
jako	jako	k8xC	jako
přídavek	přídavka	k1gFnPc2	přídavka
do	do	k7c2	do
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
farmacii	farmacie	k1gFnSc6	farmacie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jodu	jod	k1gInSc2	jod
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
jodová	jodový	k2eAgFnSc1d1	jodová
tinktura	tinktura	k1gFnSc1	tinktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
extrakci	extrakce	k1gFnSc4	extrakce
nebo	nebo	k8xC	nebo
čištění	čištění	k1gNnSc4	čištění
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
léčivých	léčivý	k2eAgFnPc2d1	léčivá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
některých	některý	k3yIgInPc2	některý
kapalných	kapalný	k2eAgInPc2d1	kapalný
přípravků	přípravek	k1gInPc2	přípravek
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
i	i	k8xC	i
vnější	vnější	k2eAgNnSc4d1	vnější
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
neřídit	řídit	k5eNaImF	řídit
motorová	motorový	k2eAgNnPc1d1	motorové
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
desinfekci	desinfekce	k1gFnSc3	desinfekce
neporaněné	poraněný	k2eNgFnSc2d1	neporaněná
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kosmetiky	kosmetika	k1gFnSc2	kosmetika
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
deodorantů	deodorant	k1gInPc2	deodorant
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
čisticích	čisticí	k2eAgInPc2d1	čisticí
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Okeny	Oken	k1gInPc7	Oken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dalších	další	k2eAgFnPc2d1	další
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kyseliny	kyselina	k1gFnPc1	kyselina
octové	octový	k2eAgFnSc2d1	octová
</s>
</p>
<p>
<s>
ethenu	ethena	k1gFnSc4	ethena
</s>
</p>
<p>
<s>
diethyletheru	diethylethrat	k5eAaPmIp1nS	diethylethrat
</s>
</p>
<p>
<s>
ethylacetátu	ethylacetát	k1gInSc3	ethylacetát
</s>
</p>
<p>
<s>
ethylakrylátu	ethylakrylát	k1gInSc3	ethylakrylát
</s>
</p>
<p>
<s>
ethylaminu	ethylamin	k2eAgFnSc4d1	ethylamin
</s>
</p>
<p>
<s>
diethylaminu	diethylamin	k2eAgFnSc4d1	diethylamin
</s>
</p>
<p>
<s>
triethylaminu	triethylamin	k1gInSc3	triethylamin
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Ethanol	ethanol	k1gInSc4	ethanol
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
===	===	k?	===
</s>
</p>
<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
hodnotné	hodnotný	k2eAgNnSc4d1	hodnotné
biopalivo	biopalivo	k1gNnSc4	biopalivo
pro	pro	k7c4	pro
spalovací	spalovací	k2eAgInPc4d1	spalovací
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
antidetonační	antidetonační	k2eAgFnPc4d1	antidetonační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nedostatkem	nedostatek	k1gInSc7	nedostatek
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
vázat	vázat	k5eAaImF	vázat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
působit	působit	k5eAaImF	působit
tak	tak	k9	tak
korozi	koroze	k1gFnSc4	koroze
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
přidáním	přidání	k1gNnSc7	přidání
vhodných	vhodný	k2eAgNnPc2d1	vhodné
aditiv	aditivum	k1gNnPc2	aditivum
(	(	kIx(	(
<g/>
antikorozních	antikorozní	k2eAgInPc2d1	antikorozní
přípravků	přípravek	k1gInPc2	přípravek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metabolismus	metabolismus	k1gInSc4	metabolismus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
===	===	k?	===
</s>
</p>
<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
nejčastěji	často	k6eAd3	často
trávicí	trávicí	k2eAgFnSc7d1	trávicí
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Vstřebávání	vstřebávání	k1gNnSc1	vstřebávání
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
trávicí	trávicí	k2eAgFnSc6d1	trávicí
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
vstřebávání	vstřebávání	k1gNnSc3	vstřebávání
dochází	docházet	k5eAaImIp3nS	docházet
už	už	k6eAd1	už
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
však	však	k9	však
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
===	===	k?	===
</s>
</p>
<p>
<s>
Hladina	hladina	k1gFnSc1	hladina
ethanolu	ethanol	k1gInSc2	ethanol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
)	)	kIx)	)
množství	množství	k1gNnSc1	množství
požité	požitý	k2eAgFnSc2d1	požitá
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
)	)	kIx)	)
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
)	)	kIx)	)
rychlosti	rychlost	k1gFnSc3	rychlost
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
)	)	kIx)	)
rychlosti	rychlost	k1gFnSc2	rychlost
detoxikace	detoxikace	k1gFnSc2	detoxikace
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Eliminace	eliminace	k1gFnSc1	eliminace
(	(	kIx(	(
<g/>
biotransformace	biotransformace	k1gFnSc1	biotransformace
<g/>
)	)	kIx)	)
alkoholu	alkohol	k1gInSc2	alkohol
probíhá	probíhat	k5eAaImIp3nS	probíhat
zhruba	zhruba	k6eAd1	zhruba
kinetikou	kinetika	k1gFnSc7	kinetika
nultého	nultý	k4xOgInSc2	nultý
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
rychlostí	rychlost	k1gFnSc7	rychlost
asi	asi	k9	asi
1	[number]	k4	1
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
kg	kg	kA	kg
tělesné	tělesný	k2eAgFnSc2d1	tělesná
váhy	váha	k1gFnSc2	váha
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
cca	cca	kA	cca
o	o	k7c4	o
0,15	[number]	k4	0,15
‰	‰	k?	‰
za	za	k7c4	za
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
detoxikaci	detoxikace	k1gFnSc3	detoxikace
100	[number]	k4	100
g	g	kA	g
alkoholu	alkohol	k1gInSc2	alkohol
obsažených	obsažený	k2eAgFnPc2d1	obsažená
asi	asi	k9	asi
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hladina	hladina	k1gFnSc1	hladina
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
2,2	[number]	k4	2,2
‰	‰	k?	‰
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutných	nutný	k2eAgNnPc2d1	nutné
zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přibližné	přibližný	k2eAgNnSc4d1	přibližné
určení	určení	k1gNnSc4	určení
hladiny	hladina	k1gFnSc2	hladina
etanolu	etanol	k1gInSc2	etanol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vzorec	vzorec	k1gInSc4	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
požitý	požitý	k2eAgInSc1d1	požitý
alkohol	alkohol	k1gInSc1	alkohol
v	v	k7c6	v
gramech	gram	k1gInPc6	gram
/	/	kIx~	/
(	(	kIx(	(
<g/>
tělesná	tělesný	k2eAgFnSc1d1	tělesná
hmotnost	hmotnost	k1gFnSc1	hmotnost
muže	muž	k1gMnSc2	muž
×	×	k?	×
0,68	[number]	k4	0,68
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tělesná	tělesný	k2eAgFnSc1d1	tělesná
hmotnost	hmotnost	k1gFnSc1	hmotnost
ženy	žena	k1gFnSc2	žena
×	×	k?	×
0,55	[number]	k4	0,55
<g/>
)	)	kIx)	)
=	=	kIx~	=
promile	promile	k1gNnSc4	promile
etanolu	etanol	k1gInSc2	etanol
v	v	k7c6	v
krviFyziologická	krviFyziologický	k2eAgFnSc1d1	krviFyziologický
hranice	hranice	k1gFnSc1	hranice
etanolu	etanol	k1gInSc2	etanol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
0,03	[number]	k4	0,03
<g/>
–	–	k?	–
<g/>
0,1	[number]	k4	0,1
promile	promile	k1gNnPc2	promile
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
0,3	[number]	k4	0,3
<g/>
–	–	k?	–
<g/>
0,5	[number]	k4	0,5
promile	promile	k1gNnPc2	promile
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
požití	požití	k1gNnSc6	požití
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
a	a	k8xC	a
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
1,0	[number]	k4	1,0
promile	promile	k1gNnPc2	promile
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
podnapilost	podnapilost	k1gFnSc4	podnapilost
<g/>
.	.	kIx.	.
1,0	[number]	k4	1,0
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
promile	promile	k1gNnPc2	promile
znamená	znamenat	k5eAaImIp3nS	znamenat
mírný	mírný	k2eAgInSc1d1	mírný
stupeň	stupeň	k1gInSc1	stupeň
opilosti	opilost	k1gFnSc2	opilost
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2,0	[number]	k4	2,0
promile	promile	k1gNnPc2	promile
pak	pak	k6eAd1	pak
střední	střední	k2eAgInPc1d1	střední
stupeň	stupeň	k1gInSc1	stupeň
opilosti	opilost	k1gFnSc2	opilost
s	s	k7c7	s
jasnými	jasný	k2eAgInPc7d1	jasný
klinickými	klinický	k2eAgInPc7d1	klinický
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
2,0	[number]	k4	2,0
<g/>
–	–	k?	–
<g/>
3,0	[number]	k4	3,0
promile	promile	k1gNnPc2	promile
je	být	k5eAaImIp3nS	být
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
jako	jako	k8xC	jako
těžký	těžký	k2eAgInSc1d1	těžký
stupeň	stupeň	k1gInSc1	stupeň
opilosti	opilost	k1gFnSc2	opilost
a	a	k8xC	a
při	při	k7c6	při
hodnotách	hodnota	k1gFnPc6	hodnota
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
3,0	[number]	k4	3,0
promile	promile	k1gNnPc2	promile
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
akutní	akutní	k2eAgFnSc6d1	akutní
otravě	otrava	k1gFnSc6	otrava
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Detoxifikace	Detoxifikace	k1gFnSc2	Detoxifikace
===	===	k?	===
</s>
</p>
<p>
<s>
Oxidace	oxidace	k1gFnSc1	oxidace
ethanolu	ethanol	k1gInSc2	ethanol
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvém	prvý	k4xOgInSc6	prvý
je	být	k5eAaImIp3nS	být
oxidován	oxidovat	k5eAaBmNgInS	oxidovat
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
pak	pak	k6eAd1	pak
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
proces	proces	k1gInSc1	proces
katalyzuje	katalyzovat	k5eAaBmIp3nS	katalyzovat
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1	alkoholdehydrogenáza
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
aldehyddehydrogenáza	aldehyddehydrogenáza	k1gFnSc1	aldehyddehydrogenáza
<g/>
.	.	kIx.	.
<g/>
Samotný	samotný	k2eAgInSc1d1	samotný
alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
metabolizován	metabolizovat	k5eAaImNgInS	metabolizovat
během	během	k7c2	během
hodin	hodina	k1gFnPc2	hodina
až	až	k8xS	až
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určité	určitý	k2eAgInPc1d1	určitý
metabolity	metabolit	k1gInPc1	metabolit
zůstavají	zůstavat	k5eAaPmIp3nP	zůstavat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
odhalení	odhalení	k1gNnSc4	odhalení
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
příjmu	příjem	k1gInSc2	příjem
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Neoxidativním	Neoxidativní	k2eAgNnSc7d1	Neoxidativní
štěpením	štěpení	k1gNnSc7	štěpení
etanolu	etanol	k1gInSc2	etanol
na	na	k7c4	na
etylglukuronid	etylglukuronid	k1gInSc4	etylglukuronid
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
právě	právě	k9	právě
množství	množství	k1gNnSc1	množství
etylglukuronidu	etylglukuronid	k1gInSc2	etylglukuronid
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrné	úměrný	k2eAgNnSc1d1	úměrné
množství	množství	k1gNnSc1	množství
zmetabolizovaného	zmetabolizovaný	k2eAgInSc2d1	zmetabolizovaný
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
vynikající	vynikající	k2eAgInSc1d1	vynikající
indikátor	indikátor	k1gInSc1	indikátor
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
etyglukuronidu	etyglukuronid	k1gInSc2	etyglukuronid
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
indikuje	indikovat	k5eAaBmIp3nS	indikovat
požití	požití	k1gNnSc1	požití
ethanolu	ethanol	k1gInSc2	ethanol
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
3	[number]	k4	3
-	-	kIx~	-
4	[number]	k4	4
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jsou	být	k5eAaImIp3nP	být
dispozici	dispozice	k1gFnSc4	dispozice
testovací	testovací	k2eAgFnSc2d1	testovací
soupravy	souprava	k1gFnSc2	souprava
a	a	k8xC	a
testovací	testovací	k2eAgInPc4d1	testovací
proužky	proužek	k1gInPc4	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgNnSc1d2	delší
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
pokrýt	pokrýt	k5eAaPmF	pokrýt
vyšetřením	vyšetření	k1gNnSc7	vyšetření
etylglukuronidu	etylglukuronid	k1gInSc2	etylglukuronid
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
však	však	k9	však
potřeba	potřeba	k1gFnSc1	potřeba
chromatografie	chromatografie	k1gFnSc1	chromatografie
a	a	k8xC	a
vyšetření	vyšetření	k1gNnSc1	vyšetření
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
časově	časově	k6eAd1	časově
i	i	k8xC	i
finančně	finančně	k6eAd1	finančně
náročnější	náročný	k2eAgMnSc1d2	náročnější
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Přímým	přímý	k2eAgNnSc7d1	přímé
působením	působení	k1gNnSc7	působení
ethanolu	ethanol	k1gInSc2	ethanol
na	na	k7c4	na
jaterní	jaterní	k2eAgFnPc4d1	jaterní
buňky	buňka	k1gFnPc4	buňka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
enzymu	enzym	k1gInSc2	enzym
gama-glutamyltransferázy	gamalutamyltransferáza	k1gFnSc2	gama-glutamyltransferáza
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
rovněž	rovněž	k9	rovněž
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
nebo	nebo	k8xC	nebo
masiví	masivět	k5eAaPmIp3nS	masivět
příjem	příjem	k1gInSc4	příjem
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecně	obecně	k6eAd1	obecně
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
materiály	materiál	k1gInPc1	materiál
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
alkohol	alkohol	k1gInSc1	alkohol
způsobil	způsobit	k5eAaPmAgInS	způsobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
celosvětově	celosvětově	k6eAd1	celosvětově
5,9	[number]	k4	5,9
%	%	kIx~	%
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kolem	kolem	k7c2	kolem
3,3	[number]	k4	3,3
miliónů	milión	k4xCgInPc2	milión
úmrtí	úmrť	k1gFnPc2	úmrť
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
podíl	podíl	k1gInSc4	podíl
alkoholu	alkohol	k1gInSc2	alkohol
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
úmrtnosti	úmrtnost	k1gFnSc6	úmrtnost
dokonce	dokonce	k9	dokonce
13,3	[number]	k4	13,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
závěru	závěr	k1gInSc3	závěr
dospěli	dochvít	k5eAaPmAgMnP	dochvít
autoři	autor	k1gMnPc1	autor
i	i	k9	i
po	po	k7c6	po
odečtení	odečtení	k1gNnSc6	odečtení
možných	možný	k2eAgInPc2d1	možný
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
účinků	účinek	k1gInPc2	účinek
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
různých	různý	k2eAgFnPc2d1	různá
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
působená	působený	k2eAgFnSc1d1	působená
alkoholem	alkohol	k1gInSc7	alkohol
souvisí	souviset	k5eAaImIp3nS	souviset
zejména	zejména	k9	zejména
s	s	k7c7	s
nádorovými	nádorový	k2eAgNnPc7d1	nádorové
onemocněními	onemocnění	k1gNnPc7	onemocnění
<g/>
,	,	kIx,	,
onemocněními	onemocnění	k1gNnPc7	onemocnění
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
úmyslnými	úmyslný	k2eAgNnPc7d1	úmyslné
i	i	k8xC	i
neúmyslnými	úmyslný	k2eNgNnPc7d1	neúmyslné
poraněními	poranění	k1gNnPc7	poranění
<g/>
,	,	kIx,	,
onemocněními	onemocnění	k1gNnPc7	onemocnění
trávicího	trávicí	k2eAgInSc2d1	trávicí
systému	systém	k1gInSc2	systém
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
<g/>
Alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
různě	různě	k6eAd1	různě
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
ethanolu	ethanol	k1gInSc2	ethanol
(	(	kIx(	(
<g/>
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
alkohol	alkohol	k1gInSc4	alkohol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
požití	požití	k1gNnSc1	požití
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
dávkách	dávka	k1gFnPc6	dávka
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
metabolismu	metabolismus	k1gInSc6	metabolismus
jedince	jedinec	k1gMnSc2	jedinec
<g/>
)	)	kIx)	)
uvolnění	uvolnění	k1gNnSc1	uvolnění
a	a	k8xC	a
euforické	euforický	k2eAgInPc1d1	euforický
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
dávkách	dávka	k1gFnPc6	dávka
útlum	útlum	k1gInSc4	útlum
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
až	až	k8xS	až
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
omamná	omamný	k2eAgFnSc1d1	omamná
látka	látka	k1gFnSc1	látka
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
nápojích	nápoj	k1gInPc6	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
a	a	k8xC	a
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
návykový	návykový	k2eAgInSc1d1	návykový
(	(	kIx(	(
<g/>
tělo	tělo	k1gNnSc1	tělo
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ho	on	k3xPp3gMnSc4	on
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
alkoholovou	alkoholový	k2eAgFnSc4d1	alkoholová
závislost	závislost	k1gFnSc4	závislost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
alkoholu	alkohol	k1gInSc3	alkohol
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
být	být	k5eAaImF	být
abstinentem	abstinent	k1gMnSc7	abstinent
<g/>
,	,	kIx,	,
pít	pít	k5eAaImF	pít
alkohol	alkohol	k1gInSc4	alkohol
uměřeně	uměřeně	k6eAd1	uměřeně
<g/>
,	,	kIx,	,
rizikově	rizikově	k6eAd1	rizikově
<g/>
,	,	kIx,	,
škodlivě	škodlivě	k6eAd1	škodlivě
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
závislý	závislý	k2eAgInSc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
škod	škoda	k1gFnPc2	škoda
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
alkohol	alkohol	k1gInSc1	alkohol
pijí	pít	k5eAaImIp3nP	pít
škodlivě	škodlivě	k6eAd1	škodlivě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vysoký	vysoký	k2eAgInSc4d1	vysoký
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
narušuje	narušovat	k5eAaImIp3nS	narušovat
REM-fázi	REMáze	k1gFnSc4	REM-fáze
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
spánek	spánek	k1gInSc1	spánek
tak	tak	k6eAd1	tak
fakticky	fakticky	k6eAd1	fakticky
znehodnocuje	znehodnocovat	k5eAaImIp3nS	znehodnocovat
<g/>
.	.	kIx.	.
</s>
<s>
Stres	stres	k1gInSc1	stres
mírní	mírnit	k5eAaImIp3nS	mírnit
pouze	pouze	k6eAd1	pouze
zdánlivě	zdánlivě	k6eAd1	zdánlivě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zvýšení	zvýšení	k1gNnSc1	zvýšení
tepové	tepový	k2eAgFnSc2d1	tepová
frekvence	frekvence	k1gFnSc2	frekvence
a	a	k8xC	a
zatížení	zatížení	k1gNnSc4	zatížení
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
mozku	mozek	k1gInSc2	mozek
stres	stres	k1gInSc4	stres
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
povrchových	povrchový	k2eAgFnPc2d1	povrchová
cév	céva	k1gFnPc2	céva
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tepelným	tepelný	k2eAgFnPc3d1	tepelná
ztrátám	ztráta	k1gFnPc3	ztráta
a	a	k8xC	a
při	pře	k1gFnSc3	pře
zranění	zranění	k1gNnPc2	zranění
může	moct	k5eAaImIp3nS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
krvácení	krvácení	k1gNnSc3	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
srážlivost	srážlivost	k1gFnSc1	srážlivost
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
naředění	naředění	k1gNnSc1	naředění
<g/>
)	)	kIx)	)
při	při	k7c6	při
intoxikaci	intoxikace	k1gFnSc6	intoxikace
alkoholem	alkohol	k1gInSc7	alkohol
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
krvácení	krvácení	k1gNnSc2	krvácení
do	do	k7c2	do
trávicího	trávicí	k2eAgInSc2d1	trávicí
systému	systém	k1gInSc2	systém
a	a	k8xC	a
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
antioxidantů	antioxidant	k1gInPc2	antioxidant
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
vínu	víno	k1gNnSc6	víno
se	se	k3xPyFc4	se
přeceňuje	přeceňovat	k5eAaImIp3nS	přeceňovat
<g/>
,	,	kIx,	,
antioxidanty	antioxidant	k1gInPc1	antioxidant
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
druzích	druh	k1gInPc6	druh
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
často	často	k6eAd1	často
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
kognitivní	kognitivní	k2eAgFnPc4d1	kognitivní
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
opilost	opilost	k1gFnSc4	opilost
a	a	k8xC	a
alkoholovou	alkoholový	k2eAgFnSc4d1	alkoholová
závislost	závislost	k1gFnSc4	závislost
<g/>
,	,	kIx,	,
narušuje	narušovat	k5eAaImIp3nS	narušovat
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
škodí	škodit	k5eAaImIp3nS	škodit
osobám	osoba	k1gFnPc3	osoba
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
imunitu	imunita	k1gFnSc4	imunita
některých	některý	k3yIgInPc2	některý
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
rakoviny	rakovina	k1gFnSc2	rakovina
některých	některý	k3yIgInPc2	některý
orgánů	orgán	k1gInPc2	orgán
buď	buď	k8xC	buď
přímým	přímý	k2eAgNnSc7d1	přímé
drážděním	dráždění	k1gNnSc7	dráždění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hltanu	hltan	k1gInSc2	hltan
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
silných	silný	k2eAgInPc2d1	silný
destilátů	destilát	k1gInPc2	destilát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zanášením	zanášení	k1gNnSc7	zanášení
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
do	do	k7c2	do
tkáně	tkáň	k1gFnSc2	tkáň
prsu	prs	k1gInSc2	prs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
obsahovat	obsahovat	k5eAaImF	obsahovat
plísňové	plísňový	k2eAgInPc4d1	plísňový
toxiny	toxin	k1gInPc4	toxin
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
pivo	pivo	k1gNnSc1	pivo
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
kvalitě	kvalita	k1gFnSc3	kvalita
sladu	slad	k1gInSc2	slad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škodlivé	škodlivý	k2eAgInPc1d1	škodlivý
produkty	produkt	k1gInPc1	produkt
kvašení	kvašení	k1gNnSc2	kvašení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nekvalitní	kvalitní	k2eNgInPc4d1	nekvalitní
destiláty	destilát	k1gInPc4	destilát
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zdraví	zdravit	k5eAaImIp3nP	zdravit
neprospěšná	prospěšný	k2eNgNnPc4d1	neprospěšné
aditiva	aditivum	k1gNnPc4	aditivum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
konzervační	konzervační	k2eAgFnSc2d1	konzervační
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgNnSc1d1	chemické
barvivo	barvivo	k1gNnSc1	barvivo
či	či	k8xC	či
aroma	aroma	k1gNnSc1	aroma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
dávky	dávka	k1gFnPc1	dávka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
euforii	euforie	k1gFnSc3	euforie
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
dávky	dávka	k1gFnPc1	dávka
ethanolu	ethanol	k1gInSc2	ethanol
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
tlumivě	tlumivě	k6eAd1	tlumivě
<g/>
,	,	kIx,	,
při	při	k7c6	při
těžší	těžký	k2eAgFnSc6d2	těžší
otravě	otrava	k1gFnSc6	otrava
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bezvědomí	bezvědomí	k1gNnSc3	bezvědomí
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc3	ohrožení
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
četné	četný	k2eAgInPc4d1	četný
receptorové	receptorové	k?	receptorové
systémy	systém	k1gInPc4	systém
(	(	kIx(	(
<g/>
GABA	GABA	kA	GABA
<g/>
,	,	kIx,	,
glutamát	glutamát	k1gInSc1	glutamát
<g/>
,	,	kIx,	,
dopamin	dopamin	k1gInSc1	dopamin
<g/>
,	,	kIx,	,
serotonin	serotonin	k1gInSc1	serotonin
<g/>
,	,	kIx,	,
endorfin	endorfin	k1gInSc1	endorfin
opiátové	opiátový	k2eAgInPc1d1	opiátový
receptory	receptor	k1gInPc7	receptor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gInSc1	jeho
návykový	návykový	k2eAgInSc1d1	návykový
potenciál	potenciál	k1gInSc1	potenciál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Akutní	akutní	k2eAgFnSc1d1	akutní
otrava	otrava	k1gFnSc1	otrava
===	===	k?	===
</s>
</p>
<p>
<s>
Účinky	účinek	k1gInPc4	účinek
ethanolu	ethanol	k1gInSc2	ethanol
při	při	k7c6	při
akutní	akutní	k2eAgFnSc6d1	akutní
intoxikaci	intoxikace	k1gFnSc6	intoxikace
(	(	kIx(	(
<g/>
otravě	otrava	k1gFnSc6	otrava
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
postižení	postižení	k1gNnSc2	postižení
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
stadií	stadion	k1gNnPc2	stadion
<g/>
;	;	kIx,	;
excitačního	excitační	k2eAgMnSc2d1	excitační
<g/>
,	,	kIx,	,
narkotického	narkotický	k2eAgMnSc2d1	narkotický
<g/>
,	,	kIx,	,
komatózního	komatózní	k2eAgNnSc2d1	komatózní
a	a	k8xC	a
alkoholového	alkoholový	k2eAgNnSc2d1	alkoholové
hypoglykemického	hypoglykemický	k2eAgNnSc2d1	hypoglykemické
kómatu	kóma	k1gNnSc2	kóma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Excitační	excitační	k2eAgNnSc1d1	excitační
stadium	stadium	k1gNnSc1	stadium
je	být	k5eAaImIp3nS	být
provázeno	provázet	k5eAaImNgNnS	provázet
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
duševní	duševní	k2eAgFnSc7d1	duševní
a	a	k8xC	a
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
aktivitou	aktivita	k1gFnSc7	aktivita
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
Subjektivně	subjektivně	k6eAd1	subjektivně
se	se	k3xPyFc4	se
taková	takový	k3xDgFnSc1	takový
osoba	osoba	k1gFnSc1	osoba
cítí	cítit	k5eAaImIp3nS	cítit
sebejistá	sebejistý	k2eAgFnSc1d1	sebejistá
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
spokojená	spokojený	k2eAgFnSc1d1	spokojená
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
kritičnosti	kritičnost	k1gFnSc2	kritičnost
a	a	k8xC	a
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
narušena	narušen	k2eAgFnSc1d1	narušena
koordinace	koordinace	k1gFnSc1	koordinace
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
prodloužen	prodloužen	k2eAgInSc4d1	prodloužen
reakční	reakční	k2eAgInSc4d1	reakční
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narkotické	narkotický	k2eAgNnSc1d1	narkotické
stadium	stadium	k1gNnSc1	stadium
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
vazodilatačními	vazodilatační	k2eAgInPc7d1	vazodilatační
účinky	účinek	k1gInPc7	účinek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
překrvením	překrvení	k1gNnSc7	překrvení
a	a	k8xC	a
zčervenáním	zčervenání	k1gNnSc7	zčervenání
kůže	kůže	k1gFnSc2	kůže
zejména	zejména	k9	zejména
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Chůze	chůze	k1gFnSc1	chůze
je	být	k5eAaImIp3nS	být
vrávoravá	vrávoravý	k2eAgFnSc1d1	vrávoravá
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
dvojité	dvojitý	k2eAgNnSc1d1	dvojité
vidění	vidění	k1gNnSc1	vidění
a	a	k8xC	a
závratě	závrať	k1gFnPc1	závrať
především	především	k9	především
při	při	k7c6	při
zavřených	zavřený	k2eAgNnPc6d1	zavřené
očích	oko	k1gNnPc6	oko
a	a	k8xC	a
vleže	vleže	k6eAd1	vleže
<g/>
.	.	kIx.	.
</s>
<s>
Stoupá	stoupat	k5eAaImIp3nS	stoupat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
puls	puls	k1gInSc4	puls
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zvracení	zvracení	k1gNnSc3	zvracení
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
objem	objem	k1gInSc1	objem
močení	močení	k1gNnSc2	močení
(	(	kIx(	(
<g/>
diuréza	diuréza	k1gFnSc1	diuréza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předešlé	předešlý	k2eAgFnSc6d1	předešlá
euforii	euforie	k1gFnSc6	euforie
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
významný	významný	k2eAgInSc1d1	významný
útlum	útlum	k1gInSc1	útlum
<g/>
,	,	kIx,	,
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
a	a	k8xC	a
pasivita	pasivita	k1gFnSc1	pasivita
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
ztrátou	ztráta	k1gFnSc7	ztráta
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kómatozní	Kómatozní	k2eAgNnSc1d1	Kómatozní
stadium	stadium	k1gNnSc1	stadium
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
bezvědomím	bezvědomí	k1gNnSc7	bezvědomí
s	s	k7c7	s
úplným	úplný	k2eAgNnSc7d1	úplné
motorickým	motorický	k2eAgNnSc7d1	motorické
ochabnutím	ochabnutí	k1gNnSc7	ochabnutí
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
hluboké	hluboký	k2eAgNnSc1d1	hluboké
a	a	k8xC	a
zpomalené	zpomalený	k2eAgNnSc1d1	zpomalené
(	(	kIx(	(
<g/>
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
respirační	respirační	k2eAgFnSc1d1	respirační
acidóza	acidóza	k1gFnSc1	acidóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvracení	zvracení	k1gNnSc6	zvracení
hrozí	hrozit	k5eAaImIp3nS	hrozit
vdechnutí	vdechnutí	k1gNnSc1	vdechnutí
žaludečního	žaludeční	k2eAgInSc2d1	žaludeční
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
zástava	zástava	k1gFnSc1	zástava
dechu	dech	k1gInSc2	dech
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc4d1	základní
první	první	k4xOgInSc4	první
pomocí	pomoc	k1gFnPc2	pomoc
je	být	k5eAaImIp3nS	být
přetočení	přetočení	k1gNnSc1	přetočení
dýchajícího	dýchající	k2eAgInSc2d1	dýchající
opilého	opilý	k2eAgInSc2d1	opilý
do	do	k7c2	do
zotavovací	zotavovací	k2eAgFnSc2d1	zotavovací
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
hypoglykemické	hypoglykemický	k2eAgNnSc1d1	hypoglykemické
kóma	kóma	k1gNnSc1	kóma
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
výrazným	výrazný	k2eAgInSc7d1	výrazný
hypoglykemizujícím	hypoglykemizující	k2eAgInSc7d1	hypoglykemizující
účinkem	účinek	k1gInSc7	účinek
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
inhibici	inhibice	k1gFnSc4	inhibice
glukoneogeneze	glukoneogeneze	k1gFnSc2	glukoneogeneze
(	(	kIx(	(
<g/>
potlačení	potlačení	k1gNnSc1	potlačení
tvorby	tvorba	k1gFnSc2	tvorba
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
<g/>
)	)	kIx)	)
z	z	k7c2	z
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
mléčné	mléčný	k2eAgFnSc2d1	mléčná
a	a	k8xC	a
glycerolu	glycerol	k1gInSc2	glycerol
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
alkoholiků	alkoholik	k1gMnPc2	alkoholik
po	po	k7c6	po
abúzu	abúzus	k1gInSc6	abúzus
(	(	kIx(	(
<g/>
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
užívání	užívání	k1gNnSc6	užívání
<g/>
)	)	kIx)	)
na	na	k7c4	na
lačno	lačno	k1gNnSc4	lačno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
projevům	projev	k1gInPc3	projev
snížení	snížení	k1gNnSc2	snížení
hladiny	hladina	k1gFnSc2	hladina
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
dochází	docházet	k5eAaImIp3nS	docházet
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příznaky	příznak	k1gInPc7	příznak
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
působení	působení	k1gNnSc2	působení
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
a	a	k8xC	a
soustavném	soustavný	k2eAgNnSc6d1	soustavné
požívání	požívání	k1gNnSc6	požívání
etanolu	etanol	k1gInSc2	etanol
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
při	při	k7c6	při
abstinenci	abstinence	k1gFnSc6	abstinence
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
orgánových	orgánový	k2eAgFnPc2d1	orgánová
změn	změna	k1gFnPc2	změna
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
cirhóza	cirhóza	k1gFnSc1	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc4	zánět
slinivky	slinivka	k1gFnSc2	slinivka
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
oběhové	oběhový	k2eAgFnSc2d1	oběhová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
hypertenze	hypertenze	k1gFnSc1	hypertenze
<g/>
,	,	kIx,	,
dysrytmie	dysrytmie	k1gFnSc1	dysrytmie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
neuromuskulární	uromuskulární	k2eNgFnSc1d1	neuromuskulární
porucha	porucha	k1gFnSc1	porucha
svalů	sval	k1gInPc2	sval
či	či	k8xC	či
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
v	v	k7c6	v
horším	zlý	k2eAgInSc6d2	horší
případě	případ	k1gInSc6	případ
i	i	k9	i
nádory	nádor	k1gInPc1	nádor
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
fetální	fetálnit	k5eAaPmIp3nS	fetálnit
alkoholový	alkoholový	k2eAgInSc1d1	alkoholový
syndrom	syndrom	k1gInSc1	syndrom
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Psychicky	psychicky	k6eAd1	psychicky
může	moct	k5eAaImIp3nS	moct
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
konzumace	konzumace	k1gFnSc1	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
demence	demence	k1gFnSc2	demence
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
užívání	užívání	k1gNnSc1	užívání
alkoholu	alkohol	k1gInSc2	alkohol
stálo	stát	k5eAaImAgNnS	stát
život	život	k1gInSc4	život
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
těchto	tento	k3xDgFnPc2	tento
úmrtí	úmrť	k1gFnPc2	úmrť
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc4	úmrtí
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zranění	zranění	k1gNnSc4	zranění
(	(	kIx(	(
<g/>
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
%	%	kIx~	%
zažívací	zažívací	k2eAgFnPc4d1	zažívací
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
%	%	kIx~	%
kardiovaskulární	kardiovaskulární	k2eAgFnSc2d1	kardiovaskulární
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
infekční	infekční	k2eAgFnSc2d1	infekční
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
,	,	kIx,	,
mentální	mentální	k2eAgNnPc1d1	mentální
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prevence	prevence	k1gFnSc1	prevence
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
škod	škoda	k1gFnPc2	škoda
působených	působený	k2eAgFnPc2d1	působená
alkoholem	alkohol	k1gInSc7	alkohol
==	==	k?	==
</s>
</p>
<p>
<s>
Chan	Chan	k1gNnSc1	Chan
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
význam	význam	k1gInSc4	význam
vysokého	vysoký	k2eAgNnSc2d1	vysoké
zdanění	zdanění	k1gNnSc2	zdanění
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
omezování	omezování	k1gNnSc1	omezování
jeho	jeho	k3xOp3gFnSc2	jeho
dostupnosti	dostupnost	k1gFnSc2	dostupnost
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dospívající	dospívající	k2eAgInPc4d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
hodin	hodina	k1gFnPc2	hodina
podeje	podej	k1gInSc2	podej
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
nočního	noční	k2eAgNnSc2d1	noční
násilí	násilí	k1gNnSc2	násilí
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojnásobení	zdvojnásobení	k1gNnSc1	zdvojnásobení
spotřební	spotřební	k2eAgFnSc2d1	spotřební
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
alkoholu	alkohol	k1gInSc2	alkohol
sníží	snížit	k5eAaPmIp3nS	snížit
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pití	pití	k1gNnSc2	pití
alkoholu	alkohol	k1gInSc2	alkohol
o	o	k7c4	o
35	[number]	k4	35
%	%	kIx~	%
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
nehody	nehoda	k1gFnSc2	nehoda
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
alkoholu	alkohol	k1gInSc2	alkohol
o	o	k7c4	o
11	[number]	k4	11
%	%	kIx~	%
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
chorob	choroba	k1gFnPc2	choroba
o	o	k7c4	o
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
abstinovat	abstinovat	k5eAaBmF	abstinovat
od	od	k7c2	od
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
respektovat	respektovat	k5eAaImF	respektovat
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Reklama	reklama	k1gFnSc1	reklama
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
sponzorské	sponzorský	k2eAgFnSc2d1	sponzorská
aktivity	aktivita	k1gFnSc2	aktivita
alkoholového	alkoholový	k2eAgInSc2d1	alkoholový
průmyslu	průmysl	k1gInSc2	průmysl
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
zakázat	zakázat	k5eAaPmF	zakázat
nebo	nebo	k8xC	nebo
striktně	striktně	k6eAd1	striktně
omezit	omezit	k5eAaPmF	omezit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
reklama	reklama	k1gFnSc1	reklama
alkoholu	alkohol	k1gInSc2	alkohol
působit	působit	k5eAaImF	působit
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dospívající	dospívající	k2eAgInPc4d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
prevence	prevence	k1gFnSc1	prevence
škod	škoda	k1gFnPc2	škoda
působených	působený	k2eAgFnPc2d1	působená
alkoholem	alkohol	k1gInSc7	alkohol
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
;	;	kIx,	;
nulová	nulový	k2eAgFnSc1d1	nulová
tolerance	tolerance	k1gFnSc1	tolerance
alkoholu	alkohol	k1gInSc2	alkohol
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
25	[number]	k4	25
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
třeba	třeba	k6eAd1	třeba
věnovat	věnovat	k5eAaImF	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
včasné	včasný	k2eAgFnSc2d1	včasná
pomoci	pomoc	k1gFnSc2	pomoc
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
alkohol	alkohol	k1gInSc4	alkohol
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc3	jejich
rodinám	rodina	k1gFnPc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
probíhat	probíhat	k5eAaImF	probíhat
stupňovitě	stupňovitě	k6eAd1	stupňovitě
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
závažnost	závažnost	k1gFnSc4	závažnost
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
škodlivého	škodlivý	k2eAgNnSc2d1	škodlivé
užívání	užívání	k1gNnSc2	užívání
často	často	k6eAd1	často
stačí	stačit	k5eAaBmIp3nS	stačit
svépomoc	svépomoc	k1gFnSc1	svépomoc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
použití	použití	k1gNnSc1	použití
svépomocného	svépomocný	k2eAgInSc2d1	svépomocný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pokročilejších	pokročilý	k2eAgInPc2d2	pokročilejší
problémů	problém	k1gInPc2	problém
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
docházet	docházet	k5eAaImF	docházet
co	co	k9	co
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
Anonymních	anonymní	k2eAgMnPc2d1	anonymní
alkoholiků	alkoholik	k1gMnPc2	alkoholik
<g/>
,	,	kIx,	,
vyhledat	vyhledat	k5eAaPmF	vyhledat
ambulantní	ambulantní	k2eAgFnSc4d1	ambulantní
léčbu	léčba	k1gFnSc4	léčba
ve	v	k7c6	v
specializovaném	specializovaný	k2eAgNnSc6d1	specializované
zařízení	zařízení	k1gNnSc6	zařízení
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
léčit	léčit	k5eAaImF	léčit
pobytově	pobytově	k6eAd1	pobytově
<g/>
.	.	kIx.	.
</s>
<s>
Kontakty	kontakt	k1gInPc1	kontakt
na	na	k7c4	na
česká	český	k2eAgNnPc4d1	české
zařízení	zařízení	k1gNnPc4	zařízení
poskytující	poskytující	k2eAgInSc4d1	poskytující
pobytovou	pobytový	k2eAgFnSc4d1	pobytová
léčbu	léčba	k1gFnSc4	léčba
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
viz	vidět	k5eAaImRp2nS	vidět
http://drnespor.eu/Strizliv.doc	[url]	k1gFnSc4	http://drnespor.eu/Strizliv.doc
</s>
</p>
<p>
<s>
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
působí	působit	k5eAaImIp3nP	působit
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
methanolem	methanol	k1gInSc7	methanol
==	==	k?	==
</s>
</p>
<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
zápachem	zápach	k1gInSc7	zápach
úplně	úplně	k6eAd1	úplně
stejný	stejný	k2eAgInSc4d1	stejný
jako	jako	k8xC	jako
mnohem	mnohem	k6eAd1	mnohem
nebezpečnější	bezpečný	k2eNgInSc4d2	nebezpečnější
methanol	methanol	k1gInSc4	methanol
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
hrozí	hrozit	k5eAaImIp3nS	hrozit
jejich	jejich	k3xOp3gFnSc1	jejich
záměna	záměna	k1gFnSc1	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
alkoholy	alkohol	k1gInPc1	alkohol
lze	lze	k6eAd1	lze
bezpečně	bezpečně	k6eAd1	bezpečně
rozlišit	rozlišit	k5eAaPmF	rozlišit
pouze	pouze	k6eAd1	pouze
laboratorně	laboratorně	k6eAd1	laboratorně
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spolehnout	spolehnout	k5eAaPmF	spolehnout
se	se	k3xPyFc4	se
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
zapálením	zapálení	k1gNnSc7	zapálení
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
methanol	methanol	k1gInSc1	methanol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
již	již	k6eAd1	již
od	od	k7c2	od
0,1	[number]	k4	0,1
gramu	gram	k1gInSc2	gram
čistého	čistý	k2eAgInSc2d1	čistý
methanolu	methanol	k1gInSc2	methanol
na	na	k7c4	na
1	[number]	k4	1
kg	kg	kA	kg
hmotnosti	hmotnost	k1gFnSc2	hmotnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přípustné	přípustný	k2eAgNnSc1d1	přípustné
množství	množství	k1gNnSc1	množství
methanolu	methanol	k1gInSc2	methanol
v	v	k7c6	v
destilátech	destilát	k1gInPc6	destilát
upravuje	upravovat	k5eAaImIp3nS	upravovat
příslušné	příslušný	k2eAgNnSc1d1	příslušné
nařízení	nařízení	k1gNnSc1	nařízení
EU	EU	kA	EU
č.	č.	k?	č.
1576	[number]	k4	1576
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
metanolem	metanol	k1gInSc7	metanol
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nejčastěji	často	k6eAd3	často
po	po	k7c6	po
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1	lehká
otrava	otrava	k1gFnSc1	otrava
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
slabostí	slabost	k1gFnSc7	slabost
<g/>
,	,	kIx,	,
bolestí	bolest	k1gFnSc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zmateností	zmatenost	k1gFnPc2	zmatenost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
poruchy	porucha	k1gFnPc4	porucha
barevného	barevný	k2eAgNnSc2d1	barevné
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
mlhavé	mlhavý	k2eAgNnSc4d1	mlhavé
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
nesnášenlivost	nesnášenlivost	k1gFnSc4	nesnášenlivost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
otrava	otrava	k1gFnSc1	otrava
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
poruchami	porucha	k1gFnPc7	porucha
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
vidění	vidění	k1gNnSc2	vidění
až	až	k6eAd1	až
slepotou	slepota	k1gFnSc7	slepota
<g/>
,	,	kIx,	,
selháním	selhání	k1gNnSc7	selhání
činnosti	činnost	k1gFnSc2	činnost
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
oběhového	oběhový	k2eAgInSc2d1	oběhový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
metabolickým	metabolický	k2eAgInSc7d1	metabolický
rozvratem	rozvrat	k1gInSc7	rozvrat
až	až	k8xS	až
multiorgánovým	multiorgánův	k2eAgNnSc7d1	multiorgánův
selháním	selhání	k1gNnSc7	selhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
protijed	protijed	k1gInSc4	protijed
a	a	k8xC	a
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
tak	tak	k6eAd1	tak
následky	následek	k1gInPc4	následek
otravy	otrava	k1gFnSc2	otrava
methanolem	methanol	k1gInSc7	methanol
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
(	(	kIx(	(
<g/>
poruchy	poruch	k1gInPc1	poruch
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
upadání	upadání	k1gNnSc1	upadání
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
)	)	kIx)	)
nutné	nutný	k2eAgNnSc1d1	nutné
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
biochemická	biochemický	k2eAgFnSc1d1	biochemická
podstata	podstata	k1gFnSc1	podstata
působení	působení	k1gNnSc2	působení
ethanolu	ethanol	k1gInSc2	ethanol
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
je	být	k5eAaImIp3nS	být
obdobná	obdobný	k2eAgFnSc1d1	obdobná
jako	jako	k9	jako
u	u	k7c2	u
methanolu	methanol	k1gInSc2	methanol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
riziková	rizikový	k2eAgFnSc1d1	riziková
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
jako	jako	k8xS	jako
protijed	protijed	k1gInSc1	protijed
(	(	kIx(	(
<g/>
antidotum	antidotum	k1gNnSc1	antidotum
<g/>
)	)	kIx)	)
při	při	k7c6	při
otravách	otrava	k1gFnPc6	otrava
methanolem	methanol	k1gInSc7	methanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILLMORE	FILLMORE	kA	FILLMORE
<g/>
,	,	kIx,	,
K.	K.	kA	K.
M.	M.	kA	M.
<g/>
;	;	kIx,	;
KERR	KERR	kA	KERR
<g/>
,	,	kIx,	,
W.	W.	kA	W.
C.	C.	kA	C.
<g/>
;	;	kIx,	;
STOCKWELL	STOCKWELL	kA	STOCKWELL
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
;	;	kIx,	;
CHIKRITZHS	CHIKRITZHS	kA	CHIKRITZHS
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
;	;	kIx,	;
BOSTROM	BOSTROM	kA	BOSTROM
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Moderate	Moderat	k1gInSc5	Moderat
alcohol	alcoholit	k5eAaPmRp2nS	alcoholit
use	usus	k1gInSc5	usus
and	and	k?	and
reduced	reduced	k1gMnSc1	reduced
mortality	mortalita	k1gFnSc2	mortalita
risk	risk	k1gInSc1	risk
<g/>
:	:	kIx,	:
Systematic	Systematice	k1gFnPc2	Systematice
error	error	k1gInSc1	error
in	in	k?	in
prospective	prospectiv	k1gInSc5	prospectiv
studies	studies	k1gMnSc1	studies
<g/>
.	.	kIx.	.
</s>
<s>
Addiction	Addiction	k1gInSc1	Addiction
Research	Research	k1gInSc1	Research
&	&	k?	&
Theory	Theora	k1gFnSc2	Theora
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
,	,	kIx,	,
iss	iss	k?	iss
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
101	[number]	k4	101
<g/>
–	–	k?	–
<g/>
132	[number]	k4	132
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1606	[number]	k4	1606
<g/>
-	-	kIx~	-
<g/>
6359	[number]	k4	6359
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HILLBOM	HILLBOM	kA	HILLBOM
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
;	;	kIx,	;
SALOHEIMO	SALOHEIMO	kA	SALOHEIMO
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
;	;	kIx,	;
JUVELA	JUVELA	kA	JUVELA
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Alcohol	Alcohol	k1gInSc1	Alcohol
consumption	consumption	k1gInSc1	consumption
<g/>
,	,	kIx,	,
blood	blood	k1gInSc1	blood
pressure	pressur	k1gMnSc5	pressur
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
risk	risk	k1gInSc1	risk
of	of	k?	of
stroke	stroke	k1gInSc1	stroke
<g/>
.	.	kIx.	.
</s>
<s>
Current	Current	k1gInSc1	Current
Hypertension	Hypertension	k1gInSc1	Hypertension
Reports	Reports	k1gInSc1	Reports
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
,	,	kIx,	,
iss	iss	k?	iss
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
208	[number]	k4	208
<g/>
–	–	k?	–
<g/>
213	[number]	k4	213
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1522	[number]	k4	1522
<g/>
-	-	kIx~	-
<g/>
6417	[number]	k4	6417
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HYNIE	HYNIE	kA	HYNIE
<g/>
,	,	kIx,	,
Sixtus	Sixtus	k1gMnSc1	Sixtus
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Farmakologie	farmakologie	k1gFnSc1	farmakologie
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
2	[number]	k4	2
<g/>
.	.	kIx.	.
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
273	[number]	k4	273
<g/>
–	–	k?	–
<g/>
550	[number]	k4	550
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
185	[number]	k4	185
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHAN	CHAN	kA	CHAN
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
Support	support	k1gInSc1	support
for	forum	k1gNnPc2	forum
strong	strong	k1gInSc4	strong
alcohol	alcohol	k1gInSc1	alcohol
policies	policies	k1gInSc1	policies
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Opening	Opening	k1gInSc1	Opening
address	address	k1gInSc1	address
at	at	k?	at
the	the	k?	the
Global	globat	k5eAaImAgMnS	globat
Alcohol	Alcohol	k1gInSc4	Alcohol
Policy	Polica	k1gFnSc2	Polica
Symposium	symposium	k1gNnSc4	symposium
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Turkey	Turkey	k1gInPc1	Turkey
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
April	April	k1gInSc1	April
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Přístupné	přístupný	k2eAgInPc4d1	přístupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.who.int/dg/speeches/2013/global_alcohol_policy_symposium_20130426/en	[url]	k1gFnPc2	http://www.who.int/dg/speeches/2013/global_alcohol_policy_symposium_20130426/en
</s>
</p>
<p>
<s>
Methanol	methanol	k1gInSc1	methanol
:	:	kIx,	:
health	health	k1gInSc1	health
and	and	k?	and
safety	safeta	k1gFnSc2	safeta
guide	guide	k6eAd1	guide
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
World	World	k1gInSc1	World
Health	Healtha	k1gFnPc2	Healtha
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Health	Health	k1gInSc1	Health
and	and	k?	and
safety	safeta	k1gFnSc2	safeta
guide	guide	k6eAd1	guide
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
.	.	kIx.	.
105	[number]	k4	105
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Environmental	Environmental	k1gMnSc1	Environmental
Health	Health	k1gInSc4	Health
Criteria	Criterium	k1gNnSc2	Criterium
196	[number]	k4	196
<g/>
:	:	kIx,	:
Methanol	methanol	k1gInSc1	methanol
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
151105	[number]	k4	151105
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
259	[number]	k4	259
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.inchem.org/documents/hsg/hsg/v105hsg.htm	[url]	k6eAd1	http://www.inchem.org/documents/hsg/hsg/v105hsg.htm
</s>
</p>
<p>
<s>
Nařízení	nařízení	k1gNnSc1	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
110	[number]	k4	110
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
o	o	k7c6	o
definici	definice	k1gFnSc6	definice
<g/>
,	,	kIx,	,
popisu	popis	k1gInSc3	popis
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc6d1	obchodní
úpravě	úprava	k1gFnSc6	úprava
<g/>
,	,	kIx,	,
označování	označování	k1gNnSc6	označování
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
označení	označení	k1gNnSc4	označení
lihovin	lihovina	k1gFnPc2	lihovina
a	a	k8xC	a
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
nařízení	nařízení	k1gNnSc2	nařízení
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
č.	č.	k?	č.
1576	[number]	k4	1576
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgMnSc1d1	úřední
věstník	věstník	k1gMnSc1	věstník
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
39	[number]	k4	39
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1725	[number]	k4	1725
<g/>
-	-	kIx~	-
<g/>
5074	[number]	k4	5074
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://eur-lex.europa.eu/legal-content/CS/TXT/?uri=uriserv:OJ.L_.2008.039.01.0016.01.CES&	[url]	k?	http://eur-lex.europa.eu/legal-content/CS/TXT/?uri=uriserv:OJ.L_.2008.039.01.0016.01.CES&
</s>
</p>
<p>
<s>
Nařízení	nařízení	k1gNnSc1	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
1334	[number]	k4	1334
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
o	o	k7c6	o
látkách	látka	k1gFnPc6	látka
určených	určený	k2eAgFnPc6d1	určená
k	k	k7c3	k
aromatizaci	aromatizace	k1gFnSc3	aromatizace
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
složkách	složka	k1gFnPc6	složka
potravin	potravina	k1gFnPc2	potravina
vyznačujících	vyznačující	k2eAgFnPc2d1	vyznačující
se	se	k3xPyFc4	se
aromatem	aroma	k1gNnSc7	aroma
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
nařízení	nařízení	k1gNnSc1	nařízení
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
č.	č.	k?	č.
1601	[number]	k4	1601
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
,	,	kIx,	,
nařízení	nařízení	k1gNnSc1	nařízení
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
2232	[number]	k4	2232
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
a	a	k8xC	a
č.	č.	k?	č.
110	[number]	k4	110
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
a	a	k8xC	a
směrnice	směrnice	k1gFnSc1	směrnice
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Text	text	k1gInSc1	text
s	s	k7c7	s
významem	význam	k1gInSc7	význam
pro	pro	k7c4	pro
EHP	EHP	kA	EHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgMnSc1d1	úřední
věstník	věstník	k1gMnSc1	věstník
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
354	[number]	k4	354
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1725	[number]	k4	1725
<g/>
-	-	kIx~	-
<g/>
5074	[number]	k4	5074
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://eur-lex.europa.eu/legal-content/CS/TXT/?uri=CELEX%3A32008R1334	[url]	k4	http://eur-lex.europa.eu/legal-content/CS/TXT/?uri=CELEX%3A32008R1334
</s>
</p>
<p>
<s>
NEŠPOR	Nešpor	k1gMnSc1	Nešpor
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
a	a	k8xC	a
medicína	medicína	k1gFnSc1	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Praktický	praktický	k2eAgMnSc1d1	praktický
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
95	[number]	k4	95
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
6739	[number]	k4	6739
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.drnespor.eu/AASouvi3.doc	[url]	k1gFnSc1	http://www.drnespor.eu/AASouvi3.doc
</s>
</p>
<p>
<s>
NEŠPOR	Nešpor	k1gMnSc1	Nešpor
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Zůstat	zůstat	k5eAaPmF	zůstat
střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
:	:	kIx,	:
praktické	praktický	k2eAgInPc4d1	praktický
návody	návod	k1gInPc4	návod
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
problém	problém	k1gInSc4	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
blízké	blízký	k2eAgFnPc1d1	blízká
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
236	[number]	k4	236
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7294	[number]	k4	7294
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.drnespor.eu	[url]	k1gInSc2	http://www.drnespor.eu
</s>
</p>
<p>
<s>
RAZVODOVSKY	RAZVODOVSKY	kA	RAZVODOVSKY
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
E.	E.	kA	E.
Fraction	Fraction	k1gInSc1	Fraction
of	of	k?	of
stroke	strokat	k5eAaPmIp3nS	strokat
mortality	mortalita	k1gFnPc4	mortalita
attributable	attributable	k6eAd1	attributable
to	ten	k3xDgNnSc4	ten
alcohol	alcohol	k1gInSc1	alcohol
consumption	consumption	k1gInSc1	consumption
in	in	k?	in
Russia	Russia	k1gFnSc1	Russia
<g/>
.	.	kIx.	.
</s>
<s>
Adicciones	Adicciones	k1gInSc1	Adicciones
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
,	,	kIx,	,
iss	iss	k?	iss
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
126	[number]	k4	126
<g/>
–	–	k?	–
<g/>
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
214	[number]	k4	214
<g/>
-	-	kIx~	-
<g/>
4840	[number]	k4	4840
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WHO	WHO	kA	WHO
<g/>
.	.	kIx.	.
</s>
<s>
Global	globat	k5eAaImAgInS	globat
status	status	k1gInSc1	status
report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
alcohol	alcohol	k1gInSc4	alcohol
and	and	k?	and
health	health	k1gInSc1	health
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
World	World	k1gInSc1	World
Health	Healtha	k1gFnPc2	Healtha
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
156475	[number]	k4	156475
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.who.int/substance_abuse/publications/global_alcohol_report/en/	[url]	k?	http://www.who.int/substance_abuse/publications/global_alcohol_report/en/
</s>
</p>
<p>
<s>
EU	EU	kA	EU
č.	č.	k?	č.
1576	[number]	k4	1576
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Methanol	methanol	k1gInSc1	methanol
</s>
</p>
<p>
<s>
Bioethanol	Bioethanol	k1gInSc1	Bioethanol
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Midgley	Midglea	k1gFnSc2	Midglea
</s>
</p>
<p>
<s>
Butanol	butanol	k1gInSc1	butanol
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ethanol	ethanol	k1gInSc1	ethanol
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ethanol	ethanol	k1gInSc4	ethanol
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
etanol	etanol	k1gInSc1	etanol
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Aethylalkohol	Aethylalkohol	k1gInSc1	Aethylalkohol
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
