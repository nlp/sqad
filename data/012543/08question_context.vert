<s>
Ethanol	ethanol	k1gInSc1	ethanol
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
chemii	chemie	k1gFnSc4	chemie
dle	dle	k7c2	dle
PČP	PČP	kA	PČP
etanol	etanol	k1gInSc1	etanol
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ethylalkohol	ethylalkohol	k1gInSc4	ethylalkohol
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
špiritus	špiritus	k?	špiritus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejnižší	nízký	k2eAgInSc1d3	nejnižší
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
kapalinu	kapalina	k1gFnSc4	kapalina
ostré	ostrý	k2eAgFnSc3d1	ostrá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
zředění	zředění	k1gNnSc6	zředění
příjemné	příjemný	k2eAgFnSc2d1	příjemná
alkoholické	alkoholický	k2eAgFnSc2d1	alkoholická
vůně	vůně	k1gFnSc2	vůně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
