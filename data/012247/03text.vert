<p>
<s>
Šarafiový	Šarafiový	k2eAgInSc1d1	Šarafiový
vodopád	vodopád	k1gInSc1	vodopád
je	být	k5eAaImIp3nS	být
ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
selektivní	selektivní	k2eAgInSc4d1	selektivní
vodopád	vodopád	k1gInSc4	vodopád
v	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Tatrách	Tatra	k1gFnPc6	Tatra
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Žiarske	Žiarske	k1gFnSc6	Žiarske
dolině	dolina	k1gFnSc6	dolina
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
podloží	podloží	k1gNnSc2	podloží
je	být	k5eAaImIp3nS	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
biotitovými	biotitový	k2eAgFnPc7d1	biotitová
a	a	k8xC	a
dvojslídnatými	dvojslídnatý	k2eAgFnPc7d1	dvojslídnatý
pararulami	pararula	k1gFnPc7	pararula
<g/>
.	.	kIx.	.
</s>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
Šarafiový	Šarafiový	k2eAgInSc1d1	Šarafiový
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1480	[number]	k4	1480
m	m	kA	m
široký	široký	k2eAgInSc1d1	široký
1	[number]	k4	1
m.	m.	k?	m.
Celá	celý	k2eAgFnSc1d1	celá
kaskáda	kaskáda	k1gFnSc1	kaskáda
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stupeň	stupeň	k1gInSc4	stupeň
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
3,5	[number]	k4	3,5
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
po	po	k7c6	po
zelené	zelený	k2eAgFnSc6d1	zelená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
od	od	k7c2	od
Žiarske	Žiarsk	k1gFnSc2	Žiarsk
chaty	chata	k1gFnSc2	chata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Tatry	Tatra	k1gFnPc1	Tatra
Vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
Západné	Západný	k2eAgInPc1d1	Západný
<g/>
,	,	kIx,	,
Belanské	Belanský	k2eAgInPc1d1	Belanský
(	(	kIx(	(
<g/>
TVZB	TVZB	kA	TVZB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Podrobný	podrobný	k2eAgInSc1d1	podrobný
turistický	turistický	k2eAgInSc1d1	turistický
atlas	atlas	k1gInSc1	atlas
ISBN	ISBN	kA	ISBN
83-87873-86-1	[number]	k4	83-87873-86-1
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Štátna	Štáten	k2eAgFnSc1d1	Štátna
ochrana	ochrana	k1gFnSc1	ochrana
prírody	príroda	k1gFnSc2	príroda
SR	SR	kA	SR
<g/>
,	,	kIx,	,
Vodopády	vodopád	k1gInPc7	vodopád
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Šarafiový	Šarafiový	k2eAgInSc1d1	Šarafiový
vodopád	vodopád	k1gInSc1	vodopád
</s>
</p>
