<s>
Včelí	včelí	k2eAgMnSc1d1	včelí
trubec	trubec	k1gMnSc1	trubec
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
z	z	k7c2	z
neoplozeného	oplozený	k2eNgNnSc2d1	neoplozené
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
královny	královna	k1gFnSc2	královna
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
trubčic	trubčice	k1gFnPc2	trubčice
<g/>
,	,	kIx,	,
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
klást	klást	k5eAaImF	klást
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
neoplozená	oplozený	k2eNgFnSc1d1	neoplozená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
