<s>
Partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
(	(	kIx(	(
<g/>
také	také	k9	také
samobřezost	samobřezost	k1gFnSc1	samobřezost
nebo	nebo	k8xC	nebo
pannobřezost	pannobřezost	k1gFnSc1	pannobřezost
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
vývin	vývin	k1gInSc1	vývin
nového	nový	k2eAgMnSc2d1	nový
jedince	jedinec	k1gMnSc2	jedinec
z	z	k7c2	z
samičího	samičí	k2eAgNnSc2d1	samičí
vajíčka	vajíčko	k1gNnSc2	vajíčko
neoplozeného	oplozený	k2eNgMnSc2d1	oplozený
samčí	samčí	k2eAgInSc4d1	samčí
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
buňkou	buňka	k1gFnSc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
principiálně	principiálně	k6eAd1	principiálně
podobná	podobný	k2eAgFnSc1d1	podobná
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
i	i	k9	i
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
apomixie	apomixie	k1gFnSc1	apomixie
<g/>
.	.	kIx.	.
</s>
<s>
Partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
u	u	k7c2	u
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
např.	např.	kA	např.
u	u	k7c2	u
pijavenek	pijavenka	k1gFnPc2	pijavenka
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Mšice	mšice	k1gFnSc1	mšice
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
například	například	k6eAd1	například
mohou	moct	k5eAaImIp3nP	moct
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
obyčejně	obyčejně	k6eAd1	obyčejně
(	(	kIx(	(
<g/>
do	do	k7c2	do
horších	zlý	k2eAgFnPc2d2	horší
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
genetická	genetický	k2eAgFnSc1d1	genetická
různorodost	různorodost	k1gFnSc1	různorodost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
partenogeneticky	partenogeneticky	k6eAd1	partenogeneticky
(	(	kIx(	(
<g/>
genetické	genetický	k2eAgFnSc2d1	genetická
vady	vada	k1gFnSc2	vada
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
přenést	přenést	k5eAaPmF	přenést
na	na	k7c4	na
potomky	potomek	k1gMnPc4	potomek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Včelí	včelí	k2eAgMnSc1d1	včelí
trubec	trubec	k1gMnSc1	trubec
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
z	z	k7c2	z
neoplozeného	oplozený	k2eNgNnSc2d1	neoplozené
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
královny	královna	k1gFnSc2	královna
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
trubčic	trubčice	k1gFnPc2	trubčice
<g/>
,	,	kIx,	,
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
klást	klást	k5eAaImF	klást
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
neoplozená	oplozený	k2eNgFnSc1d1	neoplozená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
není	být	k5eNaImIp3nS	být
partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
příliš	příliš	k6eAd1	příliš
častá	častý	k2eAgFnSc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
popsána	popsat	k5eAaPmNgFnS	popsat
např.	např.	kA	např.
u	u	k7c2	u
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
partenogenetických	partenogenetický	k2eAgMnPc2d1	partenogenetický
hadů	had	k1gMnPc2	had
je	být	k5eAaImIp3nS	být
slepák	slepák	k1gInSc1	slepák
květinový	květinový	k2eAgInSc1d1	květinový
<g/>
,	,	kIx,	,
partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
i	i	k9	i
u	u	k7c2	u
hroznýše	hroznýš	k1gMnSc2	hroznýš
královského	královský	k2eAgMnSc2d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Varan	varan	k1gMnSc1	varan
komodský	komodský	k2eAgMnSc1d1	komodský
se	se	k3xPyFc4	se
rovněž	rovněž	k6eAd1	rovněž
může	moct	k5eAaImIp3nS	moct
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
partenogeneticky	partenogeneticky	k6eAd1	partenogeneticky
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
přirozeně	přirozeně	k6eAd1	přirozeně
zřejmě	zřejmě	k6eAd1	zřejmě
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
embryo	embryo	k1gNnSc4	embryo
partenogenetické	partenogenetický	k2eAgFnSc2d1	partenogenetická
myši	myš	k1gFnSc2	myš
uměle	uměle	k6eAd1	uměle
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
fúzí	fúze	k1gFnSc7	fúze
dvou	dva	k4xCgFnPc2	dva
jader	jádro	k1gNnPc2	jádro
ze	z	k7c2	z
samičích	samičí	k2eAgFnPc2d1	samičí
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
oocytů	oocyt	k1gInPc2	oocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
změněna	změněn	k2eAgFnSc1d1	změněna
exprese	exprese	k1gFnSc1	exprese
několika	několik	k4yIc2	několik
genů	gen	k1gInPc2	gen
ovládajících	ovládající	k2eAgInPc2d1	ovládající
imprinting	imprinting	k1gInSc4	imprinting
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
myši	myš	k1gFnPc1	myš
dokonce	dokonce	k9	dokonce
dožijí	dožít	k5eAaPmIp3nP	dožít
až	až	k9	až
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
plodné	plodný	k2eAgFnPc1d1	plodná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
např.	např.	kA	např.
kobylka	kobylka	k1gFnSc1	kobylka
sága	sága	k1gFnSc1	sága
<g/>
,	,	kIx,	,
cvrčík	cvrčík	k1gInSc1	cvrčík
mravenčí	mravenčí	k2eAgInSc1d1	mravenčí
nebo	nebo	k8xC	nebo
Micromalthus	Micromalthus	k1gInSc1	Micromalthus
debilis	debilis	k1gFnSc2	debilis
<g/>
.	.	kIx.	.
thelytokie	thelytokie	k1gFnSc1	thelytokie
-	-	kIx~	-
z	z	k7c2	z
neoplozeného	oplozený	k2eNgNnSc2d1	neoplozené
vajíčka	vajíčko	k1gNnSc2	vajíčko
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
jen	jen	k9	jen
samice	samice	k1gFnSc1	samice
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
had	had	k1gMnSc1	had
slepák	slepák	k1gMnSc1	slepák
květinový	květinový	k2eAgMnSc1d1	květinový
<g/>
,	,	kIx,	,
rak	rak	k1gMnSc1	rak
mramorový	mramorový	k2eAgMnSc1d1	mramorový
<g/>
)	)	kIx)	)
arrhenotokie	arrhenotokie	k1gFnSc1	arrhenotokie
-	-	kIx~	-
protiklad	protiklad	k1gInSc1	protiklad
thelytokie	thelytokie	k1gFnSc2	thelytokie
<g/>
,	,	kIx,	,
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
se	se	k3xPyFc4	se
jen	jen	k9	jen
samci	samec	k1gMnSc3	samec
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
včelí	včelí	k2eAgMnSc1d1	včelí
trubec	trubec	k1gMnSc1	trubec
<g/>
)	)	kIx)	)
gynogeneze	gynogeneze	k1gFnPc1	gynogeneze
-	-	kIx~	-
samčí	samčí	k2eAgFnPc1d1	samčí
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesplynou	splynout	k5eNaPmIp3nP	splynout
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
spouštějí	spouštět	k5eAaImIp3nP	spouštět
vývin	vývin	k1gInSc4	vývin
neoplozeného	oplozený	k2eNgNnSc2d1	neoplozené
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
brouk	brouk	k1gMnSc1	brouk
vrtavec	vrtavec	k1gMnSc1	vrtavec
<g/>
)	)	kIx)	)
hybridogeneze	hybridogeneze	k1gFnSc1	hybridogeneze
-	-	kIx~	-
jev	jev	k1gInSc1	jev
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
u	u	k7c2	u
skokana	skokan	k1gMnSc2	skokan
zeleného	zelený	k2eAgNnSc2d1	zelené
</s>
