<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
i	i	k8xC	i
užité	užitý	k2eAgFnSc6d1	užitá
tvorbě	tvorba	k1gFnSc6	tvorba
využívá	využívat	k5eAaImIp3nS	využívat
techniku	technika	k1gFnSc4	technika
luminografie	luminografie	k1gFnSc1	luminografie
Jan	Jan	k1gMnSc1	Jan
Pohribný	Pohribný	k2eAgMnSc1d1	Pohribný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
aranžovanou	aranžovaný	k2eAgFnSc4d1	aranžovaná
a	a	k8xC	a
konceptuální	konceptuální	k2eAgFnSc4d1	konceptuální
fotografii	fotografia	k1gFnSc4	fotografia
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
–	–	k?	–
volném	volný	k2eAgInSc6d1	volný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
