<s>
Který	který	k3yIgMnSc1	který
český	český	k2eAgMnSc1d1	český
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
aranžovanou	aranžovaný	k2eAgFnSc4d1	aranžovaná
a	a	k8xC	a
konceptuální	konceptuální	k2eAgFnSc4d1	konceptuální
fotografii	fotografia	k1gFnSc4	fotografia
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
techniku	technika	k1gFnSc4	technika
luminografie	luminografie	k1gFnSc2	luminografie
<g/>
?	?	kIx.	?
</s>
