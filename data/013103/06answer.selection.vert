<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Hippies	Hippiesa	k1gFnPc2	Hippiesa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
působilo	působit	k5eAaImAgNnS	působit
ponejvíce	ponejvíce	k6eAd1	ponejvíce
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
praktikovalo	praktikovat	k5eAaImAgNnS	praktikovat
ideologii	ideologie	k1gFnSc3	ideologie
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
volné	volný	k2eAgFnSc2d1	volná
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
