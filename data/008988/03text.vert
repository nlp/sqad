<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc3d2	menší
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
země	zem	k1gFnPc1	zem
–	–	k?	–
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc1	Surinam
a	a	k8xC	a
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
–	–	k?	–
leží	ležet	k5eAaImIp3nS	ležet
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kontinentem	kontinent	k1gInSc7	kontinent
prochází	procházet	k5eAaImIp3nS	procházet
rovník	rovník	k1gInSc1	rovník
a	a	k8xC	a
obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
;	;	kIx,	;
kontinent	kontinent	k1gInSc1	kontinent
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
ohraničen	ohraničen	k2eAgMnSc1d1	ohraničen
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
je	být	k5eAaImIp3nS	být
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
úzkou	úzký	k2eAgFnSc7d1	úzká
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
šíjí	šíj	k1gFnSc7	šíj
většinou	většina	k1gFnSc7	většina
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Střední	střední	k2eAgFnSc4d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
kontinentů	kontinent	k1gInPc2	kontinent
došlo	dojít	k5eAaPmAgNnS	dojít
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Drakeův	Drakeův	k2eAgInSc1d1	Drakeův
průliv	průliv	k1gInSc1	průliv
jižní	jižní	k2eAgInSc1d1	jižní
cíp	cíp	k1gInSc1	cíp
kontinentu	kontinent	k1gInSc2	kontinent
tvořený	tvořený	k2eAgInSc1d1	tvořený
ostrovem	ostrov	k1gInSc7	ostrov
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
od	od	k7c2	od
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Ohňové	ohňový	k2eAgFnSc2d1	ohňová
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Hornův	Hornův	k2eAgInSc1d1	Hornův
mys	mys	k1gInSc1	mys
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
a	a	k8xC	a
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
Ameriky	Amerika	k1gFnPc1	Amerika
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c4	po
Amerigo	Amerigo	k1gNnSc4	Amerigo
Vespuccim	Vespuccima	k1gFnPc2	Vespuccima
<g/>
.	.	kIx.	.
</s>
<s>
Poznal	poznat	k5eAaPmAgMnS	poznat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
objevil	objevit	k5eAaPmAgMnS	objevit
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
není	být	k5eNaImIp3nS	být
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nový	nový	k2eAgInSc4d1	nový
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
pohoří	pohoří	k1gNnSc2	pohoří
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
oblasti	oblast	k1gFnPc4	oblast
porostlé	porostlý	k2eAgNnSc1d1	porostlé
tropickým	tropický	k2eAgInSc7d1	tropický
deštným	deštný	k2eAgInSc7d1	deštný
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
pevniny	pevnina	k1gFnSc2	pevnina
východně	východně	k6eAd1	východně
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
osídlení	osídlení	k1gNnSc1	osídlení
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
přechodem	přechod	k1gInSc7	přechod
lidí	člověk	k1gMnPc2	člověk
přes	přes	k7c4	přes
pevninský	pevninský	k2eAgInSc4d1	pevninský
most	most	k1gInSc4	most
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Beringově	Beringův	k2eAgInSc6d1	Beringův
průlivu	průliv	k1gInSc6	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
i	i	k9	i
náznaky	náznak	k1gInPc1	náznak
migrace	migrace	k1gFnSc2	migrace
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jižního	jižní	k2eAgInSc2d1	jižní
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
kontinent	kontinent	k1gInSc1	kontinent
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
kolonií	kolonie	k1gFnPc2	kolonie
pod	pod	k7c7	pod
španělskou	španělský	k2eAgFnSc7d1	španělská
a	a	k8xC	a
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
12	[number]	k4	12
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
republik	republika	k1gFnPc2	republika
a	a	k8xC	a
3	[number]	k4	3
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Falklandy	Falkland	k1gInPc1	Falkland
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
jihoamerickému	jihoamerický	k2eAgInSc3d1	jihoamerický
kontinentu	kontinent	k1gInSc3	kontinent
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
také	také	k9	také
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
kontrolována	kontrolovat	k5eAaImNgFnS	kontrolovat
zeměmi	zem	k1gFnPc7	zem
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Karibiku	Karibikum	k1gNnSc6	Karibikum
se	se	k3xPyFc4	se
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
k	k	k7c3	k
Severní	severní	k2eAgFnSc3d1	severní
nebo	nebo	k8xC	nebo
Střední	střední	k2eAgFnSc3d1	střední
Americe	Amerika	k1gFnSc3	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
48	[number]	k4	48
km	km	kA	km
širokou	široký	k2eAgFnSc7d1	široká
Panamskou	panamský	k2eAgFnSc7d1	Panamská
šíjí	šíj	k1gFnSc7	šíj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rozhraní	rozhraní	k1gNnSc4	rozhraní
obou	dva	k4xCgInPc2	dva
světadílů	světadíl	k1gInPc2	světadíl
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
až	až	k9	až
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Panamou	Panama	k1gFnSc7	Panama
a	a	k8xC	a
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
k	k	k7c3	k
Severní	severní	k2eAgFnSc3d1	severní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc4	souostroví
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
Aruba	Aruba	k1gFnSc1	Aruba
<g/>
,	,	kIx,	,
Curaçao	curaçao	k1gNnSc1	curaçao
<g/>
,	,	kIx,	,
Bonaire	Bonair	k1gInSc5	Bonair
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
spravované	spravovaný	k2eAgInPc4d1	spravovaný
jihoamerickými	jihoamerický	k2eAgInPc7d1	jihoamerický
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Punta	punto	k1gNnPc1	punto
Gallinas	Gallinasa	k1gFnPc2	Gallinasa
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Guajira	Guajir	k1gInSc2	Guajir
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
°	°	k?	°
27	[number]	k4	27
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cabo	Cabo	k1gMnSc1	Cabo
Froward	Froward	k1gMnSc1	Froward
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Brunswick	Brunswicka	k1gFnPc2	Brunswicka
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
(	(	kIx(	(
<g/>
53	[number]	k4	53
<g/>
°	°	k?	°
54	[number]	k4	54
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Západní	západní	k2eAgInSc1d1	západní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Punta	punto	k1gNnSc2	punto
Pariñ	Pariñ	k1gFnSc2	Pariñ
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Peru	Peru	k1gNnSc6	Peru
(	(	kIx(	(
<g/>
81	[number]	k4	81
<g/>
°	°	k?	°
22	[number]	k4	22
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Východní	východní	k2eAgInSc1d1	východní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
mys	mys	k1gInSc1	mys
Branco	Branco	k1gMnSc1	Branco
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
°	°	k?	°
45	[number]	k4	45
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
<g/>
Nejzazší	zadní	k2eAgInPc4d3	nejzazší
body	bod	k1gInPc4	bod
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc4d1	severní
bod	bod	k1gInSc4	bod
<g/>
:	:	kIx,	:
ostrovy	ostrov	k1gInPc4	ostrov
Los	los	k1gInSc1	los
Monjes	Monjes	k1gInSc1	Monjes
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Venezuely	Venezuela	k1gFnSc2	Venezuela
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
ostrov	ostrov	k1gInSc1	ostrov
Diego	Diego	k1gNnSc1	Diego
Ramírez	Ramírez	k1gInSc1	Ramírez
v	v	k7c6	v
Drakeově	Drakeův	k2eAgInSc6d1	Drakeův
průlivu	průliv	k1gInSc6	průliv
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Chile	Chile	k1gNnSc2	Chile
(	(	kIx(	(
<g/>
56	[number]	k4	56
<g/>
°	°	k?	°
32	[number]	k4	32
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Západní	západní	k2eAgInSc1d1	západní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Darwinův	Darwinův	k2eAgInSc1d1	Darwinův
ostrov	ostrov	k1gInSc1	ostrov
na	na	k7c6	na
Galapágách	Galapágy	k1gFnPc6	Galapágy
spravovaných	spravovaný	k2eAgFnPc6d1	spravovaná
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
(	(	kIx(	(
<g/>
92	[number]	k4	92
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
leží	ležet	k5eAaImIp3nS	ležet
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
ostrov	ostrov	k1gInSc1	ostrov
spravovaný	spravovaný	k2eAgInSc1d1	spravovaný
Chile	Chile	k1gNnSc3	Chile
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
už	už	k6eAd1	už
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
nepřiřazuje	přiřazovat	k5eNaImIp3nS	přiřazovat
k	k	k7c3	k
Jižní	jižní	k2eAgFnSc3d1	jižní
Americe	Amerika	k1gFnSc3	Amerika
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
k	k	k7c3	k
Oceánii	Oceánie	k1gFnSc3	Oceánie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Východní	východní	k2eAgInSc1d1	východní
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Ilha	Ilha	k1gFnSc1	Ilha
de	de	k?	de
Martin	Martin	k1gInSc1	Martin
Vaz	vaz	k1gInSc1	vaz
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Brazílie	Brazílie	k1gFnSc2	Brazílie
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
°	°	k?	°
51	[number]	k4	51
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Jihoamerický	jihoamerický	k2eAgInSc4d1	jihoamerický
subkontinent	subkontinent	k1gInSc4	subkontinent
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
velké	velký	k2eAgInPc4d1	velký
geografické	geografický	k2eAgInPc4d1	geografický
celky	celek	k1gInPc4	celek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velehorské	velehorský	k2eAgFnPc1d1	velehorská
Andy	Anda	k1gFnPc1	Anda
na	na	k7c6	na
západě	západ	k1gInSc6	západ
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc4	tři
velké	velký	k2eAgFnPc4d1	velká
nížiny	nížina	k1gFnPc4	nížina
navýchod	navýchod	k1gInSc4	navýchod
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
pohoří	pohořet	k5eAaPmIp3nP	pohořet
východně	východně	k6eAd1	východně
od	od	k7c2	od
AndNa	AndN	k1gInSc2	AndN
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
kontinentu	kontinent	k1gInSc2	kontinent
tvoří	tvořit	k5eAaImIp3nP	tvořit
Andy	Anda	k1gFnPc1	Anda
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
pevninské	pevninský	k2eAgNnSc4d1	pevninské
pohoří	pohoří	k1gNnSc4	pohoří
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velehory	velehora	k1gFnPc1	velehora
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
7	[number]	k4	7
500	[number]	k4	500
km	km	kA	km
od	od	k7c2	od
Venezuely	Venezuela	k1gFnSc2	Venezuela
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
až	až	k9	až
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
cípu	cíp	k1gInSc3	cíp
Patagonie	Patagonie	k1gFnSc2	Patagonie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
And	Anda	k1gFnPc2	Anda
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
959	[number]	k4	959
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
mimo	mimo	k7c4	mimo
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
kontinentu	kontinent	k1gInSc2	kontinent
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Santa	Santo	k1gNnSc2	Santo
Cruz	Cruz	k1gMnSc1	Cruz
–	–	k?	–
Laguna	laguna	k1gFnSc1	laguna
del	del	k?	del
Carbon	Carbon	k1gInSc1	Carbon
105	[number]	k4	105
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
bodem	bod	k1gInSc7	bod
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
mys	mys	k1gInSc1	mys
Horn.	Horn.	k1gFnSc2	Horn.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
nížinou	nížina	k1gFnSc7	nížina
je	být	k5eAaImIp3nS	být
Amazonská	amazonský	k2eAgFnSc1d1	Amazonská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
tropická	tropický	k2eAgFnSc1d1	tropická
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
odvodňována	odvodňovat	k5eAaImNgFnS	odvodňovat
Amazonkou	Amazonka	k1gFnSc7	Amazonka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
přítoky	přítok	k1gInPc7	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
And	Anda	k1gFnPc2	Anda
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
na	na	k7c4	na
východ	východ	k1gInSc4	východ
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
Amazonka	Amazonka	k1gFnSc1	Amazonka
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
7	[number]	k4	7
062	[number]	k4	062
km	km	kA	km
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvodnější	vodný	k2eAgFnSc7d3	vodný
řekou	řeka	k1gFnSc7	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
kontinentu	kontinent	k1gInSc2	kontinent
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Orinocká	orinocký	k2eAgFnSc1d1	Orinocká
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
Amazonskou	amazonský	k2eAgFnSc7d1	Amazonská
nížinou	nížina	k1gFnSc7	nížina
<g/>
,	,	kIx,	,
Guyanskou	Guyanský	k2eAgFnSc7d1	Guyanská
vysočinou	vysočina	k1gFnSc7	vysočina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
venezuelským	venezuelský	k2eAgNnSc7d1	venezuelské
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
nížina	nížina	k1gFnSc1	nížina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeky	řeka	k1gFnPc1	řeka
Paraguay	Paraguay	k1gFnSc1	Paraguay
a	a	k8xC	a
Paraná	Paraná	k1gFnSc1	Paraná
tvořící	tvořící	k2eAgFnSc2d1	tvořící
říční	říční	k2eAgInSc4d1	říční
systém	systém	k1gInSc4	systém
přitékají	přitékat	k5eAaImIp3nP	přitékat
se	se	k3xPyFc4	se
severu	sever	k1gInSc2	sever
z	z	k7c2	z
Pantanalu	Pantanal	k1gInSc2	Pantanal
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
subtropickou	subtropický	k2eAgFnSc4d1	subtropická
bažinatou	bažinatý	k2eAgFnSc4d1	bažinatá
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysočinami	vysočina	k1gFnPc7	vysočina
jsou	být	k5eAaImIp3nP	být
Guyanská	Guyanský	k2eAgFnSc1d1	Guyanská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Východopatagonská	Východopatagonský	k2eAgFnSc1d1	Východopatagonský
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
cíp	cíp	k1gInSc1	cíp
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
především	především	k9	především
Argentinou	Argentina	k1gFnSc7	Argentina
<g/>
,	,	kIx,	,
Uruguayí	Uruguay	k1gFnSc7	Uruguay
a	a	k8xC	a
Chile	Chile	k1gNnSc7	Chile
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
svébytný	svébytný	k2eAgInSc4d1	svébytný
region	region	k1gInSc4	region
Cono	Cono	k1gMnSc1	Cono
Sur	Sur	k1gMnSc1	Sur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnPc4	geologie
a	a	k8xC	a
geomorfologie	geomorfologie	k1gFnPc4	geomorfologie
===	===	k?	===
</s>
</p>
<p>
<s>
Okraj	okraj	k1gInSc1	okraj
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
geologicky	geologicky	k6eAd1	geologicky
aktivním	aktivní	k2eAgInSc7d1	aktivní
okrajem	okraj	k1gInSc7	okraj
kontinentu	kontinent	k1gInSc2	kontinent
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
subdukční	subdukční	k2eAgFnSc2d1	subdukční
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Atlantská	atlantský	k2eAgFnSc1d1	Atlantská
strana	strana	k1gFnSc1	strana
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
pasivní	pasivní	k2eAgInSc1d1	pasivní
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnPc1d1	východní
hory	hora	k1gFnPc1	hora
kontinentu	kontinent	k1gInSc2	kontinent
mají	mít	k5eAaImIp3nP	mít
prekambrický	prekambrický	k2eAgInSc4d1	prekambrický
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
překryt	překrýt	k5eAaPmNgInS	překrýt
pískovcem	pískovec	k1gInSc7	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinných	nížinný	k2eAgFnPc6d1	nížinná
strukturách	struktura	k1gFnPc6	struktura
řek	řeka	k1gFnPc2	řeka
převažují	převažovat	k5eAaImIp3nP	převažovat
třetihorní	třetihorní	k2eAgInPc1d1	třetihorní
a	a	k8xC	a
čtvrtohorní	čtvrtohorní	k2eAgInPc1d1	čtvrtohorní
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
tvoří	tvořit	k5eAaImIp3nS	tvořit
patagonská	patagonský	k2eAgFnSc1d1	patagonská
platforma	platforma	k1gFnSc1	platforma
základnu	základna	k1gFnSc4	základna
patagonské	patagonský	k2eAgFnSc2d1	patagonská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Orogenní	orogenní	k2eAgFnPc1d1	orogenní
Andy	Anda	k1gFnPc1	Anda
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinou	většina	k1gFnSc7	většina
vrstvy	vrstva	k1gFnSc2	vrstva
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
poprekambrické	poprekambrický	k2eAgInPc1d1	poprekambrický
sedimenty	sediment	k1gInPc1	sediment
<g/>
,	,	kIx,	,
a	a	k8xC	a
pohoří	pohořet	k5eAaPmIp3nS	pohořet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
středního	střední	k2eAgNnSc2d1	střední
a	a	k8xC	a
mladšího	mladý	k2eAgNnSc2d2	mladší
prekambria	prekambrium	k1gNnSc2	prekambrium
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
And	Anda	k1gFnPc2	Anda
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
geologicky	geologicky	k6eAd1	geologicky
aktivním	aktivní	k2eAgInSc6d1	aktivní
okraji	okraj	k1gInSc6	okraj
kontinentu	kontinent	k1gInSc2	kontinent
bohatý	bohatý	k2eAgInSc4d1	bohatý
výskyt	výskyt	k1gInSc4	výskyt
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
a	a	k8xC	a
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
byla	být	k5eAaImAgFnS	být
dílem	dílo	k1gNnSc7	dílo
prakontinentu	prakontinent	k1gInSc2	prakontinent
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
je	být	k5eAaImIp3nS	být
podobnost	podobnost	k1gFnSc1	podobnost
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
čediče	čedič	k1gInSc2	čedič
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
vytvoření	vytvoření	k1gNnSc6	vytvoření
východní	východní	k2eAgInPc1d1	východní
okraje	okraj	k1gInPc1	okraj
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnPc1d1	stejná
linie	linie	k1gFnPc1	linie
pískovců	pískovec	k1gInPc2	pískovec
a	a	k8xC	a
důkazy	důkaz	k1gInPc4	důkaz
zalednění	zalednění	k1gNnSc2	zalednění
v	v	k7c6	v
permu	perm	k1gInSc6	perm
a	a	k8xC	a
karbonu	karbon	k1gInSc6	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
kontinentu	kontinent	k1gInSc2	kontinent
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
několikerým	několikerý	k4xRyIgNnSc7	několikerý
zaledněním	zalednění	k1gNnSc7	zalednění
ve	v	k7c6	v
čtvrtohorách	čtvrtohory	k1gFnPc6	čtvrtohory
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
jevy	jev	k1gInPc7	jev
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ledovcová	ledovcový	k2eAgNnPc4d1	ledovcové
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
morény	moréna	k1gFnPc4	moréna
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
při	při	k7c6	při
tání	tání	k1gNnSc6	tání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celosvětově	celosvětově	k6eAd1	celosvětově
důležité	důležitý	k2eAgFnPc4d1	důležitá
zásoby	zásoba	k1gFnPc4	zásoba
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
ledek	ledek	k1gInSc1	ledek
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
měď	měď	k1gFnSc1	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgInPc4	všechen
pásy	pás	k1gInPc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Celoroční	celoroční	k2eAgInSc1d1	celoroční
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
proud	proud	k1gInSc1	proud
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgNnSc4d1	tropické
pásmo	pásmo	k1gNnSc4	pásmo
tlakových	tlakový	k2eAgFnPc2d1	tlaková
níží	níže	k1gFnPc2	níže
(	(	kIx(	(
<g/>
ITCZ	ITCZ	kA	ITCZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
tropických	tropický	k2eAgFnPc2d1	tropická
cyklón	cyklóna	k1gFnPc2	cyklóna
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
subtropické	subtropický	k2eAgFnSc2d1	subtropická
tlakové	tlakový	k2eAgFnSc2d1	tlaková
výše	výše	k1gFnSc2	výše
a	a	k8xC	a
pasátové	pasátový	k2eAgInPc1d1	pasátový
větry	vítr	k1gInPc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Studený	studený	k2eAgInSc1d1	studený
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
proud	proud	k1gInSc1	proud
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
účinek	účinek	k1gInSc4	účinek
ochlazení	ochlazení	k1gNnSc2	ochlazení
povrchové	povrchový	k2eAgFnSc2d1	povrchová
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
severního	severní	k2eAgNnSc2d1	severní
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vytvoření	vytvoření	k1gNnSc4	vytvoření
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ochlazený	ochlazený	k2eAgInSc1d1	ochlazený
vzduch	vzduch	k1gInSc1	vzduch
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
trvalou	trvalý	k2eAgFnSc4d1	trvalá
inverzi	inverze	k1gFnSc4	inverze
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
trvalé	trvalý	k2eAgFnSc2d1	trvalá
tlakové	tlakový	k2eAgFnSc2d1	tlaková
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
konvenkci	konvenkce	k1gFnSc4	konvenkce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
pouště	poušť	k1gFnPc4	poušť
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
tropická	tropický	k2eAgFnSc1d1	tropická
poloha	poloha	k1gFnSc1	poloha
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
období	období	k1gNnSc6	období
jižní	jižní	k2eAgFnSc2d1	jižní
zimy	zima	k1gFnSc2	zima
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tropické	tropický	k2eAgFnSc2d1	tropická
konvenční	konvenční	k2eAgFnSc2d1	konvenční
zóny	zóna	k1gFnSc2	zóna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
centrální	centrální	k2eAgFnSc2d1	centrální
Amazonie	Amazonie	k1gFnSc2	Amazonie
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
silné	silný	k2eAgFnSc2d1	silná
srážky	srážka	k1gFnSc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc4d1	tropické
pásmo	pásmo	k1gNnSc4	pásmo
uvnitř	uvnitř	k6eAd1	uvnitř
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
celoroční	celoroční	k2eAgFnPc4d1	celoroční
dešťové	dešťový	k2eAgFnPc4d1	dešťová
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
teplá	teplý	k2eAgFnSc1d1	teplá
tlaková	tlakový	k2eAgFnSc1d1	tlaková
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
tropů	trop	k1gInPc2	trop
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
letní	letní	k2eAgFnPc4d1	letní
dešťové	dešťový	k2eAgFnPc4d1	dešťová
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyky	jazyk	k1gInPc1	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Dvěma	dva	k4xCgInPc7	dva
hlavními	hlavní	k2eAgInPc7d1	hlavní
jazyky	jazyk	k1gInPc7	jazyk
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
jsou	být	k5eAaImIp3nP	být
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsky	portugalsky	k6eAd1	portugalsky
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
španělsky	španělsky	k6eAd1	španělsky
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
bývalých	bývalý	k2eAgFnPc6d1	bývalá
španělských	španělský	k2eAgFnPc6d1	španělská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
evropské	evropský	k2eAgInPc1d1	evropský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
pouze	pouze	k6eAd1	pouze
okrajově	okrajově	k6eAd1	okrajově
<g/>
:	:	kIx,	:
angličtina	angličtina	k1gFnSc1	angličtina
v	v	k7c6	v
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
,	,	kIx,	,
nizozemština	nizozemština	k1gFnSc1	nizozemština
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
ve	v	k7c6	v
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
tu	tu	k6eAd1	tu
existují	existovat	k5eAaImIp3nP	existovat
indiánské	indiánský	k2eAgInPc4d1	indiánský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
výjimky	výjimka	k1gFnPc4	výjimka
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
guaraní	guaraní	k1gNnSc1	guaraní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
,	,	kIx,	,
a	a	k8xC	a
kečuánština	kečuánština	k1gFnSc1	kečuánština
a	a	k8xC	a
aymarština	aymarština	k1gFnSc1	aymarština
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
a	a	k8xC	a
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
každý	každý	k3xTgInSc4	každý
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
–	–	k?	–
geografické	geografický	k2eAgInPc4d1	geografický
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
:	:	kIx,	:
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
–	–	k?	–
6959	[number]	k4	6959
m	m	kA	m
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
<g/>
:	:	kIx,	:
Ojos	Ojos	k1gInSc1	Ojos
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
–	–	k?	–
6893	[number]	k4	6893
m	m	kA	m
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
<g/>
:	:	kIx,	:
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
–	–	k?	–
Ucayali	Ucayali	k1gFnSc1	Ucayali
<g/>
,	,	kIx,	,
Marañ	Marañ	k1gFnSc1	Marañ
<g/>
)	)	kIx)	)
–	–	k?	–
7	[number]	k4	7
062	[number]	k4	062
km	km	kA	km
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Titicaca	Titicaca	k1gFnSc1	Titicaca
–	–	k?	–
8	[number]	k4	8
288	[number]	k4	288
km2	km2	k4	km2
(	(	kIx(	(
<g/>
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
:	:	kIx,	:
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
–	–	k?	–
48	[number]	k4	48
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
203	[number]	k4	203
429	[number]	k4	429
773	[number]	k4	773
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
8	[number]	k4	8
514	[number]	k4	514
877	[number]	k4	877
km2	km2	k4	km2
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
</s>
</p>
<p>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Průvodce	průvodka	k1gFnSc3	průvodka
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
