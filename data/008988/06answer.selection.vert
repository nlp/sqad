<s>
Dvěma	dva	k4xCgInPc7	dva
hlavními	hlavní	k2eAgInPc7d1	hlavní
jazyky	jazyk	k1gInPc7	jazyk
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
jsou	být	k5eAaImIp3nP	být
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
