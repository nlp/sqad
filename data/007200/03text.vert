<s>
Tygr	tygr	k1gMnSc1	tygr
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgFnPc2d1	současná
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
díky	díky	k7c3	díky
charakteristickým	charakteristický	k2eAgMnPc3d1	charakteristický
tmavým	tmavý	k2eAgMnPc3d1	tmavý
pruhům	pruh	k1gInPc3	pruh
na	na	k7c6	na
zlatožluté	zlatožlutý	k2eAgFnSc6d1	zlatožlutá
či	či	k8xC	či
rudohnědé	rudohnědý	k2eAgFnSc3d1	rudohnědá
srsti	srst	k1gFnSc3	srst
nezaměnitelný	zaměnitelný	k2eNgInSc1d1	nezaměnitelný
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
osm	osm	k4xCc4	osm
až	až	k8xS	až
devět	devět	k4xCc4	devět
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
vyhubeny	vyhubit	k5eAaPmNgFnP	vyhubit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
malým	malý	k2eAgInSc7d1	malý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
kontrastně	kontrastně	k6eAd1	kontrastně
zbarveným	zbarvený	k2eAgMnSc7d1	zbarvený
tygrem	tygr	k1gMnSc7	tygr
sumaterským	sumaterský	k2eAgNnPc3d1	sumaterské
a	a	k8xC	a
velkým	velký	k2eAgNnPc3d1	velké
<g/>
,	,	kIx,	,
světleji	světle	k6eAd2	světle
zbarveným	zbarvený	k2eAgMnSc7d1	zbarvený
tygrem	tygr	k1gMnSc7	tygr
ussurijským	ussurijský	k2eAgMnSc7d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
trupu	trup	k1gInSc2	trup
od	od	k7c2	od
140	[number]	k4	140
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
ocasu	ocas	k1gInSc2	ocas
kolem	kolem	k7c2	kolem
60	[number]	k4	60
cm	cm	kA	cm
s	s	k7c7	s
váhou	váha	k1gFnSc7	váha
od	od	k7c2	od
120	[number]	k4	120
kg	kg	kA	kg
u	u	k7c2	u
samce	samec	k1gInSc2	samec
a	a	k8xC	a
90	[number]	k4	90
kg	kg	kA	kg
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
a	a	k8xC	a
indický	indický	k2eAgMnSc1d1	indický
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
běžně	běžně	k6eAd1	běžně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc1	délka
trupu	trup	k1gInSc2	trup
přes	přes	k7c4	přes
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
90	[number]	k4	90
cm	cm	kA	cm
a	a	k8xC	a
váha	váha	k1gFnSc1	váha
samců	samec	k1gMnPc2	samec
mnohdy	mnohdy	k6eAd1	mnohdy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
kg	kg	kA	kg
<g/>
;	;	kIx,	;
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
váhy	váha	k1gFnPc4	váha
kolem	kolem	k7c2	kolem
130	[number]	k4	130
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
parametrům	parametr	k1gInPc3	parametr
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
po	po	k7c6	po
ledním	lední	k2eAgInSc6d1	lední
a	a	k8xC	a
hnědém	hnědý	k2eAgMnSc6d1	hnědý
medvědovi	medvěd	k1gMnSc6	medvěd
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgMnSc7d3	veliký
suchozemským	suchozemský	k2eAgMnSc7d1	suchozemský
predátorem	predátor	k1gMnSc7	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
běžné	běžný	k2eAgInPc1d1	běžný
poddruhy	poddruh	k1gInPc1	poddruh
stojící	stojící	k2eAgFnSc4d1	stojící
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
výše	vysoce	k6eAd2	vysoce
zmíněnými	zmíněný	k2eAgInPc7d1	zmíněný
extrémy	extrém	k1gInPc7	extrém
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
tygra	tygr	k1gMnSc4	tygr
čínského	čínský	k2eAgMnSc4d1	čínský
a	a	k8xC	a
tygra	tygr	k1gMnSc4	tygr
indočínského	indočínský	k2eAgMnSc4d1	indočínský
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgInSc7d3	nejmenší
poddruhem	poddruh	k1gInSc7	poddruh
byl	být	k5eAaImAgInS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
samotáři	samotář	k1gMnPc1	samotář
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
různé	různý	k2eAgInPc4d1	různý
biotopy	biotop	k1gInPc4	biotop
<g/>
,	,	kIx,	,
od	od	k7c2	od
tropických	tropický	k2eAgInPc2d1	tropický
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
stepi	step	k1gFnPc4	step
a	a	k8xC	a
mokřady	mokřad	k1gInPc1	mokřad
až	až	k9	až
k	k	k7c3	k
severským	severský	k2eAgFnPc3d1	severská
tundrám	tundra	k1gFnPc3	tundra
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
sahal	sahat	k5eAaImAgInS	sahat
od	od	k7c2	od
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
středoasijských	středoasijský	k2eAgFnPc2d1	středoasijská
stepí	step	k1gFnPc2	step
<g/>
,	,	kIx,	,
od	od	k7c2	od
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
přes	přes	k7c4	přes
Zadní	zadní	k2eAgFnSc4d1	zadní
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Čínu	Čína	k1gFnSc4	Čína
na	na	k7c4	na
ruský	ruský	k2eAgInSc4d1	ruský
dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
také	také	k6eAd1	také
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
indonéské	indonéský	k2eAgInPc4d1	indonéský
ostrovy	ostrov	k1gInPc4	ostrov
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Jáva	Jáva	k1gFnSc1	Jáva
a	a	k8xC	a
Bali	Bali	k1gNnSc1	Bali
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tygr	tygr	k1gMnSc1	tygr
obývá	obývat	k5eAaImIp3nS	obývat
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgInPc4d1	malý
zbytky	zbytek	k1gInPc4	zbytek
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
poddruhy	poddruh	k1gInPc1	poddruh
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
vyhubeny	vyhuben	k2eAgInPc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
3	[number]	k4	3
000	[number]	k4	000
až	až	k9	až
5	[number]	k4	5
000	[number]	k4	000
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
či	či	k8xC	či
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
klasifikován	klasifikován	k2eAgMnSc1d1	klasifikován
IUCN	IUCN	kA	IUCN
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
tigris	tigris	k1gFnSc7	tigris
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
odvozenina	odvozenina	k1gFnSc1	odvozenina
řeckého	řecký	k2eAgInSc2d1	řecký
τ	τ	k?	τ
(	(	kIx(	(
<g/>
tígris	tígris	k1gFnSc2	tígris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prapůvod	prapůvod	k1gInSc1	prapůvod
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
však	však	k9	však
orientální	orientální	k2eAgInSc4d1	orientální
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
íránský	íránský	k2eAgMnSc1d1	íránský
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
příbuznost	příbuznost	k1gFnSc4	příbuznost
s	s	k7c7	s
avestánským	avestánský	k2eAgMnSc7d1	avestánský
tigri-	tigri-	k?	tigri-
(	(	kIx(	(
<g/>
šíp	šíp	k1gInSc1	šíp
<g/>
)	)	kIx)	)
a	a	k8xC	a
staroperským	staroperský	k2eAgFnPc3d1	staroperský
tigra-	tigra-	k?	tigra-
(	(	kIx(	(
<g/>
špička	špička	k1gFnSc1	špička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
staroindickým	staroindický	k2eAgInSc7d1	staroindický
teĵ	teĵ	k?	teĵ
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Indoíránská	indoíránský	k2eAgNnPc1d1	indoíránský
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
vykládají	vykládat	k5eAaImIp3nP	vykládat
jako	jako	k8xC	jako
domácí	domácí	k2eAgFnSc1d1	domácí
odvozenina	odvozenina	k1gFnSc1	odvozenina
z	z	k7c2	z
pra-indoevropského	prandoevropský	k2eAgInSc2d1	pra-indoevropský
kořene	kořen	k1gInSc2	kořen
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
tieg-	tieg-	k?	tieg-
(	(	kIx(	(
<g/>
píchat	píchat	k5eAaImF	píchat
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
ostrý	ostrý	k2eAgInSc4d1	ostrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgFnPc4d1	velká
kočky	kočka	k1gFnPc4	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
je	být	k5eAaImIp3nS	být
však	však	k9	však
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
evolučního	evoluční	k2eAgInSc2d1	evoluční
vývoje	vývoj	k1gInSc2	vývoj
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
ramci	ramce	k1gFnSc6	ramce
ródu	ródu	k5eAaPmIp1nS	ródu
Panthera	Panthera	k1gFnSc1	Panthera
relativně	relativně	k6eAd1	relativně
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Panthera	Panthero	k1gNnSc2	Panthero
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
však	však	k9	však
týče	týkat	k5eAaImIp3nS	týkat
přesnějšího	přesný	k2eAgNnSc2d2	přesnější
určení	určení	k1gNnSc2	určení
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
zatím	zatím	k6eAd1	zatím
tápou	tápat	k5eAaImIp3nP	tápat
v	v	k7c6	v
temnotě	temnota	k1gFnSc6	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Morfologické	morfologický	k2eAgInPc1d1	morfologický
a	a	k8xC	a
genetické	genetický	k2eAgInPc1d1	genetický
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
bazální	bazální	k2eAgFnSc7d1	bazální
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
skupinou	skupina	k1gFnSc7	skupina
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
žijícím	žijící	k2eAgMnPc3d1	žijící
druhům	druh	k1gInPc3	druh
rodu	rod	k1gInSc2	rod
Panthera	Panthera	k1gFnSc1	Panthera
(	(	kIx(	(
<g/>
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
jaguár	jaguár	k1gMnSc1	jaguár
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
irbis	irbis	k1gFnSc1	irbis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
velké	velký	k2eAgFnSc2d1	velká
kočky	kočka	k1gFnSc2	kočka
podobné	podobný	k2eAgFnSc2d1	podobná
tygrovi	tygr	k1gMnSc3	tygr
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
popsány	popsat	k5eAaPmNgFnP	popsat
jako	jako	k8xC	jako
Panthera	Panthera	k1gFnSc1	Panthera
palaeosinensis	palaeosinensis	k1gFnSc1	palaeosinensis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kočka	kočka	k1gFnSc1	kočka
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
pleistocénu	pleistocén	k1gInSc2	pleistocén
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
dnešní	dnešní	k2eAgMnSc1d1	dnešní
tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
pravé	pravý	k2eAgFnPc4d1	pravá
tygří	tygří	k2eAgFnPc4d1	tygří
fosilie	fosilie	k1gFnPc4	fosilie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Jávy	Jáva	k1gFnSc2	Jáva
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
datované	datovaný	k2eAgInPc1d1	datovaný
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
1,6	[number]	k4	1,6
až	až	k8xS	až
1,8	[number]	k4	1,8
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
raného	raný	k2eAgInSc2d1	raný
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
pleistocénu	pleistocén	k1gInSc2	pleistocén
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
četné	četný	k2eAgInPc1d1	četný
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Sumatry	Sumatra	k1gFnSc2	Sumatra
a	a	k8xC	a
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
na	na	k7c4	na
Altaj	Altaj	k1gInSc4	Altaj
<g/>
,	,	kIx,	,
severních	severní	k2eAgFnPc2d1	severní
oblastí	oblast	k1gFnPc2	oblast
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
Asie	Asie	k1gFnSc2	Asie
tygr	tygr	k1gMnSc1	tygr
pronikl	proniknout	k5eAaPmAgMnS	proniknout
podle	podle	k7c2	podle
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
až	až	k9	až
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
pleistocénu	pleistocén	k1gInSc6	pleistocén
<g/>
.	.	kIx.	.
</s>
<s>
Fosilními	fosilní	k2eAgInPc7d1	fosilní
nálezy	nález	k1gInPc7	nález
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
i	i	k8xC	i
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
Beringii	Beringie	k1gFnSc6	Beringie
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
až	až	k9	až
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
se	se	k3xPyFc4	se
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
i	i	k9	i
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sachalin	Sachalin	k1gInSc1	Sachalin
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tygři	tygr	k1gMnPc1	tygr
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
nedosahovali	dosahovat	k5eNaImAgMnP	dosahovat
plné	plný	k2eAgFnSc3d1	plná
velikosti	velikost	k1gFnSc3	velikost
současného	současný	k2eAgMnSc2d1	současný
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgFnPc1d1	nalezená
kostry	kostra	k1gFnPc1	kostra
prehistorických	prehistorický	k2eAgMnPc2d1	prehistorický
tygrů	tygr	k1gMnPc2	tygr
z	z	k7c2	z
Jávy	Jáva	k1gFnSc2	Jáva
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
jejich	jejich	k3xOp3gMnPc2	jejich
současníků	současník	k1gMnPc2	současník
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
větší	veliký	k2eAgInPc1d2	veliký
exempláře	exemplář	k1gInPc1	exemplář
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
tygru	tygr	k1gMnSc3	tygr
indickému	indický	k2eAgMnSc3d1	indický
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc1	dva
prehistorické	prehistorický	k2eAgInPc1d1	prehistorický
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc2	tigris
acutidens	acutidens	k6eAd1	acutidens
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
Panthera	Panthero	k1gNnSc2	Panthero
tigris	tigris	k1gFnSc2	tigris
trinilensis	trinilensis	k1gFnSc2	trinilensis
z	z	k7c2	z
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
kosterní	kosterní	k2eAgInPc1d1	kosterní
nálezy	nález	k1gInPc1	nález
přítomnost	přítomnost	k1gFnSc1	přítomnost
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
pleistocénu	pleistocén	k1gInSc6	pleistocén
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
začátkem	začátkem	k7c2	začátkem
holocénu	holocén	k1gInSc2	holocén
(	(	kIx(	(
<g/>
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tygra	tygr	k1gMnSc4	tygr
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
genetických	genetický	k2eAgFnPc2d1	genetická
analýz	analýza	k1gFnPc2	analýza
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
indočínský	indočínský	k2eAgMnSc1d1	indočínský
nejpůvodnějším	původní	k2eAgInSc7d3	nejpůvodnější
poddruhem	poddruh	k1gInSc7	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejpodobnější	podobný	k2eAgMnSc1d3	nejpodobnější
prehistorickým	prehistorický	k2eAgMnPc3d1	prehistorický
tygrům	tygr	k1gMnPc3	tygr
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
před	před	k7c4	před
70	[number]	k4	70
000	[number]	k4	000
–	–	k?	–
100	[number]	k4	100
000	[number]	k4	000
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
dnešní	dnešní	k2eAgInPc1d1	dnešní
tygří	tygří	k2eAgInPc1d1	tygří
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
postupně	postupně	k6eAd1	postupně
šířil	šířit	k5eAaImAgMnS	šířit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
západ	západ	k1gInSc4	západ
a	a	k8xC	a
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
pronikl	proniknout	k5eAaPmAgInS	proniknout
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
v	v	k7c6	v
holocénu	holocén	k1gInSc6	holocén
<g/>
.	.	kIx.	.
</s>
<s>
Prehistorický	prehistorický	k2eAgInSc1d1	prehistorický
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
i	i	k9	i
na	na	k7c6	na
filipínském	filipínský	k2eAgInSc6d1	filipínský
ostrově	ostrov	k1gInSc6	ostrov
Palawan	Palawan	k1gInSc1	Palawan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
odtud	odtud	k6eAd1	odtud
a	a	k8xC	a
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Bornea	Borneo	k1gNnSc2	Borneo
tygři	tygr	k1gMnPc1	tygr
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
5	[number]	k4	5
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
stavů	stav	k1gInPc2	stav
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
tygři	tygr	k1gMnPc1	tygr
nikdy	nikdy	k6eAd1	nikdy
nežili	žít	k5eNaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
nálezy	nález	k1gInPc1	nález
však	však	k9	však
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
20	[number]	k4	20
000	[number]	k4	000
lety	let	k1gInPc7	let
tygr	tygr	k1gMnSc1	tygr
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pronikl	proniknout	k5eAaPmAgMnS	proniknout
i	i	k9	i
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
37	[number]	k4	37
000	[number]	k4	000
lety	let	k1gInPc7	let
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
sinhaleyus	sinhaleyus	k1gInSc1	sinhaleyus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledního	poslední	k2eAgNnSc2d1	poslední
maximálního	maximální	k2eAgNnSc2d1	maximální
zalednění	zalednění	k1gNnSc2	zalednění
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
20	[number]	k4	20
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
odkryla	odkrýt	k5eAaPmAgFnS	odkrýt
pevninský	pevninský	k2eAgInSc4d1	pevninský
most	most	k1gInSc4	most
mezi	mezi	k7c7	mezi
ostrovem	ostrov	k1gInSc7	ostrov
a	a	k8xC	a
indickým	indický	k2eAgInSc7d1	indický
subkontinentem	subkontinent	k1gInSc7	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnPc1	lance
lvi	lev	k1gMnPc1	lev
ani	ani	k8xC	ani
tygři	tygr	k1gMnPc1	tygr
nežijí	žít	k5eNaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
9	[number]	k4	9
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
již	již	k9	již
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
tygra	tygr	k1gMnSc2	tygr
malajského	malajský	k2eAgMnSc2d1	malajský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
jacksoni	jacksoň	k1gFnSc6	jacksoň
<g/>
)	)	kIx)	)
z	z	k7c2	z
Malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
poddruh	poddruh	k1gInSc1	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgFnPc1d1	genetická
analýzy	analýza	k1gFnPc1	analýza
hovoří	hovořit	k5eAaImIp3nP	hovořit
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
schéma	schéma	k1gNnSc4	schéma
rozdělení	rozdělení	k1gNnSc2	rozdělení
žijících	žijící	k2eAgMnPc2d1	žijící
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
do	do	k7c2	do
šesti	šest	k4xCc2	šest
různých	různý	k2eAgInPc2d1	různý
podddruhů	podddruh	k1gInPc2	podddruh
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
poddruhy	poddruh	k1gInPc7	poddruh
obývajícími	obývající	k2eAgInPc7d1	obývající
asijskou	asijský	k2eAgFnSc4d1	asijská
pevninu	pevnina	k1gFnSc4	pevnina
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgFnPc4d1	značná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
v	v	k7c6	v
genetickém	genetický	k2eAgInSc6d1	genetický
kódu	kód	k1gInSc6	kód
tygrů	tygr	k1gMnPc2	tygr
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
byla	být	k5eAaImAgFnS	být
separována	separovat	k5eAaBmNgFnS	separovat
od	od	k7c2	od
kontinentu	kontinent	k1gInSc2	kontinent
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
6	[number]	k4	6
000	[number]	k4	000
až	až	k9	až
12	[number]	k4	12
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
konci	konec	k1gInSc6	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
kontinentem	kontinent	k1gInSc7	kontinent
bylo	být	k5eAaImAgNnS	být
zatopeno	zatopen	k2eAgNnSc1d1	zatopeno
(	(	kIx(	(
<g/>
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Malacký	malacký	k2eAgInSc1d1	malacký
průliv	průliv	k1gInSc1	průliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
vymřelým	vymřelý	k2eAgMnSc7d1	vymřelý
tygrem	tygr	k1gMnSc7	tygr
kaspickým	kaspický	k2eAgMnSc7d1	kaspický
a	a	k8xC	a
tygrem	tygr	k1gMnSc7	tygr
ussurijským	ussurijský	k2eAgMnSc7d1	ussurijský
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
možná	možná	k9	možná
oba	dva	k4xCgMnPc1	dva
měli	mít	k5eAaImAgMnP	mít
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
dohromady	dohromady	k6eAd1	dohromady
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
poddruh	poddruh	k1gInSc1	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
<g/>
,	,	kIx,	,
též	též	k9	též
tygr	tygr	k1gMnSc1	tygr
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
amurský	amurský	k2eAgMnSc1d1	amurský
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
korejský	korejský	k2eAgMnSc1d1	korejský
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
či	či	k8xC	či
tygr	tygr	k1gMnSc1	tygr
severočínský	severočínský	k2eAgMnSc1d1	severočínský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
altaica	altaica	k1gMnSc1	altaica
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
největších	veliký	k2eAgInPc2d3	veliký
poddruhů	poddruh	k1gInPc2	poddruh
tygra	tygr	k1gMnSc2	tygr
byl	být	k5eAaImAgInS	být
kdysi	kdysi	k6eAd1	kdysi
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
oblastech	oblast	k1gFnPc6	oblast
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluhou	k7c2	zásluhou
masivního	masivní	k2eAgNnSc2d1	masivní
pronásledování	pronásledování	k1gNnSc2	pronásledování
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zredukována	zredukován	k2eAgFnSc1d1	zredukována
na	na	k7c4	na
sotva	sotva	k6eAd1	sotva
30	[number]	k4	30
jedinců	jedinec	k1gMnPc2	jedinec
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
rusko-čínské	rusko-čínský	k2eAgFnSc6d1	rusko-čínská
a	a	k8xC	a
čínsko-korejské	čínskoorejský	k2eAgFnSc6d1	čínsko-korejský
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
záchranným	záchranný	k2eAgInPc3d1	záchranný
programům	program	k1gInPc3	program
se	se	k3xPyFc4	se
však	však	k9	však
naštěstí	naštěstí	k6eAd1	naštěstí
podařilo	podařit	k5eAaPmAgNnS	podařit
zvýšit	zvýšit	k5eAaPmF	zvýšit
stav	stav	k1gInSc4	stav
populace	populace	k1gFnSc2	populace
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
na	na	k7c4	na
současných	současný	k2eAgNnPc2d1	současné
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
až	až	k9	až
400	[number]	k4	400
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
čínský	čínský	k2eAgMnSc1d1	čínský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
amoyensis	amoyensis	k1gInSc1	amoyensis
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
čí	čí	k3xOyRgMnSc1	čí
tygr	tygr	k1gMnSc1	tygr
indočínský	indočínský	k2eAgMnSc1d1	indočínský
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
intenzivnější	intenzivní	k2eAgNnSc1d2	intenzivnější
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
bílé	bílý	k2eAgFnSc2d1	bílá
menší	malý	k2eAgFnSc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgFnPc1d1	tmavá
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
poddruh	poddruh	k1gInSc1	poddruh
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
oblastech	oblast	k1gFnPc6	oblast
Číny	Čína	k1gFnSc2	Čína
od	od	k7c2	od
38	[number]	k4	38
<g/>
.	.	kIx.	.
až	až	k9	až
40	[number]	k4	40
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
až	až	k9	až
k	k	k7c3	k
severním	severní	k2eAgFnPc3d1	severní
hranicím	hranice	k1gFnPc3	hranice
provincií	provincie	k1gFnPc2	provincie
Jün-nan	Jünana	k1gFnPc2	Jün-nana
<g/>
,	,	kIx,	,
Kuang-si	Kuange	k1gFnSc4	Kuang-se
a	a	k8xC	a
Kuang-tung	Kuangung	k1gInSc4	Kuang-tung
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poddruh	poddruh	k1gInSc1	poddruh
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhynulý	vyhynulý	k2eAgMnSc1d1	vyhynulý
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existuje	existovat	k5eAaImIp3nS	existovat
malá	malý	k2eAgFnSc1d1	malá
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
přežívá	přežívat	k5eAaImIp3nS	přežívat
několik	několik	k4yIc1	několik
exemplářů	exemplář	k1gInPc2	exemplář
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
provincie	provincie	k1gFnSc2	provincie
Kuang-tung	Kuangung	k1gInSc1	Kuang-tung
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zbytková	zbytkový	k2eAgFnSc1d1	zbytková
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
(	(	kIx(	(
<g/>
inbreeding	inbreeding	k1gInSc1	inbreeding
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chovné	chovný	k2eAgInPc1d1	chovný
programy	program	k1gInPc1	program
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
tygry	tygr	k1gMnPc4	tygr
ze	z	k7c2	z
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
byly	být	k5eAaImAgInP	být
spuštěny	spustit	k5eAaPmNgInP	spustit
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
a	a	k8xC	a
omezují	omezovat	k5eAaImIp3nP	omezovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
čínské	čínský	k2eAgFnSc6d1	čínská
zoo	zoo	k1gFnSc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
tygrů	tygr	k1gMnPc2	tygr
čínských	čínský	k2eAgMnPc2d1	čínský
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
nicméně	nicméně	k8xC	nicméně
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2005	[number]	k4	2005
a	a	k8xC	a
2007	[number]	k4	2007
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
z	z	k7c2	z
57	[number]	k4	57
na	na	k7c4	na
72	[number]	k4	72
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
Save	Sav	k1gInSc2	Sav
Chinas	Chinas	k1gInSc4	Chinas
Tigers	Tigers	k1gInSc1	Tigers
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
tygry	tygr	k1gMnPc4	tygr
čínské	čínský	k2eAgFnSc2d1	čínská
mimo	mimo	k7c4	mimo
Čínu	Čína	k1gFnSc4	Čína
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
rezervaci	rezervace	k1gFnSc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
zde	zde	k6eAd1	zde
získávají	získávat	k5eAaImIp3nP	získávat
lovecké	lovecký	k2eAgInPc4d1	lovecký
návyky	návyk	k1gInPc4	návyk
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
zkušenosti	zkušenost	k1gFnPc4	zkušenost
potřebné	potřebný	k2eAgFnPc4d1	potřebná
pro	pro	k7c4	pro
přežití	přežití	k1gNnPc4	přežití
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
znovuvysazování	znovuvysazování	k1gNnSc1	znovuvysazování
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
<g/>
,	,	kIx,	,
též	též	k9	též
tygr	tygr	k1gMnSc1	tygr
bengálský	bengálský	k2eAgMnSc1d1	bengálský
či	či	k8xC	či
tygr	tygr	k1gMnSc1	tygr
královský	královský	k2eAgMnSc1d1	královský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
tigris	tigris	k1gInSc1	tigris
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
ussurijským	ussurijský	k2eAgMnSc7d1	ussurijský
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
o	o	k7c4	o
primát	primát	k1gInSc4	primát
největších	veliký	k2eAgInPc2d3	veliký
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
však	však	k9	však
srst	srst	k1gFnSc1	srst
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
a	a	k8xC	a
světlejší	světlý	k2eAgNnSc1d2	světlejší
než	než	k8xS	než
u	u	k7c2	u
poddruhů	poddruh	k1gInPc2	poddruh
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
od	od	k7c2	od
údolí	údolí	k1gNnSc2	údolí
Indu	Indus	k1gInSc2	Indus
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
až	až	k9	až
do	do	k7c2	do
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Barmy	Barma	k1gFnSc2	Barma
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
poddruhu	poddruh	k1gInSc2	poddruh
exustuje	exustovat	k5eAaImIp3nS	exustovat
jistá	jistý	k2eAgFnSc1d1	jistá
genetická	genetický	k2eAgFnSc1d1	genetická
variabilita	variabilita	k1gFnSc1	variabilita
<g/>
,	,	kIx,	,
především	především	k9	především
tygři	tygr	k1gMnPc1	tygr
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
indických	indický	k2eAgMnPc2d1	indický
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
naopak	naopak	k6eAd1	naopak
značnou	značný	k2eAgFnSc4d1	značná
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
genetickou	genetický	k2eAgFnSc4d1	genetická
příbuznost	příbuznost	k1gFnSc4	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Sariska	Sarisko	k1gNnSc2	Sarisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tygr	tygr	k1gMnSc1	tygr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
geneticky	geneticky	k6eAd1	geneticky
velmi	velmi	k6eAd1	velmi
blízcí	blízký	k2eAgMnPc1d1	blízký
tygrům	tygr	k1gMnPc3	tygr
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Ranthambore	Ranthambor	k1gInSc5	Ranthambor
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
z	z	k7c2	z
Ranthambore	Ranthambor	k1gInSc5	Ranthambor
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
ideálními	ideální	k2eAgMnPc7d1	ideální
kandidáty	kandidát	k1gMnPc7	kandidát
pro	pro	k7c4	pro
možné	možný	k2eAgMnPc4d1	možný
budoucí	budoucí	k2eAgMnSc1d1	budoucí
znovuvysazení	znovuvysazení	k1gNnSc4	znovuvysazení
v	v	k7c6	v
Sarisce	Sariska	k1gFnSc6	Sariska
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
izolovaných	izolovaný	k2eAgFnPc6d1	izolovaná
oblastech	oblast	k1gFnPc6	oblast
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
2	[number]	k4	2
500	[number]	k4	500
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
400	[number]	k4	400
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
rozdíl	rozdíl	k1gInSc1	rozdíl
proti	proti	k7c3	proti
podstatně	podstatně	k6eAd1	podstatně
vyšším	vysoký	k2eAgInPc3d2	vyšší
počtům	počet	k1gInPc3	počet
udávaným	udávaný	k2eAgInPc3d1	udávaný
po	po	k7c6	po
dřívějším	dřívější	k2eAgNnSc6d1	dřívější
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
letech	let	k1gInPc6	let
2001-2002	[number]	k4	2001-2002
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
odlišných	odlišný	k2eAgFnPc2d1	odlišná
metod	metoda	k1gFnPc2	metoda
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
obě	dva	k4xCgNnPc4	dva
čísla	číslo	k1gNnPc4	číslo
tedy	tedy	k8xC	tedy
nelze	lze	k6eNd1	lze
přímo	přímo	k6eAd1	přímo
srovnat	srovnat	k5eAaPmF	srovnat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
součty	součet	k1gInPc1	součet
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
věrohodnější	věrohodný	k2eAgNnSc4d2	věrohodnější
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
je	být	k5eAaImIp3nS	být
klasifikován	klasifikován	k2eAgMnSc1d1	klasifikován
jako	jako	k8xS	jako
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Ochranářské	ochranářský	k2eAgFnPc1d1	ochranářská
organizace	organizace	k1gFnPc1	organizace
však	však	k9	však
stále	stále	k6eAd1	stále
nepřestávají	přestávat	k5eNaImIp3nP	přestávat
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
přísný	přísný	k2eAgInSc4d1	přísný
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
zákaz	zákaz	k1gInSc4	zákaz
kriminální	kriminální	k2eAgFnSc2d1	kriminální
organizace	organizace	k1gFnSc2	organizace
stále	stále	k6eAd1	stále
provozují	provozovat	k5eAaImIp3nP	provozovat
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
černý	černý	k2eAgInSc4d1	černý
trh	trh	k1gInSc4	trh
s	s	k7c7	s
tygřími	tygří	k2eAgFnPc7d1	tygří
kůžemi	kůže	k1gFnPc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
indočínský	indočínský	k2eAgMnSc1d1	indočínský
<g/>
,	,	kIx,	,
též	též	k9	též
tygr	tygr	k1gMnSc1	tygr
Corbettův	Corbettův	k2eAgMnSc1d1	Corbettův
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
corbetti	corbetť	k1gFnSc2	corbetť
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgNnSc1d2	tmavší
<g/>
,	,	kIx,	,
pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
od	od	k7c2	od
jižních	jižní	k2eAgFnPc2d1	jižní
čínských	čínský	k2eAgFnPc2d1	čínská
provincií	provincie	k1gFnPc2	provincie
Jün-nan	Jünany	k1gInPc2	Jün-nany
<g/>
,	,	kIx,	,
Kuang-si	Kuange	k1gFnSc4	Kuang-se
a	a	k8xC	a
Kuang-tung	Kuangung	k1gInSc4	Kuang-tung
na	na	k7c6	na
severu	sever	k1gInSc6	sever
k	k	k7c3	k
Malajskému	malajský	k2eAgInSc3d1	malajský
poloostrovu	poloostrov	k1gInSc3	poloostrov
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
divoká	divoký	k2eAgFnSc1d1	divoká
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
jedinců	jedinec	k1gMnPc2	jedinec
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
,	,	kIx,	,
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc6	Laos
<g/>
,	,	kIx,	,
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
i	i	k8xC	i
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
tvoří	tvořit	k5eAaImIp3nS	tvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
samostatný	samostatný	k2eAgInSc1d1	samostatný
poddruh	poddruh	k1gInSc1	poddruh
a	a	k8xC	a
zdejší	zdejší	k2eAgMnPc1d1	zdejší
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
klasifikováni	klasifikovat	k5eAaImNgMnP	klasifikovat
odděleně	odděleně	k6eAd1	odděleně
jakožto	jakožto	k8xS	jakožto
tygři	tygr	k1gMnPc1	tygr
malajští	malajský	k2eAgMnPc1d1	malajský
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
malajský	malajský	k2eAgMnSc1d1	malajský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
jacksoni	jacksoň	k1gFnSc6	jacksoň
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tygří	tygří	k2eAgFnSc4d1	tygří
populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tygr	tygr	k1gMnSc1	tygr
indočínský	indočínský	k2eAgMnSc1d1	indočínský
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
řazeného	řazený	k2eAgInSc2d1	řazený
společně	společně	k6eAd1	společně
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
indočínským	indočínský	k2eAgMnSc7d1	indočínský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
genetické	genetický	k2eAgInPc1d1	genetický
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
populací	populace	k1gFnSc7	populace
tygra	tygr	k1gMnSc2	tygr
na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
severněji	severně	k6eAd2	severně
žijícími	žijící	k2eAgMnPc7d1	žijící
tygry	tygr	k1gMnPc7	tygr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
všeho	všecek	k3xTgInSc2	všecek
nejsou	být	k5eNaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c4	v
zbarvení	zbarvení	k1gNnSc4	zbarvení
srsti	srst	k1gFnSc3	srst
či	či	k8xC	či
stavbě	stavba	k1gFnSc3	stavba
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
výskytu	výskyt	k1gInSc2	výskyt
malajského	malajský	k2eAgMnSc2d1	malajský
tygra	tygr	k1gMnSc2	tygr
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
plynulému	plynulý	k2eAgNnSc3d1	plynulé
prolínání	prolínání	k1gNnSc3	prolínání
s	s	k7c7	s
nejjižnějšími	jižní	k2eAgFnPc7d3	nejjižnější
oblastmi	oblast	k1gFnPc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
tygra	tygr	k1gMnSc2	tygr
indočínského	indočínský	k2eAgMnSc2d1	indočínský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Malajském	malajský	k2eAgInSc6d1	malajský
poloostrově	poloostrov	k1gInSc6	poloostrov
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
odhadů	odhad	k1gInPc2	odhad
IUCN	IUCN	kA	IUCN
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
sumatrae	sumatrae	k1gInSc1	sumatrae
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
kontrastně	kontrastně	k6eAd1	kontrastně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
poddruh	poddruh	k1gInSc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
často	často	k6eAd1	často
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
z	z	k7c2	z
žijích	žijích	k2eAgInPc2d2	žijích
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Nápadné	nápadný	k2eAgInPc1d1	nápadný
jsou	být	k5eAaImIp3nP	být
mohutné	mohutný	k2eAgInPc1d1	mohutný
licousy	licousy	k1gInPc1	licousy
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
poddruh	poddruh	k1gInSc1	poddruh
tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
přežívá	přežívat	k5eAaImIp3nS	přežívat
asi	asi	k9	asi
400	[number]	k4	400
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
Sumatry	Sumatra	k1gFnSc2	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
IUCN	IUCN	kA	IUCN
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
poddruh	poddruh	k1gInSc4	poddruh
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
jávský	jávský	k2eAgMnSc1d1	jávský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
sondaica	sondaica	k1gMnSc1	sondaica
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
velikostí	velikost	k1gFnSc7	velikost
srovnatelný	srovnatelný	k2eAgMnSc1d1	srovnatelný
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
sumaterským	sumaterský	k2eAgMnSc7d1	sumaterský
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
tmavě	tmavě	k6eAd1	tmavě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc4	pruh
velmi	velmi	k6eAd1	velmi
úzké	úzký	k2eAgInPc4d1	úzký
a	a	k8xC	a
hojné	hojný	k2eAgInPc4d1	hojný
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
spatřen	spatřit	k5eAaPmNgInS	spatřit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vyhynulého	vyhynulý	k2eAgMnSc4d1	vyhynulý
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
balica	balica	k1gMnSc1	balica
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejmenší	malý	k2eAgInSc4d3	nejmenší
poddruh	poddruh	k1gInSc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
tmavěji	tmavě	k6eAd2	tmavě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
než	než	k8xS	než
tygři	tygr	k1gMnPc1	tygr
sumaterští	sumaterský	k2eAgMnPc1d1	sumaterský
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
tygrů	tygr	k1gMnPc2	tygr
jávských	jávský	k2eAgMnPc2d1	jávský
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
široké	široký	k2eAgInPc1d1	široký
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
podobné	podobný	k2eAgInPc4d1	podobný
pruhům	pruh	k1gInPc3	pruh
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgMnSc2d1	sumaterský
než	než	k8xS	než
tygra	tygr	k1gMnSc2	tygr
jávského	jávský	k2eAgMnSc2d1	jávský
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
výskyt	výskyt	k1gInSc1	výskyt
pásů	pás	k1gInPc2	pás
tmavších	tmavý	k2eAgFnPc2d2	tmavší
skvrn	skvrna	k1gFnPc2	skvrna
mezi	mezi	k7c7	mezi
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Endemický	endemický	k2eAgInSc1d1	endemický
druh	druh	k1gInSc1	druh
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Bali	Bal	k1gFnSc2	Bal
byl	být	k5eAaImAgInS	být
zásluhou	zásluha	k1gFnSc7	zásluha
intezivního	intezivní	k2eAgInSc2d1	intezivní
lovu	lov	k1gInSc2	lov
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
již	již	k6eAd1	již
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
<g/>
,	,	kIx,	,
též	též	k9	též
tygr	tygr	k1gMnSc1	tygr
turanský	turanský	k2eAgMnSc1d1	turanský
či	či	k8xC	či
tygr	tygr	k1gMnSc1	tygr
perský	perský	k2eAgMnSc1d1	perský
(	(	kIx(	(
<g/>
P.	P.	kA	P.
t.	t.	k?	t.
virgata	virgata	k1gFnSc1	virgata
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyhynulý	vyhynulý	k2eAgInSc1d1	vyhynulý
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
lišil	lišit	k5eAaImAgInS	lišit
především	především	k9	především
početnějšími	početní	k2eAgInPc7d2	početnější
<g/>
,	,	kIx,	,
užšími	úzký	k2eAgInPc7d2	užší
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
světlé	světlý	k2eAgFnPc1d1	světlá
<g/>
,	,	kIx,	,
srst	srst	k1gFnSc1	srst
poměrně	poměrně	k6eAd1	poměrně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
rozkádala	rozkádat	k5eAaImAgFnS	rozkádat
od	od	k7c2	od
Anatolie	Anatolie	k1gFnSc2	Anatolie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
přes	přes	k7c4	přes
Írán	Írán	k1gInSc4	Írán
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
do	do	k7c2	do
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgInSc2	svůj
areálu	areál	k1gInSc2	areál
byl	být	k5eAaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
již	již	k6eAd1	již
před	před	k7c7	před
staletími	staletí	k1gNnPc7	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
kaspičtí	kaspický	k2eAgMnPc1d1	kaspický
tygři	tygr	k1gMnPc1	tygr
se	se	k3xPyFc4	se
udrželi	udržet	k5eAaPmAgMnP	udržet
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
i	i	k8xC	i
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
analýzy	analýza	k1gFnPc1	analýza
molekulárních	molekulární	k2eAgMnPc2d1	molekulární
biologů	biolog	k1gMnPc2	biolog
však	však	k9	však
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
totožný	totožný	k2eAgMnSc1d1	totožný
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
usssurijským	usssurijský	k2eAgMnSc7d1	usssurijský
a	a	k8xC	a
k	k	k7c3	k
oddělení	oddělení	k1gNnSc1	oddělení
obou	dva	k4xCgFnPc2	dva
populací	populace	k1gFnPc2	populace
došlo	dojít	k5eAaPmAgNnS	dojít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k8xS	až
zásahem	zásah	k1gInSc7	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
docházelo	docházet	k5eAaImAgNnS	docházet
ve	v	k7c6	v
zvěřincích	zvěřinec	k1gMnPc6	zvěřinec
<g/>
,	,	kIx,	,
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
i	i	k8xC	i
cirkusech	cirkus	k1gInPc6	cirkus
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
cíleně	cíleně	k6eAd1	cíleně
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zkřížení	zkřížení	k1gNnSc3	zkřížení
tygra	tygr	k1gMnSc2	tygr
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
velkou	velký	k2eAgFnSc7d1	velká
kočkou	kočka	k1gFnSc7	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgNnSc1d3	nejobvyklejší
je	být	k5eAaImIp3nS	být
zkřížení	zkřížení	k1gNnSc1	zkřížení
se	s	k7c7	s
lvem	lev	k1gMnSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc4	vzhled
koťat	kotě	k1gNnPc2	kotě
z	z	k7c2	z
takového	takový	k3xDgNnSc2	takový
spojení	spojení	k1gNnSc2	spojení
závisí	záviset	k5eAaImIp3nS	záviset
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
na	na	k7c4	na
kombinaci	kombinace	k1gFnSc4	kombinace
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
samce	samec	k1gMnSc4	samec
lva	lev	k1gMnSc4	lev
s	s	k7c7	s
tygřicí	tygřice	k1gFnSc7	tygřice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
liger	liger	k1gInSc1	liger
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
otcem	otec	k1gMnSc7	otec
tygr	tygr	k1gMnSc1	tygr
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
lvice	lvice	k1gFnSc2	lvice
<g/>
,	,	kIx,	,
potomek	potomek	k1gMnSc1	potomek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tigon	tigon	k1gInSc1	tigon
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
zbarvení	zbarvení	k1gNnSc4	zbarvení
kříženců	kříženec	k1gMnPc2	kříženec
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgInPc4d1	zastoupen
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Liger	Liger	k1gInSc1	Liger
má	mít	k5eAaImIp3nS	mít
světlou	světlý	k2eAgFnSc4d1	světlá
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc7d1	podobná
lvu	lev	k1gMnSc3	lev
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
ale	ale	k9	ale
světlé	světlý	k2eAgFnPc1d1	světlá
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
zhusta	zhusta	k6eAd1	zhusta
do	do	k7c2	do
skvrn	skvrna	k1gFnPc2	skvrna
se	se	k3xPyFc4	se
rozpadající	rozpadající	k2eAgInPc1d1	rozpadající
pruhy	pruh	k1gInPc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Tigon	Tigon	k1gMnSc1	Tigon
se	se	k3xPyFc4	se
lvu	lev	k1gInSc3	lev
podobá	podobat	k5eAaImIp3nS	podobat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
liger	liger	k1gInSc4	liger
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
hřívu	hříva	k1gFnSc4	hříva
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
tak	tak	k6eAd1	tak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
jako	jako	k8xC	jako
samci	samec	k1gMnPc1	samec
lva	lev	k1gMnSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Kříženci	kříženec	k1gMnPc1	kříženec
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
mohutní	mohutný	k2eAgMnPc1d1	mohutný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
efektu	efekt	k1gInSc2	efekt
heteroze	heteroze	k1gFnPc1	heteroze
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
kříženců	kříženec	k1gMnPc2	kříženec
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
neplodní	plodní	k2eNgFnPc1d1	neplodní
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
však	však	k9	však
rozmnožovací	rozmnožovací	k2eAgFnSc4d1	rozmnožovací
schopnost	schopnost	k1gFnSc4	schopnost
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zdokumentován	zdokumentován	k2eAgInSc1d1	zdokumentován
případ	případ	k1gInSc1	případ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
spáření	spáření	k1gNnSc2	spáření
samice	samice	k1gFnSc1	samice
ligera	ligera	k1gFnSc1	ligera
se	s	k7c7	s
lvem	lev	k1gMnSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
křížení	křížení	k1gNnSc4	křížení
tygra	tygr	k1gMnSc2	tygr
s	s	k7c7	s
levhartem	levhart	k1gMnSc7	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nepodložené	podložený	k2eNgInPc1d1	nepodložený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdokumentován	zdokumentován	k2eAgInSc1d1	zdokumentován
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
případ	případ	k1gInSc1	případ
takového	takový	k3xDgNnSc2	takový
spáření	spáření	k1gNnSc2	spáření
a	a	k8xC	a
koťata	kotě	k1gNnPc4	kotě
zahynula	zahynout	k5eAaPmAgFnS	zahynout
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k8xC	i
ke	k	k7c3	k
křížení	křížení	k1gNnSc3	křížení
mezi	mezi	k7c7	mezi
tygry	tygr	k1gMnPc7	tygr
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
v	v	k7c4	v
Current	Current	k1gInSc4	Current
Biology	biolog	k1gMnPc4	biolog
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
vykazovalo	vykazovat	k5eAaImAgNnS	vykazovat
však	však	k9	však
49	[number]	k4	49
ze	z	k7c2	z
105	[number]	k4	105
testovaných	testovaný	k2eAgMnPc2d1	testovaný
tygrů	tygr	k1gMnPc2	tygr
z	z	k7c2	z
pěti	pět	k4xCc2	pět
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
znaky	znak	k1gInPc4	znak
DNA	dno	k1gNnPc4	dno
odpovídající	odpovídající	k2eAgNnPc4d1	odpovídající
přesně	přesně	k6eAd1	přesně
některému	některý	k3yIgInSc3	některý
poddruhu	poddruh	k1gInSc3	poddruh
<g/>
,	,	kIx,	,
nešlo	jít	k5eNaImAgNnS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
hybridy	hybrid	k1gInPc4	hybrid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dobrá	dobrý	k2eAgFnSc1d1	dobrá
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
čistých	čistý	k2eAgFnPc2d1	čistá
linií	linie	k1gFnPc2	linie
poddruhů	poddruh	k1gInPc2	poddruh
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
parcích	park	k1gInPc6	park
a	a	k8xC	a
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
neudržitelnou	udržitelný	k2eNgFnSc7d1	neudržitelná
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
bocích	bok	k1gInPc6	bok
<g/>
,	,	kIx,	,
ocase	ocas	k1gInSc6	ocas
a	a	k8xC	a
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
končetin	končetina	k1gFnPc2	končetina
se	se	k3xPyFc4	se
poddle	poddle	k6eAd1	poddle
poddruhu	poddruh	k1gInSc2	poddruh
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
od	od	k7c2	od
zlatožluté	zlatožlutý	k2eAgFnSc2d1	zlatožlutá
k	k	k7c3	k
červenooranžové	červenooranžový	k2eAgFnSc3d1	červenooranžová
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
končetin	končetina	k1gFnPc2	končetina
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
či	či	k8xC	či
světle	světle	k6eAd1	světle
béžová	béžový	k2eAgFnSc1d1	béžová
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
strana	strana	k1gFnSc1	strana
ucha	ucho	k1gNnSc2	ucho
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
s	s	k7c7	s
nápadnou	nápadný	k2eAgFnSc7d1	nápadná
bílou	bílý	k2eAgFnSc7d1	bílá
skvrnou	skvrna	k1gFnSc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgInPc1d1	tmavý
svislé	svislý	k2eAgInPc1d1	svislý
pruhy	pruh	k1gInPc1	pruh
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
až	až	k9	až
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kočetinách	kočetina	k1gFnPc6	kočetina
jsou	být	k5eAaImIp3nP	být
pruhy	pruh	k1gInPc1	pruh
vodorovné	vodorovný	k2eAgInPc1d1	vodorovný
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvětlejší	světlý	k2eAgInPc4d3	nejsvětlejší
pruhy	pruh	k1gInPc4	pruh
najdeme	najít	k5eAaPmIp1nP	najít
u	u	k7c2	u
tygrů	tygr	k1gMnPc2	tygr
ussurijských	ussurijský	k2eAgMnPc2d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Světlejší	světlý	k2eAgInPc4d2	světlejší
pruhy	pruh	k1gInPc4	pruh
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
<g/>
,	,	kIx,	,
především	především	k9	především
severněji	severně	k6eAd2	severně
žijící	žijící	k2eAgMnPc1d1	žijící
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Nejtmavší	tmavý	k2eAgInPc4d3	nejtmavší
pruhy	pruh	k1gInPc4	pruh
mají	mít	k5eAaImIp3nP	mít
tygři	tygr	k1gMnPc1	tygr
z	z	k7c2	z
Indonésie	Indonésie	k1gFnSc2	Indonésie
a	a	k8xC	a
Malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
tmavost	tmavost	k1gFnSc1	tmavost
pruhů	pruh	k1gInPc2	pruh
tygra	tygr	k1gMnSc2	tygr
čínského	čínský	k2eAgMnSc2d1	čínský
a	a	k8xC	a
vyhynulého	vyhynulý	k2eAgMnSc2d1	vyhynulý
tygra	tygr	k1gMnSc2	tygr
kaspického	kaspický	k2eAgMnSc2d1	kaspický
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
žijící	žijící	k2eAgMnPc1d1	žijící
tygři	tygr	k1gMnPc1	tygr
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
široké	široký	k2eAgInPc4d1	široký
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
do	do	k7c2	do
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
severních	severní	k2eAgMnPc2d1	severní
tygrů	tygr	k1gMnPc2	tygr
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Nos	nos	k1gInSc1	nos
tygra	tygr	k1gMnSc2	tygr
je	být	k5eAaImIp3nS	být
růžový	růžový	k2eAgInSc1d1	růžový
<g/>
,	,	kIx,	,
s	s	k7c7	s
přibývajícím	přibývající	k2eAgNnSc7d1	přibývající
stářím	stáří	k1gNnSc7	stáří
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
mohou	moct	k5eAaImIp3nP	moct
objevovat	objevovat	k5eAaImF	objevovat
černé	černý	k2eAgInPc1d1	černý
flíčky	flíček	k1gInPc1	flíček
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
poddruhů	poddruh	k1gInPc2	poddruh
poměrně	poměrně	k6eAd1	poměrně
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgNnSc2d1	ussurijské
díky	díky	k7c3	díky
chladnému	chladný	k2eAgNnSc3d1	chladné
klimatu	klima	k1gNnSc3	klima
více	hodně	k6eAd2	hodně
huňatý	huňatý	k2eAgInSc1d1	huňatý
s	s	k7c7	s
delšími	dlouhý	k2eAgInPc7d2	delší
chlupy	chlup	k1gInPc7	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
letní	letní	k2eAgFnSc2d1	letní
srsti	srst	k1gFnSc2	srst
tygra	tygr	k1gMnSc2	tygr
indického	indický	k2eAgNnSc2d1	indické
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
8	[number]	k4	8
do	do	k7c2	do
15	[number]	k4	15
mm	mm	kA	mm
<g/>
,	,	kIx,	,
u	u	k7c2	u
chlupů	chlup	k1gInPc2	chlup
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
30	[number]	k4	30
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
délka	délka	k1gFnSc1	délka
srsti	srst	k1gFnSc2	srst
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
17	[number]	k4	17
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
40	[number]	k4	40
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgInSc2d1	ussurijský
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
25	[number]	k4	25
až	až	k9	až
40	[number]	k4	40
mm	mm	kA	mm
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
70	[number]	k4	70
až	až	k9	až
105	[number]	k4	105
mm	mm	kA	mm
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
délku	délka	k1gFnSc4	délka
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nápadní	nápadní	k2eAgFnSc7d1	nápadní
delší	dlouhý	k2eAgFnSc7d2	delší
srstí	srst	k1gFnSc7	srst
na	na	k7c6	na
zátylku	zátylek	k1gInSc6	zátylek
a	a	k8xC	a
mohutnými	mohutný	k2eAgInPc7d1	mohutný
licousy	licousy	k1gInPc7	licousy
<g/>
.	.	kIx.	.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1	přinejmenším
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
poddruhů	poddruh	k1gInPc2	poddruh
objevuje	objevovat	k5eAaImIp3nS	objevovat
zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
delší	dlouhý	k2eAgFnSc2d2	delší
krycí	krycí	k2eAgFnSc2d1	krycí
srsti	srst	k1gFnSc2	srst
a	a	k8xC	a
podsady	podsada	k1gFnSc2	podsada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
ochlupení	ochlupení	k1gNnSc2	ochlupení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
poddruhu	poddruh	k1gInSc2	poddruh
a	a	k8xC	a
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnSc1d1	letní
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
především	především	k9	především
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
výrazně	výrazně	k6eAd1	výrazně
kratší	krátký	k2eAgFnSc7d2	kratší
a	a	k8xC	a
méně	málo	k6eAd2	málo
hustá	hustý	k2eAgFnSc1d1	hustá
než	než	k8xS	než
zimní	zimní	k2eAgFnSc1d1	zimní
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
800	[number]	k4	800
chlupů	chlup	k1gInPc2	chlup
na	na	k7c4	na
centimetr	centimetr	k1gInSc4	centimetr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
indického	indický	k2eAgMnSc2d1	indický
<g/>
,	,	kIx,	,
3	[number]	k4	3
200	[number]	k4	200
chlupů	chlup	k1gInPc2	chlup
na	na	k7c4	na
centimetr	centimetr	k1gInSc4	centimetr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc1	hodnota
srovnatelné	srovnatelný	k2eAgFnPc1d1	srovnatelná
například	například	k6eAd1	například
s	s	k7c7	s
levhartem	levhart	k1gMnSc7	levhart
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
například	například	k6eAd1	například
rys	rys	k1gInSc4	rys
má	mít	k5eAaImIp3nS	mít
srst	srst	k1gFnSc1	srst
výrazně	výrazně	k6eAd1	výrazně
hustší	hustý	k2eAgFnSc1d2	hustší
(	(	kIx(	(
<g/>
až	až	k9	až
9	[number]	k4	9
000	[number]	k4	000
chlupů	chlup	k1gInPc2	chlup
na	na	k7c4	na
centimetr	centimetr	k1gInSc4	centimetr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kratší	krátký	k2eAgFnSc7d2	kratší
srstí	srst	k1gFnSc7	srst
letní	letní	k2eAgFnPc1d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Dojem	dojem	k1gInSc1	dojem
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
podzimní	podzimní	k2eAgFnSc2d1	podzimní
výměny	výměna	k1gFnSc2	výměna
srsti	srst	k1gFnSc2	srst
u	u	k7c2	u
severně	severně	k6eAd1	severně
žijících	žijící	k2eAgMnPc2d1	žijící
tygrů	tygr	k1gMnPc2	tygr
lze	lze	k6eAd1	lze
objasnit	objasnit	k5eAaPmF	objasnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
letní	letní	k2eAgFnSc1d1	letní
srst	srst	k1gFnSc1	srst
se	se	k3xPyFc4	se
před	před	k7c7	před
zimou	zima	k1gFnSc7	zima
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
k	k	k7c3	k
jarní	jarní	k2eAgFnSc3d1	jarní
výměně	výměna	k1gFnSc3	výměna
srsti	srst	k1gFnSc3	srst
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
u	u	k7c2	u
indických	indický	k2eAgMnPc2d1	indický
tygrů	tygr	k1gMnPc2	tygr
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgInPc1d1	tygří
drápy	dráp	k1gInPc1	dráp
rovněž	rovněž	k9	rovněž
prochází	procházet	k5eAaImIp3nP	procházet
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
obměnou	obměna	k1gFnSc7	obměna
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
odlupují	odlupovat	k5eAaImIp3nP	odlupovat
po	po	k7c6	po
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zcela	zcela	k6eAd1	zcela
odpadnou	odpadnout	k5eAaPmIp3nP	odpadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tygři	tygr	k1gMnPc1	tygr
často	často	k6eAd1	často
škrábou	škrábat	k5eAaImIp3nP	škrábat
měkčí	měkký	k2eAgFnSc4d2	měkčí
stromovou	stromový	k2eAgFnSc4d1	stromová
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
existují	existovat	k5eAaImIp3nP	existovat
u	u	k7c2	u
tygrů	tygr	k1gMnPc2	tygr
různé	různý	k2eAgFnSc2d1	různá
barevné	barevný	k2eAgFnSc2d1	barevná
odchylky	odchylka	k1gFnSc2	odchylka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
spíše	spíše	k9	spíše
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kulturního	kulturní	k2eAgNnSc2d1	kulturní
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
<g/>
.	.	kIx.	.
</s>
<s>
Neobvykle	obvykle	k6eNd1	obvykle
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
populárními	populární	k2eAgInPc7d1	populární
exempláři	exemplář	k1gInPc7	exemplář
jak	jak	k8xS	jak
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
chovech	chov	k1gInPc6	chov
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známí	známý	k1gMnPc1	známý
jsou	být	k5eAaImIp3nP	být
bílí	bílý	k2eAgMnPc1d1	bílý
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
pravý	pravý	k2eAgInSc4d1	pravý
albinismus	albinismus	k1gInSc4	albinismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
albinismus	albinismus	k1gInSc4	albinismus
částečný	částečný	k2eAgMnSc1d1	částečný
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
leucismus	leucismus	k1gInSc1	leucismus
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
duhovky	duhovka	k1gFnPc1	duhovka
pravých	pravý	k2eAgInPc2d1	pravý
albínů	albín	k1gInPc2	albín
jsou	být	k5eAaImIp3nP	být
červené	červená	k1gFnPc1	červená
<g/>
,	,	kIx,	,
bílí	bílý	k2eAgMnPc1d1	bílý
tygři	tygr	k1gMnPc1	tygr
mají	mít	k5eAaImIp3nP	mít
duhovky	duhovka	k1gFnPc4	duhovka
modré	modrý	k2eAgFnPc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bílých	bílý	k2eAgMnPc2d1	bílý
tygrů	tygr	k1gMnPc2	tygr
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
na	na	k7c6	na
srsti	srst	k1gFnSc6	srst
tmavé	tmavý	k2eAgInPc4d1	tmavý
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
bílí	bílý	k2eAgMnPc1d1	bílý
tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
velkou	velký	k2eAgFnSc7d1	velká
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
dnes	dnes	k6eAd1	dnes
známí	známý	k2eAgMnPc1d1	známý
bílí	bílý	k2eAgMnPc1d1	bílý
tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc7	potomek
tygřího	tygří	k2eAgInSc2d1	tygří
samce	samec	k1gInSc2	samec
odchyceného	odchycený	k2eAgInSc2d1	odchycený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
státě	stát	k1gInSc6	stát
Madhjapradéš	Madhjapradéš	k1gInSc1	Madhjapradéš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgMnSc1	žádný
bílý	bílý	k2eAgMnSc1d1	bílý
tygr	tygr	k1gMnSc1	tygr
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
velmi	velmi	k6eAd1	velmi
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
barevnou	barevný	k2eAgFnSc7d1	barevná
odchylkou	odchylka	k1gFnSc7	odchylka
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rufinismus	rufinismus	k1gInSc1	rufinismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tygrům	tygr	k1gMnPc3	tygr
chybí	chybit	k5eAaPmIp3nS	chybit
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
tmavý	tmavý	k2eAgInSc4d1	tmavý
až	až	k6eAd1	až
černý	černý	k2eAgInSc4d1	černý
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
červenohnědé	červenohnědý	k2eAgInPc1d1	červenohnědý
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
těchto	tento	k3xDgMnPc2	tento
jedinců	jedinec	k1gMnPc2	jedinec
bývá	bývat	k5eAaImIp3nS	bývat
výrazně	výrazně	k6eAd1	výrazně
narudlá	narudlý	k2eAgFnSc1d1	narudlá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
též	též	k9	též
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
zlatí	zlatý	k2eAgMnPc1d1	zlatý
tygři	tygr	k1gMnPc1	tygr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
takové	takový	k3xDgNnSc4	takový
zvíře	zvíře	k1gNnSc1	zvíře
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
z	z	k7c2	z
Alborzu	Alborz	k1gInSc2	Alborz
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
z	z	k7c2	z
Ásámu	Ásám	k1gInSc2	Ásám
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
zbarvené	zbarvený	k2eAgMnPc4d1	zbarvený
tygry	tygr	k1gMnPc4	tygr
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
i	i	k9	i
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
šlechtění	šlechtění	k1gNnSc6	šlechtění
byli	být	k5eAaImAgMnP	být
použiti	použít	k5eAaPmNgMnP	použít
především	především	k9	především
tygři	tygr	k1gMnPc1	tygr
indičtí	indický	k2eAgMnPc1d1	indický
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
tygři	tygr	k1gMnPc1	tygr
ussurijští	ussurijský	k2eAgMnPc1d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
tygrů	tygr	k1gMnPc2	tygr
takové	takový	k3xDgFnSc2	takový
"	"	kIx"	"
<g/>
šlechtění	šlechtění	k1gNnSc2	šlechtění
<g/>
"	"	kIx"	"
ovšem	ovšem	k9	ovšem
nijak	nijak	k6eAd1	nijak
nepřispívá	přispívat	k5eNaImIp3nS	přispívat
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgNnSc1d1	Černé
zbarvení	zbarvení	k1gNnSc1	zbarvení
nebylo	být	k5eNaImAgNnS	být
u	u	k7c2	u
živého	živý	k2eAgMnSc2d1	živý
tygra	tygr	k1gMnSc2	tygr
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
věrohodné	věrohodný	k2eAgFnPc1d1	věrohodná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
takto	takto	k6eAd1	takto
zbarvených	zbarvený	k2eAgFnPc6d1	zbarvená
tygřích	tygří	k2eAgFnPc6d1	tygří
kůžích	kůže	k1gFnPc6	kůže
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
barevná	barevný	k2eAgFnSc1d1	barevná
odchylka	odchylka	k1gFnSc1	odchylka
se	se	k3xPyFc4	se
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
zřejmě	zřejmě	k6eAd1	zřejmě
občas	občas	k6eAd1	občas
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
jak	jak	k6eAd1	jak
o	o	k7c4	o
klasický	klasický	k2eAgInSc4d1	klasický
melanismus	melanismus	k1gInSc4	melanismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
černé	černý	k2eAgNnSc1d1	černé
barvivo	barvivo	k1gNnSc1	barvivo
melanin	melanin	k1gInSc1	melanin
překryje	překrýt	k5eAaPmIp3nS	překrýt
původní	původní	k2eAgFnPc4d1	původní
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
o	o	k7c4	o
extrémní	extrémní	k2eAgNnSc4d1	extrémní
rozšíření	rozšíření	k1gNnSc4	rozšíření
černých	černý	k2eAgInPc2d1	černý
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
téměř	téměř	k6eAd1	téměř
nezbývá	zbývat	k5eNaImIp3nS	zbývat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
jiné	jiný	k2eAgNnSc4d1	jiné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
zkazky	zkazka	k1gFnPc1	zkazka
o	o	k7c6	o
"	"	kIx"	"
<g/>
modrých	modrý	k2eAgFnPc2d1	modrá
<g/>
"	"	kIx"	"
tygrech	tygr	k1gMnPc6	tygr
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
zbarvení	zbarvení	k1gNnPc2	zbarvení
ovšem	ovšem	k9	ovšem
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
doložen	doložit	k5eAaPmNgInS	doložit
věrohodnými	věrohodný	k2eAgInPc7d1	věrohodný
důkazy	důkaz	k1gInPc7	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
genom	genom	k1gInSc1	genom
tygra	tygr	k1gMnSc2	tygr
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
genomu	genom	k1gInSc2	genom
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
"	"	kIx"	"
<g/>
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
"	"	kIx"	"
barevné	barevný	k2eAgFnPc1d1	barevná
variety	varieta	k1gFnPc1	varieta
jako	jako	k8xC	jako
ruská	ruský	k2eAgFnSc1d1	ruská
modrá	modrý	k2eAgFnSc1d1	modrá
kočka	kočka	k1gFnSc1	kočka
či	či	k8xC	či
kartouzská	kartouzský	k2eAgFnSc1d1	Kartouzská
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
barevných	barevný	k2eAgFnPc2d1	barevná
odchylek	odchylka	k1gFnPc2	odchylka
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
pruhování	pruhování	k1gNnSc6	pruhování
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc4d1	znám
případy	případ	k1gInPc4	případ
tygrů	tygr	k1gMnPc2	tygr
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
řídkým	řídký	k2eAgMnSc7d1	řídký
či	či	k8xC	či
téměř	téměř	k6eAd1	téměř
chybějícím	chybějící	k2eAgNnSc7d1	chybějící
pruhováním	pruhování	k1gNnSc7	pruhování
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
poddruhy	poddruh	k1gInPc7	poddruh
jsou	být	k5eAaImIp3nP	být
tygři	tygr	k1gMnPc1	tygr
ussurijští	ussurijský	k2eAgMnPc1d1	ussurijský
a	a	k8xC	a
indičtí	indický	k2eAgMnPc1d1	indický
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
samci	samec	k1gMnPc1	samec
těchto	tento	k3xDgFnPc2	tento
subspecií	subspecie	k1gFnPc2	subspecie
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
těla	tělo	k1gNnSc2	tělo
přes	přes	k7c4	přes
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připočítat	připočítat	k5eAaPmF	připočítat
ocas	ocas	k1gInSc4	ocas
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
přes	přes	k7c4	přes
90	[number]	k4	90
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
tygra	tygr	k1gMnSc2	tygr
indického	indický	k2eAgInSc2d1	indický
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
přes	přes	k7c4	přes
250	[number]	k4	250
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
kolem	kolem	k7c2	kolem
260	[number]	k4	260
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
samice	samice	k1gFnSc2	samice
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgNnSc2d1	ussurijské
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
165	[number]	k4	165
až	až	k9	až
178	[number]	k4	178
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgInSc7d3	nejmenší
žijícím	žijící	k2eAgInSc7d1	žijící
poddruhem	poddruh	k1gInSc7	poddruh
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc2	jehož
samci	samec	k1gMnPc1	samec
měří	měřit	k5eAaImIp3nP	měřit
240	[number]	k4	240
až	až	k9	až
250	[number]	k4	250
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
připadá	připadat	k5eAaImIp3nS	připadat
155	[number]	k4	155
až	až	k9	až
170	[number]	k4	170
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
215	[number]	k4	215
až	až	k9	až
230	[number]	k4	230
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
připadá	připadat	k5eAaPmIp3nS	připadat
145	[number]	k4	145
až	až	k9	až
155	[number]	k4	155
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
vyhubený	vyhubený	k2eAgMnSc1d1	vyhubený
tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
jen	jen	k9	jen
220	[number]	k4	220
až	až	k9	až
225	[number]	k4	225
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
190	[number]	k4	190
až	až	k9	až
200	[number]	k4	200
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
zmiňující	zmiňující	k2eAgInPc1d1	zmiňující
tygry	tygr	k1gMnPc4	tygr
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
těla	tělo	k1gNnSc2	tělo
290	[number]	k4	290
cm	cm	kA	cm
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
skoro	skoro	k6eAd1	skoro
4	[number]	k4	4
m	m	kA	m
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
extrémní	extrémní	k2eAgInPc1d1	extrémní
údaje	údaj	k1gInPc1	údaj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
špatných	špatný	k2eAgInPc2d1	špatný
odhadů	odhad	k1gInPc2	odhad
či	či	k8xC	či
měření	měření	k1gNnSc2	měření
přes	přes	k7c4	přes
tělní	tělní	k2eAgFnPc4d1	tělní
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgFnSc1d1	tygří
kůže	kůže	k1gFnSc1	kůže
také	také	k9	také
disponuje	disponovat	k5eAaBmIp3nS	disponovat
značnou	značný	k2eAgFnSc7d1	značná
roztažností	roztažnost	k1gFnSc7	roztažnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rovněž	rovněž	k9	rovněž
mohlo	moct	k5eAaImAgNnS	moct
zkreslit	zkreslit	k5eAaPmF	zkreslit
výsledky	výsledek	k1gInPc4	výsledek
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
známý	známý	k2eAgMnSc1d1	známý
tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
byl	být	k5eAaImAgInS	být
samec	samec	k1gInSc1	samec
zastřelený	zastřelený	k2eAgInSc1d1	zastřelený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Ili	Ili	k1gFnSc2	Ili
<g/>
.	.	kIx.	.
</s>
<s>
Naměřená	naměřený	k2eAgFnSc1d1	naměřená
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
včetně	včetně	k7c2	včetně
ocasu	ocas	k1gInSc2	ocas
činila	činit	k5eAaImAgFnS	činit
295	[number]	k4	295
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
98	[number]	k4	98
cm	cm	kA	cm
připadalo	připadat	k5eAaImAgNnS	připadat
na	na	k7c4	na
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
samec	samec	k1gInSc1	samec
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
Sichote-Aliňské	Sichote-Aliňský	k2eAgFnSc2d1	Sichote-Aliňský
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c6	v
duisburské	duisburský	k2eAgFnSc6d1	Duisburská
zoo	zoo	k1gFnSc6	zoo
<g/>
,	,	kIx,	,
měřil	měřit	k5eAaImAgInS	měřit
319	[number]	k4	319
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
99	[number]	k4	99
cm	cm	kA	cm
připadalo	připadat	k5eAaPmAgNnS	připadat
na	na	k7c4	na
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
věrohodně	věrohodně	k6eAd1	věrohodně
změřený	změřený	k2eAgMnSc1d1	změřený
tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgInSc4d1	ussurijský
ulovený	ulovený	k2eAgInSc4d1	ulovený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
měřil	měřit	k5eAaImAgInS	měřit
350	[number]	k4	350
cm	cm	kA	cm
přes	přes	k7c4	přes
tělní	tělní	k2eAgFnPc4d1	tělní
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
reálné	reálný	k2eAgFnSc3d1	reálná
celkové	celkový	k2eAgFnSc3d1	celková
délce	délka	k1gFnSc3	délka
330	[number]	k4	330
až	až	k9	až
335	[number]	k4	335
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
samce	samec	k1gMnSc2	samec
tygra	tygr	k1gMnSc2	tygr
indického	indický	k2eAgMnSc2d1	indický
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
280	[number]	k4	280
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
věrohodně	věrohodně	k6eAd1	věrohodně
změřených	změřený	k2eAgInPc2d1	změřený
exemplářů	exemplář	k1gInPc2	exemplář
měřil	měřit	k5eAaImAgInS	měřit
322,5	[number]	k4	322,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgInSc2d1	ussurijský
měří	měřit	k5eAaImIp3nS	měřit
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
97	[number]	k4	97
až	až	k9	až
105	[number]	k4	105
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
a	a	k8xC	a
tygr	tygr	k1gMnSc1	tygr
indočínský	indočínský	k2eAgMnSc1d1	indočínský
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
90	[number]	k4	90
až	až	k9	až
100	[number]	k4	100
cm	cm	kA	cm
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
měří	měřit	k5eAaImIp3nS	měřit
pouze	pouze	k6eAd1	pouze
75	[number]	k4	75
až	až	k9	až
79	[number]	k4	79
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
tygra	tygr	k1gMnSc2	tygr
čínského	čínský	k2eAgMnSc4d1	čínský
82	[number]	k4	82
až	až	k9	až
86	[number]	k4	86
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
,	,	kIx,	,
indického	indický	k2eAgMnSc2d1	indický
a	a	k8xC	a
indočínského	indočínský	k2eAgMnSc2d1	indočínský
jsou	být	k5eAaImIp3nP	být
vysoké	vysoká	k1gFnPc1	vysoká
cca	cca	kA	cca
78	[number]	k4	78
až	až	k9	až
87	[number]	k4	87
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
pouze	pouze	k6eAd1	pouze
66	[number]	k4	66
až	až	k9	až
68	[number]	k4	68
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
váží	vážit	k5eAaImIp3nP	vážit
100	[number]	k4	100
až	až	k9	až
140	[number]	k4	140
kg	kg	kA	kg
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
75	[number]	k4	75
až	až	k9	až
110	[number]	k4	110
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tygra	tygr	k1gMnSc4	tygr
indického	indický	k2eAgMnSc4d1	indický
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
váží	vážit	k5eAaImIp3nP	vážit
200	[number]	k4	200
až	až	k9	až
240	[number]	k4	240
kg	kg	kA	kg
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
125	[number]	k4	125
až	až	k9	až
160	[number]	k4	160
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
věrohodně	věrohodně	k6eAd1	věrohodně
doložená	doložený	k2eAgFnSc1d1	doložená
hmotnost	hmotnost	k1gFnSc1	hmotnost
tygra	tygr	k1gMnSc2	tygr
indického	indický	k2eAgMnSc2d1	indický
činila	činit	k5eAaImAgFnS	činit
podle	podle	k7c2	podle
Mazáka	mazák	k1gMnSc2	mazák
258	[number]	k4	258
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
doložená	doložený	k2eAgFnSc1d1	doložená
hmotnost	hmotnost	k1gFnSc1	hmotnost
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
kaspického	kaspický	k2eAgInSc2d1	kaspický
činila	činit	k5eAaImAgFnS	činit
240	[number]	k4	240
kg	kg	kA	kg
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
již	již	k6eAd1	již
výše	vysoce	k6eAd2	vysoce
zmiňované	zmiňovaný	k2eAgNnSc1d1	zmiňované
zvíře	zvíře	k1gNnSc1	zvíře
zastřelené	zastřelený	k2eAgNnSc1d1	zastřelené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Ili	Ili	k1gFnSc2	Ili
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
vážil	vážit	k5eAaImAgMnS	vážit
306,5	[number]	k4	306,5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
doložená	doložený	k2eAgFnSc1d1	doložená
hmotnost	hmotnost	k1gFnSc1	hmotnost
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
samce	samec	k1gInSc2	samec
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Circa	Circa	k1gMnSc1	Circa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
mladé	mladý	k2eAgNnSc4d1	mladé
zvíře	zvíře	k1gNnSc4	zvíře
odchycen	odchycen	k2eAgMnSc1d1	odchycen
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ussuri	Ussur	k1gFnSc2	Ussur
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
menažerii	menažerie	k1gFnSc6	menažerie
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
tygrech	tygr	k1gMnPc6	tygr
ussurijských	ussurijský	k2eAgInPc2d1	ussurijský
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
výrazně	výrazně	k6eAd1	výrazně
přes	přes	k7c4	přes
300	[number]	k4	300
kg	kg	kA	kg
jsou	být	k5eAaImIp3nP	být
neověřitelné	ověřitelný	k2eNgFnPc1d1	neověřitelná
<g/>
.	.	kIx.	.
</s>
<s>
Mazák	mazák	k1gInSc1	mazák
uvádí	uvádět	k5eAaImIp3nS	uvádět
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgInSc2d1	ussurijský
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hmotnost	hmotnost	k1gFnSc4	hmotnost
230	[number]	k4	230
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Vyhynulý	vyhynulý	k2eAgMnSc1d1	vyhynulý
tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
vážil	vážit	k5eAaImAgInS	vážit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pouze	pouze	k6eAd1	pouze
90	[number]	k4	90
až	až	k9	až
100	[number]	k4	100
kg	kg	kA	kg
(	(	kIx(	(
<g/>
samci	samec	k1gMnSc3	samec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
65	[number]	k4	65
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
jávský	jávský	k2eAgMnSc1d1	jávský
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
100	[number]	k4	100
až	až	k9	až
141	[number]	k4	141
kg	kg	kA	kg
(	(	kIx(	(
<g/>
samci	samec	k1gMnSc3	samec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
kolem	kolem	k7c2	kolem
100	[number]	k4	100
kg	kg	kA	kg
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Lví	lví	k2eAgMnPc1d1	lví
samci	samec	k1gMnPc1	samec
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
260	[number]	k4	260
až	až	k9	až
270	[number]	k4	270
cm	cm	kA	cm
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
285	[number]	k4	285
cm	cm	kA	cm
<g/>
,	,	kIx,	,
za	za	k7c7	za
největšími	veliký	k2eAgInPc7d3	veliký
tygřími	tygří	k2eAgInPc7d1	tygří
poddruhy	poddruh	k1gInPc7	poddruh
zaostávají	zaostávat	k5eAaImIp3nP	zaostávat
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
právoplatně	právoplatně	k6eAd1	právoplatně
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
kočkovitou	kočkovitý	k2eAgFnSc4d1	kočkovitá
šelmu	šelma	k1gFnSc4	šelma
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
věrohodně	věrohodně	k6eAd1	věrohodně
naměřená	naměřený	k2eAgFnSc1d1	naměřená
délka	délka	k1gFnSc1	délka
u	u	k7c2	u
lva	lev	k1gMnSc2	lev
činila	činit	k5eAaImAgFnS	činit
mezi	mezi	k7c7	mezi
305	[number]	k4	305
a	a	k8xC	a
310	[number]	k4	310
cm	cm	kA	cm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
rovněž	rovněž	k9	rovněž
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
za	za	k7c7	za
maximálními	maximální	k2eAgInPc7d1	maximální
údaji	údaj	k1gInPc7	údaj
uváděnými	uváděný	k2eAgInPc7d1	uváděný
pro	pro	k7c4	pro
tygra	tygr	k1gMnSc4	tygr
ussurijského	ussurijský	k2eAgMnSc4d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnSc2d1	ostatní
velké	velký	k2eAgFnSc2d1	velká
kočky	kočka	k1gFnSc2	kočka
má	mít	k5eAaImIp3nS	mít
tygr	tygr	k1gMnSc1	tygr
kulatou	kulatý	k2eAgFnSc4d1	kulatá
zornici	zornice	k1gFnSc4	zornice
<g/>
.	.	kIx.	.
</s>
<s>
Duhovka	duhovka	k1gFnSc1	duhovka
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgFnSc1d1	masivní
lebka	lebka	k1gFnSc1	lebka
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
velká	velký	k2eAgFnSc1d1	velká
jako	jako	k8xS	jako
lví	lví	k2eAgFnSc1d1	lví
a	a	k8xC	a
od	od	k7c2	od
lví	lví	k2eAgFnSc2d1	lví
lebky	lebka	k1gFnSc2	lebka
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgInPc1d1	drobný
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
nosní	nosní	k2eAgFnSc2d1	nosní
dutiny	dutina	k1gFnSc2	dutina
a	a	k8xC	a
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
dolní	dolní	k2eAgFnSc2d1	dolní
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
spíše	spíše	k9	spíše
konkávní	konkávní	k2eAgNnSc1d1	konkávní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
lva	lev	k1gMnSc2	lev
konvexní	konvexní	k2eAgFnSc1d1	konvexní
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velkých	velký	k2eAgMnPc2d1	velký
tygřích	tygří	k2eAgMnPc2d1	tygří
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
lebky	lebka	k1gFnSc2	lebka
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
350	[number]	k4	350
a	a	k8xC	a
360	[number]	k4	360
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgFnSc1d1	samičí
lebka	lebka	k1gFnSc1	lebka
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
290	[number]	k4	290
až	až	k9	až
310	[number]	k4	310
mm	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Lebka	lebka	k1gFnSc1	lebka
tygra	tygr	k1gMnSc2	tygr
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
ovšem	ovšem	k9	ovšem
měří	měřit	k5eAaImIp3nS	měřit
pouze	pouze	k6eAd1	pouze
295	[number]	k4	295
až	až	k9	až
340	[number]	k4	340
mm	mm	kA	mm
(	(	kIx(	(
<g/>
samci	samec	k1gMnSc3	samec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
263	[number]	k4	263
až	až	k9	až
293	[number]	k4	293
mm	mm	kA	mm
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tygra	tygr	k1gMnSc2	tygr
balijského	balijský	k2eAgInSc2d1	balijský
jsou	být	k5eAaImIp3nP	být
udávány	udáván	k2eAgInPc4d1	udáván
rozměry	rozměr	k1gInPc4	rozměr
cca	cca	kA	cca
295	[number]	k4	295
mm	mm	kA	mm
(	(	kIx(	(
<g/>
samci	samec	k1gMnSc3	samec
<g/>
)	)	kIx)	)
a	a	k8xC	a
265	[number]	k4	265
mm	mm	kA	mm
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
tygra	tygr	k1gMnSc2	tygr
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc1	objem
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
až	až	k9	až
300	[number]	k4	300
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Čelisti	čelist	k1gFnPc1	čelist
nesou	nést	k5eAaImIp3nP	nést
30	[number]	k4	30
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
složení	složení	k1gNnSc1	složení
chrupu	chrup	k1gInSc2	chrup
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
3	[number]	k4	3
⋅	⋅	k?	⋅
C	C	kA	C
1	[number]	k4	1
⋅	⋅	k?	⋅
P	P	kA	P
3	[number]	k4	3
⋅	⋅	k?	⋅
M	M	kA	M
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
3	[number]	k4	3
⋅	⋅	k?	⋅
C	C	kA	C
1	[number]	k4	1
⋅	⋅	k?	⋅
P	P	kA	P
2	[number]	k4	2
⋅	⋅	k?	⋅
M	M	kA	M
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
P	P	kA	P
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
M1	M1	k1gMnSc1	M1
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
I	i	k9	i
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Horní	horní	k2eAgFnSc1d1	horní
stolička	stolička	k1gFnSc1	stolička
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
úplně	úplně	k6eAd1	úplně
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
horní	horní	k2eAgInSc4d1	horní
premolár	premolár	k1gInSc4	premolár
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnější	nápadní	k2eAgMnSc1d3	nápadní
jsou	být	k5eAaImIp3nP	být
špičáky	špičák	k1gInPc1	špičák
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
mohou	moct	k5eAaImIp3nP	moct
vyčnívat	vyčnívat	k5eAaImF	vyčnívat
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
70	[number]	k4	70
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInPc1d1	spodní
špičáky	špičák	k1gInPc1	špičák
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
trháky	trhák	k1gInPc1	trhák
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k6eAd1	především
třetí	třetí	k4xOgInSc4	třetí
premolár	premolár	k1gInSc4	premolár
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
a	a	k8xC	a
stolička	stolička	k1gFnSc1	stolička
v	v	k7c6	v
čelisti	čelist	k1gFnSc6	čelist
dolní	dolní	k2eAgFnSc6d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
trhák	trhák	k1gInSc1	trhák
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
špičáků	špičák	k1gMnPc2	špičák
vůbec	vůbec	k9	vůbec
nejmohutnější	mohutný	k2eAgInSc1d3	nejmohutnější
zub	zub	k1gInSc1	zub
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
tygrů	tygr	k1gMnPc2	tygr
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
34	[number]	k4	34
až	až	k9	až
38	[number]	k4	38
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
tygra	tygr	k1gMnSc2	tygr
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
kočičí	kočičí	k2eAgNnSc1d1	kočičí
a	a	k8xC	a
například	například	k6eAd1	například
od	od	k7c2	od
kostry	kostra	k1gFnSc2	kostra
lva	lev	k1gMnSc2	lev
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
humeru	humer	k1gInSc6	humer
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
drobné	drobný	k2eAgInPc1d1	drobný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
má	mít	k5eAaImIp3nS	mít
55	[number]	k4	55
až	až	k9	až
56	[number]	k4	56
obratlů	obratel	k1gInPc2	obratel
(	(	kIx(	(
<g/>
7	[number]	k4	7
krčních	krční	k2eAgMnPc2d1	krční
<g/>
,	,	kIx,	,
13	[number]	k4	13
hrudních	hrudní	k2eAgMnPc2d1	hrudní
<g/>
,	,	kIx,	,
7	[number]	k4	7
bederních	bederní	k2eAgMnPc2d1	bederní
<g/>
,	,	kIx,	,
3	[number]	k4	3
křížové	křížový	k2eAgInPc1d1	křížový
a	a	k8xC	a
25	[number]	k4	25
až	až	k9	až
26	[number]	k4	26
ocasních	ocasní	k2eAgFnPc2d1	ocasní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
tvoří	tvořit	k5eAaImIp3nS	tvořit
13	[number]	k4	13
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
noze	noha	k1gFnSc6	noha
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
na	na	k7c4	na
zadní	zadní	k2eAgNnPc4d1	zadní
čtyři	čtyři	k4xCgNnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Drápy	dráp	k1gInPc1	dráp
jsou	být	k5eAaImIp3nP	být
zatahovatelné	zatahovatelný	k2eAgInPc1d1	zatahovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgNnSc1d1	tygří
srdce	srdce	k1gNnSc1	srdce
váží	vážit	k5eAaImIp3nS	vážit
600	[number]	k4	600
až	až	k9	až
1100	[number]	k4	1100
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
měří	měřit	k5eAaImIp3nP	měřit
cca	cca	kA	cca
7	[number]	k4	7
m.	m.	k?	m.
Tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
si	se	k3xPyFc3	se
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vrstvu	vrstva	k1gFnSc4	vrstva
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
kočky	kočka	k1gFnPc1	kočka
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
38	[number]	k4	38
chromozomů	chromozom	k1gInPc2	chromozom
-	-	kIx~	-
18	[number]	k4	18
autozomálních	autozomální	k2eAgInPc2d1	autozomální
párů	pár	k1gInPc2	pár
a	a	k8xC	a
dvojici	dvojice	k1gFnSc4	dvojice
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Krok	krok	k1gInSc1	krok
tygra	tygr	k1gMnSc2	tygr
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
kolem	kolem	k7c2	kolem
70	[number]	k4	70
cm	cm	kA	cm
u	u	k7c2	u
samců	samec	k1gInPc2	samec
a	a	k8xC	a
kolem	kolem	k7c2	kolem
60	[number]	k4	60
cm	cm	kA	cm
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
stopy	stopa	k1gFnSc2	stopa
silně	silně	k6eAd1	silně
odvisí	odviset	k5eAaImIp3nS	odviset
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
zanechána	zanechán	k2eAgFnSc1d1	zanechána
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
tygří	tygří	k2eAgMnPc1d1	tygří
samci	samec	k1gMnPc1	samec
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
v	v	k7c6	v
mokré	mokrý	k2eAgFnSc6d1	mokrá
hlíně	hlína	k1gFnSc6	hlína
stopu	stop	k1gInSc2	stop
14	[number]	k4	14
až	až	k9	až
17	[number]	k4	17
cm	cm	kA	cm
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
13	[number]	k4	13
až	až	k9	až
16	[number]	k4	16
cm	cm	kA	cm
širokou	široký	k2eAgFnSc4d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Stopa	stopa	k1gFnSc1	stopa
samice	samice	k1gFnSc2	samice
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
až	až	k9	až
14	[number]	k4	14
cm	cm	kA	cm
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
11	[number]	k4	11
až	až	k9	až
13	[number]	k4	13
cm	cm	kA	cm
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
čerstvě	čerstvě	k6eAd1	čerstvě
napadaném	napadaný	k2eAgNnSc6d1	napadané
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tygří	tygří	k2eAgFnSc1d1	tygří
stopa	stopa	k1gFnSc1	stopa
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
tygra	tygr	k1gMnSc2	tygr
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
na	na	k7c4	na
východ	východ	k1gInSc4	východ
přes	přes	k7c4	přes
Indočínu	Indočína	k1gFnSc4	Indočína
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Čínu	Čína	k1gFnSc4	Čína
přes	přes	k7c4	přes
Amur	Amur	k1gInSc4	Amur
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tygři	tygr	k1gMnPc1	tygr
dnes	dnes	k6eAd1	dnes
ještě	ještě	k6eAd1	ještě
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
byl	být	k5eAaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
Bali	Bal	k1gFnSc6	Bal
ještě	ještě	k9	ještě
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
byl	být	k5eAaImAgInS	být
fosilními	fosilní	k2eAgFnPc7d1	fosilní
nálezy	nález	k1gInPc4	nález
prokázán	prokázán	k2eAgInSc1d1	prokázán
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
holocénu	holocén	k1gInSc2	holocén
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
hojně	hojně	k6eAd1	hojně
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
i	i	k9	i
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Střední	střední	k2eAgFnSc3d1	střední
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vyhubeného	vyhubený	k2eAgMnSc4d1	vyhubený
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
urazit	urazit	k5eAaPmF	urazit
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
trvalého	trvalý	k2eAgInSc2d1	trvalý
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nějaký	nějaký	k3yIgMnSc1	nějaký
tygr	tygr	k1gMnSc1	tygr
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
zabloudí	zabloudit	k5eAaPmIp3nS	zabloudit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
člověk	člověk	k1gMnSc1	člověk
tygra	tygr	k1gMnSc2	tygr
vyhubil	vyhubit	k5eAaPmAgInS	vyhubit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
putující	putující	k2eAgMnSc1d1	putující
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
běžně	běžně	k6eAd1	běžně
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
v	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
a	a	k8xC	a
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Malého	Malého	k2eAgInSc2d1	Malého
a	a	k8xC	a
Velkého	velký	k2eAgInSc2d1	velký
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dokonce	dokonce	k9	dokonce
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tygrů	tygr	k1gMnPc2	tygr
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc4	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
tygra	tygr	k1gMnSc4	tygr
mohl	moct	k5eAaImAgInS	moct
zasahovat	zasahovat	k5eAaImF	zasahovat
až	až	k9	až
k	k	k7c3	k
Donu	Don	k1gInSc3	Don
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ruských	ruský	k2eAgFnPc2d1	ruská
zkazek	zkazka	k1gFnPc2	zkazka
známé	známá	k1gFnSc2	známá
"	"	kIx"	"
<g/>
líté	lítý	k2eAgNnSc1d1	líté
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
л	л	k?	л
<g/>
́	́	k?	́
<g/>
т	т	k?	т
з	з	k?	з
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
právě	právě	k9	právě
tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
teorií	teorie	k1gFnPc2	teorie
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
také	také	k9	také
o	o	k7c4	o
lva	lev	k1gMnSc4	lev
či	či	k8xC	či
levharta	levhart	k1gMnSc4	levhart
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
se	se	k3xPyFc4	se
však	však	k9	však
tygr	tygr	k1gMnSc1	tygr
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
předhůří	předhůří	k1gNnSc6	předhůří
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
ještě	ještě	k9	ještě
mezi	mezi	k7c7	mezi
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
tygři	tygr	k1gMnPc1	tygr
putovali	putovat	k5eAaImAgMnP	putovat
až	až	k9	až
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
pozorováni	pozorovat	k5eAaImNgMnP	pozorovat
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Tbilisi	Tbilisi	k1gNnSc2	Tbilisi
<g/>
,	,	kIx,	,
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
horní	horní	k2eAgFnSc2d1	horní
Kury	kura	k1gFnSc2	kura
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
Rioni	Rioeň	k1gFnSc6	Rioeň
a	a	k8xC	a
Kivirili	Kivirili	k1gFnSc6	Kivirili
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
tygři	tygr	k1gMnPc1	tygr
putovali	putovat	k5eAaImAgMnP	putovat
až	až	k9	až
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Baku	Baku	k1gNnSc2	Baku
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
Derbentu	Derbenta	k1gFnSc4	Derbenta
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
tygři	tygr	k1gMnPc1	tygr
obývali	obývat	k5eAaImAgMnP	obývat
jihovýchodní	jihovýchodní	k2eAgNnSc4d1	jihovýchodní
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
Zakavkazsko	Zakavkazsko	k1gNnSc4	Zakavkazsko
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
oblast	oblast	k1gFnSc1	oblast
Talyšských	Talyšský	k2eAgFnPc2d1	Talyšský
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
Lenkoranu	Lenkoran	k1gInSc2	Lenkoran
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
táhla	táhnout	k5eAaImAgNnP	táhnout
přes	přes	k7c4	přes
severní	severní	k2eAgInSc4d1	severní
Írán	Írán	k1gInSc4	Írán
podél	podél	k7c2	podél
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
pohoří	pohoří	k1gNnSc2	pohoří
Alborz	Alborza	k1gFnPc2	Alborza
na	na	k7c4	na
východ	východ	k1gInSc4	východ
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Atrek	Atrky	k1gFnPc2	Atrky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Íránu	Írán	k1gInSc2	Írán
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
tygr	tygr	k1gMnSc1	tygr
nikdy	nikdy	k6eAd1	nikdy
nevyskytoval	vyskytovat	k5eNaImAgMnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Atrek	Atrek	k6eAd1	Atrek
vstupoval	vstupovat	k5eAaImAgInS	vstupovat
areál	areál	k1gInSc1	areál
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
oblast	oblast	k1gFnSc4	oblast
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnPc4d1	západní
úbočí	úbočí	k1gNnPc4	úbočí
pohoří	pohoří	k1gNnSc2	pohoří
Kopet	Kopeta	k1gFnPc2	Kopeta
Dag	dag	kA	dag
bylo	být	k5eAaImAgNnS	být
tygry	tygr	k1gMnPc4	tygr
často	často	k6eAd1	často
navštěvováno	navštěvován	k2eAgNnSc1d1	navštěvováno
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
trvale	trvale	k6eAd1	trvale
nežili	žít	k5eNaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
úbočí	úbočí	k1gNnSc1	úbočí
neskýtalo	skýtat	k5eNaImAgNnS	skýtat
tygrům	tygr	k1gMnPc3	tygr
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řeka	k1gFnPc2	řeka
Tedžen	Tedžna	k1gFnPc2	Tedžna
a	a	k8xC	a
Murgháb	Murgháb	k1gInSc1	Murgháb
areál	areál	k1gInSc4	areál
rovněž	rovněž	k9	rovněž
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
do	do	k7c2	do
jižního	jižní	k2eAgInSc2d1	jižní
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
stykům	styk	k1gInPc3	styk
s	s	k7c7	s
íránskou	íránský	k2eAgFnSc7d1	íránská
a	a	k8xC	a
afgánskou	afgánský	k2eAgFnSc7d1	afgánská
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
byl	být	k5eAaImAgInS	být
areál	areál	k1gInSc1	areál
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
štíty	štít	k1gInPc4	štít
Pamíru	Pamír	k1gInSc2	Pamír
a	a	k8xC	a
Hindúkuše	Hindúkuš	k1gInSc2	Hindúkuš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalých	bývalý	k2eAgFnPc6d1	bývalá
sovětských	sovětský	k2eAgFnPc6d1	sovětská
republikách	republika	k1gFnPc6	republika
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
kavkazských	kavkazský	k2eAgFnPc2d1	kavkazská
a	a	k8xC	a
jihoturkménských	jihoturkménský	k2eAgFnPc2d1	jihoturkménský
populací	populace	k1gFnPc2	populace
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
především	především	k6eAd1	především
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řek	k1gMnSc1	řek
Vachš	Vachš	k1gMnSc1	Vachš
<g/>
,	,	kIx,	,
Amudarja	Amudarja	k1gMnSc1	Amudarja
<g/>
,	,	kIx,	,
Syrdarja	Syrdarja	k1gFnSc1	Syrdarja
a	a	k8xC	a
Ili	Ili	k1gFnSc1	Ili
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řek	k1gMnSc1	řek
Vachš	Vachš	k1gMnSc1	Vachš
a	a	k8xC	a
Amudarja	Amudarja	k1gFnSc1	Amudarja
byla	být	k5eAaImAgFnS	být
propojená	propojený	k2eAgFnSc1d1	propojená
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Ili	Ili	k1gFnSc2	Ili
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezer	jezero	k1gNnPc2	jezero
Balchaš	Balchaš	k1gInSc4	Balchaš
a	a	k8xC	a
Alaköl	Alaköl	k1gInSc4	Alaköl
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
až	až	k9	až
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Bagraš	Bagraš	k1gMnSc1	Bagraš
köl	köl	k?	köl
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Syrdarja	Syrdarja	k1gFnSc1	Syrdarja
byli	být	k5eAaImAgMnP	být
pouštními	pouštní	k2eAgFnPc7d1	pouštní
oblastmi	oblast	k1gFnPc7	oblast
izolováni	izolovat	k5eAaBmNgMnP	izolovat
od	od	k7c2	od
obou	dva	k4xCgMnPc2	dva
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
areálů	areál	k1gInPc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
osamělí	osamělý	k2eAgMnPc1d1	osamělý
jedinci	jedinec	k1gMnPc1	jedinec
opakovaně	opakovaně	k6eAd1	opakovaně
přecházeli	přecházet	k5eAaImAgMnP	přecházet
skrz	skrz	k7c4	skrz
tato	tento	k3xDgNnPc4	tento
nehostinná	hostinný	k2eNgNnPc4d1	nehostinné
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stále	stále	k6eAd1	stále
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mísení	mísení	k1gNnSc3	mísení
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
populacemi	populace	k1gFnPc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
trvale	trvale	k6eAd1	trvale
tygry	tygr	k1gMnPc4	tygr
obývané	obývaný	k2eAgFnSc2d1	obývaná
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
k	k	k7c3	k
jižním	jižní	k2eAgInPc3d1	jižní
svahům	svah	k1gInPc3	svah
Altaje	Altaj	k1gInSc2	Altaj
<g/>
,	,	kIx,	,
tygři	tygr	k1gMnPc1	tygr
se	se	k3xPyFc4	se
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zajsanu	Zajsan	k1gInSc2	Zajsan
<g/>
,	,	kIx,	,
u	u	k7c2	u
Černého	Černý	k1gMnSc2	Černý
Irtyše	Irtyš	k1gMnSc2	Irtyš
a	a	k8xC	a
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Kurčum	Kurčum	k1gInSc1	Kurčum
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
jedinci	jedinec	k1gMnPc1	jedinec
pronikali	pronikat	k5eAaImAgMnP	pronikat
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
až	až	k9	až
k	k	k7c3	k
Akmolinsku	Akmolinsko	k1gNnSc3	Akmolinsko
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Astana	Astana	k1gFnSc1	Astana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barnaulu	Barnaula	k1gFnSc4	Barnaula
a	a	k8xC	a
Bijsku	Bijska	k1gFnSc4	Bijska
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tygrů	tygr	k1gMnPc2	tygr
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
až	až	k9	až
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Ačit	Ačit	k1gMnSc1	Ačit
núr	núr	k?	núr
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
východně	východně	k6eAd1	východně
od	od	k7c2	od
Altaje	Altaj	k1gInSc2	Altaj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
minimum	minimum	k1gNnSc1	minimum
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
či	či	k8xC	či
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgFnPc1d1	nedávná
genetické	genetický	k2eAgFnPc1d1	genetická
analýzy	analýza	k1gFnPc1	analýza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
blízkou	blízký	k2eAgFnSc4d1	blízká
příbuznost	příbuznost	k1gFnSc4	příbuznost
východosibiřských	východosibiřský	k2eAgMnPc2d1	východosibiřský
tygrů	tygr	k1gMnPc2	tygr
(	(	kIx(	(
<g/>
tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
<g/>
)	)	kIx)	)
s	s	k7c7	s
tygry	tygr	k1gMnPc7	tygr
středoasijskými	středoasijský	k2eAgInPc7d1	středoasijský
a	a	k8xC	a
kavkazskými	kavkazský	k2eAgInPc7d1	kavkazský
(	(	kIx(	(
<g/>
tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
areál	areál	k1gInSc1	areál
rozšení	rozšení	k1gNnSc2	rozšení
tygra	tygr	k1gMnSc2	tygr
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
kontinuálně	kontinuálně	k6eAd1	kontinuálně
táhl	táhnout	k5eAaImAgMnS	táhnout
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
k	k	k7c3	k
tichomořskému	tichomořský	k2eAgNnSc3d1	Tichomořské
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
"	"	kIx"	"
<g/>
prázdných	prázdný	k2eAgFnPc6d1	prázdná
<g/>
"	"	kIx"	"
oblastech	oblast	k1gFnPc6	oblast
ale	ale	k8xC	ale
přeci	přeci	k?	přeci
jen	jen	k9	jen
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
byl	být	k5eAaImAgMnS	být
pozorován	pozorován	k2eAgMnSc1d1	pozorován
tygr	tygr	k1gMnSc1	tygr
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Angary	Angara	k1gFnSc2	Angara
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
pak	pak	k6eAd1	pak
u	u	k7c2	u
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řek	řeka	k1gFnPc2	řeka
Onon	Onono	k1gNnPc2	Onono
a	a	k8xC	a
Arguň	Arguň	k1gFnSc1	Arguň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
putující	putující	k2eAgMnPc1d1	putující
jedinci	jedinec	k1gMnPc1	jedinec
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
pravidelně	pravidelně	k6eAd1	pravidelně
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
začíná	začínat	k5eAaImIp3nS	začínat
areál	areál	k1gInSc1	areál
trvalého	trvalý	k2eAgInSc2d1	trvalý
výskytu	výskyt	k1gInSc2	výskyt
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
táhnoucí	táhnoucí	k2eAgNnSc4d1	táhnoucí
se	se	k3xPyFc4	se
povodím	povodit	k5eAaPmIp1nS	povodit
Amuru	Amur	k1gInSc2	Amur
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
kdysi	kdysi	k6eAd1	kdysi
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
až	až	k9	až
k	k	k7c3	k
úpatí	úpatí	k1gNnSc3	úpatí
Stanového	stanový	k2eAgNnSc2d1	stanové
pohoří	pohoří	k1gNnSc2	pohoří
na	na	k7c4	na
45	[number]	k4	45
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tichomořského	tichomořský	k2eAgNnSc2d1	Tichomořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
areál	areál	k1gInSc1	areál
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
dokonce	dokonce	k9	dokonce
až	až	k6eAd1	až
k	k	k7c3	k
50	[number]	k4	50
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Osamělí	osamělý	k2eAgMnPc1d1	osamělý
jedinci	jedinec	k1gMnPc1	jedinec
pronikali	pronikat	k5eAaImAgMnP	pronikat
i	i	k9	i
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
tygr	tygr	k1gMnSc1	tygr
dokonce	dokonce	k9	dokonce
pozorován	pozorovat	k5eAaImNgInS	pozorovat
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Aldan	Aldany	k1gInPc2	Aldany
na	na	k7c4	na
60	[number]	k4	60
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
jedinec	jedinec	k1gMnSc1	jedinec
byl	být	k5eAaImAgMnS	být
viděn	vidět	k5eAaImNgMnS	vidět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
na	na	k7c4	na
56	[number]	k4	56
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Amuru	Amur	k1gInSc2	Amur
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
areál	areál	k1gInSc1	areál
k	k	k7c3	k
západním	západní	k2eAgInPc3d1	západní
svahům	svah	k1gInPc3	svah
pohoří	pohoří	k1gNnSc2	pohoří
Velký	velký	k2eAgInSc4d1	velký
Chingan	Chingan	k1gInSc4	Chingan
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
dokonce	dokonce	k9	dokonce
až	až	k9	až
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Bujr	Bujra	k1gFnPc2	Bujra
núr	núr	k?	núr
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
táhl	táhnout	k5eAaImAgInS	táhnout
přes	přes	k7c4	přes
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
pohoří	pohoří	k1gNnSc4	pohoří
Velký	velký	k2eAgMnSc1d1	velký
Chingan	Chingan	k1gMnSc1	Chingan
a	a	k8xC	a
planinu	planina	k1gFnSc4	planina
Sungari	Sungari	k1gNnSc2	Sungari
až	až	k9	až
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
tygra	tygr	k1gMnSc2	tygr
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
táhl	táhnout	k5eAaImAgMnS	táhnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přes	přes	k7c4	přes
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
východní	východní	k2eAgFnSc2d1	východní
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Indočíny	Indočína	k1gFnSc2	Indočína
až	až	k9	až
na	na	k7c4	na
indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Nejzazší	zadní	k2eAgInSc1d3	nejzazší
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
horního	horní	k2eAgInSc2d1	horní
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Min-ťiang	Min-ťianga	k1gFnPc2	Min-ťianga
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuan	k1gMnSc1	-čchuan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
byl	být	k5eAaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Zadní	zadní	k2eAgFnSc6d1	zadní
Indii	Indie	k1gFnSc6	Indie
včetně	včetně	k7c2	včetně
Malajského	malajský	k2eAgInSc2d1	malajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
tygra	tygr	k1gMnSc2	tygr
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
také	také	k9	také
indonéské	indonéský	k2eAgInPc4d1	indonéský
ostrovy	ostrov	k1gInPc4	ostrov
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Jáva	Jáva	k1gFnSc1	Jáva
a	a	k8xC	a
Bali	Bali	k1gNnSc1	Bali
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
obýval	obývat	k5eAaImAgMnS	obývat
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc4d1	celý
indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
až	až	k9	až
ke	k	k7c3	k
svahům	svah	k1gInPc3	svah
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
vyprahlá	vyprahlý	k2eAgFnSc1d1	vyprahlá
Thárská	Thárský	k2eAgFnSc1d1	Thárská
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Indu	Indus	k1gInSc2	Indus
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zřejmě	zřejmě	k6eAd1	zřejmě
pronikl	proniknout	k5eAaPmAgInS	proniknout
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
západoasijských	západoasijský	k2eAgFnPc2d1	západoasijský
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
byly	být	k5eAaImAgFnP	být
populace	populace	k1gFnPc1	populace
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
Indu	Indus	k1gInSc2	Indus
odděleny	oddělen	k2eAgInPc1d1	oddělen
nepřekročitelnou	překročitelný	k2eNgFnSc7d1	nepřekročitelná
hrází	hráz	k1gFnSc7	hráz
vyprahlých	vyprahlý	k2eAgFnPc2d1	vyprahlá
pouští	poušť	k1gFnPc2	poušť
a	a	k8xC	a
vysokých	vysoká	k1gFnPc2	vysoká
hor.	hor.	k?	hor.
S	s	k7c7	s
postupujícím	postupující	k2eAgNnSc7d1	postupující
osidlováním	osidlování	k1gNnSc7	osidlování
odlehlých	odlehlý	k2eAgNnPc2d1	odlehlé
území	území	k1gNnPc2	území
člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
intenzivnějším	intenzivní	k2eAgInSc7d2	intenzivnější
i	i	k9	i
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
decimoval	decimovat	k5eAaBmAgInS	decimovat
stavy	stav	k1gInPc4	stav
tygrů	tygr	k1gMnPc2	tygr
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
přirozené	přirozený	k2eAgFnPc4d1	přirozená
kořisti	kořist	k1gFnPc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
tygra	tygr	k1gMnSc2	tygr
drasticky	drasticky	k6eAd1	drasticky
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
byl	být	k5eAaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
poslední	poslední	k2eAgInSc1d1	poslední
exemplář	exemplář	k1gInSc1	exemplář
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sousední	sousední	k2eAgFnSc6d1	sousední
Jávě	Jáva	k1gFnSc6	Jáva
poslední	poslední	k2eAgMnSc1d1	poslední
tygr	tygr	k1gMnSc1	tygr
zahynul	zahynout	k5eAaPmAgMnS	zahynout
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
a	a	k8xC	a
v	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
byli	být	k5eAaImAgMnP	být
tygři	tygr	k1gMnPc1	tygr
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
hojní	hojnit	k5eAaImIp3nS	hojnit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
stavy	stav	k1gInPc4	stav
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
začaly	začít	k5eAaPmAgFnP	začít
prudce	prudko	k6eAd1	prudko
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odtud	odtud	k6eAd1	odtud
tygři	tygr	k1gMnPc1	tygr
zcela	zcela	k6eAd1	zcela
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
exempláře	exemplář	k1gInPc1	exemplář
ještě	ještě	k9	ještě
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přecházely	přecházet	k5eAaImAgFnP	přecházet
z	z	k7c2	z
Íránu	Írán	k1gInSc2	Írán
přes	přes	k7c4	přes
Talyšské	Talyšský	k2eAgFnPc4d1	Talyšský
hory	hora	k1gFnPc4	hora
do	do	k7c2	do
podhůří	podhůří	k1gNnSc2	podhůří
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
bývalých	bývalý	k2eAgFnPc2d1	bývalá
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
tygr	tygr	k1gMnSc1	tygr
vymizel	vymizet	k5eAaPmAgMnS	vymizet
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
ještě	ještě	k9	ještě
žila	žít	k5eAaImAgFnS	žít
menší	malý	k2eAgFnSc1d2	menší
populace	populace	k1gFnSc1	populace
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Ili	Ili	k1gFnSc2	Ili
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
tygr	tygr	k1gMnSc1	tygr
spatřen	spatřen	k2eAgMnSc1d1	spatřen
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
u	u	k7c2	u
Ili	Ili	k1gFnSc2	Ili
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
udrželi	udržet	k5eAaPmAgMnP	udržet
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
původní	původní	k2eAgFnSc2d1	původní
oblasti	oblast	k1gFnSc2	oblast
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Amudarji	Amudarje	k1gFnSc4	Amudarje
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Vachšem	Vachš	k1gInSc7	Vachš
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
rezervace	rezervace	k1gFnSc2	rezervace
Tygrovaja	Tygrovaja	k1gMnSc1	Tygrovaja
balka	balka	k1gMnSc1	balka
(	(	kIx(	(
<g/>
Т	Т	k?	Т
б	б	k?	б
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
údolí	údolí	k1gNnSc6	údolí
Pandže	Pandž	k1gFnSc2	Pandž
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
možno	možno	k6eAd1	možno
běžně	běžně	k6eAd1	běžně
pozorovat	pozorovat	k5eAaImF	pozorovat
dospělé	dospělý	k2eAgMnPc4d1	dospělý
tygry	tygr	k1gMnPc4	tygr
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
žilo	žít	k5eAaImAgNnS	žít
už	už	k6eAd1	už
jen	jen	k9	jen
několik	několik	k4yIc1	několik
osamělých	osamělý	k2eAgMnPc2d1	osamělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
tygra	tygr	k1gMnSc4	tygr
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
za	za	k7c4	za
vyhubeného	vyhubený	k2eAgMnSc4d1	vyhubený
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
udržel	udržet	k5eAaPmAgMnS	udržet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
vyhubeného	vyhubený	k2eAgMnSc4d1	vyhubený
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
i	i	k8xC	i
Střední	střední	k2eAgFnSc3d1	střední
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
tygr	tygr	k1gMnSc1	tygr
kaspický	kaspický	k2eAgMnSc1d1	kaspický
tedy	tedy	k9	tedy
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byl	být	k5eAaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
souvislý	souvislý	k2eAgInSc1d1	souvislý
areál	areál	k1gInSc1	areál
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
do	do	k7c2	do
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
se	s	k7c7	s
zbytkovými	zbytkový	k2eAgFnPc7d1	zbytková
populacemi	populace	k1gFnPc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
tygra	tygr	k1gMnSc2	tygr
příliš	příliš	k6eAd1	příliš
nezlepšuje	zlepšovat	k5eNaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1995	[number]	k4	1995
a	a	k8xC	a
2005	[number]	k4	2005
se	se	k3xPyFc4	se
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
tygra	tygr	k1gMnSc2	tygr
dále	daleko	k6eAd2	daleko
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tygři	tygr	k1gMnPc1	tygr
dnes	dnes	k6eAd1	dnes
obývají	obývat	k5eAaImIp3nP	obývat
pouhých	pouhý	k2eAgInPc2d1	pouhý
7	[number]	k4	7
%	%	kIx~	%
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
sousedících	sousedící	k2eAgFnPc6d1	sousedící
hraničních	hraniční	k2eAgFnPc6d1	hraniční
čínských	čínský	k2eAgFnPc6d1	čínská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
a	a	k8xC	a
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
od	od	k7c2	od
jihočínské	jihočínský	k2eAgFnSc2d1	Jihočínská
provincie	provincie	k1gFnSc2	provincie
Jün-nan	Jünana	k1gFnPc2	Jün-nana
na	na	k7c6	na
severu	sever	k1gInSc6	sever
k	k	k7c3	k
Malajskému	malajský	k2eAgInSc3d1	malajský
poloostrovu	poloostrov	k1gInSc3	poloostrov
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
větší	veliký	k2eAgInSc1d2	veliký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
kapitola	kapitola	k1gFnSc1	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
tygrů	tygr	k1gMnPc2	tygr
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dramaticky	dramaticky	k6eAd1	dramaticky
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
světě	svět	k1gInSc6	svět
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
však	však	k9	však
světová	světový	k2eAgFnSc1d1	světová
populace	populace	k1gFnSc1	populace
tygrů	tygr	k1gMnPc2	tygr
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
již	již	k9	již
jen	jen	k9	jen
na	na	k7c4	na
pouhé	pouhý	k2eAgNnSc4d1	pouhé
4	[number]	k4	4
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jávští	jávský	k2eAgMnPc1d1	jávský
a	a	k8xC	a
tygři	tygr	k1gMnPc1	tygr
kaspičtí	kaspický	k2eAgMnPc1d1	kaspický
byli	být	k5eAaImAgMnP	být
zcela	zcela	k6eAd1	zcela
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
balijský	balijský	k2eAgMnSc1d1	balijský
dokonce	dokonce	k9	dokonce
již	již	k6eAd1	již
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
i	i	k9	i
tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
pouhých	pouhý	k2eAgFnPc2d1	pouhá
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nejrůznějším	různý	k2eAgInPc3d3	nejrůznější
záchranným	záchranný	k2eAgInPc3d1	záchranný
projektům	projekt	k1gInPc3	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Projekt	projekt	k1gInSc1	projekt
Tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
organizace	organizace	k1gFnSc1	organizace
WWF	WWF	kA	WWF
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zvýšit	zvýšit	k5eAaPmF	zvýšit
stavy	stav	k1gInPc4	stav
populací	populace	k1gFnPc2	populace
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
(	(	kIx(	(
<g/>
tygr	tygr	k1gMnSc1	tygr
ussurijský	ussurijský	k2eAgMnSc1d1	ussurijský
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
tygr	tygr	k1gMnSc1	tygr
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
stavy	stav	k1gInPc4	stav
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
však	však	k9	však
počty	počet	k1gInPc1	počet
tygrů	tygr	k1gMnPc2	tygr
nadále	nadále	k6eAd1	nadále
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
divoké	divoký	k2eAgFnPc1d1	divoká
populace	populace	k1gFnPc1	populace
tygrů	tygr	k1gMnPc2	tygr
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
ještě	ještě	k9	ještě
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
5000	[number]	k4	5000
až	až	k9	až
7000	[number]	k4	7000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
však	však	k9	však
počty	počet	k1gInPc1	počet
dále	daleko	k6eAd2	daleko
poklesly	poklesnout	k5eAaPmAgInP	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
3670	[number]	k4	3670
až	až	k9	až
4390	[number]	k4	4390
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
IUCN	IUCN	kA	IUCN
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
+	+	kIx~	+
výsledek	výsledek	k1gInSc1	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snížené	snížený	k2eAgInPc1d1	snížený
počty	počet	k1gInPc1	počet
jdou	jít	k5eAaImIp3nP	jít
však	však	k9	však
částečně	částečně	k6eAd1	částečně
na	na	k7c4	na
vrub	vrub	k1gInSc4	vrub
také	také	k9	také
přesnějším	přesný	k2eAgFnPc3d2	přesnější
metodám	metoda	k1gFnPc3	metoda
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
seznamech	seznam	k1gInPc6	seznam
IUCN	IUCN	kA	IUCN
veden	veden	k2eAgMnSc1d1	veden
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vyhynulého	vyhynulý	k2eAgMnSc4d1	vyhynulý
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
státech	stát	k1gInPc6	stát
-	-	kIx~	-
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
a	a	k8xC	a
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
tygrů	tygr	k1gMnPc2	tygr
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
areálu	areál	k1gInSc2	areál
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
přílohy	příloha	k1gFnSc2	příloha
k	k	k7c3	k
oficiální	oficiální	k2eAgFnSc3d1	oficiální
zprávě	zpráva	k1gFnSc3	zpráva
IUCN	IUCN	kA	IUCN
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Census	census	k1gInSc1	census
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
trend	trend	k1gInSc1	trend
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
480	[number]	k4	480
až	až	k9	až
540	[number]	k4	540
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
mírný	mírný	k2eAgInSc1d1	mírný
nárůst	nárůst	k1gInSc1	nárůst
proti	proti	k7c3	proti
číslům	číslo	k1gNnPc3	číslo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uváděla	uvádět	k5eAaImAgFnS	uvádět
423	[number]	k4	423
až	až	k9	až
502	[number]	k4	502
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
330	[number]	k4	330
až	až	k9	až
390	[number]	k4	390
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
nebyl	být	k5eNaImAgInS	být
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Číny	Čína	k1gFnSc2	Čína
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
žije	žít	k5eAaImIp3nS	žít
malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc4	počet
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
čínský	čínský	k2eAgMnSc1d1	čínský
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
zřejmě	zřejmě	k6eAd1	zřejmě
zcela	zcela	k6eAd1	zcela
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
plánováno	plánovat	k5eAaImNgNnS	plánovat
jeho	jeho	k3xOp3gNnSc1	jeho
znovuvysazení	znovuvysazení	k1gNnSc1	znovuvysazení
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
)	)	kIx)	)
žíjí	žíjit	k5eAaBmIp3nP	žíjit
dnes	dnes	k6eAd1	dnes
tygři	tygr	k1gMnPc1	tygr
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
a	a	k8xC	a
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Rajaji	Rajaje	k1gFnSc4	Rajaje
<g/>
–	–	k?	–
<g/>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Jima	Jimus	k1gMnSc2	Jimus
Corbetta	Corbett	k1gMnSc2	Corbett
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Dudhwa	Dudhw	k1gInSc2	Dudhw
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Bardia	Bardium	k1gNnSc2	Bardium
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
<g/>
,	,	kIx,	,
Buxa	Buxa	k1gMnSc1	Buxa
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Manas	Manas	k1gInSc1	Manas
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Kaziranga	Kaziranga	k1gFnSc1	Kaziranga
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Kanha	Kanha	k1gFnSc1	Kanha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Pench	Pench	k1gInSc1	Pench
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Satpura	Satpura	k1gFnSc1	Satpura
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Melghat	Melghat	k1gFnSc1	Melghat
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Bandhavgarh	Bandhavgarh	k1gInSc1	Bandhavgarh
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Simlipal	Simlipal	k1gFnSc1	Simlipal
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Indravati	Indravati	k1gFnSc1	Indravati
<g/>
,	,	kIx,	,
Nagarjunasagar	Nagarjunasagar	k1gInSc1	Nagarjunasagar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
výskyt	výskyt	k1gInSc1	výskyt
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Nagarhole	Nagarhole	k1gFnSc2	Nagarhole
a	a	k8xC	a
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Bandipur	Bandipura	k1gFnPc2	Bandipura
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgFnSc1d1	zdejší
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1200	[number]	k4	1200
-	-	kIx~	-
1700	[number]	k4	1700
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
není	být	k5eNaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
populace	populace	k1gFnSc1	populace
v	v	k7c4	v
Sundarbans	Sundarbans	k1gInSc4	Sundarbans
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdejších	zdejší	k2eAgInPc6d1	zdejší
mangrovech	mangrovy	k1gInPc6	mangrovy
žije	žít	k5eAaImIp3nS	žít
odhadem	odhad	k1gInSc7	odhad
asi	asi	k9	asi
200	[number]	k4	200
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
70	[number]	k4	70
až	až	k9	až
80	[number]	k4	80
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
pak	pak	k6eAd1	pak
na	na	k7c4	na
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
asi	asi	k9	asi
50	[number]	k4	50
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
i	i	k9	i
zásluhou	zásluha	k1gFnSc7	zásluha
přísné	přísný	k2eAgFnSc2d1	přísná
ochrany	ochrana	k1gFnSc2	ochrana
zaznamenán	zaznamenán	k2eAgInSc4d1	zaznamenán
mírný	mírný	k2eAgInSc4d1	mírný
nárůst	nárůst	k1gInSc4	nárůst
počtu	počet	k1gInSc2	počet
indických	indický	k2eAgMnPc2d1	indický
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
tygrů	tygr	k1gMnPc2	tygr
oproti	oproti	k7c3	oproti
stavu	stav	k1gInSc3	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
jihu	jih	k1gInSc6	jih
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Jün-nan	Jünana	k1gFnPc2	Jün-nana
může	moct	k5eAaImIp3nS	moct
přežívat	přežívat	k5eAaImF	přežívat
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
tygrů	tygr	k1gMnPc2	tygr
indočínských	indočínský	k2eAgMnPc2d1	indočínský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
ostrůvkovitých	ostrůvkovitý	k2eAgInPc6d1	ostrůvkovitý
zbytcích	zbytek	k1gInPc6	zbytek
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnPc1d1	zdejší
populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
než	než	k8xS	než
populace	populace	k1gFnPc1	populace
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
(	(	kIx(	(
<g/>
Barmě	Barma	k1gFnSc6	Barma
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
ještě	ještě	k9	ještě
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
rezervací	rezervace	k1gFnPc2	rezervace
Thung	Thung	k1gMnSc1	Thung
Yai	Yai	k1gMnSc1	Yai
<g/>
–	–	k?	–
<g/>
Huai	Hua	k1gFnSc2	Hua
Kha	Kha	k1gMnSc1	Kha
Khaeng	Khaeng	k1gMnSc1	Khaeng
<g/>
.	.	kIx.	.
</s>
<s>
Tamější	tamější	k2eAgFnSc1d1	tamější
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
150	[number]	k4	150
tygrů	tygr	k1gMnPc2	tygr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
části	část	k1gFnSc6	část
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc6	Laos
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc2	Vietnam
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
údajů	údaj	k1gInPc2	údaj
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
20	[number]	k4	20
tygrech	tygr	k1gMnPc6	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c4	v
rezervaci	rezervace	k1gFnSc4	rezervace
Nam	Nam	k1gFnSc1	Nam
Et-Phou	Et-Pha	k1gFnSc7	Et-Pha
Louey	Louea	k1gMnSc2	Louea
National	National	k1gMnSc2	National
Protected	Protected	k1gMnSc1	Protected
Area	Ares	k1gMnSc2	Ares
(	(	kIx(	(
<g/>
17	[number]	k4	17
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
nebyl	být	k5eNaImAgMnS	být
tygr	tygr	k1gMnSc1	tygr
vyfotografován	vyfotografovat	k5eAaPmNgMnS	vyfotografovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
přes	přes	k7c4	přes
intenzivní	intenzivní	k2eAgFnPc4d1	intenzivní
snahy	snaha	k1gFnPc4	snaha
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k1gNnSc4	málo
záznamů	záznam	k1gInPc2	záznam
o	o	k7c4	o
pozorování	pozorování	k1gNnSc4	pozorování
tygra	tygr	k1gMnSc2	tygr
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
světové	světový	k2eAgFnSc6d1	světová
tygří	tygří	k2eAgFnSc6d1	tygří
rezervaci	rezervace	k1gFnSc6	rezervace
Hukaung	Hukaung	k1gMnSc1	Hukaung
Valley	Vallea	k1gFnSc2	Vallea
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
fotopastmi	fotopast	k1gFnPc7	fotopast
identifikovat	identifikovat	k5eAaBmF	identifikovat
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
různých	různý	k2eAgMnPc2d1	různý
tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
zdejších	zdejší	k2eAgInPc2d1	zdejší
jedinců	jedinec	k1gMnPc2	jedinec
kolísají	kolísat	k5eAaImIp3nP	kolísat
na	na	k7c6	na
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
od	od	k7c2	od
7	[number]	k4	7
do	do	k7c2	do
71	[number]	k4	71
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc1	populace
ovšem	ovšem	k9	ovšem
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
silně	silně	k6eAd1	silně
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
příštím	příští	k2eAgNnSc6d1	příští
vydání	vydání	k1gNnSc6	vydání
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
bude	být	k5eAaImBp3nS	být
tento	tento	k3xDgInSc1	tento
poddruh	poddruh	k1gInSc1	poddruh
překlasifikován	překlasifikovat	k5eAaImNgInS	překlasifikovat
na	na	k7c4	na
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
žijí	žít	k5eAaImIp3nP	žít
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
tři	tři	k4xCgFnPc1	tři
oddělené	oddělený	k2eAgFnPc1d1	oddělená
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
pravděpobně	pravděpobně	k6eAd1	pravděpobně
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
počtem	počet	k1gInSc7	počet
250	[number]	k4	250
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Taman	Taman	k1gMnSc1	Taman
Negara	Negar	k1gMnSc4	Negar
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
vládní	vládní	k2eAgInSc1d1	vládní
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
terénní	terénní	k2eAgInPc1d1	terénní
prúzkumy	prúzkum	k1gInPc1	prúzkum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
oblasti	oblast	k1gFnPc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Gunung	Gunung	k1gMnSc1	Gunung
<g/>
–	–	k?	–
<g/>
Leuser	Leuser	k1gMnSc1	Leuser
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Kerinchi	Kerinche	k1gFnSc4	Kerinche
Seblat	Seble	k1gNnPc2	Seble
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Bukit-Tigapuluh	Bukit-Tigapuluha	k1gFnPc2	Bukit-Tigapuluha
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
pokračujícímu	pokračující	k2eAgNnSc3d1	pokračující
ubývání	ubývání	k1gNnSc3	ubývání
původního	původní	k2eAgInSc2d1	původní
biotopu	biotop	k1gInSc2	biotop
populace	populace	k1gFnSc2	populace
tygra	tygr	k1gMnSc2	tygr
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
stále	stále	k6eAd1	stále
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
klesající	klesající	k2eAgFnSc4d1	klesající
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
tygra	tygr	k1gMnSc4	tygr
je	být	k5eAaImIp3nS	být
ničení	ničení	k1gNnSc4	ničení
jeho	on	k3xPp3gNnSc2	on
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c6	o
kácení	kácení	k1gNnSc6	kácení
lesů	les	k1gInPc2	les
a	a	k8xC	a
pralesů	prales	k1gInPc2	prales
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
snižování	snižování	k1gNnSc2	snižování
stavů	stav	k1gInPc2	stav
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
tygrovi	tygr	k1gMnSc3	tygr
za	za	k7c4	za
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velký	k2eAgFnSc7d1	velká
hrozbou	hrozba	k1gFnSc7	hrozba
je	být	k5eAaImIp3nS	být
pytláctví	pytláctví	k1gNnSc4	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
ilegální	ilegální	k2eAgInSc4d1	ilegální
lov	lov	k1gInSc4	lov
je	být	k5eAaImIp3nS	být
žádanost	žádanost	k1gFnSc1	žádanost
tygřích	tygří	k2eAgInPc2d1	tygří
produktů	produkt	k1gInPc2	produkt
na	na	k7c6	na
čínském	čínský	k2eAgInSc6d1	čínský
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Ceněný	ceněný	k2eAgInSc1d1	ceněný
je	být	k5eAaImIp3nS	být
především	především	k9	především
prášek	prášek	k1gInSc1	prášek
z	z	k7c2	z
rozemletých	rozemletý	k2eAgFnPc2d1	rozemletá
tygřích	tygří	k2eAgFnPc2d1	tygří
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mezi	mezi	k7c7	mezi
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
léty	léto	k1gNnPc7	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stavy	stav	k1gInPc1	stav
čínských	čínský	k2eAgMnPc2d1	čínský
tygrů	tygr	k1gMnPc2	tygr
klesly	klesnout	k5eAaPmAgFnP	klesnout
na	na	k7c4	na
pouhé	pouhý	k2eAgFnPc4d1	pouhá
desítky	desítka	k1gFnPc4	desítka
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
přísně	přísně	k6eAd1	přísně
střežených	střežený	k2eAgFnPc6d1	střežená
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
trh	trh	k1gInSc4	trh
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
pokrýván	pokrýván	k2eAgInSc4d1	pokrýván
dovozy	dovoz	k1gInPc4	dovoz
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
ohrožení	ohrožení	k1gNnSc1	ohrožení
dalších	další	k2eAgInPc2d1	další
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
tygřími	tygří	k2eAgInPc7d1	tygří
produkty	produkt	k1gInPc7	produkt
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
Úmluvou	úmluva	k1gFnSc7	úmluva
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
ohroženými	ohrožený	k2eAgInPc7d1	ohrožený
druhy	druh	k1gInPc7	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
CITES	CITES	kA	CITES
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
ke	k	k7c3	k
smlouvě	smlouva	k1gFnSc3	smlouva
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
vnitrostátní	vnitrostátní	k2eAgFnSc6d1	vnitrostátní
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgInS	být
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
tygřími	tygří	k2eAgInPc7d1	tygří
produkty	produkt	k1gInPc7	produkt
zakázán	zakázat	k5eAaPmNgInS	zakázat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
stavy	stav	k1gInPc1	stav
tygrů	tygr	k1gMnPc2	tygr
dále	daleko	k6eAd2	daleko
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
používání	používání	k1gNnSc4	používání
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
tygřích	tygří	k2eAgFnPc2d1	tygří
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Sporné	sporný	k2eAgNnSc1d1	sporné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
lovu	lov	k1gInSc2	lov
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zakládání	zakládání	k1gNnSc1	zakládání
"	"	kIx"	"
<g/>
tygřích	tygří	k2eAgFnPc2d1	tygří
farem	farma	k1gFnPc2	farma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodej	prodej	k1gInSc1	prodej
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
tygrů	tygr	k1gMnPc2	tygr
chovaných	chovaný	k2eAgMnPc2d1	chovaný
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xS	jako
zástěrka	zástěrka	k1gFnSc1	zástěrka
k	k	k7c3	k
legalizaci	legalizace	k1gFnSc3	legalizace
prodeje	prodej	k1gInSc2	prodej
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
ilegálně	ilegálně	k6eAd1	ilegálně
ulovených	ulovený	k2eAgMnPc2d1	ulovený
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
býval	bývat	k5eAaImAgMnS	bývat
také	také	k9	také
hojně	hojně	k6eAd1	hojně
loven	loven	k2eAgMnSc1d1	loven
kvůli	kvůli	k7c3	kvůli
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
jen	jen	k9	jen
údajným	údajný	k2eAgInPc3d1	údajný
<g/>
)	)	kIx)	)
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
existují	existovat	k5eAaImIp3nP	existovat
projekty	projekt	k1gInPc4	projekt
plánující	plánující	k2eAgNnSc4d1	plánující
znovuvysazení	znovuvysazení	k1gNnSc4	znovuvysazení
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Save	Sav	k1gInSc2	Sav
Chinas	Chinas	k1gInSc4	Chinas
Tigers	Tigers	k1gInSc1	Tigers
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
chovem	chov	k1gInSc7	chov
tygra	tygr	k1gMnSc2	tygr
čínského	čínský	k2eAgMnSc2d1	čínský
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rezervacích	rezervace	k1gFnPc6	rezervace
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
především	především	k9	především
učí	učit	k5eAaImIp3nS	učit
loveckým	lovecký	k2eAgFnPc3d1	lovecká
dovednostem	dovednost	k1gFnPc3	dovednost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
dokázali	dokázat	k5eAaPmAgMnP	dokázat
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
obstarat	obstarat	k5eAaPmF	obstarat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
je	být	k5eAaImIp3nS	být
plánováno	plánován	k2eAgNnSc4d1	plánováno
vypuštění	vypuštění	k1gNnSc4	vypuštění
těchto	tento	k3xDgMnPc2	tento
tygrů	tygr	k1gMnPc2	tygr
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
areálu	areál	k1gInSc6	areál
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ambasadorům	ambasador	k1gMnPc3	ambasador
projektu	projekt	k1gInSc2	projekt
patří	patřit	k5eAaImIp3nP	patřit
známé	známý	k2eAgFnPc1d1	známá
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k9	jako
například	například	k6eAd1	například
herec	herec	k1gMnSc1	herec
Jackie	Jackie	k1gFnSc2	Jackie
Chan	Chan	k1gMnSc1	Chan
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
o	o	k7c6	o
podobném	podobný	k2eAgInSc6d1	podobný
projektu	projekt	k1gInSc6	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
tygry	tygr	k1gMnPc7	tygr
čínské	čínský	k2eAgFnSc2d1	čínská
chované	chovaný	k2eAgFnSc2d1	chovaná
v	v	k7c6	v
čínských	čínský	k2eAgFnPc6d1	čínská
zoo	zoo	k1gFnPc6	zoo
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
plánováno	plánován	k2eAgNnSc1d1	plánováno
znovuvysazování	znovuvysazování	k1gNnSc1	znovuvysazování
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jedinců	jedinec	k1gMnPc2	jedinec
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
lovu	lov	k1gInSc2	lov
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
založena	založen	k2eAgFnSc1d1	založena
velká	velký	k2eAgFnSc1d1	velká
tygří	tygří	k2eAgFnSc1d1	tygří
farma	farma	k1gFnSc1	farma
u	u	k7c2	u
Charbinu	Charbin	k2eAgNnSc3d1	Charbin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
tygřími	tygří	k2eAgInPc7d1	tygří
produkty	produkt	k1gInPc7	produkt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
tygří	tygří	k2eAgInSc4d1	tygří
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
tygrů	tygr	k1gMnPc2	tygr
ussurijských	ussurijský	k2eAgMnPc2d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
200	[number]	k4	200
se	se	k3xPyFc4	se
z	z	k7c2	z
genetického	genetický	k2eAgNnSc2d1	genetické
hlediska	hledisko	k1gNnSc2	hledisko
(	(	kIx(	(
<g/>
čistota	čistota	k1gFnSc1	čistota
linie	linie	k1gFnSc2	linie
<g/>
)	)	kIx)	)
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k9	jako
vhodní	vhodný	k2eAgMnPc1d1	vhodný
kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c6	na
znovuvysazení	znovuvysazení	k1gNnSc6	znovuvysazení
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
velikost	velikost	k1gFnSc1	velikost
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jednotlivý	jednotlivý	k2eAgMnSc1d1	jednotlivý
tygr	tygr	k1gMnSc1	tygr
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
obživu	obživa	k1gFnSc4	obživa
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
naučit	naučit	k5eAaPmF	naučit
tygry	tygr	k1gMnPc4	tygr
životu	život	k1gInSc2	život
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
genetická	genetický	k2eAgFnSc1d1	genetická
variabilita	variabilita	k1gFnSc1	variabilita
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
především	především	k6eAd1	především
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
ale	ale	k9	ale
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
kořisti	kořist	k1gFnPc4	kořist
tygři	tygr	k1gMnPc1	tygr
často	často	k6eAd1	často
urazí	urazit	k5eAaPmIp3nP	urazit
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
především	především	k9	především
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
chudé	chudý	k2eAgFnPc4d1	chudá
na	na	k7c4	na
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgFnSc4d1	východní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgMnPc1d1	tamní
tygři	tygr	k1gMnPc1	tygr
urazí	urazit	k5eAaPmIp3nP	urazit
za	za	k7c4	za
den	den	k1gInSc4	den
průměrně	průměrně	k6eAd1	průměrně
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
km	km	kA	km
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
však	však	k8xC	však
dokonce	dokonce	k9	dokonce
80	[number]	k4	80
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
obcházení	obcházení	k1gNnSc2	obcházení
revíru	revír	k1gInSc2	revír
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
kořisti	kořist	k1gFnSc2	kořist
urazí	urazit	k5eAaPmIp3nS	urazit
tygr	tygr	k1gMnSc1	tygr
často	často	k6eAd1	často
značnou	značný	k2eAgFnSc4d1	značná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
nového	nový	k2eAgNnSc2d1	nové
loviště	loviště	k1gNnSc2	loviště
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
mladý	mladý	k2eAgMnSc1d1	mladý
tygr	tygr	k1gMnSc1	tygr
odcházející	odcházející	k2eAgMnSc1d1	odcházející
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vzdálit	vzdálit	k5eAaPmF	vzdálit
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
revíru	revír	k1gInSc2	revír
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
výborní	výborný	k2eAgMnPc1d1	výborný
plavci	plavec	k1gMnPc1	plavec
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
koček	kočka	k1gFnPc2	kočka
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
přeplavat	přeplavat	k5eAaPmF	přeplavat
řeky	řeka	k1gFnPc4	řeka
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
případ	případ	k1gInSc1	případ
tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
uplaval	uplavat	k5eAaPmAgMnS	uplavat
celých	celý	k2eAgFnPc2d1	celá
29	[number]	k4	29
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
šplhají	šplhat	k5eAaImIp3nP	šplhat
špatně	špatně	k6eAd1	špatně
a	a	k8xC	a
neradi	nerad	k2eAgMnPc1d1	nerad
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
ale	ale	k8xC	ale
dokážou	dokázat	k5eAaPmIp3nP	dokázat
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
i	i	k9	i
do	do	k7c2	do
koruny	koruna	k1gFnSc2	koruna
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
při	při	k7c6	při
masivních	masivní	k2eAgFnPc6d1	masivní
záplavách	záplava	k1gFnPc6	záplava
v	v	k7c4	v
Sundarbans	Sundarbans	k1gInSc4	Sundarbans
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
tygrů	tygr	k1gMnPc2	tygr
se	se	k3xPyFc4	se
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
právě	právě	k6eAd1	právě
vyšplháním	vyšplhání	k1gNnSc7	vyšplhání
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
úkryt	úkryt	k1gInSc1	úkryt
slouží	sloužit	k5eAaImIp3nS	sloužit
tygrům	tygr	k1gMnPc3	tygr
chráněná	chráněný	k2eAgNnPc1d1	chráněné
místa	místo	k1gNnPc1	místo
uvnitř	uvnitř	k7c2	uvnitř
revíru	revír	k1gInSc2	revír
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyvrácené	vyvrácený	k2eAgInPc1d1	vyvrácený
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
houštiny	houština	k1gFnPc1	houština
či	či	k8xC	či
jeskyně	jeskyně	k1gFnPc1	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgInPc1d1	tygří
obývají	obývat	k5eAaImIp3nP	obývat
poměrně	poměrně	k6eAd1	poměrně
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
různých	různý	k2eAgInPc2d1	různý
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
od	od	k7c2	od
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
přes	přes	k7c4	přes
savany	savana	k1gFnPc4	savana
<g/>
,	,	kIx,	,
močály	močál	k1gInPc4	močál
a	a	k8xC	a
listnaté	listnatý	k2eAgInPc1d1	listnatý
a	a	k8xC	a
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
až	až	k9	až
po	po	k7c4	po
jehličnatý	jehličnatý	k2eAgInSc4d1	jehličnatý
severský	severský	k2eAgInSc4d1	severský
les	les	k1gInSc4	les
-	-	kIx~	-
tajgu	tajga	k1gFnSc4	tajga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Asii	Asie	k1gFnSc6	Asie
obýval	obývat	k5eAaImAgMnS	obývat
tygr	tygr	k1gMnSc1	tygr
listnaté	listnatý	k2eAgInPc4d1	listnatý
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
křovinaté	křovinatý	k2eAgInPc4d1	křovinatý
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnPc4d1	říční
údolí	údolí	k1gNnPc4	údolí
mezi	mezi	k7c7	mezi
pouštními	pouštní	k2eAgFnPc7d1	pouštní
a	a	k8xC	a
polopouštními	polopouštní	k2eAgFnPc7d1	polopouštní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
obýval	obývat	k5eAaImAgMnS	obývat
tygr	tygr	k1gMnSc1	tygr
rovněž	rovněž	k9	rovněž
subtropické	subtropický	k2eAgInPc4d1	subtropický
horské	horský	k2eAgInPc4d1	horský
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
obvykle	obvykle	k6eAd1	obvykle
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
byla	být	k5eAaImAgNnP	být
ale	ale	k8xC	ale
ulovena	uloven	k2eAgNnPc1d1	uloveno
zvířata	zvíře	k1gNnPc1	zvíře
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2	[number]	k4	2
500	[number]	k4	500
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
prokázán	prokázán	k2eAgInSc1d1	prokázán
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
4	[number]	k4	4
000	[number]	k4	000
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
Na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
tygři	tygr	k1gMnPc1	tygr
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k9	především
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
nižší	nízký	k2eAgFnSc2d2	nižší
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
obývá	obývat	k5eAaImIp3nS	obývat
dnes	dnes	k6eAd1	dnes
tygr	tygr	k1gMnSc1	tygr
především	především	k6eAd1	především
tzv.	tzv.	kA	tzv.
terai	terai	k1gNnSc1	terai
<g/>
,	,	kIx,	,
bažinaté	bažinatý	k2eAgFnPc1d1	bažinatá
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Indii	Indie	k1gFnSc6	Indie
žijí	žít	k5eAaImIp3nP	žít
tygři	tygr	k1gMnPc1	tygr
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
porostech	porost	k1gInPc6	porost
tvořených	tvořený	k2eAgFnPc2d1	tvořená
salovými	salův	k2eAgInPc7d1	salův
stromy	strom	k1gInPc7	strom
(	(	kIx(	(
<g/>
Shorea	Shorea	k1gMnSc1	Shorea
robusta	robusta	k1gMnSc1	robusta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
porostech	porost	k1gInPc6	porost
trnitých	trnitý	k2eAgFnPc2d1	trnitá
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Ranthambore	Ranthambor	k1gInSc5	Ranthambor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Sundarbans	Sundarbans	k1gInSc4	Sundarbans
žijí	žít	k5eAaImIp3nP	žít
tygři	tygr	k1gMnPc1	tygr
v	v	k7c6	v
mangrovech	mangrovy	k1gInPc6	mangrovy
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ásámu	Ásámo	k1gNnSc6	Ásámo
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
pak	pak	k6eAd1	pak
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
chladu	chlad	k1gInSc3	chlad
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolní	odolný	k2eAgMnPc1d1	odolný
především	především	k9	především
tygři	tygr	k1gMnPc1	tygr
ussurijští	ussurijský	k2eAgMnPc1d1	ussurijský
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
se	s	k7c7	s
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
30	[number]	k4	30
cm	cm	kA	cm
však	však	k9	však
tygrovi	tygr	k1gMnSc3	tygr
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
takovýchto	takovýto	k3xDgFnPc6	takovýto
oblastech	oblast	k1gFnPc6	oblast
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
tygra	tygr	k1gMnSc4	tygr
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kořistí	kořist	k1gFnPc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
prostředí	prostředí	k1gNnSc2	prostředí
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
přizpůsobivý	přizpůsobivý	k2eAgInSc1d1	přizpůsobivý
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ale	ale	k9	ale
vhodné	vhodný	k2eAgInPc4d1	vhodný
úkryty	úkryt	k1gInPc4	úkryt
<g/>
,	,	kIx,	,
dostatek	dostatek	k1gInSc4	dostatek
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
pouze	pouze	k6eAd1	pouze
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mláďata	mládě	k1gNnPc1	mládě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
až	až	k6eAd1	až
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
koťaty	kotě	k1gNnPc7	kotě
či	či	k8xC	či
dospívajícími	dospívající	k2eAgMnPc7d1	dospívající
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
úplnou	úplný	k2eAgFnSc4d1	úplná
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc4d1	sestávající
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Rodinu	rodina	k1gFnSc4	rodina
zakládají	zakládat	k5eAaImIp3nP	zakládat
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
tygři	tygr	k1gMnPc1	tygr
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
revírem	revír	k1gInSc7	revír
<g/>
.	.	kIx.	.
</s>
<s>
Revír	revír	k1gInSc4	revír
si	se	k3xPyFc3	se
tygři	tygr	k1gMnPc1	tygr
značkují	značkovat	k5eAaImIp3nP	značkovat
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
závisí	záviset	k5eAaImIp3nS	záviset
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Revír	revír	k1gInSc1	revír
samce	samec	k1gInSc2	samec
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
více	hodně	k6eAd2	hodně
revíry	revír	k1gInPc1	revír
samic	samice	k1gFnPc2	samice
(	(	kIx(	(
<g/>
2	[number]	k4	2
až	až	k9	až
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
biomasa	biomasa	k1gFnSc1	biomasa
dostupné	dostupný	k2eAgFnSc2d1	dostupná
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
revíru	revír	k1gInSc2	revír
tygřice	tygřice	k1gFnSc2	tygřice
23	[number]	k4	23
km2	km2	k4	km2
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
revíru	revír	k1gInSc2	revír
tygřího	tygří	k2eAgInSc2d1	tygří
samce	samec	k1gInSc2	samec
pak	pak	k6eAd1	pak
68	[number]	k4	68
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
chudších	chudý	k2eAgFnPc2d2	chudší
oblastech	oblast	k1gFnPc6	oblast
ruského	ruský	k2eAgInSc2d1	ruský
dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
biomasa	biomasa	k1gFnSc1	biomasa
dostupné	dostupný	k2eAgFnSc2d1	dostupná
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
revíru	revír	k1gInSc2	revír
tygřice	tygřice	k1gFnSc2	tygřice
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
200	[number]	k4	200
do	do	k7c2	do
400	[number]	k4	400
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
revíry	revír	k1gInPc1	revír
mohou	moct	k5eAaImIp3nP	moct
částečně	částečně	k6eAd1	částečně
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
revíru	revír	k1gInSc2	revír
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
hustotě	hustota	k1gFnSc3	hustota
tygří	tygří	k2eAgFnSc2d1	tygří
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Kanha	Kanha	k1gMnSc1	Kanha
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
320	[number]	k4	320
km2	km2	k4	km2
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
8	[number]	k4	8
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c4	na
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Kaziranga	Kazirang	k1gMnSc4	Kazirang
se	se	k3xPyFc4	se
uživí	uživit	k5eAaPmIp3nS	uživit
dokonce	dokonce	k9	dokonce
i	i	k9	i
16	[number]	k4	16
a	a	k8xC	a
více	hodně	k6eAd2	hodně
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c6	na
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Nagarhole	Nagarhole	k1gFnSc2	Nagarhole
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
tygří	tygří	k2eAgFnSc2d1	tygří
populace	populace	k1gFnSc2	populace
opět	opět	k6eAd1	opět
mezi	mezi	k7c7	mezi
13	[number]	k4	13
až	až	k9	až
15	[number]	k4	15
zvířaty	zvíře	k1gNnPc7	zvíře
na	na	k7c4	na
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
tygří	tygří	k2eAgFnSc2d1	tygří
populace	populace	k1gFnSc2	populace
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
0,5	[number]	k4	0,5
až	až	k9	až
1,4	[number]	k4	1,4
tygra	tygr	k1gMnSc4	tygr
na	na	k7c4	na
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
deštných	deštný	k2eAgInPc6d1	deštný
lesích	les	k1gInPc6	les
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
Sumatry	Sumatra	k1gFnSc2	Sumatra
a	a	k8xC	a
Laosu	Laos	k1gInSc2	Laos
se	se	k3xPyFc4	se
na	na	k7c6	na
hustotě	hustota	k1gFnSc6	hustota
zdejších	zdejší	k2eAgFnPc2d1	zdejší
tygřích	tygří	k2eAgFnPc2d1	tygří
populací	populace	k1gFnPc2	populace
opět	opět	k6eAd1	opět
odráží	odrážet	k5eAaImIp3nS	odrážet
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zdejších	zdejší	k2eAgFnPc2d1	zdejší
tygřích	tygří	k2eAgFnPc2d1	tygří
populací	populace	k1gFnPc2	populace
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
extrémní	extrémní	k2eAgFnSc2d1	extrémní
velikosti	velikost	k1gFnSc2	velikost
revírů	revír	k1gInPc2	revír
tygrů	tygr	k1gMnPc2	tygr
ussurijských	ussurijský	k2eAgFnPc2d1	ussurijský
nemusí	muset	k5eNaImIp3nS	muset
byt	byt	k1gInSc4	byt
pouze	pouze	k6eAd1	pouze
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
dostupnost	dostupnost	k1gFnSc1	dostupnost
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vliv	vliv	k1gInSc4	vliv
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sichote-Aliňské	Sichote-Aliňský	k2eAgFnSc6d1	Sichote-Aliňský
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
se	se	k3xPyFc4	se
mladé	mladý	k2eAgFnPc1d1	mladá
tygřice	tygřice	k1gFnPc1	tygřice
usazovaly	usazovat	k5eAaImAgFnP	usazovat
v	v	k7c6	v
revíru	revír	k1gInSc6	revír
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
svými	svůj	k3xOyFgMnPc7	svůj
zásahy	zásah	k1gInPc7	zásah
nesnižoval	snižovat	k5eNaImAgMnS	snižovat
množství	množství	k1gNnPc4	množství
dostupné	dostupný	k2eAgFnSc2d1	dostupná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
vliv	vliv	k1gInSc4	vliv
člověka	člověk	k1gMnSc2	člověk
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
tygřice	tygřice	k1gFnPc1	tygřice
vyhledávaly	vyhledávat	k5eAaImAgFnP	vyhledávat
nové	nový	k2eAgInPc4d1	nový
revíry	revír	k1gInPc4	revír
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
potřebná	potřebný	k2eAgFnSc1d1	potřebná
velikost	velikost	k1gFnSc1	velikost
revíru	revír	k1gInSc2	revír
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
výše	výše	k1gFnSc1	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
400	[number]	k4	400
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgNnPc1d1	tygří
jsou	být	k5eAaImIp3nP	být
teritoriální	teritoriální	k2eAgNnPc1d1	teritoriální
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nP	bránit
svůj	svůj	k3xOyFgInSc4	svůj
revír	revír	k1gInSc4	revír
proti	proti	k7c3	proti
dalším	další	k2eAgMnPc3d1	další
tygrům	tygr	k1gMnPc3	tygr
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Revír	revír	k1gInSc1	revír
si	se	k3xPyFc3	se
značkují	značkovat	k5eAaImIp3nP	značkovat
močí	moč	k1gFnSc7	moč
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
při	při	k7c6	při
vztyčeném	vztyčený	k2eAgInSc6d1	vztyčený
ocasu	ocas	k1gInSc6	ocas
stříkají	stříkat	k5eAaImIp3nP	stříkat
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
či	či	k8xC	či
keře	keř	k1gInPc4	keř
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
škrábance	škrábanec	k1gInPc1	škrábanec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tygři	tygr	k1gMnPc1	tygr
často	často	k6eAd1	často
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
řev	řev	k1gInSc1	řev
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
svojí	svojit	k5eAaImIp3nS	svojit
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
revíru	revír	k1gInSc6	revír
oznamují	oznamovat	k5eAaImIp3nP	oznamovat
například	například	k6eAd1	například
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
u	u	k7c2	u
tygrů	tygr	k1gMnPc2	tygr
k	k	k7c3	k
označování	označování	k1gNnSc3	označování
revíru	revír	k1gInSc2	revír
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neslouží	sloužit	k5eNaImIp3nP	sloužit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tygři	tygr	k1gMnPc1	tygr
řvou	řvát	k5eAaImIp3nP	řvát
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
tygřice	tygřice	k1gFnPc1	tygřice
často	často	k6eAd1	často
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
revír	revír	k1gInSc4	revír
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
revíru	revír	k1gInSc2	revír
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tygřice	tygřice	k1gFnSc1	tygřice
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
lvice	lvice	k1gFnSc1	lvice
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgMnPc1d1	tygří
samci	samec	k1gMnPc1	samec
putují	putovat	k5eAaImIp3nP	putovat
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
opuštěný	opuštěný	k2eAgInSc4d1	opuštěný
revír	revír	k1gInSc4	revír
nebo	nebo	k8xC	nebo
zahnat	zahnat	k5eAaPmF	zahnat
jiného	jiný	k2eAgMnSc4d1	jiný
samce	samec	k1gMnSc4	samec
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
nemají	mít	k5eNaImIp3nP	mít
specifické	specifický	k2eAgNnSc4d1	specifické
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
by	by	k9	by
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
při	při	k7c6	při
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Amuru	Amur	k1gInSc6	Amur
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
většina	většina	k1gFnSc1	většina
mláďat	mládě	k1gNnPc2	mládě
rodí	rodit	k5eAaImIp3nS	rodit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
samice	samice	k1gFnSc1	samice
připravena	připraven	k2eAgFnSc1d1	připravena
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
to	ten	k3xDgNnSc1	ten
najevo	najevo	k6eAd1	najevo
pachovými	pachový	k2eAgFnPc7d1	pachová
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tygřic	tygřice	k1gFnPc2	tygřice
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
trvá	trvat	k5eAaImIp3nS	trvat
období	období	k1gNnSc4	období
plodnosti	plodnost	k1gFnSc2	plodnost
zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
páry	pár	k1gInPc1	pár
pohromadě	pohromadě	k6eAd1	pohromadě
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k6eAd1	jen
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
čase	čas	k1gInSc6	čas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hojnému	hojný	k2eAgNnSc3d1	hojné
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
×	×	k?	×
až	až	k9	až
52	[number]	k4	52
<g/>
×	×	k?	×
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
akt	akt	k1gInSc1	akt
páření	páření	k1gNnSc2	páření
je	být	k5eAaImIp3nS	být
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
aktu	akt	k1gInSc6	akt
leží	ležet	k5eAaImIp3nS	ležet
samice	samice	k1gFnSc1	samice
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
svírá	svírat	k5eAaImIp3nS	svírat
čelistmi	čelist	k1gFnPc7	čelist
její	její	k3xOp3gInSc4	její
krk	krk	k1gInSc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
Tygřice	tygřice	k1gFnPc1	tygřice
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
útočné	útočný	k2eAgFnPc1d1	útočná
<g/>
,	,	kIx,	,
mručí	mručet	k5eAaImIp3nP	mručet
a	a	k8xC	a
ohánějí	ohánět	k5eAaImIp3nP	ohánět
se	se	k3xPyFc4	se
packami	packa	k1gFnPc7	packa
po	po	k7c6	po
partnerovi	partner	k1gMnSc6	partner
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
páření	páření	k1gNnSc4	páření
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tygřice	tygřice	k1gFnSc1	tygřice
připravena	připraven	k2eAgFnSc1d1	připravena
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
páření	páření	k1gNnSc3	páření
zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
páření	páření	k1gNnSc6	páření
porodí	porodit	k5eAaPmIp3nS	porodit
samice	samice	k1gFnSc1	samice
po	po	k7c4	po
zhruba	zhruba	k6eAd1	zhruba
103	[number]	k4	103
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Vrhy	vrh	k1gInPc1	vrh
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
až	až	k9	až
7	[number]	k4	7
mláďaty	mládě	k1gNnPc7	mládě
jsou	být	k5eAaImIp3nP	být
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
vrhu	vrh	k1gInSc2	vrh
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
3	[number]	k4	3
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
místo	místo	k7c2	místo
porodu	porod	k1gInSc2	porod
tygřice	tygřice	k1gFnSc2	tygřice
volí	volit	k5eAaImIp3nS	volit
kryté	krytý	k2eAgNnSc4d1	kryté
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
houštině	houština	k1gFnSc6	houština
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc6d1	vysoká
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnSc6d1	skalní
štěrbině	štěrbina	k1gFnSc6	štěrbina
nebo	nebo	k8xC	nebo
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
bezmocná	bezmocný	k2eAgFnSc1d1	bezmocná
<g/>
.	.	kIx.	.
</s>
<s>
Porodní	porodní	k2eAgFnSc1d1	porodní
váha	váha	k1gFnSc1	váha
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
800	[number]	k4	800
až	až	k8xS	až
1600	[number]	k4	1600
gramy	gram	k1gInPc7	gram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
skrýše	skrýš	k1gFnSc2	skrýš
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
tygřatům	tygře	k1gNnPc3	tygře
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
začína	začína	k1gFnSc1	začína
tygřice	tygřice	k1gFnSc1	tygřice
své	svůj	k3xOyFgFnSc2	svůj
obchůzky	obchůzka	k1gFnSc2	obchůzka
postupně	postupně	k6eAd1	postupně
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c6	po
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
přestává	přestávat	k5eAaImIp3nS	přestávat
tygřice	tygřice	k1gFnSc1	tygřice
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
kojit	kojit	k5eAaImF	kojit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ještě	ještě	k6eAd1	ještě
nejsou	být	k5eNaImIp3nP	být
schopna	schopen	k2eAgFnSc1d1	schopna
samostatně	samostatně	k6eAd1	samostatně
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
12	[number]	k4	12
až	až	k9	až
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
mladým	mladý	k2eAgMnPc3d1	mladý
tygrům	tygr	k1gMnPc3	tygr
mléčné	mléčný	k2eAgInPc1d1	mléčný
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
věku	věk	k1gInSc2	věk
jsou	být	k5eAaImIp3nP	být
fyzicky	fyzicky	k6eAd1	fyzicky
schopní	schopný	k2eAgMnPc1d1	schopný
samostatného	samostatný	k2eAgInSc2d1	samostatný
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
18	[number]	k4	18
až	až	k9	až
20	[number]	k4	20
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
mladí	mladý	k2eAgMnPc1d1	mladý
tygři	tygr	k1gMnPc1	tygr
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
,	,	kIx,	,
nějaký	nějaký	k3yIgInSc1	nějaký
čas	čas	k1gInSc1	čas
však	však	k9	však
ještě	ještě	k6eAd1	ještě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
revíru	revír	k1gInSc6	revír
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
odchodu	odchod	k1gInSc2	odchod
souvisí	souviset	k5eAaImIp3nS	souviset
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
narozením	narození	k1gNnSc7	narození
nového	nový	k2eAgInSc2d1	nový
vrhu	vrh	k1gInSc2	vrh
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
studie	studie	k1gFnSc2	studie
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
samci	samec	k1gMnPc1	samec
odcházeli	odcházet	k5eAaImAgMnP	odcházet
průměrně	průměrně	k6eAd1	průměrně
33	[number]	k4	33
km	km	kA	km
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
revíru	revír	k1gInSc2	revír
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
deseti	deset	k4xCc2	deset
sledovaných	sledovaný	k2eAgMnPc2d1	sledovaný
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
pouze	pouze	k6eAd1	pouze
čtyřem	čtyři	k4xCgInPc3	čtyři
podařilo	podařit	k5eAaPmAgNnS	podařit
úspěšně	úspěšně	k6eAd1	úspěšně
obsadit	obsadit	k5eAaPmF	obsadit
nový	nový	k2eAgInSc4d1	nový
revír	revír	k1gInSc4	revír
<g/>
.	.	kIx.	.
</s>
<s>
Tygřice	tygřice	k1gFnPc1	tygřice
mívají	mívat	k5eAaImIp3nP	mívat
první	první	k4xOgNnSc4	první
potomky	potomek	k1gMnPc4	potomek
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tygří	tygří	k2eAgMnPc1d1	tygří
samci	samec	k1gMnPc1	samec
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
rodiči	rodič	k1gMnPc7	rodič
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
plodné	plodný	k2eAgFnPc1d1	plodná
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
šest	šest	k4xCc4	šest
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
však	však	k9	však
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
dožití	dožití	k1gNnSc1	dožití
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgFnPc2d1	žijící
tygřic	tygřice	k1gFnPc2	tygřice
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluhou	k7c2	zásluhou
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
mláďat	mládě	k1gNnPc2	mládě
tak	tak	k6eAd1	tak
jedna	jeden	k4xCgFnSc1	jeden
tygřice	tygřice	k1gFnSc1	tygřice
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
odchová	odchovat	k5eAaPmIp3nS	odchovat
průměrně	průměrně	k6eAd1	průměrně
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
16	[number]	k4	16
až	až	k9	až
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
se	se	k3xPyFc4	se
dožijí	dožít	k5eAaPmIp3nP	dožít
věku	věk	k1gInSc3	věk
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
tiší	tichý	k2eAgMnPc1d1	tichý
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
disponují	disponovat	k5eAaBmIp3nP	disponovat
velkou	velký	k2eAgFnSc7d1	velká
škálou	škála	k1gFnSc7	škála
různých	různý	k2eAgInPc2d1	různý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
se	se	k3xPyFc4	se
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
<g/>
,	,	kIx,	,
hluboký	hluboký	k2eAgInSc1d1	hluboký
řev	řev	k1gInSc1	řev
<g/>
,	,	kIx,	,
znící	znící	k2eAgInSc1d1	znící
jako	jako	k8xC	jako
A-o-ung	Ang	k1gInSc1	A-o-ung
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vícekrát	vícekrát	k6eAd1	vícekrát
po	po	k7c6	po
sobě	se	k3xPyFc3	se
opakován	opakován	k2eAgInSc1d1	opakován
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řev	řev	k1gInSc1	řev
bývá	bývat	k5eAaImIp3nS	bývat
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
namlouvacími	namlouvací	k2eAgInPc7d1	namlouvací
rituály	rituál	k1gInPc7	rituál
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
tygr	tygr	k1gMnSc1	tygr
často	často	k6eAd1	často
vydává	vydávat	k5eAaImIp3nS	vydávat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
řevu	řev	k1gInSc2	řev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
krátké	krátký	k2eAgInPc4d1	krátký
<g/>
,	,	kIx,	,
prudké	prudký	k2eAgInPc4d1	prudký
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
zakašlání	zakašlání	k1gNnSc1	zakašlání
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
zvuk	zvuk	k1gInSc1	zvuk
vydává	vydávat	k5eAaPmIp3nS	vydávat
tygří	tygří	k2eAgInSc1d1	tygří
samec	samec	k1gInSc1	samec
i	i	k9	i
při	při	k7c6	při
pářícím	pářící	k2eAgInSc6d1	pářící
aktu	akt	k1gInSc6	akt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
potravou	potrava	k1gFnSc7	potrava
tygra	tygr	k1gMnSc4	tygr
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
stopuje	stopovat	k5eAaImIp3nS	stopovat
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
sprintu	sprint	k1gInSc6	sprint
skolí	skolit	k5eAaImIp3nS	skolit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
a	a	k8xC	a
jelenovitou	jelenovitý	k2eAgFnSc4d1	jelenovitá
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
potravy	potrava	k1gFnSc2	potrava
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nP	tvořit
malí	malý	k2eAgMnPc1d1	malý
savci	savec	k1gMnPc1	savec
jako	jako	k8xC	jako
zajíci	zajíc	k1gMnPc1	zajíc
či	či	k8xC	či
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ptáci	pták	k1gMnPc1	pták
či	či	k8xC	či
plazi	plaz	k1gMnPc1	plaz
včetně	včetně	k7c2	včetně
velkých	velký	k2eAgMnPc2d1	velký
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
sám	sám	k3xTgInSc4	sám
skolit	skolit	k5eAaImF	skolit
i	i	k9	i
tak	tak	k6eAd1	tak
mohutné	mohutný	k2eAgNnSc1d1	mohutné
zvíře	zvíře	k1gNnSc1	zvíře
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
gaur	gaur	k1gMnSc1	gaur
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
tur.	tur.	k?	tur.
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
kořistí	kořist	k1gFnSc7	kořist
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
areálu	areál	k1gInSc6	areál
jeho	jeho	k3xOp3gInSc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
jelenovitá	jelenovitý	k2eAgFnSc1d1	jelenovitá
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
párcích	párek	k1gInPc6	párek
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Čitvan	Čitvan	k1gMnSc1	Čitvan
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Narodní	Narodní	k2eAgInSc1d1	Narodní
park	park	k1gInSc1	park
Nagarhole	Nagarhole	k1gFnSc2	Nagarhole
či	či	k8xC	či
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Kanha	Kanh	k1gMnSc2	Kanh
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
velká	velký	k2eAgFnSc1d1	velká
jelenovitá	jelenovitý	k2eAgFnSc1d1	jelenovitá
zvěř	zvěř	k1gFnSc1	zvěř
(	(	kIx(	(
<g/>
axis	axis	k1gInSc1	axis
<g/>
,	,	kIx,	,
sambar	sambar	k1gInSc1	sambar
<g/>
,	,	kIx,	,
barasinga	barasinga	k1gFnSc1	barasinga
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
tygří	tygří	k2eAgFnSc2d1	tygří
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
Nagarhole	Nagarhol	k1gInSc6	Nagarhol
tvoří	tvořit	k5eAaImIp3nP	tvořit
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tygří	tygří	k2eAgFnSc6d1	tygří
kosti	kost	k1gFnSc6	kost
také	také	k9	také
obrovský	obrovský	k2eAgInSc4d1	obrovský
gaur	gaur	k1gInSc4	gaur
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnSc4d1	další
významnou	významný	k2eAgFnSc4d1	významná
kořist	kořist	k1gFnSc4	kořist
patří	patřit	k5eAaImIp3nP	patřit
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
také	také	k9	také
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
menší	malý	k2eAgMnPc1d2	menší
jelínci	jelínek	k1gMnPc1	jelínek
(	(	kIx(	(
<g/>
muntžak	muntžak	k1gMnSc1	muntžak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dikobrazi	dikobraz	k1gMnPc1	dikobraz
<g/>
,	,	kIx,	,
zajícovci	zajícovec	k1gMnPc1	zajícovec
a	a	k8xC	a
languři	langur	k1gMnPc1	langur
mají	mít	k5eAaImIp3nP	mít
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
malé	malý	k2eAgFnSc3d1	malá
velikosti	velikost	k1gFnSc3	velikost
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
kořisti	kořist	k1gFnSc6	kořist
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc4d1	omezený
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
kořisti	kořist	k1gFnSc2	kořist
také	také	k9	také
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
především	především	k9	především
nilgau	nilgau	k1gMnSc1	nilgau
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
thajské	thajský	k2eAgFnSc6d1	thajská
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Huai	Hua	k1gFnSc2	Hua
Kha	Kha	k1gFnSc2	Kha
Khaeng	Khaenga	k1gFnPc2	Khaenga
tvoří	tvořit	k5eAaImIp3nS	tvořit
tygří	tygří	k2eAgFnSc4d1	tygří
potravu	potrava	k1gFnSc4	potrava
převážně	převážně	k6eAd1	převážně
sambarové	sambarové	k2eAgFnSc4d1	sambarové
<g/>
,	,	kIx,	,
muntžaci	muntžace	k1gFnSc4	muntžace
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgFnSc1d1	divoká
prásata	prásata	k1gFnSc1	prásata
<g/>
,	,	kIx,	,
dikobrazi	dikobraz	k1gMnPc1	dikobraz
a	a	k8xC	a
jezevci	jezevec	k1gMnPc1	jezevec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Sichote-Aliň	Sichote-Aliň	k1gFnSc1	Sichote-Aliň
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
tygři	tygr	k1gMnPc1	tygr
živí	živit	k5eAaImIp3nP	živit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
divokými	divoký	k2eAgNnPc7d1	divoké
prasaty	prase	k1gNnPc7	prase
a	a	k8xC	a
jeleny	jelena	k1gFnPc4	jelena
wapiti	wapit	k5eAaImF	wapit
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
výskyt	výskyt	k1gInSc1	výskyt
tygra	tygr	k1gMnSc2	tygr
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
velké	velký	k2eAgFnSc2d1	velká
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
jelenovitá	jelenovitý	k2eAgFnSc1d1	jelenovitá
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
vymřelého	vymřelý	k2eAgMnSc2d1	vymřelý
tygra	tygr	k1gMnSc2	tygr
kaspického	kaspický	k2eAgMnSc2d1	kaspický
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
shodoval	shodovat	k5eAaImAgMnS	shodovat
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
jelena	jelen	k1gMnSc2	jelen
bucharského	bucharský	k2eAgMnSc2d1	bucharský
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gInSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
bactrianus	bactrianus	k1gMnSc1	bactrianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srnčí	srnčí	k2eAgFnSc2d1	srnčí
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
říčních	říční	k2eAgNnPc6d1	říční
údolích	údolí	k1gNnPc6	údolí
jinak	jinak	k6eAd1	jinak
suchých	suchý	k2eAgFnPc2d1	suchá
oblastí	oblast	k1gFnPc2	oblast
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
lovil	lovit	k5eAaImAgMnS	lovit
kaspický	kaspický	k2eAgMnSc1d1	kaspický
tygr	tygr	k1gMnSc1	tygr
i	i	k8xC	i
gazely	gazela	k1gFnPc1	gazela
džejran	džejrana	k1gFnPc2	džejrana
a	a	k8xC	a
divoké	divoký	k2eAgFnSc2d1	divoká
lišky	liška	k1gFnSc2	liška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středoasijských	středoasijský	k2eAgFnPc6d1	středoasijská
stepích	step	k1gFnPc6	step
pak	pak	k6eAd1	pak
i	i	k9	i
velké	velký	k2eAgFnSc2d1	velká
antilopy	antilopa	k1gFnSc2	antilopa
sajgy	sajga	k1gFnSc2	sajga
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
dokáží	dokázat	k5eAaPmIp3nP	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gFnPc4	on
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
pozorovány	pozorován	k2eAgInPc4d1	pozorován
útoky	útok	k1gInPc4	útok
tygrů	tygr	k1gMnPc2	tygr
na	na	k7c4	na
velké	velký	k2eAgMnPc4d1	velký
tury	tur	k1gMnPc4	tur
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
arni	arni	k6eAd1	arni
a	a	k8xC	a
gaur	gaur	k1gInSc4	gaur
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cílem	cíl	k1gInSc7	cíl
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
telata	tele	k1gNnPc1	tele
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
tygři	tygr	k1gMnPc1	tygr
útočí	útočit	k5eAaImIp3nP	útočit
i	i	k9	i
na	na	k7c4	na
tapíry	tapír	k1gMnPc4	tapír
čabrakové	čabrakový	k2eAgNnSc1d1	čabrakový
a	a	k8xC	a
troufnu	troufnout	k5eAaPmIp1nS	troufnout
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
mladého	mladý	k2eAgMnSc4d1	mladý
nosorožce	nosorožec	k1gMnSc4	nosorožec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
na	na	k7c4	na
slony	slon	k1gMnPc4	slon
jsou	být	k5eAaImIp3nP	být
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
slůňata	slůně	k1gNnPc4	slůně
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
důvěryhodná	důvěryhodný	k2eAgNnPc1d1	důvěryhodné
svědectví	svědectví	k1gNnPc1	svědectví
o	o	k7c6	o
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
dospělé	dospělý	k2eAgInPc4d1	dospělý
samce	samec	k1gInPc4	samec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
tvoří	tvořit	k5eAaImIp3nP	tvořit
část	část	k1gFnSc4	část
tygří	tygří	k2eAgFnSc2d1	tygří
kořisti	kořist	k1gFnSc2	kořist
i	i	k9	i
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
indičtí	indický	k2eAgMnPc1d1	indický
medvědi	medvěd	k1gMnPc1	medvěd
pyskatí	pyskatý	k2eAgMnPc1d1	pyskatý
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
tygří	tygří	k2eAgFnSc7d1	tygří
kořistí	kořist	k1gFnSc7	kořist
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
ušatí	ušatý	k2eAgMnPc1d1	ušatý
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
ussurijských	ussurijský	k2eAgMnPc2d1	ussurijský
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
tvoří	tvořit	k5eAaImIp3nP	tvořit
medvědi	medvěd	k1gMnPc1	medvěd
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
<g/>
%	%	kIx~	%
tygří	tygří	k2eAgFnSc2d1	tygří
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k8xC	i
případy	případ	k1gInPc1	případ
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
ulovení	ulovení	k1gNnSc2	ulovení
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
loví	lovit	k5eAaImIp3nS	lovit
tygr	tygr	k1gMnSc1	tygr
kromě	kromě	k7c2	kromě
jelenů	jelen	k1gMnPc2	jelen
wapiti	wapit	k5eAaBmF	wapit
především	především	k9	především
soby	soba	k1gFnPc1	soba
<g/>
,	,	kIx,	,
jeleny	jelena	k1gFnPc1	jelena
sika	sika	k1gMnSc1	sika
<g/>
,	,	kIx,	,
kabary	kabar	k1gMnPc4	kabar
<g/>
,	,	kIx,	,
srnce	srnec	k1gMnPc4	srnec
a	a	k8xC	a
goraly	goral	k1gMnPc4	goral
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
také	také	k9	také
rysy	rys	k1gMnPc4	rys
<g/>
,	,	kIx,	,
jezevce	jezevec	k1gMnPc4	jezevec
<g/>
,	,	kIx,	,
zajíce	zajíc	k1gMnPc4	zajíc
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
tetřevovité	tetřevovitý	k2eAgMnPc4d1	tetřevovitý
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
indičtí	indický	k2eAgMnPc1d1	indický
tygři	tygr	k1gMnPc1	tygr
nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
menší	malý	k2eAgFnSc7d2	menší
kořistí	kořist	k1gFnSc7	kořist
jako	jako	k8xS	jako
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
,	,	kIx,	,
želvy	želva	k1gFnPc4	želva
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
kobylky	kobylka	k1gFnPc1	kobylka
či	či	k8xC	či
žáby	žába	k1gFnPc1	žába
<g/>
.	.	kIx.	.
</s>
<s>
Kořistí	kořist	k1gFnSc7	kořist
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
masožravci	masožravec	k1gMnPc1	masožravec
jako	jako	k8xC	jako
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Levharti	levhart	k1gMnPc1	levhart
bývají	bývat	k5eAaImIp3nP	bývat
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
jako	jako	k8xC	jako
potravní	potravní	k2eAgMnPc1d1	potravní
konkurenti	konkurent	k1gMnPc1	konkurent
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
následně	následně	k6eAd1	následně
i	i	k9	i
sežráni	sežrán	k2eAgMnPc1d1	sežrán
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
tygři	tygr	k1gMnPc1	tygr
zkonzumují	zkonzumovat	k5eAaPmIp3nP	zkonzumovat
občas	občas	k6eAd1	občas
i	i	k9	i
ovoce	ovoce	k1gNnSc4	ovoce
nebo	nebo	k8xC	nebo
trávu	tráva	k1gFnSc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Kanibalismus	kanibalismus	k1gInSc1	kanibalismus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
většinou	většina	k1gFnSc7	většina
buď	buď	k8xC	buď
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
sežraná	sežraný	k2eAgNnPc4d1	sežrané
cizím	cizí	k2eAgInSc7d1	cizí
samcem	samec	k1gInSc7	samec
nebo	nebo	k8xC	nebo
o	o	k7c6	o
pojídání	pojídání	k1gNnSc6	pojídání
nalezené	nalezený	k2eAgFnSc2d1	nalezená
mršiny	mršina	k1gFnSc2	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
někdy	někdy	k6eAd1	někdy
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
větší	veliký	k2eAgMnPc1d2	veliký
kopytníci	kopytník	k1gMnPc1	kopytník
jako	jako	k8xC	jako
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgMnPc1d1	vodní
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
osli	osel	k1gMnPc1	osel
a	a	k8xC	a
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
útoky	útok	k1gInPc1	útok
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
však	však	k9	však
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
kořist	kořist	k1gFnSc4	kořist
specializovali	specializovat	k5eAaBmAgMnP	specializovat
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
se	se	k3xPyFc4	se
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
plíží	plížit	k5eAaImIp3nS	plížit
nebo	nebo	k8xC	nebo
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
číhají	číhat	k5eAaImIp3nP	číhat
a	a	k8xC	a
vrhnou	vrhnout	k5eAaPmIp3nP	vrhnout
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
po	po	k7c6	po
několika	několik	k4yIc6	několik
skocích	skok	k1gInPc6	skok
či	či	k8xC	či
krátkém	krátký	k2eAgInSc6d1	krátký
sprintu	sprint	k1gInSc6	sprint
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
lvům	lev	k1gInPc3	lev
tygři	tygr	k1gMnPc1	tygr
berou	brát	k5eAaImIp3nP	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
směr	směr	k1gInSc4	směr
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
se	se	k3xPyFc4	se
k	k	k7c3	k
vyhlédnuté	vyhlédnutý	k2eAgFnSc3d1	vyhlédnutá
kořisti	kořist	k1gFnSc3	kořist
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
krýt	krýt	k5eAaImF	krýt
a	a	k8xC	a
přiblížit	přiblížit	k5eAaPmF	přiblížit
se	se	k3xPyFc4	se
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
–	–	k?	–
35	[number]	k4	35
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
příliš	příliš	k6eAd1	příliš
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
mezi	mezi	k7c7	mezi
tygrem	tygr	k1gMnSc7	tygr
a	a	k8xC	a
kořistí	kořist	k1gFnSc7	kořist
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgNnSc1	žádný
krytí	krytí	k1gNnSc1	krytí
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
sama	sám	k3xTgMnSc4	sám
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
fází	fáze	k1gFnSc7	fáze
útoku	útok	k1gInSc2	útok
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
rychlý	rychlý	k2eAgInSc4d1	rychlý
sprint	sprint	k1gInSc4	sprint
za	za	k7c7	za
kořistí	kořist	k1gFnSc7	kořist
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tygr	tygr	k1gMnSc1	tygr
kořist	kořist	k1gFnSc4	kořist
ihned	ihned	k6eAd1	ihned
nedostihne	dostihnout	k5eNaPmIp3nS	dostihnout
<g/>
,	,	kIx,	,
žene	hnát	k5eAaImIp3nS	hnát
se	se	k3xPyFc4	se
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
maximálně	maximálně	k6eAd1	maximálně
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pronásledování	pronásledování	k1gNnSc1	pronásledování
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kořist	kořist	k1gFnSc1	kořist
dostihne	dostihnout	k5eAaPmIp3nS	dostihnout
<g/>
,	,	kIx,	,
velká	velký	k2eAgNnPc1d1	velké
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
srazit	srazit	k5eAaPmF	srazit
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
silou	síla	k1gFnSc7	síla
nárazu	náraz	k1gInSc2	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
tlamou	tlama	k1gFnSc7	tlama
na	na	k7c4	na
hrdlo	hrdlo	k1gNnSc4	hrdlo
<g/>
,	,	kIx,	,
zespodu	zespodu	k6eAd1	zespodu
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
takto	takto	k6eAd1	takto
bývá	bývat	k5eAaImIp3nS	bývat
oběť	oběť	k1gFnSc1	oběť
často	často	k6eAd1	často
zardoušena	zardoušen	k2eAgFnSc1d1	zardoušena
<g/>
.	.	kIx.	.
</s>
<s>
Tlapy	tlapa	k1gFnPc4	tlapa
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přidržení	přidržení	k1gNnSc3	přidržení
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
zabita	zabít	k5eAaPmNgFnS	zabít
překousnutím	překousnutí	k1gNnSc7	překousnutí
vazu	vaz	k1gInSc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
tygr	tygr	k1gMnSc1	tygr
útočí	útočit	k5eAaImIp3nS	útočit
i	i	k9	i
na	na	k7c4	na
krk	krk	k1gInSc4	krk
většího	veliký	k2eAgNnSc2d2	veliký
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prokousnout	prokousnout	k5eAaPmF	prokousnout
obratle	obratel	k1gInSc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
velká	velký	k2eAgNnPc1d1	velké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
velcí	velký	k2eAgMnPc1d1	velký
tuři	tur	k1gMnPc1	tur
<g/>
,	,	kIx,	,
však	však	k9	však
takto	takto	k6eAd1	takto
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
tygři	tygr	k1gMnPc1	tygr
útočí	útočit	k5eAaImIp3nP	útočit
kousanci	kousanec	k1gInPc7	kousanec
do	do	k7c2	do
hrdla	hrdlo	k1gNnSc2	hrdlo
či	či	k8xC	či
tlamy	tlama	k1gFnSc2	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Divocí	divoký	k2eAgMnPc1d1	divoký
tuři	tur	k1gMnPc1	tur
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
sloni	slon	k1gMnPc1	slon
bývají	bývat	k5eAaImIp3nP	bývat
napadáni	napadán	k2eAgMnPc1d1	napadán
i	i	k9	i
zezadu	zezadu	k6eAd1	zezadu
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
překousnout	překousnout	k5eAaPmF	překousnout
šlachy	šlacha	k1gFnPc4	šlacha
a	a	k8xC	a
zvířeti	zvíře	k1gNnSc6	zvíře
znemožnit	znemožnit	k5eAaPmF	znemožnit
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
medvěda	medvěd	k1gMnSc4	medvěd
útočí	útočit	k5eAaImIp3nS	útočit
tygr	tygr	k1gMnSc1	tygr
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zezadu	zezadu	k6eAd1	zezadu
na	na	k7c4	na
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
prokousnout	prokousnout	k5eAaPmF	prokousnout
krční	krční	k2eAgInPc4d1	krční
obratle	obratel	k1gInPc4	obratel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
dospělého	dospělý	k1gMnSc4	dospělý
slona	slon	k1gMnSc4	slon
musí	muset	k5eAaImIp3nS	muset
tygr	tygr	k1gMnSc1	tygr
útočit	útočit	k5eAaImF	útočit
zezadu	zezadu	k6eAd1	zezadu
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
klům	kel	k1gInPc3	kel
a	a	k8xC	a
chobotu	chobot	k1gInSc3	chobot
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
však	však	k9	však
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
obvykle	obvykle	k6eAd1	obvykle
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
odláká	odlákat	k5eAaPmIp3nS	odlákat
pozornost	pozornost	k1gFnSc4	pozornost
slona	slon	k1gMnSc2	slon
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhý	druhý	k4xOgMnSc1	druhý
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skoku	skok	k1gInSc6	skok
na	na	k7c4	na
hřbet	hřbet	k1gInSc4	hřbet
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tygr	tygr	k1gMnSc1	tygr
slona	slon	k1gMnSc4	slon
zranit	zranit	k5eAaPmF	zranit
kousanci	kousanec	k1gInSc3	kousanec
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
útok	útok	k1gInSc1	útok
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
u	u	k7c2	u
slona	slon	k1gMnSc2	slon
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
ztrátě	ztráta	k1gFnSc3	ztráta
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ulovenou	ulovený	k2eAgFnSc4d1	ulovená
kořist	kořist	k1gFnSc4	kořist
si	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
tygr	tygr	k1gMnSc1	tygr
odtáhne	odtáhnout	k5eAaPmIp3nS	odtáhnout
do	do	k7c2	do
vyhlédnutého	vyhlédnutý	k2eAgInSc2d1	vyhlédnutý
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
i	i	k9	i
několikasetkilové	několikasetkilový	k2eAgInPc1d1	několikasetkilový
kusy	kus	k1gInPc1	kus
dospělé	dospělý	k2eAgFnSc2d1	dospělá
zvěře	zvěř	k1gFnSc2	zvěř
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odtaženy	odtažen	k2eAgFnPc4d1	odtažena
stovky	stovka	k1gFnPc4	stovka
metrů	metr	k1gInPc2	metr
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
obvykle	obvykle	k6eAd1	obvykle
začínají	začínat	k5eAaImIp3nP	začínat
požírat	požírat	k5eAaImF	požírat
kořist	kořist	k1gFnSc4	kořist
od	od	k7c2	od
kyčlí	kyčel	k1gFnPc2	kyčel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lvi	lev	k1gMnPc1	lev
nejprve	nejprve	k6eAd1	nejprve
otevřou	otevřít	k5eAaPmIp3nP	otevřít
břicho	břicho	k1gNnSc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
požíraní	požíraný	k2eAgMnPc1d1	požíraný
kořisti	kořist	k1gFnSc6	kořist
nebo	nebo	k8xC	nebo
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
tygr	tygr	k1gMnSc1	tygr
pije	pít	k5eAaImIp3nS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jí	on	k3xPp3gFnSc7	on
celou	cela	k1gFnSc7	cela
nezpracuje	zpracovat	k5eNaPmIp3nS	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vzdálit	vzdálit	k5eAaPmF	vzdálit
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
zakryje	zakrýt	k5eAaPmIp3nS	zakrýt
ho	on	k3xPp3gInSc4	on
listím	listí	k1gNnSc7	listí
a	a	k8xC	a
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
kořisti	kořist	k1gFnSc2	kořist
obvykle	obvykle	k6eAd1	obvykle
zůstane	zůstat	k5eAaPmIp3nS	zůstat
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
sežrat	sežrat	k5eAaPmF	sežrat
při	při	k7c6	při
jednom	jeden	k4xCgNnSc6	jeden
krmení	krmení	k1gNnSc6	krmení
18	[number]	k4	18
až	až	k9	až
27	[number]	k4	27
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
případech	případ	k1gInPc6	případ
možná	možná	k9	možná
až	až	k6eAd1	až
40	[number]	k4	40
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgFnSc1d1	tygří
samice	samice	k1gFnSc1	samice
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
denně	denně	k6eAd1	denně
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
z	z	k7c2	z
uloveného	ulovený	k2eAgInSc2d1	ulovený
kusu	kus	k1gInSc2	kus
lze	lze	k6eAd1	lze
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
ulovit	ulovit	k5eAaPmF	ulovit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
kořist	kořist	k1gFnSc1	kořist
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hmotnosti	hmotnost	k1gFnSc6	hmotnost
zhruba	zhruba	k6eAd1	zhruba
2400	[number]	k4	2400
až	až	k9	až
2850	[number]	k4	2850
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
jednomu	jeden	k4xCgNnSc3	jeden
sambarovi	sambar	k1gMnSc3	sambar
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
200	[number]	k4	200
kg	kg	kA	kg
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
nebo	nebo	k8xC	nebo
jednomu	jeden	k4xCgNnSc3	jeden
muntžakovi	muntžakův	k2eAgMnPc1d1	muntžakův
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výchovy	výchova	k1gFnSc2	výchova
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
spotřeba	spotřeba	k1gFnSc1	spotřeba
masa	maso	k1gNnSc2	maso
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
tygra	tygr	k1gMnSc2	tygr
ussurijského	ussurijský	k2eAgMnSc2d1	ussurijský
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zhruba	zhruba	k6eAd1	zhruba
5000	[number]	k4	5000
kg	kg	kA	kg
masa	maso	k1gNnSc2	maso
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
kusům	kus	k1gInPc3	kus
větší	veliký	k2eAgFnSc2d2	veliký
kořisti	kořist	k1gFnSc2	kořist
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krmení	krmení	k1gNnSc6	krmení
se	se	k3xPyFc4	se
tygr	tygr	k1gMnSc1	tygr
pomocí	pomocí	k7c2	pomocí
jazyka	jazyk	k1gInSc2	jazyk
očistí	očistit	k5eAaPmIp3nS	očistit
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
špíny	špína	k1gFnSc2	špína
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
omyje	omýt	k5eAaPmIp3nS	omýt
<g/>
"	"	kIx"	"
přední	přední	k2eAgFnSc7d1	přední
tlapou	tlapa	k1gFnSc7	tlapa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
pravidelně	pravidelně	k6eAd1	pravidelně
olizována	olizován	k2eAgFnSc1d1	olizován
a	a	k8xC	a
čištěna	čištěn	k2eAgFnSc1d1	čištěna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
odpočinku	odpočinek	k1gInSc2	odpočinek
si	se	k3xPyFc3	se
tygr	tygr	k1gMnSc1	tygr
příležitostně	příležitostně	k6eAd1	příležitostně
čistí	čistit	k5eAaImIp3nS	čistit
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Výkal	výkal	k1gInSc1	výkal
tygra	tygr	k1gMnSc2	tygr
je	být	k5eAaImIp3nS	být
podlouhlý	podlouhlý	k2eAgInSc1d1	podlouhlý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
35	[number]	k4	35
až	až	k9	až
40	[number]	k4	40
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
polotuhé	polotuhý	k2eAgFnSc2d1	polotuhá
smolovité	smolovitý	k2eAgFnSc2d1	smolovitý
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
strava	strava	k1gFnSc1	strava
skládala	skládat	k5eAaImAgFnS	skládat
především	především	k6eAd1	především
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
nebo	nebo	k8xC	nebo
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nestrávené	strávený	k2eNgInPc4d1	nestrávený
zbytky	zbytek	k1gInPc4	zbytek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
chlupy	chlup	k1gInPc1	chlup
či	či	k8xC	či
kosti	kost	k1gFnPc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
tygra	tygr	k1gMnSc2	tygr
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgInPc1d1	častý
v	v	k7c4	v
Sundarbans	Sundarbans	k1gInSc4	Sundarbans
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ganžské	ganžský	k2eAgFnSc2d1	ganžský
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
Indie	Indie	k1gFnSc2	Indie
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
pak	pak	k6eAd1	pak
zcela	zcela	k6eAd1	zcela
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
Sundarbans	Sundarbans	k1gInSc4	Sundarbans
roztrháno	roztrhán	k2eAgNnSc1d1	roztrháno
tygry	tygr	k1gMnPc7	tygr
průměrně	průměrně	k6eAd1	průměrně
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
obvykle	obvykle	k6eAd1	obvykle
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
tygr	tygr	k1gMnSc1	tygr
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
stane	stanout	k5eAaPmIp3nS	stanout
"	"	kIx"	"
<g/>
lidožroutem	lidožrout	k1gMnSc7	lidožrout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tygrovi	tygr	k1gMnSc3	tygr
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
míře	míra	k1gFnSc6	míra
lovit	lovit	k5eAaImF	lovit
jeho	jeho	k3xOp3gFnSc4	jeho
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
zaměřit	zaměřit	k5eAaPmF	zaměřit
právě	právě	k9	právě
na	na	k7c4	na
lov	lov	k1gInSc4	lov
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
a	a	k8xC	a
méně	málo	k6eAd2	málo
bojovný	bojovný	k2eAgInSc1d1	bojovný
<g/>
,	,	kIx,	,
něž	jenž	k3xRgFnPc4	jenž
většina	většina	k1gFnSc1	většina
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tygr	tygr	k1gMnSc1	tygr
normálně	normálně	k6eAd1	normálně
loví	lovit	k5eAaImIp3nS	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
levharta	levhart	k1gMnSc2	levhart
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
zavítá	zavítat	k5eAaPmIp3nS	zavítat
do	do	k7c2	do
lidských	lidský	k2eAgNnPc2d1	lidské
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
,	,	kIx,	,
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
lidi	člověk	k1gMnPc4	člověk
mimo	mimo	k7c4	mimo
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dřevorubce	dřevorubec	k1gMnPc4	dřevorubec
či	či	k8xC	či
sběrače	sběrač	k1gMnPc4	sběrač
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
síle	síla	k1gFnSc3	síla
nemá	mít	k5eNaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgMnPc4	žádný
přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Smečka	smečka	k1gFnSc1	smečka
dhoulů	dhoul	k1gMnPc2	dhoul
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgMnSc4d1	schopen
tygra	tygr	k1gMnSc4	tygr
skolit	skolit	k5eAaPmF	skolit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
týká	týkat	k5eAaImIp3nS	týkat
pouze	pouze	k6eAd1	pouze
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
starých	starý	k2eAgMnPc2d1	starý
a	a	k8xC	a
nemocných	mocný	k2eNgMnPc2d1	nemocný
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
skutečného	skutečný	k2eAgMnSc2d1	skutečný
protivníka	protivník	k1gMnSc2	protivník
nelze	lze	k6eNd1	lze
dhouly	dhoul	k1gInPc4	dhoul
považovat	považovat	k5eAaImF	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
tygrovi	tygrův	k2eAgMnPc1d1	tygrův
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
a	a	k8xC	a
nedospělí	dospělý	k2eNgMnPc1d1	nedospělý
tygři	tygr	k1gMnPc1	tygr
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
stávají	stávat	k5eAaImIp3nP	stávat
kořistí	kořist	k1gFnSc7	kořist
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
tygr	tygr	k1gMnSc1	tygr
obvykle	obvykle	k6eAd1	obvykle
uhne	uhnout	k5eAaPmIp3nS	uhnout
medvědovi	medvěd	k1gMnSc3	medvěd
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
potenciální	potenciální	k2eAgMnSc1d1	potenciální
nepřítel	nepřítel	k1gMnSc1	nepřítel
ještě	ještě	k6eAd1	ještě
připadá	připadat	k5eAaImIp3nS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
podobné	podobný	k2eAgFnPc4d1	podobná
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
areály	areál	k1gInPc1	areál
výskytu	výskyt	k1gInSc2	výskyt
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
nepřekrývají	překrývat	k5eNaImIp3nP	překrývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
nekonkurují	konkurovat	k5eNaImIp3nP	konkurovat
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
šelma	šelma	k1gFnSc1	šelma
také	také	k9	také
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
jiný	jiný	k2eAgInSc1d1	jiný
biotop	biotop	k1gInSc1	biotop
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc1	přednost
otevřenému	otevřený	k2eAgNnSc3d1	otevřené
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tygři	tygr	k1gMnPc1	tygr
trpí	trpět	k5eAaImIp3nP	trpět
parazitárními	parazitární	k2eAgNnPc7d1	parazitární
onemocněními	onemocnění	k1gNnPc7	onemocnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemoci	nemoc	k1gFnPc4	nemoc
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
nebyly	být	k5eNaImAgFnP	být
doposud	doposud	k6eAd1	doposud
důkladněji	důkladně	k6eAd2	důkladně
zkoumány	zkoumat	k5eAaImNgInP	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
a	a	k8xC	a
africkém	africký	k2eAgInSc6d1	africký
kulturním	kulturní	k2eAgInSc6d1	kulturní
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
králem	král	k1gMnSc7	král
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
kulturách	kultura	k1gFnPc6	kultura
přebírá	přebírat	k5eAaImIp3nS	přebírat
toto	tento	k3xDgNnSc4	tento
postavení	postavení	k1gNnSc4	postavení
tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mu	on	k3xPp3gMnSc3	on
přívlastky	přívlastek	k1gInPc1	přívlastek
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
pán	pán	k1gMnSc1	pán
džungle	džungle	k1gFnSc2	džungle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
car	car	k1gMnSc1	car
tajgy	tajga	k1gFnSc2	tajga
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
vládce	vládce	k1gMnSc1	vládce
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
východních	východní	k2eAgFnPc6d1	východní
kulturách	kultura	k1gFnPc6	kultura
měl	mít	k5eAaImAgMnS	mít
tygr	tygr	k1gMnSc1	tygr
dokonce	dokonce	k9	dokonce
božské	božská	k1gFnPc4	božská
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
kulturách	kultura	k1gFnPc6	kultura
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
obvykle	obvykle	k6eAd1	obvykle
líčen	líčen	k2eAgMnSc1d1	líčen
jako	jako	k8xC	jako
krvelačné	krvelačný	k2eAgNnSc1d1	krvelačné
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vznešené	vznešený	k2eAgFnSc3d1	vznešená
kráse	krása	k1gFnSc3	krása
a	a	k8xC	a
eleganci	elegance	k1gFnSc3	elegance
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
a	a	k8xC	a
nejobdivovanějších	obdivovaný	k2eAgNnPc2d3	nejobdivovanější
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
symbol	symbol	k1gInSc1	symbol
divočiny	divočina	k1gFnSc2	divočina
má	mít	k5eAaImIp3nS	mít
sympatie	sympatie	k1gFnPc4	sympatie
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
jeho	jeho	k3xOp3gFnSc3	jeho
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
médií	médium	k1gNnPc2	médium
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
záchranným	záchranný	k2eAgInPc3d1	záchranný
projektům	projekt	k1gInPc3	projekt
získávat	získávat	k5eAaImF	získávat
podporu	podpora	k1gFnSc4	podpora
i	i	k8xC	i
přijetí	přijetí	k1gNnSc4	přijetí
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
popularity	popularita	k1gFnSc2	popularita
tygrů	tygr	k1gMnPc2	tygr
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
následně	následně	k6eAd1	následně
profitovat	profitovat	k5eAaBmF	profitovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
biotopech	biotop	k1gInPc6	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
mohutnost	mohutnost	k1gFnSc1	mohutnost
a	a	k8xC	a
lovecké	lovecký	k2eAgFnPc1d1	lovecká
schopnosti	schopnost	k1gFnPc1	schopnost
tygra	tygr	k1gMnSc2	tygr
udivovaly	udivovat	k5eAaImAgFnP	udivovat
člověka	člověk	k1gMnSc4	člověk
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
známá	známý	k2eAgNnPc1d1	známé
vyobrazení	vyobrazení	k1gNnPc1	vyobrazení
tygrů	tygr	k1gMnPc2	tygr
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Indu	Indus	k1gInSc2	Indus
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
5	[number]	k4	5
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
výrazně	výrazně	k6eAd1	výrazně
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
než	než	k8xS	než
nejstarší	starý	k2eAgNnSc4d3	nejstarší
zobrazení	zobrazení	k1gNnSc4	zobrazení
lvů	lev	k1gInPc2	lev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
30	[number]	k4	30
000	[number]	k4	000
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hinduismu	hinduismus	k1gInSc6	hinduismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
árijských	árijský	k2eAgInPc2d1	árijský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tygr	tygr	k1gMnSc1	tygr
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
bohyně	bohyně	k1gFnSc1	bohyně
Durga	Durga	k1gFnSc1	Durga
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
tygrovi	tygr	k1gMnSc6	tygr
<g/>
,	,	kIx,	,
Šiva	Šivum	k1gNnPc4	Šivum
zase	zase	k9	zase
sedává	sedávat	k5eAaImIp3nS	sedávat
na	na	k7c4	na
tygří	tygří	k2eAgFnSc4d1	tygří
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
si	se	k3xPyFc3	se
tygr	tygr	k1gMnSc1	tygr
našel	najít	k5eAaPmAgMnS	najít
svojí	svojit	k5eAaImIp3nS	svojit
úlohu	úloha	k1gFnSc4	úloha
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zobrazení	zobrazení	k1gNnSc4	zobrazení
zdobí	zdobit	k5eAaImIp3nP	zdobit
různé	různý	k2eAgInPc1d1	různý
chrámy	chrám	k1gInPc1	chrám
a	a	k8xC	a
svatyně	svatyně	k1gFnPc1	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tygr	tygr	k1gMnSc1	tygr
podobné	podobný	k2eAgNnSc4d1	podobné
postavení	postavení	k1gNnSc4	postavení
jako	jako	k8xS	jako
lev	lev	k1gInSc4	lev
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
protoindických	protoindický	k2eAgFnPc2d1	protoindická
maleb	malba	k1gFnPc2	malba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
000	[number]	k4	000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
je	být	k5eAaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
bojující	bojující	k2eAgMnSc1d1	bojující
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
tygry	tygr	k1gMnPc7	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jistá	jistý	k2eAgFnSc1d1	jistá
podobnost	podobnost	k1gFnSc1	podobnost
se	s	k7c7	s
sumerským	sumerský	k2eAgMnSc7d1	sumerský
Gilgamešem	Gilgameš	k1gMnSc7	Gilgameš
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
i	i	k9	i
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
skytských	skytský	k2eAgInPc2d1	skytský
národů	národ	k1gInPc2	národ
obývajících	obývající	k2eAgInPc2d1	obývající
prostory	prostor	k1gInPc7	prostor
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1000	[number]	k4	1000
a	a	k8xC	a
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
mezopotámských	mezopotámský	k2eAgInPc2d1	mezopotámský
a	a	k8xC	a
maloasijských	maloasijský	k2eAgInPc2d1	maloasijský
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
tygr	tygr	k1gMnSc1	tygr
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staroíránském	staroíránský	k2eAgNnSc6d1	staroíránský
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
vyobrazován	vyobrazován	k2eAgMnSc1d1	vyobrazován
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
starověké	starověký	k2eAgNnSc4d1	starověké
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
tygra	tygr	k1gMnSc4	tygr
až	až	k9	až
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
letech	let	k1gInPc6	let
330-325	[number]	k4	330-325
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
první	první	k4xOgMnSc1	první
tygr	tygr	k1gMnSc1	tygr
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dar	dar	k1gInSc1	dar
vládce	vládce	k1gMnSc2	vládce
Seleuka	Seleuk	k1gMnSc2	Seleuk
I.	I.	kA	I.
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
ještě	ještě	k6eAd1	ještě
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
západní	západní	k2eAgFnSc3d1	západní
kultuře	kultura	k1gFnSc3	kultura
bližší	blízký	k2eAgMnSc1d2	bližší
než	než	k8xS	než
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
byli	být	k5eAaImAgMnP	být
tygři	tygr	k1gMnPc1	tygr
využíváni	využívat	k5eAaImNgMnP	využívat
k	k	k7c3	k
boji	boj	k1gInSc3	boj
v	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
tygr	tygr	k1gMnSc1	tygr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byl	být	k5eAaImAgMnS	být
darem	dar	k1gInSc7	dar
císaři	císař	k1gMnSc3	císař
Augustovi	August	k1gMnSc3	August
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
19	[number]	k4	19
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
císaře	císař	k1gMnSc2	císař
Heliogabala	Heliogabal	k1gMnSc2	Heliogabal
bylo	být	k5eAaImAgNnS	být
předvedeno	předvést	k5eAaPmNgNnS	předvést
a	a	k8xC	a
zabito	zabít	k5eAaPmNgNnS	zabít
celkem	celkem	k6eAd1	celkem
51	[number]	k4	51
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
císaři	císař	k1gMnSc3	císař
se	se	k3xPyFc4	se
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
přestrojen	přestrojit	k5eAaPmNgMnS	přestrojit
za	za	k7c4	za
boha	bůh	k1gMnSc4	bůh
Bakcha	bakchus	k1gMnSc4	bakchus
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
svůj	svůj	k3xOyFgInSc4	svůj
vůz	vůz	k1gInSc4	vůz
táhnout	táhnout	k5eAaImF	táhnout
tygry	tygr	k1gMnPc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
se	se	k3xPyFc4	se
však	však	k9	však
tygři	tygr	k1gMnPc1	tygr
objevovali	objevovat	k5eAaImAgMnP	objevovat
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
než	než	k8xS	než
lvi	lev	k1gMnPc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
jej	on	k3xPp3gInSc4	on
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
,	,	kIx,	,
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
tygr	tygr	k1gMnSc1	tygr
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc4	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
objevil	objevit	k5eAaPmAgMnS	objevit
až	až	k6eAd1	až
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
výpravě	výprava	k1gFnSc6	výprava
do	do	k7c2	do
Činy	čina	k1gFnSc2	čina
<g/>
.	.	kIx.	.
</s>
<s>
Tygry	tygr	k1gMnPc4	tygr
spatřil	spatřit	k5eAaPmAgMnS	spatřit
na	na	k7c6	na
Kublajchánově	Kublajchánův	k2eAgInSc6d1	Kublajchánův
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
je	být	k5eAaImIp3nS	být
však	však	k9	však
jako	jako	k9	jako
lvy	lev	k1gInPc1	lev
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
babylonské	babylonský	k2eAgInPc1d1	babylonský
<g/>
,	,	kIx,	,
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
<g/>
,	,	kIx,	,
bílými	bílý	k2eAgInPc7d1	bílý
a	a	k8xC	a
červenými	červený	k2eAgInPc7d1	červený
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
tygr	tygr	k1gMnSc1	tygr
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tygr	tygr	k1gMnSc1	tygr
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Savojské	savojský	k2eAgFnSc2d1	Savojská
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
dovezen	dovezen	k2eAgInSc1d1	dovezen
roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
tygři	tygr	k1gMnPc1	tygr
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
šlechtických	šlechtický	k2eAgInPc6d1	šlechtický
dvorech	dvůr	k1gInPc6	dvůr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
tygry	tygr	k1gMnPc7	tygr
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
"	"	kIx"	"
<g/>
Šér	Šér	k1gFnSc1	Šér
Chán	chán	k1gMnSc1	chán
<g/>
"	"	kIx"	"
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Rudyarda	Rudyard	k1gMnSc2	Rudyard
Kiplinga	Kipling	k1gMnSc2	Kipling
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
z	z	k7c2	z
Medvídka	medvídek	k1gMnSc2	medvídek
Pú	Pú	k1gFnSc4	Pú
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
A.	A.	kA	A.
A.	A.	kA	A.
Milneho	Milne	k1gMnSc2	Milne
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
tygří	tygří	k2eAgFnSc6d1	tygří
kůži	kůže	k1gFnSc6	kůže
od	od	k7c2	od
Šoty	šot	k1gInPc1	šot
Rustaveliho	Rustaveliha	k1gFnSc5	Rustaveliha
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
eposem	epos	k1gInSc7	epos
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
Williama	William	k1gMnSc2	William
Blaka	Blaek	k1gMnSc2	Blaek
"	"	kIx"	"
<g/>
Tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
básní	báseň	k1gFnPc2	báseň
anglického	anglický	k2eAgInSc2d1	anglický
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgMnS	získat
Yann	Yann	k1gMnSc1	Yann
Martel	Martel	k1gMnSc1	Martel
za	za	k7c4	za
román	román	k1gInSc4	román
Pí	pí	k1gNnSc2	pí
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tygr	tygr	k1gMnSc1	tygr
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
Bookerovu	Bookerův	k2eAgFnSc4d1	Bookerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
tygr	tygr	k1gMnSc1	tygr
symbolem	symbol	k1gInSc7	symbol
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
podzimu	podzim	k1gInSc2	podzim
a	a	k8xC	a
udatnosti	udatnost	k1gFnSc2	udatnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetím	třetí	k4xOgNnSc7	třetí
znamením	znamení	k1gNnSc7	znamení
čínského	čínský	k2eAgInSc2d1	čínský
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
12	[number]	k4	12
<g/>
.	.	kIx.	.
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
kultuře	kultura	k1gFnSc6	kultura
rokem	rok	k1gInSc7	rok
tygra	tygr	k1gMnSc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
něž	jenž	k3xRgFnPc4	jenž
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
má	mít	k5eAaImIp3nS	mít
tygr	tygr	k1gMnSc1	tygr
jakožto	jakožto	k8xS	jakožto
symbol	symbol	k1gInSc1	symbol
síly	síla	k1gFnSc2	síla
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
čínské	čínský	k2eAgFnSc6d1	čínská
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Tygří	tygří	k2eAgInPc1d1	tygří
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zpracované	zpracovaný	k2eAgInPc1d1	zpracovaný
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
prášku	prášek	k1gInSc2	prášek
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
proti	proti	k7c3	proti
neduhům	neduh	k1gInPc3	neduh
jako	jako	k9	jako
revma	revma	k1gNnSc1	revma
či	či	k8xC	či
impotence	impotence	k1gFnSc1	impotence
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
produktech	produkt	k1gInPc6	produkt
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
nabízené	nabízený	k2eAgFnPc1d1	nabízená
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgInPc4d1	značný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
výrazně	výrazně	k6eAd1	výrazně
podporuje	podporovat	k5eAaImIp3nS	podporovat
nelegální	legální	k2eNgInSc1d1	nelegální
lov	lov	k1gInSc1	lov
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
pytláctví	pytláctví	k1gNnSc1	pytláctví
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
ohrožení	ohrožení	k1gNnSc2	ohrožení
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
zvolila	zvolit	k5eAaPmAgFnS	zvolit
tygra	tygr	k1gMnSc4	tygr
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
XXIV	XXIV	kA	XXIV
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
také	také	k9	také
zdobí	zdobit	k5eAaImIp3nS	zdobit
několik	několik	k4yIc4	několik
státních	státní	k2eAgInPc2d1	státní
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc4d1	silné
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
ekonomiky	ekonomika	k1gFnPc1	ekonomika
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
přízviskem	přízvisko	k1gNnSc7	přízvisko
"	"	kIx"	"
<g/>
ekonomičtí	ekonomický	k2eAgMnPc1d1	ekonomický
tygři	tygr	k1gMnPc1	tygr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
stavy	stav	k1gInPc1	stav
divokých	divoký	k2eAgMnPc2d1	divoký
tygrů	tygr	k1gMnPc2	tygr
stále	stále	k6eAd1	stále
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
chováno	chován	k2eAgNnSc1d1	chováno
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
těchto	tento	k3xDgMnPc2	tento
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
až	až	k9	až
na	na	k7c4	na
11	[number]	k4	11
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
000	[number]	k4	000
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
zoologické	zoologický	k2eAgFnPc4d1	zoologická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
různých	různý	k2eAgInPc2d1	různý
soukromých	soukromý	k2eAgInPc2d1	soukromý
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
až	až	k9	až
o	o	k7c4	o
5	[number]	k4	5
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
000	[number]	k4	000
pak	pak	k6eAd1	pak
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
tygřím	tygří	k2eAgInSc6d1	tygří
parku	park	k1gInSc6	park
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
Charbinu	Charbin	k2eAgNnSc3d1	Charbin
je	být	k5eAaImIp3nS	být
chováno	chován	k2eAgNnSc1d1	chováno
kolem	kolem	k7c2	kolem
800	[number]	k4	800
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tiger	Tigra	k1gFnPc2	Tigra
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
Г	Г	k?	Г
<g/>
,	,	kIx,	,
В	В	k?	В
Г	Г	k?	Г
<g/>
;	;	kIx,	;
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Н	Н	k?	Н
П	П	k?	П
<g/>
.	.	kIx.	.
М	М	k?	М
С	С	k?	С
С	С	k?	С
<g/>
.	.	kIx.	.
Х	Х	k?	Х
(	(	kIx(	(
<g/>
г	г	k?	г
и	и	k?	и
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
В	В	k?	В
ш	ш	k?	ш
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
552	[number]	k4	552
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Т	Т	k?	Т
<g/>
,	,	kIx,	,
s.	s.	k?	s.
83	[number]	k4	83
<g/>
–	–	k?	–
<g/>
158	[number]	k4	158
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
JACKSON	JACKSON	kA	JACKSON
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
NOWELL	NOWELL	kA	NOWELL
<g/>
,	,	kIx,	,
Kristin	Kristin	k2eAgInSc1d1	Kristin
<g/>
.	.	kIx.	.
</s>
<s>
Wild	Wild	k1gInSc1	Wild
cats	cats	k1gInSc1	cats
<g/>
:	:	kIx,	:
status	status	k1gInSc1	status
survey	survea	k1gFnSc2	survea
and	and	k?	and
conservation	conservation	k1gInSc1	conservation
action	action	k1gInSc1	action
plan	plan	k1gInSc1	plan
<g/>
.	.	kIx.	.
</s>
<s>
Gland	Gland	k1gInSc1	Gland
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
:	:	kIx,	:
IUCN	IUCN	kA	IUCN
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
xxiv	xxiva	k1gFnPc2	xxiva
+	+	kIx~	+
382	[number]	k4	382
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
8317	[number]	k4	8317
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Tiger	Tigra	k1gFnPc2	Tigra
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
,	,	kIx,	,
s.	s.	k?	s.
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
MAZÁK	mazák	k1gMnSc1	mazák
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
kočky	kočka	k1gFnPc1	kočka
a	a	k8xC	a
gepardi	gepard	k1gMnPc1	gepard
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
192	[number]	k4	192
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
MAZÁK	mazák	k1gMnSc1	mazák
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Tiger	Tiger	k1gMnSc1	Tiger
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Wittenberg	Wittenberg	k1gMnSc1	Wittenberg
Lutherstadt	Lutherstadt	k1gMnSc1	Lutherstadt
:	:	kIx,	:
A.	A.	kA	A.
Ziemsen	Ziemsen	k1gInSc1	Ziemsen
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
228	[number]	k4	228
s.	s.	k?	s.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
MAZÁK	mazák	k1gMnSc1	mazák
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
.	.	kIx.	.
</s>
<s>
Mammalian	Mammalian	k1gInSc1	Mammalian
Species	species	k1gFnSc2	species
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
152	[number]	k4	152
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SCHALLER	SCHALLER	kA	SCHALLER
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
B.	B.	kA	B.
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
deer	deer	k1gInSc1	deer
and	and	k?	and
the	the	k?	the
tiger	tiger	k1gInSc1	tiger
<g/>
:	:	kIx,	:
a	a	k8xC	a
study	stud	k1gInPc1	stud
of	of	k?	of
wildlife	wildlif	k1gInSc5	wildlif
in	in	k?	in
India	indium	k1gNnSc2	indium
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
xv	xv	k?	xv
+	+	kIx~	+
370	[number]	k4	370
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
226	[number]	k4	226
<g/>
-	-	kIx~	-
<g/>
73631	[number]	k4	73631
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SEIDENSTICKER	SEIDENSTICKER	kA	SEIDENSTICKER
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
;	;	kIx,	;
JACKSON	JACKSON	kA	JACKSON
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
CHRISTIE	CHRISTIE	kA	CHRISTIE
<g/>
,	,	kIx,	,
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
.	.	kIx.	.
</s>
<s>
Riding	Riding	k1gInSc1	Riding
the	the	k?	the
tiger	tiger	k1gInSc1	tiger
<g/>
:	:	kIx,	:
tiger	tiger	k1gInSc1	tiger
conservation	conservation	k1gInSc1	conservation
in	in	k?	in
human-dominated	humanominated	k1gMnSc1	human-dominated
landscapes	landscapes	k1gMnSc1	landscapes
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
xix	xix	k?	xix
+	+	kIx~	+
383	[number]	k4	383
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
164057	[number]	k4	164057
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
;	;	kIx,	;
SUNQUIST	SUNQUIST	kA	SUNQUIST
<g/>
,	,	kIx,	,
Fiona	Fiono	k1gNnSc2	Fiono
<g/>
.	.	kIx.	.
</s>
<s>
Wild	Wild	k6eAd1	Wild
Cats	Cats	k1gInSc1	Cats
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
:	:	kIx,	:
University	universita	k1gFnSc2	universita
Of	Of	k1gFnSc2	Of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
462	[number]	k4	462
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
226779997	[number]	k4	226779997
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Tiger	Tigra	k1gFnPc2	Tigra
<g/>
,	,	kIx,	,
s.	s.	k?	s.
343	[number]	k4	343
<g/>
–	–	k?	–
<g/>
372	[number]	k4	372
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
TILSON	TILSON	kA	TILSON
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
L.	L.	kA	L.
<g/>
;	;	kIx,	;
NYHUS	NYHUS	kA	NYHUS
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
J.	J.	kA	J.
<g/>
.	.	kIx.	.
</s>
<s>
Tigers	Tigers	k1gInSc1	Tigers
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
:	:	kIx,	:
the	the	k?	the
science	science	k1gFnSc1	science
<g/>
,	,	kIx,	,
politics	politics	k1gInSc1	politics
<g/>
,	,	kIx,	,
and	and	k?	and
conservation	conservation	k1gInSc1	conservation
of	of	k?	of
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
:	:	kIx,	:
Elsevier	Elsevier	k1gInSc1	Elsevier
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
xxi	xxi	k?	xxi
+	+	kIx~	+
524	[number]	k4	524
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8155	[number]	k4	8155
<g/>
-	-	kIx~	-
<g/>
1570	[number]	k4	1570
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CORBETT	CORBETT	kA	CORBETT
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Chrámový	chrámový	k2eAgMnSc1d1	chrámový
tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Miroslav	Miroslav	k1gMnSc1	Miroslav
Jindra	Jindra	k1gMnSc1	Jindra
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Volary	Volar	k1gInPc1	Volar
:	:	kIx,	:
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
143	[number]	k4	143
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86913	[number]	k4	86913
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
CORBETT	CORBETT	kA	CORBETT
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Tygři	tygr	k1gMnPc1	tygr
z	z	k7c2	z
Kumáonu	Kumáon	k1gInSc2	Kumáon
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Milan	Milan	k1gMnSc1	Milan
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
227	[number]	k4	227
s.	s.	k?	s.
KIPLING	KIPLING	kA	KIPLING
<g/>
,	,	kIx,	,
Rudyard	Rudyard	k1gInSc1	Rudyard
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnSc1	ilustrace
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
277	[number]	k4	277
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4342	[number]	k4	4342
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
MARTEL	MARTEL	kA	MARTEL
<g/>
,	,	kIx,	,
Yann	Yann	k1gInSc1	Yann
<g/>
.	.	kIx.	.
</s>
<s>
Pí	pí	k1gNnSc1	pí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Lucie	Lucie	k1gFnSc2	Lucie
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Mikolajkovi	Mikolajek	k1gMnSc3	Mikolajek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
350	[number]	k4	350
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
570	[number]	k4	570
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
RUSTAVELI	RUSTAVELI	kA	RUSTAVELI
<g/>
,	,	kIx,	,
Šota	Šotum	k1gNnSc2	Šotum
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
tygří	tygří	k2eAgFnSc6d1	tygří
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
622	[number]	k4	622
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tygr	tygr	k1gMnSc1	tygr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Panthera	Panther	k1gMnSc4	Panther
tigris	tigris	k1gFnSc2	tigris
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Zpráva	zpráva	k1gFnSc1	zpráva
Světoví	světový	k2eAgMnPc1d1	světový
politici	politik	k1gMnPc1	politik
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
záchraně	záchrana	k1gFnSc6	záchrana
tygrů	tygr	k1gMnPc2	tygr
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Téma	téma	k1gNnSc1	téma
Tygr	tygr	k1gMnSc1	tygr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Tygr	tygr	k1gMnSc1	tygr
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IUCN	IUCN	kA	IUCN
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
st	st	kA	st
Century	Centura	k1gFnSc2	Centura
Tiger	Tiger	k1gInSc1	Tiger
-	-	kIx~	-
projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
tygrů	tygr	k1gMnPc2	tygr
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Save	Save	k1gFnSc1	Save
Tigers	Tigers	k1gInSc1	Tigers
Now	Now	k1gFnSc1	Now
-	-	kIx~	-
projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
tygrů	tygr	k1gMnPc2	tygr
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
WWF	WWF	kA	WWF
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
