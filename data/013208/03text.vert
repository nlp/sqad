<p>
<s>
The	The	k?	The
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Association	Association	k1gInSc4	Association
of	of	k?	of
America	Americ	k1gInSc2	Americ
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
MPAA	MPAA	kA	MPAA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
obchodní	obchodní	k2eAgFnSc1d1	obchodní
organizace	organizace	k1gFnSc1	organizace
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
jako	jako	k8xC	jako
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Producers	Producers	k1gInSc4	Producers
and	and	k?	and
Distributors	Distributors	k1gInSc1	Distributors
of	of	k?	of
America	America	k1gFnSc1	America
(	(	kIx(	(
<g/>
MPPDA	MPPDA	kA	MPPDA
<g/>
)	)	kIx)	)
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zájmů	zájem	k1gInPc2	zájem
filmových	filmový	k2eAgFnPc2d1	filmová
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
šestka	šestka	k1gFnSc1	šestka
hlavních	hlavní	k2eAgFnPc2d1	hlavní
hollywodských	hollywodský	k2eAgFnPc2d1	hollywodská
továren	továrna	k1gFnPc2	továrna
na	na	k7c4	na
sny	sen	k1gInPc4	sen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Buena	Bueen	k2eAgFnSc1d1	Buena
Vista	vista	k2eAgFnSc1d1	vista
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
Pictures	Pictures	k1gMnSc1	Pictures
</s>
</p>
<p>
<s>
Paramount	Paramount	k1gMnSc1	Paramount
Pictures	Pictures	k1gMnSc1	Pictures
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
</s>
</p>
<p>
<s>
Universal	Universat	k5eAaImAgMnS	Universat
Studios	Studios	k?	Studios
</s>
</p>
<p>
<s>
Warner	Warner	k1gInSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
<g/>
Organizace	organizace	k1gFnSc1	organizace
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
systému	systém	k1gInSc3	systém
hodnocení	hodnocení	k1gNnSc1	hodnocení
přístupnosti	přístupnost	k1gFnSc2	přístupnost
filmů	film	k1gInPc2	film
nebo	nebo	k8xC	nebo
aktivitám	aktivita	k1gFnPc3	aktivita
proti	proti	k7c3	proti
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
šíření	šíření	k1gNnSc3	šíření
audiovizuálních	audiovizuální	k2eAgInPc2d1	audiovizuální
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
MPAA	MPAA	kA	MPAA
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
podařilo	podařit	k5eAaPmAgNnS	podařit
hacknout	hacknout	k5eAaImF	hacknout
a	a	k8xC	a
zobrazit	zobrazit	k5eAaPmF	zobrazit
zde	zde	k6eAd1	zde
velkou	velký	k2eAgFnSc4d1	velká
nabídku	nabídka	k1gFnSc4	nabídka
torrentů	torrent	k1gInPc2	torrent
z	z	k7c2	z
The	The	k1gMnSc5	The
Pirate	Pirat	k1gMnSc5	Pirat
Bay	Bay	k1gMnSc5	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
asociace	asociace	k1gFnSc1	asociace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
boji	boj	k1gInSc3	boj
právě	právě	k9	právě
proti	proti	k7c3	proti
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc3d1	populární
webové	webový	k2eAgFnSc3d1	webová
stránce	stránka	k1gFnSc3	stránka
The	The	k1gMnSc5	The
Pirate	Pirat	k1gMnSc5	Pirat
Bay	Bay	k1gMnSc5	Bay
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
indexuje	indexovat	k5eAaImIp3nS	indexovat
BitTorrenty	BitTorrent	k1gInPc4	BitTorrent
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
využita	využít	k5eAaPmNgFnS	využít
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
webové	webový	k2eAgMnPc4d1	webový
vyhledávače	vyhledávač	k1gMnPc4	vyhledávač
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
torrentů	torrent	k1gInPc2	torrent
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
samotného	samotný	k2eAgInSc2d1	samotný
nelegálního	legální	k2eNgInSc2d1	nelegální
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
filmů	film	k1gInPc2	film
dle	dle	k7c2	dle
MPAA	MPAA	kA	MPAA
==	==	k?	==
</s>
</p>
<p>
<s>
G	G	kA	G
–	–	k?	–
General	General	k1gMnSc1	General
Audiences	Audiences	k1gMnSc1	Audiences
–	–	k?	–
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
<g/>
,	,	kIx,	,
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
<g/>
PG	PG	kA	PG
–	–	k?	–
Parental	Parental	k1gMnSc1	Parental
guidance	guidance	k1gFnSc2	guidance
suggested	suggested	k1gMnSc1	suggested
–	–	k?	–
některé	některý	k3yIgFnPc4	některý
scény	scéna	k1gFnPc4	scéna
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Shrek	Shrek	k6eAd1	Shrek
<g/>
,	,	kIx,	,
Harry	Harro	k1gNnPc7	Harro
Potter	Potter	k1gInSc1	Potter
<g/>
,	,	kIx,	,
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
<g/>
PG-	PG-	k1gFnSc1	PG-
<g/>
13	[number]	k4	13
–	–	k?	–
Parents	Parents	k1gInSc1	Parents
strongly	strongla	k1gFnSc2	strongla
cautioned	cautioned	k1gMnSc1	cautioned
–	–	k?	–
některé	některý	k3yIgFnPc1	některý
scény	scéna	k1gFnPc1	scéna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
pod	pod	k7c4	pod
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
Spider	Spider	k1gMnSc1	Spider
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
český	český	k2eAgMnSc1d1	český
Kolja	Kolja	k1gMnSc1	Kolja
<g/>
.	.	kIx.	.
<g/>
R	R	kA	R
–	–	k?	–
Restricted	Restricted	k1gInSc1	Restricted
–	–	k?	–
Mladiství	mladiství	k1gNnSc2	mladiství
pod	pod	k7c4	pod
17	[number]	k4	17
let	léto	k1gNnPc2	léto
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
rodičů	rodič	k1gMnPc2	rodič
či	či	k8xC	či
dospělého	dospělý	k1gMnSc4	dospělý
poručníka	poručník	k1gMnSc4	poručník
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Umučení	umučení	k1gNnSc1	umučení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
Matrix	Matrix	k1gInSc1	Matrix
Reloaded	Reloaded	k1gInSc1	Reloaded
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
:	:	kIx,	:
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Thermopyl	Thermopyly	k1gFnPc2	Thermopyly
<g/>
.	.	kIx.	.
<g/>
NC-	NC-	k1gFnSc1	NC-
<g/>
17	[number]	k4	17
–	–	k?	–
Pod	pod	k7c4	pod
17	[number]	k4	17
let	léto	k1gNnPc2	léto
nepřístupné	přístupný	k2eNgFnPc1d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Showgirls	Showgirls	k1gInSc1	Showgirls
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
&	&	k?	&
June	jun	k1gMnSc5	jun
<g/>
,	,	kIx,	,
Snílci	snílek	k1gMnPc1	snílek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Association	Association	k1gInSc4	Association
of	of	k?	of
America	Americum	k1gNnSc2	Americum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
MPAA	MPAA	kA	MPAA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Protipirátská	protipirátský	k2eAgFnSc1d1	protipirátská
asociace	asociace	k1gFnSc1	asociace
nabízela	nabízet	k5eAaImAgFnS	nabízet
torrenty	torrent	k1gInPc4	torrent
</s>
</p>
