<s>
The	The	k?	The
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Association	Association	k1gInSc4	Association
of	of	k?	of
America	Americ	k1gInSc2	Americ
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
MPAA	MPAA	kA	MPAA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
obchodní	obchodní	k2eAgFnSc1d1	obchodní
organizace	organizace	k1gFnSc1	organizace
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
jako	jako	k8xC	jako
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Producers	Producers	k1gInSc4	Producers
and	and	k?	and
Distributors	Distributors	k1gInSc1	Distributors
of	of	k?	of
America	America	k1gFnSc1	America
(	(	kIx(	(
<g/>
MPPDA	MPPDA	kA	MPPDA
<g/>
)	)	kIx)	)
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zájmů	zájem	k1gInPc2	zájem
filmových	filmový	k2eAgFnPc2d1	filmová
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
