<s>
Kalorie	kalorie	k1gFnSc1
</s>
<s>
Nutriční	nutriční	k2eAgInPc1d1
údaje	údaj	k1gInPc1
na	na	k7c6
balíčku	balíček	k1gInSc6
rýže	rýže	k1gFnSc2
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
</s>
<s>
Kalorie	kalorie	k1gFnSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
cal	cal	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
již	již	k6eAd1
většinou	většinou	k6eAd1
nahrazená	nahrazený	k2eAgFnSc1d1
joulem	joule	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
jednotkou	jednotka	k1gFnSc7
soustavy	soustava	k1gFnSc2
SI	SI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalorie	kalorie	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
tzv.	tzv.	kA
zakázané	zakázaný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
hlavně	hlavně	k9
při	při	k7c6
vyjadřování	vyjadřování	k1gNnSc6
energetické	energetický	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
potravin	potravina	k1gFnPc2
(	(	kIx(
<g/>
kde	kde	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
obvykle	obvykle	k6eAd1
používá	používat	k5eAaImIp3nS
její	její	k3xOp3gInSc4
násobek	násobek	k1gInSc4
kilokalorie	kilokalorie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovalo	existovat	k5eAaImAgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgFnPc2d1
definic	definice	k1gFnPc2
kalorie	kalorie	k1gFnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
následující	následující	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
Kalorie	kalorie	k1gFnSc1
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc4
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dokáže	dokázat	k5eAaPmIp3nS
zvýšit	zvýšit	k5eAaPmF
teplotu	teplota	k1gFnSc4
1	#num#	k4
gramu	gram	k1gInSc2
vody	voda	k1gFnSc2
ze	z	k7c2
14,5	14,5	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c4
15,5	15,5	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Jelikož	jelikož	k8xS
měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
4185	#num#	k4
J	J	kA
<g/>
·	·	k?
<g/>
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
·	·	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
1	#num#	k4
cal	cal	k?
≈	≈	k?
4,185	4,185	k4
J	J	kA
</s>
<s>
Důvodem	důvod	k1gInSc7
uvedení	uvedení	k1gNnSc2
přesné	přesný	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
v	v	k7c6
definici	definice	k1gFnSc6
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
teplotě	teplota	k1gFnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
bez	bez	k7c2
udání	udání	k1gNnSc2
počáteční	počáteční	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
by	by	kYmCp3nP
jednotka	jednotka	k1gFnSc1
nebyla	být	k5eNaImAgFnS
přesně	přesně	k6eAd1
určena	určit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc4d1
kalorii	kalorie	k1gFnSc4
definoval	definovat	k5eAaBmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Henri	Henri	k1gNnSc2
Victor	Victor	k1gMnSc1
Regnault	Regnault	k1gMnSc1
jako	jako	k8xS,k8xC
množství	množství	k1gNnSc1
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
potřebného	potřebný	k2eAgNnSc2d1
k	k	k7c3
ohřátí	ohřátý	k2eAgMnPc1d1
1	#num#	k4
g	g	kA
vody	voda	k1gFnSc2
z	z	k7c2
0	#num#	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c4
1	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
střední	střední	k2eAgFnSc1d1
kalorie	kalorie	k1gFnSc1
jako	jako	k8xS,k8xC
setina	setina	k1gFnSc1
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
potřebného	potřebný	k2eAgNnSc2d1
k	k	k7c3
ohřátí	ohřátý	k2eAgMnPc1d1
gramu	gram	k1gInSc2
vody	voda	k1gFnSc2
z	z	k7c2
0	#num#	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c4
100	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
definice	definice	k1gFnSc1
uvedená	uvedený	k2eAgFnSc1d1
v	v	k7c6
úvodu	úvod	k1gInSc6
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgMnSc1d1
skoro	skoro	k6eAd1
přesně	přesně	k6eAd1
střední	střední	k2eAgFnSc6d1
kalorii	kalorie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
Regnaultova	Regnaultův	k2eAgFnSc1d1
kalorie	kalorie	k1gFnSc1
byla	být	k5eAaImAgFnS
1,008	1,008	k4
x	x	k?
větší	veliký	k2eAgFnSc2d2
než	než	k8xS
střední	střední	k2eAgFnSc2d1
kalorie	kalorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kilokalorie	kilokalorie	k1gFnSc1
</s>
<s>
Častěji	často	k6eAd2
se	se	k3xPyFc4
však	však	k9
používá	používat	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
tisíckrát	tisíckrát	k6eAd1
větší	veliký	k2eAgFnSc1d2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
tzv.	tzv.	kA
velká	velká	k1gFnSc1
kalorie	kalorie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kilokalorie	kilokalorie	k1gFnSc1
<g/>
,	,	kIx,
značená	značený	k2eAgFnSc1d1
kcal	kcal	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
také	také	k9
Cal	Cal	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgInSc1d1
tedy	tedy	k9
asi	asi	k9
4,185	4,185	k4
kJ	kJ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
o	o	k7c6
kaloriích	kalorie	k1gFnPc6
mluví	mluvit	k5eAaImIp3nS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
výživou	výživa	k1gFnSc7
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
kilokalorie	kilokalorie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Přepočty	přepočet	k1gInPc1
</s>
<s>
1	#num#	k4
kcal	kcal	k1gInSc1
=	=	kIx~
1000	#num#	k4
cal	cal	k?
≈	≈	k?
4	#num#	k4
185	#num#	k4
J	J	kA
=	=	kIx~
4,185	4,185	k4
kJ	kJ	k?
</s>
<s>
1	#num#	k4
cal	cal	k?
≈	≈	k?
4,185	4,185	k4
J	J	kA
</s>
<s>
1	#num#	k4
J	J	kA
≈	≈	k?
0,239	0,239	k4
cal	cal	k?
</s>
<s>
1	#num#	k4
kJ	kJ	k?
≈	≈	k?
239	#num#	k4
cal	cal	k?
=	=	kIx~
0,239	0,239	k4
kcal	kcalum	k1gNnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kalorie	kalorie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
