<p>
<s>
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Hockey	Hockea	k1gFnPc1	Hockea
Club	club	k1gInSc4	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
moravské	moravský	k2eAgFnSc6d1	Moravská
metropoli	metropol	k1gFnSc6	metropol
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
TJ	tj	kA	tj
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
patřil	patřit	k5eAaImAgInS	patřit
pod	pod	k7c4	pod
československé	československý	k2eAgNnSc4d1	Československé
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
pod	pod	k7c4	pod
firmu	firma	k1gFnSc4	firma
ZKL	ZKL	kA	ZKL
stal	stát	k5eAaPmAgInS	stát
civilní	civilní	k2eAgInSc1d1	civilní
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
získal	získat	k5eAaPmAgMnS	získat
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ZKL	ZKL	kA	ZKL
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
pod	pod	k7c7	pod
dnešním	dnešní	k2eAgInSc7d1	dnešní
názvem	název	k1gInSc7	název
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
celkově	celkově	k6eAd1	celkově
13	[number]	k4	13
titulů	titul	k1gInPc2	titul
mistra	mistr	k1gMnSc2	mistr
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gMnSc4	on
pasuje	pasovat	k5eAaBmIp3nS	pasovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
nejúspěšnějšího	úspěšný	k2eAgInSc2d3	nejúspěšnější
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
českým	český	k2eAgInSc7d1	český
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
tituly	titul	k1gInPc7	titul
z	z	k7c2	z
Evropského	evropský	k2eAgInSc2d1	evropský
hokejového	hokejový	k2eAgInSc2d1	hokejový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Rondo	rondo	k1gNnSc1	rondo
(	(	kIx(	(
<g/>
sponzorským	sponzorský	k2eAgInSc7d1	sponzorský
názvem	název	k1gInSc7	název
DRFG	DRFG	kA	DRFG
Arena	Areno	k1gNnSc2	Areno
<g/>
)	)	kIx)	)
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
7	[number]	k4	7
700	[number]	k4	700
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Éra	éra	k1gFnSc1	éra
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klub	klub	k1gInSc1	klub
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
československé	československý	k2eAgInPc4d1	československý
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
následována	následovat	k5eAaImNgFnS	následovat
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
sedmou	sedma	k1gFnSc7	sedma
dekádou	dekáda	k1gFnSc7	dekáda
a	a	k8xC	a
poté	poté	k6eAd1	poté
osmým	osmý	k4xOgNnSc7	osmý
desetiletím	desetiletí	k1gNnSc7	desetiletí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
mezi	mezi	k7c7	mezi
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
hrála	hrát	k5eAaImAgFnS	hrát
Kometa	kometa	k1gFnSc1	kometa
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jedné	jeden	k4xCgFnSc2	jeden
extraligové	extraligový	k2eAgFnSc2d1	extraligová
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
druholigového	druholigový	k2eAgInSc2d1	druholigový
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
české	český	k2eAgFnSc6d1	Česká
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ovšem	ovšem	k9	ovšem
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
extraligovou	extraligový	k2eAgFnSc4d1	extraligová
licenci	licence	k1gFnSc4	licence
od	od	k7c2	od
Orlů	Orel	k1gMnPc2	Orel
Znojmo	Znojmo	k1gNnSc4	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
působí	působit	k5eAaImIp3nS	působit
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
trvale	trvale	k6eAd1	trvale
v	v	k7c6	v
Extralize	extraliga	k1gFnSc6	extraliga
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc3d1	Česká
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
soutěži	soutěž	k1gFnSc3	soutěž
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2017	[number]	k4	2017
a	a	k8xC	a
2018	[number]	k4	2018
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
klubu	klub	k1gInSc2	klub
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hrající	hrající	k2eAgInSc4d1	hrající
Krajský	krajský	k2eAgInSc4d1	krajský
přebor	přebor	k1gInSc4	přebor
<g/>
,	,	kIx,	,
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ministra	ministr	k1gMnSc2	ministr
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Karola	Karola	k1gFnSc1	Karola
Bacílka	Bacílek	k1gMnSc2	Bacílek
posílena	posílen	k2eAgFnSc1d1	posílena
<g/>
,	,	kIx,	,
vyčleněna	vyčleněn	k2eAgFnSc1d1	vyčleněna
jako	jako	k8xS	jako
sportovní	sportovní	k2eAgFnSc1d1	sportovní
rota	rota	k1gFnSc1	rota
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
složek	složka	k1gFnPc2	složka
MNB	MNB	kA	MNB
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
administrativně	administrativně	k6eAd1	administrativně
i	i	k8xC	i
hospodářsky	hospodářsky	k6eAd1	hospodářsky
do	do	k7c2	do
RH	RH	kA	RH
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
1953	[number]	k4	1953
<g/>
/	/	kIx~	/
<g/>
1954	[number]	k4	1954
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc1	první
mužstvo	mužstvo	k1gNnSc1	mužstvo
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
dvou	dva	k4xCgInPc2	dva
brněnských	brněnský	k2eAgMnPc2d1	brněnský
klubů	klub	k1gInPc2	klub
–	–	k?	–
Sokola	Sokol	k1gMnSc2	Sokol
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
Brno	Brno	k1gNnSc1	Brno
I	i	k8xC	i
(	(	kIx(	(
<g/>
Bronislav	Bronislav	k1gMnSc1	Bronislav
Danda	Danda	k1gMnSc1	Danda
<g/>
,	,	kIx,	,
Slavomír	Slavomír	k1gMnSc1	Slavomír
Bartoň	Bartoň	k1gMnSc1	Bartoň
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sláma	Sláma	k1gMnSc1	Sláma
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Zamastil	zamastit	k5eAaPmAgMnS	zamastit
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
Machovský	Machovský	k2eAgMnSc1d1	Machovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sokola	Sokol	k1gMnSc4	Sokol
GZ	GZ	kA	GZ
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
(	(	kIx(	(
<g/>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bubník	Bubník	k1gMnSc1	Bubník
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Olejník	Olejník	k1gMnSc1	Olejník
a	a	k8xC	a
František	František	k1gMnSc1	František
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
(	(	kIx(	(
<g/>
Čeněk	Čeněk	k1gMnSc1	Čeněk
Liška	Liška	k1gMnSc1	Liška
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sventek	Sventek	k1gMnSc1	Sventek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Budějovic	Budějovice	k1gInPc2	Budějovice
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Kolouch	Kolouch	k1gMnSc1	Kolouch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
(	(	kIx(	(
<g/>
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Návrat	návrat	k1gInSc1	návrat
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
(	(	kIx(	(
<g/>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
přátelský	přátelský	k2eAgInSc4d1	přátelský
zápas	zápas	k1gInSc4	zápas
sehrála	sehrát	k5eAaPmAgFnS	sehrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
se	s	k7c7	s
Spartakem	Spartakus	k1gMnSc7	Spartakus
Švermou	Šverma	k1gMnSc7	Šverma
Jinovice	Jinovice	k1gFnSc2	Jinovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
brankách	branka	k1gFnPc6	branka
Bartoně	Bartoň	k1gMnSc2	Bartoň
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
Zamastila	zamastit	k5eAaPmAgNnP	zamastit
a	a	k8xC	a
Návrata	Návrat	k2eAgFnSc1d1	Návrat
<g/>
,	,	kIx,	,
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Éra	éra	k1gFnSc1	éra
mistrů	mistr	k1gMnPc2	mistr
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1953	[number]	k4	1953
<g/>
/	/	kIx~	/
<g/>
1954	[number]	k4	1954
====	====	k?	====
</s>
</p>
<p>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
B.	B.	kA	B.
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
utkala	utkat	k5eAaPmAgNnP	utkat
s	s	k7c7	s
mužstvy	mužstvo	k1gNnPc7	mužstvo
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
<g/>
,	,	kIx,	,
Tatry	Tatra	k1gFnPc1	Tatra
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
<g/>
,	,	kIx,	,
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
odehrála	odehrát	k5eAaPmAgFnS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zmiňovalo	zmiňovat	k5eAaImAgNnS	zmiňovat
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
odehrála	odehrát	k5eAaPmAgFnS	odehrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
utkání	utkání	k1gNnSc4	utkání
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
venku	venek	k1gInSc2	venek
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc4d1	místní
Dynamo	dynamo	k1gNnSc4	dynamo
rozprášila	rozprášit	k5eAaPmAgFnS	rozprášit
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
již	již	k6eAd1	již
klub	klub	k1gInSc1	klub
odehrál	odehrát	k5eAaPmAgInS	odehrát
doma	doma	k6eAd1	doma
Za	za	k7c7	za
Lužánkami	Lužánka	k1gFnPc7	Lužánka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
plzeňskému	plzeňský	k2eAgMnSc3d1	plzeňský
Spartaku	Spartakus	k1gMnSc3	Spartakus
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc2	který
tentokráte	tentokráte	k?	tentokráte
rozdrtilo	rozdrtit	k5eAaPmAgNnS	rozdrtit
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
výsledku	výsledek	k1gInSc6	výsledek
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dohrávku	dohrávka	k1gFnSc4	dohrávka
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc4	kolo
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
odehrála	odehrát	k5eAaPmAgFnS	odehrát
opět	opět	k6eAd1	opět
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Tatře	Tatra	k1gFnSc3	Tatra
Smíchov	Smíchov	k1gInSc4	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
brankách	branka	k1gFnPc6	branka
Bartoně	Bartoň	k1gMnPc4	Bartoň
<g/>
,	,	kIx,	,
dvou	dva	k4xCgMnPc6	dva
Dandy	Danda	k1gMnSc2	Danda
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
Bubníka	Bubník	k1gMnSc4	Bubník
a	a	k8xC	a
Zamastila	zamastit	k5eAaPmAgFnS	zamastit
porazila	porazit	k5eAaPmAgFnS	porazit
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
odjížděla	odjíždět	k5eAaImAgFnS	odjíždět
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Dynamo	dynamo	k1gNnSc1	dynamo
sice	sice	k8xC	sice
po	po	k7c6	po
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
dostalo	dostat	k5eAaPmAgNnS	dostat
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
dvou	dva	k4xCgMnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
odcházeli	odcházet	k5eAaImAgMnP	odcházet
z	z	k7c2	z
domácího	domácí	k2eAgInSc2d1	domácí
ledu	led	k1gInSc2	led
s	s	k7c7	s
výpraskem	výprask	k1gInSc7	výprask
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc1	první
prohra	prohra	k1gFnSc1	prohra
přišla	přijít	k5eAaPmAgFnS	přijít
hned	hned	k6eAd1	hned
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
nestačila	stačit	k5eNaBmAgFnS	stačit
venku	venku	k6eAd1	venku
na	na	k7c4	na
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
,	,	kIx,	,
prohrála	prohrát	k5eAaPmAgFnS	prohrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
si	se	k3xPyFc3	se
ale	ale	k9	ale
spravila	spravit	k5eAaPmAgFnS	spravit
reputaci	reputace	k1gFnSc4	reputace
hned	hned	k6eAd1	hned
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
Za	za	k7c7	za
Lužánkami	Lužánka	k1gFnPc7	Lužánka
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
přestřelce	přestřelka	k1gFnSc6	přestřelka
s	s	k7c7	s
Karlovými	Karlův	k2eAgInPc7d1	Karlův
Vary	Vary	k1gInPc7	Vary
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
kolo	kolo	k1gNnSc1	kolo
klub	klub	k1gInSc1	klub
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Spartak	Spartak	k1gInSc1	Spartak
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nepoučil	poučit	k5eNaPmAgInS	poučit
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
prohrál	prohrát	k5eAaPmAgInS	prohrát
vysokým	vysoký	k2eAgInSc7d1	vysoký
výsledkem	výsledek	k1gInSc7	výsledek
tentokráte	tentokráte	k?	tentokráte
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Dohrávku	dohrávka	k1gFnSc4	dohrávka
6	[number]	k4	6
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
Tatry	Tatra	k1gFnSc2	Tatra
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
že	že	k8xS	že
po	po	k7c6	po
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
překvapení	překvapený	k2eAgMnPc1d1	překvapený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
dostala	dostat	k5eAaPmAgFnS	dostat
dalších	další	k2eAgInPc2d1	další
sedm	sedm	k4xCc4	sedm
gólů	gól	k1gInPc2	gól
nemohl	moct	k5eNaImAgInS	moct
před	před	k7c7	před
utkáním	utkání	k1gNnSc7	utkání
očekávat	očekávat	k5eAaImF	očekávat
ani	ani	k8xC	ani
největší	veliký	k2eAgMnSc1d3	veliký
optimista	optimista	k1gMnSc1	optimista
<g/>
.	.	kIx.	.
</s>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
Smíchov	Smíchov	k1gInSc1	Smíchov
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
součtu	součet	k1gInSc6	součet
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
překvapivě	překvapivě	k6eAd1	překvapivě
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
!	!	kIx.	!
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
hrála	hrát	k5eAaImAgFnS	hrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
další	další	k2eAgNnSc4d1	další
kolo	kolo	k1gNnSc4	kolo
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Pardubicím	Pardubice	k1gInPc3	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
spravili	spravit	k5eAaPmAgMnP	spravit
svoji	svůj	k3xOyFgFnSc4	svůj
pokaženou	pokažený	k2eAgFnSc4d1	pokažená
reputaci	reputace	k1gFnSc4	reputace
po	po	k7c6	po
vysokém	vysoký	k2eAgInSc6d1	vysoký
debaklu	debakl	k1gInSc6	debakl
z	z	k7c2	z
minulého	minulý	k2eAgNnSc2d1	Minulé
kola	kolo	k1gNnSc2	kolo
výhrou	výhra	k1gFnSc7	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
kolo	kolo	k1gNnSc1	kolo
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
opět	opět	k6eAd1	opět
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
proti	proti	k7c3	proti
Baníku	Baník	k1gInSc3	Baník
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
po	po	k7c6	po
napínavém	napínavý	k2eAgNnSc6d1	napínavé
utkání	utkání	k1gNnSc6	utkání
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
celkově	celkově	k6eAd1	celkově
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
4	[number]	k4	4
bodů	bod	k1gInPc2	bod
před	před	k7c7	před
Vítkovicemi	Vítkovice	k1gInPc7	Vítkovice
a	a	k8xC	a
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
finálovou	finálový	k2eAgFnSc4d1	finálová
skupinu	skupina	k1gFnSc4	skupina
o	o	k7c4	o
celkové	celkový	k2eAgNnSc4d1	celkové
prvenství	prvenství	k1gNnSc4	prvenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
finálové	finálový	k2eAgFnSc6d1	finálová
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
postupně	postupně	k6eAd1	postupně
utkat	utkat	k5eAaPmF	utkat
s	s	k7c7	s
mužstvy	mužstvo	k1gNnPc7	mužstvo
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
Ústředního	ústřední	k2eAgInSc2d1	ústřední
dómu	dóm	k1gInSc2	dóm
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rudou	rudý	k2eAgFnSc7d1	rudá
hvězdou	hvězda	k1gFnSc7	hvězda
postoupily	postoupit	k5eAaPmAgFnP	postoupit
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spartakem	Spartak	k1gInSc7	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
turnajovým	turnajový	k2eAgInSc7d1	turnajový
způsobem	způsob	k1gInSc7	způsob
jednokolově	jednokolově	k6eAd1	jednokolově
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
proti	proti	k7c3	proti
Olomouci	Olomouc	k1gFnSc3	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Křídly	křídlo	k1gNnPc7	křídlo
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
zápas	zápas	k1gInSc4	zápas
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
odehrála	odehrát	k5eAaPmAgFnS	odehrát
tentokráte	tentokráte	k?	tentokráte
proti	proti	k7c3	proti
pražské	pražský	k2eAgFnSc6d1	Pražská
ÚDA	úd	k1gMnSc2	úd
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
mužstvo	mužstvo	k1gNnSc4	mužstvo
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
oni	onen	k3xDgMnPc1	onen
ovšem	ovšem	k9	ovšem
nebyli	být	k5eNaImAgMnP	být
pro	pro	k7c4	pro
Rudou	rudý	k2eAgFnSc4d1	rudá
hvězdu	hvězda	k1gFnSc4	hvězda
rovnocenným	rovnocenný	k2eAgMnSc7d1	rovnocenný
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
;	;	kIx,	;
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
zápas	zápas	k1gInSc4	zápas
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
tentokráte	tentokráte	k?	tentokráte
proti	proti	k7c3	proti
známému	známý	k1gMnSc3	známý
soupeři	soupeř	k1gMnSc3	soupeř
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
Baníku	Baník	k1gInSc2	Baník
Vítkovice	Vítkovice	k1gInPc4	Vítkovice
<g/>
,	,	kIx,	,
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
zápase	zápas	k1gInSc6	zápas
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
poslední	poslední	k2eAgMnSc1d1	poslední
soupeř	soupeř	k1gMnSc1	soupeř
-	-	kIx~	-
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Napínavý	napínavý	k2eAgInSc1d1	napínavý
duel	duel	k1gInSc1	duel
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
pro	pro	k7c4	pro
Rudou	rudý	k2eAgFnSc4d1	rudá
hvězdu	hvězda	k1gFnSc4	hvězda
byl	být	k5eAaImAgInS	být
výsledek	výsledek	k1gInSc1	výsledek
přijatelnější	přijatelný	k2eAgInSc1d2	přijatelnější
pro	pro	k7c4	pro
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
díky	díky	k7c3	díky
lepšímu	dobrý	k2eAgNnSc3d2	lepší
skóre	skóre	k1gNnSc3	skóre
získal	získat	k5eAaPmAgInS	získat
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
tedy	tedy	k9	tedy
zakončila	zakončit	k5eAaPmAgFnS	zakončit
svoji	svůj	k3xOyFgFnSc4	svůj
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
sezónu	sezóna	k1gFnSc4	sezóna
celkovým	celkový	k2eAgNnSc7d1	celkové
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1954	[number]	k4	1954
<g/>
/	/	kIx~	/
<g/>
1955	[number]	k4	1955
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
Rudé	rudý	k2eAgFnSc2d1	rudá
hvězdy	hvězda	k1gFnSc2	hvězda
pět	pět	k4xCc4	pět
hráčů	hráč	k1gMnPc2	hráč
konkrétně	konkrétně	k6eAd1	konkrétně
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sventek	Sventek	k1gMnSc1	Sventek
a	a	k8xC	a
Čeněk	Čeněk	k1gMnSc1	Čeněk
Liška	Liška	k1gMnSc1	Liška
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
mateřského	mateřský	k2eAgInSc2d1	mateřský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
do	do	k7c2	do
Spartaku	Spartak	k1gInSc2	Spartak
ZJŠ	ZJŠ	kA	ZJŠ
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
Machovský	Machovský	k2eAgMnSc1d1	Machovský
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kopsa	Kopsa	k1gFnSc1	Kopsa
<g/>
.	.	kIx.	.
</s>
<s>
Přišli	přijít	k5eAaPmAgMnP	přijít
František	František	k1gMnSc1	František
Mašlaň	Mašlaň	k1gMnSc1	Mašlaň
z	z	k7c2	z
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kasper	Kasper	k1gMnSc1	Kasper
z	z	k7c2	z
pražského	pražský	k2eAgMnSc2d1	pražský
ÚDA	úd	k1gMnSc2	úd
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Šůna	Šůna	k1gMnSc1	Šůna
z	z	k7c2	z
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Scheuer	Scheuer	k1gMnSc1	Scheuer
z	z	k7c2	z
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Chabr	Chabr	k1gMnSc1	Chabr
z	z	k7c2	z
Chomutova	Chomutov	k1gInSc2	Chomutov
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Rys	Rys	k1gMnSc1	Rys
z	z	k7c2	z
ČH	ČH	kA	ČH
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
odešel	odejít	k5eAaPmAgMnS	odejít
Miroslav	Miroslav	k1gMnSc1	Miroslav
Rys	Rys	k1gMnSc1	Rys
do	do	k7c2	do
mateřského	mateřský	k2eAgNnSc2d1	mateřské
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
začala	začít	k5eAaPmAgFnS	začít
ligu	liga	k1gFnSc4	liga
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
A	a	k9	a
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
utkala	utkat	k5eAaPmAgNnP	utkat
s	s	k7c7	s
mužstvy	mužstvo	k1gNnPc7	mužstvo
pražské	pražský	k2eAgMnPc4d1	pražský
Tankisty	tankista	k1gMnPc4	tankista
<g/>
,	,	kIx,	,
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
<g/>
,	,	kIx,	,
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
a	a	k8xC	a
ostravského	ostravský	k2eAgInSc2d1	ostravský
Baníku	Baník	k1gInSc2	Baník
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
3	[number]	k4	3
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
Tankistu	tankista	k1gMnSc4	tankista
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
prohra	prohra	k1gFnSc1	prohra
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sezóně	sezóna	k1gFnSc6	sezóna
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c6	na
ledě	led	k1gInSc6	led
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Místnímu	místní	k2eAgNnSc3d1	místní
Dynamu	dynamo	k1gNnSc3	dynamo
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
vysoko	vysoko	k6eAd1	vysoko
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Finálový	finálový	k2eAgInSc1d1	finálový
turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
až	až	k9	až
po	po	k7c6	po
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
překvapení	překvapení	k1gNnSc1	překvapení
přišlo	přijít	k5eAaPmAgNnS	přijít
hned	hned	k6eAd1	hned
prvním	první	k4xOgInSc7	první
zápasem	zápas	k1gInSc7	zápas
<g/>
,	,	kIx,	,
když	když	k8xS	když
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
Tankistou	tankista	k1gMnSc7	tankista
jenom	jenom	k8xS	jenom
remizovala	remizovat	k5eAaPmAgFnS	remizovat
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
mělo	mít	k5eAaImAgNnS	mít
přijít	přijít	k5eAaPmF	přijít
předčasné	předčasný	k2eAgNnSc1d1	předčasné
finále	finále	k1gNnSc1	finále
mezi	mezi	k7c7	mezi
Spartakem	Spartak	k1gInSc7	Spartak
Sokolovo	Sokolovo	k1gNnSc4	Sokolovo
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Rudou	rudý	k2eAgFnSc7d1	rudá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Brněnští	brněnský	k2eAgMnPc1d1	brněnský
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
zničili	zničit	k5eAaPmAgMnP	zničit
Spartaku	Spartak	k1gInSc2	Spartak
sny	sen	k1gInPc1	sen
o	o	k7c6	o
obhajobě	obhajoba	k1gFnSc6	obhajoba
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mezi	mezi	k7c7	mezi
Chomutovem	Chomutov	k1gInSc7	Chomutov
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Chomutov	Chomutov	k1gInSc1	Chomutov
sice	sice	k8xC	sice
prvně	prvně	k?	prvně
vedl	vést	k5eAaImAgInS	vést
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
minutě	minuta	k1gFnSc6	minuta
třetí	třetí	k4xOgFnSc2	třetí
třetiny	třetina	k1gFnSc2	třetina
dává	dávat	k5eAaImIp3nS	dávat
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
dvě	dva	k4xCgFnPc4	dva
branky	branka	k1gFnPc4	branka
a	a	k8xC	a
otočila	otočit	k5eAaPmAgFnS	otočit
zápas	zápas	k1gInSc4	zápas
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
dával	dávat	k5eAaImAgMnS	dávat
František	František	k1gMnSc1	František
Vaněk	Vaněk	k1gMnSc1	Vaněk
na	na	k7c4	na
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
mohlo	moct	k5eAaImAgNnS	moct
Brno	Brno	k1gNnSc1	Brno
slavit	slavit	k5eAaImF	slavit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
obstarala	obstarat	k5eAaPmAgFnS	obstarat
útočná	útočný	k2eAgFnSc1d1	útočná
formace	formace	k1gFnSc1	formace
Bubník	Bubník	k1gMnSc1	Bubník
-	-	kIx~	-
Bartoň	Bartoň	k1gMnSc1	Bartoň
-	-	kIx~	-
Danda	Danda	k1gMnSc1	Danda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1955	[number]	k4	1955
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
nikdo	nikdo	k3yNnSc1	nikdo
neodešel	odejít	k5eNaPmAgMnS	odejít
<g/>
,	,	kIx,	,
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
posílila	posílit	k5eAaPmAgFnS	posílit
svoji	svůj	k3xOyFgFnSc4	svůj
útočnou	útočný	k2eAgFnSc4d1	útočná
sílu	síla	k1gFnSc4	síla
příchodem	příchod	k1gInSc7	příchod
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pavlů	Pavlů	k1gMnSc2	Pavlů
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
opět	opět	k6eAd1	opět
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
skupinu	skupina	k1gFnSc4	skupina
suverénním	suverénní	k2eAgInSc7d1	suverénní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
ztratila	ztratit	k5eAaPmAgFnS	ztratit
jenom	jenom	k9	jenom
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
prohrou	prohra	k1gFnSc7	prohra
s	s	k7c7	s
Tankistou	tankista	k1gMnSc7	tankista
Praha	Praha	k1gFnSc1	Praha
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
ledě	led	k1gInSc6	led
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finále	finále	k1gNnSc1	finále
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
minulých	minulý	k2eAgInPc2d1	minulý
ročníků	ročník	k1gInPc2	ročník
nehrálo	hrát	k5eNaImAgNnS	hrát
turnajovým	turnajový	k2eAgInSc7d1	turnajový
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
konání	konání	k1gNnSc2	konání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soupeři	soupeř	k1gMnPc1	soupeř
se	se	k3xPyFc4	se
utkali	utkat	k5eAaPmAgMnP	utkat
každý	každý	k3xTgInSc1	každý
dvakrát	dvakrát	k6eAd1	dvakrát
systémem	systém	k1gInSc7	systém
doma-venku	domaenka	k1gFnSc4	doma-venka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
nad	nad	k7c7	nad
Chomutovem	Chomutov	k1gInSc7	Chomutov
vysoko	vysoko	k6eAd1	vysoko
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c4	na
led	led	k1gInSc4	led
Spartaku	Spartak	k1gInSc2	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
soupeře	soupeř	k1gMnSc4	soupeř
ze	z	k7c2	z
sezón	sezóna	k1gFnPc2	sezóna
minulých	minulý	k2eAgFnPc2d1	minulá
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
překvapivě	překvapivě	k6eAd1	překvapivě
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
o	o	k7c6	o
titulu	titul	k1gInSc6	titul
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
býti	být	k5eAaImF	být
již	již	k6eAd1	již
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
prohraje	prohrát	k5eAaPmIp3nS	prohrát
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
venku	venku	k6eAd1	venku
na	na	k7c6	na
ledě	led	k1gInSc6	led
Chomutova	Chomutov	k1gInSc2	Chomutov
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
o	o	k7c6	o
titulu	titul	k1gInSc6	titul
se	se	k3xPyFc4	se
znova	znova	k6eAd1	znova
muselo	muset	k5eAaImAgNnS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
mezi	mezi	k7c7	mezi
RH	RH	kA	RH
a	a	k8xC	a
Spartakem	Spartak	k1gInSc7	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
<g/>
.	.	kIx.	.
</s>
<s>
Tentokráte	Tentokráte	k?	Tentokráte
začala	začít	k5eAaPmAgFnS	začít
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
opět	opět	k6eAd1	opět
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
již	již	k6eAd1	již
po	po	k7c6	po
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
soupeř	soupeř	k1gMnSc1	soupeř
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
jen	jen	k9	jen
tak	tak	k6eAd1	tak
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
ve	v	k7c6	v
49	[number]	k4	49
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc3	minuta
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
53	[number]	k4	53
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
opustil	opustit	k5eAaPmAgMnS	opustit
led	led	k1gInSc4	led
hráč	hráč	k1gMnSc1	hráč
Spartaku	Spartak	k1gInSc2	Spartak
Karel	Karel	k1gMnSc1	Karel
Gut	Gut	k1gMnSc1	Gut
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
jakoby	jakoby	k8xS	jakoby
znovu	znovu	k6eAd1	znovu
ožila	ožít	k5eAaPmAgFnS	ožít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
sedmi	sedm	k4xCc6	sedm
minutách	minuta	k1gFnPc6	minuta
vstřelili	vstřelit	k5eAaPmAgMnP	vstřelit
její	její	k3xOp3gInSc4	její
hráči	hráč	k1gMnPc1	hráč
čtyři	čtyři	k4xCgFnPc4	čtyři
branky	branka	k1gFnPc4	branka
a	a	k8xC	a
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
s	s	k7c7	s
Vítkovicemi	Vítkovice	k1gInPc7	Vítkovice
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgInS	moct
zvrátit	zvrátit	k5eAaPmF	zvrátit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
účast	účast	k1gFnSc1	účast
klubu	klub	k1gInSc2	klub
na	na	k7c6	na
Spenglerově	Spenglerův	k2eAgInSc6d1	Spenglerův
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
československá	československý	k2eAgFnSc1d1	Československá
mužstva	mužstvo	k1gNnPc4	mužstvo
zákaz	zákaz	k1gInSc1	zákaz
startu	start	k1gInSc2	start
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ročníku	ročník	k1gInSc6	ročník
byla	být	k5eAaImAgFnS	být
povolena	povolen	k2eAgFnSc1d1	povolena
účast	účast	k1gFnSc1	účast
jednomu	jeden	k4xCgInSc3	jeden
klubu	klub	k1gInSc3	klub
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
dostala	dostat	k5eAaPmAgFnS	dostat
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
Spartakem	Spartak	k1gInSc7	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
porazila	porazit	k5eAaPmAgFnS	porazit
Inter	Inter	k1gInSc4	Inter
Milán	Milán	k1gInSc4	Milán
vysoko	vysoko	k6eAd1	vysoko
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vypořádala	vypořádat	k5eAaPmAgFnS	vypořádat
s	s	k7c7	s
domácím	domácí	k2eAgInSc7d1	domácí
Davosem	Davos	k1gInSc7	Davos
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
jako	jako	k9	jako
třešničkou	třešnička	k1gFnSc7	třešnička
na	na	k7c6	na
dortu	dort	k1gInSc6	dort
porazila	porazit	k5eAaPmAgFnS	porazit
německý	německý	k2eAgInSc4d1	německý
Füssen	Füssen	k1gInSc4	Füssen
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejstarší	starý	k2eAgInSc1d3	nejstarší
evropský	evropský	k2eAgInSc1d1	evropský
pohár	pohár	k1gInSc1	pohár
směřoval	směřovat	k5eAaImAgInS	směřovat
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1956	[number]	k4	1956
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
klub	klub	k1gInSc1	klub
posílil	posílit	k5eAaPmAgMnS	posílit
Václav	Václav	k1gMnSc1	Václav
Pantůček	Pantůček	k1gMnSc1	Pantůček
ze	z	k7c2	z
Spartaku	Spartak	k1gInSc2	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
přišel	přijít	k5eAaPmAgMnS	přijít
Rudolf	Rudolf	k1gMnSc1	Rudolf
Potsch	Potsch	k1gMnSc1	Potsch
z	z	k7c2	z
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Pantůček	Pantůček	k1gMnSc1	Pantůček
nahradil	nahradit	k5eAaPmAgMnS	nahradit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
útočné	útočný	k2eAgFnSc6d1	útočná
formaci	formace	k1gFnSc6	formace
Slavomíra	Slavomír	k1gMnSc2	Slavomír
Bartoně	Bartoň	k1gMnSc2	Bartoň
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
svůj	svůj	k3xOyFgInSc4	svůj
třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k6eAd1	tak
suverénně	suverénně	k6eAd1	suverénně
jako	jako	k8xC	jako
minulý	minulý	k2eAgInSc4d1	minulý
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc1	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc4	Sokolovo
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
na	na	k7c6	na
první	první	k4xOgFnSc6	první
místo	místo	k7c2	místo
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
26	[number]	k4	26
zápasů	zápas	k1gInPc2	zápas
19	[number]	k4	19
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
3	[number]	k4	3
remizovala	remizovat	k5eAaPmAgFnS	remizovat
a	a	k8xC	a
4	[number]	k4	4
prohrála	prohrát	k5eAaPmAgFnS	prohrát
při	při	k7c6	při
skóre	skóre	k1gNnSc6	skóre
155	[number]	k4	155
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
a	a	k8xC	a
zisku	zisk	k1gInSc2	zisk
41	[number]	k4	41
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
také	také	k9	také
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
výsledku	výsledek	k1gInSc2	výsledek
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
porazila	porazit	k5eAaPmAgFnS	porazit
Spartak	Spartak	k1gInSc4	Spartak
Motorlet	motorlet	k1gInSc1	motorlet
Praha	Praha	k1gFnSc1	Praha
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
Rudé	rudý	k2eAgFnSc2d1	rudá
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nováček	nováček	k1gMnSc1	nováček
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Václav	Václav	k1gMnSc1	Václav
Pantůček	Pantůček	k1gMnSc1	Pantůček
s	s	k7c7	s
27	[number]	k4	27
brankami	branka	k1gFnPc7	branka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
1958	[number]	k4	1958
====	====	k?	====
</s>
</p>
<p>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
mateřského	mateřský	k2eAgInSc2d1	mateřský
Chomutova	Chomutov	k1gInSc2	Chomutov
Ladislav	Ladislav	k1gMnSc1	Ladislav
Chabr	Chabr	k1gMnSc1	Chabr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
týmu	tým	k1gInSc2	tým
přišel	přijít	k5eAaPmAgMnS	přijít
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nadrchal	nadrchat	k5eAaPmAgInS	nadrchat
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc1d1	budoucí
brankářská	brankářský	k2eAgFnSc1d1	brankářská
hvězda	hvězda	k1gFnSc1	hvězda
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
juniorského	juniorský	k2eAgInSc2d1	juniorský
týmu	tým	k1gInSc2	tým
Karel	Karel	k1gMnSc1	Karel
Skopal	Skopal	k1gMnSc1	Skopal
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Šubrtem	Šubrt	k1gMnSc7	Šubrt
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začínaly	začínat	k5eAaImAgInP	začínat
objevovat	objevovat	k5eAaImF	objevovat
hlasy	hlas	k1gInPc1	hlas
kritizující	kritizující	k2eAgInPc1d1	kritizující
její	její	k3xOp3gInSc4	její
profesorský	profesorský	k2eAgInSc4d1	profesorský
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
černá	černý	k2eAgFnSc1d1	černá
kaňka	kaňka	k1gFnSc1	kaňka
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
výkonu	výkon	k1gInSc6	výkon
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
prohra	prohra	k1gFnSc1	prohra
v	v	k7c6	v
Chomutově	Chomutov	k1gInSc6	Chomutov
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Největších	veliký	k2eAgNnPc2d3	veliký
vítězství	vítězství	k1gNnPc2	vítězství
klub	klub	k1gInSc1	klub
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
s	s	k7c7	s
mužstvy	mužstvo	k1gNnPc7	mužstvo
Spartaku	Spartak	k1gInSc2	Spartak
Motorletu	motorlet	k1gInSc2	motorlet
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Baníku	baník	k1gMnSc3	baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
shodně	shodně	k6eAd1	shodně
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sezóna	sezóna	k1gFnSc1	sezóna
1958	[number]	k4	1958
<g/>
/	/	kIx~	/
<g/>
1959	[number]	k4	1959
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
týmu	tým	k1gInSc2	tým
odešli	odejít	k5eAaPmAgMnP	odejít
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
hráči	hráč	k1gMnSc3	hráč
Zamastil	zamastit	k5eAaPmAgMnS	zamastit
a	a	k8xC	a
Kolouch	Kolouch	k1gMnSc1	Kolouch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
mateřského	mateřský	k2eAgNnSc2d1	mateřské
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
týmu	tým	k1gInSc2	tým
přišli	přijít	k5eAaPmAgMnP	přijít
odbýt	odbýt	k5eAaPmF	odbýt
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
hráči	hráč	k1gMnPc1	hráč
Černý	Černý	k1gMnSc1	Černý
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Ševčík	Ševčík	k1gMnSc1	Ševčík
z	z	k7c2	z
Vítkovic	Vítkovice	k1gInPc2	Vítkovice
a	a	k8xC	a
Richter	Richter	k1gMnSc1	Richter
z	z	k7c2	z
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
juniorské	juniorský	k2eAgFnSc2d1	juniorská
týmu	tým	k1gInSc3	tým
pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgMnS	přijít
Ivo	Ivo	k1gMnSc1	Ivo
Winkler	Winkler	k1gMnSc1	Winkler
<g/>
.	.	kIx.	.
</s>
<s>
Ročník	ročník	k1gInSc1	ročník
ligy	liga	k1gFnSc2	liga
měl	mít	k5eAaImAgInS	mít
velmi	velmi	k6eAd1	velmi
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
kolech	kolo	k1gNnPc6	kolo
dostal	dostat	k5eAaPmAgInS	dostat
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc4	Sokolovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
Kladno	Kladno	k1gNnSc1	Kladno
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
už	už	k6eAd1	už
nepustilo	pustit	k5eNaPmAgNnS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
připravila	připravit	k5eAaPmAgFnS	připravit
o	o	k7c6	o
promluvení	promluvení	k1gNnSc6	promluvení
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
ostudnou	ostudný	k2eAgFnSc7d1	ostudná
porážkou	porážka	k1gFnSc7	porážka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
prohrála	prohrát	k5eAaPmAgFnS	prohrát
i	i	k9	i
se	s	k7c7	s
sestupujícím	sestupující	k2eAgInSc7d1	sestupující
týmem	tým	k1gInSc7	tým
HC	HC	kA	HC
Brnem	Brno	k1gNnSc7	Brno
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončila	skončit	k5eAaPmAgFnS	skončit
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soupiska	soupiska	k1gFnSc1	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
–	–	k?	–
TJ	tj	kA	tj
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
–	–	k?	–
TJ	tj	kA	tj
ZKL	ZKL	kA	ZKL
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Závody	závod	k1gInPc1	závod
kuličkových	kuličkový	k2eAgNnPc2d1	kuličkové
ložisek	ložisko	k1gNnPc2	ložisko
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
TJ	tj	kA	tj
Zetor	zetor	k1gInSc1	zetor
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Zetor	zetor	k1gInSc1	zetor
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
HC	HC	kA	HC
Zetor	zetor	k1gInSc1	zetor
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gFnPc1	Hockea
Club	club	k1gInSc4	club
Zetor	zetor	k1gInSc1	zetor
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
HC	HC	kA	HC
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gFnPc1	Hockea
Club	club	k1gInSc4	club
Královopolská	královopolský	k2eAgNnPc4d1	Královopolské
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Hockey	Hockey	k1gInPc4	Hockey
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
BVV	BVV	kA	BVV
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gMnSc2	Hockea
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
Brněnské	brněnský	k2eAgInPc1d1	brněnský
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Hockey	Hockey	k1gInPc4	Hockey
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gMnSc2	Hockea
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gMnSc2	Hockea
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
<g/>
/	/	kIx~	/
<g/>
Vyškov	Vyškov	k1gInSc1	Vyškov
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gFnPc1	Hockea
Club	club	k1gInSc4	club
Kometa	kometa	k1gFnSc1	kometa
<g/>
/	/	kIx~	/
<g/>
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Hockey	Hockea	k1gFnPc1	Hockea
Club	club	k1gInSc4	club
Kometa	kometa	k1gFnSc1	kometa
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Hockey	Hockey	k1gInPc4	Hockey
Club	club	k1gInSc1	club
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
fúze	fúze	k1gFnSc1	fúze
s	s	k7c7	s
HC	HC	kA	HC
Ytong	Ytong	k1gInSc1	Ytong
Brno	Brno	k1gNnSc1	Brno
=	=	kIx~	=
<g/>
>	>	kIx)	>
název	název	k1gInSc1	název
nezměněn	změnit	k5eNaPmNgInS	změnit
</s>
</p>
<p>
<s>
==	==	k?	==
Získané	získaný	k2eAgFnPc4d1	získaná
trofeje	trofej	k1gFnPc4	trofej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhrané	vyhraná	k1gFnPc4	vyhraná
domácí	domácí	k2eAgFnSc2d1	domácí
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
liga	liga	k1gFnSc1	liga
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
(	(	kIx(	(
11	[number]	k4	11
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
/	/	kIx~	/
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
/	/	kIx~	/
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
58	[number]	k4	58
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
/	/	kIx~	/
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
63	[number]	k4	63
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
/	/	kIx~	/
<g/>
66	[number]	k4	66
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
hokejová	hokejový	k2eAgFnSc1d1	hokejová
extraliga	extraliga	k1gFnSc1	extraliga
(	(	kIx(	(
2	[number]	k4	2
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhrané	vyhraná	k1gFnPc4	vyhraná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
hokejový	hokejový	k2eAgInSc1d1	hokejový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
3	[number]	k4	3
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
<g/>
/	/	kIx~	/
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
</s>
</p>
<p>
<s>
Spenglerův	Spenglerův	k2eAgInSc1d1	Spenglerův
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
soutěže	soutěž	k1gFnPc4	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Slezský	slezský	k2eAgInSc1d1	slezský
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
národní	národní	k2eAgInSc1d1	národní
revoluční	revoluční	k2eAgInSc1d1	revoluční
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Tipsport	Tipsport	k1gInSc4	Tipsport
Hockey	Hockea	k1gFnSc2	Hockea
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Rona	Ron	k1gMnSc2	Ron
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Stadion	stadion	k1gInSc1	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
Momentálně	momentálně	k6eAd1	momentálně
Kometa	kometa	k1gFnSc1	kometa
trénuje	trénovat	k5eAaImIp3nS	trénovat
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
DRFG	DRFG	kA	DRFG
Areně	Aren	k1gInSc6	Aren
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nedávnými	dávný	k2eNgInPc7d1	nedávný
úspěchy	úspěch	k1gInPc7	úspěch
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ráda	rád	k2eAgFnSc1d1	ráda
postavila	postavit	k5eAaPmAgFnS	postavit
stadion	stadion	k1gInSc4	stadion
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
parku	park	k1gInSc2	park
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
momentálních	momentální	k2eAgInPc2d1	momentální
plánů	plán	k1gInPc2	plán
by	by	k9	by
za	za	k7c7	za
novým	nový	k2eAgInSc7d1	nový
stadionem	stadion	k1gInSc7	stadion
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k5eAaImF	stát
brněnská	brněnský	k2eAgFnSc1d1	brněnská
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
A	a	k8xC	a
PLUS	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
DRFG	DRFG	kA	DRFG
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
momentálních	momentální	k2eAgMnPc2d1	momentální
vlastníků	vlastník	k1gMnPc2	vlastník
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
ze	z	k7c2	z
stadionu	stadion	k1gInSc2	stadion
udělat	udělat	k5eAaPmF	udělat
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
stavbu	stavba	k1gFnSc4	stavba
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
atelier	atelier	k1gNnSc7	atelier
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
za	za	k7c7	za
projektem	projekt	k1gInSc7	projekt
Univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
nebo	nebo	k8xC	nebo
střediskem	středisko	k1gNnSc7	středisko
CEITEC	CEITEC	kA	CEITEC
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
prvních	první	k4xOgInPc2	první
návrhů	návrh	k1gInPc2	návrh
pražské	pražský	k2eAgFnSc2d1	Pražská
O2	O2	k1gFnSc2	O2
arény	aréna	k1gFnSc2	aréna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
první	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
celého	celý	k2eAgInSc2d1	celý
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
===	===	k?	===
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNHL	ČNHL	kA	ČNHL
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNHL	ČNHL	kA	ČNHL
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNHL	ČNHL	kA	ČNHL
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNHL	ČNHL	kA	ČNHL
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Extraliga	extraliga	k1gFnSc1	extraliga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
–	–	k?	–
:	:	kIx,	:
Extraliga	extraliga	k1gFnSc1	extraliga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
kapitánů	kapitán	k1gMnPc2	kapitán
a	a	k8xC	a
trenérů	trenér	k1gMnPc2	trenér
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezónách	sezóna	k1gFnPc6	sezóna
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Celkový	celkový	k2eAgInSc1d1	celkový
přehled	přehled	k1gInSc1	přehled
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bilance	bilance	k1gFnSc1	bilance
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
soupeři	soupeř	k1gMnPc7	soupeř
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
====	====	k?	====
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
sezony	sezona	k1gFnSc2	sezona
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Statistické	statistický	k2eAgFnSc2d1	statistická
zajímavosti	zajímavost	k1gFnSc2	zajímavost
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
nahrávač	nahrávač	k1gMnSc1	nahrávač
-	-	kIx~	-
historický	historický	k2eAgInSc1d1	historický
přehled	přehled	k1gInSc1	přehled
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
pohárech	pohár	k1gInPc6	pohár
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
SP	SP	kA	SP
-	-	kIx~	-
Spenglerův	Spenglerův	k2eAgInSc4d1	Spenglerův
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
EHP	EHP	kA	EHP
-	-	kIx~	-
Evropský	evropský	k2eAgInSc1d1	evropský
hokejový	hokejový	k2eAgInSc1d1	hokejový
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
EHL	EHL	kA	EHL
-	-	kIx~	-
Evropská	evropský	k2eAgFnSc1d1	Evropská
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
SSix	SSix	k1gInSc1	SSix
-	-	kIx~	-
Super	super	k2eAgInSc1d1	super
six	six	k?	six
<g/>
,	,	kIx,	,
IIHFSup	IIHFSup	k1gMnSc1	IIHFSup
-	-	kIx~	-
IIHF	IIHF	kA	IIHF
Superpohár	superpohár	k1gInSc1	superpohár
<g/>
,	,	kIx,	,
VC	VC	kA	VC
-	-	kIx~	-
Victoria	Victorium	k1gNnSc2	Victorium
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
HLMI	HLMI	kA	HLMI
-	-	kIx~	-
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
ET	ET	kA	ET
-	-	kIx~	-
European	European	k1gMnSc1	European
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
HLM	HLM	kA	HLM
-	-	kIx~	-
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
KP	KP	kA	KP
-	-	kIx~	-
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
pohár	pohár	k1gInSc4	pohár
</s>
</p>
<p>
<s>
SP	SP	kA	SP
1955	[number]	k4	1955
–	–	k?	–
Základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SP	SP	kA	SP
1957	[number]	k4	1957
–	–	k?	–
Finále	finále	k1gNnSc1	finále
</s>
</p>
<p>
<s>
EHP	EHP	kA	EHP
1965	[number]	k4	1965
<g/>
/	/	kIx~	/
<g/>
1966	[number]	k4	1966
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
</s>
</p>
<p>
<s>
EHP	EHP	kA	EHP
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
1967	[number]	k4	1967
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
</s>
</p>
<p>
<s>
EHP	EHP	kA	EHP
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
</s>
</p>
<p>
<s>
EHP	EHP	kA	EHP
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
–	–	k?	–
Semifinále	semifinále	k1gNnSc1	semifinále
</s>
</p>
<p>
<s>
ET	ET	kA	ET
2011	[number]	k4	2011
–	–	k?	–
Jižní	jižní	k2eAgFnSc2d1	jižní
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ET	ET	kA	ET
2012	[number]	k4	2012
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
divize	divize	k1gFnSc1	divize
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ET	ET	kA	ET
2013	[number]	k4	2013
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
divize	divize	k1gFnSc1	divize
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HLM	HLM	kA	HLM
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
–	–	k?	–
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
</s>
</p>
<p>
<s>
HLM	HLM	kA	HLM
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
–	–	k?	–
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
</s>
</p>
<p>
<s>
==	==	k?	==
Hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
za	za	k7c4	za
Kometu	kometa	k1gFnSc4	kometa
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
MS	MS	kA	MS
==	==	k?	==
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
=	=	kIx~	=
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hrají	hrát	k5eAaImIp3nP	hrát
za	za	k7c4	za
Kometu	kometa	k1gFnSc4	kometa
</s>
</p>
<p>
<s>
==	==	k?	==
Hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
za	za	k7c4	za
Kometu	kometa	k1gFnSc4	kometa
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
==	==	k?	==
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
=	=	kIx~	=
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hrají	hrát	k5eAaImIp3nP	hrát
za	za	k7c4	za
Kometu	kometa	k1gFnSc4	kometa
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc4	osobnost
brněnského	brněnský	k2eAgInSc2d1	brněnský
hokeje	hokej	k1gInSc2	hokej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
IIHF	IIHF	kA	IIHF
===	===	k?	===
</s>
</p>
<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bubník	Bubník	k1gMnSc1	Bubník
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machač	Machač	k1gMnSc1	Machač
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouzek	Bouzka	k1gFnPc2	Bouzka
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
ČSLH	ČSLH	kA	ČSLH
===	===	k?	===
</s>
</p>
<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bubník	Bubník	k1gMnSc1	Bubník
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machač	Machač	k1gMnSc1	Machač
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouzek	Bouzka	k1gFnPc2	Bouzka
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nadrchal	nadrchat	k5eAaPmAgMnS	nadrchat
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Potsch	Potsch	k1gMnSc1	Potsch
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bronislav	Bronislav	k1gMnSc1	Bronislav
Danda	Danda	k1gMnSc1	Danda
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Farda	Farda	k1gMnSc1	Farda
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kasper	Kasper	k1gMnSc1	Kasper
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jiřík	Jiřík	k1gMnSc1	Jiřík
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Pantůček	Pantůček	k1gMnSc1	Pantůček
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dzurilla	Dzurilla	k1gMnSc1	Dzurilla
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
Sportovní	sportovní	k2eAgFnSc2d1	sportovní
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
===	===	k?	===
</s>
</p>
<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bubník	Bubník	k1gMnSc1	Bubník
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slavomír	Slavomír	k1gMnSc1	Slavomír
Bartoň	Bartoň	k1gMnSc1	Bartoň
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jiřík	Jiřík	k1gMnSc1	Jiřík
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bronislav	Bronislav	k1gMnSc1	Bronislav
Danda	Danda	k1gMnSc1	Danda
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nadrchal	nadrchat	k5eAaPmAgMnS	nadrchat
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Potsch	Potsch	k1gMnSc1	Potsch
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouzek	Bouzka	k1gFnPc2	Bouzka
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machač	Machač	k1gMnSc1	Machač
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kepák	Kepák	k1gMnSc1	Kepák
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Ševčík	Ševčík	k1gMnSc1	Ševčík
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Farda	Farda	k1gMnSc1	Farda
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Pantůček	Pantůček	k1gMnSc1	Pantůček
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Mašlaň	Mašlaň	k1gMnSc1	Mašlaň
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Meixner	Meixner	k1gMnSc1	Meixner
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kolouch	Kolouch	k1gMnSc1	Kolouch
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Mistrovské	mistrovský	k2eAgFnSc2d1	mistrovská
sestavy	sestava	k1gFnSc2	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Projekty	projekt	k1gInPc1	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
KOMETA	kometa	k1gFnSc1	kometa
noviny	novina	k1gFnSc2	novina
–	–	k?	–
měsíčník	měsíčník	k1gInSc1	měsíčník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
novinky	novinka	k1gFnPc4	novinka
z	z	k7c2	z
Komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
<g/>
.	.	kIx.	.
</s>
<s>
Vydává	vydávat	k5eAaImIp3nS	vydávat
Motor-Presse	Motor-Presse	k1gFnSc1	Motor-Presse
Bohemia	bohemia	k1gFnSc1	bohemia
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
PNS	PNS	kA	PNS
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
PUK	puk	k1gInSc1	puk
–	–	k?	–
časopis	časopis	k1gInSc1	časopis
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
zápasu	zápas	k1gInSc3	zápas
</s>
</p>
<p>
<s>
Kometa	kometa	k1gFnSc1	kometa
Pub	Pub	k1gFnSc2	Pub
–	–	k?	–
restaurace	restaurace	k1gFnSc2	restaurace
začleněné	začleněný	k2eAgFnPc1d1	začleněná
nabízejí	nabízet	k5eAaImIp3nP	nabízet
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
suvenýry	suvenýr	k1gInPc1	suvenýr
<g/>
,	,	kIx,	,
vstupenky	vstupenka	k1gFnPc1	vstupenka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
akce	akce	k1gFnPc1	akce
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
Komety	kometa	k1gFnSc2	kometa
–	–	k?	–
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
města	město	k1gNnPc1	město
podporující	podporující	k2eAgNnPc1d1	podporující
Kometu	kometa	k1gFnSc4	kometa
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MEITNER	MEITNER	kA	MEITNER
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
ŘEPA	Řepa	k1gMnSc1	Řepa
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
JOTA	jota	k1gFnSc1	jota
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
368	[number]	k4	368
<g/>
+	+	kIx~	+
<g/>
48	[number]	k4	48
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788072179428	[number]	k4	9788072179428
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kometa	kometa	k1gFnSc1	kometa
<g/>
:	:	kIx,	:
<g/>
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc4	Brno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgInSc1d1	neoficiální
fanouškovský	fanouškovský	k2eAgInSc1d1	fanouškovský
portál	portál	k1gInSc1	portál
o	o	k7c6	o
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
BRNO	Brno	k1gNnSc1	Brno
a	a	k8xC	a
diskusní	diskusní	k2eAgNnSc1d1	diskusní
fórum	fórum	k1gNnSc1	fórum
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgFnPc4d1	neoficiální
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
týmu	tým	k1gInSc2	tým
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
-	-	kIx~	-
Hokejportal	Hokejportal	k1gFnSc1	Hokejportal
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
DRFG	DRFG	kA	DRFG
Arena	Arena	k1gFnSc1	Arena
domácí	domácí	k2eAgFnSc1d1	domácí
hokejové	hokejový	k2eAgNnSc4d1	hokejové
hřiště	hřiště	k1gNnSc4	hřiště
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
hokej	hokej	k1gInSc4	hokej
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
na	na	k7c4	na
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
