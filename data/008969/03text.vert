<p>
<s>
K	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
íránské	íránský	k2eAgFnSc2d1	íránská
ambasády	ambasáda	k1gFnSc2	ambasáda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
teroristy	terorista	k1gMnSc2	terorista
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
britské	britský	k2eAgFnSc2d1	britská
speciální	speciální	k2eAgFnSc2d1	speciální
jednotky	jednotka	k1gFnSc2	jednotka
Special	Special	k1gMnSc1	Special
Air	Air	k1gMnSc1	Air
Service	Service	k1gFnSc1	Service
(	(	kIx(	(
<g/>
SAS	Sas	k1gMnSc1	Sas
<g/>
)	)	kIx)	)
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Nimrod	Nimrod	k1gMnSc1	Nimrod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
incident	incident	k1gInSc4	incident
přivedl	přivést	k5eAaPmAgMnS	přivést
SAS	Sas	k1gMnSc1	Sas
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
světového	světový	k2eAgInSc2d1	světový
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
přehrávána	přehrávat	k5eAaImNgFnS	přehrávat
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
ambasády	ambasáda	k1gFnSc2	ambasáda
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
v	v	k7c4	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
obsadil	obsadit	k5eAaPmAgInS	obsadit
šestičlenný	šestičlenný	k2eAgInSc1d1	šestičlenný
teroristický	teroristický	k2eAgInSc1d1	teroristický
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
Demokratické	demokratický	k2eAgNnSc4d1	demokratické
revoluční	revoluční	k2eAgNnSc4d1	revoluční
hnutí	hnutí	k1gNnSc4	hnutí
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
Arabistánu	Arabistán	k1gInSc2	Arabistán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
DRMLA	DRMLA	kA	DRMLA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budovu	budova	k1gFnSc4	budova
Prince	princ	k1gMnSc2	princ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gate	Gate	k1gNnSc7	Gate
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Kensigtonu	Kensigton	k1gInSc6	Kensigton
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teroristé	terorista	k1gMnPc1	terorista
nejprve	nejprve	k6eAd1	nejprve
požadovali	požadovat	k5eAaImAgMnP	požadovat
autonomii	autonomie	k1gFnSc4	autonomie
regionů	region	k1gInPc2	region
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
ropu	ropa	k1gFnSc4	ropa
nacházejících	nacházející	k2eAgMnPc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Íránu	Írán	k1gInSc2	Írán
známých	známá	k1gFnPc2	známá
také	také	k9	také
jako	jako	k9	jako
Chúzistán	Chúzistán	k2eAgMnSc1d1	Chúzistán
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
požadovali	požadovat	k5eAaImAgMnP	požadovat
propuštění	propuštění	k1gNnSc4	propuštění
devadesáti	devadesát	k4xCc2	devadesát
jedna	jeden	k4xCgFnSc1	jeden
svých	svůj	k3xOyFgMnPc2	svůj
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
údajných	údajný	k2eAgMnPc2d1	údajný
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
Irácké	irácký	k2eAgFnSc2d1	irácká
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
drženi	držen	k2eAgMnPc1d1	držen
v	v	k7c6	v
íránských	íránský	k2eAgFnPc6d1	íránská
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvacet	dvacet	k4xCc1	dvacet
šest	šest	k4xCc1	šest
rukojmích	rukojmí	k1gMnPc2	rukojmí
bylo	být	k5eAaImAgNnS	být
zadrženo	zadržet	k5eAaPmNgNnS	zadržet
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Trevora	Trevor	k1gMnSc2	Trevor
Locka	Locek	k1gMnSc2	Locek
<g/>
,	,	kIx,	,
policejního	policejní	k2eAgMnSc2d1	policejní
konstábla	konstábl	k1gMnSc2	konstábl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
střežení	střežení	k1gNnSc1	střežení
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
novináře	novinář	k1gMnSc2	novinář
rádia	rádio	k1gNnSc2	rádio
BBC	BBC	kA	BBC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
teroristé	terorista	k1gMnPc1	terorista
poprvé	poprvé	k6eAd1	poprvé
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pět	pět	k4xCc1	pět
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
propuštěno	propustit	k5eAaPmNgNnS	propustit
během	během	k7c2	během
několika	několik	k4yIc2	několik
následujících	následující	k2eAgInPc2d1	následující
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgMnPc1d1	policejní
vyjednavači	vyjednavač	k1gMnPc1	vyjednavač
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
uklidnit	uklidnit	k5eAaPmF	uklidnit
radikály	radikál	k1gInPc4	radikál
pomocí	pomocí	k7c2	pomocí
dodávek	dodávka	k1gFnPc2	dodávka
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
BBC	BBC	kA	BBC
odvysíláno	odvysílán	k2eAgNnSc1d1	odvysíláno
prohlášení	prohlášení	k1gNnSc1	prohlášení
teroristů	terorista	k1gMnPc2	terorista
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
hrozbu	hrozba	k1gFnSc4	hrozba
zabití	zabití	k1gNnSc1	zabití
rukojmí	rukojmí	k1gNnSc2	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
Irácký	irácký	k2eAgMnSc1d1	irácký
vůdce	vůdce	k1gMnSc1	vůdce
této	tento	k3xDgFnSc2	tento
teroristické	teroristický	k2eAgFnSc2d1	teroristická
jednotky	jednotka	k1gFnSc2	jednotka
své	svůj	k3xOyFgFnSc3	svůj
skupině	skupina	k1gFnSc3	skupina
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jordánský	jordánský	k2eAgMnSc1d1	jordánský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
v	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
zajistí	zajistit	k5eAaPmIp3nS	zajistit
jim	on	k3xPp3gMnPc3	on
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
odchod	odchod	k1gInSc1	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
ambasádě	ambasáda	k1gFnSc6	ambasáda
se	se	k3xPyFc4	se
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šestého	šestý	k4xOgInSc2	šestý
dne	den	k1gInSc2	den
obléhání	obléhání	k1gNnSc2	obléhání
zabili	zabít	k5eAaPmAgMnP	zabít
teroristé	terorista	k1gMnPc1	terorista
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rukojmích	rukojmí	k1gMnPc2	rukojmí
<g/>
,	,	kIx,	,
tiskového	tiskový	k2eAgMnSc4d1	tiskový
atašé	atašé	k1gMnSc4	atašé
Abbase	Abbasa	k1gFnSc3	Abbasa
Lavasaniho	Lavasani	k1gMnSc2	Lavasani
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vyhrocení	vyhrocení	k1gNnSc4	vyhrocení
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
premiérka	premiérka	k1gFnSc1	premiérka
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
přijmout	přijmout	k5eAaPmF	přijmout
opatření	opatření	k1gNnSc4	opatření
pro	pro	k7c4	pro
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
záchranné	záchranný	k2eAgFnSc2d1	záchranná
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
Protirevoluční	protirevoluční	k2eAgFnSc2d1	protirevoluční
válečné	válečný	k2eAgFnSc2d1	válečná
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
CRW	CRW	kA	CRW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gNnPc4	on
odnoží	odnoží	k1gNnPc4	odnoží
SAS	Sas	k1gMnSc1	Sas
byl	být	k5eAaImAgMnS	být
vydán	vydat	k5eAaPmNgMnS	vydat
již	již	k6eAd1	již
během	během	k7c2	během
několika	několik	k4yIc2	několik
prvních	první	k4xOgFnPc2	první
hodin	hodina	k1gFnPc2	hodina
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
první	první	k4xOgMnSc1	první
rukojmí	rukojmí	k1gMnSc1	rukojmí
<g/>
,	,	kIx,	,
komisař	komisař	k1gMnSc1	komisař
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
policie	policie	k1gFnSc2	policie
David	David	k1gMnSc1	David
McNee	McNe	k1gMnSc4	McNe
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obrany	obrana	k1gFnSc2	obrana
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
sdělující	sdělující	k2eAgFnSc4d1	sdělující
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
"	"	kIx"	"
<g/>
vojenská	vojenský	k2eAgFnSc1d1	vojenská
operace	operace	k1gFnSc1	operace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
médií	médium	k1gNnPc2	médium
se	se	k3xPyFc4	se
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
obléhání	obléhání	k1gNnSc2	obléhání
utábořili	utábořit	k5eAaPmAgMnP	utábořit
u	u	k7c2	u
kasáren	kasárny	k1gFnPc2	kasárny
SAS	Sas	k1gMnSc1	Sas
v	v	k7c4	v
Stirling	Stirling	k1gInSc4	Stirling
Lines	Linesa	k1gFnPc2	Linesa
v	v	k7c6	v
Herefordu	Herefordo	k1gNnSc6	Herefordo
<g/>
.	.	kIx.	.
</s>
<s>
CRW	CRW	kA	CRW
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nacházel	nacházet	k5eAaImAgMnS	nacházet
mimo	mimo	k7c4	mimo
základnu	základna	k1gFnSc4	základna
na	na	k7c6	na
cvičení	cvičení	k1gNnSc6	cvičení
a	a	k8xC	a
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
zůstal	zůstat	k5eAaPmAgMnS	zůstat
dokud	dokud	k8xS	dokud
nebyl	být	k5eNaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
médií	médium	k1gNnPc2	médium
tudíž	tudíž	k8xC	tudíž
nemohli	moct	k5eNaImAgMnP	moct
následovat	následovat	k5eAaImF	následovat
jednotky	jednotka	k1gFnSc2	jednotka
SAS	Sas	k1gMnSc1	Sas
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
ambasádu	ambasáda	k1gFnSc4	ambasáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravodajské	zpravodajský	k2eAgInPc1d1	zpravodajský
týmy	tým	k1gInPc1	tým
se	se	k3xPyFc4	se
ale	ale	k9	ale
nacházely	nacházet	k5eAaImAgFnP	nacházet
také	také	k9	také
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ambasády	ambasáda	k1gFnSc2	ambasáda
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajské	zpravodajský	k2eAgFnSc3d1	zpravodajská
jednotce	jednotka	k1gFnSc3	jednotka
organizace	organizace	k1gFnSc2	organizace
ITN	ITN	kA	ITN
se	se	k3xPyFc4	se
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nedávno	nedávno	k6eAd1	nedávno
představené	představená	k1gFnSc2	představená
ENG	ENG	kA	ENG
kamery	kamera	k1gFnSc2	kamera
podařilo	podařit	k5eAaPmAgNnS	podařit
nasnímat	nasnímat	k5eAaPmF	nasnímat
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
ambasády	ambasáda	k1gFnSc2	ambasáda
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
výhodného	výhodný	k2eAgNnSc2d1	výhodné
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
živě	živě	k6eAd1	živě
ukázaly	ukázat	k5eAaPmAgInP	ukázat
útok	útok	k1gInSc4	útok
jednotek	jednotka	k1gFnPc2	jednotka
SAS	Sas	k1gMnSc1	Sas
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
televizních	televizní	k2eAgMnPc2d1	televizní
zpravodajů	zpravodaj	k1gMnPc2	zpravodaj
"	"	kIx"	"
<g/>
dostal	dostat	k5eAaPmAgInS	dostat
tip	tip	k1gInSc1	tip
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Útok	útok	k1gInSc1	útok
SAS	Sas	k1gMnSc1	Sas
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
útok	útok	k1gInSc4	útok
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
byly	být	k5eAaImAgInP	být
sníženy	snížit	k5eAaPmNgInP	snížit
přistávací	přistávací	k2eAgInPc1d1	přistávací
dráhy	dráha	k1gFnPc4	dráha
letadel	letadlo	k1gNnPc2	letadlo
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Heathrow	Heathrow	k1gFnPc2	Heathrow
a	a	k8xC	a
British	Britisha	k1gFnPc2	Britisha
Gas	Gas	k1gFnSc2	Gas
zahájila	zahájit	k5eAaPmAgFnS	zahájit
hlučné	hlučný	k2eAgNnSc4d1	hlučné
vrtání	vrtání	k1gNnSc4	vrtání
v	v	k7c6	v
sousedící	sousedící	k2eAgFnSc6d1	sousedící
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
jednotkám	jednotka	k1gFnPc3	jednotka
SAS	Sas	k1gMnSc1	Sas
krytí	krytí	k1gNnSc2	krytí
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
přesunu	přesun	k1gInSc6	přesun
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
útokem	útok	k1gInSc7	útok
byli	být	k5eAaImAgMnP	být
teroristé	terorista	k1gMnPc1	terorista
a	a	k8xC	a
rukojmí	rukojmí	k1gMnPc1	rukojmí
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
pomocí	pomoc	k1gFnSc7	pomoc
optických	optický	k2eAgFnPc2d1	optická
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
dostat	dostat	k5eAaPmF	dostat
přes	přes	k7c4	přes
společnou	společný	k2eAgFnSc4d1	společná
zeď	zeď	k1gFnSc4	zeď
sousedící	sousedící	k2eAgFnSc2d1	sousedící
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odposlech	odposlech	k1gInSc4	odposlech
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
mikrofony	mikrofon	k1gInPc1	mikrofon
instalované	instalovaný	k2eAgInPc1d1	instalovaný
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
budově	budova	k1gFnSc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
dopředu	dopředu	k6eAd1	dopředu
nacvičen	nacvičit	k5eAaBmNgInS	nacvičit
v	v	k7c6	v
maketě	maketa	k1gFnSc6	maketa
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
kasárnách	kasárny	k1gFnPc6	kasárny
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Útok	útok	k1gInSc1	útok
začal	začít	k5eAaPmAgInS	začít
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1980	[number]	k4	1980
v	v	k7c4	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
(	(	kIx(	(
<g/>
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
<g/>
)	)	kIx)	)
detonací	detonace	k1gFnSc7	detonace
nálože	nálož	k1gFnSc2	nálož
na	na	k7c6	na
schodišti	schodiště	k1gNnSc6	schodiště
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
tři	tři	k4xCgNnPc4	tři
minut	minuta	k1gFnPc2	minuta
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
vyhozeno	vyhozen	k2eAgNnSc1d1	vyhozeno
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
rukojmí	rukojmí	k1gNnSc1	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
odstřižena	odstřižen	k2eAgFnSc1d1	odstřižen
dodávka	dodávka	k1gFnSc1	dodávka
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
SAS	Sas	k1gMnSc1	Sas
vnikli	vniknout	k5eAaPmAgMnP	vniknout
do	do	k7c2	do
ambasády	ambasáda	k1gFnSc2	ambasáda
přes	přes	k7c4	přes
střechu	střecha	k1gFnSc4	střecha
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
výbušných	výbušný	k2eAgNnPc2d1	výbušné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
odpálili	odpálit	k5eAaPmAgMnP	odpálit
střešní	střešní	k2eAgInPc4d1	střešní
rámy	rám	k1gInPc4	rám
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmatení	zmatení	k1gNnSc4	zmatení
teroristů	terorista	k1gMnPc2	terorista
během	během	k7c2	během
útoku	útok	k1gInSc2	útok
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
omračující	omračující	k2eAgInPc1d1	omračující
granáty	granát	k1gInPc1	granát
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
SAS	Sas	k1gMnSc1	Sas
byli	být	k5eAaImAgMnP	být
ozbrojeni	ozbrojit	k5eAaPmNgMnP	ozbrojit
automatickými	automatický	k2eAgMnPc7d1	automatický
samopaly	samopal	k1gInPc4	samopal
Heckler	Heckler	k1gMnSc1	Heckler
&	&	k?	&
Koch	Koch	k1gMnSc1	Koch
MP	MP	kA	MP
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
teroristů	terorista	k1gMnPc2	terorista
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
devatenáct	devatenáct	k4xCc1	devatenáct
rukojmích	rukojmí	k1gMnPc2	rukojmí
bylo	být	k5eAaImAgNnS	být
zachráněno	zachránit	k5eAaPmNgNnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
teroristů	terorista	k1gMnPc2	terorista
byl	být	k5eAaImAgInS	být
zasažen	zasažen	k2eAgInSc1d1	zasažen
27	[number]	k4	27
střelami	střela	k1gFnPc7	střela
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rukojmích	rukojmí	k1gMnPc2	rukojmí
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
útoku	útok	k1gInSc2	útok
zabit	zabít	k5eAaPmNgMnS	zabít
teroristy	terorista	k1gMnPc7	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
SAS	Sas	k1gMnSc1	Sas
<g/>
,	,	kIx,	,
štábní	štábní	k2eAgMnSc1d1	štábní
seržant	seržant	k1gMnSc1	seržant
z	z	k7c2	z
Fiji	Fij	k1gFnSc2	Fij
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
<g/>
"	"	kIx"	"
Takavesi	Takavese	k1gFnSc3	Takavese
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
slaňovací	slaňovací	k2eAgFnSc6d1	slaňovací
výstroji	výstroj	k1gFnSc6	výstroj
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
po	po	k7c6	po
útoku	útok	k1gInSc2	útok
omračujícími	omračující	k2eAgInPc7d1	omračující
granáty	granát	k1gInPc7	granát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
dostal	dostat	k5eAaPmAgMnS	dostat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
odříznout	odříznout	k5eAaPmF	odříznout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
Takavesi	Takavese	k1gFnSc4	Takavese
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
lehké	lehký	k2eAgFnPc4d1	lehká
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
navzdory	navzdory	k6eAd1	navzdory
svým	svůj	k3xOyFgNnSc7	svůj
zraněním	zranění	k1gNnSc7	zranění
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Takavesi	Takavese	k1gFnSc4	Takavese
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
poslední	poslední	k2eAgMnSc1d1	poslední
přeživší	přeživší	k2eAgMnSc1d1	přeživší
ozbrojenec	ozbrojenec	k1gMnSc1	ozbrojenec
pokusil	pokusit	k5eAaPmAgMnS	pokusit
skrýt	skrýt	k5eAaPmF	skrýt
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rukojmí	rukojmí	k1gNnSc2	rukojmí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
skutečných	skutečný	k2eAgMnPc2d1	skutečný
rukojmí	rukojmí	k1gMnSc1	rukojmí
jej	on	k3xPp3gMnSc4	on
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
SAS	Sas	k1gMnSc1	Sas
jej	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
vzít	vzít	k5eAaPmF	vzít
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabránila	zabránit	k5eAaPmAgFnS	zabránit
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
sledovala	sledovat	k5eAaImAgFnS	sledovat
média	médium	k1gNnPc1	médium
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následky	následek	k1gInPc4	následek
==	==	k?	==
</s>
</p>
<p>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
názory	názor	k1gInPc1	názor
na	na	k7c6	na
zabití	zabití	k1gNnSc6	zabití
několika	několik	k4yIc2	několik
teroristů	terorista	k1gMnPc2	terorista
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Shaie	Shaie	k1gFnSc2	Shaie
a	a	k8xC	a
Makkiho	Makki	k1gMnSc2	Makki
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
hlídali	hlídat	k5eAaImAgMnP	hlídat
íránská	íránský	k2eAgNnPc4d1	íránské
rukojmí	rukojmí	k1gNnPc4	rukojmí
a	a	k8xC	a
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
útoku	útok	k1gInSc2	útok
se	se	k3xPyFc4	se
rukojmím	rukojmí	k1gMnPc3	rukojmí
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Svědkové	svědek	k1gMnPc1	svědek
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahodili	zahodit	k5eAaPmAgMnP	zahodit
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
sedli	sednout	k5eAaPmAgMnP	sednout
si	se	k3xPyFc3	se
na	na	k7c4	na
podlahu	podlaha	k1gFnSc4	podlaha
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
za	za	k7c7	za
hlavou	hlavý	k2eAgFnSc7d1	hlavá
(	(	kIx(	(
<g/>
záběry	záběr	k1gInPc7	záběr
z	z	k7c2	z
venkovních	venkovní	k2eAgFnPc2d1	venkovní
videokamer	videokamera	k1gFnPc2	videokamera
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
zbraně	zbraň	k1gFnPc1	zbraň
vyhozené	vyhozený	k2eAgFnPc1d1	vyhozená
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
a	a	k8xC	a
vyvěšení	vyvěšení	k1gNnSc2	vyvěšení
bílé	bílý	k2eAgFnSc2d1	bílá
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dadgar	Dadgar	k1gMnSc1	Dadgar
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
rukojmí	rukojmí	k1gMnPc4	rukojmí
řekl	říct	k5eAaPmAgMnS	říct
následující	následující	k2eAgMnSc1d1	následující
(	(	kIx(	(
<g/>
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
další	další	k2eAgMnPc1d1	další
rukojmí	rukojmí	k1gMnPc1	rukojmí
to	ten	k3xDgNnSc4	ten
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Poté	poté	k6eAd1	poté
ty	ten	k3xDgMnPc4	ten
dva	dva	k4xCgMnPc4	dva
teroristy	terorista	k1gMnPc4	terorista
zvedli	zvednout	k5eAaPmAgMnP	zvednout
<g/>
,	,	kIx,	,
přitlačili	přitlačit	k5eAaPmAgMnP	přitlačit
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
a	a	k8xC	a
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
je	on	k3xPp3gFnPc4	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
...	...	k?	...
<g/>
[	[	kIx(	[
<g/>
možná	možná	k9	možná
<g/>
]	]	kIx)	]
něco	něco	k6eAd1	něco
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
kapsách	kapsa	k1gFnPc6	kapsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodně	rozhodně	k6eAd1	rozhodně
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
neměli	mít	k5eNaImAgMnP	mít
žádné	žádný	k3yNgFnPc4	žádný
zbraně	zbraň	k1gFnPc4	zbraň
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Po	po	k7c4	po
ohledání	ohledání	k1gNnSc4	ohledání
mrtvol	mrtvola	k1gFnPc2	mrtvola
byli	být	k5eAaImAgMnP	být
příslušníci	příslušník	k1gMnPc1	příslušník
SAS	Sas	k1gMnSc1	Sas
očištěni	očištěn	k2eAgMnPc1d1	očištěn
před	před	k7c7	před
porotou	porota	k1gFnSc7	porota
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vojáků	voják	k1gMnPc2	voják
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Makki	Makki	k1gNnSc1	Makki
sahá	sahat	k5eAaImIp3nS	sahat
po	po	k7c6	po
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shai	Shai	k1gNnSc1	Shai
má	mít	k5eAaImIp3nS	mít
granát	granát	k1gInSc4	granát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
střelil	střelit	k5eAaPmAgMnS	střelit
zezadu	zezadu	k6eAd1	zezadu
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Denis	Denisa	k1gFnPc2	Denisa
navštívili	navštívit	k5eAaPmAgMnP	navštívit
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
vojáky	voják	k1gMnPc4	voják
SAS	Sas	k1gMnSc1	Sas
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
v	v	k7c6	v
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
poděkovali	poděkovat	k5eAaPmAgMnP	poděkovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tom	Tom	k1gMnSc1	Tom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
přítomných	přítomný	k2eAgMnPc2d1	přítomný
vojáků	voják	k1gMnPc2	voják
SAS	Sas	k1gMnSc1	Sas
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Denisem	Denis	k1gInSc7	Denis
Thatcherem	Thatcher	k1gMnSc7	Thatcher
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgInS	mít
široký	široký	k2eAgInSc4d1	široký
úsměv	úsměv	k1gInSc4	úsměv
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
nám	my	k3xPp1nPc3	my
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Nechali	nechat	k5eAaPmAgMnP	nechat
jste	být	k5eAaImIp2nP	být
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
bastardů	bastard	k1gMnPc2	bastard
žít	žít	k5eAaImF	žít
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
ohledu	ohled	k1gInSc6	ohled
jsme	být	k5eAaImIp1nP	být
zklamali	zklamat	k5eAaPmAgMnP	zklamat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Fowzi	Fowze	k1gFnSc4	Fowze
Nejad	Nejad	k1gInSc1	Nejad
byl	být	k5eAaImAgInS	být
usvědčen	usvědčit	k5eAaPmNgInS	usvědčit
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
obléhání	obléhání	k1gNnSc4	obléhání
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
doživotí	doživotí	k1gNnSc3	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
podmínečné	podmínečný	k2eAgNnSc4d1	podmínečné
propuštění	propuštění	k1gNnSc4	propuštění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgMnS	blížit
čas	čas	k1gInSc4	čas
jeho	jeho	k3xOp3gNnSc2	jeho
podmínečného	podmínečný	k2eAgNnSc2d1	podmínečné
propuštění	propuštění	k1gNnSc2	propuštění
<g/>
,	,	kIx,	,
komentátoři	komentátor	k1gMnPc1	komentátor
případu	případ	k1gInSc2	případ
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
nejspíše	nejspíše	k9	nejspíše
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
deportovat	deportovat	k5eAaBmF	deportovat
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
tam	tam	k6eAd1	tam
mohl	moct	k5eAaImAgInS	moct
čelit	čelit	k5eAaImF	čelit
mučení	mučení	k1gNnSc3	mučení
nebo	nebo	k8xC	nebo
popravě	poprava	k1gFnSc3	poprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
dost	dost	k6eAd1	dost
možná	možná	k9	možná
bude	být	k5eAaImBp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
poskytnout	poskytnout	k5eAaPmF	poskytnout
mu	on	k3xPp3gNnSc3	on
politický	politický	k2eAgInSc4d1	politický
azyl	azyl	k1gInSc4	azyl
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgMnSc1d1	policejní
komisař	komisař	k1gMnSc1	komisař
Trevor	Trevor	k1gMnSc1	Trevor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
stráži	stráž	k1gFnSc6	stráž
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
ambasáda	ambasáda	k1gFnSc1	ambasáda
obsazena	obsadit	k5eAaPmNgFnS	obsadit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rukojmí	rukojmí	k1gNnSc2	rukojmí
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
osobně	osobně	k6eAd1	osobně
mu	on	k3xPp3gMnSc3	on
odpouštím	odpouštět	k5eAaImIp1nS	odpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dostatečně	dostatečně	k6eAd1	dostatečně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Zpravodajské	zpravodajský	k2eAgNnSc1d1	zpravodajské
vysílání	vysílání	k1gNnSc1	vysílání
postavilo	postavit	k5eAaPmAgNnS	postavit
do	do	k7c2	do
středu	střed	k1gInSc2	střed
pozornosti	pozornost	k1gFnSc2	pozornost
reportérku	reportérka	k1gFnSc4	reportérka
Kate	kat	k1gInSc5	kat
Adieovou	Adieův	k2eAgFnSc7d1	Adieův
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
všeobecně	všeobecně	k6eAd1	všeobecně
velký	velký	k2eAgInSc1d1	velký
průlom	průlom	k1gInSc1	průlom
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
–	–	k?	–
novinářky	novinářka	k1gFnSc2	novinářka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
válečné	válečná	k1gFnPc1	válečná
zóny	zóna	k1gFnSc2	zóna
a	a	k8xC	a
ostatní	ostatní	k2eAgFnSc2d1	ostatní
vyhrocené	vyhrocený	k2eAgFnSc2d1	vyhrocená
oblasti	oblast	k1gFnSc2	oblast
výlučnou	výlučný	k2eAgFnSc7d1	výlučná
záležitostí	záležitost	k1gFnSc7	záležitost
mužských	mužský	k2eAgInPc2d1	mužský
reportérů	reportér	k1gMnPc2	reportér
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
odpolední	odpolední	k2eAgMnSc1d1	odpolední
reportér	reportér	k1gMnSc1	reportér
BBC	BBC	kA	BBC
byla	být	k5eAaImAgFnS	být
Adieová	Adieová	k1gFnSc1	Adieová
prvním	první	k4xOgMnSc6	první
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
jednotky	jednotka	k1gFnSc2	jednotka
SAS	Sas	k1gMnSc1	Sas
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
ambasádu	ambasáda	k1gFnSc4	ambasáda
<g/>
.	.	kIx.	.
</s>
<s>
BBC	BBC	kA	BBC
přerušila	přerušit	k5eAaPmAgFnS	přerušit
vysílání	vysílání	k1gNnSc4	vysílání
z	z	k7c2	z
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
snookeru	snookero	k1gNnSc6	snookero
a	a	k8xC	a
Adieová	Adieová	k1gFnSc1	Adieová
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
živě	živě	k6eAd1	živě
a	a	k8xC	a
improvizovaně	improvizovaně	k6eAd1	improvizovaně
před	před	k7c7	před
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
publik	publikum	k1gNnPc2	publikum
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
krčila	krčit	k5eAaImAgFnS	krčit
za	za	k7c7	za
dveřmi	dveře	k1gFnPc7	dveře
od	od	k7c2	od
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
délce	délka	k1gFnSc6	délka
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
pěti	pět	k4xCc2	pět
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
"	"	kIx"	"
<g/>
bleskovou	bleskový	k2eAgFnSc4d1	blesková
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
"	"	kIx"	"
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Iranian	Iranian	k1gInSc1	Iranian
Embassy	Embassa	k1gFnSc2	Embassa
Siege	Sieg	k1gFnSc2	Sieg
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
