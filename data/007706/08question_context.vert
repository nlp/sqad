<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejlépe	dobře	k6eAd3	dobře
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
středověký	středověký	k2eAgInSc1d1	středověký
orloj	orloj	k1gInSc1	orloj
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
turistických	turistický	k2eAgInPc2d1	turistický
objektů	objekt	k1gInPc2	objekt
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
