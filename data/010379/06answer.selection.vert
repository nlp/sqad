<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Ignác	Ignác	k1gMnSc1	Ignác
Mácha	Mácha	k1gMnSc1	Mácha
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
Újezdě	Újezd	k1gInSc6	Újezd
čp.	čp.	k?	čp.
400	[number]	k4	400
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Bílého	bílý	k1gMnSc2	bílý
orla	orel	k1gMnSc2	orel
<g/>
;	;	kIx,	;
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
dům	dům	k1gInSc1	dům
nový	nový	k2eAgInSc1d1	nový
(	(	kIx(	(
<g/>
Újezd	Újezd	k1gInSc1	Újezd
čp.	čp.	k?	čp.
401	[number]	k4	401
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
upozorňující	upozorňující	k2eAgFnSc1d1	upozorňující
na	na	k7c4	na
Máchův	Máchův	k2eAgInSc4d1	Máchův
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
