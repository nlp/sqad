<s>
Utah	Utah	k1gInSc1	Utah
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Utah	Utah	k1gInSc1	Utah
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
horských	horský	k2eAgInPc2d1	horský
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Čtyř	čtyři	k4xCgInPc2	čtyři
rohů	roh	k1gInPc2	roh
<g/>
,	,	kIx,	,
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Idahem	Idah	k1gInSc7	Idah
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Wyomingem	Wyoming	k1gInSc7	Wyoming
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Coloradem	Colorado	k1gNnSc7	Colorado
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
219	[number]	k4	219
653	[number]	k4	653
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Utah	Utah	k1gInSc1	Utah
třináctým	třináctý	k4xOgInSc7	třináctý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
14	[number]	k4	14
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
41	[number]	k4	41
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
se	s	k7c7	s
190	[number]	k4	190
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
West	West	k1gInSc4	West
Valley	Vallea	k1gFnSc2	Vallea
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
130	[number]	k4	130
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Provo	Provo	k1gNnSc1	Provo
(	(	kIx(	(
<g/>
120	[number]	k4	120
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
West	West	k1gMnSc1	West
Jordan	Jordan	k1gMnSc1	Jordan
(	(	kIx(	(
<g/>
110	[number]	k4	110
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orem	Orem	k1gMnSc1	Orem
(	(	kIx(	(
<g/>
90	[number]	k4	90
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sandy	Sando	k1gNnPc7	Sando
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
90	[number]	k4	90
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Kings	Kingsa	k1gFnPc2	Kingsa
Peak	Peak	k1gInSc4	Peak
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
4120	[number]	k4	4120
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Uinta	Uint	k1gInSc2	Uint
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
toky	tok	k1gInPc4	tok
Medvědí	medvědí	k2eAgFnSc1d1	medvědí
řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
řeky	řeka	k1gFnPc1	řeka
Green	Grena	k1gFnPc2	Grena
a	a	k8xC	a
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
Velké	velký	k2eAgNnSc1d1	velké
Solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
jižního	jižní	k2eAgInSc2d1	jižní
Utahu	Utah	k1gInSc2	Utah
dostat	dostat	k5eAaPmF	dostat
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
průzkumných	průzkumný	k2eAgFnPc2d1	průzkumná
expedicí	expedice	k1gFnPc2	expedice
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
Španelé	Španelý	k2eAgNnSc4d1	Španelý
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pouštnímu	pouštní	k2eAgMnSc3d1	pouštní
a	a	k8xC	a
horskému	horský	k2eAgInSc3d1	horský
charakteru	charakter	k1gInSc3	charakter
ale	ale	k8xC	ale
neměli	mít	k5eNaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
území	území	k1gNnSc2	území
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
nárokovali	nárokovat	k5eAaImAgMnP	nárokovat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
Horní	horní	k2eAgFnSc2d1	horní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
Utahu	Utah	k1gInSc6	Utah
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
lovci	lovec	k1gMnPc1	lovec
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
Horní	horní	k2eAgFnSc1d1	horní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
stala	stát	k5eAaPmAgFnS	stát
částí	část	k1gFnSc7	část
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
mexicko-americké	mexickomerický	k2eAgFnSc2d1	mexicko-americká
války	válka	k1gFnSc2	válka
získaly	získat	k5eAaPmAgInP	získat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
do	do	k7c2	do
Utahu	Utah	k1gInSc2	Utah
začali	začít	k5eAaPmAgMnP	začít
hromadně	hromadně	k6eAd1	hromadně
stěhovat	stěhovat	k5eAaImF	stěhovat
mormoni	mormon	k1gMnPc1	mormon
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
Církve	církev	k1gFnSc2	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nehostinné	hostinný	k2eNgFnSc6d1	nehostinná
oblasti	oblast	k1gFnSc6	oblast
usadili	usadit	k5eAaPmAgMnP	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
USA	USA	kA	USA
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
utažské	utažský	k2eAgNnSc1d1	utažský
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
získalo	získat	k5eAaPmAgNnS	získat
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Jutů	Jut	k1gMnPc2	Jut
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
vlastní	vlastní	k2eAgFnSc6d1	vlastní
jazyce	jazyk	k1gInSc6	jazyk
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1896	[number]	k4	1896
stal	stát	k5eAaPmAgInS	stát
45	[number]	k4	45
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nábožensky	nábožensky	k6eAd1	nábožensky
nejhomogennějším	homogenní	k2eAgInPc3d3	homogenní
státům	stát	k1gInPc3	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
asi	asi	k9	asi
62	[number]	k4	62
%	%	kIx~	%
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
mormonismu	mormonismus	k1gInSc3	mormonismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stál	stát	k5eAaImAgInS	stát
u	u	k7c2	u
založení	založení	k1gNnSc2	založení
státu	stát	k1gInSc2	stát
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
utažskou	utažský	k2eAgFnSc4d1	utažská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
88	[number]	k4	88
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
koncentrovan	koncentrovan	k1gMnSc1	koncentrovan
aglomeraci	aglomerace	k1gFnSc4	aglomerace
Wasatch	Wasatcha	k1gFnPc2	Wasatcha
Front	front	k1gInSc4	front
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
centrem	centr	k1gInSc7	centr
je	být	k5eAaImIp3nS	být
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zbylého	zbylý	k2eAgNnSc2d1	zbylé
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
neobydlená	obydlený	k2eNgFnSc1d1	neobydlená
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velmi	velmi	k6eAd1	velmi
rozličné	rozličný	k2eAgInPc4d1	rozličný
útvary	útvar	k1gInPc4	útvar
od	od	k7c2	od
poměrně	poměrně	k6eAd1	poměrně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hor	hora	k1gFnPc2	hora
s	s	k7c7	s
věčně	věčně	k6eAd1	věčně
zasněženými	zasněžený	k2eAgInPc7d1	zasněžený
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
údolí	údolí	k1gNnSc4	údolí
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
vodními	vodní	k2eAgInPc7d1	vodní
toky	tok	k1gInPc7	tok
až	až	k9	až
ke	k	k7c3	k
kamenitým	kamenitý	k2eAgFnPc3d1	kamenitá
a	a	k8xC	a
solným	solný	k2eAgFnPc3d1	solná
pouštím	poušť	k1gFnPc3	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
skalnatou	skalnatý	k2eAgFnSc7d1	skalnatá
zemí	zem	k1gFnSc7	zem
rozdělenou	rozdělený	k2eAgFnSc7d1	rozdělená
na	na	k7c4	na
tří	tři	k4xCgFnPc2	tři
hlavní	hlavní	k2eAgFnSc6d1	hlavní
geologické	geologický	k2eAgFnSc6d1	geologická
oblasti	oblast	k1gFnSc6	oblast
<g/>
:	:	kIx,	:
Skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
a	a	k8xC	a
východ	východ	k1gInSc1	východ
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coloradskou	coloradský	k2eAgFnSc4d1	Coloradská
náhorní	náhorní	k2eAgFnSc4d1	náhorní
plošinu	plošina	k1gFnSc4	plošina
(	(	kIx(	(
<g/>
východ	východ	k1gInSc4	východ
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velkou	velký	k2eAgFnSc4d1	velká
pánev	pánev	k1gFnSc4	pánev
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgFnSc1d1	volná
příroda	příroda	k1gFnSc1	příroda
zde	zde	k6eAd1	zde
nabírá	nabírat	k5eAaImIp3nS	nabírat
mnoho	mnoho	k4c1	mnoho
podob	podoba	k1gFnPc2	podoba
-	-	kIx~	-
od	od	k7c2	od
skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
<g/>
,	,	kIx,	,
solných	solný	k2eAgFnPc2d1	solná
a	a	k8xC	a
písečných	písečný	k2eAgFnPc2d1	písečná
pouští	poušť	k1gFnPc2	poušť
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
zasněžené	zasněžený	k2eAgInPc4d1	zasněžený
horské	horský	k2eAgInPc4d1	horský
vrcholy	vrchol	k1gInPc4	vrchol
až	až	k9	až
po	po	k7c4	po
jehličnaté	jehličnatý	k2eAgInPc4d1	jehličnatý
lesy	les	k1gInPc4	les
v	v	k7c6	v
horských	horský	k2eAgNnPc6d1	horské
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Utah	Utah	k1gInSc1	Utah
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
států	stát	k1gInPc2	stát
Čtyř	čtyři	k4xCgInPc2	čtyři
rohů	roh	k1gInPc2	roh
(	(	kIx(	(
<g/>
Four	Four	k1gMnSc1	Four
Cornes	Cornes	k1gMnSc1	Cornes
states	states	k1gMnSc1	states
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
jejich	jejich	k3xOp3gInSc2	jejich
čtverce	čtverec	k1gInSc2	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
se	se	k3xPyFc4	se
státy	stát	k1gInPc4	stát
Idaho	Ida	k1gMnSc4	Ida
a	a	k8xC	a
Wyoming	Wyoming	k1gInSc4	Wyoming
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
bodě	bod	k1gInSc6	bod
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
hranice	hranice	k1gFnPc1	hranice
4	[number]	k4	4
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
Four	Four	k1gMnSc1	Four
Cornes	Cornes	k1gMnSc1	Cornes
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Four	Four	k1gInSc1	Four
Corners	Corners	k1gInSc1	Corners
Monument	monument	k1gInSc4	monument
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
Utah	Utah	k1gInSc1	Utah
rozlohu	rozloha	k1gFnSc4	rozloha
219	[number]	k4	219
653	[number]	k4	653
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Terén	terén	k1gInSc1	terén
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
státu	stát	k1gInSc2	stát
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
Wasatchský	Wasatchský	k2eAgInSc1d1	Wasatchský
hřeben	hřeben	k1gInSc1	hřeben
(	(	kIx(	(
<g/>
Wasatch	Wasatch	k1gInSc1	Wasatch
Range	Rang	k1gInSc2	Rang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
hřebeny	hřeben	k1gInPc1	hřeben
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc1	výška
až	až	k9	až
3650	[number]	k4	3650
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Skalnatých	skalnatý	k2eAgMnPc2d1	skalnatý
hor.	hor.	k?	hor.
Pro	pro	k7c4	pro
bohaté	bohatý	k2eAgInPc4d1	bohatý
příděly	příděl	k1gInPc4	příděl
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
vydrží	vydržet	k5eAaPmIp3nS	vydržet
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
lyžařských	lyžařský	k2eAgInPc2d1	lyžařský
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
se	se	k3xPyFc4	se
při	při	k7c6	při
úpatí	úpatí	k1gNnSc6	úpatí
tohoto	tento	k3xDgNnSc2	tento
pohoří	pohoří	k1gNnSc2	pohoří
táhne	táhnout	k5eAaImIp3nS	táhnout
Wasatch	Wasatch	k1gInSc1	Wasatch
Front	front	k1gInSc1	front
-	-	kIx~	-
pás	pás	k1gInSc1	pás
městského	městský	k2eAgNnSc2d1	Městské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
žije	žít	k5eAaImIp3nS	žít
88	[number]	k4	88
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
další	další	k2eAgNnSc1d1	další
pohoří	pohoří	k1gNnSc1	pohoří
<g/>
,	,	kIx,	,
Uinta	Uinta	k1gFnSc1	Uinta
Mountains	Mountains	k1gInSc1	Mountains
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
orientováno	orientovat	k5eAaBmNgNnS	orientovat
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Kings	Kings	k1gInSc1	Kings
Peak	Peak	k1gInSc1	Peak
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
vrchol	vrchol	k1gInSc1	vrchol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Utah	Utah	k1gInSc1	Utah
je	být	k5eAaImIp3nS	být
vysokopoloženou	vysokopoložený	k2eAgFnSc7d1	vysokopoložený
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
krajina	krajina	k1gFnSc1	krajina
má	mít	k5eAaImIp3nS	mít
nejčastěji	často	k6eAd3	často
charakter	charakter	k1gInSc4	charakter
náhorních	náhorní	k2eAgFnPc2d1	náhorní
plošin	plošina	k1gFnPc2	plošina
a	a	k8xC	a
pánví	pánev	k1gFnPc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomice	ekonomika	k1gFnSc3	ekonomika
dominuje	dominovat	k5eAaImIp3nS	dominovat
těžba	těžba	k1gFnSc1	těžba
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
východního	východní	k2eAgInSc2d1	východní
Utahu	Utah	k1gInSc2	Utah
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Uintah	Uintaha	k1gFnPc2	Uintaha
and	and	k?	and
Ouray	Ouraa	k1gFnSc2	Ouraa
Indian	Indiana	k1gFnPc2	Indiana
Reservation	Reservation	k1gInSc1	Reservation
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
i	i	k9	i
Navahové	Navah	k1gMnPc1	Navah
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
populárním	populární	k2eAgNnSc7d1	populární
místem	místo	k1gNnSc7	místo
přitahujícím	přitahující	k2eAgNnSc7d1	přitahující
turisty	turist	k1gMnPc4	turist
je	být	k5eAaImIp3nS	být
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
National	National	k1gMnSc1	National
Monument	monument	k1gInSc4	monument
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
pokrytých	pokrytý	k2eAgFnPc2d1	pokrytá
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
vrstvami	vrstva	k1gFnPc7	vrstva
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
řadu	řada	k1gFnSc4	řada
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
vynikají	vynikat	k5eAaImIp3nP	vynikat
zejména	zejména	k9	zejména
Navajo	Navajo	k6eAd1	Navajo
Sandstone	Sandston	k1gInSc5	Sandston
<g/>
,	,	kIx,	,
Kayenta	Kayent	k1gMnSc2	Kayent
Sandstone	Sandston	k1gInSc5	Sandston
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
přítoků	přítok	k1gInPc2	přítok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
zaryly	zarýt	k5eAaPmAgFnP	zarýt
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
pískovcového	pískovcový	k2eAgNnSc2d1	pískovcové
podloží	podloží	k1gNnSc2	podloží
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
divokou	divoký	k2eAgFnSc4d1	divoká
krajinu	krajina	k1gFnSc4	krajina
plnou	plný	k2eAgFnSc4d1	plná
kaňonů	kaňon	k1gInPc2	kaňon
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
roztodivných	roztodivný	k2eAgInPc2d1	roztodivný
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
vytyčena	vytyčen	k2eAgFnSc1d1	vytyčena
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Arches	Arches	k1gInSc1	Arches
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Canyonlands	Canyonlands	k1gInSc1	Canyonlands
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Zion	Zion	k1gInSc1	Zion
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Capitol	Capitola	k1gFnPc2	Capitola
Reef	Reef	k1gInSc1	Reef
<g/>
,	,	kIx,	,
Cedar	Cedar	k1gInSc1	Cedar
Breaks	Breaksa	k1gFnPc2	Breaksa
National	National	k1gFnSc2	National
Monument	monument	k1gInSc1	monument
atd.	atd.	kA	atd.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
Utah	Utah	k1gInSc1	Utah
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
a	a	k8xC	a
nejteplejší	teplý	k2eAgFnSc7d3	nejteplejší
oblastí	oblast	k1gFnSc7	oblast
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
přezdíváno	přezdíván	k2eAgNnSc4d1	přezdíváno
Dixie	Dixie	k1gFnPc4	Dixie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
první	první	k4xOgMnPc1	první
osadníci	osadník	k1gMnPc1	osadník
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
jižanských	jižanský	k2eAgInPc6d1	jižanský
státech	stát	k1gInPc6	stát
nesoucích	nesoucí	k2eAgInPc6d1	nesoucí
tutéž	týž	k3xTgFnSc4	týž
přezdívku	přezdívka	k1gFnSc4	přezdívka
<g/>
)	)	kIx)	)
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
Beaver	Beaver	k1gMnSc1	Beaver
Dam	dáma	k1gFnPc2	dáma
Wash	Wash	k1gMnSc1	Wash
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
místo	místo	k7c2	místo
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
664	[number]	k4	664
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
sem	sem	k6eAd1	sem
Mohavská	Mohavský	k2eAgFnSc1d1	Mohavská
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
rekreační	rekreační	k2eAgFnSc7d1	rekreační
destinací	destinace	k1gFnSc7	destinace
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
oblíbeným	oblíbený	k2eAgNnPc3d1	oblíbené
důchodci	důchodce	k1gMnPc1	důchodce
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Krajinu	Krajina	k1gFnSc4	Krajina
západního	západní	k2eAgInSc2d1	západní
Utahu	Utah	k1gInSc2	Utah
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
převážně	převážně	k6eAd1	převážně
hřebeny	hřeben	k1gInPc4	hřeben
a	a	k8xC	a
pánve	pánev	k1gFnPc4	pánev
pouštního	pouštní	k2eAgInSc2d1	pouštní
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
zejména	zejména	k9	zejména
Bonnevilleská	Bonnevilleský	k2eAgFnSc1d1	Bonnevilleský
solná	solný	k2eAgFnSc1d1	solná
planina	planina	k1gFnSc1	planina
(	(	kIx(	(
<g/>
Bonneville	Bonneville	k1gNnSc1	Bonneville
Salt	salto	k1gNnPc2	salto
Flats	Flats	k1gInSc1	Flats
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
slaná	slaný	k2eAgNnPc1d1	slané
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
starověkého	starověký	k2eAgNnSc2d1	starověké
sladkovodního	sladkovodní	k2eAgNnSc2d1	sladkovodní
jezera	jezero	k1gNnSc2	jezero
Bonneville	Bonneville	k1gNnSc2	Bonneville
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kdysi	kdysi	k6eAd1	kdysi
pokrývalo	pokrývat	k5eAaImAgNnS	pokrývat
většinu	většina	k1gFnSc4	většina
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Velké	velký	k2eAgFnSc2d1	velká
pánve	pánev	k1gFnSc2	pánev
<g/>
:	:	kIx,	:
Velké	velký	k2eAgNnSc1d1	velké
Solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Great	Great	k1gInSc1	Great
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Utažské	Utažský	k2eAgNnSc1d1	Utažský
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Utah	Utah	k1gInSc1	Utah
Lake	Lak	k1gInSc2	Lak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sevir	Sevir	k1gMnSc1	Sevir
(	(	kIx(	(
<g/>
Sevier	Sevier	k1gMnSc1	Sevier
Lake	Lak	k1gFnSc2	Lak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Raš	rašit	k5eAaImRp2nS	rašit
(	(	kIx(	(
<g/>
Rush	Rush	k1gInSc1	Rush
Lake	Lak	k1gInSc2	Lak
<g/>
)	)	kIx)	)
a	a	k8xC	a
Malé	Malé	k2eAgNnSc1d1	Malé
Solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Small	Small	k1gInSc1	Small
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Velkého	velký	k2eAgNnSc2d1	velké
solného	solný	k2eAgNnSc2d1	solné
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Velká	velký	k2eAgFnSc1d1	velká
solná	solný	k2eAgFnSc1d1	solná
poušť	poušť	k1gFnSc1	poušť
(	(	kIx(	(
<g/>
Great	Great	k1gInSc1	Great
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
Desert	desert	k1gInSc1	desert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jezerem	jezero	k1gNnSc7	jezero
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
část	část	k1gFnSc1	část
Wasatch	Wasatch	k1gMnSc1	Wasatch
Front	front	k1gInSc1	front
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
západních	západní	k2eAgFnPc2d1	západní
a	a	k8xC	a
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
utažského	utažský	k2eAgNnSc2d1	utažský
území	území	k1gNnSc2	území
<g/>
:	:	kIx,	:
BLM	BLM	kA	BLM
land	land	k1gInSc1	land
<g/>
,	,	kIx,	,
Utah	Utah	k1gInSc1	Utah
State	status	k1gInSc5	status
Trustland	Trustlando	k1gNnPc2	Trustlando
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
National	National	k1gMnSc1	National
Forest	Forest	k1gMnSc1	Forest
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
National	National	k1gFnSc1	National
Monument	monument	k1gInSc1	monument
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Recreation	Recreation	k1gInSc1	Recreation
Area	area	k1gFnSc1	area
a	a	k8xC	a
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Wilderness	Wildernessa	k1gFnPc2	Wildernessa
Area	area	k1gFnSc1	area
tvoří	tvořit	k5eAaImIp3nS	tvořit
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
70	[number]	k4	70
%	%	kIx~	%
území	území	k1gNnSc6	území
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Utahu	Utah	k1gInSc2	Utah
je	být	k5eAaImIp3nS	být
suchá	suchý	k2eAgFnSc1d1	suchá
a	a	k8xC	a
vysoko	vysoko	k6eAd1	vysoko
položená	položený	k2eAgFnSc1d1	položená
<g/>
.	.	kIx.	.
</s>
<s>
Nejsušší	suchý	k2eAgMnPc1d3	nejsušší
jsou	být	k5eAaImIp3nP	být
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
a	a	k8xC	a
jižní	jižní	k2eAgInSc4d1	jižní
Utah	Utah	k1gInSc4	Utah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obdrží	obdržet	k5eAaPmIp3nS	obdržet
ročně	ročně	k6eAd1	ročně
do	do	k7c2	do
300	[number]	k4	300
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
západního	západní	k2eAgInSc2d1	západní
a	a	k8xC	a
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
Utahu	Utah	k1gInSc2	Utah
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
250	[number]	k4	250
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velké	velký	k2eAgFnSc2d1	velká
solné	solný	k2eAgFnSc2d1	solná
pouště	poušť	k1gFnSc2	poušť
dokonce	dokonce	k9	dokonce
pod	pod	k7c4	pod
130	[number]	k4	130
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
říjen	říjen	k1gInSc4	říjen
až	až	k8xS	až
květen	květen	k1gInSc4	květen
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
listopad	listopad	k1gInSc4	listopad
až	až	k8xS	až
březen	březen	k1gInSc4	březen
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
oblastech	oblast	k1gFnPc6	oblast
<g/>
)	)	kIx)	)
běžné	běžný	k2eAgFnPc1d1	běžná
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
solné	solný	k2eAgFnPc4d1	solná
pouště	poušť	k1gFnPc4	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Nejbohatší	bohatý	k2eAgNnSc1d3	nejbohatší
na	na	k7c4	na
sníh	sníh	k1gInSc4	sníh
je	být	k5eAaImIp3nS	být
Wasatchský	Wasatchský	k2eAgInSc4d1	Wasatchský
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
dostávají	dostávat	k5eAaImIp3nP	dostávat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
příděl	příděl	k1gInSc1	příděl
nad	nad	k7c7	nad
12,5	[number]	k4	12,5
m	m	kA	m
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Utahu	Utah	k1gInSc2	Utah
osídleno	osídlen	k2eAgNnSc1d1	osídleno
indiány	indián	k1gMnPc4	indián
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
kreseb	kresba	k1gFnPc2	kresba
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
)	)	kIx)	)
dokládají	dokládat	k5eAaImIp3nP	dokládat
indiánské	indiánský	k2eAgNnSc1d1	indiánské
osídlení	osídlení	k1gNnSc1	osídlení
staré	starý	k2eAgFnSc2d1	stará
cca	cca	kA	cca
10-12	[number]	k4	10-12
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
známým	známý	k2eAgMnSc7d1	známý
bělochem	běloch	k1gMnSc7	běloch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
navštívit	navštívit	k5eAaPmF	navštívit
území	území	k1gNnSc4	území
Utahu	Utah	k1gInSc2	Utah
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
španělský	španělský	k2eAgMnSc1d1	španělský
konquistador	konquistador	k1gMnSc1	konquistador
Francisco	Francisco	k1gMnSc1	Francisco
Vásquez	Vásquez	k1gMnSc1	Vásquez
de	de	k?	de
Coronado	Coronada	k1gFnSc5	Coronada
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
příliš	příliš	k6eAd1	příliš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
daleko	daleko	k6eAd1	daleko
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
známými	známý	k2eAgMnPc7d1	známý
bělochy	běloch	k1gMnPc7	běloch
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Utahu	Utah	k1gInSc2	Utah
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
františkánští	františkánský	k2eAgMnPc1d1	františkánský
misionáři	misionář	k1gMnPc1	misionář
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
Silvestre	Silvestr	k1gInSc5	Silvestr
Vélez	Vélez	k1gInSc1	Vélez
de	de	k?	de
Escalante	Escalant	k1gMnSc5	Escalant
a	a	k8xC	a
Francisco	Francisco	k1gMnSc1	Francisco
Atanasio	Atanasio	k1gMnSc1	Atanasio
Domínguez	Domínguez	k1gMnSc1	Domínguez
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
expedice	expedice	k1gFnSc1	expedice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k9	až
k	k	k7c3	k
Utažskému	Utažský	k2eAgNnSc3d1	Utažský
jezeru	jezero	k1gNnSc3	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
navštívené	navštívený	k2eAgFnPc1d1	navštívená
území	území	k1gNnPc2	území
si	se	k3xPyFc3	se
nárokovalo	nárokovat	k5eAaImAgNnS	nárokovat
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
později	pozdě	k6eAd2	pozdě
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
Utahu	Utah	k1gInSc2	Utah
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
bílí	bílý	k2eAgMnPc1d1	bílý
lovci	lovec	k1gMnPc1	lovec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
slavný	slavný	k2eAgMnSc1d1	slavný
Jim	on	k3xPp3gMnPc3	on
Bridger	Bridger	k1gMnSc1	Bridger
či	či	k8xC	či
Francouz	Francouz	k1gMnSc1	Francouz
Étienne	Étienn	k1gInSc5	Étienn
Provost	Provost	k1gFnSc4	Provost
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
město	město	k1gNnSc1	město
Provo	Provo	k1gNnSc1	Provo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
usidlovat	usidlovat	k5eAaImF	usidlovat
mormoni	mormon	k1gMnPc1	mormon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
mormoni	mormon	k1gMnPc1	mormon
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gMnSc2	Lak
Valley	Vallea	k1gMnSc2	Vallea
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
značnému	značný	k2eAgInSc3d1	značný
nedostatku	nedostatek	k1gInSc3	nedostatek
vhodných	vhodný	k2eAgNnPc2d1	vhodné
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
stěhovaly	stěhovat	k5eAaImAgFnP	stěhovat
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
mormonské	mormonský	k2eAgFnPc1d1	mormonská
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
najít	najít	k5eAaPmF	najít
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
jejich	jejich	k3xOp3gFnSc1	jejich
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostávalo	dostávat	k5eAaImAgNnS	dostávat
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
skončila	skončit	k5eAaPmAgFnS	skončit
mexicko-americká	mexickomerický	k2eAgFnSc1d1	mexicko-americká
válka	válka	k1gFnSc1	válka
porážkou	porážka	k1gFnSc7	porážka
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
podle	podle	k7c2	podle
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
připadla	připadnout	k5eAaPmAgNnP	připadnout
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
území	území	k1gNnPc1	území
Horní	horní	k2eAgFnSc2d1	horní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nárokovaná	nárokovaný	k2eAgFnSc1d1	nárokovaná
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
území	území	k1gNnSc4	území
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
usedlíci	usedlík	k1gMnPc1	usedlík
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
tvořili	tvořit	k5eAaImAgMnP	tvořit
mormoni	mormon	k1gMnPc1	mormon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
-	-	kIx~	-
nazvali	nazvat	k5eAaPmAgMnP	nazvat
jej	on	k3xPp3gInSc4	on
Deseret	Deseret	k1gInSc4	Deseret
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
struktury	struktura	k1gFnPc4	struktura
už	už	k6eAd1	už
začala	začít	k5eAaPmAgFnS	začít
neoficiálně	neoficiálně	k6eAd1	neoficiálně
vytvářet	vytvářet	k5eAaImF	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hranice	hranice	k1gFnPc1	hranice
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
hranice	hranice	k1gFnSc1	hranice
dnešního	dnešní	k2eAgInSc2d1	dnešní
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
ovšem	ovšem	k9	ovšem
narazil	narazit	k5eAaPmAgInS	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odmítala	odmítat	k5eAaImAgFnS	odmítat
stát	stát	k5eAaPmF	stát
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mormoni	mormon	k1gMnPc1	mormon
nezískali	získat	k5eNaPmAgMnP	získat
větší	veliký	k2eAgFnSc4d2	veliký
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
nedostali	dostat	k5eNaPmAgMnP	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
mimo	mimo	k7c4	mimo
většinu	většina	k1gFnSc4	většina
jejích	její	k3xOp3gFnPc2	její
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nakonec	nakonec	k6eAd1	nakonec
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pouze	pouze	k6eAd1	pouze
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgNnSc4d2	menší
utažské	utažský	k2eAgNnSc4d1	utažský
teritorium	teritorium	k1gNnSc4	teritorium
(	(	kIx(	(
<g/>
schváleno	schválit	k5eAaPmNgNnS	schválit
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
dnešní	dnešní	k2eAgInSc4d1	dnešní
Utah	Utah	k1gInSc4	Utah
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
Nevady	Nevada	k1gFnSc2	Nevada
a	a	k8xC	a
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
Colorada	Colorado	k1gNnSc2	Colorado
a	a	k8xC	a
Nebrasky	Nebraska	k1gFnSc2	Nebraska
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
okrajováno	okrajovat	k5eAaImNgNnS	okrajovat
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnSc1	hranice
stávajícího	stávající	k2eAgInSc2d1	stávající
státu	stát	k1gInSc2	stát
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
guvernérem	guvernér	k1gMnSc7	guvernér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prorok	prorok	k1gMnSc1	prorok
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Církve	církev	k1gFnSc2	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
Brigham	Brigham	k1gInSc1	Brigham
Young	Young	k1gInSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Fillmore	Fillmor	k1gInSc5	Fillmor
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Teritorium	teritorium	k1gNnSc1	teritorium
převzalo	převzít	k5eAaPmAgNnS	převzít
provizorní	provizorní	k2eAgFnSc4d1	provizorní
legislativu	legislativa	k1gFnSc4	legislativa
státu	stát	k1gInSc2	stát
Deseret	Desereta	k1gFnPc2	Desereta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teritorium	teritorium	k1gNnSc1	teritorium
Utah	Utah	k1gInSc1	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
měla	mít	k5eAaImAgFnS	mít
zprvu	zprvu	k6eAd1	zprvu
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
rukou	ruka	k1gFnPc2	ruka
-	-	kIx~	-
organizovala	organizovat	k5eAaBmAgFnS	organizovat
osidlování	osidlování	k1gNnSc4	osidlování
území	území	k1gNnSc2	území
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
klíčové	klíčový	k2eAgNnSc4d1	klíčové
slovo	slovo	k1gNnSc4	slovo
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
rozhodovacích	rozhodovací	k2eAgInPc6d1	rozhodovací
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
nadvláda	nadvláda	k1gFnSc1	nadvláda
nad	nad	k7c7	nad
velkými	velký	k2eAgFnPc7d1	velká
oblastmi	oblast	k1gFnPc7	oblast
mimo	mimo	k7c4	mimo
dnešní	dnešní	k2eAgInSc4d1	dnešní
Utah	Utah	k1gInSc4	Utah
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
osídleny	osídlit	k5eAaPmNgFnP	osídlit
skupinami	skupina	k1gFnPc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
vyznáním	vyznání	k1gNnSc7	vyznání
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
velice	velice	k6eAd1	velice
kontroverzně	kontroverzně	k6eAd1	kontroverzně
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
odpor	odpor	k1gInSc4	odpor
nemormonských	mormonský	k2eNgMnPc2d1	mormonský
obyvatel	obyvatel	k1gMnPc2	obyvatel
chápala	chápat	k5eAaImAgFnS	chápat
a	a	k8xC	a
vycházela	vycházet	k5eAaImAgFnS	vycházet
mu	on	k3xPp3gMnSc3	on
vstříc	vstříc	k6eAd1	vstříc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výše	vysoce	k6eAd2	vysoce
zmíněnému	zmíněný	k2eAgNnSc3d1	zmíněné
okrajování	okrajování	k1gNnSc3	okrajování
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
a	a	k8xC	a
mormony	mormon	k1gMnPc7	mormon
začaly	začít	k5eAaPmAgFnP	začít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
teritoria	teritorium	k1gNnSc2	teritorium
růst	růst	k5eAaImF	růst
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mormoni	mormon	k1gMnPc1	mormon
byli	být	k5eAaImAgMnP	být
vnímáni	vnímat	k5eAaImNgMnP	vnímat
jako	jako	k8xS	jako
nespolehlivá	spolehlivý	k2eNgFnSc1d1	nespolehlivá
a	a	k8xC	a
neamerická	americký	k2eNgFnSc1d1	neamerická
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
přitom	přitom	k6eAd1	přitom
hrálo	hrát	k5eAaImAgNnS	hrát
nejen	nejen	k6eAd1	nejen
jejich	jejich	k3xOp3gNnPc1	jejich
praktikování	praktikování	k1gNnPc1	praktikování
polygamie	polygamie	k1gFnSc2	polygamie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uzavřenost	uzavřenost	k1gFnSc4	uzavřenost
jejich	jejich	k3xOp3gFnSc2	jejich
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
uniformní	uniformní	k2eAgInSc4d1	uniformní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
další	další	k2eAgNnPc4d1	další
náboženská	náboženský	k2eAgNnPc4d1	náboženské
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
braná	braný	k2eAgNnPc4d1	brané
ostatní	ostatní	k1gNnPc4	ostatní
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
přehnaně	přehnaně	k6eAd1	přehnaně
přísná	přísný	k2eAgNnPc1d1	přísné
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
navíc	navíc	k6eAd1	navíc
živilo	živit	k5eAaImAgNnS	živit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mormonská	mormonský	k2eAgFnSc1d1	mormonská
vláda	vláda	k1gFnSc1	vláda
teritoria	teritorium	k1gNnSc2	teritorium
se	se	k3xPyFc4	se
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
smiřovala	smiřovat	k5eAaImAgFnS	smiřovat
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
státní	státní	k2eAgInSc4d1	státní
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jen	jen	k9	jen
mohla	moct	k5eAaImAgFnS	moct
<g/>
,	,	kIx,	,
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
umenšit	umenšit	k5eAaPmF	umenšit
vliv	vliv	k1gInSc4	vliv
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
dostala	dostat	k5eAaPmAgFnS	dostat
nepravdivé	pravdivý	k2eNgFnPc4d1	nepravdivá
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
mormonském	mormonský	k2eAgNnSc6d1	mormonské
povstání	povstání	k1gNnSc6	povstání
<g/>
,	,	kIx,	,
vyslala	vyslat	k5eAaPmAgFnS	vyslat
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
Expedici	expedice	k1gFnSc4	expedice
Utah	Utah	k1gInSc1	Utah
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
teritorium	teritorium	k1gNnSc4	teritorium
zpacifikovat	zpacifikovat	k5eAaPmF	zpacifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
též	též	k9	též
sesadila	sesadit	k5eAaPmAgFnS	sesadit
Younga	Younga	k1gFnSc1	Younga
z	z	k7c2	z
postu	post	k1gInSc2	post
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Mormoni	mormon	k1gMnPc1	mormon
chápali	chápat	k5eAaImAgMnP	chápat
celou	celý	k2eAgFnSc4d1	celá
akci	akce	k1gFnSc4	akce
jako	jako	k8xC	jako
vojenský	vojenský	k2eAgInSc1d1	vojenský
útok	útok	k1gInSc1	útok
a	a	k8xC	a
Young	Young	k1gMnSc1	Young
organizoval	organizovat	k5eAaBmAgMnS	organizovat
jejich	jejich	k3xOp3gInSc4	jejich
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
centrálně	centrálně	k6eAd1	centrálně
neorganizovaných	organizovaný	k2eNgInPc2d1	neorganizovaný
střetů	střet	k1gInPc2	střet
s	s	k7c7	s
nemormonským	mormonský	k2eNgNnSc7d1	mormonský
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Meadows	Meadows	k1gInSc4	Meadows
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mormonští	mormonský	k2eAgMnPc1d1	mormonský
osídlenci	osídlenec	k1gMnPc1	osídlenec
povraždili	povraždit	k5eAaPmAgMnP	povraždit
přes	přes	k7c4	přes
120	[number]	k4	120
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
střety	střet	k1gInPc1	střet
mezi	mezi	k7c7	mezi
mormonskými	mormonský	k2eAgMnPc7d1	mormonský
ozbrojenci	ozbrojenec	k1gMnPc7	ozbrojenec
a	a	k8xC	a
vládními	vládní	k2eAgNnPc7d1	vládní
vojsky	vojsko	k1gNnPc7	vojsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Utažská	Utažský	k2eAgFnSc1d1	Utažská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
ukončila	ukončit	k5eAaPmAgFnS	ukončit
kapitulace	kapitulace	k1gFnSc1	kapitulace
mormonů	mormon	k1gMnPc2	mormon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
úřad	úřad	k1gInSc4	úřad
guvernéra	guvernér	k1gMnSc2	guvernér
Alfredu	Alfred	k1gMnSc3	Alfred
Cummingovi	Cumming	k1gMnSc3	Cumming
<g/>
,	,	kIx,	,
reálně	reálně	k6eAd1	reálně
mu	on	k3xPp3gInSc3	on
však	však	k9	však
jakožto	jakožto	k8xS	jakožto
hlavě	hlava	k1gFnSc6	hlava
Církve	církev	k1gFnSc2	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
zůstala	zůstat	k5eAaPmAgFnS	zůstat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
763	[number]	k4	763
885	[number]	k4	885
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
86,1	[number]	k4	86,1
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
80,4	[number]	k4	80,4
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
1,1	[number]	k4	1,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,2	[number]	k4	1,2
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,0	[number]	k4	2,0
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,9	[number]	k4	0,9
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
6,0	[number]	k4	6,0
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,7	[number]	k4	2,7
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
13,0	[number]	k4	13,0
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Industry	Industr	k1gInPc4	Industr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
lilie	lilie	k1gFnSc2	lilie
Calochortus	Calochortus	k1gInSc1	Calochortus
nuttallii	nuttallie	k1gFnSc4	nuttallie
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
smrk	smrk	k1gInSc1	smrk
pichlavý	pichlavý	k2eAgInSc1d1	pichlavý
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
racek	racek	k1gMnSc1	racek
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Utah	Utah	k1gInSc1	Utah
<g/>
,	,	kIx,	,
We	We	k1gFnSc1	We
Love	lov	k1gInSc5	lov
Thee	Thea	k1gFnSc3	Thea
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
vzdělávací	vzdělávací	k2eAgFnSc7d1	vzdělávací
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Brighama	Brighama	k1gFnSc1	Brighama
Younga	Younga	k1gFnSc1	Younga
<g/>
.	.	kIx.	.
</s>
<s>
Našli	najít	k5eAaPmAgMnP	najít
bychom	by	kYmCp1nP	by
zde	zde	k6eAd1	zde
i	i	k9	i
známou	známý	k2eAgFnSc4d1	známá
indiánskou	indiánský	k2eAgFnSc4d1	indiánská
rezervaci	rezervace	k1gFnSc4	rezervace
Navajo	Navajo	k6eAd1	Navajo
Nation	Nation	k1gInSc1	Nation
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
Bingham	Bingham	k1gInSc1	Bingham
Canyon	Canyon	k1gMnSc1	Canyon
Copper	Copper	k1gMnSc1	Copper
Mine	minout	k5eAaImIp3nS	minout
měděný	měděný	k2eAgInSc4d1	měděný
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
díra	díra	k1gFnSc1	díra
na	na	k7c6	na
Světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
Gary	Gara	k1gFnSc2	Gara
Herbert	Herbert	k1gMnSc1	Herbert
podepsal	podepsat	k5eAaPmAgMnS	podepsat
25	[number]	k4	25
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
zákon	zákon	k1gInSc4	zákon
přijímající	přijímající	k2eAgNnSc4d1	přijímající
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
stříbro	stříbro	k1gNnSc4	stříbro
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgFnSc4d1	oficiální
měnu	měna	k1gFnSc4	měna
k	k	k7c3	k
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
Utah	Utah	k1gInSc4	Utah
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
toto	tento	k3xDgNnSc4	tento
umožnil	umožnit	k5eAaPmAgInS	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
pozitivně	pozitivně	k6eAd1	pozitivně
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
zastánců	zastánce	k1gMnPc2	zastánce
návratu	návrat	k1gInSc2	návrat
ke	k	k7c3	k
zlatému	zlatý	k2eAgInSc3d1	zlatý
standardu	standard	k1gInSc3	standard
<g/>
.	.	kIx.	.
</s>
