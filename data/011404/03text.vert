<p>
<s>
Taktovka	taktovka	k1gFnSc1	taktovka
(	(	kIx(	(
<g/>
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
baguette	baguette	k5eAaPmIp2nP	baguette
de	de	k?	de
chef	chef	k1gInSc4	chef
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
orchestre	orchestr	k1gInSc5	orchestr
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
baton	baton	k1gInSc1	baton
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
ruční	ruční	k2eAgInSc1d1	ruční
nástroj	nástroj	k1gInSc1	nástroj
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
speciálně	speciálně	k6eAd1	speciálně
konstruované	konstruovaný	k2eAgNnSc4d1	konstruované
ukazovátko	ukazovátko	k1gNnSc4	ukazovátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
užívá	užívat	k5eAaImIp3nS	užívat
dirigent	dirigent	k1gMnSc1	dirigent
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
dirigování	dirigování	k1gNnSc6	dirigování
<g/>
.	.	kIx.	.
</s>
<s>
Taktovka	taktovka	k1gFnSc1	taktovka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tenká	tenký	k2eAgFnSc1d1	tenká
světlá	světlý	k2eAgFnSc1d1	světlá
tyčka	tyčka	k1gFnSc1	tyčka
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
nebo	nebo	k8xC	nebo
z	z	k7c2	z
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
o	o	k7c6	o
přibližné	přibližný	k2eAgFnSc6d1	přibližná
délce	délka	k1gFnSc6	délka
40	[number]	k4	40
až	až	k9	až
48	[number]	k4	48
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
opatřena	opatřit	k5eAaPmNgFnS	opatřit
vhodně	vhodně	k6eAd1	vhodně
ergonomicky	ergonomicky	k6eAd1	ergonomicky
tvarovanou	tvarovaný	k2eAgFnSc7d1	tvarovaná
rukojetí	rukojeť	k1gFnSc7	rukojeť
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dirigentovi	dirigent	k1gMnSc6	dirigent
při	při	k7c6	při
dirigování	dirigování	k1gNnSc6	dirigování
neklouzala	klouzat	k5eNaImAgFnS	klouzat
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
taktovky	taktovka	k1gFnPc1	taktovka
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
doplňkovým	doplňkový	k2eAgNnSc7d1	doplňkové
ochranným	ochranný	k2eAgNnSc7d1	ochranné
pouzdrem	pouzdro	k1gNnSc7	pouzdro
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
transportu	transport	k1gInSc2	transport
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
v	v	k7c6	v
mimohudebním	mimohudební	k2eAgInSc6d1	mimohudební
provozu	provoz	k1gInSc6	provoz
nepoškodily	poškodit	k5eNaPmAgFnP	poškodit
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nezlomily	zlomit	k5eNaPmAgInP	zlomit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
typy	typ	k1gInPc1	typ
taktovek	taktovka	k1gFnPc2	taktovka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobeny	vyroben	k2eAgFnPc1d1	vyrobena
jakožto	jakožto	k8xS	jakožto
skládací	skládací	k2eAgFnPc1d1	skládací
z	z	k7c2	z
lehkých	lehký	k2eAgFnPc2d1	lehká
slitin	slitina	k1gFnPc2	slitina
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
teleskopickou	teleskopický	k2eAgFnSc4d1	teleskopická
konstrukci	konstrukce	k1gFnSc4	konstrukce
známou	známý	k2eAgFnSc4d1	známá
např.	např.	kA	např.
z	z	k7c2	z
prutových	prutový	k2eAgFnPc2d1	prutová
antén	anténa	k1gFnPc2	anténa
nebo	nebo	k8xC	nebo
skládacích	skládací	k2eAgInPc2d1	skládací
deštníků	deštník	k1gInPc2	deštník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
před	před	k7c7	před
16	[number]	k4	16
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
===	===	k?	===
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
taktovky	taktovka	k1gFnSc2	taktovka
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
709	[number]	k4	709
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Pherekydes	Pherekydesa	k1gFnPc2	Pherekydesa
z	z	k7c2	z
Patry	patro	k1gNnPc7	patro
udával	udávat	k5eAaImAgInS	udávat
rytmus	rytmus	k1gInSc1	rytmus
máváním	mávání	k1gNnSc7	mávání
zlatou	zlatý	k2eAgFnSc4d1	zlatá
hůlkou	hůlka	k1gFnSc7	hůlka
a	a	k8xC	a
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
na	na	k7c4	na
flétny	flétna	k1gFnPc4	flétna
a	a	k8xC	a
citery	citera	k1gFnPc4	citera
<g/>
,	,	kIx,	,
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
taktovka	taktovka	k1gFnSc1	taktovka
standardně	standardně	k6eAd1	standardně
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
udával	udávat	k5eAaImAgInS	udávat
rytmus	rytmus	k1gInSc1	rytmus
cembalista	cembalista	k1gMnSc1	cembalista
(	(	kIx(	(
<g/>
např.	např.	kA	např.
srolovanými	srolovaný	k2eAgFnPc7d1	srolovaná
notami	nota	k1gFnPc7	nota
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
první	první	k4xOgMnSc1	první
houslista	houslista	k1gMnSc1	houslista
(	(	kIx(	(
<g/>
smyčcem	smyčec	k1gInSc7	smyčec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bylo	být	k5eAaImAgNnS	být
běžnou	běžný	k2eAgFnSc7d1	běžná
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
že	že	k8xS	že
dirigent	dirigent	k1gMnSc1	dirigent
stál	stát	k5eAaImAgMnS	stát
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Jeana-Baptiste	Jeana-Baptist	k1gMnSc5	Jeana-Baptist
Lullyho	Lullyha	k1gMnSc5	Lullyha
<g/>
,	,	kIx,	,
dvorního	dvorní	k2eAgMnSc2d1	dvorní
skladatele	skladatel	k1gMnSc2	skladatel
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
barokní	barokní	k2eAgFnSc1d1	barokní
taktovka	taktovka	k1gFnSc1	taktovka
<g/>
:	:	kIx,	:
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
hůl	hůl	k1gFnSc1	hůl
(	(	kIx(	(
<g/>
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
bâton	bâton	k1gInSc1	bâton
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
stál	stát	k5eAaImAgMnS	stát
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
takt	takt	k1gInSc1	takt
udával	udávat	k5eAaImAgInS	udávat
boucháním	bouchání	k1gNnSc7	bouchání
holí	hole	k1gFnPc2	hole
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1687	[number]	k4	1687
uvedl	uvést	k5eAaPmAgInS	uvést
Lully	Lulla	k1gFnSc2	Lulla
své	svůj	k3xOyFgFnSc2	svůj
Te	Te	k1gFnSc2	Te
Deum	Deum	k1gMnSc1	Deum
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
uzdravení	uzdravení	k1gNnSc2	uzdravení
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dirigování	dirigování	k1gNnSc4	dirigování
orchestru	orchestr	k1gInSc2	orchestr
použil	použít	k5eAaPmAgInS	použít
právě	právě	k9	právě
tuto	tento	k3xDgFnSc4	tento
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hůl	hůl	k1gFnSc4	hůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dirigování	dirigování	k1gNnSc2	dirigování
si	se	k3xPyFc3	se
taktovkou	taktovka	k1gFnSc7	taktovka
prorazil	prorazit	k5eAaPmAgMnS	prorazit
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
absces	absces	k1gInSc4	absces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
gangrénu	gangréna	k1gFnSc4	gangréna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Lully	Lull	k1gInPc4	Lull
dlouho	dlouho	k6eAd1	dlouho
odmítal	odmítat	k5eAaImAgInS	odmítat
amputování	amputování	k1gNnSc4	amputování
končetiny	končetina	k1gFnSc2	končetina
(	(	kIx(	(
<g/>
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
současnost	současnost	k1gFnSc1	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
taktovka	taktovka	k1gFnSc1	taktovka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
zažívá	zažívat	k5eAaImIp3nS	zažívat
rozmach	rozmach	k1gInSc1	rozmach
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
zavedl	zavést	k5eAaPmAgMnS	zavést
použití	použití	k1gNnSc4	použití
taktovky	taktovka	k1gFnSc2	taktovka
u	u	k7c2	u
dirigentského	dirigentský	k2eAgInSc2d1	dirigentský
pultu	pult	k1gInSc2	pult
skladatel	skladatel	k1gMnSc1	skladatel
Carl	Carl	k1gMnSc1	Carl
Maria	Maria	k1gFnSc1	Maria
von	von	k1gInSc4	von
Weber	weber	k1gInSc1	weber
<g/>
.	.	kIx.	.
</s>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
již	již	k9	již
stojí	stát	k5eAaImIp3nS	stát
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
orchestru	orchestr	k1gInSc3	orchestr
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zády	záda	k1gNnPc7	záda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenesený	přenesený	k2eAgInSc4d1	přenesený
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
přeneseným	přenesený	k2eAgFnPc3d1	přenesená
významu	význam	k1gInSc3	význam
zejména	zejména	k9	zejména
ve	v	k7c6	v
frazeologizmu	frazeologizmus	k1gInSc6	frazeologizmus
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
mimohudebním	mimohudební	k2eAgInSc6d1	mimohudební
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
někým	někdo	k3yInSc7	někdo
přímo	přímo	k6eAd1	přímo
řízen	řízen	k2eAgInSc1d1	řízen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Baton	baton	k1gInSc1	baton
(	(	kIx(	(
<g/>
conducting	conducting	k1gInSc1	conducting
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
</s>
</p>
<p>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
taktovka	taktovka	k1gFnSc1	taktovka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
