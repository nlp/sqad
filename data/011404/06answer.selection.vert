<s>
Kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
taktovky	taktovka	k1gFnPc1	taktovka
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
doplňkovým	doplňkový	k2eAgNnSc7d1	doplňkové
ochranným	ochranný	k2eAgNnSc7d1	ochranné
pouzdrem	pouzdro	k1gNnSc7	pouzdro
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
transportu	transport	k1gInSc2	transport
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
v	v	k7c6	v
mimohudebním	mimohudební	k2eAgInSc6d1	mimohudební
provozu	provoz	k1gInSc6	provoz
nepoškodily	poškodit	k5eNaPmAgFnP	poškodit
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nezlomily	zlomit	k5eNaPmAgInP	zlomit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
