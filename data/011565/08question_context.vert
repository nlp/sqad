<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
Veľké	Veľký	k2eAgFnSc3d1	Veľká
Orvište	Orvište	k1gFnSc3	Orvište
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
součástí	součást	k1gFnSc7	součást
seskupení	seskupení	k1gNnSc1	seskupení
tří	tři	k4xCgFnPc2	tři
obcí	obec	k1gFnPc2	obec
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Velké	velká	k1gFnSc2	velká
Orvište	Orvište	k1gMnSc2	Orvište
–	–	k?	–
Bašovce	Bašovec	k1gMnSc2	Bašovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
