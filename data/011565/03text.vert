<p>
<s>
Veľké	Veľký	k2eAgFnPc4d1	Veľká
Orvište	Orvište	k1gFnPc4	Orvište
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
spojnici	spojnice	k1gFnSc4	spojnice
měst	město	k1gNnPc2	město
Piešťany	Piešťany	k1gInPc4	Piešťany
<g/>
–	–	k?	–
<g/>
Vrbové	Vrbová	k1gFnSc2	Vrbová
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
zjištěné	zjištěný	k2eAgNnSc1d1	zjištěné
osídlení	osídlení	k1gNnSc1	osídlení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
3500	[number]	k4	3500
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1113	[number]	k4	1113
v	v	k7c6	v
zoborských	zoborský	k2eAgFnPc6d1	zoborský
listinách	listina	k1gFnPc6	listina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
Veľké	Veľký	k2eAgFnSc3d1	Veľká
Orvište	Orvište	k1gFnSc3	Orvište
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
součástí	součást	k1gFnSc7	součást
seskupení	seskupení	k1gNnSc1	seskupení
tří	tři	k4xCgFnPc2	tři
obcí	obec	k1gFnPc2	obec
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
Velké	velká	k1gFnSc2	velká
Orvište	Orvište	k1gMnSc2	Orvište
–	–	k?	–
Bašovce	Bašovec	k1gMnSc2	Bašovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
kostele	kostel	k1gInSc6	kostel
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
a	a	k8xC	a
rekonstruován	rekonstruovat	k5eAaBmNgMnS	rekonstruovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Veľké	Veľký	k2eAgFnSc2d1	Veľká
Orvište	Orvište	k1gFnSc2	Orvište
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Veľké	Veľký	k2eAgFnSc2d1	Veľká
Orvište	Orvište	k1gFnSc2	Orvište
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
