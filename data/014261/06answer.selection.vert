<s>
Velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
VKV	VKV	kA
nebo	nebo	k8xC
též	též	k9
VHF	VHF	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Very	Vera	k1gMnSc2
High	High	k1gMnSc1
Frequency	Frequenca	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
používaná	používaný	k2eAgFnSc1d1
především	především	k9
pro	pro	k7c4
přenos	přenos	k1gInSc4
rozhlasového	rozhlasový	k2eAgInSc2d1
a	a	k8xC
radiového	radiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>