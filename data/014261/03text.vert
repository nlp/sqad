<s>
Velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
</s>
<s>
Velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
VKV	VKV	kA
nebo	nebo	k8xC
též	též	k9
VHF	VHF	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Very	Vera	k1gMnSc2
High	High	k1gMnSc1
Frequency	Frequenca	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
používaná	používaný	k2eAgFnSc1d1
především	především	k9
pro	pro	k7c4
přenos	přenos	k1gInSc4
rozhlasového	rozhlasový	k2eAgInSc2d1
a	a	k8xC
radiového	radiový	k2eAgInSc2d1
signálu	signál	k1gInSc2
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velmi	velmi	k6eAd1
krátká	krátký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
definice	definice	k1gFnSc2
frekvenční	frekvenční	k2eAgInSc4d1
obor	obor	k1gInSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
frekvence	frekvence	k1gFnSc1
od	od	k7c2
30	#num#	k4
do	do	k7c2
300	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
vlnovým	vlnový	k2eAgFnPc3d1
délkám	délka	k1gFnPc3
1	#num#	k4
m	m	kA
až	až	k9
10	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
frekvence	frekvence	k1gFnPc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
přenos	přenos	k1gInSc4
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vyžadují	vyžadovat	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
šířku	šířka	k1gFnSc4
pásma	pásmo	k1gNnSc2
než	než	k8xS
na	na	k7c6
KV	KV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
tohoto	tento	k3xDgNnSc2
pásma	pásmo	k1gNnSc2
tedy	tedy	k9
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
FM	FM	kA
rozhlasové	rozhlasový	k2eAgNnSc1d1
a	a	k8xC
televizní	televizní	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Frekvence	frekvence	k1gFnPc4
těsně	těsně	k6eAd1
pod	pod	k7c7
tímto	tento	k3xDgInSc7
rozsahem	rozsah	k1gInSc7
nazýváme	nazývat	k5eAaImIp1nP
krátké	krátký	k2eAgFnPc4d1
vlny	vlna	k1gFnPc4
(	(	kIx(
<g/>
KV	KV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
tímto	tento	k3xDgInSc7
rozsahem	rozsah	k1gInSc7
ultra	ultra	k2eAgFnSc2d1
krátké	krátký	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
UKV	UKV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Rozhlas	rozhlas	k1gInSc1
</s>
<s>
Pro	pro	k7c4
analogové	analogový	k2eAgNnSc4d1
FM	FM	kA
rozhlasové	rozhlasový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pásmo	pásmo	k1gNnSc1
VKV	VKV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
rozdělení	rozdělení	k1gNnSc4
rádiových	rádiový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
na	na	k7c4
jednotlivá	jednotlivý	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
a	a	k8xC
geografii	geografie	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
využití	využití	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Název	název	k1gInSc1
pásma	pásmo	k1gNnSc2
</s>
<s>
FrekvenceVlnová	FrekvenceVlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
Příklady	příklad	k1gInPc1
využití	využití	k1gNnSc2
</s>
<s>
Pásmo	pásmo	k1gNnSc1
OIRT	OIRT	kA
(	(	kIx(
<g/>
VKV	VKV	kA
I	I	kA
<g/>
,	,	kIx,
„	„	k?
<g/>
východní	východní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
65,8	65,8	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
MHz	Mhz	kA
4,55	4,55	k4
m	m	kA
–	–	k?
4,05	4,05	k4
m	m	kA
</s>
<s>
východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
(	(	kIx(
<g/>
SNS	SNS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
začátku	začátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
76	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
MHz	Mhz	kA
3,95	3,95	k4
m	m	kA
–	–	k?
3,15	3,15	k4
m	m	kA
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
pásmo	pásmo	k1gNnSc1
ITU-R	ITU-R	k1gFnSc2
(	(	kIx(
<g/>
VKV	VKV	kA
II	II	kA
<g/>
,	,	kIx,
„	„	k?
<g/>
západní	západní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
87,5	87,5	k4
<g/>
–	–	k?
<g/>
108	#num#	k4
MHz	Mhz	kA
3,43	3,43	k4
m	m	kA
–	–	k?
2,77	2,77	k4
m	m	kA
</s>
<s>
USA	USA	kA
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
většina	většina	k1gFnSc1
světa	svět	k1gInSc2
</s>
<s>
Digitální	digitální	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
</s>
<s>
Digitální	digitální	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
DAB	DAB	kA
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
III	III	kA
<g/>
.	.	kIx.
pásmo	pásmo	k1gNnSc4
VKV	VKV	kA
(	(	kIx(
<g/>
174	#num#	k4
<g/>
-	-	kIx~
<g/>
240	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiný	jiný	k2eAgInSc1d1
systém	systém	k1gInSc1
DRM	DRM	kA
<g/>
+	+	kIx~
vysílá	vysílat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
pásmu	pásmo	k1gNnSc6
VKV	VKV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Pásmo	pásmo	k1gNnSc1
VKV	VKV	kA
se	se	k3xPyFc4
využívalo	využívat	k5eAaPmAgNnS,k5eAaImAgNnS
pro	pro	k7c4
analogové	analogový	k2eAgNnSc4d1
televizní	televizní	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
digitální	digitální	k2eAgFnSc4d1
DVB-T	DVB-T	k1gFnSc4
<g/>
/	/	kIx~
<g/>
T	T	kA
<g/>
2	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
VKV	VKV	kA
již	již	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
nepoužívá	používat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Měnič	měnič	k1gInSc1
kmitočtu	kmitočet	k1gInSc2
k	k	k7c3
VKV	VKV	kA
radiopřijímači	radiopřijímač	k1gInPc7
z	z	k7c2
pásma	pásmo	k1gNnSc2
CCIR	CCIR	kA
do	do	k7c2
pásma	pásmo	k1gNnSc2
OIRT	OIRT	kA
<g/>
↑	↑	k?
Vysílání	vysílání	k1gNnSc2
v	v	k7c6
pásmu	pásmo	k1gNnSc6
VKV	VKV	kA
OIRT1	OIRT1	k1gFnSc1
2	#num#	k4
Digitalizace	digitalizace	k1gFnSc1
čeká	čekat	k5eAaImIp3nS
i	i	k9
rádia	rádio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
vysílání	vysílání	k1gNnSc2
ve	v	k7c6
III	III	kA
<g/>
.	.	kIx.
pásmu	pásmo	k1gNnSc6
(	(	kIx(
<g/>
VHF	VHF	kA
<g/>
)	)	kIx)
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Televizniweb	Televizniwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-03	2018-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
na	na	k7c6
světě	svět	k1gInSc6
zruší	zrušit	k5eAaPmIp3nS
rozhlasové	rozhlasový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
FM	FM	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4131658-7	4131658-7	k4
</s>
