<s>
Jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgMnSc1d1	dvoulaločný
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
Stínadla	stínadlo	k1gNnSc2	stínadlo
se	se	k3xPyFc4	se
bouří	bouřit	k5eAaImIp3nS	bouřit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
Vontů	Vonta	k1gMnPc2	Vonta
zvaná	zvaný	k2eAgNnPc1d1	zvané
Uctívači	uctívač	k1gMnSc3	uctívač
ginga	ginga	k1gFnSc1	ginga
užívá	užívat	k5eAaImIp3nS	užívat
listy	list	k1gInPc4	list
ginkga	ginkga	k1gFnSc1	ginkga
jako	jako	k8xS	jako
odznak	odznak	k1gInSc1	odznak
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
