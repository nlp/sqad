<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Hongkongu	Hongkong	k1gInSc2	Hongkong
–	–	k?	–
list	list	k1gInSc1	list
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc4d1	bílý
květ	květ	k1gInSc4	květ
Bauhínie	Bauhínie	k1gFnSc2	Bauhínie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
listech	list	k1gInPc6	list
pět	pět	k4xCc4	pět
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Stylizovaný	stylizovaný	k2eAgInSc1d1	stylizovaný
květ	květ	k1gInSc1	květ
Bauhínie	Bauhínie	k1gFnSc2	Bauhínie
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
rostlina	rostlina	k1gFnSc1	rostlina
také	také	k9	také
objevena	objevit	k5eAaPmNgFnS	objevit
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
zdokumentována	zdokumentován	k2eAgFnSc1d1	zdokumentována
<g/>
,	,	kIx,	,
a	a	k8xC	a
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
lidskost	lidskost	k1gFnSc4	lidskost
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
harmonii	harmonie	k1gFnSc4	harmonie
a	a	k8xC	a
díky	díky	k7c3	díky
svojí	svůj	k3xOyFgFnSc3	svůj
bílé	bílý	k2eAgFnSc3d1	bílá
barvě	barva	k1gFnSc3	barva
také	také	k9	také
soulad	soulad	k1gInSc1	soulad
uspořádání	uspořádání	k1gNnSc1	uspořádání
jedna	jeden	k4xCgFnSc1	jeden
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hvězd	hvězda	k1gFnPc2	hvězda
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pěti	pět	k4xCc2	pět
hvězdám	hvězda	k1gFnPc3	hvězda
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hongkong	Hongkong	k1gInSc1	Hongkong
je	být	k5eAaImIp3nS	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hongkong	Hongkong	k1gInSc4	Hongkong
získal	získat	k5eAaPmAgMnS	získat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
oficiální	oficiální	k2eAgFnSc4d1	oficiální
vlajku	vlajka	k1gFnSc4	vlajka
jako	jako	k8xC	jako
britská	britský	k2eAgFnSc1d1	britská
kolonie	kolonie	k1gFnSc1	kolonie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
běžné	běžný	k2eAgFnSc2d1	běžná
vlajky	vlajka	k1gFnSc2	vlajka
britské	britský	k2eAgFnSc2d1	britská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
modrého	modrý	k2eAgNnSc2d1	modré
pole	pole	k1gNnSc2	pole
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
a	a	k8xC	a
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
příslušné	příslušný	k2eAgFnSc2d1	příslušná
kolonie	kolonie	k1gFnSc2	kolonie
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
polovině	polovina	k1gFnSc6	polovina
modrého	modré	k1gNnSc2	modré
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
blue	blue	k1gInSc1	blue
ensign	ensigna	k1gFnPc2	ensigna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
Hongkongu	Hongkong	k1gInSc2	Hongkong
používaný	používaný	k2eAgMnSc1d1	používaný
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
vlajce	vlajka	k1gFnSc6	vlajka
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
tedy	tedy	k9	tedy
existovaly	existovat	k5eAaImAgInP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
znaky	znak	k1gInPc1	znak
a	a	k8xC	a
jim	on	k3xPp3gInPc3	on
příslušné	příslušný	k2eAgFnPc1d1	příslušná
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
vlajka	vlajka	k1gFnSc1	vlajka
Hongkongu	Hongkong	k1gInSc2	Hongkong
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
procesu	proces	k1gInSc2	proces
přesunu	přesun	k1gInSc2	přesun
správy	správa	k1gFnSc2	správa
teritoria	teritorium	k1gNnSc2	teritorium
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
na	na	k7c4	na
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zavedena	zavést	k5eAaPmNgFnS	zavést
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
byla	být	k5eAaImAgFnS	být
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zároveň	zároveň	k6eAd1	zároveň
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
samotný	samotný	k2eAgInSc1d1	samotný
přesun	přesun	k1gInSc1	přesun
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
současné	současný	k2eAgFnSc2d1	současná
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
předáním	předání	k1gNnSc7	předání
Hongkongu	Hongkong	k1gInSc2	Hongkong
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Hongkongu	Hongkong	k1gInSc2	Hongkong
v	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
7147	[number]	k4	7147
různých	různý	k2eAgFnPc2d1	různá
variant	varianta	k1gFnPc2	varianta
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
výběr	výběr	k1gInSc1	výběr
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Hongkongu	Hongkong	k1gInSc2	Hongkong
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Flag	flag	k1gInSc4	flag
of	of	k?	of
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hongkongský	hongkongský	k2eAgInSc1d1	hongkongský
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hongkongská	hongkongský	k2eAgFnSc1d1	hongkongská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
