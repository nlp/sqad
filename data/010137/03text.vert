<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1922	[number]	k4	1922
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
divadelní	divadelní	k2eAgFnSc1d1	divadelní
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
maturovala	maturovat	k5eAaBmAgFnS	maturovat
na	na	k7c6	na
místním	místní	k2eAgNnSc6d1	místní
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Herectví	herectví	k1gNnSc4	herectví
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Státní	státní	k2eAgFnSc6d1	státní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
(	(	kIx(	(
<g/>
absolutorium	absolutorium	k1gNnSc4	absolutorium
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
Horáckém	horácký	k2eAgNnSc6d1	Horácké
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Krajském	krajský	k2eAgNnSc6d1	krajské
oblastním	oblastní	k2eAgNnSc6d1	oblastní
divadle	divadlo	k1gNnSc6	divadlo
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
především	především	k9	především
v	v	k7c6	v
postavách	postava	k1gFnPc6	postava
klasického	klasický	k2eAgInSc2d1	klasický
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
činohry	činohra	k1gFnSc2	činohra
Státního	státní	k2eAgNnSc2d1	státní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
angažmá	angažmá	k1gNnSc6	angažmá
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
zasloužilou	zasloužilý	k2eAgFnSc7d1	zasloužilá
umělkyní	umělkyně	k1gFnSc7	umělkyně
<g/>
.	.	kIx.	.
<g/>
Její	její	k3xOp3gFnSc1	její
filmografie	filmografie	k1gFnSc1	filmografie
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
její	její	k3xOp3gFnPc4	její
působení	působení	k1gNnSc1	působení
mimo	mimo	k7c4	mimo
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
také	také	k9	také
omezení	omezení	k1gNnSc1	omezení
její	její	k3xOp3gFnSc2	její
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
normalizačním	normalizační	k2eAgInSc7d1	normalizační
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
filmy	film	k1gInPc7	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Archa	archa	k1gFnSc1	archa
bláznů	blázen	k1gMnPc2	blázen
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Metráček	Metráček	k?	Metráček
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
Hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
tři	tři	k4xCgInPc4	tři
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
kultivovaný	kultivovaný	k2eAgInSc1d1	kultivovaný
herecký	herecký	k2eAgInSc1d1	herecký
projev	projev	k1gInSc1	projev
našel	najít	k5eAaPmAgInS	najít
rovněž	rovněž	k9	rovněž
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc2	Thálie
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
mistrovství	mistrovství	k1gNnSc4	mistrovství
v	v	k7c6	v
činohře	činohra	k1gFnSc6	činohra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
byl	být	k5eAaImAgMnS	být
herec	herec	k1gMnSc1	herec
František	František	k1gMnSc1	František
Šec	Šec	k1gMnSc1	Šec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
divadla	divadlo	k1gNnPc1	divadlo
:	:	kIx,	:
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
divadelních	divadelní	k2eAgMnPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
615	[number]	k4	615
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7008	[number]	k4	7008
<g/>
-	-	kIx~	-
<g/>
107	[number]	k4	107
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
172	[number]	k4	172
<g/>
,	,	kIx,	,
213	[number]	k4	213
<g/>
,	,	kIx,	,
304	[number]	k4	304
<g/>
,	,	kIx,	,
340	[number]	k4	340
<g/>
,	,	kIx,	,
341	[number]	k4	341
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIKEJZ	FIKEJZ	kA	FIKEJZ
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
:	:	kIx,	:
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
L	L	kA	L
<g/>
–	–	k?	–
<g/>
Ř.	Ř.	kA	Ř.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
656	[number]	k4	656
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
471	[number]	k4	471
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
613	[number]	k4	613
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
:	:	kIx,	:
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
637	[number]	k4	637
<g/>
–	–	k?	–
<g/>
1298	[number]	k4	1298
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901103	[number]	k4	901103
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
820	[number]	k4	820
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kulturně-historická	Kulturněistorický	k2eAgFnSc1d1	Kulturně-historická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
:	:	kIx,	:
M	M	kA	M
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
regionální	regionální	k2eAgNnPc4d1	regionální
studia	studio	k1gNnPc4	studio
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
575	[number]	k4	575
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7464	[number]	k4	7464
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
286	[number]	k4	286
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
587	[number]	k4	587
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
na	na	k7c4	na
Seniortip	Seniortip	k1gInSc4	Seniortip
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zora	Zora	k1gFnSc1	Zora
Rozsypalová	Rozsypalová	k1gFnSc1	Rozsypalová
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
českého	český	k2eAgNnSc2d1	české
amatérského	amatérský	k2eAgNnSc2d1	amatérské
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
