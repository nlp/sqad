<s>
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
West	West	k2eAgInSc1d1	West
Orange	Orange	k1gInSc1	Orange
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
2332	[number]	k4	2332
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
tisíce	tisíc	k4xCgInPc1	tisíc
jich	on	k3xPp3gMnPc2	on
registrovaly	registrovat	k5eAaBmAgInP	registrovat
jeho	jeho	k3xOp3gFnPc4	jeho
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
Edisonovy	Edisonův	k2eAgInPc4d1	Edisonův
vynálezy	vynález	k1gInPc4	vynález
patří	patřit	k5eAaImIp3nP	patřit
fonograf	fonograf	k1gInSc4	fonograf
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
gramofonu	gramofon	k1gInSc2	gramofon
<g/>
)	)	kIx)	)
a	a	k8xC	a
mylně	mylně	k6eAd1	mylně
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
počítána	počítán	k2eAgMnSc4d1	počítán
i	i	k9	i
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gInSc1	Edison
je	být	k5eAaImIp3nS	být
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
dodnes	dodnes	k6eAd1	dodnes
vydávaného	vydávaný	k2eAgInSc2d1	vydávaný
prestižního	prestižní	k2eAgInSc2d1	prestižní
časopisu	časopis	k1gInSc2	časopis
Science	Science	k1gFnSc2	Science
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
menšího	malý	k2eAgNnSc2d2	menší
města	město	k1gNnSc2	město
West	West	k2eAgInSc4d1	West
Orange	Orange	k1gInSc4	Orange
(	(	kIx(	(
<g/>
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
Glenmont	Glenmont	k1gInSc1	Glenmont
–	–	k?	–
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
5,5	[number]	k4	5,5
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
spravován	spravován	k2eAgInSc1d1	spravován
jako	jako	k8xS	jako
národní	národní	k2eAgFnSc1d1	národní
památka	památka	k1gFnSc1	památka
Edison	Edisona	k1gFnPc2	Edisona
National	National	k1gMnSc2	National
Historical	Historical	k1gMnSc2	Historical
Site	Sit	k1gMnSc2	Sit
<g/>
.	.	kIx.	.
</s>
<s>
Hlásil	hlásit	k5eAaImAgMnS	hlásit
se	se	k3xPyFc4	se
ke	k	k7c3	k
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
vyznání	vyznání	k1gNnSc3	vyznání
<g/>
,	,	kIx,	,
vynikal	vynikat	k5eAaImAgMnS	vynikat
svojí	svůj	k3xOyFgFnSc7	svůj
podnikavostí	podnikavost	k1gFnSc7	podnikavost
<g/>
,	,	kIx,	,
pracovitostí	pracovitost	k1gFnSc7	pracovitost
a	a	k8xC	a
cílevědomostí	cílevědomost	k1gFnSc7	cílevědomost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
manželství	manželství	k1gNnSc2	manželství
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
Glenmontu	Glenmont	k1gInSc6	Glenmont
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Orange	Orange	k1gInSc4	Orange
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gInSc2	jeho
pohřbu	pohřeb	k1gInSc2	pohřeb
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1931	[number]	k4	1931
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
Edisonovu	Edisonův	k2eAgFnSc4d1	Edisonova
počest	počest	k1gFnSc4	počest
v	v	k7c6	v
USA	USA	kA	USA
zhasnuty	zhasnout	k5eAaPmNgFnP	zhasnout
všechny	všechen	k3xTgFnPc1	všechen
žárovky	žárovka	k1gFnPc1	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
lehké	lehký	k2eAgNnSc4d1	lehké
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
malička	maličko	k1gNnSc2	maličko
byl	být	k5eAaImAgInS	být
hodně	hodně	k6eAd1	hodně
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
pouze	pouze	k6eAd1	pouze
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
jako	jako	k8xC	jako
chlapec	chlapec	k1gMnSc1	chlapec
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
,	,	kIx,	,
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
malou	malý	k2eAgFnSc4d1	malá
chemickou	chemický	k2eAgFnSc4d1	chemická
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Chemikálie	chemikálie	k1gFnPc1	chemikálie
byly	být	k5eAaImAgFnP	být
drahé	drahý	k2eAgFnPc1d1	drahá
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
si	se	k3xPyFc3	se
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
vydělávat	vydělávat	k5eAaImF	vydělávat
prodejem	prodej	k1gInSc7	prodej
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
roznáškou	roznáška	k1gFnSc7	roznáška
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
pokusech	pokus	k1gInPc6	pokus
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
částečně	částečně	k6eAd1	částečně
ohluchl	ohluchnout	k5eAaPmAgMnS	ohluchnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
noviny	novina	k1gFnPc4	novina
–	–	k?	–
The	The	k1gFnSc1	The
Weekly	Weekl	k1gInPc4	Weekl
Herald.	Herald.	k1gFnSc2	Herald.
Vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
různá	různý	k2eAgNnPc4d1	různé
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
vynálezů	vynález	k1gInPc2	vynález
souviselo	souviset	k5eAaImAgNnS	souviset
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
telegrafista	telegrafista	k1gMnSc1	telegrafista
<g/>
.	.	kIx.	.
</s>
<s>
Tiskací	tiskací	k2eAgInSc4d1	tiskací
telegraf	telegraf	k1gInSc4	telegraf
<g/>
,	,	kIx,	,
duplexní	duplexní	k2eAgInSc4d1	duplexní
telegraf	telegraf	k1gInSc4	telegraf
a	a	k8xC	a
vícekanálový	vícekanálový	k2eAgInSc4d1	vícekanálový
automatický	automatický	k2eAgInSc4d1	automatický
telegraf	telegraf	k1gInSc4	telegraf
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
vynálezy	vynález	k1gInPc4	vynález
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Edison	Edison	k1gInSc1	Edison
prodal	prodat	k5eAaPmAgInS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Utržené	utržený	k2eAgFnPc1d1	utržená
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
investoval	investovat	k5eAaBmAgInS	investovat
do	do	k7c2	do
laboratoře	laboratoř	k1gFnSc2	laboratoř
v	v	k7c4	v
Menlo	Menlo	k1gNnSc4	Menlo
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
celosvětově	celosvětově	k6eAd1	celosvětově
prvním	první	k4xOgNnSc7	první
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
aplikace	aplikace	k1gFnSc2	aplikace
nových	nový	k2eAgInPc2d1	nový
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
-	-	kIx~	-
sčítač	sčítač	k1gInSc1	sčítač
hlasů	hlas	k1gInPc2	hlas
1869	[number]	k4	1869
-	-	kIx~	-
tiskací	tiskací	k2eAgInSc1d1	tiskací
telegraf	telegraf	k1gInSc1	telegraf
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
1871	[number]	k4	1871
-	-	kIx~	-
podstatné	podstatný	k2eAgNnSc1d1	podstatné
vylepšení	vylepšení	k1gNnSc1	vylepšení
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
1875	[number]	k4	1875
-	-	kIx~	-
duplexní	duplexní	k2eAgInSc1d1	duplexní
a	a	k8xC	a
automatický	automatický	k2eAgInSc1d1	automatický
telegraf	telegraf	k1gInSc1	telegraf
a	a	k8xC	a
základní	základní	k2eAgInSc1d1	základní
<g />
.	.	kIx.	.
</s>
<s>
principy	princip	k1gInPc1	princip
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telegrafie	telegrafie	k1gFnSc2	telegrafie
1876	[number]	k4	1876
-	-	kIx~	-
uhlíkový	uhlíkový	k2eAgInSc1d1	uhlíkový
reostat	reostat	k1gInSc1	reostat
1877	[number]	k4	1877
-	-	kIx~	-
cyklostyl	cyklostyl	k1gInSc1	cyklostyl
1877	[number]	k4	1877
-	-	kIx~	-
fonograf	fonograf	k1gInSc1	fonograf
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc4	jeho
vývoj	vývoj	k1gInSc4	vývoj
probíhal	probíhat	k5eAaImAgMnS	probíhat
dalších	další	k2eAgNnPc2d1	další
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
)	)	kIx)	)
1878	[number]	k4	1878
-	-	kIx~	-
uhlíkový	uhlíkový	k2eAgInSc1d1	uhlíkový
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
podstatné	podstatný	k2eAgNnSc1d1	podstatné
vylepšení	vylepšení	k1gNnSc1	vylepšení
Bellova	Bellův	k2eAgInSc2d1	Bellův
telefonu	telefon	k1gInSc2	telefon
1879	[number]	k4	1879
-	-	kIx~	-
žárovka	žárovka	k1gFnSc1	žárovka
-	-	kIx~	-
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydržela	vydržet	k5eAaPmAgFnS	vydržet
první	první	k4xOgFnSc1	první
pokusná	pokusný	k2eAgFnSc1d1	pokusná
žárovka	žárovka	k1gFnSc1	žárovka
svítit	svítit	k5eAaImF	svítit
13,5	[number]	k4	13,5
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
již	již	k6eAd1	již
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
žárovky	žárovka	k1gFnPc1	žárovka
svítily	svítit	k5eAaImAgFnP	svítit
přes	přes	k7c4	přes
1200	[number]	k4	1200
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
1880	[number]	k4	1880
-	-	kIx~	-
elektroměr	elektroměr	k1gInSc1	elektroměr
1880	[number]	k4	1880
-	-	kIx~	-
magnetický	magnetický	k2eAgInSc1d1	magnetický
třídič	třídič	k1gInSc1	třídič
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
1881	[number]	k4	1881
-	-	kIx~	-
dynamo	dynamo	k1gNnSc1	dynamo
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
1882	[number]	k4	1882
-	-	kIx~	-
elektrocentrála	elektrocentrála	k1gFnSc1	elektrocentrála
1882	[number]	k4	1882
-	-	kIx~	-
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
předvedl	předvést	k5eAaPmAgMnS	předvést
první	první	k4xOgFnSc4	první
elektrárnu	elektrárna	k1gFnSc4	elektrárna
a	a	k8xC	a
funkční	funkční	k2eAgInSc4d1	funkční
elektrický	elektrický	k2eAgInSc4d1	elektrický
rozvod	rozvod	k1gInSc4	rozvod
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgMnSc1	první
elektricky	elektricky	k6eAd1	elektricky
osvícený	osvícený	k2eAgInSc1d1	osvícený
vánoční	vánoční	k2eAgInSc1d1	vánoční
strom	strom	k1gInSc1	strom
1883	[number]	k4	1883
-	-	kIx~	-
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
laboratořích	laboratoř	k1gFnPc6	laboratoř
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Edisonův	Edisonův	k2eAgInSc1d1	Edisonův
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
elektronky	elektronka	k1gFnSc2	elektronka
1885	[number]	k4	1885
-	-	kIx~	-
tavná	tavný	k2eAgFnSc1d1	tavná
pojistka	pojistka	k1gFnSc1	pojistka
1889	[number]	k4	1889
-	-	kIx~	-
35	[number]	k4	35
mm	mm	kA	mm
široký	široký	k2eAgInSc1d1	široký
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
s	s	k7c7	s
perforací	perforace	k1gFnSc7	perforace
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
i	i	k9	i
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
letech	léto	k1gNnPc6	léto
1889	[number]	k4	1889
-	-	kIx~	-
kinematograf	kinematograf	k1gInSc1	kinematograf
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
kinetoskop	kinetoskop	k1gInSc4	kinetoskop
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
promítací	promítací	k2eAgInSc4d1	promítací
stroj	stroj	k1gInSc4	stroj
1896	[number]	k4	1896
-	-	kIx~	-
fluorescenční	fluorescenční	k2eAgNnSc1d1	fluorescenční
stínítko	stínítko	k1gNnSc1	stínítko
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
paprsků	paprsek	k1gInPc2	paprsek
1900	[number]	k4	1900
-	-	kIx~	-
akumulátor	akumulátor	k1gInSc1	akumulátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
NiFe	nife	k1gNnSc1	nife
1902	[number]	k4	1902
-	-	kIx~	-
elektromobil	elektromobil	k1gInSc1	elektromobil
1903	[number]	k4	1903
-	-	kIx~	-
rotační	rotační	k2eAgFnSc1d1	rotační
pec	pec	k1gFnSc1	pec
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cementu	cement	k1gInSc2	cement
1913	[number]	k4	1913
-	-	kIx~	-
synchronizace	synchronizace	k1gFnSc1	synchronizace
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
promítaného	promítaný	k2eAgInSc2d1	promítaný
filmu	film	k1gInSc2	film
1914	[number]	k4	1914
-	-	kIx~	-
gramofonová	gramofonový	k2eAgFnSc1d1	gramofonová
deska	deska	k1gFnSc1	deska
1930	[number]	k4	1930
-	-	kIx~	-
umělý	umělý	k2eAgInSc1d1	umělý
kaučuk	kaučuk	k1gInSc1	kaučuk
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
mnohých	mnohý	k2eAgInPc2d1	mnohý
vynálezů	vynález	k1gInPc2	vynález
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byly	být	k5eAaImAgFnP	být
opravdu	opravdu	k6eAd1	opravdu
poprvé	poprvé	k6eAd1	poprvé
vynalezeny	vynaleznout	k5eAaPmNgFnP	vynaleznout
Edisonem	Edison	k1gInSc7	Edison
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
již	již	k6eAd1	již
průkazně	průkazně	k6eAd1	průkazně
byly	být	k5eAaImAgInP	být
vynalezeny	vynaleznout	k5eAaPmNgInP	vynaleznout
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
Edison	Edison	k1gMnSc1	Edison
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
patentů	patent	k1gInPc2	patent
také	také	k9	také
sice	sice	k8xC	sice
patří	patřit	k5eAaImIp3nP	patřit
Edisonovi	Edisonův	k2eAgMnPc1d1	Edisonův
<g/>
,	,	kIx,	,
vynalezeny	vynaleznout	k5eAaPmNgFnP	vynaleznout
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
Edisonovými	Edisonův	k2eAgMnPc7d1	Edisonův
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
elektromobil	elektromobil	k1gInSc4	elektromobil
<g/>
,	,	kIx,	,
kinofilm	kinofilm	k1gInSc4	kinofilm
<g/>
,	,	kIx,	,
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
počátků	počátek	k1gInPc2	počátek
zavádění	zavádění	k1gNnSc2	zavádění
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
ostře	ostro	k6eAd1	ostro
diskutována	diskutován	k2eAgFnSc1d1	diskutována
otázka	otázka	k1gFnSc1	otázka
vhodnosti	vhodnost	k1gFnSc2	vhodnost
použití	použití	k1gNnSc2	použití
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
nebo	nebo	k8xC	nebo
střídavého	střídavý	k2eAgInSc2d1	střídavý
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gMnSc1	Edison
byl	být	k5eAaImAgMnS	být
stoupencem	stoupenec	k1gMnSc7	stoupenec
první	první	k4xOgFnSc2	první
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
zastáncem	zastánce	k1gMnSc7	zastánce
druhé	druhý	k4xOgFnSc6	druhý
byli	být	k5eAaImAgMnP	být
George	Georg	k1gMnSc4	Georg
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
a	a	k8xC	a
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gMnSc1	Tesla
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
kvůli	kvůli	k7c3	kvůli
efektivnějšímu	efektivní	k2eAgInSc3d2	efektivnější
přenosu	přenos	k1gInSc3	přenos
<g/>
,	,	kIx,	,
snazšímu	snadný	k2eAgNnSc3d2	snazší
dosažení	dosažení	k1gNnSc3	dosažení
vysokých	vysoká	k1gFnPc2	vysoká
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
mimo	mimo	k7c4	mimo
další	další	k2eAgInPc4d1	další
důvody	důvod	k1gInPc4	důvod
i	i	k8xC	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
točivého	točivý	k2eAgNnSc2d1	točivé
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
elektromotory	elektromotor	k1gInPc7	elektromotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gInSc1	Edison
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
odmítal	odmítat	k5eAaImAgMnS	odmítat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
údajného	údajný	k2eAgNnSc2d1	údajné
vyššího	vysoký	k2eAgNnSc2d2	vyšší
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
by	by	kYmCp3nP	by
v	v	k7c6	v
prohraném	prohraný	k2eAgInSc6d1	prohraný
sporu	spor	k1gInSc6	spor
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
nemalé	malý	k2eNgFnPc4d1	nemalá
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
již	již	k6eAd1	již
zavedené	zavedený	k2eAgFnSc2d1	zavedená
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Edison	Edison	k1gInSc1	Edison
prokázal	prokázat	k5eAaPmAgInS	prokázat
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
kampaň	kampaň	k1gFnSc4	kampaň
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
elektrického	elektrický	k2eAgNnSc2d1	elektrické
křesla	křeslo	k1gNnSc2	křeslo
a	a	k8xC	a
také	také	k9	také
pokusy	pokus	k1gInPc1	pokus
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
byla	být	k5eAaImAgFnS	být
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
zabíjena	zabíjen	k2eAgNnPc4d1	zabíjeno
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
usmrcení	usmrcení	k1gNnSc1	usmrcení
slonice	slonice	k1gFnSc2	slonice
Topsy	Topsa	k1gFnSc2	Topsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
(	(	kIx(	(
<g/>
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
skončila	skončit	k5eAaPmAgFnS	skončit
válka	válka	k1gFnSc1	válka
proudů	proud	k1gInPc2	proud
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
Edisonovi	Edison	k1gMnSc3	Edison
připisuje	připisovat	k5eAaImIp3nS	připisovat
neprávem	neprávo	k1gNnSc7	neprávo
<g/>
..	..	k?	..
Ránou	Rána	k1gFnSc7	Rána
Edisonově	Edisonův	k2eAgNnSc6d1	Edisonovo
snažení	snažení	k1gNnSc6	snažení
byla	být	k5eAaImAgFnS	být
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
Světové	světový	k2eAgFnSc2d1	světová
Kolumbovy	Kolumbův	k2eAgFnSc2d1	Kolumbova
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zajistila	zajistit	k5eAaPmAgFnS	zajistit
firma	firma	k1gFnSc1	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
Electric	Electrice	k1gFnPc2	Electrice
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
získala	získat	k5eAaPmAgFnS	získat
firma	firma	k1gFnSc1	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
Electric	Electrice	k1gFnPc2	Electrice
Corporation	Corporation	k1gInSc4	Corporation
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
na	na	k7c6	na
Niagarských	niagarský	k2eAgInPc6d1	niagarský
vodopádech	vodopád	k1gInPc6	vodopád
s	s	k7c7	s
rozvodným	rozvodný	k2eAgInSc7d1	rozvodný
systémem	systém	k1gInSc7	systém
využívajícím	využívající	k2eAgInSc7d1	využívající
transformátory	transformátor	k1gInPc4	transformátor
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
podíl	podíl	k1gInSc1	podíl
spotřebičů	spotřebič	k1gInPc2	spotřebič
napájených	napájený	k2eAgInPc2d1	napájený
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Edison	Edison	k1gMnSc1	Edison
válku	válka	k1gFnSc4	válka
proudů	proud	k1gInPc2	proud
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
šel	jít	k5eAaImAgMnS	jít
velmi	velmi	k6eAd1	velmi
pragmaticky	pragmaticky	k6eAd1	pragmaticky
a	a	k8xC	a
cílevědomě	cílevědomě	k6eAd1	cílevědomě
za	za	k7c7	za
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
bylo	být	k5eAaImAgNnS	být
využití	využití	k1gNnSc1	využití
jeho	jeho	k3xOp3gInPc2	jeho
nápadů	nápad	k1gInPc2	nápad
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
prodejných	prodejný	k2eAgInPc2d1	prodejný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
založil	založit	k5eAaPmAgMnS	založit
řadu	řada	k1gFnSc4	řada
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnával	zaměstnávat	k5eAaImAgInS	zaměstnávat
v	v	k7c4	v
nich	on	k3xPp3gNnPc2	on
mnoho	mnoho	k4c4	mnoho
nadaných	nadaný	k2eAgMnPc2d1	nadaný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
pečlivě	pečlivě	k6eAd1	pečlivě
osobně	osobně	k6eAd1	osobně
vybíral	vybírat	k5eAaImAgMnS	vybírat
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
Edisonových	Edisonův	k2eAgFnPc2d1	Edisonova
firem	firma	k1gFnPc2	firma
je	být	k5eAaImIp3nS	být
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gMnSc3	on
Edison	Edison	k1gInSc1	Edison
nezaplatil	zaplatit	k5eNaPmAgInS	zaplatit
za	za	k7c4	za
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
Edisonovou	Edisonový	k2eAgFnSc7d1	Edisonový
firmou	firma	k1gFnSc7	firma
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
založená	založený	k2eAgFnSc1d1	založená
Edison	Edison	k1gMnSc1	Edison
General	General	k1gMnPc2	General
Electric	Electric	k1gMnSc1	Electric
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
ve	v	k7c4	v
společnost	společnost	k1gFnSc4	společnost
General	General	k1gFnSc4	General
Electric	Electrice	k1gFnPc2	Electrice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
firem	firma	k1gFnPc2	firma
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
společností	společnost	k1gFnPc2	společnost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
filmové	filmový	k2eAgNnSc4d1	filmové
studio	studio	k1gNnSc4	studio
Black	Blacka	k1gFnPc2	Blacka
Maria	Mario	k1gMnSc2	Mario
v	v	k7c4	v
New	New	k1gFnSc4	New
Jersey	Jersea	k1gFnSc2	Jersea
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Edison	Edison	k1gMnSc1	Edison
Botanic	Botanice	k1gFnPc2	Botanice
Research	Research	k1gMnSc1	Research
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
hledala	hledat	k5eAaImAgFnS	hledat
náhražku	náhražka	k1gFnSc4	náhražka
přírodního	přírodní	k2eAgInSc2d1	přírodní
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
Edison	Edison	k1gInSc1	Edison
Illuminating	Illuminating	k1gInSc4	Illuminating
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
automobilového	automobilový	k2eAgMnSc2d1	automobilový
magnáta	magnát	k1gMnSc2	magnát
Henryho	Henry	k1gMnSc2	Henry
Forda	ford	k1gMnSc2	ford
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
Edisonovy	Edisonův	k2eAgFnSc2d1	Edisonova
smrti	smrt	k1gFnSc2	smrt
jeho	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgMnSc7d1	dobrý
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Edison	Edisona	k1gFnPc2	Edisona
Machine	Machin	k1gInSc5	Machin
Company	Compana	k1gFnSc2	Compana
započal	započnout	k5eAaPmAgInS	započnout
svoji	svůj	k3xOyFgFnSc4	svůj
praxi	praxe	k1gFnSc4	praxe
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
československých	československý	k2eAgMnPc2d1	československý
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Edisonovu	Edisonův	k2eAgFnSc4d1	Edisonova
pozornost	pozornost	k1gFnSc4	pozornost
zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
cesty	cesta	k1gFnPc1	cesta
fotografa	fotograf	k1gMnSc2	fotograf
Jamese	Jamese	k1gFnSc2	Jamese
Ricaltona	Ricalton	k1gMnSc2	Ricalton
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
financoval	financovat	k5eAaBmAgInS	financovat
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
bambusové	bambusový	k2eAgNnSc1d1	bambusové
vlákno	vlákno	k1gNnSc1	vlákno
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
žárovce	žárovka	k1gFnSc6	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Ricalton	Ricalton	k1gInSc1	Ricalton
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1888	[number]	k4	1888
odjel	odjet	k5eAaPmAgMnS	odjet
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
na	na	k7c4	na
Ceylon	Ceylon	k1gInSc4	Ceylon
přijel	přijet	k5eAaPmAgMnS	přijet
přes	přes	k7c4	přes
Suezský	suezský	k2eAgInSc4d1	suezský
průplav	průplav	k1gInSc4	průplav
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgMnS	navštívit
všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
testoval	testovat	k5eAaImAgMnS	testovat
stovky	stovka	k1gFnPc4	stovka
vzorků	vzorek	k1gInPc2	vzorek
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
do	do	k7c2	do
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
expertem	expert	k1gMnSc7	expert
na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
bambusu	bambus	k1gInSc2	bambus
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
odchodu	odchod	k1gInSc6	odchod
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
vzorků	vzorek	k1gInPc2	vzorek
pro	pro	k7c4	pro
Edisona	Edison	k1gMnSc4	Edison
a	a	k8xC	a
doporučením	doporučení	k1gNnPc3	doporučení
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vlákno	vlákno	k1gNnSc4	vlákno
Edison	Edisona	k1gFnPc2	Edisona
používal	používat	k5eAaImAgInS	používat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
devíti	devět	k4xCc2	devět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
našel	najít	k5eAaPmAgMnS	najít
vhodnější	vhodný	k2eAgInSc4d2	vhodnější
wolfram	wolfram	k1gInSc4	wolfram
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
A.	A.	kA	A.
Edison	Edison	k1gInSc1	Edison
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
praktickým	praktický	k2eAgInSc7d1	praktický
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
známých	známý	k2eAgInPc2d1	známý
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Genialita	genialita	k1gFnSc1	genialita
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
1	[number]	k4	1
%	%	kIx~	%
inspirace	inspirace	k1gFnSc1	inspirace
a	a	k8xC	a
99	[number]	k4	99
%	%	kIx~	%
potu	pot	k1gInSc2	pot
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Spát	spát	k5eAaImF	spát
lze	lze	k6eAd1	lze
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
spát	spát	k5eAaImF	spát
déle	dlouho	k6eAd2	dlouho
je	být	k5eAaImIp3nS	být
nemístný	místný	k2eNgInSc4d1	nemístný
přepych	přepych	k1gInSc4	přepych
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Tajemství	tajemství	k1gNnSc1	tajemství
úspěchu	úspěch	k1gInSc2	úspěch
v	v	k7c6	v
životě	život	k1gInSc6	život
není	být	k5eNaImIp3nS	být
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nalézat	nalézat	k5eAaImF	nalézat
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
děláme	dělat	k5eAaImIp1nP	dělat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Na	na	k7c6	na
léčení	léčení	k1gNnSc6	léčení
trápení	trápení	k1gNnPc2	trápení
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
láhev	láhev	k1gFnSc1	láhev
whisky	whisky	k1gFnSc2	whisky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Jediná	jediný	k2eAgFnSc1d1	jediná
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
často	často	k6eAd1	často
ztrácíme	ztrácet	k5eAaImIp1nP	ztrácet
trpělivost	trpělivost	k1gFnSc4	trpělivost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hodinky	hodinka	k1gFnPc1	hodinka
-	-	kIx~	-
ručičky	ručička	k1gFnPc1	ručička
jdou	jít	k5eAaImIp3nP	jít
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Když	když	k8xS	když
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
Edison	Edison	k1gInSc1	Edison
slavný	slavný	k2eAgInSc1d1	slavný
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
novinář	novinář	k1gMnSc1	novinář
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
:	:	kIx,	:
Pane	Pan	k1gMnSc5	Pan
Edisone	Edison	k1gMnSc5	Edison
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
žárovku	žárovka	k1gFnSc4	žárovka
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
jste	být	k5eAaImIp2nP	být
znal	znát	k5eAaImAgMnS	znát
Ohmův	Ohmův	k2eAgInSc4d1	Ohmův
zákon	zákon	k1gInSc4	zákon
<g/>
?	?	kIx.	?
</s>
<s>
Odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
Neznal	znát	k5eNaImAgMnS	znát
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdybych	kdyby	kYmCp1nS	kdyby
ho	on	k3xPp3gMnSc4	on
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
tak	tak	k9	tak
by	by	kYmCp3nS	by
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
zdržovalo	zdržovat	k5eAaImAgNnS	zdržovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vynalézal	vynalézat	k5eAaImAgMnS	vynalézat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
donekonečna	donekonečna	k6eAd1	donekonečna
prováděl	provádět	k5eAaImAgMnS	provádět
pokusy	pokus	k1gInPc7	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nechtěli	chtít	k5eNaImAgMnP	chtít
moc	moc	k6eAd1	moc
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
profesoři	profesor	k1gMnPc1	profesor
připomínali	připomínat	k5eAaImAgMnP	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
Edisonů	Edison	k1gMnPc2	Edison
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
minula	minout	k5eAaImAgFnS	minout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
neobyčejnou	obyčejný	k2eNgFnSc4d1	neobyčejná
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
obdiv	obdiv	k1gInSc1	obdiv
vůči	vůči	k7c3	vůči
každému	každý	k3xTgMnSc3	každý
inženýrovi	inženýr	k1gMnSc3	inženýr
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vůči	vůči	k7c3	vůči
největšímu	veliký	k2eAgInSc3d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
–	–	k?	–
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
