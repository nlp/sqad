<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
OF	OF	kA	OF
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
"	"	kIx"	"
<g/>
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
"	"	kIx"	"
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xS	jako
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
spontánní	spontánní	k2eAgFnSc1d1	spontánní
platforma	platforma	k1gFnSc1	platforma
občanských	občanský	k2eAgFnPc2d1	občanská
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
OF	OF	kA	OF
odmítalo	odmítat	k5eAaImAgNnS	odmítat
totalitní	totalitní	k2eAgInSc4d1	totalitní
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
OF	OF	kA	OF
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
politicky	politicky	k6eAd1	politicky
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
neformálním	formální	k2eNgMnSc7d1	neformální
lídrem	lídr	k1gMnSc7	lídr
hnutí	hnutí	k1gNnSc2	hnutí
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
jediným	jediný	k2eAgMnSc7d1	jediný
předsedou	předseda	k1gMnSc7	předseda
OF	OF	kA	OF
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
roli	role	k1gFnSc4	role
hrálo	hrát	k5eAaImAgNnS	hrát
hnutí	hnutí	k1gNnSc4	hnutí
Verejnosť	Verejnosť	k1gFnSc1	Verejnosť
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
(	(	kIx(	(
<g/>
VPN	VPN	kA	VPN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
brutální	brutální	k2eAgInSc4d1	brutální
zásah	zásah	k1gInSc4	zásah
proti	proti	k7c3	proti
studentské	studentský	k2eAgFnSc3d1	studentská
demonstraci	demonstrace	k1gFnSc3	demonstrace
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgNnSc4d1	úvodní
prohlášení	prohlášení	k1gNnSc4	prohlášení
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc1	sdružení
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
Činoherním	činoherní	k2eAgInSc6d1	činoherní
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
založení	založení	k1gNnSc2	založení
bylo	být	k5eAaImAgNnS	být
začít	začít	k5eAaPmF	začít
vést	vést	k5eAaImF	vést
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
lišilo	lišit	k5eAaImAgNnS	lišit
širším	široký	k2eAgNnSc7d2	širší
polem	pole	k1gNnSc7	pole
působnosti	působnost	k1gFnSc2	působnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
i	i	k9	i
představitelé	představitel	k1gMnPc1	představitel
studentských	studentský	k2eAgInPc2d1	studentský
stávkových	stávkový	k2eAgInPc2d1	stávkový
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
široké	široký	k2eAgNnSc1d1	široké
spektrum	spektrum	k1gNnSc1	spektrum
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
levice	levice	k1gFnSc2	levice
i	i	k8xC	i
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
společným	společný	k2eAgInSc7d1	společný
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
demokratické	demokratický	k2eAgFnSc3d1	demokratická
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nestraníků	nestraník	k1gMnPc2	nestraník
kandidovalo	kandidovat	k5eAaImAgNnS	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
OF	OF	kA	OF
čtrnáct	čtrnáct	k4xCc4	čtrnáct
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
orientace	orientace	k1gFnSc1	orientace
sahala	sahat	k5eAaImAgFnS	sahat
od	od	k7c2	od
reformního	reformní	k2eAgInSc2d1	reformní
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
Obroda	obroda	k1gFnSc1	obroda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
krajní	krajní	k2eAgFnSc1d1	krajní
levice	levice	k1gFnSc1	levice
(	(	kIx(	(
<g/>
Levá	levý	k2eAgFnSc1d1	levá
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
)	)	kIx)	)
až	až	k9	až
ke	k	k7c3	k
konzervatismu	konzervatismus	k1gInSc3	konzervatismus
(	(	kIx(	(
<g/>
ODA	ODA	kA	ODA
<g/>
)	)	kIx)	)
či	či	k8xC	či
levicového	levicový	k2eAgInSc2d1	levicový
libertarianismu	libertarianismus	k1gInSc2	libertarianismus
(	(	kIx(	(
<g/>
Transnacionální	Transnacionální	k2eAgFnSc1d1	Transnacionální
radikální	radikální	k2eAgFnSc1d1	radikální
<g />
.	.	kIx.	.
</s>
<s>
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Agrární	agrární	k2eAgFnSc1d1	agrární
strana	strana	k1gFnSc1	strana
při	při	k7c6	při
Občanském	občanský	k2eAgNnSc6d1	občanské
fóru	fórum	k1gNnSc6	fórum
Liberálně	liberálně	k6eAd1	liberálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Československá	československý	k2eAgFnSc1d1	Československá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
iniciativa	iniciativa	k1gFnSc1	iniciativa
<g/>
)	)	kIx)	)
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
Humanistické	humanistický	k2eAgFnSc2d1	humanistická
internacionály	internacionála	k1gFnSc2	internacionála
Hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
občanskou	občanský	k2eAgFnSc4d1	občanská
svobodu	svoboda	k1gFnSc4	svoboda
Klub	klub	k1gInSc1	klub
angažovaných	angažovaný	k2eAgMnPc2d1	angažovaný
nestraníků	nestraník	k1gMnPc2	nestraník
Klub	klub	k1gInSc1	klub
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
Křesťansko-sociální	křesťanskoociální	k2eAgFnSc1d1	křesťansko-sociální
strana	strana	k1gFnSc1	strana
Levá	levá	k1gFnSc1	levá
alternativa	alternativa	k1gFnSc1	alternativa
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
demokratické	demokratický	k2eAgNnSc4d1	demokratické
hnutí	hnutí	k1gNnSc4	hnutí
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
aliance	aliance	k1gFnSc1	aliance
Obroda	obroda	k1gFnSc1	obroda
Romská	romský	k2eAgFnSc1d1	romská
občanská	občanský	k2eAgFnSc1d1	občanská
iniciativa	iniciativa	k1gFnSc1	iniciativa
Strana	strana	k1gFnSc1	strana
obrany	obrana	k1gFnSc2	obrana
kultury	kultura	k1gFnSc2	kultura
Transnacionální	Transnacionální	k2eAgFnSc1d1	Transnacionální
radikální	radikální	k2eAgFnSc1d1	radikální
strana	strana	k1gFnSc1	strana
V	v	k7c6	v
září	září	k1gNnSc6	září
1990	[number]	k4	1990
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Daniela	Daniel	k1gMnSc2	Daniel
Kroupy	Kroupa	k1gMnSc2	Kroupa
zformoval	zformovat	k5eAaPmAgInS	zformovat
Meziparlamentní	meziparlamentní	k2eAgInSc1d1	meziparlamentní
klub	klub	k1gInSc1	klub
demokratické	demokratický	k2eAgFnSc2d1	demokratická
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
frakcí	frakce	k1gFnSc7	frakce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
Liberální	liberální	k2eAgInSc1d1	liberální
klub	klub	k1gInSc1	klub
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
početně	početně	k6eAd1	početně
slabší	slabý	k2eAgMnPc1d2	slabší
(	(	kIx(	(
<g/>
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
např.	např.	kA	např.
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc1	Pithart
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
nebo	nebo	k8xC	nebo
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názorově	názorově	k6eAd1	názorově
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
Klub	klub	k1gInSc1	klub
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
také	také	k9	také
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
vytýkal	vytýkat	k5eAaImAgInS	vytýkat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
fóra	fórum	k1gNnSc2	fórum
odklon	odklon	k1gInSc4	odklon
napravo	napravo	k6eAd1	napravo
a	a	k8xC	a
chybějící	chybějící	k2eAgInSc1d1	chybějící
sociální	sociální	k2eAgInSc1d1	sociální
a	a	k8xC	a
ekologický	ekologický	k2eAgInSc1d1	ekologický
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Liberálové	liberál	k1gMnPc1	liberál
a	a	k8xC	a
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
předsedovi	předseda	k1gMnSc3	předseda
Klausovi	Klaus	k1gMnSc3	Klaus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
ale	ale	k8xC	ale
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
jako	jako	k8xC	jako
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
stranictví	stranictví	k1gNnSc3	stranictví
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
volnou	volný	k2eAgFnSc4d1	volná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
nemělo	mít	k5eNaImAgNnS	mít
ani	ani	k8xC	ani
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Hybatelem	hybatel	k1gMnSc7	hybatel
dění	dění	k1gNnSc2	dění
fóra	fórum	k1gNnSc2	fórum
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stával	stávat	k5eAaImAgInS	stávat
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
orgán	orgán	k1gInSc1	orgán
Koordinačního	koordinační	k2eAgNnSc2d1	koordinační
centra	centrum	k1gNnSc2	centrum
OF	OF	kA	OF
(	(	kIx(	(
<g/>
KC	KC	kA	KC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
lídr	lídr	k1gMnSc1	lídr
celého	celý	k2eAgNnSc2d1	celé
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dá	dát	k5eAaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
post	post	k1gInSc4	post
hlavního	hlavní	k2eAgMnSc2d1	hlavní
představitele	představitel	k1gMnSc2	představitel
KC	KC	kA	KC
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
byl	být	k5eAaImAgInS	být
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přípravám	příprava	k1gFnPc3	příprava
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
ČSSR	ČSSR	kA	ČSSR
přebírá	přebírat	k5eAaImIp3nS	přebírat
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
právník	právník	k1gMnSc1	právník
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc4	Pithart
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
projevem	projev	k1gInSc7	projev
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
překřtěné	překřtěný	k2eAgFnSc2d1	překřtěná
na	na	k7c6	na
"	"	kIx"	"
<g/>
možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyším	slyšet	k5eAaImIp1nS	slyšet
trávu	tráva	k1gFnSc4	tráva
růst	růst	k1gInSc1	růst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
Úřad	úřad	k1gInSc4	úřad
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
novinář	novinář	k1gMnSc1	novinář
Jan	Jan	k1gMnSc1	Jan
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dovádí	dovádět	k5eAaImIp3nS	dovádět
hnutí	hnutí	k1gNnSc4	hnutí
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
svobodným	svobodný	k2eAgFnPc3d1	svobodná
parlamentním	parlamentní	k2eAgFnPc3d1	parlamentní
volbám	volba	k1gFnPc3	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k8xC	však
kandidovat	kandidovat	k5eAaImF	kandidovat
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
už	už	k6eAd1	už
dopředu	dopředu	k6eAd1	dopředu
avizované	avizovaný	k2eAgNnSc4d1	avizované
odstoupení	odstoupení	k1gNnSc4	odstoupení
po	po	k7c6	po
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
den	den	k1gInSc4	den
po	po	k7c4	po
oznámení	oznámení	k1gNnSc4	oznámení
volebních	volební	k2eAgInPc2d1	volební
výsledků	výsledek	k1gInPc2	výsledek
rezignací	rezignace	k1gFnPc2	rezignace
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
fóra	fórum	k1gNnSc2	fórum
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
postavili	postavit	k5eAaPmAgMnP	postavit
tři	tři	k4xCgMnPc1	tři
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
členové	člen	k1gMnPc1	člen
KC	KC	kA	KC
Petr	Petr	k1gMnSc1	Petr
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sedláček	Sedláček	k1gMnSc1	Sedláček
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Fišera	Fišer	k1gMnSc2	Fišer
<g/>
.	.	kIx.	.
</s>
<s>
Proměňující	proměňující	k2eAgFnSc1d1	proměňující
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
regionálních	regionální	k2eAgNnPc2d1	regionální
fór	fórum	k1gNnPc2	fórum
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
způsobily	způsobit	k5eAaPmAgFnP	způsobit
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
OF	OF	kA	OF
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
konající	konající	k2eAgInPc4d1	konající
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
stanovy	stanova	k1gFnPc1	stanova
zde	zde	k6eAd1	zde
schválené	schválený	k2eAgFnPc1d1	schválená
počítaly	počítat	k5eAaImAgFnP	počítat
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
nového	nový	k2eAgMnSc2d1	nový
kolektivního	kolektivní	k2eAgMnSc2d1	kolektivní
lídra	lídr	k1gMnSc2	lídr
OF	OF	kA	OF
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představovali	představovat	k5eAaImAgMnP	představovat
čtyři	čtyři	k4xCgMnPc1	čtyři
představitelé	představitel	k1gMnPc1	představitel
následně	následně	k6eAd1	následně
zvolení	zvolený	k2eAgMnPc1d1	zvolený
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
Palouš	Palouš	k1gMnSc1	Palouš
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kučera	Kučera	k1gMnSc1	Kučera
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
(	(	kIx(	(
<g/>
švagrová	švagrová	k1gFnSc1	švagrová
V.	V.	kA	V.
Havla	Havel	k1gMnSc2	Havel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
na	na	k7c6	na
tomtéž	týž	k3xTgInSc6	týž
sněmu	sněm	k1gInSc6	sněm
však	však	k9	však
byla	být	k5eAaImAgFnS	být
položena	položen	k2eAgFnSc1d1	položena
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
není	být	k5eNaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
ustanovit	ustanovit	k5eAaPmF	ustanovit
přímo	přímo	k6eAd1	přímo
předsedu	předseda	k1gMnSc4	předseda
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Palouš	Palouš	k1gMnSc1	Palouš
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
představitelů	představitel	k1gMnPc2	představitel
OF	OF	kA	OF
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
volbě	volba	k1gFnSc6	volba
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
přirozeně	přirozeně	k6eAd1	přirozeně
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jako	jako	k8xS	jako
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
prohavlovskou	prohavlovský	k2eAgFnSc7d1	prohavlovská
částí	část	k1gFnSc7	část
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
Protikandidátem	protikandidát	k1gMnSc7	protikandidát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
-	-	kIx~	-
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
federální	federální	k2eAgMnSc1d1	federální
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
podporovaly	podporovat	k5eAaImAgInP	podporovat
zejména	zejména	k9	zejména
regiony	region	k1gInPc1	region
a	a	k8xC	a
Meziparlamentní	meziparlamentní	k2eAgInSc1d1	meziparlamentní
klub	klub	k1gInSc1	klub
demokratické	demokratický	k2eAgFnSc2d1	demokratická
pravice	pravice	k1gFnSc2	pravice
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
kandidátem	kandidát	k1gMnSc7	kandidát
byl	být	k5eAaImAgMnS	být
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
kandidatury	kandidatura	k1gFnPc1	kandidatura
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Hostivaři	Hostivař	k1gFnSc6	Hostivař
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
115	[number]	k4	115
ku	k	k7c3	k
52	[number]	k4	52
hlasům	hlas	k1gInPc3	hlas
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
OF	OF	kA	OF
právě	právě	k9	právě
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
nově	nově	k6eAd1	nově
schválené	schválený	k2eAgFnPc1d1	schválená
pravomoci	pravomoc	k1gFnPc1	pravomoc
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
mohl	moct	k5eAaImAgMnS	moct
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
transformací	transformace	k1gFnSc7	transformace
OF	OF	kA	OF
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hnutí	hnutí	k1gNnSc2	hnutí
ke	k	k7c3	k
straně	strana	k1gFnSc3	strana
(	(	kIx(	(
<g/>
sněm	sněm	k1gInSc1	sněm
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
rozpory	rozpor	k1gInPc1	rozpor
dvou	dva	k4xCgInPc2	dva
politických	politický	k2eAgMnPc2d1	politický
proudů	proud	k1gInPc2	proud
však	však	k9	však
způsobily	způsobit	k5eAaPmAgInP	způsobit
postupný	postupný	k2eAgInSc4d1	postupný
rozpad	rozpad	k1gInSc4	rozpad
OF	OF	kA	OF
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nastal	nastat	k5eAaPmAgInS	nastat
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zaniká	zanikat	k5eAaImIp3nS	zanikat
předsednické	předsednický	k2eAgFnPc4d1	předsednická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
posledním	poslední	k2eAgMnSc7d1	poslední
předsedou	předseda	k1gMnSc7	předseda
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
fórum	fórum	k1gNnSc4	fórum
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Jan	Jan	k1gMnSc1	Jan
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
východním	východní	k2eAgNnSc7d1	východní
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
Neues	Neues	k1gInSc1	Neues
Forum	forum	k1gNnSc1	forum
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
fórum	fórum	k1gNnSc1	fórum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
adjektivum	adjektivum	k1gNnSc4	adjektivum
občanské	občanský	k2eAgFnSc2d1	občanská
přidal	přidat	k5eAaPmAgMnS	přidat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Logem	log	k1gInSc7	log
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
bylo	být	k5eAaImAgNnS	být
modré	modrý	k2eAgNnSc1d1	modré
písmeno	písmeno	k1gNnSc1	písmeno
O	O	kA	O
se	s	k7c7	s
vmalovanýma	vmalovaný	k2eAgNnPc7d1	vmalovaný
modrýma	modrý	k2eAgNnPc7d1	modré
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
úsměvem	úsměv	k1gInSc7	úsměv
a	a	k8xC	a
červené	červený	k2eAgNnSc1d1	červené
písmeno	písmeno	k1gNnSc1	písmeno
F.	F.	kA	F.
Logo	logo	k1gNnSc1	logo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
na	na	k7c6	na
základě	základ	k1gInSc6	základ
neformální	formální	k2eNgFnSc2d1	neformální
jednodenní	jednodenní	k2eAgFnSc2d1	jednodenní
rychlosoutěže	rychlosoutěž	k1gFnSc2	rychlosoutěž
mezi	mezi	k7c7	mezi
středními	střední	k2eAgFnPc7d1	střední
a	a	k8xC	a
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
vyhlášené	vyhlášený	k2eAgFnSc2d1	vyhlášená
Občanským	občanský	k2eAgNnSc7d1	občanské
fórem	fórum	k1gNnSc7	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Vybráno	vybrán	k2eAgNnSc1d1	vybráno
bylo	být	k5eAaImAgNnS	být
logo	logo	k1gNnSc1	logo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
student	student	k1gMnSc1	student
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
<g/>
,	,	kIx,	,
oboru	obor	k1gInSc6	obor
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
knižní	knižní	k2eAgFnSc2d1	knižní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
Pavel	Pavel	k1gMnSc1	Pavel
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
prý	prý	k9	prý
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
čtvrt	čtvrt	k1gFnSc4	čtvrt
hodiny	hodina	k1gFnSc2	hodina
před	před	k7c7	před
uzávěrkou	uzávěrka	k1gFnSc7	uzávěrka
soutěže	soutěž	k1gFnSc2	soutěž
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
žertů	žert	k1gInPc2	žert
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
bavili	bavit	k5eAaImAgMnP	bavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
návrh	návrh	k1gInSc4	návrh
seriózního	seriózní	k2eAgMnSc2d1	seriózní
<g/>
,	,	kIx,	,
technického	technický	k2eAgNnSc2d1	technické
loga	logo	k1gNnSc2	logo
již	již	k9	již
nestačí	stačit	k5eNaBmIp3nS	stačit
nikdo	nikdo	k3yNnSc1	nikdo
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Šťastný	Šťastný	k1gMnSc1	Šťastný
pak	pak	k6eAd1	pak
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
veškerou	veškerý	k3xTgFnSc4	veškerý
grafiku	grafika	k1gFnSc4	grafika
pro	pro	k7c4	pro
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
grafiky	grafika	k1gFnSc2	grafika
návrhu	návrh	k1gInSc2	návrh
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
tohoto	tento	k3xDgNnSc2	tento
loga	logo	k1gNnSc2	logo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
též	též	k9	též
autorem	autor	k1gMnSc7	autor
log	log	k1gInSc4	log
pro	pro	k7c4	pro
Datart	Datart	k1gInSc4	Datart
<g/>
,	,	kIx,	,
E-Gate	E-Gat	k1gMnSc5	E-Gat
<g/>
,	,	kIx,	,
Seznam	seznámit	k5eAaPmRp2nS	seznámit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Slovanský	slovanský	k2eAgInSc1d1	slovanský
dům	dům	k1gInSc1	dům
a	a	k8xC	a
Banánové	banánový	k2eAgFnPc1d1	banánová
rybičky	rybička	k1gFnPc1	rybička
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
aliance	aliance	k1gFnSc1	aliance
-	-	kIx~	-
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
kandidují	kandidovat	k5eAaImIp3nP	kandidovat
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
na	na	k7c6	na
kandidátkách	kandidátka	k1gFnPc6	kandidátka
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
strana	strana	k1gFnSc1	strana
působí	působit	k5eAaImIp3nS	působit
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
-	-	kIx~	-
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
při	při	k7c6	při
definitivním	definitivní	k2eAgNnSc6d1	definitivní
rozštěpení	rozštěpení	k1gNnSc6	rozštěpení
OF	OF	kA	OF
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
vůdcem	vůdce	k1gMnSc7	vůdce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
hnutí	hnutí	k1gNnSc1	hnutí
-	-	kIx~	-
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
při	při	k7c6	při
definitivním	definitivní	k2eAgNnSc6d1	definitivní
rozštěpení	rozštěpení	k1gNnSc6	rozštěpení
OF	OF	kA	OF
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Liberálního	liberální	k2eAgInSc2d1	liberální
klubu	klub	k1gInSc2	klub
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Svobodné	svobodný	k2eAgMnPc4d1	svobodný
demokraty	demokrat	k1gMnPc4	demokrat
a	a	k8xC	a
ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
sloučila	sloučit	k5eAaPmAgFnS	sloučit
s	s	k7c7	s
Národními	národní	k2eAgMnPc7d1	národní
socialisty	socialist	k1gMnPc7	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
pro	pro	k7c4	pro
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
společnost	společnost	k1gFnSc4	společnost
-	-	kIx~	-
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
ČSNS	ČSNS	kA	ČSNS
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
odcházejí	odcházet	k5eAaImIp3nP	odcházet
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
někteří	některý	k3yIgMnPc1	některý
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
demokraté	demokrat	k1gMnPc1	demokrat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
Stranu	strana	k1gFnSc4	strana
pro	pro	k7c4	pro
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
SUK	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
:	:	kIx,	:
Listopad	listopad	k1gInSc1	listopad
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85270	[number]	k4	85270
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7239	[number]	k4	7239
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
Trutnov	Trutnov	k1gInSc4	Trutnov
Zrychlený	zrychlený	k2eAgInSc1d1	zrychlený
tep	tep	k1gInSc1	tep
dějin	dějiny	k1gFnPc2	dějiny
Marta	Marta	k1gFnSc1	Marta
Kubišová	Kubišová	k1gFnSc1	Kubišová
Kruh	kruh	k1gInSc1	kruh
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
inteligence	inteligence	k1gFnSc2	inteligence
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Občanské	občanský	k2eAgFnSc2d1	občanská
fórum	fórum	k1gNnSc4	fórum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Projekt	projekt	k1gInSc1	projekt
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
videa	video	k1gNnSc2	video
<g/>
,	,	kIx,	,
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
chronologie	chronologie	k1gFnPc4	chronologie
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
sametový	sametový	k2eAgInSc4d1	sametový
kýč	kýč	k1gInSc4	kýč
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k6eAd1	plus
Ekologická	ekologický	k2eAgFnSc1d1	ekologická
osina	osina	k1gFnSc1	osina
v	v	k7c6	v
polistopadové	polistopadový	k2eAgFnSc6d1	polistopadová
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
unikátních	unikátní	k2eAgInPc2d1	unikátní
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
záznamů	záznam	k1gInPc2	záznam
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
až	až	k6eAd1	až
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
okologickou	okologický	k2eAgFnSc7d1	okologický
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
