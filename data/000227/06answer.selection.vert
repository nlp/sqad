<s>
Prvním	první	k4xOgMnSc7	první
neformálním	formální	k2eNgMnSc7d1	neformální
lídrem	lídr	k1gMnSc7	lídr
hnutí	hnutí	k1gNnSc2	hnutí
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
jediným	jediný	k2eAgMnSc7d1	jediný
předsedou	předseda	k1gMnSc7	předseda
OF	OF	kA	OF
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
