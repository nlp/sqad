<p>
<s>
Sládek	Sládek	k1gMnSc1	Sládek
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
vaření	vaření	k1gNnSc2	vaření
piva	pivo	k1gNnSc2	pivo
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
technologický	technologický	k2eAgInSc4d1	technologický
proces	proces	k1gInSc4	proces
výroby	výroba	k1gFnSc2	výroba
od	od	k7c2	od
přípravy	příprava	k1gFnSc2	příprava
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vystírání	vystírání	k1gNnSc4	vystírání
<g/>
,	,	kIx,	,
rmutování	rmutování	k1gNnSc4	rmutování
<g/>
,	,	kIx,	,
schlazení	schlazení	k1gNnSc4	schlazení
<g/>
,	,	kIx,	,
dozrávání	dozrávání	k1gNnSc4	dozrávání
až	až	k9	až
po	po	k7c4	po
finální	finální	k2eAgNnSc4d1	finální
stáčení	stáčení	k1gNnSc4	stáčení
piva	pivo	k1gNnSc2	pivo
do	do	k7c2	do
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
lahví	lahev	k1gFnPc2	lahev
nebo	nebo	k8xC	nebo
plechovek	plechovka	k1gFnPc2	plechovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
pivovarském	pivovarský	k2eAgInSc6d1	pivovarský
provozu	provoz	k1gInSc6	provoz
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
technologa	technolog	k1gMnSc2	technolog
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
a	a	k8xC	a
zásahy	zásah	k1gInPc1	zásah
sládka	sládek	k1gMnSc2	sládek
do	do	k7c2	do
výrobního	výrobní	k2eAgInSc2d1	výrobní
procesu	proces	k1gInSc2	proces
mají	mít	k5eAaImIp3nP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
konečnou	konečný	k2eAgFnSc4d1	konečná
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
sládci	sládek	k1gMnPc1	sládek
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Ondřej	Ondřej	k1gMnSc1	Ondřej
Poupě	Poupě	k1gMnSc1	Poupě
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
–	–	k?	–
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
–	–	k?	–
český	český	k2eAgMnSc1d1	český
sládek	sládek	k1gMnSc1	sládek
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
českého	český	k2eAgNnSc2d1	české
odborného	odborný	k2eAgNnSc2d1	odborné
pivovarnictví	pivovarnictví	k1gNnSc2	pivovarnictví
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Groll	Groll	k1gMnSc1	Groll
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
–	–	k?	–
bavorský	bavorský	k2eAgMnSc1d1	bavorský
sládek	sládek	k1gMnSc1	sládek
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc2	tvůrce
piva	pivo	k1gNnSc2	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Černohorský	Černohorský	k1gMnSc1	Černohorský
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
český	český	k2eAgMnSc1d1	český
sládek	sládek	k1gMnSc1	sládek
a	a	k8xC	a
výzkumník	výzkumník	k1gMnSc1	výzkumník
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
piva	pivo	k1gNnSc2	pivo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sládek	sládek	k1gMnSc1	sládek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sládek	sládek	k1gMnSc1	sládek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
