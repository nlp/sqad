<s>
V	v	k7c6
případě	případ	k1gInSc6
ohrožení	ohrožení	k1gNnSc2
si	se	k3xPyFc3
žába	žába	k1gFnSc1
zlomí	zlomit	k5eAaPmIp3nS
kůstky	kůstka	k1gFnPc4
na	na	k7c6
prstech	prst	k1gInPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
ostré	ostrý	k2eAgFnPc1d1
špice	špice	k1gFnPc1
pak	pak	k6eAd1
prorazí	prorazit	k5eAaPmIp3nP
kůži	kůže	k1gFnSc4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
obraně	obrana	k1gFnSc3
jako	jako	k8xC
drápy	dráp	k1gInPc1
(	(	kIx(
<g/>
nejde	jít	k5eNaImIp3nS
o	o	k7c4
skutečné	skutečný	k2eAgInPc4d1
drápy	dráp	k1gInPc4
<g/>
,	,	kIx,
protože	protože	k8xS
nejsou	být	k5eNaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
keratinem	keratin	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
přezdívku	přezdívka	k1gFnSc4
Wolverine	Wolverine	k1gInSc1
frog	frog	k1gMnSc1
podle	podle	k7c2
postavy	postava	k1gFnSc2
z	z	k7c2
komiksu	komiks	k1gInSc2
X-Men	X-Man	k1gInPc1
<g/>
.	.	kIx.
</s>