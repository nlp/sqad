<s>
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
</s>
<s>
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
obojživelníci	obojživelník	k1gMnPc1
(	(	kIx(
<g/>
Amphibia	Amphibia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
žáby	žába	k1gFnPc1
(	(	kIx(
<g/>
Anura	Anura	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kvikuňkovití	kvikuňkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Arthroleptidae	Arthroleptidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
vlasatice	vlasatice	k1gFnSc1
(	(	kIx(
<g/>
Trichobatrachus	Trichobatrachus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Trichobatrachus	Trichobatrachus	k1gMnSc1
robustusBoulenger	robustusBoulenger	k1gMnSc1
<g/>
,	,	kIx,
1900	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Astylosternus	Astylosternus	k1gMnSc1
robustus	robustus	k1gMnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Trichobatrachus	Trichobatrachus	k1gMnSc1
robustus	robustus	k2eAgMnSc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
žába	žába	k1gFnSc1
srstnatá	srstnatý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
žáby	žába	k1gFnSc2
z	z	k7c2
monotypického	monotypický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Trichobatrachus	Trichobatrachus	k1gInSc1
<g/>
,	,	kIx,
obývající	obývající	k2eAgInPc1d1
deštné	deštný	k2eAgInPc1d1
pralesy	prales	k1gInPc1
podél	podél	k7c2
Guinejského	guinejský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
od	od	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Nigérie	Nigérie	k1gFnSc2
po	po	k7c4
vysočinu	vysočina	k1gFnSc4
Mayombe	Mayomb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS
délky	délka	k1gFnPc4
okolo	okolo	k7c2
11	#num#	k4
cm	cm	kA
a	a	k8xC
váhy	váha	k1gFnSc2
asi	asi	k9
80	#num#	k4
g	g	kA
<g/>
,	,	kIx,
hlava	hlava	k1gFnSc1
je	být	k5eAaImIp3nS
krátká	krátký	k2eAgFnSc1d1
a	a	k8xC
široká	široký	k2eAgFnSc1d1
se	se	k3xPyFc4
zakulaceným	zakulacený	k2eAgInSc7d1
čenichem	čenich	k1gInSc7
<g/>
,	,	kIx,
samci	samec	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
větší	veliký	k2eAgFnPc1d2
než	než	k8xS
samice	samice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
pohlavního	pohlavní	k2eAgInSc2d1
dimorfismu	dimorfismus	k1gInSc2
je	být	k5eAaImIp3nS
také	také	k9
množství	množství	k1gNnSc1
dlouhých	dlouhý	k2eAgInPc2d1
tenkých	tenký	k2eAgInPc2d1
kožních	kožní	k2eAgInPc2d1
výrůstků	výrůstek	k1gInPc2
na	na	k7c6
bocích	bok	k1gInPc6
a	a	k8xC
bedrech	bedra	k1gNnPc6
samců	samec	k1gInPc2
<g/>
,	,	kIx,
sloužících	sloužící	k2eAgInPc2d1
k	k	k7c3
usnadnění	usnadnění	k1gNnSc3
dýchání	dýchání	k1gNnSc2
při	při	k7c6
pobytu	pobyt	k1gInSc6
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
hlídají	hlídat	k5eAaImIp3nP
nakladená	nakladený	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vylíhnutí	vylíhnutí	k1gNnSc6
pulců	pulec	k1gMnPc2
se	se	k3xPyFc4
samci	samec	k1gMnPc1
vracejí	vracet	k5eAaImIp3nP
na	na	k7c4
souš	souš	k1gFnSc4
a	a	k8xC
kožní	kožní	k2eAgNnSc4d1
třepení	třepení	k1gNnSc4
ztrácejí	ztrácet	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
ohrožení	ohrožení	k1gNnSc2
si	se	k3xPyFc3
žába	žába	k1gFnSc1
zlomí	zlomit	k5eAaPmIp3nS
kůstky	kůstka	k1gFnPc4
na	na	k7c6
prstech	prst	k1gInPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
ostré	ostrý	k2eAgFnPc1d1
špice	špice	k1gFnPc1
pak	pak	k6eAd1
prorazí	prorazit	k5eAaPmIp3nP
kůži	kůže	k1gFnSc4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
obraně	obrana	k1gFnSc3
jako	jako	k8xC
drápy	dráp	k1gInPc1
(	(	kIx(
<g/>
nejde	jít	k5eNaImIp3nS
o	o	k7c4
skutečné	skutečný	k2eAgInPc4d1
drápy	dráp	k1gInPc4
<g/>
,	,	kIx,
protože	protože	k8xS
nejsou	být	k5eNaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
keratinem	keratin	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
přezdívku	přezdívka	k1gFnSc4
Wolverine	Wolverine	k1gInSc1
frog	frog	k1gMnSc1
podle	podle	k7c2
postavy	postava	k1gFnSc2
z	z	k7c2
komiksu	komiks	k1gInSc2
X-Men	X-Man	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Potravu	potrava	k1gFnSc4
vlasatice	vlasatice	k1gFnSc2
třásnité	třásnitý	k2eAgNnSc1d1
tvoří	tvořit	k5eAaImIp3nS
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
plži	plž	k1gMnPc1
a	a	k8xC
červi	červ	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dožívá	dožívat	k5eAaImIp3nS
se	s	k7c7
zhruba	zhruba	k6eAd1
pěti	pět	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Domorodci	domorodec	k1gMnPc1
z	z	k7c2
kmene	kmen	k1gInSc2
Bakossi	Bakosse	k1gFnSc4
tuto	tento	k3xDgFnSc4
žábu	žába	k1gFnSc4
loví	lovit	k5eAaImIp3nP
pomocí	pomocí	k7c2
oštěpů	oštěp	k1gInPc2
a	a	k8xC
pojídají	pojídat	k5eAaImIp3nP
jako	jako	k9
lék	lék	k1gInSc4
proti	proti	k7c3
neplodnosti	neplodnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
AmphibiaWeb	AmphibiaWba	k1gFnPc2
<g/>
↑	↑	k?
Squeakers	Squeakers	k1gInSc1
and	and	k?
Cricket	Cricket	k1gInSc1
Frogs	Frogs	k1gInSc1
<g/>
:	:	kIx,
Arthroleptidae	Arthroleptidae	k1gInSc1
-	-	kIx~
Hairy	Hair	k1gInPc1
Frog	Froga	k1gFnPc2
<g/>
↑	↑	k?
Planet	planeta	k1gFnPc2
Save	Save	k1gFnSc1
<g/>
↑	↑	k?
Wild	Wild	k1gInSc1
Animals	Animals	k1gInSc1
Online	Onlin	k1gInSc5
<g/>
.	.	kIx.
www.wildanimalsonline.com	www.wildanimalsonline.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Salamandra	salamandr	k1gMnSc2
Journal	Journal	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Trichobatrachus	Trichobatrachus	k1gInSc1
robustus	robustus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Obojživelníci	obojživelník	k1gMnPc1
</s>
