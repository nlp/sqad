<s>
Vlasatice	vlasatice	k1gFnSc1
třásnitá	třásnitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Trichobatrachus	Trichobatrachus	k1gMnSc1
robustus	robustus	k2eAgMnSc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
žába	žába	k1gFnSc1
srstnatá	srstnatý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
žáby	žába	k1gFnSc2
z	z	k7c2
monotypického	monotypický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Trichobatrachus	Trichobatrachus	k1gInSc1
<g/>
,	,	kIx,
obývající	obývající	k2eAgInPc1d1
deštné	deštný	k2eAgInPc1d1
pralesy	prales	k1gInPc1
podél	podél	k7c2
Guinejského	guinejský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
od	od	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Nigérie	Nigérie	k1gFnSc2
po	po	k7c4
vysočinu	vysočina	k1gFnSc4
Mayombe	Mayomb	k1gMnSc5
<g/>
.	.	kIx.
</s>