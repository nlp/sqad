<p>
<s>
Hostina	hostina	k1gFnSc1	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
(	(	kIx(	(
<g/>
A	a	k9	a
Feast	Feast	k1gFnSc4	Feast
for	forum	k1gNnPc2	forum
Crows	Crows	k1gInSc1	Crows
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
fantasy	fantas	k1gInPc4	fantas
série	série	k1gFnSc1	série
Píseň	píseň	k1gFnSc1	píseň
ledu	led	k1gInSc2	led
a	a	k8xC	a
ohně	oheň	k1gInSc2	oheň
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
autora	autor	k1gMnSc2	autor
George	Georg	k1gMnSc2	Georg
R.	R.	kA	R.
R.	R.	kA	R.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
události	událost	k1gFnPc4	událost
předchozí	předchozí	k2eAgFnSc2d1	předchozí
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
Bouře	bouř	k1gFnSc2	bouř
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Hostina	hostina	k1gFnSc1	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
knize	kniha	k1gFnSc6	kniha
v	v	k7c6	v
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
odehrávat	odehrávat	k5eAaImF	odehrávat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
ale	ale	k8xC	ale
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
styl	styl	k1gInSc1	styl
psaní	psaní	k1gNnSc2	psaní
by	by	kYmCp3nS	by
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
množství	množství	k1gNnSc4	množství
flashbacků	flashback	k1gInPc2	flashback
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
časovou	časový	k2eAgFnSc4d1	časová
mezeru	mezera	k1gFnSc4	mezera
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
roce	rok	k1gInSc6	rok
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
psaní	psaní	k1gNnSc2	psaní
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
George	Georg	k1gInSc2	Georg
Martin	Martin	k1gMnSc1	Martin
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
manuskriptu	manuskript	k1gInSc2	manuskript
jeho	jeho	k3xOp3gNnSc2	jeho
samotného	samotný	k2eAgNnSc2d1	samotné
a	a	k8xC	a
vydavatele	vydavatel	k1gMnPc4	vydavatel
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kapitoly	kapitola	k1gFnPc1	kapitola
vyšly	vyjít	k5eAaPmAgFnP	vyjít
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
různých	různý	k2eAgFnPc6d1	různá
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Nerozhodl	rozhodnout	k5eNaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
však	však	k9	však
text	text	k1gInSc1	text
rozdělit	rozdělit	k5eAaPmF	rozdělit
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
lokací	lokace	k1gFnPc2	lokace
příběhu	příběh	k1gInSc2	příběh
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
měly	mít	k5eAaImAgInP	mít
zčásti	zčásti	k6eAd1	zčásti
odehrávat	odehrávat	k5eAaImF	odehrávat
ve	v	k7c4	v
zhruba	zhruba	k6eAd1	zhruba
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
víceméně	víceméně	k9	víceméně
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hostina	hostina	k1gFnSc1	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
věnuje	věnovat	k5eAaImIp3nS	věnovat
událostem	událost	k1gFnPc3	událost
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
přístavišti	přístaviště	k1gNnSc6	přístaviště
<g/>
,	,	kIx,	,
v	v	k7c6	v
Řekotočí	Řekotočí	k1gFnSc6	Řekotočí
<g/>
,	,	kIx,	,
na	na	k7c6	na
Železných	železný	k2eAgInPc6d1	železný
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
v	v	k7c6	v
Dorne	Dorn	k1gMnSc5	Dorn
<g/>
.	.	kIx.	.
</s>
<s>
Hostina	hostina	k1gFnSc1	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
vyšla	vyjít	k5eAaPmAgFnS	vyjít
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
dějových	dějový	k2eAgFnPc2d1	dějová
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následující	následující	k2eAgFnSc1d1	následující
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Tanec	tanec	k1gInSc1	tanec
s	s	k7c7	s
draky	drak	k1gMnPc7	drak
až	až	k6eAd1	až
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
také	také	k9	také
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
nejspíš	nejspíš	k9	nejspíš
bude	být	k5eAaImBp3nS	být
sága	sága	k1gFnSc1	sága
sestávat	sestávat	k5eAaImF	sestávat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Locus	Locus	k1gMnSc1	Locus
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
jako	jako	k8xC	jako
předchozí	předchozí	k2eAgInPc4d1	předchozí
3	[number]	k4	3
díly	díl	k1gInPc4	díl
ságy	sága	k1gFnSc2	sága
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
svazků	svazek	k1gInPc2	svazek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
regentka	regentka	k1gFnSc1	regentka
Cersei	Cerse	k1gFnSc2	Cerse
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vládnout	vládnout	k5eAaImF	vládnout
Sedmi	sedm	k4xCc2	sedm
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
paranoia	paranoia	k1gFnSc1	paranoia
a	a	k8xC	a
pýcha	pýcha	k1gFnSc1	pýcha
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
zaslepuje	zaslepovat	k5eAaImIp3nS	zaslepovat
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
jí	on	k3xPp3gFnSc7	on
nikdo	nikdo	k3yNnSc1	nikdo
nestojí	stát	k5eNaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sama	sám	k3xTgFnSc1	sám
de	de	k?	de
facto	facto	k1gNnSc1	facto
vládla	vládnout	k5eAaImAgFnS	vládnout
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
ironicky	ironicky	k6eAd1	ironicky
však	však	k9	však
vychází	vycházet	k5eAaImIp3nS	vycházet
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yRnSc4	co
Cersei	Cersei	k1gNnSc1	Cersei
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
svého	svůj	k3xOyFgMnSc2	svůj
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Tommena	Tommen	k2eAgFnSc1d1	Tommen
<g/>
,	,	kIx,	,
korunovat	korunovat	k5eAaBmF	korunovat
a	a	k8xC	a
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
Margery	Marger	k1gMnPc7	Marger
Tyrell	Tyrell	k1gMnSc1	Tyrell
<g/>
.	.	kIx.	.
</s>
<s>
Tyrelly	Tyrella	k1gFnPc1	Tyrella
ale	ale	k9	ale
nenávidí	nenávidět	k5eAaImIp3nP	nenávidět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
obkopuje	obkopovat	k5eAaImIp3nS	obkopovat
neschopnými	schopný	k2eNgMnPc7d1	neschopný
pochlebovači	pochlebovač	k1gMnPc7	pochlebovač
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
si	se	k3xPyFc3	se
nevědomky	nevědomky	k6eAd1	nevědomky
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Nechává	nechávat	k5eAaImIp3nS	nechávat
hledat	hledat	k5eAaImF	hledat
Tyriona	Tyriona	k1gFnSc1	Tyriona
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
vidí	vidět	k5eAaImIp3nS	vidět
za	za	k7c7	za
každým	každý	k3xTgInSc7	každý
komplotem	komplot	k1gInSc7	komplot
<g/>
,	,	kIx,	,
a	a	k8xC	a
znepřátelí	znepřátelit	k5eAaPmIp3nS	znepřátelit
si	se	k3xPyFc3	se
i	i	k9	i
bratra	bratr	k1gMnSc4	bratr
Jaimeho	Jaime	k1gMnSc4	Jaime
<g/>
.	.	kIx.	.
</s>
<s>
Intrikuje	intrikovat	k5eAaImIp3nS	intrikovat
proti	proti	k7c3	proti
Margery	Margera	k1gFnSc2	Margera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
obviněná	obviněný	k2eAgFnSc1d1	obviněná
z	z	k7c2	z
nevěry	nevěra	k1gFnSc2	nevěra
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
hříchů	hřích	k1gInPc2	hřích
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Margery	Margera	k1gFnSc2	Margera
po	po	k7c6	po
právu	právo	k1gNnSc6	právo
<g/>
)	)	kIx)	)
obviněna	obviněn	k2eAgFnSc1d1	obviněna
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
samého	samý	k3xTgMnSc4	samý
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
dopis	dopis	k1gInSc4	dopis
Jaimemu	Jaimem	k1gInSc2	Jaimem
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaime	Jaimat	k5eAaPmIp3nS	Jaimat
Lannister	Lannister	k1gInSc1	Lannister
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
Řekotočí	Řekotočí	k1gFnSc2	Řekotočí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
navrátil	navrátit	k5eAaPmAgInS	navrátit
do	do	k7c2	do
Tommenových	Tommenův	k2eAgFnPc2d1	Tommenův
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Tullyů	Tully	k1gInPc2	Tully
nakonec	nakonec	k6eAd1	nakonec
dobude	dobýt	k5eAaPmIp3nS	dobýt
bez	bez	k7c2	bez
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
když	když	k8xS	když
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
Edmura	Edmura	k1gFnSc1	Edmura
Tullyho	Tully	k1gMnSc2	Tully
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
poslaný	poslaný	k2eAgMnSc1d1	poslaný
jako	jako	k8xC	jako
zajatec	zajatec	k1gMnSc1	zajatec
do	do	k7c2	do
Casterlyovy	Casterlyův	k2eAgFnSc2d1	Casterlyův
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
Lannisterů	Lannister	k1gInPc2	Lannister
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
i	i	k9	i
s	s	k7c7	s
Robbovou	Robbův	k2eAgFnSc7d1	Robbův
vdovou	vdova	k1gFnSc7	vdova
Jeyne	Jeyn	k1gInSc5	Jeyn
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pošle	poslat	k5eAaPmIp3nS	poslat
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přidělí	přidělit	k5eAaPmIp3nS	přidělit
jim	on	k3xPp3gMnPc3	on
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
Jeyne	Jeyn	k1gInSc5	Jeyn
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Novými	nový	k2eAgMnPc7d1	nový
vládci	vládce	k1gMnPc7	vládce
Řekotočí	Řekotočí	k1gFnSc2	Řekotočí
jsou	být	k5eAaImIp3nP	být
Tommenovým	Tommenův	k2eAgInSc7d1	Tommenův
výnosem	výnos	k1gInSc7	výnos
stanoveni	stanoven	k2eAgMnPc1d1	stanoven
Freyové	Freyus	k1gMnPc1	Freyus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ale	ale	k9	ale
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
povrchní	povrchní	k2eAgFnSc4d1	povrchní
podporu	podpora	k1gFnSc4	podpora
říčních	říční	k2eAgMnPc2d1	říční
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brienne	Briennout	k5eAaImIp3nS	Briennout
z	z	k7c2	z
Tarthu	Tarth	k1gInSc2	Tarth
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
splnit	splnit	k5eAaPmF	splnit
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jí	jíst	k5eAaImIp3nS	jíst
dal	dát	k5eAaPmAgMnS	dát
Jaime	Jaim	k1gInSc5	Jaim
Lannister	Lannister	k1gInSc1	Lannister
-	-	kIx~	-
najít	najít	k5eAaPmF	najít
Sansu	Sans	k1gInSc3	Sans
Stark	Stark	k1gInSc4	Stark
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Putuje	putovat	k5eAaImIp3nS	putovat
územím	území	k1gNnSc7	území
zpustošeným	zpustošený	k2eAgNnSc7d1	zpustošené
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInSc1	její
úkol	úkol	k1gInSc1	úkol
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
beznadějný	beznadějný	k2eAgMnSc1d1	beznadějný
(	(	kIx(	(
<g/>
čtenář	čtenář	k1gMnSc1	čtenář
ostatně	ostatně	k6eAd1	ostatně
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sansa	Sansa	k1gFnSc1	Sansa
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Baelišem	Baeliš	k1gMnSc7	Baeliš
v	v	k7c6	v
Údolí	údolí	k1gNnSc6	údolí
Arryn	Arryna	k1gFnPc2	Arryna
<g/>
)	)	kIx)	)
a	a	k8xC	a
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
nemrtvou	mrtvý	k2eNgFnSc7d1	nemrtvá
Catelyn	Catelyn	k1gInSc1	Catelyn
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
její	její	k3xOp3gFnSc4	její
historce	historka	k1gFnSc6	historka
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
buď	buď	k8xC	buď
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
Jaimeho	Jaime	k1gMnSc4	Jaime
Lannistera	Lannister	k1gMnSc4	Lannister
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čelit	čelit	k5eAaImF	čelit
oprátce	oprátka	k1gFnSc3	oprátka
<g/>
.	.	kIx.	.
</s>
<s>
Brienne	Briennout	k5eAaImIp3nS	Briennout
si	se	k3xPyFc3	se
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Jaimem	Jaim	k1gMnSc7	Jaim
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pouto	pouto	k1gNnSc1	pouto
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
dusit	dusit	k5eAaImF	dusit
na	na	k7c6	na
oprátce	oprátka	k1gFnSc6	oprátka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
jedno	jeden	k4xCgNnSc1	jeden
neznámé	známý	k2eNgNnSc1d1	neznámé
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítel	přítel	k1gMnSc1	přítel
Jona	Jona	k1gMnSc1	Jona
Sněha	Sněha	k1gMnSc1	Sněha
Samwell	Samwell	k1gMnSc1	Samwell
Tarly	Tarla	k1gFnSc2	Tarla
putuje	putovat	k5eAaImIp3nS	putovat
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
v	v	k7c6	v
Citadele	citadela	k1gFnSc6	citadela
Mistrem	mistr	k1gMnSc7	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
království	království	k1gNnSc6	království
Dorne	Dorn	k1gInSc5	Dorn
se	se	k3xPyFc4	se
dcery	dcera	k1gFnSc2	dcera
zabitého	zabitý	k2eAgMnSc2d1	zabitý
prince	princ	k1gMnSc2	princ
Oberyna	Oberyn	k1gMnSc2	Oberyn
snaží	snažit	k5eAaImIp3nP	snažit
přimět	přimět	k5eAaPmF	přimět
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc5	vládce
Dorne	Dorn	k1gMnSc5	Dorn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pomstil	pomstit	k5eAaImAgMnS	pomstit
Lannisterům	Lannister	k1gMnPc3	Lannister
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
jejich	jejich	k3xOp3gMnSc2	jejich
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Doran	Doran	k1gMnSc1	Doran
však	však	k9	však
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
míru	mír	k1gInSc2	mír
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Arianne	Ariann	k1gInSc5	Ariann
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
snaží	snažit	k5eAaImIp3nP	snažit
intrikovat	intrikovat	k5eAaImF	intrikovat
a	a	k8xC	a
jmenovat	jmenovat	k5eAaImF	jmenovat
princeznu	princezna	k1gFnSc4	princezna
Myrcellu	Myrcell	k1gInSc2	Myrcell
<g/>
,	,	kIx,	,
sestru	sestra	k1gFnSc4	sestra
krále	král	k1gMnSc4	král
Tommena	Tommen	k2eAgMnSc4d1	Tommen
a	a	k8xC	a
snoubenku	snoubenka	k1gFnSc4	snoubenka
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přiměla	přimět	k5eAaPmAgFnS	přimět
Dorne	Dorn	k1gInSc5	Dorn
k	k	k7c3	k
akci	akce	k1gFnSc4	akce
proti	proti	k7c3	proti
Lannisterum	Lannisterum	k1gNnSc4	Lannisterum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Železných	železný	k2eAgInPc6d1	železný
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
stává	stávat	k5eAaImIp3nS	stávat
Euron	Euron	k1gMnSc1	Euron
Greyjoy	Greyjoa	k1gFnSc2	Greyjoa
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Balona	Balona	k1gFnSc1	Balona
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Theona	Theona	k1gFnSc1	Theona
a	a	k8xC	a
Aši	Aš	k1gFnSc3	Aš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
magického	magický	k2eAgInSc2d1	magický
rohu	roh	k1gInSc2	roh
chce	chtít	k5eAaImIp3nS	chtít
zmocnit	zmocnit	k5eAaPmF	zmocnit
draků	drak	k1gInPc2	drak
královny	královna	k1gFnSc2	královna
Daenerys	Daenerysa	k1gFnPc2	Daenerysa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vypravěči	vypravěč	k1gMnPc5	vypravěč
==	==	k?	==
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
kapitola	kapitola	k1gFnSc1	kapitola
je	být	k5eAaImIp3nS	být
vyprávěna	vyprávět	k5eAaImNgFnS	vyprávět
v	v	k7c6	v
er-formě	erorma	k1gFnSc6	er-forma
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
stav	stav	k1gInSc1	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
silně	silně	k6eAd1	silně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
styl	styl	k1gInSc4	styl
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pohledů	pohled	k1gInPc2	pohled
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
nezaujatý	zaujatý	k2eNgMnSc1d1	nezaujatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hostině	hostina	k1gFnSc6	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
je	být	k5eAaImIp3nS	být
dvanáct	dvanáct	k4xCc1	dvanáct
takových	takový	k3xDgFnPc2	takový
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
vypravěč	vypravěč	k1gMnSc1	vypravěč
prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
vypravěči	vypravěč	k1gMnPc1	vypravěč
===	===	k?	===
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
<g/>
:	:	kIx,	:
Pate	pat	k1gInSc5	pat
<g/>
,	,	kIx,	,
učedník	učedník	k1gMnSc1	učedník
Citadely	citadela	k1gFnSc2	citadela
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cersei	Cersei	k6eAd1	Cersei
Lannister	Lannister	k1gInSc1	Lannister
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
regentka	regentka	k1gFnSc1	regentka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sansa	Sansa	k1gFnSc1	Sansa
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Eddarda	Eddarda	k1gFnSc1	Eddarda
a	a	k8xC	a
Catelyn	Catelyn	k1gNnSc1	Catelyn
Stark	Stark	k1gInSc4	Stark
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
psána	psán	k2eAgFnSc1d1	psána
jako	jako	k8xS	jako
Alayne	Alayn	k1gInSc5	Alayn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arya	Arya	k6eAd1	Arya
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Eddarda	Eddarda	k1gFnSc1	Eddarda
a	a	k8xC	a
Catelyn	Catelyn	k1gNnSc1	Catelyn
Stark	Stark	k1gInSc1	Stark
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
psána	psát	k5eAaImNgFnS	psát
jako	jako	k8xC	jako
Kočka	kočka	k1gFnSc1	kočka
z	z	k7c2	z
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ser	srát	k5eAaImRp2nS	srát
Jaime	Jaim	k1gMnSc5	Jaim
Lannister	Lannister	k1gMnSc1	Lannister
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
královny	královna	k1gFnSc2	královna
Cersei	Cerse	k1gFnSc2	Cerse
a	a	k8xC	a
Tyriona	Tyrion	k1gMnSc4	Tyrion
Lannistera	Lannister	k1gMnSc4	Lannister
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
lorda	lord	k1gMnSc2	lord
Tywina	Tywino	k1gNnSc2	Tywino
Lannistera	Lannister	k1gMnSc2	Lannister
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brienne	Briennout	k5eAaPmIp3nS	Briennout
z	z	k7c2	z
Tarthu	Tarth	k1gInSc2	Tarth
<g/>
,	,	kIx,	,
bojovnice	bojovnice	k1gFnSc1	bojovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samwell	Samwell	k1gInSc1	Samwell
Tarly	Tarla	k1gFnSc2	Tarla
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Noční	noční	k2eAgFnSc2d1	noční
hlídky	hlídka	k1gFnSc2	hlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vedlejší	vedlejší	k2eAgMnPc1d1	vedlejší
vypravěči	vypravěč	k1gMnPc1	vypravěč
===	===	k?	===
</s>
</p>
<p>
<s>
Prorok	prorok	k1gMnSc1	prorok
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Utopený	utopený	k1gMnSc1	utopený
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Aeron	Aeron	k1gMnSc1	Aeron
Greyjoy	Greyjoa	k1gFnSc2	Greyjoa
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Balona	Balon	k1gMnSc2	Balon
Greyjoye	Greyjoye	k1gNnSc2	Greyjoye
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dcera	dcera	k1gFnSc1	dcera
Krakatice	krakatice	k1gFnSc1	krakatice
<g/>
,	,	kIx,	,
Aša	Aša	k1gFnSc1	Aša
Greyjoy	Greyjoa	k1gFnSc2	Greyjoa
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Balona	Balona	k1gFnSc1	Balona
Greyjoye	Greyjoye	k1gFnSc1	Greyjoye
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železný	Železný	k1gMnSc1	Železný
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Plenitel	plenitel	k1gMnSc1	plenitel
<g/>
,	,	kIx,	,
Victarion	Victarion	k1gInSc1	Victarion
Greyjoy	Greyjoa	k1gFnSc2	Greyjoa
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Balona	Balon	k1gMnSc2	Balon
Greyjoye	Greyjoy	k1gFnSc2	Greyjoy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
stráží	strážit	k5eAaImIp3nS	strážit
<g/>
,	,	kIx,	,
Areo	area	k1gFnSc5	area
Hotah	Hotah	k1gMnSc1	Hotah
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
strážce	strážce	k1gMnSc1	strážce
prince	princ	k1gMnSc2	princ
Dorana	Doran	k1gMnSc2	Doran
Martella	Martell	k1gMnSc2	Martell
z	z	k7c2	z
Dorne	Dorn	k1gInSc5	Dorn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poskvrněný	poskvrněný	k2eAgMnSc1d1	poskvrněný
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
Arys	Arys	k1gInSc1	Arys
Oakheart	Oakheart	k1gInSc1	Oakheart
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
královské	královský	k2eAgFnSc2d1	královská
gardy	garda	k1gFnSc2	garda
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
stráž	stráž	k1gFnSc1	stráž
princezny	princezna	k1gFnSc2	princezna
Myrcelly	Myrcella	k1gFnSc2	Myrcella
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královnotvůrce	Královnotvůrec	k1gInPc1	Královnotvůrec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Princezna	princezna	k1gFnSc1	princezna
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
Arianne	Ariann	k1gInSc5	Ariann
Martell	Martell	k1gInSc1	Martell
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
prince	princ	k1gMnSc2	princ
Dorana	Doran	k1gMnSc2	Doran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Hostina	hostina	k1gFnSc1	hostina
pro	pro	k7c4	pro
vrány	vrána	k1gFnPc4	vrána
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
