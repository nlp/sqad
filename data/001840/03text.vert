<s>
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
carolinii	carolinie	k1gFnSc3	carolinie
(	(	kIx(	(
<g/>
Z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
obří	obří	k2eAgMnSc1d1	obří
jižní	jižní	k2eAgMnSc1d1	jižní
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
masožravých	masožravý	k2eAgMnPc2d1	masožravý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
teropodů	teropod	k1gMnPc2	teropod
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
carcharodontosauridae	carcharodontosauridae	k1gNnSc2	carcharodontosauridae
<g/>
,	,	kIx,	,
známým	známý	k2eAgInSc7d1	známý
podle	podle	k7c2	podle
dobře	dobře	k6eAd1	dobře
zachované	zachovaný	k2eAgFnSc2d1	zachovaná
kostry	kostra	k1gFnSc2	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
obří	obří	k2eAgMnSc1d1	obří
dravec	dravec	k1gMnSc1	dravec
žil	žít	k5eAaImAgMnS	žít
asi	asi	k9	asi
před	před	k7c7	před
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
miliony	milion	k4xCgInPc1	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
automechanikem	automechanik	k1gMnSc7	automechanik
a	a	k8xC	a
sběratelem	sběratel	k1gMnSc7	sběratel
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
Rubenem	Ruben	k1gMnSc7	Ruben
Carolinim	Carolinima	k1gFnPc2	Carolinima
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
popsán	popsat	k5eAaPmNgMnS	popsat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
podle	podle	k7c2	podle
kostry	kostra	k1gFnSc2	kostra
kompletní	kompletní	k2eAgInSc1d1	kompletní
asi	asi	k9	asi
ze	z	k7c2	z
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
druhého	druhý	k4xOgMnSc4	druhý
největšího	veliký	k2eAgMnSc4d3	veliký
známého	známý	k2eAgMnSc4d1	známý
teropoda	teropod	k1gMnSc4	teropod
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Velikost	velikost	k1gFnSc1	velikost
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
populárního	populární	k2eAgMnSc4d1	populární
tyranosaura	tyranosaur	k1gMnSc4	tyranosaur
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
tabulky	tabulka	k1gFnSc2	tabulka
velikosti	velikost	k1gFnSc2	velikost
masožravých	masožravý	k2eAgMnPc2d1	masožravý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
předchozí	předchozí	k2eAgMnSc1d1	předchozí
rekordman	rekordman	k1gMnSc1	rekordman
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
právem	právem	k6eAd1	právem
<g/>
,	,	kIx,	,
držel	držet	k5eAaImAgMnS	držet
celých	celý	k2eAgNnPc2d1	celé
90	[number]	k4	90
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
měl	mít	k5eAaImAgMnS	mít
celkově	celkově	k6eAd1	celkově
delší	dlouhý	k2eAgFnSc4d2	delší
lebku	lebka	k1gFnSc4	lebka
(	(	kIx(	(
<g/>
160	[number]	k4	160
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
možná	možná	k9	možná
až	až	k9	až
200	[number]	k4	200
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
m	m	kA	m
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
okolo	okolo	k7c2	okolo
8	[number]	k4	8
tun	tuna	k1gFnPc2	tuna
-	-	kIx~	-
největší	veliký	k2eAgNnSc1d3	veliký
dobře	dobře	k6eAd1	dobře
známý	známý	k2eAgMnSc1d1	známý
jedinec	jedinec	k1gMnSc1	jedinec
tyranosaura	tyranosaur	k1gMnSc2	tyranosaur
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
o	o	k7c6	o
1	[number]	k4	1
metr	metr	k1gInSc1	metr
kratší	krátký	k2eAgInSc1d2	kratší
a	a	k8xC	a
vážil	vážit	k5eAaImAgInS	vážit
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
1	[number]	k4	1
tunu	tuna	k1gFnSc4	tuna
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
mozek	mozek	k1gInSc4	mozek
giganotosaura	giganotosaur	k1gMnSc2	giganotosaur
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
pokročilejšího	pokročilý	k2eAgMnSc2d2	pokročilejší
celurosauridního	celurosauridní	k2eAgMnSc2d1	celurosauridní
teropoda	teropod	k1gMnSc2	teropod
tyranosaura	tyranosaur	k1gMnSc2	tyranosaur
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
měl	mít	k5eAaImAgMnS	mít
sice	sice	k8xC	sice
lebku	lebka	k1gFnSc4	lebka
téměř	téměř	k6eAd1	téměř
až	až	k9	až
o	o	k7c4	o
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
tyranosaurus	tyranosaurus	k1gInSc4	tyranosaurus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývalý	bývalý	k2eAgMnSc1d1	bývalý
rekordman	rekordman	k1gMnSc1	rekordman
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
větších	veliký	k2eAgInPc2d2	veliký
teropodů	teropod	k1gInPc2	teropod
jako	jako	k9	jako
byl	být	k5eAaImAgInS	být
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
či	či	k8xC	či
Spinosaurus	Spinosaurus	k1gInSc1	Spinosaurus
mnohem	mnohem	k6eAd1	mnohem
mohutnější	mohutný	k2eAgInSc1d2	mohutnější
a	a	k8xC	a
více	hodně	k6eAd2	hodně
přizpůsobenou	přizpůsobený	k2eAgFnSc4d1	přizpůsobená
drtivým	drtivý	k2eAgInPc3d1	drtivý
nárazům	náraz	k1gInPc3	náraz
a	a	k8xC	a
čelistnímu	čelistní	k2eAgInSc3d1	čelistní
stisku	stisk	k1gInSc3	stisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fosílií	fosílie	k1gFnPc2	fosílie
objevených	objevený	k2eAgFnPc2d1	objevená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
1997	[number]	k4	1997
až	až	k9	až
2001	[number]	k4	2001
popsán	popsán	k2eAgInSc4d1	popsán
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
velmi	velmi	k6eAd1	velmi
blízce	blízce	k6eAd1	blízce
příbuzného	příbuzný	k2eAgMnSc4d1	příbuzný
teropoda	teropod	k1gMnSc4	teropod
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
vědecké	vědecký	k2eAgNnSc4d1	vědecké
jméno	jméno	k1gNnSc4	jméno
Mapusaurus	Mapusaurus	k1gMnSc1	Mapusaurus
roseae	rosea	k1gInSc2	rosea
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
prokazatelně	prokazatelně	k6eAd1	prokazatelně
dorůstal	dorůstat	k5eAaImAgInS	dorůstat
délky	délka	k1gFnSc2	délka
12,2	[number]	k4	12,2
<g/>
-	-	kIx~	-
<g/>
12,6	[number]	k4	12,6
m	m	kA	m
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
však	však	k9	však
být	být	k5eAaImF	být
celkově	celkově	k6eAd1	celkově
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologové	paleontolog	k1gMnPc1	paleontolog
Blanco	blanco	k6eAd1	blanco
a	a	k8xC	a
Mazzetta	Mazzetta	k1gFnSc1	Mazzetta
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
biomechanickou	biomechanický	k2eAgFnSc4d1	biomechanická
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
lokomoce	lokomoce	k1gFnPc1	lokomoce
(	(	kIx(	(
<g/>
pohybových	pohybový	k2eAgFnPc2d1	pohybová
schopností	schopnost	k1gFnPc2	schopnost
<g/>
)	)	kIx)	)
giganotosaura	giganotosaura	k1gFnSc1	giganotosaura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
výsledků	výsledek	k1gInPc2	výsledek
dokázal	dokázat	k5eAaPmAgInS	dokázat
tento	tento	k3xDgInSc4	tento
obří	obří	k2eAgInSc4d1	obří
teropod	teropod	k1gInSc4	teropod
i	i	k9	i
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
hmotnosti	hmotnost	k1gFnSc6	hmotnost
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
5-8	[number]	k4	5-8
tun	tuna	k1gFnPc2	tuna
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
rychlosti	rychlost	k1gFnSc2	rychlost
až	až	k9	až
14	[number]	k4	14
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
50,4	[number]	k4	50,4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
nepochybně	pochybně	k6eNd1	pochybně
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nejrychlejším	rychlý	k2eAgInPc3d3	nejrychlejší
velkým	velký	k2eAgInPc3d1	velký
teropodům	teropod	k1gInPc3	teropod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
výpočet	výpočet	k1gInSc1	výpočet
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
obří	obří	k2eAgMnSc1d1	obří
dravec	dravec	k1gMnSc1	dravec
běhal	běhat	k5eAaImAgMnS	běhat
skoro	skoro	k6eAd1	skoro
o	o	k7c4	o
6	[number]	k4	6
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k9	kolik
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
nejrychlejší	rychlý	k2eAgMnPc1d3	nejrychlejší
lidští	lidský	k2eAgMnPc1d1	lidský
sprinteři	sprinter	k1gMnPc1	sprinter
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Usain	Usain	k2eAgInSc1d1	Usain
Bolt	Bolt	k1gInSc1	Bolt
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
rody	rod	k1gInPc7	rod
Tyrannotitan	Tyrannotitan	k1gInSc1	Tyrannotitan
<g/>
,	,	kIx,	,
Mapusaurus	Mapusaurus	k1gInSc1	Mapusaurus
a	a	k8xC	a
Carcharodontosaurus	Carcharodontosaurus	k1gInSc1	Carcharodontosaurus
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Carcharodontosauridae	Carcharodontosaurida	k1gFnSc2	Carcharodontosaurida
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
a	a	k8xC	a
Mapusaurus	Mapusaurus	k1gMnSc1	Mapusaurus
byli	být	k5eAaImAgMnP	být
navíc	navíc	k6eAd1	navíc
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
podčeledi	podčeleď	k1gFnSc2	podčeleď
Giganotosaurinae	Giganotosaurina	k1gFnSc2	Giganotosaurina
paleontology	paleontolog	k1gMnPc7	paleontolog
Coriou	Coria	k1gMnSc7	Coria
a	a	k8xC	a
Curriem	Currium	k1gNnSc7	Currium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sdílejí	sdílet	k5eAaImIp3nP	sdílet
více	hodně	k6eAd2	hodně
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
než	než	k8xS	než
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
příslušníky	příslušník	k1gMnPc7	příslušník
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
především	především	k9	především
v	v	k7c6	v
části	část	k1gFnSc6	část
části	část	k1gFnSc2	část
Země	zem	k1gFnSc2	zem
obrů	obr	k1gMnPc2	obr
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Putování	putování	k1gNnPc2	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
:	:	kIx,	:
Gigantičtí	gigantický	k2eAgMnPc1d1	gigantický
ještěři	ještěr	k1gMnPc1	ještěr
s	s	k7c7	s
Nigelem	Nigel	k1gMnSc7	Nigel
Marvenem	Marven	k1gMnSc7	Marven
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
smečky	smečka	k1gFnPc1	smečka
giganotosaurů	giganotosaur	k1gInPc2	giganotosaur
loví	lovit	k5eAaImIp3nP	lovit
mladé	mladý	k2eAgFnPc1d1	mladá
argentinosaury	argentinosaura	k1gFnPc1	argentinosaura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
díle	dílo	k1gNnSc6	dílo
třetí	třetí	k4xOgFnSc2	třetí
série	série	k1gFnSc2	série
populárního	populární	k2eAgNnSc2d1	populární
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Pravěk	pravěk	k1gInSc1	pravěk
útočí	útočit	k5eAaImIp3nS	útočit
(	(	kIx(	(
<g/>
Primeval	Primeval	k1gFnSc1	Primeval
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
trhlinou	trhlina	k1gFnSc7	trhlina
času	čas	k1gInSc2	čas
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
sežere	sežrat	k5eAaPmIp3nS	sežrat
Nigela	Nigela	k1gFnSc1	Nigela
Marvena	Marven	k2eAgFnSc1d1	Marvena
a	a	k8xC	a
napadne	napadnout	k5eAaPmIp3nS	napadnout
obrovské	obrovský	k2eAgNnSc4d1	obrovské
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Coria	Coria	k1gFnSc1	Coria
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
and	and	k?	and
Currie	Currie	k1gFnSc1	Currie
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
J.	J.	kA	J.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Braincase	Braincasa	k1gFnSc3	Braincasa
of	of	k?	of
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
carolinii	carolinie	k1gFnSc4	carolinie
(	(	kIx(	(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
:	:	kIx,	:
Theropoda	Theropoda	k1gMnSc1	Theropoda
<g/>
)	)	kIx)	)
from	from	k1gMnSc1	from
the	the	k?	the
Upper	Upper	k1gMnSc1	Upper
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
Vertebrate	Vertebrat	k1gInSc5	Vertebrat
Paleontology	paleontolog	k1gMnPc7	paleontolog
<g/>
,	,	kIx,	,
22	[number]	k4	22
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
802	[number]	k4	802
<g/>
-	-	kIx~	-
<g/>
811	[number]	k4	811
<g/>
.	.	kIx.	.
</s>
<s>
Coria	Coria	k1gFnSc1	Coria
RA	ra	k0	ra
&	&	k?	&
Salgado	Salgada	k1gFnSc5	Salgada
L	L	kA	L
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
new	new	k?	new
giant	giant	k1gMnSc1	giant
carnivorous	carnivorous	k1gMnSc1	carnivorous
dinosaur	dinosaur	k1gMnSc1	dinosaur
from	from	k1gMnSc1	from
the	the	k?	the
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Patagonia	Patagonium	k1gNnSc2	Patagonium
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
377	[number]	k4	377
<g/>
:	:	kIx,	:
225	[number]	k4	225
<g/>
-	-	kIx~	-
<g/>
226	[number]	k4	226
Coria	Corium	k1gNnSc2	Corium
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
and	and	k?	and
Currie	Currie	k1gFnSc1	Currie
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
J.	J.	kA	J.
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k9	a
new	new	k?	new
carcharodontosaurid	carcharodontosaurid	k1gInSc1	carcharodontosaurid
(	(	kIx(	(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
,	,	kIx,	,
Theropoda	Theropoda	k1gMnSc1	Theropoda
<g/>
)	)	kIx)	)
from	from	k1gMnSc1	from
the	the	k?	the
Upper	Upper	k1gMnSc1	Upper
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Geodiversitas	Geodiversitas	k1gMnSc1	Geodiversitas
<g/>
,	,	kIx,	,
28	[number]	k4	28
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
118	[number]	k4	118
<g/>
.	.	kIx.	.
pdf	pdf	k?	pdf
link	link	k1gInSc1	link
Seebacher	Seebachra	k1gFnPc2	Seebachra
<g/>
,	,	kIx,	,
F.	F.	kA	F.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
new	new	k?	new
method	method	k1gInSc1	method
to	ten	k3xDgNnSc1	ten
calculate	calculat	k1gInSc5	calculat
allometric	allometric	k1gMnSc1	allometric
length-mass	lengthassa	k1gFnPc2	length-massa
relationships	relationships	k1gInSc4	relationships
of	of	k?	of
dinosaurs	dinosaurs	k1gInSc1	dinosaurs
<g/>
.	.	kIx.	.
</s>
<s>
Journal	Journat	k5eAaImAgMnS	Journat
of	of	k?	of
Vertebrate	Vertebrat	k1gInSc5	Vertebrat
Paleontology	paleontolog	k1gMnPc4	paleontolog
21	[number]	k4	21
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Blanco	blanco	k6eAd1	blanco
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Ernesto	Ernesta	k1gMnSc5	Ernesta
<g/>
;	;	kIx,	;
Mazzetta	Mazzetta	k1gFnSc1	Mazzetta
<g/>
,	,	kIx,	,
Gerardo	Gerardo	k1gNnSc1	Gerardo
V.	V.	kA	V.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k9	a
new	new	k?	new
approach	approach	k1gInSc1	approach
to	ten	k3xDgNnSc1	ten
evaluate	evaluat	k1gInSc5	evaluat
the	the	k?	the
cursorial	cursorial	k1gMnSc1	cursorial
ability	abilita	k1gFnSc2	abilita
of	of	k?	of
the	the	k?	the
giant	giant	k1gInSc1	giant
theropod	theropoda	k1gFnPc2	theropoda
Giganotosaurus	Giganotosaurus	k1gInSc4	Giganotosaurus
carolinii	carolinie	k1gFnSc4	carolinie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Acta	Act	k2eAgFnSc1d1	Acta
Palaeontologica	Palaeontologic	k2eAgFnSc1d1	Palaeontologic
Polonica	Polonica	k1gFnSc1	Polonica
46	[number]	k4	46
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
193	[number]	k4	193
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Giganotosaurus	Giganotosaurus	k1gInSc1	Giganotosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Článek	článek	k1gInSc4	článek
na	na	k7c4	na
blogu	bloga	k1gFnSc4	bloga
Dinosaurusblog	Dinosaurusblog	k1gInSc1	Dinosaurusblog
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
pravěkých	pravěký	k2eAgMnPc6d1	pravěký
predátorech	predátor	k1gMnPc6	predátor
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
blogu	blog	k1gInSc6	blog
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
největších	veliký	k2eAgInPc6d3	veliký
teropodech	teropod	k1gInPc6	teropod
v	v	k7c6	v
Hradeckém	hradecký	k2eAgInSc6d1	hradecký
deníku	deník	k1gInSc6	deník
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
Galerie	galerie	k1gFnSc1	galerie
Giganotosaurus	Giganotosaurus	k1gInSc4	Giganotosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
