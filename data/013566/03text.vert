<s>
Magnetar	Magnetar	k1gMnSc1
</s>
<s>
Vyrenderovaný	Vyrenderovaný	k2eAgInSc1d1
koncept	koncept	k1gInSc1
magnetaru	magnetar	k1gInSc2
se	s	k7c7
zvýrazněnými	zvýrazněný	k2eAgFnPc7d1
siločarami	siločára	k1gFnPc7
supersilného	supersilný	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Magnetar	Magnetar	k1gInSc1
je	být	k5eAaImIp3nS
neutronová	neutronový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
s	s	k7c7
extrémně	extrémně	k6eAd1
silným	silný	k2eAgNnSc7d1
magnetickým	magnetický	k2eAgNnSc7d1
polem	pole	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpad	rozpad	k1gInSc1
nestabilní	stabilní	k2eNgFnSc2d1
kůry	kůra	k1gFnSc2
doprovází	doprovázet	k5eAaImIp3nS
mohutné	mohutný	k2eAgFnSc2d1
emise	emise	k1gFnSc2
vysokoenergetického	vysokoenergetický	k2eAgNnSc2d1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
především	především	k9
rentgenového	rentgenový	k2eAgNnSc2d1
a	a	k8xC
gama	gama	k1gNnSc1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Teoreticky	teoreticky	k6eAd1
tyto	tento	k3xDgInPc4
objekty	objekt	k1gInPc4
předpověděli	předpovědět	k5eAaPmAgMnP
Robert	Robert	k1gMnSc1
Duncan	Duncan	k1gMnSc1
a	a	k8xC
Christopher	Christophra	k1gFnPc2
Thompson	Thompson	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujícího	následující	k2eAgNnSc2d1
desetiletí	desetiletí	k1gNnSc2
byla	být	k5eAaImAgFnS
magnetarová	magnetarový	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
široce	široko	k6eAd1
akceptována	akceptovat	k5eAaBmNgFnS
jako	jako	k8xS,k8xC
možné	možný	k2eAgNnSc1d1
fyzikální	fyzikální	k2eAgNnSc1d1
vysvětlení	vysvětlení	k1gNnSc1
pozorovaných	pozorovaný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
SGR	SGR	kA
(	(	kIx(
<g/>
Soft	Soft	k?
gamma	gamma	k1gNnSc1
repeater	repeater	k1gMnSc1
–	–	k?
zdroj	zdroj	k1gInSc1
opakovaných	opakovaný	k2eAgInPc2d1
záblesků	záblesk	k1gInPc2
měkkého	měkký	k2eAgNnSc2d1
gamma	gammum	k1gNnSc2
záření	záření	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
AXP	AXP	kA
(	(	kIx(
<g/>
Anomalous	Anomalous	k1gMnSc1
X-Ray	X-Raa	k1gFnSc2
Pulsar	pulsar	k1gInSc1
–	–	k?
nepravidelný	pravidelný	k2eNgInSc1d1
zábleskový	zábleskový	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
rentgenového	rentgenový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc4
magnetar	magnetar	k1gInSc4
detekovala	detekovat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
Chryssa	Chryssa	k1gFnSc1
Kouveliotou	Kouveliota	k1gFnSc7
z	z	k7c2
Marshallova	Marshallův	k2eAgNnSc2d1
kosmického	kosmický	k2eAgNnSc2d1
letového	letový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
v	v	k7c6
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
magnetaru	magnetar	k1gInSc2
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
supernova	supernova	k1gFnSc1
zhroutí	zhroutit	k5eAaPmIp3nS
do	do	k7c2
neutronové	neutronový	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
<g/>
,	,	kIx,
síla	síla	k1gFnSc1
jejího	její	k3xOp3gNnSc2
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
dramaticky	dramaticky	k6eAd1
vzroste	vzrůst	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duncan	Duncany	k1gInPc2
a	a	k8xC
Thompson	Thompsona	k1gFnPc2
vypočítali	vypočítat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
neutronové	neutronový	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
dosahující	dosahující	k2eAgFnPc1d1
ohromných	ohromný	k2eAgFnPc2d1
108	#num#	k4
T	T	kA
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
za	za	k7c2
jistých	jistý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
narůst	narůst	k5eAaPmF
ještě	ještě	k9
více	hodně	k6eAd2
<g/>
,	,	kIx,
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
1011	#num#	k4
T.	T.	kA
Takovou	takový	k3xDgFnSc4
vysoce	vysoce	k6eAd1
magnetickou	magnetický	k2eAgFnSc4d1
neutronovou	neutronový	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
nazýváme	nazývat	k5eAaImIp1nP
magnetar	magnetar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vnějších	vnější	k2eAgFnPc6d1
vrstvách	vrstva	k1gFnPc6
magnetaru	magnetar	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
plazmatu	plazma	k1gNnSc2
těžkých	těžký	k2eAgInPc2d1
prvků	prvek	k1gInPc2
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
železa	železo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
tlak	tlak	k1gInSc1
vzrůst	vzrůst	k5eAaPmF
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
„	„	k?
<g/>
hvězdotřesení	hvězdotřesení	k1gNnPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Energie	energie	k1gFnSc1
těchto	tento	k3xDgFnPc2
seismických	seismický	k2eAgFnPc2d1
vibrací	vibrace	k1gFnPc2
je	být	k5eAaImIp3nS
extrémně	extrémně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
záblesk	záblesk	k1gInSc4
rentgenového	rentgenový	k2eAgInSc2d1
a	a	k8xC
gama	gama	k1gNnSc2
záření	záření	k1gNnSc2
(	(	kIx(
<g/>
gama	gama	k1gNnSc1
záblesk	záblesk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomové	astronom	k1gMnPc1
takové	takový	k3xDgInPc1
objekty	objekt	k1gInPc1
znají	znát	k5eAaImIp3nP
jako	jako	k9
SGR	SGR	kA
<g/>
.	.	kIx.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
asi	asi	k9
desetina	desetina	k1gFnSc1
explozí	exploze	k1gFnPc2
supernovy	supernova	k1gFnSc2
vyústí	vyústit	k5eAaPmIp3nS
v	v	k7c4
magnetar	magnetar	k1gInSc4
a	a	k8xC
ne	ne	k9
v	v	k7c4
obvyklejší	obvyklý	k2eAgFnSc4d2
neutronovou	neutronový	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
nebo	nebo	k8xC
pulsar	pulsar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
<g/>
,	,	kIx,
jen	jen	k9
pokud	pokud	k8xS
má	můj	k3xOp1gFnSc1
hvězda	hvězda	k1gFnSc1
rychlou	rychlý	k2eAgFnSc4d1
rotaci	rotace	k1gFnSc4
a	a	k8xC
silné	silný	k2eAgNnSc4d1
magnetické	magnetický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
,	,	kIx,
ještě	ještě	k9
než	než	k8xS
vybuchne	vybuchnout	k5eAaPmIp3nS
jako	jako	k9
supernova	supernova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
magnetaru	magnetar	k1gInSc2
způsobuje	způsobovat	k5eAaImIp3nS
konvekcí	konvekce	k1gFnSc7
řízené	řízený	k2eAgNnSc1d1
dynamo	dynamo	k1gNnSc1
horké	horký	k2eAgFnSc2d1
neutronové	neutronový	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
z	z	k7c2
jádra	jádro	k1gNnSc2
hvězdy	hvězda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
začne	začít	k5eAaPmIp3nS
fungovat	fungovat	k5eAaImF
asi	asi	k9
v	v	k7c6
prvních	první	k4xOgInPc6
10	#num#	k4
sekundách	sekunda	k1gFnPc6
života	život	k1gInSc2
neutronové	neutronový	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
neutronová	neutronový	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
rotuje	rotovat	k5eAaImIp3nS
stejně	stejně	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
perioda	perioda	k1gFnSc1
konvekce	konvekce	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
deset	deset	k4xCc4
milisekund	milisekunda	k1gFnPc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
konvekční	konvekční	k2eAgInPc1d1
proudy	proud	k1gInPc1
působit	působit	k5eAaImF
globálně	globálně	k6eAd1
a	a	k8xC
přenášet	přenášet	k5eAaImF
významné	významný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
své	svůj	k3xOyFgFnSc2
kinetické	kinetický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
do	do	k7c2
energie	energie	k1gFnSc2
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
pomaleji	pomale	k6eAd2
rotujících	rotující	k2eAgFnPc2d1
neutronových	neutronový	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
se	se	k3xPyFc4
konvekční	konvekční	k2eAgInPc1d1
proudy	proud	k1gInPc1
formují	formovat	k5eAaImIp3nP
jen	jen	k9
místně	místně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
magnetaru	magnetar	k1gInSc2
jako	jako	k8xC,k8xS
SGR	SGR	kA
je	být	k5eAaImIp3nS
krátký	krátký	k2eAgInSc4d1
<g/>
:	:	kIx,
energie	energie	k1gFnSc1
vydávaná	vydávaný	k2eAgFnSc1d1
explozemi	exploze	k1gFnPc7
při	při	k7c6
hvězdotřesení	hvězdotřesení	k1gNnSc6
zpomaluje	zpomalovat	k5eAaImIp3nS
rotaci	rotace	k1gFnSc4
(	(	kIx(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
magnetary	magnetara	k1gFnPc1
rotují	rotovat	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
pomaleji	pomale	k6eAd2
než	než	k8xS
jiné	jiný	k2eAgFnPc1d1
neutronové	neutronový	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
srovnatelného	srovnatelný	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
oslabuje	oslabovat	k5eAaImIp3nS
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
hvězdotřesení	hvězdotřesení	k1gNnSc1
asi	asi	k9
po	po	k7c4
10	#num#	k4
000	#num#	k4
letech	léto	k1gNnPc6
utichnou	utichnout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězda	Hvězda	k1gMnSc1
pak	pak	k9
ještě	ještě	k6eAd1
vyzařuje	vyzařovat	k5eAaImIp3nS
rentgenové	rentgenový	k2eAgInPc4d1
paprsky	paprsek	k1gInPc4
a	a	k8xC
změní	změnit	k5eAaPmIp3nS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c4
objekt	objekt	k1gInSc4
nazývaný	nazývaný	k2eAgInSc1d1
astronomy	astronom	k1gMnPc4
AXP	AXP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalších	další	k2eAgInPc6d1
10	#num#	k4
000	#num#	k4
letech	léto	k1gNnPc6
přestane	přestat	k5eAaPmIp3nS
vydávat	vydávat	k5eAaImF,k5eAaPmF
i	i	k9
tyto	tento	k3xDgInPc4
záblesky	záblesk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
je	být	k5eAaImIp3nS
však	však	k9
zdrojem	zdroj	k1gInSc7
obrovských	obrovský	k2eAgNnPc2d1
vzplanutí	vzplanutí	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některá	některý	k3yIgFnSc1
byla	být	k5eAaImAgFnS
přímo	přímo	k6eAd1
pozorována	pozorován	k2eAgFnSc1d1
<g/>
,	,	kIx,
např.	např.	kA
SGR	SGR	kA
1806-20	1806-20	k4
27	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
růstem	růst	k1gInSc7
přesnosti	přesnost	k1gFnSc2
dalekohledů	dalekohled	k1gInPc2
se	se	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
zaznamenání	zaznamenání	k1gNnSc1
mnoha	mnoho	k4c2
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2004	#num#	k4
bylo	být	k5eAaImAgNnS
známo	znám	k2eAgNnSc1d1
4	#num#	k4
SGR	SGR	kA
a	a	k8xC
5	#num#	k4
AXP	AXP	kA
a	a	k8xC
další	další	k2eAgMnPc1d1
čtyři	čtyři	k4xCgMnPc1
kandidáti	kandidát	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
ještě	ještě	k6eAd1
vyžaduje	vyžadovat	k5eAaImIp3nS
potvrzení	potvrzení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Vliv	vliv	k1gInSc1
supersilných	supersilný	k2eAgFnPc2d1
magnetických	magnetický	k2eAgFnPc2d1
polí	pole	k1gFnPc2
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
přes	přes	k7c4
10	#num#	k4
GT	GT	kA
je	být	k5eAaImIp3nS
dost	dost	k6eAd1
silné	silný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
smazalo	smazat	k5eAaPmAgNnS
data	datum	k1gNnPc4
z	z	k7c2
kreditní	kreditní	k2eAgFnSc2d1
karty	karta	k1gFnSc2
z	z	k7c2
poloviny	polovina	k1gFnSc2
vzdálenosti	vzdálenost	k1gFnSc2
Měsíce	měsíc	k1gInSc2
od	od	k7c2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Malý	malý	k2eAgInSc1d1
neodymový	odymový	k2eNgInSc1d1
magnet	magnet	k1gInSc1
má	mít	k5eAaImIp3nS
pole	pole	k1gNnSc4
kolem	kolem	k7c2
1	#num#	k4
T	T	kA
<g/>
,	,	kIx,
Země	zem	k1gFnPc1
má	mít	k5eAaImIp3nS
geomagnetické	geomagnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
30-60	30-60	k4
mikrotesla	mikrotést	k5eAaPmAgFnS
a	a	k8xC
většinu	většina	k1gFnSc4
záznamových	záznamový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
lze	lze	k6eAd1
vymazat	vymazat	k5eAaPmF
polem	pole	k1gNnSc7
1	#num#	k4
militesla	militést	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
magnetaru	magnetar	k1gInSc2
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
smrtelné	smrtelný	k2eAgNnSc1d1
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
do	do	k7c2
1000	#num#	k4
km	km	kA
kvůli	kvůli	k7c3
deformaci	deformace	k1gFnSc3
atomů	atom	k1gInPc2
v	v	k7c6
živé	živý	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Původ	původ	k1gInSc1
magnetarů	magnetar	k1gInPc2
–	–	k?
CNN	CNN	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Nejjasnější	jasný	k2eAgInSc4d3
záblesk	záblesk	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
Sky	Sky	k1gMnSc3
and	and	k?
Telescope	Telescop	k1gMnSc5
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
magnetar	magnetara	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vznik	vznik	k1gInSc1
magnetarů	magnetar	k1gMnPc2
vyřešen	vyřešit	k5eAaPmNgInS
vytvářejí	vytvářet	k5eAaImIp3nP
se	se	k3xPyFc4
při	při	k7c6
explozích	exploze	k1gFnPc6
největších	veliký	k2eAgFnPc2d3
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NASA	NASA	kA
<g/>
:	:	kIx,
Objev	objev	k1gInSc1
„	„	k?
<g/>
magnetarů	magnetar	k1gInPc2
<g/>
“	“	k?
vyřešil	vyřešit	k5eAaPmAgMnS
záhadu	záhada	k1gFnSc4
starou	stará	k1gFnSc4
19	#num#	k4
let	léto	k1gNnPc2
Citát	citát	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
…	…	k?
<g/>
nasvědčovalo	nasvědčovat	k5eAaImAgNnS
magnetickému	magnetický	k2eAgNnSc3d1
poli	pole	k1gNnSc3
o	o	k7c6
síle	síla	k1gFnSc6
asi	asi	k9
800	#num#	k4
biliónů	bilión	k4xCgInPc2
gaussů	gauss	k1gInPc2
<g/>
…	…	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
C.	C.	kA
Duncan	Duncan	k1gMnSc1
<g/>
,	,	kIx,
Texaská	texaský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Austinu	Austin	k1gInSc6
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Magnetary	Magnetara	k1gFnPc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
SGR	SGR	kA
&	&	k?
velmi	velmi	k6eAd1
silná	silný	k2eAgNnPc1d1
magnetická	magnetický	k2eAgNnPc1d1
pole	pole	k1gNnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NASA	NASA	kA
Astrophysics	Astrophysics	k1gInSc1
Data	datum	k1gNnSc2
System	Syst	k1gInSc7
(	(	kIx(
<g/>
ADS	ADS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Duncan	Duncan	k1gMnSc1
&	&	k?
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
Ap.	ap.	kA
<g/>
J.	J.	kA
392	#num#	k4
<g/>
,	,	kIx,
L	L	kA
<g/>
9	#num#	k4
<g/>
)	)	kIx)
1992	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NASA	NASA	kA
ADS	ADS	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
:	:	kIx,
Objev	objev	k1gInSc1
magnetaru	magnetar	k1gInSc2
spojený	spojený	k2eAgInSc1d1
s	s	k7c7
SGR	SGR	kA
1900	#num#	k4
<g/>
+	+	kIx~
<g/>
14	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Chryssa	Chryssa	k1gFnSc1
Kouveliotou	Kouveliota	k1gFnSc7
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Duncan	Duncan	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Christopher	Christophra	k1gFnPc2
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Magnetars	Magnetars	k1gInSc1
<g/>
,	,	kIx,
<g/>
“	“	k?
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
<g/>
,	,	kIx,
Feb	Feb	k1gMnSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
34-41	34-41	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
PDF	PDF	kA
<g/>
)	)	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
C.	C.	kA
Duncan	Duncan	k1gMnSc1
a	a	k8xC
Christopher	Christophra	k1gFnPc2
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Vznik	vznik	k1gInSc1
velmi	velmi	k6eAd1
silně	silně	k6eAd1
magnetických	magnetický	k2eAgFnPc2d1
neutronových	neutronový	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
:	:	kIx,
Souvislosti	souvislost	k1gFnPc1
se	se	k3xPyFc4
záblesky	záblesk	k1gInPc7
gama	gama	k1gNnSc3
paprsků	paprsek	k1gInPc2
<g/>
,	,	kIx,
<g/>
“	“	k?
Astronomical	Astronomical	k1gFnSc1
Journal	Journal	k1gFnSc1
<g/>
,	,	kIx,
ročník	ročník	k1gInSc1
392	#num#	k4
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
1	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
9	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
L	L	kA
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
