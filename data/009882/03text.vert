<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jošt	Jošt	k1gMnSc1	Jošt
(	(	kIx(	(
<g/>
také	také	k9	také
Judok	Judok	k1gInSc1	Judok
či	či	k8xC	či
Jodok	Jodok	k1gInSc1	Jodok
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Josse	Josse	k1gFnSc1	Josse
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
poustevník	poustevník	k1gMnSc1	poustevník
a	a	k8xC	a
světec	světec	k1gMnSc1	světec
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Bretaně	Bretaň	k1gFnSc2	Bretaň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
jako	jako	k9	jako
poutník	poutník	k1gMnSc1	poutník
prochodil	prochodit	k5eAaPmAgMnS	prochodit
celou	celý	k2eAgFnSc4d1	celá
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgInS	navštívit
také	také	k9	také
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
počátkem	počátek	k1gInSc7	počátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
668	[number]	k4	668
nebo	nebo	k8xC	nebo
669	[number]	k4	669
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
675	[number]	k4	675
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Joštův	Joštův	k2eAgInSc4d1	Joštův
životopis	životopis	k1gInSc4	životopis
sepsal	sepsat	k5eAaPmAgMnS	sepsat
neznámý	známý	k2eNgMnSc1d1	neznámý
autor	autor	k1gMnSc1	autor
až	až	k6eAd1	až
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
synem	syn	k1gMnSc7	syn
Judhaëla	Judhaël	k1gMnSc2	Judhaël
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc2	vládce
bretaňského	bretaňský	k2eAgNnSc2d1	bretaňské
knížectví	knížectví	k1gNnSc2	knížectví
Domnonea	Domnone	k1gInSc2	Domnone
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
637	[number]	k4	637
přijal	přijmout	k5eAaPmAgMnS	přijmout
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Judhaël	Judhaël	k1gInSc1	Judhaël
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
605	[number]	k4	605
nebo	nebo	k8xC	nebo
607	[number]	k4	607
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
,	,	kIx,	,
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Joštův	Joštův	k2eAgMnSc1d1	Joštův
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Judikael	Judikael	k1gMnSc1	Judikael
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vzdal	vzdát	k5eAaPmAgMnS	vzdát
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Jošta	Jošt	k1gMnSc2	Jošt
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
640	[number]	k4	640
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
svatý	svatý	k2eAgInSc4d1	svatý
Meven	Meven	k1gInSc4	Meven
<g/>
.	.	kIx.	.
</s>
<s>
Jošt	Jošt	k1gMnSc1	Jošt
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
panovnickou	panovnický	k2eAgFnSc4d1	panovnická
roli	role	k1gFnSc4	role
nepřijmout	přijmout	k5eNaPmF	přijmout
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
opustil	opustit	k5eAaPmAgMnS	opustit
domov	domov	k1gInSc4	domov
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
poutníků	poutník	k1gMnPc2	poutník
směřujících	směřující	k2eAgMnPc2d1	směřující
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
došel	dojít	k5eAaPmAgMnS	dojít
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Authii	Authie	k1gFnSc3	Authie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Haymonem	Haymon	k1gMnSc7	Haymon
<g/>
,	,	kIx,	,
vládcem	vládce	k1gMnSc7	vládce
hrabství	hrabství	k1gNnSc2	hrabství
Ponthieu	Ponthie	k1gMnSc3	Ponthie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Jošt	Jošt	k1gMnSc1	Jošt
zůstal	zůstat	k5eAaPmAgMnS	zůstat
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
kněz	kněz	k1gMnSc1	kněz
u	u	k7c2	u
Haymonova	Haymonův	k2eAgInSc2d1	Haymonův
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
644	[number]	k4	644
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Haymona	Haymon	k1gMnSc4	Haymon
žil	žít	k5eAaImAgMnS	žít
jako	jako	k9	jako
poustevník	poustevník	k1gMnSc1	poustevník
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Brahic	Brahice	k1gInPc2	Brahice
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgInSc1d1	nynější
Saint-Josse-au-Bois	Saint-Josseu-Bois	k1gInSc1	Saint-Josse-au-Bois
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Atributy	atribut	k1gInPc4	atribut
==	==	k?	==
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
v	v	k7c6	v
poutnickém	poutnický	k2eAgNnSc6d1	poutnické
rouchu	roucho	k1gNnSc6	roucho
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jako	jako	k9	jako
opat	opat	k1gMnSc1	opat
s	s	k7c7	s
mitrou	mitra	k1gFnSc7	mitra
a	a	k8xC	a
pastýřskou	pastýřský	k2eAgFnSc7d1	pastýřská
holí	hole	k1gFnSc7	hole
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
poustevník	poustevník	k1gMnSc1	poustevník
s	s	k7c7	s
Biblí	bible	k1gFnSc7	bible
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
podle	podle	k7c2	podle
koruny	koruna	k1gFnSc2	koruna
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jošta	Jošt	k1gMnSc2	Jošt
</s>
</p>
