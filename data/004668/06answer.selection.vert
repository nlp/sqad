<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
preambule	preambule	k1gFnSc2	preambule
navazuje	navazovat	k5eAaImIp3nS	navazovat
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
státnosti	státnost	k1gFnSc2	státnost
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
bývalých	bývalý	k2eAgFnPc2d1	bývalá
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
