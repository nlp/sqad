<s>
VOR	vor	k1gInSc1
(	(	kIx(
<g/>
radiomaják	radiomaják	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1
pozemní	pozemní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
D-VOR	D-VOR	k1gFnSc2
<g/>
/	/	kIx~
<g/>
DME	dmout	k5eAaImIp3nS
</s>
<s>
palubní	palubní	k2eAgInSc1d1
VOR	vor	k1gInSc1
<g/>
/	/	kIx~
<g/>
ILS	ILS	kA
s	s	k7c7
mechanickou	mechanický	k2eAgFnSc7d1
stupnicí	stupnice	k1gFnSc7
</s>
<s>
VOR	vor	kA
(	(	kIx(
<g/>
VHF	VHF	kA
Omnidirectional	Omnidirectional	k1gFnSc4
Radio	radio	k1gNnSc4
Range	Range	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
VKV	VKV	kA
všesměrový	všesměrový	k2eAgInSc1d1
radiomaják	radiomaják	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
používaných	používaný	k2eAgInPc2d1
v	v	k7c6
leteckém	letecký	k2eAgNnSc6d1
provozu	provoz	k1gInSc6
pro	pro	k7c4
přístrojovou	přístrojový	k2eAgFnSc4d1
navigaci	navigace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
určit	určit	k5eAaPmF
letadlu	letadlo	k1gNnSc3
směr	směr	k1gInSc4
vůči	vůči	k7c3
konkrétnímu	konkrétní	k2eAgInSc3d1
majáku	maják	k1gInSc3
-	-	kIx~
a	a	k8xC
to	ten	k3xDgNnSc1
přímo	přímo	k6eAd1
ze	z	k7c2
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přijímač	přijímač	k1gMnSc1
musel	muset	k5eAaImAgMnS
mít	mít	k5eAaImF
nějakou	nějaký	k3yIgFnSc4
speciální	speciální	k2eAgFnSc4d1
směrovou	směrový	k2eAgFnSc4d1
anténu	anténa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
systému	systém	k1gInSc2
nesměrových	směrový	k2eNgInPc2d1
majáků	maják	k1gInPc2
NDB	NDB	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zaměřovat	zaměřovat	k5eAaImF
směrovou	směrový	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vylepšením	vylepšení	k1gNnSc7
systému	systém	k1gInSc2
VOR	vor	k1gInSc1
je	být	k5eAaImIp3nS
systém	systém	k1gInSc4
D-VOR	D-VOR	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Signál	signál	k1gInSc4
D-VOR	D-VOR	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
přesnější	přesný	k2eAgFnSc4d2
navigaci	navigace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
kompatibilní	kompatibilní	k2eAgInPc4d1
<g/>
:	:	kIx,
Přijímače	přijímač	k1gInPc4
VOR	vor	k1gInSc1
přijímají	přijímat	k5eAaImIp3nP
i	i	k9
signál	signál	k1gInSc4
D-VOR	D-VOR	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
navigaci	navigace	k1gFnSc4
v	v	k7c6
bezprostředním	bezprostřední	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
letišť	letiště	k1gNnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
T-VOR	T-VOR	k1gFnSc1
(	(	kIx(
<g/>
Terminal-VOR	Terminal-VOR	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Všesměrových	všesměrový	k2eAgInPc2d1
majáků	maják	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
letectví	letectví	k1gNnSc6
používá	používat	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
VOR	vor	k1gInSc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
frekvenční	frekvenční	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
od	od	k7c2
108	#num#	k4
až	až	k9
118	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
VOR	vor	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
určit	určit	k5eAaPmF
pouze	pouze	k6eAd1
azimut	azimut	k1gInSc4
k	k	k7c3
majáku	maják	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
často	často	k6eAd1
kombinuje	kombinovat	k5eAaImIp3nS
s	s	k7c7
měřením	měření	k1gNnSc7
vzdálenosti	vzdálenost	k1gFnSc2
DME	dmout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinace	kombinace	k1gFnSc1
obou	dva	k4xCgInPc2
systémů	systém	k1gInPc2
indikuje	indikovat	k5eAaBmIp3nS
polohu	poloha	k1gFnSc4
vůči	vůči	k7c3
majáku	maják	k1gInSc3
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
souřadnicích	souřadnice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
podobném	podobný	k2eAgInSc6d1
principu	princip	k1gInSc6
funguje	fungovat	k5eAaImIp3nS
i	i	k9
původně	původně	k6eAd1
vojenský	vojenský	k2eAgInSc1d1
a	a	k8xC
výrazně	výrazně	k6eAd1
přesnější	přesný	k2eAgInSc1d2
systém	systém	k1gInSc1
TACAN	TACAN	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
slučován	slučován	k2eAgInSc1d1
s	s	k7c7
VOR	vor	k1gInSc1
do	do	k7c2
společného	společný	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
označovaného	označovaný	k2eAgInSc2d1
VORTAC	VORTAC	kA
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Princip	princip	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c6
vysílání	vysílání	k1gNnSc6
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
směrově	směrově	k6eAd1
závislý	závislý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vlastně	vlastně	k9
o	o	k7c6
složení	složení	k1gNnSc6
dvou	dva	k4xCgInPc2
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
je	být	k5eAaImIp3nS
konstantně	konstantně	k6eAd1
vysílán	vysílat	k5eAaImNgMnS
všesměrovou	všesměrový	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
a	a	k8xC
druhý	druhý	k4xOgMnSc1
vysílán	vysílat	k5eAaImNgMnS
"	"	kIx"
<g/>
rotující	rotující	k2eAgFnSc7d1
<g/>
"	"	kIx"
směrovou	směrový	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiomaják	radiomaják	k1gInSc1
navíc	navíc	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
sekvenci	sekvence	k1gFnSc4
dvou	dva	k4xCgInPc2
nebo	nebo	k8xC
tří	tři	k4xCgInPc2
znaků	znak	k1gInPc2
v	v	k7c6
Morseově	Morseův	k2eAgFnSc6d1
abecedě	abeceda	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
slouží	sloužit	k5eAaImIp3nS
pilotovi	pilot	k1gMnSc3
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
majáku	maják	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Rotace	rotace	k1gFnSc2
<g/>
"	"	kIx"
druhé	druhý	k4xOgFnSc2
antény	anténa	k1gFnSc2
není	být	k5eNaImIp3nS
řešena	řešit	k5eAaImNgFnS
mechanicky	mechanicky	k6eAd1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
elektronicky	elektronicky	k6eAd1
<g/>
,	,	kIx,
použitím	použití	k1gNnSc7
dvojice	dvojice	k1gFnSc2
na	na	k7c4
sebe	sebe	k3xPyFc4
kolmých	kolmý	k2eAgFnPc2d1
pevných	pevný	k2eAgFnPc2d1
antén	anténa	k1gFnPc2
a	a	k8xC
skládáním	skládání	k1gNnSc7
jejich	jejich	k3xOp3gInPc2
fázově	fázově	k6eAd1
posunutých	posunutý	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
"	"	kIx"
<g/>
rotace	rotace	k1gFnSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
30	#num#	k4
otáček	otáčka	k1gFnPc2
<g/>
/	/	kIx~
<g/>
sec	sec	kA
(	(	kIx(
<g/>
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
lze	lze	k6eAd1
také	také	k9
chápat	chápat	k5eAaImF
jako	jako	k9
amplitudovou	amplitudový	k2eAgFnSc4d1
modulaci	modulace	k1gFnSc4
30	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přijímač	přijímač	k1gInSc1
v	v	k7c6
letadle	letadlo	k1gNnSc6
poté	poté	k6eAd1
z	z	k7c2
fázového	fázový	k2eAgInSc2d1
posuvu	posuv	k1gInSc2
výše	výše	k1gFnSc2,k1gFnSc2wB
popsané	popsaný	k2eAgFnPc4d1
dvojice	dvojice	k1gFnPc4
signálů	signál	k1gInPc2
určí	určit	k5eAaPmIp3nS
azimut	azimut	k1gInSc1
letadla	letadlo	k1gNnSc2
od	od	k7c2
vysílače	vysílač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
v	v	k7c6
úhlových	úhlový	k2eAgInPc6d1
stupních	stupeň	k1gInPc6
a	a	k8xC
nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
radiál	radiála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiál	radiála	k1gFnPc2
je	být	k5eAaImIp3nS
orientovaná	orientovaný	k2eAgFnSc1d1
přímka	přímka	k1gFnSc1
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
majáku	maják	k1gInSc2
a	a	k8xC
prochází	procházet	k5eAaImIp3nS
přijímačem	přijímač	k1gInSc7
(	(	kIx(
<g/>
resp.	resp.	kA
všemi	všecek	k3xTgMnPc7
přijímači	přijímač	k1gMnPc7
v	v	k7c6
konkrétním	konkrétní	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
letadlo	letadlo	k1gNnSc4
na	na	k7c4
radiálu	radiála	k1gFnSc4
nula	nula	k1gFnSc1
a	a	k8xC
přístroj	přístroj	k1gInSc1
indikuje	indikovat	k5eAaBmIp3nS
"	"	kIx"
<g/>
FROM	FROM	kA
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
JIŽNĚ	jižně	k6eAd1
radiomajáku	radiomaják	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
indikováno	indikován	k2eAgNnSc1d1
"	"	kIx"
<g/>
TO	to	k9
<g/>
"	"	kIx"
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
SEVERNĚ	severně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gInSc2
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
majáků	maják	k1gInPc2
VOR	vor	k1gInSc1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
i	i	k9
systém	systém	k1gInSc1
DME	dmout	k5eAaImIp3nS
(	(	kIx(
<g/>
Distance	distance	k1gFnSc1
Measuring	Measuring	k1gInSc1
Equipment	Equipment	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
měření	měření	k1gNnSc4
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
majáku	maják	k1gInSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
hovoříme	hovořit	k5eAaImIp1nP
o	o	k7c6
majáku	maják	k1gInSc6
VOR	vor	k1gInSc1
<g/>
/	/	kIx~
<g/>
DME	dmout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Doplerovský	Doplerovský	k2eAgInSc1d1
VOR	vor	k1gInSc1
-	-	kIx~
D-VOR	D-VOR	k1gFnSc1
</s>
<s>
Vylepšením	vylepšení	k1gNnSc7
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
dopplerovský	dopplerovský	k2eAgInSc1d1
VOR	vor	k1gInSc1
(	(	kIx(
<g/>
D-VOR	D-VOR	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
Dopplerův	Dopplerův	k2eAgInSc1d1
princip	princip	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
kompatibilní	kompatibilní	k2eAgFnSc1d1
s	s	k7c7
VOR	vor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vysílaný	vysílaný	k2eAgInSc1d1
signál	signál	k1gInSc1
D-VOR	D-VOR	k1gFnSc2
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
směrová	směrový	k2eAgFnSc1d1
vysílací	vysílací	k2eAgFnSc1d1
anténa	anténa	k1gFnSc1
navíc	navíc	k6eAd1
ještě	ještě	k6eAd1
pohybovala	pohybovat	k5eAaImAgFnS
kolem	kolem	k7c2
pevné	pevný	k2eAgFnSc2d1
antény	anténa	k1gFnSc2
po	po	k7c6
kruhové	kruhový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
o	o	k7c6
poloměru	poloměr	k1gInSc6
přibližně	přibližně	k6eAd1
6-7	6-7	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
stejnou	stejná	k1gFnSc4
rychlostí	rychlost	k1gFnSc7
30	#num#	k4
otáček	otáčka	k1gFnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
rotuje	rotovat	k5eAaImIp3nS
signál	signál	k1gInSc1
původního	původní	k2eAgInSc2d1
VOR	vor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
rychlost	rychlost	k1gFnSc4
je	být	k5eAaImIp3nS
technicky	technicky	k6eAd1
těžko	těžko	k6eAd1
realizovatelná	realizovatelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
obvodová	obvodový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
by	by	kYmCp3nS
činila	činit	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
čtyřnásobek	čtyřnásobek	k1gInSc4
rychlosti	rychlost	k1gFnSc2
zvuku	zvuk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
pohyblivá	pohyblivý	k2eAgFnSc1d1
anténa	anténa	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
několika	několik	k4yIc7
desítkami	desítka	k1gFnPc7
do	do	k7c2
kruhu	kruh	k1gInSc2
pevně	pevně	k6eAd1
umístěných	umístěný	k2eAgFnPc2d1
antén	anténa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
pak	pak	k6eAd1
vysílač	vysílač	k1gInSc1
postupně	postupně	k6eAd1
přepíná	přepínat	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
simuluje	simulovat	k5eAaImIp3nS
rychlý	rychlý	k2eAgInSc4d1
kruhový	kruhový	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
druhé	druhý	k4xOgFnSc2
antény	anténa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kontrola	kontrola	k1gFnSc1
přijímače	přijímač	k1gInSc2
-	-	kIx~
VOT	VOT	kA
</s>
<s>
Pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
přijímače	přijímač	k1gInSc2
VOR	vor	k1gInSc1
existují	existovat	k5eAaImIp3nP
ještě	ještě	k9
testovací	testovací	k2eAgInPc4d1
vysílače	vysílač	k1gInPc4
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
VOT	VOT	kA
(	(	kIx(
<g/>
VOR	vor	k1gInSc1
receiver	receiver	k1gInSc1
test	test	k1gInSc4
facility	facilita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
VOR	vor	k1gInSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInPc1
signály	signál	k1gInPc1
nejsou	být	k5eNaImIp3nP
směrové	směrový	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
příjmu	příjem	k1gInSc6
signálu	signál	k1gInSc2
VOT	VOT	kA
z	z	k7c2
jakéhokoliv	jakýkoliv	k3yIgInSc2
směru	směr	k1gInSc2
musí	muset	k5eAaImIp3nS
přijímač	přijímač	k1gInSc1
ukázat	ukázat	k5eAaPmF
nulový	nulový	k2eAgInSc4d1
radiál	radiála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
přijímače	přijímač	k1gInSc2
</s>
<s>
Použití	použití	k1gNnPc1
vysvětlíme	vysvětlit	k5eAaPmIp1nP
na	na	k7c6
jednoduchém	jednoduchý	k2eAgInSc6d1
příkladu	příklad	k1gInSc6
(	(	kIx(
<g/>
na	na	k7c6
palubě	paluba	k1gFnSc6
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
stejný	stejný	k2eAgInSc4d1
indikátor	indikátor	k1gInSc4
VOR	vor	k1gInSc1
<g/>
/	/	kIx~
<g/>
ILS	ILS	kA
jako	jako	k8xC,k8xS
na	na	k7c6
obrázku	obrázek	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilot	pilot	k1gMnSc1
chce	chtít	k5eAaImIp3nS
letět	letět	k5eAaImF
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
pozice	pozice	k1gFnSc2
přímo	přímo	k6eAd1
k	k	k7c3
radiomajáku	radiomaják	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naladí	naladit	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc4
frekvenci	frekvence	k1gFnSc4
a	a	k8xC
odposlechne	odposlechnout	k5eAaPmIp3nS
identifikační	identifikační	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjem	příjem	k1gInSc1
signálu	signál	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
přístroji	přístroj	k1gInSc6
indikován	indikovat	k5eAaBmNgMnS
zmizením	zmizení	k1gNnSc7
červeného	červený	k2eAgInSc2d1
praporku	praporek	k1gInSc2
„	„	k?
<g/>
NAV	navit	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otáčením	otáčení	k1gNnSc7
knoflíku	knoflík	k1gInSc2
OBS	OBS	kA
(	(	kIx(
<g/>
omni	omnout	k5eAaPmRp2nS
bearing	bearing	k1gInSc1
selector	selector	k1gInSc4
<g/>
)	)	kIx)
tak	tak	k9
dlouho	dlouho	k6eAd1
až	až	k6eAd1
směrové	směrový	k2eAgNnSc1d1
břevno	břevno	k1gNnSc1
bude	být	k5eAaImBp3nS
veprostřed	veprostřed	k6eAd1
a	a	k8xC
žlutý	žlutý	k2eAgInSc1d1
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1
praporek	praporek	k1gInSc1
bude	být	k5eAaImBp3nS
ukazovat	ukazovat	k5eAaImF
na	na	k7c6
„	„	k?
<g/>
TO	to	k9
<g/>
“	“	k?
pilot	pilot	k1gMnSc1
zjistí	zjistit	k5eAaPmIp3nS
na	na	k7c6
jakém	jaký	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
radiálu	radiála	k1gFnSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
(	(	kIx(
<g/>
dole	dole	k6eAd1
pod	pod	k7c7
menší	malý	k2eAgFnSc7d2
žlutou	žlutý	k2eAgFnSc7d1
šipkou	šipka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
vidí	vidět	k5eAaImIp3nS
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
kurs	kurs	k1gInSc1
musí	muset	k5eAaImIp3nS
nasadit	nasadit	k5eAaPmF
aby	aby	kYmCp3nS
doletěl	doletět	k5eAaPmAgMnS
k	k	k7c3
radiomajáku	radiomaják	k1gInSc3
(	(	kIx(
<g/>
nahoře	nahoře	k6eAd1
nad	nad	k7c7
větší	veliký	k2eAgFnSc7d2
šipkou	šipka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasadí	nasadit	k5eAaPmIp3nS
tento	tento	k3xDgInSc4
kurs	kurs	k1gInSc4
a	a	k8xC
směrové	směrový	k2eAgNnSc4d1
břevno	břevno	k1gNnSc4
mu	on	k3xPp3gMnSc3
ukazuje	ukazovat	k5eAaImIp3nS
směrovou	směrový	k2eAgFnSc4d1
odchylku	odchylka	k1gFnSc4
od	od	k7c2
radiálu	radiála	k1gFnSc4
po	po	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
letí	letět	k5eAaImIp3nS
k	k	k7c3
majáku	maják	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přelétnutí	přelétnutí	k1gNnSc6
majáku	maják	k1gInSc2
přepadne	přepadnout	k5eAaPmIp3nS
žlutý	žlutý	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
na	na	k7c4
„	„	k?
<g/>
FROM	FROM	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Přesnost	přesnost	k1gFnSc1
</s>
<s>
Přesnost	přesnost	k1gFnSc1
VOR	vor	k1gInSc1
je	být	k5eAaImIp3nS
cca	cca	kA
1	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
nad	nad	k7c7
majákem	maják	k1gInSc7
existuje	existovat	k5eAaImIp3nS
kužel	kužel	k1gInSc1
nespolehlivé	spolehlivý	k2eNgFnSc2d1
indikace	indikace	k1gFnSc2
(	(	kIx(
<g/>
50	#num#	k4
stupňů	stupeň	k1gInPc2
od	od	k7c2
svislice	svislice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
elevační	elevační	k2eAgInSc1d1
úhel	úhel	k1gInSc1
je	být	k5eAaImIp3nS
40	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s>
Majáky	maják	k1gInPc1
VOR	vor	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
majáků	maják	k1gInPc2
VOR	vor	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NázevIdentifikační	NázevIdentifikační	k2eAgFnSc1d1
znakFrekvence	znakFrekvence	k1gFnSc1
[	[	kIx(
<g/>
MHz	Mhz	kA
<g/>
]	]	kIx)
<g/>
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
BrnoBNO	BrnoBNO	k?
<g/>
114,454	114,454	k4
<g/>
9	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
DešnáOKF	DešnáOKF	k?
<g/>
113,154	113,154	k4
<g/>
8	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
9	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
44	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
FrýdlantOKX	FrýdlantOKX	k?
<g/>
114,855	114,855	k4
<g/>
0	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
ChebOKG	ChebOKG	k?
<g/>
115,750	115,750	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
NeratoviceNER	NeratoviceNER	k?
<g/>
112,255	112,255	k4
<g/>
0	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
OstravaOTA	OstravaOTA	k?
<g/>
117,454	117,454	k4
<g/>
9	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
PísekPSK	PísekPSK	k?
<g/>
117,604	117,604	k4
<g/>
9	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
PrahaOKL	PrahaOKL	k?
<g/>
112,650	112,650	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
ŘevničovRVC	ŘevničovRVC	k?
<g/>
114,655	114,655	k4
<g/>
0	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
VlašimVLM	VlašimVLM	k?
<g/>
114,349	114,349	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
VožiceVOZ	VožiceVOZ	k?
<g/>
116,954	116,954	k4
<g/>
9	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Radionavigační	radionavigační	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
/	/	kIx~
<g/>
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
ČR	ČR	kA
2	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Traťová	traťový	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://airnav.eu/index.php?stranka=VOR	http://airnav.eu/index.php?stranka=VOR	k?
</s>
