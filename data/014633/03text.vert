<s>
Hugh	Hugha	k1gFnPc2
Alexander	Alexandra	k1gFnPc2
Kennedy	Kenneda	k1gMnSc2
</s>
<s>
Hugh	Hugha	k1gFnPc2
Alexander	Alexandra	k1gFnPc2
Kennedy	Kenneda	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1809	#num#	k4
<g/>
Madrás	Madrás	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1878	#num#	k4
<g/>
Reading	Reading	k1gInSc1
<g/>
,	,	kIx,
Berkshire	Berkshir	k1gInSc5
Národnost	národnost	k1gFnSc1
</s>
<s>
anglická	anglický	k2eAgNnPc4d1
Občanství	občanství	k1gNnPc4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
armádní	armádní	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
a	a	k8xC
šachista	šachista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hugh	Hugha	k1gFnPc2
Alexander	Alexandra	k1gFnPc2
Kennedy	Kenneda	k1gMnSc2
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1809	#num#	k4
<g/>
,	,	kIx,
Madrás	Madrás	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1878	#num#	k4
<g/>
,	,	kIx,
Reading	Reading	k1gInSc1
<g/>
,	,	kIx,
Berkshire	Berkshir	k1gMnSc5
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
armádní	armádní	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
a	a	k8xC
anglický	anglický	k2eAgMnSc1d1
šachový	šachový	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
londýnských	londýnský	k2eAgMnPc2d1
šachistů	šachista	k1gMnPc2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1842	#num#	k4
založil	založit	k5eAaPmAgMnS
první	první	k4xOgInSc4
šachový	šachový	k2eAgInSc4d1
klub	klub	k1gInSc4
v	v	k7c6
Brightonu	Brighton	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
Kennedyho	Kennedy	k1gMnSc2
šachových	šachový	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
lze	lze	k6eAd1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
<g/>
:	:	kIx,
</s>
<s>
roku	rok	k1gInSc2
1844	#num#	k4
prohru	prohra	k1gFnSc4
v	v	k7c6
zápase	zápas	k1gInSc6
s	s	k7c7
Howardem	Howard	k1gMnSc7
Stauntonem	Staunton	k1gInSc7
v	v	k7c6
poměru	poměr	k1gInSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
roku	rok	k1gInSc2
1845	#num#	k4
účast	účast	k1gFnSc1
na	na	k7c6
dvou	dva	k4xCgFnPc6
telegrafických	telegrafický	k2eAgFnPc6d1
partiích	partie	k1gFnPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
Liverpoolu	Liverpool	k1gInSc6
společně	společně	k6eAd1
Howardem	Howard	k1gInSc7
Stauntonem	Staunton	k1gInSc7
proti	proti	k7c3
týmu	tým	k1gInSc3
v	v	k7c6
Londýně	Londýn	k1gInSc6
složenému	složený	k2eAgNnSc3d1
z	z	k7c2
Henryho	Henry	k1gMnSc2
Thomase	Thomas	k1gMnSc2
Buckleho	Buckle	k1gMnSc2
<g/>
,	,	kIx,
Georgeho	George	k1gMnSc2
Walkera	Walker	k1gMnSc2
<g/>
,	,	kIx,
Williama	William	k1gMnSc2
Davise	Davise	k1gFnSc1
Evanse	Evanse	k1gFnSc1
<g/>
,	,	kIx,
Georga	Georga	k1gFnSc1
Perigala	Perigala	k1gFnSc1
<g/>
1	#num#	k4
a	a	k8xC
Williama	William	k1gMnSc4
Josiaha	Josiah	k1gMnSc4
Tucketta	Tuckett	k1gMnSc4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	s	k7c7
Stauntonem	Staunton	k1gInSc7
jednu	jeden	k4xCgFnSc4
partii	partie	k1gFnSc4
remizovali	remizovat	k5eAaPmAgMnP
a	a	k8xC
jednu	jeden	k4xCgFnSc4
prohráli	prohrát	k5eAaPmAgMnP
<g/>
,	,	kIx,
</s>
<s>
roku	rok	k1gInSc2
1846	#num#	k4
prohru	prohra	k1gFnSc4
v	v	k7c6
zápase	zápas	k1gInSc6
s	s	k7c7
Eliahem	Eliah	k1gMnSc7
Williamsem	Williams	k1gMnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
roku	rok	k1gInSc2
1849	#num#	k4
prohru	prohra	k1gFnSc4
rovněž	rovněž	k6eAd1
v	v	k7c6
Londýně	Londýn	k1gInSc6
s	s	k7c7
Eduardem	Eduard	k1gMnSc7
Löwem	Löw	k1gMnSc7
6	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
účast	účast	k1gFnSc1
na	na	k7c6
šachovém	šachový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
roku	rok	k1gInSc2
1851	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgMnS
šestý	šestý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
porazil	porazit	k5eAaPmAgMnS
Karla	Karel	k1gMnSc4
Mayeta	Mayeto	k1gNnSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
druhém	druhý	k4xOgMnSc6
prohrál	prohrát	k5eAaPmAgMnS
s	s	k7c7
pozdějším	pozdní	k2eAgMnSc7d2
finalistou	finalista	k1gMnSc7
Marmadukem	Marmaduk	k1gMnSc7
Wyvillem	Wyvill	k1gMnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zápasech	zápas	k1gInPc6
o	o	k7c6
páté	pátá	k1gFnSc6
až	až	k9
osmé	osmý	k4xOgNnSc4
místo	místo	k1gNnSc4
nejprve	nejprve	k6eAd1
porazil	porazit	k5eAaPmAgMnS
J.	J.	kA
R.	R.	kA
Mucklowa	Mucklowa	k1gFnSc1
<g/>
1	#num#	k4
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
prohrál	prohrát	k5eAaPmAgMnS
s	s	k7c7
Józsefem	József	k1gMnSc7
Szénem	Szén	k1gMnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
George	Georg	k1gInSc2
Perigal	Perigal	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Josiah	Josiah	k1gMnSc1
Tuckett	Tuckett	k1gMnSc1
a	a	k8xC
J.	J.	kA
R.	R.	kA
Mucklow	Mucklow	k1gMnPc1
<g/>
,	,	kIx,
angličtí	anglický	k2eAgMnPc1d1
šachisté	šachista	k1gMnPc1
<g/>
,	,	kIx,
bližší	blízký	k2eAgFnPc1d2
podrobnosti	podrobnost	k1gFnPc1
nezjištěny	zjištěn	k2eNgFnPc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Gaige	Gaige	k1gInSc1
<g/>
,	,	kIx,
Jeremy	Jerem	k1gInPc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chess	Chess	k1gInSc1
Personalia	Personalia	k1gFnSc1
<g/>
,	,	kIx,
A	a	k8xC
Biobibliography	Biobibliographa	k1gFnPc1
<g/>
,	,	kIx,
McFarland	McFarland	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
209	#num#	k4
<g/>
↑	↑	k?
http://www.chessgames.com/perl/chessplayer?pid=10333	http://www.chessgames.com/perl/chessplayer?pid=10333	k4
<g/>
↑	↑	k?
The	The	k1gFnSc1
Brighton	Brighton	k1gInSc1
Chess	Chess	k1gInSc1
Club	club	k1gInSc1
<g/>
:	:	kIx,
Introduction	Introduction	k1gInSc1
and	and	k?
Background	Background	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Formation	Formation	k1gInSc1
of	of	k?
the	the	k?
First	First	k1gInSc1
Club	club	k1gInSc1
<g/>
↑	↑	k?
Edo	Eda	k1gMnSc5
Ratings	Ratings	k1gInSc4
<g/>
,	,	kIx,
Kennedy	Kenned	k1gMnPc4
<g/>
,	,	kIx,
H.A	H.A	k1gFnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://www.chesscafe.com/text/spinrad15.pdf	http://www.chesscafe.com/text/spinrad15.pdf	k1gMnSc1
<g/>
↑	↑	k?
The	The	k1gMnSc1
Kibitzer	Kibitzer	k1gMnSc1
<g/>
1	#num#	k4
2	#num#	k4
I	i	k8xC
grandi	grand	k1gMnPc1
matches	matchesa	k1gFnPc2
fino	fino	k6eAd1
al	ala	k1gFnPc2
1849	#num#	k4
<g/>
↑	↑	k?
http://xoomer.alice.it/cserica/scacchi/storiascacchi/tornei/1851-99/1851London.htm	http://xoomer.alice.it/cserica/scacchi/storiascacchi/tornei/1851-99/1851London.htm	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
ChessGames	ChessGames	k1gInSc1
–	–	k?
Hugh	Hugh	k1gInSc1
Alexander	Alexandra	k1gFnPc2
Kennedy	Kenneda	k1gMnSc2
(	(	kIx(
<g/>
partie	partie	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
9627	#num#	k4
5553	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
291222704	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šachy	šach	k1gInPc1
</s>
