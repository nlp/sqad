<s>
Kašťa	Kašť	k2eAgFnSc1d1
bělavá	bělavý	k2eAgFnSc1d1
</s>
<s>
Kašťa	Kašť	k2eAgFnSc1d1
bělavá	bělavý	k2eAgFnSc1d1
Kašťa	Kašťa	k1gFnSc1
bělavá	bělavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
<g/>
)	)	kIx)
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
nižší	nízký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Magnoliopsida	Magnoliopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
vavřínotvaré	vavřínotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Laurales	Laurales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
vavřínovité	vavřínovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Lauraceae	Lauracea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
kašťa	kašťa	k1gFnSc1
(	(	kIx(
<g/>
Sassafras	sassafras	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
<g/>
(	(	kIx(
<g/>
Nutt	Nutt	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Nees	Nees	k1gInSc1
<g/>
,	,	kIx,
1836	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Laurus	Laurus	k1gInSc1
sassafras	sassafras	k1gInSc1
</s>
<s>
Sassafras	sassafras	k1gInSc1
officinale	officinale	k6eAd1
</s>
<s>
sassafras	sassafras	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nákres	nákres	k1gInSc1
větvičky	větvička	k1gFnSc2
s	s	k7c7
plody	plod	k1gInPc7
</s>
<s>
Kašťa	Kašťa	k1gFnSc1
bělavá	bělavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
strom	strom	k1gInSc1
nebo	nebo	k8xC
jen	jen	k9
rozložitý	rozložitý	k2eAgInSc1d1
keř	keř	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
původem	původ	k1gInSc7
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgInPc2
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
kašťa	kašťa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druh	druh	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
roste	růst	k5eAaImIp3nS
od	od	k7c2
Atlantského	atlantský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
po	po	k7c4
východ	východ	k1gInSc4
Texasu	Texas	k1gInSc2
<g/>
,	,	kIx,
druhé	druhý	k4xOgInPc1
dva	dva	k4xCgInPc1
druhy	druh	k1gInPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
kvůli	kvůli	k7c3
léčivým	léčivý	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
i	i	k8xC
efektnímu	efektní	k2eAgInSc3d1
vzhledu	vzhled	k1gInSc3
dovezen	dovezen	k2eAgMnSc1d1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
<g/>
,	,	kIx,
opadavá	opadavý	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ve	v	k7c6
formě	forma	k1gFnSc6
stromu	strom	k1gInSc2
nebo	nebo	k8xC
keře	keř	k1gInSc2
s	s	k7c7
množstvím	množství	k1gNnSc7
zahušťujících	zahušťující	k2eAgInPc2d1
kořenových	kořenový	k2eAgInPc2d1
výběžků	výběžek	k1gInPc2
vyrůstajících	vyrůstající	k2eAgInPc2d1
z	z	k7c2
plytkých	plytký	k2eAgNnPc2d1
<g/>
,	,	kIx,
široce	široko	k6eAd1
rozložených	rozložený	k2eAgInPc2d1
kořenů	kořen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
strom	strom	k1gInSc1
s	s	k7c7
rozbrázděnou	rozbrázděný	k2eAgFnSc7d1
<g/>
,	,	kIx,
oranžově	oranžově	k6eAd1
hnědou	hnědý	k2eAgFnSc7d1
kůrou	kůra	k1gFnSc7
dorůstá	dorůstat	k5eAaImIp3nS
do	do	k7c2
výšky	výška	k1gFnSc2
10	#num#	k4
až	až	k9
20	#num#	k4
m	m	kA
a	a	k8xC
mívá	mívat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
úzkou	úzký	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
bývá	bývat	k5eAaImIp3nS
pyramidální	pyramidální	k2eAgNnPc1d1
<g/>
,	,	kIx,
oválná	oválný	k2eAgNnPc1d1
nebo	nebo	k8xC
zcela	zcela	k6eAd1
nepravidelná	pravidelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letorosty	letorost	k1gInPc7
jsou	být	k5eAaImIp3nP
leskle	leskle	k6eAd1
zelené	zelený	k2eAgInPc1d1
až	až	k6eAd1
žlutozelené	žlutozelený	k2eAgInPc1d1
a	a	k8xC
střídavě	střídavě	k6eAd1
porůstají	porůstat	k5eAaImIp3nP
řapíkatými	řapíkatý	k2eAgInPc7d1
listy	list	k1gInPc7
vejčitého	vejčitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
s	s	k7c7
klínovitou	klínovitý	k2eAgFnSc7d1
bázi	báze	k1gFnSc6
bývají	bývat	k5eAaImIp3nP
variabilní	variabilní	k2eAgFnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
jednom	jeden	k4xCgInSc6
stromu	strom	k1gInSc6
se	se	k3xPyFc4
současně	současně	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
celokrajné	celokrajný	k2eAgFnPc1d1
<g/>
,	,	kIx,
dvoulaločné	dvoulaločný	k2eAgFnPc1d1
(	(	kIx(
<g/>
nesymetrické	symetrický	k2eNgFnPc1d1
<g/>
)	)	kIx)
i	i	k8xC
trojlaločné	trojlaločný	k2eAgFnPc1d1
(	(	kIx(
<g/>
symetrické	symetrický	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgInPc4d1
10	#num#	k4
až	až	k8xS
15	#num#	k4
a	a	k8xC
široké	široký	k2eAgNnSc4d1
5	#num#	k4
až	až	k9
10	#num#	k4
cm	cm	kA
a	a	k8xC
mají	mít	k5eAaImIp3nP
vždy	vždy	k6eAd1
zaoblené	zaoblený	k2eAgInPc4d1
konce	konec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
barvu	barva	k1gFnSc4
svěže	svěže	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
se	se	k3xPyFc4
zbarvují	zbarvovat	k5eAaImIp3nP
žlutě	žlutě	k6eAd1
<g/>
,	,	kIx,
oranžově	oranžově	k6eAd1
nebo	nebo	k8xC
červeně	červeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
poškození	poškození	k1gNnSc4
kořene	kořen	k1gInSc2
nebo	nebo	k8xC
kmene	kmen	k1gInSc2
vyrůstá	vyrůstat	k5eAaImIp3nS
u	u	k7c2
paty	pata	k1gFnSc2
stromu	strom	k1gInSc2
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
kořenových	kořenový	k2eAgFnPc2d1
odnoží	odnož	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgFnPc1d1
pro	pro	k7c4
množení	množení	k1gNnSc4
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozemnuté	rozemnutý	k2eAgInPc1d1
listy	list	k1gInPc1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
poraněná	poraněný	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
zvláštní	zvláštní	k2eAgInPc1d1
<g/>
,	,	kIx,
ne	ne	k9
všem	všecek	k3xTgFnPc3
příjemné	příjemný	k2eAgNnSc1d1
kořeněné	kořeněný	k2eAgNnSc1d1
aroma	aroma	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Zelenožluté	zelenožlutý	k2eAgInPc4d1
samčí	samčí	k2eAgInPc4d1
nebo	nebo	k8xC
samičí	samičí	k2eAgInPc4d1
květy	květ	k1gInPc4
rostou	růst	k5eAaImIp3nP
na	na	k7c6
oddělených	oddělený	k2eAgInPc6d1
stromech	strom	k1gInPc6
v	v	k7c6
malokvětých	malokvětý	k2eAgInPc6d1
koncových	koncový	k2eAgInPc6d1
převislých	převislý	k2eAgInPc6d1
hroznech	hrozen	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
počátku	počátek	k1gInSc2
zkrácené	zkrácený	k2eAgInPc4d1
a	a	k8xC
květy	květ	k1gInPc1
směstnané	směstnaný	k2eAgInPc1d1
do	do	k7c2
svazečků	svazeček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vonné	vonný	k2eAgInPc1d1
květy	květ	k1gInPc1
<g/>
,	,	kIx,
mívající	mívající	k2eAgInPc1d1
v	v	k7c6
průměru	průměr	k1gInSc6
asi	asi	k9
7	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
rozkvétají	rozkvétat	k5eAaImIp3nP
v	v	k7c6
březnu	březen	k1gInSc6
a	a	k8xC
dubnu	duben	k1gInSc6
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
ještě	ještě	k9
před	před	k7c7
vyrašením	vyrašení	k1gNnSc7
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednopohlavnost	jednopohlavnost	k1gFnSc1
květů	květ	k1gInPc2
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
zakrněním	zakrnění	k1gNnSc7
tyčinek	tyčinka	k1gFnPc2
nebo	nebo	k8xC
pestíků	pestík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
okvětí	okvětí	k1gNnSc1
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgInSc1d1
ve	v	k7c6
dvou	dva	k4xCgInPc6
kruzích	kruh	k1gInPc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
šest	šest	k4xCc4
okvětních	okvětní	k2eAgInPc2d1
plátků	plátek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samčí	samčí	k2eAgInSc1d1
květ	květ	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
šest	šest	k4xCc1
nebo	nebo	k8xC
devět	devět	k4xCc1
tyčinek	tyčinka	k1gFnPc2
přirostlých	přirostlý	k2eAgMnPc2d1
u	u	k7c2
ústí	ústí	k1gNnSc2
květní	květní	k2eAgFnSc2d1
trubky	trubka	k1gFnSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc1
prašníky	prašník	k1gInPc1
se	se	k3xPyFc4
otvírají	otvírat	k5eAaImIp3nP
chlopněmi	chlopeň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samičí	samičí	k2eAgInSc1d1
květ	květ	k1gInSc1
má	mít	k5eAaImIp3nS
svrchní	svrchní	k2eAgInSc1d1
semeník	semeník	k1gInSc1
s	s	k7c7
jednoduchou	jednoduchý	k2eAgFnSc7d1
čnělkou	čnělka	k1gFnSc7
s	s	k7c7
hlavičkovitou	hlavičkovitý	k2eAgFnSc7d1
bliznou	blizna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ploidie	Ploidie	k1gFnSc1
druhu	druh	k1gInSc2
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
n	n	k0
=	=	kIx~
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Opylené	Opylený	k2eAgInPc4d1
samičí	samičí	k2eAgInPc4d1
květy	květ	k1gInPc4
vytvářejí	vytvářet	k5eAaImIp3nP
jednosemenné	jednosemenný	k2eAgInPc4d1
plody	plod	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
modré	modrý	k2eAgFnPc1d1
<g/>
,	,	kIx,
asi	asi	k9
1	#num#	k4
cm	cm	kA
velké	velký	k2eAgFnSc2d1
a	a	k8xC
masité	masitý	k2eAgFnSc2d1
peckovice	peckovice	k1gFnSc2
mající	mající	k2eAgInSc4d1
vespod	vespod	k6eAd1
šarlatově	šarlatově	k6eAd1
zbarvenou	zbarvený	k2eAgFnSc4d1
číšku	číška	k1gFnSc4
na	na	k7c6
rudých	rudý	k2eAgFnPc6d1
stopkách	stopka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dozrávají	dozrávat	k5eAaImIp3nP
v	v	k7c6
září	září	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
oblíbenou	oblíbený	k2eAgFnSc7d1
potravou	potrava	k1gFnSc7
pro	pro	k7c4
ptáky	pták	k1gMnPc4
a	a	k8xC
hlodavce	hlodavec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Ekologie	ekologie	k1gFnSc1
</s>
<s>
Kašťa	Kašťa	k6eAd1
bělavá	bělavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
dřevinou	dřevina	k1gFnSc7
žádající	žádající	k2eAgNnSc1d1
plné	plný	k2eAgNnSc1d1
oslunění	oslunění	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
skupině	skupina	k1gFnSc6
ostatních	ostatní	k2eAgInPc2d1
stromů	strom	k1gInPc2
roste	růst	k5eAaImIp3nS
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
a	a	k8xC
většinou	většina	k1gFnSc7
bývá	bývat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
citlivá	citlivý	k2eAgFnSc1d1
i	i	k9
na	na	k7c4
malý	malý	k2eAgInSc4d1
požár	požár	k1gInSc4
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
může	moct	k5eAaImIp3nS
snadno	snadno	k6eAd1
zahubit	zahubit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlépe	dobře	k6eAd3
roste	růst	k5eAaImIp3nS
v	v	k7c6
propustné	propustný	k2eAgFnSc6d1
<g/>
,	,	kIx,
vlhké	vlhký	k2eAgFnSc6d1
<g/>
,	,	kIx,
kyselé	kyselý	k2eAgFnSc3d1
<g/>
,	,	kIx,
jílovité	jílovitý	k2eAgFnSc3d1
půdě	půda	k1gFnSc3
<g/>
;	;	kIx,
snáší	snášet	k5eAaImIp3nS
však	však	k9
i	i	k9
půdy	půda	k1gFnPc1
písčité	písčitý	k2eAgFnPc1d1
a	a	k8xC
suché	suchý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
obvykle	obvykle	k6eAd1
produkuje	produkovat	k5eAaImIp3nS
velká	velký	k2eAgNnPc4d1
množství	množství	k1gNnPc4
kořenových	kořenový	k2eAgInPc2d1
výhonků	výhonek	k1gInPc2
<g/>
,	,	kIx,
vyroste	vyrůst	k5eAaPmIp3nS
bez	bez	k7c2
jejich	jejich	k3xOp3gNnPc2
odstraňování	odstraňování	k1gNnPc2
široký	široký	k2eAgInSc1d1
<g/>
,	,	kIx,
mnohokmenný	mnohokmenný	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
volném	volný	k2eAgNnSc6d1
prostranství	prostranství	k1gNnSc6
tak	tak	k9
vytvoří	vytvořit	k5eAaPmIp3nS
rozlehlou	rozlehlý	k2eAgFnSc4d1
kolonii	kolonie	k1gFnSc4
pocházející	pocházející	k2eAgFnSc4d1
z	z	k7c2
jediného	jediný	k2eAgInSc2d1
exempláře	exemplář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stromy	strom	k1gInPc1
začínají	začínat	k5eAaImIp3nP
kvést	kvést	k5eAaImF
ve	v	k7c6
věku	věk	k1gInSc6
asi	asi	k9
10	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
nejvíce	hodně	k6eAd3,k6eAd1
plodů	plod	k1gInPc2
produkují	produkovat	k5eAaImIp3nP
ve	v	k7c6
25	#num#	k4
až	až	k8xS
50	#num#	k4
létech	léto	k1gNnPc6
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
plodí	plodit	k5eAaImIp3nS
ob	ob	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zralá	zralý	k2eAgNnPc1d1
semena	semeno	k1gNnPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
plody	plod	k1gInPc7
šířena	šířit	k5eAaImNgFnS
hlavně	hlavně	k6eAd1
ptáky	pták	k1gMnPc4
nebo	nebo	k8xC
vodou	voda	k1gFnSc7
při	při	k7c6
záplavách	záplava	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dormance	Dormanka	k1gFnSc3
semen	semeno	k1gNnPc2
obvykle	obvykle	k6eAd1
končí	končit	k5eAaImIp3nS
až	až	k9
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
klíčivost	klíčivost	k1gFnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
35	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dormance	Dormanka	k1gFnSc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
uspíšit	uspíšit	k5eAaPmF
stratifikaci	stratifikace	k1gFnSc3
po	po	k7c4
dobu	doba	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
ve	v	k7c6
vlhkém	vlhký	k2eAgInSc6d1
písku	písek	k1gInSc6
při	při	k7c6
teplotě	teplota	k1gFnSc6
5	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Semena	semeno	k1gNnPc4
klíčí	klíčit	k5eAaImIp3nS
ještě	ještě	k9
i	i	k9
po	po	k7c6
šesti	šest	k4xCc6
létech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
stromy	strom	k1gInPc7
mnohem	mnohem	k6eAd1
častěji	často	k6eAd2
šíří	šířit	k5eAaImIp3nS
odnožemi	odnož	k1gFnPc7
než	než	k8xS
semeny	semeno	k1gNnPc7
<g/>
,	,	kIx,
také	také	k9
pařezy	pařez	k1gInPc1
stromů	strom	k1gInPc2
silně	silně	k6eAd1
obrůstají	obrůstat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobře	dobře	k6eAd1
se	se	k3xPyFc4
množí	množit	k5eAaImIp3nP
kořenovými	kořenový	k2eAgInPc7d1
řízky	řízek	k1gInPc7
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
řízky	řízek	k1gInPc1
z	z	k7c2
větví	větev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnPc4d2
rostliny	rostlina	k1gFnPc4
se	se	k3xPyFc4
velmi	velmi	k6eAd1
špatně	špatně	k6eAd1
přesazují	přesazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
nevyhovujících	vyhovující	k2eNgInPc2d1
půdních	půdní	k2eAgInPc2d1
nebo	nebo	k8xC
klimatických	klimatický	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
je	být	k5eAaImIp3nS
kmen	kmen	k1gInSc1
krátký	krátký	k2eAgInSc1d1
nebo	nebo	k8xC
vyrůstá	vyrůstat	k5eAaImIp3nS
jen	jen	k9
keř	keř	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
mladých	mladý	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
bývají	bývat	k5eAaImIp3nP
šťavnaté	šťavnatý	k2eAgFnPc1d1
větvičky	větvička	k1gFnPc1
v	v	k7c6
zimě	zima	k1gFnSc6
a	a	k8xC
mladé	mladý	k2eAgInPc1d1
listy	list	k1gInPc1
na	na	k7c6
jaře	jaro	k1gNnSc6
okousávány	okousáván	k2eAgInPc1d1
vysokou	vysoký	k2eAgFnSc7d1
zvěří	zvěř	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Kašťa	Kašťa	k6eAd1
bělavá	bělavý	k2eAgFnSc1d1
má	mít	k5eAaImIp3nS
světlé	světlý	k2eAgNnSc4d1
<g/>
,	,	kIx,
lehké	lehký	k2eAgNnSc4d1
a	a	k8xC
měkké	měkký	k2eAgNnSc4d1
dřevo	dřevo	k1gNnSc4
<g/>
,	,	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
nábytek	nábytek	k1gInSc1
<g/>
,	,	kIx,
dýhy	dýha	k1gFnPc1
a	a	k8xC
drobné	drobný	k2eAgInPc1d1
předměty	předmět	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
prosáklé	prosáklý	k2eAgNnSc1d1
éterickým	éterický	k2eAgInSc7d1
olejem	olej	k1gInSc7
obsahujícím	obsahující	k2eAgNnSc6d1
asi	asi	k9
80	#num#	k4
%	%	kIx~
safrolu	safrol	k1gInSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc4
mj.	mj.	kA
působí	působit	k5eAaImIp3nS
insekticidně	insekticidně	k6eAd1
a	a	k8xC
proto	proto	k8xC
dřevo	dřevo	k1gNnSc1
není	být	k5eNaImIp3nS
napadáno	napadat	k5eAaBmNgNnS,k5eAaPmNgNnS
hmyzem	hmyz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
olej	olej	k1gInSc1
extrahoval	extrahovat	k5eAaBmAgInS
z	z	k7c2
kořenů	kořen	k1gInPc2
i	i	k8xC
kůry	kůra	k1gFnSc2
a	a	k8xC
přidával	přidávat	k5eAaImAgMnS
do	do	k7c2
kosmetických	kosmetický	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
toaletních	toaletní	k2eAgNnPc2d1
mýdel	mýdlo	k1gNnPc2
<g/>
,	,	kIx,
napouštěl	napouštět	k5eAaImAgMnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
vonný	vonný	k2eAgInSc4d1
tabák	tabák	k1gInSc4
nebo	nebo	k8xC
se	se	k3xPyFc4
ochucovaly	ochucovat	k5eAaImAgInP
likéry	likér	k1gInPc1
<g/>
,	,	kIx,
cukrářské	cukrářský	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
<g/>
,	,	kIx,
čaje	čaj	k1gInSc2
<g/>
,	,	kIx,
limonády	limonáda	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
také	také	k9
důležitou	důležitý	k2eAgFnSc4d1
součásti	součást	k1gFnPc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
populárního	populární	k2eAgNnSc2d1
kořenového	kořenový	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c4
zjištěné	zjištěný	k2eAgFnPc4d1
potenciální	potenciální	k2eAgFnPc4d1
karcinogenity	karcinogenita	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
používání	používání	k1gNnSc1
safrolu	safrol	k1gInSc2
u	u	k7c2
výrobků	výrobek	k1gInPc2
zakázáno	zakázat	k5eAaPmNgNnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
vůně	vůně	k1gFnSc1
je	být	k5eAaImIp3nS
vyráběna	vyrábět	k5eAaImNgFnS
synteticky	synteticky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Samčí	samčí	k2eAgNnSc1d1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
samičí	samičí	k2eAgInSc4d1
květ	květ	k1gInSc4
</s>
<s>
Samičí	samičí	k2eAgNnSc1d1
květenství	květenství	k1gNnSc1
</s>
<s>
Plody	plod	k1gInPc1
</s>
<s>
Listy	list	k1gInPc1
</s>
<s>
Podzimní	podzimní	k2eAgInPc1d1
listy	list	k1gInPc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Sasafras	Sasafras	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
</s>
<s>
Kušťa	Kušťa	k1gFnSc1
(	(	kIx(
<g/>
lidový	lidový	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Dendrologie	dendrologie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Kašťa	Kašťa	k1gFnSc1
bělavá	bělavý	k2eAgFnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
P.	P.	kA
Horáček	Horáček	k1gMnSc1
a	a	k8xC
J.	J.	kA
Mencl	Mencl	k1gMnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
31.12	31.12	k4
<g/>
.2006	.2006	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POLÍVKA	Polívka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užitkové	užitkový	k2eAgFnPc4d1
a	a	k8xC
pamětihodné	pamětihodný	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
cizích	cizí	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
:	:	kIx,
Sassafras	sassafras	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wendys	Wendys	k1gInSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
1908	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRIGGS	GRIGGS	kA
<g/>
,	,	kIx,
Margene	Margen	k1gInSc5
M.	M.	kA
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Forest	Forest	k1gMnSc1
Service	Service	k1gFnSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
C.	C.	kA
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Plant	planta	k1gFnPc2
Finder	Finder	k1gMnSc1
<g/>
:	:	kIx,
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
MO	MO	kA
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHRISTMAN	CHRISTMAN	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plant	planta	k1gFnPc2
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
:	:	kIx,
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Floridata	Floridata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
Tallahassee	Tallahassee	k1gInSc1
<g/>
,	,	kIx,
FL	FL	kA
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
27.09	27.09	k4
<g/>
.2006	.2006	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flora	Flora	k1gMnSc1
of	of	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
MO	MO	kA
&	&	k?
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Herbaria	Herbarium	k1gNnSc2
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
MA	MA	kA
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kašťa	kašťa	k1gFnSc1
bělavá	bělavý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
