<s>
Kašťa	Kašťa	k1gFnSc1
bělavá	bělavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sassafras	sassafras	k1gInSc1
albidum	albidum	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
strom	strom	k1gInSc1
nebo	nebo	k8xC
jen	jen	k9
rozložitý	rozložitý	k2eAgInSc1d1
keř	keř	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
původem	původ	k1gInSc7
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgInPc2
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
kašťa	kašťa	k1gFnSc1
<g/>
.	.	kIx.
</s>