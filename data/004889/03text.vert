<s>
Garota	Garota	k1gFnSc1	Garota
<g/>
,	,	kIx,	,
též	též	k9	též
garotta	garotta	k1gMnSc1	garotta
nebo	nebo	k8xC	nebo
garote	garot	k1gInSc5	garot
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šp	šp	k?	šp
<g/>
.	.	kIx.	.
garrote	garrot	k1gInSc5	garrot
je	být	k5eAaImIp3nS	být
popravčí	popravčí	k2eAgInSc4d1	popravčí
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
kdysi	kdysi	k6eAd1	kdysi
pod	pod	k7c7	pod
jejich	jejich	k3xOp3gInSc7	jejich
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Posazený	posazený	k2eAgMnSc1d1	posazený
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
byl	být	k5eAaImAgMnS	být
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
zařízení	zařízení	k1gNnSc2	zařízení
udušen	udušen	k2eAgMnSc1d1	udušen
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
zlomen	zlomen	k2eAgInSc4d1	zlomen
hrtan	hrtan	k1gInSc4	hrtan
a	a	k8xC	a
trachea	trachea	k1gFnSc1	trachea
<g/>
.	.	kIx.	.
</s>
<s>
Garota	Garota	k1gFnSc1	Garota
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
už	už	k9	už
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nikde	nikde	k6eAd1	nikde
pomocí	pomocí	k7c2	pomocí
garoty	garota	k1gFnSc2	garota
nepopravuje	popravovat	k5eNaImIp3nS	popravovat
<g/>
.	.	kIx.	.
</s>
<s>
Garota	Garota	k1gFnSc1	Garota
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
zardoušením	zardoušení	k1gNnPc3	zardoušení
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
lavici	lavice	k1gFnSc6	lavice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
opřen	opřít	k5eAaPmNgInS	opřít
o	o	k7c4	o
kůl	kůl	k1gInSc4	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kůlu	kůl	k1gInSc6	kůl
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc1d1	připevněn
železný	železný	k2eAgInSc1d1	železný
kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
obojek	obojek	k1gInSc1	obojek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
svírá	svírat	k5eAaImIp3nS	svírat
krk	krk	k1gInSc1	krk
<g/>
.	.	kIx.	.
</s>
<s>
Obojek	obojek	k1gInSc1	obojek
je	být	k5eAaImIp3nS	být
pomalu	pomalu	k6eAd1	pomalu
utahován	utahovat	k5eAaImNgInS	utahovat
šroubem	šroub	k1gInSc7	šroub
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
neudusí	udusit	k5eNaPmIp3nS	udusit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
garota	garota	k1gFnSc1	garota
drát	drát	k5eAaImF	drát
se	s	k7c7	s
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
rukojeťmi	rukojeť	k1gFnPc7	rukojeť
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
držena	držet	k5eAaImNgFnS	držet
popravčím	popravčí	k1gMnSc7	popravčí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
další	další	k2eAgInSc1d1	další
typ	typ	k1gInSc1	typ
garoty	garota	k1gFnSc2	garota
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
šroubu	šroub	k1gInSc3	šroub
připevněna	připevněn	k2eAgNnPc4d1	připevněno
dvě	dva	k4xCgNnPc4	dva
ramena	rameno	k1gNnPc4	rameno
se	s	k7c7	s
zátěží	zátěž	k1gFnSc7	zátěž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Stačilo	stačit	k5eAaBmAgNnS	stačit
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
jen	jen	k9	jen
prudce	prudko	k6eAd1	prudko
škubnout	škubnout	k5eAaPmF	škubnout
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
otáčející	otáčející	k2eAgInSc4d1	otáčející
šroub	šroub	k1gInSc4	šroub
zlomil	zlomit	k5eAaPmAgMnS	zlomit
tracheu	trachea	k1gFnSc4	trachea
a	a	k8xC	a
hrtan	hrtan	k1gInSc4	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kůlu	kůl	k1gInSc6	kůl
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
výstupek	výstupek	k1gInSc1	výstupek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
rozdrtil	rozdrtit	k5eAaPmAgInS	rozdrtit
malý	malý	k2eAgInSc1d1	malý
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
se	se	k3xPyFc4	se
i	i	k9	i
garota	garota	k1gFnSc1	garota
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
prstenci	prstenec	k1gInPc7	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
prstenec	prstenec	k1gInSc1	prstenec
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
prstence	prstenec	k1gInPc1	prstenec
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
sousední	sousední	k2eAgInPc4d1	sousední
obratle	obratel	k1gInPc4	obratel
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
míchy	mícha	k1gFnSc2	mícha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
téměř	téměř	k6eAd1	téměř
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
typů	typ	k1gInPc2	typ
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
tenké	tenký	k2eAgNnSc4d1	tenké
ostří	ostří	k1gNnSc4	ostří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
procházelo	procházet	k5eAaImAgNnS	procházet
skrze	skrze	k?	skrze
kůl	kůl	k1gInSc4	kůl
a	a	k8xC	a
bleskurychle	bleskurychle	k6eAd1	bleskurychle
přeťalo	přetít	k5eAaPmAgNnS	přetít
míchu	mícha	k1gFnSc4	mícha
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
obratli	obratel	k1gInPc7	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
garota	garota	k1gFnSc1	garota
užívána	užívat	k5eAaImNgFnS	užívat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
i	i	k9	i
při	při	k7c6	při
popravě	poprava	k1gFnSc6	poprava
inckého	incký	k2eAgMnSc2d1	incký
krále	král	k1gMnSc2	král
Atahualpy	Atahualpa	k1gFnSc2	Atahualpa
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
garota	garota	k1gFnSc1	garota
užívána	užíván	k2eAgFnSc1d1	užívána
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1828	[number]	k4	1828
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jediným	jediný	k2eAgInSc7d1	jediný
povoleným	povolený	k2eAgInSc7d1	povolený
civilním	civilní	k2eAgInSc7d1	civilní
způsobem	způsob	k1gInSc7	způsob
popravy	poprava	k1gFnSc2	poprava
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
se	se	k3xPyFc4	se
totéž	týž	k3xTgNnSc1	týž
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zrušen	zrušit	k5eAaPmNgInS	zrušit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
a	a	k8xC	a
garota	garota	k1gFnSc1	garota
tam	tam	k6eAd1	tam
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1897	[number]	k4	1897
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
veřejné	veřejný	k2eAgFnSc3d1	veřejná
popravě	poprava	k1gFnSc3	poprava
garotou	garota	k1gFnSc7	garota
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnPc4d1	následující
popravy	poprava	k1gFnPc4	poprava
už	už	k9	už
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
přístupu	přístup	k1gInSc2	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
civilní	civilní	k2eAgFnSc1d1	civilní
poprava	poprava	k1gFnSc1	poprava
garotou	garota	k1gFnSc7	garota
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnPc3d1	poslední
popraveným	popravený	k2eAgMnPc3d1	popravený
byl	být	k5eAaImAgMnS	být
José	Josá	k1gFnPc4	Josá
María	Maríum	k1gNnSc2	Maríum
Jarabo	Jaraba	k1gMnSc5	Jaraba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
civilní	civilní	k2eAgInSc1d1	civilní
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Posledními	poslední	k2eAgMnPc7d1	poslední
popravenými	popravený	k2eAgMnPc7d1	popravený
garotou	garota	k1gFnSc7	garota
byli	být	k5eAaImAgMnP	být
Heinz	Heinz	k1gMnSc1	Heinz
Chez	Chez	k1gMnSc1	Chez
a	a	k8xC	a
Salvador	Salvador	k1gMnSc1	Salvador
Puig	Puig	k1gMnSc1	Puig
Antich	Antich	k1gMnSc1	Antich
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
civilní	civilní	k2eAgInSc1d1	civilní
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
garotou	garota	k1gFnSc7	garota
za	za	k7c4	za
loupežnou	loupežný	k2eAgFnSc4d1	loupežná
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
odsouzený	odsouzený	k1gMnSc1	odsouzený
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
garotou	garota	k1gFnSc7	garota
byl	být	k5eAaImAgMnS	být
pedofilní	pedofilní	k2eAgMnSc1d1	pedofilní
loupežný	loupežný	k2eAgMnSc1d1	loupežný
vrah	vrah	k1gMnSc1	vrah
José	Josá	k1gFnSc2	Josá
Luis	Luisa	k1gFnPc2	Luisa
Cerveto	Cerveto	k1gNnSc1	Cerveto
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prováděn	provádět	k5eAaImNgMnS	provádět
garotou	garota	k1gFnSc7	garota
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgNnSc7d1	poslední
popraveným	popravený	k2eAgNnSc7d1	popravené
Antone	Anton	k1gMnSc5	Anton
Arenis	Arenis	k1gFnSc6	Arenis
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Garota	Garota	k1gFnSc1	Garota
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
např.	např.	kA	např.
Jeden	jeden	k4xCgInSc4	jeden
svět	svět	k1gInSc4	svět
nestačí	stačit	k5eNaBmIp3nS	stačit
filmového	filmový	k2eAgInSc2d1	filmový
cyklu	cyklus	k1gInSc2	cyklus
o	o	k7c4	o
Jamesi	Jamese	k1gFnSc4	Jamese
Bondovi	Bonda	k1gMnSc3	Bonda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
1492	[number]	k4	1492
<g/>
:	:	kIx,	:
Dobytí	dobytí	k1gNnSc4	dobytí
ráje	ráj	k1gInSc2	ráj
režiséra	režisér	k1gMnSc2	režisér
Ridleyho	Ridley	k1gMnSc2	Ridley
Scotta	Scott	k1gMnSc2	Scott
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
Goyovy	Goyův	k2eAgInPc1d1	Goyův
přízraky	přízrak	k1gInPc1	přízrak
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Garota	Garot	k1gMnSc2	Garot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
Oběšení	oběšení	k1gNnSc2	oběšení
</s>
