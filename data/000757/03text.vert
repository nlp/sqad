<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1350	[number]	k4	1350
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
1350	[number]	k4	1350
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1373	[number]	k4	1373
<g/>
-	-	kIx~	-
<g/>
1384	[number]	k4	1384
kancléřem	kancléř	k1gMnSc7	kancléř
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1379	[number]	k4	1379
<g/>
-	-	kIx~	-
<g/>
1396	[number]	k4	1396
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
třetího	třetí	k4xOgMnSc2	třetí
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
a	a	k8xC	a
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
patřili	patřit	k5eAaImAgMnP	patřit
ke	k	k7c3	k
vzdělané	vzdělaný	k2eAgFnSc3d1	vzdělaná
šlechtě	šlechta	k1gFnSc3	šlechta
sloužící	sloužící	k1gFnSc2	sloužící
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
a	a	k8xC	a
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
královský	královský	k2eAgMnSc1d1	královský
notář	notář	k1gMnSc1	notář
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Očko	očko	k1gNnSc4	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc1d1	státní
služba	služba	k1gFnSc1	služba
očekávala	očekávat	k5eAaImAgFnS	očekávat
i	i	k9	i
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Jana	Jan	k1gMnSc2	Jan
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
a	a	k8xC	a
Padově	Padova	k1gFnSc6	Padova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
studiích	studie	k1gFnPc6	studie
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Montpellieru	Montpellier	k1gInSc6	Montpellier
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
brzy	brzy	k6eAd1	brzy
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgMnS	připravovat
na	na	k7c4	na
kariéru	kariéra	k1gFnSc4	kariéra
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
Sorboně	Sorbona	k1gFnSc6	Sorbona
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
přípravě	příprava	k1gFnSc6	příprava
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1373	[number]	k4	1373
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
kanonické	kanonický	k2eAgNnSc4d1	kanonické
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
přítel	přítel	k1gMnSc1	přítel
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jeho	jeho	k3xOp3gMnSc7	jeho
královským	královský	k2eAgMnSc7d1	královský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1375	[number]	k4	1375
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
míšeňským	míšeňský	k2eAgMnSc7d1	míšeňský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1379	[number]	k4	1379
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jmenován	jmenován	k2eAgMnSc1d1	jmenován
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zemským	zemský	k2eAgMnSc7d1	zemský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
královým	králův	k2eAgMnPc3d1	králův
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
přežil	přežít	k5eAaPmAgMnS	přežít
podle	podle	k7c2	podle
dobové	dobový	k2eAgFnSc2d1	dobová
literatury	literatura	k1gFnSc2	literatura
mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
skutečně	skutečně	k6eAd1	skutečně
nákaza	nákaza	k1gFnSc1	nákaza
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
touto	tento	k3xDgFnSc7	tento
chorobou	choroba	k1gFnSc7	choroba
,	,	kIx,	,
neboť	neboť	k8xC	neboť
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
mor	mor	k1gInSc1	mor
<g/>
"	"	kIx"	"
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
označovalo	označovat	k5eAaImAgNnS	označovat
i	i	k9	i
řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgNnPc2d1	jiné
vážných	vážný	k2eAgNnPc2d1	vážné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
Jenštejna	Jenštejn	k1gMnSc2	Jenštejn
projevila	projevit	k5eAaPmAgFnS	projevit
nějaká	nějaký	k3yIgFnSc1	nějaký
forma	forma	k1gFnSc1	forma
epilepsie	epilepsie	k1gFnSc1	epilepsie
způsobená	způsobený	k2eAgFnSc1d1	způsobená
zánětem	zánět	k1gInSc7	zánět
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
různá	různý	k2eAgNnPc4d1	různé
náboženská	náboženský	k2eAgNnPc4d1	náboženské
vidění	vidění	k1gNnPc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
proměnu	proměna	k1gFnSc4	proměna
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
náboženských	náboženský	k2eAgInPc6d1	náboženský
postojích	postoj	k1gInPc6	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
relativně	relativně	k6eAd1	relativně
světského	světský	k2eAgNnSc2d1	světské
zaměření	zaměření	k1gNnSc2	zaměření
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nositelem	nositel	k1gMnSc7	nositel
asketických	asketický	k2eAgFnPc2d1	asketická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
postavil	postavit	k5eAaPmAgInS	postavit
asketický	asketický	k2eAgInSc1d1	asketický
život	život	k1gInSc1	život
jako	jako	k8xS	jako
program	program	k1gInSc1	program
a	a	k8xC	a
vzor	vzor	k1gInSc1	vzor
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nedařilo	dařit	k5eNaImAgNnS	dařit
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
odmítal	odmítat	k5eAaImAgMnS	odmítat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
spor	spor	k1gInSc1	spor
bude	být	k5eAaImBp3nS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
Božím	boží	k2eAgInSc7d1	boží
zásahem	zásah	k1gInSc7	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Neochota	neochota	k1gFnSc1	neochota
řešit	řešit	k5eAaImF	řešit
problém	problém	k1gInSc4	problém
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
začala	začít	k5eAaPmAgFnS	začít
podporovat	podporovat	k5eAaImF	podporovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sledovala	sledovat	k5eAaImAgFnS	sledovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
mocenské	mocenský	k2eAgInPc4d1	mocenský
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
rozrůstal	rozrůstat	k5eAaImAgMnS	rozrůstat
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
začalo	začít	k5eAaPmAgNnS	začít
zastávat	zastávat	k5eAaImF	zastávat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
totiž	totiž	k9	totiž
podporoval	podporovat	k5eAaImAgMnS	podporovat
papeže	papež	k1gMnSc4	papež
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
vzdoropapeži	vzdoropapež	k1gMnSc3	vzdoropapež
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jeho	jeho	k3xOp3gInSc1	jeho
postoj	postoj	k1gInSc1	postoj
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
odrážel	odrážet	k5eAaImAgMnS	odrážet
na	na	k7c6	na
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
církev	církev	k1gFnSc1	církev
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vyhrotit	vyhrotit	k5eAaPmF	vyhrotit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
až	až	k9	až
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvojpapežství	dvojpapežství	k1gNnSc4	dvojpapežství
začal	začít	k5eAaPmAgMnS	začít
prohlašovat	prohlašovat	k5eAaImF	prohlašovat
za	za	k7c4	za
ďáblovo	ďáblův	k2eAgNnSc4d1	ďáblovo
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dále	daleko	k6eAd2	daleko
vyostřilo	vyostřit	k5eAaPmAgNnS	vyostřit
spor	spor	k1gInSc4	spor
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
nepřál	přát	k5eNaImAgMnS	přát
být	být	k5eAaImF	být
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
angažován	angažován	k2eAgInSc1d1	angažován
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
a	a	k8xC	a
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
začala	začít	k5eAaPmAgFnS	začít
vrcholit	vrcholit	k5eAaImF	vrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1384	[number]	k4	1384
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Čúchem	Čúch	k1gInSc7	Čúch
ze	z	k7c2	z
Zásady	zásada	k1gFnSc2	zásada
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
jez	jez	k1gInSc1	jez
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
arcibiskupskými	arcibiskupský	k2eAgInPc7d1	arcibiskupský
pozemky	pozemek	k1gInPc7	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
protestech	protest	k1gInPc6	protest
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jez	jez	k1gInSc1	jez
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Čúch	Čúch	k?	Čúch
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
podal	podat	k5eAaPmAgMnS	podat
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
král	král	k1gMnSc1	král
využil	využít	k5eAaPmAgMnS	využít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
několikadennímu	několikadenní	k2eAgNnSc3d1	několikadenní
zatčení	zatčení	k1gNnSc3	zatčení
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
Václav	Václav	k1gMnSc1	Václav
povolil	povolit	k5eAaPmAgMnS	povolit
Janu	Jan	k1gMnSc3	Jan
Čúchovi	Čúch	k1gMnSc3	Čúch
nahradit	nahradit	k5eAaPmF	nahradit
si	se	k3xPyFc3	se
škodu	škoda	k1gFnSc4	škoda
vypleněním	vyplenění	k1gNnSc7	vyplenění
arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
odvolal	odvolat	k5eAaPmAgMnS	odvolat
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
zemského	zemský	k2eAgMnSc2d1	zemský
kancléře	kancléř	k1gMnSc2	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
bylo	být	k5eAaImAgNnS	být
oslabit	oslabit	k5eAaPmF	oslabit
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
narušovat	narušovat	k5eAaImF	narušovat
legitimní	legitimní	k2eAgFnPc4d1	legitimní
pravomoce	pravomoc	k1gFnPc4	pravomoc
pražského	pražský	k2eAgNnSc2d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
další	další	k2eAgNnSc1d1	další
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgFnPc2	tento
akcí	akce	k1gFnPc2	akce
bylo	být	k5eAaImAgNnS	být
donutit	donutit	k5eAaPmF	donutit
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
spolupráce	spolupráce	k1gFnSc1	spolupráce
byla	být	k5eAaImAgFnS	být
nemožná	možný	k2eNgFnSc1d1	nemožná
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
velké	velký	k2eAgNnSc4d1	velké
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
sporem	spor	k1gInSc7	spor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
s	s	k7c7	s
litomyšlským	litomyšlský	k2eAgMnSc7d1	litomyšlský
biskupem	biskup	k1gMnSc7	biskup
Janem	Jan	k1gMnSc7	Jan
Soběslavem	Soběslav	k1gMnSc7	Soběslav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
docílil	docílit	k5eAaPmAgMnS	docílit
vzetí	vzetí	k1gNnSc4	vzetí
Jana	Jan	k1gMnSc2	Jan
Soběslava	Soběslav	k1gMnSc2	Soběslav
do	do	k7c2	do
klatby	klatba	k1gFnSc2	klatba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1392	[number]	k4	1392
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
církevní	církevní	k2eAgFnSc2d1	církevní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nutně	nutně	k6eAd1	nutně
musela	muset	k5eAaImAgFnS	muset
vést	vést	k5eAaImF	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
porážce	porážka	k1gFnSc3	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
byl	být	k5eAaImAgInS	být
umučen	umučit	k5eAaPmNgMnS	umučit
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc4	takový
násilí	násilí	k1gNnSc4	násilí
vůči	vůči	k7c3	vůči
představiteli	představitel	k1gMnSc3	představitel
církve	církev	k1gFnSc2	církev
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
normy	norma	k1gFnPc4	norma
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
podal	podat	k5eAaPmAgMnS	podat
papežské	papežský	k2eAgFnSc3d1	Papežská
kurii	kurie	k1gFnSc3	kurie
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
však	však	k9	však
převážila	převážit	k5eAaPmAgNnP	převážit
realistická	realistický	k2eAgNnPc1d1	realistické
politika	politikum	k1gNnPc1	politikum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
IX	IX	kA	IX
<g/>
.	.	kIx.	.
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
vzdoropapeži	vzdoropapež	k1gMnSc3	vzdoropapež
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
spor	spor	k1gInSc4	spor
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
nemá	mít	k5eNaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
nepodnikl	podniknout	k5eNaPmAgMnS	podniknout
žádné	žádný	k3yNgInPc4	žádný
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
stal	stát	k5eAaPmAgMnS	stát
zcela	zcela	k6eAd1	zcela
izolovaným	izolovaný	k2eAgMnSc7d1	izolovaný
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
podpory	podpora	k1gFnSc2	podpora
(	(	kIx(	(
<g/>
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ho	on	k3xPp3gMnSc4	on
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
podpořit	podpořit	k5eAaPmF	podpořit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
prohraných	prohraný	k2eAgInPc6d1	prohraný
sporech	spor	k1gInPc6	spor
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1396	[number]	k4	1396
nucen	nucen	k2eAgInSc1d1	nucen
abdikovat	abdikovat	k5eAaBmF	abdikovat
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
t.	t.	k?	t.
<g/>
r.	r.	kA	r.
pak	pak	k6eAd1	pak
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
svého	své	k1gNnSc2	své
synovce	synovec	k1gMnSc2	synovec
Olbrama	Olbram	k1gMnSc2	Olbram
ze	z	k7c2	z
Škvorce	Škvorka	k1gFnSc6	Škvorka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
abdikaci	abdikace	k1gFnSc6	abdikace
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1397	[number]	k4	1397
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
alexandrijským	alexandrijský	k2eAgMnSc7d1	alexandrijský
latinským	latinský	k2eAgMnSc7d1	latinský
patriarchou	patriarcha	k1gMnSc7	patriarcha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
prakticky	prakticky	k6eAd1	prakticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1398	[number]	k4	1398
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
nemožný	možný	k2eNgInSc1d1	nemožný
i	i	k8xC	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
byl	být	k5eAaImAgInS	být
zabaven	zabaven	k2eAgInSc1d1	zabaven
majetek	majetek	k1gInSc1	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1400	[number]	k4	1400
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
svaté	svatý	k2eAgMnPc4d1	svatý
Praxedy	Praxed	k1gMnPc4	Praxed
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jako	jako	k9	jako
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
zlomený	zlomený	k2eAgMnSc1d1	zlomený
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
legendy	legenda	k1gFnSc2	legenda
zemřel	zemřít	k5eAaPmAgMnS	zemřít
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
zápasil	zápasit	k5eAaImAgInS	zápasit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hodnocení	hodnocení	k1gNnSc1	hodnocení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
a	a	k8xC	a
evidentně	evidentně	k6eAd1	evidentně
bylo	být	k5eAaImAgNnS	být
problematické	problematický	k2eAgNnSc1d1	problematické
již	již	k6eAd1	již
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
mělo	mít	k5eAaImAgNnS	mít
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgInPc2	svůj
zájmů	zájem	k1gInPc2	zájem
shodných	shodný	k2eAgInPc2d1	shodný
se	s	k7c7	s
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
dokázal	dokázat	k5eAaPmAgMnS	dokázat
z	z	k7c2	z
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
udělat	udělat	k5eAaPmF	udělat
nástroj	nástroj	k1gInSc4	nástroj
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
politikou	politika	k1gFnSc7	politika
později	pozdě	k6eAd2	pozdě
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
ani	ani	k9	ani
sama	sám	k3xTgFnSc1	sám
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
chvíli	chvíle	k1gFnSc6	chvíle
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
majetku	majetek	k1gInSc2	majetek
zrušil	zrušit	k5eAaPmAgMnS	zrušit
tzv.	tzv.	kA	tzv.
odúmrtní	odúmrtní	k2eAgNnSc1d1	odúmrtní
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
pohanských	pohanský	k2eAgInPc2d1	pohanský
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
svátku	svátek	k1gInSc2	svátek
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1389	[number]	k4	1389
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
církve	církev	k1gFnSc2	církev
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
slaven	slavit	k5eAaImNgInS	slavit
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
kalendáře	kalendář	k1gInSc2	kalendář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gNnSc2	jeho
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
spadá	spadat	k5eAaImIp3nS	spadat
založení	založení	k1gNnSc4	založení
Betlémské	betlémský	k2eAgFnSc2d1	Betlémská
kaple	kaple	k1gFnSc2	kaple
a	a	k8xC	a
jako	jako	k8xS	jako
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgMnS	starat
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
chrámu	chrám	k1gInSc2	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
razantnímu	razantní	k2eAgNnSc3d1	razantní
potření	potření	k1gNnSc3	potření
nejen	nejen	k6eAd1	nejen
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
sekt	sekta	k1gFnPc2	sekta
a	a	k8xC	a
alchymistů	alchymista	k1gMnPc2	alchymista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
příznivců	příznivec	k1gMnPc2	příznivec
avignonského	avignonský	k2eAgMnSc2d1	avignonský
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedl	vést	k5eAaImAgInS	vést
s	s	k7c7	s
králem	král	k1gMnSc7	král
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
však	však	k9	však
vnitřně	vnitřně	k6eAd1	vnitřně
oslabovaly	oslabovat	k5eAaImAgFnP	oslabovat
nejen	nejen	k6eAd1	nejen
státní	státní	k2eAgFnPc1d1	státní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
církevní	církevní	k2eAgFnSc4d1	církevní
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
předjímaly	předjímat	k5eAaImAgInP	předjímat
husitství	husitství	k1gNnSc4	husitství
<g/>
,	,	kIx,	,
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
pomohly	pomoct	k5eAaPmAgInP	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
živnou	živný	k2eAgFnSc4d1	živná
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
šířily	šířit	k5eAaImAgInP	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
s	s	k7c7	s
králem	král	k1gMnSc7	král
nakonec	nakonec	k6eAd1	nakonec
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kladrubské	kladrubský	k2eAgNnSc1d1	Kladrubské
opatství	opatství	k1gNnSc1	opatství
nebylo	být	k5eNaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
skončil	skončit	k5eAaPmAgInS	skončit
umučením	umučení	k1gNnSc7	umučení
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pomuku	Pomuk	k1gInSc2	Pomuk
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
jeho	jeho	k3xOp3gFnPc4	jeho
tři	tři	k4xCgFnPc4	tři
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
pro	pro	k7c4	pro
něho	on	k3xPp3gNnSc2	on
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
osobně	osobně	k6eAd1	osobně
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
patrný	patrný	k2eAgInSc1d1	patrný
výrazný	výrazný	k2eAgInSc1d1	výrazný
názorový	názorový	k2eAgInSc1d1	názorový
posun	posun	k1gInSc1	posun
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
prošel	projít	k5eAaPmAgMnS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
rokem	rok	k1gInSc7	rok
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
církevní	církevní	k2eAgMnPc4d1	církevní
představitele	představitel	k1gMnPc4	představitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nebránili	bránit	k5eNaImAgMnP	bránit
světskému	světský	k2eAgInSc3d1	světský
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
účastnil	účastnit	k5eAaImAgMnS	účastnit
honů	hon	k1gInPc2	hon
<g/>
,	,	kIx,	,
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
antickou	antický	k2eAgFnSc4d1	antická
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
<g/>
,	,	kIx,	,
skládal	skládat	k5eAaImAgMnS	skládat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
dobrým	dobrý	k2eAgMnSc7d1	dobrý
tanečníkem	tanečník	k1gMnSc7	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
přežil	přežít	k5eAaPmAgMnS	přežít
svoji	svůj	k3xOyFgFnSc4	svůj
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
osobnost	osobnost	k1gFnSc1	osobnost
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velmi	velmi	k6eAd1	velmi
netolerantní	tolerantní	k2eNgMnSc1d1	netolerantní
asketa	asketa	k1gMnSc1	asketa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spal	spát	k5eAaImAgMnS	spát
na	na	k7c6	na
kamenné	kamenný	k2eAgFnSc6d1	kamenná
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
,	,	kIx,	,
nosil	nosit	k5eAaImAgMnS	nosit
řetězy	řetěz	k1gInPc4	řetěz
<g/>
,	,	kIx,	,
praktikoval	praktikovat	k5eAaImAgMnS	praktikovat
sebemrskačství	sebemrskačství	k1gNnSc4	sebemrskačství
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgMnSc7	svůj
zpovědníkem	zpovědník	k1gMnSc7	zpovědník
několikrát	několikrát	k6eAd1	několikrát
napomínán	napomínán	k2eAgMnSc1d1	napomínán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zpřetrhal	zpřetrhat	k5eAaPmAgMnS	zpřetrhat
i	i	k8xC	i
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
orientoval	orientovat	k5eAaBmAgInS	orientovat
se	se	k3xPyFc4	se
čistě	čistě	k6eAd1	čistě
na	na	k7c4	na
římského	římský	k2eAgMnSc4d1	římský
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
umělecké	umělecký	k2eAgNnSc1d1	umělecké
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
obsáhlé	obsáhlý	k2eAgInPc4d1	obsáhlý
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
náboženská	náboženský	k2eAgFnSc1d1	náboženská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Organizoval	organizovat	k5eAaBmAgMnS	organizovat
finančně	finančně	k6eAd1	finančně
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
přestavbu	přestavba	k1gFnSc4	přestavba
několika	několik	k4yIc2	několik
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
především	především	k9	především
Jenštejna	Jenštejn	k1gMnSc2	Jenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechával	nechávat	k5eAaImAgMnS	nechávat
malovat	malovat	k5eAaImF	malovat
svoje	svůj	k3xOyFgFnPc4	svůj
náboženské	náboženský	k2eAgFnPc4d1	náboženská
vize	vize	k1gFnPc4	vize
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
badatelů	badatel	k1gMnPc2	badatel
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
pozdějším	pozdní	k2eAgFnPc3d2	pozdější
vizím	vize	k1gFnPc3	vize
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
jeho	jeho	k3xOp3gFnPc4	jeho
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
duševním	duševní	k2eAgNnSc6d1	duševní
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc7d1	vlastní
tvorbou	tvorba	k1gFnSc7	tvorba
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hudební	hudební	k2eAgFnSc1d1	hudební
činnost	činnost	k1gFnSc1	činnost
nebyla	být	k5eNaImAgFnS	být
nijak	nijak	k6eAd1	nijak
systematická	systematický	k2eAgFnSc1d1	systematická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1380	[number]	k4	1380
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
o	o	k7c4	o
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hudební	hudební	k2eAgNnSc1d1	hudební
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Die	Die	k1gFnSc2	Die
Hymnen	Hymnen	k2eAgInSc1d1	Hymnen
Johanns	Johanns	k1gInSc1	Johanns
von	von	k1gInSc1	von
Jenstein	Jenstein	k2eAgInSc1d1	Jenstein
<g/>
,	,	kIx,	,
Erzbischofs	Erzbischofs	k1gInSc1	Erzbischofs
von	von	k1gInSc1	von
Prag	Prag	k1gInSc4	Prag
Q.	Q.	kA	Q.
M.	M.	kA	M.
Drevese	Drevese	k1gFnSc1	Drevese
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
literární	literární	k2eAgFnSc1d1	literární
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
náboženská	náboženský	k2eAgNnPc1d1	náboženské
a	a	k8xC	a
filosofická	filosofický	k2eAgNnPc1d1	filosofické
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
traktáty	traktát	k1gInPc1	traktát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Tractatulus	Tractatulus	k1gInSc1	Tractatulus
de	de	k?	de
potestate	potestat	k1gInSc5	potestat
clavium	clavium	k1gNnSc4	clavium
Traktat	Traktat	k1gMnSc4	Traktat
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jenšteina	Jenštein	k1gMnSc2	Jenštein
proti	proti	k7c3	proti
Vojtěchovi	Vojtěch	k1gMnSc3	Vojtěch
Rankovu	Rankův	k2eAgMnSc3d1	Rankův
o	o	k7c6	o
odúmrtech	odúmrt	k1gInPc6	odúmrt
Epistola	Epistola	k1gFnSc1	Epistola
ad	ad	k7c4	ad
Iohannem	Iohanno	k1gNnSc7	Iohanno
Noviforensem	Noviforens	k1gInSc7	Noviforens
missa	miss	k1gMnSc2	miss
Libellum	Libellum	k1gInSc1	Libellum
de	de	k?	de
fuga	fuga	k1gFnSc1	fuga
mundi	mund	k1gMnPc1	mund
De	De	k?	De
consideratione	consideration	k1gInSc5	consideration
Sermones	Sermonesa	k1gFnPc2	Sermonesa
<g/>
;	;	kIx,	;
Liber	libra	k1gFnPc2	libra
dialogorum	dialogorum	k1gInSc1	dialogorum
</s>
