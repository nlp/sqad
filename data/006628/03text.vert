<s>
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
s	s	k7c7	s
osamostatněním	osamostatnění	k1gNnSc7	osamostatnění
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
přejmenováním	přejmenování	k1gNnSc7	přejmenování
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
200	[number]	k4	200
poslanci	poslanec	k1gMnSc6	poslanec
volených	volená	k1gFnPc2	volená
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
poměrným	poměrný	k2eAgInSc7d1	poměrný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
naplněn	naplnit	k5eAaPmNgInS	naplnit
až	až	k9	až
volbami	volba	k1gFnPc7	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Čítá	čítat	k5eAaImIp3nS	čítat
81	[number]	k4	81
senátorů	senátor	k1gMnPc2	senátor
volených	volená	k1gFnPc2	volená
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	let	k1gInSc4	let
většinovým	většinový	k2eAgInSc7d1	většinový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
obmění	obměnit	k5eAaPmIp3nS	obměnit
třetina	třetina	k1gFnSc1	třetina
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
samostatným	samostatný	k2eAgInSc7d1	samostatný
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měly	mít	k5eAaImAgInP	mít
působnost	působnost	k1gFnSc4	působnost
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
sněmů	sněm	k1gInPc2	sněm
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
český	český	k2eAgInSc1d1	český
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgInSc1d1	moravský
a	a	k8xC	a
slezský	slezský	k2eAgInSc1d1	slezský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgInSc1d1	rakouský
Říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
československé	československý	k2eAgInPc1d1	československý
parlamenty	parlament	k1gInPc1	parlament
(	(	kIx(	(
<g/>
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc4d1	Česká
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zároveň	zároveň	k6eAd1	zároveň
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgNnSc4	svůj
poměrné	poměrný	k2eAgNnSc4d1	poměrné
zastoupení	zastoupení	k1gNnSc4	zastoupení
i	i	k9	i
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přechodného	přechodný	k2eAgNnSc2d1	přechodné
ustanovení	ustanovení	k1gNnSc2	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
106	[number]	k4	106
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
dnem	dnem	k7c2	dnem
účinnosti	účinnost	k1gFnSc2	účinnost
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
pro	pro	k7c4	pro
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
106	[number]	k4	106
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
ústavy	ústava	k1gFnSc2	ústava
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zvolení	zvolení	k1gNnSc1	zvolení
Senátu	senát	k1gInSc2	senát
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkce	funkce	k1gFnSc1	funkce
Senátu	senát	k1gInSc2	senát
Prozatímní	prozatímní	k2eAgInSc1d1	prozatímní
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ustaví	ustavit	k5eAaPmIp3nS	ustavit
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
nabytí	nabytí	k1gNnSc2	nabytí
účinnosti	účinnost	k1gFnSc2	účinnost
takového	takový	k3xDgInSc2	takový
zákona	zákon	k1gInSc2	zákon
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkce	funkce	k1gFnSc1	funkce
Senátu	senát	k1gInSc2	senát
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Prozatímní	prozatímní	k2eAgInSc1d1	prozatímní
Senát	senát	k1gInSc1	senát
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
zřízen	zřízen	k2eAgInSc1d1	zřízen
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
funkce	funkce	k1gFnPc4	funkce
senátu	senát	k1gInSc2	senát
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
volbami	volba	k1gFnPc7	volba
naplněn	naplněn	k2eAgInSc4d1	naplněn
řádný	řádný	k2eAgInSc4d1	řádný
senát	senát	k1gInSc4	senát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
bylo	být	k5eAaImAgNnS	být
voleno	volit	k5eAaImNgNnS	volit
všech	všecek	k3xTgMnPc2	všecek
81	[number]	k4	81
senátorů	senátor	k1gMnPc2	senátor
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
plné	plný	k2eAgNnSc4d1	plné
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc1d1	zbylý
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
měly	mít	k5eAaImAgFnP	mít
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
zkrácené	zkrácený	k2eAgNnSc4d1	zkrácené
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
nebo	nebo	k8xC	nebo
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
pravomoci	pravomoc	k1gFnPc4	pravomoc
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodování	rozhodování	k1gNnSc4	rozhodování
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
voleb	volba	k1gFnPc2	volba
jeho	jeho	k3xOp3gInPc2	jeho
členů	člen	k1gInPc2	člen
a	a	k8xC	a
podmínkách	podmínka	k1gFnPc6	podmínka
jejich	jejich	k3xOp3gInSc2	jejich
mandátu	mandát	k1gInSc2	mandát
stanoví	stanovit	k5eAaPmIp3nS	stanovit
ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přijatá	přijatý	k2eAgFnSc1d1	přijatá
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
15	[number]	k4	15
až	až	k9	až
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
podrobnosti	podrobnost	k1gFnPc4	podrobnost
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
247	[number]	k4	247
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
měnit	měnit	k5eAaImF	měnit
Ústavní	ústavní	k2eAgInSc4d1	ústavní
pořádek	pořádek	k1gInSc4	pořádek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vydává	vydávat	k5eAaImIp3nS	vydávat
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
důvěru	důvěra	k1gFnSc4	důvěra
či	či	k8xC	či
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
a	a	k8xC	a
projednává	projednávat	k5eAaImIp3nS	projednávat
státní	státní	k2eAgInSc4d1	státní
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
účet	účet	k1gInSc4	účet
roku	rok	k1gInSc2	rok
minulého	minulý	k2eAgInSc2d1	minulý
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
<g />
.	.	kIx.	.
</s>
<s>
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
věcí	věc	k1gFnPc2	věc
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
může	moct	k5eAaImIp3nS	moct
zřídit	zřídit	k5eAaPmF	zřídit
vyšetřovací	vyšetřovací	k2eAgFnSc4d1	vyšetřovací
komisi	komise	k1gFnSc4	komise
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
)	)	kIx)	)
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
případě	případ	k1gInSc6	případ
napadení	napadení	k1gNnSc2	napadení
nebo	nebo	k8xC	nebo
plnění	plnění	k1gNnSc2	plnění
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
závazků	závazek	k1gInPc2	závazek
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
účasti	účast	k1gFnSc6	účast
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
obranných	obranný	k2eAgInPc6d1	obranný
systémech	systém	k1gInPc6	systém
NATO	NATO	kA	NATO
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
armády	armáda	k1gFnSc2	armáda
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
státu	stát	k1gInSc2	stát
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
s	s	k7c7	s
pobytem	pobyt	k1gInSc7	pobyt
cizích	cizí	k2eAgNnPc2d1	cizí
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
nerozhoduje	rozhodovat	k5eNaImIp3nS	rozhodovat
<g/>
-li	i	k?	-li
zde	zde	k6eAd1	zde
vláda	vláda	k1gFnSc1	vláda
<g/>
)	)	kIx)	)
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
jmenováním	jmenování	k1gNnSc7	jmenování
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1992	[number]	k4	1992
přijala	přijmout	k5eAaPmAgFnS	přijmout
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
465	[number]	k4	465
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
sídle	sídlo	k1gNnSc6	sídlo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
účinný	účinný	k2eAgInSc4d1	účinný
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc4	sídlo
ČNR	ČNR	kA	ČNR
vymezil	vymezit	k5eAaPmAgInS	vymezit
výčtem	výčet	k1gInSc7	výčet
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
čp.	čp.	k?	čp.
176	[number]	k4	176
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
518	[number]	k4	518
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
§	§	k?	§
3	[number]	k4	3
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
celý	celý	k2eAgInSc4d1	celý
tento	tento	k3xDgInSc4	tento
komplex	komplex	k1gInSc4	komplex
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
způsob	způsob	k1gInSc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vláda	vláda	k1gFnSc1	vláda
učinila	učinit	k5eAaPmAgFnS	učinit
svým	svůj	k3xOyFgNnSc7	svůj
nařízením	nařízení	k1gNnSc7	nařízení
č.	č.	k?	č.
118	[number]	k4	118
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Nově	nově	k6eAd1	nově
pak	pak	k6eAd1	pak
stanovil	stanovit	k5eAaPmAgInS	stanovit
sídlo	sídlo	k1gNnSc4	sídlo
parlamentu	parlament	k1gInSc2	parlament
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tedy	tedy	k9	tedy
obou	dva	k4xCgFnPc2	dva
jeho	jeho	k3xOp3gFnPc2	jeho
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
Kanceláře	kancelář	k1gFnPc1	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Kanceláře	kancelář	k1gFnSc2	kancelář
Senátu	senát	k1gInSc2	senát
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
59	[number]	k4	59
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
sídle	sídlo	k1gNnSc6	sídlo
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
rovněž	rovněž	k9	rovněž
celý	celý	k2eAgInSc4d1	celý
komplex	komplex	k1gInSc4	komplex
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
zákon	zákon	k1gInSc4	zákon
rovněž	rovněž	k9	rovněž
ustavil	ustavit	k5eAaPmAgMnS	ustavit
Kancelář	kancelář	k1gFnSc4	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Kancelář	kancelář	k1gFnSc1	kancelář
Senátu	senát	k1gInSc2	senát
jako	jako	k8xC	jako
státní	státní	k2eAgFnSc2d1	státní
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
Valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
paláce	palác	k1gInSc2	palác
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
jízdárnou	jízdárna	k1gFnSc7	jízdárna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Kolovratským	Kolovratský	k2eAgInSc7d1	Kolovratský
palácem	palác	k1gInSc7	palác
a	a	k8xC	a
Malým	malý	k2eAgInSc7d1	malý
Fürstenberským	Fürstenberský	k2eAgInSc7d1	Fürstenberský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
blocích	blok	k1gInPc6	blok
domů	dům	k1gInPc2	dům
a	a	k8xC	a
paláců	palác	k1gInPc2	palác
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Malostranským	malostranský	k2eAgNnSc7d1	Malostranské
a	a	k8xC	a
Valdštejnským	valdštejnský	k2eAgNnSc7d1	Valdštejnské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
Sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
a	a	k8xC	a
Thunovské	Thunovský	k2eAgFnSc2d1	Thunovská
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zejména	zejména	k9	zejména
Thunovský	Thunovský	k2eAgInSc1d1	Thunovský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
176	[number]	k4	176
<g/>
)	)	kIx)	)
při	při	k7c6	při
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
Sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1779	[number]	k4	1779
<g/>
–	–	k?	–
<g/>
1794	[number]	k4	1794
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
jej	on	k3xPp3gInSc4	on
koupili	koupit	k5eAaPmAgMnP	koupit
čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgFnPc1d1	stavová
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
přebudovali	přebudovat	k5eAaPmAgMnP	přebudovat
na	na	k7c4	na
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
,	,	kIx,	,
kanceláře	kancelář	k1gFnPc4	kancelář
a	a	k8xC	a
archív	archív	k1gInSc4	archív
Zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
obnovený	obnovený	k2eAgInSc4d1	obnovený
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
vybudován	vybudován	k2eAgInSc1d1	vybudován
sál	sál	k1gInSc1	sál
přes	přes	k7c4	přes
dvě	dva	k4xCgNnPc4	dva
patra	patro	k1gNnPc4	patro
výšky	výška	k1gFnSc2	výška
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
jednací	jednací	k2eAgInSc1d1	jednací
sál	sál	k1gInSc1	sál
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
přijata	přijmout	k5eAaPmNgFnS	přijmout
její	její	k3xOp3gFnSc1	její
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bloku	blok	k1gInSc6	blok
sídlí	sídlet	k5eAaImIp3nS	sídlet
rovněž	rovněž	k9	rovněž
Kancelář	kancelář	k1gFnSc1	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
jednací	jednací	k2eAgInPc1d1	jednací
sály	sál	k1gInPc1	sál
sněmovních	sněmovní	k2eAgInPc2d1	sněmovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
sídlu	sídlo	k1gNnSc3	sídlo
sněmovny	sněmovna	k1gFnSc2	sněmovna
patří	patřit	k5eAaImIp3nS	patřit
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1	[number]	k4	1
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlo	sídlo	k1gNnSc1	sídlo
Českého	český	k2eAgNnSc2d1	české
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
a	a	k8xC	a
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
a	a	k8xC	a
blok	blok	k1gInSc1	blok
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
:	:	kIx,	:
čp.	čp.	k?	čp.
6	[number]	k4	6
(	(	kIx(	(
<g/>
Palác	palác	k1gInSc1	palác
Smiřických	smiřický	k2eAgFnPc2d1	Smiřická
neboli	neboli	k8xC	neboli
U	u	k7c2	u
Montágů	Montág	k1gInPc2	Montág
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
7	[number]	k4	7
(	(	kIx(	(
<g/>
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
518	[number]	k4	518
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Velikovský	Velikovský	k2eAgInSc1d1	Velikovský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Tomášské	tomášský	k2eAgFnSc2d1	Tomášská
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
čp.	čp.	k?	čp.
8	[number]	k4	8
(	(	kIx(	(
<g/>
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Thunovské	Thunovský	k2eAgFnSc2d1	Thunovská
a	a	k8xC	a
Tomášské	tomášský	k2eAgFnSc2d1	Tomášská
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smiřický	smiřický	k2eAgInSc1d1	smiřický
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
význačný	význačný	k2eAgMnSc1d1	význačný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1618	[number]	k4	1618
zde	zde	k6eAd1	zde
skupina	skupina	k1gFnSc1	skupina
protestantských	protestantský	k2eAgMnPc2d1	protestantský
šlechticů	šlechtic	k1gMnPc2	šlechtic
tajně	tajně	k6eAd1	tajně
smluvila	smluvit	k5eAaPmAgFnS	smluvit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
třetí	třetí	k4xOgFnSc4	třetí
pražskou	pražský	k2eAgFnSc4d1	Pražská
defenestraci	defenestrace	k1gFnSc4	defenestrace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
počátku	počátek	k1gInSc2	počátek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
patřil	patřit	k5eAaImAgInS	patřit
za	za	k7c2	za
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
k	k	k7c3	k
centrům	centrum	k1gNnPc3	centrum
českého	český	k2eAgInSc2d1	český
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
