<s>
Město	město	k1gNnSc1	město
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Jabłonków	Jabłonków	k1gFnSc1	Jabłonków
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Jablunkau	Jablunkaus	k1gInSc3	Jablunkaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýchodnější	východní	k2eAgNnSc1d3	nejvýchodnější
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Olše	olše	k1gFnSc2	olše
a	a	k8xC	a
Lomné	lomný	k2eAgFnPc4d1	Lomná
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
