<p>
<s>
Město	město	k1gNnSc1	město
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Jabłonków	Jabłonków	k1gFnSc1	Jabłonków
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Jablunkau	Jablunkaus	k1gInSc3	Jablunkaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýchodnější	východní	k2eAgNnSc1d3	nejvýchodnější
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Olše	olše	k1gFnSc2	olše
a	a	k8xC	a
Lomné	lomný	k2eAgFnPc4d1	Lomná
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
početná	početný	k2eAgFnSc1d1	početná
polská	polský	k2eAgFnSc1d1	polská
menšina	menšina	k1gFnSc1	menšina
(	(	kIx(	(
<g/>
1	[number]	k4	1
228	[number]	k4	228
podle	podle	k7c2	podle
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1435	[number]	k4	1435
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
jablum	jablum	k1gInSc1	jablum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
gabella	gabello	k1gNnSc2	gabello
a	a	k8xC	a
jež	jenž	k3xRgNnSc1	jenž
značí	značit	k5eAaImIp3nS	značit
"	"	kIx"	"
<g/>
poplatek	poplatek	k1gInSc1	poplatek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
osada	osada	k1gFnSc1	osada
či	či	k8xC	či
hradiště	hradiště	k1gNnSc1	hradiště
totiž	totiž	k9	totiž
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
stezce	stezka	k1gFnSc6	stezka
a	a	k8xC	a
od	od	k7c2	od
obchodníků	obchodník	k1gMnPc2	obchodník
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
vybírány	vybírán	k2eAgInPc1d1	vybírán
celní	celní	k2eAgInPc1d1	celní
poplatky	poplatek	k1gInPc1	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
získala	získat	k5eAaPmAgFnS	získat
městská	městský	k2eAgFnSc1d1	městská
práva	práv	k2eAgFnSc1d1	práva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
obce	obec	k1gFnSc2	obec
dva	dva	k4xCgInPc4	dva
mlýny	mlýn	k1gInPc4	mlýn
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
usídlili	usídlit	k5eAaPmAgMnP	usídlit
první	první	k4xOgMnPc1	první
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
modlitebnu	modlitebna	k1gFnSc4	modlitebna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
jablunkovští	jablunkovský	k2eAgMnPc1d1	jablunkovský
Židé	Žid	k1gMnPc1	Žid
založili	založit	k5eAaPmAgMnP	založit
spolek	spolek	k1gInSc4	spolek
Jablunkauer	Jablunkaura	k1gFnPc2	Jablunkaura
Israelitischen	Israelitischna	k1gFnPc2	Israelitischna
Bethausverein	Bethausverein	k1gInSc1	Bethausverein
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
vedli	vést	k5eAaImAgMnP	vést
i	i	k9	i
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
spolek	spolek	k1gInSc1	spolek
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
katolického	katolický	k2eAgInSc2d1	katolický
hřbitova	hřbitov	k1gInSc2	hřbitov
pozemek	pozemka	k1gFnPc2	pozemka
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
i	i	k9	i
s	s	k7c7	s
márnicí	márnice	k1gFnSc7	márnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
náhrobní	náhrobní	k2eAgInSc1d1	náhrobní
kámen	kámen	k1gInSc1	kámen
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
ovšem	ovšem	k9	ovšem
nese	nést	k5eAaImIp3nS	nést
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
a	a	k8xC	a
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
starší	starý	k2eAgInPc1d2	starší
kameny	kámen	k1gInPc1	kámen
bez	bez	k7c2	bez
datace	datace	k1gFnSc2	datace
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
až	až	k6eAd1	až
do	do	k7c2	do
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
hřbitovu	hřbitov	k1gInSc3	hřbitov
evidováno	evidovat	k5eAaImNgNnS	evidovat
26	[number]	k4	26
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
židovských	židovský	k2eAgInPc2d1	židovský
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
6	[number]	k4	6
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nestálo	stát	k5eNaImAgNnS	stát
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Folklór	folklór	k1gInSc4	folklór
==	==	k?	==
</s>
</p>
<p>
<s>
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
horské	horský	k2eAgFnSc2d1	horská
části	část	k1gFnSc2	část
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
s	s	k7c7	s
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
folklórem	folklór	k1gInSc7	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
Jablunkově	Jablunkův	k2eAgNnSc6d1	Jablunkův
koná	konat	k5eAaImIp3nS	konat
třídenní	třídenní	k2eAgFnSc1d1	třídenní
přehlídka	přehlídka	k1gFnSc1	přehlídka
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
Jablunkovska	Jablunkovsko	k1gNnSc2	Jablunkovsko
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Gorolski	Gorolsk	k1gFnSc2	Gorolsk
święto	święto	k1gNnSc1	święto
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
na	na	k7c6	na
hlídaní	hlídaný	k2eAgMnPc1d1	hlídaný
ovcí	ovce	k1gFnPc2	ovce
chován	chován	k2eAgInSc1d1	chován
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vymřelý	vymřelý	k2eAgMnSc1d1	vymřelý
ovčácký	ovčácký	k2eAgMnSc1d1	ovčácký
pes	pes	k1gMnSc1	pes
beskydský	beskydský	k2eAgMnSc1d1	beskydský
bundáš	bundat	k5eAaBmIp2nS	bundat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
vojenská	vojenský	k2eAgNnPc4d1	vojenské
pietní	pietní	k2eAgNnPc4d1	pietní
místa	místo	k1gNnPc4	místo
Zde	zde	k6eAd1	zde
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Augustin	Augustin	k1gMnSc1	Augustin
Handzel	Handzel	k1gMnSc1	Handzel
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Tomalík	Tomalík	k1gMnSc1	Tomalík
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Göbel	Göbel	k1gMnSc1	Göbel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
CČsH	CČsH	k1gMnSc1	CČsH
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Drahoš	Drahoš	k1gMnSc1	Drahoš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Karol	Karol	k1gInSc1	Karol
Piegza	Piegza	k1gFnSc1	Piegza
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
</s>
</p>
<p>
<s>
==	==	k?	==
Muzea	muzeum	k1gNnSc2	muzeum
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Jablunkově	Jablunkův	k2eAgFnSc6d1	Jablunkova
-	-	kIx~	-
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
náměstí	náměstí	k1gNnSc1	náměstí
14	[number]	k4	14
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Siemianowice	Siemianowice	k1gFnSc1	Siemianowice
Śląskie	Śląskie	k1gFnSc1	Śląskie
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Gogolin	Gogolin	k1gInSc1	Gogolin
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Kysucké	Kysucký	k2eAgNnSc1d1	Kysucký
Nové	Nové	k2eAgNnSc1d1	Nové
Mesto	Mesto	k1gNnSc1	Mesto
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Tjačiv	Tjačit	k5eAaPmDgInS	Tjačit
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jablunkovské	jablunkovský	k2eAgInPc1d1	jablunkovský
tunely	tunel	k1gInPc1	tunel
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Jablunkově	Jablunkův	k2eAgFnSc6d1	Jablunkova
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panoramatická	panoramatický	k2eAgFnSc1d1	panoramatická
mapa	mapa	k1gFnSc1	mapa
Jablunkova	Jablunkův	k2eAgNnSc2d1	Jablunkův
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
</s>
</p>
<p>
<s>
Slezsko	Slezsko	k1gNnSc1	Slezsko
-	-	kIx~	-
Beskydský	beskydský	k2eAgInSc1d1	beskydský
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
-	-	kIx~	-
základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
</s>
</p>
<p>
<s>
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Muzea	muzeum	k1gNnSc2	muzeum
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
v	v	k7c6	v
Jablunkově	Jablunkův	k2eAgFnSc6d1	Jablunkova
</s>
</p>
