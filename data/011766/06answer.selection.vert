<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vynález	vynález	k1gInSc4	vynález
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
obvykle	obvykle	k6eAd1	obvykle
připisován	připisovat	k5eAaImNgInS	připisovat
skotskému	skotský	k1gInSc3	skotský
vynálezci	vynálezce	k1gMnPc1	vynálezce
Jamesi	Jamese	k1gFnSc4	Jamese
Wattovi	Watt	k1gMnSc3	Watt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
</s>
