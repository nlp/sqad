<p>
<s>
Ampér	ampér	k1gInSc1	ampér
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
A	a	k9	a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
pro	pro	k7c4	pro
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
fixací	fixace	k1gFnSc7	fixace
číselné	číselný	k2eAgFnSc2d1	číselná
hodnoty	hodnota	k1gFnSc2	hodnota
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
rovna	roven	k2eAgFnSc1d1	rovna
1,602	[number]	k4	1,602
176	[number]	k4	176
634	[number]	k4	634
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
jednotkou	jednotka	k1gFnSc7	jednotka
C	C	kA	C
<g/>
,	,	kIx,	,
rovnou	rovnou	k6eAd1	rovnou
A	a	k9	a
s	s	k7c7	s
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
pomocí	pomocí	k7c2	pomocí
Δ	Δ	k?	Δ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Definice	definice	k1gFnSc1	definice
ampéru	ampér	k1gInSc2	ampér
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ampér	ampér	k1gInSc1	ampér
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
SI	si	k1gNnSc6	si
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
fixací	fixace	k1gFnSc7	fixace
číselné	číselný	k2eAgFnSc2d1	číselná
hodnoty	hodnota	k1gFnSc2	hodnota
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
rovna	roven	k2eAgFnSc1d1	rovna
1,602	[number]	k4	1,602
176	[number]	k4	176
634	[number]	k4	634
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
jednotkou	jednotka	k1gFnSc7	jednotka
C	C	kA	C
<g/>
,	,	kIx,	,
rovnou	rovnou	k6eAd1	rovnou
A	a	k9	a
s	s	k7c7	s
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
pomocí	pomocí	k7c2	pomocí
Δ	Δ	k?	Δ
<g/>
.	.	kIx.	.
<g/>
Zafixování	zafixování	k1gNnSc1	zafixování
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
přesně	přesně	k6eAd1	přesně
také	také	k9	také
hodnotu	hodnota	k1gFnSc4	hodnota
Josephsonovy	Josephsonův	k2eAgFnSc2d1	Josephsonův
konstanty	konstanta	k1gFnSc2	konstanta
a	a	k8xC	a
von	von	k1gInSc1	von
Klitzingovy	Klitzingův	k2eAgFnSc2d1	Klitzingův
konstanty	konstanta	k1gFnSc2	konstanta
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
pro	pro	k7c4	pro
Josephsonův	Josephsonův	k2eAgInSc4d1	Josephsonův
jev	jev	k1gInSc4	jev
resp.	resp.	kA	resp.
kvantový	kvantový	k2eAgInSc1d1	kvantový
Hallův	Hallův	k2eAgInSc1d1	Hallův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
etalony	etalon	k1gInPc4	etalon
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
U	U	kA	U
a	a	k8xC	a
elektrického	elektrický	k2eAgInSc2d1	elektrický
odporu	odpor	k1gInSc2	odpor
R.	R.	kA	R.
Realizace	realizace	k1gFnSc1	realizace
ampéru	ampér	k1gInSc2	ampér
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
aplikaci	aplikace	k1gFnSc4	aplikace
Ohmova	Ohmův	k2eAgInSc2d1	Ohmův
zákona	zákon	k1gInSc2	zákon
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stará	starý	k2eAgFnSc1d1	stará
definice	definice	k1gFnSc1	definice
(	(	kIx(	(
<g/>
platná	platný	k2eAgFnSc1d1	platná
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
byl	být	k5eAaImAgInS	být
ampér	ampér	k1gInSc1	ampér
definován	definovat	k5eAaBmNgInS	definovat
podle	podle	k7c2	podle
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ampér	ampér	k1gInSc1	ampér
je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
dvěma	dva	k4xCgInPc7	dva
přímými	přímý	k2eAgInPc7d1	přímý
rovnoběžnými	rovnoběžný	k2eAgInPc7d1	rovnoběžný
nekonečně	konečně	k6eNd1	konečně
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vodiči	vodič	k1gInPc7	vodič
zanedbatelného	zanedbatelný	k2eAgInSc2d1	zanedbatelný
kruhového	kruhový	k2eAgInSc2d1	kruhový
průřezu	průřez	k1gInSc2	průřez
umístěnými	umístěný	k2eAgInPc7d1	umístěný
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
1	[number]	k4	1
metr	metr	k1gInSc1	metr
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
stálou	stálý	k2eAgFnSc4d1	stálá
sílu	síla	k1gFnSc4	síla
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
7	[number]	k4	7
newtonu	newton	k1gInSc2	newton
na	na	k7c4	na
1	[number]	k4	1
metr	metr	k1gInSc4	metr
délky	délka	k1gFnSc2	délka
vodiče	vodič	k1gInSc2	vodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
matematikovi	matematik	k1gMnSc3	matematik
a	a	k8xC	a
fyzikovi	fyzik	k1gMnSc3	fyzik
Ampè	Ampè	k1gMnSc3	Ampè
(	(	kIx(	(
<g/>
1775	[number]	k4	1775
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ampér	ampér	k1gInSc1	ampér
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
desetina	desetina	k1gFnSc1	desetina
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
jednotky	jednotka	k1gFnSc2	jednotka
proudu	proud	k1gInSc2	proud
ze	z	k7c2	z
soustavy	soustava	k1gFnSc2	soustava
CGS	CGS	kA	CGS
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
abampér	abampér	k1gInSc1	abampér
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgInSc1d1	absolutní
ampér	ampér	k1gInSc1	ampér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
generuje	generovat	k5eAaImIp3nS	generovat
sílu	síla	k1gFnSc4	síla
dvou	dva	k4xCgInPc2	dva
dynů	dyn	k1gInPc2	dyn
(	(	kIx(	(
<g/>
1	[number]	k4	1
dyn	dyna	k1gFnPc2	dyna
=	=	kIx~	=
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
N	N	kA	N
≈	≈	k?	≈
1,019	[number]	k4	1,019
<g/>
716	[number]	k4	716
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
kp	kp	k?	kp
<g/>
)	)	kIx)	)
na	na	k7c4	na
centimetr	centimetr	k1gInSc4	centimetr
délky	délka	k1gFnSc2	délka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
vodiči	vodič	k1gInPc7	vodič
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
jeden	jeden	k4xCgInSc4	jeden
centimetr	centimetr	k1gInSc4	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
coulomb	coulomb	k1gInSc1	coulomb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
pomocí	pomocí	k7c2	pomocí
ampéru	ampér	k1gInSc2	ampér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proud	proud	k1gInSc1	proud
I	i	k9	i
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
jeden	jeden	k4xCgInSc1	jeden
ampér	ampér	k1gInSc1	ampér
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgInSc1d1	rovný
toku	tok	k1gInSc3	tok
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
Q	Q	kA	Q
jeden	jeden	k4xCgInSc4	jeden
coulomb	coulomb	k1gInSc4	coulomb
za	za	k7c4	za
čas	čas	k1gInSc4	čas
t	t	k?	t
1	[number]	k4	1
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Etalon	etalon	k1gInSc4	etalon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
metrické	metrický	k2eAgFnSc6d1	metrická
soustavě	soustava	k1gFnSc6	soustava
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
etalon	etalon	k1gInSc1	etalon
určen	určen	k2eAgInSc1d1	určen
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
Ampér	ampér	k1gInSc1	ampér
jako	jako	k8xS	jako
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
0,001	[number]	k4	0,001
118	[number]	k4	118
g	g	kA	g
stříbra	stříbro	k1gNnSc2	stříbro
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
konferencí	konference	k1gFnSc7	konference
pro	pro	k7c4	pro
jednotky	jednotka	k1gFnPc4	jednotka
elektrické	elektrický	k2eAgFnPc4d1	elektrická
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
a	a	k8xC	a
schválena	schválit	k5eAaPmNgFnS	schválit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zákonem	zákon	k1gInSc7	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
André-Marie	André-Marie	k1gFnSc1	André-Marie
Ampè	Ampè	k1gFnSc2	Ampè
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
</s>
</p>
<p>
<s>
Ohmův	Ohmův	k2eAgInSc1d1	Ohmův
zákon	zákon	k1gInSc1	zákon
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ampér	ampér	k1gInSc1	ampér
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
znění	znění	k1gNnSc1	znění
definice	definice	k1gFnSc2	definice
jednotky	jednotka	k1gFnSc2	jednotka
–	–	k?	–
BIMP	BIMP	kA	BIMP
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
