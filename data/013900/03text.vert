<s>
V	v	k7c6
zajetí	zajetí	k1gNnSc6
Vikingů	Viking	k1gMnPc2
</s>
<s>
Vikingská	vikingský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
V	v	k7c4
zajetí	zajetí	k1gNnSc4
Vikingů	Viking	k1gMnPc2
je	být	k5eAaImIp3nS
název	název	k1gInSc1
společného	společný	k2eAgNnSc2d1
českého	český	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
dvou	dva	k4xCgInPc2
dobrodružných	dobrodružný	k2eAgInPc2d1
a	a	k8xC
na	na	k7c4
sebe	sebe	k3xPyFc4
navazujících	navazující	k2eAgInPc2d1
románů	román	k1gInPc2
norské	norský	k2eAgFnSc2d1
spisovatelky	spisovatelka	k1gFnSc2
Torill	Torill	k1gFnSc1
Thorstad	Thorstad	k1gFnSc1
Haugerové	Haugerová	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgInPc4
dva	dva	k4xCgInPc4
díly	díl	k1gInPc4
autorčiny	autorčin	k2eAgFnSc2d1
šestidílné	šestidílný	k2eAgFnSc2d1
série	série	k1gFnSc2
z	z	k7c2
doby	doba	k1gFnSc2
Vikingů	Viking	k1gMnPc2
z	z	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
o	o	k7c4
romány	román	k1gInPc4
Rø	Rø	k1gMnSc1
av	av	k?
vikinger	vikinger	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
,	,	kIx,
Uneseni	unesen	k2eAgMnPc1d1
Vikingy	Viking	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
Flukten	Flukten	k2eAgInSc1d1
fra	fra	k?
vikingene	vikingen	k1gInSc5
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
,	,	kIx,
Útěk	útěk	k1gInSc1
před	před	k7c7
Vikingy	Viking	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kniha	kniha	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1988	#num#	k4
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
nakladatelství	nakladatelství	k1gNnSc6
Albatros	albatros	k1gMnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
Hany	Hana	k1gFnSc2
Ševčíkové	Ševčíková	k1gFnSc2
jako	jako	k9
180	#num#	k4
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
edice	edice	k1gFnSc2
Knihy	kniha	k1gFnSc2
odvahy	odvaha	k1gFnSc2
a	a	k8xC
dobrodružství	dobrodružství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
knize	kniha	k1gFnSc6
se	se	k3xPyFc4
první	první	k4xOgInSc1
díl	díl	k1gInSc1
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
Únos	únos	k1gInSc4
a	a	k8xC
druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
Útěk	útěk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
knihy	kniha	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Únos	únos	k1gInSc1
</s>
<s>
Jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1
Patrik	Patrik	k1gMnSc1
a	a	k8xC
desetiletá	desetiletý	k2eAgFnSc1d1
Sunnita	sunnita	k1gMnSc1
jsou	být	k5eAaImIp3nP
sourozenci	sourozenec	k1gMnSc3
a	a	k8xC
žijí	žít	k5eAaImIp3nP
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
v	v	k7c6
Irsku	Irsko	k1gNnSc6
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prožívají	prožívat	k5eAaImIp3nP
bezstarostné	bezstarostný	k2eAgNnSc4d1
dětství	dětství	k1gNnSc4
a	a	k8xC
jen	jen	k9
z	z	k7c2
doslechu	doslech	k1gInSc2
se	se	k3xPyFc4
dozvídají	dozvídat	k5eAaImIp3nP
o	o	k7c6
řádění	řádění	k1gNnSc6
loupeživých	loupeživý	k2eAgMnPc2d1
a	a	k8xC
krutých	krutý	k2eAgMnPc2d1
Vikingů	Viking	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
však	však	k9
Vikingové	Viking	k1gMnPc1
vesnici	vesnice	k1gFnSc4
přepadnou	přepadnout	k5eAaPmIp3nP
a	a	k8xC
vypálí	vypálit	k5eAaPmIp3nP
a	a	k8xC
sourozence	sourozenec	k1gMnSc4
unesou	unést	k5eAaPmIp3nP
do	do	k7c2
Norska	Norsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
stanou	stanout	k5eAaPmIp3nP
otroky	otrok	k1gMnPc7
mocného	mocný	k2eAgNnSc2d1
jarla	jarl	k1gMnSc2
Hå	Hå	k1gFnSc1
a	a	k8xC
dostanou	dostat	k5eAaPmIp3nP
nové	nový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Reim	Reima	k1gFnPc2
a	a	k8xC
Tira	Tirum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
v	v	k7c4
otroctví	otroctví	k1gNnSc4
jim	on	k3xPp3gMnPc3
přináší	přinášet	k5eAaImIp3nS
drsné	drsný	k2eAgInPc4d1
zážitky	zážitek	k1gInPc4
a	a	k8xC
krutou	krutý	k2eAgFnSc4d1
dřinu	dřina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
nové	nový	k2eAgMnPc4d1
přátele	přítel	k1gMnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
jarlova	jarlův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Sigurda	Sigurd	k1gMnSc2
<g/>
,	,	kIx,
pojmenovaného	pojmenovaný	k2eAgMnSc2d1
podle	podle	k7c2
hrdiny	hrdina	k1gMnSc2
staré	starý	k2eAgFnSc2d1
severské	severský	k2eAgFnSc2d1
ságy	sága	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
sourozenci	sourozenec	k1gMnSc3
však	však	k9
neustále	neustále	k6eAd1
doufají	doufat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednou	jednou	k6eAd1
dostanou	dostat	k5eAaPmIp3nP
zpět	zpět	k6eAd1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
jarl	jarl	k1gMnSc1
Hå	Hå	k1gMnSc1
na	na	k7c6
jednom	jeden	k4xCgInSc6
vikingském	vikingský	k2eAgInSc6d1
nájezdu	nájezd	k1gInSc6
zahyne	zahynout	k5eAaPmIp3nS
<g/>
,	,	kIx,
přepadnou	přepadnout	k5eAaPmIp3nP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
dvorec	dvorec	k1gInSc4
ziskuchtiví	ziskuchtivý	k2eAgMnPc1d1
sousedé	soused	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalém	nastalý	k2eAgInSc6d1
zmatku	zmatek	k1gInSc6
se	se	k3xPyFc4
sourozencům	sourozenec	k1gMnPc3
podaří	podařit	k5eAaPmIp3nS
uprchnout	uprchnout	k5eAaPmF
a	a	k8xC
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
sedlákovi	sedlák	k1gMnSc3
Bredemu	Bredem	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
ženě	žena	k1gFnSc3
Bergliotě	Bergliota	k1gFnSc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
s	s	k7c7
otroky	otrok	k1gMnPc7
zacházejí	zacházet	k5eAaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
s	s	k7c7
lidmi	člověk	k1gMnPc7
svého	svůj	k3xOyFgInSc2
vlastního	vlastní	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Útěk	útěk	k1gInSc1
</s>
<s>
Sedlák	Sedlák	k1gMnSc1
Brede	Bred	k1gInSc5
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
opustit	opustit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
chudou	chudý	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
v	v	k7c6
Norsku	Norsko	k1gNnSc6
a	a	k8xC
vydat	vydat	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
Island	Island	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
tam	tam	k6eAd1
vybudoval	vybudovat	k5eAaPmAgMnS
nové	nový	k2eAgNnSc4d1
hospodářství	hospodářství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
loď	loď	k1gFnSc4
sebou	se	k3xPyFc7
vezme	vzít	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
ovce	ovce	k1gFnPc4
a	a	k8xC
kozy	koza	k1gFnPc4
<g/>
,	,	kIx,
otroky	otrok	k1gMnPc4
včetně	včetně	k7c2
Reima	Reimum	k1gNnSc2
a	a	k8xC
Tiry	Tira	k1gFnSc2
a	a	k8xC
také	také	k9
všechny	všechen	k3xTgMnPc4
členy	člen	k1gMnPc4
své	svůj	k3xOyFgFnSc2
domácnosti	domácnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
vyplují	vyplout	k5eAaPmIp3nP
ještě	ještě	k9
dvě	dva	k4xCgFnPc1
další	další	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
Islandu	Island	k1gInSc6
přistane	přistat	k5eAaPmIp3nS
jen	jen	k9
Bredova	Bredův	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
byly	být	k5eAaImAgFnP
zřejmě	zřejmě	k6eAd1
zničeny	zničit	k5eAaPmNgFnP
během	během	k7c2
bouře	bouř	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
cestě	cesta	k1gFnSc6
potkala	potkat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Brede	Brede	k6eAd1
si	se	k3xPyFc3
zabere	zabrat	k5eAaPmIp3nS
na	na	k7c6
Islandu	Island	k1gInSc6
volnou	volný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
a	a	k8xC
vystaví	vystavit	k5eAaPmIp3nS
si	se	k3xPyFc3
tam	tam	k6eAd1
dvorec	dvorec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syn	syn	k1gMnSc1
bohatého	bohatý	k2eAgMnSc2d1
souseda	soused	k1gMnSc2
Grim	Grim	k1gMnSc1
ukradne	ukradnout	k5eAaPmIp3nS
Reimovi	Reim	k1gMnSc3
jeho	jeho	k3xOp3gFnPc2
luk	louka	k1gFnPc2
a	a	k8xC
zabije	zabít	k5eAaPmIp3nS
s	s	k7c7
ním	on	k3xPp3gMnSc7
Hå	Hå	k2eAgMnSc4d1
příbuzného	příbuzný	k2eAgMnSc4d1
Narveho	Narve	k1gMnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
sporu	spor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vraždy	vražda	k1gFnSc2
je	být	k5eAaImIp3nS
obviněn	obvinit	k5eAaPmNgMnS
Reim	Reim	k1gMnSc1
<g/>
,	,	kIx,
pravda	pravda	k1gFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
ukáže	ukázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
Reima	Reima	k1gFnSc1
s	s	k7c7
Tirou	Tira	k1gFnSc7
chtějí	chtít	k5eAaImIp3nP
poslat	poslat	k5eAaPmF
jako	jako	k9
uprchlé	uprchlý	k2eAgMnPc4d1
otroky	otrok	k1gMnPc4
zpět	zpět	k6eAd1
do	do	k7c2
Norska	Norsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
přátelům	přítel	k1gMnPc3
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podaří	podařit	k5eAaPmIp3nS
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
obchodní	obchodní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
plující	plující	k2eAgFnSc4d1
do	do	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
vrátit	vrátit	k5eAaPmF
se	se	k3xPyFc4
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.kodovky.cz/kniha/180	http://www.kodovky.cz/kniha/180	k4
</s>
