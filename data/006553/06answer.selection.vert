<s>
Arthur	Arthur	k1gMnSc1	Arthur
Kornberg	Kornberg	k1gMnSc1	Kornberg
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
bakterie	bakterie	k1gFnSc2	bakterie
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
první	první	k4xOgFnSc1	první
DNA	dna	k1gFnSc1	dna
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
DNA	dno	k1gNnSc2	dno
polymeráza	polymeráza	k1gFnSc1	polymeráza
I.	I.	kA	I.
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
replikaci	replikace	k1gFnSc6	replikace
spíše	spíše	k9	spíše
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
<g/>
,	,	kIx,	,
i	i	k9	i
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
důležitým	důležitý	k2eAgInSc7d1	důležitý
milníkem	milník	k1gInSc7	milník
<g/>
.	.	kIx.	.
</s>
