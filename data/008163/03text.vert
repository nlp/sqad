<p>
<s>
Olivier	Olivier	k1gMnSc1	Olivier
Messiaen	Messiana	k1gFnPc2	Messiana
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1908	[number]	k4	1908
Avignon	Avignon	k1gInSc1	Avignon
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
Clichy	Clicha	k1gFnSc2	Clicha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
učiteli	učitel	k1gMnPc7	učitel
Marcel	Marcel	k1gMnSc1	Marcel
Dupré	Duprý	k2eAgInPc4d1	Duprý
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Dukas	Dukas	k1gMnSc1	Dukas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
varhaník	varhaník	k1gMnSc1	varhaník
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
kostele	kostel	k1gInSc6	kostel
Svaté	svatý	k2eAgFnSc2d1	svatá
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
profesorem	profesor	k1gMnSc7	profesor
harmonie	harmonie	k1gFnSc2	harmonie
a	a	k8xC	a
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
Yvonne	Yvonn	k1gInSc5	Yvonn
Loriodovou	Loriodový	k2eAgFnSc7d1	Loriodový
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
klavírní	klavírní	k2eAgFnSc7d1	klavírní
interpretkou	interpretka	k1gFnSc7	interpretka
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
působil	působit	k5eAaImAgMnS	působit
skladatel	skladatel	k1gMnSc1	skladatel
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
žáky	žák	k1gMnPc7	žák
patří	patřit	k5eAaImIp3nP	patřit
Pierre	Pierr	k1gInSc5	Pierr
Boulez	Boulez	k1gMnSc1	Boulez
<g/>
,	,	kIx,	,
Karlheinz	Karlheinz	k1gMnSc1	Karlheinz
Stockhausen	Stockhausen	k1gInSc1	Stockhausen
a	a	k8xC	a
Iannis	Iannis	k1gFnSc1	Iannis
Xenakis	Xenakis	k1gFnSc1	Xenakis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jej	on	k3xPp3gInSc4	on
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
dvanáctitónová	dvanáctitónový	k2eAgFnSc1d1	dvanáctitónová
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
průkopníkem	průkopník	k1gMnSc7	průkopník
serialismu	serialismus	k1gInSc2	serialismus
<g/>
.	.	kIx.	.
</s>
<s>
Inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
se	se	k3xPyFc4	se
matematickými	matematický	k2eAgFnPc7d1	matematická
strukturami	struktura	k1gFnPc7	struktura
a	a	k8xC	a
zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
hudby	hudba	k1gFnSc2	hudba
středověké	středověký	k2eAgFnSc2d1	středověká
také	také	k9	také
o	o	k7c4	o
indickou	indický	k2eAgFnSc4d1	indická
rytmiku	rytmika	k1gFnSc4	rytmika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
nezvykle	zvykle	k6eNd1	zvykle
pestrý	pestrý	k2eAgInSc1d1	pestrý
<g/>
,	,	kIx,	,
od	od	k7c2	od
tonální	tonální	k2eAgFnSc2d1	tonální
a	a	k8xC	a
harmonické	harmonický	k2eAgFnSc2d1	harmonická
hudby	hudba	k1gFnSc2	hudba
až	až	k9	až
po	po	k7c4	po
vyhraněně	vyhraněně	k6eAd1	vyhraněně
rytmický	rytmický	k2eAgInSc4d1	rytmický
styl	styl	k1gInSc4	styl
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
skladbách	skladba	k1gFnPc6	skladba
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
prvkem	prvek	k1gInSc7	prvek
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
palindromické	palindromický	k2eAgInPc4d1	palindromický
rytmy	rytmus	k1gInPc4	rytmus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
melodický	melodický	k2eAgInSc4d1	melodický
materiál	materiál	k1gInSc4	materiál
bývají	bývat	k5eAaImIp3nP	bývat
použité	použitý	k2eAgFnPc1d1	použitá
módy	móda	k1gFnPc1	móda
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
transponovatelností	transponovatelnost	k1gFnSc7	transponovatelnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Messiaen	Messiaen	k1gInSc4	Messiaen
sám	sám	k3xTgInSc4	sám
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
inspirátorem	inspirátor	k1gMnSc7	inspirátor
poválečné	poválečný	k2eAgFnSc2d1	poválečná
hudební	hudební	k2eAgFnSc2d1	hudební
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
avantgardu	avantgarda	k1gFnSc4	avantgarda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
tematice	tematika	k1gFnSc3	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Vybírá	vybírat	k5eAaImIp3nS	vybírat
si	se	k3xPyFc3	se
témata	téma	k1gNnPc1	téma
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
věrouky	věrouka	k1gFnSc2	věrouka
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
velká	velký	k2eAgNnPc4d1	velké
eschatologická	eschatologický	k2eAgNnPc4d1	eschatologické
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
vysloveného	vyslovený	k2eAgNnSc2d1	vyslovené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
za	za	k7c4	za
"	"	kIx"	"
<g/>
akt	akt	k1gInSc4	akt
víry	víra	k1gFnSc2	víra
oslavující	oslavující	k2eAgNnSc1d1	oslavující
mystérium	mystérium	k1gNnSc1	mystérium
Kristovo	Kristův	k2eAgNnSc1d1	Kristovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
země	zem	k1gFnSc2	zem
pro	pro	k7c4	pro
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
představuje	představovat	k5eAaImIp3nS	představovat
mystickou	mystický	k2eAgFnSc4d1	mystická
svatbu	svatba	k1gFnSc4	svatba
nebes	nebesa	k1gNnPc2	nebesa
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oslavené	oslavený	k2eAgNnSc1d1	oslavené
tělo	tělo	k1gNnSc1	tělo
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvacet	dvacet	k4xCc4	dvacet
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
Ježíše	Ježíš	k1gMnSc2	Ježíš
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
piano	piano	k1gNnSc4	piano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvartet	kvartet	k1gInSc1	kvartet
pro	pro	k7c4	pro
konec	konec	k1gInSc4	konec
času	čas	k1gInSc2	čas
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
vzniká	vznikat	k5eAaImIp3nS	vznikat
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
válečného	válečný	k2eAgNnSc2d1	válečné
nasazení	nasazení	k1gNnSc2	nasazení
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
zajetí	zajetí	k1gNnSc2	zajetí
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
malé	malý	k2eAgFnPc1d1	malá
liturgie	liturgie	k1gFnPc1	liturgie
božské	božský	k2eAgFnSc2d1	božská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
pro	pro	k7c4	pro
ženský	ženský	k2eAgInSc4d1	ženský
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barvy	barva	k1gFnPc1	barva
nebeského	nebeský	k2eAgNnSc2d1	nebeské
města	město	k1gNnSc2	město
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
xylofon	xylofon	k1gInSc4	xylofon
<g/>
,	,	kIx,	,
marimbu	marimba	k1gFnSc4	marimba
a	a	k8xC	a
3	[number]	k4	3
skupiny	skupina	k1gFnSc2	skupina
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proměnění	proměnění	k1gNnSc1	proměnění
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
pro	pro	k7c4	pro
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
napsal	napsat	k5eAaPmAgMnS	napsat
operu	opera	k1gFnSc4	opera
Svatý	svatý	k2eAgMnSc1d1	svatý
František	František	k1gMnSc1	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc3	Assis
</s>
</p>
<p>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
elektronický	elektronický	k2eAgInSc1d1	elektronický
nástroj	nástroj	k1gInSc1	nástroj
Martenotovy	Martenotův	k2eAgFnSc2d1	Martenotův
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Třech	tři	k4xCgFnPc6	tři
malých	malý	k2eAgFnPc6d1	malá
liturgiích	liturgie	k1gFnPc6	liturgie
používá	používat	k5eAaImIp3nS	používat
vedle	vedle	k7c2	vedle
ženského	ženský	k2eAgInSc2d1	ženský
sboru	sbor	k1gInSc2	sbor
Martenotovy	Martenotův	k2eAgFnSc2d1	Martenotův
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
celestu	celesta	k1gFnSc4	celesta
<g/>
,	,	kIx,	,
vibrafon	vibrafon	k1gInSc4	vibrafon
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
barevnosti	barevnost	k1gFnPc4	barevnost
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
živelné	živelný	k2eAgFnPc1d1	živelná
<g/>
,	,	kIx,	,
nepietní	pietní	k2eNgFnPc1d1	pietní
atmosféry	atmosféra	k1gFnPc1	atmosféra
plné	plný	k2eAgFnPc1d1	plná
života	život	k1gInSc2	život
a	a	k8xC	a
dynamiky	dynamika	k1gFnSc2	dynamika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
skladatelův	skladatelův	k2eAgInSc4d1	skladatelův
text	text	k1gInSc4	text
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
premiéře	premiéra	k1gFnSc6	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
měla	mít	k5eAaImAgFnS	mít
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
skandálu	skandál	k1gInSc3	skandál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
varhanní	varhanní	k2eAgFnPc4d1	varhanní
skladby	skladba	k1gFnPc4	skladba
patří	patřit	k5eAaImIp3nS	patřit
Meditace	meditace	k1gFnSc1	meditace
nad	nad	k7c7	nad
tajemstvím	tajemství	k1gNnSc7	tajemství
Svaté	svatý	k2eAgFnSc2d1	svatá
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
opatřil	opatřit	k5eAaPmAgMnS	opatřit
vlastními	vlastní	k2eAgFnPc7d1	vlastní
úvahami	úvaha	k1gFnPc7	úvaha
inspirovanými	inspirovaný	k2eAgFnPc7d1	inspirovaná
trojiční	trojiční	k2eAgInSc4d1	trojiční
naukou	nauka	k1gFnSc7	nauka
teologické	teologický	k2eAgMnPc4d1	teologický
summy	summ	k1gMnPc4	summ
Tomáše	Tomáš	k1gMnSc4	Tomáš
Akvinského	Akvinský	k2eAgMnSc4d1	Akvinský
nebo	nebo	k8xC	nebo
liturgií	liturgie	k1gFnSc7	liturgie
mešního	mešní	k2eAgInSc2d1	mešní
kánonu	kánon	k1gInSc2	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nS	patřit
Kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Nanebevstoupení	nanebevstoupení	k1gNnSc4	nanebevstoupení
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
experimentální	experimentální	k2eAgInSc1d1	experimentální
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
obtížné	obtížný	k2eAgFnPc1d1	obtížná
Čtyři	čtyři	k4xCgFnPc1	čtyři
etudy	etuda	k1gFnPc1	etuda
rytmu	rytmus	k1gInSc2	rytmus
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
etud	etuda	k1gFnPc2	etuda
používá	používat	k5eAaImIp3nS	používat
čtyři	čtyři	k4xCgFnPc1	čtyři
řady	řada	k1gFnPc1	řada
různých	různý	k2eAgFnPc2d1	různá
tónových	tónový	k2eAgFnPc2d1	tónová
délek	délka	k1gFnPc2	délka
a	a	k8xC	a
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
úhozů	úhoz	k1gInPc2	úhoz
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
silových	silový	k2eAgFnPc2d1	silová
intenzit	intenzita	k1gFnPc2	intenzita
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
tónových	tónový	k2eAgFnPc2d1	tónová
délek	délka	k1gFnPc2	délka
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
dvanáctitónové	dvanáctitónový	k2eAgFnPc1d1	dvanáctitónová
řady	řada	k1gFnPc1	řada
tónových	tónový	k2eAgFnPc2d1	tónová
výšek	výška	k1gFnPc2	výška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
čistě	čistě	k6eAd1	čistě
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
kompozice	kompozice	k1gFnSc1	kompozice
pevně	pevně	k6eAd1	pevně
svázaná	svázaný	k2eAgFnSc1d1	svázaná
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
vnější	vnější	k2eAgFnSc2d1	vnější
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
zvukových	zvukový	k2eAgInPc2d1	zvukový
prvků	prvek	k1gInPc2	prvek
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
Představy	představa	k1gFnSc2	představa
slova	slovo	k1gNnSc2	slovo
Amen	amen	k1gNnSc2	amen
provedeného	provedený	k2eAgNnSc2d1	provedené
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
částí	část	k1gFnPc2	část
podle	podle	k7c2	podle
dní	den	k1gInPc2	den
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
hudebním	hudební	k2eAgMnSc7d1	hudební
jazykem	jazyk	k1gMnSc7	jazyk
sám	sám	k3xTgMnSc1	sám
například	například	k6eAd1	například
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
)	)	kIx)	)
kruhový	kruhový	k2eAgInSc4d1	kruhový
pohyb	pohyb	k1gInSc4	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
nebo	nebo	k8xC	nebo
oběhy	oběh	k1gInPc4	oběh
planet	planeta	k1gFnPc2	planeta
za	za	k7c4	za
užití	užití	k1gNnSc4	užití
různých	různý	k2eAgInPc2d1	různý
intervalů	interval	k1gInPc2	interval
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
hudební	hudební	k2eAgFnSc2d1	hudební
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
nejsou	být	k5eNaImIp3nP	být
cizí	cizí	k2eAgInPc4d1	cizí
geometrické	geometrický	k2eAgInPc4d1	geometrický
pojmy	pojem	k1gInPc4	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
používá	používat	k5eAaImIp3nS	používat
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgInSc2	svůj
dlouholetého	dlouholetý	k2eAgInSc2d1	dlouholetý
experimentu	experiment	k1gInSc2	experiment
se	s	k7c7	s
zachycováním	zachycování	k1gNnSc7	zachycování
ptačího	ptačí	k2eAgInSc2d1	ptačí
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
převádění	převádění	k1gNnSc2	převádění
do	do	k7c2	do
hudební	hudební	k2eAgFnSc2d1	hudební
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
ptačího	ptačí	k2eAgInSc2d1	ptačí
zpěvu	zpěv	k1gInSc2	zpěv
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vedle	vedle	k7c2	vedle
okouzlení	okouzlení	k1gNnSc2	okouzlení
přírodou	příroda	k1gFnSc7	příroda
skladatelovým	skladatelův	k2eAgInSc7d1	skladatelův
celoživotním	celoživotní	k2eAgInSc7d1	celoživotní
zájmem	zájem	k1gInSc7	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Přineslo	přinést	k5eAaPmAgNnS	přinést
řadu	řad	k1gInSc2	řad
samostatných	samostatný	k2eAgInPc2d1	samostatný
cyklů	cyklus	k1gInPc2	cyklus
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
Exotičtí	exotický	k2eAgMnPc1d1	exotický
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
nebo	nebo	k8xC	nebo
Katalog	katalog	k1gInSc1	katalog
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
hudební	hudební	k2eAgInPc4d1	hudební
názory	názor	k1gInPc4	názor
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Techniky	technika	k1gFnSc2	technika
mého	můj	k3xOp1gInSc2	můj
hudebního	hudební	k2eAgInSc2d1	hudební
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
"	"	kIx"	"
<g/>
sluchový	sluchový	k2eAgInSc1d1	sluchový
vjem	vjem	k1gInSc1	vjem
rozkošnicky	rozkošnicky	k6eAd1	rozkošnicky
rafinovaných	rafinovaný	k2eAgFnPc2d1	rafinovaná
radostí	radost	k1gFnPc2	radost
v	v	k7c6	v
mihotavé	mihotavý	k2eAgFnSc6d1	mihotavá
hudbě	hudba	k1gFnSc6	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dokládá	dokládat	k5eAaImIp3nS	dokládat
skladba	skladba	k1gFnSc1	skladba
Chronochromie	chronochromie	k1gFnSc1	chronochromie
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
zvonkohru	zvonkohra	k1gFnSc4	zvonkohra
<g/>
,	,	kIx,	,
xylofon	xylofon	k1gInSc4	xylofon
<g/>
,	,	kIx,	,
marimbu	marimba	k1gFnSc4	marimba
a	a	k8xC	a
3	[number]	k4	3
skupiny	skupina	k1gFnSc2	skupina
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
efekt	efekt	k1gInSc1	efekt
synestezie	synestezie	k1gFnSc2	synestezie
(	(	kIx(	(
<g/>
vyvolání	vyvolání	k1gNnSc4	vyvolání
barevného	barevný	k2eAgInSc2d1	barevný
vjemu	vjem	k1gInSc2	vjem
pomocí	pomocí	k7c2	pomocí
tónů	tón	k1gInPc2	tón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Messiaenova	Messiaenův	k2eAgNnSc2d1	Messiaenův
díla	dílo	k1gNnSc2	dílo
zabírá	zabírat	k5eAaImIp3nS	zabírat
téma	téma	k1gNnSc4	téma
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
vyjádřené	vyjádřený	k2eAgNnSc1d1	vyjádřené
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
písňovém	písňový	k2eAgInSc6d1	písňový
triptychu	triptych	k1gInSc6	triptych
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Tristana	Tristan	k1gMnSc2	Tristan
a	a	k8xC	a
Isoldy	Isolda	k1gMnSc2	Isolda
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
osudová	osudový	k2eAgFnSc1d1	osudová
láska	láska	k1gFnSc1	láska
překračující	překračující	k2eAgFnSc2d1	překračující
hranice	hranice	k1gFnSc2	hranice
lidské	lidský	k2eAgFnSc2d1	lidská
tělesnosti	tělesnost	k1gFnSc2	tělesnost
a	a	k8xC	a
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harawi	Harawi	k6eAd1	Harawi
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Zpěvy	zpěv	k1gInPc4	zpěv
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladbou	skladba	k1gFnSc7	skladba
pro	pro	k7c4	pro
ženský	ženský	k2eAgInSc4d1	ženský
soprán	soprán	k1gInSc4	soprán
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
quiché	quichý	k2eAgFnSc2d1	quichý
značí	značit	k5eAaImIp3nP	značit
harawi	harawi	k6eAd1	harawi
milostné	milostný	k2eAgFnPc1d1	milostná
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
končí	končit	k5eAaImIp3nP	končit
smrtí	smrt	k1gFnPc2	smrt
obou	dva	k4xCgMnPc2	dva
milenců	milenec	k1gMnPc2	milenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
Turangalila	Turangalila	k1gFnSc1	Turangalila
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
skladby	skladba	k1gFnSc2	skladba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgNnPc4	dva
sanskrtská	sanskrtský	k2eAgNnPc4d1	sanskrtské
slova	slovo	k1gNnPc4	slovo
s	s	k7c7	s
významy	význam	k1gInPc7	význam
milostné	milostný	k2eAgFnSc2d1	milostná
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rytmus	rytmus	k1gInSc1	rytmus
skladby	skladba	k1gFnSc2	skladba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
indických	indický	k2eAgFnPc2d1	indická
rág	rág	k?	rág
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
zpěvů	zpěv	k1gInPc2	zpěv
pro	pro	k7c4	pro
dvanáct	dvanáct	k4xCc4	dvanáct
smíšených	smíšený	k2eAgInPc2d1	smíšený
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
umělým	umělý	k2eAgInSc7d1	umělý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sám	sám	k3xTgMnSc1	sám
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
skladatelů	skladatel	k1gMnPc2	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olivier	Olivier	k1gMnSc1	Olivier
Messiaen	Messiaen	k2eAgMnSc1d1	Messiaen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://writings.heatherodonnell.info/Messiaen_and_Thoreau.html	[url]	k1gMnSc1	http://writings.heatherodonnell.info/Messiaen_and_Thoreau.html
</s>
</p>
