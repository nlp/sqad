<p>
<s>
Prefektura	prefektura	k1gFnSc1	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
高	高	k?	高
<g/>
,	,	kIx,	,
Kóči-ken	Kóčien	k1gInSc1	Kóči-ken
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
47	[number]	k4	47
prefektur	prefektura	k1gFnPc2	prefektura
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Šikoku	Šikok	k1gInSc2	Šikok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Kóči	Kóč	k1gFnSc2	Kóč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
prefektura	prefektura	k1gFnSc1	prefektura
na	na	k7c6	na
Šikoku	Šikok	k1gInSc6	Šikok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
nejmenší	malý	k2eAgInSc4d3	nejmenší
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prefektura	prefektura	k1gFnSc1	prefektura
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
7104,87	[number]	k4	7104,87
km2	km2	k4	km2
a	a	k8xC	a
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
měla	mít	k5eAaImAgFnS	mít
796	[number]	k4	796
196	[number]	k4	196
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
reformami	reforma	k1gFnPc7	reforma
Meidži	Meidž	k1gFnSc3	Meidž
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
prefektury	prefektura	k1gFnSc2	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
provincie	provincie	k1gFnSc1	provincie
Tosa	Tosa	k1gFnSc1	Tosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Prefektura	prefektura	k1gFnSc1	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Šikoku	Šikok	k1gInSc2	Šikok
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
jen	jen	k9	jen
na	na	k7c6	na
pár	pár	k4xCyI	pár
místech	místo	k1gNnPc6	místo
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
okolí	okolí	k1gNnSc4	okolí
měst	město	k1gNnPc2	město
Kóči	Kóč	k1gFnSc2	Kóč
a	a	k8xC	a
Šimanto	Šimanta	k1gFnSc5	Šimanta
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
planiny	planina	k1gFnPc1	planina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Kóči	Kóči	k1gNnSc2	Kóči
leží	ležet	k5eAaImIp3nS	ležet
11	[number]	k4	11
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
市	市	k?	市
<g/>
,	,	kIx,	,
ši	ši	k?	ši
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kō	Kō	k1gFnSc2	Kō
Prefecture	Prefectur	k1gMnSc5	Prefectur
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Prefektura	prefektura	k1gFnSc1	prefektura
Kóči	Kóč	k1gInSc6	Kóč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc4d1	lokální
odkaz	odkaz	k1gInSc4	odkaz
<g/>
:	:	kIx,	:
Kochi	Koch	k1gMnSc5	Koch
prefecture	prefectur	k1gMnSc5	prefectur
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Kō	Kō	k1gMnSc5	Kō
prefecture	prefectur	k1gMnSc5	prefectur
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
prefektury	prefektura	k1gFnSc2	prefektura
Kóči	Kóč	k1gFnSc2	Kóč
</s>
</p>
