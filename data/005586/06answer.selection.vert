<s>
Proteáza	Proteáza	k1gFnSc1	Proteáza
(	(	kIx(	(
<g/>
též	též	k9	též
proteináza	proteináza	k1gFnSc1	proteináza
či	či	k8xC	či
peptidáza	peptidáza	k1gFnSc1	peptidáza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
štěpí	štěpit	k5eAaImIp3nP	štěpit
proteiny	protein	k1gInPc4	protein
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
