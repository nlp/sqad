<s>
Proteáza	Proteáza	k1gFnSc1	Proteáza
(	(	kIx(	(
<g/>
též	též	k9	též
proteináza	proteináza	k1gFnSc1	proteináza
či	či	k8xC	či
peptidáza	peptidáza	k1gFnSc1	peptidáza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
štěpí	štěpit	k5eAaImIp3nP	štěpit
proteiny	protein	k1gInPc4	protein
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
hydroláz	hydroláza	k1gFnPc2	hydroláza
<g/>
.	.	kIx.	.
</s>
<s>
Hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
peptidické	peptidický	k2eAgFnPc4d1	peptidická
vazby	vazba	k1gFnPc4	vazba
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yQgFnPc2	který
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
peptidickém	peptidický	k2eAgInSc6d1	peptidický
řetězci	řetězec	k1gInSc6	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
proteáz	proteáza	k1gFnPc2	proteáza
<g/>
.	.	kIx.	.
</s>
<s>
Proteázy	Proteáza	k1gFnPc1	Proteáza
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
dle	dle	k7c2	dle
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
štěpí	štěpit	k5eAaImIp3nS	štěpit
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
na	na	k7c4	na
exoproteázy	exoproteáza	k1gFnPc4	exoproteáza
a	a	k8xC	a
endoproteázy	endoproteáza	k1gFnPc4	endoproteáza
<g/>
:	:	kIx,	:
exoproteázy	exoproteáza	k1gFnPc4	exoproteáza
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
od	od	k7c2	od
terminálních	terminální	k2eAgInPc2d1	terminální
konců	konec	k1gInPc2	konec
proteinů	protein	k1gInPc2	protein
endoproteázy	endoproteáza	k1gFnSc2	endoproteáza
štěpí	štěpit	k5eAaImIp3nS	štěpit
proteiny	protein	k1gInPc4	protein
uvnitř	uvnitř	k7c2	uvnitř
peptidického	peptidický	k2eAgInSc2d1	peptidický
řetězce	řetězec	k1gInSc2	řetězec
a	a	k8xC	a
narušují	narušovat	k5eAaImIp3nP	narušovat
jeho	jeho	k3xOp3gFnSc4	jeho
terciární	terciární	k2eAgFnSc4d1	terciární
strukturu	struktura	k1gFnSc4	struktura
Při	při	k7c6	při
ovlivňování	ovlivňování	k1gNnSc6	ovlivňování
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
uvnitř	uvnitř	k7c2	uvnitř
organismu	organismus	k1gInSc2	organismus
hrají	hrát	k5eAaImIp3nP	hrát
daleko	daleko	k6eAd1	daleko
významnější	významný	k2eAgFnSc4d2	významnější
úlohu	úloha	k1gFnSc4	úloha
endoproteázy	endoproteáza	k1gFnSc2	endoproteáza
<g/>
.	.	kIx.	.
</s>
<s>
Endoproteázy	Endoproteáza	k1gFnPc1	Endoproteáza
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
katalytických	katalytický	k2eAgFnPc2d1	katalytická
skupin	skupina	k1gFnPc2	skupina
přítomných	přítomný	k2eAgFnPc2d1	přítomná
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proteáza	proteáza	k1gFnSc1	proteáza
na	na	k7c4	na
bílkovinu	bílkovina	k1gFnSc4	bílkovina
působí	působit	k5eAaImIp3nP	působit
Cysteinové	Cysteinový	k2eAgFnPc1d1	Cysteinový
proteázy	proteáza	k1gFnPc1	proteáza
<g/>
:	:	kIx,	:
katepsiny	katepsina	k1gFnPc1	katepsina
<g/>
,	,	kIx,	,
calpainy	calpaina	k1gFnPc1	calpaina
<g/>
,	,	kIx,	,
papain	papain	k1gInSc1	papain
<g/>
,	,	kIx,	,
cathepsin-Z-like	cathepsin-Zikat	k5eAaPmIp3nS	cathepsin-Z-likat
proteáza	proteáza	k1gFnSc1	proteáza
Serinové	Serinový	k2eAgFnSc2d1	Serinový
proteázy	proteáza	k1gFnSc2	proteáza
<g/>
:	:	kIx,	:
chymotrypsin	chymotrypsin	k1gInSc1	chymotrypsin
<g/>
,	,	kIx,	,
trypsin	trypsin	k1gInSc1	trypsin
<g/>
,	,	kIx,	,
elastáza	elastáza	k1gFnSc1	elastáza
Treoninové	Treoninový	k2eAgFnSc2d1	Treoninový
proteázy	proteáza	k1gFnSc2	proteáza
Metalloproteázy	Metalloproteáza	k1gFnSc2	Metalloproteáza
Glutamátové	Glutamátový	k2eAgFnSc2d1	Glutamátová
proteázy	proteáza	k1gFnSc2	proteáza
Aspartátové	Aspartátový	k2eAgFnSc2d1	Aspartátový
proteázy	proteáza	k1gFnSc2	proteáza
<g/>
:	:	kIx,	:
pepsin	pepsin	k1gInSc1	pepsin
<g/>
,	,	kIx,	,
chymosin	chymosin	k1gMnSc1	chymosin
<g/>
,	,	kIx,	,
renin	renin	k1gMnSc1	renin
<g/>
,	,	kIx,	,
katepsin	katepsin	k1gMnSc1	katepsin
D	D	kA	D
</s>
