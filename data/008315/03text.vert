<p>
<s>
Avantasia	Avantasia	k1gFnSc1	Avantasia
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc4	projekt
metalové	metalový	k2eAgFnSc2d1	metalová
opery	opera	k1gFnSc2	opera
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Tobiasem	Tobias	k1gMnSc7	Tobias
Sammetem	Sammet	k1gMnSc7	Sammet
<g/>
,	,	kIx,	,
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
skupiny	skupina	k1gFnSc2	skupina
Edguy	Edgua	k1gFnSc2	Edgua
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původně	původně	k6eAd1	původně
jednorázového	jednorázový	k2eAgInSc2d1	jednorázový
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gInSc7	jehož
názvem	název	k1gInSc7	název
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dvě	dva	k4xCgNnPc4	dva
power	powero	k1gNnPc2	powero
<g/>
/	/	kIx~	/
<g/>
speedmetalová	speedmetalový	k2eAgFnSc1d1	speedmetalový
konceptuální	konceptuální	k2eAgFnSc1d1	konceptuální
alba	alba	k1gFnSc1	alba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
stala	stát	k5eAaPmAgFnS	stát
plnohodnotná	plnohodnotný	k2eAgFnSc1d1	plnohodnotná
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
duem	duo	k1gNnSc7	duo
Sammet	Sammet	k1gInSc1	Sammet
a	a	k8xC	a
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
kromě	kromě	k7c2	kromě
postu	post	k1gInSc2	post
kytaristy	kytarista	k1gMnSc2	kytarista
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
také	také	k9	také
celkovou	celkový	k2eAgFnSc4d1	celková
produkci	produkce	k1gFnSc4	produkce
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
organizaci	organizace	k1gFnSc4	organizace
nahrávání	nahrávání	k1gNnSc2	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k5eAaImF	Sammet
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
fantasy	fantas	k1gInPc1	fantas
příběhů	příběh	k1gInPc2	příběh
začal	začít	k5eAaPmAgInS	začít
soustředit	soustředit	k5eAaPmF	soustředit
spíše	spíše	k9	spíše
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
tři	tři	k4xCgNnPc4	tři
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
vydaná	vydaný	k2eAgNnPc4d1	vydané
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
až	až	k9	až
2010	[number]	k4	2010
tak	tak	k6eAd1	tak
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
více	hodně	k6eAd2	hodně
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
žánrů	žánr	k1gInPc2	žánr
<g/>
;	;	kIx,	;
od	od	k7c2	od
power	powra	k1gFnPc2	powra
metalu	metal	k1gInSc2	metal
až	až	k9	až
po	po	k7c4	po
pop	pop	k1gInSc4	pop
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
a	a	k8xC	a
textově	textově	k6eAd1	textově
jsou	být	k5eAaImIp3nP	být
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
zpovědí	zpověď	k1gFnSc7	zpověď
tohoto	tento	k3xDgMnSc2	tento
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
formě	forma	k1gFnSc3	forma
konceptuálního	konceptuální	k2eAgInSc2d1	konceptuální
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
Avantasia	Avantasia	k1gFnSc1	Avantasia
navrátila	navrátit	k5eAaPmAgFnS	navrátit
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
třech	tři	k4xCgFnPc6	tři
deskách	deska	k1gFnPc6	deska
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Ghostlights	Ghostlights	k1gInSc1	Ghostlights
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
také	také	k9	také
určitý	určitý	k2eAgInSc1d1	určitý
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
hudebnímu	hudební	k2eAgNnSc3d1	hudební
stylu	styl	k1gInSc2	styl
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ale	ale	k9	ale
také	také	k9	také
zastoupené	zastoupený	k2eAgInPc4d1	zastoupený
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
desek	deska	k1gFnPc2	deska
toho	ten	k3xDgInSc2	ten
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
albech	album	k1gNnPc6	album
Avantasia	Avantasium	k1gNnSc2	Avantasium
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hostujících	hostující	k2eAgMnPc2d1	hostující
zpěváků	zpěvák	k1gMnPc2	zpěvák
a	a	k8xC	a
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stálým	stálý	k2eAgFnPc3d1	stálá
personám	persona	k1gFnPc3	persona
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Michael	Michael	k1gMnSc1	Michael
Kiske	Kisk	k1gFnSc2	Kisk
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Jø	Jø	k1gMnSc5	Jø
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Catley	Catlea	k1gFnSc2	Catlea
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Rodenberg	Rodenberg	k1gMnSc1	Rodenberg
či	či	k8xC	či
Amanda	Amanda	k1gFnSc1	Amanda
Somerville	Somerville	k1gFnSc1	Somerville
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
projekt	projekt	k1gInSc4	projekt
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
Sammetovi	Sammetův	k2eAgMnPc1d1	Sammetův
s	s	k7c7	s
Paethem	Paeth	k1gInSc7	Paeth
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podpory	podpora	k1gFnSc2	podpora
alb	alba	k1gFnPc2	alba
pořádat	pořádat	k5eAaImF	pořádat
samostatná	samostatný	k2eAgNnPc4d1	samostatné
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
či	či	k8xC	či
také	také	k9	také
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Metalová	metalový	k2eAgFnSc1d1	metalová
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
nápady	nápad	k1gInPc4	nápad
na	na	k7c4	na
napsání	napsání	k1gNnSc4	napsání
metalové	metalový	k2eAgFnSc2d1	metalová
opery	opera	k1gFnSc2	opera
začal	začít	k5eAaPmAgMnS	začít
Tobias	Tobias	k1gMnSc1	Tobias
Sammet	Sammet	k1gMnSc1	Sammet
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začínající	začínající	k2eAgFnSc2d1	začínající
německé	německý	k2eAgFnSc2d1	německá
powermetalové	powermetal	k1gMnPc1	powermetal
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Edguy	Edgua	k1gFnSc2	Edgua
<g/>
,	,	kIx,	,
zapisovat	zapisovat	k5eAaImF	zapisovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Theater	Theatra	k1gFnPc2	Theatra
of	of	k?	of
Salvation	Salvation	k1gInSc1	Salvation
jeho	jeho	k3xOp3gFnSc2	jeho
domovské	domovský	k2eAgFnSc2d1	domovská
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
pouze	pouze	k6eAd1	pouze
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
studiový	studiový	k2eAgInSc1d1	studiový
projekt	projekt	k1gInSc1	projekt
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
konceptuální	konceptuální	k2eAgNnPc4d1	konceptuální
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speed	speed	k1gInSc4	speed
<g/>
/	/	kIx~	/
<g/>
powermetalové	powermetal	k1gMnPc1	powermetal
desky	deska	k1gFnSc2	deska
The	The	k1gFnSc1	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
Part	part	k1gInSc1	part
II	II	kA	II
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hudebně	hudebně	k6eAd1	hudebně
nepřímo	přímo	k6eNd1	přímo
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
alb	album	k1gNnPc2	album
Keeper	Keepero	k1gNnPc2	Keepero
of	of	k?	of
the	the	k?	the
Seven	Seven	k2eAgInSc4d1	Seven
Keys	Keys	k1gInSc4	Keys
Part	part	k1gInSc1	part
1	[number]	k4	1
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
Keeper	Keeper	k1gInSc1	Keeper
of	of	k?	of
the	the	k?	the
Seven	Seven	k2eAgInSc4d1	Seven
Keys	Keys	k1gInSc4	Keys
Part	part	k1gInSc1	part
2	[number]	k4	2
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
Helloween	Helloween	k1gInSc1	Helloween
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k5eAaPmF	Sammet
album	album	k1gNnSc4	album
textově	textově	k6eAd1	textově
zaobalil	zaobalit	k5eAaPmAgInS	zaobalit
do	do	k7c2	do
fantasy	fantas	k1gInPc1	fantas
příběhu	příběh	k1gInSc2	příběh
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
postavy	postava	k1gFnSc2	postava
Gabriela	Gabriel	k1gMnSc4	Gabriel
Laymanna	Laymann	k1gMnSc4	Laymann
<g/>
,	,	kIx,	,
učně	učeň	k1gMnPc4	učeň
Řádu	řád	k1gInSc2	řád
bratří	bratr	k1gMnPc2	bratr
kazatelů	kazatel	k1gMnPc2	kazatel
<g/>
,	,	kIx,	,
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
si	se	k3xPyFc3	se
pozval	pozvat	k5eAaPmAgMnS	pozvat
řadu	řada	k1gFnSc4	řada
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
zpěvů	zpěv	k1gInPc2	zpěv
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
Andre	Andr	k1gInSc5	Andr
Matos	Matos	k1gMnSc1	Matos
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Kiske	Kiske	k1gFnSc1	Kiske
<g/>
,	,	kIx,	,
Kai	Kai	k1gFnSc1	Kai
Hansen	Hansen	k1gInSc1	Hansen
<g/>
,	,	kIx,	,
Sharon	Sharon	k1gInSc1	Sharon
den	den	k1gInSc1	den
Adel	Adel	k1gInSc1	Adel
či	či	k8xC	či
Oliver	Oliver	k1gMnSc1	Oliver
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
obsadili	obsadit	k5eAaPmAgMnP	obsadit
kromě	kromě	k7c2	kromě
Sammeta	Sammeto	k1gNnSc2	Sammeto
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
čtyři	čtyři	k4xCgMnPc1	čtyři
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
;	;	kIx,	;
Henjo	Henjo	k1gMnSc1	Henjo
Richter	Richter	k1gMnSc1	Richter
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jens	Jens	k1gInSc1	Jens
Ludwig	Ludwig	k1gInSc1	Ludwig
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Markus	Markus	k1gMnSc1	Markus
Großkopf	Großkopf	k1gMnSc1	Großkopf
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alex	Alex	k1gMnSc1	Alex
Holzwarth	Holzwarth	k1gMnSc1	Holzwarth
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
souprava	souprava	k1gFnSc1	souprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nahrávání	nahrávání	k1gNnSc2	nahrávání
druhého	druhý	k4xOgMnSc2	druhý
dílu	díl	k1gInSc2	díl
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
ke	k	k7c3	k
zpěvákům	zpěvák	k1gMnPc3	zpěvák
připojili	připojit	k5eAaPmAgMnP	připojit
Bob	Bob	k1gMnSc1	Bob
Catley	Catlea	k1gFnSc2	Catlea
a	a	k8xC	a
Rob	roba	k1gFnPc2	roba
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
The	The	k1gFnSc6	The
Metal	metal	k1gInSc4	metal
Opera	opera	k1gFnSc1	opera
Part	part	k1gInSc1	part
II	II	kA	II
Sammet	Sammet	k1gInSc1	Sammet
hojně	hojně	k6eAd1	hojně
využil	využít	k5eAaPmAgInS	využít
sborové	sborový	k2eAgInPc4d1	sborový
refrény	refrén	k1gInPc4	refrén
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
staly	stát	k5eAaPmAgFnP	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
poznávacích	poznávací	k2eAgInPc2d1	poznávací
znaků	znak	k1gInPc2	znak
jeho	jeho	k3xOp3gFnSc2	jeho
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
<g/>
Obě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
byla	být	k5eAaImAgFnS	být
hudebními	hudební	k2eAgFnPc7d1	hudební
kritiky	kritika	k1gFnSc2	kritika
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
kladně	kladně	k6eAd1	kladně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
Avantasie	Avantasie	k1gFnSc2	Avantasie
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
Sammet	Sammet	k1gInSc4	Sammet
je	být	k5eAaImIp3nS	být
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
něco	něco	k3yInSc1	něco
výjimečného	výjimečný	k2eAgNnSc2d1	výjimečné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
nahrávání	nahrávání	k1gNnSc1	nahrávání
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
znamenalo	znamenat	k5eAaImAgNnS	znamenat
spousty	spousta	k1gFnPc4	spousta
stresových	stresový	k2eAgFnPc2d1	stresová
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
dostal	dostat	k5eAaPmAgMnS	dostat
"	"	kIx"	"
<g/>
až	až	k9	až
na	na	k7c4	na
hranice	hranice	k1gFnPc4	hranice
svých	svůj	k3xOyFgFnPc2	svůj
možností	možnost	k1gFnPc2	možnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
náročnosti	náročnost	k1gFnSc2	náročnost
organizování	organizování	k1gNnSc2	organizování
tak	tak	k9	tak
velkého	velký	k2eAgInSc2d1	velký
projektu	projekt	k1gInSc2	projekt
s	s	k7c7	s
tak	tak	k6eAd1	tak
málo	málo	k4c7	málo
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
proto	proto	k8xC	proto
už	už	k6eAd1	už
dále	daleko	k6eAd2	daleko
neměl	mít	k5eNaImAgMnS	mít
chuť	chuť	k1gFnSc4	chuť
se	se	k3xPyFc4	se
do	do	k7c2	do
podobného	podobný	k2eAgInSc2d1	podobný
projektu	projekt	k1gInSc2	projekt
znovu	znovu	k6eAd1	znovu
pouštět	pouštět	k5eAaImF	pouštět
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
Edguy	Edgua	k1gFnSc2	Edgua
Sammeta	Sammet	k1gMnSc2	Sammet
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
plně	plně	k6eAd1	plně
podporovali	podporovat	k5eAaImAgMnP	podporovat
a	a	k8xC	a
dle	dle	k7c2	dle
Dirka	Direk	k1gInSc2	Direk
Sauera	Sauero	k1gNnSc2	Sauero
<g/>
,	,	kIx,	,
kytaristy	kytarista	k1gMnPc4	kytarista
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
Edguy	Edgua	k1gFnPc4	Edgua
díky	díky	k7c3	díky
hostům	host	k1gMnPc3	host
působícím	působící	k2eAgInSc6d1	působící
v	v	k7c6	v
Avantasii	Avantasie	k1gFnSc6	Avantasie
hodně	hodně	k6eAd1	hodně
nových	nový	k2eAgMnPc2d1	nový
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovení	obnovení	k1gNnSc1	obnovení
projektu	projekt	k1gInSc2	projekt
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
Sammet	Sammet	k1gMnSc1	Sammet
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
nepokračovat	pokračovat	k5eNaImF	pokračovat
a	a	k8xC	a
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
Edguy	Edgua	k1gFnPc1	Edgua
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nahrát	nahrát	k5eAaBmF	nahrát
další	další	k2eAgFnSc4d1	další
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ho	on	k3xPp3gMnSc4	on
dovedly	dovést	k5eAaPmAgFnP	dovést
prosby	prosba	k1gFnPc1	prosba
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
přesvědčovali	přesvědčovat	k5eAaImAgMnP	přesvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
zkušeností	zkušenost	k1gFnPc2	zkušenost
než	než	k8xS	než
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k1gInSc1	Sammet
měl	mít	k5eAaImAgInS	mít
zároveň	zároveň	k6eAd1	zároveň
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
ho	on	k3xPp3gInSc4	on
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Nuclear	Nuclear	k1gInSc1	Nuclear
Blast	Blast	k1gFnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
obrovský	obrovský	k2eAgInSc1d1	obrovský
vliv	vliv	k1gInSc1	vliv
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nP	by
dle	dle	k7c2	dle
Sammeta	Sammet	k1gMnSc2	Sammet
žádné	žádný	k3yNgNnSc1	žádný
pokračování	pokračování	k1gNnSc1	pokračování
Avantasie	Avantasie	k1gFnSc2	Avantasie
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Paeth	Paeth	k1gMnSc1	Paeth
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
zvukový	zvukový	k2eAgMnSc1d1	zvukový
technik	technik	k1gMnSc1	technik
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
kompletní	kompletní	k2eAgFnSc4d1	kompletní
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Sammet	Sammet	k1gInSc1	Sammet
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
pouze	pouze	k6eAd1	pouze
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkou	myšlenka	k1gFnSc7	myšlenka
Sammeta	Sammeto	k1gNnSc2	Sammeto
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
natočit	natočit	k5eAaBmF	natočit
pokračování	pokračování	k1gNnSc4	pokračování
předchozích	předchozí	k2eAgNnPc2d1	předchozí
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
album	album	k1gNnSc4	album
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
vlastní	vlastní	k2eAgNnSc4d1	vlastní
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
The	The	k1gFnSc1	The
Scarecrow	Scarecrow	k1gFnSc1	Scarecrow
tak	tak	k6eAd1	tak
spojuje	spojovat	k5eAaImIp3nS	spojovat
hard	hard	k6eAd1	hard
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
speed	speed	k1gInSc1	speed
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgMnSc1d1	klasický
heavy	heava	k1gFnPc4	heava
metal	metat	k5eAaImAgMnS	metat
či	či	k8xC	či
rock	rock	k1gInSc4	rock
–	–	k?	–
Sammetovy	Sammetův	k2eAgInPc4d1	Sammetův
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
hudební	hudební	k2eAgInPc4d1	hudební
žánry	žánr	k1gInPc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
textově	textově	k6eAd1	textově
referovala	referovat	k5eAaBmAgFnS	referovat
o	o	k7c6	o
autorových	autorův	k2eAgInPc6d1	autorův
pocitech	pocit	k1gInPc6	pocit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Sammetovu	Sammetův	k2eAgFnSc4d1	Sammetův
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
zpověď	zpověď	k1gFnSc4	zpověď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
nahrávání	nahrávání	k1gNnSc6	nahrávání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
trojčlenné	trojčlenný	k2eAgNnSc1d1	trojčlenné
jádro	jádro	k1gNnSc1	jádro
Tobias	Tobias	k1gMnSc1	Tobias
Sammet	Sammet	k1gMnSc1	Sammet
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Singer	Singra	k1gFnPc2	Singra
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
souprava	souprava	k1gFnSc1	souprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
chtěli	chtít	k5eAaImAgMnP	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
technické	technický	k2eAgFnPc4d1	technická
úrovně	úroveň	k1gFnPc4	úroveň
zvuku	zvuk	k1gInSc2	zvuk
bicích	bicí	k2eAgInPc2d1	bicí
<g/>
,	,	kIx,	,
zamluvili	zamluvit	k5eAaPmAgMnP	zamluvit
si	se	k3xPyFc3	se
na	na	k7c4	na
týden	týden	k1gInSc4	týden
drahé	drahý	k2eAgNnSc1d1	drahé
německé	německý	k2eAgNnSc1d1	německé
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nahrávání	nahrávání	k1gNnSc4	nahrávání
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
skladeb	skladba	k1gFnPc2	skladba
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
Paethově	Paethův	k2eAgNnSc6d1	Paethův
studiu	studio	k1gNnSc6	studio
Gate	Gat	k1gFnSc2	Gat
Studio	studio	k1gNnSc1	studio
ve	v	k7c6	v
Wolfsburgu	Wolfsburg	k1gInSc6	Wolfsburg
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
hosté	host	k1gMnPc1	host
dorazili	dorazit	k5eAaPmAgMnP	dorazit
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgMnSc1d1	jiný
své	svůj	k3xOyFgInPc4	svůj
party	part	k1gInPc4	part
nahráli	nahrát	k5eAaPmAgMnP	nahrát
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hosté	host	k1gMnPc1	host
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
Avantasie	Avantasie	k1gFnSc2	Avantasie
přijali	přijmout	k5eAaPmAgMnP	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
zpěváci	zpěvák	k1gMnPc1	zpěvák
Roy	Roy	k1gMnSc1	Roy
Khan	Khan	k1gMnSc1	Khan
<g/>
,	,	kIx,	,
Jø	Jø	k1gMnSc5	Jø
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Kiske	Kisk	k1gFnSc2	Kisk
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Catley	Catlea	k1gFnSc2	Catlea
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
Somerville	Somerville	k1gFnSc1	Somerville
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Hartmann	Hartmann	k1gMnSc1	Hartmann
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Sammet	Sammet	k1gMnSc1	Sammet
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nezpívat	zpívat	k5eNaImF	zpívat
jen	jen	k6eAd1	jen
jedním	jeden	k4xCgInSc7	jeden
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
různé	různý	k2eAgFnPc4d1	různá
polohy	poloha	k1gFnPc4	poloha
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
kytarové	kytarový	k2eAgFnPc4d1	kytarová
party	parta	k1gFnPc4	parta
a	a	k8xC	a
sóla	sólo	k1gNnPc4	sólo
nahráli	nahrát	k5eAaBmAgMnP	nahrát
Henjo	Henjo	k6eAd1	Henjo
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Kai	Kai	k1gMnSc1	Kai
Hansen	Hansen	k1gInSc1	Hansen
a	a	k8xC	a
Rudolph	Rudolph	k1gInSc1	Rudolph
Schenke	Schenk	k1gFnSc2	Schenk
a	a	k8xC	a
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
hrál	hrát	k5eAaImAgMnS	hrát
Michael	Michael	k1gMnSc1	Michael
Rodenberg	Rodenberg	k1gMnSc1	Rodenberg
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
předcházely	předcházet	k5eAaImAgFnP	předcházet
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Lost	Lost	k1gMnSc1	Lost
in	in	k?	in
Space	Space	k1gFnSc1	Space
Part	part	k1gInSc1	part
I	i	k9	i
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lost	Lost	k1gInSc1	Lost
in	in	k?	in
Space	Spaec	k1gInSc2	Spaec
Part	parta	k1gFnPc2	parta
II	II	kA	II
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Scarecrow	Scarecrow	k1gMnPc3	Scarecrow
nakonec	nakonec	k6eAd1	nakonec
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
na	na	k7c6	na
osmé	osmý	k4xOgFnSc6	osmý
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
Media	medium	k1gNnSc2	medium
Control	Controla	k1gFnPc2	Controla
Charts	Chartsa	k1gFnPc2	Chartsa
a	a	k8xC	a
na	na	k7c6	na
desáté	desátý	k4xOgFnSc6	desátý
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
žebříčku	žebříček	k1gInSc6	žebříček
Sverigetopplistan	Sverigetopplistan	k1gInSc1	Sverigetopplistan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podpory	podpora	k1gFnSc2	podpora
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
Sammet	Sammet	k1gMnSc1	Sammet
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
s	s	k7c7	s
Avantasií	Avantasie	k1gFnSc7	Avantasie
odehrát	odehrát	k5eAaPmF	odehrát
pár	pár	k4xCyI	pár
koncertů	koncert	k1gInPc2	koncert
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
velkých	velký	k2eAgInPc6d1	velký
festivalech	festival	k1gInPc6	festival
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
i	i	k9	i
český	český	k2eAgInSc4d1	český
Masters	Masters	k1gInSc4	Masters
of	of	k?	of
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
kapely	kapela	k1gFnSc2	kapela
pro	pro	k7c4	pro
živá	živý	k2eAgNnPc4d1	živé
vystoupení	vystoupení	k1gNnPc4	vystoupení
tvořil	tvořit	k5eAaImAgInS	tvořit
Sammet	Sammet	k1gInSc1	Sammet
<g/>
,	,	kIx,	,
na	na	k7c4	na
kytary	kytara	k1gFnPc4	kytara
hráli	hrát	k5eAaImAgMnP	hrát
Paeth	Paeth	k1gMnSc1	Paeth
a	a	k8xC	a
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
basy	basa	k1gFnSc2	basa
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Robert	Robert	k1gMnSc1	Robert
Hunecke-Rizzo	Hunecke-Rizza	k1gFnSc5	Hunecke-Rizza
<g/>
,	,	kIx,	,
postu	post	k1gInSc6	post
klávesisty	klávesista	k1gMnSc2	klávesista
Rodenberg	Rodenberg	k1gMnSc1	Rodenberg
a	a	k8xC	a
za	za	k7c2	za
bicí	bicí	k2eAgFnSc2d1	bicí
usedl	usednout	k5eAaPmAgMnS	usednout
Felix	Felix	k1gMnSc1	Felix
Bohnke	Bohnk	k1gInSc2	Bohnk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hostujících	hostující	k2eAgMnPc2d1	hostující
zpěváků	zpěvák	k1gMnPc2	zpěvák
se	se	k3xPyFc4	se
koncertů	koncert	k1gInPc2	koncert
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
Lande	Land	k1gInSc5	Land
<g/>
,	,	kIx,	,
Somerville	Somervill	k1gMnPc4	Somervill
<g/>
,	,	kIx,	,
Catley	Catle	k1gMnPc4	Catle
<g/>
,	,	kIx,	,
Claudy	Claud	k1gMnPc4	Claud
Yang	Yanga	k1gFnPc2	Yanga
a	a	k8xC	a
Andre	Andr	k1gInSc5	Andr
Matos	Matos	k1gMnSc1	Matos
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c4	na
Masters	Masters	k1gInSc4	Masters
of	of	k?	of
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
na	na	k7c4	na
Wacken	Wacken	k2eAgInSc4d1	Wacken
Open	Open	k1gInSc4	Open
Air	Air	k1gMnPc2	Air
bylo	být	k5eAaImAgNnS	být
zaznamenáváno	zaznamenávat	k5eAaImNgNnS	zaznamenávat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c4	na
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Flying	Flying	k1gInSc1	Flying
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
turné	turné	k1gNnSc4	turné
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Scarecrow	Scarecrow	k1gFnSc2	Scarecrow
měl	mít	k5eAaImAgMnS	mít
Sammet	Sammet	k1gMnSc1	Sammet
dostatek	dostatek	k1gInSc4	dostatek
složených	složený	k2eAgFnPc2d1	složená
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
vydal	vydat	k5eAaPmAgMnS	vydat
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
značně	značně	k6eAd1	značně
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
desek	deska	k1gFnPc2	deska
Avantasie	Avantasie	k1gFnSc2	Avantasie
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgMnS	chtít
tím	ten	k3xDgNnSc7	ten
fanoušky	fanoušek	k1gMnPc7	fanoušek
zatěžovat	zatěžovat	k5eAaImF	zatěžovat
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
tyto	tento	k3xDgFnPc4	tento
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jedná	jednat	k5eAaImIp3nS	jednat
textově	textově	k6eAd1	textově
o	o	k7c4	o
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
určitou	určitý	k2eAgFnSc7d1	určitá
autorovou	autorův	k2eAgFnSc7d1	autorova
osobní	osobní	k2eAgFnSc7d1	osobní
reflexí	reflexe	k1gFnSc7	reflexe
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
dalšího	další	k2eAgNnSc2d1	další
alba	album	k1gNnSc2	album
ale	ale	k9	ale
dostávali	dostávat	k5eAaImAgMnP	dostávat
Sammet	Sammet	k1gMnSc1	Sammet
a	a	k8xC	a
Paeth	Paeth	k1gMnSc1	Paeth
nové	nový	k2eAgInPc4d1	nový
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
udělat	udělat	k5eAaPmF	udělat
desky	deska	k1gFnPc1	deska
dvě	dva	k4xCgNnPc1	dva
<g/>
;	;	kIx,	;
The	The	k1gFnSc1	The
Wicked	Wicked	k1gInSc4	Wicked
Symphony	Symphona	k1gFnSc2	Symphona
a	a	k8xC	a
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Babylon	Babylon	k1gInSc1	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
vydání	vydání	k1gNnSc1	vydání
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dlouho	dlouho	k6eAd1	dlouho
odkládalo	odkládat	k5eAaImAgNnS	odkládat
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neustále	neustále	k6eAd1	neustále
přicházejících	přicházející	k2eAgInPc2d1	přicházející
nových	nový	k2eAgInPc2d1	nový
nápadů	nápad	k1gInPc2	nápad
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
protože	protože	k8xS	protože
Sammet	Sammet	k1gMnSc1	Sammet
zároveň	zároveň	k6eAd1	zároveň
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
domovskou	domovský	k2eAgFnSc7d1	domovská
skupinou	skupina	k1gFnSc7	skupina
Edguy	Edgua	k1gFnSc2	Edgua
<g/>
.	.	kIx.	.
</s>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Eric	Eric	k1gFnSc4	Eric
Singer	Singra	k1gFnPc2	Singra
se	se	k3xPyFc4	se
zase	zase	k9	zase
nemohl	moct	k5eNaImAgMnS	moct
nahrávání	nahrávání	k1gNnSc4	nahrávání
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
Sammet	Sammet	k1gInSc4	Sammet
a	a	k8xC	a
Paeth	Paeth	k1gInSc4	Paeth
nemohli	moct	k5eNaImAgMnP	moct
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
oslovili	oslovit	k5eAaPmAgMnP	oslovit
Alexe	Alex	k1gMnSc5	Alex
Holzwartha	Holzwarth	k1gMnSc2	Holzwarth
a	a	k8xC	a
Felixe	Felix	k1gMnSc2	Felix
Bohnkeho	Bohnke	k1gMnSc2	Bohnke
<g/>
;	;	kIx,	;
bicí	bicí	k2eAgInSc1d1	bicí
tedy	tedy	k8xC	tedy
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgMnPc1	tři
hudebníci	hudebník	k1gMnPc1	hudebník
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dle	dle	k7c2	dle
Sammeta	Sammeto	k1gNnSc2	Sammeto
přidalo	přidat	k5eAaPmAgNnS	přidat
desce	deska	k1gFnSc6	deska
"	"	kIx"	"
<g/>
na	na	k7c6	na
atraktivitě	atraktivita	k1gFnSc6	atraktivita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
vyšla	vyjít	k5eAaPmAgFnS	vyjít
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hostující	hostující	k2eAgMnPc1d1	hostující
zpěváci	zpěvák	k1gMnPc1	zpěvák
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
představili	představit	k5eAaPmAgMnP	představit
Lande	Land	k1gInSc5	Land
<g/>
,	,	kIx,	,
Kiske	Kisk	k1gMnPc4	Kisk
<g/>
,	,	kIx,	,
Catley	Catle	k1gMnPc4	Catle
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
Meine	Mein	k1gMnSc5	Mein
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Owens	Owens	k1gInSc1	Owens
<g/>
,	,	kIx,	,
Cloudy	Clouda	k1gFnPc1	Clouda
Yang	Yang	k1gMnSc1	Yang
<g/>
,	,	kIx,	,
Matos	Matos	k1gMnSc1	Matos
<g/>
,	,	kIx,	,
Ralf	Ralf	k1gMnSc1	Ralf
Zdiarstek	Zdiarstek	k1gInSc1	Zdiarstek
<g/>
,	,	kIx,	,
Russell	Russell	k1gMnSc1	Russell
Allen	Allen	k1gMnSc1	Allen
a	a	k8xC	a
Jon	Jon	k1gMnSc1	Jon
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kytary	kytara	k1gFnPc4	kytara
hráli	hrát	k5eAaImAgMnP	hrát
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Paeth	Paeth	k1gMnSc1	Paeth
a	a	k8xC	a
Bruce	Bruce	k1gMnSc1	Bruce
Kulick	Kulick	k1gMnSc1	Kulick
<g/>
,	,	kIx,	,
o	o	k7c6	o
orchestraci	orchestrace	k1gFnSc6	orchestrace
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Rodenberg	Rodenberg	k1gMnSc1	Rodenberg
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jensem	Jens	k1gMnSc7	Jens
Johanssonem	Johansson	k1gMnSc7	Johansson
obsadil	obsadit	k5eAaPmAgMnS	obsadit
také	také	k9	také
post	post	k1gInSc4	post
klávesisty	klávesista	k1gMnSc2	klávesista
<g/>
.	.	kIx.	.
<g/>
Samotné	samotný	k2eAgFnPc1d1	samotná
The	The	k1gFnPc1	The
Wicked	Wicked	k1gInSc1	Wicked
Symphony	Symphona	k1gFnSc2	Symphona
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
a	a	k8xC	a
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
Ö3	Ö3	k1gFnSc6	Ö3
Austria	Austrium	k1gNnSc2	Austrium
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Boxset	Boxset	k1gMnSc1	Boxset
obsahující	obsahující	k2eAgMnSc1d1	obsahující
obě	dva	k4xCgFnPc4	dva
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
žebříčku	žebříček	k1gInSc6	žebříček
Schweizer	Schweizra	k1gFnPc2	Schweizra
Hitparade	Hitparad	k1gInSc5	Hitparad
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vystupování	vystupování	k1gNnSc2	vystupování
na	na	k7c6	na
letních	letní	k2eAgInPc6d1	letní
festivalech	festival	k1gInPc6	festival
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
Sammet	Sammet	k1gInSc1	Sammet
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
projektem	projekt	k1gInSc7	projekt
uspořádat	uspořádat	k5eAaPmF	uspořádat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
čítalo	čítat	k5eAaImAgNnS	čítat
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc1	jedenáct
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
evropských	evropský	k2eAgInPc6d1	evropský
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každé	každý	k3xTgNnSc1	každý
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
hudebníků	hudebník	k1gMnPc2	hudebník
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
koncertní	koncertní	k2eAgFnSc4d1	koncertní
sérii	série	k1gFnSc4	série
probíhající	probíhající	k2eAgFnSc4d1	probíhající
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
výjimky	výjimka	k1gFnPc4	výjimka
stejná	stejná	k1gFnSc1	stejná
jako	jako	k8xC	jako
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
The	The	k1gFnSc3	The
Scarecrow	Scarecrow	k1gFnSc2	Scarecrow
<g/>
;	;	kIx,	;
Kiske	Kisk	k1gFnSc2	Kisk
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Matose	Matosa	k1gFnSc3	Matosa
a	a	k8xC	a
Cloudy	Clouda	k1gFnPc1	Clouda
Yang	Yanga	k1gFnPc2	Yanga
se	se	k3xPyFc4	se
turné	turné	k1gNnSc1	turné
vůbec	vůbec	k9	vůbec
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInSc1d1	další
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
začali	začít	k5eAaPmAgMnP	začít
Sammet	Sammet	k1gMnSc1	Sammet
a	a	k8xC	a
Paeth	Paeth	k1gMnSc1	Paeth
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
šestém	šestý	k4xOgNnSc6	šestý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
Avantasie	Avantasie	k1gFnSc2	Avantasie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
The	The	k1gFnSc2	The
Mystery	Myster	k1gInPc4	Myster
of	of	k?	of
Time	Tim	k1gFnSc2	Tim
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
dalšího	další	k2eAgInSc2d1	další
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
tentokrát	tentokrát	k6eAd1	tentokrát
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
období	období	k1gNnSc6	období
viktoriánském	viktoriánský	k2eAgNnSc6d1	viktoriánské
a	a	k8xC	a
vyprávěl	vyprávět	k5eAaImAgInS	vyprávět
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
vědci	vědec	k1gMnSc6	vědec
Aaronovi	Aaron	k1gMnSc6	Aaron
Blackwellovi	Blackwell	k1gMnSc6	Blackwell
snažícím	snažící	k2eAgFnPc3d1	snažící
se	se	k3xPyFc4	se
zkoumat	zkoumat	k5eAaImF	zkoumat
a	a	k8xC	a
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
časem	čas	k1gInSc7	čas
a	a	k8xC	a
vědou	věda	k1gFnSc7	věda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hostující	hostující	k2eAgMnPc1d1	hostující
zpěváci	zpěvák	k1gMnPc1	zpěvák
se	se	k3xPyFc4	se
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
představili	představit	k5eAaPmAgMnP	představit
Joe	Joe	k1gMnSc1	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turner	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
Kiske	Kiske	k1gFnSc1	Kiske
<g/>
,	,	kIx,	,
Biff	Biff	k1gMnSc1	Biff
Byford	Byford	k1gMnSc1	Byford
<g/>
,	,	kIx,	,
Ronnie	Ronnie	k1gFnSc1	Ronnie
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Catley	Catle	k1gMnPc4	Catle
a	a	k8xC	a
Cloudy	Cloud	k1gMnPc4	Cloud
Yang	Yang	k1gInSc4	Yang
<g/>
,	,	kIx,	,
na	na	k7c4	na
kytary	kytara	k1gFnPc4	kytara
kromě	kromě	k7c2	kromě
Paetha	Paeth	k1gMnSc2	Paeth
hráli	hrát	k5eAaImAgMnP	hrát
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Kulick	Kulick	k1gMnSc1	Kulick
a	a	k8xC	a
Arjen	Arjen	k2eAgInSc1d1	Arjen
Lucassen	Lucassen	k1gInSc1	Lucassen
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInSc1d1	bicí
nahrál	nahrát	k5eAaBmAgInS	nahrát
Russell	Russell	k1gInSc1	Russell
Gilbrook	Gilbrook	k1gInSc1	Gilbrook
a	a	k8xC	a
o	o	k7c4	o
zvuk	zvuk	k1gInSc4	zvuk
kláves	klávesa	k1gFnPc2	klávesa
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
postaral	postarat	k5eAaPmAgMnS	postarat
Rodenberg	Rodenberg	k1gMnSc1	Rodenberg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nahrávání	nahrávání	k1gNnSc6	nahrávání
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
také	také	k9	také
filmový	filmový	k2eAgInSc1d1	filmový
orchestr	orchestr	k1gInSc1	orchestr
Deutsches	Deutsches	k1gMnSc1	Deutsches
Filmorchester	Filmorchester	k1gMnSc1	Filmorchester
Babelsberg	Babelsberg	k1gMnSc1	Babelsberg
<g/>
.	.	kIx.	.
<g/>
The	The	k1gFnSc1	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Time	Tim	k1gInSc2	Tim
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
evropských	evropský	k2eAgFnPc2d1	Evropská
hitparád	hitparáda	k1gFnPc2	hitparáda
umístilo	umístit	k5eAaPmAgNnS	umístit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Finsku	Finsko	k1gNnSc6	Finsko
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avantasia	Avantasia	k1gFnSc1	Avantasia
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
vypravila	vypravit	k5eAaPmAgFnS	vypravit
na	na	k7c4	na
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgNnSc4d2	veliký
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
odehrála	odehrát	k5eAaPmAgFnS	odehrát
přes	přes	k7c4	přes
30	[number]	k4	30
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgInPc1d1	samostatný
koncerty	koncert	k1gInPc1	koncert
opět	opět	k6eAd1	opět
trvaly	trvat	k5eAaImAgInP	trvat
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
byl	být	k5eAaImAgInS	být
koncertní	koncertní	k2eAgInSc1d1	koncertní
set	set	k1gInSc1	set
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
kapely	kapela	k1gFnSc2	kapela
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
postu	post	k1gInSc6	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
;	;	kIx,	;
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Hunecke-Rizzo	Hunecke-Rizza	k1gFnSc5	Hunecke-Rizza
nemohl	moct	k5eNaImAgMnS	moct
turné	turné	k1gNnSc4	turné
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
na	na	k7c4	na
basu	basa	k1gFnSc4	basa
hrál	hrát	k5eAaImAgMnS	hrát
André	André	k1gMnSc1	André
Neygenfind	Neygenfind	k1gMnSc1	Neygenfind
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hostující	hostující	k2eAgMnSc1d1	hostující
zpěváci	zpěvák	k1gMnPc1	zpěvák
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
Kiske	Kiske	k1gFnSc4	Kiske
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Somerville	Somerville	k1gFnSc1	Somerville
<g/>
,	,	kIx,	,
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Rettke	Rettk	k1gFnSc2	Rettk
a	a	k8xC	a
Catley	Catlea	k1gFnSc2	Catlea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
The	The	k1gFnSc6	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Time	Tim	k1gMnSc4	Tim
byl	být	k5eAaImAgMnS	být
Sammet	Sammet	k1gMnSc1	Sammet
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
natočí	natočit	k5eAaBmIp3nS	natočit
také	také	k9	také
sequel	sequet	k5eAaImAgMnS	sequet
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
desce	deska	k1gFnSc6	deska
začalo	začít	k5eAaPmAgNnS	začít
duo	duo	k1gNnSc1	duo
Sammet	Sammeta	k1gFnPc2	Sammeta
a	a	k8xC	a
Paeth	Paetha	k1gFnPc2	Paetha
pracovat	pracovat	k5eAaImF	pracovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgMnSc1	první
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
Space	Space	k1gMnSc2	Space
Police	police	k1gFnSc2	police
<g/>
:	:	kIx,	:
Defenders	Defenders	k1gInSc1	Defenders
of	of	k?	of
the	the	k?	the
Crown	Crown	k1gInSc1	Crown
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
Edguy	Edgua	k1gFnSc2	Edgua
první	první	k4xOgNnPc1	první
nahrávání	nahrávání	k1gNnSc1	nahrávání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
specifická	specifický	k2eAgNnPc4d1	specifické
zvukem	zvuk	k1gInSc7	zvuk
klasického	klasický	k2eAgNnSc2d1	klasické
piana	piano	k1gNnSc2	piano
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
písní	píseň	k1gFnPc2	píseň
<g/>
;	;	kIx,	;
dle	dle	k7c2	dle
Sammeta	Sammeto	k1gNnSc2	Sammeto
"	"	kIx"	"
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
díru	díra	k1gFnSc4	díra
mezi	mezi	k7c7	mezi
kytarami	kytara	k1gFnPc7	kytara
a	a	k8xC	a
multi-zvukovými	multivukový	k2eAgFnPc7d1	multi-zvukový
klávesami	klávesa	k1gFnPc7	klávesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgFnPc3d1	předchozí
nahrávkám	nahrávka	k1gFnPc3	nahrávka
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
metalové	metalový	k2eAgNnSc4d1	metalové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
hudebně	hudebně	k6eAd1	hudebně
podobné	podobný	k2eAgNnSc1d1	podobné
prvním	první	k4xOgMnSc6	první
dvěma	dva	k4xCgFnPc3	dva
deskám	deska	k1gFnPc3	deska
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
úplný	úplný	k2eAgInSc4d1	úplný
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
speed	speed	k1gInSc1	speed
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k1gInSc1	Sammet
totiž	totiž	k9	totiž
zároveň	zároveň	k6eAd1	zároveň
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
použil	použít	k5eAaPmAgInS	použít
na	na	k7c6	na
albech	album	k1gNnPc6	album
The	The	k1gFnSc2	The
Scarecrow	Scarecrow	k1gFnPc1	Scarecrow
a	a	k8xC	a
The	The	k1gFnPc1	The
Wicked	Wicked	k1gInSc1	Wicked
Symphony	Symphona	k1gFnSc2	Symphona
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudebníků	hudebník	k1gMnPc2	hudebník
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
hráli	hrát	k5eAaImAgMnP	hrát
Paeth	Paeth	k1gInSc4	Paeth
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Kulick	Kulick	k1gMnSc1	Kulick
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rodenberg	Rodenberg	k1gInSc1	Rodenberg
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
orchestrace	orchestrace	k1gFnSc1	orchestrace
<g/>
)	)	kIx)	)
a	a	k8xC	a
bicí	bicí	k2eAgInSc1d1	bicí
nahrál	nahrát	k5eAaBmAgInS	nahrát
Bohnke	Bohnke	k1gInSc1	Bohnke
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpěvu	zpěv	k1gInSc6	zpěv
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Sammeta	Sammet	k1gMnSc2	Sammet
podílelo	podílet	k5eAaImAgNnS	podílet
dalších	další	k2eAgNnPc2d1	další
deset	deset	k4xCc1	deset
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
;	;	kIx,	;
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Manson	Manson	k1gMnSc1	Manson
<g/>
,	,	kIx,	,
Dee	Dee	k1gMnSc1	Dee
Snider	Snider	k1gMnSc1	Snider
<g/>
,	,	kIx,	,
Geoff	Geoff	k1gMnSc1	Geoff
Tate	Tate	k1gFnSc1	Tate
<g/>
,	,	kIx,	,
Kiske	Kiske	k1gFnSc1	Kiske
<g/>
,	,	kIx,	,
Herbie	Herbie	k1gFnSc1	Herbie
Langhans	Langhans	k1gInSc1	Langhans
<g/>
,	,	kIx,	,
Sharon	Sharon	k1gInSc1	Sharon
den	den	k1gInSc1	den
Adel	Adel	k1gInSc4	Adel
<g/>
,	,	kIx,	,
Marco	Marco	k1gNnSc4	Marco
Hietala	Hietal	k1gMnSc2	Hietal
a	a	k8xC	a
Catley	Catlea	k1gMnSc2	Catlea
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Ghostlights	Ghostlights	k1gInSc1	Ghostlights
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
Deska	deska	k1gFnSc1	deska
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Avantasii	Avantasie	k1gFnSc4	Avantasie
historický	historický	k2eAgInSc4d1	historický
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
devíti	devět	k4xCc6	devět
žebříčcích	žebříček	k1gInPc6	žebříček
a	a	k8xC	a
kromě	kromě	k7c2	kromě
Evropy	Evropa	k1gFnSc2	Evropa
bodovala	bodovat	k5eAaImAgFnS	bodovat
také	také	k9	také
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
zaměřujícími	zaměřující	k2eAgFnPc7d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
tvrdší	tvrdý	k2eAgFnSc4d2	tvrdší
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
klasické	klasický	k2eAgInPc4d1	klasický
státy	stát	k1gInPc4	stát
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
Ghostlights	Ghostlightsa	k1gFnPc2	Ghostlightsa
umístilo	umístit	k5eAaPmAgNnS	umístit
také	také	k9	také
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
dle	dle	k7c2	dle
hodnocení	hodnocení	k1gNnSc2	hodnocení
IFPI	IFPI	kA	IFPI
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
také	také	k9	také
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
jedenácté	jedenáctý	k4xOgNnSc4	jedenáctý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úvodní	úvodní	k2eAgFnSc7d1	úvodní
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Mystery	Myster	k1gMnPc4	Myster
of	of	k?	of
a	a	k8xC	a
Blood	Blooda	k1gFnPc2	Blooda
Red	Red	k1gMnSc4	Red
Rose	Ros	k1gMnSc4	Ros
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
Avantasia	Avantasia	k1gFnSc1	Avantasia
pokusila	pokusit	k5eAaPmAgFnS	pokusit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
Eurovision	Eurovision	k1gInSc1	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
kola	kolo	k1gNnSc2	kolo
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
období	období	k1gNnSc2	období
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
Ghostlights	Ghostlightsa	k1gFnPc2	Ghostlightsa
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
Avantasia	Avantasium	k1gNnSc2	Avantasium
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jak	jak	k6eAd1	jak
samostatné	samostatný	k2eAgInPc4d1	samostatný
tříhodinové	tříhodinový	k2eAgInPc4d1	tříhodinový
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
samostatných	samostatný	k2eAgInPc2d1	samostatný
koncertů	koncert	k1gInPc2	koncert
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
vyprodaném	vyprodaný	k2eAgNnSc6d1	vyprodané
pražském	pražský	k2eAgNnSc6d1	Pražské
Foru	forum	k1gNnSc6	forum
Karlín	Karlín	k1gInSc1	Karlín
své	svůj	k3xOyFgNnSc4	svůj
vystoupení	vystoupení	k1gNnSc4	vystoupení
zaznamenávala	zaznamenávat	k5eAaImAgFnS	zaznamenávat
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
klasické	klasický	k2eAgFnSc2d1	klasická
sestavy	sestava	k1gFnSc2	sestava
Avantasie	Avantasie	k1gFnSc1	Avantasie
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
zpěváci	zpěvák	k1gMnPc1	zpěvák
Atkins	Atkinsa	k1gFnPc2	Atkinsa
<g/>
,	,	kIx,	,
Catley	Catley	k1gInPc1	Catley
<g/>
,	,	kIx,	,
Kiske	Kisk	k1gInPc1	Kisk
<g/>
,	,	kIx,	,
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
Langhans	Langhans	k1gInSc1	Langhans
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Somerville	Somerville	k1gFnSc1	Somerville
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
slavil	slavit	k5eAaImAgInS	slavit
Sammet	Sammet	k1gInSc4	Sammet
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
z	z	k7c2	z
Edguy	Edgua	k1gFnSc2	Edgua
25	[number]	k4	25
let	léto	k1gNnPc2	léto
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
výročním	výroční	k2eAgNnSc6d1	výroční
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgMnS	odehrát
s	s	k7c7	s
Avantasií	Avantasie	k1gFnSc7	Avantasie
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
tři	tři	k4xCgNnPc1	tři
vystoupení	vystoupení	k1gNnPc1	vystoupení
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedno	jeden	k4xCgNnSc1	jeden
na	na	k7c6	na
festivalu	festival	k1gInSc2	festival
Wacken	Wackna	k1gFnPc2	Wackna
Open	Open	k1gMnSc1	Open
Air	Air	k1gMnSc1	Air
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
krátkého	krátký	k2eAgNnSc2d1	krátké
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
Michael	Michael	k1gMnSc1	Michael
Kiske	Kisk	k1gInSc2	Kisk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Geoff	Geoff	k1gMnSc1	Geoff
Tate	Tat	k1gInSc2	Tat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sammetova	Sammetův	k2eAgFnSc1d1	Sammetův
záchranná	záchranný	k2eAgFnSc1d1	záchranná
brzda	brzda	k1gFnSc1	brzda
(	(	kIx(	(
<g/>
od	od	k7c2	od
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
Edguy	Edgu	k1gMnPc7	Edgu
Sammet	Sammeta	k1gFnPc2	Sammeta
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
Avantasie	Avantasie	k1gFnSc2	Avantasie
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
totiž	totiž	k9	totiž
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
"	"	kIx"	"
<g/>
musel	muset	k5eAaImAgMnS	muset
vyskočit	vyskočit	k5eAaPmF	vyskočit
<g/>
"	"	kIx"	"
z	z	k7c2	z
rozjetého	rozjetý	k2eAgInSc2d1	rozjetý
vlaku	vlak	k1gInSc2	vlak
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
totiž	totiž	k9	totiž
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
kolem	kolem	k6eAd1	kolem
něj	on	k3xPp3gMnSc4	on
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
dělat	dělat	k5eAaImF	dělat
a	a	k8xC	a
pro	pro	k7c4	pro
jakou	jaký	k3yRgFnSc4	jaký
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
kapel	kapela	k1gFnPc2	kapela
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k1gMnSc1	Sammet
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nedělat	dělat	k5eNaImF	dělat
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zatáhl	zatáhnout	k5eAaPmAgMnS	zatáhnout
za	za	k7c4	za
ruční	ruční	k2eAgFnSc4d1	ruční
brzdu	brzda	k1gFnSc4	brzda
<g/>
"	"	kIx"	"
a	a	k8xC	a
pustil	pustit	k5eAaPmAgMnS	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
vlastního	vlastní	k2eAgNnSc2d1	vlastní
hudebního	hudební	k2eAgNnSc2d1	hudební
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgMnS	začít
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
zamýšlená	zamýšlený	k2eAgFnSc1d1	zamýšlená
jako	jako	k8xS	jako
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
ale	ale	k9	ale
vzešel	vzejít	k5eAaPmAgInS	vzejít
nový	nový	k2eAgInSc1d1	nový
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
Avanatasii	Avanatasie	k1gFnSc4	Avanatasie
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
Sammeta	Sammet	k1gMnSc4	Sammet
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
skládání	skládání	k1gNnSc2	skládání
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Birminghamu	Birmingham	k1gInSc6	Birmingham
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
též	též	k9	též
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
pak	pak	k6eAd1	pak
knížky	knížka	k1gFnPc1	knížka
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poa	Poa	k1gMnSc2	Poa
nebo	nebo	k8xC	nebo
Williama	William	k1gMnSc2	William
Somerseta	Somerset	k1gMnSc2	Somerset
Maughama	Maugham	k1gMnSc2	Maugham
<g/>
.	.	kIx.	.
<g/>
Album	album	k1gNnSc1	album
příběhově	příběhově	k6eAd1	příběhově
nenavazuje	navazovat	k5eNaImIp3nS	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnPc4d1	předchozí
dvě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
začíná	začínat	k5eAaImIp3nS	začínat
novou	nový	k2eAgFnSc4d1	nová
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
každá	každý	k3xTgFnSc1	každý
píseň	píseň	k1gFnSc1	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeden	jeden	k4xCgMnSc1	jeden
"	"	kIx"	"
<g/>
psychologický	psychologický	k2eAgInSc1d1	psychologický
příběh	příběh	k1gInSc1	příběh
<g/>
"	"	kIx"	"
o	o	k7c4	o
"	"	kIx"	"
<g/>
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
nedaří	dařit	k5eNaImIp3nS	dařit
najít	najít	k5eAaPmF	najít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
záři	zář	k1gFnSc6	zář
světel	světlo	k1gNnPc2	světlo
a	a	k8xC	a
všeho	všecek	k3xTgNnSc2	všecek
líbivého	líbivý	k2eAgNnSc2d1	líbivé
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
jedinec	jedinec	k1gMnSc1	jedinec
pak	pak	k6eAd1	pak
dle	dle	k7c2	dle
Sammeta	Sammeto	k1gNnSc2	Sammeto
nachází	nacházet	k5eAaImIp3nS	nacházet
únik	únik	k1gInSc1	únik
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
temnoty	temnota	k1gFnSc2	temnota
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
tam	tam	k6eAd1	tam
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gNnSc3	on
běžný	běžný	k2eAgInSc4d1	běžný
svět	svět	k1gInSc4	svět
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dát	dát	k5eAaPmF	dát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sammet	Sammet	k1gInSc1	Sammet
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
"	"	kIx"	"
<g/>
monstrum	monstrum	k1gNnSc4	monstrum
inspirované	inspirovaný	k2eAgNnSc4d1	inspirované
vlastními	vlastní	k2eAgInPc7d1	vlastní
pocity	pocit	k1gInPc7	pocit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
jeho	jeho	k3xOp3gNnSc4	jeho
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
<g/>
Deska	deska	k1gFnSc1	deska
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Moonglow	Moonglow	k1gFnSc2	Moonglow
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
vydání	vydání	k1gNnSc1	vydání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
uvedení	uvedení	k1gNnSc6	uvedení
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
navázat	navázat	k5eAaPmF	navázat
"	"	kIx"	"
<g/>
světové	světový	k2eAgNnSc4d1	světové
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hudebníci	hudebník	k1gMnPc1	hudebník
se	se	k3xPyFc4	se
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
kromě	kromě	k7c2	kromě
Sammeta	Sammeto	k1gNnSc2	Sammeto
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
podíleli	podílet	k5eAaImAgMnP	podílet
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Hartmann	Hartmann	k1gMnSc1	Hartmann
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Rodenberg	Rodenberg	k1gInSc1	Rodenberg
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Felix	Felix	k1gMnSc1	Felix
Bohnke	Bohnk	k1gFnSc2	Bohnk
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hostujícími	hostující	k2eAgMnPc7d1	hostující
zpěváky	zpěvák	k1gMnPc7	zpěvák
jsou	být	k5eAaImIp3nP	být
Mille	Mille	k1gFnSc1	Mille
Petrozza	Petrozza	k1gFnSc1	Petrozza
<g/>
,	,	kIx,	,
Candice	Candice	k1gFnSc1	Candice
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Lande	Land	k1gMnSc5	Land
<g/>
,	,	kIx,	,
Catley	Catley	k1gInPc1	Catley
<g/>
,	,	kIx,	,
Kiske	Kiske	k1gNnPc1	Kiske
<g/>
,	,	kIx,	,
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
Hansi	Hans	k1gMnSc3	Hans
Kürsch	Kürscha	k1gFnPc2	Kürscha
a	a	k8xC	a
Langhans	Langhansa	k1gFnPc2	Langhansa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
propracovalo	propracovat	k5eAaPmAgNnS	propracovat
do	do	k7c2	do
některých	některý	k3yIgMnPc2	některý
hudebních	hudební	k2eAgMnPc2d1	hudební
žebříčků	žebříček	k1gInPc2	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
místo	místo	k1gNnSc1	místo
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
v	v	k7c6	v
pro	pro	k7c4	pro
Sammeta	Sammeta	k1gFnSc1	Sammeta
domácím	domácí	k2eAgNnSc6d1	domácí
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
skončilo	skončit	k5eAaPmAgNnS	skončit
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevilo	objevit	k5eAaPmAgNnS	objevit
ještě	ještě	k9	ještě
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
též	též	k9	též
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pátou	pátý	k4xOgFnSc4	pátý
pozici	pozice	k1gFnSc4	pozice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
deska	deska	k1gFnSc1	deska
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
a	a	k8xC	a
kanadské	kanadský	k2eAgFnSc6d1	kanadská
hitparádě	hitparáda	k1gFnSc6	hitparáda
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc1	první
dvě	dva	k4xCgNnPc1	dva
alba	album	k1gNnPc1	album
vydaná	vydaný	k2eAgNnPc1d1	vydané
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
The	The	k1gFnSc7	The
Metal	metat	k5eAaImAgMnS	metat
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
The	The	k1gFnSc1	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
Part	part	k1gInSc1	part
II	II	kA	II
byla	být	k5eAaImAgFnS	být
hudebně	hudebně	k6eAd1	hudebně
čistou	čistý	k2eAgFnSc7d1	čistá
kombinací	kombinace	k1gFnSc7	kombinace
power	powra	k1gFnPc2	powra
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
speed	speed	k1gInSc4	speed
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
tvořily	tvořit	k5eAaImAgFnP	tvořit
balady	balada	k1gFnPc1	balada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
hrány	hrát	k5eAaImNgFnP	hrát
v	v	k7c6	v
pomalejším	pomalý	k2eAgNnSc6d2	pomalejší
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Sammet	Sammet	k1gMnSc1	Sammet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
celý	celý	k2eAgInSc4d1	celý
projekt	projekt	k1gInSc4	projekt
obnovoval	obnovovat	k5eAaImAgMnS	obnovovat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
nenavazovat	navazovat	k5eNaImF	navazovat
na	na	k7c4	na
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
album	album	k1gNnSc4	album
The	The	k1gMnSc2	The
Scarecrow	Scarecrow	k1gMnSc2	Scarecrow
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
jeho	jeho	k3xOp3gInPc1	jeho
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
žánry	žánr	k1gInPc1	žánr
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
speed	speed	k1gInSc4	speed
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
power	power	k1gInSc1	power
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
hardrockové	hardrockový	k2eAgFnPc1d1	hardrocková
a	a	k8xC	a
rockové	rockový	k2eAgFnPc1d1	rocková
písně	píseň	k1gFnPc1	píseň
či	či	k8xC	či
irské	irský	k2eAgInPc1d1	irský
motivy	motiv	k1gInPc1	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Kontrast	kontrast	k1gInSc1	kontrast
k	k	k7c3	k
rychlým	rychlý	k2eAgFnPc3d1	rychlá
písním	píseň	k1gFnPc3	píseň
s	s	k7c7	s
tvrdými	tvrdý	k2eAgMnPc7d1	tvrdý
kytarovými	kytarový	k2eAgMnPc7d1	kytarový
riffy	riff	k1gMnPc7	riff
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomalejší	pomalý	k2eAgFnPc1d2	pomalejší
rockové	rockový	k2eAgFnPc1d1	rocková
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
blíží	blížit	k5eAaImIp3nS	blížit
až	až	k9	až
k	k	k7c3	k
pop	pop	k1gMnSc1	pop
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
období	období	k1gNnSc6	období
alb	album	k1gNnPc2	album
The	The	k1gFnSc2	The
Scarecrow	Scarecrow	k1gFnSc1	Scarecrow
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Wicked	Wicked	k1gInSc1	Wicked
Symphony	Symphona	k1gFnSc2	Symphona
a	a	k8xC	a
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Babylon	Babylon	k1gInSc1	Babylon
totiž	totiž	k9	totiž
Sammet	Sammet	k1gInSc1	Sammet
celkově	celkově	k6eAd1	celkově
své	svůj	k3xOyFgFnPc4	svůj
skladby	skladba	k1gFnPc4	skladba
zpomaloval	zpomalovat	k5eAaImAgMnS	zpomalovat
a	a	k8xC	a
hudebně	hudebně	k6eAd1	hudebně
se	se	k3xPyFc4	se
přikláněl	přiklánět	k5eAaImAgInS	přiklánět
právě	právě	k9	právě
k	k	k7c3	k
rocku	rock	k1gInSc3	rock
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
také	také	k9	také
celkový	celkový	k2eAgInSc4d1	celkový
zvuk	zvuk	k1gInSc4	zvuk
poslední	poslední	k2eAgFnSc2d1	poslední
zmiňované	zmiňovaný	k2eAgFnSc2d1	zmiňovaná
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
rockově	rockově	k6eAd1	rockově
laděné	laděný	k2eAgFnPc1d1	laděná
kytary	kytara	k1gFnPc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
Sammet	Sammet	k1gInSc1	Sammet
držel	držet	k5eAaImAgInS	držet
i	i	k9	i
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
The	The	k1gFnSc6	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Time	Tim	k1gInSc2	Tim
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k9	ale
textově	textově	k6eAd1	textově
navrátil	navrátit	k5eAaPmAgMnS	navrátit
ke	k	k7c3	k
konceptu	koncept	k1gInSc3	koncept
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
alb	alba	k1gFnPc2	alba
<g/>
;	;	kIx,	;
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
ustálenému	ustálený	k2eAgInSc3d1	ustálený
příběhu	příběh	k1gInSc3	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
s	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
využity	využít	k5eAaPmNgInP	využít
Hammondovy	Hammondův	k2eAgInPc1d1	Hammondův
varhany	varhany	k1gInPc1	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
Ghostlights	Ghostlightsa	k1gFnPc2	Ghostlightsa
vydaná	vydaný	k2eAgFnSc1d1	vydaná
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
kladla	klást	k5eAaImAgFnS	klást
ovšem	ovšem	k9	ovšem
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
metalový	metalový	k2eAgInSc4d1	metalový
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc4	tvrdost
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
objevily	objevit	k5eAaPmAgInP	objevit
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Sammet	Sammet	k1gMnSc1	Sammet
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
předchozích	předchozí	k2eAgNnPc6d1	předchozí
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
dle	dle	k7c2	dle
skladatelova	skladatelův	k2eAgInSc2d1	skladatelův
názoru	názor	k1gInSc2	názor
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xC	jak
variabilní	variabilní	k2eAgFnSc1d1	variabilní
může	moct	k5eAaImIp3nS	moct
Avantasia	Avantasia	k1gFnSc1	Avantasia
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Avantasia	Avantasia	k1gFnSc1	Avantasia
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgInPc4	některý
hudební	hudební	k2eAgInPc4d1	hudební
prvky	prvek	k1gInPc4	prvek
společné	společný	k2eAgInPc4d1	společný
se	se	k3xPyFc4	se
Sammetovou	Sammetový	k2eAgFnSc7d1	Sammetový
další	další	k2eAgFnSc7d1	další
kapelou	kapela	k1gFnSc7	kapela
Edguy	Edgua	k1gFnSc2	Edgua
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
textech	text	k1gInPc6	text
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
Edguy	Edgua	k1gFnSc2	Edgua
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
písně	píseň	k1gFnPc4	píseň
s	s	k7c7	s
humorným	humorný	k2eAgInSc7d1	humorný
podtextem	podtext	k1gInSc7	podtext
a	a	k8xC	a
vtipnými	vtipný	k2eAgFnPc7d1	vtipná
vsuvkami	vsuvka	k1gFnPc7	vsuvka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Avantasii	Avantasie	k1gFnSc6	Avantasie
Sammet	Sammeta	k1gFnPc2	Sammeta
píše	psát	k5eAaImIp3nS	psát
skladby	skladba	k1gFnPc4	skladba
buď	buď	k8xC	buď
zapadající	zapadající	k2eAgFnPc4d1	zapadající
do	do	k7c2	do
konceptuálního	konceptuální	k2eAgInSc2d1	konceptuální
příběhu	příběh	k1gInSc2	příběh
či	či	k8xC	či
texty	text	k1gInPc4	text
myšlené	myšlený	k2eAgInPc4d1	myšlený
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
zpověď	zpověď	k1gFnSc1	zpověď
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
psaná	psaný	k2eAgFnSc1d1	psaná
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
různých	různý	k2eAgFnPc2d1	různá
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
alegorií	alegorie	k1gFnPc2	alegorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Tobias	Tobias	k1gMnSc1	Tobias
Sammet	Sammet	k1gMnSc1	Sammet
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
</s>
</p>
<p>
<s>
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Metal	metal	k1gInSc1	metal
Opera	opera	k1gFnSc1	opera
Part	part	k1gInSc1	part
II	II	kA	II
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Scarecrow	Scarecrow	k1gFnSc1	Scarecrow
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Wicked	Wicked	k1gInSc1	Wicked
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Babylon	Babylon	k1gInSc1	Babylon
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
Time	Tim	k1gInSc2	Tim
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ghostlights	Ghostlights	k1gInSc1	Ghostlights
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moonglow	Moonglow	k?	Moonglow
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Avantasia	Avantasium	k1gNnSc2	Avantasium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
