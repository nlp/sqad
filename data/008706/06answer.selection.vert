<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
většiny	většina	k1gFnSc2	většina
energie	energie	k1gFnSc2	energie
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
