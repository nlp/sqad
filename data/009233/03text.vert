<p>
<s>
Sinusové	sinusový	k2eAgNnSc1d1	sinusové
pravítko	pravítko	k1gNnSc1	pravítko
je	být	k5eAaImIp3nS	být
měřící	měřící	k2eAgFnSc1d1	měřící
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
,	,	kIx,	,
sloužíci	sloužík	k1gMnPc1	sloužík
k	k	k7c3	k
nastavení	nastavení	k1gNnSc3	nastavení
úhlu	úhel	k1gInSc2	úhel
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
vodorovné	vodorovný	k2eAgFnSc3d1	vodorovná
rovině	rovina	k1gFnSc3	rovina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
příměrná	příměrný	k2eAgFnSc1d1	příměrný
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
sinusového	sinusový	k2eAgNnSc2d1	sinusové
pravítka	pravítko	k1gNnSc2	pravítko
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
rovnoběžné	rovnoběžný	k2eAgInPc4d1	rovnoběžný
válce	válec	k1gInPc4	válec
se	s	k7c7	s
stanovenou	stanovený	k2eAgFnSc7d1	stanovená
osovou	osový	k2eAgFnSc7d1	Osová
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
L.	L.	kA	L.
Pokud	pokud	k8xS	pokud
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
válců	válec	k1gInPc2	válec
zvedneme	zvednout	k5eAaPmIp1nP	zvednout
(	(	kIx(	(
<g/>
podložíme	podložit	k5eAaPmIp1nP	podložit
<g/>
)	)	kIx)	)
o	o	k7c6	o
míru	mír	k1gInSc6	mír
x	x	k?	x
<g/>
,	,	kIx,	,
nakloní	naklonit	k5eAaPmIp3nS	naklonit
se	se	k3xPyFc4	se
spojnice	spojnice	k1gFnSc1	spojnice
os	osa	k1gFnPc2	osa
válců	válec	k1gInPc2	válec
o	o	k7c4	o
ůhel	ůhel	k1gInSc4	ůhel
α	α	k?	α
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
úkosu	úkos	k1gInSc2	úkos
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
U	U	kA	U
vypočteme	vypočíst	k5eAaPmIp1nP	vypočíst
jeho	jeho	k3xOp3gInSc4	jeho
úhel	úhel	k1gInSc4	úhel
α	α	k?	α
a	a	k8xC	a
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
stanovíme	stanovit	k5eAaPmIp1nP	stanovit
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
míru	míra	k1gFnSc4	míra
x	x	k?	x
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
L	L	kA	L
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
sin	sin	kA	sin
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
</s>
</p>
<p>
<s>
Rozměr	rozměr	k1gInSc4	rozměr
x	x	k?	x
sestavíme	sestavit	k5eAaPmIp1nP	sestavit
z	z	k7c2	z
koncových	koncový	k2eAgFnPc2d1	koncová
měrek	měrka	k1gFnPc2	měrka
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
podložíme	podložit	k5eAaPmIp1nP	podložit
váleček	váleček	k1gInSc4	váleček
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
kuželovitosti	kuželovitost	k1gFnSc2	kuželovitost
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
sinusové	sinusový	k2eAgNnSc4d1	sinusové
pravítko	pravítko	k1gNnSc4	pravítko
opatřené	opatřený	k2eAgInPc4d1	opatřený
středícími	středící	k2eAgInPc7d1	středící
hroty	hrot	k1gInPc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
kontrolujeme	kontrolovat	k5eAaImIp1nP	kontrolovat
poloviční	poloviční	k2eAgInSc4d1	poloviční
úhel	úhel	k1gInSc4	úhel
α	α	k?	α
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
daný	daný	k2eAgInSc4d1	daný
kuželovitostí	kuželovitost	k1gFnSc7	kuželovitost
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
K.	K.	kA	K.
</s>
</p>
<p>
<s>
Úkos	úkos	k1gInSc4	úkos
i	i	k8xC	i
kuželovitost	kuželovitost	k1gFnSc4	kuželovitost
pak	pak	k6eAd1	pak
kontrolujeme	kontrolovat	k5eAaImIp1nP	kontrolovat
pomocí	pomocí	k7c2	pomocí
úchylkoměru	úchylkoměr	k1gInSc2	úchylkoměr
nebo	nebo	k8xC	nebo
nádrhu	nádrh	k1gInSc2	nádrh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Zkontrolujte	zkontrolovat	k5eAaPmRp2nP	zkontrolovat
úkos	úkos	k1gInSc1	úkos
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
s	s	k7c7	s
tolerancí	tolerance	k1gFnSc7	tolerance
±	±	k?	±
<g/>
2	[number]	k4	2
<g/>
́	́	k?	́
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
80	[number]	k4	80
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
máme	mít	k5eAaImIp1nP	mít
sinusové	sinusový	k2eAgNnSc4d1	sinusové
pravítko	pravítko	k1gNnSc4	pravítko
o	o	k7c6	o
jmenovité	jmenovitý	k2eAgFnSc6d1	jmenovitá
délce	délka	k1gFnSc6	délka
L	L	kA	L
=	=	kIx~	=
200	[number]	k4	200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úkos	úkos	k1gInSc1	úkos
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
představuje	představovat	k5eAaImIp3nS	představovat
úhel	úhel	k1gInSc4	úhel
α	α	k?	α
=	=	kIx~	=
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
38	[number]	k4	38
<g/>
́	́	k?	́
<g/>
12	[number]	k4	12
<g/>
́ ́	́ ́	k?	́ ́
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
x	x	k?	x
=	=	kIx~	=
5,71	[number]	k4	5,71
<g/>
.	.	kIx.	.
</s>
<s>
Tolerance	tolerance	k1gFnSc1	tolerance
±	±	k?	±
<g/>
2	[number]	k4	2
<g/>
́	́	k?	́
<g/>
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
80	[number]	k4	80
<g/>
mm	mm	kA	mm
představuje	představovat	k5eAaImIp3nS	představovat
rozdíl	rozdíl	k1gInSc4	rozdíl
±	±	k?	±
<g/>
0,047	[number]	k4	0,047
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Zkontrolujte	zkontrolovat	k5eAaPmRp2nP	zkontrolovat
kuželovitost	kuželovitost	k1gFnSc4	kuželovitost
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
pomocí	pomocí	k7c2	pomocí
sinusového	sinusový	k2eAgNnSc2d1	sinusové
pravítka	pravítko	k1gNnSc2	pravítko
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
L	L	kA	L
=	=	kIx~	=
200	[number]	k4	200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kuželovitost	kuželovitost	k1gFnSc1	kuželovitost
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
představuje	představovat	k5eAaImIp3nS	představovat
úhel	úhel	k1gInSc4	úhel
α	α	k?	α
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
=	=	kIx~	=
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
23	[number]	k4	23
<g/>
́	́	k?	́
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
́ ́	́ ́	k?	́ ́
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
x	x	k?	x
=	=	kIx~	=
8,325	[number]	k4	8,325
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
