<s>
Je	být	k5eAaImIp3nS	být
sinusové	sinusový	k2eAgNnSc4d1	sinusové
pravítko	pravítko	k1gNnSc4	pravítko
měřící	měřící	k2eAgFnSc1d1	měřící
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
,	,	kIx,	,
sloužíci	sloužík	k1gMnPc1	sloužík
k	k	k7c3	k
nastavení	nastavení	k1gNnSc3	nastavení
úhlu	úhel	k1gInSc2	úhel
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
vodorovné	vodorovný	k2eAgFnSc3d1	vodorovná
rovině	rovina	k1gFnSc3	rovina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
příměrná	příměrný	k2eAgFnSc1d1	příměrný
deska	deska	k1gFnSc1	deska
<g/>
?	?	kIx.	?
</s>
