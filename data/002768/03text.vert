<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
(	(	kIx(	(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
6959	[number]	k4	6959
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
nalézající	nalézající	k2eAgFnSc1d1	nalézající
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
mimo	mimo	k7c4	mimo
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
okolní	okolní	k2eAgInPc4d1	okolní
štíty	štít	k1gInPc4	štít
až	až	k9	až
o	o	k7c4	o
1000	[number]	k4	1000
m.	m.	k?	m.
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
okolí	okolí	k1gNnSc1	okolí
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Aconcagua	Aconcagu	k1gInSc2	Aconcagu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úbočích	úbočí	k1gNnPc6	úbočí
hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Polský	polský	k2eAgInSc1d1	polský
ledovec	ledovec	k1gInSc1	ledovec
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
Anglický	anglický	k2eAgInSc1d1	anglický
ledovec	ledovec	k1gInSc1	ledovec
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hory	hora	k1gFnSc2	hora
pramení	pramenit	k5eAaImIp3nS	pramenit
řeka	řeka	k1gFnSc1	řeka
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odtéká	odtékat	k5eAaImIp3nS	odtékat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Valparaíso	Valparaísa	k1gFnSc5	Valparaísa
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Inků	Ink	k1gMnPc2	Ink
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Anco-cahuac	Ancoahuac	k1gFnSc1	Anco-cahuac
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
v	v	k7c6	v
kečuánštině	kečuánština	k1gFnSc6	kečuánština
znamená	znamenat	k5eAaImIp3nS	znamenat
Bílá	bílý	k2eAgFnSc1d1	bílá
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Inkové	Ink	k1gMnPc1	Ink
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
dokázáno	dokázán	k2eAgNnSc1d1	dokázáno
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesporně	sporně	k6eNd1	sporně
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
vrcholech	vrchol	k1gInPc6	vrchol
And	Anda	k1gFnPc2	Anda
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
jejich	jejich	k3xOp3gInPc1	jejich
stopy	stop	k1gInPc1	stop
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
přes	přes	k7c4	přes
6000	[number]	k4	6000
m.	m.	k?	m.
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
okolí	okolí	k1gNnSc4	okolí
Aconcaguy	Aconcagua	k1gFnSc2	Aconcagua
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
José	Josá	k1gFnSc2	Josá
de	de	k?	de
San	San	k1gMnSc1	San
Martín	Martín	k1gMnSc1	Martín
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
armádou	armáda	k1gFnSc7	armáda
o	o	k7c4	o
5300	[number]	k4	5300
vojácích	voják	k1gMnPc6	voják
<g/>
,	,	kIx,	,
10	[number]	k4	10
600	[number]	k4	600
mulách	mula	k1gFnPc6	mula
a	a	k8xC	a
1600	[number]	k4	1600
koních	koní	k2eAgFnPc2d1	koní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1817	[number]	k4	1817
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
masiv	masiv	k1gInSc4	masiv
And	Anda	k1gFnPc2	Anda
právě	právě	k6eAd1	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
uviděl	uvidět	k5eAaPmAgMnS	uvidět
Acocaguu	Acocagua	k1gFnSc4	Acocagua
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Paul	Paul	k1gMnSc1	Paul
Gussfeldt	Gussfeldt	k1gMnSc1	Gussfeldt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
horolezců	horolezec	k1gMnPc2	horolezec
Německa	Německo	k1gNnSc2	Německo
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
6560	[number]	k4	6560
m.	m.	k?	m.
První	první	k4xOgFnSc1	první
kdo	kdo	k3yQnSc1	kdo
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
samotného	samotný	k2eAgInSc2d1	samotný
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnSc2	hora
byl	být	k5eAaImAgMnS	být
horský	horský	k2eAgMnSc1d1	horský
vůdce	vůdce	k1gMnSc1	vůdce
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
Mathias	Mathias	k1gInSc1	Mathias
Zurbriggen	Zurbriggen	k1gInSc1	Zurbriggen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
expedice	expedice	k1gFnSc2	expedice
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Andy	Anda	k1gFnSc2	Anda
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Chile	Chile	k1gNnPc7	Chile
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
údolími	údolí	k1gNnPc7	údolí
de	de	k?	de
las	laso	k1gNnPc2	laso
Vacas	Vacas	k1gInSc1	Vacas
a	a	k8xC	a
de	de	k?	de
los	los	k1gInSc1	los
Horcones	Horcones	k1gMnSc1	Horcones
Inferior	Inferior	k1gMnSc1	Inferior
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Andesově	Andesův	k2eAgNnSc6d1	Andesův
horském	horský	k2eAgNnSc6d1	horské
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
v	v	k7c6	v
argentinské	argentinský	k2eAgFnSc6d1	Argentinská
provincii	provincie	k1gFnSc6	provincie
Mendoza	Mendoz	k1gMnSc2	Mendoz
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
od	od	k7c2	od
provincie	provincie	k1gFnSc2	provincie
San	San	k1gMnSc1	San
Juan	Juan	k1gMnSc1	Juan
a	a	k8xC	a
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Chile	Chile	k1gNnSc7	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Mendoza	Mendoz	k1gMnSc2	Mendoz
leží	ležet	k5eAaImIp3nS	ležet
112	[number]	k4	112
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Aconcaguy	Aconcagua	k1gFnSc2	Aconcagua
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
tzv.	tzv.	kA	tzv.
skupiny	skupina	k1gFnSc2	skupina
Seven	Seven	k2eAgInSc1d1	Seven
Summits	Summits	k1gInSc1	Summits
(	(	kIx(	(
<g/>
Sedm	sedm	k4xCc4	sedm
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
souřadnice	souřadnice	k1gFnPc1	souřadnice
vrcholu	vrchol	k1gInSc2	vrchol
jsou	být	k5eAaImIp3nP	být
-	-	kIx~	-
<g/>
276	[number]	k4	276
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
70.010	[number]	k4	70.010
<g/>
812	[number]	k4	812
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
odejmutím	odejmutí	k1gNnSc7	odejmutí
plošiny	plošina	k1gFnSc2	plošina
Nazca	Nazc	k1gInSc2	Nazc
Plate	plat	k1gInSc5	plat
pod	pod	k7c7	pod
jihoamerickou	jihoamerický	k2eAgFnSc7d1	jihoamerická
deskou	deska	k1gFnSc7	deska
během	během	k7c2	během
geologicky	geologicky	k6eAd1	geologicky
nedávnou	dávný	k2eNgFnSc7d1	nedávná
Andeanskou	Andeanský	k2eAgFnSc7d1	Andeanský
orogenezí	orogeneze	k1gFnSc7	orogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Aconcagua	Aconcagua	k6eAd1	Aconcagua
není	být	k5eNaImIp3nS	být
vulkán	vulkán	k1gInSc1	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
povolení	povolení	k1gNnSc4	povolení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
700	[number]	k4	700
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc3	vrchol
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
kolem	kolem	k7c2	kolem
3500	[number]	k4	3500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Normální	normální	k2eAgFnSc1d1	normální
trasa	trasa	k1gFnSc1	trasa
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
výstupu	výstup	k1gInSc2	výstup
vede	vést	k5eAaImIp3nS	vést
sutěmi	suť	k1gFnPc7	suť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
žádná	žádný	k3yNgNnPc1	žádný
sněhová	sněhový	k2eAgNnPc1d1	sněhové
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
nějakých	nějaký	k3yIgInPc6	nějaký
úsecích	úsek	k1gInPc6	úsek
je	být	k5eAaImIp3nS	být
nutností	nutnost	k1gFnSc7	nutnost
vybavení	vybavení	k1gNnSc2	vybavení
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
(	(	kIx(	(
<g/>
mačky	mačka	k1gFnPc1	mačka
<g/>
,	,	kIx,	,
cepín	cepín	k1gInSc1	cepín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
umrzlému	umrzlý	k2eAgInSc3d1	umrzlý
terénu	terén	k1gInSc3	terén
apod.	apod.	kA	apod.
Normálka	normálka	k6eAd1	normálka
prochází	procházet	k5eAaImIp3nS	procházet
dolinou	dolina	k1gFnSc7	dolina
Horcones	Horconesa	k1gFnPc2	Horconesa
na	na	k7c4	na
Plaza	plaz	k1gMnSc4	plaz
de	de	k?	de
Mulas	Mulas	k1gMnSc1	Mulas
(	(	kIx(	(
<g/>
4200	[number]	k4	4200
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výstup	výstup	k1gInSc1	výstup
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
hřebeni	hřeben	k1gInSc6	hřeben
a	a	k8xC	a
míjí	míjet	k5eAaImIp3nS	míjet
výškové	výškový	k2eAgInPc4d1	výškový
tábory	tábor	k1gInPc4	tábor
Nido	Nido	k6eAd1	Nido
des	des	k1gNnSc1	des
Condores	Condoresa	k1gFnPc2	Condoresa
(	(	kIx(	(
<g/>
4877	[number]	k4	4877
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
(	(	kIx(	(
<g/>
5950	[number]	k4	5950
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Independencia	Independencia	k1gFnSc1	Independencia
(	(	kIx(	(
<g/>
6546	[number]	k4	6546
m	m	kA	m
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
nejvýše	vysoce	k6eAd3	vysoce
položená	položený	k2eAgFnSc1d1	položená
chata	chata	k1gFnSc1	chata
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
po	po	k7c6	po
Normální	normální	k2eAgFnSc6d1	normální
cestě	cesta	k1gFnSc6	cesta
technicky	technicky	k6eAd1	technicky
poměrně	poměrně	k6eAd1	poměrně
snadnou	snadný	k2eAgFnSc7d1	snadná
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nějakého	nějaký	k3yIgNnSc2	nějaký
skutečně	skutečně	k6eAd1	skutečně
náročného	náročný	k2eAgNnSc2d1	náročné
lezení	lezení	k1gNnSc2	lezení
<g/>
,	,	kIx,	,
obtíží	obtíž	k1gFnPc2	obtíž
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc4d1	nízký
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnPc1	hodnota
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
nižší	nízký	k2eAgInSc1d2	nižší
oproti	oproti	k7c3	oproti
tlaku	tlak	k1gInSc3	tlak
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
Polská	polský	k2eAgFnSc1d1	polská
cesta	cesta	k1gFnSc1	cesta
-	-	kIx~	-
vede	vést	k5eAaImIp3nS	vést
údolím	údolí	k1gNnSc7	údolí
Vacas	Vacasa	k1gFnPc2	Vacasa
do	do	k7c2	do
tábora	tábor	k1gMnSc2	tábor
Plaza	plaz	k1gMnSc2	plaz
Argentina	Argentina	k1gFnSc1	Argentina
umístěném	umístěný	k2eAgInSc6d1	umístěný
východně	východně	k6eAd1	východně
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
stoupání	stoupání	k1gNnSc1	stoupání
Polským	polský	k2eAgInSc7d1	polský
ledovcem	ledovec	k1gInSc7	ledovec
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
až	až	k9	až
na	na	k7c4	na
severní	severní	k2eAgNnSc4d1	severní
žebro	žebro	k1gNnSc4	žebro
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
táborem	tábor	k1gInSc7	tábor
Independencia	Independencium	k1gNnSc2	Independencium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pokračování	pokračování	k1gNnSc1	pokračování
Polské	polský	k2eAgFnSc2d1	polská
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
u	u	k7c2	u
"	"	kIx"	"
<g/>
normálky	normálka	k1gFnSc2	normálka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Náročnější	náročný	k2eAgInSc1d2	náročnější
výstup	výstup	k1gInSc1	výstup
než	než	k8xS	než
normální	normální	k2eAgFnSc1d1	normální
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
schůdná	schůdný	k2eAgFnSc1d1	schůdná
i	i	k9	i
zdatnými	zdatný	k2eAgMnPc7d1	zdatný
a	a	k8xC	a
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
vysokohorskými	vysokohorský	k2eAgMnPc7d1	vysokohorský
turisty	turist	k1gMnPc7	turist
<g/>
.	.	kIx.	.
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
stěna	stěna	k1gFnSc1	stěna
-	-	kIx~	-
Jižní	jižní	k2eAgFnSc1d1	jižní
stěnou	stěna	k1gFnSc7	stěna
vede	vést	k5eAaImIp3nS	vést
několik	několik	k4yIc4	několik
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
náročným	náročný	k2eAgNnPc3d1	náročné
lezením	lezení	k1gNnPc3	lezení
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
strmém	strmý	k2eAgInSc6d1	strmý
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
jižní	jižní	k2eAgFnSc7d1	jižní
stěnou	stěna	k1gFnSc7	stěna
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
v	v	k7c4	v
r.	r.	kA	r.
1954	[number]	k4	1954
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
Paragot	Paragot	k1gInSc1	Paragot
<g/>
,	,	kIx,	,
Poulet	Poulet	k1gInSc1	Poulet
<g/>
,	,	kIx,	,
Dagory	Dagor	k1gInPc1	Dagor
<g/>
,	,	kIx,	,
Berandini	Berandin	k2eAgMnPc1d1	Berandin
<g/>
,	,	kIx,	,
Lesseur	Lesseura	k1gFnPc2	Lesseura
a	a	k8xC	a
Denis	Denisa	k1gFnPc2	Denisa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
výstup	výstup	k1gInSc4	výstup
po	po	k7c4	po
Normal	Normal	k1gInSc4	Normal
route	route	k5eAaPmIp2nP	route
byl	být	k5eAaImAgMnS	být
učiněn	učinit	k5eAaPmNgMnS	učinit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
trval	trvat	k5eAaImAgInS	trvat
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lezců	lezec	k1gMnPc2	lezec
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
tři	tři	k4xCgInPc4	tři
výškové	výškový	k2eAgInPc4d1	výškový
tábory	tábor	k1gInPc4	tábor
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
jim	on	k3xPp3gMnPc3	on
trvá	trvat	k5eAaImIp3nS	trvat
více	hodně	k6eAd2	hodně
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
udáváno	udáván	k2eAgNnSc1d1	udáváno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
klimaticky	klimaticky	k6eAd1	klimaticky
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
nižším	nízký	k2eAgFnPc3d2	nižší
osmitisícovkám	osmitisícovka	k1gFnPc3	osmitisícovka
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
objektivní	objektivní	k2eAgNnSc1d1	objektivní
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
představuje	představovat	k5eAaImIp3nS	představovat
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
studený	studený	k2eAgInSc1d1	studený
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
někdy	někdy	k6eAd1	někdy
rychlosti	rychlost	k1gFnPc4	rychlost
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Viento	Viento	k1gNnSc1	Viento
Blanco	blanco	k2eAgFnPc1d1	blanco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezvětří	bezvětří	k1gNnSc2	bezvětří
a	a	k8xC	a
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
počasí	počasí	k1gNnSc4	počasí
otočit	otočit	k5eAaPmF	otočit
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Viento	Viento	k1gNnSc1	Viento
Blanco	blanco	k6eAd1	blanco
je	být	k5eAaImIp3nS	být
úkaz	úkaz	k1gInSc1	úkaz
známý	známý	k2eAgInSc1d1	známý
právě	právě	k6eAd1	právě
z	z	k7c2	z
Aconcaguy	Aconcagua	k1gFnSc2	Aconcagua
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
velká	velký	k2eAgFnSc1d1	velká
vodní	vodní	k2eAgFnSc1d1	vodní
masa	masa	k1gFnSc1	masa
a	a	k8xC	a
sráží	srážet	k5eAaImIp3nS	srážet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
Aconcaguou	Aconcagua	k1gFnSc7	Aconcagua
s	s	k7c7	s
horkým	horký	k2eAgInSc7d1	horký
vzduchem	vzduch	k1gInSc7	vzduch
z	z	k7c2	z
pamp	pampa	k1gFnPc2	pampa
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
masivu	masiv	k1gInSc2	masiv
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podobný	podobný	k2eAgInSc4d1	podobný
princip	princip	k1gInSc4	princip
jako	jako	k9	jako
u	u	k7c2	u
tornáda	tornádo	k1gNnSc2	tornádo
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
Viento	Viento	k1gNnSc1	Viento
Blanco	blanco	k6eAd1	blanco
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Mračna	mračna	k1gFnSc1	mračna
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nS	hromadit
kolem	kolem	k7c2	kolem
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
rotovat	rotovat	k5eAaImF	rotovat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
natahují	natahovat	k5eAaImIp3nP	natahovat
další	další	k2eAgFnSc1d1	další
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
větrný	větrný	k2eAgInSc1d1	větrný
vír	vír	k1gInSc1	vír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
zároveň	zároveň	k6eAd1	zároveň
prudce	prudko	k6eAd1	prudko
klesá	klesat	k5eAaImIp3nS	klesat
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
hodiny	hodina	k1gFnPc4	hodina
ale	ale	k8xC	ale
i	i	k9	i
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
je	být	k5eAaImIp3nS	být
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
ve	v	k7c6	v
vědeckofantastickém	vědeckofantastický	k2eAgInSc6d1	vědeckofantastický
románu	román	k1gInSc6	román
Roberta	Roberta	k1gFnSc1	Roberta
A.	A.	kA	A.
Heinleina	Heinleina	k1gFnSc1	Heinleina
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
drsná	drsný	k2eAgFnSc1d1	drsná
milenka	milenka	k1gFnSc1	milenka
<g/>
.	.	kIx.	.
</s>
