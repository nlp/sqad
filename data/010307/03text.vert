<p>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
je	být	k5eAaImIp3nS	být
obranná	obranný	k2eAgFnSc1d1	obranná
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
obranných	obranný	k2eAgFnPc2d1	obranná
tvrzí	tvrz	k1gFnPc2	tvrz
<g/>
,	,	kIx,	,
či	či	k8xC	či
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
umělou	umělý	k2eAgFnSc7d1	umělá
(	(	kIx(	(
<g/>
či	či	k8xC	či
využití	využití	k1gNnSc1	využití
přírodní	přírodní	k2eAgFnPc1d1	přírodní
<g/>
)	)	kIx)	)
prohlubní	prohlubeň	k1gFnSc7	prohlubeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zamezit	zamezit	k5eAaPmF	zamezit
přístupu	přístup	k1gInSc3	přístup
útočníků	útočník	k1gMnPc2	útočník
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
většinou	většina	k1gFnSc7	většina
pomocí	pomocí	k7c2	pomocí
padacího	padací	k2eAgInSc2d1	padací
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
vodními	vodní	k2eAgInPc7d1	vodní
příkopy	příkop	k1gInPc7	příkop
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
můžeme	moct	k5eAaImIp1nP	moct
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
jezdectví	jezdectví	k1gNnSc6	jezdectví
či	či	k8xC	či
v	v	k7c6	v
dostihovém	dostihový	k2eAgInSc6d1	dostihový
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgFnPc1	tento
malé	malý	k2eAgFnPc1d1	malá
terénní	terénní	k2eAgFnPc1d1	terénní
stavby	stavba	k1gFnPc1	stavba
slouží	sloužit	k5eAaImIp3nP	sloužit
coby	coby	k?	coby
překážky	překážka	k1gFnPc1	překážka
při	při	k7c6	při
závodech	závod	k1gInPc6	závod
jezdců	jezdec	k1gMnPc2	jezdec
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
v	v	k7c6	v
jezdectví	jezdectví	k1gNnSc6	jezdectví
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dostihovém	dostihový	k2eAgInSc6d1	dostihový
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Příkop	příkop	k1gInSc1	příkop
(	(	kIx(	(
<g/>
válečnictví	válečnictví	k1gNnSc1	válečnictví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgInSc4d1	vodní
příkop	příkop	k1gInSc4	příkop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
