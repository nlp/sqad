<s>
Vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
je	být	k5eAaImIp3nS	být
obranná	obranný	k2eAgFnSc1d1	obranná
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
obranných	obranný	k2eAgFnPc2d1	obranná
tvrzí	tvrz	k1gFnPc2	tvrz
<g/>
,	,	kIx,	,
či	či	k8xC	či
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
