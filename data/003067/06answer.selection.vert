<s>
Britský	britský	k2eAgMnSc1d1	britský
odborník	odborník	k1gMnSc1	odborník
Simon	Simon	k1gMnSc1	Simon
Baron-Cohen	Baron-Cohen	k2eAgMnSc1d1	Baron-Cohen
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
autisté	autista	k1gMnPc1	autista
mají	mít	k5eAaImIp3nP	mít
extrémně	extrémně	k6eAd1	extrémně
mužský	mužský	k2eAgInSc4d1	mužský
typ	typ	k1gInSc4	typ
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hladinou	hladina	k1gFnSc7	hladina
testosteronu	testosteron	k1gInSc2	testosteron
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
matky	matka	k1gFnSc2	matka
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
