<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
vlajka	vlajka	k1gFnSc1	vlajka
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
Radou	rada	k1gFnSc7	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
