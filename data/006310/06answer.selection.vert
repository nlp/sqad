<s>
Schutzstaffel	Schutzstaffel	k1gInSc1	Schutzstaffel
(	(	kIx(	(
<g/>
známější	známý	k2eAgInSc1d2	známější
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SS	SS	kA	SS
nebo	nebo	k8xC	nebo
runami	runa	k1gFnPc7	runa
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ochranný	ochranný	k2eAgInSc4d1	ochranný
oddíl	oddíl	k1gInSc4	oddíl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
organizace	organizace	k1gFnSc1	organizace
NSDAP	NSDAP	kA	NSDAP
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
z	z	k7c2	z
horlivě	horlivě	k6eAd1	horlivě
oddaných	oddaný	k2eAgMnPc2d1	oddaný
přívrženců	přívrženec	k1gMnPc2	přívrženec
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
působících	působící	k2eAgNnPc2d1	působící
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
osobní	osobní	k2eAgFnSc1d1	osobní
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
