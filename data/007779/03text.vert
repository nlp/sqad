<s>
Želva	želva	k1gFnSc1	želva
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
flavomarginata	flavomarginata	k1gFnSc1	flavomarginata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
Cuora	Cuoro	k1gNnSc2	Cuoro
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
želvy	želva	k1gFnPc4	želva
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
želvy	želva	k1gFnSc2	želva
C.	C.	kA	C.
flavomarginata	flavomarginat	k1gMnSc2	flavomarginat
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
schopností	schopnost	k1gFnSc7	schopnost
posunout	posunout	k5eAaPmF	posunout
plastron	plastron	k1gInSc4	plastron
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
karapaxu	karapax	k1gInSc2	karapax
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
vazy	vaz	k1gInPc4	vaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
karapax	karapax	k1gInSc4	karapax
a	a	k8xC	a
plastron	plastron	k1gInSc4	plastron
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoce	vysoce	k6eAd1	vysoce
klenutý	klenutý	k2eAgInSc4d1	klenutý
krunýř	krunýř	k1gInSc4	krunýř
<g/>
.	.	kIx.	.
</s>
<s>
Karapax	Karapax	k1gInSc1	Karapax
a	a	k8xC	a
plastron	plastron	k1gInSc1	plastron
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc4d1	hnědá
barvy	barva	k1gFnPc4	barva
kromě	kromě	k7c2	kromě
krémově	krémově	k6eAd1	krémově
žlutého	žlutý	k2eAgInSc2d1	žlutý
pruhu	pruh	k1gInSc2	pruh
na	na	k7c6	na
obratlovém	obratlový	k2eAgInSc6d1	obratlový
kýlu	kýl	k1gInSc6	kýl
<g/>
.	.	kIx.	.
</s>
<s>
Okraj	okraj	k1gInSc1	okraj
plastronu	plastron	k1gInSc2	plastron
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
světleji	světle	k6eAd2	světle
pigmentovaný	pigmentovaný	k2eAgMnSc1d1	pigmentovaný
kvůli	kvůli	k7c3	kvůli
okrajovým	okrajový	k2eAgInPc3d1	okrajový
štítkům	štítek	k1gInPc3	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
mají	mít	k5eAaImIp3nP	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
široká	široký	k2eAgFnSc1d1	široká
linka	linka	k1gFnSc1	linka
začínající	začínající	k2eAgFnSc1d1	začínající
za	za	k7c7	za
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
mezi	mezi	k7c7	mezi
končetinami	končetina	k1gFnPc7	končetina
má	mít	k5eAaImIp3nS	mít
světlejší	světlý	k2eAgFnSc4d2	světlejší
růžovou	růžový	k2eAgFnSc4d1	růžová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
tlapkách	tlapka	k1gFnPc6	tlapka
mají	mít	k5eAaImIp3nP	mít
pět	pět	k4xCc4	pět
drápů	dráp	k1gInPc2	dráp
<g/>
,	,	kIx,	,
na	na	k7c4	na
zadních	zadní	k2eAgNnPc2d1	zadní
čtyři	čtyři	k4xCgNnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Samce	samec	k1gInPc1	samec
a	a	k8xC	a
samicem	samic	k1gMnSc7	samic
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
všežravce	všežravec	k1gMnPc4	všežravec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
žížalami	žížala	k1gFnPc7	žížala
<g/>
,	,	kIx,	,
larvami	larva	k1gFnPc7	larva
<g/>
,	,	kIx,	,
hlemýždi	hlemýžď	k1gMnPc7	hlemýžď
<g/>
,	,	kIx,	,
slimáky	slimák	k1gMnPc7	slimák
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgFnPc7d1	drobná
rybkami	rybka	k1gFnPc7	rybka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Listovou	listový	k2eAgFnSc4d1	listová
zeleninu	zelenina	k1gFnSc4	zelenina
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
křížil	křížit	k5eAaImAgInS	křížit
s	s	k7c7	s
želvou	želva	k1gFnSc7	želva
japonskou	japonský	k2eAgFnSc7d1	japonská
(	(	kIx(	(
<g/>
Mauremys	Mauremysa	k1gFnPc2	Mauremysa
japonica	japonica	k6eAd1	japonica
<g/>
)	)	kIx)	)
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
s	s	k7c7	s
želvou	želva	k1gFnSc7	želva
okinavskou	okinavský	k2eAgFnSc7d1	okinavský
(	(	kIx(	(
<g/>
Geoemyda	Geoemyda	k1gMnSc1	Geoemyda
japonica	japonica	k1gMnSc1	japonica
<g/>
)	)	kIx)	)
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
i	i	k8xC	i
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
Chu-nan	Chuan	k1gInSc1	Chu-nan
<g/>
,	,	kIx,	,
Che-nan	Chean	k1gInSc1	Che-nan
<g/>
,	,	kIx,	,
An-chuej	Anhuej	k1gInSc1	An-chuej
<g/>
,	,	kIx,	,
Chu-pej	Chuej	k1gFnSc1	Chu-pej
<g/>
,	,	kIx,	,
Čchung-čching	Čchung-čching	k1gInSc1	Čchung-čching
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuan	k1gMnSc1	-čchuan
<g/>
,	,	kIx,	,
Če-ťiang	Če-ťiang	k1gMnSc1	Če-ťiang
a	a	k8xC	a
provincii	provincie	k1gFnSc4	provincie
Ťiang-su	Ťiang	k1gInSc2	Ťiang-s
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
odvodňovacích	odvodňovací	k2eAgInPc2d1	odvodňovací
kanálů	kanál	k1gInPc2	kanál
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
<g/>
,	,	kIx,	,
Ishigaki	Ishigaki	k1gNnSc2	Ishigaki
a	a	k8xC	a
Iriomote	Iriomot	k1gMnSc5	Iriomot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chinese	Chinese	k1gFnSc2	Chinese
box	box	k1gInSc1	box
turtle	turtlat	k5eAaPmIp3nS	turtlat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Želva	želva	k1gFnSc1	želva
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Cuora	Cuora	k1gMnSc1	Cuora
flavomarginata	flavomarginata	k1gFnSc1	flavomarginata
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
