<s>
Želva	želva	k1gFnSc1	želva
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
flavomarginata	flavomarginata	k1gFnSc1	flavomarginata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
Cuora	Cuoro	k1gNnSc2	Cuoro
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
želvy	želva	k1gFnPc4	želva
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
<g/>
.	.	kIx.	.
</s>
