<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
do	do	k7c2	do
následných	následný	k2eAgInPc2d1	následný
stylů	styl	k1gInPc2	styl
<g/>
:	:	kIx,	:
Melodický	melodický	k2eAgInSc1d1	melodický
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
melodeath	melodeath	k1gInSc1	melodeath
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
extrémní	extrémní	k2eAgFnSc1d1	extrémní
forma	forma	k1gFnSc1	forma
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
