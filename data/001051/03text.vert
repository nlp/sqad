<s>
Třeboň	Třeboň	k1gFnSc1	Třeboň
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wittingau	Wittingaa	k1gFnSc4	Wittingaa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Wittingův	Wittingův	k2eAgInSc4d1	Wittingův
či	či	k8xC	či
Vítkův	Vítkův	k2eAgInSc4d1	Vítkův
Luh	luh	k1gInSc4	luh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Trebonis	Trebonis	k1gFnSc1	Trebonis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
22	[number]	k4	22
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Třeboňské	třeboňský	k2eAgFnSc6d1	Třeboňská
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
stoce	stoka	k1gFnSc6	stoka
mezi	mezi	k7c7	mezi
rybníky	rybník	k1gInPc1	rybník
Svět	svět	k1gInSc1	svět
a	a	k8xC	a
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
téměř	téměř	k6eAd1	téměř
8,5	[number]	k4	8,5
tisíce	tisíc	k4xCgInSc2	tisíc
trvale	trvale	k6eAd1	trvale
hlášených	hlášený	k2eAgMnPc2d1	hlášený
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lázeňským	lázeňský	k2eAgMnSc7d1	lázeňský
a	a	k8xC	a
rekreačním	rekreační	k2eAgNnSc7d1	rekreační
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
a	a	k8xC	a
střediskem	středisko	k1gNnSc7	středisko
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
města	město	k1gNnSc2	město
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dvorec	dvorec	k1gInSc1	dvorec
a	a	k8xC	a
poté	poté	k6eAd1	poté
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
(	(	kIx(	(
<g/>
Witingenowe	Witingenowe	k1gInSc1	Witingenowe
<g/>
)	)	kIx)	)
připomínán	připomínat	k5eAaImNgMnS	připomínat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
tříbit	tříbit	k5eAaImF	tříbit
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
třebit	třebit	k5eAaImF	třebit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
šlechtit	šlechtit	k5eAaImF	šlechtit
<g/>
,	,	kIx,	,
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
či	či	k8xC	či
zdokonalovat	zdokonalovat	k5eAaImF	zdokonalovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
vytříbený	vytříbený	k2eAgInSc1d1	vytříbený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tříbila	tříbit	k5eAaImAgFnS	tříbit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
půda	půda	k1gFnSc1	půda
po	po	k7c6	po
vykácených	vykácený	k2eAgInPc6d1	vykácený
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
zalesněné	zalesněný	k2eAgNnSc1d1	zalesněné
a	a	k8xC	a
podmáčené	podmáčený	k2eAgNnSc1d1	podmáčené
(	(	kIx(	(
<g/>
lužní	lužní	k2eAgInPc1d1	lužní
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
osídlení	osídlení	k1gNnPc2	osídlení
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
těžily	těžit	k5eAaImAgInP	těžit
a	a	k8xC	a
zpracovávaly	zpracovávat	k5eAaImAgInP	zpracovávat
stromy	strom	k1gInPc1	strom
<g/>
;	;	kIx,	;
krajina	krajina	k1gFnSc1	krajina
resp.	resp.	kA	resp.
půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
tříbila	tříbit	k5eAaImAgFnS	tříbit
<g/>
/	/	kIx~	/
<g/>
třebila	třebit	k5eAaImAgFnS	třebit
pro	pro	k7c4	pro
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
získala	získat	k5eAaPmAgFnS	získat
jména	jméno	k1gNnSc2	jméno
obce	obec	k1gFnSc2	obec
Třebonín	Třebonín	k1gInSc1	Třebonín
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
Třebichovice	Třebichovice	k1gFnSc1	Třebichovice
<g/>
,	,	kIx,	,
Třebušín	Třebušín	k1gInSc1	Třebušín
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
třebit	třebit	k5eAaImF	třebit
jako	jako	k8xC	jako
kácet	kácet	k5eAaImF	kácet
či	či	k8xC	či
ničit	ničit	k5eAaImF	ničit
les	les	k1gInSc4	les
<g/>
)	)	kIx)	)
1341	[number]	k4	1341
<g/>
:	:	kIx,	:
Třeboň	Třeboň	k1gFnSc1	Třeboň
získala	získat	k5eAaPmAgFnS	získat
statut	statut	k1gInSc4	statut
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1376	[number]	k4	1376
a	a	k8xC	a
1378	[number]	k4	1378
tzv.	tzv.	kA	tzv.
právo	právo	k1gNnSc1	právo
měst	město	k1gNnPc2	město
královských	královský	k2eAgFnPc2d1	královská
a	a	k8xC	a
výsadu	výsada	k1gFnSc4	výsada
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
(	(	kIx(	(
<g/>
Vítek	Vítek	k1gMnSc1	Vítek
z	z	k7c2	z
Prčic	Prčice	k1gFnPc2	Prčice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1366	[number]	k4	1366
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
záštitu	záštita	k1gFnSc4	záštita
bratrů	bratr	k1gMnPc2	bratr
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
svůj	svůj	k3xOyFgInSc4	svůj
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
1367	[number]	k4	1367
<g/>
:	:	kIx,	:
Čtyři	čtyři	k4xCgMnPc1	čtyři
rožmberští	rožmberský	k2eAgMnPc1d1	rožmberský
bratři	bratr	k1gMnPc1	bratr
zakládají	zakládat	k5eAaImIp3nP	zakládat
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
Augustiniánský	augustiniánský	k2eAgInSc4d1	augustiniánský
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
středisko	středisko	k1gNnSc1	středisko
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
1379	[number]	k4	1379
<g/>
:	:	kIx,	:
založen	založen	k2eAgInSc1d1	založen
knížecí	knížecí	k2eAgInSc1d1	knížecí
pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
postaveno	postavit	k5eAaPmNgNnS	postavit
opevnění	opevnění	k1gNnSc2	opevnění
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
osvědčilo	osvědčit	k5eAaPmAgNnS	osvědčit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
Třeboň	Třeboň	k1gFnSc4	Třeboň
v	v	k7c4	v
nedobytnou	dobytný	k2eNgFnSc4d1	nedobytná
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
schopnou	schopný	k2eAgFnSc4d1	schopná
odolat	odolat	k5eAaPmF	odolat
mnohým	mnohý	k2eAgInPc3d1	mnohý
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
Netolický	netolický	k2eAgMnSc1d1	netolický
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zakládá	zakládat	k5eAaImIp3nS	zakládat
první	první	k4xOgInPc4	první
rybníky	rybník	k1gInPc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
navazují	navazovat	k5eAaImIp3nP	navazovat
rybníkáři	rybníkář	k1gMnPc1	rybníkář
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Ruthard	Ruthard	k1gMnSc1	Ruthard
z	z	k7c2	z
Malešova	Malešův	k2eAgInSc2d1	Malešův
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Krčín	Krčín	k1gMnSc1	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
za	za	k7c4	za
majitele	majitel	k1gMnSc4	majitel
Viléma	Vilém	k1gMnSc4	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
třeboňském	třeboňský	k2eAgInSc6d1	třeboňský
zámku	zámek	k1gInSc6	zámek
alchymistickou	alchymistický	k2eAgFnSc4d1	alchymistická
dílnu	dílna	k1gFnSc4	dílna
Edward	Edward	k1gMnSc1	Edward
Kelley	Kellea	k1gFnSc2	Kellea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Viléma	Vilém	k1gMnSc2	Vilém
do	do	k7c2	do
Třeboně	Třeboň	k1gFnSc2	Třeboň
přenesl	přenést	k5eAaPmAgInS	přenést
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
rožmberský	rožmberský	k2eAgMnSc1d1	rožmberský
vladař	vladař	k1gMnSc1	vladař
Petr	Petr	k1gMnSc1	Petr
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1611	[number]	k4	1611
bez	bez	k7c2	bez
dědiců	dědic	k1gMnPc2	dědic
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Švamberkům	Švamberka	k1gMnPc3	Švamberka
a	a	k8xC	a
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c4	na
Bílé	bílý	k2eAgNnSc4d1	bílé
hoře	hoře	k1gNnSc4	hoře
přímo	přímo	k6eAd1	přímo
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
eggenberské	eggenberský	k2eAgNnSc1d1	eggenberský
panství	panství	k1gNnSc1	panství
Třeboň	Třeboň	k1gFnSc1	Třeboň
stalo	stát	k5eAaPmAgNnS	stát
majetkem	majetek	k1gInSc7	majetek
knížecího	knížecí	k2eAgInSc2d1	knížecí
rodu	rod	k1gInSc2	rod
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
spravovali	spravovat	k5eAaImAgMnP	spravovat
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
reformy	reforma	k1gFnSc2	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc4	zámek
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
resp.	resp.	kA	resp.
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
klášter	klášter	k1gInSc1	klášter
řádu	řád	k1gInSc2	řád
augistiniánů	augistinián	k1gMnPc2	augistinián
-	-	kIx~	-
kanovníků	kanovník	k1gMnPc2	kanovník
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgFnPc1d1	klášterní
prostory	prostora	k1gFnPc1	prostora
připadly	připadnout	k5eAaPmAgFnP	připadnout
třeboňskému	třeboňský	k2eAgInSc3d1	třeboňský
děkanátu	děkanát	k1gInSc3	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
katolický	katolický	k2eAgInSc1d1	katolický
vikariát	vikariát	k1gInSc1	vikariát
<g/>
.	.	kIx.	.
</s>
<s>
Třeboňský	třeboňský	k2eAgInSc1d1	třeboňský
vikariát	vikariát	k1gInSc1	vikariát
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
učitel	učitel	k1gMnSc1	učitel
Václav	Václav	k1gMnSc1	Václav
Hucek	Hucek	k1gMnSc1	Hucek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Berta	Berta	k1gFnSc1	Berta
zde	zde	k6eAd1	zde
při	při	k7c6	při
rašeliništi	rašeliniště	k1gNnSc6	rašeliniště
zakládají	zakládat	k5eAaImIp3nP	zakládat
první	první	k4xOgFnPc1	první
slatinné	slatinný	k2eAgFnPc1d1	slatinná
lázně	lázeň	k1gFnPc1	lázeň
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Třeboň	Třeboň	k1gFnSc1	Třeboň
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
jako	jako	k9	jako
plnohodnotné	plnohodnotný	k2eAgNnSc1d1	plnohodnotné
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
politický	politický	k2eAgMnSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
celostátní	celostátní	k2eAgFnSc6d1	celostátní
reorganizaci	reorganizace	k1gFnSc6	reorganizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
Třeboňský	třeboňský	k2eAgInSc1d1	třeboňský
okres	okres	k1gInSc1	okres
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
městu	město	k1gNnSc3	město
udělen	udělen	k2eAgInSc4d1	udělen
lázeňský	lázeňský	k2eAgInSc4d1	lázeňský
statut	statut	k1gInSc4	statut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
s	s	k7c7	s
věžičkou	věžička	k1gFnSc7	věžička
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Na	na	k7c6	na
Sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
-	-	kIx~	-
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
(	(	kIx(	(
<g/>
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
<g/>
)	)	kIx)	)
náměstí	náměstí	k1gNnSc4	náměstí
obsazeno	obsazen	k2eAgNnSc4d1	obsazeno
sovětskými	sovětský	k2eAgInPc7d1	sovětský
okupačními	okupační	k2eAgInPc7d1	okupační
tanky	tank	k1gInPc7	tank
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
památkovou	památkový	k2eAgFnSc7d1	památková
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
navštěvováno	navštěvovat	k5eAaImNgNnS	navštěvovat
turisty	turist	k1gMnPc7	turist
a	a	k8xC	a
lázeňskými	lázeňský	k2eAgMnPc7d1	lázeňský
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
dvojí	dvojí	k4xRgFnPc4	dvojí
lázně	lázeň	k1gFnPc4	lázeň
<g/>
:	:	kIx,	:
Bertiny	Bertin	k2eAgFnPc4d1	Bertina
lázně	lázeň	k1gFnPc4	lázeň
a	a	k8xC	a
Lázně	lázeň	k1gFnSc2	lázeň
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
podnik	podnik	k1gInSc1	podnik
Rybářství	rybářství	k1gNnSc2	rybářství
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
rybářství	rybářství	k1gNnSc1	rybářství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
Regent	regent	k1gMnSc1	regent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rokem	rok	k1gInSc7	rok
založení	založení	k1gNnSc2	založení
1379	[number]	k4	1379
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
pivovary	pivovar	k1gInPc4	pivovar
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pátým	pátý	k4xOgInSc7	pátý
největším	veliký	k2eAgInSc7d3	veliký
areálem	areál	k1gInSc7	areál
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
zámcích	zámek	k1gInPc6	zámek
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
a	a	k8xC	a
v	v	k7c6	v
Jaroměřicích	Jaroměřice	k1gFnPc6	Jaroměřice
nad	nad	k7c7	nad
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
Lednicko-Valtickém	lednickoaltický	k2eAgInSc6d1	lednicko-valtický
areálu	areál	k1gInSc6	areál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
sídlí	sídlet	k5eAaImIp3nS	sídlet
Státní	státní	k2eAgInSc1d1	státní
oblastní	oblastní	k2eAgInSc1d1	oblastní
archiv	archiv	k1gInSc1	archiv
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
Jihočeský	jihočeský	k2eAgInSc4d1	jihočeský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Širší	široký	k2eAgNnSc1d2	širší
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
historickou	historický	k2eAgFnSc4d1	historická
část	část	k1gFnSc4	část
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Třeboň	Třeboň	k1gFnSc1	Třeboň
I	I	kA	I
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
pod	pod	k7c7	pod
Světskou	světský	k2eAgFnSc7d1	světská
hrází	hráz	k1gFnSc7	hráz
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Třeboň	Třeboň	k1gFnSc1	Třeboň
II	II	kA	II
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1	bývalý
augustiniánský	augustiniánský	k2eAgInSc1d1	augustiniánský
klášter	klášter	k1gInSc1	klášter
je	být	k5eAaImIp3nS	být
perlou	perla	k1gFnSc7	perla
jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
Biosférickou	biosférický	k2eAgFnSc7d1	biosférická
rezervací	rezervace	k1gFnSc7	rezervace
Unesco	Unesco	k1gNnSc1	Unesco
<g/>
,	,	kIx,	,
<g/>
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
pečuje	pečovat	k5eAaImIp3nS	pečovat
Ústav	ústav	k1gInSc1	ústav
krajinné	krajinný	k2eAgFnSc2d1	krajinná
ekologie	ekologie	k1gFnSc2	ekologie
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Třeboň	Třeboň	k1gFnSc1	Třeboň
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
okolí	okolí	k1gNnSc1	okolí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
vhodné	vhodný	k2eAgFnPc1d1	vhodná
jak	jak	k8xS	jak
pro	pro	k7c4	pro
pěší	pěší	k2eAgFnSc4d1	pěší
turistiku	turistika	k1gFnSc4	turistika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
relaxační	relaxační	k2eAgFnSc4d1	relaxační
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
a	a	k8xC	a
projížďky	projížďka	k1gFnPc4	projížďka
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Třeboň	Třeboň	k1gFnSc1	Třeboň
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc4	osm
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
šest	šest	k4xCc4	šest
(	(	kIx(	(
<g/>
Branná	branný	k2eAgFnSc1d1	Branná
<g/>
,	,	kIx,	,
Břilice	Břilice	k1gFnPc1	Břilice
<g/>
,	,	kIx,	,
Holičky	holička	k1gFnPc1	holička
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
Přeseka	Přeseka	k1gFnSc1	Přeseka
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Hlína	hlína	k1gFnSc1	hlína
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vesnice	vesnice	k1gFnPc1	vesnice
přičleněné	přičleněný	k2eAgFnPc1d1	přičleněná
k	k	k7c3	k
městu	město	k1gNnSc3	město
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
;	;	kIx,	;
samotná	samotný	k2eAgFnSc1d1	samotná
Třeboň	Třeboň	k1gFnSc1	Třeboň
je	být	k5eAaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
Třeboň	Třeboň	k1gFnSc4	Třeboň
I	i	k9	i
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgNnSc4d1	zahrnující
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
území	území	k1gNnSc4	území
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kopeček	kopeček	k1gInSc4	kopeček
<g/>
.	.	kIx.	.
</s>
<s>
Třeboň	Třeboň	k1gFnSc1	Třeboň
II	II	kA	II
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
jednak	jednak	k8xC	jednak
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
a	a	k8xC	a
jednak	jednak	k8xC	jednak
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
od	od	k7c2	od
Třeboně	Třeboň	k1gFnSc2	Třeboň
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sever	sever	k1gInSc4	sever
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
území	území	k1gNnSc1	území
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
Třeboně	Třeboň	k1gFnSc2	Třeboň
II	II	kA	II
<g/>
,	,	kIx,	,
rozprostřené	rozprostřený	k2eAgFnPc1d1	rozprostřená
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
výpadovky	výpadovka	k1gFnSc2	výpadovka
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
někdejší	někdejší	k2eAgFnSc4d1	někdejší
osadu	osada	k1gFnSc4	osada
Daskabát	Daskabát	k1gInSc4	Daskabát
a	a	k8xC	a
sídliště	sídliště	k1gNnSc4	sídliště
Hliník	hliník	k1gInSc1	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Třeboně	Třeboň	k1gFnSc2	Třeboň
II	II	kA	II
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
za	za	k7c7	za
Pivovarem	pivovar	k1gInSc7	pivovar
Bohemia	bohemia	k1gFnSc1	bohemia
Regent	regent	k1gMnSc1	regent
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Brannou	branný	k2eAgFnSc4d1	Branná
a	a	k8xC	a
Nové	Nové	k2eAgInPc4d1	Nové
Hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
na	na	k7c4	na
Domanín	Domanín	k1gInSc4	Domanín
<g/>
,	,	kIx,	,
Borovany	Borovan	k1gMnPc4	Borovan
a	a	k8xC	a
Trhové	trhový	k2eAgMnPc4d1	trhový
Sviny	Svin	k1gMnPc4	Svin
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
osm	osm	k4xCc1	osm
částí	část	k1gFnPc2	část
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
šesti	šest	k4xCc6	šest
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Branná	branný	k2eAgFnSc1d1	Branná
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Břilice	Břilice	k1gFnSc2	Břilice
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Holičky	holička	k1gFnSc2	holička
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Holičky	holička	k1gFnSc2	holička
u	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
Hlíny	hlína	k1gFnSc2	hlína
<g/>
)	)	kIx)	)
Nová	nový	k2eAgFnSc1d1	nová
Hlína	hlína	k1gFnSc1	hlína
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Holičky	holička	k1gFnSc2	holička
u	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
Hlíny	hlína	k1gFnSc2	hlína
<g/>
)	)	kIx)	)
Přeseka	Přeseek	k1gInSc2	Přeseek
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Stará	starý	k2eAgFnSc1d1	stará
Hlína	hlína	k1gFnSc1	hlína
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Třeboň	Třeboň	k1gFnSc1	Třeboň
I	i	k9	i
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
)	)	kIx)	)
Třeboň	Třeboň	k1gFnSc1	Třeboň
II	II	kA	II
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
rybník	rybník	k1gInSc1	rybník
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stoka	stoka	k1gFnSc1	stoka
Nová	nový	k2eAgFnSc1d1	nová
řeka	řeka	k1gFnSc1	řeka
Třeboňské	třeboňský	k2eAgNnSc1d1	Třeboňské
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
Zámek	zámek	k1gInSc1	zámek
Třeboň	Třeboň	k1gFnSc1	Třeboň
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
kašnou	kašna	k1gFnSc7	kašna
a	a	k8xC	a
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
radnicí	radnice	k1gFnSc7	radnice
<g/>
,	,	kIx,	,
podloubími	podloubí	k1gNnPc7	podloubí
<g/>
,	,	kIx,	,
Mariánským	mariánský	k2eAgInSc7d1	mariánský
sloupem	sloup	k1gInSc7	sloup
a	a	k8xC	a
kamennou	kamenný	k2eAgFnSc7d1	kamenná
kašnou	kašna	k1gFnSc7	kašna
renesanční	renesanční	k2eAgFnSc1d1	renesanční
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
věží	věžit	k5eAaImIp3nS	věžit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
konstruktivistická	konstruktivistický	k2eAgFnSc1d1	konstruktivistická
budova	budova	k1gFnSc1	budova
Spořitelny	spořitelna	k1gFnSc2	spořitelna
na	na	k7c6	na
Masarykové	Masarykové	k2eAgNnSc6d1	Masarykové
náměstí	náměstí	k1gNnSc6	náměstí
hotel	hotel	k1gInSc1	hotel
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInSc1d1	renesanční
dům	dům	k1gInSc1	dům
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
hotel	hotel	k1gInSc4	hotel
Bílý	bílý	k2eAgInSc4d1	bílý
koníček	koníček	k1gInSc4	koníček
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInSc4d1	renesanční
dům	dům	k1gInSc4	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Vratislavský	vratislavský	k2eAgInSc4d1	vratislavský
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
Třeboňské	třeboňský	k2eAgNnSc4d1	Třeboňské
lázeňství	lázeňství	k1gNnSc4	lázeňství
augustiniánský	augustiniánský	k2eAgInSc1d1	augustiniánský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
a	a	k8xC	a
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
a	a	k8xC	a
třeboňskou	třeboňský	k2eAgFnSc7d1	Třeboňská
Madonou	Madona	k1gFnSc7	Madona
a	a	k8xC	a
původně	původně	k6eAd1	původně
také	také	k9	také
s	s	k7c7	s
deskovými	deskový	k2eAgInPc7d1	deskový
obrazy	obraz	k1gInPc7	obraz
Mistra	mistr	k1gMnSc2	mistr
třeboňského	třeboňský	k2eAgInSc2d1	třeboňský
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
vystavenými	vystavený	k2eAgInPc7d1	vystavený
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnPc4	Alžběta
se	s	k7c7	s
hřbitovem	hřbitov	k1gInSc7	hřbitov
<g/>
,	,	kIx,	,
na	na	k7c6	na
Budějovickém	budějovický	k2eAgNnSc6d1	Budějovické
předměstí	předměstí	k1gNnSc6	předměstí
Schwarzenberská	schwarzenberský	k2eAgFnSc1d1	Schwarzenberská
hrobka	hrobka	k1gFnSc1	hrobka
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Domaníně	Domanína	k1gFnSc6	Domanína
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Knížecí	knížecí	k2eAgFnSc1d1	knížecí
pivovar	pivovar	k1gInSc4	pivovar
Regent	regent	k1gMnSc1	regent
(	(	kIx(	(
<g/>
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stylová	stylový	k2eAgFnSc1d1	stylová
restaurace	restaurace	k1gFnSc1	restaurace
hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
v	v	k7c6	v
Domaníně	Domanína	k1gFnSc6	Domanína
<g/>
,	,	kIx,	,
kamenné	kamenný	k2eAgNnSc1d1	kamenné
sousoší	sousoší	k1gNnSc1	sousoší
Anděla	Anděl	k1gMnSc2	Anděl
Strážce	strážce	k1gMnSc2	strážce
v	v	k7c6	v
Třeboňském	třeboňský	k2eAgInSc6d1	třeboňský
parku	park	k1gInSc6	park
Dva	dva	k4xCgInPc4	dva
železné	železný	k2eAgInPc4d1	železný
kříže	kříž	k1gInPc4	kříž
s	s	k7c7	s
Ukřižovaným	ukřižovaný	k2eAgInSc7d1	ukřižovaný
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
na	na	k7c4	na
Nové	Nové	k2eAgInPc4d1	Nové
Hrady	hrad	k1gInPc4	hrad
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
<g />
.	.	kIx.	.
</s>
<s>
finančního	finanční	k2eAgInSc2d1	finanční
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Lesostavby	Lesostavba	k1gFnPc4	Lesostavba
Třeboň	Třeboň	k1gFnSc4	Třeboň
<g/>
)	)	kIx)	)
Třeboňské	třeboňský	k2eAgFnPc1d1	Třeboňská
rybí	rybí	k2eAgFnPc1d1	rybí
sádky	sádka	k1gFnPc1	sádka
s	s	k7c7	s
roubenou	roubený	k2eAgFnSc7d1	roubená
prodejnou	prodejna	k1gFnSc7	prodejna
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
pískovcovou	pískovcový	k2eAgFnSc7d1	pískovcová
sochou	socha	k1gFnSc7	socha
rybáře	rybář	k1gMnSc4	rybář
pod	pod	k7c7	pod
světskou	světský	k2eAgFnSc7d1	světská
hrází	hráz	k1gFnSc7	hráz
Rybník	rybník	k1gInSc1	rybník
Svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
Nevděk	nevděk	k1gInSc1	nevděk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krčínův	Krčínův	k2eAgInSc4d1	Krčínův
a	a	k8xC	a
Šustův	Šustův	k2eAgInSc4d1	Šustův
pomník	pomník	k1gInSc4	pomník
Opatovický	opatovický	k2eAgInSc1d1	opatovický
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
obora	obora	k1gFnSc1	obora
<g/>
,	,	kIx,	,
hřebčín	hřebčín	k1gInSc1	hřebčín
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
Barokní	barokní	k2eAgNnSc4d1	barokní
sousoší	sousoší	k1gNnSc4	sousoší
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Augustin	Augustin	k1gMnSc1	Augustin
<g/>
,	,	kIx,	,
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
<g/>
)	)	kIx)	)
Čtyři	čtyři	k4xCgFnPc1	čtyři
velké	velký	k2eAgFnPc1d1	velká
městské	městský	k2eAgFnPc1d1	městská
brány	brána	k1gFnPc1	brána
(	(	kIx(	(
<g/>
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
<g/>
,	,	kIx,	,
Hradecká	hradecký	k2eAgFnSc1d1	hradecká
<g/>
,	,	kIx,	,
Novohradská	novohradský	k2eAgFnSc1d1	Novohradská
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc1d1	renesanční
Svinenská	svinenský	k2eAgFnSc1d1	Svinenská
brána	brána	k1gFnSc1	brána
s	s	k7c7	s
polychromovanou	polychromovaný	k2eAgFnSc7d1	polychromovaná
<g />
.	.	kIx.	.
</s>
<s>
soškou	soška	k1gFnSc7	soška
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
<g/>
,	,	kIx,	,
obloukovými	obloukový	k2eAgInPc7d1	obloukový
štíty	štít	k1gInPc7	štít
a	a	k8xC	a
sgrafitovou	sgrafitový	k2eAgFnSc4d1	sgrafitová
výzdobu	výzdoba	k1gFnSc4	výzdoba
<g/>
)	)	kIx)	)
Kotěrova	Kotěrův	k2eAgFnSc1d1	Kotěrova
vodárna	vodárna	k1gFnSc1	vodárna
Na	na	k7c6	na
Kopečku	kopeček	k1gInSc6	kopeček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
42	[number]	k4	42
m	m	kA	m
vysoké	vysoký	k2eAgFnSc6d1	vysoká
vodárenské	vodárenský	k2eAgFnSc6d1	vodárenská
věži	věž	k1gFnSc6	věž
umístěna	umístěn	k2eAgFnSc1d1	umístěna
galerie	galerie	k1gFnSc1	galerie
buddhistického	buddhistický	k2eAgNnSc2d1	buddhistické
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Pavla	Pavla	k1gFnSc1	Pavla
Pergola	pergola	k1gFnSc1	pergola
a	a	k8xC	a
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
nástropní	nástropní	k2eAgInSc4d1	nástropní
obraz	obraz	k1gInSc4	obraz
(	(	kIx(	(
<g/>
al	ala	k1gFnPc2	ala
fresco	fresco	k6eAd1	fresco
<g/>
)	)	kIx)	)
předání	předání	k1gNnSc1	předání
ramene	rameno	k1gNnSc2	rameno
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
svatému	svatý	k1gMnSc3	svatý
Václavu	Václav	k1gMnSc3	Václav
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc1d1	blízký
mlýn	mlýn	k1gInSc1	mlýn
a	a	k8xC	a
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
Beseda	beseda	k1gFnSc1	beseda
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
Domek	domek	k1gInSc1	domek
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
u	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
,	,	kIx,	,
lázeňská	lázeňský	k2eAgFnSc1d1	lázeňská
kolonáda	kolonáda	k1gFnSc1	kolonáda
<g/>
,	,	kIx,	,
sousoší	sousoší	k1gNnSc1	sousoší
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g />
.	.	kIx.	.
</s>
<s>
Nepomuckého	Nepomuckého	k2eAgNnPc1d1	Nepomuckého
Historická	historický	k2eAgNnPc1d1	historické
kasárna	kasárna	k1gNnPc1	kasárna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Finanční	finanční	k2eAgInSc1d1	finanční
a	a	k8xC	a
městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
Historická	historický	k2eAgFnSc1d1	historická
Rožmberská	rožmberský	k2eAgFnSc1d1	Rožmberská
ulice	ulice	k1gFnSc1	ulice
s	s	k7c7	s
cukrárnami	cukrárna	k1gFnPc7	cukrárna
<g/>
,	,	kIx,	,
kavárnami	kavárna	k1gFnPc7	kavárna
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc7	hotel
a	a	k8xC	a
restauracemi	restaurace	k1gFnPc7	restaurace
Historická	historický	k2eAgFnSc1d1	historická
obchodní	obchodní	k2eAgFnSc1d1	obchodní
Březanova	Březanův	k2eAgFnSc1d1	Březanova
ulice	ulice	k1gFnSc1	ulice
s	s	k7c7	s
podloubími	podloubí	k1gNnPc7	podloubí
od	od	k7c2	od
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnPc2	Jiljí
a	a	k8xC	a
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g />
.	.	kIx.	.
</s>
<s>
Královny	královna	k1gFnPc1	královna
<g/>
,	,	kIx,	,
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgInSc1d1	bývalý
augustiniánský	augustiniánský	k2eAgInSc1d1	augustiniánský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
gotickými	gotický	k2eAgFnPc7d1	gotická
kaplemi	kaple	k1gFnPc7	kaple
<g/>
,	,	kIx,	,
rajským	rajský	k2eAgInSc7d1	rajský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
,	,	kIx,	,
křížovou	křížový	k2eAgFnSc7d1	křížová
chodbou	chodba	k1gFnSc7	chodba
a	a	k8xC	a
klášterním	klášterní	k2eAgNnSc7d1	klášterní
nádvořím	nádvoří	k1gNnSc7	nádvoří
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
krytý	krytý	k2eAgInSc1d1	krytý
obloukový	obloukový	k2eAgInSc1d1	obloukový
most	most	k1gInSc1	most
s	s	k7c7	s
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
Třeboňského	třeboňský	k2eAgMnSc2d1	třeboňský
augustiniánského	augustiniánský	k2eAgMnSc2d1	augustiniánský
opata	opat	k1gMnSc2	opat
"	"	kIx"	"
<g/>
písmáka	písmák	k1gMnSc2	písmák
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
Kříže	kříž	k1gInSc2	kříž
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
Husova	Husův	k2eAgFnSc1d1	Husova
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
čtvrť	čtvrť	k1gFnSc1	čtvrť
s	s	k7c7	s
Tyršovým	Tyršův	k2eAgInSc7d1	Tyršův
stadionem	stadion	k1gInSc7	stadion
<g/>
,	,	kIx,	,
Tyršův	Tyršův	k2eAgInSc4d1	Tyršův
pomník	pomník	k1gInSc4	pomník
Třeboňský	třeboňský	k2eAgInSc4d1	třeboňský
menhir	menhir	k1gInSc4	menhir
u	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
Boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
datovaná	datovaný	k2eAgFnSc1d1	datovaná
1649	[number]	k4	1649
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kiosku	kiosek	k1gInSc3	kiosek
u	u	k7c2	u
Schwarzenberské	schwarzenberský	k2eAgFnSc2d1	Schwarzenberská
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
,	,	kIx,	,
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
barokní	barokní	k2eAgFnSc2d1	barokní
kaple	kaple	k1gFnSc2	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
při	při	k7c6	při
Pražské	pražský	k2eAgFnSc6d1	Pražská
silnici	silnice	k1gFnSc6	silnice
<g />
.	.	kIx.	.
</s>
<s>
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
a	a	k8xC	a
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
datovaná	datovaný	k2eAgFnSc1d1	datovaná
1747	[number]	k4	1747
<g/>
,	,	kIx,	,
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
Okresní	okresní	k2eAgFnSc2d1	okresní
radnice	radnice	k1gFnSc2	radnice
s	s	k7c7	s
věžičkou	věžička	k1gFnSc7	věžička
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Sadech	sad	k1gInPc6	sad
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
vedle	vedle	k7c2	vedle
pošty	pošta	k1gFnSc2	pošta
<g/>
)	)	kIx)	)
domečková	domečkový	k2eAgFnSc1d1	domečkový
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
datovaná	datovaný	k2eAgFnSc1d1	datovaná
A.d.	A.d.	k1gFnSc1	A.d.
<g/>
:	:	kIx,	:
<g/>
1810	[number]	k4	1810
za	za	k7c7	za
hřbitovem	hřbitov	k1gInSc7	hřbitov
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgInSc4d1	Němcové
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
První	první	k4xOgFnSc2	první
a	a	k8xC	a
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	s	k7c7	s
sousoším	sousoší	k1gNnSc7	sousoší
od	od	k7c2	od
Gabriela	Gabriel	k1gMnSc2	Gabriel
K.	K.	kA	K.
a	a	k8xC	a
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
pamětními	pamětní	k2eAgFnPc7d1	pamětní
deskami	deska	k1gFnPc7	deska
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
padlých	padlý	k1gMnPc2	padlý
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914-1918	[number]	k4	1914-1918
a	a	k8xC	a
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
obnovený	obnovený	k2eAgInSc1d1	obnovený
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
před	před	k7c7	před
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
Sokolské	sokolský	k2eAgFnSc6d1	Sokolská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Sousední	sousední	k2eAgFnSc1d1	sousední
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
barokní	barokní	k2eAgFnSc1d1	barokní
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
proti	proti	k7c3	proti
Budějovické	budějovický	k2eAgFnSc3d1	Budějovická
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zvonku	zvonek	k1gInSc2	zvonek
na	na	k7c6	na
Budějovickém	budějovický	k2eAgNnSc6d1	Budějovické
předměstí	předměstí	k1gNnSc6	předměstí
v	v	k7c6	v
Sokolské	sokolský	k2eAgFnSc6d1	Sokolská
ulici	ulice	k1gFnSc6	ulice
se	s	k7c7	s
sousední	sousední	k2eAgFnSc7d1	sousední
kovárnou	kovárna	k1gFnSc7	kovárna
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
bronzový	bronzový	k2eAgInSc1d1	bronzový
zvon	zvon	k1gInSc1	zvon
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
poschodí	poschodí	k1gNnSc6	poschodí
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
ztratil	ztratit	k5eAaPmAgMnS	ztratit
i	i	k9	i
s	s	k7c7	s
konzolí	konzole	k1gFnSc7	konzole
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
balvan	balvan	k1gInSc1	balvan
inženýra	inženýr	k1gMnSc2	inženýr
Pavla	Pavel	k1gMnSc2	Pavel
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
saních	saně	k1gFnPc6	saně
vedle	vedle	k7c2	vedle
parkoviště	parkoviště	k1gNnSc2	parkoviště
na	na	k7c6	na
trávníku	trávník	k1gInSc6	trávník
před	před	k7c7	před
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
restaurací	restaurace	k1gFnSc7	restaurace
"	"	kIx"	"
<g/>
U	u	k7c2	u
Světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
na	na	k7c6	na
Hliníku	hliník	k1gInSc6	hliník
litinový	litinový	k2eAgInSc4d1	litinový
kříž	kříž	k1gInSc4	kříž
s	s	k7c7	s
"	"	kIx"	"
<g/>
Ukřižovaným	ukřižovaný	k2eAgMnSc7d1	ukřižovaný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
podstavec	podstavec	k1gInSc1	podstavec
datován	datován	k2eAgInSc1d1	datován
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
se	s	k7c7	s
"	"	kIx"	"
<g/>
středověkým	středověký	k2eAgInSc7d1	středověký
<g/>
"	"	kIx"	"
odrazníkem	odrazník	k1gInSc7	odrazník
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
Sokolské	sokolský	k2eAgFnSc2d1	Sokolská
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ulic	ulice	k1gFnPc2	ulice
Chelčického	Chelčický	k2eAgInSc2d1	Chelčický
a	a	k8xC	a
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
domečková	domečkový	k2eAgFnSc1d1	domečkový
sloupková	sloupkový	k2eAgFnSc1d1	sloupková
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
<g/>
,	,	kIx,	,
zabudovaná	zabudovaný	k2eAgFnSc1d1	zabudovaná
do	do	k7c2	do
secesního	secesní	k2eAgInSc2d1	secesní
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
č.	č.	k?	č.
p.	p.	k?	p.
175	[number]	k4	175
<g/>
,	,	kIx,	,
Třeboň	Třeboň	k1gFnSc1	Třeboň
II	II	kA	II
<g/>
)	)	kIx)	)
poblíž	poblíž	k6eAd1	poblíž
ústí	ústit	k5eAaImIp3nS	ústit
Havlíčkovy	Havlíčkův	k2eAgFnPc4d1	Havlíčkova
ulice	ulice	k1gFnPc4	ulice
do	do	k7c2	do
Palackého	Palacký	k1gMnSc2	Palacký
náměstí	náměstí	k1gNnSc2	náměstí
Palackého	Palacký	k1gMnSc2	Palacký
náměstí	náměstí	k1gNnSc2	náměstí
(	(	kIx(	(
<g/>
Na	na	k7c6	na
Hliníku	hliník	k1gInSc6	hliník
<g/>
)	)	kIx)	)
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
kasáren	kasárny	k1gFnPc2	kasárny
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sídlem	sídlo	k1gNnSc7	sídlo
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
z	z	k7c2	z
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
Secesní	secesní	k2eAgFnSc1d1	secesní
budova	budova	k1gFnSc1	budova
finančního	finanční	k2eAgInSc2d1	finanční
úřadu	úřad	k1gInSc2	úřad
ve	v	k7c6	v
Vrchlického	Vrchlického	k2eAgFnSc6d1	Vrchlického
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
sídlo	sídlo	k1gNnSc4	sídlo
katastrálního	katastrální	k2eAgInSc2d1	katastrální
úřadu	úřad	k1gInSc2	úřad
<g/>
)	)	kIx)	)
historický	historický	k2eAgMnSc1d1	historický
renesančně	renesančně	k6eAd1	renesančně
barokní	barokní	k2eAgInSc1d1	barokní
schwarzenberský	schwarzenberský	k2eAgInSc1d1	schwarzenberský
statek	statek	k1gInSc1	statek
Dvory	Dvůr	k1gInPc1	Dvůr
(	(	kIx(	(
<g/>
Dvorce	dvorec	k1gInPc1	dvorec
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
též	též	k9	též
se	s	k7c7	s
stylovou	stylový	k2eAgFnSc7d1	stylová
restaurací	restaurace	k1gFnSc7	restaurace
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
věžičkou	věžička	k1gFnSc7	věžička
(	(	kIx(	(
<g/>
zdaleka	zdaleka	k6eAd1	zdaleka
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
od	od	k7c2	od
rybníka	rybník	k1gInSc2	rybník
Svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
vpravo	vpravo	k6eAd1	vpravo
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
Třeboně	Třeboň	k1gFnSc2	Třeboň
na	na	k7c4	na
Lišov	Lišov	k1gInSc4	Lišov
barokní	barokní	k2eAgInSc1d1	barokní
schwarzenberský	schwarzenberský	k2eAgInSc1d1	schwarzenberský
statek	statek	k1gInSc1	statek
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgFnSc6d1	Jiráskova
ulici	ulice	k1gFnSc6	ulice
s	s	k7c7	s
tělocvičnou	tělocvičný	k2eAgFnSc7d1	Tělocvičná
(	(	kIx(	(
<g/>
sokolovnou	sokolovna	k1gFnSc7	sokolovna
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dopravním	dopravní	k2eAgNnSc7d1	dopravní
hřištěm	hřiště	k1gNnSc7	hřiště
u	u	k7c2	u
Tyršova	Tyršův	k2eAgInSc2d1	Tyršův
stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g />
.	.	kIx.	.
</s>
<s>
Třeboňské	třeboňský	k2eAgInPc1d1	třeboňský
roubené	roubený	k2eAgInPc1d1	roubený
seníky	seník	k1gInPc1	seník
na	na	k7c6	na
Mokrých	Mokrých	k2eAgNnPc6d1	Mokrých
lukách	luka	k1gNnPc6	luka
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
velké	velký	k2eAgInPc1d1	velký
schwarzenberské	schwarzenberský	k2eAgInPc1d1	schwarzenberský
seníky	seník	k1gInPc1	seník
na	na	k7c6	na
stezce	stezka	k1gFnSc6	stezka
"	"	kIx"	"
<g/>
Hrádeček	hrádeček	k1gInSc1	hrádeček
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vyhořel	vyhořet	k5eAaPmAgMnS	vyhořet
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
je	být	k5eAaImIp3nS	být
zrekonstruován	zrekonstruovat	k5eAaPmNgInS	zrekonstruovat
na	na	k7c4	na
penzion	penzion	k1gInSc4	penzion
poblíž	poblíž	k7c2	poblíž
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
za	za	k7c7	za
mostkem	mostek	k1gInSc7	mostek
u	u	k7c2	u
Lesostaveb	Lesostavba	k1gFnPc2	Lesostavba
<g/>
)	)	kIx)	)
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
v	v	k7c6	v
Krčínově	Krčínův	k2eAgFnSc6d1	Krčínova
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
Třeboň	Třeboň	k1gFnSc1	Třeboň
I	i	k8xC	i
čp.	čp.	k?	čp.
50	[number]	k4	50
<g/>
)	)	kIx)	)
rybí	rybí	k2eAgFnSc2d1	rybí
sádky	sádka	k1gFnSc2	sádka
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
rybářské	rybářský	k2eAgFnSc2d1	rybářská
bašty	bašta	k1gFnSc2	bašta
pod	pod	k7c7	pod
rybníkem	rybník	k1gInSc7	rybník
Svět	svět	k1gInSc1	svět
Rybářská	rybářský	k2eAgFnSc1d1	rybářská
bašta	bašta	k1gFnSc1	bašta
(	(	kIx(	(
<g/>
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
)	)	kIx)	)
na	na	k7c6	na
Hliníku	hliník	k1gInSc6	hliník
za	za	k7c7	za
hotelem	hotel	k1gInSc7	hotel
Bohemia	bohemia	k1gFnSc1	bohemia
Tradičně	tradičně	k6eAd1	tradičně
vyniká	vynikat	k5eAaImIp3nS	vynikat
zejména	zejména	k9	zejména
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
figuruje	figurovat	k5eAaImIp3nS	figurovat
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
písni	píseň	k1gFnSc6	píseň
Okolo	okolo	k7c2	okolo
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
Stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
multikulturní	multikulturní	k2eAgInSc1d1	multikulturní
festival	festival	k1gInSc1	festival
Okolo	okolo	k7c2	okolo
Třeboně	Třeboň	k1gFnSc2	Třeboň
se	se	k3xPyFc4	se
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
pořádá	pořádat	k5eAaImIp3nS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
národně	národně	k6eAd1	národně
obrozenecký	obrozenecký	k2eAgInSc1d1	obrozenecký
zpěvácký	zpěvácký	k2eAgInSc1d1	zpěvácký
spolek	spolek	k1gInSc1	spolek
Pěslav	Pěslav	k1gFnSc2	Pěslav
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
navázal	navázat	k5eAaPmAgMnS	navázat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
činný	činný	k2eAgInSc4d1	činný
spolek	spolek	k1gInSc4	spolek
Třeboňští	třeboňský	k2eAgMnPc1d1	třeboňský
pištci	pištec	k1gMnPc1	pištec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
festival	festival	k1gInSc1	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
Třeboňská	třeboňský	k2eAgFnSc1d1	Třeboňská
Nocturna	Nocturna	k1gFnSc1	Nocturna
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
obrazy	obraz	k1gInPc4	obraz
regionálních	regionální	k2eAgMnPc2d1	regionální
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgMnPc7	jaký
byli	být	k5eAaImAgMnP	být
František	František	k1gMnSc1	František
Líbal	Líbal	k1gMnSc1	Líbal
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Volf	Volf	k1gMnSc1	Volf
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
Anifilm	Anifilma	k1gFnPc2	Anifilma
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
festivalu	festival	k1gInSc3	festival
předcházel	předcházet	k5eAaImAgMnS	předcházet
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
AniFest	AniFest	k1gInSc1	AniFest
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
o	o	k7c4	o
festival	festival	k1gInSc4	festival
dělila	dělit	k5eAaImAgFnS	dělit
Třeboň	Třeboň	k1gFnSc1	Třeboň
s	s	k7c7	s
Teplicemi	Teplice	k1gFnPc7	Teplice
<g/>
)	)	kIx)	)
z	z	k7c2	z
Třeboně	Třeboň	k1gFnSc2	Třeboň
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
poté	poté	k6eAd1	poté
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
Anifilmem	Anifilm	k1gInSc7	Anifilm
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgInPc2	každý
14	[number]	k4	14
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
konají	konat	k5eAaImIp3nP	konat
trhy	trh	k1gInPc1	trh
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
farmářskými	farmářský	k2eAgInPc7d1	farmářský
trhy	trh	k1gInPc7	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
rovinnému	rovinný	k2eAgInSc3d1	rovinný
profilu	profil	k1gInSc3	profil
krajiny	krajina	k1gFnSc2	krajina
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
cykloturistika	cykloturistika	k1gFnSc1	cykloturistika
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
masový	masový	k2eAgInSc1d1	masový
příliv	příliv	k1gInSc1	příliv
cykloturistů	cykloturista	k1gMnPc2	cykloturista
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
trasy	trasa	k1gFnPc1	trasa
jsou	být	k5eAaImIp3nP	být
značené	značený	k2eAgFnPc1d1	značená
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
-	-	kIx~	-
neznačené	značený	k2eNgFnPc1d1	neznačená
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
stejně	stejně	k6eAd1	stejně
hezké	hezký	k2eAgNnSc1d1	hezké
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
oblibě	obliba	k1gFnSc3	obliba
cyklistiky	cyklistika	k1gFnSc2	cyklistika
a	a	k8xC	a
cestovnímu	cestovní	k2eAgInSc3d1	cestovní
ruchu	ruch	k1gInSc3	ruch
částečně	částečně	k6eAd1	částečně
postaveném	postavený	k2eAgInSc6d1	postavený
na	na	k7c6	na
cyklistice	cyklistika	k1gFnSc6	cyklistika
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
jen	jen	k9	jen
asi	asi	k9	asi
5,5	[number]	k4	5,5
km	km	kA	km
komunikací	komunikace	k1gFnPc2	komunikace
vyhrazených	vyhrazený	k2eAgInPc2d1	vyhrazený
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
km	km	kA	km
stezek	stezka	k1gFnPc2	stezka
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
a	a	k8xC	a
2,4	[number]	k4	2,4
km	km	kA	km
cyklistických	cyklistický	k2eAgInPc2d1	cyklistický
pruhů	pruh	k1gInPc2	pruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
tedy	tedy	k9	tedy
chybí	chybět	k5eAaImIp3nS	chybět
komunikace	komunikace	k1gFnSc1	komunikace
nebo	nebo	k8xC	nebo
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostor	prostor	k1gInSc1	prostor
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
cyklistický	cyklistický	k2eAgInSc4d1	cyklistický
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
Třeboňsku	Třeboňsko	k1gNnSc6	Třeboňsko
(	(	kIx(	(
<g/>
SO	So	kA	So
ORP	ORP	kA	ORP
Třeboňsko	Třeboňsko	k1gNnSc4	Třeboňsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
takových	takový	k3xDgFnPc2	takový
cest	cesta	k1gFnPc2	cesta
vyhrazených	vyhrazený	k2eAgFnPc2d1	vyhrazená
cyklistům	cyklista	k1gMnPc3	cyklista
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vyvažují	vyvažovat	k5eAaImIp3nP	vyvažovat
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
nová	nový	k2eAgFnSc1d1	nová
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
Třeboň	Třeboň	k1gFnSc1	Třeboň
-	-	kIx~	-
Gmünd	Gmünd	k1gInSc1	Gmünd
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
jezdí	jezdit	k5eAaImIp3nS	jezdit
přes	přes	k7c4	přes
Třeboň	Třeboň	k1gFnSc4	Třeboň
autobusy	autobus	k1gInPc1	autobus
převážející	převážející	k2eAgMnPc1d1	převážející
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cyklobusy	cyklobus	k1gInPc1	cyklobus
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
spoj	spoj	k1gInSc4	spoj
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trasa	trasa	k1gFnSc1	trasa
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
-	-	kIx~	-
Znojmo	Znojmo	k1gNnSc1	Znojmo
-	-	kIx~	-
provozuje	provozovat	k5eAaImIp3nS	provozovat
Jihotrans	Jihotrans	k1gInSc4	Jihotrans
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
Třeboně	Třeboň	k1gFnSc2	Třeboň
nemá	mít	k5eNaImIp3nS	mít
rádo	rád	k2eAgNnSc1d1	rádo
cyklisty	cyklista	k1gMnPc7	cyklista
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
včetně	včetně	k7c2	včetně
cykloturistů	cykloturista	k1gMnPc2	cykloturista
je	být	k5eAaImIp3nS	být
podstatným	podstatný	k2eAgInSc7d1	podstatný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
místních	místní	k2eAgInPc2d1	místní
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kolem	kolem	k7c2	kolem
svátku	svátek	k1gInSc2	svátek
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
vodnická	vodnický	k2eAgFnSc1d1	Vodnická
"	"	kIx"	"
<g/>
Čochtanova	Čochtanův	k2eAgFnSc1d1	Čochtanova
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
"	"	kIx"	"
a	a	k8xC	a
tradiční	tradiční	k2eAgFnPc4d1	tradiční
Myslivecké	myslivecký	k2eAgFnPc4d1	myslivecká
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
Rybářské	rybářský	k2eAgFnPc1d1	rybářská
slavnosti	slavnost	k1gFnPc1	slavnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
web	web	k1gInSc1	web
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Třeboni	Třeboň	k1gFnSc6	Třeboň
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
měsíčně	měsíčně	k6eAd1	měsíčně
vydávaném	vydávaný	k2eAgInSc6d1	vydávaný
magazínu	magazín	k1gInSc6	magazín
Třeboňský	třeboňský	k2eAgInSc4d1	třeboňský
svět	svět	k1gInSc4	svět
(	(	kIx(	(
<g/>
vydává	vydávat	k5eAaImIp3nS	vydávat
Město	město	k1gNnSc1	město
Třeboň	Třeboň	k1gFnSc1	Třeboň
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
s	s	k7c7	s
aktuálním	aktuální	k2eAgInSc7d1	aktuální
kulturním	kulturní	k2eAgInSc7d1	kulturní
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
Netolický	netolický	k2eAgMnSc1d1	netolický
Jakub	Jakub	k1gMnSc1	Jakub
Krčín	Krčín	k1gMnSc1	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
Petr	Petr	k1gMnSc1	Petr
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
Václav	Václav	k1gMnSc1	Václav
Hucek	Hucek	k1gMnSc1	Hucek
Berta	Berta	k1gFnSc1	Berta
Hucková	Hucková	k1gFnSc1	Hucková
Adolf	Adolf	k1gMnSc1	Adolf
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
Vilém	Vilém	k1gMnSc1	Vilém
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hule	hulit	k5eAaImSgInS	hulit
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
Jiří	Jiří	k1gMnSc1	Jiří
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
Horsens	Horsens	k1gInSc4	Horsens
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc4	Dánsko
Interlaken	Interlakna	k1gFnPc2	Interlakna
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Schrems	Schremsa	k1gFnPc2	Schremsa
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Utena	Uten	k1gInSc2	Uten
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
podnět	podnět	k1gInSc1	podnět
k	k	k7c3	k
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
zapsání	zapsání	k1gNnSc3	zapsání
vzácné	vzácný	k2eAgFnSc2d1	vzácná
a	a	k8xC	a
jedinečné	jedinečný	k2eAgFnSc2d1	jedinečná
vodohospodářské	vodohospodářský	k2eAgFnSc2d1	vodohospodářská
soustavy	soustava	k1gFnSc2	soustava
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
