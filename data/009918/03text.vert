<p>
<s>
Jaderné	jaderný	k2eAgNnSc1d1	jaderné
letadlo	letadlo	k1gNnSc1	letadlo
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
let	let	k1gInSc4	let
využívá	využívat	k5eAaPmIp3nS	využívat
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
jaderných	jaderný	k2eAgNnPc2d1	jaderné
letadel	letadlo	k1gNnPc2	letadlo
započal	započnout	k5eAaPmAgInS	započnout
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
probíhal	probíhat	k5eAaImAgInS	probíhat
až	až	k6eAd1	až
do	do	k7c2	do
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kvůli	kvůli	k7c3	kvůli
finanční	finanční	k2eAgFnSc3d1	finanční
náročnosti	náročnost	k1gFnSc3	náročnost
a	a	k8xC	a
nadbytečnosti	nadbytečnost	k1gFnSc3	nadbytečnost
postupně	postupně	k6eAd1	postupně
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
pohonu	pohon	k1gInSc6	pohon
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
operační	operační	k2eAgInSc4d1	operační
dolet	dolet	k1gInSc4	dolet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
technologie	technologie	k1gFnPc1	technologie
mezikontinentálních	mezikontinentální	k2eAgFnPc2d1	mezikontinentální
balistických	balistický	k2eAgFnPc2d1	balistická
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
doplňování	doplňování	k1gNnSc1	doplňování
paliva	palivo	k1gNnSc2	palivo
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
úder	úder	k1gInSc4	úder
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
nepřátelském	přátelský	k2eNgNnSc6d1	nepřátelské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
mocnosti	mocnost	k1gFnPc1	mocnost
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vývoje	vývoj	k1gInSc2	vývoj
jaderných	jaderný	k2eAgNnPc2d1	jaderné
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výzkumu	výzkum	k1gInSc2	výzkum
ale	ale	k8xC	ale
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
dařilo	dařit	k5eAaImAgNnS	dařit
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
problémy	problém	k1gInPc4	problém
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
ozářením	ozáření	k1gNnSc7	ozáření
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
radiačních	radiační	k2eAgInPc2d1	radiační
štítů	štít	k1gInPc2	štít
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
možné	možný	k2eAgNnSc1d1	možné
posádku	posádka	k1gFnSc4	posádka
odstínit	odstínit	k5eAaPmF	odstínit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
rizika	riziko	k1gNnPc4	riziko
byla	být	k5eAaImAgFnS	být
ekologická	ekologický	k2eAgFnSc1d1	ekologická
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
případnou	případný	k2eAgFnSc7d1	případná
havárií	havárie	k1gFnSc7	havárie
letadla	letadlo	k1gNnSc2	letadlo
a	a	k8xC	a
možnou	možný	k2eAgFnSc7d1	možná
kontaminací	kontaminace	k1gFnSc7	kontaminace
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
letadla	letadlo	k1gNnSc2	letadlo
na	na	k7c6	na
nepřátelském	přátelský	k2eNgNnSc6d1	nepřátelské
území	území	k1gNnSc6	území
neslo	nést	k5eAaImAgNnS	nést
také	také	k6eAd1	také
riziko	riziko	k1gNnSc1	riziko
vyzrazení	vyzrazení	k1gNnSc2	vyzrazení
draze	draha	k1gFnSc3	draha
zaplaceného	zaplacený	k2eAgInSc2d1	zaplacený
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
problém	problém	k1gInSc1	problém
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
letadla	letadlo	k1gNnSc2	letadlo
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
dostatečně	dostatečně	k6eAd1	dostatečně
silného	silný	k2eAgInSc2d1	silný
motoru	motor	k1gInSc2	motor
s	s	k7c7	s
potřebným	potřebný	k2eAgInSc7d1	potřebný
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
pohonný	pohonný	k2eAgInSc1d1	pohonný
systém	systém	k1gInSc1	systém
samotný	samotný	k2eAgInSc1d1	samotný
vážil	vážit	k5eAaImAgInS	vážit
okolo	okolo	k7c2	okolo
80	[number]	k4	80
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
několikanásobně	několikanásobně	k6eAd1	několikanásobně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
používané	používaný	k2eAgNnSc4d1	používané
pohonné	pohonný	k2eAgNnSc4d1	pohonné
soustrojí	soustrojí	k1gNnSc4	soustrojí
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
cena	cena	k1gFnSc1	cena
programu	program	k1gInSc2	program
USA	USA	kA	USA
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
24	[number]	k4	24
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
pohonu	pohon	k1gInSc2	pohon
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvětvích	odvětví	k1gNnPc6	odvětví
včetně	včetně	k7c2	včetně
zmenšení	zmenšení	k1gNnSc2	zmenšení
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
,	,	kIx,	,
vylepšení	vylepšení	k1gNnSc3	vylepšení
stínění	stínění	k1gNnPc2	stínění
a	a	k8xC	a
stínících	stínící	k2eAgInPc2d1	stínící
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
vývoji	vývoj	k1gInSc3	vývoj
nových	nový	k2eAgInPc2d1	nový
typů	typ	k1gInPc2	typ
jaderných	jaderný	k2eAgNnPc2d1	jaderné
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
reaktorů	reaktor	k1gInPc2	reaktor
a	a	k8xC	a
řídící	řídící	k2eAgFnSc2d1	řídící
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jaderný	jaderný	k2eAgInSc1d1	jaderný
pohon	pohon	k1gInSc1	pohon
letadel	letadlo	k1gNnPc2	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
letadel	letadlo	k1gNnPc2	letadlo
byly	být	k5eAaImAgInP	být
vyvíjeny	vyvíjen	k2eAgInPc1d1	vyvíjen
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jednodušším	jednoduchý	k2eAgInSc7d2	jednodušší
byl	být	k5eAaImAgInS	být
pohon	pohon	k1gInSc4	pohon
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
druhým	druhý	k4xOgNnSc7	druhý
pohon	pohon	k1gInSc4	pohon
s	s	k7c7	s
nepřímým	přímý	k2eNgInSc7d1	nepřímý
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
podobný	podobný	k2eAgInSc4d1	podobný
klasickému	klasický	k2eAgInSc3d1	klasický
proudovému	proudový	k2eAgInSc3d1	proudový
motoru	motor	k1gInSc3	motor
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
před	před	k7c7	před
expanzí	expanze	k1gFnSc7	expanze
v	v	k7c6	v
turbíně	turbína	k1gFnSc6	turbína
neohřívá	ohřívat	k5eNaImIp3nS	ohřívat
zažehnutím	zažehnutí	k1gNnSc7	zažehnutí
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
energií	energie	k1gFnSc7	energie
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
kompresi	komprese	k1gFnSc6	komprese
a	a	k8xC	a
nasávání	nasávání	k1gNnSc6	nasávání
vzduchu	vzduch	k1gInSc2	vzduch
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
proudí	proudit	k5eAaImIp3nS	proudit
přes	přes	k7c4	přes
turbínu	turbína	k1gFnSc4	turbína
do	do	k7c2	do
výstupní	výstupní	k2eAgFnSc2d1	výstupní
části	část	k1gFnSc2	část
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
pohonu	pohon	k1gInSc2	pohon
je	být	k5eAaImIp3nS	být
vypouštění	vypouštění	k1gNnSc4	vypouštění
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímý	přímý	k2eNgInSc1d1	nepřímý
cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
reaktorovou	reaktorový	k2eAgFnSc7d1	reaktorová
částí	část	k1gFnSc7	část
a	a	k8xC	a
pohonnou	pohonný	k2eAgFnSc7d1	pohonná
jednotkou	jednotka	k1gFnSc7	jednotka
vložen	vložen	k2eAgInSc1d1	vložen
tepelný	tepelný	k2eAgInSc1d1	tepelný
výměník	výměník	k1gInSc1	výměník
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vypouštěním	vypouštění	k1gNnSc7	vypouštění
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
odpadají	odpadat	k5eAaImIp3nP	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
těžší	těžký	k2eAgNnSc4d2	těžší
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
náročnější	náročný	k2eAgMnSc1d2	náročnější
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
ani	ani	k8xC	ani
prototyp	prototyp	k1gInSc1	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
otestovaly	otestovat	k5eAaPmAgInP	otestovat
reaktory	reaktor	k1gInPc1	reaktor
s	s	k7c7	s
keramickým	keramický	k2eAgNnSc7d1	keramické
palivem	palivo	k1gNnSc7	palivo
moderovaným	moderovaný	k2eAgInSc7d1	moderovaný
lehkou	lehký	k2eAgFnSc7d1	lehká
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
hydridy	hydrid	k1gInPc1	hydrid
a	a	k8xC	a
chlazené	chlazený	k2eAgFnPc1d1	chlazená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
také	také	k9	také
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
pohonného	pohonný	k2eAgInSc2d1	pohonný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systémy	systém	k1gInPc1	systém
HTRE	HTRE	kA	HTRE
===	===	k?	===
</s>
</p>
<p>
<s>
Systémy	systém	k1gInPc1	systém
HTRE	HTRE	kA	HTRE
(	(	kIx(	(
<g/>
Heat	Heat	k2eAgInSc1d1	Heat
Transfer	transfer	k1gInSc1	transfer
Reactor	Reactor	k1gMnSc1	Reactor
Experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
experimentální	experimentální	k2eAgInPc4d1	experimentální
pohonné	pohonný	k2eAgInPc4d1	pohonný
systémy	systém	k1gInPc4	systém
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
komponent	komponenta	k1gFnPc2	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
byly	být	k5eAaImAgInP	být
odzkoušeny	odzkoušen	k2eAgInPc1d1	odzkoušen
tři	tři	k4xCgInPc1	tři
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
systému	systém	k1gInSc2	systém
HTRE-1	HTRE-1	k1gMnPc2	HTRE-1
byl	být	k5eAaImAgInS	být
moderovaný	moderovaný	k2eAgInSc1d1	moderovaný
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
chlazený	chlazený	k2eAgMnSc1d1	chlazený
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Strukturální	strukturální	k2eAgFnPc1d1	strukturální
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
chlazené	chlazený	k2eAgInPc1d1	chlazený
vodou	voda	k1gFnSc7	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
okolo	okolo	k7c2	okolo
70	[number]	k4	70
°	°	k?	°
<g/>
C.	C.	kA	C.
Vzduch	vzduch	k1gInSc1	vzduch
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
teplot	teplota	k1gFnPc2	teplota
přes	přes	k7c4	přes
700	[number]	k4	700
°	°	k?	°
<g/>
C.	C.	kA	C.
Palivo	palivo	k1gNnSc1	palivo
bylo	být	k5eAaImAgNnS	být
disperzního	disperzní	k2eAgInSc2d1	disperzní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uraničitý	uraničitý	k2eAgInSc1d1	uraničitý
byl	být	k5eAaImAgInS	být
rozptýlen	rozptýlen	k2eAgInSc1d1	rozptýlen
v	v	k7c6	v
chrom-niklové	chromiklový	k2eAgFnSc6d1	chrom-niklový
oceli	ocel	k1gFnSc6	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
soubor	soubor	k1gInSc1	soubor
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
koncentrických	koncentrický	k2eAgInPc2d1	koncentrický
plátů	plát	k1gInPc2	plát
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
reaktorová	reaktorový	k2eAgFnSc1d1	reaktorová
nádoba	nádoba	k1gFnSc1	nádoba
válcového	válcový	k2eAgInSc2d1	válcový
tvaru	tvar	k1gInSc2	tvar
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
37	[number]	k4	37
otvorů	otvor	k1gInPc2	otvor
s	s	k7c7	s
palivovými	palivový	k2eAgInPc7d1	palivový
soubory	soubor	k1gInPc7	soubor
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
proudil	proudit	k5eAaImAgInS	proudit
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
HTRE-1	HTRE-1	k4	HTRE-1
byl	být	k5eAaImAgMnS	být
první	první	k4xOgInSc4	první
letecký	letecký	k2eAgInSc4d1	letecký
pohonný	pohonný	k2eAgInSc4d1	pohonný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
základech	základ	k1gInPc6	základ
systému	systém	k1gInSc2	systém
HTRE-1	HTRE-1	k1gMnPc2	HTRE-1
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
i	i	k9	i
systémy	systém	k1gInPc4	systém
HTRE-2	HTRE-2	k1gMnPc2	HTRE-2
a	a	k8xC	a
HTRE-	HTRE-	k1gMnPc2	HTRE-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
systému	systém	k1gInSc2	systém
HTRE-	HTRE-	k1gFnSc2	HTRE-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
reaktoru	reaktor	k1gInSc3	reaktor
použitému	použitý	k2eAgInSc3d1	použitý
u	u	k7c2	u
HTRE-	HTRE-	k1gFnSc2	HTRE-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
především	především	k9	především
k	k	k7c3	k
testování	testování	k1gNnSc3	testování
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
moderátorů	moderátor	k1gMnPc2	moderátor
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
HTRE-3	HTRE-3	k1gFnSc2	HTRE-3
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
typů	typ	k1gInPc2	typ
moderovaný	moderovaný	k2eAgInSc1d1	moderovaný
hydridem	hydrid	k1gInSc7	hydrid
zirkonia	zirkonium	k1gNnSc2	zirkonium
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
palivo	palivo	k1gNnSc1	palivo
bylo	být	k5eAaImAgNnS	být
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
jako	jako	k8xS	jako
v	v	k7c6	v
HTRE-	HTRE-	k1gFnSc6	HTRE-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
konstrukčními	konstrukční	k2eAgFnPc7d1	konstrukční
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgInPc3d1	předchozí
typům	typ	k1gInPc3	typ
systému	systém	k1gInSc2	systém
HTRE	HTRE	kA	HTRE
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
reaktor	reaktor	k1gInSc1	reaktor
systému	systém	k1gInSc2	systém
HTRE-3	HTRE-3	k1gFnSc2	HTRE-3
byl	být	k5eAaImAgInS	být
usazen	usadit	k5eAaPmNgInS	usadit
horizontálně	horizontálně	k6eAd1	horizontálně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
co	co	k9	co
nejsnáze	snadno	k6eAd3	snadno
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
rámu	rám	k1gInSc2	rám
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reaktor	reaktor	k1gInSc1	reaktor
ARE	ar	k1gInSc5	ar
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
letadel	letadlo	k1gNnPc2	letadlo
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
malým	malý	k2eAgInPc3d1	malý
rozměrům	rozměr	k1gInPc3	rozměr
letadla	letadlo	k1gNnSc2	letadlo
a	a	k8xC	a
omezené	omezený	k2eAgFnSc2d1	omezená
nosnosti	nosnost	k1gFnSc2	nosnost
důležitý	důležitý	k2eAgInSc1d1	důležitý
parametr	parametr	k1gInSc1	parametr
hustoty	hustota	k1gFnSc2	hustota
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výborných	výborný	k2eAgFnPc2d1	výborná
hodnot	hodnota	k1gFnPc2	hodnota
reaktory	reaktor	k1gInPc1	reaktor
s	s	k7c7	s
tavenými	tavený	k2eAgFnPc7d1	tavená
solemi	sůl	k1gFnPc7	sůl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
i	i	k9	i
do	do	k7c2	do
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
generace	generace	k1gFnSc2	generace
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
ARE	ar	k1gInSc5	ar
(	(	kIx(	(
<g/>
Aircraft	Aircraft	k2eAgInSc1d1	Aircraft
Reactor	Reactor	k1gInSc1	Reactor
Experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
tepelném	tepelný	k2eAgInSc6d1	tepelný
výkonu	výkon	k1gInSc6	výkon
2,5	[number]	k4	2,5
MW	MW	kA	MW
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
fluoridovou	fluoridový	k2eAgFnSc4d1	fluoridová
sůl	sůl	k1gFnSc4	sůl
NaF-ZrF	NaF-ZrF	k1gFnSc1	NaF-ZrF
<g/>
4	[number]	k4	4
<g/>
-UF	-UF	k?	-UF
<g/>
4	[number]	k4	4
a	a	k8xC	a
jako	jako	k9	jako
moderátor	moderátor	k1gInSc1	moderátor
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
oxid	oxid	k1gInSc1	oxid
berylnatý	berylnatý	k2eAgInSc1d1	berylnatý
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
projektovaná	projektovaný	k2eAgFnSc1d1	projektovaná
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
820	[number]	k4	820
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
otestován	otestován	k2eAgInSc1d1	otestován
reaktor	reaktor	k1gInSc1	reaktor
společnosti	společnost	k1gFnSc2	společnost
Pratt	Pratt	k1gMnSc1	Pratt
&	&	k?	&
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
,	,	kIx,	,
také	také	k9	také
s	s	k7c7	s
kapalným	kapalný	k2eAgNnSc7d1	kapalné
palivem	palivo	k1gNnSc7	palivo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tavených	tavený	k2eAgFnPc2d1	tavená
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
NEPA	NEPA	kA	NEPA
===	===	k?	===
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
programu	program	k1gInSc2	program
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc1	projekt
NEPA	NEPA	kA	NEPA
(	(	kIx(	(
<g/>
Nuclear	Nuclear	k1gMnSc1	Nuclear
Energy	Energ	k1gInPc4	Energ
for	forum	k1gNnPc2	forum
the	the	k?	the
Propulsion	Propulsion	k1gInSc1	Propulsion
of	of	k?	of
Aircraft	Aircraft	k1gInSc1	Aircraft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
využitelnost	využitelnost	k1gFnSc1	využitelnost
a	a	k8xC	a
proveditelnost	proveditelnost	k1gFnSc1	proveditelnost
jaderného	jaderný	k2eAgInSc2d1	jaderný
pohonu	pohon	k1gInSc2	pohon
pro	pro	k7c4	pro
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejrychlejšího	rychlý	k2eAgNnSc2d3	nejrychlejší
vyvinutí	vyvinutí	k1gNnSc2	vyvinutí
funkčního	funkční	k2eAgInSc2d1	funkční
reaktoru	reaktor	k1gInSc2	reaktor
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
letadla	letadlo	k1gNnSc2	letadlo
bude	být	k5eAaImBp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
pohonem	pohon	k1gInSc7	pohon
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
<g/>
,	,	kIx,	,
vzduchem	vzduch	k1gInSc7	vzduch
chlazeným	chlazený	k2eAgInSc7d1	chlazený
a	a	k8xC	a
vodíkem	vodík	k1gInSc7	vodík
moderovaným	moderovaný	k2eAgInSc7d1	moderovaný
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Stínění	stínění	k1gNnSc1	stínění
posádky	posádka	k1gFnSc2	posádka
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
řešilo	řešit	k5eAaImAgNnS	řešit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
reaktoru	reaktor	k1gInSc2	reaktor
samotného	samotný	k2eAgMnSc2d1	samotný
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stínění	stínění	k1gNnSc1	stínění
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
byla	být	k5eAaImAgFnS	být
okolo	okolo	k7c2	okolo
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
okolo	okolo	k7c2	okolo
kabiny	kabina	k1gFnSc2	kabina
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Stínění	stínění	k1gNnSc1	stínění
bylo	být	k5eAaImAgNnS	být
nejtlustší	tlustý	k2eAgNnSc1d3	nejtlustší
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
reaktoru	reaktor	k1gInSc2	reaktor
k	k	k7c3	k
posádce	posádka	k1gFnSc3	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
letadla	letadlo	k1gNnSc2	letadlo
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
potřebné	potřebný	k2eAgNnSc1d1	potřebné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
NEPA	NEPA	kA	NEPA
převeden	převést	k5eAaPmNgInS	převést
pod	pod	k7c7	pod
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
projekt	projekt	k1gInSc4	projekt
ANP	ANP	kA	ANP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
ANP	ANP	kA	ANP
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
ANP	ANP	kA	ANP
((	((	k?	((
<g/>
Advanced	Advanced	k1gMnSc1	Advanced
Nuclear	Nuclear	k1gMnSc1	Nuclear
Propulsion	Propulsion	k1gInSc1	Propulsion
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vývoje	vývoj	k1gInSc2	vývoj
pohonných	pohonný	k2eAgFnPc2d1	pohonná
jednotek	jednotka	k1gFnPc2	jednotka
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vydaly	vydat	k5eAaPmAgInP	vydat
dvěma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
společnost	společnost	k1gFnSc1	společnost
General	General	k1gFnSc2	General
Electric	Electrice	k1gFnPc2	Electrice
a	a	k8xC	a
motor	motor	k1gInSc1	motor
s	s	k7c7	s
nepřímým	přímý	k2eNgInSc7d1	nepřímý
cyklem	cyklus	k1gInSc7	cyklus
společnost	společnost	k1gFnSc1	společnost
Pratt	Pratt	k1gMnSc1	Pratt
&	&	k?	&
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
série	série	k1gFnSc1	série
testů	test	k1gInPc2	test
systému	systém	k1gInSc2	systém
HTRE	HTRE	kA	HTRE
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
celého	celý	k2eAgInSc2d1	celý
programu	program	k1gInSc2	program
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
dva	dva	k4xCgInPc1	dva
experimentální	experimentální	k2eAgInPc1d1	experimentální
stroje	stroj	k1gInPc1	stroj
postavené	postavený	k2eAgInPc1d1	postavený
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
účinků	účinek	k1gInPc2	účinek
radiace	radiace	k1gFnSc2	radiace
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postavit	postavit	k5eAaPmF	postavit
zkušební	zkušební	k2eAgInSc4d1	zkušební
reaktor	reaktor	k1gInSc4	reaktor
a	a	k8xC	a
umístit	umístit	k5eAaPmF	umístit
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
klasického	klasický	k2eAgNnSc2d1	klasické
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
možný	možný	k2eAgMnSc1d1	možný
kandidát	kandidát	k1gMnSc1	kandidát
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
největší	veliký	k2eAgMnSc1d3	veliký
bombardér	bombardér	k1gMnSc1	bombardér
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
<g/>
,	,	kIx,	,
Convair	Convair	k1gInSc4	Convair
B-	B-	k1gFnSc2	B-
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
byl	být	k5eAaImAgMnS	být
zkušební	zkušební	k2eAgInSc4d1	zkušební
reaktor	reaktor	k1gInSc4	reaktor
umístěn	umístěn	k2eAgInSc4d1	umístěn
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
pumovnice	pumovnice	k1gFnSc2	pumovnice
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
letounu	letoun	k1gInSc2	letoun
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
také	také	k9	také
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
byl	být	k5eAaImAgInS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
radiační	radiační	k2eAgInSc1d1	radiační
štít	štít	k1gInSc1	štít
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
12	[number]	k4	12
tun	tuna	k1gFnPc2	tuna
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
pryže	pryž	k1gFnSc2	pryž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
let	léto	k1gNnPc2	léto
zkušebního	zkušební	k2eAgInSc2d1	zkušební
letounu	letoun	k1gInSc2	letoun
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
NB-36H	NB-36H	k1gFnSc2	NB-36H
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1955	[number]	k4	1955
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgNnPc2	dva
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
47	[number]	k4	47
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
první	první	k4xOgInSc1	první
zkušební	zkušební	k2eAgInSc1d1	zkušební
motor	motor	k1gInSc1	motor
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
X-	X-	k1gFnSc2	X-
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tah	tah	k1gInSc1	tah
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
letadla	letadlo	k1gNnSc2	letadlo
nedostačující	dostačující	k2eNgInPc1d1	nedostačující
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byl	být	k5eAaImAgInS	být
rozpracován	rozpracován	k2eAgInSc1d1	rozpracován
plán	plán	k1gInSc1	plán
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
bombardér	bombardér	k1gInSc4	bombardér
WS-	WS-	k1gMnPc2	WS-
<g/>
125	[number]	k4	125
<g/>
.	.	kIx.	.
</s>
<s>
Designem	design	k1gInSc7	design
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnSc7	konstrukce
byla	být	k5eAaImAgFnS	být
pověřena	pověřen	k2eAgFnSc1d1	pověřena
společnost	společnost	k1gFnSc1	společnost
Convair	Convaira	k1gFnPc2	Convaira
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
provozuschopný	provozuschopný	k2eAgInSc4d1	provozuschopný
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finanční	finanční	k2eAgFnSc3d1	finanční
náročnosti	náročnost	k1gFnSc3	náročnost
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
o	o	k7c6	o
pozastavení	pozastavení	k1gNnSc6	pozastavení
programu	program	k1gInSc2	program
a	a	k8xC	a
testů	test	k1gInPc2	test
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Všechen	všechen	k3xTgInSc1	všechen
výzkum	výzkum	k1gInSc1	výzkum
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
soustředěn	soustředit	k5eAaPmNgInS	soustředit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Aviation	Aviation	k1gInSc1	Aviation
Week	Week	k1gInSc1	Week
otištěn	otištěn	k2eAgInSc1d1	otištěn
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
měl	mít	k5eAaImAgInS	mít
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
otestovat	otestovat	k5eAaPmF	otestovat
bombardér	bombardér	k1gInSc4	bombardér
poháněný	poháněný	k2eAgInSc4d1	poháněný
jaderným	jaderný	k2eAgInSc7d1	jaderný
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
konvenční	konvenční	k2eAgInSc4d1	konvenční
bombardér	bombardér	k1gInSc4	bombardér
Mjasiščev	Mjasiščev	k1gMnSc1	Mjasiščev
M-50	M-50	k1gMnSc1	M-50
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
zpráva	zpráva	k1gFnSc1	zpráva
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
planý	planý	k2eAgInSc4d1	planý
poplach	poplach	k1gInSc4	poplach
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
navrátit	navrátit	k5eAaPmF	navrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
programu	program	k1gInSc3	program
jaderného	jaderný	k2eAgInSc2d1	jaderný
bombardéru	bombardér	k1gInSc2	bombardér
WS-	WS-	k1gFnSc2	WS-
<g/>
125	[number]	k4	125
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
článku	článek	k1gInSc2	článek
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
má	mít	k5eAaImIp3nS	mít
náskok	náskok	k1gInSc4	náskok
3-5	[number]	k4	3-5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nestalo	stát	k5eNaPmAgNnS	stát
nic	nic	k3yNnSc1	nic
převratného	převratný	k2eAgNnSc2d1	převratné
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
vypracovat	vypracovat	k5eAaPmF	vypracovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
skutečném	skutečný	k2eAgInSc6d1	skutečný
stavu	stav	k1gInSc6	stav
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
zrušení	zrušení	k1gNnSc3	zrušení
programu	program	k1gInSc2	program
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
zahájení	zahájení	k1gNnSc3	zahájení
sovětského	sovětský	k2eAgInSc2d1	sovětský
programu	program	k1gInSc2	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vývoje	vývoj	k1gInSc2	vývoj
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
došlo	dojít	k5eAaPmAgNnS	dojít
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
nařízení	nařízení	k1gNnSc4	nařízení
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výzkumu	výzkum	k1gInSc2	výzkum
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
samotné	samotný	k2eAgFnPc4d1	samotná
proveditelnosti	proveditelnost	k1gFnPc4	proveditelnost
přišel	přijít	k5eAaPmAgMnS	přijít
I.	I.	kA	I.
V.	V.	kA	V.
Kurčatov	Kurčatov	k1gInSc1	Kurčatov
<g/>
.	.	kIx.	.
</s>
<s>
Vývojem	vývoj	k1gInSc7	vývoj
letadel	letadlo	k1gNnPc2	letadlo
byly	být	k5eAaImAgFnP	být
pověřeny	pověřit	k5eAaPmNgFnP	pověřit
konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
kanceláře	kancelář	k1gFnPc1	kancelář
A.	A.	kA	A.
Tupoleva	Tupolev	k1gMnSc4	Tupolev
a	a	k8xC	a
V.	V.	kA	V.
Mjasiščeva	Mjasiščeva	k1gFnSc1	Mjasiščeva
<g/>
.	.	kIx.	.
</s>
<s>
Vývojem	vývoj	k1gInSc7	vývoj
pohonu	pohon	k1gInSc2	pohon
pro	pro	k7c4	pro
letadla	letadlo	k1gNnSc2	letadlo
byly	být	k5eAaImAgFnP	být
pověřeny	pověřit	k5eAaPmNgFnP	pověřit
kanceláře	kancelář	k1gFnPc1	kancelář
N.	N.	kA	N.
D.	D.	kA	D.
Kuznětsova	Kuznětsův	k2eAgNnPc1d1	Kuznětsův
a	a	k8xC	a
A.	A.	kA	A.
M.	M.	kA	M.
Ljulky	Ljulka	k1gMnSc2	Ljulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výzkumu	výzkum	k1gInSc2	výzkum
bylo	být	k5eAaImAgNnS	být
vyzkoušeno	vyzkoušet	k5eAaPmNgNnS	vyzkoušet
několik	několik	k4yIc1	několik
motorů	motor	k1gInPc2	motor
(	(	kIx(	(
<g/>
náporový	náporový	k2eAgMnSc1d1	náporový
<g/>
,	,	kIx,	,
turbovrtulový	turbovrtulový	k2eAgMnSc1d1	turbovrtulový
a	a	k8xC	a
proudový	proudový	k2eAgMnSc1d1	proudový
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
technologiemi	technologie	k1gFnPc7	technologie
přenosu	přenos	k1gInSc2	přenos
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
proudový	proudový	k2eAgInSc1d1	proudový
motor	motor	k1gInSc1	motor
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
celého	celý	k2eAgInSc2d1	celý
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
stínění	stínění	k1gNnSc2	stínění
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
souvisí	souviset	k5eAaImIp3nS	souviset
právě	právě	k9	právě
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
kancelář	kancelář	k1gFnSc1	kancelář
A.	A.	kA	A.
Tupoleva	Tupoleva	k1gFnSc1	Tupoleva
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
odhadů	odhad	k1gInPc2	odhad
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
letadlo	letadlo	k1gNnSc1	letadlo
provozuschopné	provozuschopný	k2eAgNnSc1d1	provozuschopné
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
začátkem	začátkem	k7c2	začátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
kancelář	kancelář	k1gFnSc1	kancelář
A.	A.	kA	A.
Tupoleva	Tupoleva	k1gFnSc1	Tupoleva
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
osadit	osadit	k5eAaPmF	osadit
testovací	testovací	k2eAgNnSc1d1	testovací
letadlo	letadlo	k1gNnSc1	letadlo
malým	malý	k2eAgInSc7d1	malý
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Vybrán	vybrat	k5eAaPmNgInS	vybrat
byl	být	k5eAaImAgInS	být
bombardér	bombardér	k1gInSc1	bombardér
Tupolev	Tupolev	k1gMnPc2	Tupolev
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1961-1965	[number]	k4	1961-1965
upravený	upravený	k2eAgInSc1d1	upravený
bombardér	bombardér	k1gInSc1	bombardér
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
95	[number]	k4	95
<g/>
LAL	LAL	kA	LAL
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
34	[number]	k4	34
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
s	s	k7c7	s
funkčním	funkční	k2eAgInSc7d1	funkční
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
testování	testování	k1gNnSc2	testování
bylo	být	k5eAaImAgNnS	být
ověření	ověření	k1gNnSc4	ověření
funkčnosti	funkčnost	k1gFnSc2	funkčnost
radiačních	radiační	k2eAgInPc2d1	radiační
štítů	štít	k1gInPc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
výzkumu	výzkum	k1gInSc2	výzkum
bylo	být	k5eAaImAgNnS	být
vyvinout	vyvinout	k5eAaPmF	vyvinout
funkční	funkční	k2eAgNnSc4d1	funkční
letadlo	letadlo	k1gNnSc4	letadlo
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
jaderného	jaderný	k2eAgInSc2d1	jaderný
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
dokázal	dokázat	k5eAaPmAgInS	dokázat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
letět	letět	k5eAaImF	letět
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
následkem	následkem	k7c2	následkem
zkušebních	zkušební	k2eAgInPc2d1	zkušební
letů	let	k1gInPc2	let
a	a	k8xC	a
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
stínění	stínění	k1gNnSc2	stínění
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
ozáření	ozáření	k1gNnSc4	ozáření
několik	několik	k4yIc1	několik
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základech	základ	k1gInPc6	základ
bombardéru	bombardér	k1gInSc2	bombardér
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
95	[number]	k4	95
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
turbovrtulový	turbovrtulový	k2eAgInSc1d1	turbovrtulový
letoun	letoun	k1gInSc1	letoun
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
119	[number]	k4	119
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
motorů	motor	k1gInPc2	motor
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
poháněné	poháněný	k2eAgMnPc4d1	poháněný
jaderným	jaderný	k2eAgInSc7d1	jaderný
reaktorem	reaktor	k1gInSc7	reaktor
přes	přes	k7c4	přes
nepřímý	přímý	k2eNgInSc4d1	nepřímý
cyklus	cyklus	k1gInSc4	cyklus
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
zbývající	zbývající	k2eAgFnPc1d1	zbývající
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
standardní	standardní	k2eAgInPc4d1	standardní
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS	Tupolet
se	se	k3xPyFc4	se
také	také	k9	také
zabýval	zabývat	k5eAaImAgMnS	zabývat
vytvořením	vytvoření	k1gNnSc7	vytvoření
nadzvukového	nadzvukový	k2eAgInSc2d1	nadzvukový
letounu	letoun	k1gInSc2	letoun
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
120	[number]	k4	120
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
všechny	všechen	k3xTgInPc1	všechen
projekty	projekt	k1gInPc1	projekt
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
kancelář	kancelář	k1gFnSc1	kancelář
V.	V.	kA	V.
Mjasiščeva	Mjasiščeva	k1gFnSc1	Mjasiščeva
===	===	k?	===
</s>
</p>
<p>
<s>
Paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
kanceláře	kancelář	k1gFnSc2	kancelář
Tupoleva	Tupoleva	k1gFnSc1	Tupoleva
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
výzkumu	výzkum	k1gInSc6	výzkum
letadel	letadlo	k1gNnPc2	letadlo
i	i	k8xC	i
kancelář	kancelář	k1gFnSc1	kancelář
Mjasiščeva	Mjasiščeva	k1gFnSc1	Mjasiščeva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
nadzvukového	nadzvukový	k2eAgInSc2d1	nadzvukový
letounu	letoun	k1gInSc2	letoun
M-	M-	k1gFnSc2	M-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
kancelář	kancelář	k1gFnSc1	kancelář
Ljulky	Ljulka	k1gFnSc2	Ljulka
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
motor	motor	k1gInSc4	motor
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cyklem	cyklus	k1gInSc7	cyklus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
tahu	tah	k1gInSc2	tah
přes	přes	k7c4	přes
20	[number]	k4	20
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Vzlet	vzlet	k1gInSc4	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
pomocí	pomocí	k7c2	pomocí
standardního	standardní	k2eAgNnSc2d1	standardní
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
potřebné	potřebný	k2eAgFnSc2d1	potřebná
letové	letový	k2eAgFnSc2d1	letová
hladiny	hladina	k1gFnSc2	hladina
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
motory	motor	k1gInPc1	motor
přepnuly	přepnout	k5eAaPmAgInP	přepnout
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovaná	navrhovaný	k2eAgFnSc1d1	navrhovaná
rychlost	rychlost	k1gFnSc1	rychlost
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
machy	macha	k1gFnSc2	macha
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
usazena	usadit	k5eAaPmNgFnS	usadit
v	v	k7c6	v
olověném	olověný	k2eAgInSc6d1	olověný
štítu	štít	k1gInSc6	štít
a	a	k8xC	a
reaktor	reaktor	k1gInSc1	reaktor
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
výzkumu	výzkum	k1gInSc6	výzkum
byly	být	k5eAaImAgInP	být
provedeny	proveden	k2eAgInPc1d1	proveden
odhady	odhad	k1gInPc1	odhad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
s	s	k7c7	s
vhodnými	vhodný	k2eAgInPc7d1	vhodný
reaktory	reaktor	k1gInPc7	reaktor
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
provozovat	provozovat	k5eAaImF	provozovat
bombardér	bombardér	k1gInSc4	bombardér
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
2	[number]	k4	2
machy	macha	k1gFnSc2	macha
<g/>
,	,	kIx,	,
operačním	operační	k2eAgInSc7d1	operační
doletem	dolet	k1gInSc7	dolet
25	[number]	k4	25
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
dostupem	dostup	k1gInSc7	dostup
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Mjasiščev	Mjasiščet	k5eAaPmDgInS	Mjasiščet
se	se	k3xPyFc4	se
paralelně	paralelně	k6eAd1	paralelně
zabýval	zabývat	k5eAaImAgInS	zabývat
vývojem	vývoj	k1gInSc7	vývoj
dalších	další	k2eAgNnPc2d1	další
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
jaderným	jaderný	k2eAgInSc7d1	jaderný
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
již	již	k9	již
v	v	k7c6	v
návrhové	návrhový	k2eAgFnSc6d1	návrhová
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
