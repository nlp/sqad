<s>
Jamajka	Jamajka	k1gFnSc1	Jamajka
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
ve	v	k7c6	v
Velkých	velký	k2eAgFnPc6d1	velká
Antilách	Antily	k1gFnPc6	Antily
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
