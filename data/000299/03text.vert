<s>
Vesnice	vesnice	k1gFnSc1	vesnice
Malín	Malína	k1gFnPc2	Malína
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Malin	malina	k1gFnPc2	malina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
3	[number]	k4	3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Malín	Malína	k1gFnPc2	Malína
je	být	k5eAaImIp3nS	být
nejspíš	nejspíš	k9	nejspíš
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
malina	malina	k1gFnSc1	malina
nebo	nebo	k8xC	nebo
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalých	bývalý	k2eAgFnPc2d1	bývalá
staroslovanských	staroslovanský	k2eAgFnPc2d1	staroslovanská
<g/>
,	,	kIx,	,
germánských	germánský	k2eAgFnPc2d1	germánská
a	a	k8xC	a
keltských	keltský	k2eAgFnPc2d1	keltská
osad	osada	k1gFnPc2	osada
slovanským	slovanský	k2eAgInSc7d1	slovanský
rodem	rod	k1gInSc7	rod
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
hradiště	hradiště	k1gNnSc4	hradiště
a	a	k8xC	a
razili	razit	k5eAaImAgMnP	razit
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
985	[number]	k4	985
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
mince	mince	k1gFnSc2	mince
zvané	zvaný	k2eAgInPc4d1	zvaný
denáry	denár	k1gInPc4	denár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvraždění	vyvraždění	k1gNnSc6	vyvraždění
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
995	[number]	k4	995
přešel	přejít	k5eAaPmAgInS	přejít
Malín	Malín	k1gInSc1	Malín
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Malína	Malín	k1gInSc2	Malín
se	se	k3xPyFc4	se
rozprchlo	rozprchnout	k5eAaPmAgNnS	rozprchnout
vojsko	vojsko	k1gNnSc1	vojsko
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Brněnského	brněnský	k2eAgMnSc2d1	brněnský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
chtěl	chtít	k5eAaImAgInS	chtít
uplatnit	uplatnit	k5eAaPmF	uplatnit
svá	svůj	k3xOyFgNnPc4	svůj
nástupnická	nástupnický	k2eAgNnPc4d1	nástupnické
práva	právo	k1gNnPc4	právo
proti	proti	k7c3	proti
Bořivojovi	Bořivoj	k1gMnSc3	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
přešel	přejít	k5eAaPmAgMnS	přejít
Malín	Malín	k1gMnSc1	Malín
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Malín	Malín	k1gInSc1	Malín
několikrát	několikrát	k6eAd1	několikrát
vydrancován	vydrancovat	k5eAaPmNgInS	vydrancovat
procházejícími	procházející	k2eAgNnPc7d1	procházející
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Malín	Malína	k1gFnPc2	Malína
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Sedlec	Sedlec	k1gInSc1	Sedlec
<g/>
.	.	kIx.	.
</s>
<s>
Malín	Malín	k1gMnSc1	Malín
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
hlavně	hlavně	k6eAd1	hlavně
slavným	slavný	k2eAgInSc7d1	slavný
malínským	malínský	k2eAgInSc7d1	malínský
křenem	křen	k1gInSc7	křen
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Množislav	Množislav	k1gMnSc1	Množislav
Bačkora	bačkora	k1gFnSc1	bačkora
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bačkora	bačkora	k1gFnSc1	bačkora
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Josefa	Josef	k1gMnSc2	Josef
M.	M.	kA	M.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Bílejovský	Bílejovský	k1gMnSc1	Bílejovský
(	(	kIx(	(
<g/>
1480	[number]	k4	1480
<g/>
-	-	kIx~	-
<g/>
1555	[number]	k4	1555
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starokališnický	starokališnický	k2eAgMnSc1d1	starokališnický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
kronikář	kronikář	k1gMnSc1	kronikář
Josef	Josef	k1gMnSc1	Josef
Lacina	Lacina	k1gMnSc1	Lacina
(	(	kIx(	(
<g/>
též	též	k9	též
Kolda	Kolda	k1gMnSc1	Kolda
Malínský	malínský	k2eAgMnSc1d1	malínský
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malín	Malína	k1gFnPc2	Malína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
Aplikace	aplikace	k1gFnSc1	aplikace
adresy	adresa	k1gFnSc2	adresa
na	na	k7c4	na
MVCR	MVCR	kA	MVCR
Databáze	databáze	k1gFnSc2	databáze
statistických	statistický	k2eAgInPc2d1	statistický
obvodů	obvod	k1gInPc2	obvod
</s>
