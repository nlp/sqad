<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zkráceně	zkráceně	k6eAd1	zkráceně
též	též	k9	též
L.A.	L.A.	k1gFnSc1	L.A.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
druhé	druhý	k4xOgFnSc2	druhý
nejlidnatější	lidnatý	k2eAgFnSc2d3	nejlidnatější
město	město	k1gNnSc1	město
USA	USA	kA	USA
<g/>
,	,	kIx,	,
středisko	středisko	k1gNnSc1	středisko
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
světových	světový	k2eAgNnPc2d1	světové
center	centrum	k1gNnPc2	centrum
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
u	u	k7c2	u
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1	[number]	k4	1
302	[number]	k4	302
km2	km2	k4	km2
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
885	[number]	k4	885
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
pak	pak	k6eAd1	pak
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
tvoří	tvořit	k5eAaImIp3nS	tvořit
přes	přes	k7c4	přes
80	[number]	k4	80
samostatných	samostatný	k2eAgNnPc2d1	samostatné
měst	město	k1gNnPc2	město
a	a	k8xC	a
městeček	městečko	k1gNnPc2	městečko
(	(	kIx(	(
<g/>
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
Pasadena	Pasaden	k2eAgFnSc1d1	Pasadena
<g/>
,	,	kIx,	,
Burbank	Burbank	k1gInSc1	Burbank
<g/>
,	,	kIx,	,
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
Inglewood	Inglewood	k1gInSc1	Inglewood
<g/>
,	,	kIx,	,
Compton	Compton	k1gInSc1	Compton
<g/>
,	,	kIx,	,
Torrance	Torrance	k1gFnSc1	Torrance
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Monica	Monicum	k1gNnSc2	Monicum
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
"	"	kIx"	"
<g/>
City	city	k1gNnSc1	city
of	of	k?	of
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Downtownu	Downtown	k1gInSc2	Downtown
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
centrum	centrum	k1gNnSc1	centrum
L.A.	L.A.	k1gFnSc2	L.A.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
většinou	většinou	k6eAd1	většinou
nízké	nízký	k2eAgInPc4d1	nízký
rodinné	rodinný	k2eAgInPc4d1	rodinný
domky	domek	k1gInPc4	domek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
lidé	člověk	k1gMnPc1	člověk
stráví	strávit	k5eAaPmIp3nP	strávit
dvakrát	dvakrát	k6eAd1	dvakrát
víc	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
dojížděním	dojíždění	k1gNnSc7	dojíždění
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
amerických	americký	k2eAgNnPc6d1	americké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
hispánských	hispánský	k2eAgMnPc2d1	hispánský
obyvatel	obyvatel	k1gMnPc2	obyvatel
způsobený	způsobený	k2eAgInSc4d1	způsobený
hlavně	hlavně	k9	hlavně
imigrací	imigrace	k1gFnSc7	imigrace
chudých	chudý	k2eAgMnPc2d1	chudý
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
,	,	kIx,	,
kriminalita	kriminalita	k1gFnSc1	kriminalita
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
poprvé	poprvé	k6eAd1	poprvé
přistáli	přistát	k5eAaImAgMnP	přistát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
kalifornského	kalifornský	k2eAgNnSc2d1	kalifornské
pobřeží	pobřeží	k1gNnSc2	pobřeží
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1542	[number]	k4	1542
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
L.A.	L.A.	k1gMnPc1	L.A.
založili	založit	k5eAaPmAgMnP	založit
teprve	teprve	k6eAd1	teprve
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1781	[number]	k4	1781
jako	jako	k8xS	jako
malé	malý	k2eAgNnSc4d1	malé
misijní	misijní	k2eAgNnSc4d1	misijní
sídlo	sídlo	k1gNnSc4	sídlo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
El	Ela	k1gFnPc2	Ela
Pueblo	Pueblo	k1gMnPc2	Pueblo
de	de	k?	de
Nuestra	Nuestra	k1gFnSc1	Nuestra
Señ	Señ	k1gFnSc2	Señ
la	la	k1gNnSc1	la
Reina	Rein	k2eAgNnSc2d1	Reino
de	de	k?	de
los	los	k1gInSc1	los
Ángeles	Ángeles	k1gInSc1	Ángeles
de	de	k?	de
la	la	k1gNnSc1	la
Porciúncula	Porciúncula	k1gFnSc1	Porciúncula
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
Naší	náš	k3xOp1gFnSc2	náš
Paní	paní	k1gFnSc2	paní
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
andělů	anděl	k1gMnPc2	anděl
z	z	k7c2	z
Porciunkule	porciunkule	k1gFnSc2	porciunkule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
do	do	k7c2	do
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
náleželo	náležet	k5eAaImAgNnS	náležet
L.A.	L.A.	k1gMnPc3	L.A.
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
americkým	americký	k2eAgNnSc7d1	americké
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mělo	mít	k5eAaImAgNnS	mít
1	[number]	k4	1
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Napojení	napojení	k1gNnSc1	napojení
na	na	k7c4	na
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
už	už	k6eAd1	už
mělo	mít	k5eAaImAgNnS	mít
první	první	k4xOgFnSc4	první
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
významu	význam	k1gInSc3	význam
jako	jako	k8xC	jako
přístav	přístav	k1gInSc4	přístav
začalo	začít	k5eAaPmAgNnS	začít
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
ohrožované	ohrožovaný	k2eAgNnSc1d1	ohrožované
častými	častý	k2eAgNnPc7d1	časté
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
žádným	žádný	k3yNgNnSc7	žádný
velkým	velký	k2eAgNnSc7d1	velké
poničeno	poničen	k2eAgNnSc4d1	poničeno
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
do	do	k7c2	do
města	město	k1gNnSc2	město
nový	nový	k2eAgInSc4d1	nový
vodovod	vodovod	k1gInSc4	vodovod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
do	do	k7c2	do
města	město	k1gNnSc2	město
přišlo	přijít	k5eAaPmAgNnS	přijít
mnoho	mnoho	k4c1	mnoho
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932	[number]	k4	1932
a	a	k8xC	a
1984	[number]	k4	1984
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
X.	X.	kA	X.
a	a	k8xC	a
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
následek	následek	k1gInSc4	následek
vládního	vládní	k2eAgInSc2d1	vládní
plánu	plán	k1gInSc2	plán
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
se	se	k3xPyFc4	se
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
stalo	stát	k5eAaPmAgNnS	stát
městem	město	k1gNnSc7	město
bohatých	bohatý	k2eAgFnPc2d1	bohatá
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
domy	dům	k1gInPc4	dům
na	na	k7c6	na
luxusních	luxusní	k2eAgNnPc6d1	luxusní
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	on	k3xPp3gMnPc4	on
Beverly	Beverl	k1gMnPc4	Beverl
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
městem	město	k1gNnSc7	město
chudých	chudý	k2eAgInPc2d1	chudý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
velkou	velký	k2eAgFnSc7d1	velká
imigrací	imigrace	k1gFnSc7	imigrace
chudých	chudý	k2eAgMnPc2d1	chudý
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
(	(	kIx(	(
<g/>
City	city	k1gNnSc1	city
of	of	k?	of
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
celkem	celkem	k6eAd1	celkem
114	[number]	k4	114
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
Neighborhoods	Neighborhoods	k1gInSc1	Neighborhoods
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgInPc3d1	hlavní
a	a	k8xC	a
nejznámějším	známý	k2eAgInPc3d3	nejznámější
náleží	náležet	k5eAaImIp3nS	náležet
např.	např.	kA	např.
Downtown	Downtown	k1gInSc1	Downtown
<g/>
,	,	kIx,	,
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
Venice	Venice	k1gFnSc1	Venice
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
součástí	součást	k1gFnPc2	součást
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
nejsou	být	k5eNaImIp3nP	být
rovněž	rovněž	k9	rovněž
známé	známý	k2eAgFnPc1d1	známá
Beverly	Beverla	k1gFnPc1	Beverla
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
Hermosa	Hermosa	k1gFnSc1	Hermosa
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gFnSc1	Monica
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
městy	město	k1gNnPc7	město
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
součástí	součást	k1gFnSc7	součást
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
County	Counta	k1gFnSc2	Counta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
LA	la	k1gNnSc1	la
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
hlavních	hlavní	k2eAgInPc2d1	hlavní
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
distriktů	distrikt	k1gInPc2	distrikt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Downtown	Downtown	k1gMnSc1	Downtown
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Eastern	Eastern	k1gMnSc1	Eastern
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
Echo	echo	k1gNnSc1	echo
Park	park	k1gInSc1	park
and	and	k?	and
Westlake	Westlak	k1gInSc2	Westlak
Harbor	Harbora	k1gFnPc2	Harbora
Area	area	k1gFnSc1	area
Greater	Greater	k1gInSc1	Greater
Hollywood	Hollywood	k1gInSc1	Hollywood
Los	los	k1gInSc1	los
Feliz	Feliz	k1gInSc1	Feliz
and	and	k?	and
Silverlake	Silverlak	k1gMnSc2	Silverlak
San	San	k1gMnSc2	San
Fernando	Fernanda	k1gFnSc5	Fernanda
and	and	k?	and
Crescenta	Crescento	k1gNnSc2	Crescento
Valleys	Valleysa	k1gFnPc2	Valleysa
South	South	k1gMnSc1	South
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
West	West	k1gMnSc1	West
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Wilshire	Wilshir	k1gInSc5	Wilshir
Downtown	Downtown	k1gInSc1	Downtown
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
historickým	historický	k2eAgNnSc7d1	historické
místem	místo	k1gNnSc7	místo
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
El	Ela	k1gFnPc2	Ela
Pueblo	Pueblo	k1gMnSc1	Pueblo
de	de	k?	de
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
LA	la	k1gNnSc1	la
Union	union	k1gInSc1	union
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
nejstarší	starý	k2eAgFnSc4d3	nejstarší
sakrální	sakrální	k2eAgFnSc4d1	sakrální
stavbu	stavba	k1gFnSc4	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
kostel	kostel	k1gInSc4	kostel
La	la	k1gNnSc2	la
Iglesia	Iglesius	k1gMnSc2	Iglesius
de	de	k?	de
Nuestra	Nuestr	k1gMnSc2	Nuestr
Señ	Señ	k1gMnSc2	Señ
la	la	k1gNnSc2	la
Reina	Rein	k2eAgInSc2d1	Rein
de	de	k?	de
Los	los	k1gInSc4	los
Ángeles	Ángelesa	k1gFnPc2	Ángelesa
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Church	Churcha	k1gFnPc2	Churcha
of	of	k?	of
Our	Our	k1gFnSc2	Our
Lady	lady	k1gFnPc2	lady
the	the	k?	the
Queen	Quena	k1gFnPc2	Quena
of	of	k?	of
the	the	k?	the
Angels	Angels	k1gInSc4	Angels
<g/>
)	)	kIx)	)
vystavěný	vystavěný	k2eAgInSc4d1	vystavěný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
a	a	k8xC	a
nejstarší	starý	k2eAgFnSc4d3	nejstarší
budovu	budova	k1gFnSc4	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Avila	Avil	k1gMnSc2	Avil
Adobe	Adobe	kA	Adobe
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
budova	budova	k1gFnSc1	budova
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
v	v	k7c6	v
historizujícím	historizující	k2eAgInSc6d1	historizující
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
pak	pak	k6eAd1	pak
138	[number]	k4	138
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
budova	budova	k1gFnSc1	budova
losangeleské	losangeleský	k2eAgFnSc2d1	losangeleská
radnice	radnice	k1gFnSc2	radnice
(	(	kIx(	(
<g/>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
City	city	k1gNnSc1	city
Hall	Hall	k1gMnSc1	Hall
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
radnice	radnice	k1gFnSc2	radnice
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
již	již	k6eAd1	již
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
části	část	k1gFnSc6	část
Downtownu	Downtown	k1gInSc2	Downtown
Civic	Civice	k1gFnPc2	Civice
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
část	část	k1gFnSc1	část
Downtownu	Downtown	k1gInSc3	Downtown
Bunker	Bunker	k1gMnSc1	Bunker
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
koncertní	koncertní	k2eAgFnSc4d1	koncertní
budovu	budova	k1gFnSc4	budova
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
F.	F.	kA	F.
Gehryho	Gehry	k1gMnSc2	Gehry
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Concert	Concert	k1gInSc4	Concert
Hall	Hall	k1gInSc4	Hall
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
ulici	ulice	k1gFnSc6	ulice
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Contemporary	Contemporara	k1gFnSc2	Contemporara
Art	Art	k1gFnSc2	Art
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
s	s	k7c7	s
díly	dílo	k1gNnPc7	dílo
M.	M.	kA	M.
Rothka	Rothek	k1gMnSc2	Rothek
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Rauschenberga	Rauschenberga	k1gFnSc1	Rauschenberga
nebo	nebo	k8xC	nebo
C.	C.	kA	C.
Oldenburga	Oldenburga	k1gFnSc1	Oldenburga
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Bunker	Bunkra	k1gFnPc2	Bunkra
Hill	Hill	k1gMnSc1	Hill
leží	ležet	k5eAaImIp3nS	ležet
nejviditelnější	viditelný	k2eAgFnSc4d3	nejviditelnější
část	část	k1gFnSc4	část
Downtownu	Downtown	k1gInSc2	Downtown
tvořící	tvořící	k2eAgNnSc1d1	tvořící
panorama	panorama	k1gNnSc1	panorama
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Financial	Financial	k1gMnSc1	Financial
District	District	k1gMnSc1	District
<g/>
.	.	kIx.	.
</s>
<s>
Nejpůsobivější	působivý	k2eAgFnSc7d3	nejpůsobivější
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Bank	banka	k1gFnPc2	banka
Tower	Towra	k1gFnPc2	Towra
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
73	[number]	k4	73
podlaží	podlaží	k1gNnSc2	podlaží
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
310	[number]	k4	310
m.	m.	k?	m.
Další	další	k2eAgFnSc4d1	další
dominantu	dominanta	k1gFnSc4	dominanta
tvoří	tvořit	k5eAaImIp3nP	tvořit
California	Californium	k1gNnPc1	Californium
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnSc2	dvojice
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
229	[number]	k4	229
m.	m.	k?	m.
Západně	západně	k6eAd1	západně
od	od	k7c2	od
městské	městský	k2eAgFnSc2d1	městská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Downtown	Downtowna	k1gFnPc2	Downtowna
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
West	West	k1gInSc4	West
Lake	Lak	k1gFnSc2	Lak
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgInSc1d3	veliký
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Downtownu	Downtown	k1gInSc2	Downtown
MacArthur	MacArthura	k1gFnPc2	MacArthura
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
budova	budova	k1gFnSc1	budova
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Bullocks	Bullocks	k1gInSc1	Bullocks
Wilshire	Wilshir	k1gInSc5	Wilshir
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Downtownu	Downtown	k1gInSc2	Downtown
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
University	universita	k1gFnSc2	universita
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
čtvrti	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
centrum	centrum	k1gNnSc1	centrum
vědy	věda	k1gFnSc2	věda
California	Californium	k1gNnSc2	Californium
Science	Science	k1gFnSc2	Science
Centre	centr	k1gInSc5	centr
s	s	k7c7	s
několia	několium	k1gNnPc1	několium
muzei	muzeum	k1gNnPc7	muzeum
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
areál	areál	k1gInSc1	areál
University	universita	k1gFnSc2	universita
of	of	k?	of
Southern	Southern	k1gInSc1	Southern
California	Californium	k1gNnSc2	Californium
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
parků	park	k1gInPc2	park
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
Exposition	Exposition	k1gInSc1	Exposition
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
čtvrť	čtvrť	k1gFnSc1	čtvrť
Hollywood	Hollywood	k1gInSc1	Hollywood
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Downtownu	Downtown	k1gInSc2	Downtown
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
čtvrti	čtvrt	k1gFnSc2	čtvrt
tvoří	tvořit	k5eAaImIp3nS	tvořit
ulice	ulice	k1gFnSc1	ulice
Hollywood	Hollywood	k1gInSc1	Hollywood
Boulevard	Boulevard	k1gMnSc1	Boulevard
a	a	k8xC	a
Sunset	Sunset	k1gMnSc1	Sunset
Boulevard	Boulevard	k1gMnSc1	Boulevard
(	(	kIx(	(
<g/>
střed	střed	k1gInSc1	střed
čtvrti	čtvrt	k1gFnSc2	čtvrt
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gNnSc4	on
protíná	protínat	k5eAaImIp3nS	protínat
Vine	vinout	k5eAaImIp3nS	vinout
St.	st.	kA	st.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
památkám	památka	k1gFnPc3	památka
náleží	náležet	k5eAaImIp3nS	náležet
kino	kino	k1gNnSc4	kino
Grauman	Grauman	k1gMnSc1	Grauman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Egyptian	Egyptian	k1gInSc1	Egyptian
Theatre	Theatr	k1gInSc5	Theatr
postavené	postavený	k2eAgFnPc4d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
odehrála	odehrát	k5eAaPmAgFnS	odehrát
první	první	k4xOgFnSc1	první
hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
filmová	filmový	k2eAgFnSc1d1	filmová
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
kino	kino	k1gNnSc4	kino
Man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chinese	Chinese	k1gFnPc1	Chinese
Theater	Theatrum	k1gNnPc2	Theatrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
promítání	promítání	k1gNnSc4	promítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
7000	[number]	k4	7000
Hollywood	Hollywood	k1gInSc1	Hollywood
Boulevard	Boulevard	k1gInSc1	Boulevard
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
Hollywood	Hollywood	k1gInSc1	Hollywood
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Hotel	hotel	k1gInSc1	hotel
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
v	v	k7c6	v
historizujícím	historizující	k2eAgInSc6d1	historizující
španělském	španělský	k2eAgInSc6d1	španělský
koloniálním	koloniální	k2eAgInSc6d1	koloniální
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
poprvé	poprvé	k6eAd1	poprvé
udělovaly	udělovat	k5eAaImAgFnP	udělovat
ceny	cena	k1gFnPc1	cena
Filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
čtvrť	čtvrť	k1gFnSc1	čtvrť
postupně	postupně	k6eAd1	postupně
upadala	upadat	k5eAaPmAgFnS	upadat
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
nejvíce	hodně	k6eAd3	hodně
vystihují	vystihovat	k5eAaImIp3nP	vystihovat
prázdné	prázdný	k2eAgInPc4d1	prázdný
bloky	blok	k1gInPc4	blok
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
s	s	k7c7	s
oplocenými	oplocený	k2eAgNnPc7d1	oplocené
parkovišti	parkoviště	k1gNnPc7	parkoviště
a	a	k8xC	a
místy	místy	k6eAd1	místy
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
bytových	bytový	k2eAgInPc2d1	bytový
domů	dům	k1gInPc2	dům
a	a	k8xC	a
komerčních	komerční	k2eAgFnPc2d1	komerční
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
filmových	filmový	k2eAgNnPc2d1	filmové
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
v	v	k7c6	v
hollywoodské	hollywoodský	k2eAgFnSc6d1	hollywoodská
čtvrti	čtvrt	k1gFnSc6	čtvrt
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
Paramount	Paramount	k1gInSc4	Paramount
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Melrose	Melrosa	k1gFnSc3	Melrosa
Ave	ave	k1gNnSc6	ave
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Hollywoodu	Hollywood	k1gInSc2	Hollywood
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čtvrť	čtvrť	k1gFnSc1	čtvrť
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hills	k1gInSc1	Hills
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
typickou	typický	k2eAgFnSc4d1	typická
losangeleskou	losangeleský	k2eAgFnSc4d1	losangeleská
zástavbu	zástavba	k1gFnSc4	zástavba
<g/>
,	,	kIx,	,
dvoupodlažní	dvoupodlažní	k2eAgInPc4d1	dvoupodlažní
domy	dům	k1gInPc4	dům
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
dřevostavby	dřevostavba	k1gFnSc2	dřevostavba
<g/>
)	)	kIx)	)
se	s	k7c7	s
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
čtvrti	čtvrt	k1gFnSc2	čtvrt
tvoří	tvořit	k5eAaImIp3nS	tvořit
vrchovina	vrchovina	k1gFnSc1	vrchovina
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hills	k1gInSc1	Hills
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
pohoří	pohoří	k1gNnSc2	pohoří
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Mountains	Mountains	k1gInSc4	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
vrchu	vrch	k1gInSc2	vrch
Mount	Mount	k1gInSc1	Mount
Lee	Lea	k1gFnSc3	Lea
(	(	kIx(	(
<g/>
521	[number]	k4	521
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
slavný	slavný	k2eAgInSc1d1	slavný
nápis	nápis	k1gInSc1	nápis
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
mají	mít	k5eAaImIp3nP	mít
výšku	výška	k1gFnSc4	výška
14	[number]	k4	14
m	m	kA	m
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozmístěna	rozmístěn	k2eAgNnPc1d1	rozmístěno
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
110	[number]	k4	110
m.	m.	k?	m.
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
původně	původně	k6eAd1	původně
umístěn	umístit	k5eAaPmNgInS	umístit
jako	jako	k8xC	jako
upoutávka	upoutávka	k1gFnSc1	upoutávka
na	na	k7c4	na
realitní	realitní	k2eAgInSc4d1	realitní
rozvoj	rozvoj	k1gInSc4	rozvoj
dané	daný	k2eAgFnSc2d1	daná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
čtvrti	čtvrt	k1gFnSc2	čtvrt
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hills	k1gInSc1	Hills
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
Griffith	Griffith	k1gMnSc1	Griffith
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
celou	celý	k2eAgFnSc4d1	celá
plochu	plocha	k1gFnSc4	plocha
tvoří	tvořit	k5eAaImIp3nS	tvořit
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
Griffith	Griffith	k1gInSc1	Griffith
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
tvoří	tvořit	k5eAaImIp3nS	tvořit
vrchovina	vrchovina	k1gFnSc1	vrchovina
Hollywood	Hollywood	k1gInSc1	Hollywood
Hills	Hills	k1gInSc1	Hills
<g/>
.	.	kIx.	.
</s>
<s>
Trasy	trasa	k1gFnPc1	trasa
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
na	na	k7c4	na
piknik	piknik	k1gInSc4	piknik
a	a	k8xC	a
výhledy	výhled	k1gInPc4	výhled
na	na	k7c4	na
město	město	k1gNnSc4	město
sem	sem	k6eAd1	sem
přivádějí	přivádět	k5eAaImIp3nP	přivádět
řadu	řada	k1gFnSc4	řada
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc7d1	dominantní
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
Griffith	Griffitha	k1gFnPc2	Griffitha
Observatory	Observator	k1gMnPc4	Observator
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Hollywoodu	Hollywood	k1gInSc2	Hollywood
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrtit	k5eAaImRp2nS	čtvrtit
Mid-Wilshire	Mid-Wilshir	k1gMnSc5	Mid-Wilshir
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
lokalitou	lokalita	k1gFnSc7	lokalita
pro	pro	k7c4	pro
bydlení	bydlení	k1gNnSc4	bydlení
a	a	k8xC	a
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
nemovistostí	nemovistost	k1gFnPc2	nemovistost
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
na	na	k7c6	na
Wilshire	Wilshir	k1gInSc5	Wilshir
Blvd	Blvdo	k1gNnPc2	Blvdo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
LA	la	k1gNnSc1	la
County	Counta	k1gFnSc2	Counta
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gFnPc2	Art
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
bohatou	bohatý	k2eAgFnSc7d1	bohatá
a	a	k8xC	a
různorodou	různorodý	k2eAgFnSc7d1	různorodá
sbírkou	sbírka	k1gFnSc7	sbírka
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnSc7	fotografia
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Mid-Wilshire	Mid-Wilshir	k1gInSc5	Mid-Wilshir
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc4	čtvrť
Westwood	Westwooda	k1gFnPc2	Westwooda
s	s	k7c7	s
cihlovou	cihlový	k2eAgFnSc7d1	cihlová
zástavbou	zástavba	k1gFnSc7	zástavba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
areál	areál	k1gInSc1	areál
University	universita	k1gFnSc2	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
at	at	k?	at
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Westwood	Westwood	k1gInSc4	Westwood
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
řadou	řada	k1gFnSc7	řada
kinosálů	kinosál	k1gInPc2	kinosál
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
je	být	k5eAaImIp3nS	být
Westwood	Westwood	k1gInSc1	Westwood
Village	Villag	k1gFnSc2	Villag
Theatre	Theatr	k1gInSc5	Theatr
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnPc1d1	další
z	z	k7c2	z
losangeleských	losangeleský	k2eAgNnPc2d1	losangeleské
muzeí	muzeum	k1gNnPc2	muzeum
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
sbírkami	sbírka	k1gFnPc7	sbírka
Hammer	Hammra	k1gFnPc2	Hammra
Museum	museum	k1gNnSc4	museum
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
díla	dílo	k1gNnSc2	dílo
např.	např.	kA	např.
Rembrandta	Rembrandt	k1gMnSc4	Rembrandt
<g/>
,	,	kIx,	,
Gustava	Gustav	k1gMnSc4	Gustav
Moreau	Moreaa	k1gMnSc4	Moreaa
<g/>
,	,	kIx,	,
Van	van	k1gInSc4	van
Gogha	Gogh	k1gMnSc2	Gogh
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Westwoodu	Westwood	k1gInSc2	Westwood
leží	ležet	k5eAaImIp3nS	ležet
čtvrť	čtvrť	k1gFnSc1	čtvrť
Brentwood	Brentwooda	k1gFnPc2	Brentwooda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
najdeme	najít	k5eAaPmIp1nP	najít
další	další	k2eAgNnSc4d1	další
muzeum	muzeum	k1gNnSc4	muzeum
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
sbírkami	sbírka	k1gFnPc7	sbírka
Getty	Getta	k1gFnSc2	Getta
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
budovy	budova	k1gFnSc2	budova
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
R.	R.	kA	R.
Meier	Meier	k1gInSc1	Meier
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
Gauguina	Gauguina	k1gMnSc1	Gauguina
<g/>
,	,	kIx,	,
Van	van	k1gInSc4	van
Gogha	Gogh	k1gMnSc2	Gogh
<g/>
,	,	kIx,	,
italských	italský	k2eAgMnPc2d1	italský
renesančních	renesanční	k2eAgMnPc2d1	renesanční
malířů	malíř	k1gMnPc2	malíř
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Venice	Venice	k1gFnSc1	Venice
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc4	oblast
založil	založit	k5eAaPmAgMnS	založit
investor	investor	k1gMnSc1	investor
Abbot	Abbot	k1gInSc4	Abbot
Kinney	Kinnea	k1gFnSc2	Kinnea
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
vytvořit	vytvořit	k5eAaPmF	vytvořit
zde	zde	k6eAd1	zde
kulturně-uměleckou	kulturněmělecký	k2eAgFnSc4d1	kulturně-umělecký
oblast	oblast	k1gFnSc4	oblast
s	s	k7c7	s
kanály	kanál	k1gInPc7	kanál
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jen	jen	k9	jen
část	část	k1gFnSc1	část
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
rodinnými	rodinný	k2eAgInPc7d1	rodinný
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
pevniny	pevnina	k1gFnSc2	pevnina
propojují	propojovat	k5eAaImIp3nP	propojovat
převážně	převážně	k6eAd1	převážně
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
mosty	most	k1gInPc4	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
ceněné	ceněný	k2eAgInPc4d1	ceněný
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
pobřežní	pobřežní	k1gMnPc1	pobřežní
Pacific	Pacifice	k1gInPc2	Pacifice
Ave	ave	k1gNnPc2	ave
a	a	k8xC	a
Winward	Winwarda	k1gFnPc2	Winwarda
Ave	ave	k1gNnSc1	ave
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stalo	stát	k5eAaPmAgNnS	stát
určitým	určitý	k2eAgInSc7d1	určitý
centrem	centr	k1gInSc7	centr
alternativní	alternativní	k2eAgFnSc2d1	alternativní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
hospod	hospod	k?	hospod
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
bazarů	bazar	k1gInPc2	bazar
a	a	k8xC	a
pouličních	pouliční	k2eAgMnPc2d1	pouliční
prodavačů	prodavač	k1gMnPc2	prodavač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Winward	Winward	k1gInSc4	Winward
Ave	ave	k1gNnSc2	ave
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
napodobeninou	napodobenina	k1gFnSc7	napodobenina
benátských	benátský	k2eAgInPc2d1	benátský
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zdařilých	zdařilý	k2eAgInPc2d1	zdařilý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
pláž	pláž	k1gFnSc1	pláž
Venice	Venice	k1gFnSc2	Venice
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
celá	celý	k2eAgNnPc1d1	celé
obklopena	obklopen	k2eAgNnPc1d1	obklopeno
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
ulice	ulice	k1gFnPc1	ulice
přímo	přímo	k6eAd1	přímo
přechází	přecházet	k5eAaImIp3nP	přecházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
více	hodně	k6eAd2	hodně
de	de	k?	de
jure	jure	k1gInSc1	jure
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
součástí	součást	k1gFnSc7	součást
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k2eAgInSc1d1	West
Hollywood	Hollywood	k1gInSc1	Hollywood
leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
prochází	procházet	k5eAaImIp3nS	procházet
třída	třída	k1gFnSc1	třída
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
ulice	ulice	k1gFnSc2	ulice
leží	ležet	k5eAaImIp3nS	ležet
Melrose	Melrosa	k1gFnSc3	Melrosa
Ave	ave	k1gNnSc2	ave
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
módou	móda	k1gFnSc7	móda
<g/>
,	,	kIx,	,
butiky	butik	k1gInPc7	butik
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc1	obchod
se	s	k7c7	s
starožitnostmi	starožitnost	k1gFnPc7	starožitnost
a	a	k8xC	a
menšími	malý	k2eAgFnPc7d2	menší
galeriemi	galerie	k1gFnPc7	galerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oblast	oblast	k1gFnSc1	oblast
podél	podél	k7c2	podél
ulice	ulice	k1gFnSc2	ulice
Sunset	Sunset	k1gMnSc1	Sunset
Blvd	Blvd	k1gMnSc1	Blvd
Sunset	Sunset	k1gMnSc1	Sunset
Strip	Strip	k1gMnSc1	Strip
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
nočního	noční	k2eAgInSc2d1	noční
života	život	k1gInSc2	život
v	v	k7c6	v
LA	la	k1gNnSc6	la
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
rockových	rockový	k2eAgInPc2d1	rockový
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
Sunset	Sunseta	k1gFnPc2	Sunseta
Blvd	Blvda	k1gFnPc2	Blvda
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
losangeleskou	losangeleský	k2eAgFnSc4d1	losangeleská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
hotel	hotel	k1gInSc4	hotel
Chateau	Chateaus	k1gInSc2	Chateaus
Marmont	Marmont	k1gInSc4	Marmont
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
francouzských	francouzský	k2eAgFnPc2d1	francouzská
královských	královský	k2eAgFnPc2d1	královská
rezidencí	rezidence	k1gFnPc2	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
West	Westa	k1gFnPc2	Westa
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
rezidenčních	rezidenční	k2eAgFnPc2d1	rezidenční
čtvrtí	čtvrt	k1gFnPc2	čtvrt
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klidné	klidný	k2eAgFnPc1d1	klidná
ulice	ulice	k1gFnPc1	ulice
jsou	být	k5eAaImIp3nP	být
lemovány	lemován	k2eAgInPc4d1	lemován
vzrostlými	vzrostlý	k2eAgInPc7d1	vzrostlý
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
stavbami	stavba	k1gFnPc7	stavba
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Rodeo	rodeo	k1gNnSc1	rodeo
Drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
této	tento	k3xDgFnSc2	tento
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vymezené	vymezený	k2eAgNnSc1d1	vymezené
třídami	třída	k1gFnPc7	třída
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Blvd	Blvd	k1gMnSc1	Blvd
a	a	k8xC	a
Wiltshire	Wiltshir	k1gInSc5	Wiltshir
Blvd	Blvdo	k1gNnPc2	Blvdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
luxusní	luxusní	k2eAgInPc4d1	luxusní
obchody	obchod	k1gInPc4	obchod
a	a	k8xC	a
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
světové	světový	k2eAgFnPc4d1	světová
obchodní	obchodní	k2eAgFnPc4d1	obchodní
"	"	kIx"	"
<g/>
značky	značka	k1gFnPc4	značka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Beverly	Beverla	k1gMnSc2	Beverla
Hills	Hills	k1gInSc4	Hills
<g/>
,	,	kIx,	,
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Santa	Santo	k1gNnSc2	Santo
Monica	Monic	k1gInSc2	Monic
Blvd	Blvda	k1gFnPc2	Blvda
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
budovu	budova	k1gFnSc4	budova
radnice	radnice	k1gFnSc2	radnice
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
v	v	k7c6	v
historizujícím	historizující	k2eAgInSc6d1	historizující
španělském	španělský	k2eAgInSc6d1	španělský
koloniálním	koloniální	k2eAgInSc6d1	koloniální
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
nejdražší	drahý	k2eAgFnSc4d3	nejdražší
radnici	radnice	k1gFnSc4	radnice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
vrchů	vrch	k1gInPc2	vrch
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Mountains	Mountains	k1gInSc4	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
letoviskem	letovisko	k1gNnSc7	letovisko
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
LA	la	k1gNnSc2	la
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
22	[number]	k4	22
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
85	[number]	k4	85
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
od	od	k7c2	od
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
ulice	ulice	k1gFnSc2	ulice
Ocean	Oceana	k1gFnPc2	Oceana
Ave	ave	k1gNnSc2	ave
leží	ležet	k5eAaImIp3nS	ležet
3	[number]	k4	3
<g/>
rd	rd	k?	rd
St.	st.	kA	st.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
Santa	Santa	k1gFnSc1	Santa
Monica	Monica	k1gFnSc1	Monica
Promenade	Promenad	k1gInSc5	Promenad
s	s	k7c7	s
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
připomíná	připomínat	k5eAaImIp3nS	připomínat
španělská	španělský	k2eAgFnSc1d1	španělská
či	či	k8xC	či
evropská	evropský	k2eAgNnPc1d1	Evropské
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
také	také	k9	také
molo	molo	k1gNnSc1	molo
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
-	-	kIx~	-
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
Pier	pier	k1gInSc4	pier
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
kolem	kolo	k1gNnSc7	kolo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
stánků	stánek	k1gInPc2	stánek
a	a	k8xC	a
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
Santa	Sant	k1gMnSc2	Sant
Monica	Monicus	k1gMnSc2	Monicus
<g/>
,	,	kIx,	,
za	za	k7c2	za
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Venice	Venice	k1gFnSc2	Venice
<g/>
,	,	kIx,	,
pláží	pláž	k1gFnPc2	pláž
Playa	Play	k1gInSc2	Play
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
a	a	k8xC	a
losangeleským	losangeleský	k2eAgNnSc7d1	losangeleské
letištěm	letiště	k1gNnSc7	letiště
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
oblíbená	oblíbený	k2eAgNnPc1d1	oblíbené
města	město	k1gNnPc1	město
Manhattan	Manhattan	k1gInSc1	Manhattan
Beach	Beach	k1gInSc1	Beach
<g/>
,	,	kIx,	,
Hermosa	Hermosa	k1gFnSc1	Hermosa
Beach	Beach	k1gInSc1	Beach
<g/>
,	,	kIx,	,
Redondo	Redondo	k6eAd1	Redondo
Beach	Beach	k1gMnSc1	Beach
a	a	k8xC	a
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
není	být	k5eNaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
Sacramento	Sacramento	k1gNnSc1	Sacramento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
ekonomicko-průmyslovým	ekonomickorůmyslový	k2eAgInSc7d1	ekonomicko-průmyslový
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
největším	veliký	k2eAgNnSc7d3	veliký
centrem	centrum	k1gNnSc7	centrum
výrobního	výrobní	k2eAgInSc2d1	výrobní
průmyslu	průmysl	k1gInSc2	průmysl
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příjmy	příjem	k1gInPc1	příjem
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
luxusního	luxusní	k2eAgNnSc2d1	luxusní
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tady	tady	k6eAd1	tady
probíhá	probíhat	k5eAaImIp3nS	probíhat
přeprava	přeprava	k1gFnSc1	přeprava
zboží	zboží	k1gNnSc2	zboží
mezi	mezi	k7c7	mezi
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
označováni	označovat	k5eAaImNgMnP	označovat
jako	jako	k9	jako
Angelenos	Angelenos	k1gMnSc1	Angelenos
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velice	velice	k6eAd1	velice
multikulturní	multikulturní	k2eAgNnSc1d1	multikulturní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
etnických	etnický	k2eAgFnPc2d1	etnická
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
např.	např.	kA	např.
i	i	k8xC	i
Arméni	Armén	k1gMnPc1	Armén
či	či	k8xC	či
Kambodžané	Kambodžan	k1gMnPc1	Kambodžan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
15	[number]	k4	15
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
hispánských	hispánský	k2eAgMnPc2d1	hispánský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Hispánců	Hispánec	k1gMnPc2	Hispánec
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
i	i	k9	i
počet	počet	k1gInSc1	počet
Asiatů	Asiat	k1gMnPc2	Asiat
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Rasové	rasový	k2eAgNnSc1d1	rasové
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
49	[number]	k4	49
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
indiáni	indián	k1gMnPc1	indián
a	a	k8xC	a
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
,	,	kIx,	,
11,1	[number]	k4	11,1
%	%	kIx~	%
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
26,9	[number]	k4	26,9
%	%	kIx~	%
jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
2,3	[number]	k4	2,3
%	%	kIx~	%
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
48,9	[number]	k4	48,9
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
Hispánci	Hispánek	k1gMnPc1	Hispánek
nebo	nebo	k8xC	nebo
Latinos	Latinos	k1gInSc1	Latinos
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
členy	člen	k1gMnPc4	člen
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
28,5	[number]	k4	28,5
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
běloši	běloch	k1gMnPc1	běloch
nehispánského	hispánský	k2eNgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
etnických	etnický	k2eAgFnPc6d1	etnická
skupinách	skupina	k1gFnPc6	skupina
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
dozvíte	dozvědět	k5eAaPmIp2nP	dozvědět
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
žije	žít	k5eAaImIp3nS	žít
3792621	[number]	k4	3792621
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
182	[number]	k4	182
144	[number]	k4	144
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
1	[number]	k4	1
485	[number]	k4	485
576	[number]	k4	576
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
663	[number]	k4	663
746	[number]	k4	746
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
státě	stát	k1gInSc6	stát
USA	USA	kA	USA
a	a	k8xC	a
61	[number]	k4	61
792	[number]	k4	792
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
amerických	americký	k2eAgNnPc2d1	americké
teritorií	teritorium	k1gNnPc2	teritorium
(	(	kIx(	(
<g/>
Portoriko	Portoriko	k1gNnSc1	Portoriko
<g/>
,	,	kIx,	,
Guam	Guam	k1gInSc1	Guam
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Mariany	Mariana	k1gFnPc1	Mariana
<g/>
,	,	kIx,	,
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
1	[number]	k4	1
512	[number]	k4	512
720	[number]	k4	720
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
100	[number]	k4	100
252	[number]	k4	252
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
376	[number]	k4	376
767	[number]	k4	767
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
64	[number]	k4	64
730	[number]	k4	730
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
94	[number]	k4	94
104	[number]	k4	104
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
,	,	kIx,	,
996	[number]	k4	996
996	[number]	k4	996
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
lidé	člověk	k1gMnPc1	člověk
Hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
a	a	k8xC	a
13	[number]	k4	13
859	[number]	k4	859
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
LA	la	k1gNnSc4	la
hrají	hrát	k5eAaImIp3nP	hrát
dva	dva	k4xCgInPc1	dva
baseballové	baseballový	k2eAgInPc1d1	baseballový
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ligu	liga	k1gFnSc4	liga
MLB	MLB	kA	MLB
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc4d1	národní
ligu	liga	k1gFnSc4	liga
hrají	hrát	k5eAaImIp3nP	hrát
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
Dodgers	Dodgersa	k1gFnPc2	Dodgersa
a	a	k8xC	a
Americkou	americký	k2eAgFnSc4d1	americká
ligu	liga	k1gFnSc4	liga
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Angels	Angelsa	k1gFnPc2	Angelsa
of	of	k?	of
Anaheim	Anaheima	k1gFnPc2	Anaheima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
město	město	k1gNnSc4	město
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
blízký	blízký	k2eAgInSc4d1	blízký
Anaheim	Anaheim	k1gInSc4	Anaheim
hrají	hrát	k5eAaImIp3nP	hrát
Anaheim	Anaheim	k1gInSc4	Anaheim
Ducks	Ducksa	k1gFnPc2	Ducksa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
lize	liga	k1gFnSc6	liga
NBA	NBA	kA	NBA
hrají	hrát	k5eAaImIp3nP	hrát
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Lakers	Lakers	k1gInSc1	Lakers
a	a	k8xC	a
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Clippers	Clippersa	k1gFnPc2	Clippersa
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Galaxy	Galax	k1gInPc1	Galax
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Metro	metro	k1gNnSc1	metro
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
10	[number]	k4	10
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
využívá	využívat	k5eAaImIp3nS	využívat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
k	k	k7c3	k
dojíždění	dojíždění	k1gNnSc3	dojíždění
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
LACMTA	LACMTA	kA	LACMTA
-	-	kIx~	-
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
County	Counta	k1gFnSc2	Counta
Metropolitian	Metropolitian	k1gInSc1	Metropolitian
Transportation	Transportation	k1gInSc1	Transportation
Authority	Authorita	k1gFnSc2	Authorita
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
a	a	k8xC	a
rychlodrážní	rychlodrážní	k2eAgFnPc1d1	rychlodrážní
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
Fialovou	fialový	k2eAgFnSc4d1	fialová
linku	linka	k1gFnSc4	linka
představuje	představovat	k5eAaImIp3nS	představovat
metro	metro	k1gNnSc1	metro
vedoucí	vedoucí	k1gFnSc2	vedoucí
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
do	do	k7c2	do
North	Northa	k1gFnPc2	Northa
Hollywood	Hollywood	k1gInSc1	Hollywood
a	a	k8xC	a
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
po	po	k7c6	po
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgFnSc6d1	Zelené
a	a	k8xC	a
Modré	modrý	k2eAgFnSc6d1	modrá
lince	linka	k1gFnSc6	linka
jezdí	jezdit	k5eAaImIp3nP	jezdit
moderní	moderní	k2eAgFnPc1d1	moderní
rychlodrážní	rychlodrážní	k2eAgFnPc1d1	rychlodrážní
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
L.A.	L.A.	k1gFnSc2	L.A.
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přes	přes	k7c4	přes
Compton	Compton	k1gInSc4	Compton
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
normální	normální	k2eAgInSc4d1	normální
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
přepraví	přepravit	k5eAaPmIp3nS	přepravit
průměrně	průměrně	k6eAd1	průměrně
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
normálních	normální	k2eAgInPc2d1	normální
místních	místní	k2eAgInPc2d1	místní
autobusů	autobus	k1gInPc2	autobus
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
tzv.	tzv.	kA	tzv.
rapidní	rapidní	k2eAgFnSc1d1	rapidní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
3	[number]	k4	3
speciálních	speciální	k2eAgFnPc2d1	speciální
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
cestují	cestovat	k5eAaImIp3nP	cestovat
větší	veliký	k2eAgFnSc7d2	veliký
rychlostí	rychlost	k1gFnSc7	rychlost
na	na	k7c4	na
delší	dlouhý	k2eAgFnPc4d2	delší
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
pomalejší	pomalý	k2eAgInPc1d2	pomalejší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgFnSc4d2	menší
kapacitu	kapacita	k1gFnSc4	kapacita
než	než	k8xS	než
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
výstavba	výstavba	k1gFnSc1	výstavba
dalších	další	k2eAgFnPc2d1	další
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
L.A.	L.A.	k1gFnSc2	L.A.
až	až	k9	až
k	k	k7c3	k
Ocean	Ocean	k1gMnSc1	Ocean
Avenue	avenue	k1gFnSc7	avenue
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Monice	Monika	k1gFnSc6	Monika
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc6	prodloužení
zlaté	zlatý	k2eAgFnSc2d1	zlatá
linky	linka	k1gFnSc2	linka
do	do	k7c2	do
East	Easta	k1gFnPc2	Easta
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
modré	modrý	k2eAgFnSc2d1	modrá
linky	linka	k1gFnSc2	linka
s	s	k7c7	s
nádražím	nádraží	k1gNnSc7	nádraží
Union	union	k1gInSc1	union
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prodlužování	prodlužování	k1gNnSc2	prodlužování
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
se	se	k3xPyFc4	se
také	také	k9	také
plánuje	plánovat	k5eAaImIp3nS	plánovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
fialové	fialový	k2eAgFnSc2d1	fialová
linky	linka	k1gFnSc2	linka
metra	metro	k1gNnSc2	metro
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
International	International	k1gFnPc2	International
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
LAX	LAX	kA	LAX
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvytíženějších	vytížený	k2eAgNnPc2d3	nejvytíženější
letišť	letiště	k1gNnPc2	letiště
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
vedoucí	vedoucí	k1gFnSc2	vedoucí
okolo	okolo	k6eAd1	okolo
a	a	k8xC	a
skrz	skrz	k7c4	skrz
město	město	k1gNnSc4	město
jsou	být	k5eAaImIp3nP	být
osmiproudé	osmiproudý	k2eAgInPc1d1	osmiproudý
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc4d1	rychlý
systém	systém	k1gInSc4	systém
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
okrese	okres	k1gInSc6	okres
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
silničními	silniční	k2eAgFnPc7d1	silniční
tepnami	tepna	k1gFnPc7	tepna
celého	celý	k2eAgInSc2d1	celý
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
Wilshire	Wilshir	k1gInSc5	Wilshir
Boulevard	Boulevard	k1gMnSc1	Boulevard
a	a	k8xC	a
Sunset	Sunset	k1gMnSc1	Sunset
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
dálnici	dálnice	k1gFnSc6	dálnice
jezdí	jezdit	k5eAaImIp3nP	jezdit
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
nákladní	nákladní	k2eAgInPc1d1	nákladní
trolejbusy	trolejbus	k1gInPc1	trolejbus
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
má	mít	k5eAaImIp3nS	mít
25	[number]	k4	25
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
Únik	únik	k1gInSc1	únik
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
