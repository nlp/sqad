<s>
Monty	Monta	k1gMnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
:	:	kIx,	:
Smysl	smysl	k1gInSc1	smysl
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Monty	Monta	k1gMnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
The	The	k1gFnSc7	The
Meaning	Meaning	k1gInSc4	Meaning
of	of	k?	of
Life	Life	k1gInSc1	Life
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
filmová	filmový	k2eAgFnSc1d1	filmová
hudební	hudební	k2eAgFnSc1d1	hudební
komedie	komedie	k1gFnSc1	komedie
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Terry	Terr	k1gInPc4	Terr
Jones	Jonesa	k1gFnPc2	Jonesa
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
skečů	skeč	k1gInPc2	skeč
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
etapách	etapa	k1gFnPc6	etapa
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
volně	volně	k6eAd1	volně
propojené	propojený	k2eAgNnSc1d1	propojené
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kinematografický	kinematografický	k2eAgInSc1d1	kinematografický
počin	počin	k1gInSc1	počin
tedy	tedy	k9	tedy
nemá	mít	k5eNaImIp3nS	mít
jednolitý	jednolitý	k2eAgInSc4d1	jednolitý
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Nemálo	málo	k6eNd1	málo
scén	scéna	k1gFnPc2	scéna
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
proti	proti	k7c3	proti
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
zakázán	zakázat	k5eAaPmNgInS	zakázat
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
<g/>
.	.	kIx.	.
</s>
<s>
Graham	Graham	k1gMnSc1	Graham
Chapman	Chapman	k1gMnSc1	Chapman
John	John	k1gMnSc1	John
Cleese	Cleese	k1gFnSc2	Cleese
Terry	Terra	k1gFnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
Eric	Eric	k1gFnSc4	Eric
Idle	Idle	k1gNnSc2	Idle
Terry	Terra	k1gFnSc2	Terra
Jones	Jones	k1gMnSc1	Jones
Michael	Michael	k1gMnSc1	Michael
Palin	Palin	k2eAgInSc4d1	Palin
Carol	Carol	k1gInSc4	Carol
Clevelandová	Clevelandový	k2eAgFnSc1d1	Clevelandová
Patricia	Patricius	k1gMnSc2	Patricius
Quinnová	Quinnová	k1gFnSc1	Quinnová
Mark	Mark	k1gMnSc1	Mark
Holmes	Holmes	k1gMnSc1	Holmes
Simon	Simon	k1gMnSc1	Simon
Jones	Jones	k1gMnSc1	Jones
Matt	Matt	k1gMnSc1	Matt
Frewer	Frewer	k1gMnSc1	Frewer
Film	film	k1gInSc4	film
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
i	i	k9	i
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kapitoly	kapitola	k1gFnPc1	kapitola
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
několik	několik	k4yIc4	několik
nezávislých	závislý	k2eNgInPc2d1	nezávislý
skečů	skeč	k1gInPc2	skeč
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Crimson	Crimson	k1gInSc1	Crimson
Permanent	permanent	k1gInSc1	permanent
Assurance	Assurance	k1gFnSc2	Assurance
-	-	kIx~	-
úvodní	úvodní	k2eAgInSc4d1	úvodní
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Terrym	Terrym	k1gInSc4	Terrym
Gilliamem	Gilliam	k1gInSc7	Gilliam
<g/>
.	.	kIx.	.
</s>
<s>
Satira	satira	k1gFnSc1	satira
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
nevybíravé	vybíravý	k2eNgFnPc4d1	nevybíravá
praktiky	praktika	k1gFnPc4	praktika
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
korporacích	korporace	k1gFnPc6	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnPc1d2	starší
úředníci	úředník	k1gMnPc1	úředník
se	se	k3xPyFc4	se
vzbouří	vzbouřit	k5eAaPmIp3nP	vzbouřit
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
mladším	mladý	k2eAgMnPc3d2	mladší
bezcitným	bezcitný	k2eAgMnPc3d1	bezcitný
šéfům	šéf	k1gMnPc3	šéf
<g/>
,	,	kIx,	,
ovládnou	ovládnout	k5eAaPmIp3nP	ovládnout
budovu	budova	k1gFnSc4	budova
a	a	k8xC	a
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
ji	on	k3xPp3gFnSc4	on
v	v	k7c4	v
"	"	kIx"	"
<g/>
pirátskou	pirátský	k2eAgFnSc4d1	pirátská
loď	loď	k1gFnSc4	loď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
plenit	plenit	k5eAaImF	plenit
komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
finanční	finanční	k2eAgInSc4d1	finanční
sektor	sektor	k1gInSc4	sektor
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výpadech	výpad	k1gInPc6	výpad
novodobých	novodobý	k2eAgMnPc2d1	novodobý
pirátů	pirát	k1gMnPc2	pirát
leží	ležet	k5eAaImIp3nP	ležet
finanční	finanční	k2eAgMnPc1d1	finanční
giganti	gigant	k1gMnPc1	gigant
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
odplouvá	odplouvat	k5eAaImIp3nS	odplouvat
a	a	k8xC	a
přepadává	přepadávat	k5eAaImIp3nS	přepadávat
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgFnSc1d1	moderní
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
tvaru	tvar	k1gInSc6	tvar
Země	zem	k1gFnSc2	zem
ukázaly	ukázat	k5eAaPmAgInP	ukázat
jako	jako	k9	jako
katastrofálně	katastrofálně	k6eAd1	katastrofálně
mylné	mylný	k2eAgNnSc1d1	mylné
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
film	film	k1gInSc1	film
začíná	začínat	k5eAaImIp3nS	začínat
scénkou	scénka	k1gFnSc7	scénka
s	s	k7c7	s
plovoucími	plovoucí	k2eAgFnPc7d1	plovoucí
rybami	ryba	k1gFnPc7	ryba
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
mají	mít	k5eAaImIp3nP	mít
tváře	tvář	k1gFnPc1	tvář
šesti	šest	k4xCc2	šest
členů	člen	k1gInPc2	člen
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
a	a	k8xC	a
věnují	věnovat	k5eAaImIp3nP	věnovat
se	se	k3xPyFc4	se
filozofické	filozofický	k2eAgFnSc3d1	filozofická
konverzaci	konverzace	k1gFnSc3	konverzace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc1	jejich
přítel	přítel	k1gMnSc1	přítel
(	(	kIx(	(
<g/>
ryba	ryba	k1gFnSc1	ryba
jménem	jméno	k1gNnSc7	jméno
Howard	Howarda	k1gFnPc2	Howarda
<g/>
)	)	kIx)	)
servírován	servírován	k2eAgInSc1d1	servírován
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
zákazníkovi	zákazník	k1gMnSc3	zákazník
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
zpívaná	zpívaný	k2eAgFnSc1d1	zpívaná
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
1	[number]	k4	1
<g/>
:	:	kIx,	:
Zázrak	zázrak	k1gInSc1	zázrak
zrození	zrození	k1gNnSc1	zrození
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
I	I	kA	I
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Miracle	Miracle	k1gFnSc2	Miracle
Of	Of	k1gMnSc1	Of
Birth	Birth	k1gMnSc1	Birth
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
skečů	skeč	k1gInPc2	skeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
přivážena	přivážen	k2eAgFnSc1d1	přivážena
na	na	k7c4	na
porodní	porodní	k2eAgInSc4d1	porodní
sál	sál	k1gInSc4	sál
<g/>
.	.	kIx.	.
</s>
<s>
Doktoři	doktor	k1gMnPc1	doktor
si	se	k3xPyFc3	se
nechají	nechat	k5eAaPmIp3nP	nechat
především	především	k6eAd1	především
přivézt	přivézt	k5eAaPmF	přivézt
různé	různý	k2eAgInPc4d1	různý
přístroje	přístroj	k1gInPc4	přístroj
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
jim	on	k3xPp3gMnPc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc1	něco
schází	scházet	k5eAaImIp3nS	scházet
-	-	kIx~	-
pacientka	pacientka	k1gFnSc1	pacientka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rodička	rodička	k1gFnSc1	rodička
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
má	mít	k5eAaImIp3nS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sálu	sál	k1gInSc2	sál
přijde	přijít	k5eAaPmIp3nS	přijít
ředitel	ředitel	k1gMnSc1	ředitel
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
dít	dít	k5eAaBmF	dít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
porodu	porod	k1gInSc6	porod
neví	vědět	k5eNaImIp3nS	vědět
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Doktoři	doktor	k1gMnPc1	doktor
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
odpovědím	odpověď	k1gFnPc3	odpověď
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
banální	banální	k2eAgFnSc4d1	banální
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
cenu	cena	k1gFnSc4	cena
drahých	drahý	k2eAgInPc2d1	drahý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
se	se	k3xPyFc4	se
s	s	k7c7	s
porodem	porod	k1gInSc7	porod
příliš	příliš	k6eAd1	příliš
nemažou	mazat	k5eNaImIp3nP	mazat
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc4	dítě
letmo	letmo	k6eAd1	letmo
ukážou	ukázat	k5eAaPmIp3nP	ukázat
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
strčí	strčit	k5eAaPmIp3nS	strčit
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
inkubátoru	inkubátor	k1gInSc2	inkubátor
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
doktor	doktor	k1gMnSc1	doktor
matce	matka	k1gFnSc3	matka
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
bude	být	k5eAaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
nalezne	naleznout	k5eAaPmIp3nS	naleznout
na	na	k7c6	na
videokazetě	videokazeta	k1gFnSc6	videokazeta
<g/>
.	.	kIx.	.
</s>
<s>
Zázrak	zázrak	k1gInSc1	zázrak
zrození	zrození	k1gNnSc2	zrození
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
2	[number]	k4	2
<g/>
:	:	kIx,	:
Třetí	třetí	k4xOgInSc1	třetí
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Miracle	Miracle	k1gFnSc2	Miracle
Of	Of	k1gMnSc1	Of
Birth	Birth	k1gMnSc1	Birth
<g/>
,	,	kIx,	,
part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Third	Third	k1gMnSc1	Third
World	World	k1gMnSc1	World
<g/>
)	)	kIx)	)
-	-	kIx~	-
děj	děj	k1gInSc1	děj
této	tento	k3xDgFnSc2	tento
skeče	skeč	k1gInSc2	skeč
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
Sheffieldu	Sheffield	k1gInSc2	Sheffield
<g/>
,	,	kIx,	,
Yorkshire	Yorkshir	k1gInSc5	Yorkshir
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
katolicky	katolicky	k6eAd1	katolicky
založené	založený	k2eAgFnSc2d1	založená
a	a	k8xC	a
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
přijde	přijít	k5eAaPmIp3nS	přijít
domů	dům	k1gInPc2	dům
a	a	k8xC	a
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
manželce	manželka	k1gFnSc3	manželka
a	a	k8xC	a
dětem	dítě	k1gFnPc3	dítě
(	(	kIx(	(
<g/>
kterých	který	k3yIgNnPc6	který
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavřeli	zavřít	k5eAaPmAgMnP	zavřít
válcovnu	válcovna	k1gFnSc4	válcovna
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nebudou	být	k5eNaImBp3nP	být
peníze	peníz	k1gInPc1	peníz
na	na	k7c6	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
prodají	prodat	k5eAaPmIp3nP	prodat
na	na	k7c4	na
vědecké	vědecký	k2eAgInPc4d1	vědecký
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesmím	smět	k5eNaImIp1nS	smět
nosit	nosit	k5eAaImF	nosit
ty	ten	k3xDgFnPc4	ten
gumičky	gumička	k1gFnPc4	gumička
na	na	k7c6	na
ocásku	ocásek	k1gInSc6	ocásek
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jinak	jinak	k6eAd1	jinak
udělali	udělat	k5eAaPmAgMnP	udělat
hodně	hodně	k6eAd1	hodně
dobrých	dobrý	k2eAgFnPc2d1	dobrá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
zachovali	zachovat	k5eAaPmAgMnP	zachovat
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
majestát	majestát	k1gInSc4	majestát
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
mystérium	mystérium	k1gNnSc1	mystérium
církve	církev	k1gFnSc2	církev
svaté	svatý	k2eAgFnSc2d1	svatá
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
následuje	následovat	k5eAaImIp3nS	následovat
hudební	hudební	k2eAgFnSc1d1	hudební
vložka	vložka	k1gFnSc1	vložka
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečná	konečný	k2eNgFnSc1d1	nekonečná
řada	řada	k1gFnSc1	řada
dětí	dítě	k1gFnPc2	dítě
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
ulici	ulice	k1gFnSc4	ulice
sleduje	sledovat	k5eAaImIp3nS	sledovat
scénu	scéna	k1gFnSc4	scéna
bohatý	bohatý	k2eAgMnSc1d1	bohatý
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
protestant	protestant	k1gMnSc1	protestant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
manželce	manželka	k1gFnSc3	manželka
výhody	výhoda	k1gFnSc2	výhoda
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
zejména	zejména	k9	zejména
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
dopřát	dopřát	k5eAaPmF	dopřát
sex	sex	k1gInSc4	sex
s	s	k7c7	s
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
,	,	kIx,	,
kdykoli	kdykoli	k6eAd1	kdykoli
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zamane	zamanout	k5eAaPmIp3nS	zamanout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
choť	choť	k1gFnSc1	choť
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
2	[number]	k4	2
<g/>
:	:	kIx,	:
Růst	růst	k1gInSc1	růst
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
II	II	kA	II
<g/>
:	:	kIx,	:
Growth	Growth	k1gMnSc1	Growth
and	and	k?	and
Learning	Learning	k1gInSc1	Learning
<g/>
)	)	kIx)	)
-	-	kIx~	-
část	část	k1gFnSc1	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
shromáždění	shromáždění	k1gNnSc1	shromáždění
hochů	hoch	k1gMnPc2	hoch
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Žáci	Žák	k1gMnPc1	Žák
zpívají	zpívat	k5eAaImIp3nP	zpívat
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
pak	pak	k6eAd1	pak
učitel	učitel	k1gMnSc1	učitel
probírá	probírat	k5eAaImIp3nS	probírat
s	s	k7c7	s
nedospělými	dospělý	k2eNgMnPc7d1	nedospělý
chlapci	chlapec	k1gMnPc7	chlapec
sexuální	sexuální	k2eAgFnSc7d1	sexuální
výchovu	výchova	k1gFnSc4	výchova
<g/>
,	,	kIx,	,
hodina	hodina	k1gFnSc1	hodina
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
názornou	názorný	k2eAgFnSc7d1	názorná
ukázkou	ukázka	k1gFnSc7	ukázka
soulože	soulož	k1gFnSc2	soulož
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
hrají	hrát	k5eAaImIp3nP	hrát
učitelé	učitel	k1gMnPc1	učitel
ragby	ragby	k1gNnSc4	ragby
proti	proti	k7c3	proti
žákům	žák	k1gMnPc3	žák
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
muži	muž	k1gMnPc1	muž
své	svůj	k3xOyFgMnPc4	svůj
svěřence	svěřenec	k1gMnPc4	svěřenec
vůbec	vůbec	k9	vůbec
nešetří	šetřit	k5eNaImIp3nS	šetřit
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
chlapci	chlapec	k1gMnPc1	chlapec
v	v	k7c6	v
bolestech	bolest	k1gFnPc6	bolest
svíjí	svíjet	k5eAaImIp3nP	svíjet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
učitelé	učitel	k1gMnPc1	učitel
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
<g/>
.	.	kIx.	.
</s>
<s>
Scénka	scénka	k1gFnSc1	scénka
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
absurdního	absurdní	k2eAgInSc2d1	absurdní
děje	děj	k1gInSc2	děj
uprostřed	uprostřed	k7c2	uprostřed
bitevního	bitevní	k2eAgNnSc2d1	bitevní
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
boj	boj	k1gInSc1	boj
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
III	III	kA	III
<g/>
:	:	kIx,	:
Fighting	Fighting	k1gInSc1	Fighting
Each	Each	k1gMnSc1	Each
Other	Other	k1gMnSc1	Other
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
zákopu	zákop	k1gInSc6	zákop
chtějí	chtít	k5eAaImIp3nP	chtít
muži	muž	k1gMnPc1	muž
věnovat	věnovat	k5eAaPmF	věnovat
dárky	dárek	k1gInPc4	dárek
svému	svůj	k3xOyFgInSc3	svůj
veliteli	velitel	k1gMnPc7	velitel
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgMnSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgMnSc6	druhý
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
hrstka	hrstka	k1gFnSc1	hrstka
přeživších	přeživší	k2eAgFnPc2d1	přeživší
se	se	k3xPyFc4	se
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
nakonec	nakonec	k6eAd1	nakonec
rozhádá	rozhádat	k5eAaPmIp3nS	rozhádat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgInSc1d1	další
skeči	skeč	k1gInPc7	skeč
pojaté	pojatý	k2eAgFnSc2d1	pojatá
jako	jako	k8xS	jako
parodie	parodie	k1gFnSc2	parodie
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcviku	výcvik	k1gInSc2	výcvik
komanduje	komandovat	k5eAaBmIp3nS	komandovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
šarže	šarže	k1gFnSc1	šarže
vojíny	vojín	k1gMnPc7	vojín
na	na	k7c6	na
cvičišti	cvičiště	k1gNnSc6	cvičiště
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
pochodovat	pochodovat	k5eAaImF	pochodovat
a	a	k8xC	a
seržant	seržant	k1gMnSc1	seržant
chce	chtít	k5eAaImIp3nS	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
si	se	k3xPyFc3	se
snad	snad	k9	snad
někdo	někdo	k3yInSc1	někdo
dovede	dovést	k5eAaPmIp3nS	dovést
představit	představit	k5eAaPmF	představit
lepší	dobrý	k2eAgFnSc4d2	lepší
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásí	přihlásit	k5eAaPmIp3nS	přihlásit
se	se	k3xPyFc4	se
vojín	vojín	k1gMnSc1	vojín
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
radši	rád	k6eAd2	rád
doma	doma	k6eAd1	doma
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Seržant	seržant	k1gMnSc1	seržant
jej	on	k3xPp3gMnSc4	on
za	za	k7c7	za
nimi	on	k3xPp3gNnPc7	on
pustí	pustit	k5eAaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
voják	voják	k1gMnSc1	voják
má	mít	k5eAaImIp3nS	mít
rozečtenou	rozečtený	k2eAgFnSc4d1	rozečtená
knížku	knížka	k1gFnSc4	knížka
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Seržant	seržant	k1gMnSc1	seržant
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
dovolí	dovolit	k5eAaPmIp3nS	dovolit
mumlaje	mumlat	k5eAaImSgInS	mumlat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgFnSc1d1	dnešní
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
na	na	k7c4	na
pytel	pytel	k1gInSc4	pytel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgInSc1d1	další
skeči	skeč	k1gInPc7	skeč
situované	situovaný	k2eAgFnSc2d1	situovaná
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
do	do	k7c2	do
období	období	k1gNnSc2	období
anglo-zulské	angloulský	k2eAgFnSc2d1	anglo-zulský
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
velitelé	velitel	k1gMnPc1	velitel
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
věnují	věnovat	k5eAaImIp3nP	věnovat
svým	svůj	k3xOyFgFnPc3	svůj
činnostem	činnost	k1gFnPc3	činnost
(	(	kIx(	(
<g/>
holení	holení	k1gNnSc4	holení
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
zuří	zuřit	k5eAaImIp3nS	zuřit
boj	boj	k1gInSc4	boj
a	a	k8xC	a
umírají	umírat	k5eAaImIp3nP	umírat
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
se	se	k3xPyFc4	se
ráno	ráno	k6eAd1	ráno
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
něco	něco	k6eAd1	něco
kouslo	kousnout	k5eAaPmAgNnS	kousnout
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
tím	ten	k3xDgInSc7	ten
příliš	příliš	k6eAd1	příliš
obtěžovat	obtěžovat	k5eAaImF	obtěžovat
doktora	doktor	k1gMnSc4	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
se	se	k3xPyFc4	se
dostaví	dostavit	k5eAaPmIp3nS	dostavit
a	a	k8xC	a
vyřkne	vyřknout	k5eAaPmIp3nS	vyřknout
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
tygr	tygr	k1gMnSc1	tygr
-	-	kIx~	-
muži	muž	k1gMnSc3	muž
chybí	chybit	k5eAaPmIp3nS	chybit
celá	celý	k2eAgFnSc1d1	celá
noha	noha	k1gFnSc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zorganizována	zorganizován	k2eAgFnSc1d1	zorganizována
pátrací	pátrací	k2eAgFnSc1d1	pátrací
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
objeví	objevit	k5eAaPmIp3nS	objevit
tygra	tygr	k1gMnSc4	tygr
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
2	[number]	k4	2
muže	muž	k1gMnPc4	muž
převlečené	převlečený	k2eAgNnSc1d1	převlečené
za	za	k7c4	za
tygra	tygr	k1gMnSc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Middle	Middle	k1gFnSc2	Middle
Of	Of	k1gFnSc1	Of
The	The	k1gMnSc1	The
Film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hlasatelka	hlasatelka	k1gFnSc1	hlasatelka
ohlásí	ohlásit	k5eAaPmIp3nS	ohlásit
střed	střed	k1gInSc4	střed
filmu	film	k1gInSc2	film
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
scénka	scénka	k1gFnSc1	scénka
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Najdi	najít	k5eAaPmRp2nS	najít
rybu	ryba	k1gFnSc4	ryba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
má	mít	k5eAaImIp3nS	mít
evokovat	evokovat	k5eAaBmF	evokovat
honosný	honosný	k2eAgInSc1d1	honosný
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
pohovka	pohovka	k1gFnSc1	pohovka
<g/>
,	,	kIx,	,
koberec	koberec	k1gInSc1	koberec
<g/>
,	,	kIx,	,
stojan	stojan	k1gInSc1	stojan
se	s	k7c7	s
svícemi	svíce	k1gFnPc7	svíce
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
patro	patro	k1gNnSc1	patro
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
střih	střih	k1gInSc4	střih
a	a	k8xC	a
pohled	pohled	k1gInSc4	pohled
do	do	k7c2	do
akvária	akvárium	k1gNnSc2	akvárium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ryby	ryba	k1gFnPc1	ryba
komentují	komentovat	k5eAaBmIp3nP	komentovat
předcházející	předcházející	k2eAgFnSc4d1	předcházející
scénku	scénka	k1gFnSc4	scénka
<g/>
.	.	kIx.	.
</s>
<s>
Čekají	čekat	k5eAaImIp3nP	čekat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
a	a	k8xC	a
uhodnou	uhodnout	k5eAaPmIp3nP	uhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Část	část	k1gFnSc1	část
4	[number]	k4	4
<g/>
:	:	kIx,	:
Střední	střední	k2eAgInSc1d1	střední
věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
IV	IV	kA	IV
<g/>
:	:	kIx,	:
Middle	Middle	k1gMnSc1	Middle
Age	Age	k1gMnSc1	Age
<g/>
)	)	kIx)	)
-	-	kIx~	-
pár	pár	k4xCyI	pár
amerických	americký	k2eAgMnPc2d1	americký
turistů	turist	k1gMnPc2	turist
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
občerstvit	občerstvit	k5eAaPmF	občerstvit
do	do	k7c2	do
restaurace	restaurace	k1gFnSc2	restaurace
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
havajské	havajský	k2eAgFnSc2d1	Havajská
hladomorny	hladomorna	k1gFnSc2	hladomorna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
pálena	pálen	k2eAgFnSc1d1	pálena
stará	starý	k2eAgFnSc1d1	stará
žena	žena	k1gFnSc1	žena
žhavým	žhavý	k2eAgNnSc7d1	žhavé
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
u	u	k7c2	u
zdí	zeď	k1gFnPc2	zeď
jsou	být	k5eAaImIp3nP	být
přivázány	přivázán	k2eAgFnPc1d1	přivázána
další	další	k2eAgFnPc1d1	další
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
číšníka	číšník	k1gMnSc2	číšník
si	se	k3xPyFc3	se
turisté	turist	k1gMnPc1	turist
objednají	objednat	k5eAaPmIp3nP	objednat
menu	menu	k1gNnSc4	menu
<g/>
:	:	kIx,	:
konverzaci	konverzace	k1gFnSc4	konverzace
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
o	o	k7c4	o
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
číšník	číšník	k1gMnSc1	číšník
je	on	k3xPp3gMnPc4	on
trochu	trochu	k6eAd1	trochu
uvede	uvést	k5eAaPmIp3nS	uvést
do	do	k7c2	do
tématu	téma	k1gNnSc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
rozhovor	rozhovor	k1gInSc1	rozhovor
však	však	k9	však
vázne	váznout	k5eAaImIp3nS	váznout
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
vracejí	vracet	k5eAaImIp3nP	vracet
lístek	lístek	k1gInSc4	lístek
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tahle	tenhle	k3xDgFnSc1	tenhle
konverzace	konverzace	k1gFnSc1	konverzace
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
moc	moc	k6eAd1	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Číšník	číšník	k1gMnSc1	číšník
se	se	k3xPyFc4	se
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
další	další	k2eAgFnSc1d1	další
<g/>
:	:	kIx,	:
transplantace	transplantace	k1gFnSc1	transplantace
živých	živý	k2eAgInPc2d1	živý
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
5	[number]	k4	5
<g/>
:	:	kIx,	:
Transplantace	transplantace	k1gFnSc2	transplantace
živých	živý	k2eAgInPc2d1	živý
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
V	V	kA	V
<g/>
:	:	kIx,	:
Live	Live	k1gNnSc4	Live
Organ	organon	k1gNnPc2	organon
Transplants	Transplantsa	k1gFnPc2	Transplantsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
drsnější	drsný	k2eAgFnSc6d2	drsnější
scénce	scénka	k1gFnSc6	scénka
navštíví	navštívit	k5eAaPmIp3nP	navštívit
dva	dva	k4xCgMnPc1	dva
"	"	kIx"	"
<g/>
záchranáři	záchranář	k1gMnPc1	záchranář
<g/>
"	"	kIx"	"
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
dárců	dárce	k1gMnPc2	dárce
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgNnSc4	svůj
játra	játra	k1gNnPc4	játra
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
a	a	k8xC	a
podepsal	podepsat	k5eAaPmAgMnS	podepsat
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
odebráním	odebrání	k1gNnSc7	odebrání
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
mu	on	k3xPp3gMnSc3	on
je	on	k3xPp3gNnSc4	on
odeberou	odebrat	k5eAaPmIp3nP	odebrat
zaživa	zaživa	k6eAd1	zaživa
(	(	kIx(	(
<g/>
odběr	odběr	k1gInSc1	odběr
prý	prý	k9	prý
stejně	stejně	k6eAd1	stejně
nikdo	nikdo	k3yNnSc1	nikdo
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
řve	řvát	k5eAaImIp3nS	řvát
a	a	k8xC	a
všude	všude	k6eAd1	všude
stříká	stříkat	k5eAaImIp3nS	stříkat
spousta	spousta	k1gFnSc1	spousta
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
vejde	vejít	k5eAaPmIp3nS	vejít
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
"	"	kIx"	"
<g/>
záchranářům	záchranář	k1gMnPc3	záchranář
<g/>
"	"	kIx"	"
o	o	k7c6	o
chybách	chyba	k1gFnPc6	chyba
svého	své	k1gNnSc2	své
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
vložce	vložka	k1gFnSc6	vložka
je	být	k5eAaImIp3nS	být
přemlouvána	přemlouván	k2eAgFnSc1d1	přemlouvána
k	k	k7c3	k
darování	darování	k1gNnSc3	darování
jater	játra	k1gNnPc2	játra
i	i	k8xC	i
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
ve	v	k7c4	v
Very	Very	k1gInPc4	Very
Big	Big	k1gFnSc2	Big
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
America	America	k1gFnSc1	America
probíhá	probíhat	k5eAaImIp3nS	probíhat
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
řečeny	řečen	k2eAgInPc4d1	řečen
2	[number]	k4	2
body	bod	k1gInPc4	bod
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
)	)	kIx)	)
lidé	člověk	k1gMnPc1	člověk
nenosí	nosit	k5eNaImIp3nP	nosit
příliš	příliš	k6eAd1	příliš
klobouky	klobouk	k1gInPc1	klobouk
a	a	k8xC	a
b	b	k?	b
<g/>
)	)	kIx)	)
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
"	"	kIx"	"
<g/>
pirátská	pirátský	k2eAgFnSc1d1	pirátská
loď	loď	k1gFnSc1	loď
<g/>
"	"	kIx"	"
z	z	k7c2	z
The	The	k1gFnSc2	The
Crimson	Crimson	k1gNnSc1	Crimson
Permanent	permanent	k1gInSc4	permanent
Assurance	Assurance	k1gFnSc2	Assurance
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
omlouvají	omlouvat	k5eAaImIp3nP	omlouvat
za	za	k7c4	za
proradný	proradný	k2eAgInSc4d1	proradný
atak	atak	k1gInSc4	atak
předfilmu	předfilm	k1gInSc2	předfilm
a	a	k8xC	a
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
loď-budovu	loďudův	k2eAgFnSc4d1	loď-budův
odvážných	odvážný	k2eAgMnPc2d1	odvážný
starců	stařec	k1gMnPc2	stařec
nechají	nechat	k5eAaPmIp3nP	nechat
spadnout	spadnout	k5eAaPmF	spadnout
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
6	[number]	k4	6
<g/>
:	:	kIx,	:
Podzim	podzim	k1gInSc1	podzim
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
VI	VI	kA	VI
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Autumn	Autumn	k1gMnSc1	Autumn
Years	Years	k1gInSc1	Years
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
první	první	k4xOgFnSc6	první
skeči	skeč	k1gInSc3	skeč
přikráčí	přikráčet	k5eAaImIp3nP	přikráčet
do	do	k7c2	do
luxusní	luxusní	k2eAgFnSc2d1	luxusní
restaurace	restaurace	k1gFnSc2	restaurace
velmi	velmi	k6eAd1	velmi
obézní	obézní	k2eAgMnSc1d1	obézní
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Poručí	poručit	k5eAaPmIp3nS	poručit
si	se	k3xPyFc3	se
donést	donést	k5eAaPmF	donést
kýbl	kýbl	k?	kýbl
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jej	on	k3xPp3gMnSc4	on
číšník	číšník	k1gMnSc1	číšník
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
vybranými	vybraný	k2eAgFnPc7d1	vybraná
pochutinami	pochutina	k1gFnPc7	pochutina
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
zvrací	zvracet	k5eAaImIp3nS	zvracet
do	do	k7c2	do
kýblu	kýblu	k?	kýblu
<g/>
.	.	kIx.	.
</s>
<s>
Pozvrací	pozvracet	k5eAaPmIp3nS	pozvracet
i	i	k9	i
jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
a	a	k8xC	a
uklízečku	uklízečka	k1gFnSc4	uklízečka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
povolána	povolán	k2eAgFnSc1d1	povolána
k	k	k7c3	k
úklidu	úklid	k1gInSc3	úklid
koberce	koberec	k1gInSc2	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Hosté	host	k1gMnPc1	host
jsou	být	k5eAaImIp3nP	být
šokováni	šokovat	k5eAaBmNgMnP	šokovat
a	a	k8xC	a
znechuceni	znechutit	k5eAaPmNgMnP	znechutit
<g/>
.	.	kIx.	.
</s>
<s>
Číšník	číšník	k1gMnSc1	číšník
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
klid	klid	k1gInSc4	klid
a	a	k8xC	a
dekorum	dekorum	k1gNnSc4	dekorum
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
praskne	prasknout	k5eAaPmIp3nS	prasknout
<g/>
,	,	kIx,	,
hosté	host	k1gMnPc1	host
restaurace	restaurace	k1gFnSc2	restaurace
jsou	být	k5eAaImIp3nP	být
potřísněni	potřísněn	k2eAgMnPc1d1	potřísněn
zbytky	zbytek	k1gInPc7	zbytek
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
částmi	část	k1gFnPc7	část
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
hromadně	hromadně	k6eAd1	hromadně
zvrací	zvracet	k5eAaImIp3nS	zvracet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
restaurace	restaurace	k1gFnSc1	restaurace
uklízí	uklízet	k5eAaImIp3nS	uklízet
<g/>
.	.	kIx.	.
</s>
<s>
Číšník	číšník	k1gMnSc1	číšník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prve	prve	k6eAd1	prve
obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
obézního	obézní	k2eAgMnSc4d1	obézní
zákazníka	zákazník	k1gMnSc4	zákazník
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
baví	bavit	k5eAaImIp3nS	bavit
s	s	k7c7	s
uklízečkou	uklízečka	k1gFnSc7	uklízečka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
dávkou	dávka	k1gFnSc7	dávka
skepse	skepse	k1gFnSc2	skepse
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
exkluzivních	exkluzivní	k2eAgInPc6d1	exkluzivní
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
ničemu	ničema	k1gFnSc4	ničema
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
číšník	číšník	k1gMnSc1	číšník
Gaston	Gaston	k1gInSc1	Gaston
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
kameru	kamera	k1gFnSc4	kamera
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
mu	on	k3xPp3gMnSc3	on
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obdarovával	obdarovávat	k5eAaImAgMnS	obdarovávat
druhé	druhý	k4xOgMnPc4	druhý
lidi	člověk	k1gMnPc4	člověk
radostí	radost	k1gFnPc2	radost
a	a	k8xC	a
šířil	šířit	k5eAaImAgMnS	šířit
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
klid	klid	k1gInSc1	klid
a	a	k8xC	a
spokojenost	spokojenost	k1gFnSc1	spokojenost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
číšníkem	číšník	k1gMnSc7	číšník
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
Gaston	Gaston	k1gInSc1	Gaston
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
rozzuří	rozzuřit	k5eAaPmIp3nS	rozzuřit
<g/>
,	,	kIx,	,
pošle	poslat	k5eAaPmIp3nS	poslat
kameru	kamera	k1gFnSc4	kamera
"	"	kIx"	"
<g/>
do	do	k7c2	do
prdele	prdel	k1gFnSc2	prdel
<g/>
"	"	kIx"	"
a	a	k8xC	a
chvatně	chvatně	k6eAd1	chvatně
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
7	[number]	k4	7
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
VII	VII	kA	VII
<g/>
:	:	kIx,	:
Death	Death	k1gMnSc1	Death
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
je	být	k5eAaImIp3nS	být
záběr	záběr	k1gInSc1	záběr
na	na	k7c4	na
prchajícího	prchající	k2eAgMnSc4d1	prchající
muže	muž	k1gMnSc4	muž
pronásledovaného	pronásledovaný	k2eAgInSc2d1	pronásledovaný
houfem	houf	k1gInSc7	houf
polonahých	polonahý	k2eAgNnPc2d1	polonahé
děvčat	děvče	k1gNnPc2	děvče
v	v	k7c6	v
přilbách	přilba	k1gFnPc6	přilba
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
zvolit	zvolit	k5eAaPmF	zvolit
způsob	způsob	k1gInSc4	způsob
své	svůj	k3xOyFgFnSc2	svůj
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
utíká	utíkat	k5eAaImIp3nS	utíkat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
ke	k	k7c3	k
skalnímu	skalní	k2eAgInSc3d1	skalní
útesu	útes	k1gInSc3	útes
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
děvčata	děvče	k1gNnPc1	děvče
dobíhají	dobíhat	k5eAaImIp3nP	dobíhat
<g/>
,	,	kIx,	,
zřítí	zřítit	k5eAaPmIp3nP	zřítit
se	se	k3xPyFc4	se
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
jámy	jáma	k1gFnSc2	jáma
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
postává	postávat	k5eAaImIp3nS	postávat
hlouček	hlouček	k1gInSc1	hlouček
truchlících	truchlící	k2eAgMnPc2d1	truchlící
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
skeči	skeč	k1gInSc6	skeč
si	se	k3xPyFc3	se
přijde	přijít	k5eAaPmIp3nS	přijít
"	"	kIx"	"
<g/>
chmurný	chmurný	k2eAgMnSc1d1	chmurný
žnec	žnec	k1gMnSc1	žnec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
smrťák	smrťák	k1gInSc1	smrťák
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
osazenstvo	osazenstvo	k1gNnSc4	osazenstvo
večírku	večírek	k1gInSc2	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
berou	brát	k5eAaImIp3nP	brát
jeho	jeho	k3xOp3gFnSc4	jeho
přítomnost	přítomnost	k1gFnSc4	přítomnost
lehkovážně	lehkovážně	k6eAd1	lehkovážně
<g/>
,	,	kIx,	,
nenuceně	nuceně	k6eNd1	nuceně
se	se	k3xPyFc4	se
ptají	ptat	k5eAaImIp3nP	ptat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gNnSc2	jejich
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Žnec	žnec	k1gMnSc1	žnec
jim	on	k3xPp3gFnPc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
pomazánka	pomazánka	k1gFnSc1	pomazánka
z	z	k7c2	z
lososa	losos	k1gMnSc2	losos
obsahující	obsahující	k2eAgInSc4d1	obsahující
botulotoxin	botulotoxin	k1gInSc4	botulotoxin
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
jejich	jejich	k3xOp3gFnPc4	jejich
duše	duše	k1gFnPc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
reklamuje	reklamovat	k5eAaImIp3nS	reklamovat
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pomazánku	pomazánka	k1gFnSc4	pomazánka
nejedla	jíst	k5eNaImAgFnS	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Duším	duše	k1gFnPc3	duše
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
následovat	následovat	k5eAaImF	následovat
žence	ženc	k1gMnPc4	ženc
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
tak	tak	k6eAd1	tak
nasednou	nasednout	k5eAaPmIp3nP	nasednout
do	do	k7c2	do
aut	auto	k1gNnPc2	auto
svých	svůj	k3xOyFgMnPc2	svůj
bývalých	bývalý	k2eAgMnPc2d1	bývalý
těl	tělo	k1gNnPc2	tělo
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obrovském	obrovský	k2eAgNnSc6d1	obrovské
sálu	sál	k1gInSc2	sál
shromážděno	shromáždit	k5eAaPmNgNnS	shromáždit
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
scén	scéna	k1gFnPc2	scéna
filmu	film	k1gInSc2	film
a	a	k8xC	a
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
přichází	přicházet	k5eAaImIp3nS	přicházet
zpívaná	zpívaný	k2eAgFnSc1d1	zpívaná
pasáž	pasáž	k1gFnSc1	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
End	End	k1gFnSc2	End
Of	Of	k1gFnSc1	Of
The	The	k1gMnSc1	The
Film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlasatelka	hlasatelka	k1gFnSc1	hlasatelka
oznámí	oznámit	k5eAaPmIp3nS	oznámit
konec	konec	k1gInSc4	konec
filmu	film	k1gInSc2	film
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
z	z	k7c2	z
režie	režie	k1gFnSc2	režie
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
obálku	obálka	k1gFnSc4	obálka
se	s	k7c7	s
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nic	nic	k3yNnSc1	nic
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Buďte	budit	k5eAaImRp2nP	budit
hodní	hodný	k2eAgMnPc1d1	hodný
k	k	k7c3	k
jiným	jiný	k2eAgMnPc3d1	jiný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
nejezte	jíst	k5eNaImRp2nP	jíst
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
semtam	semtam	k6eAd1	semtam
si	se	k3xPyFc3	se
přečtěte	přečíst	k5eAaPmRp2nP	přečíst
hezkou	hezký	k2eAgFnSc4d1	hezká
knížku	knížka	k1gFnSc4	knížka
<g/>
,	,	kIx,	,
choďte	chodit	k5eAaImRp2nP	chodit
na	na	k7c4	na
procházky	procházka	k1gFnPc4	procházka
a	a	k8xC	a
zkuste	zkusit	k5eAaPmRp2nP	zkusit
žít	žít	k5eAaImF	žít
harmonicky	harmonicky	k6eAd1	harmonicky
a	a	k8xC	a
v	v	k7c6	v
míru	mír	k1gInSc6	mír
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
všech	všecek	k3xTgNnPc2	všecek
vyznání	vyznání	k1gNnPc2	vyznání
a	a	k8xC	a
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Závěrem	závěrem	k6eAd1	závěrem
upozorní	upozornit	k5eAaPmIp3nS	upozornit
na	na	k7c4	na
přehlídku	přehlídka	k1gFnSc4	přehlídka
penisů	penis	k1gInPc2	penis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
následovat	následovat	k5eAaImF	následovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trochu	trochu	k6eAd1	trochu
zdvihla	zdvihnout	k5eAaPmAgFnS	zdvihnout
tlak	tlak	k1gInSc4	tlak
cenzorům	cenzor	k1gMnPc3	cenzor
a	a	k8xC	a
zapálila	zapálit	k5eAaPmAgFnS	zapálit
jiskru	jiskra	k1gFnSc4	jiskra
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
přimět	přimět	k5eAaPmF	přimět
diváky	divák	k1gMnPc4	divák
přesycené	přesycený	k2eAgFnPc4d1	přesycená
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvedli	zvednout	k5eAaPmAgMnP	zvednout
svá	svůj	k3xOyFgNnPc4	svůj
pozadí	pozadí	k1gNnPc4	pozadí
a	a	k8xC	a
šli	jít	k5eAaImAgMnP	jít
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Žádány	žádán	k2eAgFnPc1d1	žádána
jsou	být	k5eAaImIp3nP	být
nechutnosti	nechutnost	k1gFnPc1	nechutnost
<g/>
.	.	kIx.	.
</s>
<s>
Kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
poděla	poděla	k?	poděla
legrace	legrace	k1gFnSc1	legrace
<g/>
?	?	kIx.	?
</s>
<s>
Na	na	k7c6	na
tabuli	tabule	k1gFnSc6	tabule
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gFnSc2	The
Very	Vera	k1gFnSc2	Vera
Big	Big	k1gFnSc2	Big
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
America	Americ	k1gInSc2	Americ
(	(	kIx(	(
<g/>
Velmi	velmi	k6eAd1	velmi
velká	velký	k2eAgFnSc1d1	velká
americká	americký	k2eAgFnSc1d1	americká
korporace	korporace	k1gFnSc1	korporace
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
názvy	název	k1gInPc1	název
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
scénkách	scénka	k1gFnPc6	scénka
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
čitelné	čitelný	k2eAgFnPc1d1	čitelná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
slovní	slovní	k2eAgFnPc1d1	slovní
hříčky	hříčka	k1gFnPc1	hříčka
nebo	nebo	k8xC	nebo
vtípky	vtípek	k1gInPc1	vtípek
odkazující	odkazující	k2eAgInPc1d1	odkazující
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
skeče	skeč	k1gInPc4	skeč
ve	v	k7c6	v
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jméno	jméno	k1gNnSc4	jméno
firmy	firma	k1gFnSc2	firma
"	"	kIx"	"
<g/>
Liver	livra	k1gFnPc2	livra
Donors	Donors	k1gInSc1	Donors
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
scénku	scénka	k1gFnSc4	scénka
s	s	k7c7	s
darováním	darování	k1gNnSc7	darování
jater	játra	k1gNnPc2	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
viditelných	viditelný	k2eAgNnPc2d1	viditelné
jmen	jméno	k1gNnPc2	jméno
na	na	k7c4	na
tabuli	tabule	k1gFnSc4	tabule
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
zmíní	zmínit	k5eAaPmIp3nS	zmínit
americký	americký	k2eAgMnSc1d1	americký
turista	turista	k1gMnSc1	turista
jména	jméno	k1gNnSc2	jméno
dvou	dva	k4xCgMnPc2	dva
německých	německý	k2eAgMnPc2d1	německý
filosofů	filosof	k1gMnPc2	filosof
<g/>
:	:	kIx,	:
Arthura	Arthur	k1gMnSc2	Arthur
Schopenhauera	Schopenhauer	k1gMnSc2	Schopenhauer
a	a	k8xC	a
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Nietzsche	Nietzsch	k1gMnSc2	Nietzsch
<g/>
.	.	kIx.	.
</s>
