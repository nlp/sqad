<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
turnajů	turnaj	k1gInPc2	turnaj
mužů	muž	k1gMnPc2	muž
i	i	k8xC	i
žen	žena	k1gFnPc2	žena
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
konat	konat	k5eAaImF	konat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
i	i	k9	i
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
turnaje	turnaj	k1gInPc1	turnaj
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
i	i	k8xC	i
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
)	)	kIx)	)
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
</s>
</p>
<p>
<s>
Open	Open	k1gInSc1	Open
European	Europeany	k1gInPc2	Europeany
Championships	Championshipsa	k1gFnPc2	Championshipsa
a	a	k8xC	a
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
a	a	k8xC	a
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muži	muž	k1gMnPc1	muž
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Ženy	žena	k1gFnPc1	žena
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
florbale	florbal	k1gInSc5	florbal
</s>
</p>
