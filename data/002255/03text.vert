<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Švédské	švédský	k2eAgNnSc1d1	švédské
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
řeči	řeč	k1gFnSc6	řeč
Sverige	Sverig	k1gFnSc2	Sverig
[	[	kIx(	[
<g/>
svæ	svæ	k?	svæ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
severských	severský	k2eAgInPc2d1	severský
států	stát	k1gInPc2	stát
na	na	k7c6	na
Skandinávském	skandinávský	k2eAgInSc6d1	skandinávský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
pomocí	pomocí	k7c2	pomocí
unikátního	unikátní	k2eAgInSc2d1	unikátní
mostu-tunelu	mostuunel	k1gInSc2	mostu-tunel
přes	přes	k7c4	přes
průliv	průliv	k1gInSc4	průliv
Öresund	Öresunda	k1gFnPc2	Öresunda
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
449	[number]	k4	449
964	[number]	k4	964
km2	km2	k4	km2
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
9,6	[number]	k4	9,6
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
20	[number]	k4	20
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
koncentrováno	koncentrovat	k5eAaBmNgNnS	koncentrovat
především	především	k9	především
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
polovině	polovina	k1gFnSc6	polovina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
85	[number]	k4	85
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Švédska	Švédsko	k1gNnSc2	Švédsko
je	být	k5eAaImIp3nS	být
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
v	v	k7c4	v
nezávislý	závislý	k2eNgInSc4d1	nezávislý
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc4d1	jednotný
stát	stát	k1gInSc4	stát
již	již	k6eAd1	již
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
země	zem	k1gFnSc2	zem
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svá	svůj	k3xOyFgNnPc4	svůj
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
a	a	k8xC	a
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
většinu	většina	k1gFnSc4	většina
území	území	k1gNnPc2	území
dobytých	dobytý	k2eAgNnPc2d1	dobyté
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
však	však	k9	však
Švédsko	Švédsko	k1gNnSc1	Švédsko
opět	opět	k6eAd1	opět
přišlo	přijít	k5eAaPmAgNnS	přijít
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
polovina	polovina	k1gFnSc1	polovina
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
podrobena	podrobit	k5eAaPmNgFnS	podrobit
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
naposledy	naposledy	k6eAd1	naposledy
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vojenskými	vojenský	k2eAgInPc7d1	vojenský
prostředky	prostředek	k1gInPc7	prostředek
donutilo	donutit	k5eAaPmAgNnS	donutit
Norsko	Norsko	k1gNnSc4	Norsko
vstoupit	vstoupit	k5eAaPmF	vstoupit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
do	do	k7c2	do
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
mír	mír	k1gInSc4	mír
a	a	k8xC	a
země	země	k1gFnSc1	země
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
nezúčastněnosti	nezúčastněnost	k1gFnSc2	nezúčastněnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
status	status	k1gInSc4	status
neutrality	neutralita	k1gFnSc2	neutralita
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Švédsko	Švédsko	k1gNnSc1	Švédsko
konstituční	konstituční	k2eAgNnSc1d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
s	s	k7c7	s
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
demokracií	demokracie	k1gFnSc7	demokracie
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
zemí	zem	k1gFnPc2	zem
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
masiv	masiv	k1gInSc1	masiv
Skandinávských	skandinávský	k2eAgFnPc2d1	skandinávská
hor.	hor.	k?	hor.
Hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
koncentrována	koncentrovat	k5eAaBmNgFnS	koncentrovat
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
Švédska	Švédsko	k1gNnSc2	Švédsko
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
sociální	sociální	k2eAgFnSc2d1	sociální
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
ekologicky	ekologicky	k6eAd1	ekologicky
šetrnou	šetrný	k2eAgFnSc4d1	šetrná
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc4d1	moderní
a	a	k8xC	a
liberální	liberální	k2eAgFnSc4d1	liberální
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Gustavem	Gustav	k1gMnSc7	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Sverige	Sverige	k1gInSc1	Sverige
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Sveů	Sve	k1gInPc2	Sve
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
svear	svear	k1gInSc1	svear
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
suiones	suiones	k1gInSc1	suiones
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
obýval	obývat	k5eAaImAgMnS	obývat
oblast	oblast	k1gFnSc4	oblast
centrálního	centrální	k2eAgNnSc2d1	centrální
Švédska	Švédsko	k1gNnSc2	Švédsko
(	(	kIx(	(
<g/>
Svealand	Svealand	k1gInSc1	Svealand
<g/>
)	)	kIx)	)
-	-	kIx~	-
Swerige	Swerige	k1gFnSc1	Swerige
(	(	kIx(	(
<g/>
Svea	Sve	k2eAgFnSc1d1	Sve
rike	rike	k1gFnSc1	rike
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
Sveů	Sve	k1gMnPc2	Sve
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kmene	kmen	k1gInSc2	kmen
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pragermánského	pragermánský	k2eAgMnSc2d1	pragermánský
*	*	kIx~	*
<g/>
swihoniz	swihoniz	k1gInSc1	swihoniz
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Švédska	Švédsko	k1gNnSc2	Švédsko
bylo	být	k5eAaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3	nejstarší
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
jim	on	k3xPp3gMnPc3	on
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Švédské	švédský	k2eAgNnSc1d1	švédské
království	království	k1gNnSc1	království
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
98	[number]	k4	98
n.	n.	k?	n.
l.	l.	k?	l.
římský	římský	k2eAgMnSc1d1	římský
historik	historik	k1gMnSc1	historik
Tacitus	Tacitus	k1gMnSc1	Tacitus
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
Svealand	Svealanda	k1gFnPc2	Svealanda
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
Švédska	Švédsko	k1gNnSc2	Švédsko
spojení	spojení	k1gNnSc2	spojení
území	území	k1gNnSc2	území
Svealand	Svealando	k1gNnPc2	Svealando
a	a	k8xC	a
Götaland	Götalanda	k1gFnPc2	Götalanda
pod	pod	k7c4	pod
jednoho	jeden	k4xCgMnSc4	jeden
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
krále	král	k1gMnSc4	král
jménem	jméno	k1gNnSc7	jméno
Erik	Erik	k1gMnSc1	Erik
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
970	[number]	k4	970
<g/>
-	-	kIx~	-
<g/>
995	[number]	k4	995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
středověku	středověk	k1gInSc2	středověk
patřili	patřit	k5eAaImAgMnP	patřit
Švédové	Švéd	k1gMnPc1	Švéd
mezi	mezi	k7c7	mezi
Vikingy	Viking	k1gMnPc7	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Pokřesťanštění	pokřesťanštění	k1gNnSc1	pokřesťanštění
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jednotného	jednotný	k2eAgInSc2d1	jednotný
švédského	švédský	k2eAgInSc2d1	švédský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1389	[number]	k4	1389
se	se	k3xPyFc4	se
tři	tři	k4xCgInPc4	tři
státy	stát	k1gInPc1	stát
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
sjednotily	sjednotit	k5eAaPmAgInP	sjednotit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kalmarská	Kalmarský	k2eAgFnSc1d1	Kalmarská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzdorovali	vzdorovat	k5eAaImAgMnP	vzdorovat
Švédové	Švéd	k1gMnPc1	Švéd
snaze	snaha	k1gFnSc3	snaha
centralizovat	centralizovat	k5eAaBmF	centralizovat
moc	moc	k6eAd1	moc
pod	pod	k7c4	pod
korunu	koruna	k1gFnSc4	koruna
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
ozbrojené	ozbrojený	k2eAgInPc4d1	ozbrojený
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
odpojilo	odpojit	k5eAaPmAgNnS	odpojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Gustav	Gustav	k1gMnSc1	Gustav
Eriksson	Eriksson	k1gMnSc1	Eriksson
Vasa	Vasa	k1gMnSc1	Vasa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Gustav	Gustav	k1gMnSc1	Gustav
I.	I.	kA	I.
Vasa	Vasa	k1gMnSc1	Vasa
založil	založit	k5eAaPmAgMnS	založit
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
monarchii	monarchie	k1gFnSc4	monarchie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
moderní	moderní	k2eAgInSc1d1	moderní
švédský	švédský	k2eAgInSc1d1	švédský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Vasa	Vasa	k1gMnSc1	Vasa
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
Švédy	Švéd	k1gMnPc4	Švéd
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rychlého	rychlý	k2eAgInSc2d1	rychlý
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zařadil	zařadit	k5eAaPmAgMnS	zařadit
Švédsko	Švédsko	k1gNnSc4	Švédsko
mezi	mezi	k7c4	mezi
evropské	evropský	k2eAgFnPc4d1	Evropská
velmoci	velmoc	k1gFnPc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pozice	pozice	k1gFnSc1	pozice
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
hroutit	hroutit	k5eAaImF	hroutit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
carské	carský	k2eAgNnSc1d1	carské
Rusko	Rusko	k1gNnSc1	Rusko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	s	k7c7	s
státy	stát	k1gInPc7	stát
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
severské	severský	k2eAgFnSc6d1	severská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
východní	východní	k2eAgFnSc2d1	východní
poloviny	polovina	k1gFnSc2	polovina
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Finské	finský	k2eAgNnSc4d1	finské
velkoknížectví	velkoknížectví	k1gNnSc4	velkoknížectví
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
historie	historie	k1gFnSc1	historie
Švédska	Švédsko	k1gNnSc2	Švédsko
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
pokojná	pokojný	k2eAgFnSc1d1	pokojná
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
válkou	válka	k1gFnSc7	válka
byla	být	k5eAaImAgFnS	být
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
Norsku	Norsko	k1gNnSc3	Norsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Švédskem	Švédsko	k1gNnSc7	Švédsko
řízená	řízený	k2eAgFnSc1d1	řízená
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
unie	unie	k1gFnSc1	unie
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neutrální	neutrální	k2eAgMnSc1d1	neutrální
i	i	k8xC	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
neangažovalo	angažovat	k5eNaBmAgNnS	angažovat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
země	zem	k1gFnPc4	zem
členem	člen	k1gInSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
vojenské	vojenský	k2eAgFnSc2d1	vojenská
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Skandinávském	skandinávský	k2eAgInSc6d1	skandinávský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Botnického	botnický	k2eAgInSc2d1	botnický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Švédsko	Švédsko	k1gNnSc1	Švédsko
od	od	k7c2	od
Norska	Norsko	k1gNnSc2	Norsko
Skandinávské	skandinávský	k2eAgNnSc4d1	skandinávské
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
země	zem	k1gFnSc2	zem
činí	činit	k5eAaImIp3nS	činit
449	[number]	k4	449
964	[number]	k4	964
km2	km2	k4	km2
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
je	být	k5eAaImIp3nS	být
54	[number]	k4	54
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Norrland	Norrland	k1gInSc1	Norrland
zabírá	zabírat	k5eAaImIp3nS	zabírat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hornatou	hornatý	k2eAgFnSc4d1	hornatá
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
rozlehlými	rozlehlý	k2eAgInPc7d1	rozlehlý
lesy	les	k1gInPc7	les
a	a	k8xC	a
velkými	velký	k2eAgFnPc7d1	velká
zásobami	zásoba	k1gFnPc7	zásoba
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Svealand	Svealand	k1gInSc1	Svealand
má	mít	k5eAaImIp3nS	mít
zvlněné	zvlněný	k2eAgInPc4d1	zvlněný
hřebeny	hřeben	k1gInPc4	hřeben
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
tisíc	tisíc	k4xCgInSc1	tisíc
švédských	švédský	k2eAgNnPc2d1	švédské
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Götaland	Götaland	k1gInSc1	Götaland
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kamenité	kamenitý	k2eAgFnPc4d1	kamenitá
vrchoviny	vrchovina	k1gFnPc4	vrchovina
Små	Små	k1gFnSc2	Små
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc2d1	bohatá
roviny	rovina	k1gFnSc2	rovina
Skå	Skå	k1gFnSc2	Skå
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
území	území	k1gNnSc6	území
Švédska	Švédsko	k1gNnSc2	Švédsko
leží	ležet	k5eAaImIp3nS	ležet
za	za	k7c7	za
Severním	severní	k2eAgInSc7d1	severní
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podíl	podíl	k1gInSc1	podíl
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Gotland	Gotland	k1gInSc4	Gotland
a	a	k8xC	a
Öland	Öland	k1gInSc4	Öland
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
Vänern	Vänern	k1gInSc4	Vänern
a	a	k8xC	a
Vättern	Vättern	k1gInSc4	Vättern
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
polohu	poloha	k1gFnSc4	poloha
má	mít	k5eAaImIp3nS	mít
Švédsko	Švédsko	k1gNnSc1	Švédsko
převážně	převážně	k6eAd1	převážně
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
teplému	teplý	k2eAgInSc3d1	teplý
Golfskému	golfský	k2eAgInSc3d1	golfský
proudu	proud	k1gInSc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Švédska	Švédsko	k1gNnSc2	Švédsko
převládají	převládat	k5eAaImIp3nP	převládat
listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
borovice	borovice	k1gFnPc4	borovice
a	a	k8xC	a
smrky	smrk	k1gInPc4	smrk
a	a	k8xC	a
na	na	k7c6	na
úplném	úplný	k2eAgInSc6d1	úplný
severu	sever	k1gInSc6	sever
břízy	bříza	k1gFnSc2	bříza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
severního	severní	k2eAgNnSc2d1	severní
Švédska	Švédsko	k1gNnSc2	Švédsko
převládá	převládat	k5eAaImIp3nS	převládat
subarktické	subarktický	k2eAgNnSc1d1	subarktické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
v	v	k7c4	v
určité	určitý	k2eAgInPc4d1	určitý
letní	letní	k2eAgInPc4d1	letní
dny	den	k1gInPc4	den
slunce	slunce	k1gNnSc2	slunce
vůbec	vůbec	k9	vůbec
nezapadá	zapadat	k5eNaPmIp3nS	zapadat
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
naopak	naopak	k6eAd1	naopak
vůbec	vůbec	k9	vůbec
nevychází	vycházet	k5eNaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
lze	lze	k6eAd1	lze
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
systémem	systém	k1gInSc7	systém
webových	webový	k2eAgFnPc2d1	webová
kamer	kamera	k1gFnPc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvacet	dvacet	k4xCc4	dvacet
osm	osm	k4xCc4	osm
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
6326,4	[number]	k4	6326,4
km2	km2	k4	km2
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabírá	zabírat	k5eAaImIp3nS	zabírat
1,4	[number]	k4	1,4
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
Finsko	Finsko	k1gNnSc1	Finsko
Největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
švédského	švédský	k2eAgNnSc2d1	švédské
území	území	k1gNnSc2	území
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
výběžku	výběžek	k1gInSc2	výběžek
a	a	k8xC	a
hraničního	hraniční	k2eAgInSc2d1	hraniční
hřebene	hřeben	k1gInSc2	hřeben
Skandinávského	skandinávský	k2eAgNnSc2d1	skandinávské
pohoří	pohoří	k1gNnSc2	pohoří
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Baltskému	baltský	k2eAgInSc3d1	baltský
štítu	štít	k1gInSc3	štít
<g/>
,	,	kIx,	,
složeného	složený	k2eAgNnSc2d1	složené
především	především	k9	především
ze	z	k7c2	z
žul	žula	k1gFnPc2	žula
a	a	k8xC	a
rul	rula	k1gFnPc2	rula
<g/>
.	.	kIx.	.
</s>
<s>
Ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pokrýval	pokrývat	k5eAaImAgMnS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
mocnosti	mocnost	k1gFnSc3	mocnost
až	až	k6eAd1	až
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
odstranil	odstranit	k5eAaPmAgInS	odstranit
pokryv	pokryv	k1gInSc1	pokryv
starých	starý	k2eAgFnPc2d1	stará
zvětralin	zvětralina	k1gFnPc2	zvětralina
<g/>
,	,	kIx,	,
zbrousil	zbrousit	k5eAaPmAgMnS	zbrousit
krystalické	krystalický	k2eAgFnSc2d1	krystalická
horniny	hornina	k1gFnSc2	hornina
skalního	skalní	k2eAgInSc2d1	skalní
podkladu	podklad	k1gInSc2	podklad
do	do	k7c2	do
zaoblených	zaoblený	k2eAgInPc2d1	zaoblený
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
morény	moréna	k1gFnPc4	moréna
<g/>
.	.	kIx.	.
</s>
<s>
Hlinitojílovité	hlinitojílovitý	k2eAgInPc1d1	hlinitojílovitý
nánosy	nános	k1gInPc1	nános
z	z	k7c2	z
tavných	tavný	k2eAgFnPc2d1	tavná
vod	voda	k1gFnPc2	voda
ledovce	ledovec	k1gInSc2	ledovec
a	a	k8xC	a
mořských	mořský	k2eAgFnPc2d1	mořská
usazenin	usazenina	k1gFnPc2	usazenina
dnes	dnes	k6eAd1	dnes
představují	představovat	k5eAaImIp3nP	představovat
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
orné	orný	k2eAgFnPc4d1	orná
půdy	půda	k1gFnPc4	půda
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
-	-	kIx~	-
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Švédsku	Švédsko	k1gNnSc6	Švédsko
mezi	mezi	k7c7	mezi
Stockholmem	Stockholm	k1gInSc7	Stockholm
a	a	k8xC	a
Göteborgem	Göteborg	k1gInSc7	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
králem	král	k1gMnSc7	král
je	být	k5eAaImIp3nS	být
Carl	Carl	k1gMnSc1	Carl
XVI	XVI	kA	XVI
Gustaf	Gustaf	k1gMnSc1	Gustaf
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
nejliberálnějších	liberální	k2eAgFnPc6d3	nejliberálnější
zemi	zem	k1gFnSc4	zem
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
sociálního	sociální	k2eAgInSc2d1	sociální
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
soustavy	soustava	k1gFnPc4	soustava
soudů	soud	k1gInPc2	soud
-	-	kIx~	-
obecné	obecný	k2eAgInPc4d1	obecný
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
trestními	trestní	k2eAgInPc7d1	trestní
a	a	k8xC	a
občanskoprávními	občanskoprávní	k2eAgInPc7d1	občanskoprávní
případy	případ	k1gInPc7	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
obecné	obecný	k2eAgInPc1d1	obecný
správní	správní	k2eAgInPc1d1	správní
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
správními	správní	k2eAgFnPc7d1	správní
věcmi	věc	k1gFnPc7	věc
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
stupně	stupeň	k1gInPc1	stupeň
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
-	-	kIx~	-
okresní	okresní	k2eAgInPc1d1	okresní
soudy	soud	k1gInPc1	soud
(	(	kIx(	(
<g/>
tingsrätt	tingsrätt	k1gInSc1	tingsrätt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvolací	odvolací	k2eAgInPc1d1	odvolací
soudy	soud	k1gInPc1	soud
(	(	kIx(	(
<g/>
hovrätt	hovrätt	k1gInSc1	hovrätt
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
Högsta	Högsta	k1gMnSc1	Högsta
domstolen	domstolit	k5eAaPmNgMnS	domstolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
správní	správní	k2eAgNnSc1d1	správní
soudnictví	soudnictví	k1gNnSc1	soudnictví
je	být	k5eAaImIp3nS	být
trojstupňové	trojstupňový	k2eAgNnSc1d1	trojstupňové
a	a	k8xC	a
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
krajských	krajský	k2eAgInPc2d1	krajský
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
länsrätt	länsrätt	k1gInSc1	länsrätt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
správních	správní	k2eAgInPc2d1	správní
odvolacích	odvolací	k2eAgInPc2d1	odvolací
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
kammarrätt	kammarrätt	k5eAaPmF	kammarrätt
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Regeringsrätten	Regeringsrätten	k2eAgMnSc1d1	Regeringsrätten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
-	-	kIx~	-
v	v	k7c6	v
EU	EU	kA	EU
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
Švédsko	Švédsko	k1gNnSc1	Švédsko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
neutrální	neutrální	k2eAgInSc1d1	neutrální
stát	stát	k1gInSc1	stát
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
vojenské	vojenský	k2eAgFnSc2d1	vojenská
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
účastní	účastnit	k5eAaImIp3nP	účastnit
mírových	mírový	k2eAgFnPc2d1	mírová
misí	mise	k1gFnPc2	mise
v	v	k7c6	v
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
,	,	kIx,	,
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
,	,	kIx,	,
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
Čadu	Čad	k1gInSc6	Čad
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
složky	složka	k1gFnPc4	složka
<g/>
:	:	kIx,	:
pozemní	pozemní	k2eAgNnSc1d1	pozemní
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
a	a	k8xC	a
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
mělo	mít	k5eAaImAgNnS	mít
brannou	branný	k2eAgFnSc4d1	Branná
povinnost	povinnost	k1gFnSc4	povinnost
od	od	k7c2	od
18	[number]	k4	18
do	do	k7c2	do
47	[number]	k4	47
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
Švéd	Švéd	k1gMnSc1	Švéd
musel	muset	k5eAaImAgMnS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
minimálně	minimálně	k6eAd1	minimálně
7	[number]	k4	7
měsíců	měsíc	k1gInPc2	měsíc
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pozemních	pozemní	k2eAgNnPc2d1	pozemní
vojsk	vojsko	k1gNnPc2	vojsko
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
sloužit	sloužit	k5eAaImF	sloužit
sedm	sedm	k4xCc4	sedm
a	a	k8xC	a
půl	půl	k1xP	půl
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
u	u	k7c2	u
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
mezi	mezi	k7c7	mezi
8	[number]	k4	8
a	a	k8xC	a
15	[number]	k4	15
měsíci	měsíc	k1gInPc7	měsíc
a	a	k8xC	a
u	u	k7c2	u
letectva	letectvo	k1gNnSc2	letectvo
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
služby	služba	k1gFnSc2	služba
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
od	od	k7c2	od
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
povinné	povinný	k2eAgFnSc2d1	povinná
služby	služba	k1gFnSc2	služba
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
každý	každý	k3xTgInSc1	každý
povolán	povolán	k2eAgInSc1d1	povolán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
věku	věk	k1gInSc2	věk
47	[number]	k4	47
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
mohly	moct	k5eAaImAgFnP	moct
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
vstoupit	vstoupit	k5eAaPmF	vstoupit
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kraje	kraj	k1gInSc2	kraj
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Švédska	Švédsko	k1gNnSc2	Švédsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
úrovních	úroveň	k1gFnPc6	úroveň
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
na	na	k7c4	na
21	[number]	k4	21
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
län	län	k?	län
<g/>
)	)	kIx)	)
a	a	k8xC	a
290	[number]	k4	290
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
kommun	kommun	k1gInSc1	kommun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kraji	kraj	k1gInSc6	kraj
působí	působit	k5eAaImIp3nS	působit
krajské	krajský	k2eAgNnSc1d1	krajské
představenstvo	představenstvo	k1gNnSc1	představenstvo
(	(	kIx(	(
<g/>
länsstyrelse	länsstyrelse	k1gFnSc1	länsstyrelse
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
guvernérem	guvernér	k1gMnSc7	guvernér
(	(	kIx(	(
<g/>
landshövding	landshövding	k1gInSc4	landshövding
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jmenované	jmenovaný	k2eAgInPc1d1	jmenovaný
vládou	vláda	k1gFnSc7	vláda
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
krajská	krajský	k2eAgFnSc1d1	krajská
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
landsting	landsting	k1gInSc1	landsting
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volená	volená	k1gFnSc1	volená
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
obce	obec	k1gFnPc4	obec
(	(	kIx(	(
<g/>
kommun	kommun	k1gMnSc1	kommun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
úrovni	úroveň	k1gFnSc6	úroveň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc1d1	sociální
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc1	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
odvoz	odvoz	k1gInSc1	odvoz
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
obce	obec	k1gFnSc2	obec
stojí	stát	k5eAaImIp3nS	stát
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
(	(	kIx(	(
<g/>
kommunfullmäktige	kommunfullmäktige	k1gNnSc1	kommunfullmäktige
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volené	volený	k2eAgFnPc1d1	volená
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
obecní	obecní	k2eAgFnSc4d1	obecní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
kommunstyrelse	kommunstyrelse	k6eAd1	kommunstyrelse
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejího	její	k3xOp3gMnSc4	její
předsedu	předseda	k1gMnSc4	předseda
(	(	kIx(	(
<g/>
kommunstyrelsens	kommunstyrelsens	k6eAd1	kommunstyrelsens
ordförande	ordförand	k1gInSc5	ordförand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
obec	obec	k1gFnSc1	obec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
obvykle	obvykle	k6eAd1	obvykle
město	město	k1gNnSc1	město
či	či	k8xC	či
větší	veliký	k2eAgNnSc1d2	veliký
sídlo	sídlo	k1gNnSc1	sídlo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
bez	bez	k7c2	bez
vlastní	vlastní	k2eAgFnSc2d1	vlastní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
málo	málo	k6eAd1	málo
osídlených	osídlený	k2eAgFnPc6d1	osídlená
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
mají	mít	k5eAaImIp3nP	mít
samosprávné	samosprávný	k2eAgFnPc1d1	samosprávná
obce	obec	k1gFnPc1	obec
značnou	značný	k2eAgFnSc4d1	značná
rozlohu	rozloha	k1gFnSc4	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
rozlohu	rozloha	k1gFnSc4	rozloha
má	mít	k5eAaImIp3nS	mít
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
Kiruna	Kiruna	k1gFnSc1	Kiruna
(	(	kIx(	(
<g/>
19	[number]	k4	19
446	[number]	k4	446
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pilířům	pilíř	k1gInPc3	pilíř
prosperující	prosperující	k2eAgFnSc2d1	prosperující
švédské	švédský	k2eAgFnSc2d1	švédská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
patří	patřit	k5eAaImIp3nP	patřit
přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
-	-	kIx~	-
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
poměrně	poměrně	k6eAd1	poměrně
štědrou	štědrý	k2eAgFnSc4d1	štědrá
sociální	sociální	k2eAgFnSc4d1	sociální
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vysoký	vysoký	k2eAgInSc1d1	vysoký
životní	životní	k2eAgInSc1d1	životní
standard	standard	k1gInSc1	standard
<g/>
.	.	kIx.	.
</s>
<s>
Výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
tvoří	tvořit	k5eAaImIp3nS	tvořit
51,3	[number]	k4	51,3
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
číslo	číslo	k1gNnSc4	číslo
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
odvětvím	odvětví	k1gNnSc7	odvětví
primárního	primární	k2eAgInSc2d1	primární
sektoru	sektor	k1gInSc2	sektor
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
je	být	k5eAaImIp3nS	být
lesní	lesní	k2eAgNnSc1d1	lesní
hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
45	[number]	k4	45
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
lesních	lesní	k2eAgFnPc2d1	lesní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celkem	celkem	k6eAd1	celkem
62	[number]	k4	62
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
i	i	k9	i
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
jsou	být	k5eAaImIp3nP	být
kontrolovány	kontrolovat	k5eAaImNgFnP	kontrolovat
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
produkční	produkční	k2eAgFnSc7d1	produkční
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
Norrland	Norrland	k1gInSc1	Norrland
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
řeky	řeka	k1gFnPc1	řeka
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
množství	množství	k1gNnSc2	množství
pil	pít	k5eAaImAgInS	pít
a	a	k8xC	a
také	také	k9	také
papírny	papírna	k1gFnPc1	papírna
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Botnického	botnický	k2eAgInSc2d1	botnický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vedle	vedle	k7c2	vedle
stavebního	stavební	k2eAgNnSc2d1	stavební
dříví	dříví	k1gNnSc2	dříví
a	a	k8xC	a
dřevní	dřevní	k2eAgFnSc2d1	dřevní
hmoty	hmota	k1gFnSc2	hmota
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
papíru	papír	k1gInSc2	papír
také	také	k9	také
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
terpentýn	terpentýn	k1gInSc1	terpentýn
<g/>
,	,	kIx,	,
barviva	barvivo	k1gNnPc1	barvivo
a	a	k8xC	a
umělé	umělý	k2eAgFnPc1d1	umělá
hmoty	hmota	k1gFnPc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
výrobu	výroba	k1gFnSc4	výroba
jsou	být	k5eAaImIp3nP	být
příznivé	příznivý	k2eAgFnPc1d1	příznivá
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
tvoří	tvořit	k5eAaImIp3nS	tvořit
méně	málo	k6eAd2	málo
než	než	k8xS	než
7	[number]	k4	7
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
dokáže	dokázat	k5eAaPmIp3nS	dokázat
uspokojit	uspokojit	k5eAaPmF	uspokojit
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
domácí	domácí	k2eAgFnSc2d1	domácí
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Nejintenzivněji	intenzivně	k6eAd3	intenzivně
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
úrodném	úrodný	k2eAgNnSc6d1	úrodné
Skå	Skå	k1gMnPc4	Skå
v	v	k7c6	v
zázemí	zázemí	k1gNnSc4	zázemí
Malmö	Malmö	k1gFnSc2	Malmö
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
vegetačním	vegetační	k2eAgNnSc6d1	vegetační
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ječmen	ječmen	k1gInSc4	ječmen
<g/>
,	,	kIx,	,
oves	oves	k1gInSc4	oves
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc4	brambor
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
krmné	krmný	k2eAgFnPc4d1	krmná
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
velikosti	velikost	k1gFnSc6	velikost
35	[number]	k4	35
ha	ha	kA	ha
totiž	totiž	k9	totiž
jasně	jasně	k6eAd1	jasně
dominuje	dominovat	k5eAaImIp3nS	dominovat
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
vepřů	vepř	k1gMnPc2	vepř
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
je	být	k5eAaImIp3nS	být
rozsah	rozsah	k1gInSc1	rozsah
chovu	chov	k1gInSc2	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
norků	norek	k1gMnPc2	norek
a	a	k8xC	a
lišek	liška	k1gFnPc2	liška
včetně	včetně	k7c2	včetně
polárních	polární	k2eAgFnPc2d1	polární
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
stačí	stačit	k5eAaBmIp3nS	stačit
pokrýt	pokrýt	k5eAaPmF	pokrýt
domácí	domácí	k2eAgFnSc4d1	domácí
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větší	veliký	k2eAgInSc1d2	veliký
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
efekt	efekt	k1gInSc1	efekt
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
sledi	sleď	k1gMnPc1	sleď
<g/>
,	,	kIx,	,
makrely	makrela	k1gFnPc1	makrela
a	a	k8xC	a
tresky	treska	k1gFnPc1	treska
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
mechanizace	mechanizace	k1gFnSc1	mechanizace
a	a	k8xC	a
produktivita	produktivita	k1gFnSc1	produktivita
výroby	výroba	k1gFnSc2	výroba
podíl	podíl	k1gInSc1	podíl
zaměstnaných	zaměstnaný	k1gMnPc2	zaměstnaný
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc6	lesnictví
a	a	k8xC	a
rybolovu	rybolov	k1gInSc6	rybolov
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
těžební	těžební	k2eAgInSc4d1	těžební
průmysl	průmysl	k1gInSc4	průmysl
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
střediska	středisko	k1gNnPc4	středisko
a	a	k8xC	a
firmy	firma	k1gFnPc4	firma
<g/>
:	:	kIx,	:
ABB	ABB	kA	ABB
Electrolux	Electrolux	k1gInSc1	Electrolux
Err-Bee	Err-Bee	k1gNnSc1	Err-Bee
Hagmans	Hagmansa	k1gFnPc2	Hagmansa
Husqvarna	Husqvarno	k1gNnSc2	Husqvarno
H	H	kA	H
<g/>
&	&	k?	&
<g/>
M	M	kA	M
IKEA	IKEA	kA	IKEA
JBT	JBT	kA	JBT
FoodTech	FoodTech	k1gInSc4	FoodTech
Koenigsegg	Koenigsegga	k1gFnPc2	Koenigsegga
Oriflame	Oriflam	k1gInSc5	Oriflam
SAAB	SAAB	kA	SAAB
Saab	Saab	k1gInSc1	Saab
Automobile	automobil	k1gInSc6	automobil
Scania	Scanium	k1gNnSc2	Scanium
SKF	SKF	kA	SKF
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Tieto	Tiet	k2eAgNnSc1d1	Tiet
Volvo	Volvo	k1gNnSc1	Volvo
Gastronomie	gastronomie	k1gFnSc2	gastronomie
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
imigraci	imigrace	k1gFnSc3	imigrace
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Stockholmu	Stockholm	k1gInSc2	Stockholm
spousta	spousta	k1gFnSc1	spousta
etnických	etnický	k2eAgFnPc2d1	etnická
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
jsou	být	k5eAaImIp3nP	být
Švédové	Švéd	k1gMnPc1	Švéd
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
jíst	jíst	k5eAaImF	jíst
exotická	exotický	k2eAgNnPc4d1	exotické
jídla	jídlo	k1gNnPc4	jídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
italská	italský	k2eAgNnPc1d1	italské
<g/>
,	,	kIx,	,
řecká	řecký	k2eAgNnPc1d1	řecké
nebo	nebo	k8xC	nebo
španělská	španělský	k2eAgNnPc1d1	španělské
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
jako	jako	k9	jako
tradiční	tradiční	k2eAgFnPc4d1	tradiční
místní	místní	k2eAgFnPc4d1	místní
gastronomické	gastronomický	k2eAgFnPc4d1	gastronomická
speciality	specialita	k1gFnPc4	specialita
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
plody	plod	k1gInPc1	plod
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
sobí	sobí	k2eAgNnSc1d1	sobí
a	a	k8xC	a
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
tradičních	tradiční	k2eAgNnPc2d1	tradiční
jídel	jídlo	k1gNnPc2	jídlo
také	také	k9	také
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
švédskými	švédský	k2eAgFnPc7d1	švédská
oslavami	oslava	k1gFnPc7	oslava
a	a	k8xC	a
svátky	svátek	k1gInPc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
speciality	specialita	k1gFnPc1	specialita
:	:	kIx,	:
Renstek	Renstek	k1gInSc1	Renstek
<g/>
=	=	kIx~	=
sobí	sobí	k2eAgInSc1d1	sobí
steak	steak	k1gInSc1	steak
s	s	k7c7	s
brusinkami	brusinka	k1gFnPc7	brusinka
a	a	k8xC	a
se	s	k7c7	s
smetanovými	smetanový	k2eAgFnPc7d1	smetanová
bramborami	brambora	k1gFnPc7	brambora
Köttbullar	Köttbullara	k1gFnPc2	Köttbullara
<g/>
=	=	kIx~	=
masové	masový	k2eAgFnPc1d1	masová
kuličky	kulička	k1gFnPc1	kulička
s	s	k7c7	s
brusinkami	brusinka	k1gFnPc7	brusinka
a	a	k8xC	a
brambory	brambor	k1gInPc7	brambor
nebo	nebo	k8xC	nebo
salátem	salát	k1gInSc7	salát
z	z	k7c2	z
červené	červený	k2eAgFnSc2d1	červená
řepy	řepa	k1gFnSc2	řepa
s	s	k7c7	s
majonézou	majonéza	k1gFnSc7	majonéza
Makka	Makka	k1gFnSc1	Makka
<g/>
=	=	kIx~	=
<g/>
druh	druh	k1gInSc1	druh
obloženého	obložený	k2eAgInSc2d1	obložený
chleba	chléb	k1gInSc2	chléb
Smöra	Smör	k1gInSc2	Smör
bröd	bröda	k1gFnPc2	bröda
<g/>
=	=	kIx~	=
<g/>
chleb	chlebit	k5eAaImRp2nS	chlebit
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
<g/>
,	,	kIx,	,
u	u	k7c2	u
Švédů	Švéd	k1gMnPc2	Švéd
nejběžnejší	jběžnejší	k2eNgFnSc1d1	jběžnejší
svačina	svačina	k1gFnSc1	svačina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
oběd	oběd	k1gInSc1	oběd
<g/>
,	,	kIx,	,
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
Smöra	Smöra	k1gFnSc1	Smöra
bröd	bröd	k6eAd1	bröd
je	být	k5eAaImIp3nS	být
připravován	připravován	k2eAgInSc1d1	připravován
s	s	k7c7	s
vařenými	vařený	k2eAgFnPc7d1	vařená
malými	malý	k2eAgFnPc7d1	malá
krevetami	kreveta	k1gFnPc7	kreveta
<g/>
,	,	kIx,	,
vařeným	vařený	k2eAgNnSc7d1	vařené
vajíčkem	vajíčko	k1gNnSc7	vajíčko
a	a	k8xC	a
majonézou	majonéza	k1gFnSc7	majonéza
nebo	nebo	k8xC	nebo
s	s	k7c7	s
rostbífem	rostbíf	k1gInSc7	rostbíf
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
řepou	řepa	k1gFnSc7	řepa
<g/>
.	.	kIx.	.
</s>
<s>
Gravad	Gravad	k1gInSc1	Gravad
lax	lax	k?	lax
<g/>
=	=	kIx~	=
marinovaný	marinovaný	k2eAgMnSc1d1	marinovaný
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
podávaný	podávaný	k2eAgInSc1d1	podávaný
s	s	k7c7	s
koprem	kopr	k1gInSc7	kopr
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jako	jako	k9	jako
předkrm	předkrm	k1gInSc1	předkrm
Rökt	Rökt	k1gInSc1	Rökt
lax	lax	k?	lax
<g/>
=	=	kIx~	=
<g/>
uzený	uzený	k2eAgMnSc1d1	uzený
losos	losos	k1gMnSc1	losos
Fisksoppa	Fisksopp	k1gMnSc2	Fisksopp
<g/>
=	=	kIx~	=
<g/>
rybí	rybí	k2eAgFnSc1d1	rybí
polévka	polévka	k1gFnSc1	polévka
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
majonézy	majonéza	k1gFnSc2	majonéza
<g/>
,	,	kIx,	,
chlebem	chléb	k1gInSc7	chléb
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
a	a	k8xC	a
salátem	salát	k1gInSc7	salát
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rajčaty	rajče	k1gNnPc7	rajče
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
pikantním	pikantní	k2eAgNnSc7d1	pikantní
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	s	k7c7	s
smetanou	smetana	k1gFnSc7	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Smörgå	Smörgå	k?	Smörgå
<g/>
=	=	kIx~	=
je	být	k5eAaImIp3nS	být
doslova	doslova	k6eAd1	doslova
bufet	bufet	k1gInSc4	bufet
<g/>
,	,	kIx,	,
my	my	k3xPp1nPc1	my
ho	on	k3xPp3gInSc4	on
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
švédský	švédský	k2eAgInSc4d1	švédský
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
<g/>
brunch	brunch	k1gInSc1	brunch
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc4	několik
dopravních	dopravní	k2eAgFnPc2d1	dopravní
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
kvalita	kvalita	k1gFnSc1	kvalita
je	být	k5eAaImIp3nS	být
standardně	standardně	k6eAd1	standardně
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
běžné	běžný	k2eAgNnSc1d1	běžné
vlakové	vlakový	k2eAgNnSc1d1	vlakové
jízdné	jízdné	k1gNnSc1	jízdné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dražší	drahý	k2eAgInPc1d2	dražší
pokud	pokud	k8xS	pokud
získáte	získat	k5eAaPmIp2nP	získat
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
nějakou	nějaký	k3yIgFnSc4	nějaký
slevu	sleva	k1gFnSc4	sleva
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňování	uplatňování	k1gNnSc1	uplatňování
slev	sleva	k1gFnPc2	sleva
v	v	k7c6	v
autobuse	autobus	k1gInSc6	autobus
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
poněkud	poněkud	k6eAd1	poněkud
problematické	problematický	k2eAgNnSc1d1	problematické
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
každém	každý	k3xTgMnSc6	každý
dopravci	dopravce	k1gMnSc6	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k6eAd1	doprava
vlakem	vlak	k1gInSc7	vlak
je	být	k5eAaImIp3nS	být
pohodlná	pohodlný	k2eAgFnSc1d1	pohodlná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
si	se	k3xPyFc3	se
ale	ale	k9	ale
použitím	použití	k1gNnSc7	použití
autobusu	autobus	k1gInSc2	autobus
příliš	příliš	k6eAd1	příliš
nepomůžete	pomoct	k5eNaPmIp2nP	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
poměrně	poměrně	k6eAd1	poměrně
bohatý	bohatý	k2eAgInSc4d1	bohatý
systém	systém	k1gInSc4	systém
slev	sleva	k1gFnPc2	sleva
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
lze	lze	k6eAd1	lze
nějakou	nějaký	k3yIgFnSc4	nějaký
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
se	se	k3xPyFc4	se
slevou	sleva	k1gFnSc7	sleva
je	být	k5eAaImIp3nS	být
však	však	k9	však
vlak	vlak	k1gInSc1	vlak
poměrně	poměrně	k6eAd1	poměrně
drahý	drahý	k2eAgInSc1d1	drahý
a	a	k8xC	a
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
akční	akční	k2eAgFnPc4d1	akční
jízdenky	jízdenka	k1gFnPc4	jízdenka
jako	jako	k8xC	jako
například	například	k6eAd1	například
EuroPass	EuroPassa	k1gFnPc2	EuroPassa
či	či	k8xC	či
InterRail	InterRaila	k1gFnPc2	InterRaila
<g/>
.	.	kIx.	.
</s>
<s>
Zdarma	zdarma	k6eAd1	zdarma
lze	lze	k6eAd1	lze
vyřídit	vyřídit	k5eAaPmF	vyřídit
kartu	karta	k1gFnSc4	karta
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
můžete	moct	k5eAaImIp2nP	moct
nakupovat	nakupovat	k5eAaBmF	nakupovat
jízdenky	jízdenka	k1gFnPc4	jízdenka
v	v	k7c6	v
automatech	automat	k1gInPc6	automat
a	a	k8xC	a
automaticky	automaticky	k6eAd1	automaticky
máte	mít	k5eAaImIp2nP	mít
20	[number]	k4	20
%	%	kIx~	%
slevu	sleva	k1gFnSc4	sleva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
auto	auto	k1gNnSc1	auto
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejlevnějším	levný	k2eAgInSc7d3	nejlevnější
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
disponuje	disponovat	k5eAaBmIp3nS	disponovat
poměrně	poměrně	k6eAd1	poměrně
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
1044	[number]	k4	1044
km	km	kA	km
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
ani	ani	k8xC	ani
tunely	tunel	k1gInPc1	tunel
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
je	být	k5eAaImIp3nS	být
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Platí	platit	k5eAaImIp3nS	platit
celoroční	celoroční	k2eAgNnSc1d1	celoroční
povinné	povinný	k2eAgNnSc1d1	povinné
svícení	svícení	k1gNnSc1	svícení
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgFnPc1d1	povolena
pneumatiky	pneumatika	k1gFnPc1	pneumatika
s	s	k7c7	s
hroty	hrot	k1gInPc7	hrot
a	a	k8xC	a
řetězy	řetěz	k1gInPc7	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Povolená	povolený	k2eAgFnSc1d1	povolená
hranice	hranice	k1gFnSc1	hranice
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
0,2	[number]	k4	0,2
promile	promile	k1gNnPc2	promile
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
1	[number]	k4	1
promile	promile	k1gNnPc2	promile
následuje	následovat	k5eAaImIp3nS	následovat
nekompromisně	kompromisně	k6eNd1	kompromisně
vězení	vězení	k1gNnSc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
nemá	mít	k5eNaImIp3nS	mít
Švédsko	Švédsko	k1gNnSc1	Švédsko
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
ani	ani	k8xC	ani
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
fjordy	fjord	k1gInPc4	fjord
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Švédska	Švédsko	k1gNnSc2	Švédsko
se	se	k3xPyFc4	se
s	s	k7c7	s
trajekty	trajekt	k1gInPc7	trajekt
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
nepotkáte	potkat	k5eNaPmIp2nP	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Trajektu	trajekt	k1gInSc3	trajekt
se	se	k3xPyFc4	se
ale	ale	k9	ale
často	často	k6eAd1	často
nevyhnete	vyhnout	k5eNaPmIp2nP	vyhnout
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
sice	sice	k8xC	sice
použít	použít	k5eAaPmF	použít
i	i	k9	i
placené	placený	k2eAgInPc4d1	placený
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenově	cenově	k6eAd1	cenově
si	se	k3xPyFc3	se
nepomůžete	pomoct	k5eNaPmIp2nP	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jedete	jet	k5eAaImIp2nP	jet
autem	auto	k1gNnSc7	auto
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
si	se	k3xPyFc3	se
zavčasu	zavčasu	k?	zavčasu
rezervujte	rezervovat	k5eAaBmRp2nP	rezervovat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
bývá	bývat	k5eAaImIp3nS	bývat
hodně	hodně	k6eAd1	hodně
plno	pln	k2eAgNnSc1d1	plno
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
a	a	k8xC	a
často	často	k6eAd1	často
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
i	i	k9	i
příměstské	příměstský	k2eAgFnPc4d1	příměstská
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
připravte	připravit	k5eAaPmRp2nP	připravit
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vícedenní	vícedenní	k2eAgInSc4d1	vícedenní
pobyt	pobyt	k1gInSc4	pobyt
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
koupit	koupit	k5eAaPmF	koupit
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
měsíční	měsíční	k2eAgFnSc4d1	měsíční
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
již	již	k6eAd1	již
za	za	k7c4	za
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
můžete	moct	k5eAaImIp2nP	moct
využít	využít	k5eAaPmF	využít
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
stanice	stanice	k1gFnPc4	stanice
vyzdobené	vyzdobený	k2eAgFnPc4d1	vyzdobená
řadou	řada	k1gFnSc7	řada
moderních	moderní	k2eAgNnPc2d1	moderní
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
9	[number]	k4	9
292	[number]	k4	292
959	[number]	k4	959
(	(	kIx(	(
<g/>
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
cca	cca	kA	cca
8	[number]	k4	8
730	[number]	k4	730
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Švédové	Švédové	k2eAgFnPc1d1	Švédové
a	a	k8xC	a
562	[number]	k4	562
000	[number]	k4	000
cizinci	cizinec	k1gMnPc7	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
imigrantským	imigrantský	k2eAgInSc7d1	imigrantský
původem	původ	k1gInSc7	původ
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc4	osoba
narozené	narozený	k2eAgFnPc4d1	narozená
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
narozené	narozený	k2eAgFnPc4d1	narozená
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
narozenými	narozený	k2eAgMnPc7d1	narozený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
20.1	[number]	k4	20.1
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejpočetnější	početní	k2eAgFnSc2d3	nejpočetnější
skupiny	skupina	k1gFnSc2	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Finové	Fin	k1gMnPc1	Fin
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Iráčané	Iráčan	k1gMnPc1	Iráčan
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
také	také	k9	také
na	na	k7c4	na
7000	[number]	k4	7000
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
muslimských	muslimský	k2eAgMnPc2d1	muslimský
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
500	[number]	k4	500
000	[number]	k4	000
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgMnPc4	tento
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přivedl	přivést	k5eAaPmAgInS	přivést
štědrý	štědrý	k2eAgInSc1d1	štědrý
švédský	švédský	k2eAgInSc1d1	švédský
sociální	sociální	k2eAgInSc1d1	sociální
systém	systém	k1gInSc1	systém
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
benevolentní	benevolentní	k2eAgInSc1d1	benevolentní
přistěhovalecký	přistěhovalecký	k2eAgInSc1d1	přistěhovalecký
režim	režim	k1gInSc1	režim
švédských	švédský	k2eAgFnPc2d1	švédská
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
menšiny	menšina	k1gFnSc2	menšina
Dánů	Dán	k1gMnPc2	Dán
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Finů	Fin	k1gMnPc2	Fin
a	a	k8xC	a
Sámů	Sámo	k1gMnPc2	Sámo
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Öresundu	Öresund	k1gInSc2	Öresund
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
jezera	jezero	k1gNnSc2	jezero
Mälaren	Mälarno	k1gNnPc2	Mälarno
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Plodnost	plodnost	k1gFnSc1	plodnost
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
kolísá	kolísat	k5eAaImIp3nS	kolísat
okolo	okolo	k7c2	okolo
1,9	[number]	k4	1,9
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
plodném	plodný	k2eAgInSc6d1	plodný
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
Švédsko	Švédsko	k1gNnSc1	Švédsko
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
v	v	k7c6	v
přírůstku	přírůstek	k1gInSc6	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podprůměru	podprůměr	k1gInSc6	podprůměr
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
Erik	Erik	k1gMnSc1	Erik
a	a	k8xC	a
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
švédština	švédština	k1gFnSc1	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc4d1	severogermánský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc4d1	podobný
norštině	norština	k1gFnSc6	norština
a	a	k8xC	a
dánštině	dánština	k1gFnSc6	dánština
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
švédštiny	švédština	k1gFnSc2	švédština
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
dánština	dánština	k1gFnSc1	dánština
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
švédský	švédský	k2eAgInSc4d1	švédský
parlament	parlament	k1gInSc4	parlament
švédštinu	švédština	k1gFnSc4	švédština
nikdy	nikdy	k6eAd1	nikdy
neschválil	schválit	k5eNaPmAgMnS	schválit
jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
huvudsprå	huvudsprå	k?	huvudsprå
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
schváleno	schválit	k5eAaPmNgNnS	schválit
5	[number]	k4	5
jazyků	jazyk	k1gInPc2	jazyk
menšin	menšina	k1gFnPc2	menšina
(	(	kIx(	(
<g/>
finština	finština	k1gFnSc1	finština
<g/>
,	,	kIx,	,
meänkieli	meänkiet	k5eAaBmAgMnP	meänkiet
-	-	kIx~	-
tornedalská	tornedalský	k2eAgFnSc1d1	tornedalská
finština	finština	k1gFnSc1	finština
<g/>
,	,	kIx,	,
romština	romština	k1gFnSc1	romština
<g/>
,	,	kIx,	,
laponština	laponština	k1gFnSc1	laponština
a	a	k8xC	a
jidiš	jidiš	k1gNnSc1	jidiš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
ve	v	k7c6	v
vybraných	vybraný	k2eAgInPc6d1	vybraný
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
85	[number]	k4	85
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podle	podle	k7c2	podle
švédských	švédský	k2eAgNnPc2d1	švédské
hledisek	hledisko	k1gNnPc2	hledisko
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
sídlo	sídlo	k1gNnSc4	sídlo
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Göteborg	Göteborg	k1gInSc1	Göteborg
a	a	k8xC	a
Malmö	Malmö	k1gFnSc1	Malmö
<g/>
.	.	kIx.	.
</s>
<s>
Pokřesťanštění	pokřesťanštění	k1gNnSc1	pokřesťanštění
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Olof	Olof	k1gMnSc1	Olof
Skottkonung	Skottkonung	k1gMnSc1	Skottkonung
<g/>
)	)	kIx)	)
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jednotného	jednotný	k2eAgInSc2d1	jednotný
švédského	švédský	k2eAgInSc2d1	švédský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
přestoupilo	přestoupit	k5eAaPmAgNnS	přestoupit
Švédsko	Švédsko	k1gNnSc1	Švédsko
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Gustava	Gustav	k1gMnSc2	Gustav
I.	I.	kA	I.
Vasy	Vasy	k1gInPc4	Vasy
k	k	k7c3	k
luteránství	luteránství	k1gNnSc3	luteránství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
luterské	luterský	k2eAgFnSc3d1	luterská
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hlásí	hlásit	k5eAaImIp3nS	hlásit
kolem	kolem	k7c2	kolem
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Imigrace	imigrace	k1gFnSc1	imigrace
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
růstu	růst	k1gInSc2	růst
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
změn	změna	k1gFnPc2	změna
během	během	k7c2	během
nedávné	dávný	k2eNgFnSc2d1	nedávná
historie	historie	k1gFnSc2	historie
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
transformovala	transformovat	k5eAaBmAgFnS	transformovat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInPc1d1	sociální
a	a	k8xC	a
politické	politický	k2eAgInPc1d1	politický
aspekty	aspekt	k1gInPc1	aspekt
imigrace	imigrace	k1gFnSc2	imigrace
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
polemiku	polemika	k1gFnSc4	polemika
ohledně	ohledně	k7c2	ohledně
etnické	etnický	k2eAgFnSc2d1	etnická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
,	,	kIx,	,
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
přínosu	přínos	k1gInSc2	přínos
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
systému	systém	k1gInSc2	systém
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
vzestupnou	vzestupný	k2eAgFnSc4d1	vzestupná
sociální	sociální	k2eAgFnSc4d1	sociální
mobilitu	mobilita	k1gFnSc4	mobilita
<g/>
,	,	kIx,	,
trestnou	trestný	k2eAgFnSc4d1	trestná
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tu	tu	k6eAd1	tu
počty	počet	k1gInPc4	počet
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
silně	silně	k6eAd1	silně
kolísaly	kolísat	k5eAaImAgInP	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Imigrace	imigrace	k1gFnSc1	imigrace
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
přistěhovávání	přistěhovávání	k1gNnSc2	přistěhovávání
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
platil	platit	k5eAaImAgInS	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
stěhující	stěhující	k2eAgFnSc2d1	stěhující
se	se	k3xPyFc4	se
z	z	k7c2	z
třetích	třetí	k4xOgFnPc2	třetí
zemí	zem	k1gFnPc2	zem
ležících	ležící	k2eAgFnPc2d1	ležící
mimo	mimo	k7c4	mimo
jednotný	jednotný	k2eAgInSc4d1	jednotný
skandinávský	skandinávský	k2eAgInSc4d1	skandinávský
trh	trh	k1gInSc4	trh
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
státy	stát	k1gInPc1	stát
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
pracovním	pracovní	k2eAgFnPc3d1	pracovní
silám	síla	k1gFnPc3	síla
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
vždy	vždy	k6eAd1	vždy
přistupovalo	přistupovat	k5eAaImAgNnS	přistupovat
jako	jako	k9	jako
k	k	k7c3	k
imigrantům	imigrant	k1gMnPc3	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
imigračního	imigrační	k2eAgInSc2d1	imigrační
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
samo	sám	k3xTgNnSc1	sám
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
multikulturní	multikulturní	k2eAgFnSc4d1	multikulturní
přistěhovaleckou	přistěhovalecký	k2eAgFnSc4d1	přistěhovalecká
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
bylo	být	k5eAaImAgNnS	být
zrovnoprávnění	zrovnoprávnění	k1gNnSc1	zrovnoprávnění
imigrantů	imigrant	k1gMnPc2	imigrant
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
blahobytu	blahobyt	k1gInSc2	blahobyt
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
kulturní	kulturní	k2eAgNnSc4d1	kulturní
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
souběh	souběh	k1gInSc1	souběh
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
zde	zde	k6eAd1	zde
k	k	k7c3	k
vnitropolitickým	vnitropolitický	k2eAgInPc3d1	vnitropolitický
konfliktům	konflikt	k1gInPc3	konflikt
provázeným	provázený	k2eAgInPc3d1	provázený
výtržnostmi	výtržnost	k1gFnPc7	výtržnost
proti	proti	k7c3	proti
imigrantům	imigrant	k1gMnPc3	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
hlasitěji	hlasitě	k6eAd2	hlasitě
zaznívaly	zaznívat	k5eAaImAgInP	zaznívat
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
multikulturní	multikulturní	k2eAgMnSc1d1	multikulturní
a	a	k8xC	a
přistěhovaleckou	přistěhovalecký	k2eAgFnSc7d1	přistěhovalecká
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
746	[number]	k4	746
921	[number]	k4	921
obyvatel	obyvatel	k1gMnPc2	obyvatel
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
narozených	narozený	k2eAgInPc2d1	narozený
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
%	%	kIx~	%
švédské	švédský	k2eAgFnSc2d1	švédská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
1	[number]	k4	1
216	[number]	k4	216
659	[number]	k4	659
nebo	nebo	k8xC	nebo
70	[number]	k4	70
<g/>
%	%	kIx~	%
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
530	[number]	k4	530
262	[number]	k4	262
či	či	k8xC	či
30	[number]	k4	30
<g/>
%	%	kIx~	%
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Okolo	okolo	k7c2	okolo
27	[number]	k4	27
<g/>
%	%	kIx~	%
či	či	k8xC	či
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Švédska	Švédsko	k1gNnSc2	Švédsko
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
plně	plně	k6eAd1	plně
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
původ	původ	k1gInSc4	původ
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
;	;	kIx,	;
1	[number]	k4	1
427	[number]	k4	427
296	[number]	k4	296
bylo	být	k5eAaImAgNnS	být
narozeno	narodit	k5eAaPmNgNnS	narodit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
430	[number]	k4	430
253	[number]	k4	253
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
ve	v	k7c4	v
Švédku	Švédka	k1gFnSc4	Švédka
rodičům	rodič	k1gMnPc3	rodič
narozeným	narozený	k2eAgInPc3d1	narozený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
666	[number]	k4	666
723	[number]	k4	723
obyvatel	obyvatel	k1gMnPc2	obyvatel
mělo	mít	k5eAaImAgNnS	mít
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
narozeného	narozený	k2eAgInSc2d1	narozený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
s	s	k7c7	s
druhým	druhý	k4xOgNnSc7	druhý
narozeným	narozený	k2eAgInPc3d1	narozený
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
482	[number]	k4	482
855	[number]	k4	855
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
narozenými	narozený	k2eAgMnPc7d1	narozený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
7	[number]	k4	7
<g/>
%	%	kIx~	%
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
narozenému	narozený	k2eAgInSc3d1	narozený
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
26,5	[number]	k4	26,5
<g/>
%	%	kIx~	%
švédské	švédský	k2eAgFnSc2d1	švédská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
filmografie	filmografie	k1gFnSc1	filmografie
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
...	...	k?	...
Ze	z	k7c2	z
švédských	švédský	k2eAgInPc2d1	švédský
sportů	sport	k1gInPc2	sport
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgInPc3d3	nejúspěšnější
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
florbal	florbal	k1gInSc4	florbal
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
a	a	k8xC	a
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
školu	škola	k1gFnSc4	škola
moderních	moderní	k2eAgMnPc2d1	moderní
brankářů	brankář	k1gMnPc2	brankář
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
reputaci	reputace	k1gFnSc4	reputace
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
brankářská	brankářský	k2eAgFnSc1d1	brankářská
hokejová	hokejový	k2eAgFnSc1d1	hokejová
škola	škola	k1gFnSc1	škola
GDI	GDI	kA	GDI
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
úspěšné	úspěšný	k2eAgMnPc4d1	úspěšný
švédské	švédský	k2eAgMnPc4d1	švédský
hokejové	hokejový	k2eAgMnPc4d1	hokejový
brankáře	brankář	k1gMnPc4	brankář
patří	patřit	k5eAaImIp3nS	patřit
Henrik	Henrik	k1gMnSc1	Henrik
Lundqvist	Lundqvist	k1gMnSc1	Lundqvist
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Fasth	Fasth	k1gMnSc1	Fasth
<g/>
,	,	kIx,	,
Jonas	Jonas	k1gMnSc1	Jonas
Gustavsson	Gustavsson	k1gMnSc1	Gustavsson
známí	známit	k5eAaImIp3nP	známit
švédští	švédský	k2eAgMnPc1d1	švédský
sportovci	sportovec	k1gMnPc1	sportovec
-	-	kIx~	-
Henrik	Henrik	k1gMnSc1	Henrik
Zetterberg	Zetterberg	k1gMnSc1	Zetterberg
<g/>
,	,	kIx,	,
Henrik	Henrik	k1gMnSc1	Henrik
Lundqvist	Lundqvist	k1gMnSc1	Lundqvist
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Forsberg	Forsberg	k1gMnSc1	Forsberg
<g/>
,	,	kIx,	,
Anja	Anj	k2eAgFnSc1d1	Anja
Pärsonová	Pärsonová	k1gFnSc1	Pärsonová
<g/>
,	,	kIx,	,
Mats	Mats	k1gInSc1	Mats
Sundin	Sundin	k2eAgInSc1d1	Sundin
<g/>
,	,	kIx,	,
Björn	Björn	k1gMnSc1	Björn
Borg	Borg	k1gMnSc1	Borg
<g/>
,	,	kIx,	,
Mats	Mats	k1gInSc1	Mats
Wilander	Wilander	k1gInSc1	Wilander
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Edberg	Edberg	k1gMnSc1	Edberg
<g/>
,	,	kIx,	,
Zlatan	Zlatan	k1gInSc4	Zlatan
Ibrahimović	Ibrahimović	k1gFnSc2	Ibrahimović
Švédsko	Švédsko	k1gNnSc4	Švédsko
má	mít	k5eAaImIp3nS	mít
jednotný	jednotný	k2eAgInSc1d1	jednotný
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
a	a	k8xC	a
odborné	odborný	k2eAgNnSc4d1	odborné
studium	studium	k1gNnSc4	studium
s	s	k7c7	s
programy	program	k1gInPc7	program
škol	škola	k1gFnPc2	škola
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
od	od	k7c2	od
7	[number]	k4	7
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
němčina	němčina	k1gFnSc1	němčina
nebo	nebo	k8xC	nebo
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
třídě	třída	k1gFnSc6	třída
si	se	k3xPyFc3	se
žáci	žák	k1gMnPc1	žák
mohou	moct	k5eAaImIp3nP	moct
částečně	částečně	k6eAd1	částečně
volit	volit	k5eAaImF	volit
skladbu	skladba	k1gFnSc4	skladba
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
působí	působit	k5eAaImIp3nS	působit
třináct	třináct	k4xCc4	třináct
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
a	a	k8xC	a
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
je	být	k5eAaImIp3nS	být
vzdělání	vzdělání	k1gNnSc1	vzdělání
dospělých	dospělí	k1gMnPc2	dospělí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
denních	denní	k2eAgMnPc2d1	denní
<g/>
,	,	kIx,	,
večerních	večerní	k2eAgInPc2d1	večerní
<g/>
,	,	kIx,	,
dálkových	dálkový	k2eAgInPc2d1	dálkový
i	i	k8xC	i
domácích	domácí	k2eAgInPc2d1	domácí
kursů	kurs	k1gInPc2	kurs
a	a	k8xC	a
na	na	k7c6	na
lidových	lidový	k2eAgFnPc6d1	lidová
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
švédským	švédský	k2eAgMnPc3d1	švédský
vědcům	vědec	k1gMnPc3	vědec
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Nils	Nils	k1gInSc1	Nils
Gustaf	Gustaf	k1gMnSc1	Gustaf
Dalén	Dalén	k1gInSc1	Dalén
či	či	k8xC	či
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
každoročně	každoročně	k6eAd1	každoročně
udělovány	udělován	k2eAgFnPc4d1	udělována
Nobelovy	Nobelův	k2eAgFnPc4d1	Nobelova
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
