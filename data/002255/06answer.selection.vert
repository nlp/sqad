<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
pomocí	pomocí	k7c2	pomocí
unikátního	unikátní	k2eAgInSc2d1	unikátní
mostu-tunelu	mostuunel	k1gInSc2	mostu-tunel
přes	přes	k7c4	přes
průliv	průliv	k1gInSc4	průliv
Öresund	Öresunda	k1gFnPc2	Öresunda
<g/>
.	.	kIx.	.
</s>
