<s>
Ikebana	ikebana	k1gFnSc1	ikebana
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
生	生	k?	生
<g/>
,	,	kIx,	,
活	活	k?	活
nebo	nebo	k8xC	nebo
い	い	k?	い
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
oživené	oživený	k2eAgFnPc4d1	oživená
květiny	květina	k1gFnPc4	květina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
umění	umění	k1gNnSc4	umění
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
také	také	k9	také
známé	známý	k2eAgFnPc4d1	známá
jako	jako	k9	jako
kadó	kadó	k?	kadó
(	(	kIx(	(
<g/>
華	華	k?	華
nebo	nebo	k8xC	nebo
花	花	k?	花
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
cesta	cesta	k1gFnSc1	cesta
<g/>
/	/	kIx~	/
<g/>
umění	umění	k1gNnSc1	umění
květin	květina	k1gFnPc2	květina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
