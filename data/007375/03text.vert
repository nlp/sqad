<s>
Ikebana	ikebana	k1gFnSc1	ikebana
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
生	生	k?	生
<g/>
,	,	kIx,	,
活	活	k?	活
nebo	nebo	k8xC	nebo
い	い	k?	い
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
oživené	oživený	k2eAgFnPc4d1	oživená
květiny	květina	k1gFnPc4	květina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
umění	umění	k1gNnSc4	umění
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
také	také	k9	také
známé	známý	k2eAgFnPc4d1	známá
jako	jako	k9	jako
kadó	kadó	k?	kadó
(	(	kIx(	(
<g/>
華	華	k?	華
nebo	nebo	k8xC	nebo
花	花	k?	花
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
cesta	cesta	k1gFnSc1	cesta
<g/>
/	/	kIx~	/
<g/>
umění	umění	k1gNnSc1	umění
květin	květina	k1gFnPc2	květina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dekorativního	dekorativní	k2eAgNnSc2d1	dekorativní
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
japonská	japonský	k2eAgFnSc1d1	japonská
ikebana	ikebana	k1gFnSc1	ikebana
snaží	snažit	k5eAaImIp3nS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
harmonii	harmonie	k1gFnSc4	harmonie
lineární	lineární	k2eAgFnSc2d1	lineární
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
rytmu	rytmus	k1gInSc2	rytmus
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zvýrazňovat	zvýrazňovat	k5eAaImF	zvýrazňovat
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
krásu	krása	k1gFnSc4	krása
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
Japonci	Japonec	k1gMnPc1	Japonec
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
lineární	lineární	k2eAgInPc4d1	lineární
aspekty	aspekt	k1gInPc4	aspekt
aranžování	aranžování	k1gNnSc2	aranžování
<g/>
.	.	kIx.	.
</s>
<s>
Ikebana	ikebana	k1gFnSc1	ikebana
věnuje	věnovat	k5eAaPmIp3nS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
váze	váha	k1gFnSc3	váha
<g/>
,	,	kIx,	,
stonkům	stonek	k1gInPc3	stonek
<g/>
,	,	kIx,	,
listům	list	k1gInPc3	list
a	a	k8xC	a
větvím	větvit	k5eAaImIp1nS	větvit
stejnou	stejný	k2eAgFnSc7d1	stejná
měrou	míra	k1gFnSc7wR	míra
jako	jako	k8xS	jako
květům	květ	k1gInPc3	květ
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
struktura	struktura	k1gFnSc1	struktura
japonského	japonský	k2eAgNnSc2d1	Japonské
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
liniích	linie	k1gFnPc6	linie
alegorizujících	alegorizující	k2eAgNnPc2d1	alegorizující
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ikebana	ikebana	k1gFnSc1	ikebana
tyto	tento	k3xDgFnPc4	tento
linie	linie	k1gFnPc4	linie
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
tři	tři	k4xCgInPc4	tři
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
pedagogů	pedagog	k1gMnPc2	pedagog
ikebany	ikebana	k1gFnSc2	ikebana
byl	být	k5eAaImAgMnS	být
Senkó	Senkó	k1gMnSc1	Senkó
Ikenobó	Ikenobó	k1gMnSc1	Ikenobó
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
škol	škola	k1gFnPc2	škola
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Mělké	mělký	k2eAgFnPc4d1	mělká
misky	miska	k1gFnPc4	miska
nejdříve	dříve	k6eAd3	dříve
používal	používat	k5eAaImAgMnS	používat
Unšin	Unšin	k1gMnSc1	Unšin
Ohara	Ohara	k1gMnSc1	Ohara
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
stylu	styl	k1gInSc2	styl
moribana	moribana	k1gFnSc1	moribana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
vedeny	vést	k5eAaImNgFnP	vést
rozepře	rozepře	k1gFnPc1	rozepře
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
ikebana	ikebana	k1gFnSc1	ikebana
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
řemeslo	řemeslo	k1gNnSc1	řemeslo
nebo	nebo	k8xC	nebo
pouhá	pouhý	k2eAgFnSc1d1	pouhá
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
japonských	japonský	k2eAgInPc2d1	japonský
šógunů	šógun	k1gInPc2	šógun
<g/>
,	,	kIx,	,
Hidejoši	Hidejoš	k1gMnPc1	Hidejoš
a	a	k8xC	a
Jošimasa	Jošimas	k1gMnSc2	Jošimas
<g/>
,	,	kIx,	,
nacházeli	nacházet	k5eAaImAgMnP	nacházet
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
ikebaně	ikebana	k1gFnSc6	ikebana
pro	pro	k7c4	pro
zklidnění	zklidnění	k1gNnSc4	zklidnění
své	svůj	k3xOyFgFnSc2	svůj
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
Averill	Averilla	k1gFnPc2	Averilla
uvádí	uvádět	k5eAaImIp3nS	uvádět
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
ikebana	ikebana	k1gFnSc1	ikebana
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
"	"	kIx"	"
<g/>
cenný	cenný	k2eAgInSc4d1	cenný
trénink	trénink	k1gInSc4	trénink
i	i	k9	i
pro	pro	k7c4	pro
mužský	mužský	k2eAgInSc4d1	mužský
mozek	mozek	k1gInSc4	mozek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc2	historie
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
aranžování	aranžování	k1gNnSc4	aranžování
květin	květina	k1gFnPc2	květina
převzali	převzít	k5eAaPmAgMnP	převzít
Japonci	Japonec	k1gMnPc1	Japonec
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
buddhismem	buddhismus	k1gInSc7	buddhismus
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
přirozeně	přirozeně	k6eAd1	přirozeně
naplněno	naplnit	k5eAaPmNgNnS	naplnit
čínskou	čínský	k2eAgFnSc7d1	čínská
buddhistickou	buddhistický	k2eAgFnSc7d1	buddhistická
filozofií	filozofie	k1gFnSc7	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Buddhistická	buddhistický	k2eAgFnSc1d1	buddhistická
touha	touha	k1gFnSc1	touha
uchovat	uchovat	k5eAaPmF	uchovat
život	život	k1gInSc4	život
tak	tak	k9	tak
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
celé	celý	k2eAgFnSc2d1	celá
ikebany	ikebana	k1gFnSc2	ikebana
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
většinu	většina	k1gFnSc4	většina
pravidel	pravidlo	k1gNnPc2	pravidlo
uspořádání	uspořádání	k1gNnSc4	uspořádání
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
květinové	květinový	k2eAgFnSc2d1	květinová
vázy	váza	k1gFnSc2	váza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uvažováno	uvažován	k2eAgNnSc1d1	uvažováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínské	čínský	k2eAgFnPc1d1	čínská
kytice	kytice	k1gFnPc1	kytice
měly	mít	k5eAaImAgFnP	mít
původně	původně	k6eAd1	původně
jistý	jistý	k2eAgInSc4d1	jistý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Averill	Averill	k1gMnSc1	Averill
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Odporné	odporný	k2eAgFnPc1d1	odporná
pohřební	pohřební	k2eAgFnPc1d1	pohřební
kytice	kytice	k1gFnPc1	kytice
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
jako	jako	k8xS	jako
masy	masa	k1gFnPc1	masa
skvělých	skvělý	k2eAgFnPc2d1	skvělá
květin	květina	k1gFnPc2	květina
na	na	k7c6	na
krátkých	krátký	k2eAgInPc6d1	krátký
stoncích	stonek	k1gInPc6	stonek
<g/>
,	,	kIx,	,
hrubě	hrubě	k6eAd1	hrubě
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
svázané	svázaný	k2eAgFnPc4d1	svázaná
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
původní	původní	k2eAgFnPc1d1	původní
kytice	kytice	k1gFnPc1	kytice
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
nárokují	nárokovat	k5eAaImIp3nP	nárokovat
původ	původ	k1gInSc4	původ
ikebany	ikebana	k1gFnSc2	ikebana
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
ikebanu	ikebana	k1gFnSc4	ikebana
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vynikající	vynikající	k2eAgInSc4d1	vynikající
způsob	způsob	k1gInSc4	způsob
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ale	ale	k9	ale
přitom	přitom	k6eAd1	přitom
podle	podle	k7c2	podle
autorky	autorka	k1gFnSc2	autorka
není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
původní	původní	k2eAgInSc1d1	původní
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
původně	původně	k6eAd1	původně
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
řecký	řecký	k2eAgMnSc1d1	řecký
jako	jako	k8xS	jako
čínský	čínský	k2eAgMnSc1d1	čínský
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
rysech	rys	k1gInPc6	rys
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
značná	značný	k2eAgFnSc1d1	značná
pochybnost	pochybnost	k1gFnSc1	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
způsob	způsob	k1gInSc1	způsob
aranžování	aranžování	k1gNnSc2	aranžování
řecký	řecký	k2eAgMnSc1d1	řecký
nebo	nebo	k8xC	nebo
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
květinová	květinový	k2eAgFnSc1d1	květinová
výzdoba	výzdoba	k1gFnSc1	výzdoba
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
systém	systém	k1gInSc4	systém
známý	známý	k2eAgInSc4d1	známý
jako	jako	k9	jako
Šin	Šin	k1gFnSc7	Šin
no	no	k9	no
hana	hana	k1gFnSc1	hana
(	(	kIx(	(
<g/>
真	真	k?	真
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
soustředné	soustředný	k2eAgNnSc4d1	soustředné
aranžování	aranžování	k1gNnSc4	aranžování
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
přírodní	přírodní	k2eAgFnSc4d1	přírodní
krásu	krása	k1gFnSc4	krása
pomocí	pomocí	k7c2	pomocí
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
,	,	kIx,	,
napodobit	napodobit	k5eAaPmF	napodobit
přírodní	přírodní	k2eAgFnPc4d1	přírodní
scény	scéna	k1gFnPc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Jošimasa	Jošimasa	k1gFnSc1	Jošimasa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
足	足	k?	足
義	義	k?	義
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1435	[number]	k4	1435
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osmý	osmý	k4xOgMnSc1	osmý
šógun	šógun	k1gMnSc1	šógun
dynastie	dynastie	k1gFnSc2	dynastie
Ašikaga	Ašikaga	k1gFnSc1	Ašikaga
a	a	k8xC	a
velkorysý	velkorysý	k2eAgInSc1d1	velkorysý
patron	patron	k1gInSc1	patron
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgMnSc7d3	veliký
propagátorem	propagátor	k1gMnSc7	propagátor
čajového	čajový	k2eAgInSc2d1	čajový
obřadu	obřad	k1gInSc2	obřad
čanoju	čanoju	k5eAaPmIp1nS	čanoju
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
茶	茶	k?	茶
<g/>
)	)	kIx)	)
a	a	k8xC	a
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
malíř	malíř	k1gMnSc1	malíř
Sóami	Sóa	k1gFnPc7	Sóa
<g/>
,	,	kIx,	,
pokrokový	pokrokový	k2eAgMnSc1d1	pokrokový
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
Jošimasy	Jošimasa	k1gFnSc2	Jošimasa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zřejmě	zřejmě	k6eAd1	zřejmě
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
japonského	japonský	k2eAgNnSc2d1	Japonské
umění	umění	k1gNnSc2	umění
ikebana	ikebana	k1gFnSc1	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Sóami	Sóa	k1gFnPc7	Sóa
(	(	kIx(	(
<g/>
相	相	k?	相
<g/>
,	,	kIx,	,
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1525	[number]	k4	1525
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
linie	linie	k1gFnPc1	linie
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
tři	tři	k4xCgInPc4	tři
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
konceptu	koncept	k1gInSc2	koncept
vycházejí	vycházet	k5eAaImIp3nP	vycházet
i	i	k9	i
moderní	moderní	k2eAgInPc1d1	moderní
principy	princip	k1gInPc1	princip
uspořádání	uspořádání	k1gNnSc4	uspořádání
ikebany	ikebana	k1gFnSc2	ikebana
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Aranžování	aranžování	k1gNnSc1	aranžování
ikebana	ikebana	k1gFnSc1	ikebana
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
buddhistických	buddhistický	k2eAgFnPc6d1	buddhistická
tradicích	tradice	k1gFnPc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgFnSc2d1	dobrá
a	a	k8xC	a
zlé	zlý	k2eAgFnSc2d1	zlá
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
upravuje	upravovat	k5eAaImIp3nS	upravovat
jak	jak	k6eAd1	jak
výběr	výběr	k1gInSc4	výběr
materiálu	materiál	k1gInSc2	materiál
tak	tak	k8xC	tak
formy	forma	k1gFnSc2	forma
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
některých	některý	k3yIgFnPc2	některý
květin	květina	k1gFnPc2	květina
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgInPc4d1	Červené
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
při	při	k7c6	při
pohřbech	pohřeb	k1gInPc6	pohřeb
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nežádoucí	žádoucí	k2eNgFnSc4d1	nežádoucí
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
představuje	představovat	k5eAaImIp3nS	představovat
červené	červený	k2eAgInPc4d1	červený
plameny	plamen	k1gInPc4	plamen
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Lichý	lichý	k2eAgInSc1d1	lichý
počet	počet	k1gInSc1	počet
květů	květ	k1gInPc2	květ
je	být	k5eAaImIp3nS	být
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgFnPc1	některý
čísla	číslo	k1gNnSc2	číslo
přinášejí	přinášet	k5eAaImIp3nP	přinášet
smůlu	smůla	k1gFnSc4	smůla
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nejsou	být	k5eNaImIp3nP	být
použity	použit	k2eAgFnPc1d1	použita
při	při	k7c6	při
aranžování	aranžování	k1gNnSc6	aranžování
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
lichými	lichý	k2eAgNnPc7d1	liché
čísly	číslo	k1gNnPc7	číslo
se	se	k3xPyFc4	se
aranžmá	aranžmá	k1gNnSc1	aranžmá
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
symetrii	symetrie	k1gFnSc3	symetrie
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgFnSc3d1	jednoduchá
rovnováze	rovnováha	k1gFnSc3	rovnováha
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
zřídka	zřídka	k6eAd1	zřídka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
Japonců	Japonec	k1gMnPc2	Japonec
není	být	k5eNaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
v	v	k7c4	v
umění	umění	k1gNnSc4	umění
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prvky	prvek	k1gInPc1	prvek
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
aranžování	aranžování	k1gNnSc6	aranžování
květin	květina	k1gFnPc2	květina
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jako	jako	k9	jako
klasifikace	klasifikace	k1gFnSc2	klasifikace
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
lidstvo	lidstvo	k1gNnSc1	lidstvo
a	a	k8xC	a
země	země	k1gFnSc1	země
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
dítě	dítě	k1gNnSc4	dítě
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgInSc1d1	označován
"	"	kIx"	"
<g/>
šin	šin	k?	šin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
真	真	k?	真
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
délce	délka	k1gFnSc3	délka
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
ústřední	ústřední	k2eAgFnSc3d1	ústřední
ose	osa	k1gFnSc3	osa
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
<g/>
°	°	k?	°
a	a	k8xC	a
linie	linie	k1gFnSc1	linie
směřuje	směřovat	k5eAaImIp3nS	směřovat
mírně	mírně	k6eAd1	mírně
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
linie	linie	k1gFnSc1	linie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
soe	soe	k?	soe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
副	副	k?	副
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odkloněna	odklonit	k5eAaPmNgFnS	odklonit
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
osy	osa	k1gFnSc2	osa
o	o	k7c4	o
45	[number]	k4	45
<g/>
°	°	k?	°
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vzbuzovat	vzbuzovat	k5eAaImF	vzbuzovat
dojem	dojem	k1gInSc4	dojem
že	že	k8xS	že
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
stranou	strana	k1gFnSc7	strana
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnPc1	třetí
linie	linie	k1gFnPc1	linie
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
zemi	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
hikae	hikae	k6eAd1	hikae
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
控	控	k?	控
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
školách	škola	k1gFnPc6	škola
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
synonymní	synonymní	k2eAgInSc1d1	synonymní
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
tai	tai	k?	tai
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
体	体	k?	体
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aranžuje	aranžovat	k5eAaImIp3nS	aranžovat
se	se	k3xPyFc4	se
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc1d1	předchozí
linie	linie	k1gFnPc1	linie
"	"	kIx"	"
<g/>
soe	soe	k?	soe
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
odklání	odklánět	k5eAaImIp3nS	odklánět
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
75	[number]	k4	75
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
linie	linie	k1gFnPc1	linie
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
aranžmá	aranžmá	k1gNnPc2	aranžmá
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
doplněny	doplněn	k2eAgInPc1d1	doplněn
dalšími	další	k2eAgInPc7d1	další
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
džúši	džúsat	k5eAaPmIp1nSwK	džúsat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schematické	schematický	k2eAgNnSc1d1	schematické
označení	označení	k1gNnSc1	označení
linií	linie	k1gFnPc2	linie
v	v	k7c6	v
nákresech	nákres	k1gInPc6	nákres
ikebanistů	ikebanista	k1gMnPc2	ikebanista
bývá	bývat	k5eAaImIp3nS	bývat
následující	následující	k2eAgInSc1d1	následující
-	-	kIx~	-
šin	šin	k?	šin
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
kolečkem	kolečko	k1gNnSc7	kolečko
<g/>
,	,	kIx,	,
soe	soe	k?	soe
čtverečkem	čtvereček	k1gInSc7	čtvereček
a	a	k8xC	a
hikae	hikae	k6eAd1	hikae
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
velkým	velký	k2eAgNnSc7d1	velké
T.	T.	kA	T.
Myšlenka	myšlenka	k1gFnSc1	myšlenka
použití	použití	k1gNnSc2	použití
rozlišení	rozlišení	k1gNnSc2	rozlišení
pohlaví	pohlaví	k1gNnSc2	pohlaví
u	u	k7c2	u
neživých	živý	k2eNgInPc2d1	neživý
objektů	objekt	k1gInPc2	objekt
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
japonskou	japonský	k2eAgFnSc4d1	japonská
zahradní	zahradní	k2eAgFnSc4d1	zahradní
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Pochopení	pochopení	k1gNnSc1	pochopení
vázy	váza	k1gFnSc2	váza
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
něčeho	něco	k3yInSc2	něco
významnějšího	významný	k2eAgNnSc2d2	významnější
než	než	k8xS	než
pouhé	pouhý	k2eAgNnSc1d1	pouhé
nádobu	nádoba	k1gFnSc4	nádoba
na	na	k7c4	na
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
japonská	japonský	k2eAgFnSc1d1	japonská
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Vidí	vidět	k5eAaImIp3nP	vidět
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
povrch	povrch	k1gInSc4	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vytékají	vytékat	k5eAaImIp3nP	vytékat
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
aranžování	aranžování	k1gNnSc6	aranžování
ikebany	ikebana	k1gFnSc2	ikebana
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
představy	představa	k1gFnSc2	představa
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k6eAd1	jen
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dané	daný	k2eAgFnPc4d1	daná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Váza	váza	k1gFnSc1	váza
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
představy	představa	k1gFnSc2	představa
vždy	vždy	k6eAd1	vždy
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
a	a	k8xC	a
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
body	bod	k1gInPc4	bod
kompasu	kompas	k1gInSc2	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
přesně	přesně	k6eAd1	přesně
určují	určovat	k5eAaImIp3nP	určovat
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ve	v	k7c6	v
váze	váha	k1gFnSc6	váha
květy	květ	k1gInPc1	květ
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
během	během	k7c2	během
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
když	když	k8xS	když
převládá	převládat	k5eAaImIp3nS	převládat
silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
křivky	křivka	k1gFnPc1	křivka
větví	větev	k1gFnPc2	větev
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
vliv	vliv	k1gInSc4	vliv
silného	silný	k2eAgInSc2d1	silný
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
radují	radovat	k5eAaImIp3nP	radovat
Japonci	Japonec	k1gMnPc1	Japonec
z	z	k7c2	z
nízkých	nízký	k2eAgFnPc2d1	nízká
<g/>
,	,	kIx,	,
širokých	široký	k2eAgFnPc2d1	široká
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
chladnější	chladný	k2eAgMnSc1d2	chladnější
a	a	k8xC	a
více	hodně	k6eAd2	hodně
osvěžující	osvěžující	k2eAgNnSc4d1	osvěžující
prostředí	prostředí	k1gNnSc4	prostředí
než	než	k8xS	než
vzpřímené	vzpřímený	k2eAgFnSc2d1	vzpřímená
vázy	váza	k1gFnSc2	váza
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
květy	květ	k1gInPc7	květ
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
kompozici	kompozice	k1gFnSc4	kompozice
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
zdát	zdát	k5eAaPmF	zdát
divné	divný	k2eAgNnSc1d1	divné
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gInSc4	náš
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
domova	domov	k1gInSc2	domov
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
uspořádáním	uspořádání	k1gNnSc7	uspořádání
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovky	stovka	k1gFnPc1	stovka
obyčejných	obyčejný	k2eAgFnPc2d1	obyčejná
událostí	událost	k1gFnPc2	událost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
oznámeny	oznámit	k5eAaPmNgFnP	oznámit
pomocí	pomocí	k7c2	pomocí
půvabných	půvabný	k2eAgFnPc2d1	půvabná
květinových	květinový	k2eAgFnPc2d1	květinová
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
japonských	japonský	k2eAgMnPc2d1	japonský
básníků	básník	k1gMnPc2	básník
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
srovnání	srovnání	k1gNnSc6	srovnání
jejich	jejich	k3xOp3gFnPc2	jejich
velmi	velmi	k6eAd1	velmi
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
větví	větev	k1gFnPc2	větev
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
šťastným	šťastný	k2eAgNnSc7d1	šťastné
manželstvím	manželství	k1gNnSc7	manželství
<g/>
,	,	kIx,	,
atd	atd	kA	atd
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
využito	využít	k5eAaPmNgNnS	využít
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
oslav	oslava	k1gFnPc2	oslava
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
pro	pro	k7c4	pro
kompozice	kompozice	k1gFnPc4	kompozice
připravené	připravený	k2eAgFnPc4d1	připravená
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
větve	větev	k1gFnSc2	větev
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
návrat	návrat	k1gInSc4	návrat
z	z	k7c2	z
nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
větev	větev	k1gFnSc1	větev
tvoří	tvořit	k5eAaImIp3nS	tvořit
kompletní	kompletní	k2eAgInSc4d1	kompletní
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
topeniště	topeniště	k1gNnSc2	topeniště
(	(	kIx(	(
<g/>
hibači	hibač	k1gInSc6	hibač
-	-	kIx~	-
火	火	k?	火
<g/>
)	)	kIx)	)
v	v	k7c6	v
domě	dům	k1gInSc6	dům
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
aranžování	aranžování	k1gNnSc6	aranžování
ikebany	ikebana	k1gFnSc2	ikebana
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
použity	použit	k2eAgInPc4d1	použit
bílé	bílý	k2eAgInPc4d1	bílý
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
vodu	voda	k1gFnSc4	voda
k	k	k7c3	k
uhašení	uhašení	k1gNnSc3	uhašení
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
vždy	vždy	k6eAd1	vždy
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
hořlavost	hořlavost	k1gFnSc4	hořlavost
konstrukcí	konstrukce	k1gFnSc7	konstrukce
mnoha	mnoho	k4c2	mnoho
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hořlavost	hořlavost	k1gFnSc1	hořlavost
střech	střecha	k1gFnPc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
květiny	květina	k1gFnPc1	květina
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
vhodná	vhodný	k2eAgNnPc4d1	vhodné
aranžmá	aranžmá	k1gNnPc4	aranžmá
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
šťastné	šťastný	k2eAgFnPc4d1	šťastná
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pro	pro	k7c4	pro
ty	ten	k3xDgFnPc4	ten
smutné	smutná	k1gFnPc4	smutná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
dědictví	dědictví	k1gNnSc2	dědictví
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
zářivě	zářivě	k6eAd1	zářivě
barevných	barevný	k2eAgFnPc2d1	barevná
chryzantém	chryzantéma	k1gFnPc2	chryzantéma
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádné	žádný	k3yNgFnPc4	žádný
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dlouho	dlouho	k6eAd1	dlouho
nevadnou	vadnout	k5eNaImIp3nP	vadnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
takto	takto	k6eAd1	takto
nebyla	být	k5eNaImAgFnS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
chybná	chybný	k2eAgFnSc1d1	chybná
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bohatství	bohatství	k1gNnSc1	bohatství
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
navždy	navždy	k6eAd1	navždy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
smuteční	smuteční	k2eAgFnSc4d1	smuteční
vazbu	vazba	k1gFnSc4	vazba
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
bílé	bílý	k2eAgInPc1d1	bílý
květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
suchými	suchý	k2eAgInPc7d1	suchý
listy	list	k1gInPc7	list
a	a	k8xC	a
větvemi	větev	k1gFnPc7	větev
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
pocit	pocit	k1gInSc4	pocit
pokoje	pokoj	k1gInSc2	pokoj
a	a	k8xC	a
zklidnění	zklidnění	k1gNnSc4	zklidnění
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zasílané	zasílaný	k2eAgInPc1d1	zasílaný
květinové	květinový	k2eAgInPc1d1	květinový
dary	dar	k1gInPc1	dar
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
jako	jako	k9	jako
poupata	poupě	k1gNnPc4	poupě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
odeslány	odeslat	k5eAaPmNgInP	odeslat
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
to	ten	k3xDgNnSc4	ten
potěšení	potěšení	k1gNnSc4	potěšení
vidět	vidět	k5eAaImF	vidět
je	on	k3xPp3gFnPc4	on
otevírat	otevírat	k5eAaImF	otevírat
-	-	kIx~	-
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
zásadě	zásada	k1gFnSc3	zásada
euroatlantické	euroatlantický	k2eAgFnSc2d1	euroatlantická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgInPc4	všechen
květy	květ	k1gInPc4	květ
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dokonale	dokonale	k6eAd1	dokonale
zcela	zcela	k6eAd1	zcela
rozvity	rozvit	k2eAgFnPc1d1	rozvita
nebo	nebo	k8xC	nebo
rozvíjející	rozvíjející	k2eAgMnSc1d1	rozvíjející
se	se	k3xPyFc4	se
před	před	k7c7	před
odesláním	odeslání	k1gNnSc7	odeslání
z	z	k7c2	z
květinářství	květinářství	k1gNnSc2	květinářství
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
popisují	popisovat	k5eAaImIp3nP	popisovat
přeměnu	přeměna	k1gFnSc4	přeměna
květu	květ	k1gInSc2	květ
na	na	k7c4	na
ovoce	ovoce	k1gNnSc4	ovoce
jako	jako	k8xS	jako
stěhování	stěhování	k1gNnSc4	stěhování
duše	duše	k1gFnSc2	duše
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
květů	květ	k1gInPc2	květ
na	na	k7c4	na
listy	list	k1gInPc4	list
a	a	k8xC	a
z	z	k7c2	z
listů	list	k1gInPc2	list
do	do	k7c2	do
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
čtyř	čtyři	k4xCgNnPc2	čtyři
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
je	být	k5eAaImIp3nS	být
duše	duše	k1gFnSc1	duše
rostliny	rostlina	k1gFnPc4	rostlina
v	v	k7c6	v
květech	květ	k1gInPc6	květ
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
vchází	vcházet	k5eAaImIp3nP	vcházet
do	do	k7c2	do
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
aranžmá	aranžmá	k1gNnSc4	aranžmá
květin	květina	k1gFnPc2	květina
volně	volně	k6eAd1	volně
upravené	upravený	k2eAgFnPc1d1	upravená
a	a	k8xC	a
rozprostřené	rozprostřený	k2eAgFnPc1d1	rozprostřená
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
větve	větev	k1gFnPc1	větev
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
mírně	mírně	k6eAd1	mírně
ohnuty	ohnout	k5eAaPmNgFnP	ohnout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
přední	přední	k2eAgFnSc3d1	přední
straně	strana	k1gFnSc3	strana
a	a	k8xC	a
další	další	k1gNnSc4	další
směrem	směr	k1gInSc7	směr
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
nebe	nebe	k1gNnSc2	nebe
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větve	větev	k1gFnPc1	větev
představující	představující	k2eAgFnPc1d1	představující
lidstvo	lidstvo	k1gNnSc4	lidstvo
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
bodech	bod	k1gInPc6	bod
směřující	směřující	k2eAgMnPc1d1	směřující
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
vzad	vzad	k6eAd1	vzad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
ponechat	ponechat	k5eAaPmF	ponechat
úsek	úsek	k1gInSc4	úsek
mizu-giwa	mizuiw	k1gInSc2	mizu-giw
(	(	kIx(	(
<g/>
水	水	k?	水
<g/>
)	)	kIx)	)
volný	volný	k2eAgInSc4d1	volný
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
stonky	stonek	k1gInPc1	stonek
zdánlivě	zdánlivě	k6eAd1	zdánlivě
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Výhony	výhon	k1gInPc1	výhon
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
úsekem	úsek	k1gInSc7	úsek
jako	jako	k8xS	jako
krátké	krátký	k2eAgInPc4d1	krátký
letorosty	letorost	k1gInPc4	letorost
<g/>
,	,	kIx,	,
asi	asi	k9	asi
7	[number]	k4	7
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Krátkost	krátkost	k1gFnSc1	krátkost
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
v	v	k7c6	v
kompozici	kompozice	k1gFnSc6	kompozice
odráží	odrážet	k5eAaImIp3nS	odrážet
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
váze	váha	k1gFnSc6	váha
je	být	k5eAaImIp3nS	být
regulováno	regulovat	k5eAaImNgNnS	regulovat
také	také	k9	také
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
toky	toka	k1gFnPc1	toka
řek	řeka	k1gFnPc2	řeka
plné	plný	k2eAgFnSc2d1	plná
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vázy	váza	k1gFnPc1	váza
naplněné	naplněný	k2eAgFnPc1d1	naplněná
až	až	k9	až
po	po	k7c4	po
okraj	okraj	k1gInSc4	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
používají	používat	k5eAaImIp3nP	používat
vosk	vosk	k1gInSc4	vosk
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
vázy	váza	k1gFnSc2	váza
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
voda	voda	k1gFnSc1	voda
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přetéká	přetékat	k5eAaImIp3nS	přetékat
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
nádoba	nádoba	k1gFnSc1	nádoba
byla	být	k5eAaImAgFnS	být
naplněna	naplnit	k5eAaPmNgFnS	naplnit
vodou	voda	k1gFnSc7	voda
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nemůže	moct	k5eNaImIp3nS	moct
vytékat	vytékat	k5eAaImF	vytékat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
vhdoné	vhdoný	k2eAgNnSc1d1	vhdoný
používat	používat	k5eAaImF	používat
mladé	mladý	k2eAgInPc4d1	mladý
zelené	zelený	k2eAgInPc4d1	zelený
listy	list	k1gInPc4	list
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zastřiženy	zastřihnout	k5eAaPmNgInP	zastřihnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
chladivý	chladivý	k2eAgInSc1d1	chladivý
efekt	efekt	k1gInSc1	efekt
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mizu-giwa	Mizuiwa	k1gFnSc1	Mizu-giwa
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
v	v	k7c6	v
kterémkoli	kterýkoli	k3yIgNnSc6	kterýkoli
jiném	jiný	k2eAgNnSc6d1	jiné
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc1	čtyři
centimetry	centimetr	k1gInPc1	centimetr
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
plné	plný	k2eAgNnSc4d1	plné
a	a	k8xC	a
široké	široký	k2eAgNnSc4d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
tolik	tolik	k4xDc1	tolik
různých	různý	k2eAgInPc2d1	různý
ohybů	ohyb	k1gInPc2	ohyb
větví	větev	k1gFnPc2	větev
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Mělké	mělký	k2eAgFnPc1d1	mělká
vázy	váza	k1gFnPc1	váza
<g/>
,	,	kIx,	,
s	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nejvíce	hodně	k6eAd3	hodně
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
zlaté	zlatý	k1gInPc1	zlatý
nebo	nebo	k8xC	nebo
žluté	žlutý	k2eAgInPc1d1	žlutý
listy	list	k1gInPc1	list
v	v	k7c6	v
kompozici	kompozice	k1gFnSc6	kompozice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
čas	čas	k1gInSc4	čas
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnPc1	kompozice
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
plné	plný	k2eAgFnPc1d1	plná
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
méně	málo	k6eAd2	málo
bohatě	bohatě	k6eAd1	bohatě
větvených	větvený	k2eAgFnPc2d1	větvená
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Mizu-giwa	Mizuiwa	k1gFnSc1	Mizu-giwa
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
asi	asi	k9	asi
devět	devět	k4xCc4	devět
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
listy	list	k1gInPc1	list
začínají	začínat	k5eAaImIp3nP	začínat
padat	padat	k5eAaImF	padat
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
vidí	vidět	k5eAaImIp3nS	vidět
více	hodně	k6eAd2	hodně
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vtomto	vtomt	k2eAgNnSc1d1	vtomto
období	období	k1gNnSc1	období
panuje	panovat	k5eAaImIp3nS	panovat
nálada	nálada	k1gFnSc1	nálada
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
klid	klid	k1gInSc4	klid
a	a	k8xC	a
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
kompozic	kompozice	k1gFnPc2	kompozice
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
nebo	nebo	k8xC	nebo
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
krásu	krása	k1gFnSc4	krása
linie	linie	k1gFnSc2	linie
než	než	k8xS	než
listy	list	k1gInPc4	list
nebo	nebo	k8xC	nebo
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
aranžmá	aranžmá	k1gNnSc1	aranžmá
provedeno	provést	k5eAaPmNgNnS	provést
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
několika	několik	k4yIc7	několik
křivkami	křivka	k1gFnPc7	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
a	a	k8xC	a
plodenství	plodenství	k1gNnSc1	plodenství
nebo	nebo	k8xC	nebo
větve	větev	k1gFnPc1	větev
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
vice	vika	k1gFnSc3	vika
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
symbolizující	symbolizující	k2eAgNnSc1d1	symbolizující
lidstvo	lidstvo	k1gNnSc4	lidstvo
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
nemají	mít	k5eNaImIp3nP	mít
být	být	k5eAaImF	být
směřovány	směřován	k2eAgInPc1d1	směřován
k	k	k7c3	k
přední	přední	k2eAgFnSc3d1	přední
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
mírně	mírně	k6eAd1	mírně
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
linie	linie	k1gFnSc1	linie
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
a	a	k8xC	a
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
jako	jako	k8xS	jako
větve	větev	k1gFnPc1	větev
stromu	strom	k1gInSc2	strom
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
rostliny	rostlina	k1gFnPc1	rostlina
mají	mít	k5eAaImIp3nP	mít
řidší	řídký	k2eAgInSc4d2	řidší
růst	růst	k1gInSc4	růst
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
bezlistého	bezlistý	k2eAgInSc2d1	bezlistý
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mizu-giwa	mizuiwa	k1gFnSc1	mizu-giwa
(	(	kIx(	(
<g/>
水	水	k?	水
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
jiném	jiný	k2eAgNnSc6d1	jiné
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
výšku	výška	k1gFnSc4	výška
jedenáct	jedenáct	k4xCc4	jedenáct
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zásady	zásada	k1gFnSc2	zásada
aranžování	aranžování	k1gNnSc2	aranžování
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ikebana	ikebana	k1gFnSc1	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
kompozici	kompozice	k1gFnSc6	kompozice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
materiály	materiál	k1gInPc1	materiál
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zásady	zásada	k1gFnPc4	zásada
úpravy	úprava	k1gFnSc2	úprava
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aranžování	aranžování	k1gNnSc6	aranžování
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
moribana	moribana	k1gFnSc1	moribana
je	být	k5eAaImIp3nS	být
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
kenzan	kenzan	k1gInSc4	kenzan
kovový	kovový	k2eAgMnSc1d1	kovový
ježek	ježek	k1gMnSc1	ježek
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vpichují	vpichovat	k5eAaImIp3nP	vpichovat
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aranžování	aranžování	k1gNnSc6	aranžování
rikka	rikek	k1gInSc2	rikek
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
také	také	k9	také
slaměné	slaměný	k2eAgFnPc1d1	slaměná
podložky	podložka	k1gFnPc1	podložka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
aranžování	aranžování	k1gNnSc3	aranžování
květin	květina	k1gFnPc2	květina
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ikebana	ikebana	k1gFnSc1	ikebana
florex	florex	k1gInSc1	florex
(	(	kIx(	(
<g/>
oasis	oasis	k1gInSc1	oasis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
aranžování	aranžování	k1gNnPc2	aranžování
ikebany	ikebana	k1gFnSc2	ikebana
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
býval	bývat	k5eAaImAgInS	bývat
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
aranžování	aranžování	k1gNnSc4	aranžování
systém	systém	k1gInSc4	systém
opor	opora	k1gFnPc2	opora
<g/>
,	,	kIx,	,
kolíků	kolík	k1gInPc2	kolík
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
kubari	kubari	k6eAd1	kubari
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
správných	správný	k2eAgInPc2d1	správný
materiálů	materiál	k1gInPc2	materiál
pro	pro	k7c4	pro
ikebanu	ikebana	k1gFnSc4	ikebana
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
kterou	který	k3yQgFnSc4	který
nelze	lze	k6eNd1	lze
řešit	řešit	k5eAaImF	řešit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
vyjmenováním	vyjmenování	k1gNnSc7	vyjmenování
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
estetický	estetický	k2eAgInSc4d1	estetický
výrazový	výrazový	k2eAgInSc4d1	výrazový
prostředek	prostředek	k1gInSc4	prostředek
odrážející	odrážející	k2eAgInSc4d1	odrážející
individuální	individuální	k2eAgInSc4d1	individuální
záměr	záměr	k1gInSc4	záměr
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
správný	správný	k2eAgInSc1d1	správný
výběr	výběr	k1gInSc1	výběr
lze	lze	k6eAd1	lze
naučit	naučit	k5eAaPmF	naučit
až	až	k9	až
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
vnímáním	vnímání	k1gNnSc7	vnímání
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
rozvíjením	rozvíjení	k1gNnSc7	rozvíjení
prostorové	prostorový	k2eAgFnSc2d1	prostorová
a	a	k8xC	a
barevné	barevný	k2eAgFnSc2d1	barevná
představivosti	představivost	k1gFnSc2	představivost
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc1	materiál
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označené	označený	k2eAgNnSc1d1	označené
"	"	kIx"	"
<g/>
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
tvarování	tvarování	k1gNnPc2	tvarování
nebo	nebo	k8xC	nebo
prořezávání	prořezávání	k1gNnPc2	prořezávání
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1	Japonské
umění	umění	k1gNnSc1	umění
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Japonsko	Japonsko	k1gNnSc1	Japonsko
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
spíše	spíše	k9	spíše
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
převládající	převládající	k2eAgInSc4d1	převládající
evropský	evropský	k2eAgInSc4d1	evropský
styl	styl	k1gInSc4	styl
a	a	k8xC	a
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1	Japonské
umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nahlíženo	nahlížen	k2eAgNnSc4d1	nahlíženo
skrze	skrze	k?	skrze
uznávané	uznávaný	k2eAgFnSc2d1	uznávaná
evropské	evropský	k2eAgFnSc2d1	Evropská
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obdiv	obdiv	k1gInSc1	obdiv
k	k	k7c3	k
bonsajím	bonsaj	k1gFnPc3	bonsaj
a	a	k8xC	a
gejšám	gejša	k1gFnPc3	gejša
<g/>
,	,	kIx,	,
sakurám	sakura	k1gFnPc3	sakura
<g/>
,	,	kIx,	,
ukijo-e	ukijot	k5eAaPmIp3nS	ukijo-at
(	(	kIx(	(
<g/>
浮	浮	k?	浮
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
malebné	malebný	k2eAgFnSc2d1	malebná
a	a	k8xC	a
zdobné	zdobný	k2eAgFnSc2d1	zdobná
secesní	secesní	k2eAgFnSc2d1	secesní
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přichází	přicházet	k5eAaImIp3nS	přicházet
japonská	japonský	k2eAgFnSc1d1	japonská
kultura	kultura	k1gFnSc1	kultura
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
obdivu	obdiv	k1gInSc2	obdiv
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
japonské	japonský	k2eAgFnSc2d1	japonská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
Evropané	Evropan	k1gMnPc1	Evropan
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
úžasu	úžas	k1gInSc6	úžas
před	před	k7c7	před
kaligrafií	kaligrafie	k1gFnSc7	kaligrafie
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
書	書	k?	書
[	[	kIx(	[
<g/>
šodó	šodó	k1gNnSc1	šodó
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekturou	architektura	k1gFnSc7	architektura
japonského	japonský	k2eAgInSc2d1	japonský
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
tušovou	tušový	k2eAgFnSc7d1	tušová
malbou	malba	k1gFnSc7	malba
a	a	k8xC	a
ikebanou	ikebana	k1gFnSc7	ikebana
které	který	k3yIgInPc4	který
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
omezenou	omezený	k2eAgFnSc4d1	omezená
společenskou	společenský	k2eAgFnSc4d1	společenská
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgInPc4d1	japonský
koníčky	koníček	k1gInPc4	koníček
(	(	kIx(	(
<g/>
hobby	hobby	k1gNnSc2	hobby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
nahlížet	nahlížet	k5eAaImF	nahlížet
na	na	k7c4	na
ikebanu	ikebana	k1gFnSc4	ikebana
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
význam	význam	k1gInSc4	význam
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
na	na	k7c4	na
ikebanu	ikebana	k1gFnSc4	ikebana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
Evropany	Evropan	k1gMnPc4	Evropan
přizpůsobena	přizpůsoben	k2eAgNnPc4d1	přizpůsobeno
pro	pro	k7c4	pro
evropské	evropský	k2eAgInPc4d1	evropský
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
často	často	k6eAd1	často
domnívají	domnívat	k5eAaImIp3nP	domnívat
že	že	k8xS	že
ikebana	ikebana	k1gFnSc1	ikebana
je	být	k5eAaImIp3nS	být
každodenní	každodenní	k2eAgFnSc7d1	každodenní
součástí	součást	k1gFnSc7	součást
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Ikebana	ikebana	k1gFnSc1	ikebana
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
období	období	k1gNnSc6	období
široce	široko	k6eAd1	široko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
vrstvami	vrstva	k1gFnPc7	vrstva
kastovního	kastovní	k2eAgInSc2d1	kastovní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
buddhistických	buddhistický	k2eAgInPc2d1	buddhistický
rituálů	rituál	k1gInPc2	rituál
šlechty	šlechta	k1gFnSc2	šlechta
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
(	(	kIx(	(
<g/>
styl	styl	k1gInSc1	styl
rikka	rikek	k1gInSc2	rikek
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měšťany	měšťan	k1gMnPc4	měšťan
(	(	kIx(	(
<g/>
styl	styl	k1gInSc1	styl
seika	seiek	k1gInSc2	seiek
(	(	kIx(	(
<g/>
生	生	k?	生
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ikebana	ikebana	k1gFnSc1	ikebana
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
mezi	mezi	k7c7	mezi
buržoazií	buržoazie	k1gFnSc7	buržoazie
(	(	kIx(	(
<g/>
styl	styl	k1gInSc1	styl
moribana	moriban	k1gMnSc2	moriban
a	a	k8xC	a
džijúka	džijúek	k1gMnSc2	džijúek
<g/>
,	,	kIx,	,
počátek	počátek	k1gInSc4	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ikebanou	ikebana	k1gFnSc7	ikebana
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
aktivně	aktivně	k6eAd1	aktivně
zabývají	zabývat	k5eAaImIp3nP	zabývat
čtyři	čtyři	k4xCgFnPc1	čtyři
skupiny	skupina	k1gFnPc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
učitelé	učitel	k1gMnPc1	učitel
ikebany	ikebana	k1gFnSc2	ikebana
studenti	student	k1gMnPc1	student
ikebany	ikebana	k1gFnPc4	ikebana
nepraktikující	praktikující	k2eNgMnPc1d1	nepraktikující
absolventi	absolvent	k1gMnPc1	absolvent
studia	studio	k1gNnSc2	studio
ikebany	ikebana	k1gFnSc2	ikebana
zájemci	zájemce	k1gMnSc3	zájemce
o	o	k7c4	o
ikebanu	ikebana	k1gFnSc4	ikebana
Učitelé	učitel	k1gMnPc1	učitel
ikebany	ikebana	k1gFnSc2	ikebana
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
většinu	většina	k1gFnSc4	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skupin	skupina	k1gFnPc2	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
milionové	milionový	k2eAgFnSc6d1	milionová
skupině	skupina	k1gFnSc6	skupina
Sógecukai	Sógecuka	k1gFnSc2	Sógecuka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
učiteli	učitel	k1gMnPc7	učitel
i	i	k8xC	i
žáky	žák	k1gMnPc7	žák
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
<g/>
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tendence	tendence	k1gFnSc1	tendence
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
nepraktikujících	praktikující	k2eNgMnPc2d1	nepraktikující
absolventů	absolvent	k1gMnPc2	absolvent
studia	studio	k1gNnSc2	studio
ikebany	ikebana	k1gFnSc2	ikebana
je	být	k5eAaImIp3nS	být
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Studenty	student	k1gMnPc7	student
ikebany	ikebana	k1gFnSc2	ikebana
jsou	být	k5eAaImIp3nP	být
příslušnice	příslušnice	k1gFnSc1	příslušnice
japonské	japonský	k2eAgFnSc2d1	japonská
maloburžoazie	maloburžoazie	k1gFnSc2	maloburžoazie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
náklady	náklad	k1gInPc7	náklad
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
především	především	k6eAd1	především
estetikou	estetika	k1gFnSc7	estetika
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
už	už	k6eAd1	už
smysl	smysl	k1gInSc4	smysl
nějak	nějak	k6eAd1	nějak
zdobit	zdobit	k5eAaImF	zdobit
<g/>
.	.	kIx.	.
</s>
<s>
Zájemkyně	zájemkyně	k1gFnSc1	zájemkyně
o	o	k7c4	o
ikebanu	ikebana	k1gFnSc4	ikebana
rekrutují	rekrutovat	k5eAaImIp3nP	rekrutovat
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
movitějších	movitý	k2eAgFnPc6d2	movitější
kastách	kasta	k1gFnPc6	kasta
japonské	japonský	k2eAgFnSc2d1	japonská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
polovinu	polovina	k1gFnSc4	polovina
studentů	student	k1gMnPc2	student
ikebany	ikebana	k1gFnSc2	ikebana
tvořily	tvořit	k5eAaImAgFnP	tvořit
mladé	mladý	k2eAgFnPc1d1	mladá
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
neprovdané	provdaný	k2eNgNnSc1d1	neprovdané
<g/>
)	)	kIx)	)
úřednice	úřednice	k1gFnSc1	úřednice
a	a	k8xC	a
zaměstnankyně	zaměstnankyně	k1gFnSc1	zaměstnankyně
nevýrobních	výrobní	k2eNgNnPc2d1	nevýrobní
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Ikebana	ikebana	k1gFnSc1	ikebana
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
zakořeněných	zakořeněný	k2eAgFnPc2d1	zakořeněná
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
ikebana	ikebana	k1gFnSc1	ikebana
vnímána	vnímán	k2eAgFnSc1d1	vnímána
jako	jako	k9	jako
pokrokový	pokrokový	k2eAgInSc4d1	pokrokový
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc4d1	moderní
a	a	k8xC	a
progresivní	progresivní	k2eAgInSc4d1	progresivní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Natolik	natolik	k6eAd1	natolik
progresivní	progresivní	k2eAgNnSc1d1	progresivní
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
neochotně	ochotně	k6eNd1	ochotně
přijímán	přijímat	k5eAaImNgMnS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Ikebana	ikebana	k1gFnSc1	ikebana
přesto	přesto	k8xC	přesto
představuje	představovat	k5eAaImIp3nS	představovat
umění	umění	k1gNnSc1	umění
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
především	především	k9	především
množství	množství	k1gNnSc1	množství
feudálních	feudální	k2eAgFnPc2d1	feudální
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
náboženských	náboženský	k2eAgFnPc2d1	náboženská
idejí	idea	k1gFnPc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
působí	působit	k5eAaImIp3nS	působit
asi	asi	k9	asi
3	[number]	k4	3
200	[number]	k4	200
různých	různý	k2eAgFnPc2d1	různá
škol	škola	k1gFnPc2	škola
aranžování	aranžování	k1gNnSc2	aranžování
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Školy	škola	k1gFnPc1	škola
preferují	preferovat	k5eAaImIp3nP	preferovat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc2d1	vlastní
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
linií	linie	k1gFnSc7	linie
,	,	kIx,	,
a	a	k8xC	a
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
jing	jing	k1gInSc1	jing
<g/>
"	"	kIx"	"
陰	陰	k?	陰
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jó	jó	k0	jó
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
jang	jang	k1gInSc1	jang
<g/>
"	"	kIx"	"
<g/>
陽	陽	k?	陽
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc4	název
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
aranžmá	aranžmá	k1gNnSc2	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dodržovány	dodržován	k2eAgFnPc4d1	dodržována
pravidla	pravidlo	k1gNnPc4	pravidlo
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
směřováním	směřování	k1gNnSc7	směřování
linie	linie	k1gFnSc2	linie
symbolizující	symbolizující	k2eAgFnSc2d1	symbolizující
zemi	zem	k1gFnSc3	zem
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
ženský	ženský	k2eAgInSc4d1	ženský
typ	typ	k1gInSc4	typ
aranžmá	aranžmá	k1gNnSc2	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
Aranžmá	aranžmá	k1gNnSc1	aranžmá
má	mít	k5eAaImIp3nS	mít
linii	linie	k1gFnSc4	linie
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jó	jó	k0	jó
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
mužský	mužský	k2eAgInSc4d1	mužský
typ	typ	k1gInSc4	typ
aranžmá	aranžmá	k1gNnSc2	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
Aranžmá	aranžmá	k1gNnSc1	aranžmá
má	mít	k5eAaImIp3nS	mít
linii	linie	k1gFnSc4	linie
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
základního	základní	k2eAgNnSc2d1	základní
směrově	směrově	k6eAd1	směrově
určeného	určený	k2eAgNnSc2d1	určené
uspořádání	uspořádání	k1gNnSc2	uspořádání
linií	linie	k1gFnPc2	linie
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
mužský	mužský	k2eAgInSc1d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc1d1	ženský
typ	typ	k1gInSc1	typ
aranžmá	aranžmá	k1gNnSc2	aranžmá
i	i	k8xC	i
dalšími	další	k2eAgFnPc7d1	další
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
v	v	k7c6	v
textuře	textura	k1gFnSc6	textura
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
charakteristikách	charakteristika	k1gFnPc6	charakteristika
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úhel	úhel	k1gInSc1	úhel
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
však	však	k9	však
dán	dán	k2eAgInSc1d1	dán
japonskou	japonský	k2eAgFnSc7d1	japonská
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
úhlem	úhel	k1gInSc7	úhel
pohledu	pohled	k1gInSc2	pohled
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
významů	význam	k1gInPc2	význam
a	a	k8xC	a
symbolů	symbol	k1gInPc2	symbol
spojeni	spojit	k5eAaPmNgMnP	spojit
s	s	k7c7	s
typy	typ	k1gInPc7	typ
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
však	však	k9	však
lhostejné	lhostejný	k2eAgNnSc1d1	lhostejné
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
typ	typ	k1gInSc4	typ
aranžér	aranžér	k1gMnSc1	aranžér
připraví	připravit	k5eAaPmIp3nS	připravit
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
posouzení	posouzení	k1gNnSc1	posouzení
toho	ten	k3xDgNnSc2	ten
jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
aranžmá	aranžmá	k1gNnSc3	aranžmá
připraveny	připraven	k2eAgFnPc1d1	připravena
<g/>
.	.	kIx.	.
</s>
<s>
Uměním	umění	k1gNnSc7	umění
aranžování	aranžování	k1gNnPc4	aranžování
květin	květina	k1gFnPc2	květina
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
skupiny	skupina	k1gFnPc1	skupina
aranžérů	aranžér	k1gMnPc2	aranžér
<g/>
,	,	kIx,	,
sdružené	sdružený	k2eAgFnPc1d1	sdružená
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
"	"	kIx"	"
<g/>
školách	škola	k1gFnPc6	škola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
školy	škola	k1gFnPc1	škola
nejsou	být	k5eNaImIp3nP	být
svou	svůj	k3xOyFgFnSc7	svůj
podstatou	podstata	k1gFnSc7	podstata
pouze	pouze	k6eAd1	pouze
místa	místo	k1gNnPc4	místo
sloužící	sloužící	k2eAgNnPc4d1	sloužící
k	k	k7c3	k
výuce	výuka	k1gFnSc3	výuka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
a	a	k8xC	a
rozvíjení	rozvíjení	k1gNnSc3	rozvíjení
kultury	kultura	k1gFnSc2	kultura
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
součástí	součást	k1gFnPc2	součást
buddhistického	buddhistický	k2eAgNnSc2d1	buddhistické
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
školy	škola	k1gFnPc1	škola
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
pojetím	pojetí	k1gNnSc7	pojetí
a	a	k8xC	a
především	především	k9	především
pojmenováním	pojmenování	k1gNnSc7	pojmenování
některých	některý	k3yIgFnPc2	některý
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
principů	princip	k1gInPc2	princip
a	a	k8xC	a
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
školu	škola	k1gFnSc4	škola
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
která	který	k3yQgFnSc1	který
později	pozdě	k6eAd2	pozdě
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
styl	styl	k1gInSc4	styl
ikebany	ikebana	k1gFnSc2	ikebana
založil	založit	k5eAaPmAgInS	založit
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc1	Imoko
(	(	kIx(	(
<g/>
小	小	k?	小
妹	妹	k?	妹
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
607-608	[number]	k4	607-608
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
císařekého	císařeký	k2eAgInSc2d1	císařeký
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
aranžmá	aranžmá	k1gNnSc4	aranžmá
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ikebana	ikebana	k1gFnSc1	ikebana
v	v	k7c6	v
buddhistickém	buddhistický	k2eAgInSc6d1	buddhistický
klášteře	klášter	k1gInSc6	klášter
Rokkakudo	Rokkakudo	k1gNnSc4	Rokkakudo
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
u	u	k7c2	u
města	město	k1gNnSc2	město
Kjóto	Kjót	k2eAgNnSc1d1	Kjóto
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc4	Imoko
nazval	nazvat	k5eAaBmAgMnS	nazvat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ikenobó	Ikenobó	k1gFnSc6	Ikenobó
(	(	kIx(	(
<g/>
池	池	k?	池
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
tak	tak	k6eAd1	tak
nazýval	nazývat	k5eAaImAgInS	nazývat
(	(	kIx(	(
<g/>
池	池	k?	池
<g/>
)	)	kIx)	)
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
..	..	k?	..
Samotný	samotný	k2eAgInSc1d1	samotný
vznik	vznik	k1gInSc1	vznik
prvního	první	k4xOgInSc2	první
skutečného	skutečný	k2eAgInSc2d1	skutečný
stylu	styl	k1gInSc2	styl
ikebany	ikebana	k1gFnSc2	ikebana
-	-	kIx~	-
tatebana	tatebana	k1gFnSc1	tatebana
je	být	k5eAaImIp3nS	být
však	však	k9	však
datován	datovat	k5eAaImNgInS	datovat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
třináctého	třináctý	k4xOgInSc2	třináctý
či	či	k8xC	či
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc1	Imoko
přivezl	přivézt	k5eAaPmAgInS	přivézt
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
čínskou	čínský	k2eAgFnSc4d1	čínská
buddhistickou	buddhistický	k2eAgFnSc4d1	buddhistická
tradici	tradice	k1gFnSc4	tradice
umění	umění	k1gNnSc1	umění
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
(	(	kIx(	(
<g/>
kuge	kuge	k1gInSc1	kuge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
způsobu	způsob	k1gInSc2	způsob
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
který	který	k3yIgInSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc1	Imoko
se	se	k3xPyFc4	se
ikebana	ikebana	k1gFnSc1	ikebana
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
že	že	k8xS	že
"	"	kIx"	"
<g/>
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc4	Imoko
založil	založit	k5eAaPmAgMnS	založit
ikebanu	ikebana	k1gFnSc4	ikebana
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
zkratkou	zkratka	k1gFnSc7	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikají	vznikat	k5eAaImIp3nP	vznikat
školy	škola	k1gFnPc4	škola
Ohara	Ohar	k1gInSc2	Ohar
(	(	kIx(	(
<g/>
小	小	k?	小
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sógecu	Sógeca	k1gFnSc4	Sógeca
(	(	kIx(	(
<g/>
草	草	k?	草
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kózan	Kózan	k1gMnSc1	Kózan
(	(	kIx(	(
<g/>
広	広	k?	広
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Korjú	Korjú	k1gMnSc1	Korjú
šóókai	šóóka	k1gFnSc2	šóóka
(	(	kIx(	(
<g/>
古	古	k?	古
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rjúsei-ha	Rjúseia	k1gFnSc1	Rjúsei-ha
(	(	kIx(	(
<g/>
龍	龍	k?	龍
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kokusai	Kokusai	k1gNnSc1	Kokusai
(	(	kIx(	(
<g/>
国	国	k?	国
-	-	kIx~	-
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
školy	škola	k1gFnPc1	škola
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
typy	typ	k1gInPc4	typ
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
styl	styl	k1gInSc1	styl
ikebany	ikebana	k1gFnSc2	ikebana
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
tatebana	tateban	k1gMnSc2	tateban
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
vztyčené	vztyčený	k2eAgFnPc4d1	vztyčená
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInSc1d2	novější
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc1	styl
rikka	rikek	k1gInSc2	rikek
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
při	při	k7c6	při
čajovém	čajový	k2eAgInSc6d1	čajový
obřadu	obřad	k1gInSc6	obřad
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
čabana	čabana	k1gFnSc1	čabana
(	(	kIx(	(
<g/>
茶	茶	k?	茶
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nageire	Nageir	k1gMnSc5	Nageir
(	(	kIx(	(
<g/>
投	投	k?	投
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
netvarovaného	tvarovaný	k2eNgInSc2d1	netvarovaný
designu	design	k1gInSc2	design
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
seika	seiek	k1gInSc2	seiek
nebo	nebo	k8xC	nebo
šóka	šók	k1gInSc2	šók
(	(	kIx(	(
<g/>
生	生	k?	生
<g/>
)	)	kIx)	)
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Seika	Seika	k1gMnSc1	Seika
nebo	nebo	k8xC	nebo
šóka	šóka	k1gMnSc1	šóka
styl	styl	k1gInSc4	styl
je	být	k5eAaImIp3nS	být
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
úprava	úprava	k1gFnSc1	úprava
květin	květina	k1gFnPc2	květina
v	v	k7c6	v
asymetrické	asymetrický	k2eAgFnSc6d1	asymetrická
kompozici	kompozice	k1gFnSc6	kompozice
tvořící	tvořící	k2eAgInSc1d1	tvořící
tvar	tvar	k1gInSc1	tvar
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Květiny	květina	k1gFnPc1	květina
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
tokonomě	tokonomě	k6eAd1	tokonomě
(	(	kIx(	(
<g/>
床	床	k?	床
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
je	být	k5eAaImIp3nS	být
respektováno	respektován	k2eAgNnSc1d1	respektováno
pravidlo	pravidlo	k1gNnSc1	pravidlo
umisťovat	umisťovat	k5eAaImF	umisťovat
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
aranžmá	aranžmá	k1gNnSc4	aranžmá
ikebana	ikebana	k1gFnSc1	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
publikacích	publikace	k1gFnPc6	publikace
se	se	k3xPyFc4	se
názvy	název	k1gInPc7	název
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
prvků	prvek	k1gInPc2	prvek
ikebany	ikebana	k1gFnSc2	ikebana
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
předáváno	předávat	k5eAaImNgNnS	předávat
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
ústním	ústní	k2eAgNnSc7d1	ústní
podáním	podání	k1gNnSc7	podání
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mistři	mistr	k1gMnPc1	mistr
osvojili	osvojit	k5eAaPmAgMnP	osvojit
nějaký	nějaký	k3yIgInSc4	nějaký
směr	směr	k1gInSc4	směr
aranžování	aranžování	k1gNnSc2	aranžování
který	který	k3yQgInSc4	který
nějak	nějak	k6eAd1	nějak
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
a	a	k8xC	a
poté	poté	k6eAd1	poté
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
rozeznávány	rozeznávat	k5eAaImNgInP	rozeznávat
především	především	k9	především
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
ikebany	ikebana	k1gFnSc2	ikebana
-	-	kIx~	-
moribana	moribana	k1gFnSc1	moribana
(	(	kIx(	(
<g/>
盛	盛	k?	盛
<g/>
)	)	kIx)	)
a	a	k8xC	a
nageire	nageir	k1gInSc5	nageir
(	(	kIx(	(
<g/>
投	投	k?	投
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Racionální	racionální	k2eAgInSc1d1	racionální
způsob	způsob	k1gInSc1	způsob
analýzy	analýza	k1gFnSc2	analýza
ikebany	ikebana	k1gFnSc2	ikebana
však	však	k9	však
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
dobře	dobře	k6eAd1	dobře
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ikebany	ikebana	k1gFnSc2	ikebana
se	se	k3xPyFc4	se
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
čtyři	čtyři	k4xCgInPc1	čtyři
typy	typ	k1gInPc1	typ
úpravy	úprava	k1gFnSc2	úprava
<g/>
:	:	kIx,	:
vertikální	vertikální	k2eAgFnSc2d1	vertikální
<g/>
,	,	kIx,	,
horizontální	horizontální	k2eAgFnSc2d1	horizontální
<g/>
,	,	kIx,	,
nakloněný	nakloněný	k2eAgMnSc1d1	nakloněný
a	a	k8xC	a
skloněný	skloněný	k2eAgMnSc1d1	skloněný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsou	být	k5eAaImIp3nP	být
rozeznávány	rozeznáván	k2eAgFnPc1d1	rozeznávána
pravá	pravá	k1gFnSc1	pravá
a	a	k8xC	a
levá	levý	k2eAgFnSc1d1	levá
varianta	varianta	k1gFnSc1	varianta
(	(	kIx(	(
<g/>
běžná	běžný	k2eAgFnSc1d1	běžná
a	a	k8xC	a
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tatebana	Tatebana	k1gFnSc1	Tatebana
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
styl	styl	k1gInSc4	styl
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
historicky	historicky	k6eAd1	historicky
nejstarší	starý	k2eAgInSc1d3	nejstarší
styl	styl	k1gInSc1	styl
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
,	,	kIx,	,
styl	styl	k1gInSc4	styl
který	který	k3yQgInSc4	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
během	během	k7c2	během
období	období	k1gNnSc2	období
Muromači	Muromač	k1gMnSc3	Muromač
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
nejstarší	starý	k2eAgInSc1d3	nejstarší
styl	styl	k1gInSc1	styl
ikebany	ikebana	k1gFnSc2	ikebana
Kuge	Kug	k1gFnSc2	Kug
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
dokonce	dokonce	k9	dokonce
styly	styl	k1gInPc1	styl
Joriširo	Joriširo	k1gNnSc1	Joriširo
(	(	kIx(	(
<g/>
依	依	k?	依
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kuge	Kuge	k1gFnSc1	Kuge
(	(	kIx(	(
<g/>
供	供	k?	供
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
<g/>
/	/	kIx~	/
<g/>
moderněji	moderně	k6eAd2	moderně
čtené	čtený	k2eAgNnSc1d1	čtené
[	[	kIx(	[
<g/>
kuka	kuka	k1gFnSc1	kuka
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
buddhistické	buddhistický	k2eAgFnSc6d1	buddhistická
terminologii	terminologie	k1gFnSc6	terminologie
též	též	k9	též
仏	仏	k?	仏
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
bukka	bukka	k6eAd1	bukka
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
bucubana	bucubana	k1gFnSc1	bucubana
<g/>
]	]	kIx)	]
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
uvedeny	uvést	k5eAaPmNgInP	uvést
jako	jako	k8xC	jako
způsoby	způsob	k1gInPc1	způsob
aranžování	aranžování	k1gNnPc2	aranžování
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ikebana	ikebana	k1gFnSc1	ikebana
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
styly	styl	k1gInPc1	styl
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Kuge	Kuge	k6eAd1	Kuge
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
buddhistickou	buddhistický	k2eAgFnSc4d1	buddhistická
úpravu	úprava	k1gFnSc4	úprava
květin	květina	k1gFnPc2	květina
pro	pro	k7c4	pro
oltáře	oltář	k1gInPc4	oltář
<g/>
,	,	kIx,	,
používanou	používaný	k2eAgFnSc4d1	používaná
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
buddhismus	buddhismus	k1gInSc1	buddhismus
šířil	šířit	k5eAaImAgMnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
květin	květina	k1gFnPc2	květina
na	na	k7c4	na
oltáře	oltář	k1gInPc4	oltář
(	(	kIx(	(
<g/>
kuge	kuge	k1gInSc4	kuge
<g/>
)	)	kIx)	)
Ono	onen	k3xDgNnSc1	onen
no	no	k9	no
Imoko	Imoko	k1gNnSc1	Imoko
(	(	kIx(	(
<g/>
小	小	k?	小
妹	妹	k?	妹
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
ikebany	ikebana	k1gFnSc2	ikebana
zřejmě	zřejmě	k6eAd1	zřejmě
viděl	vidět	k5eAaImAgMnS	vidět
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
buddhistickém	buddhistický	k2eAgInSc6d1	buddhistický
klášteru	klášter	k1gInSc3	klášter
napodobit	napodobit	k5eAaPmF	napodobit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
úprav	úprava	k1gFnPc2	úprava
pak	pak	k6eAd1	pak
vzniká	vznikat	k5eAaImIp3nS	vznikat
tatebana	tatebana	k1gFnSc1	tatebana
<g/>
.	.	kIx.	.
</s>
<s>
Aranžmá	aranžmá	k1gNnSc7	aranžmá
tatebana	tateban	k1gMnSc2	tateban
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
do	do	k7c2	do
vázy	váza	k1gFnSc2	váza
z	z	k7c2	z
úzkým	úzký	k2eAgNnSc7d1	úzké
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgNnSc4d1	masivní
aranžmá	aranžmá	k1gNnSc4	aranžmá
tvořila	tvořit	k5eAaImAgFnS	tvořit
větev	větev	k1gFnSc1	větev
z	z	k7c2	z
borovice	borovice	k1gFnSc2	borovice
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
byly	být	k5eAaImAgInP	být
menší	malý	k2eAgInPc1d2	menší
květy	květ	k1gInPc1	květ
výrazné	výrazný	k2eAgInPc1d1	výrazný
listy	list	k1gInPc1	list
nebo	nebo	k8xC	nebo
plody	plod	k1gInPc1	plod
a	a	k8xC	a
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
větší	veliký	k2eAgInPc4d2	veliký
květy	květ	k1gInPc4	květ
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
v	v	k7c6	v
aranžmá	aranžmá	k1gNnSc6	aranžmá
působil	působit	k5eAaImAgInS	působit
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
přírodně	přírodně	k6eAd1	přírodně
a	a	k8xC	a
linie	linie	k1gFnPc1	linie
byly	být	k5eAaImAgFnP	být
méně	málo	k6eAd2	málo
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byla	být	k5eAaImAgFnS	být
osa	osa	k1gFnSc1	osa
aranžmá	aranžmá	k1gNnSc2	aranžmá
spíše	spíše	k9	spíše
vertikální	vertikální	k2eAgNnSc4d1	vertikální
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stylu	styl	k1gInSc2	styl
tatebana	tateban	k1gMnSc2	tateban
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
později	pozdě	k6eAd2	pozdě
styl	styl	k1gInSc4	styl
rikka	rikko	k1gNnSc2	rikko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rikka	Rikek	k1gInSc2	Rikek
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
rikka	rikek	k1gInSc2	rikek
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
tatebana	tateban	k1gMnSc2	tateban
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
a	a	k8xC	a
aranžování	aranžování	k1gNnSc1	aranžování
květin	květina	k1gFnPc2	květina
pro	pro	k7c4	pro
buddhistické	buddhistický	k2eAgInPc4d1	buddhistický
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
se	se	k3xPyFc4	se
vydělilo	vydělit	k5eAaPmAgNnS	vydělit
sedm	sedm	k4xCc1	sedm
hlavních	hlavní	k2eAgFnPc2d1	hlavní
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
devíti	devět	k4xCc2	devět
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každý	každý	k3xTgInSc1	každý
podporuje	podporovat	k5eAaImIp3nS	podporovat
jiné	jiný	k2eAgFnPc4d1	jiná
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
linie	linie	k1gFnPc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgNnP	být
vytvořena	vytvořen	k2eAgNnPc1d1	vytvořeno
důležitá	důležitý	k2eAgNnPc1d1	důležité
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
charakteru	charakter	k1gInSc3	charakter
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
délky	délka	k1gFnPc4	délka
a	a	k8xC	a
kombinace	kombinace	k1gFnPc4	kombinace
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
kenzanu	kenzan	k1gInSc2	kenzan
nebo	nebo	k8xC	nebo
Komiwara	Komiwara	k1gFnSc1	Komiwara
(	(	kIx(	(
<g/>
svazky	svazek	k1gInPc1	svazek
slámy	sláma	k1gFnSc2	sláma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
podoba	podoba	k1gFnSc1	podoba
stylu	styl	k1gInSc2	styl
je	být	k5eAaImIp3nS	být
rikka	rikka	k6eAd1	rikka
šimpútai	šimpútai	k6eAd1	šimpútai
(	(	kIx(	(
<g/>
představena	představen	k2eAgFnSc1d1	představena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čabana	Čabana	k1gFnSc1	Čabana
(	(	kIx(	(
<g/>
茶	茶	k?	茶
hepburnův	hepburnův	k2eAgInSc4d1	hepburnův
přepis	přepis	k1gInSc4	přepis
<g/>
:	:	kIx,	:
chabana	chabana	k1gFnSc1	chabana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jemně	jemně	k6eAd1	jemně
uspořádané	uspořádaný	k2eAgNnSc1d1	uspořádané
aranžmá	aranžmá	k1gNnSc1	aranžmá
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
čajový	čajový	k2eAgInSc4d1	čajový
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
účastnit	účastnit	k5eAaImF	účastnit
hosté	host	k1gMnPc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Čabana	Čabana	k1gFnSc1	Čabana
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
hosta	host	k1gMnSc2	host
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
hostitele	hostitel	k1gMnSc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
přirozené	přirozený	k2eAgNnSc1d1	přirozené
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
aranžováno	aranžován	k2eAgNnSc1d1	aranžováno
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
a	a	k8xC	a
kvetoucího	kvetoucí	k2eAgInSc2d1	kvetoucí
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
naaranžováno	naaranžovat	k5eAaPmNgNnS	naaranžovat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
kimona	kimono	k1gNnSc2	kimono
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hostitel	hostitel	k1gMnSc1	hostitel
informován	informován	k2eAgMnSc1d1	informován
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
šóka	šók	k1gInSc2	šók
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
seika	seika	k6eAd1	seika
(	(	kIx(	(
<g/>
生	生	k?	生
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shō	shō	k?	shō
(	(	kIx(	(
<g/>
hepburnův	hepburnův	k2eAgInSc1d1	hepburnův
přepis	přepis	k1gInSc1	přepis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seikwa	seikw	k2eAgMnSc4d1	seikw
(	(	kIx(	(
<g/>
historický	historický	k2eAgInSc4d1	historický
<g/>
,	,	kIx,	,
obsoletní	obsoletní	k2eAgInSc4d1	obsoletní
přepis	přepis	k1gInSc4	přepis
slova	slovo	k1gNnSc2	slovo
seika	seiek	k1gInSc2	seiek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ikebono	ikebona	k1gFnSc5	ikebona
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zjednodušenou	zjednodušený	k2eAgFnSc7d1	zjednodušená
formou	forma	k1gFnSc7	forma
stylu	styl	k1gInSc2	styl
rikka	rikko	k1gNnSc2	rikko
a	a	k8xC	a
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejtradičnějších	tradiční	k2eAgFnPc2d3	nejtradičnější
forem	forma	k1gFnPc2	forma
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
šin	šin	k?	šin
(	(	kIx(	(
<g/>
真	真	k?	真
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soe	soe	k?	soe
(	(	kIx(	(
<g/>
副	副	k?	副
<g/>
)	)	kIx)	)
a	a	k8xC	a
tai	tai	k?	tai
(	(	kIx(	(
<g/>
体	体	k?	体
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
hlavních	hlavní	k2eAgFnPc2d1	hlavní
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
linie	linie	k1gFnPc1	linie
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
připojeny	připojit	k5eAaPmNgFnP	připojit
přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
sebou	se	k3xPyFc7	se
v	v	k7c6	v
kenzanu	kenzan	k1gInSc6	kenzan
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
kubari	kubari	k1gNnSc2	kubari
(	(	kIx(	(
<g/>
upevnění	upevnění	k1gNnSc2	upevnění
kousky	kousek	k1gInPc1	kousek
větví	větvit	k5eAaImIp3nP	větvit
<g/>
,	,	kIx,	,
klacíky	klacík	k1gInPc1	klacík
<g/>
,	,	kIx,	,
配	配	k?	配
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
klasickým	klasický	k2eAgInSc7d1	klasický
a	a	k8xC	a
moderním	moderní	k2eAgInSc7d1	moderní
šóka	šóka	k6eAd1	šóka
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
styl	styl	k1gInSc1	styl
šóka	šóka	k6eAd1	šóka
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
nejvýše	vysoce	k6eAd3	vysoce
dvěma	dva	k4xCgInPc7	dva
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
musí	muset	k5eAaImIp3nS	muset
původně	původně	k6eAd1	původně
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
povoleny	povolit	k5eAaPmNgFnP	povolit
jen	jen	k9	jen
klasické	klasický	k2eAgFnPc1d1	klasická
nádoby	nádoba	k1gFnPc1	nádoba
a	a	k8xC	a
upevnění	upevnění	k1gNnSc1	upevnění
květin	květina	k1gFnPc2	květina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
starou	starý	k2eAgFnSc7d1	stará
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnSc1d1	moderní
šóka	šóka	k1gMnSc1	šóka
povoluje	povolovat	k5eAaImIp3nS	povolovat
tři	tři	k4xCgInPc4	tři
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
každou	každý	k3xTgFnSc4	každý
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
nádobu	nádoba	k1gFnSc4	nádoba
a	a	k8xC	a
kenzan	kenzan	k1gInSc4	kenzan
jako	jako	k8xS	jako
spojovací	spojovací	k2eAgFnSc4d1	spojovací
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
stylu	styl	k1gInSc2	styl
je	být	k5eAaImIp3nS	být
škola	škola	k1gFnSc1	škola
Ikenobó	Ikenobó	k1gFnSc1	Ikenobó
(	(	kIx(	(
<g/>
池	池	k?	池
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aranžmá	aranžmá	k1gNnSc1	aranžmá
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
šóka	šók	k1gInSc2	šók
působí	působit	k5eAaImIp3nS	působit
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
osy	osa	k1gFnPc1	osa
jsou	být	k5eAaImIp3nP	být
velikostně	velikostně	k6eAd1	velikostně
odlišeny	odlišit	k5eAaPmNgInP	odlišit
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
rozdíl	rozdíl	k1gInSc4	rozdíl
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnPc1	linie
jsou	být	k5eAaImIp3nP	být
málo	málo	k6eAd1	málo
odkloněny	odklonit	k5eAaPmNgInP	odklonit
od	od	k7c2	od
svislice	svislice	k1gFnSc2	svislice
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
větev	větev	k1gFnSc1	větev
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
15	[number]	k4	15
<g/>
°	°	k?	°
odkloněna	odkloněn	k2eAgNnPc1d1	odkloněno
odpředu	odpředu	k6eAd1	odpředu
od	od	k7c2	od
svislice	svislice	k1gFnSc2	svislice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
linii	linie	k1gFnSc4	linie
šin	šin	k?	šin
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
větev	větev	k1gFnSc1	větev
<g/>
,	,	kIx,	,
soe	soe	k?	soe
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
větvička	větvička	k1gFnSc1	větvička
dřeviny	dřevina	k1gFnSc2	dřevina
anebo	anebo	k8xC	anebo
květina	květina	k1gFnSc1	květina
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
stonkem	stonek	k1gInSc7	stonek
<g/>
,	,	kIx,	,
na	na	k7c6	na
hikae	hikae	k1gFnSc6	hikae
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
květy	květ	k1gInPc1	květ
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
doplňkový	doplňkový	k2eAgInSc1d1	doplňkový
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
květů	květ	k1gInPc2	květ
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
chryzantémy	chryzantéma	k1gFnPc1	chryzantéma
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
kosatce	kosatec	k1gInPc1	kosatec
<g/>
,	,	kIx,	,
tulipány	tulipán	k1gInPc1	tulipán
<g/>
,	,	kIx,	,
narcisy	narcis	k1gInPc1	narcis
<g/>
,	,	kIx,	,
kamélie	kamélie	k1gFnPc1	kamélie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Šóka	Šókum	k1gNnSc2	Šókum
šófútai	šófúta	k1gFnSc2	šófúta
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
linie	linie	k1gFnPc1	linie
(	(	kIx(	(
<g/>
šin	šin	k?	šin
<g/>
,	,	kIx,	,
soe	soe	k?	soe
a	a	k8xC	a
tai	tai	k?	tai
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
dalšími	další	k2eAgFnPc7d1	další
významově	významově	k6eAd1	významově
druhořadými	druhořadý	k2eAgInPc7d1	druhořadý
prvky	prvek	k1gInPc7	prvek
aširae	aširae	k1gNnSc1	aširae
(	(	kIx(	(
<g/>
あ	あ	k?	あ
<g/>
/	/	kIx~	/
<g/>
配	配	k?	配
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
aširai	aširai	k6eAd1	aširai
(	(	kIx(	(
<g/>
あ	あ	k?	あ
<g/>
/	/	kIx~	/
<g/>
配	配	k?	配
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
krásu	krása	k1gFnSc4	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
květy	květ	k1gInPc1	květ
i	i	k8xC	i
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
upevněny	upevnit	k5eAaPmNgFnP	upevnit
v	v	k7c6	v
kenzanu	kenzan	k1gInSc6	kenzan
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
"	"	kIx"	"
<g/>
šin	šin	k?	šin
<g/>
"	"	kIx"	"
u	u	k7c2	u
šóka	šók	k1gInSc2	šók
šófútai	šófúta	k1gFnSc2	šófúta
vede	vést	k5eAaImIp3nS	vést
středem	střed	k1gInSc7	střed
aranžmá	aranžmá	k1gNnSc2	aranžmá
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
celek	celek	k1gInSc4	celek
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
"	"	kIx"	"
<g/>
soe	soe	k?	soe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
jo	jo	k9	jo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
tai	tai	k?	tai
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Soe	Soe	k?	Soe
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
růstu	růst	k1gInSc6	růst
rostliny	rostlina	k1gFnSc2	rostlina
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
tai	tai	k?	tai
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Šóka	Šókum	k1gNnSc2	Šókum
šimpútai	šimpúta	k1gFnSc2	šimpúta
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
šóka	šóka	k6eAd1	šóka
šimpútai	šimpútai	k6eAd1	šimpútai
předvedl	předvést	k5eAaPmAgMnS	předvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
Ikenobó	Ikenobó	k1gFnSc1	Ikenobó
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
<g/>
'	'	kIx"	'
<g/>
ei	ei	k?	ei
Ikenobó	Ikenobó	k1gFnSc1	Ikenobó
(	(	kIx(	(
<g/>
池	池	k?	池
専	専	k?	専
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgMnSc1d1	moderní
<g/>
,	,	kIx,	,
volnou	volný	k2eAgFnSc7d1	volná
parafrází	parafráze	k1gFnSc7	parafráze
stylu	styl	k1gInSc2	styl
šóka	šókum	k1gNnSc2	šókum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
velmi	velmi	k6eAd1	velmi
úsporně	úsporně	k6eAd1	úsporně
a	a	k8xC	a
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
linie	linie	k1gFnSc2	linie
přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
pravidel	pravidlo	k1gNnPc2	pravidlo
šóka	šók	k1gInSc2	šók
je	být	k5eAaImIp3nS	být
zmírněna	zmírnit	k5eAaPmNgFnS	zmírnit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
subjektivní	subjektivní	k2eAgFnSc2d1	subjektivní
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dřevitý	dřevitý	k2eAgInSc1d1	dřevitý
materiál	materiál	k1gInSc1	materiál
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
bylinným	bylinný	k2eAgInSc7d1	bylinný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc4d1	celkový
dojem	dojem	k1gInSc4	dojem
harmonický	harmonický	k2eAgInSc4d1	harmonický
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgInPc1d1	hlavní
směry	směr	k1gInPc1	směr
jsou	být	k5eAaImIp3nP	být
šu	šu	k?	šu
<g/>
,	,	kIx,	,
jo	jo	k9	jo
a	a	k8xC	a
aširai	aširai	k6eAd1	aširai
(	(	kIx(	(
<g/>
あ	あ	k?	あ
<g/>
/	/	kIx~	/
<g/>
配	配	k?	配
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šu	Šu	k?	Šu
a	a	k8xC	a
jo	jo	k9	jo
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
,	,	kIx,	,
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
podstatu	podstata	k1gFnSc4	podstata
a	a	k8xC	a
aširai	aširae	k1gFnSc4	aširae
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
doplněk	doplněk	k1gInSc1	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nageire	Nageir	k1gMnSc5	Nageir
<g/>
.	.	kIx.	.
</s>
<s>
Nageire	Nageir	k1gMnSc5	Nageir
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
投	投	k?	投
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aranžmá	aranžmá	k1gNnSc1	aranžmá
ve	v	k7c6	v
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
linie	linie	k1gFnPc4	linie
-	-	kIx~	-
šin	šin	k?	šin
<g/>
,	,	kIx,	,
soe	soe	k?	soe
a	a	k8xC	a
tai	tai	k?	tai
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
kubari	kubari	k6eAd1	kubari
(	(	kIx(	(
<g/>
kousky	kousek	k1gInPc4	kousek
větví	větev	k1gFnPc2	větev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
váze	váha	k1gFnSc6	váha
linie	linie	k1gFnSc2	linie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
volně	volně	k6eAd1	volně
umístěny	umístit	k5eAaPmNgFnP	umístit
stonky	stonka	k1gFnPc1	stonka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stylu	styl	k1gInSc6	styl
nageire	nageir	k1gInSc5	nageir
stonek	stonek	k1gInSc1	stonek
každé	každý	k3xTgFnSc2	každý
rostliny	rostlina	k1gFnSc2	rostlina
stojí	stát	k5eAaImIp3nS	stát
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
přirozený	přirozený	k2eAgInSc4d1	přirozený
charakter	charakter	k1gInSc4	charakter
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
a	a	k8xC	a
stonky	stonek	k1gInPc1	stonek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
křížit	křížit	k5eAaImF	křížit
<g/>
,	,	kIx,	,
ořezávají	ořezávat	k5eAaImIp3nP	ořezávat
se	se	k3xPyFc4	se
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
větvičky	větvička	k1gFnPc4	větvička
i	i	k8xC	i
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
charakter	charakter	k1gInSc4	charakter
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
nageire	nageir	k1gInSc5	nageir
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
pro	pro	k7c4	pro
aranžování	aranžování	k1gNnSc4	aranžování
do	do	k7c2	do
štíhlých	štíhlý	k2eAgFnPc2d1	štíhlá
váz	váza	k1gFnPc2	váza
a	a	k8xC	a
aranžování	aranžování	k1gNnPc2	aranžování
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Heika	Heika	k1gFnSc1	Heika
(	(	kIx(	(
<g/>
瓶	瓶	k?	瓶
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
podobný	podobný	k2eAgInSc1d1	podobný
nageire	nageir	k1gMnSc5	nageir
<g/>
.	.	kIx.	.
</s>
<s>
Aranžuje	aranžovat	k5eAaImIp3nS	aranžovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
váz	váza	k1gFnPc2	váza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
větví	větev	k1gFnPc2	větev
je	být	k5eAaImIp3nS	být
svislá	svislý	k2eAgFnSc1d1	svislá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kaskádovitého	kaskádovitý	k2eAgInSc2d1	kaskádovitý
stylu	styl	k1gInSc2	styl
heika	heika	k6eAd1	heika
je	být	k5eAaImIp3nS	být
větev	větev	k1gFnSc4	větev
šin	šin	k?	šin
umístěna	umístěn	k2eAgFnSc1d1	umístěna
šikmo	šikmo	k6eAd1	šikmo
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
vázy	váza	k1gFnSc2	váza
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
soe	soe	k?	soe
<g/>
,	,	kIx,	,
kolmo	kolmo	k6eAd1	kolmo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
aranžmá	aranžmá	k1gNnSc1	aranžmá
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
širší	široký	k2eAgFnSc1d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Moribana	Moribana	k1gFnSc1	Moribana
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
moribana	moriban	k1gMnSc2	moriban
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
uspořádání	uspořádání	k1gNnSc2	uspořádání
školy	škola	k1gFnSc2	škola
Ohara	Ohar	k1gInSc2	Ohar
<g/>
.	.	kIx.	.
</s>
<s>
Moribana	Moribana	k1gFnSc1	Moribana
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
způsob	způsob	k1gInSc4	způsob
aranžmá	aranžmá	k1gNnSc2	aranžmá
ikebany	ikebana	k1gFnSc2	ikebana
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
i	i	k9	i
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
aranžmá	aranžmá	k1gNnSc6	aranžmá
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
způsobů	způsob	k1gInPc2	způsob
úprav	úprava	k1gFnPc2	úprava
aranžmá	aranžmá	k1gNnPc2	aranžmá
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
moribana	moriban	k1gMnSc2	moriban
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
škola	škola	k1gFnSc1	škola
používá	používat	k5eAaImIp3nS	používat
osobité	osobitý	k2eAgInPc4d1	osobitý
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
(	(	kIx(	(
<g/>
花	花	k?	花
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
skupinou	skupina	k1gFnSc7	skupina
aranžérů	aranžér	k1gMnPc2	aranžér
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
názory	názor	k1gInPc7	názor
<g/>
)	)	kIx)	)
Ohara	Ohara	k1gFnSc1	Ohara
(	(	kIx(	(
<g/>
小	小	k?	小
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
koncepce	koncepce	k1gFnSc2	koncepce
je	být	k5eAaImIp3nS	být
Nacuki	Nacuki	k1gNnSc1	Nacuki
Ohara	Ohar	k1gInSc2	Ohar
(	(	kIx(	(
<g/>
小	小	k?	小
夏	夏	k?	夏
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
respektuje	respektovat	k5eAaImIp3nS	respektovat
základní	základní	k2eAgNnSc1d1	základní
pravidla	pravidlo	k1gNnPc1	pravidlo
ikebany	ikebana	k1gFnSc2	ikebana
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
s	s	k7c7	s
elegancí	elegance	k1gFnSc7	elegance
vyniká	vynikat	k5eAaImIp3nS	vynikat
expresívností	expresívnost	k1gFnSc7	expresívnost
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
povahou	povaha	k1gFnSc7	povaha
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
aranžmá	aranžmá	k1gNnSc4	aranžmá
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ikebana	ikebana	k1gFnSc1	ikebana
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
projevy	projev	k1gInPc4	projev
individuality	individualita	k1gFnSc2	individualita
autora	autor	k1gMnSc2	autor
pomocí	pomocí	k7c2	pomocí
charakteru	charakter	k1gInSc2	charakter
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
formy	forma	k1gFnPc1	forma
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
pohled	pohled	k1gInSc4	pohled
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
upraveny	upravit	k5eAaPmNgInP	upravit
pro	pro	k7c4	pro
pohledy	pohled	k1gInPc4	pohled
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
dělena	dělit	k5eAaImNgFnS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
(	(	kIx(	(
<g/>
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
základní	základní	k2eAgFnSc1d1	základní
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozlišen	rozlišen	k2eAgInSc4d1	rozlišen
styl	styl	k1gInSc4	styl
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgInSc4d1	vertikální
<g/>
)	)	kIx)	)
a	a	k8xC	a
skloněný	skloněný	k2eAgMnSc1d1	skloněný
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
Hana	Hana	k1gFnSc1	Hana
išó	išó	k?	išó
je	být	k5eAaImIp3nS	být
dělena	dělit	k5eAaImNgFnS	dělit
na	na	k7c4	na
lineární	lineární	k2eAgNnSc4d1	lineární
<g/>
,	,	kIx,	,
paprskovitá	paprskovitý	k2eAgNnPc1d1	paprskovité
a	a	k8xC	a
kruhovitá	kruhovitý	k2eAgNnPc1d1	kruhovité
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Džijúka	Džijúek	k1gInSc2	Džijúek
<g/>
.	.	kIx.	.
</s>
<s>
Džijúka	Džijúka	k1gFnSc1	Džijúka
(	(	kIx(	(
<g/>
自	自	k?	自
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
čtení	čtení	k1gNnSc1	čtení
též	též	k9	též
džijúbana	džijúbana	k1gFnSc1	džijúbana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
zcela	zcela	k6eAd1	zcela
volných	volný	k2eAgFnPc2d1	volná
úprav	úprava	k1gFnPc2	úprava
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použít	použít	k5eAaPmF	použít
libovolný	libovolný	k2eAgInSc4d1	libovolný
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
upravený	upravený	k2eAgInSc1d1	upravený
<g/>
.	.	kIx.	.
</s>
<s>
Neživé	živý	k2eNgNnSc1d1	neživé
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
nekvetoucí	kvetoucí	k2eNgInSc1d1	nekvetoucí
<g/>
,	,	kIx,	,
nerostlinný	rostlinný	k2eNgInSc1d1	rostlinný
materiál	materiál	k1gInSc1	materiál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
druhy	druh	k1gInPc4	druh
upevnění	upevnění	k1gNnPc2	upevnění
jsou	být	k5eAaImIp3nP	být
povolena	povolen	k2eAgNnPc1d1	povoleno
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
styl	styl	k1gInSc4	styl
džijúka	džijúka	k6eAd1	džijúka
má	mít	k5eAaImIp3nS	mít
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gFnPc4	on
třeba	třeba	k6eAd1	třeba
zohlednit	zohlednit	k5eAaPmF	zohlednit
v	v	k7c6	v
aranžmá	aranžmá	k1gNnSc6	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
formální	formální	k2eAgNnPc4d1	formální
pravidla	pravidlo	k1gNnPc4	pravidlo
patří	patřit	k5eAaImIp3nS	patřit
jen	jen	k9	jen
uspořádání	uspořádání	k1gNnSc1	uspořádání
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
rikka	rikko	k1gNnSc2	rikko
šimpútai	šimpúta	k1gFnSc2	šimpúta
(	(	kIx(	(
<g/>
立	立	k?	立
<g/>
)	)	kIx)	)
předvedl	předvést	k5eAaPmAgMnS	předvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
Ikebonó	Ikebonó	k1gFnSc1	Ikebonó
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
<g/>
'	'	kIx"	'
<g/>
ei	ei	k?	ei
Ikenobó	Ikenobó	k1gFnSc1	Ikenobó
(	(	kIx(	(
<g/>
池	池	k?	池
専	専	k?	専
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
moderní	moderní	k2eAgFnSc4d1	moderní
variantu	varianta	k1gFnSc4	varianta
stylu	styl	k1gInSc2	styl
rikka	rikek	k1gInSc2	rikek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
různé	různý	k2eAgInPc4d1	různý
poměry	poměr	k1gInPc4	poměr
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnPc4	kombinace
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
aranžmá	aranžmá	k1gNnSc1	aranžmá
uspořádáno	uspořádán	k2eAgNnSc1d1	uspořádáno
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
tradičního	tradiční	k2eAgInSc2d1	tradiční
stylu	styl	k1gInSc2	styl
rikka	rikko	k1gNnSc2	rikko
<g/>
,	,	kIx,	,
jer	jer	k1gInSc4	jer
volnější	volný	k2eAgInSc4d2	volnější
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
parafrází	parafráze	k1gFnSc7	parafráze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
kenzan	kenzan	k1gInSc4	kenzan
jako	jako	k9	jako
u	u	k7c2	u
rikka	rikko	k1gNnSc2	rikko
<g/>
,	,	kIx,	,
materiál	materiál	k1gInSc1	materiál
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc1	směr
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
vedení	vedení	k1gNnSc2	vedení
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
či	či	k8xC	či
barevné	barevný	k2eAgFnPc1d1	barevná
kombinace	kombinace	k1gFnPc1	kombinace
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
specifikovány	specifikován	k2eAgInPc4d1	specifikován
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Rikka	Rikka	k1gFnSc1	Rikka
šimpútai	šimpúta	k1gFnSc2	šimpúta
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vždy	vždy	k6eAd1	vždy
působit	působit	k5eAaImF	působit
dojmem	dojem	k1gInSc7	dojem
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
styly	styl	k1gInPc4	styl
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
zen	zen	k2eAgFnSc1d1	zen
<g/>
'	'	kIx"	'
<g/>
eika	eika	k1gFnSc1	eika
(	(	kIx(	(
<g/>
前	前	k?	前
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
avantgardní	avantgardní	k2eAgFnSc1d1	avantgardní
forma	forma	k1gFnSc1	forma
ikebany	ikebana	k1gFnSc2	ikebana
<g/>
.	.	kIx.	.
kenzan	kenzan	k1gInSc1	kenzan
-	-	kIx~	-
kovový	kovový	k2eAgMnSc1d1	kovový
ježek	ježek	k1gMnSc1	ježek
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vpichují	vpichovat	k5eAaImIp3nP	vpichovat
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
postříbřený	postříbřený	k2eAgMnSc1d1	postříbřený
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
úchytky	úchytka	k1gFnPc1	úchytka
a	a	k8xC	a
kroužky	kroužek	k1gInPc1	kroužek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
držet	držet	k5eAaImF	držet
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
<g />
.	.	kIx.	.
</s>
<s>
materiál	materiál	k1gInSc1	materiál
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
zhotovené	zhotovený	k2eAgNnSc1d1	zhotovené
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
bronzu	bronz	k1gInSc2	bronz
nebo	nebo	k8xC	nebo
železa	železo	k1gNnSc2	železo
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
kolíky	kolík	k1gInPc1	kolík
nebo	nebo	k8xC	nebo
slaměná	slaměný	k2eAgFnSc1d1	slaměná
podložka	podložka	k1gFnSc1	podložka
nebo	nebo	k8xC	nebo
florex	florex	k1gInSc1	florex
(	(	kIx(	(
<g/>
oasis	oasis	k1gFnSc1	oasis
<g/>
)	)	kIx)	)
nádoba	nádoba	k1gFnSc1	nádoba
-	-	kIx~	-
podle	podle	k7c2	podle
záměru	záměr	k1gInSc2	záměr
japonského	japonský	k2eAgInSc2d1	japonský
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
plochá	plochý	k2eAgFnSc1d1	plochá
miska	miska	k1gFnSc1	miska
(	(	kIx(	(
<g/>
moribana	moribana	k1gFnSc1	moribana
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
váza	váza	k1gFnSc1	váza
(	(	kIx(	(
<g/>
nageire	nageir	k1gInSc5	nageir
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
materiál	materiál	k1gInSc1	materiál
lýko	lýko	k1gNnSc1	lýko
nebo	nebo	k8xC	nebo
plastové	plastový	k2eAgInPc1d1	plastový
pásky	pásek	k1gInPc1	pásek
aranžovací	aranžovací	k2eAgInPc1d1	aranžovací
drát	drát	k5eAaImF	drát
nůž	nůž	k1gInSc4	nůž
<g/>
,	,	kIx,	,
nůžky	nůžky	k1gFnPc4	nůžky
Ikebana	ikebana	k1gFnSc1	ikebana
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
aranžování	aranžování	k1gNnSc2	aranžování
které	který	k3yRgMnPc4	který
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
principech	princip	k1gInPc6	princip
používá	používat	k5eAaImIp3nS	používat
a	a	k8xC	a
propaguje	propagovat	k5eAaImIp3nS	propagovat
tradiční	tradiční	k2eAgInSc4d1	tradiční
šovinistický	šovinistický	k2eAgInSc4d1	šovinistický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
možný	možný	k2eAgInSc1d1	možný
a	a	k8xC	a
správný	správný	k2eAgInSc1d1	správný
způsob	způsob	k1gInSc1	způsob
nadřazenost	nadřazenost	k1gFnSc1	nadřazenost
mužů	muž	k1gMnPc2	muž
nad	nad	k7c7	nad
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
symbol	symbol	k1gInSc4	symbol
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zobrazovány	zobrazován	k2eAgFnPc1d1	zobrazována
jako	jako	k8xC	jako
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
slabší	slabý	k2eAgFnSc1d2	slabší
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
přirozeně	přirozeně	k6eAd1	přirozeně
níže	nízce	k6eAd2	nízce
postavené	postavený	k2eAgFnPc1d1	postavená
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
prvek	prvek	k1gInSc1	prvek
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
částečně	částečně	k6eAd1	částečně
skryt	skryt	k2eAgInSc1d1	skryt
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
přístup	přístup	k1gInSc1	přístup
odráží	odrážet	k5eAaImIp3nS	odrážet
hluboce	hluboko	k6eAd1	hluboko
tradice	tradice	k1gFnPc4	tradice
japonské	japonský	k2eAgFnSc2d1	japonská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
faktu	fakt	k1gInSc6	fakt
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
aranžování	aranžování	k1gNnPc1	aranžování
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
především	především	k9	především
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
zásady	zásada	k1gFnPc1	zásada
akceptují	akceptovat	k5eAaBmIp3nP	akceptovat
<g/>
,	,	kIx,	,
chápu	chápat	k5eAaImIp1nS	chápat
jako	jako	k9	jako
přirozeně	přirozeně	k6eAd1	přirozeně
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
vzory	vzor	k1gInPc1	vzor
aranžování	aranžování	k1gNnSc2	aranžování
<g/>
,	,	kIx,	,
určují	určovat	k5eAaImIp3nP	určovat
skupiny	skupina	k1gFnPc1	skupina
vedené	vedený	k2eAgFnPc1d1	vedená
i	i	k9	i
v	v	k7c4	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tradičně	tradičně	k6eAd1	tradičně
převážně	převážně	k6eAd1	převážně
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ikebaně	ikebana	k1gFnSc6	ikebana
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
řada	řada	k1gFnSc1	řada
zásad	zásada	k1gFnPc2	zásada
vyjadřující	vyjadřující	k2eAgFnSc2d1	vyjadřující
pověry	pověra	k1gFnSc2	pověra
ohledně	ohledně	k7c2	ohledně
numerologie	numerologie	k1gFnSc2	numerologie
nebo	nebo	k8xC	nebo
barvy	barva	k1gFnSc2	barva
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
takové	takový	k3xDgFnSc2	takový
zásady	zásada	k1gFnSc2	zásada
ovšem	ovšem	k9	ovšem
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
způsobem	způsob	k1gInSc7	způsob
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
vytvořit	vytvořit	k5eAaPmF	vytvořit
funkční	funkční	k2eAgInPc1d1	funkční
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
esteticky	esteticky	k6eAd1	esteticky
vkusnou	vkusný	k2eAgFnSc4d1	vkusná
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
