<p>
<s>
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Slovenska	Slovensko	k1gNnSc2	Slovensko
demokratska	demokratsk	k1gInSc2	demokratsk
stranka	stranka	k1gFnSc1	stranka
<g/>
;	;	kIx,	;
SDS	SDS	kA	SDS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovinská	slovinský	k2eAgFnSc1d1	slovinská
pravicová	pravicový	k2eAgFnSc1d1	pravicová
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
nesla	nést	k5eAaImAgFnS	nést
označení	označení	k1gNnSc4	označení
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
(	(	kIx(	(
<g/>
Socialdemokratska	Socialdemokratska	k1gFnSc1	Socialdemokratska
stranka	stranka	k1gFnSc1	stranka
Slovenije	Slovenije	k1gFnSc1	Slovenije
<g/>
;	;	kIx,	;
SDSS	SDSS	kA	SDSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
===	===	k?	===
</s>
</p>
<p>
<s>
SDS	SDS	kA	SDS
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
odlišných	odlišný	k2eAgInPc2d1	odlišný
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
–	–	k?	–
Sociálně	sociálně	k6eAd1	sociálně
demokratického	demokratický	k2eAgInSc2d1	demokratický
svazu	svaz	k1gInSc2	svaz
Slovinsku	Slovinsko	k1gNnSc3	Slovinsko
(	(	kIx(	(
<g/>
Socialdemokratska	Socialdemokratska	k1gFnSc1	Socialdemokratska
zveza	zveza	k1gFnSc1	zveza
Slovenije	Slovenije	k1gFnSc1	Slovenije
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovinského	slovinský	k2eAgInSc2d1	slovinský
demokratického	demokratický	k2eAgInSc2d1	demokratický
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
členy	člen	k1gInPc4	člen
koalice	koalice	k1gFnSc2	koalice
DEMOS	DEMOS	kA	DEMOS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovinský	slovinský	k2eAgInSc1d1	slovinský
demokratický	demokratický	k2eAgInSc1d1	demokratický
svaz	svaz	k1gInSc1	svaz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1989	[number]	k4	1989
a	a	k8xC	a
Sociálně	sociálně	k6eAd1	sociálně
demokratický	demokratický	k2eAgInSc1d1	demokratický
svaz	svaz	k1gInSc1	svaz
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
svazy	svaz	k1gInPc1	svaz
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jako	jako	k9	jako
opoziční	opoziční	k2eAgNnSc4d1	opoziční
hnutí	hnutí	k1gNnSc4	hnutí
k	k	k7c3	k
vládnoucímu	vládnoucí	k2eAgInSc3d1	vládnoucí
Svazu	svaz	k1gInSc3	svaz
komunistů	komunista	k1gMnPc2	komunista
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
politický	politický	k2eAgInSc1d1	politický
monopol	monopol	k1gInSc1	monopol
držel	držet	k5eAaImAgInS	držet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
svazy	svaz	k1gInPc1	svaz
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
podporovaly	podporovat	k5eAaImAgInP	podporovat
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
a	a	k8xC	a
politickému	politický	k2eAgInSc3d1	politický
pluralismu	pluralismus	k1gInSc3	pluralismus
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc3	vytvoření
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
a	a	k8xC	a
dodržování	dodržování	k1gNnSc2	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
politických	politický	k2eAgFnPc2d1	politická
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
práv	právo	k1gNnPc2	právo
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
integraci	integrace	k1gFnSc4	integrace
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
do	do	k7c2	do
euroatlantických	euroatlantický	k2eAgFnPc2d1	euroatlantická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Sociálně	sociálně	k6eAd1	sociálně
demokratického	demokratický	k2eAgInSc2d1	demokratický
svazu	svaz	k1gInSc2	svaz
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
odborový	odborový	k2eAgInSc1d1	odborový
předák	předák	k1gInSc1	předák
France	Franc	k1gMnSc2	Franc
Tomšič	Tomšič	k1gInSc4	Tomšič
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1987	[number]	k4	1987
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
polské	polský	k2eAgFnSc2d1	polská
Solidarity	solidarita	k1gFnSc2	solidarita
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
stávku	stávka	k1gFnSc4	stávka
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Tomšič	Tomšič	k1gMnSc1	Tomšič
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Jože	Joža	k1gFnSc3	Joža
Pučnik	Pučnik	k1gInSc1	Pučnik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pučnik	Pučnik	k1gInSc1	Pučnik
ze	z	k7c2	z
Svazu	svaz	k1gInSc2	svaz
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
středovou	středový	k2eAgFnSc4d1	středová
nemarxistickou	marxistický	k2eNgFnSc4d1	nemarxistická
sociálnědemokratickou	sociálnědemokratický	k2eAgFnSc4d1	sociálnědemokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
tržního	tržní	k2eAgNnSc2d1	tržní
hospodářství	hospodářství	k1gNnSc2	hospodářství
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
sociálního	sociální	k2eAgInSc2d1	sociální
státu	stát	k1gInSc2	stát
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
či	či	k8xC	či
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovinský	slovinský	k2eAgInSc1d1	slovinský
demokratický	demokratický	k2eAgInSc1d1	demokratický
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
SDZ	SDZ	kA	SDZ
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
širokou	široký	k2eAgFnSc7d1	široká
roztříštěnou	roztříštěný	k2eAgFnSc7d1	roztříštěná
koalicí	koalice	k1gFnSc7	koalice
různých	různý	k2eAgFnPc2d1	různá
liberálních	liberální	k2eAgFnPc2d1	liberální
<g/>
,	,	kIx,	,
sociálně-liberálních	sociálněiberální	k2eAgFnPc2d1	sociálně-liberální
a	a	k8xC	a
občansko-nacionalistických	občanskoacionalistický	k2eAgFnPc2d1	občansko-nacionalistický
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
SDZ	SDZ	kA	SDZ
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Sociálně-liberální	Sociálněiberální	k2eAgNnSc1d1	Sociálně-liberální
křídlo	křídlo	k1gNnSc1	křídlo
založilo	založit	k5eAaPmAgNnS	založit
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
konzervativci	konzervativec	k1gMnPc1	konzervativec
pak	pak	k6eAd1	pak
Národní	národní	k2eAgFnSc4d1	národní
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
nesdílející	sdílející	k2eNgInPc1d1	sdílející
názory	názor	k1gInPc1	názor
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgFnPc2	dva
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Pučnikovy	Pučnikův	k2eAgFnSc2d1	Pučnikův
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
SDSS	SDSS	kA	SDSS
poslední	poslední	k2eAgFnSc7d1	poslední
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Státního	státní	k2eAgNnSc2d1	státní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
poté	poté	k6eAd1	poté
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Drnovšekova	Drnovšekův	k2eAgInSc2d1	Drnovšekův
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Janšova	Janšův	k2eAgFnSc1d1	Janšův
SDSS	SDSS	kA	SDSS
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1993	[number]	k4	1993
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
předseda	předseda	k1gMnSc1	předseda
SDSS	SDSS	kA	SDSS
Pučnik	Pučnik	k1gMnSc1	Pučnik
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
Pučnikovou	Pučnikův	k2eAgFnSc7d1	Pučnikův
podporou	podpora	k1gFnSc7	podpora
zvolen	zvolit	k5eAaPmNgMnS	zvolit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Slovinského	slovinský	k2eAgInSc2d1	slovinský
demokratického	demokratický	k2eAgInSc2d1	demokratický
svazu	svaz	k1gInSc2	svaz
Janez	Janez	k1gMnSc1	Janez
Janša	Janša	k1gMnSc1	Janša
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Desetidenní	desetidenní	k2eAgFnSc2d1	desetidenní
války	válka	k1gFnSc2	válka
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Pučnik	Pučnik	k1gMnSc1	Pučnik
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
SDSS	SDSS	kA	SDSS
<g/>
.	.	kIx.	.
</s>
<s>
Janša	Janša	k1gMnSc1	Janša
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
Drnovšekovy	Drnovšekův	k2eAgFnSc2d1	Drnovšekův
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
nucen	nucen	k2eAgInSc1d1	nucen
odstoupit	odstoupit	k5eAaPmF	odstoupit
kvůli	kvůli	k7c3	kvůli
zneužití	zneužití	k1gNnSc3	zneužití
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Sesazení	sesazení	k1gNnSc1	sesazení
Janši	Janše	k1gFnSc4	Janše
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
vládní	vládní	k2eAgFnSc4d1	vládní
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vygradovala	vygradovat	k5eAaPmAgFnS	vygradovat
vystoupením	vystoupení	k1gNnSc7	vystoupení
SDSS	SDSS	kA	SDSS
z	z	k7c2	z
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
a	a	k8xC	a
nastavení	nastavení	k1gNnSc4	nastavení
jejího	její	k3xOp3gInSc2	její
ostrého	ostrý	k2eAgInSc2d1	ostrý
kurzu	kurz	k1gInSc2	kurz
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
Liberální	liberální	k2eAgFnSc3d1	liberální
demokracii	demokracie	k1gFnSc3	demokracie
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
SDSS	SDSS	kA	SDSS
(	(	kIx(	(
<g/>
SDS	SDS	kA	SDS
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
příštích	příští	k2eAgNnPc2d1	příští
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátkého	krátký	k2eAgNnSc2d1	krátké
období	období	k1gNnSc2	období
Bajukovy	Bajukův	k2eAgFnSc2d1	Bajukův
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
SDSS	SDSS	kA	SDSS
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
Národní	národní	k2eAgFnSc7d1	národní
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
rozkolu	rozkol	k1gInSc6	rozkol
Slovinského	slovinský	k2eAgInSc2d1	slovinský
demokratického	demokratický	k2eAgInSc2d1	demokratický
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
SDSS	SDSS	kA	SDSS
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lidové	lidový	k2eAgFnSc6d1	lidová
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
změnila	změnit	k5eAaPmAgFnS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
na	na	k7c4	na
Slovinskou	slovinský	k2eAgFnSc4d1	slovinská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
programu	program	k1gInSc6	program
přijatém	přijatý	k2eAgInSc6d1	přijatý
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
definovala	definovat	k5eAaBmAgFnS	definovat
jako	jako	k9	jako
středová	středový	k2eAgFnSc1d1	středová
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
politice	politika	k1gFnSc6	politika
přijala	přijmout	k5eAaPmAgFnS	přijmout
zcela	zcela	k6eAd1	zcela
liberální	liberální	k2eAgInSc4d1	liberální
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
středovou	středový	k2eAgFnSc4d1	středová
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zachovala	zachovat	k5eAaPmAgFnS	zachovat
i	i	k9	i
v	v	k7c6	v
sociálních	sociální	k2eAgFnPc6d1	sociální
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
SDS	SDS	kA	SDS
znovu	znovu	k6eAd1	znovu
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
podporu	podpora	k1gFnSc4	podpora
integrace	integrace	k1gFnSc2	integrace
do	do	k7c2	do
euroatlantických	euroatlantický	k2eAgFnPc2d1	euroatlantická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
SDS	SDS	kA	SDS
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
křesťansko-demokratickým	křesťanskoemokratický	k2eAgNnSc7d1	křesťansko-demokratické
Novým	nový	k2eAgNnSc7d1	nové
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
<g/>
,	,	kIx,	,
Slovinskou	slovinský	k2eAgFnSc7d1	slovinská
lidovou	lidový	k2eAgFnSc7d1	lidová
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
důchodců	důchodce	k1gMnPc2	důchodce
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
(	(	kIx(	(
<g/>
DeSUS	DeSUS	k1gFnSc1	DeSUS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kabinet	kabinet	k1gInSc1	kabinet
SDS	SDS	kA	SDS
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
realizovat	realizovat	k5eAaBmF	realizovat
fiskální	fiskální	k2eAgFnSc2d1	fiskální
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
realizována	realizován	k2eAgFnSc1d1	realizována
reforma	reforma	k1gFnSc1	reforma
samosprávy	samospráva	k1gFnSc2	samospráva
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zamýšleno	zamýšlen	k2eAgNnSc1d1	zamýšleno
vytvoření	vytvoření	k1gNnSc1	vytvoření
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
omezit	omezit	k5eAaPmF	omezit
postavení	postavení	k1gNnSc4	postavení
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
opozice	opozice	k1gFnSc1	opozice
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
jako	jako	k9	jako
snahu	snaha	k1gFnSc4	snaha
pošpinit	pošpinit	k5eAaPmF	pošpinit
předchozí	předchozí	k2eAgFnPc4d1	předchozí
vlády	vláda	k1gFnPc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
důchodovém	důchodový	k2eAgInSc6d1	důchodový
systému	systém	k1gInSc6	systém
realizované	realizovaný	k2eAgNnSc1d1	realizované
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
koaliční	koaliční	k2eAgFnSc4d1	koaliční
DeSUS	DeSUS	k1gFnSc4	DeSUS
byly	být	k5eAaImAgFnP	být
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
SDS	SDS	kA	SDS
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
kabinet	kabinet	k1gInSc1	kabinet
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Janša	Janša	k1gMnSc1	Janša
byli	být	k5eAaImAgMnP	být
obviňováni	obviňovat	k5eAaImNgMnP	obviňovat
z	z	k7c2	z
pokusů	pokus	k1gInPc2	pokus
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
nezávislosti	nezávislost	k1gFnSc2	nezávislost
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
sice	sice	k8xC	sice
SDS	SDS	kA	SDS
získala	získat	k5eAaPmAgFnS	získat
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
než	než	k8xS	než
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
ji	on	k3xPp3gFnSc4	on
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
SDS	SDS	kA	SDS
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
stínovou	stínový	k2eAgFnSc4d1	stínová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
zástupce	zástupce	k1gMnSc1	zástupce
jiných	jiný	k2eAgFnPc2d1	jiná
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
strana	strana	k1gFnSc1	strana
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
26,9	[number]	k4	26,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
SDS	SDS	kA	SDS
vede	vést	k5eAaImIp3nS	vést
ve	v	k7c6	v
volebních	volební	k2eAgInPc6d1	volební
průzkumech	průzkum	k1gInPc6	průzkum
a	a	k8xC	a
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
od	od	k7c2	od
vládní	vládní	k2eAgFnSc2d1	vládní
Sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
konstantní	konstantní	k2eAgInSc4d1	konstantní
náskok	náskok	k1gInSc4	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
i	i	k9	i
volební	volební	k2eAgInPc1d1	volební
průzkumy	průzkum	k1gInPc1	průzkum
před	před	k7c7	před
předčasnými	předčasný	k2eAgFnPc7d1	předčasná
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
vítězství	vítězství	k1gNnSc4	vítězství
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
středolevá	středolevý	k2eAgNnPc1d1	středolevé
Pozitivna	pozitivno	k1gNnPc4	pozitivno
Slovenija	Slovenijus	k1gMnSc2	Slovenijus
lublaňského	lublaňský	k2eAgMnSc2d1	lublaňský
župana	župan	k1gMnSc2	župan
Zorana	Zoran	k1gMnSc2	Zoran
Jankoviće	Janković	k1gMnSc2	Janković
<g/>
.	.	kIx.	.
<g/>
SDS	SDS	kA	SDS
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
dvaceti	dvacet	k4xCc7	dvacet
sedmi	sedm	k4xCc7	sedm
tisíci	tisíc	k4xCgInPc7	tisíc
členy	člen	k1gMnPc7	člen
největší	veliký	k2eAgFnSc1d3	veliký
slovinskou	slovinský	k2eAgFnSc7d1	slovinská
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předsedové	předseda	k1gMnPc1	předseda
===	===	k?	===
</s>
</p>
<p>
<s>
France	Franc	k1gMnSc4	Franc
Tomšič	Tomšič	k1gMnSc1	Tomšič
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jože	Joža	k1gFnSc3	Joža
Pučnik	Pučnik	k1gMnSc1	Pučnik
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Janez	Janez	k1gMnSc1	Janez
Janša	Janša	k1gMnSc1	Janša
(	(	kIx(	(
<g/>
od	od	k7c2	od
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zástupci	zástupce	k1gMnPc1	zástupce
ve	v	k7c6	v
Skupščině	Skupščin	k2eAgNnSc6d1	Skupščin
a	a	k8xC	a
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
shromáždění	shromáždění	k1gNnSc6	shromáždění
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Slovenian	Sloveniany	k1gInPc2	Sloveniany
Democratic	Democratice	k1gFnPc2	Democratice
Party	party	k1gFnSc7	party
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Janeze	Janeze	k1gFnSc1	Janeze
Drnovšeka	Drnovšeka	k1gFnSc1	Drnovšeka
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Janeze	Janeze	k1gFnSc2	Janeze
Janši	Janše	k1gFnSc4	Janše
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
<g/>
)	)	kIx)	)
SDS	SDS	kA	SDS
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
C2010	C2010	k4	C2010
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
<g/>
)	)	kIx)	)
</s>
</p>
