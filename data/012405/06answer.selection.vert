<s>
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Slovenska	Slovensko	k1gNnSc2	Slovensko
demokratska	demokratsk	k1gInSc2	demokratsk
stranka	stranka	k1gFnSc1	stranka
<g/>
;	;	kIx,	;
SDS	SDS	kA	SDS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovinská	slovinský	k2eAgFnSc1d1	slovinská
pravicová	pravicový	k2eAgFnSc1d1	pravicová
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
