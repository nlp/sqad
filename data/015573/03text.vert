<s>
Zoo	zoo	k1gFnSc1
Tycoon	Tycoona	k1gFnPc2
</s>
<s>
Zoo	zoo	k1gFnSc1
TycoonVývojářBlue	TycoonVývojářBlu	k1gFnSc2
Fang	Fanga	k1gFnPc2
GamesVydavatelMicrosoft	GamesVydavatelMicrosoft	k1gMnSc1
StudiosDesignérBlue	StudiosDesignérBlu	k1gFnSc2
Fang	Fang	k1gMnSc1
GamesHerní	GamesHerní	k2eAgMnSc1d1
sérieZoo	sérieZoo	k6eAd1
TycoonPlatformyMicrosoft	TycoonPlatformyMicrosoft	k2eAgInSc4d1
WindowsMac	WindowsMac	k1gInSc4
OS	OS	kA
XDatum	XDatum	k1gNnSc1
vydání	vydání	k1gNnSc2
<g/>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2001	#num#	k4
<g/>
Žánrbudovatelská	Žánrbudovatelský	k2eAgFnSc1d1
strategieHerní	strategieHerní	k2eAgFnSc1d1
módsingle	módsingle	k1gFnSc1
playerKlasifikaceESRB	playerKlasifikaceESRB	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zoo	zoo	k1gFnSc1
Tycoon	Tycoon	k1gInSc1
je	být	k5eAaImIp3nS
budovatelská	budovatelský	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vyvinula	vyvinout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Blue	Blu	k1gFnSc2
Fang	Fang	k1gMnSc1
Games	Games	k1gMnSc1
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
divize	divize	k1gFnSc1
Microsoft	Microsoft	kA
Studios	Studios	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
ve	v	k7c6
hře	hra	k1gFnSc6
ujímá	ujímat	k5eAaImIp3nS
zoologické	zoologický	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
musí	muset	k5eAaImIp3nS
udržovat	udržovat	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vydělávala	vydělávat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
pro	pro	k7c4
Microsoft	Microsoft	kA
Windows	Windows	kA
a	a	k8xC
Mac	Mac	kA
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
převedena	převést	k5eAaPmNgFnS
také	také	k9
na	na	k7c4
Nintendo	Nintendo	k1gNnSc4
DS	DS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byly	být	k5eAaImAgInP
vydány	vydán	k2eAgInPc1d1
dva	dva	k4xCgInPc1
datadisky	datadisek	k1gInPc1
<g/>
,	,	kIx,
Zoo	zoo	k1gFnSc1
Tycoon	Tycoon	k1gMnSc1
<g/>
:	:	kIx,
Dinosaur	Dinosaur	k1gMnSc1
Digs	Digsa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
do	do	k7c2
hry	hra	k1gFnSc2
přidával	přidávat	k5eAaImAgInS
vyhynulá	vyhynulý	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
Zoo	zoo	k1gNnSc1
Tycoon	Tycoon	k1gNnSc1
<g/>
:	:	kIx,
Marine	Marin	k1gMnSc5
Mania	Manius	k1gMnSc4
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgMnPc4d1
mořské	mořský	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
série	série	k1gFnSc2
Zoo	zoo	k1gFnPc2
Tycoon	Tycoon	k1gInSc1
<g/>
,	,	kIx,
Zoo	zoo	k1gFnSc1
Tycoon	Tycoon	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Zoo	zoo	k1gFnSc2
Tycoon	Tycoona	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Official	Official	k1gInSc1
website	websit	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zoo	zoo	k1gFnSc1
Tycoon	Tycoona	k1gFnPc2
Wiki	Wik	k1gFnSc2
</s>
<s>
Tycoon	Tycoon	k1gMnSc1
GAMES	GAMES	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
