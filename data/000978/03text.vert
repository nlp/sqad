<s>
Sv.	sv.	kA	sv.
Zdislava	Zdislava	k1gFnSc1	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
(	(	kIx(	(
<g/>
1220	[number]	k4	1220
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Křižanov	Křižanov	k1gInSc1	Křižanov
-	-	kIx~	-
1252	[number]	k4	1252
Lemberk	Lemberk	k1gInSc1	Lemberk
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
šlechtična	šlechtična	k1gFnSc1	šlechtična
a	a	k8xC	a
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
špitálu	špitál	k1gInSc2	špitál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
papežem	papež	k1gMnSc7	papež
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c4	za
svatou	svatá	k1gFnSc4	svatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
požádal	požádat	k5eAaPmAgMnS	požádat
litoměřický	litoměřický	k2eAgMnSc1d1	litoměřický
biskup	biskup	k1gMnSc1	biskup
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
patrocinia	patrocinium	k1gNnSc2	patrocinium
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
sv.	sv.	kA	sv.
Zdislava	Zdislava	k1gFnSc1	Zdislava
hlavní	hlavní	k2eAgFnSc1d1	hlavní
patronkou	patronka	k1gFnSc7	patronka
Litoměřické	litoměřický	k2eAgFnSc2d1	Litoměřická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
patronkou	patronka	k1gFnSc7	patronka
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
z	z	k7c2	z
Křižanova	Křižanův	k2eAgInSc2d1	Křižanův
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Sibyla	Sibyla	k1gFnSc1	Sibyla
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
s	s	k7c7	s
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
Štaufskou	Štaufský	k2eAgFnSc7d1	Štaufská
<g/>
,	,	kIx,	,
nevěstou	nevěsta	k1gFnSc7	nevěsta
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Zdislava	Zdislava	k1gFnSc1	Zdislava
měla	mít	k5eAaImAgFnS	mít
několik	několik	k4yIc4	několik
sourozenců	sourozenec	k1gMnPc2	sourozenec
-	-	kIx~	-
dospělého	dospělý	k1gMnSc2	dospělý
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
dožily	dožít	k5eAaPmAgFnP	dožít
sestry	sestra	k1gFnPc1	sestra
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Eliška	Eliška	k1gFnSc1	Eliška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
Smila	Smil	k1gMnSc2	Smil
z	z	k7c2	z
Lichtemburka	Lichtemburek	k1gMnSc2	Lichtemburek
<g/>
,	,	kIx,	,
a	a	k8xC	a
Eufémie	eufémie	k1gFnSc1	eufémie
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Bočka	Boček	k1gMnSc2	Boček
z	z	k7c2	z
Obřan	Obřana	k1gFnPc2	Obřana
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
mládí	mládí	k1gNnSc6	mládí
nevíme	vědět	k5eNaImIp1nP	vědět
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
velmi	velmi	k6eAd1	velmi
vřelý	vřelý	k2eAgInSc4d1	vřelý
vztah	vztah	k1gInSc4	vztah
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
k	k	k7c3	k
duchovnu	duchovno	k1gNnSc3	duchovno
a	a	k8xC	a
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
možná	možná	k9	možná
její	její	k3xOp3gFnSc1	její
výchova	výchova	k1gFnSc1	výchova
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
moravských	moravský	k2eAgInPc2d1	moravský
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Zdislavinu	Zdislavin	k2eAgFnSc4d1	Zdislavina
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Havel	Havel	k1gMnSc1	Havel
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yIgMnSc4	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1238	[number]	k4	1238
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
blízký	blízký	k2eAgMnSc1d1	blízký
důvěrník	důvěrník	k1gMnSc1	důvěrník
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
snad	snad	k9	snad
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doložení	doložený	k2eAgMnPc1d1	doložený
synové	syn	k1gMnPc1	syn
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
Zdislav	Zdislava	k1gFnPc2	Zdislava
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
dcera	dcera	k1gFnSc1	dcera
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
pouze	pouze	k6eAd1	pouze
Žďárská	žďárský	k2eAgFnSc1d1	žďárská
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Zdislava	Zdislava	k1gFnSc1	Zdislava
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ráznou	rázný	k2eAgFnSc7d1	rázná
a	a	k8xC	a
energickou	energický	k2eAgFnSc7d1	energická
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
dominikánskými	dominikánský	k2eAgInPc7d1	dominikánský
kláštery	klášter	k1gInPc7	klášter
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
a	a	k8xC	a
v	v	k7c6	v
Jablonném	Jablonné	k1gNnSc6	Jablonné
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
špitálem	špitál	k1gInSc7	špitál
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgNnSc3	svůj
vysokému	vysoký	k2eAgNnSc3d1	vysoké
postavení	postavení	k1gNnSc3	postavení
<g/>
,	,	kIx,	,
osobně	osobně	k6eAd1	osobně
docházela	docházet	k5eAaImAgFnS	docházet
a	a	k8xC	a
nemocných	nemocný	k1gMnPc2	nemocný
se	se	k3xPyFc4	se
ujímala	ujímat	k5eAaImAgFnS	ujímat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
předčasně	předčasně	k6eAd1	předčasně
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
tradovanou	tradovaný	k2eAgFnSc4d1	tradovaná
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
antropologický	antropologický	k2eAgInSc4d1	antropologický
výzkum	výzkum	k1gInSc4	výzkum
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
v	v	k7c6	v
Jablonném	Jablonné	k1gNnSc6	Jablonné
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
stavbu	stavba	k1gFnSc4	stavba
začala	začít	k5eAaPmAgFnS	začít
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedožila	dožít	k5eNaPmAgFnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
V	v	k7c6	v
erbu	erb	k1gInSc6	erb
lvice	lvice	k1gFnSc2	lvice
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Aleny	Alena	k1gFnSc2	Alena
Vrbové	Vrbová	k1gFnSc2	Vrbová
<g/>
;	;	kIx,	;
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
také	také	k9	také
kniha	kniha	k1gFnSc1	kniha
Světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
tmách	tma	k1gFnPc6	tma
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Durycha	Durych	k1gMnSc2	Durych
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
její	její	k3xOp3gFnSc6	její
beatifikaci	beatifikace	k1gFnSc6	beatifikace
požádal	požádat	k5eAaPmAgMnS	požádat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
litoměřický	litoměřický	k2eAgInSc4d1	litoměřický
biskup	biskup	k1gInSc4	biskup
Augustin	Augustin	k1gMnSc1	Augustin
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
Hille	Hille	k1gFnSc1	Hille
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
proces	proces	k1gInSc1	proces
blahořečení	blahořečení	k1gNnSc2	blahořečení
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g/>
.	.	kIx.	.
</s>
<s>
Impulzem	impulz	k1gInSc7	impulz
pro	pro	k7c4	pro
beatifikaci	beatifikace	k1gFnSc4	beatifikace
byla	být	k5eAaImAgFnS	být
aktivita	aktivita	k1gFnSc1	aktivita
faráře	farář	k1gMnSc2	farář
z	z	k7c2	z
Jablonného	Jablonné	k1gNnSc2	Jablonné
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
Josefa	Josef	k1gMnSc2	Josef
Tschörche	Tschörch	k1gMnSc2	Tschörch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
nepamětného	pamětný	k2eNgInSc2d1	pamětný
kultu	kult	k1gInSc2	kult
paní	paní	k1gFnSc2	paní
Zdislavy	Zdislava	k1gFnSc2	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
litoměřického	litoměřický	k2eAgMnSc2d1	litoměřický
biskupa	biskup	k1gMnSc2	biskup
Emanuela	Emanuel	k1gMnSc2	Emanuel
Jana	Jan	k1gMnSc2	Jan
Schöbela	Schöbel	k1gMnSc2	Schöbel
<g/>
,	,	kIx,	,
litoměřického	litoměřický	k2eAgMnSc2d1	litoměřický
kanovníka	kanovník	k1gMnSc2	kanovník
Josefa	Josef	k1gMnSc2	Josef
Štěrby	Štěrba	k1gMnSc2	Štěrba
a	a	k8xC	a
litoměřického	litoměřický	k2eAgMnSc2d1	litoměřický
dominikána	dominikán	k1gMnSc2	dominikán
Benedikta	Benedikt	k1gMnSc2	Benedikt
Kundráta	Kundrát	k1gMnSc2	Kundrát
byly	být	k5eAaImAgFnP	být
vykonány	vykonán	k2eAgFnPc1d1	vykonána
všechny	všechen	k3xTgFnPc1	všechen
potřebné	potřebný	k2eAgFnPc1d1	potřebná
práce	práce	k1gFnPc1	práce
pro	pro	k7c4	pro
beatifikaci	beatifikace	k1gFnSc4	beatifikace
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
beatifikace	beatifikace	k1gFnSc2	beatifikace
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1900	[number]	k4	1900
a	a	k8xC	a
dodán	dodán	k2eAgInSc1d1	dodán
Kongregaci	kongregace	k1gFnSc4	kongregace
pro	pro	k7c4	pro
svatořečení	svatořečení	k1gNnSc4	svatořečení
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Kongregace	kongregace	k1gFnSc1	kongregace
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
procesu	proces	k1gInSc3	proces
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1907	[number]	k4	1907
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
X.	X.	kA	X.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1907	[number]	k4	1907
Zdislavu	Zdislava	k1gFnSc4	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
blahořečil	blahořečit	k5eAaImAgInS	blahořečit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
papežem	papež	k1gMnSc7	papež
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
uznána	uznat	k5eAaPmNgFnS	uznat
za	za	k7c4	za
svatou	svatý	k2eAgFnSc4d1	svatá
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
zázrak	zázrak	k1gInSc1	zázrak
uzdravení	uzdravení	k1gNnSc4	uzdravení
MUDr.	MUDr.	kA	MUDr.
Františka	František	k1gMnSc2	František
Straky	Straka	k1gMnSc2	Straka
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
příbuzní	příbuzný	k1gMnPc1	příbuzný
modlili	modlit	k5eAaImAgMnP	modlit
ke	k	k7c3	k
Zdislavě	Zdislava	k1gFnSc3	Zdislava
a	a	k8xC	a
MUDr.	MUDr.	kA	MUDr.
Straka	Straka	k1gMnSc1	Straka
se	se	k3xPyFc4	se
probudil	probudit	k5eAaPmAgMnS	probudit
z	z	k7c2	z
klinické	klinický	k2eAgFnSc2d1	klinická
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
stotisícovém	stotisícový	k2eAgNnSc6d1	stotisícové
shromáždění	shromáždění	k1gNnSc6	shromáždění
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Olomouci-Neředíně	Olomouci-Neředín	k1gInSc6	Olomouci-Neředín
svatořečil	svatořečit	k5eAaBmAgMnS	svatořečit
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
Zdislavu	Zdislava	k1gFnSc4	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
blahoslaveným	blahoslavený	k2eAgMnSc7d1	blahoslavený
Janem	Jan	k1gMnSc7	Jan
Sarkanderem	Sarkander	k1gMnSc7	Sarkander
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
litoměřického	litoměřický	k2eAgMnSc2d1	litoměřický
biskupa	biskup	k1gMnSc2	biskup
Josefa	Josef	k1gMnSc2	Josef
Koukla	kouknout	k5eAaPmAgFnS	kouknout
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
patronku	patronka	k1gFnSc4	patronka
Litoměřické	litoměřický	k2eAgFnSc2d1	Litoměřická
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
Jablonném	Jablonné	k1gNnSc6	Jablonné
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
je	být	k5eAaImIp3nS	být
diecézním	diecézní	k2eAgNnSc7d1	diecézní
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
ustanovení	ustanovení	k1gNnSc2	ustanovení
krajského	krajský	k2eAgNnSc2d1	krajské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
patronka	patronka	k1gFnSc1	patronka
celého	celý	k2eAgInSc2d1	celý
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vydala	vydat	k5eAaPmAgFnS	vydat
ČNB	ČNB	kA	ČNB
k	k	k7c3	k
750	[number]	k4	750
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pamětní	pamětní	k2eAgFnSc4d1	pamětní
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
minci	mince	k1gFnSc4	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
200	[number]	k4	200
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g/>
,	,	kIx,	,
Prostřední	prostřední	k2eAgFnSc1d1	prostřední
Bečva	Bečva	k1gFnSc1	Bečva
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vsetín	Vsetín	k1gInSc1	Vsetín
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g/>
,	,	kIx,	,
Studnice	studnice	k1gFnSc1	studnice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Oldřichovice	Oldřichovice	k1gFnSc2	Oldřichovice
(	(	kIx(	(
<g/>
Napajedla	napajedlo	k1gNnSc2	napajedlo
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Sedmipočetníků	Sedmipočetník	k1gInPc2	Sedmipočetník
a	a	k8xC	a
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
ve	v	k7c6	v
Víru	Vír	k1gInSc6	Vír
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Kelníkách	Kelník	k1gInPc6	Kelník
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Lavičkách	lavička	k1gFnPc6	lavička
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnPc1	Zdislava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
domově	domov	k1gInSc6	domov
důchodců	důchodce	k1gMnPc2	důchodce
na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
Rudě	ruda	k1gFnSc6	ruda
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
(	(	kIx(	(
<g/>
Uherský	uherský	k2eAgInSc4d1	uherský
Brod	Brod	k1gInSc4	Brod
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Domově	domov	k1gInSc6	domov
důchodců	důchodce	k1gMnPc2	důchodce
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Lvová	lvový	k2eAgFnSc1d1	Lvová
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
ve	v	k7c6	v
Štěnovickém	Štěnovický	k2eAgInSc6d1	Štěnovický
Borku	borek	k1gInSc6	borek
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Charitním	charitní	k2eAgInSc6d1	charitní
domově	domov	k1gInSc6	domov
Zdislava	Zdislava	k1gFnSc1	Zdislava
(	(	kIx(	(
<g/>
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
)	)	kIx)	)
Kaplička	kaplička	k1gFnSc1	kaplička
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Pecce	pecka	k1gFnSc6	pecka
(	(	kIx(	(
<g/>
okr	okr	k1gInSc1	okr
<g/>
.	.	kIx.	.
</s>
<s>
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Šanově	Šanov	k1gInSc6	Šanov
u	u	k7c2	u
Slavičína	Slavičín	k1gInSc2	Slavičín
(	(	kIx(	(
<g/>
farnost	farnost	k1gFnSc1	farnost
Pitín	Pitín	k1gMnSc1	Pitín
<g/>
)	)	kIx)	)
Kaplička	kaplička	k1gFnSc1	kaplička
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
ve	v	k7c6	v
Cvikově	Cvikov	k1gInSc6	Cvikov
(	(	kIx(	(
<g/>
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
dětské	dětský	k2eAgFnSc3d1	dětská
léčebně	léčebna	k1gFnSc3	léčebna
a	a	k8xC	a
ke	k	k7c3	k
Kalvarii	Kalvarie	k1gFnSc3	Kalvarie
od	od	k7c2	od
hotelu	hotel	k1gInSc2	hotel
Sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Oldřichovicích	Oldřichovice	k1gFnPc6	Oldřichovice
<g/>
,	,	kIx,	,
farnost	farnost	k1gFnSc1	farnost
Pohořelice	Pohořelice	k1gFnPc1	Pohořelice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
<g />
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
Komenda	komenda	k1gFnSc1	komenda
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Dubu	dub	k1gInSc6	dub
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
Vitráž	vitráž	k1gFnSc1	vitráž
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jana	k1gFnSc1	Jana
Sarkandera	Sarkandera	k1gFnSc1	Sarkandera
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Mozaika	mozaika	k1gFnSc1	mozaika
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
kostele	kostel	k1gInSc6	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Žďáře	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Sádrový	sádrový	k2eAgInSc1d1	sádrový
reliéf	reliéf	k1gInSc1	reliéf
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnPc4	Zdislava
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Havlem	Havel	k1gMnSc7	Havel
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Křižanově	Křižanův	k2eAgFnSc6d1	Křižanova
Pamětní	pamětní	k2eAgFnPc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
Zdislava	Zdislava	k1gFnSc1	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
Zvon	zvon	k1gInSc1	zvon
sv.	sv.	kA	sv.
Zdislava	Zdislava	k1gFnSc1	Zdislava
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
mince	mince	k1gFnSc1	mince
k	k	k7c3	k
750	[number]	k4	750
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
župa	župa	k1gFnSc1	župa
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
V	V	kA	V
Domov	domov	k1gInSc1	domov
důchodců	důchodce	k1gMnPc2	důchodce
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Červená	červenat	k5eAaImIp3nS	červenat
Voda	voda	k1gFnSc1	voda
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g />
.	.	kIx.	.
</s>
<s>
Kopřivnice	Kopřivnice	k1gFnSc1	Kopřivnice
(	(	kIx(	(
<g/>
www.zdislava.net	www.zdislava.net	k1gMnSc1	www.zdislava.net
<g/>
)	)	kIx)	)
V	V	kA	V
Nemocnice	nemocnice	k1gFnSc2	nemocnice
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc4d1	velké
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
Charitní	charitní	k2eAgInSc1d1	charitní
dům	dům	k1gInSc1	dům
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Ostrava	Ostrava	k1gFnSc1	Ostrava
V	v	k7c4	v
Domov	domov	k1gInSc4	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Plzeň	Plzeň	k1gFnSc1	Plzeň
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
(	(	kIx(	(
<g/>
školka	školka	k1gFnSc1	školka
<g/>
)	)	kIx)	)
v	v	k7c6	v
Předbořicích	Předbořice	k1gFnPc6	Předbořice
V	v	k7c6	v
Rodina	rodina	k1gFnSc1	rodina
sv.	sv.	kA	sv.
<g />
.	.	kIx.	.
</s>
<s>
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Akreditovaný	akreditovaný	k2eAgInSc1d1	akreditovaný
kvalifikační	kvalifikační	k2eAgInSc1d1	kvalifikační
kurz	kurz	k1gInSc1	kurz
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
V	V	kA	V
Kurz	kurz	k1gInSc1	kurz
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
studující	studující	k1gFnSc4	studující
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc4	Brno
V	v	k7c4	v
Střední	střední	k1gMnPc4	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc2d1	sociální
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Aš	Aš	k1gFnSc4	Aš
V	v	k7c4	v
Domov	domov	k1gInSc4	domov
klidného	klidný	k2eAgNnSc2d1	klidné
stáří	stáří	k1gNnSc2	stáří
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Pravětín	Pravětín	k1gInSc4	Pravětín
Skauti	skaut	k1gMnPc1	skaut
-	-	kIx~	-
smečka	smečka	k1gFnSc1	smečka
<g />
.	.	kIx.	.
</s>
<s>
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
V	v	k7c6	v
Komunita	komunita	k1gFnSc1	komunita
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
drogově	drogově	k6eAd1	drogově
závislým	závislý	k2eAgMnPc3d1	závislý
Žibřidice	Žibřidice	k1gFnSc1	Žibřidice
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
V	v	k7c4	v
Domov	domov	k1gInSc4	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Předbořicích	Předbořik	k1gInPc6	Předbořik
u	u	k7c2	u
Kovářova	Kovářův	k2eAgInSc2d1	Kovářův
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
Havlovice	Havlovice	k1gFnSc2	Havlovice
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
V	V	kA	V
Domov	domov	k1gInSc1	domov
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g />
.	.	kIx.	.
</s>
<s>
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
Charitní	charitní	k2eAgInSc4d1	charitní
domov	domov	k1gInSc4	domov
Zdislava	Zdislava	k1gFnSc1	Zdislava
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
Oddělení	oddělení	k1gNnSc2	oddělení
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Domově	domov	k1gInSc6	domov
svaté	svatý	k2eAgFnSc2d1	svatá
Rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
komenda	komenda	k1gFnSc1	komenda
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
Českého	český	k2eAgInSc2d1	český
velkobailiviku	velkobailivik	k1gInSc2	velkobailivik
Vojenského	vojenský	k2eAgInSc2d1	vojenský
a	a	k8xC	a
špitálního	špitální	k2eAgInSc2d1	špitální
řádu	řád	k1gInSc2	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Lazara	Lazar	k1gMnSc2	Lazar
Jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc2d1	sociální
svaté	svatý	k2eAgFnSc2d1	svatá
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
Centrum	centrum	k1gNnSc1	centrum
Zdislava	Zdislava	k1gFnSc1	Zdislava
<g/>
,	,	kIx,	,
denní	denní	k2eAgInSc1d1	denní
stacionář	stacionář	k1gInSc1	stacionář
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Česko-Německá	českoěmecký	k2eAgFnSc1d1	česko-německá
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
sv.	sv.	kA	sv.
Zdislavy	Zdislava	k1gFnSc2	Zdislava
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
</s>
