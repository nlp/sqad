<s>
Blockchain	Blockchain	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Blockchain	Blockchain	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
speciální	speciální	k2eAgInSc4d1
druh	druh	k1gInSc4
distribuované	distribuovaný	k2eAgFnSc2d1
decentralizované	decentralizovaný	k2eAgFnSc2d1
databáze	databáze	k1gFnSc2
uchovávající	uchovávající	k2eAgMnSc1d1
neustále	neustále	k6eAd1
se	se	k3xPyFc4
rozšiřující	rozšiřující	k2eAgInSc4d1
počet	počet	k1gInSc4
záznamů	záznam	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgFnP
proti	proti	k7c3
neoprávněnému	oprávněný	k2eNgInSc3d1
zásahu	zásah	k1gInSc3
jak	jak	k8xS,k8xC
z	z	k7c2
vnější	vnější	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
ze	z	k7c2
strany	strana	k1gFnSc2
samotných	samotný	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
peer-to-peer	peer-to-pera	k1gFnPc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastější	častý	k2eAgFnSc7d3
aplikací	aplikace	k1gFnSc7
technologie	technologie	k1gFnSc2
blockchainu	blockchain	k1gInSc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
jako	jako	k8xC,k8xS
účetní	účetní	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
kryptoměn	kryptoměn	k2eAgMnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
bitcoinu	bitcoinout	k5eAaPmIp1nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
uchovává	uchovávat	k5eAaImIp3nS
transakce	transakce	k1gFnPc4
provedené	provedený	k2eAgFnPc4d1
uživateli	uživatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinace	kombinace	k1gFnSc1
s	s	k7c7
kryptografií	kryptografie	k1gFnSc7
umožňuje	umožňovat	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
anonymitu	anonymita	k1gFnSc4
operací	operace	k1gFnPc2
a	a	k8xC
zabránit	zabránit	k5eAaPmF
neoprávněným	oprávněný	k2eNgFnPc3d1
transakcím	transakce	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
výhody	výhoda	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
blockchainu	blockchainout	k5eAaPmIp1nS
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Schopnost	schopnost	k1gFnSc1
velkého	velký	k2eAgInSc2d1
počtu	počet	k1gInSc2
uzlů	uzel	k1gInPc2
dospět	dospět	k5eAaPmF
k	k	k7c3
jednomu	jeden	k4xCgInSc3
konsenzu	konsenz	k1gInSc3
ohledně	ohledně	k7c2
nejaktuálnějšího	aktuální	k2eAgInSc2d3
stavu	stav	k1gInSc2
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
záznamů	záznam	k1gInPc2
v	v	k7c6
účetní	účetní	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
uzly	uzel	k1gInPc4
anonymní	anonymní	k2eAgInPc4d1
<g/>
,	,	kIx,
připojeny	připojen	k2eAgInPc4d1
přes	přes	k7c4
nespolehlivé	spolehlivý	k2eNgNnSc4d1
spojení	spojení	k1gNnSc4
jeden	jeden	k4xCgMnSc1
k	k	k7c3
druhému	druhý	k4xOgMnSc3
<g/>
,	,	kIx,
či	či	k8xC
vedeny	veden	k2eAgMnPc4d1
podvodníky	podvodník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
transakce	transakce	k1gFnPc1
upravovat	upravovat	k5eAaImF
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Schopnost	schopnost	k1gFnSc1
kteréhokoliv	kterýkoliv	k3yIgInSc2
uzlu	uzel	k1gInSc2
rozhodnout	rozhodnout	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
přijatelnou	přijatelný	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
jistoty	jistota	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
zadaná	zadaný	k2eAgFnSc1d1
transakce	transakce	k1gFnSc1
do	do	k7c2
blockchainu	blockchain	k1gInSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s>
Schopnost	schopnost	k1gFnSc1
libovolného	libovolný	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
či	či	k8xC
přijímá	přijímat	k5eAaImIp3nS
transakci	transakce	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
rozhodnout	rozhodnout	k5eAaPmF
s	s	k7c7
ucházející	ucházející	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
jistoty	jistota	k1gFnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
transakce	transakce	k1gFnSc1
validní	validní	k2eAgFnSc1d1
a	a	k8xC
začlenitelná	začlenitelný	k2eAgFnSc1d1
do	do	k7c2
blockchainu	blockchain	k1gInSc2
natrvalo	natrvalo	k6eAd1
a	a	k8xC
zda	zda	k8xS
nedošlo	dojít	k5eNaPmAgNnS
ke	k	k7c3
kolizi	kolize	k1gFnSc3
dvou	dva	k4xCgInPc6
transakcí	transakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
k	k	k7c3
řešení	řešení	k1gNnSc3
problému	problém	k1gInSc2
double-spendingu	double-spending	k1gInSc2
(	(	kIx(
<g/>
dvojutrácení	dvojutrácený	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dostatečně	dostatečně	k6eAd1
velká	velký	k2eAgFnSc1d1
překážka	překážka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
brání	bránit	k5eAaImIp3nS
útočníkům	útočník	k1gMnPc3
v	v	k7c6
úpravách	úprava	k1gFnPc6
či	či	k8xC
přepsání	přepsání	k1gNnSc1
transakcí	transakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
výměny	výměna	k1gFnPc1
<g/>
/	/	kIx~
<g/>
transakce	transakce	k1gFnPc1
jsou	být	k5eAaImIp3nP
ukládány	ukládat	k5eAaImNgFnP
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
blocích	blok	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
lineárně	lineárně	k6eAd1
navazují	navazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nS
chtěl	chtít	k5eAaImAgMnS
uživatel	uživatel	k1gMnSc1
změnit	změnit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
transakci	transakce	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
její	její	k3xOp3gInSc1
blok	blok	k1gInSc1
schován	schován	k2eAgInSc1d1
např.	např.	kA
za	za	k7c7
již	již	k9
10	#num#	k4
dalšími	další	k2eAgInPc7d1
bloky	blok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc1
blok	blok	k1gInSc1
zapečetěn	zapečetit	k5eAaPmNgInS
digitálním	digitální	k2eAgInSc7d1
podpisem	podpis	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
generuje	generovat	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
informací	informace	k1gFnPc2
uložených	uložený	k2eAgFnPc2d1
v	v	k7c6
bloku	blok	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
změnu	změna	k1gFnSc4
transakce	transakce	k1gFnSc2
přepsal	přepsat	k5eAaPmAgInS
i	i	k9
tento	tento	k3xDgInSc4
podpis	podpis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
však	však	k9
přepsaly	přepsat	k5eAaPmAgInP
i	i	k9
veškeré	veškerý	k3xTgInPc1
podpisy	podpis	k1gInPc1
v	v	k7c6
následujících	následující	k2eAgInPc6d1
blocích	blok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
daný	daný	k2eAgInSc1d1
ledger	ledger	k1gInSc1
shodovat	shodovat	k5eAaImF
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
kopiemi	kopie	k1gFnPc7
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
by	by	kYmCp3nS
"	"	kIx"
<g/>
záškodník	záškodník	k1gMnSc1
<g/>
"	"	kIx"
provést	provést	k5eAaPmF
51	#num#	k4
<g/>
%	%	kIx~
útok	útok	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
ovládat	ovládat	k5eAaImF
většinu	většina	k1gFnSc4
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
ekonomicky	ekonomicky	k6eAd1
extrémně	extrémně	k6eAd1
nákladné	nákladný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Automatická	automatický	k2eAgFnSc1d1
forma	forma	k1gFnSc1
řešení	řešení	k1gNnSc2
konfliktních	konfliktní	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nevalidní	validní	k2eNgFnSc1d1
transakce	transakce	k1gFnSc1
(	(	kIx(
<g/>
například	například	k6eAd1
snaha	snaha	k1gFnSc1
utratit	utratit	k5eAaPmF
částku	částka	k1gFnSc4
na	na	k7c6
účtu	účet	k1gInSc6
vícekrát	vícekrát	k6eAd1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nestanou	stanout	k5eNaPmIp3nP
součástí	součást	k1gFnSc7
potvrzeného	potvrzený	k2eAgInSc2d1
datasetu	dataset	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInSc1d1
princip	princip	k1gInSc1
</s>
<s>
Implementace	implementace	k1gFnSc1
blockchainu	blockchain	k1gInSc2
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
záznamů	záznam	k1gInPc2
<g/>
:	:	kIx,
transakcí	transakce	k1gFnPc2
a	a	k8xC
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transakce	transakce	k1gFnSc1
představují	představovat	k5eAaImIp3nP
data	datum	k1gNnPc1
vložená	vložený	k2eAgNnPc1d1
do	do	k7c2
databáze	databáze	k1gFnPc4
uživateli	uživatel	k1gMnSc3
<g/>
,	,	kIx,
bloky	blok	k1gInPc1
pak	pak	k8xC
záznamy	záznam	k1gInPc1
potvrzující	potvrzující	k2eAgInPc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
a	a	k8xC
jak	jak	k6eAd1
byla	být	k5eAaImAgFnS
konkrétní	konkrétní	k2eAgFnSc1d1
transakce	transakce	k1gFnSc1
přidána	přidat	k5eAaPmNgFnS
do	do	k7c2
databáze	databáze	k1gFnSc2
blockchainu	blockchain	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transakce	transakce	k1gFnSc1
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářen	k2eAgFnPc4d1
uživateli	uživatel	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
systém	systém	k1gInSc4
používají	používat	k5eAaImIp3nP
jako	jako	k9
databázi	databáze	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
kryptoměn	kryptoměn	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
účetní	účetní	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloky	blok	k1gInPc1
oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
těžaři	těžař	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
používají	používat	k5eAaImIp3nP
software	software	k1gInSc4
či	či	k8xC
hardware	hardware	k1gInSc4
vytvořený	vytvořený	k2eAgInSc4d1
specificky	specificky	k6eAd1
k	k	k7c3
vytváření	vytváření	k1gNnSc3
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Transakce	transakce	k1gFnSc2
vytvořené	vytvořený	k2eAgFnSc2d1
uživateli	uživatel	k1gMnSc3
jsou	být	k5eAaImIp3nP
volně	volně	k6eAd1
předávané	předávaný	k2eAgInPc1d1
od	od	k7c2
uzlu	uzel	k1gInSc2
k	k	k7c3
uzlu	uzel	k1gInSc3
podle	podle	k7c2
toho	ten	k3xDgInSc2
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
zrovna	zrovna	k6eAd1
s	s	k7c7
kým	kdo	k3yRnSc7,k3yInSc7,k3yQnSc7
navázané	navázaný	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definice	definice	k1gFnSc1
validní	validní	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
standardu	standard	k1gInSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
implementuje	implementovat	k5eAaImIp3nS
většina	většina	k1gFnSc1
uzlů	uzel	k1gInPc2
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
většiny	většina	k1gFnSc2
kryptoměn	kryptoměn	k2eAgInSc1d1
je	být	k5eAaImIp3nS
za	za	k7c4
validní	validní	k2eAgFnSc4d1
transakci	transakce	k1gFnSc4
většinou	většinou	k6eAd1
považována	považován	k2eAgFnSc1d1
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
správný	správný	k2eAgInSc4d1
elektronický	elektronický	k2eAgInSc4d1
podpis	podpis	k1gInSc4
uživatele	uživatel	k1gMnSc2
<g/>
,	,	kIx,
utrácí	utrácet	k5eAaImIp3nS
peníze	peníz	k1gInPc4
z	z	k7c2
existující	existující	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
uživatel	uživatel	k1gMnSc1
podpisem	podpis	k1gInSc7
prokazuje	prokazovat	k5eAaImIp3nS
vlastnictví	vlastnictví	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
splňuje	splňovat	k5eAaImIp3nS
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
třeba	třeba	k6eAd1
patřičný	patřičný	k2eAgInSc4d1
honorář	honorář	k1gInSc4
(	(	kIx(
<g/>
fee	fee	k?
<g/>
)	)	kIx)
pro	pro	k7c4
těžaře	těžař	k1gMnPc4
nebo	nebo	k8xC
uplynutí	uplynutí	k1gNnPc4
dostatečné	dostatečný	k2eAgFnPc4d1
doby	doba	k1gFnPc4
od	od	k7c2
chvíle	chvíle	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
zadána	zadán	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
transakce	transakce	k1gFnSc1
s	s	k7c7
tímto	tento	k3xDgInSc7
kusem	kus	k1gInSc7
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Těžaři	těžař	k1gMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
snaží	snažit	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
blok	blok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
a	a	k8xC
začleňuje	začleňovat	k5eAaImIp3nS
tyto	tento	k3xDgFnPc4
transakce	transakce	k1gFnPc4
do	do	k7c2
blockchainu	blockchain	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kryptoměnách	kryptoměna	k1gFnPc6
založených	založený	k2eAgFnPc2d1
na	na	k7c4
bitcoinu	bitcoina	k1gFnSc4
jsou	být	k5eAaImIp3nP
těžaři	těžař	k1gMnPc1
motivováni	motivovat	k5eAaBmNgMnP
k	k	k7c3
těžení	těžení	k1gNnSc3
dvěma	dva	k4xCgFnPc7
druhy	druh	k1gInPc1
odměn	odměna	k1gFnPc2
<g/>
:	:	kIx,
předdefinovanou	předdefinovaný	k2eAgFnSc7d1
odměnou	odměna	k1gFnSc7
za	za	k7c4
vytěžený	vytěžený	k2eAgInSc4d1
blok	blok	k1gInSc4
a	a	k8xC
transakčními	transakční	k2eAgInPc7d1
poplatky	poplatek	k1gInPc7
či	či	k8xC
honoráři	honorář	k1gInPc7
(	(	kIx(
<g/>
fee	fee	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
vyplaceny	vyplatit	k5eAaPmNgFnP
kterémukoliv	kterýkoliv	k3yIgMnSc3
těžaři	těžař	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
správně	správně	k6eAd1
potvrdí	potvrdit	k5eAaPmIp3nS
transakci	transakce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Decentralizace	decentralizace	k1gFnSc1
</s>
<s>
Každý	každý	k3xTgInSc1
uzel	uzel	k1gInSc1
v	v	k7c6
síti	síť	k1gFnSc6
decentralizované	decentralizovaný	k2eAgInPc4d1
kryptoměny	kryptoměn	k2eAgInPc4d1
obsahuje	obsahovat	k5eAaImIp3nS
kompletní	kompletní	k2eAgFnSc4d1
či	či	k8xC
částečnou	částečný	k2eAgFnSc4d1
kopii	kopie	k1gFnSc4
blockchainu	blockchain	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
vyřešen	vyřešen	k2eAgInSc1d1
problém	problém	k1gInSc1
centralizované	centralizovaný	k2eAgFnSc2d1
databáze	databáze	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
používají	používat	k5eAaImIp3nP
ostatní	ostatní	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
bankovnictví	bankovnictví	k1gNnSc4
nebo	nebo	k8xC
PayPal	PayPal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
běžná	běžný	k2eAgFnSc1d1
účetní	účetní	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
pouze	pouze	k6eAd1
pasivně	pasivně	k6eAd1
zaznamenává	zaznamenávat	k5eAaImIp3nS
přesuny	přesun	k1gInPc4
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
bankovních	bankovní	k2eAgFnPc2d1
poukázek	poukázka	k1gFnPc2
či	či	k8xC
příkazů	příkaz	k1gInPc2
k	k	k7c3
úhradě	úhrada	k1gFnSc3
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
existují	existovat	k5eAaImIp3nP
nezávisle	závisle	k6eNd1
na	na	k7c6
této	tento	k3xDgFnSc6
knize	kniha	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
kryptoměn	kryptoměn	k2eAgMnSc1d1
jsou	být	k5eAaImIp3nP
jednotky	jednotka	k1gFnPc4
měny	měna	k1gFnPc4
a	a	k8xC
blockchain	blockchain	k1gInSc4
pevně	pevně	k6eAd1
spojeny	spojit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blockchain	Blockchain	k1gInSc1
lze	lze	k6eAd1
v	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
považovat	považovat	k5eAaImF
za	za	k7c4
jediné	jediný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotky	jednotka	k1gFnPc1
kryptoměny	kryptoměn	k2eAgFnPc1d1
existují	existovat	k5eAaImIp3nP
ve	v	k7c6
formě	forma	k1gFnSc6
neutraceného	utracený	k2eNgInSc2d1
součtu	součet	k1gInSc2
všech	všecek	k3xTgFnPc2
transakcí	transakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Transakce	transakce	k1gFnSc1
ve	v	k7c6
formě	forma	k1gFnSc6
záznamů	záznam	k1gInPc2
plátce	plátce	k1gMnPc4
X	X	kA
posílá	posílat	k5eAaImIp3nS
Y	Y	kA
jednotek	jednotka	k1gFnPc2
měny	měna	k1gFnPc4
příjemci	příjemce	k1gMnPc1
Z	z	k7c2
jsou	být	k5eAaImIp3nP
propagovány	propagovat	k5eAaImNgFnP
celou	celý	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
použitím	použití	k1gNnSc7
softwarových	softwarový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
asymetrické	asymetrický	k2eAgFnSc2d1
kryptografie	kryptografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kterýkoliv	kterýkoliv	k3yIgInSc1
uzel	uzel	k1gInSc1
sítě	síť	k1gFnSc2
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
tuto	tento	k3xDgFnSc4
transakci	transakce	k1gFnSc4
ověřit	ověřit	k5eAaPmF
<g/>
,	,	kIx,
přidat	přidat	k5eAaPmF
do	do	k7c2
kopie	kopie	k1gFnSc2
své	svůj	k3xOyFgFnPc4
účetní	účetní	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
a	a	k8xC
přeposlat	přeposlat	k5eAaImF,k5eAaPmF
tyto	tento	k3xDgInPc4
přírůstky	přírůstek	k1gInPc4
ostatním	ostatní	k2eAgMnPc3d1
uzlům	uzel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
blockchainů	blockchain	k1gMnPc2
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existují	existovat	k5eAaImIp3nP
jak	jak	k6eAd1
decentralizované	decentralizovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
centralizované	centralizovaný	k2eAgFnPc4d1
blockchainy	blockchaina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Bitcoin	Bitcoin	k2eAgInSc1d1
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgInSc1d3
existující	existující	k2eAgInSc1d1
blockchain	blockchain	k1gInSc1
je	být	k5eAaImIp3nS
decentralizovaný	decentralizovaný	k2eAgInSc4d1
a	a	k8xC
veřejný	veřejný	k2eAgInSc4d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
většina	většina	k1gFnSc1
alternativních	alternativní	k2eAgInPc2d1
blockchainů	blockchain	k1gInPc2
v	v	k7c6
obecném	obecný	k2eAgNnSc6d1
povědomí	povědomí	k1gNnSc6
<g/>
,	,	kIx,
např.	např.	kA
Ethereum	Ethereum	k1gNnSc1
<g/>
,	,	kIx,
Monero	Monero	k1gNnSc1
<g/>
,	,	kIx,
Litecoin	Litecoin	k1gInSc1
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
veřejně	veřejně	k6eAd1
obchodovatelné	obchodovatelný	k2eAgFnPc1d1
alternativní	alternativní	k2eAgFnPc1d1
kryptoměny	kryptoměn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finančním	finanční	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
experimentuje	experimentovat	k5eAaImIp3nS
s	s	k7c7
využitím	využití	k1gNnSc7
soukromých	soukromý	k2eAgMnPc2d1
blockchainů	blockchain	k1gMnPc2
pro	pro	k7c4
účely	účel	k1gInPc4
privátních	privátní	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
buď	buď	k8xC
zcela	zcela	k6eAd1
soukromé	soukromý	k2eAgFnSc2d1
nebo	nebo	k8xC
tzv.	tzv.	kA
consortium	consortium	k1gNnSc4
blockchainy	blockchaina	k1gFnSc2
omezují	omezovat	k5eAaImIp3nP
přístup	přístup	k1gInSc4
k	k	k7c3
jejich	jejich	k3xOp3gFnPc3
transakcím	transakce	k1gFnPc3
pouze	pouze	k6eAd1
na	na	k7c4
autorizované	autorizovaný	k2eAgMnPc4d1
členy	člen	k1gMnPc4
příslušné	příslušný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nebo	nebo	k8xC
obchodní	obchodní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
konsorcia	konsorcium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
všeobecně	všeobecně	k6eAd1
panuje	panovat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
provoz	provoz	k1gInSc4
soukromých	soukromý	k2eAgNnPc2d1
a	a	k8xC
consortium	consortium	k1gNnSc4
blockchainů	blockchain	k1gMnPc2
je	být	k5eAaImIp3nS
zbytečně	zbytečně	k6eAd1
drahý	drahý	k2eAgInSc1d1
a	a	k8xC
mohly	moct	k5eAaImAgInP
by	by	kYmCp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
klasické	klasický	k2eAgInPc1d1
databázové	databázový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
z	z	k7c2
oboru	obor	k1gInSc2
finančních	finanční	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
obchodního	obchodní	k2eAgInSc2d1
kreditu	kredit	k1gInSc2
argumentují	argumentovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
operační	operační	k2eAgInPc1d1
náklady	náklad	k1gInPc1
jsou	být	k5eAaImIp3nP
jen	jen	k6eAd1
jedním	jeden	k4xCgInSc7
z	z	k7c2
aspektů	aspekt	k1gInPc2
ceny	cena	k1gFnSc2
provedení	provedení	k1gNnSc4
obchodní	obchodní	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průmyslu	průmysl	k1gInSc6
tak	tak	k9
komplikovaném	komplikovaný	k2eAgInSc6d1
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc4d1
obchod	obchod	k1gInSc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
blockchainů	blockchain	k1gInPc2
vyváženy	vyvážen	k2eAgInPc1d1
úsporami	úspora	k1gFnPc7
v	v	k7c6
administrativě	administrativa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řešení	řešení	k1gNnSc1
problému	problém	k1gInSc2
vícenásobného	vícenásobný	k2eAgNnSc2d1
utrácení	utrácení	k1gNnSc2
</s>
<s>
V	v	k7c6
decentralizovaných	decentralizovaný	k2eAgFnPc6d1
distribuovaných	distribuovaný	k2eAgFnPc6d1
databázích	databáze	k1gFnPc6
vzniká	vznikat	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
druh	druh	k1gInSc1
problému	problém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
klasických	klasický	k2eAgFnPc2d1
(	(	kIx(
<g/>
centralizovaných	centralizovaný	k2eAgFnPc2d1
<g/>
)	)	kIx)
databází	databáze	k1gFnPc2
řešen	řešit	k5eAaImNgInS
atomicitou	atomicita	k1gFnSc7
systémových	systémový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
takzvaný	takzvaný	k2eAgInSc4d1
double	double	k2eAgInSc4d1
spending	spending	k1gInSc4
problém	problém	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
též	též	k9
problém	problém	k1gInSc1
dvojutrácení	dvojutrácený	k2eAgMnPc1d1
<g/>
,	,	kIx,
víceutrácení	víceutrácení	k1gNnSc1
či	či	k8xC
problém	problém	k1gInSc1
vícenásobného	vícenásobný	k2eAgNnSc2d1
utrácení	utrácení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
problém	problém	k1gInSc4
bránil	bránit	k5eAaImAgMnS
prakticky	prakticky	k6eAd1
do	do	k7c2
vzniku	vznik	k1gInSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
vytvoření	vytvoření	k1gNnSc4
distribuovaných	distribuovaný	k2eAgFnPc2d1
databází	databáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Problém	problém	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
dva	dva	k4xCgInPc4
či	či	k8xC
více	hodně	k6eAd2
uzlů	uzel	k1gInPc2
v	v	k7c6
topologicky	topologicky	k6eAd1
vzdálených	vzdálený	k2eAgInPc6d1
bodech	bod	k1gInPc6
sítě	síť	k1gFnSc2
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
zadají	zadat	k5eAaPmIp3nP
transakce	transakce	k1gFnPc4
přikazující	přikazující	k2eAgInSc4d1
přesun	přesun	k1gInSc4
peněz	peníze	k1gInPc2
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
účtu	účet	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
výsledné	výsledný	k2eAgFnSc6d1
sumě	suma	k1gFnSc6
je	být	k5eAaImIp3nS
přesunovaná	přesunovaný	k2eAgFnSc1d1
částka	částka	k1gFnSc1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
zůstatek	zůstatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nP
po	po	k7c6
určitém	určitý	k2eAgInSc6d1
čase	čas	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
propagaci	propagace	k1gFnSc3
všech	všecek	k3xTgFnPc2
zadaných	zadaný	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
by	by	kYmCp3nS
vedl	vést	k5eAaImAgInS
k	k	k7c3
nekonzistenci	nekonzistence	k1gFnSc3
zůstatků	zůstatek	k1gInPc2
na	na	k7c6
účtech	účet	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k9
mohly	moct	k5eAaImAgFnP
dostat	dostat	k5eAaPmF
do	do	k7c2
minusu	minus	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nP
v	v	k7c6
konečném	konečný	k2eAgInSc6d1
důsledku	důsledek	k1gInSc6
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
důvěry	důvěra	k1gFnSc2
v	v	k7c4
síť	síť	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInSc4
zánik	zánik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgInPc1d1
kryptoměny	kryptoměn	k2eAgInPc1d1
se	se	k3xPyFc4
s	s	k7c7
tímto	tento	k3xDgInSc7
problémem	problém	k1gInSc7
vyrovnávají	vyrovnávat	k5eAaImIp3nP
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
použitím	použití	k1gNnSc7
systémů	systém	k1gInPc2
decentralizovaných	decentralizovaný	k2eAgNnPc2d1
časových	časový	k2eAgNnPc2d1
razítek	razítko	k1gNnPc2
a	a	k8xC
hlasování	hlasování	k1gNnPc2
o	o	k7c6
pořadí	pořadí	k1gNnSc6
transakcí	transakce	k1gFnPc2
založených	založený	k2eAgFnPc2d1
na	na	k7c6
algoritmech	algoritmus	k1gInPc6
proof	proof	k1gInSc4
of	of	k?
work	work	k1gInSc1
(	(	kIx(
<g/>
hlasování	hlasování	k1gNnSc1
na	na	k7c6
základě	základ	k1gInSc6
prokázaného	prokázaný	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
<g/>
)	)	kIx)
či	či	k8xC
proof	proof	k1gInSc1
of	of	k?
stake	stake	k1gInSc1
(	(	kIx(
<g/>
hlasování	hlasování	k1gNnSc1
na	na	k7c6
základě	základ	k1gInSc6
prokázání	prokázání	k1gNnSc2
vlastnictví	vlastnictví	k1gNnSc2
podílu	podíl	k1gInSc2
kryptoměny	kryptoměn	k2eAgMnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
takzvanému	takzvaný	k2eAgNnSc3d1
51	#num#	k4
<g/>
%	%	kIx~
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vlastník	vlastník	k1gMnSc1
víc	hodně	k6eAd2
jak	jak	k8xC,k8xS
51	#num#	k4
%	%	kIx~
kryptoměny	kryptoměn	k2eAgInPc4d1
či	či	k8xC
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
získává	získávat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
i	i	k9
zpětně	zpětně	k6eAd1
upravovat	upravovat	k5eAaImF
blockchain	blockchain	k1gInSc4
a	a	k8xC
zneplatnit	zneplatnit	k5eAaImF,k5eAaPmF
již	již	k6eAd1
proběhnuté	proběhnutý	k2eAgFnPc4d1
transakce	transakce	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
svojí	svůj	k3xOyFgFnSc7
vytvořenou	vytvořený	k2eAgFnSc7d1
blockchainovou	blockchainový	k2eAgFnSc7d1
větví	větev	k1gFnSc7
předstihne	předstihnout	k5eAaPmIp3nS
již	již	k6eAd1
existující	existující	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
51	#num#	k4
<g/>
%	%	kIx~
útok	útok	k1gInSc1
ale	ale	k8xC
neumožňuje	umožňovat	k5eNaImIp3nS
vytvářet	vytvářet	k5eAaImF
a	a	k8xC
podepisovat	podepisovat	k5eAaImF
transakce	transakce	k1gFnPc4
u	u	k7c2
účtů	účet	k1gInPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
nemá	mít	k5eNaImIp3nS
útočník	útočník	k1gMnSc1
přístup	přístup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
implementací	implementace	k1gFnPc2
kryptoměn	kryptoměn	k2eAgInSc1d1
tak	tak	k8xC,k8xS
obsahuje	obsahovat	k5eAaImIp3nS
ještě	ještě	k6eAd1
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
dodatečné	dodatečný	k2eAgFnPc1d1
pojistky	pojistka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
vést	vést	k5eAaImF
k	k	k7c3
diverzifikaci	diverzifikace	k1gFnSc3
sítě	síť	k1gFnSc2
těžařů	těžař	k1gMnPc2
(	(	kIx(
<g/>
memory	memor	k1gInPc1
hard	hard	k6eAd1
hashovací	hashovací	k2eAgInPc1d1
algoritmy	algoritmus	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
těmto	tento	k3xDgInPc3
útokům	útok	k1gInPc3
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
předcházet	předcházet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demotivací	Demotivace	k1gFnPc2
pro	pro	k7c4
samotné	samotný	k2eAgMnPc4d1
útočníky	útočník	k1gMnPc4
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
vstupní	vstupní	k2eAgFnSc1d1
investice	investice	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
překonat	překonat	k5eAaPmF
množství	množství	k1gNnSc4
držené	držený	k2eAgFnSc2d1
kryptoměny	kryptoměn	k2eAgFnPc4d1
nebo	nebo	k8xC
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
celého	celý	k2eAgInSc2d1
zbytku	zbytek	k1gInSc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
ale	ale	k9
takový	takový	k3xDgInSc1
útok	útok	k1gInSc1
skutečně	skutečně	k6eAd1
podařil	podařit	k5eAaPmAgInS
<g/>
,	,	kIx,
klesla	klesnout	k5eAaPmAgFnS
by	by	kYmCp3nS
důvěra	důvěra	k1gFnSc1
v	v	k7c4
danou	daný	k2eAgFnSc4d1
kryptoměnu	kryptoměn	k2eAgFnSc4d1
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
i	i	k9
její	její	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
útok	útok	k1gInSc1
velmi	velmi	k6eAd1
nevýhodným	výhodný	k2eNgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
těžařská	těžařský	k2eAgNnPc1d1
uskupení	uskupení	k1gNnPc1
<g/>
,	,	kIx,
takzvané	takzvaný	k2eAgFnPc1d1
mining	mining	k1gInSc4
pooly	poola	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
až	až	k9
moc	moc	k6eAd1
vysoké	vysoký	k2eAgNnSc4d1
procentuální	procentuální	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
svůj	svůj	k3xOyFgInSc4
výkon	výkon	k1gInSc4
snížila	snížit	k5eAaPmAgFnS
nebo	nebo	k8xC
je	on	k3xPp3gInPc4
opustili	opustit	k5eAaPmAgMnP
sami	sám	k3xTgMnPc1
těžaři	těžař	k1gMnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
důvěra	důvěra	k1gFnSc1
v	v	k7c4
samotnou	samotný	k2eAgFnSc4d1
kryptoměnu	kryptoměn	k2eAgFnSc4d1
bitcoin	bitcoin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
blockchainové	blockchainový	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
</s>
<s>
Velkou	velký	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
blockchainu	blockchain	k1gInSc2
představuje	představovat	k5eAaImIp3nS
problém	problém	k1gInSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
nadměrného	nadměrný	k2eAgNnSc2d1
využívání	využívání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvoření	vytvoření	k1gNnSc1
jednoho	jeden	k4xCgMnSc4
bloku	blok	k1gInSc2
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
trvá	trvat	k5eAaImIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
při	při	k7c6
globálním	globální	k2eAgNnSc6d1
využití	využití	k1gNnSc6
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
snadno	snadno	k6eAd1
přetížen	přetížen	k2eAgInSc1d1
a	a	k8xC
neúnosně	únosně	k6eNd1
by	by	kYmCp3nS
se	se	k3xPyFc4
zpomalil	zpomalit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
však	však	k9
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
dokud	dokud	k8xS
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
nebude	být	k5eNaImBp3nS
vyřešen	vyřešit	k5eAaPmNgInS
<g/>
,	,	kIx,
k	k	k7c3
všeobecné	všeobecný	k2eAgFnSc3d1
aplikaci	aplikace	k1gFnSc3
blockchainové	blockchainový	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
nedojde	dojít	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
problém	problém	k1gInSc1
blockchainové	blockchainový	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
je	být	k5eAaImIp3nS
nadměrná	nadměrný	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dostupných	dostupný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
má	mít	k5eAaImIp3nS
provoz	provoz	k1gInSc4
a	a	k8xC
přidávání	přidávání	k1gNnSc4
nových	nový	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
do	do	k7c2
bitcoinové	bitcoinový	k2eAgFnSc2d1
databáze	databáze	k1gFnSc2
spotřebu	spotřeba	k1gFnSc4
srovnatelnou	srovnatelný	k2eAgFnSc4d1
s	s	k7c7
třetinou	třetina	k1gFnSc7
roční	roční	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
nutnost	nutnost	k1gFnSc4
připojení	připojení	k1gNnSc2
k	k	k7c3
internetu	internet	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
v	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
světa	svět	k1gInSc2
samozřejmostí	samozřejmost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nakonec	nakonec	k6eAd1
také	také	k9
pro	pro	k7c4
blockchainovou	blockchainový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
ve	v	k7c6
špatných	špatný	k2eAgFnPc6d1
rukách	ruka	k1gFnPc6
škůdce	škůdce	k1gMnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
pomocník	pomocník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totalitní	totalitní	k2eAgFnPc4d1
vlády	vláda	k1gFnPc4
by	by	kYmCp3nS
například	například	k6eAd1
tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
mohly	moct	k5eAaImAgInP
využívat	využívat	k5eAaImF,k5eAaPmF
k	k	k7c3
absolutní	absolutní	k2eAgFnSc3d1
kontrole	kontrola	k1gFnSc3
svých	svůj	k3xOyFgMnPc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
její	její	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
okamžitě	okamžitě	k6eAd1
odhalený	odhalený	k2eAgInSc1d1
každý	každý	k3xTgInSc1
protivládní	protivládní	k2eAgInSc1d1
či	či	k8xC
nevhodný	vhodný	k2eNgInSc1d1
krok	krok	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Block	Block	k6eAd1
time	timat	k5eAaPmIp3nS
</s>
<s>
Pojem	pojem	k1gInSc1
block	block	k6eAd1
time	time	k6eAd1
představuje	představovat	k5eAaImIp3nS
parametr	parametr	k1gInSc1
udávající	udávající	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
vydanými	vydaný	k2eAgInPc7d1
bloky	blok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
kryptoměn	kryptoměn	k2eAgInSc1d1
používá	používat	k5eAaImIp3nS
block	block	k1gInSc1
time	tim	k1gFnSc2
v	v	k7c6
řádu	řád	k1gInSc6
desítek	desítka	k1gFnPc2
minut	minuta	k1gFnPc2
(	(	kIx(
<g/>
10	#num#	k4
minut	minuta	k1gFnPc2
v	v	k7c6
případě	případ	k1gInSc6
bitcoinu	bitcoin	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
ale	ale	k9
objevují	objevovat	k5eAaImIp3nP
nové	nový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
snižují	snižovat	k5eAaImIp3nP
na	na	k7c4
řádově	řádově	k6eAd1
desítky	desítka	k1gFnPc4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
takového	takový	k3xDgInSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
Ethereum	Ethereum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
10	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
sekundami	sekunda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Parametr	parametr	k1gInSc1
block	block	k6eAd1
time	time	k6eAd1
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
ověřování	ověřování	k1gNnSc6
validity	validita	k1gFnSc2
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
většina	většina	k1gFnSc1
systémů	systém	k1gInPc2
považuje	považovat	k5eAaImIp3nS
transakci	transakce	k1gFnSc4
kvůli	kvůli	k7c3
problému	problém	k1gInSc3
vícenásobného	vícenásobný	k2eAgNnSc2d1
utrácení	utrácení	k1gNnSc2
za	za	k7c4
validní	validní	k2eAgNnSc4d1
až	až	k9
po	po	k7c6
několika	několik	k4yIc6
dalších	další	k2eAgInPc6d1
blocích	blok	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
bitcoinu	bitcoin	k1gInSc2
jich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
šest	šest	k4xCc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
je	být	k5eAaImIp3nS
transakce	transakce	k1gFnSc1
začleněna	začleněn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
dá	dát	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
kratší	krátký	k2eAgInSc1d2
čas	čas	k1gInSc1
mezi	mezi	k7c7
vydáním	vydání	k1gNnSc7
dvou	dva	k4xCgInPc2
bloků	blok	k1gInPc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
rychleji	rychle	k6eAd2
mohou	moct	k5eAaImIp3nP
probíhat	probíhat	k5eAaImF
důvěryhodné	důvěryhodný	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Implementace	implementace	k1gFnSc1
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
–	–	k?
Původní	původní	k2eAgFnSc1d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Namecoin	Namecoin	k1gInSc1
–	–	k?
Přidává	přidávat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
uchovávat	uchovávat	k5eAaImF
v	v	k7c6
transakcích	transakce	k1gFnPc6
libovolná	libovolný	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Peercoin	Peercoin	k2eAgInSc1d1
–	–	k?
Hlasovací	hlasovací	k2eAgInSc1d1
algoritmus	algoritmus	k1gInSc1
Proof	Proof	k1gInSc1
of	of	k?
work	work	k1gInSc1
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
Proof	Proof	k1gInSc4
of	of	k?
stake	stake	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ethereum	Ethereum	k1gInSc1
–	–	k?
Turingovsky	Turingovsko	k1gNnPc7
kompletní	kompletní	k2eAgMnSc1d1
chytré	chytrý	k2eAgInPc4d1
kontrakty	kontrakt	k1gInPc4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
sekundový	sekundový	k2eAgInSc1d1
block	block	k1gInSc1
time	timat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
datové	datový	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
</s>
<s>
Existují	existovat	k5eAaImIp3nP
však	však	k9
principiálně	principiálně	k6eAd1
odlišné	odlišný	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
k	k	k7c3
fungování	fungování	k1gNnSc3
decentralizovaných	decentralizovaný	k2eAgFnPc2d1
databází	databáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
blockchainu	blockchain	k1gInSc2
existuje	existovat	k5eAaImIp3nS
rovněž	rovněž	k9
takzvaný	takzvaný	k2eAgInSc1d1
tangle	tangle	k1gInSc1
<g/>
,	,	kIx,
datová	datový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
acyklickém	acyklický	k2eAgInSc6d1
grafu	graf	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tangle	Tangle	k1gNnSc1
lze	lze	k6eAd1
připodobnit	připodobnit	k5eAaPmF
ke	k	k7c3
shluku	shluk	k1gInSc3
jednotlivých	jednotlivý	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
řetězec	řetězec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
kryptoměny	kryptomět	k5eAaPmNgFnP
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
tangle	tangle	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
IOTA	IOTA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
tvoří	tvořit	k5eAaImIp3nS
transakce	transakce	k1gFnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vždy	vždy	k6eAd1
musí	muset	k5eAaImIp3nS
potvrdit	potvrdit	k5eAaPmF
dvě	dva	k4xCgFnPc4
náhodně	náhodně	k6eAd1
vybrané	vybraný	k2eAgFnPc4d1
platby	platba	k1gFnPc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
datovém	datový	k2eAgInSc6d1
shluku	shluk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
technologie	technologie	k1gFnSc2
blockchain	blockchaina	k1gFnPc2
</s>
<s>
Kromě	kromě	k7c2
kryptoměn	kryptoměn	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
blockchainová	blockchainový	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
také	také	k9
další	další	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
zavedeno	zaveden	k2eAgNnSc1d1
nebo	nebo	k8xC
na	na	k7c4
své	svůj	k3xOyFgNnSc4
zavedení	zavedení	k1gNnSc4
teprve	teprve	k6eAd1
čeká	čekat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodně	výhodně	k6eAd1
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
aplikovat	aplikovat	k5eAaBmF
například	například	k6eAd1
v	v	k7c6
těchto	tento	k3xDgFnPc6
situacích	situace	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Decentralizované	decentralizovaný	k2eAgFnPc1d1
volby	volba	k1gFnPc1
–	–	k?
Několikrát	několikrát	k6eAd1
již	již	k6eAd1
nastala	nastat	k5eAaPmAgFnS
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
s	s	k7c7
demokratickými	demokratický	k2eAgFnPc7d1
volbami	volba	k1gFnPc7
manipulováno	manipulován	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blockchain	Blockchain	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
takovým	takový	k3xDgInPc3
problémům	problém	k1gInPc3
zamezit	zamezit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
v	v	k7c6
rukách	ruka	k1gFnPc6
občanů	občan	k1gMnPc2
a	a	k8xC
s	s	k7c7
daty	datum	k1gNnPc7
by	by	kYmCp3nS
nemohl	moct	k5eNaImAgInS
nikdo	nikdo	k3yNnSc1
tajně	tajně	k6eAd1
manipulovat	manipulovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenka	myšlenka	k1gFnSc1
takto	takto	k6eAd1
uskutečněných	uskutečněný	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
má	mít	k5eAaImIp3nS
dnes	dnes	k6eAd1
mnoho	mnoho	k4c1
svých	svůj	k3xOyFgMnPc2
příznivců	příznivec	k1gMnPc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
patří	patřit	k5eAaImIp3nP
také	také	k9
občané	občan	k1gMnPc1
z	z	k7c2
Gruzie	Gruzie	k1gFnSc2
nebo	nebo	k8xC
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zápis	zápis	k1gInSc1
v	v	k7c6
katastru	katastr	k1gInSc6
nemovitostí	nemovitost	k1gFnPc2
–	–	k?
Zápis	zápis	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
blockchainu	blockchain	k1gInSc2
nikdo	nikdo	k3yNnSc1
nevymaže	vymazat	k5eNaPmIp3nS
a	a	k8xC
v	v	k7c6
katastru	katastr	k1gInSc6
nemovitostí	nemovitost	k1gFnPc2
bude	být	k5eAaImBp3nS
zapsán	zapsat	k5eAaPmNgInS
navždy	navždy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamezilo	zamezit	k5eAaPmAgNnS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
neoprávněnému	oprávněný	k2eNgMnSc3d1
nakládání	nakládání	k1gNnSc4
s	s	k7c7
nemovitostmi	nemovitost	k1gFnPc7
<g/>
,	,	kIx,
zpronevěrám	zpronevěra	k1gFnPc3
finančních	finanční	k2eAgFnPc2d1
záloh	záloha	k1gFnPc2
atd.	atd.	kA
Systém	systém	k1gInSc1
také	také	k9
sníží	snížit	k5eAaPmIp3nS
náklady	náklad	k1gInPc4
na	na	k7c4
zápis	zápis	k1gInSc4
a	a	k8xC
provoz	provoz	k1gInSc4
databáze	databáze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
takových	takový	k3xDgInPc2
projektů	projekt	k1gInPc2
je	být	k5eAaImIp3nS
proto	proto	k8xC
již	již	k6eAd1
v	v	k7c6
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Autorská	autorský	k2eAgNnPc1d1
a	a	k8xC
umělecká	umělecký	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
–	–	k?
S	s	k7c7
érou	éra	k1gFnSc7
internetu	internet	k1gInSc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
problém	problém	k1gInSc1
nelegálního	legální	k2eNgNnSc2d1
kopírování	kopírování	k1gNnSc2
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
knih	kniha	k1gFnPc2
i	i	k8xC
filmů	film	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
může	moct	k5eAaImIp3nS
technologie	technologie	k1gFnSc1
blockchain	blockchain	k1gInSc4
vyřešit	vyřešit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožní	umožnit	k5eAaPmIp3nS
vytvářet	vytvářet	k5eAaImF
po	po	k7c6
vzoru	vzor	k1gInSc6
bitcoinu	bitcoin	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
nereplikovatelných	replikovatelný	k2eNgMnPc2d1
tokenů	token	k1gMnPc2
také	také	k6eAd1
nereplikovatelnou	replikovatelný	k2eNgFnSc4d1
kopii	kopie	k1gFnSc4
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesně	přesně	k6eAd1
o	o	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
pokouší	pokoušet	k5eAaImIp3nS
RIAA	RIAA	kA
<g/>
.	.	kIx.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1
sféra	sféra	k1gFnSc1
–	–	k?
Technologie	technologie	k1gFnSc1
blockchainu	blockchain	k1gInSc2
by	by	kYmCp3nS
zde	zde	k6eAd1
umožnila	umožnit	k5eAaPmAgFnS
snáze	snadno	k6eAd2
a	a	k8xC
efektivněji	efektivně	k6eAd2
provádět	provádět	k5eAaImF
například	například	k6eAd1
finanční	finanční	k2eAgInPc4d1
audity	audit	k1gInPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
místo	místo	k7c2
namátkové	namátkový	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
transakcí	transakce	k1gFnPc2
zkontrolují	zkontrolovat	k5eAaPmIp3nP
všechny	všechen	k3xTgFnPc4
transakce	transakce	k1gFnPc4
za	za	k7c4
použití	použití	k1gNnSc4
specifického	specifický	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
půjdou	jít	k5eAaImIp3nP
snáze	snadno	k6eAd2
odhalovat	odhalovat	k5eAaImF
podvody	podvod	k1gInPc4
</s>
<s>
Přeprava	přeprava	k1gFnSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
původ	původ	k1gInSc1
věcí	věc	k1gFnPc2
–	–	k?
Již	již	k9
v	v	k7c6
dubnu	duben	k1gInSc6
2018	#num#	k4
oznámily	oznámit	k5eAaPmAgFnP
společnosti	společnost	k1gFnPc1
IBM	IBM	kA
a	a	k8xC
Helzberg	Helzberg	k1gMnSc1
Diamonds	Diamonds	k1gInSc1
spolupráci	spolupráce	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
vyvinula	vyvinout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
IBM	IBM	kA
blockchainovou	blockchainový	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
TrustChain	TrustChaina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
TrustChain	TrustChain	k2eAgInSc1d1
má	mít	k5eAaImIp3nS
ověřovat	ověřovat	k5eAaImF
pravost	pravost	k1gFnSc4
diamantů	diamant	k1gInPc2
<g/>
,	,	kIx,
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
nebo	nebo	k8xC
šperků	šperk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
také	také	k9
Wal-Mart	Wal-Mart	k1gInSc1
ke	k	k7c3
sledování	sledování	k1gNnSc3
dodávek	dodávka	k1gFnPc2
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
z	z	k7c2
Číny	Čína	k1gFnSc2
a	a	k8xC
přepravní	přepravní	k2eAgInSc1d1
gigant	gigant	k1gInSc1
Maersk	Maersk	k1gInSc4
pro	pro	k7c4
sledování	sledování	k1gNnSc4
přepravovaných	přepravovaný	k2eAgInPc2d1
kontejnerů	kontejner	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zamezení	zamezení	k1gNnSc1
odcizení	odcizení	k1gNnSc2
identity	identita	k1gFnSc2
–	–	k?
Technologie	technologie	k1gFnSc1
blockchain	blockchain	k1gInSc1
by	by	kYmCp3nS
také	také	k9
mohla	moct	k5eAaImAgFnS
pomoci	pomoct	k5eAaPmF
s	s	k7c7
ochranou	ochrana	k1gFnSc7
identity	identita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
jen	jen	k9
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
ukradena	ukrást	k5eAaPmNgFnS
celkem	celkem	k6eAd1
v	v	k7c4
16,7	16,7	k4
miliónů	milión	k4xCgInPc2
případech	případ	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Block	Block	k1gMnSc1
chain	chain	k1gMnSc1
(	(	kIx(
<g/>
database	database	k6eAd1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LÁNSKÝ	Lánský	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kryptoměny	Kryptoměn	k2eAgInPc4d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7400	#num#	k4
<g/>
-	-	kIx~
<g/>
722	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vysvětlení	vysvětlení	k1gNnSc1
Blockchainu	Blockchain	k1gInSc2
pomocí	pomocí	k7c2
výměny	výměna	k1gFnSc2
hokejových	hokejový	k2eAgFnPc2d1
kartiček	kartička	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vysvětlení	vysvětlení	k1gNnSc1
Blockchainu	Blockchain	k1gInSc2
pomocí	pomocí	k7c2
hokejových	hokejový	k2eAgFnPc2d1
kartiček	kartička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIRA	FIRA	kA
Media	medium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-09	2019-08-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TRANG	TRANG	kA
<g/>
,	,	kIx,
Diana	Diana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Christophe	Christophe	k1gNnSc7
Spoerry	Spoerra	k1gFnSc2
on	on	k3xPp3gMnSc1
blockchain	blockchain	k1gMnSc1
technology	technolog	k1gMnPc7
in	in	k?
trade	trade	k6eAd1
finance	finance	k1gFnPc1
<g/>
:	:	kIx,
Operational	Operational	k1gFnSc1
cost	cost	k1gInSc1
is	is	k?
only	onla	k1gFnSc2
one	one	k?
aspect	aspect	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
of	of	k?
Crypto	Crypto	k1gNnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bitcoin	Bitcoin	k2eAgInSc4d1
Miners	Miners	k1gInSc4
Ditch	Ditch	k1gMnSc1
Ghash	Ghash	k1gMnSc1
<g/>
.	.	kIx.
<g/>
io	io	k?
Pool	Pool	k1gInSc1
Over	Over	k1gMnSc1
Fears	Fears	k1gInSc1
of	of	k?
51	#num#	k4
<g/>
%	%	kIx~
Attack	Attacka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CoinPesk	CoinPesk	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
FINEX	FINEX	kA
<g/>
,	,	kIx,
Magazín	magazín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blockchain	Blockchain	k1gInSc1
-	-	kIx~
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
blockchain	blockchain	k2eAgMnSc1d1
a	a	k8xC
jak	jak	k6eAd1
funguje	fungovat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
Aktuální	aktuální	k2eAgFnSc1d1
2019	#num#	k4
<g/>
]	]	kIx)
»	»	k?
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
finex	finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IOTA	IOTA	kA
-	-	kIx~
kryptoměna	kryptoměn	k2eAgFnSc1d1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
nepotřebuje	potřebovat	k5eNaImIp3nS
mining	mining	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaký	jaký	k9
je	být	k5eAaImIp3nS
kurz	kurz	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
MIKSA	MIKSA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IOTA	IOTA	kA
<g/>
:	:	kIx,
kryptoměna	kryptoměn	k2eAgFnSc1d1
nové	nový	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
základem	základ	k1gInSc7
internetu	internet	k1gInSc2
věcí	věc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
Blockchain	Blockchain	k2eAgMnSc1d1
-	-	kIx~
jednoduché	jednoduchý	k2eAgNnSc1d1
vysvětlení	vysvětlení	k1gNnSc1
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
Bitcoin	Bitcoin	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
A	a	k8xC
jak	jak	k6eAd1
funguje	fungovat	k5eAaImIp3nS
blockchain	blockchain	k1gInSc1
<g/>
?	?	kIx.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS
se	se	k3xPyFc4
překlad	překlad	k1gInSc1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
blockchain	blockchain	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k8xS,k8xC
blockchain	blockchain	k1gInSc1
změní	změnit	k5eAaPmIp3nS
budoucnost	budoucnost	k1gFnSc1
Internet	Internet	k1gInSc1
of	of	k?
Things	Things	k1gInSc1
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
blockchain	blockchain	k2eAgMnSc1d1
a	a	k8xC
jak	jak	k6eAd1
funguje	fungovat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
definice	definice	k1gFnSc1
blockchainu	blockchainout	k5eAaPmIp1nS
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kryptoměny	Kryptoměn	k2eAgInPc1d1
založené	založený	k2eAgInPc1d1
na	na	k7c4
SHA-256	SHA-256	k1gFnSc4
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
•	•	k?
Bitcoin	Bitcoin	k1gMnSc1
cash	cash	k1gFnSc2
•	•	k?
Peercoin	Peercoin	k1gInSc1
•	•	k?
Namecoin	Namecoina	k1gFnPc2
založené	založený	k2eAgFnPc4d1
na	na	k7c6
Scryptu	Scrypt	k1gInSc6
</s>
<s>
Auroracoin	Auroracoin	k1gMnSc1
•	•	k?
Dogecoin	Dogecoin	k1gMnSc1
•	•	k?
Litecoin	Litecoin	k1gMnSc1
•	•	k?
PotCoin	PotCoin	k1gMnSc1
založené	založený	k2eAgInPc4d1
na	na	k7c4
Zerocoinu	Zerocoina	k1gFnSc4
</s>
<s>
Zcoin	Zcoin	k1gMnSc1
•	•	k?
ZeroVert	ZeroVert	k1gMnSc1
•	•	k?
Anoncoin	Anoncoin	k1gMnSc1
•	•	k?
SmartCash	SmartCash	k1gMnSc1
•	•	k?
PIVX	PIVX	kA
•	•	k?
Zcash	Zcash	k1gInSc1
•	•	k?
Komodo	komoda	k1gFnSc5
založené	založený	k2eAgInPc1d1
na	na	k7c4
CryptoNote	CryptoNot	k1gMnSc5
</s>
<s>
Bytecoin	Bytecoin	k1gMnSc1
•	•	k?
Monero	Monero	k1gNnSc4
•	•	k?
DigitalNote	DigitalNot	k1gInSc5
•	•	k?
Boolberry	Boolberr	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c4
Ethash	Ethash	k1gInSc4
</s>
<s>
Ethereum	Ethereum	k1gInSc1
•	•	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
•	•	k?
Ubiq	Ubiq	k1gMnSc1
Jiné	jiná	k1gFnSc2
proof-of-work	proof-of-work	k1gInSc1
skripty	skript	k1gInPc4
</s>
<s>
Dash	Dash	k1gMnSc1
•	•	k?
Primecoin	Primecoin	k1gMnSc1
•	•	k?
Digibyte	Digibyt	k1gInSc5
Bez	bez	k1gInSc4
proof-of-work	proof-of-work	k1gInSc1
skriptu	skript	k1gInSc2
</s>
<s>
BlackCoin	BlackCoin	k1gMnSc1
•	•	k?
Burstcoin	Burstcoin	k1gMnSc1
•	•	k?
Counterparty	Counterparta	k1gFnSc2
•	•	k?
Enigma	enigma	k1gFnSc1
•	•	k?
FunFair	FunFair	k1gMnSc1
•	•	k?
Gridcoin	Gridcoin	k1gMnSc1
•	•	k?
Lisk	Lisk	k1gMnSc1
•	•	k?
Melonport	Melonport	k1gInSc1
•	•	k?
NEM	NEM	kA
•	•	k?
NEO	NEO	kA
•	•	k?
Nxt	Nxt	k1gMnSc1
•	•	k?
OmiseGO	OmiseGO	k1gMnSc1
•	•	k?
Polkadot	Polkadot	k1gMnSc1
•	•	k?
Qtum	Qtum	k1gMnSc1
•	•	k?
RChain	RChain	k1gMnSc1
•	•	k?
Ripple	Ripple	k1gMnSc1
•	•	k?
Simple	Simple	k1gMnSc1
Token	Token	k2eAgMnSc1d1
•	•	k?
Stellar	Stellar	k1gMnSc1
•	•	k?
Shadow	Shadow	k1gMnSc1
Technologie	technologie	k1gFnSc2
</s>
<s>
Blockchain	Blockchain	k2eAgInSc1d1
•	•	k?
Proof-of-stake	Proof-of-stake	k1gInSc1
•	•	k?
Proof-of-work	Proof-of-work	k1gInSc1
system	syst	k1gInSc7
•	•	k?
Zerocash	Zerocash	k1gInSc1
•	•	k?
Zerocoin	Zerocoin	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Anonymní	anonymní	k2eAgFnSc1d1
internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
•	•	k?
Kryptoanarchismus	Kryptoanarchismus	k1gInSc1
•	•	k?
Digitální	digitální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Digital	Digital	kA
currency	currenca	k1gFnPc1
exchanger	exchanger	k1gMnSc1
•	•	k?
Double-spending	Double-spending	k1gInSc1
•	•	k?
Virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
</s>
