<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
pěti	pět	k4xCc7	pět
Českými	český	k2eAgInPc7d1	český
lvy	lev	k1gInPc7	lev
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
v	v	k7c6	v
11	[number]	k4	11
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
