<s>
DiscoBalls	DiscoBalls	k6eAd1	DiscoBalls
je	být	k5eAaImIp3nS	být
osmičlenná	osmičlenný	k2eAgFnSc1d1	osmičlenná
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
české	český	k2eAgFnPc4d1	Česká
skupiny	skupina	k1gFnPc4	skupina
hrající	hrající	k2eAgFnSc1d1	hrající
styl	styl	k1gInSc1	styl
ska	ska	k?	ska
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbení	oblíbení	k1gNnPc1	oblíbení
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
odehráli	odehrát	k5eAaPmAgMnP	odehrát
přes	přes	k7c4	přes
500	[number]	k4	500
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Moskvu	Moskva	k1gFnSc4	Moskva
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
čile	čile	k6eAd1	čile
koncertovat	koncertovat	k5eAaImF	koncertovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
i	i	k9	i
čtrnáctidenní	čtrnáctidenní	k2eAgNnSc4d1	čtrnáctidenní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
či	či	k8xC	či
evropské	evropský	k2eAgNnSc1d1	Evropské
turné	turné	k1gNnSc1	turné
s	s	k7c7	s
moskevskou	moskevský	k2eAgFnSc7d1	Moskevská
kapelou	kapela	k1gFnSc7	kapela
M.	M.	kA	M.
<g/>
A.D.	A.D.	k1gMnPc2	A.D.
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Zahráli	zahrát	k5eAaPmAgMnP	zahrát
si	se	k3xPyFc3	se
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Reel	Reel	k1gMnSc1	Reel
Big	Big	k1gMnSc1	Big
Fish	Fish	k1gMnSc1	Fish
<g/>
,	,	kIx,	,
Ska-P	Ska-P	k1gMnSc1	Ska-P
<g/>
,	,	kIx,	,
Bad	Bad	k1gMnSc1	Bad
Manners	Mannersa	k1gFnPc2	Mannersa
<g/>
,	,	kIx,	,
Spitfire	Spitfir	k1gMnSc5	Spitfir
<g/>
,	,	kIx,	,
The	The	k1gFnPc3	The
Valkyrians	Valkyriansa	k1gFnPc2	Valkyriansa
<g/>
,	,	kIx,	,
Skarface	Skarface	k1gFnPc1	Skarface
<g/>
,	,	kIx,	,
Spicy	Spica	k1gFnPc1	Spica
Roots	Roots	k1gInSc1	Roots
<g/>
,	,	kIx,	,
Neville	Neville	k1gInSc1	Neville
Staple	stapl	k1gInSc5	stapl
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Specials	Specials	k1gInSc1	Specials
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Random	Random	k1gInSc4	Random
Hand	Handa	k1gFnPc2	Handa
nebo	nebo	k8xC	nebo
Big	Big	k1gFnSc2	Big
D	D	kA	D
and	and	k?	and
the	the	k?	the
Kids	Kids	k1gInSc1	Kids
Table	tablo	k1gNnSc6	tablo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
CD	CD	kA	CD
Disco	disco	k1gNnSc4	disco
Very	Vera	k1gFnSc2	Vera
Channel	Channela	k1gFnPc2	Channela
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
ska	ska	k?	ska
<g/>
&	&	k?	&
<g/>
reggae	reggae	k1gInSc1	reggae
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydali	vydat	k5eAaPmAgMnP	vydat
své	své	k1gNnSc4	své
druhé	druhý	k4xOgFnPc4	druhý
CD	CD	kA	CD
Rise	Ris	k1gFnPc1	Ris
and	and	k?	and
Shine	Shin	k1gInSc5	Shin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vydali	vydat	k5eAaPmAgMnP	vydat
své	své	k1gNnSc4	své
třetí	třetí	k4xOgFnSc4	třetí
albu	alba	k1gFnSc4	alba
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dance	Danka	k1gFnSc3	Danka
like	likat	k5eAaPmIp3nS	likat
nobody	noboda	k1gFnSc2	noboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
watching	watching	k1gInSc1	watching
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
i	i	k9	i
vinylové	vinylový	k2eAgFnSc2d1	vinylová
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
známým	známý	k2eAgNnSc7d1	známé
německým	německý	k2eAgNnSc7d1	německé
blogerem	blogero	k1gNnSc7	blogero
a	a	k8xC	a
hudebním	hudební	k2eAgMnSc7d1	hudební
kritikem	kritik	k1gMnSc7	kritik
Der	drát	k5eAaImRp2nS	drát
Dude	Dude	k1gNnSc4	Dude
Goes	Goes	k1gInSc1	Goes
Ska	Ska	k1gFnPc1	Ska
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
ska	ska	k?	ska
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
také	také	k9	také
první	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
klip	klip	k1gInSc1	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Shake	Shake	k1gInSc1	Shake
your	your	k1gInSc1	your
ass	ass	k?	ass
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Disco	disco	k1gNnSc2	disco
Very	Vera	k1gFnSc2	Vera
Channel	Channel	k1gInSc1	Channel
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Rise	Ris	k1gInSc2	Ris
and	and	k?	and
Shine	Shin	k1gInSc5	Shin
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Dance	Danka	k1gFnSc6	Danka
like	like	k1gFnPc2	like
nobody	noboda	k1gFnSc2	noboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
watching	watching	k1gInSc1	watching
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Cross	Cross	k1gInSc1	Cross
2007	[number]	k4	2007
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Skankin	Skankin	k1gInSc1	Skankin
Praga	Praga	k1gFnSc1	Praga
<g/>
:	:	kIx,	:
Matusalem	Matusal	k1gInSc7	Matusal
ska	ska	k?	ska
sampler	sampler	k1gInSc1	sampler
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Chancers	Chancers	k1gInSc1	Chancers
<g/>
,	,	kIx,	,
Green	Green	k2eAgInSc1d1	Green
smätroll	smätroll	k1gInSc1	smätroll
<g/>
,	,	kIx,	,
DiscoBalls	DiscoBalls	k1gInSc1	DiscoBalls
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Skannibal	Skannibal	k1gInSc1	Skannibal
Party	parta	k1gFnSc2	parta
Vol	vol	k6eAd1	vol
<g/>
.9	.9	k4	.9
(	(	kIx(	(
<g/>
Mad	Mad	k1gFnSc6	Mad
Butcher	Butchra	k1gFnPc2	Butchra
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Kings	Kings	k1gInSc1	Kings
of	of	k?	of
Prague	Prague	k1gInSc1	Prague
vol	vol	k6eAd1	vol
<g/>
.1	.1	k4	.1
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
SOYUZ	SOYUZ	kA	SOYUZ
split	split	k1gInSc1	split
album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
DiscoBalls	DiscoBalls	k1gInSc1	DiscoBalls
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
A.D.	A.D.	k1gFnSc4	A.D.
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
Wisecräcker	Wisecräckra	k1gFnPc2	Wisecräckra
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
DiscoBalls	DiscoBalls	k1gInSc1	DiscoBalls
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
MTB	MTB	kA	MTB
FreeRide	FreeRid	k1gInSc5	FreeRid
-	-	kIx~	-
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
DVD	DVD	kA	DVD
o	o	k7c6	o
sjezdu	sjezd	k1gInSc6	sjezd
a	a	k8xC	a
freeridu	freerid	k1gInSc6	freerid
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
</s>
