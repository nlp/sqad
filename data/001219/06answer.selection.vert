<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Trajánovy	Trajánův	k2eAgFnSc2d1	Trajánova
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
986	[number]	k4	986
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
srážka	srážka	k1gFnSc1	srážka
armád	armáda	k1gFnPc2	armáda
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
bulharského	bulharský	k2eAgInSc2d1	bulharský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
bulharským	bulharský	k2eAgNnSc7d1	bulharské
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
