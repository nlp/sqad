<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Trajánovy	Trajánův	k2eAgFnSc2d1	Trajánova
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
986	[number]	k4	986
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
srážka	srážka	k1gFnSc1	srážka
armád	armáda	k1gFnPc2	armáda
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
bulharského	bulharský	k2eAgInSc2d1	bulharský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
bulharským	bulharský	k2eAgNnSc7d1	bulharské
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
985	[number]	k4	985
se	se	k3xPyFc4	se
bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
Samuel	Samuel	k1gMnSc1	Samuel
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
významného	významný	k2eAgNnSc2d1	významné
města	město	k1gNnSc2	město
Larissa	Lariss	k1gMnSc2	Lariss
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
Berrhoiu	Berrhoium	k1gNnSc3	Berrhoium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Basileios	Basileios	k1gMnSc1	Basileios
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
získat	získat	k5eAaPmF	získat
strategický	strategický	k2eAgInSc4d1	strategický
bod	bod	k1gInSc4	bod
Serdiku	Serdik	k1gInSc2	Serdik
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
však	však	k9	však
vlivem	vlivem	k7c2	vlivem
špatné	špatný	k2eAgFnSc2d1	špatná
přípravy	příprava	k1gFnSc2	příprava
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
pak	pak	k6eAd1	pak
rychlým	rychlý	k2eAgInSc7d1	rychlý
pochodem	pochod	k1gInSc7	pochod
překvapil	překvapit	k5eAaPmAgInS	překvapit
císaře	císař	k1gMnSc2	císař
i	i	k9	i
jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
horské	horský	k2eAgFnSc6d1	horská
soutěsce	soutěska	k1gFnSc6	soutěska
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Trajánova	Trajánův	k2eAgFnSc1d1	Trajánova
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
byzantských	byzantský	k2eAgMnPc2d1	byzantský
vojáků	voják	k1gMnPc2	voják
zde	zde	k6eAd1	zde
pobil	pobít	k5eAaPmAgMnS	pobít
<g/>
.	.	kIx.	.
</s>
<s>
Basileiovi	Basileius	k1gMnSc3	Basileius
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
podařilo	podařit	k5eAaPmAgNnS	podařit
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
úsilím	úsilí	k1gNnSc7	úsilí
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
s	s	k7c7	s
několika	několik	k4yIc7	několik
muži	muž	k1gMnPc7	muž
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
byzantské	byzantský	k2eAgNnSc4d1	byzantské
území	území	k1gNnSc4	území
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
.	.	kIx.	.
</s>
