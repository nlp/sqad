<s>
Fakulta	fakulta	k1gFnSc1	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
FI	fi	k0	fi
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
<g/>
,	,	kIx,	,
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
doplňkovou	doplňkový	k2eAgFnSc4d1	doplňková
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informatiky	informatika	k1gFnSc2	informatika
jako	jako	k8xS	jako
disciplíny	disciplína	k1gFnSc2	disciplína
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
metodám	metoda	k1gFnPc3	metoda
<g/>
,	,	kIx,	,
modelům	model	k1gInPc3	model
a	a	k8xC	a
nástrojům	nástroj	k1gInPc3	nástroj
zpracování	zpracování	k1gNnSc3	zpracování
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
pomocí	pomocí	k7c2	pomocí
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
informatická	informatický	k2eAgFnSc1d1	informatická
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmá	sedmý	k4xOgFnSc1	sedmý
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
<g/>
.	.	kIx.	.
</s>
<s>
Navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
budování	budování	k1gNnSc2	budování
oboru	obor	k1gInSc6	obor
matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
na	na	k7c6	na
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c4	na
průkopnickou	průkopnický	k2eAgFnSc4d1	průkopnická
činnost	činnost	k1gFnSc4	činnost
prvních	první	k4xOgMnPc2	první
brněnských	brněnský	k2eAgMnPc2d1	brněnský
informatiků	informatik	k1gMnPc2	informatik
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
a	a	k8xC	a
aplikované	aplikovaný	k2eAgFnSc3d1	aplikovaná
informatice	informatika	k1gFnSc3	informatika
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
bakalářských	bakalářský	k2eAgInPc6d1	bakalářský
<g/>
,	,	kIx,	,
navazujících	navazující	k2eAgInPc6d1	navazující
magisterských	magisterský	k2eAgInPc6d1	magisterský
a	a	k8xC	a
doktorských	doktorský	k2eAgInPc6d1	doktorský
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
placená	placený	k2eAgFnSc1d1	placená
forma	forma	k1gFnSc1	forma
studia	studio	k1gNnSc2	studio
pak	pak	k6eAd1	pak
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
středoškolských	středoškolský	k2eAgMnPc2d1	středoškolský
učitelů	učitel	k1gMnPc2	učitel
v	v	k7c6	v
informatických	informatický	k2eAgInPc6d1	informatický
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
Botanické	botanický	k2eAgFnSc6d1	botanická
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
při	při	k7c6	při
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Královo	Brno-Králův	k2eAgNnSc4d1	Brno-Královo
Pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
fakulta	fakulta	k1gFnSc1	fakulta
krátce	krátce	k6eAd1	krátce
sídlila	sídlit	k5eAaImAgFnS	sídlit
na	na	k7c6	na
Burešově	Burešův	k2eAgFnSc6d1	Burešova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
využívá	využívat	k5eAaPmIp3nS	využívat
bezbariérový	bezbariérový	k2eAgInSc4d1	bezbariérový
vstup	vstup	k1gInSc4	vstup
z	z	k7c2	z
Hrnčířské	hrnčířský	k2eAgFnSc2d1	Hrnčířská
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvopočátku	prvopočátek	k1gInSc6	prvopočátek
akademická	akademický	k2eAgFnSc1d1	akademická
obec	obec	k1gFnSc1	obec
čítala	čítat	k5eAaImAgFnS	čítat
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc4	dva
desítky	desítka	k1gFnPc1	desítka
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
400	[number]	k4	400
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
fakulta	fakulta	k1gFnSc1	fakulta
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
okolo	okolo	k7c2	okolo
2	[number]	k4	2
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
fakulta	fakulta	k1gFnSc1	fakulta
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
na	na	k7c6	na
Burešově	Burešův	k2eAgFnSc6d1	Burešova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Botanické	botanický	k2eAgFnSc6d1	botanická
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
4	[number]	k4	4
vzájemně	vzájemně	k6eAd1	vzájemně
propojené	propojený	k2eAgFnSc2d1	propojená
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
čtverce	čtverec	k1gInSc2	čtverec
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
menší	malý	k2eAgNnSc1d2	menší
nekryté	krytý	k2eNgNnSc1d1	nekryté
nádvoří	nádvoří	k1gNnSc1	nádvoří
se	s	k7c7	s
zelení	zeleň	k1gFnSc7	zeleň
a	a	k8xC	a
lavičkami	lavička	k1gFnPc7	lavička
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
významně	významně	k6eAd1	významně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
nové	nový	k2eAgFnPc4d1	nová
posluchárny	posluchárna	k1gFnPc4	posluchárna
(	(	kIx(	(
<g/>
označeny	označen	k2eAgFnPc4d1	označena
písmenem	písmeno	k1gNnSc7	písmeno
D	D	kA	D
a	a	k8xC	a
číslicí	číslice	k1gFnSc7	číslice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
demolici	demolice	k1gFnSc3	demolice
a	a	k8xC	a
znovuvybudování	znovuvybudování	k1gNnSc3	znovuvybudování
vstupní	vstupní	k2eAgFnSc2d1	vstupní
budovy	budova	k1gFnSc2	budova
fakulty	fakulta	k1gFnSc2	fakulta
dosahující	dosahující	k2eAgInSc1d1	dosahující
nově	nova	k1gFnSc3	nova
výšky	výška	k1gFnSc2	výška
pěti	pět	k4xCc2	pět
pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
výrazně	výrazně	k6eAd1	výrazně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
seminárních	seminární	k2eAgFnPc2d1	seminární
<g/>
,	,	kIx,	,
počítačově-laboratorních	počítačověaboratorní	k2eAgFnPc2d1	počítačově-laboratorní
a	a	k8xC	a
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
místností	místnost	k1gFnPc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
byla	být	k5eAaImAgFnS	být
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
z	z	k7c2	z
průčelí	průčelí	k1gNnSc2	průčelí
komplexu	komplex	k1gInSc2	komplex
nyní	nyní	k6eAd1	nyní
vyčnívající	vyčnívající	k2eAgFnSc1d1	vyčnívající
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgFnPc4d1	poskytující
prostory	prostora	k1gFnPc4	prostora
komerčním	komerční	k2eAgFnPc3d1	komerční
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
fakultou	fakulta	k1gFnSc7	fakulta
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vědeckotechnickému	vědeckotechnický	k2eAgInSc3d1	vědeckotechnický
parku	park	k1gInSc3	park
CERIT	CERIT	kA	CERIT
Science	Science	k1gFnSc1	Science
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
vstupní	vstupní	k2eAgFnSc1d1	vstupní
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
A	a	k8xC	a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pětipatrová	pětipatrový	k2eAgFnSc1d1	pětipatrová
stavba	stavba	k1gFnSc1	stavba
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
podzemním	podzemní	k2eAgNnSc7d1	podzemní
zaměstnaneckým	zaměstnanecký	k2eAgNnSc7d1	zaměstnanecké
parkovištěm	parkoviště	k1gNnSc7	parkoviště
a	a	k8xC	a
předsazeným	předsazený	k2eAgInSc7d1	předsazený
objektem	objekt	k1gInSc7	objekt
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgMnPc4d1	průmyslový
partnery	partner	k1gMnPc4	partner
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
výrazná	výrazný	k2eAgNnPc4d1	výrazné
velká	velký	k2eAgNnPc4d1	velké
okna	okno	k1gNnPc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
dále	daleko	k6eAd2	daleko
areál	areál	k1gInSc4	areál
fakulty	fakulta	k1gFnSc2	fakulta
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
(	(	kIx(	(
<g/>
označena	označen	k2eAgNnPc4d1	označeno
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
pískově-béžové	pískověéžový	k2eAgFnSc6d1	pískově-béžový
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
zejména	zejména	k9	zejména
kanceláře	kancelář	k1gFnPc1	kancelář
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc1	několik
seminárních	seminární	k2eAgFnPc2d1	seminární
místností	místnost	k1gFnPc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Děkanát	děkanát	k1gInSc1	děkanát
fakulty	fakulta	k1gFnSc2	fakulta
sdílí	sdílet	k5eAaImIp3nS	sdílet
společně	společně	k6eAd1	společně
s	s	k7c7	s
ekonomicko-personálním	ekonomickoersonální	k2eAgNnSc7d1	ekonomicko-personální
oddělením	oddělení	k1gNnSc7	oddělení
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
patro	patro	k1gNnSc1	patro
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
studijní	studijní	k2eAgNnSc1d1	studijní
oddělení	oddělení	k1gNnSc1	oddělení
se	se	k3xPyFc4	se
z	z	k7c2	z
děkanátu	děkanát	k1gInSc2	děkanát
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
vstupní	vstupní	k2eAgFnSc2d1	vstupní
budovy	budova	k1gFnSc2	budova
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
nad	nad	k7c4	nad
hlavní	hlavní	k2eAgInSc4d1	hlavní
vstup	vstup	k1gInSc4	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
fakulty	fakulta	k1gFnSc2	fakulta
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
půdorysu	půdorys	k1gInSc6	půdorys
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
rohu	roh	k1gInSc6	roh
osobní	osobní	k2eAgMnSc1d1	osobní
a	a	k8xC	a
nákladní	nákladní	k2eAgInPc1d1	nákladní
výtahy	výtah	k1gInPc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
pater	patro	k1gNnPc2	patro
vstupní	vstupní	k2eAgFnSc2d1	vstupní
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
přímý	přímý	k2eAgInSc1d1	přímý
průchod	průchod	k1gInSc1	průchod
do	do	k7c2	do
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pětipatrové	pětipatrový	k2eAgFnSc3d1	pětipatrová
výšce	výška	k1gFnSc3	výška
objektu	objekt	k1gInSc2	objekt
jsou	být	k5eAaImIp3nP	být
střechy	střecha	k1gFnPc1	střecha
budov	budova	k1gFnPc2	budova
využívány	využívat	k5eAaImNgInP	využívat
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
antén	anténa	k1gFnPc2	anténa
mobilních	mobilní	k2eAgMnPc2d1	mobilní
operátorů	operátor	k1gMnPc2	operátor
apod.	apod.	kA	apod.
Objekt	objekt	k1gInSc4	objekt
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
vstup	vstup	k1gInSc1	vstup
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
jen	jen	k9	jen
přes	přes	k7c4	přes
vstupní	vstupní	k2eAgFnSc4d1	vstupní
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
serverového	serverový	k2eAgNnSc2d1	serverové
technického	technický	k2eAgNnSc2d1	technické
zázemí	zázemí	k1gNnSc2	zázemí
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
provozu	provoz	k1gInSc2	provoz
Informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
fakulty	fakulta	k1gFnSc2	fakulta
funguje	fungovat	k5eAaImIp3nS	fungovat
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
budově	budova	k1gFnSc6	budova
nepřetržitá	přetržitý	k2eNgFnSc1d1	nepřetržitá
dohledová	dohledový	k2eAgFnSc1d1	dohledová
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
nepřiléhajících	přiléhající	k2eNgFnPc2d1	nepřiléhající
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
využíváno	využíván	k2eAgNnSc1d1	využíváno
jako	jako	k9	jako
vnější	vnější	k2eAgNnSc1d1	vnější
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
parkoviště	parkoviště	k1gNnSc1	parkoviště
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vjezd	vjezd	k1gInSc1	vjezd
řídí	řídit	k5eAaImIp3nS	řídit
vrátnice	vrátnice	k1gFnPc4	vrátnice
<g/>
.	.	kIx.	.
</s>
<s>
Vrátnice	vrátnice	k1gFnSc1	vrátnice
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
centrálním	centrální	k2eAgInSc7d1	centrální
pultem	pult	k1gInSc7	pult
pro	pro	k7c4	pro
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
požární	požární	k2eAgFnSc4d1	požární
signalizaci	signalizace	k1gFnSc4	signalizace
celého	celý	k2eAgInSc2d1	celý
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Vjezd	vjezd	k1gInSc1	vjezd
do	do	k7c2	do
podzemního	podzemní	k2eAgNnSc2d1	podzemní
zaměstnaneckého	zaměstnanecký	k2eAgNnSc2d1	zaměstnanecké
parkoviště	parkoviště	k1gNnSc2	parkoviště
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Hrnčířské	hrnčířský	k2eAgFnSc2d1	Hrnčířská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
před	před	k7c7	před
vstupní	vstupní	k2eAgFnSc7d1	vstupní
budovou	budova	k1gFnSc7	budova
fakulty	fakulta	k1gFnSc2	fakulta
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
demolici	demolice	k1gFnSc6	demolice
původní	původní	k2eAgFnSc2d1	původní
jednopatrové	jednopatrový	k2eAgFnSc2d1	jednopatrová
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
k	k	k7c3	k
vodorovnému	vodorovný	k2eAgNnSc3d1	vodorovné
narovnání	narovnání	k1gNnSc3	narovnání
prohloubeného	prohloubený	k2eAgInSc2d1	prohloubený
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
svažující	svažující	k2eAgFnPc1d1	svažující
se	se	k3xPyFc4	se
chodníky	chodník	k1gInPc1	chodník
i	i	k8xC	i
vstupní	vstupní	k2eAgNnSc1d1	vstupní
schodiště	schodiště	k1gNnSc1	schodiště
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
zcela	zcela	k6eAd1	zcela
bezbariérový	bezbariérový	k2eAgMnSc1d1	bezbariérový
<g/>
,	,	kIx,	,
bezplošinový	bezplošinový	k2eAgMnSc1d1	bezplošinový
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
parkování	parkování	k1gNnSc2	parkování
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
již	již	k6eAd1	již
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
nemá	mít	k5eNaImIp3nS	mít
menzu	menza	k1gFnSc4	menza
ani	ani	k8xC	ani
bufet	bufet	k1gInSc4	bufet
s	s	k7c7	s
výdejem	výdej	k1gInSc7	výdej
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dříve	dříve	k6eAd2	dříve
provozovala	provozovat	k5eAaImAgFnS	provozovat
správa	správa	k1gFnSc1	správa
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
menz	menza	k1gFnPc2	menza
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
prodejnu	prodejna	k1gFnSc4	prodejna
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
a	a	k8xC	a
denní	denní	k2eAgFnSc7d1	denní
nabídkou	nabídka	k1gFnSc7	nabídka
teplého	teplý	k2eAgNnSc2d1	teplé
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
pracoviště	pracoviště	k1gNnSc4	pracoviště
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
jsou	být	k5eAaImIp3nP	být
katedry	katedra	k1gFnPc1	katedra
<g/>
,	,	kIx,	,
výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
účelová	účelový	k2eAgNnPc1d1	účelové
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
děkanát	děkanát	k1gInSc1	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
organizační	organizační	k2eAgFnSc7d1	organizační
jednotkou	jednotka	k1gFnSc7	jednotka
fakulty	fakulta	k1gFnSc2	fakulta
zodpovědnou	zodpovědný	k2eAgFnSc4d1	zodpovědná
za	za	k7c4	za
organizaci	organizace	k1gFnSc4	organizace
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
poskytování	poskytování	k1gNnSc2	poskytování
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
činnosti	činnost	k1gFnPc4	činnost
fakulty	fakulta	k1gFnSc2	fakulta
jsou	být	k5eAaImIp3nP	být
katedry	katedra	k1gFnPc1	katedra
<g/>
:	:	kIx,	:
Katedra	katedra	k1gFnSc1	katedra
teorie	teorie	k1gFnSc2	teorie
programování	programování	k1gNnSc2	programování
Katedra	katedra	k1gFnSc1	katedra
počítačových	počítačový	k2eAgInPc2d1	počítačový
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
komunikací	komunikace	k1gFnPc2	komunikace
Katedra	katedra	k1gFnSc1	katedra
počítačové	počítačový	k2eAgFnSc2d1	počítačová
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
designu	design	k1gInSc2	design
Katedra	katedra	k1gFnSc1	katedra
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
Vedoucí	vedoucí	k1gFnSc7	vedoucí
kateder	katedra	k1gFnPc2	katedra
jsou	být	k5eAaImIp3nP	být
podřízeni	podřízen	k2eAgMnPc1d1	podřízen
děkanovi	děkanův	k2eAgMnPc1d1	děkanův
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
uvedené	uvedený	k2eAgFnPc4d1	uvedená
katedry	katedra	k1gFnPc4	katedra
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
oddělení	oddělení	k1gNnSc1	oddělení
celouniverzitního	celouniverzitní	k2eAgNnSc2d1	celouniverzitní
Centra	centrum	k1gNnSc2	centrum
jazykového	jazykový	k2eAgNnSc2d1	jazykové
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
oddělení	oddělení	k1gNnSc2	oddělení
Katedry	katedra	k1gFnSc2	katedra
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
účelová	účelový	k2eAgNnPc4d1	účelové
zařízení	zařízení	k1gNnPc4	zařízení
fakulty	fakulta	k1gFnSc2	fakulta
patří	patřit	k5eAaImIp3nS	patřit
Centrum	centrum	k1gNnSc1	centrum
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gFnSc1	vedoucí
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
jsou	být	k5eAaImIp3nP	být
podřízeni	podřízen	k2eAgMnPc1d1	podřízen
tajemníkovi	tajemníkův	k2eAgMnPc1d1	tajemníkův
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
Fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkana	děkan	k1gMnSc4	děkan
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
fakultního	fakultní	k2eAgInSc2d1	fakultní
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektor	rektor	k1gMnSc1	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc1d1	čtyřleté
a	a	k8xC	a
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
děkanem	děkan	k1gMnSc7	děkan
nejvýše	nejvýše	k6eAd1	nejvýše
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
proděkany	proděkan	k1gMnPc4	proděkan
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
její	její	k3xOp3gNnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
informatiky	informatika	k1gFnSc2	informatika
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
působil	působit	k5eAaImAgMnS	působit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
rektora	rektor	k1gMnSc2	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
docent	docent	k1gMnSc1	docent
informatiky	informatika	k1gFnSc2	informatika
Luděk	Luděk	k1gMnSc1	Luděk
Matyska	Matyska	k1gFnSc1	Matyska
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
osobností	osobnost	k1gFnSc7	osobnost
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
děkana	děkan	k1gMnSc2	děkan
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
profesor	profesor	k1gMnSc1	profesor
informatiky	informatika	k1gFnSc2	informatika
Michal	Michal	k1gMnSc1	Michal
Kozubek	Kozubek	k1gInSc1	Kozubek
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
zářím	září	k1gNnSc7	září
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
fakultním	fakultní	k2eAgInSc7d1	fakultní
senátem	senát	k1gInSc7	senát
opět	opět	k6eAd1	opět
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
hlasů	hlas	k1gInPc2	hlas
fakultního	fakultní	k2eAgInSc2d1	fakultní
senátu	senát	k1gInSc2	senát
vůči	vůči	k7c3	vůči
předcházejícímu	předcházející	k2eAgMnSc3d1	předcházející
děkanovi	děkan	k1gMnSc3	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávu	samospráva	k1gFnSc4	samospráva
fakulty	fakulta	k1gFnSc2	fakulta
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
její	její	k3xOp3gFnSc1	její
akademická	akademický	k2eAgFnSc1d1	akademická
obec	obec	k1gFnSc1	obec
(	(	kIx(	(
<g/>
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
zapsaní	zapsaný	k2eAgMnPc1d1	zapsaný
studenti	student	k1gMnPc1	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
akademické	akademický	k2eAgInPc1d1	akademický
orgány	orgán	k1gInPc1	orgán
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
děkan	děkan	k1gMnSc1	děkan
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Jiří	Jiří	k1gMnSc1	Jiří
Barnat	Barnat	k1gMnSc1	Barnat
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
statutární	statutární	k2eAgMnSc1d1	statutární
zástupce	zástupce	k1gMnSc1	zástupce
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Petr	Petr	k1gMnSc1	Petr
Hliněný	hliněný	k2eAgMnSc1d1	hliněný
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Pavel	Pavel	k1gMnSc1	Pavel
Matula	Matula	k1gMnSc1	Matula
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
a	a	k8xC	a
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Václav	Václav	k1gMnSc1	Václav
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
Sc	Sc	k1gFnSc1	Sc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
zahraničí	zahraničí	k1gNnSc4	zahraničí
a	a	k8xC	a
vnější	vnější	k2eAgInPc1d1	vnější
vztahy	vztah	k1gInPc1	vztah
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Petr	Petr	k1gMnSc1	Petr
Sojka	Sojka	k1gMnSc1	Sojka
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Jan	Jan	k1gMnSc1	Jan
Strejček	Strejček	k1gMnSc1	Strejček
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
RNDr.	RNDr.	kA	RNDr.
Lenka	Lenka	k1gFnSc1	Lenka
Bartošková	Bartošková	k1gFnSc1	Bartošková
–	–	k?	–
tajemnice	tajemnice	k1gFnSc1	tajemnice
V	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
objektu	objekt	k1gInSc2	objekt
fakulty	fakulta	k1gFnSc2	fakulta
sídlí	sídlet	k5eAaImIp3nS	sídlet
Ústav	ústav	k1gInSc1	ústav
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
–	–	k?	–
pracoviště	pracoviště	k1gNnSc1	pracoviště
s	s	k7c7	s
celouniverzitní	celouniverzitní	k2eAgFnSc7d1	celouniverzitní
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
CERIT	CERIT	kA	CERIT
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
průmyslových	průmyslový	k2eAgMnPc2d1	průmyslový
partnerů	partner	k1gMnPc2	partner
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
Kybernetický	kybernetický	k2eAgInSc1d1	kybernetický
polygon	polygon	k1gInSc1	polygon
(	(	kIx(	(
<g/>
KYPO	kypa	k1gFnSc5	kypa
<g/>
)	)	kIx)	)
jako	jako	k9	jako
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
metod	metoda	k1gFnPc2	metoda
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
kritickou	kritický	k2eAgFnSc4d1	kritická
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
informatiky	informatika	k1gFnSc2	informatika
nabízí	nabízet	k5eAaImIp3nS	nabízet
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
nebo	nebo	k8xC	nebo
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
informatiku	informatika	k1gFnSc4	informatika
a	a	k8xC	a
také	také	k9	také
dvouoborové	dvouoborový	k2eAgNnSc4d1	dvouoborový
studium	studium	k1gNnSc4	studium
pro	pro	k7c4	pro
učitele	učitel	k1gMnPc4	učitel
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
přírodovědeckou	přírodovědecký	k2eAgFnSc7d1	Přírodovědecká
fakultou	fakulta	k1gFnSc7	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
informatiky	informatika	k1gFnSc2	informatika
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
koncipováno	koncipován	k2eAgNnSc1d1	koncipováno
jako	jako	k8xC	jako
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
mírou	míra	k1gFnSc7	míra
volitelných	volitelný	k2eAgFnPc2d1	volitelná
specializací	specializace	k1gFnPc2	specializace
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
dopředu	dopříst	k5eAaPmIp1nS	dopříst
dán	dát	k5eAaPmNgInS	dát
seznam	seznam	k1gInSc1	seznam
předmětů	předmět	k1gInPc2	předmět
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
mírou	míra	k1gFnSc7	míra
volitelnosti	volitelnost	k1gFnSc2	volitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
třístupňové	třístupňový	k2eAgNnSc1d1	třístupňové
<g/>
,	,	kIx,	,
od	od	k7c2	od
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
navazující	navazující	k2eAgNnSc4d1	navazující
magisterský	magisterský	k2eAgInSc4d1	magisterský
až	až	k8xS	až
po	po	k7c4	po
doktorský	doktorský	k2eAgInSc4d1	doktorský
stupeň	stupeň	k1gInSc4	stupeň
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Majoritně	majoritně	k6eAd1	majoritně
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlastních	vlastní	k2eAgInPc6d1	vlastní
prostorech	prostor	k1gInPc6	prostor
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
anglickém	anglický	k2eAgInSc6d1	anglický
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízen	k2eAgInPc4d1	nabízen
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
studijný	studijný	k2eAgInSc4d1	studijný
programy	program	k1gInPc4	program
a	a	k8xC	a
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
informatika	informatika	k1gFnSc1	informatika
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
informatika	informatika	k1gFnSc1	informatika
Bioinformatika	Bioinformatik	k1gMnSc2	Bioinformatik
Informatika	informatik	k1gMnSc2	informatik
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
Sociální	sociální	k2eAgFnSc1d1	sociální
informatika	informatika	k1gFnSc1	informatika
Informatika	informatika	k1gFnSc1	informatika
Matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
Paralelní	paralelní	k2eAgFnSc1d1	paralelní
a	a	k8xC	a
distribuované	distribuovaný	k2eAgInPc4d1	distribuovaný
systémy	systém	k1gInPc4	systém
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
grafika	grafika	k1gFnSc1	grafika
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
obrazu	obraz	k1gInSc2	obraz
Počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
komunikace	komunikace	k1gFnSc2	komunikace
Počítačové	počítačový	k2eAgInPc1d1	počítačový
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
dat	datum	k1gNnPc2	datum
Programovatelné	programovatelný	k2eAgFnSc2d1	programovatelná
technické	technický	k2eAgFnSc2d1	technická
struktury	struktura	k1gFnSc2	struktura
Umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zpracování	zpracování	k1gNnSc1	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
Informatika	informatika	k1gFnSc1	informatika
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
obor	obor	k1gInSc4	obor
Fyzika	fyzika	k1gFnSc1	fyzika
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
Matematika	matematik	k1gMnSc2	matematik
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
Fakulta	fakulta	k1gFnSc1	fakulta
informatiky	informatika	k1gFnSc2	informatika
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
v	v	k7c6	v
obdobných	obdobný	k2eAgInPc6d1	obdobný
magisterských	magisterský	k2eAgInPc6d1	magisterský
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nabízejí	nabízet	k5eAaImIp3nP	nabízet
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
informatika	informatika	k1gFnSc1	informatika
Služby	služba	k1gFnSc2	služba
-	-	kIx~	-
výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc1	řízení
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
(	(	kIx(	(
<g/>
SSME	SSME	kA	SSME
<g/>
)	)	kIx)	)
Informatika	informatika	k1gFnSc1	informatika
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
Informační	informační	k2eAgInPc1d1	informační
systémy	systém	k1gInPc1	systém
Počítačové	počítačový	k2eAgInPc1d1	počítačový
systémy	systém	k1gInPc1	systém
V	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
Informatika	informatika	k1gFnSc1	informatika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
akademického	akademický	k2eAgInSc2d1	akademický
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
plánovano	plánovana	k1gFnSc5	plánovana
vyšší	vysoký	k2eAgMnSc1d2	vyšší
stipendium	stipendium	k1gNnSc4	stipendium
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
průmyslovými	průmyslový	k2eAgMnPc7d1	průmyslový
partnery	partner	k1gMnPc7	partner
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
analýzy	analýza	k1gFnSc2	analýza
biomedicínského	biomedicínský	k2eAgInSc2d1	biomedicínský
obrazu	obraz	k1gInSc2	obraz
Centrum	centrum	k1gNnSc1	centrum
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
inovací	inovace	k1gFnPc2	inovace
v	v	k7c6	v
informačních	informační	k2eAgFnPc6d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
technologiích	technologie	k1gFnPc6	technologie
Centrum	centrum	k1gNnSc1	centrum
zpracování	zpracování	k1gNnSc4	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
-	-	kIx~	-
http://nlp.fi.muni.cz/	[url]	k?	http://nlp.fi.muni.cz/
Institut	institut	k1gInSc4	institut
teoretické	teoretický	k2eAgFnSc2d1	teoretická
informatiky	informatika	k1gFnSc2	informatika
Vysoce	vysoce	k6eAd1	vysoce
paralelní	paralelní	k2eAgInPc1d1	paralelní
a	a	k8xC	a
distribuované	distribuovaný	k2eAgInPc1d1	distribuovaný
výpočetní	výpočetní	k2eAgInPc1d1	výpočetní
systémy	systém	k1gInPc1	systém
Akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
fakulty	fakulta	k1gFnSc2	fakulta
tvoří	tvořit	k5eAaImIp3nP	tvořit
její	její	k3xOp3gMnPc1	její
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
studenti	student	k1gMnPc1	student
zapsaní	zapsaný	k2eAgMnPc1d1	zapsaný
ve	v	k7c6	v
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
realizovaných	realizovaný	k2eAgFnPc2d1	realizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
členstvím	členství	k1gNnSc7	členství
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
obci	obec	k1gFnSc6	obec
fakulty	fakulta	k1gFnSc2	fakulta
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
akademické	akademický	k2eAgInPc1d1	akademický
obřady	obřad	k1gInPc1	obřad
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
jsou	být	k5eAaImIp3nP	být
imatrikulace	imatrikulace	k1gFnPc1	imatrikulace
(	(	kIx(	(
<g/>
přijetí	přijetí	k1gNnSc1	přijetí
studenta	student	k1gMnSc2	student
do	do	k7c2	do
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
a	a	k8xC	a
promoce	promoce	k1gFnSc1	promoce
(	(	kIx(	(
<g/>
předání	předání	k1gNnSc1	předání
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
diplomu	diplom	k1gInSc2	diplom
absolventovi	absolvent	k1gMnSc3	absolvent
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
obřady	obřad	k1gInPc1	obřad
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
se	s	k7c7	s
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
slibem	slib	k1gInSc7	slib
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
proděkani	proděkan	k1gMnPc1	proděkan
<g/>
,	,	kIx,	,
nositelé	nositel	k1gMnPc1	nositel
vědecko-pedagogických	vědeckoedagogický	k2eAgInPc2d1	vědecko-pedagogický
či	či	k8xC	či
akademicko-vědeckých	akademickoědecký	k2eAgInPc2d1	akademicko-vědecký
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
další	další	k2eAgFnPc1d1	další
osoby	osoba	k1gFnPc1	osoba
pověřené	pověřený	k2eAgFnPc1d1	pověřená
děkanem	děkan	k1gMnSc7	děkan
a	a	k8xC	a
pedel	pedel	k1gMnSc1	pedel
jsou	být	k5eAaImIp3nP	být
oprávněni	oprávnit	k5eAaPmNgMnP	oprávnit
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
užívat	užívat	k5eAaImF	užívat
talár	talár	k1gInSc4	talár
<g/>
.	.	kIx.	.
</s>
<s>
Fakultními	fakultní	k2eAgFnPc7d1	fakultní
insigniemi	insignie	k1gFnPc7	insignie
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
medaile	medaile	k1gFnSc2	medaile
a	a	k8xC	a
žezlo	žezlo	k1gNnSc1	žezlo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
umělecký	umělecký	k2eAgMnSc1d1	umělecký
litec	litec	k1gMnSc1	litec
Jaromír	Jaromír	k1gMnSc1	Jaromír
Gargulák	Gargulák	k1gMnSc1	Gargulák
<g/>
.	.	kIx.	.
<g/>
Avers	avers	k1gInSc1	avers
medaile	medaile	k1gFnSc2	medaile
nese	nést	k5eAaImIp3nS	nést
portrét	portrét	k1gInSc1	portrét
Kurta	Kurt	k1gMnSc2	Kurt
Gödela	Gödel	k1gMnSc2	Gödel
(	(	kIx(	(
<g/>
brněnského	brněnský	k2eAgMnSc2d1	brněnský
matematika	matematik	k1gMnSc2	matematik
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
teoretickým	teoretický	k2eAgInPc3d1	teoretický
základům	základ	k1gInPc3	základ
informatiky	informatika	k1gFnSc2	informatika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
reversu	revers	k1gInSc6	revers
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
znak	znak	k1gInSc1	znak
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
žezla	žezlo	k1gNnSc2	žezlo
zpodobňuje	zpodobňovat	k5eAaImIp3nS	zpodobňovat
oheň	oheň	k1gInSc4	oheň
předaný	předaný	k2eAgInSc4d1	předaný
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
lidstvu	lidstvo	k1gNnSc3	lidstvo
Prométheem	Prométheus	k1gMnSc7	Prométheus
a	a	k8xC	a
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
obdobně	obdobně	k6eAd1	obdobně
všestranný	všestranný	k2eAgInSc4d1	všestranný
význam	význam	k1gInSc4	význam
informatiky	informatika	k1gFnSc2	informatika
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
fakulty	fakulta	k1gFnSc2	fakulta
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Penroseův	Penroseův	k2eAgInSc1d1	Penroseův
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
stylizovaný	stylizovaný	k2eAgInSc1d1	stylizovaný
jako	jako	k8xS	jako
ligatura	ligatura	k1gFnSc1	ligatura
písmen	písmeno	k1gNnPc2	písmeno
F	F	kA	F
a	a	k8xC	a
I.	I.	kA	I.
S	s	k7c7	s
logem	log	k1gInSc7	log
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
výrok	výrok	k1gInSc1	výrok
Blaise	Blaise	k1gFnSc2	Blaise
Pascala	Pascal	k1gMnSc2	Pascal
"	"	kIx"	"
Le	Le	k1gFnSc4	Le
silence	silenka	k1gFnSc3	silenka
éternel	éternela	k1gFnPc2	éternela
de	de	k?	de
ces	ces	k1gNnSc1	ces
espaces	espaces	k1gMnSc1	espaces
infinis	infinis	k1gFnSc3	infinis
m	m	kA	m
<g/>
'	'	kIx"	'
<g/>
effraie	effraie	k1gFnSc1	effraie
<g/>
.	.	kIx.	.
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
"	"	kIx"	"
<g/>
Věčné	věčný	k2eAgNnSc4d1	věčné
mlčení	mlčení	k1gNnSc4	mlčení
těch	ten	k3xDgInPc2	ten
nekonečných	konečný	k2eNgInPc2d1	nekonečný
prostorů	prostor	k1gInPc2	prostor
mě	já	k3xPp1nSc2	já
děsí	děsit	k5eAaImIp3nP	děsit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
loga	logo	k1gNnSc2	logo
je	být	k5eAaImIp3nS	být
vyučující	vyučující	k2eAgMnSc1d1	vyučující
Petr	Petr	k1gMnSc1	Petr
Sojka	Sojka	k1gMnSc1	Sojka
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
ostře	ostro	k6eAd1	ostro
šafránová	šafránový	k2eAgFnSc1d1	šafránová
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
122	[number]	k4	122
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
studentský	studentský	k2eAgInSc4d1	studentský
divadelní	divadelní	k2eAgInSc4d1	divadelní
soubor	soubor	k1gInSc4	soubor
ProFIdivadlo	ProFIdivadlo	k1gNnSc4	ProFIdivadlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
každoročně	každoročně	k6eAd1	každoročně
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svátku	svátek	k1gInSc2	svátek
Dies	Dies	k1gInSc1	Dies
Academicus	Academicus	k1gInSc1	Academicus
uvádí	uvádět	k5eAaImIp3nS	uvádět
inscenaci	inscenace	k1gFnSc4	inscenace
nastudovanou	nastudovaný	k2eAgFnSc4d1	nastudovaná
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
pedagoga	pedagog	k1gMnSc2	pedagog
Josefa	Josef	k1gMnSc2	Josef
Prokeše	Prokeš	k1gMnSc2	Prokeš
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předmětu	předmět	k1gInSc2	předmět
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
akce	akce	k1gFnPc4	akce
pořádané	pořádaný	k2eAgFnPc4d1	pořádaná
studenty	student	k1gMnPc7	student
fakulty	fakulta	k1gFnSc2	fakulta
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Petra	Petr	k1gMnSc2	Petr
Sojky	Sojka	k1gMnSc2	Sojka
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
každoroční	každoroční	k2eAgInSc1d1	každoroční
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
Fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
již	již	k6eAd1	již
přilákal	přilákat	k5eAaPmAgInS	přilákat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Nositeli	nositel	k1gMnPc7	nositel
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
ocenění	ocenění	k1gNnPc2	ocenění
za	za	k7c4	za
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
činnost	činnost	k1gFnSc4	činnost
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
studentů	student	k1gMnPc2	student
fakulty	fakulta	k1gFnSc2	fakulta
jsou	být	k5eAaImIp3nP	být
Jozef	Jozef	k1gInSc4	Jozef
Gruska	Grusk	k1gInSc2	Grusk
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Brandejs	Brandejs	k1gMnSc1	Brandejs
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Zezula	Zezula	k1gMnSc1	Zezula
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Brázdil	brázdit	k5eAaImAgMnS	brázdit
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hořejš	Hořejš	k1gMnSc1	Hořejš
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
ocenění	ocenění	k1gNnSc1	ocenění
za	za	k7c4	za
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
činnost	činnost	k1gFnSc4	činnost
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
Karel	Karel	k1gMnSc1	Karel
Pala	Pala	k1gMnSc1	Pala
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Horák	Horák	k1gMnSc1	Horák
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Rychlý	Rychlý	k1gMnSc1	Rychlý
(	(	kIx(	(
<g/>
za	za	k7c4	za
zpracování	zpracování	k1gNnSc4	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
projekt	projekt	k1gInSc1	projekt
Internetové	internetový	k2eAgFnSc2d1	internetová
jazykové	jazykový	k2eAgFnSc2d1	jazyková
příručky	příručka	k1gFnSc2	příručka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Nálepa	Nálep	k1gMnSc2	Nálep
(	(	kIx(	(
<g/>
multimédia	multimédium	k1gNnSc2	multimédium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Balážia	Balážium	k1gNnSc2	Balážium
(	(	kIx(	(
<g/>
biometrika	biometrika	k1gFnSc1	biometrika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
Řehůřek	Řehůřek	k1gMnSc1	Řehůřek
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
devadesátého	devadesátý	k4xOgNnSc2	devadesátý
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
univerzity	univerzita	k1gFnSc2	univerzita
ocenil	ocenit	k5eAaPmAgMnS	ocenit
rektor	rektor	k1gMnSc1	rektor
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
zlatou	zlatá	k1gFnSc4	zlatá
medailí	medaile	k1gFnPc2	medaile
"	"	kIx"	"
<g/>
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
dobrého	dobrý	k2eAgNnSc2d1	dobré
jména	jméno	k1gNnSc2	jméno
školy	škola	k1gFnSc2	škola
<g/>
"	"	kIx"	"
absolventa	absolvent	k1gMnSc4	absolvent
fakulty	fakulta	k1gFnSc2	fakulta
Thanh	Thanh	k1gMnSc1	Thanh
Han	Hana	k1gFnPc2	Hana
The	The	k1gMnSc1	The
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
magisterské	magisterský	k2eAgFnSc2d1	magisterská
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
obhájené	obhájená	k1gFnSc2	obhájená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
dizertační	dizertační	k2eAgFnSc2d1	dizertační
práce	práce	k1gFnSc2	práce
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
software	software	k1gInSc1	software
pdfTeX	pdfTeX	k?	pdfTeX
umožňující	umožňující	k2eAgInSc1d1	umožňující
export	export	k1gInSc1	export
dokumentů	dokument	k1gInPc2	dokument
z	z	k7c2	z
typografického	typografický	k2eAgInSc2d1	typografický
systému	systém	k1gInSc2	systém
TeX	TeX	k1gFnSc2	TeX
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
formátu	formát	k1gInSc2	formát
PDF	PDF	kA	PDF
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
systému	systém	k1gInSc2	systém
TeX	TeX	k1gMnSc1	TeX
Donald	Donald	k1gMnSc1	Donald
Knuth	Knuth	k1gMnSc1	Knuth
fakultu	fakulta	k1gFnSc4	fakulta
osobně	osobně	k6eAd1	osobně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
od	od	k7c2	od
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
převzal	převzít	k5eAaPmAgMnS	převzít
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
