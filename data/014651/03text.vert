<s>
Pratur	pratur	k1gMnSc1
</s>
<s>
Pratur	pratur	k1gMnSc1
Vyobrazení	vyobrazení	k1gNnSc2
pratura	pratur	k1gMnSc2
z	z	k7c2
Brehmova	Brehmův	k2eAgInSc2d1
Života	život	k1gInSc2
zvířat	zvíře	k1gNnPc2
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
vyhynulý	vyhynulý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
sudokopytníci	sudokopytník	k1gMnPc1
(	(	kIx(
<g/>
Cetartiodactyla	Cetartiodactyla	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
turovití	turovití	k1gMnPc1
(	(	kIx(
<g/>
Bovidae	Bovidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
tuři	tur	k1gMnPc1
(	(	kIx(
<g/>
Bovinae	Bovinae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
tur	tur	k1gMnSc1
(	(	kIx(
<g/>
Bos	bos	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Bos	bos	k1gMnSc1
primigeniusBojanus	primigeniusBojanus	k1gMnSc1
<g/>
,	,	kIx,
1827	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Poddruhy	poddruh	k1gInPc1
a	a	k8xC
formy	forma	k1gFnPc1
</s>
<s>
†	†	k?
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
primigenius	primigenius	k1gMnSc1
–	–	k?
pratur	pratur	k1gMnSc1
euroasijský	euroasijský	k2eAgMnSc1d1
</s>
<s>
†	†	k?
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
namadicus	namadicus	k1gMnSc1
–	–	k?
pratur	pratur	k1gMnSc1
indický	indický	k2eAgMnSc1d1
</s>
<s>
†	†	k?
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
mauretanicus	mauretanicus	k1gMnSc1
–	–	k?
pratur	pratur	k1gMnSc1
africký	africký	k2eAgMnSc1d1
</s>
<s>
Bos	bos	k2eAgMnSc1d1
primigenius	primigenius	k1gMnSc1
f.	f.	k?
taurus	taurus	k1gMnSc1
–	–	k?
tur	tur	k1gMnSc1
domácí	domácí	k2eAgMnSc1d1
</s>
<s>
Bos	bos	k2eAgMnSc1d1
primigenius	primigenius	k1gMnSc1
f.	f.	k?
indicus	indicus	k1gMnSc1
–	–	k?
zebu	zebu	k1gMnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pratur	pratur	k1gMnSc1
(	(	kIx(
<g/>
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vyhynulý	vyhynulý	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
rodu	rod	k1gInSc2
Bos	bos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druh	druh	k1gInSc1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
přežívá	přežívat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
domestikovaných	domestikovaný	k2eAgFnPc2d1
forem	forma	k1gFnPc2
tur	tur	k1gMnSc1
domácí	domácí	k1gMnSc1
(	(	kIx(
<g/>
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
f.	f.	kA
taurus	taurus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
zebu	zebu	k1gMnSc1
(	(	kIx(
<g/>
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
f.	f.	kA
indicus	indicus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
mohutné	mohutný	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
dosahující	dosahující	k2eAgFnSc2d1
kohoutkové	kohoutkový	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
ke	k	k7c3
2	#num#	k4
metrům	metr	k1gInPc3
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc6
800	#num#	k4
až	až	k9
1000	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Býci	býk	k1gMnPc1
měli	mít	k5eAaImAgMnP
krátkou	krátký	k2eAgFnSc4d1
černou	černý	k2eAgFnSc4d1
srst	srst	k1gFnSc4
s	s	k7c7
šedivým	šedivý	k2eAgInSc7d1
či	či	k8xC
nažloutlým	nažloutlý	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
přes	přes	k7c4
celý	celý	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
<g/>
,	,	kIx,
krávy	kráva	k1gFnPc1
hnědočervenou	hnědočervený	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
pomník	pomník	k1gInSc1
v	v	k7c6
Jaktorově	Jaktorův	k2eAgInSc6d1
připomíná	připomínat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
uhynul	uhynout	k5eAaPmAgMnS
poslední	poslední	k2eAgMnSc1d1
pratur	pratur	k1gMnSc1
na	na	k7c6
této	tento	k3xDgFnSc6
planetě	planeta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
rozšířen	rozšířit	k5eAaPmNgInS
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
středního	střední	k2eAgInSc2d1
Východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
paleolitu	paleolit	k1gInSc2
byl	být	k5eAaImAgInS
oblíbenou	oblíbený	k2eAgFnSc7d1
lovnou	lovný	k2eAgFnSc7d1
zvěří	zvěř	k1gFnSc7
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pátém	pátý	k4xOgNnSc6
století	století	k1gNnSc6
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vymřel	vymřít	k5eAaPmAgMnS
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
a	a	k8xC
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vymizel	vymizet	k5eAaPmAgMnS
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Balkánského	balkánský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
zmizel	zmizet	k5eAaPmAgMnS
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
raného	raný	k2eAgInSc2d1
a	a	k8xC
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
a	a	k8xC
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přežíval	přežívat	k5eAaImAgMnS
již	již	k9
jen	jen	k9
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
Moldavsku	Moldavsko	k1gNnSc6
<g/>
,	,	kIx,
Sedmihradsku	Sedmihradsko	k1gNnSc6
a	a	k8xC
Východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdéle	dlouho	k6eAd3
přežil	přežít	k5eAaPmAgMnS
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
odedávna	odedávna	k6eAd1
pod	pod	k7c7
ochranou	ochrana	k1gFnSc7
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
kus	kus	k1gInSc4
zde	zde	k6eAd1
uhynul	uhynout	k5eAaPmAgMnS
roku	rok	k1gInSc2
1627	#num#	k4
v	v	k7c6
rezervaci	rezervace	k1gFnSc6
u	u	k7c2
městečka	městečko	k1gNnSc2
Jaktorova	Jaktorův	k2eAgNnSc2d1
<g/>
,	,	kIx,
asi	asi	k9
50	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
vymření	vymření	k1gNnSc2
pratura	pratur	k1gMnSc4
byl	být	k5eAaImAgInS
lov	lov	k1gInSc1
ze	z	k7c2
strany	strana	k1gFnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Návrat	návrat	k1gInSc1
pratura	pratur	k1gMnSc2
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
německý	německý	k2eAgMnSc1d1
zoolog	zoolog	k1gMnSc1
Lutz	Lutz	k1gMnSc1
Heck	Heck	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
pratura	pratur	k1gMnSc4
zpětně	zpětně	k6eAd1
vyšlechtit	vyšlechtit	k5eAaPmF
z	z	k7c2
několika	několik	k4yIc2
primitivních	primitivní	k2eAgNnPc2d1
plemen	plemeno	k1gNnPc2
domácího	domácí	k2eAgInSc2d1
skotu	skot	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
tohoto	tento	k3xDgInSc2
experimentu	experiment	k1gInSc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Heckův	Heckův	k2eAgInSc1d1
skot	skot	k1gInSc1
neboli	neboli	k8xC
zpětně	zpětně	k6eAd1
vyšlechtěný	vyšlechtěný	k2eAgMnSc1d1
pratur	pratur	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgInS
i	i	k9
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
v	v	k7c6
zooparku	zoopark	k1gInSc6
Chomutov	Chomutov	k1gInSc1
a	a	k8xC
v	v	k7c6
oboře	obora	k1gFnSc6
u	u	k7c2
obce	obec	k1gFnSc2
Křišťanov	Křišťanov	k1gInSc1
na	na	k7c6
Prachaticku	Prachaticko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heckův	Heckův	k2eAgInSc1d1
skot	skot	k1gInSc1
má	mít	k5eAaImIp3nS
sice	sice	k8xC
zbarvení	zbarvení	k1gNnSc1
a	a	k8xC
tvar	tvar	k1gInSc1
rohů	roh	k1gInPc2
blízké	blízký	k2eAgFnSc2d1
původnímu	původní	k2eAgMnSc3d1
praturovi	pratur	k1gMnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
o	o	k7c4
něco	něco	k3yInSc4
drobnější	drobný	k2eAgMnPc1d2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
slabší	slabý	k2eAgInPc4d2
rohy	roh	k1gInPc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
chování	chování	k1gNnSc4
není	být	k5eNaImIp3nS
podobné	podobný	k2eAgNnSc1d1
praturovi	pratur	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Tauros	Taurosa	k1gFnPc2
krávy	kráva	k1gFnSc2
v	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
Milovice	Milovice	k1gFnPc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
zpětného	zpětný	k2eAgNnSc2d1
šlechtění	šlechtění	k1gNnSc2
ujala	ujmout	k5eAaPmAgFnS
nizozemská	nizozemský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Tauros	Taurosa	k1gFnPc2
foundation	foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovala	pracovat	k5eAaImAgFnS
s	s	k7c7
přibližně	přibližně	k6eAd1
30	#num#	k4
druhy	druh	k1gInPc4
skotu	skot	k1gInSc2
z	z	k7c2
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
projektu	projekt	k1gInSc2
jich	on	k3xPp3gInPc2
zařadila	zařadit	k5eAaPmAgFnS
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přibližně	přibližně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
vyšlechtěna	vyšlechtěn	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
jak	jak	k8xS,k8xC
vzhledem	vzhled	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
chováním	chování	k1gNnSc7
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
původní	původní	k2eAgMnSc1d1
pratur	pratur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
bude	být	k5eAaImBp3nS
do	do	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
probíhat	probíhat	k5eAaImF
v	v	k7c6
ohrazených	ohrazený	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
<g/>
,	,	kIx,
po	po	k7c6
dosažení	dosažení	k1gNnSc6
cíle	cíl	k1gInSc2
ve	v	k7c6
šlechtění	šlechtění	k1gNnSc6
by	by	kYmCp3nS
měla	mít	k5eAaImAgNnP
být	být	k5eAaImF
zvířata	zvíře	k1gNnPc1
uznána	uznán	k2eAgNnPc1d1
jako	jako	k8xS,k8xC
volně	volně	k6eAd1
žijící	žijící	k2eAgMnSc1d1
a	a	k8xC
následně	následně	k6eAd1
by	by	kYmCp3nS
měla	mít	k5eAaImAgNnP
být	být	k5eAaImF
vypouštěna	vypouštět	k5eAaImNgNnP
do	do	k7c2
volné	volný	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
nových	nový	k2eAgFnPc2d1
prací	práce	k1gFnPc2
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
geny	gen	k1gInPc1
původního	původní	k2eAgMnSc2d1
divokého	divoký	k2eAgMnSc2d1
pratura	pratur	k1gMnSc2
má	mít	k5eAaImIp3nS
také	také	k9
zubr	zubr	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistilo	zjistit	k5eAaPmAgNnS
se	se	k3xPyFc4
totiž	totiž	k9
<g/>
,	,	kIx,
že	že	k8xS
rodičovskými	rodičovský	k2eAgInPc7d1
druhy	druh	k1gInPc7
zubra	zubr	k1gMnSc2
evropského	evropský	k2eAgInSc2d1
jsou	být	k5eAaImIp3nP
pratur	pratur	k1gMnSc1
a	a	k8xC
Bison	Bison	k1gMnSc1
priscus	priscus	k1gMnSc1
či	či	k8xC
Bison	Bison	k1gMnSc1
schoetensacki	schoetensack	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Několik	několik	k4yIc4
týdnů	týden	k1gInPc2
staré	starý	k2eAgNnSc4d1
tele	tele	k1gNnSc4
pratura	pratur	k1gMnSc2
v	v	k7c4
Přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
velkých	velký	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
Milovice	Milovice	k1gFnPc1
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
malé	malý	k2eAgNnSc1d1
stádo	stádo	k1gNnSc1
zpětně	zpětně	k6eAd1
šlechtěného	šlechtěný	k2eAgMnSc4d1
pratura	pratur	k1gMnSc4
vypuštěno	vypuštěn	k2eAgNnSc1d1
do	do	k7c2
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Pod	pod	k7c7
Benáteckým	benátecký	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
vojenském	vojenský	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
Milovice	Milovice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pratur	pratur	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
pratur	pratur	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Divoký	divoký	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
ferus	ferus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
pratur	pratur	k1gMnSc1
(	(	kIx(
<g/>
Bos	bos	k1gMnSc1
primigenius	primigenius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
klíčové	klíčový	k2eAgInPc4d1
druhy	druh	k1gInPc4
pro	pro	k7c4
formování	formování	k1gNnSc4
české	český	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
odborná	odborný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2014	#num#	k4
</s>
<s>
Dospělá	dospělý	k2eAgFnSc1d1
kráva	kráva	k1gFnSc1
pratura	pratur	k1gMnSc2
v	v	k7c4
Přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
velkých	velký	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
Milovice	Milovice	k1gFnPc1
<g/>
,	,	kIx,
léto	léto	k1gNnSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
ENVIGOGIKA	ENVIGOGIKA	kA
<g/>
,	,	kIx,
Redakce	redakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
zásadní	zásadní	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
současné	současný	k2eAgInPc4d1
environmentálně	environmentálně	k6eAd1
<g/>
/	/	kIx~
<g/>
ekologicky	ekologicky	k6eAd1
orientované	orientovaný	k2eAgFnSc2d1
vzdělávací	vzdělávací	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
i	i	k8xC
praxe	praxe	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Envigogika	Envigogikum	k1gNnPc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
3061	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.147	10.147	k4
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
18023061.50	18023061.50	k4
<g/>
.	.	kIx.
↑	↑	k?
ekolist	ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
„	„	k?
<g/>
Milovický	milovický	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
prostor	prostor	k1gInSc4
spásají	spásat	k5eAaPmIp3nP,k5eAaImIp3nP
nově	nově	k6eAd1
i	i	k8xC
pratuři	pratur	k1gMnPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
13.10	13.10	k4
<g/>
.2015	.2015	k4
<g/>
..	..	k?
ekolist	ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
Krajina	Krajina	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Pratuři	pratur	k1gMnPc1
z	z	k7c2
Milovic	Milovice	k1gFnPc2
zaujali	zaujmout	k5eAaPmAgMnP
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
i	i	k8xC
média	médium	k1gNnPc1
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
Mexiku	Mexiko	k1gNnSc6
<g/>
,	,	kIx,
Švýcarsku	Švýcarsko	k1gNnSc6
a	a	k8xC
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
15.10	15.10	k4
<g/>
.2015	.2015	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tauros	Taurosa	k1gFnPc2
Programme	Programme	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Locations	Locations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Where	Wher	k1gInSc5
to	ten	k3xDgNnSc1
meet	meet	k2eAgInSc1d1
the	the	k?
Tauros	Taurosa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4143377-4	4143377-4	k4
</s>
