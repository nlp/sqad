<p>
<s>
Tacitron	Tacitron	k1gInSc1	Tacitron
je	být	k5eAaImIp3nS	být
plynem	plyn	k1gInSc7	plyn
plněná	plněný	k2eAgFnSc1d1	plněná
výbojka	výbojka	k1gFnSc1	výbojka
řízená	řízený	k2eAgFnSc1d1	řízená
mřížkou	mřížka	k1gFnSc7	mřížka
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnSc3d1	možná
přivedením	přivedení	k1gNnSc7	přivedení
záporného	záporný	k2eAgInSc2d1	záporný
potenciálu	potenciál	k1gInSc2	potenciál
na	na	k7c4	na
řídicí	řídicí	k2eAgFnSc4d1	řídicí
mřížku	mřížka	k1gFnSc4	mřížka
výboj	výboj	k1gInSc1	výboj
přerušit	přerušit	k5eAaPmF	přerušit
(	(	kIx(	(
<g/>
zhasnout	zhasnout	k5eAaPmF	zhasnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snížením	snížení	k1gNnSc7	snížení
záporného	záporný	k2eAgNnSc2d1	záporné
předpětí	předpětí	k1gNnSc2	předpětí
mřížky	mřížka	k1gFnSc2	mřížka
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
blízkou	blízký	k2eAgFnSc4d1	blízká
potenciálu	potenciál	k1gInSc3	potenciál
katody	katoda	k1gFnSc2	katoda
tacitron	tacitron	k1gInSc1	tacitron
opět	opět	k6eAd1	opět
zapálí	zapálit	k5eAaPmIp3nS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
tacitronů	tacitron	k1gInPc2	tacitron
zachovány	zachovat	k5eAaPmNgFnP	zachovat
ostatní	ostatní	k2eAgFnPc1d1	ostatní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
nízký	nízký	k2eAgInSc4d1	nízký
spád	spád	k1gInSc4	spád
na	na	k7c6	na
oblouku	oblouk	k1gInSc6	oblouk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
propouštět	propouštět	k5eAaImF	propouštět
značný	značný	k2eAgInSc4d1	značný
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ztracený	ztracený	k2eAgInSc4d1	ztracený
anodový	anodový	k2eAgInSc4d1	anodový
výkon	výkon	k1gInSc4	výkon
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
</p>
