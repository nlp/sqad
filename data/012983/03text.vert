<p>
<s>
Capital	Capital	k1gMnSc1	Capital
Punishment	Punishment	k1gInSc4	Punishment
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
například	například	k6eAd1	například
kapelami	kapela	k1gFnPc7	kapela
Cabaret	Cabaret	k1gInSc1	Cabaret
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
Throbbing	Throbbing	k1gInSc4	Throbbing
Gristle	Gristle	k1gFnSc2	Gristle
či	či	k8xC	či
Brianem	Brian	k1gInSc7	Brian
Enem	Enem	k?	Enem
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Ben	Ben	k1gInSc1	Ben
Stiller	Stiller	k1gInSc1	Stiller
<g/>
,	,	kIx,	,
Kriss	Kriss	k1gInSc1	Kriss
Roebling	Roebling	k1gInSc1	Roebling
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Swann	Swann	k1gMnSc1	Swann
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Zusi	Zus	k1gFnSc2	Zus
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
jediné	jediný	k2eAgNnSc1d1	jediné
album	album	k1gNnSc1	album
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Roadkill	Roadkill	k1gInSc1	Roadkill
(	(	kIx(	(
<g/>
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Variety	varieta	k1gFnSc2	varieta
Recording	Recording	k1gInSc1	Recording
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
Stiller	Stiller	k1gMnSc1	Stiller
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
bicí	bicí	k2eAgFnSc4d1	bicí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Zusi	Zus	k1gFnSc3	Zus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
v	v	k7c6	v
reedici	reedice	k1gFnSc6	reedice
(	(	kIx(	(
<g/>
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Captured	Captured	k1gMnSc1	Captured
Tracks	Tracks	k1gInSc1	Tracks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
kapela	kapela	k1gFnSc1	kapela
také	také	k6eAd1	také
vydala	vydat	k5eAaPmAgFnS	vydat
nové	nový	k2eAgInPc4d1	nový
extended	extended	k1gInSc4	extended
play	play	k0	play
s	s	k7c7	s
názvem	název	k1gInSc7	název
This	This	k1gInSc1	This
Is	Is	k1gFnPc2	Is
Capital	Capital	k1gMnSc1	Capital
Punishment	Punishment	k1gMnSc1	Punishment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Capital	Capital	k1gMnSc1	Capital
Punishment	Punishment	k1gMnSc1	Punishment
na	na	k7c4	na
Discogs	Discogs	k1gInSc4	Discogs
</s>
</p>
