<p>
<s>
Daphnis	Daphnis	k1gFnSc1	Daphnis
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc4	Saturn
<g/>
,	,	kIx,	,
též	též	k9	též
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Saturn	Saturn	k1gMnSc1	Saturn
XXXV	XXXV	kA	XXXV
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
činí	činit	k5eAaImIp3nS	činit
136	[number]	k4	136
504	[number]	k4	504
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc1	rozměra
tělesa	těleso	k1gNnSc2	těleso
jsou	být	k5eAaImIp3nP	být
6	[number]	k4	6
–	–	k?	–
8	[number]	k4	8
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
Dafnisovi	Dafnis	k1gMnSc6	Dafnis
<g/>
,	,	kIx,	,
synovi	syn	k1gMnSc6	syn
boha	bůh	k1gMnSc4	bůh
Herma	Hermes	k1gMnSc4	Hermes
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
vědeckým	vědecký	k2eAgInSc7d1	vědecký
týmem	tým	k1gInSc7	tým
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
S	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
trvá	trvat	k5eAaImIp3nS	trvat
měsíci	měsíc	k1gInSc3	měsíc
0,5940	[number]	k4	0,5940
dne	den	k1gInSc2	den
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Daphnis	Daphnis	k1gFnSc2	Daphnis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
