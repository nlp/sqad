<s>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
[	[	kIx(	[
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
eibrhem	eibrh	k1gInSc7	eibrh
linkoln	linkoln	k1gMnSc1	linkoln
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1809	[number]	k4	1809
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861	[number]	k4	1861
-	-	kIx~	-
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
vedl	vést	k5eAaImAgMnS	vést
severní	severní	k2eAgInPc4d1	severní
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
farmáře	farmář	k1gMnSc2	farmář
a	a	k8xC	a
dělníka	dělník	k1gMnSc2	dělník
Thomase	Thomas	k1gMnSc2	Thomas
Lincolna	Lincoln	k1gMnSc2	Lincoln
a	a	k8xC	a
Nancy	Nancy	k1gFnSc2	Nancy
Hanksové	Hanksová	k1gFnSc2	Hanksová
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
negramotná	gramotný	k2eNgFnSc1d1	negramotná
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
velkému	velký	k2eAgInSc3d1	velký
majetku	majetek	k1gInSc3	majetek
a	a	k8xC	a
různým	různý	k2eAgInPc3d1	různý
vedlejším	vedlejší	k2eAgInPc3d1	vedlejší
příjmům	příjem	k1gInPc3	příjem
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
chudých	chudý	k2eAgInPc6d1	chudý
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Macon	Macona	k1gFnPc2	Macona
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Malárie	malárie	k1gFnSc1	malárie
připravila	připravit	k5eAaPmAgFnS	připravit
Abrahama	Abraham	k1gMnSc4	Abraham
Lincolna	Lincoln	k1gMnSc4	Lincoln
o	o	k7c4	o
matku	matka	k1gFnSc4	matka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Indianě	Indiana	k1gFnSc6	Indiana
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
Abraham	Abraham	k1gMnSc1	Abraham
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vydal	vydat	k5eAaPmAgMnS	vydat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
obchodníkem	obchodník	k1gMnSc7	obchodník
Jamesem	James	k1gMnSc7	James
Gentrym	Gentrym	k1gInSc4	Gentrym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
zaměstnal	zaměstnat	k5eAaPmAgInS	zaměstnat
na	na	k7c4	na
přepravu	přeprava	k1gFnSc4	přeprava
zboží	zboží	k1gNnSc2	zboží
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
nové	nový	k2eAgFnSc3d1	nová
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc1	Orleans
poznal	poznat	k5eAaPmAgInS	poznat
hrůzy	hrůza	k1gFnPc4	hrůza
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
mnoho	mnoho	k6eAd1	mnoho
povolání	povolání	k1gNnSc4	povolání
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
dřevorubcem	dřevorubec	k1gMnSc7	dřevorubec
<g/>
,	,	kIx,	,
lodivodem	lodivod	k1gMnSc7	lodivod
<g/>
,	,	kIx,	,
lovcem	lovec	k1gMnSc7	lovec
<g/>
,	,	kIx,	,
zeměměřičem	zeměměřič	k1gMnSc7	zeměměřič
<g/>
,	,	kIx,	,
vojákem	voják	k1gMnSc7	voják
<g/>
,	,	kIx,	,
poštmistrem	poštmistr	k1gMnSc7	poštmistr
a	a	k8xC	a
pomocníkem	pomocník	k1gMnSc7	pomocník
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k8xC	i
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
zapojoval	zapojovat	k5eAaImAgInS	zapojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
jeho	jeho	k3xOp3gFnSc7	jeho
řečnický	řečnický	k2eAgInSc1d1	řečnický
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Salemu	Salem	k1gInSc2	Salem
si	se	k3xPyFc3	se
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
zálibě	záliba	k1gFnSc3	záliba
-	-	kIx~	-
knihám	kniha	k1gFnPc3	kniha
-	-	kIx~	-
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
pověst	pověst	k1gFnSc4	pověst
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
začala	začít	k5eAaPmAgFnS	začít
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
na	na	k7c4	na
Abrahama	Abraham	k1gMnSc4	Abraham
Lincolna	Lincoln	k1gMnSc4	Lincoln
začali	začít	k5eAaPmAgMnP	začít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
obracet	obracet	k5eAaImF	obracet
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
spory	spor	k1gInPc7	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jim	on	k3xPp3gMnPc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
poprvé	poprvé	k6eAd1	poprvé
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volebním	volební	k2eAgInSc6d1	volební
programu	program	k1gInSc6	program
vyzdvihoval	vyzdvihovat	k5eAaImAgInS	vyzdvihovat
zřízení	zřízení	k1gNnSc4	zřízení
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc1	zavedení
cla	clo	k1gNnSc2	clo
<g/>
,	,	kIx,	,
také	také	k9	také
zlepšení	zlepšení	k1gNnSc4	zlepšení
veřejných	veřejný	k2eAgFnPc2d1	veřejná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jasně	jasně	k6eAd1	jasně
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgNnSc7	svůj
smýšlením	smýšlení	k1gNnSc7	smýšlení
tíhne	tíhnout	k5eAaImIp3nS	tíhnout
k	k	k7c3	k
Whigům	whig	k1gMnPc3	whig
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
milic	milice	k1gFnPc2	milice
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgMnPc7	který
měl	mít	k5eAaImAgMnS	mít
jako	jako	k9	jako
kapitán	kapitán	k1gMnSc1	kapitán
bránit	bránit	k5eAaImF	bránit
hranice	hranice	k1gFnPc1	hranice
státu	stát	k1gInSc2	stát
před	před	k7c7	před
Indiány	Indián	k1gMnPc7	Indián
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nezapojil	zapojit	k5eNaPmAgMnS	zapojit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
uspěl	uspět	k5eAaPmAgInS	uspět
až	až	k6eAd1	až
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
pokus	pokus	k1gInSc4	pokus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ještě	ještě	k6eAd1	ještě
třikrát	třikrát	k6eAd1	třikrát
stále	stále	k6eAd1	stále
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
sledoval	sledovat	k5eAaImAgInS	sledovat
soudy	soud	k1gInPc4	soud
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
složil	složit	k5eAaPmAgMnS	složit
advokátské	advokátský	k2eAgFnPc4d1	advokátská
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Springfieldu	Springfield	k1gInSc2	Springfield
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
společníkem	společník	k1gMnSc7	společník
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
advokátní	advokátní	k2eAgFnSc3d1	advokátní
kanceláři	kancelář	k1gFnSc3	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Whig	whig	k1gMnSc1	whig
Party	part	k1gInPc4	part
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnSc7	jejich
volebním	volební	k2eAgMnSc7d1	volební
lídrem	lídr	k1gMnSc7	lídr
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1842	[number]	k4	1842
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Mary	Mary	k1gFnSc7	Mary
Toddovou	Toddův	k2eAgFnSc7d1	Toddova
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
znal	znát	k5eAaImAgMnS	znát
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gNnSc3	on
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Edwarda	Edward	k1gMnSc4	Edward
a	a	k8xC	a
Williama	William	k1gMnSc4	William
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
Roberta	Robert	k1gMnSc2	Robert
a	a	k8xC	a
Thomase	Thomas	k1gMnSc2	Thomas
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
dožil	dožít	k5eAaPmAgMnS	dožít
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
federálního	federální	k2eAgInSc2d1	federální
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
hlavně	hlavně	k6eAd1	hlavně
projevem	projev	k1gInSc7	projev
proti	proti	k7c3	proti
Mexické	mexický	k2eAgFnSc3d1	mexická
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
prakticky	prakticky	k6eAd1	prakticky
skončila	skončit	k5eAaPmAgFnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
odmítal	odmítat	k5eAaImAgMnS	odmítat
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdálo	zdát	k5eAaImAgNnS	zdát
nespravedlivé	spravedlivý	k2eNgNnSc1d1	nespravedlivé
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
nadměrnou	nadměrný	k2eAgFnSc4d1	nadměrná
koncentraci	koncentrace	k1gFnSc4	koncentrace
moci	moct	k5eAaImF	moct
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímuž	jejíž	k3xOyRp3gNnSc3	jejíž
zesílení	zesílení	k1gNnSc3	zesílení
i	i	k8xC	i
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
přispěla	přispět	k5eAaPmAgFnS	přispět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
1848	[number]	k4	1848
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Zacharyho	Zachary	k1gMnSc4	Zachary
Taylora	Taylor	k1gMnSc4	Taylor
<g/>
,	,	kIx,	,
kandidáta	kandidát	k1gMnSc4	kandidát
Whig	whig	k1gMnSc1	whig
Party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
"	"	kIx"	"
<g/>
Kompromisu	kompromis	k1gInSc2	kompromis
1820	[number]	k4	1820
<g/>
"	"	kIx"	"
opět	opět	k6eAd1	opět
projednávala	projednávat	k5eAaImAgFnS	projednávat
otázka	otázka	k1gFnSc1	otázka
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
se	se	k3xPyFc4	se
domluvit	domluvit	k5eAaPmF	domluvit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
otrokářství	otrokářství	k1gNnSc1	otrokářství
zcela	zcela	k6eAd1	zcela
povoleno	povolit	k5eAaPmNgNnS	povolit
nebo	nebo	k8xC	nebo
navždy	navždy	k6eAd1	navždy
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
přirozeně	přirozeně	k6eAd1	přirozeně
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
otrokářství	otrokářství	k1gNnSc1	otrokářství
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vznesl	vznést	k5eAaPmAgMnS	vznést
i	i	k9	i
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
zákaz	zákaz	k1gInSc4	zákaz
otroctví	otroctví	k1gNnSc2	otroctví
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
distriktu	distrikt	k1gInSc6	distrikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
znepokojil	znepokojit	k5eAaPmAgInS	znepokojit
všechny	všechen	k3xTgMnPc4	všechen
otrokáře	otrokář	k1gMnPc4	otrokář
a	a	k8xC	a
po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
diskuzích	diskuze	k1gFnPc6	diskuze
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
předložen	předložit	k5eAaPmNgInS	předložit
k	k	k7c3	k
hlasování	hlasování	k1gNnSc3	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
otrokářství	otrokářství	k1gNnSc2	otrokářství
v	v	k7c6	v
USA	USA	kA	USA
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
významně	významně	k6eAd1	významně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
budoucí	budoucí	k2eAgFnSc4d1	budoucí
politickou	politický	k2eAgFnSc4d1	politická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
této	tento	k3xDgFnSc2	tento
krize	krize	k1gFnSc2	krize
neviděl	vidět	k5eNaImAgMnS	vidět
žádné	žádný	k3yNgNnSc4	žádný
východisko	východisko	k1gNnSc4	východisko
k	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgInS	odmítat
i	i	k9	i
radikální	radikální	k2eAgInPc4d1	radikální
zásahy	zásah	k1gInPc4	zásah
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
federativních	federativní	k2eAgInPc2d1	federativní
vládních	vládní	k2eAgInPc2d1	vládní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
znovu	znovu	k6eAd1	znovu
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
za	za	k7c4	za
skončenou	skončený	k2eAgFnSc4d1	skončená
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
advokátní	advokátní	k2eAgFnSc6d1	advokátní
praxi	praxe	k1gFnSc6	praxe
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
nevzdal	vzdát	k5eNaPmAgInS	vzdát
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
však	však	k9	však
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Severem	sever	k1gInSc7	sever
a	a	k8xC	a
Jihem	jih	k1gInSc7	jih
dále	daleko	k6eAd2	daleko
zostřovaly	zostřovat	k5eAaImAgFnP	zostřovat
a	a	k8xC	a
napětí	napětí	k1gNnSc1	napětí
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Mírové	mírový	k2eAgNnSc1d1	Mírové
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
se	se	k3xPyFc4	se
už	už	k6eAd1	už
zdálo	zdát	k5eAaImAgNnS	zdát
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nově	nově	k6eAd1	nově
přijímaných	přijímaný	k2eAgNnPc2d1	přijímané
teritorií	teritorium	k1gNnPc2	teritorium
sami	sám	k3xTgMnPc1	sám
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
otroctví	otroctví	k1gNnPc1	otroctví
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
či	či	k8xC	či
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
otrokářství	otrokářství	k1gNnSc2	otrokářství
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
i	i	k8xC	i
samotnou	samotný	k2eAgFnSc4d1	samotná
Whig	whig	k1gMnSc1	whig
Party	part	k1gInPc4	part
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
se	se	k3xPyFc4	se
odhodlal	odhodlat	k5eAaPmAgInS	odhodlat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
funkce	funkce	k1gFnSc2	funkce
senátora	senátor	k1gMnSc2	senátor
se	se	k3xPyFc4	se
nevydařil	vydařit	k5eNaPmAgMnS	vydařit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zásadně	zásadně	k6eAd1	zásadně
odmítavý	odmítavý	k2eAgInSc1d1	odmítavý
nekompromisní	kompromisní	k2eNgInSc1d1	nekompromisní
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
otrokářství	otrokářství	k1gNnSc6	otrokářství
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
proti	proti	k7c3	proti
demokratickému	demokratický	k2eAgMnSc3d1	demokratický
obhájci	obhájce	k1gMnSc3	obhájce
senátorského	senátorský	k2eAgNnSc2d1	senátorské
křesla	křeslo	k1gNnSc2	křeslo
Stephenu	Stephen	k1gMnSc3	Stephen
Douglasovi	Douglas	k1gMnSc3	Douglas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
nově	nově	k6eAd1	nově
přijímaných	přijímaný	k2eAgNnPc6d1	přijímané
teritoriích	teritorium	k1gNnPc6	teritorium
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
opět	opět	k6eAd1	opět
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zavilým	zavilý	k2eAgMnSc7d1	zavilý
odpůrcem	odpůrce	k1gMnSc7	odpůrce
otrokářství	otrokářství	k1gNnSc2	otrokářství
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
rovnost	rovnost	k1gFnSc4	rovnost
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gNnSc6	jeho
vyjádření	vyjádření	k1gNnSc6	vyjádření
mylná	mylný	k2eAgFnSc1d1	mylná
<g/>
:	:	kIx,	:
Otázka	otázka	k1gFnSc1	otázka
otroctví	otroctví	k1gNnSc2	otroctví
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
volebního	volební	k2eAgInSc2d1	volební
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
sice	sice	k8xC	sice
senátorské	senátorský	k2eAgFnPc4d1	senátorská
volby	volba	k1gFnPc4	volba
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Douglas	Douglas	k1gInSc1	Douglas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
1860	[number]	k4	1860
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
znásobily	znásobit	k5eAaPmAgFnP	znásobit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
volebním	volební	k2eAgInSc6d1	volební
sjezdu	sjezd	k1gInSc6	sjezd
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgMnS	konat
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
nominován	nominován	k2eAgInSc1d1	nominován
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Demokraté	demokrat	k1gMnPc1	demokrat
byli	být	k5eAaImAgMnP	být
natolik	natolik	k6eAd1	natolik
rozštěpení	rozštěpený	k2eAgMnPc1d1	rozštěpený
<g/>
,	,	kIx,	,
že	že	k8xS	že
nominovali	nominovat	k5eAaBmAgMnP	nominovat
kandidáty	kandidát	k1gMnPc7	kandidát
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc4	John
C.	C.	kA	C.
Breckenridge	Breckenridg	k1gMnSc4	Breckenridg
a	a	k8xC	a
Stephena	Stephen	k1gMnSc4	Stephen
A.	A.	kA	A.
Douglase	Douglas	k1gInSc6	Douglas
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
program	program	k1gInSc1	program
republikánů	republikán	k1gMnPc2	republikán
sliboval	slibovat	k5eAaImAgInS	slibovat
konec	konec	k1gInSc1	konec
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
ochranná	ochranný	k2eAgNnPc4d1	ochranné
cla	clo	k1gNnPc4	clo
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
udělování	udělování	k1gNnSc4	udělování
amerického	americký	k2eAgMnSc2d1	americký
státního	státní	k2eAgMnSc2d1	státní
občanství	občanství	k1gNnSc4	občanství
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
žádanou	žádaný	k2eAgFnSc7d1	žádaná
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Hannibal	Hannibal	k1gInSc1	Hannibal
Hamlin	Hamlin	k2eAgInSc1d1	Hamlin
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
smrti	smrt	k1gFnSc2	smrt
bojovníka	bojovník	k1gMnSc2	bojovník
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
otroků	otrok	k1gMnPc2	otrok
Johna	John	k1gMnSc4	John
Browna	Brown	k1gMnSc4	Brown
získala	získat	k5eAaPmAgFnS	získat
republikánská	republikánský	k2eAgFnSc1d1	republikánská
dvojice	dvojice	k1gFnSc1	dvojice
další	další	k2eAgInSc4d1	další
hlasy	hlas	k1gInPc4	hlas
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1860	[number]	k4	1860
obdržel	obdržet	k5eAaPmAgInS	obdržet
1	[number]	k4	1
865	[number]	k4	865
593	[number]	k4	593
hlasů	hlas	k1gInPc2	hlas
proti	proti	k7c3	proti
Douglasovým	Douglasův	k2eAgInPc3d1	Douglasův
1	[number]	k4	1
382	[number]	k4	382
713	[number]	k4	713
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
republikáni	republikán	k1gMnPc1	republikán
tak	tak	k6eAd1	tak
získali	získat	k5eAaPmAgMnP	získat
podporu	podpora	k1gFnSc4	podpora
40	[number]	k4	40
<g/>
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
180	[number]	k4	180
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
na	na	k7c6	na
kandidátkách	kandidátka	k1gFnPc6	kandidátka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
inaugurace	inaugurace	k1gFnSc2	inaugurace
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
schválil	schválit	k5eAaPmAgInS	schválit
Kongres	kongres	k1gInSc1	kongres
návrh	návrh	k1gInSc1	návrh
třináctého	třináctý	k4xOgInSc2	třináctý
ústavního	ústavní	k2eAgInSc2d1	ústavní
dodatku	dodatek	k1gInSc2	dodatek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
zaručit	zaručit	k5eAaPmF	zaručit
existenci	existence	k1gFnSc4	existence
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
neschválily	schválit	k5eNaPmAgFnP	schválit
a	a	k8xC	a
tak	tak	k6eAd1	tak
nikdy	nikdy	k6eAd1	nikdy
nevzešel	vzejít	k5eNaPmAgMnS	vzejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxem	paradox	k1gInSc7	paradox
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
ratifikovali	ratifikovat	k5eAaBmAgMnP	ratifikovat
nový	nový	k2eAgInSc4d1	nový
13	[number]	k4	13
<g/>
.	.	kIx.	.
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
netýkal	týkat	k5eNaImAgMnS	týkat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zachování	zachování	k1gNnPc4	zachování
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc4	jeho
ukončení	ukončení	k1gNnSc4	ukončení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
180	[number]	k4	180
mandáty	mandát	k1gInPc7	mandát
porazil	porazit	k5eAaPmAgMnS	porazit
Douglase	Douglasa	k1gFnSc3	Douglasa
i	i	k8xC	i
Breckinridge	Breckinridg	k1gInSc2	Breckinridg
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
dohromady	dohromady	k6eAd1	dohromady
pouze	pouze	k6eAd1	pouze
84	[number]	k4	84
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
státy	stát	k1gInPc1	stát
tento	tento	k3xDgInSc4	tento
výsledek	výsledek	k1gInSc1	výsledek
velmi	velmi	k6eAd1	velmi
znepokojil	znepokojit	k5eAaPmAgInS	znepokojit
<g/>
.	.	kIx.	.
</s>
<s>
Extrémisté	extrémista	k1gMnPc1	extrémista
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
k	k	k7c3	k
odtrhnutí	odtrhnutí	k1gNnSc3	odtrhnutí
od	od	k7c2	od
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
popíral	popírat	k5eAaImAgMnS	popírat
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
úmysl	úmysl	k1gInSc4	úmysl
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
odporu	odpor	k1gInSc2	odpor
však	však	k9	však
sílil	sílit	k5eAaImAgInS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
bojovného	bojovný	k2eAgInSc2d1	bojovný
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1860	[number]	k4	1860
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
z	z	k7c2	z
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Mississippi	Mississippi	k1gFnSc1	Mississippi
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
,	,	kIx,	,
Louisiana	Louisiana	k1gFnSc1	Louisiana
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
následovali	následovat	k5eAaImAgMnP	následovat
jejího	její	k3xOp3gInSc2	její
příkladu	příklad	k1gInSc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
Konfederativní	konfederativní	k2eAgInPc4d1	konfederativní
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
CSA	CSA	kA	CSA
<g/>
,	,	kIx,	,
Confederate	Confederat	k1gInSc5	Confederat
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jefferson	Jefferson	k1gMnSc1	Jefferson
Davis	Davis	k1gFnSc4	Davis
<g/>
,	,	kIx,	,
Jih	jih	k1gInSc4	jih
podporovala	podporovat	k5eAaImAgFnS	podporovat
Anglie	Anglie	k1gFnSc1	Anglie
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Sever	sever	k1gInSc1	sever
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgMnS	být
konkurencí	konkurence	k1gFnSc7	konkurence
a	a	k8xC	a
Jih	jih	k1gInSc1	jih
jim	on	k3xPp3gMnPc3	on
dodával	dodávat	k5eAaImAgInS	dodávat
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
severních	severní	k2eAgInPc2d1	severní
otrokářských	otrokářský	k2eAgInPc2d1	otrokářský
států	stát	k1gInPc2	stát
od	od	k7c2	od
Delaware	Delawar	k1gMnSc5	Delawar
až	až	k9	až
po	po	k7c4	po
Arkansas	Arkansas	k1gInSc4	Arkansas
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
nejprve	nejprve	k6eAd1	nejprve
vyčkávací	vyčkávací	k2eAgInSc4d1	vyčkávací
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
tak	tak	k6eAd1	tak
stál	stát	k5eAaImAgInS	stát
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
svého	svůj	k3xOyFgNnSc2	svůj
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
před	před	k7c7	před
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
hrozbou	hrozba	k1gFnSc7	hrozba
-	-	kIx~	-
rozpadem	rozpad	k1gInSc7	rozpad
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
s	s	k7c7	s
jižními	jižní	k2eAgInPc7d1	jižní
státy	stát	k1gInPc7	stát
skončily	skončit	k5eAaPmAgFnP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Jih	jih	k1gInSc1	jih
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
začal	začít	k5eAaPmAgMnS	začít
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
federální	federální	k2eAgFnSc4d1	federální
pevnost	pevnost	k1gFnSc4	pevnost
Sumter	Sumter	k1gMnSc1	Sumter
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgMnSc1d1	ležící
nedaleko	nedaleko	k7c2	nedaleko
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižanského	jižanský	k2eAgInSc2d1	jižanský
Charlestonu	charleston	k1gInSc2	charleston
<g/>
,	,	kIx,	,
krvavá	krvavý	k2eAgFnSc1d1	krvavá
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
naplno	naplno	k6eAd1	naplno
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
<g/>
.	.	kIx.	.
</s>
<s>
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
ke	k	k7c3	k
Konfederaci	konfederace	k1gFnSc3	konfederace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Maryland	Maryland	k1gInSc1	Maryland
<g/>
,	,	kIx,	,
Kentucky	Kentuck	k1gInPc1	Kentuck
<g/>
,	,	kIx,	,
Delaware	Delawar	k1gMnSc5	Delawar
a	a	k8xC	a
Missouri	Missouri	k1gFnPc1	Missouri
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
Unii	unie	k1gFnSc6	unie
a	a	k8xC	a
pokusily	pokusit	k5eAaPmAgFnP	pokusit
se	se	k3xPyFc4	se
zůstat	zůstat	k5eAaPmF	zůstat
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
zuřila	zuřit	k5eAaImAgFnS	zuřit
lokální	lokální	k2eAgFnSc1d1	lokální
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
příznivci	příznivec	k1gMnPc7	příznivec
a	a	k8xC	a
odpůrci	odpůrce	k1gMnPc1	odpůrce
otrokářství	otrokářství	k1gNnSc2	otrokářství
<g/>
.	.	kIx.	.
</s>
<s>
Konfederace	konfederace	k1gFnSc1	konfederace
přesunula	přesunout	k5eAaPmAgFnS	přesunout
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
do	do	k7c2	do
Richmondu	Richmond	k1gInSc2	Richmond
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
150	[number]	k4	150
km	km	kA	km
od	od	k7c2	od
Washingtonu	Washington	k1gInSc2	Washington
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
významná	významný	k2eAgFnSc1d1	významná
bitva	bitva	k1gFnSc1	bitva
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Bull	bulla	k1gFnPc2	bulla
Run	Runa	k1gFnPc2	Runa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stálo	stát	k5eAaImAgNnS	stát
35	[number]	k4	35
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
Unie	unie	k1gFnSc2	unie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Irvina	Irvin	k1gMnSc2	Irvin
McDowella	McDowella	k1gMnSc1	McDowella
a	a	k8xC	a
20	[number]	k4	20
000	[number]	k4	000
Konfederačních	konfederační	k2eAgMnPc2d1	konfederační
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Beauregardem	Beauregard	k1gMnSc7	Beauregard
<g/>
.	.	kIx.	.
</s>
<s>
Unionistické	unionistický	k2eAgInPc1d1	unionistický
oddíly	oddíl	k1gInPc1	oddíl
utrpěly	utrpět	k5eAaPmAgInP	utrpět
bolestnou	bolestný	k2eAgFnSc4d1	bolestná
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
státy	stát	k1gInPc4	stát
Unie	unie	k1gFnSc2	unie
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
šok	šok	k1gInSc1	šok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jih	jih	k1gInSc1	jih
se	se	k3xPyFc4	se
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
na	na	k7c6	na
Severu	sever	k1gInSc6	sever
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
několikanásobná	několikanásobný	k2eAgFnSc1d1	několikanásobná
změna	změna	k1gFnSc1	změna
armádního	armádní	k2eAgNnSc2d1	armádní
velení	velení	k1gNnSc2	velení
nepřinesla	přinést	k5eNaPmAgFnS	přinést
Severu	sever	k1gInSc2	sever
žádný	žádný	k3yNgInSc4	žádný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
generál	generál	k1gMnSc1	generál
Jihu	jih	k1gInSc2	jih
Lee	Lea	k1gFnSc6	Lea
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
jednotkami	jednotka	k1gFnPc7	jednotka
úspěšně	úspěšně	k6eAd1	úspěšně
pronikl	proniknout	k5eAaPmAgInS	proniknout
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
trvala	trvat	k5eAaImAgFnS	trvat
4	[number]	k4	4
dny	den	k1gInPc4	den
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Meada	Mead	k1gMnSc2	Mead
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
konečně	konečně	k6eAd1	konečně
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vítězství	vítězství	k1gNnSc1	vítězství
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
důležité	důležitý	k2eAgNnSc1d1	důležité
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vojenského	vojenský	k2eAgNnSc2d1	vojenské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morálního	morální	k2eAgNnSc2d1	morální
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
a	a	k8xC	a
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
armádním	armádní	k2eAgMnSc7d1	armádní
velitelem	velitel	k1gMnSc7	velitel
Unie	unie	k1gFnSc2	unie
generála	generál	k1gMnSc4	generál
Ulyssa	Ulyss	k1gMnSc4	Ulyss
Granta	Grant	k1gMnSc4	Grant
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
toho	ten	k3xDgNnSc2	ten
roku	rok	k1gInSc2	rok
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
vyčerpávaly	vyčerpávat	k5eAaImAgFnP	vyčerpávat
Konfederativní	konfederativní	k2eAgFnSc4d1	konfederativní
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
akcemi	akce	k1gFnPc7	akce
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
podnikl	podniknout	k5eAaPmAgMnS	podniknout
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
několik	několik	k4yIc4	několik
jednání	jednání	k1gNnPc2	jednání
o	o	k7c6	o
kompromisním	kompromisní	k2eAgInSc6d1	kompromisní
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
zastavit	zastavit	k5eAaPmF	zastavit
boje	boj	k1gInPc4	boj
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgInPc1d1	jižní
státy	stát	k1gInPc1	stát
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
uznají	uznat	k5eAaPmIp3nP	uznat
osvobození	osvobození	k1gNnSc4	osvobození
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
přikláněl	přiklánět	k5eAaImAgMnS	přiklánět
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
otroctví	otroctví	k1gNnSc2	otroctví
úplně	úplně	k6eAd1	úplně
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
několik	několik	k4yIc4	několik
dobrých	dobrý	k2eAgInPc2d1	dobrý
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
práce	práce	k1gFnSc2	práce
otroků	otrok	k1gMnPc2	otrok
podporovala	podporovat	k5eAaImAgFnS	podporovat
vojenské	vojenský	k2eAgNnSc4d1	vojenské
úsilí	úsilí	k1gNnSc4	úsilí
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
upadající	upadající	k2eAgFnPc1d1	upadající
morálka	morálka	k1gFnSc1	morálka
občanů	občan	k1gMnPc2	občan
Severu	sever	k1gInSc2	sever
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
povzbudit	povzbudit	k5eAaPmF	povzbudit
nějakým	nějaký	k3yIgInSc7	nějaký
vznešeným	vznešený	k2eAgInSc7d1	vznešený
mravním	mravní	k2eAgInSc7d1	mravní
ideálem	ideál	k1gInSc7	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
otrokářství	otrokářství	k1gNnSc2	otrokářství
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
také	také	k9	také
opravdu	opravdu	k6eAd1	opravdu
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
nemluvě	nemluva	k1gFnSc6	nemluva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovýto	takovýto	k3xDgInSc1	takovýto
postup	postup	k1gInSc1	postup
by	by	kYmCp3nS	by
zmařil	zmařit	k5eAaPmAgInS	zmařit
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
Konfederace	konfederace	k1gFnSc2	konfederace
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
podpory	podpora	k1gFnSc2	podpora
Francie	Francie	k1gFnSc2	Francie
či	či	k8xC	či
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Jefferson	Jefferson	k1gMnSc1	Jefferson
Davis	Davis	k1gFnSc4	Davis
však	však	k9	však
tyto	tento	k3xDgInPc4	tento
návrhy	návrh	k1gInPc4	návrh
odmítal	odmítat	k5eAaImAgInS	odmítat
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stát	stát	k5eAaImF	stát
svědkem	svědek	k1gMnSc7	svědek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
zaháněna	zahánět	k5eAaImNgFnS	zahánět
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
do	do	k7c2	do
úzkých	úzké	k1gInPc2	úzké
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Robert	Robert	k1gMnSc1	Robert
Edward	Edward	k1gMnSc1	Edward
Lee	Lea	k1gFnSc3	Lea
donucen	donutit	k5eAaPmNgMnS	donutit
u	u	k7c2	u
Appomattoxu	Appomattox	k1gInSc2	Appomattox
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
i	i	k9	i
americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
de	de	k?	de
facto	facto	k1gNnSc1	facto
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
poslední	poslední	k2eAgFnPc1d1	poslední
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
600	[number]	k4	600
000	[number]	k4	000
mrtvými	mrtvý	k1gMnPc7	mrtvý
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
nejničivější	ničivý	k2eAgFnSc1d3	nejničivější
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Materiální	materiální	k2eAgFnPc4d1	materiální
škody	škoda	k1gFnPc4	škoda
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vůbec	vůbec	k9	vůbec
vyčíslit	vyčíslit	k5eAaPmF	vyčíslit
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
války	válka	k1gFnSc2	válka
-	-	kIx~	-
problém	problém	k1gInSc1	problém
otroctví	otroctví	k1gNnSc2	otroctví
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
vyhranou	vyhraný	k2eAgFnSc7d1	vyhraná
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
vyřešen	vyřešit	k5eAaPmNgMnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Antietamu	Antietam	k1gInSc2	Antietam
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1862	[number]	k4	1862
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1863	[number]	k4	1863
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
územích	území	k1gNnPc6	území
obsazených	obsazený	k2eAgInPc2d1	obsazený
vojsky	vojsky	k6eAd1	vojsky
Unie	unie	k1gFnSc2	unie
nahlížet	nahlížet	k5eAaImF	nahlížet
na	na	k7c4	na
otroky	otrok	k1gMnPc4	otrok
jako	jako	k8xC	jako
na	na	k7c4	na
svobodné	svobodný	k2eAgMnPc4d1	svobodný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
však	však	k9	však
nemalou	malý	k2eNgFnSc7d1	nemalá
mírou	míra	k1gFnSc7	míra
přispěl	přispět	k5eAaPmAgInS	přispět
i	i	k9	i
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
vítězství	vítězství	k1gNnSc3	vítězství
Unie	unie	k1gFnSc2	unie
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
černoši	černoch	k1gMnPc1	černoch
mohli	moct	k5eAaImAgMnP	moct
hlásit	hlásit	k5eAaImF	hlásit
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
výraznou	výrazný	k2eAgFnSc7d1	výrazná
mírou	míra	k1gFnSc7	míra
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
složení	složení	k1gNnSc4	složení
unionistické	unionistický	k2eAgFnSc2d1	unionistická
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
jich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sloužilo	sloužit	k5eAaImAgNnS	sloužit
téměř	téměř	k6eAd1	téměř
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
Preliminary	Preliminar	k1gInPc4	Preliminar
Proclamation	Proclamation	k1gInSc1	Proclamation
(	(	kIx(	(
<g/>
předběžnému	předběžný	k2eAgNnSc3d1	předběžné
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
<g/>
)	)	kIx)	)
předcházel	předcházet	k5eAaImAgInS	předcházet
složitý	složitý	k2eAgInSc1d1	složitý
proces	proces	k1gInSc1	proces
sebeuvědomování	sebeuvědomování	k1gNnSc2	sebeuvědomování
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
hlavní	hlavní	k2eAgInPc1d1	hlavní
cíl	cíl	k1gInSc1	cíl
války	válka	k1gFnSc2	válka
viděl	vidět	k5eAaImAgInS	vidět
v	v	k7c6	v
zachování	zachování	k1gNnSc6	zachování
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
osvobození	osvobození	k1gNnSc3	osvobození
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
konečnému	konečný	k2eAgNnSc3d1	konečné
zrušení	zrušení	k1gNnSc3	zrušení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
s	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
dodatkem	dodatek	k1gInSc7	dodatek
k	k	k7c3	k
Ústavě	ústava	k1gFnSc3	ústava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
přijetí	přijetí	k1gNnSc1	přijetí
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedožil	dožít	k5eNaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
Emancipační	emancipační	k2eAgNnPc4d1	emancipační
prohlášení	prohlášení	k1gNnPc4	prohlášení
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1862	[number]	k4	1862
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
zahraničněpolitické	zahraničněpolitický	k2eAgFnSc6d1	zahraničněpolitická
pozici	pozice	k1gFnSc6	pozice
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
sympatizující	sympatizující	k2eAgFnSc1d1	sympatizující
s	s	k7c7	s
Jihem	jih	k1gInSc7	jih
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
nemohly	moct	k5eNaImAgFnP	moct
otevřeně	otevřeně	k6eAd1	otevřeně
postavit	postavit	k5eAaPmF	postavit
za	za	k7c4	za
Konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
veřejná	veřejný	k2eAgFnSc1d1	veřejná
podpora	podpora	k1gFnSc1	podpora
otroctví	otroctví	k1gNnSc2	otroctví
byla	být	k5eAaImAgFnS	být
politicky	politicky	k6eAd1	politicky
neúnosná	únosný	k2eNgFnSc1d1	neúnosná
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
tak	tak	k9	tak
zažehnala	zažehnat	k5eAaPmAgFnS	zažehnat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
zásahu	zásah	k1gInSc2	zásah
těchto	tento	k3xDgFnPc2	tento
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
otevření	otevření	k1gNnSc4	otevření
národního	národní	k2eAgInSc2d1	národní
hřbitova	hřbitov	k1gInSc2	hřbitov
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
rozhodujících	rozhodující	k2eAgFnPc2d1	rozhodující
bitev	bitva	k1gFnPc2	bitva
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
přednesl	přednést	k5eAaPmAgMnS	přednést
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
kromě	kromě	k7c2	kromě
jiného	jiný	k1gMnSc2	jiný
pravil	pravit	k5eAaImAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
mrtví	mrtvý	k1gMnPc1	mrtvý
nezemřeli	zemřít	k5eNaPmAgMnP	zemřít
nadarmo	nadarmo	k6eAd1	nadarmo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
13	[number]	k4	13
<g/>
.	.	kIx.	.
ústavní	ústavní	k2eAgInSc1d1	ústavní
dodatek	dodatek	k1gInSc1	dodatek
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc1	otroctví
definitivně	definitivně	k6eAd1	definitivně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
schválily	schválit	k5eAaPmAgFnP	schválit
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
státních	státní	k2eAgNnPc2d1	státní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1865	[number]	k4	1865
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
otroctví	otroctví	k1gNnSc1	otroctví
přetrvávalo	přetrvávat	k5eAaImAgNnS	přetrvávat
jen	jen	k9	jen
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
pohraničních	pohraniční	k2eAgInPc6d1	pohraniční
státech	stát	k1gInPc6	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
Delaware	Delawar	k1gInSc5	Delawar
<g/>
.	.	kIx.	.
</s>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Začáteční	začáteční	k2eAgFnPc1d1	začáteční
těžké	těžký	k2eAgFnPc1d1	těžká
porážky	porážka	k1gFnPc1	porážka
unionistických	unionistický	k2eAgFnPc2d1	unionistická
jednotek	jednotka	k1gFnPc2	jednotka
nezůstaly	zůstat	k5eNaPmAgFnP	zůstat
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
Demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
za	za	k7c4	za
poslance	poslanec	k1gMnPc4	poslanec
Demokraty	demokrat	k1gMnPc4	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
demokratických	demokratický	k2eAgInPc2d1	demokratický
mandátů	mandát	k1gInPc2	mandát
z	z	k7c2	z
24	[number]	k4	24
na	na	k7c4	na
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
si	se	k3xPyFc3	se
i	i	k9	i
za	za	k7c2	za
války	válka	k1gFnSc2	válka
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
promluví	promluvit	k5eAaPmIp3nP	promluvit
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nP	pokusit
se	se	k3xPyFc4	se
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
potom	potom	k6eAd1	potom
denně	denně	k6eAd1	denně
obléhali	obléhat	k5eAaImAgMnP	obléhat
návštěvníci	návštěvník	k1gMnPc1	návštěvník
snažíce	snažit	k5eAaImSgFnP	snažit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
nějaké	nějaký	k3yIgNnSc4	nějaký
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
poradit	poradit	k5eAaPmF	poradit
prezidentovi	prezident	k1gMnSc3	prezident
anebo	anebo	k8xC	anebo
mu	on	k3xPp3gMnSc3	on
aspoň	aspoň	k9	aspoň
poskytnout	poskytnout	k5eAaPmF	poskytnout
slovní	slovní	k2eAgFnSc4d1	slovní
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
nelehkém	lehký	k2eNgNnSc6d1	nelehké
postavení	postavení	k1gNnSc6	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Demokraté	demokrat	k1gMnPc1	demokrat
mu	on	k3xPp3gInSc3	on
především	především	k9	především
vyčítali	vyčítat	k5eAaImAgMnP	vyčítat
počáteční	počáteční	k2eAgFnSc4d1	počáteční
váhavost	váhavost	k1gFnSc4	váhavost
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
všichni	všechen	k3xTgMnPc1	všechen
Republikáni	republikán	k1gMnPc1	republikán
nebyli	být	k5eNaImAgMnP	být
zajedno	zajedno	k6eAd1	zajedno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
radikálové	radikál	k1gMnPc1	radikál
nominovali	nominovat	k5eAaBmAgMnP	nominovat
vlastního	vlastní	k2eAgMnSc4d1	vlastní
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Charlese	Charles	k1gMnSc2	Charles
Frémonta	Frémont	k1gMnSc2	Frémont
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
sjezd	sjezd	k1gInSc1	sjezd
však	však	k9	však
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c4	v
Baltimore	Baltimore	k1gInSc4	Baltimore
znovu	znovu	k6eAd1	znovu
nominovat	nominovat	k5eAaBmF	nominovat
Lincolna	Lincoln	k1gMnSc4	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Demokraté	demokrat	k1gMnPc1	demokrat
nominovali	nominovat	k5eAaBmAgMnP	nominovat
generála	generál	k1gMnSc4	generál
McClellana	McClellan	k1gMnSc4	McClellan
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
postavil	postavit	k5eAaPmAgInS	postavit
i	i	k9	i
lid	lid	k1gInSc1	lid
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vítězství	vítězství	k1gNnSc2	vítězství
poměrem	poměr	k1gInSc7	poměr
212	[number]	k4	212
:	:	kIx,	:
21	[number]	k4	21
mandátem	mandát	k1gInSc7	mandát
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoznačným	jednoznačný	k2eAgNnSc7d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
mu	on	k3xPp3gMnSc3	on
dalo	dát	k5eAaPmAgNnS	dát
i	i	k9	i
77,5	[number]	k4	77,5
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
vojáků	voják	k1gMnPc2	voják
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1864	[number]	k4	1864
byl	být	k5eAaImAgInS	být
znovuuveden	znovuuveden	k2eAgInSc1d1	znovuuveden
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Inaugurace	inaugurace	k1gFnSc1	inaugurace
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
část	část	k1gFnSc1	část
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgNnPc4d1	občanské
sdružení	sdružení	k1gNnPc4	sdružení
a	a	k8xC	a
černošský	černošský	k2eAgInSc4d1	černošský
armádní	armádní	k2eAgInSc4d1	armádní
prapor	prapor	k1gInSc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
zaobíral	zaobírat	k5eAaImAgMnS	zaobírat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
Jihu	jih	k1gInSc2	jih
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
předložil	předložit	k5eAaPmAgInS	předložit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
návrh	návrh	k1gInSc4	návrh
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
jihu	jih	k1gInSc2	jih
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přednesl	přednést	k5eAaPmAgMnS	přednést
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
jižanské	jižanský	k2eAgFnSc2d1	jižanská
kapitulace	kapitulace	k1gFnSc2	kapitulace
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
základní	základní	k2eAgFnSc2d1	základní
linie	linie	k1gFnSc2	linie
budoucí	budoucí	k2eAgFnSc2d1	budoucí
politiky	politika	k1gFnSc2	politika
<g/>
:	:	kIx,	:
ohleduplnost	ohleduplnost	k1gFnSc1	ohleduplnost
vůči	vůči	k7c3	vůči
Jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
možno	možno	k6eAd1	možno
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
vytvoření	vytvoření	k1gNnSc4	vytvoření
vlastních	vlastní	k2eAgFnPc2d1	vlastní
vlád	vláda	k1gFnPc2	vláda
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgMnPc2	který
však	však	k9	však
očekával	očekávat	k5eAaImAgMnS	očekávat
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
prohlášení	prohlášení	k1gNnSc4	prohlášení
loajality	loajalita	k1gFnSc2	loajalita
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
možno	možno	k6eAd1	možno
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
začlenění	začlenění	k1gNnSc4	začlenění
povstalců	povstalec	k1gMnPc2	povstalec
do	do	k7c2	do
civilního	civilní	k2eAgInSc2d1	civilní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
navrácením	navrácení	k1gNnSc7	navrácení
veškerých	veškerý	k3xTgNnPc2	veškerý
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sveřepého	sveřepý	k2eAgMnSc4d1	sveřepý
zastánce	zastánce	k1gMnSc4	zastánce
Ústavy	ústava	k1gFnSc2	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
pak	pak	k6eAd1	pak
odpůrce	odpůrce	k1gMnSc2	odpůrce
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
omezení	omezení	k1gNnSc4	omezení
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
bodech	bod	k1gInPc6	bod
či	či	k8xC	či
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
let	let	k1gInSc4	let
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Kongres	kongres	k1gInSc1	kongres
mohl	moct	k5eAaImAgInS	moct
vydávat	vydávat	k5eAaImF	vydávat
státní	státní	k2eAgFnPc4d1	státní
bankovky	bankovka	k1gFnPc4	bankovka
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
peněz	peníze	k1gInPc2	peníze
s	s	k7c7	s
nuceným	nucený	k2eAgInSc7d1	nucený
oběhem	oběh	k1gInSc7	oběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
financoval	financovat	k5eAaBmAgInS	financovat
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
náklady	náklad	k1gInPc4	náklad
vojsk	vojsko	k1gNnPc2	vojsko
Severu	sever	k1gInSc2	sever
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
tyto	tento	k3xDgFnPc4	tento
bankovky	bankovka	k1gFnPc4	bankovka
(	(	kIx(	(
<g/>
tištěné	tištěný	k2eAgFnPc4d1	tištěná
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
zeleným	zelený	k2eAgInSc7d1	zelený
inkoustem	inkoust	k1gInSc7	inkoust
<g/>
,	,	kIx,	,
od	od	k7c2	od
čehož	což	k3yQnSc2	což
přijaly	přijmout	k5eAaPmAgInP	přijmout
označení	označení	k1gNnSc4	označení
green	grena	k1gFnPc2	grena
backs	backs	k6eAd1	backs
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
prostoupily	prostoupit	k5eAaPmAgInP	prostoupit
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
severských	severský	k2eAgInPc2d1	severský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
emise	emise	k1gFnPc4	emise
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
ražení	ražení	k1gNnSc1	ražení
<g/>
)	)	kIx)	)
hotových	hotový	k2eAgInPc2d1	hotový
peněz	peníze	k1gInPc2	peníze
měla	mít	k5eAaImAgFnS	mít
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
Kongres	kongres	k1gInSc1	kongres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc1	tento
samotné	samotný	k2eAgNnSc1d1	samotné
vydávání	vydávání	k1gNnSc1	vydávání
peněz	peníze	k1gInPc2	peníze
nebylo	být	k5eNaImAgNnS	být
zatíženo	zatížit	k5eAaPmNgNnS	zatížit
úrokem	úrok	k1gInSc7	úrok
<g/>
;	;	kIx,	;
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
zpřetrhána	zpřetrhán	k2eAgFnSc1d1	zpřetrhána
možnost	možnost	k1gFnSc1	možnost
lidu	lid	k1gInSc2	lid
volit	volit	k5eAaImF	volit
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c4	o
vydávání	vydávání	k1gNnSc4	vydávání
peněz	peníze	k1gInPc2	peníze
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
starali	starat	k5eAaImAgMnP	starat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
princip	princip	k1gInSc1	princip
peněz	peníze	k1gInPc2	peníze
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
veřejným	veřejný	k2eAgFnPc3d1	veřejná
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
soukromým	soukromý	k2eAgInSc7d1	soukromý
subjektem	subjekt	k1gInSc7	subjekt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
za	za	k7c2	za
něj	on	k3xPp3gInSc2	on
účtoval	účtovat	k5eAaImAgInS	účtovat
úrok	úrok	k1gInSc1	úrok
<g/>
,	,	kIx,	,
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
Woodrowa	Woodrowus	k1gMnSc2	Woodrowus
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
Kongresu	kongres	k1gInSc2	kongres
byly	být	k5eAaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
papírové	papírový	k2eAgFnPc1d1	papírová
bankovky	bankovka	k1gFnPc1	bankovka
vytištěny	vytisknout	k5eAaPmNgFnP	vytisknout
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
450	[number]	k4	450
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zpočátku	zpočátku	k6eAd1	zpočátku
postačovalo	postačovat	k5eAaImAgNnS	postačovat
ke	k	k7c3	k
krytí	krytí	k1gNnSc3	krytí
válečných	válečný	k2eAgFnPc2d1	válečná
ztrát	ztráta	k1gFnPc2	ztráta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bezbřehé	bezbřehý	k2eAgFnSc3d1	bezbřehá
inflaci	inflace	k1gFnSc3	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
války	válka	k1gFnSc2	válka
však	však	k9	však
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
rozpočtu	rozpočet	k1gInSc2	rozpočet
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
dát	dát	k5eAaPmF	dát
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
cenné	cenný	k2eAgInPc4d1	cenný
papíry	papír	k1gInPc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
akcí	akce	k1gFnSc7	akce
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
bankéř	bankéř	k1gMnSc1	bankéř
Jay	Jay	k1gMnSc1	Jay
Cooke	Cooke	k1gFnSc1	Cooke
z	z	k7c2	z
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
prodat	prodat	k5eAaPmF	prodat
soukromým	soukromý	k2eAgFnPc3d1	soukromá
osobám	osoba	k1gFnPc3	osoba
vládní	vládní	k2eAgFnSc2d1	vládní
válečné	válečná	k1gFnSc2	válečná
papíry	papír	k1gInPc1	papír
za	za	k7c7	za
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
se	s	k7c7	s
splatností	splatnost	k1gFnSc7	splatnost
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
výsledek	výsledek	k1gInSc1	výsledek
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
takto	takto	k6eAd1	takto
získaných	získaný	k2eAgInPc2d1	získaný
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vykonával	vykonávat	k5eAaImAgInS	vykonávat
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nově	nově	k6eAd1	nově
zavedena	zaveden	k2eAgFnSc1d1	zavedena
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
příslušný	příslušný	k2eAgInSc1d1	příslušný
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
orgán	orgán	k1gInSc1	orgán
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
daňový	daňový	k2eAgInSc4d1	daňový
příjem	příjem	k1gInSc4	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
významných	významný	k2eAgInPc2d1	významný
zákonů	zákon	k1gInPc2	zákon
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
hospodářství	hospodářství	k1gNnSc2	hospodářství
přijal	přijmout	k5eAaPmAgInS	přijmout
Kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
ochranném	ochranný	k2eAgNnSc6d1	ochranné
clu	clo	k1gNnSc6	clo
<g/>
,	,	kIx,	,
o	o	k7c6	o
transkontinentální	transkontinentální	k2eAgFnSc6d1	transkontinentální
železnici	železnice	k1gFnSc6	železnice
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
usedlostech	usedlost	k1gFnPc6	usedlost
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
mohli	moct	k5eAaImAgMnP	moct
osadníci	osadník	k1gMnPc1	osadník
získat	získat	k5eAaPmF	získat
bezplatně	bezplatně	k6eAd1	bezplatně
160	[number]	k4	160
akrů	akr	k1gInPc2	akr
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Government	Government	k1gInSc1	Government
of	of	k?	of
the	the	k?	the
people	people	k6eAd1	people
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
the	the	k?	the
people	people	k6eAd1	people
<g/>
,	,	kIx,	,
for	forum	k1gNnPc2	forum
the	the	k?	the
people	people	k6eAd1	people
<g/>
,	,	kIx,	,
shall	shall	k1gMnSc1	shall
not	nota	k1gFnPc2	nota
perish	perish	k1gMnSc1	perish
from	from	k1gMnSc1	from
the	the	k?	the
Earth	Earth	k1gMnSc1	Earth
<g/>
.	.	kIx.	.
</s>
<s>
You	You	k?	You
can	can	k?	can
fool	fool	k1gInSc1	fool
all	all	k?	all
the	the	k?	the
people	people	k6eAd1	people
some	somat	k5eAaPmIp3nS	somat
of	of	k?	of
the	the	k?	the
time	time	k1gInSc1	time
<g/>
,	,	kIx,	,
and	and	k?	and
some	some	k1gInSc1	some
of	of	k?	of
the	the	k?	the
people	people	k6eAd1	people
all	all	k?	all
the	the	k?	the
time	time	k1gInSc1	time
<g/>
,	,	kIx,	,
but	but	k?	but
you	you	k?	you
cannot	cannota	k1gFnPc2	cannota
fool	fool	k1gMnSc1	fool
all	all	k?	all
the	the	k?	the
people	people	k6eAd1	people
all	all	k?	all
the	the	k?	the
time	time	k1gInSc1	time
<g/>
.	.	kIx.	.
</s>
<s>
Be	Be	k?	Be
sure	surat	k5eAaPmIp3nS	surat
you	you	k?	you
put	puta	k1gFnPc2	puta
your	your	k1gMnSc1	your
feet	feet	k1gMnSc1	feet
in	in	k?	in
the	the	k?	the
right	right	k1gInSc1	right
place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
then	then	k1gMnSc1	then
stand	stand	k?	stand
firm	firm	k1gMnSc1	firm
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
slow	slow	k?	slow
walker	walker	k1gInSc1	walker
<g/>
,	,	kIx,	,
but	but	k?	but
I	i	k8xC	i
never	never	k1gMnSc1	never
walk	walk	k1gMnSc1	walk
back	back	k1gMnSc1	back
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Abrahama	Abraham	k1gMnSc4	Abraham
Lincolna	Lincoln	k1gMnSc4	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naplnění	naplnění	k1gNnSc4	naplnění
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
už	už	k6eAd1	už
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
představení	představení	k1gNnSc4	představení
ve	v	k7c6	v
Fordově	Fordův	k2eAgNnSc6d1	Fordovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgMnSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
fanatickým	fanatický	k2eAgMnSc7d1	fanatický
přívržencem	přívrženec	k1gMnSc7	přívrženec
Jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
lóže	lóže	k1gFnSc2	lóže
a	a	k8xC	a
z	z	k7c2	z
těsné	těsný	k2eAgFnSc2d1	těsná
blízkosti	blízkost	k1gFnSc2	blízkost
prezidenta	prezident	k1gMnSc2	prezident
střelil	střelit	k5eAaPmAgMnS	střelit
zezadu	zezadu	k6eAd1	zezadu
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nastal	nastat	k5eAaPmAgInS	nastat
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
atentátník	atentátník	k1gMnSc1	atentátník
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
John	John	k1gMnSc1	John
Wilkes	Wilkes	k1gMnSc1	Wilkes
Booth	Booth	k1gMnSc1	Booth
<g/>
,	,	kIx,	,
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
za	za	k7c4	za
dvanáct	dvanáct	k4xCc4	dvanáct
dní	den	k1gInPc2	den
ho	on	k3xPp3gMnSc4	on
vojáci	voják	k1gMnPc1	voják
chytili	chytit	k5eAaPmAgMnP	chytit
a	a	k8xC	a
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
všemožné	všemožný	k2eAgFnSc3d1	všemožná
lékařské	lékařský	k2eAgFnSc3d1	lékařská
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
atentát	atentát	k1gInSc4	atentát
byl	být	k5eAaImAgMnS	být
spiknutím	spiknutí	k1gNnSc7	spiknutí
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Bootha	Booth	k1gMnSc2	Booth
podílelo	podílet	k5eAaImAgNnS	podílet
ještě	ještě	k9	ještě
dalších	další	k2eAgNnPc2d1	další
devět	devět	k4xCc1	devět
atentátníků	atentátník	k1gMnPc2	atentátník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
přívrženci	přívrženec	k1gMnPc7	přívrženec
Jihu	jih	k1gInSc2	jih
a	a	k8xC	a
jednali	jednat	k5eAaImAgMnP	jednat
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Lincolna	Lincoln	k1gMnSc2	Lincoln
chtěli	chtít	k5eAaImAgMnP	chtít
zabít	zabít	k5eAaPmF	zabít
Granta	Grant	k1gMnSc4	Grant
<g/>
,	,	kIx,	,
Johnsona	Johnson	k1gMnSc4	Johnson
a	a	k8xC	a
Sewarda	Seward	k1gMnSc4	Seward
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
státního	státní	k2eAgMnSc4d1	státní
sekretáře	sekretář	k1gMnSc4	sekretář
(	(	kIx(	(
<g/>
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připoutaného	připoutaný	k2eAgInSc2d1	připoutaný
na	na	k7c6	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spáchán	spáchán	k2eAgInSc4d1	spáchán
atentát	atentát	k1gInSc4	atentát
nožem	nůž	k1gInSc7	nůž
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
zranění	zranění	k1gNnSc1	zranění
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
spiklenců	spiklenec	k1gMnPc2	spiklenec
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgMnSc1	jeden
zemřel	zemřít	k5eAaPmAgInS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
narozeniny	narozeniny	k1gFnPc1	narozeniny
v	v	k7c6	v
USA	USA	kA	USA
slaví	slavit	k5eAaImIp3nS	slavit
jako	jako	k9	jako
národní	národní	k2eAgInSc4d1	národní
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
připomínaly	připomínat	k5eAaImAgFnP	připomínat
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zachránil	zachránit	k5eAaPmAgInS	zachránit
jednotu	jednota	k1gFnSc4	jednota
amerického	americký	k2eAgInSc2d1	americký
národa	národ	k1gInSc2	národ
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
každého	každý	k3xTgMnSc4	každý
podmanit	podmanit	k5eAaPmF	podmanit
svou	svůj	k3xOyFgFnSc7	svůj
prostotou	prostota	k1gFnSc7	prostota
<g/>
,	,	kIx,	,
prostotou	prostota	k1gFnSc7	prostota
významného	významný	k2eAgMnSc2d1	významný
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
protivil	protivit	k5eAaImAgInS	protivit
každý	každý	k3xTgInSc4	každý
kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k8xC	jako
službu	služba	k1gFnSc4	služba
národu	národ	k1gInSc2	národ
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
blahu	blaho	k1gNnSc3	blaho
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
neustále	neustále	k6eAd1	neustále
trápily	trápit	k5eAaImAgFnP	trápit
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
udělal	udělat	k5eAaPmAgMnS	udělat
správné	správný	k2eAgNnSc4d1	správné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nechával	nechávat	k5eAaImAgMnS	nechávat
vést	vést	k5eAaImF	vést
lidskostí	lidskost	k1gFnSc7	lidskost
a	a	k8xC	a
slušností	slušnost	k1gFnSc7	slušnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
natrvalo	natrvalo	k6eAd1	natrvalo
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
