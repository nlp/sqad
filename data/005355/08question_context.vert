<s>
Kořen	kořen	k1gInSc1
v	v	k7c6
lingvistice	lingvistika	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
jednotlivý	jednotlivý	k2eAgInSc4d1
morfém	morfém	k1gInSc4
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1
nese	nést	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>