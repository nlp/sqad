<s>
Kořen	kořen	k1gInSc1	kořen
v	v	k7c6	v
lingvistice	lingvistika	k1gFnSc6	lingvistika
označuje	označovat	k5eAaImIp3nS	označovat
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
morfém	morfém	k1gInSc4	morfém
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nese	nést	k5eAaImIp3nS	nést
základní	základní	k2eAgInSc4d1	základní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
ho	on	k3xPp3gInSc4	on
dále	daleko	k6eAd2	daleko
členit	členit	k5eAaImF	členit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
významové	významový	k2eAgFnPc4d1	významová
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Přidáváním	přidávání	k1gNnSc7	přidávání
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
spojováním	spojování	k1gNnSc7	spojování
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
kořeny	kořen	k1gInPc7	kořen
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
kmen	kmen	k1gInSc1	kmen
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
flektivních	flektivní	k2eAgInPc6d1	flektivní
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc6	časování
přidávají	přidávat	k5eAaImIp3nP	přidávat
různé	různý	k2eAgFnPc1d1	různá
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
mluvnických	mluvnický	k2eAgFnPc2d1	mluvnická
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemění	měnit	k5eNaImIp3nS	měnit
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
i	i	k9	i
tvar	tvar	k1gInSc4	tvar
samotného	samotný	k2eAgInSc2d1	samotný
kmene	kmen	k1gInSc2	kmen
i	i	k8xC	i
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semitských	semitský	k2eAgInPc6d1	semitský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
souhláskový	souhláskový	k2eAgInSc1d1	souhláskový
kořen	kořen	k1gInSc1	kořen
tvořený	tvořený	k2eAgInSc1d1	tvořený
nejčastěji	často	k6eAd3	často
třemi	tři	k4xCgFnPc7	tři
souhláskami	souhláska	k1gFnPc7	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
nejen	nejen	k6eAd1	nejen
předponami	předpona	k1gFnPc7	předpona
a	a	k8xC	a
příponami	přípona	k1gFnPc7	přípona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různou	různý	k2eAgFnSc7d1	různá
vokalizací	vokalizace	k1gFnSc7	vokalizace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
doplňováním	doplňování	k1gNnSc7	doplňování
různých	různý	k2eAgFnPc2d1	různá
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
vykřikovat	vykřikovat	k5eAaImF	vykřikovat
je	být	k5eAaImIp3nS	být
předpona	předpona	k1gFnSc1	předpona
vy-	vy-	k?	vy-
<g/>
,	,	kIx,	,
kořen	kořen	k1gInSc1	kořen
-křik-	řik-	k?	-křik-
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
-ovat	vat	k1gInSc1	-ovat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
infinitiv	infinitiv	k1gInSc1	infinitiv
nemá	mít	k5eNaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
)	)	kIx)	)
</s>
