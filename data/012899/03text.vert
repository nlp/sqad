<p>
<s>
Kaltenštejn	Kaltenštejn	k1gInSc1	Kaltenštejn
byl	být	k5eAaImAgInS	být
slezský	slezský	k2eAgInSc4d1	slezský
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
osamělé	osamělý	k2eAgFnSc6d1	osamělá
kupě	kupa	k1gFnSc6	kupa
u	u	k7c2	u
Černé	Černé	k2eAgFnSc2d1	Černé
Vody	voda	k1gFnSc2	voda
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Kaltenštejn	Kaltenštejna	k1gFnPc2	Kaltenštejna
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
připomínán	připomínat	k5eAaImNgInS	připomínat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
listinách	listina	k1gFnPc6	listina
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1295	[number]	k4	1295
a	a	k8xC	a
1296	[number]	k4	1296
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
svídnickým	svídnický	k2eAgMnSc7d1	svídnický
vévodou	vévoda	k1gMnSc7	vévoda
Bolkem	Bolek	k1gMnSc7	Bolek
I.	I.	kA	I.
a	a	k8xC	a
vratislavským	vratislavský	k2eAgMnSc7d1	vratislavský
biskupem	biskup	k1gMnSc7	biskup
Janem	Jan	k1gMnSc7	Jan
Romkem	Romek	k1gMnSc7	Romek
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc1d1	postavený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nacházel	nacházet	k5eAaImAgMnS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
"	"	kIx"	"
<g/>
Niské	Niský	k2eAgFnSc6d1	Niský
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c6	na
biskupské	biskupský	k2eAgFnSc6d1	biskupská
půdě	půda	k1gFnSc6	půda
<g/>
)	)	kIx)	)
a	a	k8xC	a
držel	držet	k5eAaImAgMnS	držet
jej	on	k3xPp3gMnSc4	on
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
původních	původní	k2eAgMnPc2d1	původní
stavitelů	stavitel	k1gMnPc2	stavitel
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgNnPc2d1	označované
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepřátele	nepřítel	k1gMnPc4	nepřítel
církve	církev	k1gFnSc2	církev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozsudku	rozsudek	k1gInSc2	rozsudek
vyneseného	vynesený	k2eAgInSc2d1	vynesený
krakovským	krakovský	k2eAgInSc7d1	krakovský
biskupem	biskup	k1gInSc7	biskup
měl	mít	k5eAaImAgMnS	mít
vévoda	vévoda	k1gMnSc1	vévoda
hrad	hrad	k1gInSc1	hrad
buď	buď	k8xC	buď
zbořit	zbořit	k5eAaPmF	zbořit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
předat	předat	k5eAaPmF	předat
vratislavskému	vratislavský	k2eAgMnSc3d1	vratislavský
biskupovi	biskup	k1gMnSc3	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
druhou	druhý	k4xOgFnSc4	druhý
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
roku	rok	k1gInSc2	rok
1299	[number]	k4	1299
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
dosvědčen	dosvědčen	k2eAgInSc4d1	dosvědčen
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kastelána	kastelán	k1gMnSc2	kastelán
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
byl	být	k5eAaImAgMnS	být
biskupův	biskupův	k2eAgMnSc1d1	biskupův
bratr	bratr	k1gMnSc1	bratr
Dětřich	Dětřich	k1gMnSc1	Dětřich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zastavován	zastavován	k2eAgInSc1d1	zastavován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
více	hodně	k6eAd2	hodně
držitelům	držitel	k1gMnPc3	držitel
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
částečně	částečně	k6eAd1	částečně
zdědil	zdědit	k5eAaPmAgInS	zdědit
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
opět	opět	k6eAd1	opět
vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
biskupové	biskup	k1gMnPc1	biskup
drželi	držet	k5eAaImAgMnP	držet
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
spravovány	spravován	k2eAgFnPc4d1	spravována
vsi	ves	k1gFnPc4	ves
Stará	starý	k2eAgFnSc1d1	stará
Červená	červený	k2eAgFnSc1d1	červená
Voda	voda	k1gFnSc1	voda
a	a	k8xC	a
Jindřichovice	Jindřichovice	k1gFnSc1	Jindřichovice
u	u	k7c2	u
Vidnavy	Vidnava	k1gFnSc2	Vidnava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
hrad	hrad	k1gInSc4	hrad
sice	sice	k8xC	sice
zřejmě	zřejmě	k6eAd1	zřejmě
obsazen	obsazen	k2eAgMnSc1d1	obsazen
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
finančního	finanční	k2eAgNnSc2d1	finanční
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
biskupské	biskupský	k2eAgFnSc2d1	biskupská
pokladny	pokladna	k1gFnSc2	pokladna
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
zastavován	zastavovat	k5eAaImNgInS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Zástavními	zástavní	k2eAgMnPc7d1	zástavní
držiteli	držitel	k1gMnPc7	držitel
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1435	[number]	k4	1435
Pelkan	Pelkana	k1gFnPc2	Pelkana
z	z	k7c2	z
Kalkova	Kalkov	k1gInSc2	Kalkov
<g/>
,	,	kIx,	,
1441	[number]	k4	1441
Hanušek	Hanuška	k1gFnPc2	Hanuška
z	z	k7c2	z
Mušína	Mušín	k1gInSc2	Mušín
<g/>
,	,	kIx,	,
1443	[number]	k4	1443
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
1455	[number]	k4	1455
Kryštof	Kryštof	k1gMnSc1	Kryštof
z	z	k7c2	z
Czechssdorfu	Czechssdorf	k1gInSc2	Czechssdorf
<g/>
,	,	kIx,	,
1460	[number]	k4	1460
<g/>
–	–	k?	–
<g/>
1470	[number]	k4	1470
Mikuláš	mikuláš	k1gInSc1	mikuláš
Meinholt	Meinholt	k2eAgInSc1d1	Meinholt
<g/>
/	/	kIx~	/
<g/>
Meynholt	Meynholt	k1gInSc1	Meynholt
(	(	kIx(	(
<g/>
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
hrad	hrad	k1gInSc4	hrad
Frýdberk	Frýdberk	k1gInSc4	Frýdberk
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
1470	[number]	k4	1470
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
kanovník	kanovník	k1gMnSc1	kanovník
a	a	k8xC	a
opavský	opavský	k2eAgMnSc1d1	opavský
vévoda	vévoda	k1gMnSc1	vévoda
Přemek	Přemek	k1gMnSc1	Přemek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1472	[number]	k4	1472
<g/>
–	–	k?	–
<g/>
1497	[number]	k4	1497
opět	opět	k6eAd1	opět
Hynek	Hynek	k1gMnSc1	Hynek
Meinholt	Meinholt	k1gMnSc1	Meinholt
a	a	k8xC	a
1497	[number]	k4	1497
<g/>
–	–	k?	–
<g/>
1505	[number]	k4	1505
jeho	jeho	k3xOp3gMnSc7	jeho
zeť	zeť	k1gMnSc1	zeť
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Tetova	Tetov	k1gInSc2	Tetov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1441	[number]	k4	1441
se	se	k3xPyFc4	se
Kaltenštejn	Kaltenštejn	k1gMnSc1	Kaltenštejn
stal	stát	k5eAaPmAgMnS	stát
svědkem	svědek	k1gMnSc7	svědek
násilných	násilný	k2eAgFnPc2d1	násilná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
rytíř	rytíř	k1gMnSc1	rytíř
Zikmund	Zikmund	k1gMnSc1	Zikmund
Rachna	Rachna	k?	Rachna
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
unesl	unést	k5eAaPmAgInS	unést
dceru	dcera	k1gFnSc4	dcera
kladského	kladský	k2eAgMnSc2d1	kladský
hejtmana	hejtman	k1gMnSc2	hejtman
Půty	Půta	k1gMnSc2	Půta
z	z	k7c2	z
Častolovic	Častolovice	k1gFnPc2	Častolovice
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
prchal	prchat	k5eAaImAgMnS	prchat
před	před	k7c7	před
jejím	její	k3xOp3gMnSc7	její
otčímem	otčím	k1gMnSc7	otčím
Hynkem	Hynek	k1gMnSc7	Hynek
Krušinou	krušina	k1gFnSc7	krušina
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
hrad	hrad	k1gInSc4	hrad
překvapivě	překvapivě	k6eAd1	překvapivě
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
vojsko	vojsko	k1gNnSc1	vojsko
však	však	k9	však
hrad	hrad	k1gInSc1	hrad
hrad	hrad	k1gInSc4	hrad
dobylo	dobýt	k5eAaPmAgNnS	dobýt
a	a	k8xC	a
hlavní	hlavní	k2eAgMnPc1d1	hlavní
protagonisté	protagonista	k1gMnPc1	protagonista
mučeni	mučit	k5eAaImNgMnP	mučit
a	a	k8xC	a
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
stavu	stav	k1gInSc2	stav
hradu	hrad	k1gInSc2	hrad
zřejmě	zřejmě	k6eAd1	zřejmě
nedotkly	dotknout	k5eNaPmAgFnP	dotknout
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
opravován	opravován	k2eAgMnSc1d1	opravován
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
polovině	polovina	k1gFnSc6	polovina
byl	být	k5eAaImAgInS	být
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dispozice	dispozice	k1gFnSc1	dispozice
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opravám	oprava	k1gFnPc3	oprava
navrhovaným	navrhovaný	k2eAgFnPc3d1	navrhovaná
roku	rok	k1gInSc2	rok
1443	[number]	k4	1443
zřejmě	zřejmě	k6eAd1	zřejmě
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Roth	Roth	k1gMnSc1	Roth
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konsolidace	konsolidace	k1gFnSc1	konsolidace
biskupských	biskupský	k2eAgFnPc2d1	biskupská
držav	država	k1gFnPc2	država
na	na	k7c6	na
Jesenicku	Jesenicko	k1gNnSc6	Jesenicko
Kaltenštejn	Kaltenštejna	k1gFnPc2	Kaltenštejna
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
ze	z	k7c2	z
zástavy	zástava	k1gFnSc2	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Nepřistoupil	přistoupit	k5eNaPmAgMnS	přistoupit
však	však	k9	však
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
opravě	oprava	k1gFnSc3	oprava
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
již	již	k9	již
roku	rok	k1gInSc2	rok
1512	[number]	k4	1512
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
zcela	zcela	k6eAd1	zcela
zbořit	zbořit	k5eAaPmF	zbořit
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
vratislavské	vratislavský	k2eAgFnSc2d1	Vratislavská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgInS	být
Kaltenštejn	Kaltenštejn	k1gInSc1	Kaltenštejn
využit	využít	k5eAaPmNgInS	využít
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
blízký	blízký	k2eAgInSc4d1	blízký
hrad	hrad	k1gInSc4	hrad
Jánský	jánský	k2eAgInSc1d1	jánský
Vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
probíhala	probíhat	k5eAaImAgNnP	probíhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
středověkými	středověký	k2eAgInPc7d1	středověký
hrady	hrad	k1gInPc7	hrad
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesloužily	sloužit	k5eNaImAgInP	sloužit
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
novodobých	novodobý	k2eAgFnPc2d1	novodobá
staveb	stavba	k1gFnPc2	stavba
zámeckého	zámecký	k2eAgInSc2d1	zámecký
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Kaltenštejn	Kaltenštejn	k1gInSc4	Kaltenštejn
k	k	k7c3	k
nejzachovalejším	zachovalý	k2eAgFnPc3d3	nejzachovalejší
<g/>
.	.	kIx.	.
</s>
<s>
Východozápadně	Východozápadně	k6eAd1	Východozápadně
orientované	orientovaný	k2eAgNnSc1d1	orientované
vejčité	vejčitý	k2eAgNnSc1d1	vejčité
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnPc1d1	maximální
rozměry	rozměra	k1gFnPc1	rozměra
45	[number]	k4	45
×	×	k?	×
30	[number]	k4	30
m	m	kA	m
<g/>
)	)	kIx)	)
nemuselo	muset	k5eNaImAgNnS	muset
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kupy	kupa	k1gFnSc2	kupa
<g/>
,	,	kIx,	,
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
volně	volně	k6eAd1	volně
stojí	stát	k5eAaImIp3nS	stát
kruhový	kruhový	k2eAgInSc4d1	kruhový
bergfrit	bergfrit	k1gInSc4	bergfrit
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
10	[number]	k4	10
m	m	kA	m
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
dosud	dosud	k6eAd1	dosud
20	[number]	k4	20
m.	m.	k?	m.
K	k	k7c3	k
hradbě	hradba	k1gFnSc3	hradba
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
přimyká	přimykat	k5eAaImIp3nS	přimykat
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
brankou	branka	k1gFnSc7	branka
v	v	k7c6	v
obvodní	obvodní	k2eAgFnSc6d1	obvodní
zdi	zeď	k1gFnSc6	zeď
jádra	jádro	k1gNnSc2	jádro
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
dvě	dva	k4xCgFnPc4	dva
předhradí	předhradí	k1gNnPc2	předhradí
<g/>
.	.	kIx.	.
</s>
<s>
Pozdějšího	pozdní	k2eAgInSc2d2	pozdější
původu	původ	k1gInSc2	původ
než	než	k8xS	než
jádro	jádro	k1gNnSc1	jádro
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
parkánová	parkánový	k2eAgFnSc1d1	Parkánová
zeď	zeď	k1gFnSc1	zeď
kolem	kolem	k7c2	kolem
celého	celý	k2eAgInSc2d1	celý
vrcholu	vrchol	k1gInSc2	vrchol
kopce	kopec	k1gInSc2	kopec
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
hradu	hrad	k1gInSc2	hrad
==	==	k?	==
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
hradu	hrad	k1gInSc2	hrad
tvoří	tvořit	k5eAaImIp3nS	tvořit
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc1	jeho
nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
nepatrné	patrný	k2eNgInPc1d1	patrný
zbytky	zbytek	k1gInPc1	zbytek
(	(	kIx(	(
<g/>
suterén	suterén	k1gInSc1	suterén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
věžovitý	věžovitý	k2eAgMnSc1d1	věžovitý
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
×	×	k?	×
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
patrový	patrový	k2eAgInSc1d1	patrový
<g/>
.	.	kIx.	.
</s>
<s>
Sklepy	sklep	k1gInPc1	sklep
paláce	palác	k1gInSc2	palác
byly	být	k5eAaImAgInP	být
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgInSc1d1	jihovýchodní
sklep	sklep	k1gInSc1	sklep
má	mít	k5eAaImIp3nS	mít
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
zbytek	zbytek	k1gInSc1	zbytek
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Zdi	zeď	k1gFnPc1	zeď
byly	být	k5eAaImAgFnP	být
silné	silný	k2eAgInPc1d1	silný
až	až	k9	až
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hradba	hradba	k1gFnSc1	hradba
jádra	jádro	k1gNnSc2	jádro
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
jen	jen	k9	jen
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
zbytku	zbytek	k1gInSc6	zbytek
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
asi	asi	k9	asi
sedm	sedm	k4xCc4	sedm
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
další	další	k2eAgFnSc7d1	další
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
bergfrit	bergfrit	k1gInSc1	bergfrit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
průměr	průměr	k1gInSc4	průměr
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
10,6	[number]	k4	10,6
metru	metr	k1gInSc2	metr
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
zdí	zeď	k1gFnPc2	zeď
4,35	[number]	k4	4,35
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgNnPc6d1	horní
patrech	patro	k1gNnPc6	patro
byla	být	k5eAaImAgFnS	být
zeď	zeď	k1gFnSc1	zeď
silná	silný	k2eAgFnSc1d1	silná
jen	jen	k9	jen
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
je	být	k5eAaImIp3nS	být
portálkem	portálek	k1gInSc7	portálek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Portálek	portálek	k1gInSc1	portálek
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
měla	mít	k5eAaImAgFnS	mít
střílny	střílna	k1gFnPc4	střílna
a	a	k8xC	a
ochoz	ochoz	k1gInSc4	ochoz
s	s	k7c7	s
cimbuřím	cimbuří	k1gNnSc7	cimbuří
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
výbuchem	výbuch	k1gInSc7	výbuch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přízemí	přízemí	k1gNnSc1	přízemí
věže	věž	k1gFnSc2	věž
tvořila	tvořit	k5eAaImAgFnS	tvořit
zaklenutá	zaklenutý	k2eAgFnSc1d1	zaklenutá
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgNnPc6d2	vyšší
patrech	patro	k1gNnPc6	patro
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
měl	mít	k5eAaImAgInS	mít
severozápadní	severozápadní	k2eAgNnSc4d1	severozápadní
předhradí	předhradí	k1gNnSc4	předhradí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
hradu	hrad	k1gInSc3	hrad
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
budova	budova	k1gFnSc1	budova
připomínaná	připomínaný	k2eAgFnSc1d1	připomínaná
roku	rok	k1gInSc2	rok
1443	[number]	k4	1443
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
stupňovitého	stupňovitý	k2eAgInSc2d1	stupňovitý
pilíře	pilíř	k1gInSc2	pilíř
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
první	první	k4xOgFnSc1	první
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
vedoucí	vedoucí	k1gFnSc1	vedoucí
do	do	k7c2	do
předhradí	předhradí	k1gNnSc2	předhradí
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
hranolové	hranolový	k2eAgFnSc2d1	hranolová
bašty	bašta	k1gFnSc2	bašta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dochován	dochovat	k5eAaPmNgInS	dochovat
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
portálu	portál	k1gInSc2	portál
s	s	k7c7	s
drážkou	drážka	k1gFnSc7	drážka
po	po	k7c6	po
padacím	padací	k2eAgInPc3d1	padací
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
bráně	brána	k1gFnSc3	brána
patřila	patřit	k5eAaImAgFnS	patřit
místnost	místnost	k1gFnSc1	místnost
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
strážnice	strážnice	k1gFnSc1	strážnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
krytá	krytý	k2eAgFnSc1d1	krytá
studna	studna	k1gFnSc1	studna
patrná	patrný	k2eAgFnSc1d1	patrná
ještě	ještě	k9	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
obíhá	obíhat	k5eAaImIp3nS	obíhat
parkánová	parkánový	k2eAgFnSc1d1	Parkánová
hradba	hradba	k1gFnSc1	hradba
a	a	k8xC	a
hradba	hradba	k1gFnSc1	hradba
vnějšího	vnější	k2eAgNnSc2d1	vnější
předhradí	předhradí	k1gNnSc2	předhradí
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
jen	jen	k9	jen
asi	asi	k9	asi
1,1	[number]	k4	1,1
metru	metr	k1gInSc2	metr
a	a	k8xC	a
osazená	osazený	k2eAgFnSc1d1	osazená
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
nevýraznou	výrazný	k2eNgFnSc7d1	nevýrazná
baštou	bašta	k1gFnSc7	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgNnSc1d1	vnější
předhradí	předhradí	k1gNnSc1	předhradí
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
předhradí	předhradí	k1gNnSc2	předhradí
byla	být	k5eAaImAgNnP	být
jen	jen	k6eAd1	jen
otvorem	otvor	k1gInSc7	otvor
s	s	k7c7	s
vraty	vrat	k1gInPc7	vrat
bez	bez	k7c2	bez
příkopu	příkop	k1gInSc2	příkop
a	a	k8xC	a
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
na	na	k7c6	na
předhradí	předhradí	k1gNnSc6	předhradí
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
stáje	stáj	k1gFnPc1	stáj
<g/>
,	,	kIx,	,
větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňovat	k5eAaImNgInP	zmiňovat
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KOUŘIL	Kouřil	k1gMnSc1	Kouřil
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
PRIX	PRIX	kA	PRIX
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
;	;	kIx,	;
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Archeologický	archeologický	k2eAgInSc1d1	archeologický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
645	[number]	k4	645
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86023	[number]	k4	86023
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PLAČEK	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
439	[number]	k4	439
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kaltenštejn	Kaltenštejna	k1gFnPc2	Kaltenštejna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Kaltenštejn	Kaltenštejn	k1gNnSc1	Kaltenštejn
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Kaltenštejn	Kaltenštejno	k1gNnPc2	Kaltenštejno
na	na	k7c4	na
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
