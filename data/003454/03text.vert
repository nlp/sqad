<s>
The	The	k?	The
69	[number]	k4	69
Eyes	Eyesa	k1gFnPc2	Eyesa
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
gothic	gothice	k1gInPc2	gothice
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
69	[number]	k4	69
Eyes	Eyesa	k1gFnPc2	Eyesa
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
styl	styl	k1gInSc1	styl
skupiny	skupina	k1gFnSc2	skupina
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
do	do	k7c2	do
industriálního	industriální	k2eAgMnSc2d1	industriální
a	a	k8xC	a
glam	glam	k1gInSc1	glam
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
gothic	gothic	k1gMnSc1	gothic
rockový	rockový	k2eAgMnSc1d1	rockový
až	až	k8xS	až
gothic	gothic	k1gMnSc1	gothic
metalový	metalový	k2eAgInSc4d1	metalový
zvuk	zvuk	k1gInSc4	zvuk
přišel	přijít	k5eAaPmAgMnS	přijít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c6	na
albu	album	k1gNnSc6	album
Wasting	Wasting	k1gInSc1	Wasting
the	the	k?	the
Dawn	Dawn	k1gInSc1	Dawn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
získali	získat	k5eAaPmAgMnP	získat
The	The	k1gFnSc4	The
69	[number]	k4	69
Eyes	Eyesa	k1gFnPc2	Eyesa
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
album	album	k1gNnSc4	album
Blessed	Blessed	k1gMnSc1	Blessed
Be	Be	k1gMnSc1	Be
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydali	vydat	k5eAaPmAgMnP	vydat
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
desku	deska	k1gFnSc4	deska
Paris	Paris	k1gMnSc1	Paris
Kills	Killsa	k1gFnPc2	Killsa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
Framed	Framed	k1gInSc1	Framed
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
řadové	řadový	k2eAgNnSc1d1	řadové
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Devils	Devilsa	k1gFnPc2	Devilsa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
představeno	představen	k2eAgNnSc1d1	představeno
album	album	k1gNnSc1	album
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
.	.	kIx.	.
</s>
<s>
Jyrki	Jyrki	k6eAd1	Jyrki
69	[number]	k4	69
(	(	kIx(	(
<g/>
Jyrki	Jyrk	k1gFnSc2	Jyrk
Pekka	Pekka	k1gMnSc1	Pekka
Emil	Emil	k1gMnSc1	Emil
Linnankivi	Linnankiev	k1gFnSc3	Linnankiev
<g/>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
Bazie	Bazie	k1gFnSc2	Bazie
(	(	kIx(	(
<g/>
Pasi	pase	k1gFnSc4	pase
Moilanen	Moilanna	k1gFnPc2	Moilanna
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Timo-Timo	Timo-Tima	k1gFnSc5	Timo-Tima
(	(	kIx(	(
<g/>
Timo	Timo	k6eAd1	Timo
Tapio	Tapio	k6eAd1	Tapio
Pitkänen	Pitkänen	k2eAgInSc1d1	Pitkänen
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
Archzie	Archzie	k1gFnSc1	Archzie
(	(	kIx(	(
<g/>
Arto	Arto	k1gMnSc1	Arto
Väinö	Väinö	k1gMnSc1	Väinö
Ensio	Ensio	k6eAd1	Ensio
Ojajärvi	Ojajärev	k1gFnSc3	Ojajärev
<g/>
)	)	kIx)	)
–	–	k?	–
basa	basa	k1gFnSc1	basa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Jussi	Jusse	k1gFnSc4	Jusse
69	[number]	k4	69
(	(	kIx(	(
<g/>
Jussi	Jusse	k1gFnSc3	Jusse
Heikki	Heikk	k1gFnSc2	Heikk
Tapio	Tapio	k6eAd1	Tapio
Vuori	Vuor	k1gFnSc2	Vuor
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Bump	Bump	k1gInSc1	Bump
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Grind	Grind	k1gMnSc1	Grind
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
City	city	k1gNnSc1	city
Resurrection	Resurrection	k1gInSc1	Resurrection
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Savage	Savage	k1gFnSc1	Savage
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Wrap	Wrap	k1gMnSc1	Wrap
Your	Your	k1gMnSc1	Your
Troubles	Troubles	k1gMnSc1	Troubles
in	in	k?	in
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Wasting	Wasting	k1gInSc1	Wasting
the	the	k?	the
Dawn	Dawn	k1gInSc1	Dawn
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Blessed	Blessed	k1gMnSc1	Blessed
Be	Be	k1gMnSc1	Be
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
Kills	Killsa	k1gFnPc2	Killsa
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Framed	Framed	k1gInSc1	Framed
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
–	–	k?	–
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Blessed	Blessed	k1gMnSc1	Blessed
of	of	k?	of
the	the	k?	the
69	[number]	k4	69
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Devils	Devils	k1gInSc1	Devils
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Back	Back	k1gInSc1	Back
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
X	X	kA	X
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Universal	Universat	k5eAaImAgInS	Universat
Monsters	Monsters	k1gInSc1	Monsters
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Music	Musice	k1gFnPc2	Musice
for	forum	k1gNnPc2	forum
Tattooed	Tattooed	k1gMnSc1	Tattooed
Ladies	Ladies	k1gMnSc1	Ladies
&	&	k?	&
Motorcycle	Motorcycle	k1gInSc1	Motorcycle
Mamas	Mamasa	k1gFnPc2	Mamasa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Velvet	Velvet	k1gMnSc1	Velvet
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Call	Call	k1gMnSc1	Call
Me	Me	k1gMnSc1	Me
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Wasting	Wasting	k1gInSc1	Wasting
the	the	k?	the
Dawn	Dawn	k1gInSc1	Dawn
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Gothic	Gothice	k1gFnPc2	Gothice
Girl	girl	k1gFnSc2	girl
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Brandon	Brandon	k1gInSc1	Brandon
Lee	Lea	k1gFnSc3	Lea
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Chair	Chair	k1gMnSc1	Chair
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Stolen	stolen	k2eAgInSc1d1	stolen
Season	Season	k1gInSc1	Season
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Dance	Danka	k1gFnSc6	Danka
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Betty	Betty	k1gFnSc6	Betty
Blue	Blue	k1gNnSc4	Blue
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Crashing	Crashing	k1gInSc1	Crashing
High	High	k1gInSc1	High
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Lost	Losta	k1gFnPc2	Losta
Boys	boy	k1gMnPc2	boy
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Devils	Devilsa	k1gFnPc2	Devilsa
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Feel	Feel	k1gInSc1	Feel
Berlin	berlina	k1gFnPc2	berlina
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Sister	Sister	k1gInSc1	Sister
of	of	k?	of
Charity	charita	k1gFnSc2	charita
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Perfect	Perfect	k1gMnSc1	Perfect
Skin	skin	k1gMnSc1	skin
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Never	Never	k1gInSc1	Never
say	say	k?	say
die	die	k?	die
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Rocker	rocker	k1gMnSc1	rocker
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Ghost	Ghost	k1gFnSc1	Ghost
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Red	Red	k1gFnSc2	Red
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
