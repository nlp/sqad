<p>
<s>
Košťálová	košťálový	k2eAgFnSc1d1	košťálová
zelenina	zelenina	k1gFnSc1	zelenina
je	být	k5eAaImIp3nS	být
zelenina	zelenina	k1gFnSc1	zelenina
se	s	k7c7	s
ztlustlou	ztlustlý	k2eAgFnSc7d1	ztlustlá
lodyhou	lodyha	k1gFnSc7	lodyha
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
s	s	k7c7	s
košťálem	košťál	k1gInSc7	košťál
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
rodu	rod	k1gInSc2	rod
brukev	brukev	k1gFnSc1	brukev
(	(	kIx(	(
<g/>
Brassica	Brassica	k1gFnSc1	Brassica
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
brukvovité	brukvovitý	k2eAgFnSc2d1	brukvovitá
(	(	kIx(	(
<g/>
Brassicaceae	Brassicacea	k1gFnSc2	Brassicacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
košťálové	košťálový	k2eAgFnPc4d1	košťálová
zeleniny	zelenina	k1gFnPc4	zelenina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
druhu	druh	k1gInSc2	druh
brukev	brukev	k1gFnSc1	brukev
zelná	zelný	k2eAgFnSc1d1	zelná
(	(	kIx(	(
<g/>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
kadeřávek	kadeřávek	k1gInSc1	kadeřávek
<g/>
,	,	kIx,	,
květák	květák	k1gInSc1	květák
<g/>
,	,	kIx,	,
brokolice	brokolice	k1gFnSc1	brokolice
a	a	k8xC	a
kedluben	kedluben	k1gInSc1	kedluben
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhu	druh	k1gInSc2	druh
brukev	brukev	k1gFnSc1	brukev
čínská	čínský	k2eAgFnSc1d1	čínská
(	(	kIx(	(
<g/>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
sinensis	sinensis	k1gFnSc1	sinensis
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
zelí	zelí	k1gNnSc1	zelí
čínské	čínský	k2eAgNnSc1d1	čínské
a	a	k8xC	a
do	do	k7c2	do
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
campestris	campestris	k1gFnSc1	campestris
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
zelí	zelí	k1gNnSc1	zelí
pekingské	pekingský	k2eAgNnSc1d1	pekingské
<g/>
.	.	kIx.	.
</s>
<s>
Květák	květák	k1gInSc1	květák
a	a	k8xC	a
brokolice	brokolice	k1gFnPc1	brokolice
jsou	být	k5eAaImIp3nP	být
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
výše	vysoce	k6eAd2	vysoce
vyjmenované	vyjmenovaný	k2eAgFnPc1d1	vyjmenovaná
jsou	být	k5eAaImIp3nP	být
dvouleté	dvouletý	k2eAgFnPc1d1	dvouletá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
košťálová	košťálový	k2eAgFnSc1d1	košťálová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
hlávkové	hlávkový	k2eAgNnSc1d1	hlávkové
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Prapředek	prapředek	k1gMnSc1	prapředek
dnešních	dnešní	k2eAgFnPc2d1	dnešní
košťálovin	košťálovina	k1gFnPc2	košťálovina
<g/>
,	,	kIx,	,
brukev	brukev	k1gFnSc1	brukev
zelná	zelný	k2eAgFnSc1d1	zelná
<g/>
,	,	kIx,	,
divoce	divoce	k6eAd1	divoce
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
až	až	k9	až
po	po	k7c4	po
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Tuhle	tenhle	k3xDgFnSc4	tenhle
bylinu	bylina	k1gFnSc4	bylina
začali	začít	k5eAaPmAgMnP	začít
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
šlechtit	šlechtit	k5eAaImF	šlechtit
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
ji	on	k3xPp3gFnSc4	on
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
Caulis	Caulis	k1gInSc1	Caulis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
lodyha	lodyha	k1gFnSc1	lodyha
<g/>
,	,	kIx,	,
košťál	košťál	k1gInSc1	košťál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mnohá	mnohý	k2eAgNnPc4d1	mnohé
staletí	staletí	k1gNnPc4	staletí
se	se	k3xPyFc4	se
tak	tak	k9	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
z	z	k7c2	z
neduživých	duživý	k2eNgFnPc2d1	neduživá
rostlinek	rostlinka	k1gFnPc2	rostlinka
hlávková	hlávkový	k2eAgNnPc4d1	hlávkové
zelí	zelí	k1gNnSc4	zelí
<g/>
,	,	kIx,	,
kapusty	kapusta	k1gFnPc4	kapusta
a	a	k8xC	a
brokolice	brokolice	k1gFnPc4	brokolice
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dnešních	dnešní	k2eAgFnPc2d1	dnešní
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Květák	květák	k1gInSc1	květák
byl	být	k5eAaImAgInS	být
zase	zase	k9	zase
vypěstován	vypěstovat	k5eAaPmNgInS	vypěstovat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc2	novověk
arabskými	arabský	k2eAgInPc7d1	arabský
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
z	z	k7c2	z
drobných	drobný	k2eAgFnPc2d1	drobná
zelených	zelený	k2eAgFnPc2d1	zelená
růžiček	růžička	k1gFnPc2	růžička
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
postupně	postupně	k6eAd1	postupně
získat	získat	k5eAaPmF	získat
až	až	k9	až
dnešní	dnešní	k2eAgFnPc1d1	dnešní
bílé	bílý	k2eAgFnPc1d1	bílá
obří	obří	k2eAgFnPc1d1	obří
růžice	růžice	k1gFnPc1	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
je	být	k5eAaImIp3nS	být
růžičková	růžičkový	k2eAgFnSc1d1	růžičková
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1	vyšlechtěná
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kedluben	kedluben	k1gInSc1	kedluben
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
středozemí	středozemí	k1gNnSc6	středozemí
a	a	k8xC	a
až	až	k6eAd1	až
Číny	Čína	k1gFnSc2	Čína
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgNnSc1d1	čínské
i	i	k8xC	i
pekingské	pekingský	k2eAgNnSc1d1	pekingské
zelí	zelí	k1gNnSc1	zelí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgFnSc1d1	původní
brukev	brukev	k1gFnSc1	brukev
zelná	zelný	k2eAgFnSc1d1	zelná
===	===	k?	===
</s>
</p>
<p>
<s>
Planá	planý	k2eAgFnSc1d1	planá
brukev	brukev	k1gFnSc1	brukev
zelná	zelný	k2eAgFnSc1d1	zelná
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
bylina	bylina	k1gFnSc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
rokem	rok	k1gInSc7	rok
tvoří	tvořit	k5eAaImIp3nS	tvořit
růžici	růžice	k1gFnSc4	růžice
velkých	velký	k2eAgInPc2d1	velký
<g/>
,	,	kIx,	,
tlustých	tlustý	k2eAgInPc2d1	tlustý
<g/>
,	,	kIx,	,
šťavnatých	šťavnatý	k2eAgInPc2d1	šťavnatý
listů	list	k1gInPc2	list
na	na	k7c6	na
zdřevnatělém	zdřevnatělý	k2eAgInSc6d1	zdřevnatělý
košťálu	košťál	k1gInSc6	košťál
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
vyhání	vyhánět	k5eAaImIp3nS	vyhánět
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
stvol	stvol	k1gInSc4	stvol
nesoucí	nesoucí	k2eAgInPc4d1	nesoucí
žluté	žlutý	k2eAgInPc4d1	žlutý
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
salinitou	salinita	k1gFnSc7	salinita
i	i	k8xC	i
zásaditých	zásaditý	k2eAgMnPc2d1	zásaditý
(	(	kIx(	(
<g/>
na	na	k7c6	na
vápencovém	vápencový	k2eAgNnSc6d1	vápencové
podloží	podloží	k1gNnSc6	podloží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesnáší	snášet	k5eNaImIp3nS	snášet
konkurenci	konkurence	k1gFnSc4	konkurence
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
většinou	většinou	k6eAd1	většinou
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jiné	jiný	k2eAgFnPc1d1	jiná
rostliny	rostlina	k1gFnPc1	rostlina
nerostou	růst	k5eNaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zelí	zelí	k1gNnSc1	zelí
hlávkové	hlávkový	k2eAgNnSc1d1	hlávkové
===	===	k?	===
</s>
</p>
<p>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
tuhou	tuhý	k2eAgFnSc4d1	tuhá
hlávku	hlávka	k1gFnSc4	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
prvních	první	k4xOgInPc2	první
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
se	se	k3xPyFc4	se
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
hlávky	hlávka	k1gFnPc1	hlávka
až	až	k9	až
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
u	u	k7c2	u
bílého	bílý	k2eAgNnSc2d1	bílé
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
červeného	červený	k2eAgNnSc2d1	červené
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
červeným	červený	k2eAgNnSc7d1	červené
zelím	zelí	k1gNnSc7	zelí
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
chuti	chuť	k1gFnPc4	chuť
i	i	k9	i
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
červené	červený	k2eAgNnSc1d1	červené
zelí	zelí	k1gNnSc1	zelí
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
vlákniny	vláknina	k1gFnPc4	vláknina
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
sacharidů	sacharid	k1gInPc2	sacharid
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
minerálu	minerál	k1gInSc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Hlávky	hlávka	k1gFnPc1	hlávka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
nejen	nejen	k6eAd1	nejen
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
hlávky	hlávka	k1gFnPc4	hlávka
špičaté	špičatý	k2eAgFnPc4d1	špičatá
<g/>
.	.	kIx.	.
</s>
<s>
Rozmáhají	rozmáhat	k5eAaImIp3nP	rozmáhat
se	se	k3xPyFc4	se
i	i	k9	i
okrasné	okrasný	k2eAgInPc1d1	okrasný
kultivary	kultivar	k1gInPc1	kultivar
červeného	červený	k2eAgNnSc2d1	červené
a	a	k8xC	a
bílého	bílý	k2eAgNnSc2d1	bílé
zelí	zelí	k1gNnSc2	zelí
které	který	k3yQgFnSc2	který
hlávky	hlávka	k1gFnSc2	hlávka
netvoří	tvořit	k5eNaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zelí	zelí	k1gNnSc1	zelí
lze	lze	k6eAd1	lze
uchovávat	uchovávat	k5eAaImF	uchovávat
i	i	k9	i
jako	jako	k9	jako
kysané	kysaný	k2eAgFnPc1d1	kysaná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
asi	asi	k9	asi
tatarský	tatarský	k2eAgInSc4d1	tatarský
vynález	vynález	k1gInSc4	vynález
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zalíbil	zalíbit	k5eAaPmAgInS	zalíbit
Slovanům	Slovan	k1gMnPc3	Slovan
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
seznámili	seznámit	k5eAaPmAgMnP	seznámit
ostatní	ostatní	k2eAgFnSc4d1	ostatní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
způsob	způsob	k1gInSc1	způsob
konzervace	konzervace	k1gFnSc2	konzervace
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
mléčném	mléčný	k2eAgNnSc6d1	mléčné
kvašení	kvašení	k1gNnSc6	kvašení
<g/>
,	,	kIx,	,
vyvolaném	vyvolaný	k2eAgInSc6d1	vyvolaný
mikroorganismem	mikroorganismus	k1gInSc7	mikroorganismus
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
brassiace	brassiace	k1gFnSc2	brassiace
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prý	prý	k9	prý
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
konzervace	konzervace	k1gFnSc2	konzervace
dodali	dodat	k5eAaPmAgMnP	dodat
Čechové	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šlapání	šlapání	k1gNnSc1	šlapání
zelí	zelí	k1gNnSc2	zelí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kapusta	kapusta	k1gFnSc1	kapusta
hlávková	hlávkový	k2eAgFnSc1d1	hlávková
===	===	k?	===
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
tmavozelené	tmavozelený	k2eAgInPc4d1	tmavozelený
zvlněné	zvlněný	k2eAgInPc4d1	zvlněný
listy	list	k1gInPc4	list
se	s	k7c7	s
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
aromatickou	aromatický	k2eAgFnSc7d1	aromatická
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
vůni	vůně	k1gFnSc6	vůně
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
obdobě	obdoba	k1gFnSc3	obdoba
jako	jako	k8xS	jako
zelí	zelí	k1gNnSc1	zelí
<g/>
.	.	kIx.	.
</s>
<s>
Mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kapusty	kapusta	k1gFnSc2	kapusta
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
konec	konec	k1gInSc4	konec
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vypěstovány	vypěstován	k2eAgInPc1d1	vypěstován
první	první	k4xOgFnPc1	první
hlávky	hlávka	k1gFnPc1	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Kapusta	kapusta	k1gFnSc1	kapusta
je	být	k5eAaImIp3nS	být
otužilá	otužilý	k2eAgFnSc1d1	otužilá
rostlina	rostlina	k1gFnSc1	rostlina
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
přezimovat	přezimovat	k5eAaBmF	přezimovat
i	i	k9	i
na	na	k7c6	na
záhonu	záhon	k1gInSc6	záhon
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
kultivarů	kultivar	k1gInPc2	kultivar
je	být	k5eAaImIp3nS	být
kapusta	kapusta	k1gFnSc1	kapusta
listová	listový	k2eAgFnSc1d1	listová
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
košťálu	košťál	k1gInSc6	košťál
vysokém	vysoký	k2eAgInSc6d1	vysoký
až	až	k8xS	až
120	[number]	k4	120
cm	cm	kA	cm
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
křehké	křehký	k2eAgFnPc1d1	křehká
ploché	plochý	k2eAgFnPc1d1	plochá
listy	lista	k1gFnPc1	lista
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
sklízí	sklízet	k5eAaImIp3nS	sklízet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
však	však	k9	však
populární	populární	k2eAgFnSc1d1	populární
nestala	stát	k5eNaPmAgFnS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kadeřávek	kadeřávek	k1gInSc4	kadeřávek
===	===	k?	===
</s>
</p>
<p>
<s>
Jinak	jinak	k6eAd1	jinak
také	také	k9	také
kapusta	kapusta	k1gFnSc1	kapusta
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
neboli	neboli	k8xC	neboli
jarmuz	jarmuz	k1gInSc1	jarmuz
<g/>
.	.	kIx.	.
</s>
<s>
Listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
výnosy	výnos	k1gInPc7	výnos
zkadeřených	zkadeřený	k2eAgFnPc2d1	zkadeřená
(	(	kIx(	(
<g/>
i	i	k8xC	i
hladkých	hladký	k2eAgInPc2d1	hladký
<g/>
)	)	kIx)	)
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
obdobné	obdobný	k2eAgNnSc4d1	obdobné
využití	využití	k1gNnSc4	využití
jako	jako	k8xS	jako
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ceněná	ceněný	k2eAgFnSc1d1	ceněná
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
košťálu	košťál	k1gInSc6	košťál
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rostlina	rostlina	k1gFnSc1	rostlina
nejvíce	hodně	k6eAd3	hodně
podobná	podobný	k2eAgFnSc1d1	podobná
původnímu	původní	k2eAgInSc3d1	původní
planému	planý	k2eAgInSc3d1	planý
druhu	druh	k1gInSc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
rostlina	rostlina	k1gFnSc1	rostlina
dekorativní	dekorativní	k2eAgFnSc1d1	dekorativní
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
košťál	košťál	k1gInSc1	košťál
vysoký	vysoký	k2eAgInSc1d1	vysoký
až	až	k9	až
3	[number]	k4	3
metry	metr	k1gInPc4	metr
a	a	k8xC	a
pak	pak	k6eAd1	pak
vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
palmu	palma	k1gFnSc4	palma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Květák	květák	k1gInSc4	květák
===	===	k?	===
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
nerozvinuté	rozvinutý	k2eNgNnSc1d1	nerozvinuté
zdužnatělé	zdužnatělý	k2eAgNnSc1d1	zdužnatělé
květenství	květenství	k1gNnSc1	květenství
převážně	převážně	k6eAd1	převážně
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
stravitelný	stravitelný	k2eAgMnSc1d1	stravitelný
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
dietní	dietní	k2eAgFnSc4d1	dietní
potravinu	potravina	k1gFnSc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
menšímu	malý	k2eAgInSc3d2	menší
kořenovému	kořenový	k2eAgInSc3d1	kořenový
systému	systém	k1gInSc3	systém
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
množství	množství	k1gNnSc1	množství
snadno	snadno	k6eAd1	snadno
přijatelných	přijatelný	k2eAgFnPc2d1	přijatelná
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
dostatečné	dostatečná	k1gFnSc2	dostatečná
zásobování	zásobování	k1gNnSc2	zásobování
rostlin	rostlina	k1gFnPc2	rostlina
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejnáročnější	náročný	k2eAgFnSc7d3	nejnáročnější
košťálovou	košťálový	k2eAgFnSc7d1	košťálová
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
skladovatelností	skladovatelnost	k1gFnPc2	skladovatelnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
v	v	k7c6	v
úzkém	úzký	k2eAgNnSc6d1	úzké
teplotním	teplotní	k2eAgNnSc6d1	teplotní
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brokolice	brokolice	k1gFnSc2	brokolice
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
příbuzná	příbuzná	k1gFnSc1	příbuzná
květáku	květák	k1gInSc2	květák
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
nerozvinuté	rozvinutý	k2eNgNnSc4d1	nerozvinuté
květenství	květenství	k1gNnSc4	květenství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
nachové	nachový	k2eAgInPc1d1	nachový
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejhodnotnějších	hodnotný	k2eAgFnPc2d3	nejhodnotnější
křehkých	křehký	k2eAgFnPc2d1	křehká
zelenin	zelenina	k1gFnPc2	zelenina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Brokolice	brokolice	k1gFnPc4	brokolice
má	mít	k5eAaImIp3nS	mít
typické	typický	k2eAgNnSc1d1	typické
aroma	aroma	k1gNnSc1	aroma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zvýrazní	zvýraznit	k5eAaPmIp3nS	zvýraznit
při	při	k7c6	při
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
sklizeň	sklizeň	k1gFnSc1	sklizeň
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
narostení	narostení	k1gNnSc6	narostení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
růžice	růžice	k1gFnSc2	růžice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odřízne	odříznout	k5eAaPmIp3nS	odříznout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rostlina	rostlina	k1gFnSc1	rostlina
ponechá	ponechat	k5eAaPmIp3nS	ponechat
na	na	k7c6	na
záhonu	záhon	k1gInSc6	záhon
<g/>
,	,	kIx,	,
po	po	k7c6	po
čase	čas	k1gInSc6	čas
narostou	narůst	k5eAaPmIp3nP	narůst
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Růžičková	růžičkový	k2eAgFnSc1d1	růžičková
kapusta	kapusta	k1gFnSc1	kapusta
===	===	k?	===
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
asi	asi	k9	asi
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
drobné	drobný	k2eAgInPc4d1	drobný
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
z	z	k7c2	z
úžlabních	úžlabní	k2eAgInPc2d1	úžlabní
pupenů	pupen	k1gInPc2	pupen
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
semknuté	semknutý	k2eAgFnPc1d1	semknutá
do	do	k7c2	do
růžiček	růžička	k1gFnPc2	růžička
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stejnoměrný	stejnoměrný	k2eAgInSc4d1	stejnoměrný
vývoj	vývoj	k1gInSc4	vývoj
růžiček	růžička	k1gFnPc2	růžička
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
odstranit	odstranit	k5eAaPmF	odstranit
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
růstový	růstový	k2eAgInSc1d1	růstový
vrchol	vrchol	k1gInSc1	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Sklízí	sklízet	k5eAaImIp3nS	sklízet
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
až	až	k9	až
do	do	k7c2	do
zámrazu	zámraz	k1gInSc2	zámraz
(	(	kIx(	(
<g/>
možno	možno	k6eAd1	možno
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zimy	zima	k1gFnSc2	zima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
rostliny	rostlina	k1gFnSc2	rostlina
přibližně	přibližně	k6eAd1	přibližně
0,3	[number]	k4	0,3
kg	kg	kA	kg
růžiček	růžička	k1gFnPc2	růžička
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
zeleninu	zelenina	k1gFnSc4	zelenina
z	z	k7c2	z
domácí	domácí	k2eAgFnSc2d1	domácí
sklizně	sklizeň	k1gFnSc2	sklizeň
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
ve	v	k7c6	v
zmražené	zmražený	k2eAgFnSc6d1	zmražená
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kedlubna	kedlubna	k1gFnSc1	kedlubna
===	===	k?	===
</s>
</p>
<p>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
stonkovou	stonkový	k2eAgFnSc4d1	stonková
hlízu	hlíza	k1gFnSc4	hlíza
<g/>
,	,	kIx,	,
bulvu	bulva	k1gFnSc4	bulva
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
světle	světle	k6eAd1	světle
či	či	k8xC	či
tmavě	tmavě	k6eAd1	tmavě
zelenou	zelený	k2eAgFnSc4d1	zelená
nebo	nebo	k8xC	nebo
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Jíst	jíst	k5eAaImF	jíst
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
i	i	k9	i
povařené	povařený	k2eAgInPc4d1	povařený
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
rané	raný	k2eAgFnPc1d1	raná
i	i	k8xC	i
pozdní	pozdní	k2eAgFnPc1d1	pozdní
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
vadou	vada	k1gFnSc7	vada
je	být	k5eAaImIp3nS	být
dřevnatost	dřevnatost	k1gFnSc1	dřevnatost
a	a	k8xC	a
pukání	pukání	k1gNnSc1	pukání
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
nepravidelné	pravidelný	k2eNgFnSc6d1	nepravidelná
zálivce	zálivka	k1gFnSc6	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rychlení	rychlení	k1gNnSc4	rychlení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pěstování	pěstování	k1gNnSc3	pěstování
ve	v	k7c6	v
foliových	foliový	k2eAgInPc6d1	foliový
krytech	kryt	k1gInPc6	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
přípravě	příprava	k1gFnSc6	příprava
nezapomínáme	zapomínat	k5eNaImIp1nP	zapomínat
ani	ani	k8xC	ani
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
středové	středový	k2eAgInPc4d1	středový
lístečky	lísteček	k1gInPc4	lísteček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jemně	jemně	k6eAd1	jemně
nakrájené	nakrájený	k2eAgInPc4d1	nakrájený
přidáváme	přidávat	k5eAaImIp1nP	přidávat
k	k	k7c3	k
pokrmům	pokrm	k1gInPc3	pokrm
až	až	k8xS	až
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
evropského	evropský	k2eAgNnSc2d1	Evropské
kedlubnového	kedlubnový	k2eAgNnSc2d1	kedlubnové
vývojového	vývojový	k2eAgNnSc2d1	vývojové
centra	centrum	k1gNnSc2	centrum
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
mluvit	mluvit	k5eAaImF	mluvit
i	i	k9	i
o	o	k7c6	o
centru	centrum	k1gNnSc6	centrum
tibetském	tibetský	k2eAgNnSc6d1	tibetské
nebo	nebo	k8xC	nebo
východoasijském	východoasijský	k2eAgNnSc6d1	východoasijské
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
linie	linie	k1gFnPc4	linie
je	být	k5eAaImIp3nS	být
sklon	sklon	k1gInSc1	sklon
ke	k	k7c3	k
tloustnutí	tloustnutí	k1gNnSc3	tloustnutí
stonku	stonek	k1gInSc2	stonek
<g/>
,	,	kIx,	,
košťálu	košťál	k1gInSc2	košťál
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
kedlubna	kedlubna	k1gFnSc1	kedlubna
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Gigant	gigant	k1gMnSc1	gigant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
věhlas	věhlas	k1gInSc4	věhlas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc1d1	veliký
<g/>
,	,	kIx,	,
nedřevnatí	dřevnatět	k5eNaImIp3nS	dřevnatět
a	a	k8xC	a
podrží	podržet	k5eAaPmIp3nS	podržet
si	se	k3xPyFc3	se
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
dobré	dobrý	k2eAgFnPc4d1	dobrá
chuťové	chuťový	k2eAgFnPc4d1	chuťová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
vitamin	vitamin	k1gInSc1	vitamin
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Vitamin	vitamin	k1gInSc1	vitamin
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Vitamin	vitamin	k1gInSc1	vitamin
C	C	kA	C
<g/>
,	,	kIx,	,
z	z	k7c2	z
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
soli	sůl	k1gFnSc2	sůl
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čínské	čínský	k2eAgNnSc1d1	čínské
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
pekingské	pekingský	k2eAgNnSc1d1	pekingské
zelí	zelí	k1gNnSc1	zelí
===	===	k?	===
</s>
</p>
<p>
<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
zelí	zelí	k1gNnSc1	zelí
pěstované	pěstovaný	k2eAgNnSc1d1	pěstované
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
cm	cm	kA	cm
a	a	k8xC	a
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
nesetkáte	setkat	k5eNaPmIp2nP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Odrůdy	odrůda	k1gFnPc1	odrůda
pěstované	pěstovaný	k2eAgFnPc1d1	pěstovaná
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byly	být	k5eAaImAgInP	být
vyšlechtěny	vyšlechtěn	k2eAgInPc1d1	vyšlechtěn
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgFnPc1d1	obdobná
čínskému	čínský	k2eAgMnSc3d1	čínský
zelí	zelí	k1gNnSc2	zelí
je	být	k5eAaImIp3nS	být
zelí	zelí	k1gNnSc1	zelí
pekingské	pekingský	k2eAgNnSc1d1	pekingské
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pekingské	pekingský	k2eAgNnSc1d1	pekingské
zelí	zelí	k1gNnSc1	zelí
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
řídké	řídký	k2eAgInPc4d1	řídký
hlávky	hlávek	k1gInPc4	hlávek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
uvnitř	uvnitř	k7c2	uvnitř
hlávky	hlávka	k1gFnSc2	hlávka
listy	list	k1gInPc1	list
jemnější	jemný	k2eAgInPc1d2	jemnější
a	a	k8xC	a
křehčí	křehký	k2eAgInPc1d2	křehčí
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
a	a	k8xC	a
řapíky	řapík	k1gInPc1	řapík
čínského	čínský	k2eAgNnSc2d1	čínské
zelí	zelí	k1gNnSc2	zelí
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
aromatičtější	aromatický	k2eAgFnPc1d2	aromatičtější
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
hlávky	hlávka	k1gFnSc2	hlávka
není	být	k5eNaImIp3nS	být
košťál	košťál	k1gInSc1	košťál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Romanesco	Romanesco	k1gMnSc1	Romanesco
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
košťálové	košťálový	k2eAgFnSc2d1	košťálová
zeleniny	zelenina	k1gFnSc2	zelenina
"	"	kIx"	"
<g/>
Romanesco	Romanesco	k1gMnSc1	Romanesco
<g/>
"	"	kIx"	"
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
na	na	k7c6	na
Jadranu	Jadran	k1gInSc6	Jadran
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
chutí	chuť	k1gFnSc7	chuť
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
květáku	květák	k1gInSc3	květák
i	i	k8xC	i
brokolici	brokolice	k1gFnSc3	brokolice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
růžici	růžice	k1gFnSc4	růžice
z	z	k7c2	z
drobných	drobný	k2eAgFnPc2d1	drobná
zelenožlutých	zelenožlutý	k2eAgFnPc2d1	zelenožlutá
růžiček	růžička	k1gFnPc2	růžička
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Brukvovité	brukvovitý	k2eAgFnPc1d1	brukvovitá
rostliny	rostlina	k1gFnPc1	rostlina
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
úrodné	úrodný	k2eAgFnPc1d1	úrodná
půdy	půda	k1gFnPc1	půda
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
zásobou	zásoba	k1gFnSc7	zásoba
živin	živina	k1gFnPc2	živina
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
absorpci	absorpce	k1gFnSc4	absorpce
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
půda	půda	k1gFnSc1	půda
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
pH	ph	kA	ph
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
vláhu	vláha	k1gFnSc4	vláha
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
raději	rád	k6eAd2	rád
slunnější	slunný	k2eAgFnSc4d2	slunnější
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
pěstujeme	pěstovat	k5eAaImIp1nP	pěstovat
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
širokých	široký	k2eAgInPc6d1	široký
sponech	spon	k1gInPc6	spon
<g/>
,	,	kIx,	,
nesnášejí	snášet	k5eNaImIp3nP	snášet
hustou	hustý	k2eAgFnSc4d1	hustá
výsadbu	výsadba	k1gFnSc4	výsadba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stravování	stravování	k1gNnSc2	stravování
===	===	k?	===
</s>
</p>
<p>
<s>
Brukvovitá	brukvovitý	k2eAgFnSc1d1	brukvovitá
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
a	a	k8xC	a
kedluben	kedluben	k1gInSc1	kedluben
<g/>
,	,	kIx,	,
tvořila	tvořit	k5eAaImAgFnS	tvořit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
lidské	lidský	k2eAgFnSc2d1	lidská
stravy	strava	k1gFnSc2	strava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
skladovatelnost	skladovatelnost	k1gFnSc4	skladovatelnost
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rostliny	rostlina	k1gFnPc1	rostlina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hodně	hodně	k6eAd1	hodně
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
kalorický	kalorický	k2eAgInSc1d1	kalorický
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
využívány	využívat	k5eAaPmNgInP	využívat
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
redukčních	redukční	k2eAgFnPc6d1	redukční
dietách	dieta	k1gFnPc6	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
omaštěním	omaštění	k1gNnSc7	omaštění
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
kaloricky	kaloricky	k6eAd1	kaloricky
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlávkové	Hlávkové	k2eAgNnSc1d1	Hlávkové
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
zeleninou	zelenina	k1gFnSc7	zelenina
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
se	se	k3xPyFc4	se
nakrouhané	nakrouhaný	k2eAgNnSc1d1	nakrouhané
buď	buď	k8xC	buď
syrové	syrový	k2eAgNnSc1d1	syrové
či	či	k8xC	či
lehce	lehko	k6eAd1	lehko
povařené	povařený	k2eAgFnSc2d1	povařená
formou	forma	k1gFnSc7	forma
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dušené	dušený	k2eAgNnSc1d1	dušené
klasickým	klasický	k2eAgInSc7d1	klasický
"	"	kIx"	"
<g/>
českým	český	k2eAgInSc7d1	český
<g/>
"	"	kIx"	"
způsobem	způsob	k1gInSc7	způsob
s	s	k7c7	s
jíškou	jíška	k1gFnSc7	jíška
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
současně	současně	k6eAd1	současně
s	s	k7c7	s
brambory	brambor	k1gInPc7	brambor
a	a	k8xC	a
knedlíky	knedlík	k1gInPc7	knedlík
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zelných	zelný	k2eAgInPc2d1	zelný
listů	list	k1gInPc2	list
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k8xS	jako
obalů	obal	k1gInPc2	obal
pro	pro	k7c4	pro
různě	různě	k6eAd1	různě
připravená	připravený	k2eAgNnPc4d1	připravené
masa	maso	k1gNnPc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Způsobem	způsob	k1gInSc7	způsob
konzervace	konzervace	k1gFnSc2	konzervace
zelí	zelí	k1gNnSc2	zelí
je	být	k5eAaImIp3nS	být
zakvašování	zakvašování	k1gNnSc1	zakvašování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
prastarým	prastarý	k2eAgInSc7d1	prastarý
způsobem	způsob	k1gInSc7	způsob
uchovávání	uchovávání	k1gNnSc2	uchovávání
přes	přes	k7c4	přes
zimní	zimní	k2eAgNnSc4d1	zimní
období	období	k1gNnSc4	období
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
i	i	k8xC	i
nedostatkového	dostatkový	k2eNgInSc2d1	nedostatkový
vitamínu	vitamín	k1gInSc2	vitamín
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Hlávková	hlávkový	k2eAgFnSc1d1	hlávková
kapusta	kapusta	k1gFnSc1	kapusta
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
krouhaná	krouhaný	k2eAgFnSc1d1	krouhaná
dušená	dušený	k2eAgFnSc1d1	dušená
a	a	k8xC	a
připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
ze	z	k7c2	z
zelí	zelí	k1gNnSc2	zelí
přílohy	příloha	k1gFnSc2	příloha
ke	k	k7c3	k
knedlíkům	knedlík	k1gInPc3	knedlík
a	a	k8xC	a
bramborám	brambora	k1gFnPc3	brambora
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
celých	celý	k2eAgInPc2d1	celý
listů	list	k1gInPc2	list
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
zabalení	zabalení	k1gNnSc4	zabalení
mletých	mletý	k2eAgFnPc2d1	mletá
mas	masa	k1gFnPc2	masa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kedluben	kedluben	k1gInSc1	kedluben
se	se	k3xPyFc4	se
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
v	v	k7c6	v
syrovém	syrový	k2eAgInSc6d1	syrový
stavu	stav	k1gInSc6	stav
buď	buď	k8xC	buď
celý	celý	k2eAgInSc4d1	celý
nebo	nebo	k8xC	nebo
nastrouhaný	nastrouhaný	k2eAgInSc4d1	nastrouhaný
v	v	k7c6	v
salátech	salát	k1gInPc6	salát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nastrouhaného	nastrouhaný	k2eAgMnSc2d1	nastrouhaný
se	se	k3xPyFc4	se
také	také	k6eAd1	také
často	často	k6eAd1	často
"	"	kIx"	"
<g/>
dělá	dělat	k5eAaImIp3nS	dělat
kedlubnové	kedlubnový	k2eAgNnSc1d1	kedlubnové
zelí	zelí	k1gNnSc1	zelí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Jarní	jarní	k2eAgFnPc1d1	jarní
odrůdy	odrůda	k1gFnPc1	odrůda
jsou	být	k5eAaImIp3nP	být
chutnější	chutný	k2eAgFnPc1d2	chutnější
podzimních	podzimní	k2eAgFnPc2d1	podzimní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květáku	květák	k1gInSc2	květák
se	se	k3xPyFc4	se
za	za	k7c4	za
syrova	syrovo	k1gNnPc4	syrovo
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
spařeného	spařený	k2eAgInSc2d1	spařený
a	a	k8xC	a
rozebraného	rozebraný	k2eAgInSc2d1	rozebraný
na	na	k7c4	na
drobné	drobný	k2eAgFnPc4d1	drobná
růžičky	růžička	k1gFnPc4	růžička
používá	používat	k5eAaImIp3nS	používat
do	do	k7c2	do
zeleninových	zeleninový	k2eAgInPc2d1	zeleninový
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
růžičky	růžička	k1gFnPc1	růžička
dozlatova	dozlatova	k6eAd1	dozlatova
osmaží	osmažit	k5eAaPmIp3nP	osmažit
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozdrcený	rozdrcený	k2eAgMnSc1d1	rozdrcený
se	se	k3xPyFc4	se
zapéká	zapékat	k5eAaImIp3nS	zapékat
či	či	k8xC	či
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
zeleninové	zeleninový	k2eAgFnPc4d1	zeleninová
placičky	placička	k1gFnPc4	placička
<g/>
.	.	kIx.	.
</s>
<s>
Brokolice	brokolice	k1gFnSc1	brokolice
má	mít	k5eAaImIp3nS	mít
obdobný	obdobný	k2eAgInSc4d1	obdobný
způsob	způsob	k1gInSc4	způsob
využití	využití	k1gNnSc2	využití
jako	jako	k9	jako
květák	květák	k1gInSc4	květák
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jen	jen	k9	jen
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
zapéká	zapékat	k5eAaImIp3nS	zapékat
s	s	k7c7	s
mletými	mletý	k2eAgNnPc7d1	mleté
masy	maso	k1gNnPc7	maso
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc7	sýr
a	a	k8xC	a
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Růžičková	růžičkový	k2eAgFnSc1d1	růžičková
kapusta	kapusta	k1gFnSc1	kapusta
se	se	k3xPyFc4	se
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
převařená	převařený	k2eAgFnSc1d1	převařená
buď	buď	k8xC	buď
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
zeleninových	zeleninový	k2eAgInPc2d1	zeleninový
salátů	salát	k1gInPc2	salát
či	či	k8xC	či
polévek	polévka	k1gFnPc2	polévka
nebo	nebo	k8xC	nebo
dušená	dušený	k2eAgFnSc1d1	dušená
i	i	k8xC	i
zapékaná	zapékaný	k2eAgFnSc1d1	zapékaná
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kadeřávku	Kadeřávek	k1gMnSc3	Kadeřávek
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
kapusty	kapusta	k1gFnSc2	kapusta
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gInPc4	jeho
čerstvé	čerstvý	k2eAgInPc4d1	čerstvý
široké	široký	k2eAgInPc4d1	široký
listy	list	k1gInPc4	list
se	se	k3xPyFc4	se
také	také	k9	také
obalují	obalovat	k5eAaImIp3nP	obalovat
v	v	k7c6	v
těstíčku	těstíčko	k1gNnSc6	těstíčko
a	a	k8xC	a
smaží	smažit	k5eAaImIp3nS	smažit
na	na	k7c6	na
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
nebo	nebo	k8xC	nebo
pekingské	pekingský	k2eAgNnSc1d1	pekingské
zelí	zelí	k1gNnSc1	zelí
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
používá	používat	k5eAaImIp3nS	používat
za	za	k7c4	za
syrová	syrový	k2eAgNnPc4d1	syrové
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
zeleninových	zeleninový	k2eAgInPc2d1	zeleninový
salátů	salát	k1gInPc2	salát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
konzumace	konzumace	k1gFnSc1	konzumace
košťálové	košťálový	k2eAgFnSc2d1	košťálová
zeleniny	zelenina	k1gFnSc2	zelenina
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
plynatostí	plynatost	k1gFnSc7	plynatost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zelenina	zelenina	k1gFnSc1	zelenina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
komplex	komplex	k1gInSc4	komplex
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
žaludek	žaludek	k1gInSc4	žaludek
ani	ani	k8xC	ani
tenké	tenký	k2eAgNnSc4d1	tenké
střevo	střevo	k1gNnSc4	střevo
nedokážou	dokázat	k5eNaPmIp3nP	dokázat
zcela	zcela	k6eAd1	zcela
rozložit	rozložit	k5eAaPmF	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
tamní	tamní	k2eAgFnPc1d1	tamní
bakterie	bakterie	k1gFnPc1	bakterie
je	on	k3xPp3gMnPc4	on
štěpí	štěpit	k5eAaImIp3nP	štěpit
a	a	k8xC	a
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
sirovodík	sirovodík	k1gInSc4	sirovodík
a	a	k8xC	a
metan	metan	k1gInSc4	metan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
===	===	k?	===
</s>
</p>
<p>
<s>
Brokolice	brokolice	k1gFnSc1	brokolice
<g/>
,	,	kIx,	,
květák	květák	k1gInSc1	květák
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
růžičková	růžičkový	k2eAgFnSc1d1	růžičková
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc1	zelí
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
košťálová	košťálový	k2eAgFnSc1d1	košťálová
zelenina	zelenina	k1gFnSc1	zelenina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
isothiocyanáty	isothiocyanát	k1gInPc7	isothiocyanát
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
–	–	k?	–
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
enzymům	enzym	k1gInPc3	enzym
vedoucím	vedoucí	k1gFnPc3	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nádorových	nádorový	k2eAgFnPc2d1	nádorová
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
prevenci	prevence	k1gFnSc3	prevence
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dusičnany	dusičnan	k1gInPc1	dusičnan
a	a	k8xC	a
dusitany	dusitan	k1gInPc1	dusitan
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
košťálové	košťálový	k2eAgFnSc6d1	košťálová
zelenině	zelenina	k1gFnSc6	zelenina
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
osobám	osoba	k1gFnPc3	osoba
po	po	k7c6	po
infarktu	infarkt	k1gInSc6	infarkt
k	k	k7c3	k
rychlejšímu	rychlý	k2eAgNnSc3d2	rychlejší
zotavení	zotavení	k1gNnSc3	zotavení
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
omezují	omezovat	k5eAaImIp3nP	omezovat
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
míru	míra	k1gFnSc4	míra
poškození	poškození	k1gNnSc2	poškození
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dále	daleko	k6eAd2	daleko
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
DNA	DNA	kA	DNA
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Košťálová	košťálový	k2eAgFnSc1d1	košťálová
zelenina	zelenina	k1gFnSc1	zelenina
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
vlákniny	vláknina	k1gFnPc4	vláknina
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
a	a	k8xC	a
draslík	draslík	k1gInSc1	draslík
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitamíny	vitamín	k1gInPc1	vitamín
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
provitamín	provitamín	k1gInSc4	provitamín
A	a	k9	a
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
listovou	listový	k2eAgFnSc4d1	listová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vaření	vaření	k1gNnSc1	vaření
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Zelenina-Kadlec	Zelenina-Kadlec	k1gInSc1	Zelenina-Kadlec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Semo	Semo	k?	Semo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
