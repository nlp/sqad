<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ש	ש	k?	ש
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ה	ה	k?	ה
<g/>
,	,	kIx,	,
Moše	mocha	k1gFnSc6	mocha
<g/>
,	,	kIx,	,
vykládáno	vykládat	k5eAaImNgNnS	vykládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
vytažený	vytažený	k2eAgMnSc1d1	vytažený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
postav	postava	k1gFnPc2	postava
izraelského	izraelský	k2eAgInSc2d1	izraelský
národa	národ	k1gInSc2	národ
a	a	k8xC	a
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
náboženský	náboženský	k2eAgInSc1d1	náboženský
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
,	,	kIx,	,
starozákonní	starozákonní	k2eAgMnSc1d1	starozákonní
prorok	prorok	k1gMnSc1	prorok
<g/>
,	,	kIx,	,
vykladač	vykladač	k1gMnSc1	vykladač
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
připisováno	připisovat	k5eAaImNgNnS	připisovat
autorství	autorství	k1gNnSc1	autorství
Tóry	tóra	k1gFnSc2	tóra
-	-	kIx~	-
pěti	pět	k4xCc3	pět
knih	kniha	k1gFnPc2	kniha
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
:	:	kIx,	:
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
Exodus	Exodus	k1gInSc1	Exodus
<g/>
,	,	kIx,	,
Leviticus	Leviticus	k1gInSc1	Leviticus
<g/>
,	,	kIx,	,
Numeri	Numeri	k1gNnSc1	Numeri
a	a	k8xC	a
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k9	jako
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
okolo	okolo	k7c2	okolo
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Levi	Lev	k1gFnSc2	Lev
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
Árón	Árón	k1gMnSc1	Árón
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
Mirjam	Mirjam	k1gFnSc7	Mirjam
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
existence	existence	k1gFnSc1	existence
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložena	doložen	k2eAgFnSc1d1	doložena
<g/>
,	,	kIx,	,
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
či	či	k8xC	či
na	na	k7c6	na
Sinaji	Sinaj	k1gInSc6	Sinaj
nejsou	být	k5eNaImIp3nP	být
důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
události	událost	k1gFnPc4	událost
popsané	popsaný	k2eAgFnPc4d1	popsaná
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Exodus	Exodus	k1gInSc4	Exodus
popisující	popisující	k2eAgNnPc4d1	popisující
otroctví	otroctví	k1gNnPc4	otroctví
Izraelitů	izraelita	k1gMnPc2	izraelita
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
jako	jako	k8xC	jako
proroka	prorok	k1gMnSc4	prorok
uznává	uznávat	k5eAaImIp3nS	uznávat
několik	několik	k4yIc4	několik
světových	světový	k2eAgNnPc2d1	světové
náboženství	náboženství	k1gNnPc2	náboženství
–	–	k?	–
například	například	k6eAd1	například
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
mormonismus	mormonismus	k1gInSc1	mormonismus
<g/>
,	,	kIx,	,
bahaismus	bahaismus	k1gInSc1	bahaismus
i	i	k8xC	i
moonismus	moonismus	k1gInSc1	moonismus
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšův	Mojžíšův	k2eAgInSc4d1	Mojžíšův
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
etapy	etapa	k1gFnPc4	etapa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
trvala	trvat	k5eAaImAgFnS	trvat
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biblického	biblický	k2eAgNnSc2d1	biblické
vyprávění	vyprávění	k1gNnSc2	vyprávění
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
židovským	židovská	k1gFnPc3	židovská
rodičům	rodič	k1gMnPc3	rodič
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
žil	žít	k5eAaImAgMnS	žít
celý	celý	k2eAgInSc4d1	celý
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nařízení	nařízení	k1gNnSc3	nařízení
faraona	faraon	k1gMnSc2	faraon
zabít	zabít	k5eAaPmF	zabít
všechny	všechen	k3xTgMnPc4	všechen
malé	malý	k2eAgMnPc4d1	malý
chlapce	chlapec	k1gMnPc4	chlapec
-	-	kIx~	-
syny	syn	k1gMnPc7	syn
narozené	narozený	k2eAgNnSc4d1	narozené
Izraelitům	izraelita	k1gMnPc3	izraelita
–	–	k?	–
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
uchránili	uchránit	k5eAaPmAgMnP	uchránit
tohoto	tento	k3xDgInSc2	tento
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
vložili	vložit	k5eAaPmAgMnP	vložit
do	do	k7c2	do
třtinové	třtinový	k2eAgFnSc2d1	třtinová
ošatky	ošatka	k1gFnSc2	ošatka
a	a	k8xC	a
pustili	pustit	k5eAaPmAgMnP	pustit
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
novorozence	novorozenec	k1gMnPc4	novorozenec
nalezla	naleznout	k5eAaPmAgFnS	naleznout
faraonova	faraonův	k2eAgFnSc1d1	faraonova
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kojná	kojná	k1gFnSc1	kojná
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
,	,	kIx,	,
hájil	hájit	k5eAaImAgMnS	hájit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
sporu	spor	k1gInSc6	spor
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
otroka	otrok	k1gMnSc2	otrok
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
zabil	zabít	k5eAaPmAgMnS	zabít
faraonova	faraonův	k2eAgMnSc4d1	faraonův
služebníka	služebník	k1gMnSc4	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
tedy	tedy	k9	tedy
utéci	utéct	k5eAaPmF	utéct
mimo	mimo	k7c4	mimo
Egypt	Egypt	k1gInSc4	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
v	v	k7c6	v
Midjanu	Midjan	k1gInSc6	Midjan
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
Siporou	Sipora	k1gFnSc7	Sipora
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
midjánského	midjánský	k2eAgMnSc2d1	midjánský
kněze	kněz	k1gMnSc2	kněz
Jetra	Jetr	k1gMnSc2	Jetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
hořícím	hořící	k2eAgInSc6d1	hořící
keři	keř	k1gInSc6	keř
zjevil	zjevit	k5eAaPmAgMnS	zjevit
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
vysvobodit	vysvobodit	k5eAaPmF	vysvobodit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
otroctví	otroctví	k1gNnSc2	otroctví
a	a	k8xC	a
přivést	přivést	k5eAaPmF	přivést
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
zjeveno	zjevit	k5eAaPmNgNnS	zjevit
Boží	boží	k2eAgNnSc1d1	boží
jméno	jméno	k1gNnSc1	jméno
JHVH	JHVH	kA	JHVH
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
faraonem	faraon	k1gMnSc7	faraon
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Bůh	bůh	k1gMnSc1	bůh
skrze	skrze	k?	skrze
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
potrestal	potrestat	k5eAaPmAgInS	potrestat
Egypt	Egypt	k1gInSc1	Egypt
deseti	deset	k4xCc7	deset
ranami	rána	k1gFnPc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vyhubení	vyhubení	k1gNnSc1	vyhubení
všech	všecek	k3xTgMnPc2	všecek
prvorozených	prvorozený	k2eAgMnPc2d1	prvorozený
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
i	i	k8xC	i
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
této	tento	k3xDgFnSc6	tento
zkázy	zkáza	k1gFnSc2	zkáza
uchráněni	uchránit	k5eAaPmNgMnP	uchránit
díky	díky	k7c3	díky
krvi	krev	k1gFnSc3	krev
obětovaného	obětovaný	k2eAgMnSc2d1	obětovaný
beránka	beránek	k1gMnSc2	beránek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
události	událost	k1gFnSc3	událost
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
vznik	vznik	k1gInSc4	vznik
svátku	svátek	k1gInSc2	svátek
Pesach	pesach	k1gInSc1	pesach
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
faraon	faraon	k1gInSc4	faraon
Izraelity	izraelita	k1gMnSc2	izraelita
propustil	propustit	k5eAaPmAgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
rozmyslel	rozmyslet	k5eAaPmAgMnS	rozmyslet
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
Izraelity	izraelita	k1gMnSc2	izraelita
s	s	k7c7	s
Mojžíšem	Mojžíš	k1gMnSc7	Mojžíš
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
Bůh	bůh	k1gMnSc1	bůh
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
přejít	přejít	k5eAaPmF	přejít
napříč	napříč	k7c7	napříč
Rákosovým	rákosový	k2eAgNnSc7d1	rákosové
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Egypťané	Egypťan	k1gMnPc1	Egypťan
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
utopili	utopit	k5eAaPmAgMnP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
provedl	provést	k5eAaPmAgMnS	provést
izraelský	izraelský	k2eAgInSc4d1	izraelský
lid	lid	k1gInSc4	lid
pouští	poušť	k1gFnPc2	poušť
a	a	k8xC	a
po	po	k7c6	po
čtyřicetiletém	čtyřicetiletý	k2eAgNnSc6d1	čtyřicetileté
putování	putování	k1gNnSc6	putování
jej	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
Moabu	Moab	k1gInSc2	Moab
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
vůdce	vůdce	k1gMnSc1	vůdce
Izraele	Izrael	k1gInSc2	Izrael
Jozue	Jozue	k1gMnSc1	Jozue
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lid	lid	k1gInSc4	lid
převedl	převést	k5eAaPmAgMnS	převést
přes	přes	k7c4	přes
Jordán	Jordán	k1gInSc4	Jordán
do	do	k7c2	do
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
Kanaánu	Kanaán	k1gInSc2	Kanaán
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Polygamie	polygamie	k1gFnSc2	polygamie
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
a	a	k8xC	a
Polygamie	polygamie	k1gFnSc1	polygamie
v	v	k7c6	v
mormonismu	mormonismus	k1gInSc6	mormonismus
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
měl	mít	k5eAaImAgMnS	mít
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
pouze	pouze	k6eAd1	pouze
Siporu	Sipora	k1gFnSc4	Sipora
a	a	k8xC	a
záhadnou	záhadný	k2eAgFnSc4d1	záhadná
kúšanku	kúšanka	k1gFnSc4	kúšanka
<g/>
,	,	kIx,	,
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
v	v	k7c6	v
Nu	nu	k9	nu
12	[number]	k4	12
<g/>
,	,	kIx,	,
1	[number]	k4	1
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Soudců	soudce	k1gMnPc2	soudce
je	být	k5eAaImIp3nS	být
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
ještě	ještě	k6eAd1	ještě
jiná	jiný	k2eAgFnSc1d1	jiná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
s	s	k7c7	s
kúšankou	kúšanka	k1gFnSc7	kúšanka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
církve	církev	k1gFnPc1	církev
využívají	využívat	k5eAaImIp3nP	využívat
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
Mojžíšově	Mojžíšův	k2eAgFnSc6d1	Mojžíšova
polygamii	polygamie	k1gFnSc6	polygamie
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
učení	učení	k1gNnSc3	učení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
polygamie	polygamie	k1gFnSc2	polygamie
v	v	k7c6	v
mormonismu	mormonismus	k1gInSc6	mormonismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštně	zvláštně	k6eAd1	zvláštně
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
Chóbab	Chóbab	k1gMnSc1	Chóbab
chtěl	chtít	k5eAaImAgMnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
ho	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
průvodcem	průvodce	k1gMnSc7	průvodce
Izraelců	Izraelec	k1gMnPc2	Izraelec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zná	znát	k5eAaImIp3nS	znát
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
utábořit	utábořit	k5eAaPmF	utábořit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
vede	vést	k5eAaImIp3nS	vést
Hospodinův	Hospodinův	k2eAgInSc1d1	Hospodinův
oblak	oblak	k1gInSc1	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
rozpor	rozpor	k1gInSc1	rozpor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
finální	finální	k2eAgFnSc6d1	finální
redakci	redakce	k1gFnSc6	redakce
Mojžíšových	Mojžíšových	k2eAgFnPc2d1	Mojžíšových
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
Tóry	tóra	k1gFnSc2	tóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Urim	Urima	k1gFnPc2	Urima
a	a	k8xC	a
Thummim	Thummima	k1gFnPc2	Thummima
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
také	také	k6eAd1	také
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Árona	Áron	k1gMnSc4	Áron
za	za	k7c4	za
velekněze	velekněz	k1gMnSc4	velekněz
izraelského	izraelský	k2eAgInSc2d1	izraelský
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
Jahveho	Jahve	k1gMnSc2	Jahve
dal	dát	k5eAaPmAgMnS	dát
náprsník	náprsník	k1gInSc4	náprsník
s	s	k7c7	s
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
reprezentujícími	reprezentující	k2eAgInPc7d1	reprezentující
12	[number]	k4	12
kmenů	kmen	k1gInPc2	kmen
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
2	[number]	k4	2
posvátné	posvátný	k2eAgInPc4d1	posvátný
věštecké	věštecký	k2eAgInPc4d1	věštecký
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
Urim	Urim	k1gInSc4	Urim
a	a	k8xC	a
Thummim	Thummim	k1gInSc4	Thummim
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejrozšířenější	rozšířený	k2eAgNnSc1d3	nejrozšířenější
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
faraon	faraon	k1gInSc1	faraon
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
žádal	žádat	k5eAaImAgMnS	žádat
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
propuštění	propuštění	k1gNnSc2	propuštění
svého	svůj	k3xOyFgInSc2	svůj
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Ramesse	Ramess	k1gMnSc4	Ramess
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1279	[number]	k4	1279
<g/>
–	–	k?	–
<g/>
1213	[number]	k4	1213
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
kandidátem	kandidát	k1gMnSc7	kandidát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
nějak	nějak	k6eAd1	nějak
vysledovat	vysledovat	k5eAaPmF	vysledovat
Mojžíše	Mojžíš	k1gMnPc4	Mojžíš
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
známých	známý	k2eAgFnPc2d1	známá
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
určení	určení	k1gNnSc2	určení
Exodu	Exodus	k1gInSc6	Exodus
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1648	[number]	k4	1648
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
nástup	nástup	k1gInSc1	nástup
15	[number]	k4	15
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
)	)	kIx)	)
až	až	k9	až
1208	[number]	k4	1208
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
pátý	pátý	k4xOgInSc1	pátý
rok	rok	k1gInSc1	rok
vlády	vláda	k1gFnSc2	vláda
syna	syn	k1gMnSc2	syn
Rameese	Rameese	k1gFnSc2	Rameese
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Merenptaha	Merenptaha	k1gFnSc1	Merenptaha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Monotheismus	monotheismus	k1gInSc1	monotheismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
intervalů	interval	k1gInPc2	interval
Mojžíšova	Mojžíšův	k2eAgInSc2d1	Mojžíšův
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
také	také	k9	také
zároveň	zároveň	k6eAd1	zároveň
nachází	nacházet	k5eAaImIp3nS	nacházet
období	období	k1gNnSc4	období
panování	panování	k1gNnSc2	panování
Amenhotepa	Amenhotep	k1gMnSc2	Amenhotep
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
známějšího	známý	k2eAgNnSc2d2	známější
jako	jako	k9	jako
Achnaton	Achnaton	k1gInSc1	Achnaton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
provedl	provést	k5eAaPmAgInS	provést
radikální	radikální	k2eAgFnSc4d1	radikální
monotheizující	monotheizující	k2eAgFnSc4d1	monotheizující
reformu	reforma	k1gFnSc4	reforma
egyptského	egyptský	k2eAgNnSc2d1	egyptské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
značně	značně	k6eAd1	značně
pacifistickému	pacifistický	k2eAgNnSc3d1	pacifistické
založení	založení	k1gNnSc3	založení
se	se	k3xPyFc4	se
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
prchající	prchající	k2eAgMnPc4d1	prchající
Židy	Žid	k1gMnPc4	Žid
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
tu	tu	k6eAd1	tu
otázka	otázka	k1gFnSc1	otázka
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
vlivu	vliv	k1gInSc2	vliv
atonismu	atonismus	k1gInSc2	atonismus
a	a	k8xC	a
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Achnatonův	Achnatonův	k2eAgInSc1d1	Achnatonův
Hymnus	hymnus	k1gInSc1	hymnus
na	na	k7c4	na
Slunce	slunce	k1gNnSc4	slunce
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
podobá	podobat	k5eAaImIp3nS	podobat
104	[number]	k4	104
<g/>
.	.	kIx.	.
žalmu	žalm	k1gInSc2	žalm
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
text	text	k1gInSc4	text
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
z	z	k7c2	z
kterého	který	k3yIgNnSc2	který
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
názorů	názor	k1gInPc2	názor
tu	tu	k6eAd1	tu
však	však	k9	však
je	být	k5eAaImIp3nS	být
i	i	k9	i
teorie	teorie	k1gFnSc1	teorie
možné	možný	k2eAgFnSc2d1	možná
existence	existence	k1gFnSc2	existence
jiné	jiný	k2eAgFnSc2d1	jiná
obecné	obecný	k2eAgFnSc2d1	obecná
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc1	tento
texty	text	k1gInPc1	text
čerpaly	čerpat	k5eAaImAgInP	čerpat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
výraznější	výrazný	k2eAgFnSc1d2	výraznější
je	být	k5eAaImIp3nS	být
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
<g/>
)	)	kIx)	)
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
judaismem	judaismus	k1gInSc7	judaismus
a	a	k8xC	a
atonismem	atonismus	k1gInSc7	atonismus
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
současníkem	současník	k1gMnSc7	současník
této	tento	k3xDgFnSc2	tento
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
atonismu	atonismus	k1gInSc6	atonismus
počátky	počátek	k1gInPc4	počátek
monoteistických	monoteistický	k2eAgInPc2d1	monoteistický
světových	světový	k2eAgInPc2d1	světový
náboženství	náboženství	k1gNnPc1	náboženství
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
snahy	snaha	k1gFnPc1	snaha
zmapovat	zmapovat	k5eAaPmF	zmapovat
putování	putování	k1gNnSc4	putování
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
<g/>
,	,	kIx,	,
především	především	k9	především
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
Izraelitů	izraelita	k1gMnPc2	izraelita
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
a	a	k8xC	a
nalézt	nalézt	k5eAaBmF	nalézt
tak	tak	k6eAd1	tak
místo	místo	k7c2	místo
přechodu	přechod	k1gInSc2	přechod
Rákosového	rákosový	k2eAgNnSc2d1	rákosové
<g/>
/	/	kIx~	/
<g/>
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rona	Ron	k1gMnSc2	Ron
Wyatta	Wyatt	k1gMnSc2	Wyatt
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
přechodu	přechod	k1gInSc2	přechod
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pláže	pláž	k1gFnSc2	pláž
u	u	k7c2	u
města	město	k1gNnSc2	město
Nuweiba	Nuweib	k1gMnSc2	Nuweib
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
podporu	podpora	k1gFnSc4	podpora
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
pláž	pláž	k1gFnSc1	pláž
je	být	k5eAaImIp3nS	být
i	i	k9	i
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
mohli	moct	k5eAaImAgMnP	moct
Izraelité	izraelita	k1gMnPc1	izraelita
utábořit	utábořit	k5eAaPmF	utábořit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
naopak	naopak	k6eAd1	naopak
pláž	pláž	k1gFnSc1	pláž
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
putování	putování	k1gNnSc2	putování
podle	podle	k7c2	podle
Rona	Ron	k1gMnSc2	Ron
Wyatta	Wyatt	k1gMnSc2	Wyatt
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
poloha	poloha	k1gFnSc1	poloha
Ethamu	Etham	k1gInSc2	Etham
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
i	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
hloubka	hloubka	k1gFnSc1	hloubka
podmořské	podmořský	k2eAgFnSc2d1	podmořská
vyvýšeniny	vyvýšenina	k1gFnSc2	vyvýšenina
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
Izraelité	izraelita	k1gMnPc1	izraelita
mohli	moct	k5eAaImAgMnP	moct
přejít	přejít	k5eAaPmF	přejít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubka	hloubka	k1gFnSc1	hloubka
až	až	k9	až
765	[number]	k4	765
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
Izraelité	izraelita	k1gMnPc1	izraelita
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
překonat	překonat	k5eAaPmF	překonat
17	[number]	k4	17
<g/>
stupňové	stupňový	k2eAgFnPc4d1	stupňová
stoupání	stoupání	k1gNnPc4	stoupání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
přechodu	přechod	k1gInSc2	přechod
je	být	k5eAaImIp3nS	být
navrhována	navrhován	k2eAgFnSc1d1	navrhována
Tiranská	tiranský	k2eAgFnSc1d1	Tiranská
úžina	úžina	k1gFnSc1	úžina
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
pláže	pláž	k1gFnSc2	pláž
u	u	k7c2	u
města	město	k1gNnSc2	město
Nuweiba	Nuweiba	k1gFnSc1	Nuweiba
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
Ted	Ted	k1gMnSc1	Ted
Charlton	Charlton	k1gInSc4	Charlton
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vodní	vodní	k2eAgFnPc1d1	vodní
hradby	hradba	k1gFnPc1	hradba
sloužící	sloužící	k1gFnSc1	sloužící
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
přecházejících	přecházející	k2eAgMnPc2d1	přecházející
Židů	Žid	k1gMnPc2	Žid
byla	být	k5eAaImAgFnS	být
voda	voda	k1gFnSc1	voda
zledovatělá	zledovatělý	k2eAgFnSc1d1	zledovatělá
díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
východnímu	východní	k2eAgInSc3d1	východní
větru	vítr	k1gInSc3	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
jako	jako	k9	jako
místo	místo	k7c2	místo
přechodu	přechod	k1gInSc2	přechod
Tiranskou	tiranský	k2eAgFnSc4d1	Tiranská
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
úžině	úžina	k1gFnSc6	úžina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
bible	bible	k1gFnSc1	bible
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
je	být	k5eAaImIp3nS	být
překlad	překlad	k1gInSc1	překlad
Rákosového	rákosový	k2eAgNnSc2d1	rákosové
moře	moře	k1gNnSc2	moře
jako	jako	k8xC	jako
Rudé	rudý	k1gMnPc4	rudý
moře	moře	k1gNnSc2	moře
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
chybný	chybný	k2eAgInSc4d1	chybný
a	a	k8xC	a
Rákosové	rákosový	k2eAgNnSc4d1	rákosové
moře	moře	k1gNnSc4	moře
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Suezské	suezský	k2eAgFnSc2d1	Suezská
šíje	šíj	k1gFnSc2	šíj
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gInSc4	on
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
se	s	k7c7	s
Sirbónským	Sirbónský	k2eAgNnSc7d1	Sirbónský
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nauka	nauka	k1gFnSc1	nauka
a	a	k8xC	a
Smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Mormonismus	Mormonismus	k1gInSc1	Mormonismus
(	(	kIx(	(
<g/>
specifické	specifický	k2eAgNnSc1d1	specifické
americké	americký	k2eAgNnSc1d1	americké
odvětví	odvětví	k1gNnSc1	odvětví
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
postavu	postava	k1gFnSc4	postava
proroka	prorok	k1gMnSc2	prorok
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
teologii	teologie	k1gFnSc6	teologie
i	i	k8xC	i
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Nauky	nauka	k1gFnSc2	nauka
a	a	k8xC	a
Smluv	smlouva	k1gFnPc2	smlouva
se	se	k3xPyFc4	se
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
zjevil	zjevit	k5eAaPmAgMnS	zjevit
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1836	[number]	k4	1836
mormonskému	mormonský	k2eAgMnSc3d1	mormonský
prorokovi	prorok	k1gMnSc3	prorok
Josephu	Joseph	k1gMnSc3	Joseph
Smithovi	Smith	k1gMnSc3	Smith
v	v	k7c6	v
kirtlandském	kirtlandský	k2eAgInSc6d1	kirtlandský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
tohoto	tento	k3xDgNnSc2	tento
zjevení	zjevení	k1gNnSc2	zjevení
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
předání	předání	k1gNnSc1	předání
kněžských	kněžský	k2eAgInPc2d1	kněžský
klíčů	klíč	k1gInPc2	klíč
ke	k	k7c3	k
shromáždění	shromáždění	k1gNnSc3	shromáždění
všech	všecek	k3xTgInPc2	všecek
kmenů	kmen	k1gInPc2	kmen
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Nauka	nauka	k1gFnSc1	nauka
a	a	k8xC	a
Smlouvy	smlouva	k1gFnPc1	smlouva
110	[number]	k4	110
Mormonský	mormonský	k2eAgMnSc1d1	mormonský
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
učení	učení	k1gNnSc2	učení
teologa	teolog	k1gMnSc2	teolog
Smithe	Smithe	k1gNnSc2	Smithe
vyvolen	vyvolit	k5eAaPmNgMnS	vyvolit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
úkolu	úkol	k1gInSc3	úkol
v	v	k7c6	v
předsmrtelném	předsmrtelný	k2eAgInSc6d1	předsmrtelný
životě	život	k1gInSc6	život
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
příchod	příchod	k1gInSc1	příchod
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Knihy	kniha	k1gFnSc2	kniha
Mormonovy	mormonův	k2eAgFnSc2d1	Mormonova
prorokován	prorokován	k2eAgInSc1d1	prorokován
už	už	k9	už
Josefem	Josef	k1gMnSc7	Josef
egyptským	egyptský	k2eAgMnSc7d1	egyptský
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Kniha	kniha	k1gFnSc1	kniha
Mormonova	mormonův	k2eAgFnSc1d1	Mormonova
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Nefi	Nef	k1gInSc3	Nef
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgMnSc7	svůj
tchánem	tchán	k1gMnSc7	tchán
Jetrem	Jetr	k1gMnSc7	Jetr
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
k	k	k7c3	k
Melchisedechově	Melchisedechův	k2eAgNnSc6d1	Melchisedechovo
kněžství	kněžství	k1gNnSc1	kněžství
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
učil	učit	k5eAaImAgInS	učit
také	také	k9	také
Izraelity	izraelita	k1gMnPc4	izraelita
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kněžství	kněžství	k1gNnSc1	kněžství
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
za	za	k7c4	za
nevíru	nevíra	k1gFnSc4	nevíra
Izraelitům	izraelita	k1gMnPc3	izraelita
odebráno	odebrat	k5eAaPmNgNnS	odebrat
a	a	k8xC	a
kmenům	kmen	k1gInPc3	kmen
tak	tak	k9	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pouze	pouze	k6eAd1	pouze
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
,	,	kIx,	,
Aaronovo	Aaronův	k2eAgNnSc1d1	Aaronovo
kněžství	kněžství	k1gNnSc1	kněžství
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Nauka	nauka	k1gFnSc1	nauka
a	a	k8xC	a
Smlouvy	smlouva	k1gFnPc1	smlouva
84	[number]	k4	84
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Proroci	prorok	k1gMnPc1	prorok
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
prorockou	prorocký	k2eAgFnSc7d1	prorocká
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
islámské	islámský	k2eAgFnSc6d1	islámská
tradici	tradice	k1gFnSc6	tradice
a	a	k8xC	a
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	se	k3xPyFc4	se
s	s	k7c7	s
nim	on	k3xPp3gInPc3	on
nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3	nejrozsáhlejší
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
najdeme	najít	k5eAaPmIp1nP	najít
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Korán	korán	k1gInSc1	korán
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
Mojžíšův	Mojžíšův	k2eAgInSc1d1	Mojžíšův
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
paralelou	paralela	k1gFnSc7	paralela
k	k	k7c3	k
Mohamedově	Mohamedův	k2eAgFnSc3d1	Mohamedova
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
"	"	kIx"	"
<g/>
hidžru	hidžra	k1gFnSc4	hidžra
<g/>
"	"	kIx"	"
-	-	kIx~	-
exodus	exodus	k1gInSc1	exodus
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgMnS	přinést
Písmo	písmo	k1gNnSc4	písmo
(	(	kIx(	(
<g/>
Tóru	tóra	k1gFnSc4	tóra
<g/>
)	)	kIx)	)
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
7	[number]	k4	7
nebí	nebe	k1gNnPc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Islámské	islámský	k2eAgInPc4d1	islámský
zdroje	zdroj	k1gInPc4	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
egyptský	egyptský	k2eAgInSc1d1	egyptský
Faraon	faraon	k1gInSc1	faraon
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Mojžíšova	Mojžíšův	k2eAgNnSc2d1	Mojžíšovo
narození	narození	k1gNnSc2	narození
sen	sen	k1gInSc1	sen
o	o	k7c6	o
ohni	oheň	k1gInSc6	oheň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
spálil	spálit	k5eAaPmAgMnS	spálit
vše	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
kromě	kromě	k7c2	kromě
Izraelců	Izraelec	k1gMnPc2	Izraelec
<g/>
.	.	kIx.	.
</s>
<s>
Egyptští	egyptský	k2eAgMnPc1d1	egyptský
kouzelníci	kouzelník	k1gMnPc1	kouzelník
a	a	k8xC	a
vykladači	vykladač	k1gMnPc1	vykladač
snů	sen	k1gInPc2	sen
Faraonovi	faraon	k1gMnSc3	faraon
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nP	narodit
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Izraelce	Izraelec	k1gMnPc4	Izraelec
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
a	a	k8xC	a
uvrhne	uvrhnout	k5eAaPmIp3nS	uvrhnout
na	na	k7c4	na
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
zemi	zem	k1gFnSc4	zem
zkázu	zkáza	k1gFnSc4	zkáza
a	a	k8xC	a
katastrofy	katastrofa	k1gFnPc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Faraon	faraon	k1gMnSc1	faraon
proto	proto	k8xC	proto
přikázal	přikázat	k5eAaPmAgMnS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
každý	každý	k3xTgMnSc1	každý
nově	nově	k6eAd1	nově
narozený	narozený	k2eAgMnSc1d1	narozený
izraelský	izraelský	k2eAgMnSc1d1	izraelský
chlapec	chlapec	k1gMnSc1	chlapec
okamžitě	okamžitě	k6eAd1	okamžitě
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
matka	matka	k1gFnSc1	matka
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
zjevení	zjevení	k1gNnSc2	zjevení
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
Egypťané	Egypťan	k1gMnPc1	Egypťan
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
jí	jíst	k5eAaImIp3nS	jíst
poté	poté	k6eAd1	poté
Mojžíše	Mojžíš	k1gMnPc4	Mojžíš
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k8xC	jako
adoptovaného	adoptovaný	k2eAgMnSc2d1	adoptovaný
královského	královský	k2eAgMnSc2d1	královský
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Korán	korán	k1gInSc1	korán
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
Faraonovi	faraonův	k2eAgMnPc1d1	faraonův
poradci	poradce	k1gMnPc1	poradce
později	pozdě	k6eAd2	pozdě
egyptskému	egyptský	k2eAgMnSc3d1	egyptský
králi	král	k1gMnSc3	král
poradili	poradit	k5eAaPmAgMnP	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nezabíjeli	zabíjet	k5eNaImAgMnP	zabíjet
mužská	mužský	k2eAgNnPc4d1	mužské
novorozeňata	novorozeně	k1gNnPc4	novorozeně
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
takto	takto	k6eAd1	takto
přeživších	přeživší	k2eAgMnPc2d1	přeživší
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Mojžíšův	Mojžíšův	k2eAgMnSc1d1	Mojžíšův
bratr	bratr	k1gMnSc1	bratr
Áron	áron	k1gInSc1	áron
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
neznámého	známý	k2eNgMnSc4d1	neznámý
Egypťana	Egypťan	k1gMnSc4	Egypťan
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Midjánu	Midján	k1gInSc2	Midján
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poznal	poznat	k5eAaPmAgMnS	poznat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
Sipporu	Sippora	k1gFnSc4	Sippora
i	i	k8xC	i
svého	svůj	k3xOyFgMnSc4	svůj
budoucího	budoucí	k2eAgMnSc4d1	budoucí
tchána	tchán	k1gMnSc4	tchán
<g/>
,	,	kIx,	,
Jetra	Jetra	k1gMnSc1	Jetra
(	(	kIx(	(
<g/>
Šuajba	Šuajba	k1gMnSc1	Šuajba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Korán	korán	k1gInSc1	korán
28	[number]	k4	28
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jetro	Jetro	k1gNnSc1	Jetro
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
islámské	islámský	k2eAgFnSc2d1	islámská
tradice	tradice	k1gFnSc2	tradice
i	i	k8xC	i
Koránu	korán	k1gInSc2	korán
stal	stát	k5eAaPmAgInS	stát
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
<g/>
)	)	kIx)	)
Jetrovým	Jetrův	k2eAgMnSc7d1	Jetrův
zetěm	zeť	k1gMnSc7	zeť
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Midjánu	Midján	k1gInSc6	Midján
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Korán	korán	k1gInSc1	korán
28	[number]	k4	28
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teofanie	Teofanie	k1gFnSc2	Teofanie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
byl	být	k5eAaImAgMnS	být
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
povolán	povolat	k5eAaPmNgMnS	povolat
za	za	k7c4	za
proroka	prorok	k1gMnSc4	prorok
<g/>
.	.	kIx.	.
</s>
<s>
Korán	korán	k1gInSc1	korán
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
(	(	kIx(	(
<g/>
známou	známá	k1gFnSc4	známá
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
jako	jako	k8xC	jako
scéna	scéna	k1gFnSc1	scéna
s	s	k7c7	s
hořícím	hořící	k2eAgInSc7d1	hořící
keřem	keř	k1gInSc7	keř
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Korán	korán	k1gInSc1	korán
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
Korán	korán	k1gInSc1	korán
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k9	ještě
další	další	k2eAgFnSc1d1	další
verze	verze	k1gFnSc1	verze
příběhu	příběh	k1gInSc2	příběh
s	s	k7c7	s
hořícím	hořící	k2eAgInSc7d1	hořící
keřem	keř	k1gInSc7	keř
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
Izrael	Izrael	k1gInSc4	Izrael
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
skrze	skrze	k?	skrze
něj	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
Bůh	bůh	k1gMnSc1	bůh
Izraeli	Izrael	k1gMnSc3	Izrael
na	na	k7c4	na
hoře	hoře	k1gNnSc4	hoře
Sinaj	Sinaj	k1gInSc4	Sinaj
Desatero	desatero	k1gNnSc1	desatero
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
stal	stát	k5eAaPmAgInS	stát
Božím	boží	k2eAgInSc7d1	boží
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Izrael	Izrael	k1gInSc4	Izrael
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
tehdy	tehdy	k6eAd1	tehdy
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
nacházejí	nacházet	k5eAaImIp3nP	nacházet
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c6	o
těch	ten	k3xDgFnPc6	ten
událostech	událost	k1gFnPc6	událost
<g/>
)	)	kIx)	)
v	v	k7c6	v
Tóře	tóra	k1gFnSc6	tóra
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
Pentateuchu	pentateuch	k1gInSc2	pentateuch
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
Pět	pět	k4xCc1	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tradice	tradice	k1gFnSc1	tradice
jejich	jejich	k3xOp3gNnSc2	jejich
autorství	autorství	k1gNnSc2	autorství
připisuje	připisovat	k5eAaImIp3nS	připisovat
právě	právě	k9	právě
Mojžíšovi	Mojžíšův	k2eAgMnPc1d1	Mojžíšův
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
často	často	k6eAd1	často
o	o	k7c6	o
"	"	kIx"	"
<g/>
Mojžíšově	Mojžíšův	k2eAgInSc6d1	Mojžíšův
zákoně	zákon	k1gInSc6	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
židovství	židovství	k1gNnSc4	židovství
nepřekonatelný	překonatelný	k2eNgMnSc1d1	nepřekonatelný
prorok	prorok	k1gMnSc1	prorok
a	a	k8xC	a
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
<g/>
,	,	kIx,	,
vzor	vzor	k1gInSc1	vzor
a	a	k8xC	a
model	model	k1gInSc1	model
každého	každý	k3xTgMnSc4	každý
proroka	prorok	k1gMnSc4	prorok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
chápe	chápat	k5eAaImIp3nS	chápat
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
učení	učení	k1gNnSc4	učení
jako	jako	k8xS	jako
překonání	překonání	k1gNnSc4	překonání
(	(	kIx(	(
<g/>
dovedení	dovedení	k1gNnSc4	dovedení
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
,	,	kIx,	,
naplnění	naplnění	k1gNnSc4	naplnění
<g/>
)	)	kIx)	)
Mojžíšova	Mojžíšův	k2eAgInSc2d1	Mojžíšův
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
tak	tak	k9	tak
především	především	k9	především
sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
spisy	spis	k1gInPc4	spis
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
představují	představovat	k5eAaImIp3nP	představovat
Ježíše	Ježíš	k1gMnSc2	Ježíš
jako	jako	k8xC	jako
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
dokonalého	dokonalý	k2eAgMnSc2d1	dokonalý
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
učení	učení	k1gNnSc2	učení
jako	jako	k9	jako
nový	nový	k2eAgInSc1d1	nový
Mojžíšův	Mojžíšův	k2eAgInSc1d1	Mojžíšův
zákon	zákon	k1gInSc1	zákon
či	či	k8xC	či
jeho	jeho	k3xOp3gNnSc1	jeho
naplnění	naplnění	k1gNnSc1	naplnění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Jahvista	Jahvista	k1gMnSc1	Jahvista
a	a	k8xC	a
Elohista	Elohista	k1gMnSc1	Elohista
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
spojením	spojení	k1gNnSc7	spojení
několika	několik	k4yIc2	několik
navzájem	navzájem	k6eAd1	navzájem
odlišných	odlišný	k2eAgFnPc2d1	odlišná
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
například	například	k6eAd1	například
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mojžíšův	Mojžíšův	k2eAgMnSc1d1	Mojžíšův
tchán	tchán	k1gMnSc1	tchán
bývá	bývat	k5eAaImIp3nS	bývat
jednou	jednou	k6eAd1	jednou
nazýván	nazývat	k5eAaImNgInS	nazývat
Reúel	Reúel	k1gInSc1	Reúel
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
zas	zas	k6eAd1	zas
Jetro	Jetro	k1gNnSc1	Jetro
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
veškeré	veškerý	k3xTgInPc4	veškerý
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
životopis	životopis	k1gInSc4	životopis
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
vyškrtnutí	vyškrtnutí	k1gNnSc4	vyškrtnutí
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
jako	jako	k8xC	jako
nereálné	reálný	k2eNgInPc4d1	nereálný
postavy	postav	k1gInPc4	postav
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
stále	stále	k6eAd1	stále
jen	jen	k9	jen
hypotetické	hypotetický	k2eAgFnSc2d1	hypotetická
–	–	k?	–
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dostupných	dostupný	k2eAgInPc2d1	dostupný
pramenů	pramen	k1gInPc2	pramen
jednoduše	jednoduše	k6eAd1	jednoduše
nelze	lze	k6eNd1	lze
provést	provést	k5eAaPmF	provést
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
druhé	druhý	k4xOgNnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
byla	být	k5eAaImAgFnS	být
ztvárněna	ztvárnit	k5eAaPmNgFnS	ztvárnit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
:	:	kIx,	:
písních	píseň	k1gFnPc6	píseň
<g/>
,	,	kIx,	,
výtvarných	výtvarný	k2eAgNnPc6d1	výtvarné
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
románech	román	k1gInPc6	román
i	i	k8xC	i
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románském	románský	k2eAgNnSc6d1	románské
umění	umění	k1gNnSc6	umění
bývá	bývat	k5eAaImIp3nS	bývat
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
především	především	k9	především
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
Vyzdvižení	vyzdvižení	k1gNnSc2	vyzdvižení
a	a	k8xC	a
svržení	svržení	k1gNnSc1	svržení
měděného	měděný	k2eAgMnSc2d1	měděný
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
Mojžíšovy	Mojžíšův	k2eAgFnPc1d1	Mojžíšova
ruce	ruka	k1gFnPc1	ruka
podpírané	podpíraný	k2eAgFnPc1d1	podpíraná
Áronem	áron	k1gInSc7	áron
a	a	k8xC	a
Chúrem	Chúr	k1gInSc7	Chúr
<g/>
,	,	kIx,	,
Usmrcení	usmrcení	k1gNnSc6	usmrcení
prvorozených	prvorozený	k2eAgMnPc2d1	prvorozený
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Zázračný	zázračný	k2eAgInSc4d1	zázračný
přechod	přechod	k1gInSc4	přechod
Rudého	rudý	k2eAgNnSc2d1	Rudé
(	(	kIx(	(
<g/>
Rákosového	rákosový	k2eAgNnSc2d1	rákosové
<g/>
)	)	kIx)	)
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
objevení	objevení	k1gNnSc1	objevení
pramene	pramen	k1gInSc2	pramen
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
či	či	k8xC	či
Přijetí	přijetí	k1gNnSc4	přijetí
desek	deska	k1gFnPc2	deska
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgNnPc3d3	nejvýznamnější
dílům	dílo	k1gNnPc3	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Emailované	emailovaný	k2eAgFnPc1d1	Emailovaná
destičky	destička	k1gFnPc1	destička
nebo	nebo	k8xC	nebo
tepané	tepaný	k2eAgInPc1d1	tepaný
medailony	medailon	k1gInPc1	medailon
na	na	k7c6	na
románských	románský	k2eAgInPc6d1	románský
oltářních	oltářní	k2eAgInPc6d1	oltářní
nástavcích	nástavec	k1gInPc6	nástavec
či	či	k8xC	či
křížích	kříž	k1gInPc6	kříž
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
retáblu	retábl	k1gInSc6	retábl
v	v	k7c6	v
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
gotiky	gotika	k1gFnSc2	gotika
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
Mojžíšovo	Mojžíšův	k2eAgNnSc1d1	Mojžíšovo
reprezentační	reprezentační	k2eAgNnSc1d1	reprezentační
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
sérii	série	k1gFnSc6	série
starozákonních	starozákonní	k2eAgMnPc2d1	starozákonní
králů	král	k1gMnPc2	král
a	a	k8xC	a
prororků	prororek	k1gInPc2	prororek
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
kašně	kašna	k1gFnSc6	kašna
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kartouzy	kartouza	k1gFnSc2	kartouza
v	v	k7c6	v
Dijonu	Dijon	k1gInSc6	Dijon
-	-	kIx~	-
Champmol	Champmol	k1gInSc1	Champmol
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
Clause	Clause	k1gFnSc2	Clause
Slutera	Sluter	k1gMnSc2	Sluter
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgInPc1d1	gotický
reliéfy	reliéf	k1gInPc1	reliéf
na	na	k7c6	na
kachlích	kachel	k1gInPc6	kachel
legendární	legendární	k2eAgFnSc2d1	legendární
scény	scéna	k1gFnSc2	scéna
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
a	a	k8xC	a
medailích	medaile	k1gFnPc6	medaile
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jáchymovské	jáchymovský	k2eAgFnSc2d1	Jáchymovská
<g />
.	.	kIx.	.
</s>
<s>
tolary	tolar	k1gInPc1	tolar
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
–	–	k?	–
socha	socha	k1gFnSc1	socha
od	od	k7c2	od
Michelangela	Michelangel	k1gMnSc2	Michelangel
v	v	k7c6	v
chrámě	chrám	k1gInSc6	chrám
San	San	k1gFnPc2	San
Pietro	Pietro	k1gNnSc1	Pietro
in	in	k?	in
Vincoli	Vincole	k1gFnSc6	Vincole
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
průčelí	průčelí	k1gNnSc6	průčelí
kopie	kopie	k1gFnSc2	kopie
Mariina	Mariin	k2eAgInSc2d1	Mariin
loretánského	loretánský	k2eAgInSc2d1	loretánský
domku	domek	k1gInSc2	domek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Loretu	Loreto	k1gNnSc6	Loreto
či	či	k8xC	či
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Luca	Luc	k1gInSc2	Luc
Signorelli	Signorelle	k1gFnSc4	Signorelle
<g/>
:	:	kIx,	:
fresky	freska	k1gFnPc4	freska
z	z	k7c2	z
Mojžíšova	Mojžíšův	k2eAgInSc2d1	Mojžíšův
života	život	k1gInSc2	život
v	v	k7c6	v
Sixtinské	sixtinský	k2eAgFnSc6d1	Sixtinská
kapli	kaple	k1gFnSc6	kaple
Téma	téma	k1gNnSc1	téma
Mojžíše	Mojžíš	k1gMnSc2	Mojžíš
v	v	k7c6	v
novodobém	novodobý	k2eAgNnSc6d1	novodobé
malířství	malířství	k1gNnSc6	malířství
<g/>
:	:	kIx,	:
Fedor	Fedor	k1gInSc1	Fedor
Antonovič	Antonovič	k1gInSc1	Antonovič
von	von	k1gInSc1	von
Moller	Moller	k1gInSc1	Moller
<g/>
:	:	kIx,	:
Matka	matka	k1gFnSc1	matka
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
spouští	spouštět	k5eAaImIp3nS	spouštět
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
koši	koš	k1gInSc6	koš
na	na	k7c4	na
Nil	Nil	k1gInSc4	Nil
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Moreau	Moreaa	k1gMnSc4	Moreaa
<g/>
:	:	kIx,	:
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
pohozen	pohodit	k5eAaPmNgMnS	pohodit
na	na	k7c4	na
Nil	Nil	k1gInSc4	Nil
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Merwart	Merwart	k1gInSc1	Merwart
<g/>
:	:	kIx,	:
Mladý	mladý	k2eAgMnSc1d1	mladý
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Meyer	Meyer	k1gMnSc1	Meyer
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
Mojžíšova	Mojžíšův	k2eAgFnSc1d1	Mojžíšova
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vážných	vážný	k2eAgInPc2d1	vážný
filmů	film	k1gInPc2	film
lze	lze	k6eAd1	lze
za	za	k7c4	za
nejúspěšnější	úspěšný	k2eAgFnPc4d3	nejúspěšnější
považovat	považovat	k5eAaImF	považovat
film	film	k1gInSc4	film
Desatero	desatero	k1gNnSc4	desatero
přikázání	přikázání	k1gNnSc2	přikázání
(	(	kIx(	(
<g/>
The	The	k1gFnSc2	The
Ten	ten	k3xDgInSc1	ten
Commandments	Commandments	k1gInSc1	Commandments
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
americké	americký	k2eAgInPc4d1	americký
filmy	film	k1gInPc4	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
jeho	jeho	k3xOp3gInSc1	jeho
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
seriálový	seriálový	k2eAgInSc1d1	seriálový
remake	remake	k1gInSc1	remake
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pochopitelně	pochopitelně	k6eAd1	pochopitelně
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
i	i	k9	i
četné	četný	k2eAgFnPc1d1	četná
parodie	parodie	k1gFnPc1	parodie
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
postava	postava	k1gFnSc1	postava
Mojžíše	Mojžíš	k1gMnPc4	Mojžíš
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
populární	populární	k2eAgFnSc4d1	populární
píseň	píseň	k1gFnSc4	píseň
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Spirituál	spirituál	k1gMnSc1	spirituál
kvintet	kvintet	k1gInSc1	kvintet
či	či	k8xC	či
album	album	k1gNnSc1	album
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
od	od	k7c2	od
Učedníků	učedník	k1gMnPc2	učedník
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
při	při	k7c6	při
střídání	střídání	k1gNnSc6	střídání
mluveného	mluvený	k2eAgInSc2d1	mluvený
textu	text	k1gInSc2	text
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
rekapituluje	rekapitulovat	k5eAaBmIp3nS	rekapitulovat
Mojžíšův	Mojžíšův	k2eAgInSc1d1	Mojžíšův
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
