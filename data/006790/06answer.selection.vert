<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
Izrael	Izrael	k1gInSc4	Izrael
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
skrze	skrze	k?	skrze
něj	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
Bůh	bůh	k1gMnSc1	bůh
Izraeli	Izrael	k1gMnSc3	Izrael
na	na	k7c4	na
hoře	hoře	k1gNnSc4	hoře
Sinaj	Sinaj	k1gInSc4	Sinaj
Desatero	desatero	k1gNnSc1	desatero
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
stal	stát	k5eAaPmAgInS	stát
Božím	boží	k2eAgInSc7d1	boží
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
