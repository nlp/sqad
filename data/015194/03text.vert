<s>
Umbar	Umbar	k1gMnSc1
</s>
<s>
Umbar	Umbar	k1gInSc4
Geografie	geografie	k1gFnSc2
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
přístav	přístav	k1gInSc4
Umbar	Umbar	k1gInSc1
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
západština	západština	k1gFnSc1
(	(	kIx(
<g/>
Westron	Westron	k1gInSc1
<g/>
)	)	kIx)
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Královští	královský	k2eAgMnPc1d1
Númenorejci	Númenorejec	k1gMnPc1
<g/>
,	,	kIx,
<g/>
Černí	černý	k2eAgMnPc1d1
Númenorejci	Númenorejec	k1gMnPc1
<g/>
,	,	kIx,
<g/>
Gondorští	Gondorský	k2eAgMnPc1d1
<g/>
,	,	kIx,
<g/>
Umbarští	Umbarský	k2eAgMnPc1d1
korzáři	korzár	k1gMnPc1
<g/>
,	,	kIx,
Haradští	Haradský	k2eAgMnPc1d1
Náboženství	náboženství	k1gNnPc1
</s>
<s>
melkorismus	melkorismus	k1gInSc1
a	a	k8xC
kult	kult	k1gInSc1
Saurona	Saurona	k1gFnSc1
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Númenor	Númenor	k1gInSc1
(	(	kIx(
<g/>
zpočátku	zpočátku	k6eAd1
<g/>
)	)	kIx)
Gondor	Gondor	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
<g/>
)	)	kIx)
Druh	druh	k1gInSc1
celku	celek	k1gInSc2
</s>
<s>
region	region	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Podřízené	podřízený	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
přístav	přístav	k1gInSc1
Umbar	Umbar	k1gInSc1
<g/>
,	,	kIx,
Umbarská	Umbarský	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
Druhý	druhý	k4xOgInSc1
věk	věk	k1gInSc1
Zánik	zánik	k1gInSc1
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1
věk	věk	k1gInSc1
Vládcové	vládce	k1gMnPc5
</s>
<s>
králové	král	k1gMnPc1
Númenoru	Númenor	k1gInSc2
Černí	černit	k5eAaImIp3nP
Númenorejci	Númenorejec	k1gMnPc1
králové	králová	k1gFnSc3
Gondoru	Gondor	k1gInSc2
Umbarští	Umbarský	k2eAgMnPc1d1
korzáři	korzár	k1gMnPc1
králové	králová	k1gFnSc2
Obnoveného	obnovený	k2eAgNnSc2d1
království	království	k1gNnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Umbar	Umbar	k1gMnSc1
je	být	k5eAaImIp3nS
země	zem	k1gFnPc4
a	a	k8xC
stejnojmenný	stejnojmenný	k2eAgInSc4d1
přístav	přístav	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
ve	v	k7c4
fantasy	fantas	k1gInPc4
světě	svět	k1gInSc6
J.	J.	kA
R.	R.	kA
R.	R.	kA
Tolkiena	Tolkien	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opevněný	opevněný	k2eAgInSc1d1
Umbar	Umbar	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
Númenorejci	Númenorejec	k1gMnPc7
v	v	k7c6
Umbarské	Umbarský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
někdy	někdy	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1800	#num#	k4
druhého	druhý	k4xOgInSc2
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
opěrným	opěrný	k2eAgInSc7d1
bodem	bod	k1gInSc7
a	a	k8xC
centrem	centrum	k1gNnSc7
moci	moc	k1gFnSc2
Númenorejců	Númenorejec	k1gMnPc2
ve	v	k7c6
Středozemi	Středozem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
Númenoru	Númenor	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
centrem	centrum	k1gNnSc7
Černých	Černá	k1gFnPc2
Númenorejců	Númenorejec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
Umbar	Umbar	k1gInSc1
součástí	součást	k1gFnPc2
Gondoru	Gondor	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
občanskou	občanský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
v	v	k7c6
království	království	k1gNnSc6
byl	být	k5eAaImAgMnS
Umbar	Umbar	k1gMnSc1
obsazen	obsadit	k5eAaPmNgMnS
následníky	následník	k1gMnPc7
poraženého	poražený	k1gMnSc2
uzurpátora	uzurpátor	k1gMnSc2
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
vešli	vejít	k5eAaPmAgMnP
ve	v	k7c4
známost	známost	k1gFnSc4
jako	jako	k8xS,k8xC
Umbarští	Umbarský	k2eAgMnPc1d1
korzáři	korzár	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbar	Umbar	k1gInSc1
přezdívaný	přezdívaný	k2eAgInSc1d1
také	také	k9
jako	jako	k8xC,k8xS
Město	město	k1gNnSc1
Korzárů	korzár	k1gMnPc2
stál	stát	k5eAaImAgInS
ve	v	k7c6
Válce	válka	k1gFnSc6
o	o	k7c4
Prsten	prsten	k1gInSc4
na	na	k7c6
Sauronově	Sauronův	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
Svobodných	svobodný	k2eAgInPc2d1
národů	národ	k1gInPc2
uznali	uznat	k5eAaPmAgMnP
Umbarští	Umbarský	k2eAgMnPc1d1
moc	moc	k6eAd1
krále	král	k1gMnSc4
Elessara	Elessar	k1gMnSc4
a	a	k8xC
Umbar	Umbar	k1gInSc1
byl	být	k5eAaImAgInS
začleněn	začlenit	k5eAaPmNgInS
zpět	zpět	k6eAd1
pod	pod	k7c4
Gondor	Gondor	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Obnoveného	obnovený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Umbar	Umbar	k1gInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
západu	západ	k1gInSc2
omýván	omývat	k5eAaImNgMnS
Belfalaskou	Belfalaský	k2eAgFnSc7d1
zátokou	zátoka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jihu	jih	k1gInSc2
a	a	k8xC
východu	východ	k1gInSc2
je	být	k5eAaImIp3nS
obklopen	obklopit	k5eAaPmNgMnS
zeměmi	zem	k1gFnPc7
Haradu	Harad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Umbar	Umbar	k1gMnSc1
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
Harondorem	Harondor	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
konci	konec	k1gInSc6
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
sporným	sporný	k2eAgNnPc3d1
územím	území	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dál	daleko	k6eAd2
na	na	k7c6
severu	sever	k1gInSc6
se	se	k3xPyFc4
rozkládal	rozkládat	k5eAaImAgInS
Gondor	Gondor	k1gInSc1
a	a	k8xC
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
Mordor	Mordor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
město	město	k1gNnSc4
Umbar	Umbara	k1gFnPc2
založili	založit	k5eAaPmAgMnP
Númenorejci	Númenorejec	k1gMnPc1
v	v	k7c6
zátoce	zátoka	k1gFnSc6
stejného	stejný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
přístavu	přístav	k1gInSc6
poskytovala	poskytovat	k5eAaImAgFnS
dokonalou	dokonalý	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
a	a	k8xC
umožnila	umožnit	k5eAaPmAgFnS
tak	tak	k6eAd1
Umbaru	Umbara	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
nejmocnějším	mocný	k2eAgInSc7d3
přístavem	přístav	k1gInSc7
ve	v	k7c6
Středozemi	Středozem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
Umbaru	Umbar	k1gInSc2
</s>
<s>
Přístav	přístav	k1gInSc1
Númenorejců	Númenorejec	k1gMnPc2
</s>
<s>
Umbar	Umbar	k1gInSc1
byl	být	k5eAaImAgInS
lidmi	člověk	k1gMnPc7
z	z	k7c2
Númenoru	Númenor	k1gInSc2
založen	založit	k5eAaPmNgInS
někdy	někdy	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1800	#num#	k4
druhého	druhý	k4xOgInSc2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Númenorejci	Númenorejec	k1gMnPc1
zakládali	zakládat	k5eAaImAgMnP
přístavy	přístav	k1gInPc4
a	a	k8xC
obchodní	obchodní	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Středozemě	Středozem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
pro	pro	k7c4
město	město	k1gNnSc4
Númenorejci	Númenorejec	k1gMnPc1
pravděpodobně	pravděpodobně	k6eAd1
přejali	přejmout	k5eAaPmAgMnP
od	od	k7c2
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
Umbar	Umbara	k1gFnPc2
však	však	k9
v	v	k7c6
quenijštině	quenijština	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
osud	osud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2280	#num#	k4
se	se	k3xPyFc4
Umbar	Umbar	k1gMnSc1
stal	stát	k5eAaPmAgMnS
mocnou	mocný	k2eAgFnSc7d1
pevností	pevnost	k1gFnSc7
a	a	k8xC
přístavem	přístav	k1gInSc7
Númenoru	Númenor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
moc	moc	k6eAd1
nadále	nadále	k6eAd1
rostla	růst	k5eAaImAgFnS
a	a	k8xC
Númenorejci	Númenorejec	k1gMnSc3
z	z	k7c2
něj	on	k3xPp3gNnSc2
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
centrum	centrum	k1gNnSc4
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moci	moct	k5eAaImF
Umbaru	Umbara	k1gFnSc4
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
obával	obávat	k5eAaImAgMnS
samotný	samotný	k2eAgMnSc1d1
Sauron	Sauron	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
neodvažoval	odvažovat	k5eNaImAgMnS
opouštět	opouštět	k5eAaImF
opevněný	opevněný	k2eAgMnSc1d1
Mordor	Mordor	k1gMnSc1
a	a	k8xC
soustředil	soustředit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
raději	rád	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbar	Umbar	k1gMnSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
stal	stát	k5eAaPmAgInS
nejmocnějším	mocný	k2eAgNnSc7d3
lidským	lidský	k2eAgNnSc7d1
městem	město	k1gNnSc7
ve	v	k7c6
Středozemi	Středozem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdce	srdce	k1gNnSc1
většiny	většina	k1gFnSc2
Númenorejců	Númenorejec	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
obrátila	obrátit	k5eAaPmAgFnS
proti	proti	k7c3
elfům	elf	k1gMnPc3
a	a	k8xC
Valar	Valar	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jim	on	k3xPp3gMnPc3
odepřeli	odepřít	k5eAaPmAgMnP
vstup	vstup	k1gInSc4
do	do	k7c2
Zemí	zem	k1gFnPc2
neumírajících	umírající	k2eNgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbar	Umbar	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
centrem	centrum	k1gNnSc7
tzv.	tzv.	kA
Královských	královský	k2eAgMnPc2d1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
podporovali	podporovat	k5eAaImAgMnP
krále	král	k1gMnSc4
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
postoji	postoj	k1gInSc6
vůči	vůči	k7c3
Valar	Valara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menšina	menšina	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
nazývali	nazývat	k5eAaImAgMnP
Věrní	věrný	k2eAgMnPc1d1
a	a	k8xC
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nadále	nadále	k6eAd1
ctili	ctít	k5eAaImAgMnP
Valar	Valar	k1gInSc4
<g/>
,	,	kIx,
přebývali	přebývat	k5eAaImAgMnP
v	v	k7c6
přístavu	přístav	k1gInSc6
Pelargir	Pelargir	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Anduiny	Anduina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
krále	král	k1gMnSc2
Tar-Palantíra	Tar-Palantír	k1gMnSc2
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c4
Umbaru	Umbara	k1gFnSc4
jeho	jeho	k3xOp3gMnSc1
synovec	synovec	k1gMnSc1
Pharazôn	Pharazôn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
velel	velet	k5eAaImAgInS
armádám	armáda	k1gFnPc3
Númenoru	Númenora	k1gFnSc4
ve	v	k7c6
Středozemi	Středozem	k1gFnSc6
a	a	k8xC
podrobil	podrobit	k5eAaPmAgMnS
si	se	k3xPyFc3
tehdy	tehdy	k6eAd1
mnohé	mnohý	k2eAgInPc1d1
národy	národ	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
strýce	strýc	k1gMnSc2
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
Númenor	Númenor	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
sestřenici	sestřenice	k1gFnSc4
a	a	k8xC
zmocnil	zmocnit	k5eAaPmAgInS
se	se	k3xPyFc4
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
vycítil	vycítit	k5eAaPmAgMnS
Sauron	Sauron	k1gMnSc1
svou	svůj	k3xOyFgFnSc4
šanci	šance	k1gFnSc4
a	a	k8xC
přešel	přejít	k5eAaPmAgMnS
do	do	k7c2
protiútoku	protiútok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sauronova	Sauronův	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
vytlačovala	vytlačovat	k5eAaImAgFnS
Dúnadany	Dúnadan	k1gInPc4
z	z	k7c2
jejich	jejich	k3xOp3gNnSc2
území	území	k1gNnSc2
a	a	k8xC
Sauron	Sauron	k1gMnSc1
tehdy	tehdy	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Númenorejce	Númenorejec	k1gMnSc4
zatlačí	zatlačit	k5eAaPmIp3nP
do	do	k7c2
moře	moře	k1gNnSc2
a	a	k8xC
zmocní	zmocnit	k5eAaPmIp3nS
se	se	k3xPyFc4
Númenoru	Númenor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
dozvěděl	dozvědět	k5eAaPmAgMnS
král	král	k1gMnSc1
Ar-Pharazôn	Ar-Pharazôn	k1gMnSc1
<g/>
,	,	kIx,
vyzbrojil	vyzbrojit	k5eAaPmAgMnS
obrovské	obrovský	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
Umbaru	Umbar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
pochodoval	pochodovat	k5eAaImAgMnS
sedm	sedm	k4xCc4
dní	den	k1gInPc2
do	do	k7c2
nitra	nitro	k1gNnSc2
Středozemě	Středozem	k1gFnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
hlasatele	hlasatel	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
Temného	temný	k2eAgMnSc4d1
pána	pán	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dobrovolně	dobrovolně	k6eAd1
podrobil	podrobit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sauron	Sauron	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
viděl	vidět	k5eAaImAgMnS
nesmírnou	smírný	k2eNgFnSc4d1
moc	moc	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
bez	bez	k7c2
boje	boj	k1gInSc2
vyšel	vyjít	k5eAaPmAgInS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
králi	král	k1gMnSc3
se	se	k3xPyFc4
pokořil	pokořit	k5eAaPmAgMnS
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
odvést	odvést	k5eAaPmF
na	na	k7c4
Númenor	Númenor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
však	však	k9
postupně	postupně	k6eAd1
zkazil	zkazit	k5eAaPmAgInS
srdce	srdce	k1gNnSc4
většiny	většina	k1gFnSc2
Dúnadanů	Dúnadan	k1gMnPc2
včetně	včetně	k7c2
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbar	Umbar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
stál	stát	k5eAaImAgInS
na	na	k7c6
králově	králův	k2eAgFnSc6d1
straně	strana	k1gFnSc6
tak	tak	k6eAd1
upadnul	upadnout	k5eAaPmAgMnS
k	k	k7c3
uctívání	uctívání	k1gNnSc3
Melkora	Melkor	k1gMnSc2
a	a	k8xC
tmy	tma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
3319	#num#	k4
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Ar-Pharazôn	Ar-Pharazôn	k1gInSc4
naveden	naveden	k2eAgInSc4d1
Sauronem	Sauron	k1gInSc7
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Valar	Valar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilúvatar	Ilúvatar	k1gInSc1
tehdy	tehdy	k6eAd1
změnil	změnit	k5eAaPmAgInS
tvar	tvar	k1gInSc1
Ardy	Arda	k1gFnSc2
a	a	k8xC
voda	voda	k1gFnSc1
pohltila	pohltit	k5eAaPmAgFnS
celý	celý	k2eAgInSc4d1
Númenor	Númenor	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
obrovskou	obrovský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežili	přežít	k5eAaPmAgMnP
pouze	pouze	k6eAd1
Věrní	věrný	k2eAgMnPc1d1
vedení	vedení	k1gNnSc4
Amandilovým	Amandilův	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Elendilem	Elendil	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
Černých	Černých	k2eAgMnPc2d1
Númenorejců	Númenorejec	k1gMnPc2
</s>
<s>
Sauronův	Sauronův	k2eAgMnSc1d1
duch	duch	k1gMnSc1
po	po	k7c6
pádu	pád	k1gInSc6
Númenoru	Númenor	k1gInSc2
zamířil	zamířit	k5eAaPmAgMnS
zpátky	zpátky	k6eAd1
do	do	k7c2
Mordoru	Mordor	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vládl	vládnout	k5eAaImAgInS
zemím	zem	k1gFnPc3
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbar	Umbar	k1gInSc1
stál	stát	k5eAaImAgInS
na	na	k7c6
straně	strana	k1gFnSc6
Temného	temný	k2eAgMnSc2d1
pána	pán	k1gMnSc2
a	a	k8xC
ve	v	k7c6
válce	válka	k1gFnSc6
posledního	poslední	k2eAgNnSc2d1
spojenectví	spojenectví	k1gNnSc2
přišly	přijít	k5eAaPmAgFnP
do	do	k7c2
Mordoru	Mordor	k1gInSc2
posily	posila	k1gFnSc2
z	z	k7c2
Umbaru	Umbar	k1gInSc2
a	a	k8xC
Haradu	Harad	k1gInSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Herumorem	Herumor	k1gMnSc7
a	a	k8xC
Fuinurem	Fuinur	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
pravděpodobně	pravděpodobně	k6eAd1
zahynuli	zahynout	k5eAaPmAgMnP
během	během	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
armádám	armáda	k1gFnPc3
Posledního	poslední	k2eAgNnSc2d1
spojenectví	spojenectví	k1gNnSc2
podařilo	podařit	k5eAaPmAgNnS
Saurona	Sauron	k1gMnSc4
porazit	porazit	k5eAaPmF
a	a	k8xC
odebrat	odebrat	k5eAaPmF
mu	on	k3xPp3gMnSc3
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
prsten	prsten	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duch	duch	k1gMnSc1
Temného	temný	k2eAgMnSc2d1
pána	pán	k1gMnSc2
tehdy	tehdy	k6eAd1
uprchl	uprchnout	k5eAaPmAgMnS
na	na	k7c4
východ	východ	k1gInSc4
a	a	k8xC
v	v	k7c6
Umbaru	Umbar	k1gInSc6
se	se	k3xPyFc4
moci	moc	k1gFnPc1
chopili	chopit	k5eAaPmAgMnP
Černí	černý	k2eAgMnPc1d1
Númenorejci	Númenorejec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
soupeřili	soupeřit	k5eAaImAgMnP
s	s	k7c7
Elendilovými	Elendilův	k2eAgMnPc7d1
syny	syn	k1gMnPc7
a	a	k8xC
Gondorem	Gondor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Pevnost	pevnost	k1gFnSc1
Gondoru	Gondor	k1gInSc2
</s>
<s>
Gondor	Gondor	k1gInSc1
za	za	k7c2
doby	doba	k1gFnSc2
svého	svůj	k3xOyFgInSc2
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
830	#num#	k4
T.	T.	kA
v.	v.	k?
se	se	k3xPyFc4
útokem	útok	k1gInSc7
z	z	k7c2
moře	moře	k1gNnSc2
i	i	k8xC
souše	souš	k1gFnSc2
podařilo	podařit	k5eAaPmAgNnS
gondorskému	gondorský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Eärnilovi	Eärnil	k1gMnSc3
Umbaru	Umbar	k1gMnSc3
zmocnit	zmocnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černí	černý	k2eAgMnPc1d1
Númenorejci	Númenorejec	k1gMnPc1
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
do	do	k7c2
Haradu	Harad	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
však	však	k9
nadále	nadále	k6eAd1
podnikali	podnikat	k5eAaImAgMnP
proti	proti	k7c3
gondorským	gondorský	k2eAgFnPc3d1
protiútoky	protiútok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1015	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
s	s	k7c7
Haradskými	Haradský	k2eAgFnPc7d1
zabit	zabít	k5eAaPmNgMnS
gondorský	gondorský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ciryandil	Ciryandil	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
samotný	samotný	k2eAgInSc1d1
Umbar	Umbar	k1gInSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
udržet	udržet	k5eAaPmF
v	v	k7c6
moci	moc	k1gFnSc6
Gondoru	Gondor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1050	#num#	k4
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
stabilizována	stabilizován	k2eAgFnSc1d1
Ciryandilovým	Ciryandilův	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Hyármendacilem	Hyármendacil	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
Jižany	Jižan	k1gMnPc7
porazil	porazit	k5eAaPmAgMnS
za	za	k7c7
řekou	řeka	k1gFnSc7
Harnen	Harnna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Umbaru	Umbar	k1gInSc6
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
postaven	postavit	k5eAaPmNgInS
památník	památník	k1gInSc4
připomínající	připomínající	k2eAgNnSc4d1
Sauronovo	Sauronův	k2eAgNnSc4d1
pokoření	pokoření	k1gNnSc4
před	před	k7c7
Ar-Pharazônem	Ar-Pharazôn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1432	#num#	k4
vypukla	vypuknout	k5eAaPmAgFnS
v	v	k7c6
Gondoru	Gondor	k1gInSc6
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
králem	král	k1gMnSc7
Eldacarem	Eldacar	k1gMnSc7
a	a	k8xC
Castamirem	Castamir	k1gMnSc7
Uchvatitelem	uchvatitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
brodu	brod	k1gInSc2
Erui	Eru	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1447	#num#	k4
byl	být	k5eAaImAgInS
Castamir	Castamir	k1gInSc1
zabit	zabit	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
poraženo	porazit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Castamirovi	Castamirův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
však	však	k9
s	s	k7c7
většinou	většina	k1gFnSc7
gondorského	gondorský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
uprchli	uprchnout	k5eAaPmAgMnP
do	do	k7c2
Umbaru	Umbar	k1gInSc2
a	a	k8xC
vyhlásili	vyhlásit	k5eAaPmAgMnP
tam	tam	k6eAd1
na	na	k7c6
Gondoru	Gondor	k1gInSc6
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
Korzárů	korzár	k1gMnPc2
</s>
<s>
Středozemě	Středozem	k1gFnPc1
během	během	k7c2
Války	válka	k1gFnSc2
o	o	k7c4
Prsten	prsten	k1gInSc4
</s>
<s>
Castamirovi	Castamirův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
se	se	k3xPyFc4
tak	tak	k9
ujali	ujmout	k5eAaPmAgMnP
vlády	vláda	k1gFnPc4
nad	nad	k7c7
Umbarem	Umbar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
silného	silný	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
neustále	neustále	k6eAd1
ohrožovali	ohrožovat	k5eAaImAgMnP
pobřeží	pobřeží	k1gNnSc4
Gondoru	Gondor	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
lid	lid	k1gInSc1
označoval	označovat	k5eAaImAgInS
jako	jako	k9
Umbarské	Umbarský	k2eAgMnPc4d1
korzáry	korzár	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1634	#num#	k4
vyplenili	vyplenit	k5eAaPmAgMnP
korzáři	korzár	k1gMnPc1
Pelagrir	Pelagrira	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
gondorský	gondorský	k2eAgMnSc1d1
král	král	k1gMnSc1
Minardil	Minardil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1810	#num#	k4
sebral	sebrat	k5eAaPmAgInS
všechny	všechen	k3xTgFnPc4
gondorské	gondorský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
král	král	k1gMnSc1
Telumehtar	Telumehtar	k1gMnSc1
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
cenu	cena	k1gFnSc4
velkých	velký	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
podařilo	podařit	k5eAaPmAgNnS
gondorským	gondorský	k2eAgMnPc3d1
znovu	znovu	k6eAd1
obsadit	obsadit	k5eAaPmF
Umbar	Umbar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
tenkrát	tenkrát	k6eAd1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
poslední	poslední	k2eAgMnPc1d1
Castamirovi	Castamirův	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gondorští	Gondorský	k2eAgMnPc1d1
však	však	k9
tentokrát	tentokrát	k6eAd1
Umbar	Umbar	k1gMnSc1
příliš	příliš	k6eAd1
pevně	pevně	k6eAd1
neopevnili	opevnit	k5eNaPmAgMnP
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
město	město	k1gNnSc1
okolo	okolo	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
Gondor	Gondor	k1gInSc4
zaútočili	zaútočit	k5eAaPmAgMnP
spojené	spojený	k2eAgFnPc4d1
armády	armáda	k1gFnPc4
Vozatajů	vozataj	k1gMnPc2
<g/>
,	,	kIx,
Varjagů	Varjag	k1gMnPc2
a	a	k8xC
Haradských	Haradský	k2eAgMnPc2d1
<g/>
,	,	kIx,
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
Jižanů	Jižan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umbarští	Umbarský	k2eAgMnPc1d1
korzáři	korzár	k1gMnPc1
začali	začít	k5eAaPmAgMnP
znovu	znovu	k6eAd1
napadat	napadat	k5eAaImF,k5eAaBmF,k5eAaPmF
pobřeží	pobřeží	k1gNnSc4
Gondoru	Gondor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
největší	veliký	k2eAgInSc1d3
útok	útok	k1gInSc1
přišel	přijít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2758	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tři	tři	k4xCgFnPc1
obrovské	obrovský	k2eAgFnPc1d1
flotily	flotila	k1gFnPc1
vylodily	vylodit	k5eAaPmAgFnP
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
současně	současně	k6eAd1
s	s	k7c7
útokem	útok	k1gInSc7
Vrchovců	Vrchovec	k1gMnPc2
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správci	správce	k1gMnSc3
Beregondovi	Beregond	k1gMnSc3
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
dalšího	další	k2eAgInSc2d1
roku	rok	k1gInSc2
vyhnat	vyhnat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
znovupovstání	znovupovstání	k1gNnSc6
Saurona	Saurona	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2951	#num#	k4
se	se	k3xPyFc4
Umbarští	Umbarský	k2eAgMnPc1d1
dobrovolně	dobrovolně	k6eAd1
poddali	poddat	k5eAaPmAgMnP
moci	moct	k5eAaImF
Mordoru	Mordor	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2980	#num#	k4
se	se	k3xPyFc4
malé	malý	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
vedené	vedený	k2eAgNnSc1d1
tajemným	tajemný	k2eAgInSc7d1
Thorongilem	Thorongil	k1gInSc7
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
noci	noc	k1gFnSc6
nepozorovaně	pozorovaně	k6eNd1
připlout	připlout	k5eAaPmF
do	do	k7c2
Umbaru	Umbar	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
Gondorským	Gondorský	k2eAgFnPc3d1
podařilo	podařit	k5eAaPmAgNnS
zapálit	zapálit	k5eAaPmF
většinu	většina	k1gFnSc4
umbarské	umbarský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
a	a	k8xC
Thorongilovi	Thorongilův	k2eAgMnPc1d1
dokonce	dokonce	k9
usmrtit	usmrtit	k5eAaPmF
Kapitána	kapitán	k1gMnSc4
přístavu	přístav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
tyto	tento	k3xDgFnPc4
velké	velký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
vyslal	vyslat	k5eAaPmAgMnS
Umbar	Umbar	k1gMnSc1
velké	velký	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
mělo	mít	k5eAaImAgNnS
podpořit	podpořit	k5eAaPmF
Sauronův	Sauronův	k2eAgInSc4d1
útok	útok	k1gInSc4
proti	proti	k7c3
Minas	Minas	k1gMnSc1
Tirith	Tirith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
bitvy	bitva	k1gFnSc2
na	na	k7c6
pelennorských	pelennorský	k2eAgNnPc6d1
polích	pole	k1gNnPc6
se	se	k3xPyFc4
však	však	k9
Umbarští	Umbarský	k2eAgMnPc1d1
nedostali	dostat	k5eNaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
byli	být	k5eAaImAgMnP
poraženi	porazit	k5eAaPmNgMnP
armádou	armáda	k1gFnSc7
mrtvých	mrtvý	k1gMnPc2
z	z	k7c2
Šeré	šerý	k2eAgFnSc2d1
brázdy	brázda	k1gFnSc2
vedených	vedený	k2eAgMnPc2d1
Aragornem	Aragorn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opuštěné	opuštěný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
poté	poté	k6eAd1
využil	využít	k5eAaPmAgMnS
Aragorn	Aragorn	k1gInSc4
k	k	k7c3
přivedení	přivedení	k1gNnSc3
posil	posila	k1gFnPc2
z	z	k7c2
jižních	jižní	k2eAgNnPc2d1
lén	léno	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
vítězství	vítězství	k1gNnSc3
v	v	k7c6
bitvě	bitva	k1gFnSc6
před	před	k7c7
Minas	Minas	k1gMnSc1
Tirith	Tirith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konečném	konečný	k2eAgInSc6d1
pádu	pád	k1gInSc6
Saurona	Saurona	k1gFnSc1
a	a	k8xC
vytvoření	vytvoření	k1gNnSc1
Obnoveného	obnovený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
uznal	uznat	k5eAaPmAgInS
Umbar	Umbar	k1gInSc1
vládu	vláda	k1gFnSc4
krále	král	k1gMnSc4
Elessara	Elessar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TOLKIEN	TOLKIEN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reuel	Reuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pán	pán	k1gMnSc1
Prstenů	prsten	k1gInPc2
-	-	kIx~
Návrat	návrat	k1gInSc1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
373	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Dodatky	dodatek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
TOLKIEN	TOLKIEN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reuel	Reuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silmarillion	Silmarillion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
999	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Umbar	Umbara	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Středozem	Středozem	k1gFnSc1
</s>
<s>
Harad	Harad	k6eAd1
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
Prsten	prsten	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Království	království	k1gNnSc1
a	a	k8xC
země	země	k1gFnSc1
ve	v	k7c6
Třetím	třetí	k4xOgInSc6
věku	věk	k1gInSc6
v	v	k7c6
Tolkienových	Tolkienův	k2eAgFnPc6d1
legendách	legenda	k1gFnPc6
Lidé	člověk	k1gMnPc1
</s>
<s>
Arnor	Arnor	k1gInSc1
•	•	k?
Hůrka	hůrka	k1gFnSc1
•	•	k?
Dol	dol	k1gInSc1
•	•	k?
Dol	dol	k1gInSc1
Amroth	Amroth	k1gInSc1
•	•	k?
Éothéod	Éothéod	k1gInSc1
•	•	k?
Esgaroth	Esgaroth	k1gInSc1
•	•	k?
Gondor	Gondor	k1gInSc1
•	•	k?
Harad	Harad	k1gInSc1
•	•	k?
Rhovanion	Rhovanion	k1gInSc1
•	•	k?
Rhû	Rhû	k1gMnSc1
•	•	k?
Rohan	Rohan	k1gMnSc1
•	•	k?
Umbar	Umbar	k1gMnSc1
Elfové	elf	k1gMnPc1
</s>
<s>
Lindon	Lindon	k1gInSc1
•	•	k?
Lothlórien	Lothlórien	k1gInSc1
•	•	k?
Temný	temný	k2eAgInSc1d1
hvozd	hvozd	k1gInSc1
•	•	k?
Roklinka	roklinka	k1gFnSc1
Trpaslíci	trpaslík	k1gMnPc5
</s>
<s>
Osamělá	osamělý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Železné	železný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
•	•	k?
Moria	Moria	k1gFnSc1
Hobiti	hobit	k1gMnPc1
</s>
<s>
Hůrka	hůrka	k1gFnSc1
•	•	k?
Kosatcová	kosatcový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
•	•	k?
Kraj	kraj	k7c2
Enti	Ent	k1gFnSc2
</s>
<s>
Fangorn	Fangorn	k1gInSc4
Nepřátelé	nepřítel	k1gMnPc1
</s>
<s>
Angmar	Angmar	k1gInSc1
•	•	k?
Dol	dol	k1gInSc1
Guldur	Guldur	k1gMnSc1
•	•	k?
Železný	Železný	k1gMnSc1
pas	pas	k6eAd1
•	•	k?
Mordor	Mordor	k1gInSc1
•	•	k?
Minas	Minas	k1gInSc1
Morgul	Morgul	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tolkien	Tolkien	k2eAgInSc1d1
</s>
