<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Svaz	svaz	k1gInSc1
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republikС	republikС	k?
С	С	k?
С	С	k?
Р	Р	k1gMnSc1
Sovětskich	Sovětskich	k1gMnSc1
Socialističeskich	Socialističeskich	k1gMnSc1
Respublik	Respublik	k1gMnSc1
</s>
<s>
↓	↓	k?
</s>
<s>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
↓	↓	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
Internacionála	Internacionála	k1gFnSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
Hymna	hymna	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
:	:	kIx,
П	П	k?
в	в	k?
с	с	k?
<g/>
,	,	kIx,
с	с	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
Proletáři	proletář	k1gMnPc1
všech	všecek	k3xTgFnPc2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
spojte	spojit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Moskva	Moskva	k1gFnSc1
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
22	#num#	k4
402	#num#	k4
220	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Pik	pik	k1gInSc1
Kommunizma	Kommunizmum	k1gNnSc2
(	(	kIx(
<g/>
7	#num#	k4
495	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Jenisej	Jenisej	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
539	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
293	#num#	k4
047	#num#	k4
571	#num#	k4
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Rusové	Rus	k1gMnPc1
(	(	kIx(
<g/>
50,78	50,78	k4
%	%	kIx~
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ukrajinci	Ukrajinec	k1gMnPc1
(	(	kIx(
<g/>
15,45	15,45	k4
%	%	kIx~
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Uzbeci	Uzbek	k1gMnPc1
(	(	kIx(
<g/>
5,84	5,84	k4
%	%	kIx~
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
národů	národ	k1gInPc2
a	a	k8xC
etnik	etnikum	k1gNnPc2
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
ruština	ruština	k1gFnSc1
<g/>
,	,	kIx,
dalších	další	k2eAgInPc2d1
14	#num#	k4
oficiálních	oficiální	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
oficiálně	oficiálně	k6eAd1
žádné	žádný	k3yNgNnSc4
(	(	kIx(
<g/>
ateismus	ateismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
byly	být	k5eAaImAgFnP
trpěny	trpěn	k2eAgFnPc1d1
pravoslaví	pravoslaví	k1gNnSc1
<g/>
,	,	kIx,
katolictví	katolictví	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
a	a	k8xC
šamanismus	šamanismus	k1gInSc1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
měna	měna	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
rubl	rubl	k1gInSc1
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1922	#num#	k4
spojením	spojení	k1gNnSc7
čtyř	čtyři	k4xCgFnPc2
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc4
1991	#num#	k4
rozpadem	rozpad	k1gInSc7
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Běloruská	běloruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Zakavkazská	zakavkazský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Bucharská	bucharský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Chórezmská	Chórezmský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Tuvinská	Tuvinský	k2eAgFnSc1d1
aratská	aratský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Běloruská	běloruský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Moldavská	moldavský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Ázerbájdžánská	ázerbájdžánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Republika	republika	k1gFnSc1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Republika	republika	k1gFnSc1
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Republika	republika	k1gFnSc1
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Estonská	estonský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
sporné	sporný	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Lotyšská	lotyšský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
sporné	sporný	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Litevská	litevský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
sporné	sporný	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
С	С	k?
<g/>
,	,	kIx,
Sovětskij	Sovětskij	k1gMnSc1
Sojuz	Sojuz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Svaz	svaz	k1gInSc1
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
<g/>
́	́	k?
<g/>
з	з	k?
С	С	k?
<g/>
́	́	k?
<g/>
т	т	k?
С	С	k?
<g/>
́	́	k?
<g/>
ч	ч	k?
Р	Р	k?
<g/>
́	́	k?
<g/>
б	б	k?
<g/>
,	,	kIx,
Sojuz	Sojuz	k1gInSc1
Sovětskich	Sovětskich	k1gMnSc1
Socialističeskich	Socialističeskich	k1gMnSc1
Respublik	Respublik	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
SSSR	SSSR	kA
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
eurasijský	eurasijský	k2eAgInSc1d1
stát	stát	k1gInSc1
se	s	k7c7
socialistickým	socialistický	k2eAgNnSc7d1
zřízením	zřízení	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
existoval	existovat	k5eAaImAgInS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
1922	#num#	k4
až	až	k6eAd1
1991	#num#	k4
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
dřívějšího	dřívější	k2eAgNnSc2d1
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
měl	mít	k5eAaImAgInS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
ústavě	ústava	k1gFnSc6
zakotvenou	zakotvený	k2eAgFnSc4d1
vedoucí	vedoucí	k2eAgFnSc4d1
roli	role	k1gFnSc4
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
dominantní	dominantní	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
do	do	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
nominálně	nominálně	k6eAd1
unií	unie	k1gFnSc7
svazových	svazový	k2eAgFnPc2d1
republik	republika	k1gFnPc2
s	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
šlo	jít	k5eAaImAgNnS
o	o	k7c4
silně	silně	k6eAd1
centralizovaný	centralizovaný	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Říjnová	říjnový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
v	v	k7c6
listopadu	listopad	k1gInSc6
1917	#num#	k4
vedla	vést	k5eAaImAgFnS
k	k	k7c3
pádu	pád	k1gInSc3
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
a	a	k8xC
odstranění	odstranění	k1gNnSc2
prozatímní	prozatímní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
bolševici	bolševik	k1gMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
v	v	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1922	#num#	k4
založen	založen	k2eAgInSc1d1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgFnSc4d1
Ruskou	ruský	k2eAgFnSc4d1
sovětskou	sovětský	k2eAgFnSc4d1
federativní	federativní	k2eAgFnSc4d1
socialistickou	socialistický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
,	,	kIx,
Ukrajinskou	ukrajinský	k2eAgFnSc4d1
<g/>
,	,	kIx,
Běloruskou	běloruský	k2eAgFnSc4d1
sovětskou	sovětský	k2eAgFnSc4d1
socialistickou	socialistický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
a	a	k8xC
Zakavkazskou	zakavkazský	k2eAgFnSc4d1
federativní	federativní	k2eAgFnSc4d1
socialistickou	socialistický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
další	další	k2eAgFnPc1d1
sovětské	sovětský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
ve	v	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
prvního	první	k4xOgMnSc2
sovětského	sovětský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Vladimira	Vladimir	k1gMnSc2
Iljiče	Iljič	k1gMnSc2
Lenina	Lenin	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
se	se	k3xPyFc4
dalším	další	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
země	zem	k1gFnSc2
stal	stát	k5eAaPmAgMnS
Josif	Josif	k1gMnSc1
Vissarionovič	Vissarionovič	k1gMnSc1
Stalin	Stalin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zahájil	zahájit	k5eAaPmAgMnS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
industrializaci	industrializace	k1gFnSc4
<g/>
,	,	kIx,
spojenou	spojený	k2eAgFnSc4d1
s	s	k7c7
plánovanou	plánovaný	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
a	a	k8xC
politickým	politický	k2eAgInSc7d1
útlakem	útlak	k1gInSc7
<g/>
;	;	kIx,
v	v	k7c6
tzv.	tzv.	kA
gulazích	gulag	k1gInPc6
tehdy	tehdy	k6eAd1
zahynulo	zahynout	k5eAaPmAgNnS
několik	několik	k4yIc1
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
nacistické	nacistický	k2eAgNnSc1d1
Německo	Německo	k1gNnSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
napadli	napadnout	k5eAaPmAgMnP
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
s	s	k7c7
ním	on	k3xPp3gMnSc7
dříve	dříve	k6eAd2
podepsali	podepsat	k5eAaPmAgMnP
dohodu	dohoda	k1gFnSc4
o	o	k7c6
neútočení	neútočení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
následujících	následující	k2eAgNnPc2d1
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
těžkých	těžký	k2eAgInPc2d1
bojů	boj	k1gInPc2
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
světových	světový	k2eAgFnPc2d1
supervelmocí	supervelmoc	k1gFnPc2
<g/>
;	;	kIx,
tou	ten	k3xDgFnSc7
druhou	druhý	k4xOgFnSc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
získal	získat	k5eAaPmAgInS
(	(	kIx(
<g/>
staro	staro	k6eAd1
<g/>
)	)	kIx)
<g/>
nová	nový	k2eAgNnPc4d1
území	území	k1gNnPc4
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
počet	počet	k1gInSc1
svazových	svazový	k2eAgFnPc2d1
republik	republika	k1gFnPc2
stoupl	stoupnout	k5eAaPmAgInS
na	na	k7c4
16	#num#	k4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
se	se	k3xPyFc4
ustálil	ustálit	k5eAaPmAgInS
na	na	k7c4
15	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
se	s	k7c7
satelitními	satelitní	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
dlouhém	dlouhý	k2eAgInSc6d1
ideologickém	ideologický	k2eAgInSc6d1
a	a	k8xC
politickém	politický	k2eAgInSc6d1
boji	boj	k1gInSc6
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gMnPc7
spojenci	spojenec	k1gMnPc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
nakonec	nakonec	k6eAd1
neuspěl	uspět	k5eNaPmAgMnS
kvůli	kvůli	k7c3
ekonomickým	ekonomický	k2eAgMnPc3d1
problémům	problém	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
zahraničním	zahraniční	k2eAgMnSc7d1
a	a	k8xC
domácím	domácí	k2eAgInPc3d1
nepokojům	nepokoj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
poslední	poslední	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc4
pokusil	pokusit	k5eAaPmAgMnS
reformovat	reformovat	k5eAaBmF
stát	stát	k5eAaPmF,k5eAaImF
politikou	politika	k1gFnSc7
perestrojky	perestrojka	k1gFnSc2
a	a	k8xC
glasnosti	glasnost	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
zhroutil	zhroutit	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
rozpuštěn	rozpustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práva	právo	k1gNnPc1
a	a	k8xC
povinnosti	povinnost	k1gFnPc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
převzala	převzít	k5eAaPmAgFnS
Ruská	ruský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
<g/>
,	,	kIx,
klima	klima	k1gNnSc1
a	a	k8xC
prostředí	prostředí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Geografie	geografie	k1gFnSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
rozlohou	rozloha	k1gFnSc7
22	#num#	k4
402	#num#	k4
200	#num#	k4
km	km	kA
<g/>
2	#num#	k4
byl	být	k5eAaImAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
největším	veliký	k2eAgInSc7d3
státem	stát	k1gInSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
status	status	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
Rusku	Ruska	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pokrýval	pokrývat	k5eAaImAgInS
šestinu	šestina	k1gFnSc4
suchozemského	suchozemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
velikost	velikost	k1gFnSc4
byla	být	k5eAaImAgFnS
srovnatelná	srovnatelný	k2eAgFnSc1d1
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Evropská	evropský	k2eAgFnSc1d1
část	část	k1gFnSc1
představovala	představovat	k5eAaImAgFnS
čtvrtinu	čtvrtina	k1gFnSc4
území	území	k1gNnSc2
státu	stát	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
kulturním	kulturní	k2eAgNnSc7d1
a	a	k8xC
ekonomickým	ekonomický	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
v	v	k7c6
Asii	Asie	k1gFnSc6
dosahovala	dosahovat	k5eAaImAgFnS
k	k	k7c3
Tichému	tichý	k2eAgInSc3d1
oceánu	oceán	k1gInSc3
a	a	k8xC
Afghánistánu	Afghánistán	k1gInSc6
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgFnPc2
oblastí	oblast	k1gFnPc2
ve	v	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
byla	být	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
obydlená	obydlený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Táhl	táhnout	k5eAaImAgMnS
se	se	k3xPyFc4
od	od	k7c2
východu	východ	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
přes	přes	k7c4
vzdálenost	vzdálenost	k1gFnSc4
10	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
11	#num#	k4
časovými	časový	k2eAgNnPc7d1
pásmy	pásmo	k1gNnPc7
a	a	k8xC
přes	přes	k7c4
7	#num#	k4
200	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
severu	sever	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasahoval	zasahovat	k5eAaImAgMnS
do	do	k7c2
pěti	pět	k4xCc2
klimatických	klimatický	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
<g/>
:	:	kIx,
tundra	tundra	k1gFnSc1
<g/>
,	,	kIx,
tajga	tajga	k1gFnSc1
<g/>
,	,	kIx,
step	step	k1gFnSc1
<g/>
,	,	kIx,
poušť	poušť	k1gFnSc1
a	a	k8xC
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
měl	mít	k5eAaImAgInS
nejdelší	dlouhý	k2eAgFnSc4d3
hranici	hranice	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měřila	měřit	k5eAaImAgFnS
přes	přes	k7c4
60	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
a	a	k8xC
půl	půl	k1xP
obvodu	obvod	k1gInSc2
Země	zem	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
z	z	k7c2
toho	ten	k3xDgNnSc2
zaujímalo	zaujímat	k5eAaImAgNnS
pobřeží	pobřeží	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
Beringovým	Beringův	k2eAgInSc7d1
průlivem	průliv	k1gInSc7
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1945	#num#	k4
až	až	k9
1991	#num#	k4
hraničil	hraničit	k5eAaImAgInS
se	s	k7c7
zeměmi	zem	k1gFnPc7
jako	jako	k9
Afghánistán	Afghánistán	k1gInSc4
<g/>
,	,	kIx,
ČLR	ČLR	kA
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
SSSR	SSSR	kA
byla	být	k5eAaImAgFnS
Pik	pik	k1gInSc4
Kommunizma	Kommunizmum	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Qullai	Qulla	k1gFnSc3
Ismoili	Ismoili	k1gFnPc2
Somoni	Somon	k1gMnPc1
<g/>
)	)	kIx)
v	v	k7c6
Tádžikistánu	Tádžikistán	k1gInSc6
s	s	k7c7
7	#num#	k4
495	#num#	k4
m.	m.	k?
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
také	také	k9
zahrnoval	zahrnovat	k5eAaImAgMnS
většinu	většina	k1gFnSc4
největších	veliký	k2eAgNnPc2d3
světových	světový	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
<g/>
;	;	kIx,
Kaspické	kaspický	k2eAgNnSc1d1
moře	moře	k1gNnSc1
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
Íránem	Írán	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
Bajkal	Bajkal	k1gInSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc1d3
a	a	k8xC
nejhlubší	hluboký	k2eAgNnSc1d3
sladkovodní	sladkovodní	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
také	také	k9
vnitřním	vnitřní	k2eAgInSc7d1
vodním	vodní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
SSSR	SSSR	kA
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Říjnová	říjnový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
a	a	k8xC
Ruská	ruský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
při	při	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
</s>
<s>
Hladomor	hladomor	k1gInSc1
v	v	k7c6
Povolží	Povolží	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1921	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
zahubil	zahubit	k5eAaPmAgMnS
odhadem	odhad	k1gInSc7
6	#num#	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
</s>
<s>
Po	po	k7c6
říjnové	říjnový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
roku	rok	k1gInSc2
1917	#num#	k4
se	se	k3xPyFc4
ocitlo	ocitnout	k5eAaPmAgNnS
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
v	v	k7c6
chaosu	chaos	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
prozatímní	prozatímní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	s	k7c7
snahou	snaha	k1gFnSc7
uspořádat	uspořádat	k5eAaPmF
demokratické	demokratický	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc2
bolševici	bolševik	k1gMnPc1
ustanovili	ustanovit	k5eAaPmAgMnP
okamžitě	okamžitě	k6eAd1
první	první	k4xOgInSc1
sovět	sovět	k1gInSc1
a	a	k8xC
po	po	k7c6
návratu	návrat	k1gInSc6
Lenina	Lenin	k1gMnSc2
z	z	k7c2
exilu	exil	k1gInSc2
začali	začít	k5eAaPmAgMnP
postupně	postupně	k6eAd1
získávat	získávat	k5eAaImF
stále	stále	k6eAd1
více	hodně	k6eAd2
moci	moct	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
rudá	rudý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
anulovala	anulovat	k5eAaBmAgFnS
dluhy	dluh	k1gInPc4
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
obrannou	obranný	k2eAgFnSc4d1
složku	složka	k1gFnSc4
zřídili	zřídit	k5eAaPmAgMnP
komunisté	komunista	k1gMnPc1
Rudou	rudý	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
dalšími	další	k2eAgInPc7d1
dekrety	dekret	k1gInPc7
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
odluka	odluka	k1gFnSc1
církve	církev	k1gFnSc2
od	od	k7c2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1918	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
RSFSR	RSFSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
podepsali	podepsat	k5eAaPmAgMnP
v	v	k7c6
Brest-Litevsku	Brest-Litevsko	k1gNnSc6
zástupci	zástupce	k1gMnPc1
sovětského	sovětský	k2eAgNnSc2d1
Ruska	Rusko	k1gNnSc2
separátní	separátní	k2eAgFnSc4d1
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
(	(	kIx(
<g/>
Brestlitevský	brestlitevský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
ukončující	ukončující	k2eAgFnSc1d1
účast	účast	k1gFnSc1
Ruska	Rusko	k1gNnSc2
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
tvrdých	tvrdý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
separátního	separátní	k2eAgInSc2d1
míru	mír	k1gInSc2
přišlo	přijít	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
o	o	k7c4
Finsko	Finsko	k1gNnSc4
<g/>
,	,	kIx,
Pobaltí	Pobaltí	k1gNnSc4
<g/>
,	,	kIx,
část	část	k1gFnSc4
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
Ukrajinu	Ukrajina	k1gFnSc4
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc4
a	a	k8xC
Besarábii	Besarábie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
ovšem	ovšem	k9
v	v	k7c6
zemi	zem	k1gFnSc6
vypukla	vypuknout	k5eAaPmAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
proti	proti	k7c3
komunistům	komunista	k1gMnPc3
povstalo	povstat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
jejich	jejich	k3xOp3gMnPc2
odpůrců	odpůrce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
velmi	velmi	k6eAd1
nejednotní	jednotný	k2eNgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
nemohli	moct	k5eNaImAgMnP
být	být	k5eAaImF
pro	pro	k7c4
bolševiky	bolševik	k1gMnPc4
silným	silný	k2eAgMnSc7d1
protivníkem	protivník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
i	i	k9
přes	přes	k7c4
několik	několik	k4yIc4
intervencí	intervence	k1gFnPc2
vojsk	vojsko	k1gNnPc2
Dohody	dohoda	k1gFnSc2
dokázala	dokázat	k5eAaPmAgFnS
během	během	k7c2
několika	několik	k4yIc2
let	léto	k1gNnPc2
odpor	odpor	k1gInSc4
potlačit	potlačit	k5eAaPmF
a	a	k8xC
ustanovit	ustanovit	k5eAaPmF
sovětskou	sovětský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
<g/>
,	,	kIx,
Zakavkazska	Zakavkazsko	k1gNnSc2
<g/>
,	,	kIx,
Ukrajiny	Ukrajina	k1gFnSc2
a	a	k8xC
Běloruska	Bělorusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
však	však	k9
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
Rusko-polské	rusko-polský	k2eAgFnSc2d1
války	válka	k1gFnSc2
o	o	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Běloruska	Bělorusko	k1gNnSc2
a	a	k8xC
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
také	také	k9
ke	k	k7c3
změně	změna	k1gFnSc3
<g/>
;	;	kIx,
po	po	k7c6
izolacionismu	izolacionismus	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyvolaly	vyvolat	k5eAaPmAgFnP
spory	spora	k1gFnPc4
ohledně	ohledně	k7c2
neuznání	neuznání	k1gNnSc2
starých	starý	k2eAgInPc2d1
ruských	ruský	k2eAgInPc2d1
dluhů	dluh	k1gInPc2
<g/>
,	,	kIx,
navázala	navázat	k5eAaPmAgFnS
RSFSR	RSFSR	kA
postupně	postupně	k6eAd1
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
Francií	Francie	k1gFnSc7
a	a	k8xC
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1922	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
spojení	spojení	k1gNnSc3
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
Ukrajinské	ukrajinský	k2eAgInPc1d1
SSR	SSR	kA
<g/>
,	,	kIx,
Běloruské	běloruský	k2eAgFnPc4d1
SSR	SSR	kA
a	a	k8xC
Zakavkazské	zakavkazský	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
tak	tak	k6eAd1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Období	období	k1gNnSc1
stalinismu	stalinismus	k1gInSc2
</s>
<s>
Pět	pět	k4xCc1
maršálů	maršál	k1gMnPc2
SSSR	SSSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
dva	dva	k4xCgMnPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
–	–	k?
Buďonnyj	Buďonnyj	k1gInSc1
a	a	k8xC
Vorošilov	Vorošilov	k1gInSc1
–	–	k?
přežili	přežít	k5eAaPmAgMnP
stalinské	stalinský	k2eAgFnPc4d1
čistky	čistka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Stalinismus	stalinismus	k1gInSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
čistka	čistka	k1gFnSc1
a	a	k8xC
Kolektivizace	kolektivizace	k1gFnSc1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
roku	rok	k1gInSc2
1924	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
Vladimir	Vladimir	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Lenin	Lenin	k1gMnSc1
<g/>
,	,	kIx,
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
chopil	chopit	k5eAaPmAgMnS
Josif	Josif	k1gMnSc1
Stalin	Stalin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
realizaci	realizace	k1gFnSc4
mnohých	mnohý	k2eAgInPc2d1
velkolepých	velkolepý	k2eAgInPc2d1
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnPc1
i	i	k8xC
muži	muž	k1gMnPc1
byli	být	k5eAaImAgMnP
(	(	kIx(
<g/>
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
<g/>
)	)	kIx)
zrovnoprávněni	zrovnoprávněn	k2eAgMnPc1d1
<g/>
,	,	kIx,
zahájena	zahájit	k5eAaPmNgFnS
byla	být	k5eAaImAgFnS
kolektivizace	kolektivizace	k1gFnSc1
a	a	k8xC
industrializace	industrializace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
vzdělání	vzdělání	k1gNnSc3
<g/>
,	,	kIx,
byť	byť	k8xS
ovlivněnému	ovlivněný	k2eAgInSc3d1
socialistickou	socialistický	k2eAgFnSc4d1
ideologií	ideologie	k1gFnSc7
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
přístup	přístup	k1gInSc4
všichni	všechen	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšířila	rozšířit	k5eAaPmAgNnP
se	se	k3xPyFc4
i	i	k8xC
lékařská	lékařský	k2eAgFnSc1d1
péče	péče	k1gFnSc1
a	a	k8xC
likvidovány	likvidován	k2eAgFnPc1d1
tak	tak	k6eAd1
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
mnohé	mnohý	k2eAgFnPc4d1
choroby	choroba	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
cholera	cholera	k1gFnSc1
<g/>
,	,	kIx,
malárie	malárie	k1gFnSc1
apod.	apod.	kA
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
plánů	plán	k1gInPc2
budování	budování	k1gNnSc2
socialistického	socialistický	k2eAgInSc2d1
státu	stát	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
násilná	násilný	k2eAgFnSc1d1
kolektivizace	kolektivizace	k1gFnSc1
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zemi	zem	k1gFnSc4
po	po	k7c6
následných	následný	k2eAgFnPc6d1
chybách	chyba	k1gFnPc6
v	v	k7c6
celém	celý	k2eAgInSc6d1
zemědělském	zemědělský	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
postihl	postihnout	k5eAaPmAgInS
hladomor	hladomor	k1gInSc1
<g/>
,	,	kIx,
často	často	k6eAd1
vyvolaný	vyvolaný	k2eAgInSc4d1
jako	jako	k8xC,k8xS
donucovací	donucovací	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
proti	proti	k7c3
odpůrcům	odpůrce	k1gMnPc3
kolektivizace	kolektivizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
gulazích	gulag	k1gInPc6
na	na	k7c6
Sibiři	Sibiř	k1gFnSc6
zahynuly	zahynout	k5eAaPmAgInP
miliony	milion	k4xCgInPc1
odpůrců	odpůrce	k1gMnPc2
tehdejšího	tehdejší	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalinem	Stalin	k1gMnSc7
vyvolaný	vyvolaný	k2eAgMnSc1d1
hladomor	hladomor	k1gMnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
postihl	postihnout	k5eAaPmAgInS
Ukrajinu	Ukrajina	k1gFnSc4
<g/>
,	,	kIx,
jižní	jižní	k2eAgNnSc4d1
Rusko	Rusko	k1gNnSc4
a	a	k8xC
severní	severní	k2eAgInSc4d1
Kazachstán	Kazachstán	k1gInSc4
<g/>
,	,	kIx,
si	se	k3xPyFc3
vyžádal	vyžádat	k5eAaPmAgInS
až	až	k9
7	#num#	k4
milionů	milion	k4xCgInPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
celkem	celkem	k6eAd1
měl	mít	k5eAaImAgMnS
Stalinův	Stalinův	k2eAgInSc4d1
režim	režim	k1gInSc4
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
na	na	k7c6
svědomí	svědomí	k1gNnSc6
až	až	k9
11	#num#	k4
milionů	milion	k4xCgInPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
akce	akce	k1gFnPc1
snížily	snížit	k5eAaPmAgFnP
i	i	k9
jinak	jinak	k6eAd1
velký	velký	k2eAgInSc4d1
hospodářský	hospodářský	k2eAgInSc4d1
růst	růst	k1gInSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
snížily	snížit	k5eAaPmAgFnP
bojeschopnost	bojeschopnost	k1gFnSc4
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
poškodily	poškodit	k5eAaPmAgInP
i	i	k9
další	další	k2eAgFnPc1d1
důležité	důležitý	k2eAgFnPc1d1
složky	složka	k1gFnPc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
zkolektivizováno	zkolektivizovat	k5eAaPmNgNnS
90	#num#	k4
%	%	kIx~
veškerých	veškerý	k3xTgFnPc2
zemědělských	zemědělský	k2eAgFnPc2d1
hospodářství	hospodářství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
a	a	k8xC
velkolepá	velkolepý	k2eAgFnSc1d1
industrializace	industrializace	k1gFnSc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
zaostalých	zaostalý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
zemi	zem	k1gFnSc4
pomoci	pomoc	k1gFnSc2
vymanit	vymanit	k5eAaPmF
se	se	k3xPyFc4
mnohde	mnohde	k6eAd1
ze	z	k7c2
středověkých	středověký	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaváděla	zavádět	k5eAaImAgFnS
se	se	k3xPyFc4
elektřina	elektřina	k1gFnSc1
<g/>
,	,	kIx,
zvýšila	zvýšit	k5eAaPmAgFnS
se	se	k3xPyFc4
vzdělanost	vzdělanost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
průmyslové	průmyslový	k2eAgInPc1d1
závody	závod	k1gInPc1
schopné	schopný	k2eAgInPc1d1
vyrábět	vyrábět	k5eAaImF
vojenskou	vojenský	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
byly	být	k5eAaImAgFnP
odpovědí	odpověď	k1gFnSc7
na	na	k7c4
vzrůstající	vzrůstající	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
Německa	Německo	k1gNnSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
podepsal	podepsat	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
spojeneckou	spojenecký	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Československem	Československo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
si	se	k3xPyFc3
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
získal	získat	k5eAaPmAgMnS
mnoho	mnoho	k4c4
sympatizantů	sympatizant	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
evropské	evropský	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
časech	čas	k1gInPc6
světové	světový	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
SSSR	SSSR	kA
díky	díky	k7c3
izolovanosti	izolovanost	k1gFnSc3
od	od	k7c2
okolního	okolní	k2eAgInSc2d1
světa	svět	k1gInSc2
nepostihla	postihnout	k5eNaPmAgNnP
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Východní	východní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sovětské	sovětský	k2eAgInPc1d1
válečné	válečný	k2eAgInPc1d1
zločiny	zločin	k1gInPc1
a	a	k8xC
Sovětští	sovětský	k2eAgMnPc1d1
váleční	váleční	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
německém	německý	k2eAgNnSc6d1
obležení	obležení	k1gNnSc6
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
trvalo	trvat	k5eAaImAgNnS
872	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
víc	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
jednal	jednat	k5eAaImAgInS
před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
nejprve	nejprve	k6eAd1
s	s	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
zejména	zejména	k9
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
není	být	k5eNaImIp3nS
ochotna	ochoten	k2eAgFnSc1d1
podporovat	podporovat	k5eAaImF
jeho	jeho	k3xOp3gFnSc3
expanzi	expanze	k1gFnSc3
vůči	vůči	k7c3
státům	stát	k1gInPc3
spadajícím	spadající	k2eAgInPc3d1
dříve	dříve	k6eAd2
pod	pod	k7c4
vládu	vláda	k1gFnSc4
carského	carský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
obrátil	obrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
nacistické	nacistický	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
si	se	k3xPyFc3
nakonec	nakonec	k6eAd1
dohodl	dohodnout	k5eAaPmAgMnS
rozdělení	rozdělení	k1gNnSc4
sfér	sféra	k1gFnPc2
vlivu	vliv	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
paktu	pakt	k1gInSc2
Ribbentrop	Ribbentrop	k1gInSc1
<g/>
–	–	k?
<g/>
Molotov	Molotov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Podílel	podílet	k5eAaImAgInS
se	se	k3xPyFc4
na	na	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
Polsku	Polsko	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bez	bez	k7c2
vyhlášení	vyhlášení	k1gNnSc2
války	válka	k1gFnSc2
přepadl	přepadnout	k5eAaPmAgInS
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabral	zabrat	k5eAaPmAgMnS
východní	východní	k2eAgFnSc4d1
polovinu	polovina	k1gFnSc4
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
též	též	k9
si	se	k3xPyFc3
zajistil	zajistit	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Baltským	baltský	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
když	když	k8xS
nejprve	nejprve	k6eAd1
přinutil	přinutit	k5eAaPmAgMnS
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
a	a	k8xC
poté	poté	k6eAd1
násilím	násilí	k1gNnSc7
zabral	zabrat	k5eAaPmAgMnS
Estonsko	Estonsko	k1gNnSc4
<g/>
,	,	kIx,
Litvu	Litva	k1gFnSc4
a	a	k8xC
Lotyšsko	Lotyšsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
války	válka	k1gFnSc2
se	se	k3xPyFc4
vůči	vůči	k7c3
obyvatelstvu	obyvatelstvo	k1gNnSc3
dobytých	dobytý	k2eAgFnPc2d1
a	a	k8xC
obsazených	obsazený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
rozsáhle	rozsáhle	k6eAd1
dopouštěl	dopouštět	k5eAaImAgInS
válečných	válečný	k2eAgInPc2d1
zločinů	zločin	k1gInPc2
a	a	k8xC
zločinů	zločin	k1gInPc2
proti	proti	k7c3
lidskosti	lidskost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
válčil	válčit	k5eAaImAgInS
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
s	s	k7c7
Finskem	Finsko	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hned	hned	k6eAd1
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
a	a	k8xC
1940	#num#	k4
–	–	k?
tzv.	tzv.	kA
zimní	zimní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
pak	pak	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1941	#num#	k4
až	až	k9
1945	#num#	k4
-	-	kIx~
tzv.	tzv.	kA
pokračovací	pokračovací	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
přepadení	přepadení	k1gNnSc4
Finska	Finsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byl	být	k5eAaImAgMnS
vyloučen	vyloučit	k5eAaPmNgMnS
ze	z	k7c2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Churchill	Churchill	k1gMnSc1
<g/>
,	,	kIx,
Roosevelt	Roosevelt	k1gMnSc1
a	a	k8xC
Stalin	Stalin	k1gMnSc1
v	v	k7c6
Jaltě	Jalta	k1gFnSc6
na	na	k7c6
Krymu	Krym	k1gInSc6
v	v	k7c6
únoru	únor	k1gInSc6
1945	#num#	k4
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
byl	být	k5eAaImAgInS
nečekaně	nečekaně	k6eAd1
napaden	napadnout	k5eAaPmNgInS
Německem	Německo	k1gNnSc7
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
a	a	k8xC
kvůli	kvůli	k7c3
nepřipravenosti	nepřipravenost	k1gFnSc3
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
postupovali	postupovat	k5eAaImAgMnP
Němci	Němec	k1gMnPc1
úspěšně	úspěšně	k6eAd1
hluboko	hluboko	k6eAd1
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacistickým	nacistický	k2eAgFnPc3d1
silám	síla	k1gFnPc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
obsadit	obsadit	k5eAaPmF
západní	západní	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
též	též	k9
i	i	k9
významnou	významný	k2eAgFnSc4d1
část	část	k1gFnSc4
RSFSR	RSFSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
až	až	k9
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Stalingradu	Stalingrad	k1gInSc2
i	i	k9
díky	díky	k7c3
pomoci	pomoc	k1gFnSc3
západních	západní	k2eAgMnPc2d1
Spojenců	spojenec	k1gMnPc2
a	a	k8xC
přesunu	přesun	k1gInSc2
průmyslových	průmyslový	k2eAgInPc2d1
závodů	závod	k1gInPc2
do	do	k7c2
bezpečných	bezpečný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Uralu	Ural	k1gInSc2
se	se	k3xPyFc4
situace	situace	k1gFnSc1
obrátila	obrátit	k5eAaPmAgFnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
Němce	Němec	k1gMnPc4
vytlačila	vytlačit	k5eAaPmAgFnS
přes	přes	k7c4
východní	východní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
až	až	k9
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
podkladě	podklad	k1gInSc6
dohod	dohoda	k1gFnPc2
jaltské	jaltský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
vyhlásil	vyhlásit	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	#num#	k4
válku	válka	k1gFnSc4
Japonsku	Japonsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
týdne	týden	k1gInSc2
rozdrtil	rozdrtit	k5eAaPmAgInS
ve	v	k7c6
skvěle	skvěle	k6eAd1
naplánované	naplánovaný	k2eAgFnSc6d1
a	a	k8xC
provedené	provedený	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
Kuantungskou	Kuantungský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
obklíčené	obklíčený	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
se	se	k3xPyFc4
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	#num#	k4
vzdaly	vzdát	k5eAaPmAgFnP
(	(	kIx(
<g/>
na	na	k7c6
Sachalinu	Sachalin	k1gInSc6
tak	tak	k6eAd1
učinily	učinit	k5eAaImAgFnP,k5eAaPmAgFnP
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
posledním	poslední	k2eAgMnSc6d1
z	z	k7c2
Kurilských	kurilský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
v	v	k7c6
boji	boj	k1gInSc6
8,7	8,7	k4
milionů	milion	k4xCgInPc2
vojáků	voják	k1gMnPc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
5,8	5,8	k4
milionů	milion	k4xCgInPc2
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
1,4	1,4	k4
milionů	milion	k4xCgInPc2
Ukrajinců	Ukrajinec	k1gMnPc2
a	a	k8xC
1,5	1,5	k4
milionů	milion	k4xCgInPc2
příslušníků	příslušník	k1gMnPc2
ostatních	ostatní	k2eAgInPc2d1
sovětských	sovětský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
v	v	k7c6
letech	léto	k1gNnPc6
1941-42	1941-42	k4
v	v	k7c6
německém	německý	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
zahynulo	zahynout	k5eAaPmAgNnS
okolo	okolo	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
milionů	milion	k4xCgInPc2
sovětských	sovětský	k2eAgMnPc2d1
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
německého	německý	k2eAgInSc2d1
plánu	plán	k1gInSc2
genocidy	genocida	k1gFnSc2
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
tzv.	tzv.	kA
Generalplan	Generalplan	k1gInSc1
Ost	Ost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Němečtí	německý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c6
sovětském	sovětský	k2eAgNnSc6d1
území	území	k1gNnSc6
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
dopouštěli	dopouštět	k5eAaImAgMnP
válečných	válečný	k2eAgMnPc2d1
zločinů	zločin	k1gInPc2
a	a	k8xC
zločinů	zločin	k1gInPc2
proti	proti	k7c3
lidskosti	lidskost	k1gFnSc3
jako	jako	k8xC,k8xS
byl	být	k5eAaImAgMnS
například	například	k6eAd1
masakr	masakr	k1gInSc4
v	v	k7c6
Oděse	Oděsa	k1gFnSc6
v	v	k7c6
říjnu	říjen	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
rumunské	rumunský	k2eAgFnSc2d1
a	a	k8xC
německé	německý	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
zastřelily	zastřelit	k5eAaPmAgFnP
nebo	nebo	k8xC
upálily	upálit	k5eAaPmAgFnP
až	až	k9
34	#num#	k4
000	#num#	k4
oděských	oděský	k2eAgMnPc2d1
Židů	Žid	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
německými	německý	k2eAgNnPc7d1
a	a	k8xC
finskými	finský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
životy	život	k1gInPc4
1	#num#	k4
milionu	milion	k4xCgInSc2
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
byly	být	k5eAaImAgFnP
vyvražděny	vyvražděn	k2eAgFnPc1d1
stovky	stovka	k1gFnPc1
vesnic	vesnice	k1gFnPc2
a	a	k8xC
celkem	celkem	k6eAd1
zahynula	zahynout	k5eAaPmAgFnS
asi	asi	k9
čtvrtina	čtvrtina	k1gFnSc1
tehdejší	tehdejší	k2eAgFnSc2d1
běloruské	běloruský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poválečné	poválečný	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
utrpěl	utrpět	k5eAaPmAgInS
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
<g/>
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
největší	veliký	k2eAgFnSc2d3
vojenské	vojenský	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
ze	z	k7c2
všech	všecek	k3xTgFnPc2
bojujících	bojující	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
Německý	německý	k2eAgInSc1d1
wehrmacht	wehrmacht	k1gInSc1
utrpěl	utrpět	k5eAaPmAgInS
80	#num#	k4
%	%	kIx~
veškerých	veškerý	k3xTgFnPc2
vojenských	vojenský	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
při	při	k7c6
bojích	boj	k1gInPc6
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
osvobodila	osvobodit	k5eAaPmAgFnS
střední	střední	k2eAgFnSc7d1
a	a	k8xC
východní	východní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
vítězství	vítězství	k1gNnSc3
<g/>
,	,	kIx,
za	za	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
zaplatil	zaplatit	k5eAaPmAgInS
26	#num#	k4
miliony	milion	k4xCgInPc1
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
zničenou	zničený	k2eAgFnSc7d1
velkou	velký	k2eAgFnSc7d1
částí	část	k1gFnSc7
země	zem	k1gFnSc2
a	a	k8xC
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
SSSR	SSSR	kA
ohromnou	ohromný	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
a	a	k8xC
komunisté	komunista	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
vnímáni	vnímán	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
zachránci	zachránce	k1gMnPc1
lidstva	lidstvo	k1gNnSc2
před	před	k7c7
nacismem	nacismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadšení	nadšení	k1gNnPc1
však	však	k8xC
rychle	rychle	k6eAd1
vystřídalo	vystřídat	k5eAaPmAgNnS
zklamání	zklamání	k1gNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
si	se	k3xPyFc3
osvobozené	osvobozený	k2eAgFnPc4d1
země	zem	k1gFnPc4
podrobil	podrobit	k5eAaPmAgMnS
a	a	k8xC
udělal	udělat	k5eAaPmAgMnS
z	z	k7c2
nich	on	k3xPp3gMnPc2
pouze	pouze	k6eAd1
formálně	formálně	k6eAd1
nezávislé	závislý	k2eNgInPc1d1
satelity	satelit	k1gInPc1
s	s	k7c7
komunistickým	komunistický	k2eAgNnSc7d1
zřízením	zřízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
samotném	samotný	k2eAgInSc6d1
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
pokračovalo	pokračovat	k5eAaImAgNnS
rychle	rychle	k6eAd1
tempo	tempo	k1gNnSc1
industrializace	industrializace	k1gFnSc2
započaté	započatý	k2eAgFnSc2d1
již	již	k6eAd1
v	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Stalinem	Stalin	k1gMnSc7
<g/>
,	,	kIx,
přicházely	přicházet	k5eAaImAgFnP
velké	velký	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
(	(	kIx(
<g/>
například	například	k6eAd1
sestrojení	sestrojení	k1gNnSc1
jaderné	jaderný	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stavěla	stavět	k5eAaImAgNnP
se	se	k3xPyFc4
nová	nový	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
,	,	kIx,
elektrárny	elektrárna	k1gFnPc1
i	i	k8xC
továrny	továrna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režim	režim	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
stále	stále	k6eAd1
velmi	velmi	k6eAd1
tuhý	tuhý	k2eAgInSc1d1
a	a	k8xC
i	i	k9
přes	přes	k7c4
určité	určitý	k2eAgNnSc4d1
nadšení	nadšení	k1gNnSc4
z	z	k7c2
porážky	porážka	k1gFnSc2
Německa	Německo	k1gNnSc2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
plánovitě	plánovitě	k6eAd1
šířit	šířit	k5eAaImF
nenávist	nenávist	k1gFnSc4
k	k	k7c3
západním	západní	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
této	tento	k3xDgFnSc3
válce	válka	k1gFnSc3
se	se	k3xPyFc4
SSSR	SSSR	kA
podařilo	podařit	k5eAaPmAgNnS
rozšířit	rozšířit	k5eAaPmF
socialismus	socialismus	k1gInSc4
do	do	k7c2
východní	východní	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
na	na	k7c6
západní	západní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
(	(	kIx(
<g/>
Kuba	Kuba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
vznikly	vzniknout	k5eAaPmAgFnP
komunistické	komunistický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
spolupracující	spolupracující	k2eAgInPc1d1
se	s	k7c7
SSSR	SSSR	kA
<g/>
,	,	kIx,
a	a	k8xC
oficiálně	oficiálně	k6eAd1
usilující	usilující	k2eAgFnPc1d1
o	o	k7c4
nastolení	nastolení	k1gNnSc4
komunismu	komunismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Jimmy	Jimm	k1gInPc1
Carter	Cartra	k1gFnPc2
a	a	k8xC
Leonid	Leonid	k1gInSc1
Brežněv	Brežněv	k1gFnSc2
při	při	k7c6
ratifikaci	ratifikace	k1gFnSc6
START-2	START-2	k1gFnSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
soupeřil	soupeřit	k5eAaImAgInS
s	s	k7c7
USA	USA	kA
v	v	k7c6
kosmickém	kosmický	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
(	(	kIx(
<g/>
vyslání	vyslání	k1gNnSc6
prvního	první	k4xOgMnSc2
člověka	člověk	k1gMnSc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ideologicky	ideologicky	k6eAd1
(	(	kIx(
<g/>
souboj	souboj	k1gInSc1
–	–	k?
podle	podle	k7c2
slov	slovo	k1gNnPc2
mnohých	mnohý	k2eAgMnPc2d1
sovětských	sovětský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
–	–	k?
socialismu	socialismus	k1gInSc2
a	a	k8xC
kapitalismu	kapitalismus	k1gInSc2
<g/>
,	,	kIx,
či	či	k8xC
–	–	k?
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
bývalo	bývat	k5eAaImAgNnS
označováno	označovat	k5eAaImNgNnS
na	na	k7c6
západě	západ	k1gInSc6
–	–	k?
demokracie	demokracie	k1gFnSc2
a	a	k8xC
totality	totalita	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
nepřímo	přímo	k6eNd1
vojensky	vojensky	k6eAd1
<g/>
,	,	kIx,
podporou	podpora	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
spojenců	spojenec	k1gMnPc2
(	(	kIx(
<g/>
korejská	korejský	k2eAgFnSc1d1
či	či	k8xC
vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
arabsko-izraelský	arabsko-izraelský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k1gNnPc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
jinde	jinde	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
se	se	k3xPyFc4
tak	tak	k9
stal	stát	k5eAaPmAgMnS
bipolárním	bipolární	k2eAgNnSc7d1
<g/>
;	;	kIx,
vznikly	vzniknout	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
sféry	sféra	k1gFnPc1
vlivu	vliv	k1gInSc2
<g/>
,	,	kIx,
sovětská	sovětský	k2eAgFnSc1d1
a	a	k8xC
americká	americký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zánik	zánik	k1gInSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Rozpad	rozpad	k1gInSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Závody	závod	k1gInPc1
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
mezi	mezi	k7c7
SSSR	SSSR	kA
a	a	k8xC
USA	USA	kA
<g/>
,	,	kIx,
vyjádřené	vyjádřený	k2eAgFnSc2d1
počtem	počet	k1gInSc7
jaderných	jaderný	k2eAgFnPc2d1
hlavic	hlavice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
boji	boj	k1gInSc6
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
SSSR	SSSR	kA
ekonomicky	ekonomicky	k6eAd1
postupně	postupně	k6eAd1
vyčerpáván	vyčerpáván	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
nedosáhl	dosáhnout	k5eNaPmAgInS
takových	takový	k3xDgNnPc2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
pak	pak	k6eAd1
nastal	nastat	k5eAaPmAgInS
hlavně	hlavně	k9
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
politikou	politika	k1gFnSc7
cíleného	cílený	k2eAgNnSc2d1
zbrojení	zbrojení	k1gNnSc2
dokázal	dokázat	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
donutit	donutit	k5eAaPmF
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
vynakládat	vynakládat	k5eAaImF
na	na	k7c4
zbrojení	zbrojení	k1gNnSc4
až	až	k8xS
desítky	desítka	k1gFnPc4
procent	procento	k1gNnPc2
HDP	HDP	kA
země	zem	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
generálové	generál	k1gMnPc1
postupně	postupně	k6eAd1
prohrávali	prohrávat	k5eAaImAgMnP
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
tedy	tedy	k9
následně	následně	k6eAd1
projevilo	projevit	k5eAaPmAgNnS
ekonomickým	ekonomický	k2eAgInSc7d1
poklesem	pokles	k1gInSc7
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
nedostatky	nedostatek	k1gInPc7
socialistické	socialistický	k2eAgFnSc2d1
centrálně	centrálně	k6eAd1
plánované	plánovaný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
úpadkem	úpadek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1986	#num#	k4
zasáhla	zasáhnout	k5eAaPmAgFnS
Černobylská	černobylský	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
prokázala	prokázat	k5eAaPmAgFnS
neschopnost	neschopnost	k1gFnSc4
a	a	k8xC
hlavně	hlavně	k9
nepřipravenost	nepřipravenost	k1gFnSc4
klíčových	klíčový	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
orgánů	orgán	k1gInPc2
čelit	čelit	k5eAaImF
události	událost	k1gFnPc4
takového	takový	k3xDgInSc2
rozsahu	rozsah	k1gInSc2
<g/>
,	,	kIx,
byť	byť	k8xS
země	země	k1gFnSc1
jako	jako	k8xC,k8xS
samotná	samotný	k2eAgFnSc1d1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
oficiálně	oficiálně	k6eAd1
připravená	připravený	k2eAgFnSc1d1
stále	stále	k6eAd1
i	i	k9
na	na	k7c4
jadernou	jaderný	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
území	území	k1gNnSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
k	k	k7c3
tzv.	tzv.	kA
ropnému	ropný	k2eAgInSc3d1
zlomu	zlom	k1gInSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
produkce	produkce	k1gFnSc1
ropy	ropa	k1gFnSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
svého	svůj	k3xOyFgNnSc2
historického	historický	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
a	a	k8xC
od	od	k7c2
toho	ten	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
začala	začít	k5eAaPmAgFnS
klesat	klesat	k5eAaImF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
poptávka	poptávka	k1gFnSc1
po	po	k7c6
ropě	ropa	k1gFnSc6
(	(	kIx(
<g/>
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
průmyslu	průmysl	k1gInSc2
<g/>
)	)	kIx)
nepřestala	přestat	k5eNaPmAgFnS
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
významný	významný	k2eAgInSc1d1
překmit	překmit	k1gInSc1
prakticky	prakticky	k6eAd1
nebyl	být	k5eNaImAgInS
nijak	nijak	k6eAd1
korigován	korigovat	k5eAaBmNgInS
a	a	k8xC
během	během	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
až	až	k6eAd1
dvou	dva	k4xCgInPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
příčinou	příčina	k1gFnSc7
vážných	vážný	k2eAgInPc2d1
hospodářských	hospodářský	k2eAgInPc2d1
problémů	problém	k1gInPc2
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
alokací	alokace	k1gFnSc7
potřebné	potřebný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
Náhorní	náhorní	k2eAgInSc4d1
Karabach	Karabach	k1gInSc4
mezi	mezi	k7c7
arménskými	arménský	k2eAgMnPc7d1
separatisty	separatista	k1gMnPc7
a	a	k8xC
Ázerbájdžánci	Ázerbájdžánec	k1gMnPc7
vypukla	vypuknout	k5eAaPmAgFnS
během	během	k7c2
rozpadu	rozpad	k1gInSc2
SSSR	SSSR	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
nabyly	nabýt	k5eAaPmAgFnP
všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
trendy	trend	k1gInPc4
a	a	k8xC
vlivy	vliv	k1gInPc4
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dál	daleko	k6eAd2
konkrétnějších	konkrétní	k2eAgFnPc2d2
podob	podoba	k1gFnPc2
–	–	k?
synergii	synergie	k1gFnSc3
neudržitelnosti	neudržitelnost	k1gFnSc2
stávajícího	stávající	k2eAgInSc2d1
stavu	stav	k1gInSc2
a	a	k8xC
„	„	k?
<g/>
vítr	vítr	k1gInSc1
změny	změna	k1gFnSc2
<g/>
“	“	k?
začaly	začít	k5eAaPmAgFnP
cítit	cítit	k5eAaImF
státy	stát	k1gInPc4
pod	pod	k7c7
sovětským	sovětský	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
hnutí	hnutí	k1gNnSc2
Solidarita	solidarita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
letech	let	k1gInPc6
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
těchto	tento	k3xDgInPc6
státech	stát	k1gInPc6
ke	k	k7c3
změnám	změna	k1gFnPc3
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
KSSS	KSSS	kA
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc1
jako	jako	k8xS,k8xC
odpověď	odpověď	k1gFnSc1
na	na	k7c4
narůstající	narůstající	k2eAgNnSc4d1
množství	množství	k1gNnSc4
problémů	problém	k1gInPc2
zahájil	zahájit	k5eAaPmAgMnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
program	program	k1gInSc1
tzv.	tzv.	kA
perestrojky	perestrojka	k1gFnSc2
a	a	k8xC
demokratizace	demokratizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stažena	stažen	k2eAgFnSc1d1
byla	být	k5eAaImAgNnP
i	i	k9
vojska	vojsko	k1gNnPc1
z	z	k7c2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politika	politika	k1gFnSc1
glasnosti	glasnost	k1gFnSc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
změně	změna	k1gFnSc3
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
probírala	probírat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
oficiálně	oficiálně	k6eAd1
podporovaného	podporovaný	k2eAgNnSc2d1
budování	budování	k1gNnSc2
komunismu	komunismus	k1gInSc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
mluvit	mluvit	k5eAaImF
o	o	k7c6
omylech	omyl	k1gInPc6
a	a	k8xC
přehmatech	přehmat	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k8xS,k8xC
dřívějších	dřívější	k2eAgInPc2d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k9
současných	současný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kredit	kredit	k1gInSc1
sovětské	sovětský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
tak	tak	k9
ještě	ještě	k6eAd1
klesl	klesnout	k5eAaPmAgInS
a	a	k8xC
nespokojenost	nespokojenost	k1gFnSc4
se	se	k3xPyFc4
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
klesající	klesající	k2eAgFnSc7d1
hospodářskou	hospodářský	k2eAgFnSc7d1
výkonnosti	výkonnost	k1gFnSc6
zvýšila	zvýšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
náboženské	náboženský	k2eAgInPc1d1
<g/>
,	,	kIx,
národnostní	národnostní	k2eAgInPc1d1
i	i	k8xC
sociální	sociální	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
později	pozdě	k6eAd2
přerostly	přerůst	k5eAaPmAgInP
do	do	k7c2
neřešitelných	řešitelný	k2eNgFnPc2d1
mezí	mez	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Gorbačov	Gorbačov	k1gInSc1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
vztahů	vztah	k1gInPc2
se	s	k7c7
Západem	západ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
podpepsal	podpepsat	k5eAaPmAgInS,k5eAaImAgInS
s	s	k7c7
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Reaganem	Reagan	k1gMnSc7
Smlouvu	smlouva	k1gFnSc4
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
středního	střední	k2eAgInSc2d1
a	a	k8xC
kratšího	krátký	k2eAgInSc2d2
doletu	dolet	k1gInSc2
(	(	kIx(
<g/>
INF	INF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
významně	významně	k6eAd1
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
redukci	redukce	k1gFnSc3
světového	světový	k2eAgInSc2d1
jaderného	jaderný	k2eAgInSc2d1
arzenálu	arzenál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
veřejně	veřejně	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
Brežněvovu	Brežněvův	k2eAgFnSc4d1
doktrínu	doktrína	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dal	dát	k5eAaPmAgMnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
nebude	být	k5eNaImBp3nS
do	do	k7c2
dění	dění	k1gNnSc2
ve	v	k7c6
státech	stát	k1gInPc6
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
nijak	nijak	k6eAd1
zasahovat	zasahovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změny	změna	k1gFnPc1
hranic	hranice	k1gFnPc2
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
SSSR	SSSR	kA
se	se	k3xPyFc4
proto	proto	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
rozpadl	rozpadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vyhlásily	vyhlásit	k5eAaPmAgFnP
nezávislost	nezávislost	k1gFnSc4
pobaltské	pobaltský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
referendu	referendum	k1gNnSc6
ve	v	k7c6
zbylých	zbylý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
sice	sice	k8xC
většina	většina	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
vyslovila	vyslovit	k5eAaPmAgFnS
pro	pro	k7c4
zachování	zachování	k1gNnSc4
SSSR	SSSR	kA
<g/>
,	,	kIx,
avšak	avšak	k8xC
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
12	#num#	k4
republikami	republika	k1gFnPc7
se	se	k3xPyFc4
nadále	nadále	k6eAd1
horšily	horšit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
přestal	přestat	k5eAaPmAgInS
existovat	existovat	k5eAaImF
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1991	#num#	k4
<g/>
;	;	kIx,
většina	většina	k1gFnSc1
nástupnických	nástupnický	k2eAgInPc2d1
států	stát	k1gInPc2
současně	současně	k6eAd1
utvořila	utvořit	k5eAaPmAgFnS
Společenství	společenství	k1gNnSc3
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nástupnické	nástupnický	k2eAgInPc4d1
státy	stát	k1gInPc4
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
považuje	považovat	k5eAaImIp3nS
jen	jen	k9
těchto	tento	k3xDgFnPc2
12	#num#	k4
svazových	svazový	k2eAgFnPc2d1
republik	republika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pobaltské	pobaltský	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
tj.	tj.	kA
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
a	a	k8xC
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
považují	považovat	k5eAaImIp3nP
své	svůj	k3xOyFgNnSc4
nucené	nucený	k2eAgNnSc4d1
členství	členství	k1gNnSc4
v	v	k7c6
SSSR	SSSR	kA
za	za	k7c4
okupaci	okupace	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
nejsou	být	k5eNaImIp3nP
tudíž	tudíž	k8xC
nástupnickými	nástupnický	k2eAgInPc7d1
státy	stát	k1gInPc7
v	v	k7c6
plném	plný	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
vnímána	vnímat	k5eAaImNgFnS
i	i	k9
v	v	k7c6
západních	západní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozpad	rozpad	k1gInSc1
SSSR	SSSR	kA
měl	mít	k5eAaImAgInS
globální	globální	k2eAgInSc1d1
geopolitický	geopolitický	k2eAgInSc1d1
význam	význam	k1gInSc1
–	–	k?
doslova	doslova	k6eAd1
zatřásl	zatřást	k5eAaPmAgMnS
s	s	k7c7
rovnováhou	rovnováha	k1gFnSc7
sil	síla	k1gFnPc2
ve	v	k7c6
světě	svět	k1gInSc6
–	–	k?
z	z	k7c2
dvoupolárního	dvoupolární	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
rozděleného	rozdělený	k2eAgNnSc2d1
na	na	k7c4
ten	ten	k3xDgInSc4
východní	východní	k2eAgInSc4d1
a	a	k8xC
západní	západní	k2eAgInSc4d1
<g/>
)	)	kIx)
zbyly	zbýt	k5eAaPmAgFnP
jako	jako	k8xC,k8xS
poslední	poslední	k2eAgFnSc4d1
supervelmoc	supervelmoc	k1gFnSc4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
zastihl	zastihnout	k5eAaPmAgMnS
obě	dva	k4xCgFnPc4
supervelmoci	supervelmoc	k1gFnPc4
s	s	k7c7
obrovskou	obrovský	k2eAgFnSc7d1
infrastrukturou	infrastruktura	k1gFnSc7
a	a	k8xC
hlavně	hlavně	k9
arzenálem	arzenál	k1gInSc7
zastarávající	zastarávající	k2eAgFnSc2d1
a	a	k8xC
nyní	nyní	k6eAd1
nevyužitelné	využitelný	k2eNgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
průmyslu	průmysl	k1gInSc2
přímo	přímo	k6eAd1
napojeného	napojený	k2eAgInSc2d1
na	na	k7c6
zbrojení	zbrojení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
„	„	k?
<g/>
desovětizaci	desovětizace	k1gFnSc4
<g/>
“	“	k?
satelitních	satelitní	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
států	stát	k1gInPc2
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
–	–	k?
konec	konec	k1gInSc1
principu	princip	k1gInSc2
politického	politický	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
jedné	jeden	k4xCgFnSc2
vedoucí	vedoucí	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
zavedení	zavedení	k1gNnSc1
demokratických	demokratický	k2eAgInPc2d1
principů	princip	k1gInPc2
<g/>
,	,	kIx,
často	často	k6eAd1
včetně	včetně	k7c2
přepsání	přepsání	k1gNnSc2
samotné	samotný	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
decentralizace	decentralizace	k1gFnSc1
plánování	plánování	k1gNnSc2
<g/>
,	,	kIx,
liberalizace	liberalizace	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
mnoha	mnoho	k4c2
ostatních	ostatní	k2eAgFnPc2d1
agend	agenda	k1gFnPc2
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
fungováním	fungování	k1gNnSc7
státu	stát	k1gInSc2
<g/>
,	,	kIx,
privatizace	privatizace	k1gFnSc2
státního	státní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
zavedení	zavedení	k1gNnSc4
soukromého	soukromý	k2eAgNnSc2d1
podnikání	podnikání	k1gNnSc2
<g/>
,	,	kIx,
příliv	příliv	k1gInSc1
zahraničního	zahraniční	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ekonomickými	ekonomický	k2eAgFnPc7d1
a	a	k8xC
politickými	politický	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
i	i	k9
k	k	k7c3
(	(	kIx(
<g/>
většímu	veliký	k2eAgMnSc3d2
či	či	k8xC
menšímu	malý	k2eAgMnSc3d2
<g/>
)	)	kIx)
vyrovnání	vyrovnání	k1gNnSc3
se	se	k3xPyFc4
se	s	k7c7
zločiny	zločin	k1gInPc7
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
bývalého	bývalý	k2eAgInSc2d1
sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
států	stát	k1gInPc2
pod	pod	k7c7
jeho	jeho	k3xOp3gInSc7
vlivem	vliv	k1gInSc7
zasáhla	zasáhnout	k5eAaPmAgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
hospodářská	hospodářský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
kvůli	kvůli	k7c3
přehnané	přehnaný	k2eAgFnSc3d1
privatizaci	privatizace	k1gFnSc3
propukla	propuknout	k5eAaPmAgFnS
hluboká	hluboký	k2eAgFnSc1d1
hospodářská	hospodářský	k2eAgFnSc1d1
a	a	k8xC
sociální	sociální	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
zkrachovala	zkrachovat	k5eAaPmAgFnS
většina	většina	k1gFnSc1
státních	státní	k2eAgInPc2d1
podniků	podnik	k1gInPc2
a	a	k8xC
rapidně	rapidně	k6eAd1
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
nezaměstnanost	nezaměstnanost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
na	na	k7c6
pomníku	pomník	k1gInSc6
Karla	Karel	k1gMnSc4
Marxe	Marx	k1gMnSc4
objevil	objevit	k5eAaPmAgInS
nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Proletáři	proletář	k1gMnPc1
celého	celý	k2eAgInSc2d1
světa-promiňte	světa-prominit	k5eAaPmRp2nP
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Plakát	plakát	k1gInSc1
sovětsko-kubánského	sovětsko-kubánský	k2eAgNnSc2d1
přátelství	přátelství	k1gNnSc2
z	z	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
s	s	k7c7
Fidelem	Fidel	k1gMnSc7
Castrem	Castr	k1gMnSc7
a	a	k8xC
Nikitou	Nikita	k1gMnSc7
Chruščovem	Chruščovo	k1gNnSc7
</s>
<s>
Gerald	Gerald	k6eAd1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Leonid	Leonid	k1gInSc1
Brežněv	Brežněv	k1gMnSc1
a	a	k8xC
Henry	Henry	k1gMnSc1
Kissinger	Kissinger	k1gMnSc1
se	se	k3xPyFc4
neformálně	formálně	k6eNd1
baví	bavit	k5eAaImIp3nS
na	na	k7c6
Vladivostockém	vladivostocký	k2eAgInSc6d1
summitu	summit	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
</s>
<s>
Konečná	konečný	k2eAgNnPc1d1
politická	politický	k2eAgNnPc1d1
rozhodnutí	rozhodnutí	k1gNnPc1
v	v	k7c6
letech	let	k1gInPc6
1925	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
vždy	vždy	k6eAd1
Stalin	Stalin	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jinak	jinak	k6eAd1
byla	být	k5eAaImAgFnS
sovětská	sovětský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
vyhlašována	vyhlašovat	k5eAaImNgFnS
komisí	komise	k1gFnSc7
pro	pro	k7c4
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
Ústředního	ústřední	k2eAgInSc2d1
výboru	výbor	k1gInSc2
KSSS	KSSS	kA
nebo	nebo	k8xC
nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
strany	strana	k1gFnSc2
-	-	kIx~
politbyrem	politbyro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
mělo	mít	k5eAaImAgNnS
na	na	k7c4
starosti	starost	k1gFnPc4
oddělené	oddělený	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
do	do	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
známé	známá	k1gFnSc2
jako	jako	k8xS,k8xC
Lidový	lidový	k2eAgInSc1d1
komisariát	komisariát	k1gInSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
či	či	k8xC
Narkomindel	Narkomindlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvlivnější	vlivný	k2eAgFnSc4d3
mluvčí	mluvčí	k1gFnSc4
patřili	patřit	k5eAaImAgMnP
Georgij	Georgij	k1gMnSc1
Čičerin	Čičerin	k1gInSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
-	-	kIx~
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maxim	Maxim	k1gMnSc1
Litvinov	Litvinov	k1gInSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
-	-	kIx~
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vjačeslav	Vjačeslav	k1gMnSc1
Molotov	Molotov	k1gInSc1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
-	-	kIx~
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
Vyšinskij	Vyšinskij	k1gMnSc1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
-	-	kIx~
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Andrej	Andrej	k1gMnSc1
Gromyko	Gromyko	k1gNnSc4
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intelektuálové	intelektuál	k1gMnPc1
sídlili	sídlit	k5eAaImAgMnP
v	v	k7c6
Moskevském	moskevský	k2eAgInSc6d1
státním	státní	k2eAgInSc6d1
institutu	institut	k1gInSc6
mezinárodních	mezinárodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Kominterna	Kominterna	k1gFnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Komunistická	komunistický	k2eAgFnSc1d1
internacionála	internacionála	k1gFnSc1
byla	být	k5eAaImAgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Kremlu	Kreml	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
obhajovala	obhajovat	k5eAaImAgFnS
světový	světový	k2eAgInSc4d1
komunismus	komunismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komiterna	Komiterna	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
"	"	kIx"
<g/>
bojovat	bojovat	k5eAaImF
všemi	všecek	k3xTgInPc7
dostupnými	dostupný	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c4
svržení	svržení	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
buržoazie	buržoazie	k1gFnSc2
a	a	k8xC
vytvoření	vytvoření	k1gNnSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jako	jako	k8xS,k8xC
přechodné	přechodný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
než	než	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
zrušení	zrušení	k1gNnSc3
státu	stát	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
smírčí	smírčí	k2eAgNnSc1d1
opatření	opatření	k1gNnSc1
vůči	vůči	k7c3
Británii	Británie	k1gFnSc3
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RVHP	RVHP	kA
(	(	kIx(
<g/>
С	С	k?
Э	Э	k?
В	В	k?
<g/>
,	,	kIx,
Sovět	Sovět	k1gMnSc1
Ekonomičeskoj	Ekonomičeskoj	k1gInSc1
Vzaimopomošči	Vzaimopomošč	k1gFnSc3
<g/>
,	,	kIx,
С	С	k?
<g/>
,	,	kIx,
SEV	SEV	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1949	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
hospodářskou	hospodářský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
pod	pod	k7c7
sovětskou	sovětský	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
státy	stát	k1gInPc4
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
řadou	řada	k1gFnSc7
komunistických	komunistický	k2eAgInPc2d1
států	stát	k1gInPc2
jinde	jinde	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
byla	být	k5eAaImAgFnS
znepokojena	znepokojit	k5eAaPmNgFnS
Marshallovým	Marshallův	k2eAgInSc7d1
plánem	plán	k1gInSc7
a	a	k8xC
cílem	cíl	k1gInSc7
RVHP	RVHP	kA
bylo	být	k5eAaImAgNnS
zabránit	zabránit	k5eAaPmF
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
země	zem	k1gFnPc1
v	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
vlivu	vliv	k1gInSc2
směřovaly	směřovat	k5eAaImAgFnP
k	k	k7c3
Američanům	Američan	k1gMnPc3
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc3d1
Asii	Asie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
RVHP	RVHP	kA
byla	být	k5eAaImAgFnS
také	také	k9
odpovědí	odpověď	k1gFnSc7
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
hospodářskou	hospodářský	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
OEEC	OEEC	kA
<g/>
)	)	kIx)
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Varšavská	varšavský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
byl	být	k5eAaImAgInS
vojenský	vojenský	k2eAgInSc1d1
pakt	pakt	k1gInSc1
zformovaný	zformovaný	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
mezi	mezi	k7c7
SSSR	SSSR	kA
a	a	k8xC
sedmi	sedm	k4xCc7
sovětskými	sovětský	k2eAgInPc7d1
satelitními	satelitní	k2eAgInPc7d1
státy	stát	k1gInPc7
střední	střední	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
během	během	k7c2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varšavská	varšavský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
vojenským	vojenský	k2eAgInSc7d1
doplňkem	doplněk	k1gInSc7
RVHP	RVHP	kA
<g/>
,	,	kIx,
regionální	regionální	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
socialistické	socialistický	k2eAgInPc4d1
státy	stát	k1gInPc4
střední	střední	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
vstup	vstup	k1gInSc4
západního	západní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
do	do	k7c2
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kominforma	kominforma	k1gFnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
-	-	kIx~
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Informační	informační	k2eAgNnSc1d1
byro	byro	k1gNnSc1
komunistických	komunistický	k2eAgFnPc2d1
a	a	k8xC
dělnických	dělnický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
oficiální	oficiální	k2eAgFnSc7d1
agenturou	agentura	k1gFnSc7
mezinárodního	mezinárodní	k2eAgNnSc2d1
komunistického	komunistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
od	od	k7c2
rozpuštění	rozpuštění	k1gNnSc2
Kominterny	Kominterna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
koordinovat	koordinovat	k5eAaBmF
akce	akce	k1gFnPc4
mezi	mezi	k7c7
komunistickými	komunistický	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
pod	pod	k7c7
sovětským	sovětský	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalin	Stalin	k1gMnSc1
ji	on	k3xPp3gFnSc4
využil	využít	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nařídil	nařídit	k5eAaPmAgMnS
západoevropským	západoevropský	k2eAgFnPc3d1
komunistickým	komunistický	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
opustily	opustit	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
výlučně	výlučně	k6eAd1
parlamentní	parlamentní	k2eAgFnSc4d1
linii	linie	k1gFnSc4
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
soustředily	soustředit	k5eAaPmAgInP
na	na	k7c4
politické	politický	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
operacím	operace	k1gFnPc3
ohledně	ohledně	k7c2
Marshallova	Marshallův	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Také	také	k6eAd1
koordinovala	koordinovat	k5eAaBmAgFnS
mezinárodní	mezinárodní	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
komunistickým	komunistický	k2eAgMnPc3d1
povstalcům	povstalec	k1gMnPc3
během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
v	v	k7c6
letech	léto	k1gNnPc6
1947	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
vyloučena	vyloučen	k2eAgFnSc1d1
Jugoslávie	Jugoslávie	k1gFnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
Tito	tento	k3xDgMnPc1
trval	trvat	k5eAaImAgInS
na	na	k7c6
nezávislém	závislý	k2eNgInSc6d1
programu	program	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noviny	novina	k1gFnSc2
Kominformy	kominforma	k1gFnSc2
"	"	kIx"
<g/>
Pro	pro	k7c4
trvalý	trvalý	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
lidovou	lidový	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
podporovaly	podporovat	k5eAaImAgFnP
Stalinovy	Stalinův	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncentrace	koncentrace	k1gFnSc1
Kominformy	kominforma	k1gFnSc2
na	na	k7c4
Evropu	Evropa	k1gFnSc4
v	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
zahraniční	zahraniční	k2eAgFnSc3d1
politice	politika	k1gFnSc3
znamenala	znamenat	k5eAaImAgFnS
důraz	důraz	k1gInSc4
na	na	k7c4
světovou	světový	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vyjadřovala	vyjadřovat	k5eAaImAgFnS
jednotnou	jednotný	k2eAgFnSc4d1
ideologii	ideologie	k1gFnSc4
<g/>
,	,	kIx,
umožňovala	umožňovat	k5eAaImAgFnS
účastníkům	účastník	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
soustřeďovali	soustřeďovat	k5eAaImAgMnP
spíše	spíše	k9
na	na	k7c4
osobnosti	osobnost	k1gFnPc4
než	než	k8xS
na	na	k7c4
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
kremelský	kremelský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
nejvyššího	vysoký	k2eAgInSc2d3
sovětu	sovět	k1gInSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
1982	#num#	k4
</s>
<s>
V	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
byly	být	k5eAaImAgInP
tři	tři	k4xCgInPc1
zdroje	zdroj	k1gInPc1
moci	moc	k1gFnSc2
<g/>
:	:	kIx,
Zákonodárnou	zákonodárný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
reprezentoval	reprezentovat	k5eAaImAgInS
Nejvyšší	vysoký	k2eAgInSc1d3
sovět	sovět	k1gInSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
vládu	vláda	k1gFnSc4
reprezentovala	reprezentovat	k5eAaImAgFnS
Rada	rada	k1gFnSc1
ministrů	ministr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
byla	být	k5eAaImAgFnS
jedinou	jediný	k2eAgFnSc7d1
legální	legální	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
a	a	k8xC
jediným	jediný	k2eAgMnSc7d1
tvůrcem	tvůrce	k1gMnSc7
politiky	politika	k1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
způsobu	způsob	k1gInSc3
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
do	do	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
Stalinova	Stalinův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
<g/>
)	)	kIx)
o	o	k7c4
totalitní	totalitní	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
o	o	k7c6
tzv.	tzv.	kA
kvazitotalitní	kvazitotalitní	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc1
a	a	k8xC
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
na	na	k7c6
Rudém	rudý	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Perestrojka	perestrojka	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Perestrojka	perestrojka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Gorbačov	Gorbačov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
v	v	k7c6
čele	čelo	k1gNnSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgMnS
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
zkostnatělého	zkostnatělý	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
označovaly	označovat	k5eAaImAgInP
hesly	heslo	k1gNnPc7
perestrojka	perestrojka	k1gFnSc1
(	(	kIx(
<g/>
přestavba	přestavba	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
glasnosť	glasnosť	k1gFnSc4
(	(	kIx(
<g/>
otevřenost	otevřenost	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Svazová	svazový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
Republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
byl	být	k5eAaImAgInS
federací	federace	k1gFnSc7
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
(	(	kIx(
<g/>
SSR	SSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
krátce	krátce	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
byly	být	k5eAaImAgInP
autonomní	autonomní	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
ovládány	ovládán	k2eAgInPc1d1
centrální	centrální	k2eAgFnSc7d1
komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
(	(	kIx(
<g/>
KSSS	KSSS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Republiky	republika	k1gFnSc2
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
dělily	dělit	k5eAaImAgFnP
na	na	k7c6
gubernie	gubernie	k1gFnSc1
(	(	kIx(
<g/>
správní	správní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
přejatá	přejatý	k2eAgFnSc1d1
z	z	k7c2
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgFnP
především	především	k9
oblastmi	oblast	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
RSFSR	RSFSR	kA
tvořily	tvořit	k5eAaImAgInP
i	i	k9
kraje	kraj	k1gInPc1
<g/>
,	,	kIx,
národnostní	národnostní	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
autonomní	autonomní	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
autonomní	autonomní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc4d2
republiky	republika	k1gFnPc4
–	–	k?
tedy	tedy	k8xC
pobaltské	pobaltský	k2eAgFnPc1d1
<g/>
,	,	kIx,
kavkazské	kavkazský	k2eAgFnPc1d1
a	a	k8xC
Moldavská	moldavský	k2eAgFnSc1d1
SSR	SSR	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
naopak	naopak	k6eAd1
na	na	k7c4
oblasti	oblast	k1gFnPc4
ani	ani	k8xC
kraje	kraj	k1gInPc1
nedělily	dělit	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
některých	některý	k3yIgFnPc2
svazových	svazový	k2eAgFnPc2d1
republik	republika	k1gFnPc2
(	(	kIx(
<g/>
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
Gruzínské	gruzínský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Uzbecké	uzbecký	k2eAgFnSc2d1
a	a	k8xC
Tádžické	tádžický	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
autonomní	autonomní	k2eAgFnPc1d1
sovětské	sovětský	k2eAgFnPc1d1
socialistické	socialistický	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
(	(	kIx(
<g/>
ASSR	ASSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlidnatější	lidnatý	k2eAgInSc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
byly	být	k5eAaImAgInP
Baškirská	baškirský	k2eAgFnSc1d1
a	a	k8xC
Tatarská	tatarský	k2eAgFnSc1d1
ASSR	ASSR	kA
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
Povolží	Povolží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc7d1
správní	správní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
nižšího	nízký	k2eAgInSc2d2
řádu	řád	k1gInSc2
byl	být	k5eAaImAgInS
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
rajón	rajón	k1gInSc1
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
odpovídající	odpovídající	k2eAgInSc4d1
okresu	okres	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
nerovnoměrnému	rovnoměrný	k2eNgNnSc3d1
osídlení	osídlení	k1gNnSc3
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
značně	značně	k6eAd1
lišily	lišit	k5eAaImAgFnP
svou	svůj	k3xOyFgFnSc7
velikostí	velikost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rajóny	rajón	k1gInPc4
se	se	k3xPyFc4
dělila	dělit	k5eAaImAgFnS
také	také	k9
větší	veliký	k2eAgNnPc4d2
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
části	část	k1gFnSc6
nástupnických	nástupnický	k2eAgInPc2d1
států	stát	k1gInPc2
je	být	k5eAaImIp3nS
základní	základní	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
na	na	k7c4
oblasti	oblast	k1gFnPc4
a	a	k8xC
rajóny	rajón	k1gInPc4
používáno	používán	k2eAgNnSc1d1
i	i	k9
v	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1961	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
18	#num#	k4
velkých	velký	k2eAgInPc2d1
ekonomických	ekonomický	k2eAgInPc2d1
rajónů	rajón	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
pokrývaly	pokrývat	k5eAaImAgFnP
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
#	#	kIx~
</s>
<s>
Republika	republika	k1gFnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
svazu	svaz	k1gInSc2
republik	republika	k1gFnPc2
1956	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
SFSR	SFSR	kA
</s>
<s>
2	#num#	k4
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
3	#num#	k4
</s>
<s>
Běloruská	běloruský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
4	#num#	k4
</s>
<s>
Uzbecká	uzbecký	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
5	#num#	k4
</s>
<s>
Kazašská	kazašský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
6	#num#	k4
</s>
<s>
Gruzínská	gruzínský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
7	#num#	k4
</s>
<s>
Ázerbájdžánská	ázerbájdžánský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
8	#num#	k4
</s>
<s>
Litevská	litevský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
9	#num#	k4
</s>
<s>
Moldavská	moldavský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
10	#num#	k4
</s>
<s>
Lotyšská	lotyšský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
11	#num#	k4
</s>
<s>
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
12	#num#	k4
</s>
<s>
Tádžická	tádžický	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
13	#num#	k4
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
14	#num#	k4
</s>
<s>
Turkmenská	turkmenský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
15	#num#	k4
</s>
<s>
Estonská	estonský	k2eAgFnSc1d1
SSR	SSR	kA
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Ozbrojené	ozbrojený	k2eAgFnSc2d1
síly	síla	k1gFnSc2
SSSR	SSSR	kA
<g/>
,	,	kIx,
rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
sovětská	sovětský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
sovětské	sovětský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
a	a	k8xC
sovětské	sovětský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Balistická	balistický	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
SS-20	SS-20	k1gMnPc2
středního	střední	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
ve	v	k7c6
velmi	velmi	k6eAd1
krátkém	krátký	k2eAgInSc6d1
čase	čas	k1gInSc6
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
zničit	zničit	k5eAaPmF
prakticky	prakticky	k6eAd1
jakýkoli	jakýkoli	k3yIgInSc4
vojenský	vojenský	k2eAgInSc4d1
cíl	cíl	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasazení	nasazení	k1gNnPc1
těchto	tento	k3xDgInPc2
mobilních	mobilní	k2eAgInPc2d1
systémů	systém	k1gInPc2
na	na	k7c6
konci	konec	k1gInSc6
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
zahájilo	zahájit	k5eAaPmAgNnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
nový	nový	k2eAgInSc4d1
zbrojní	zbrojní	k2eAgInSc4d1
závod	závod	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
NATO	nato	k6eAd1
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nasadilo	nasadit	k5eAaPmAgNnS
rakety	raketa	k1gFnSc2
Pershing	Pershing	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
vojenského	vojenský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
ze	z	k7c2
září	září	k1gNnSc2
1925	#num#	k4
se	se	k3xPyFc4
sovětské	sovětský	k2eAgFnPc1d1
ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
skládaly	skládat	k5eAaImAgFnP
ze	z	k7c2
tří	tři	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
pozemních	pozemní	k2eAgFnPc2d1
síl	síl	k1gInSc1
<g/>
,	,	kIx,
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
sjednocené	sjednocený	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
OGPU	OGPU	kA
<g/>
)	)	kIx)
a	a	k8xC
vnitřních	vnitřní	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
OGPU	OGPU	kA
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
nezávislou	závislý	k2eNgFnSc7d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
se	se	k3xPyFc4
součila	součit	k5eAaImAgFnS,k5eAaPmAgFnS
s	s	k7c7
NKVD	NKVD	kA
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
její	její	k3xOp3gNnPc4
vnitřní	vnitřní	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
pod	pod	k7c7
společným	společný	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
obranných	obranný	k2eAgInPc2d1
a	a	k8xC
vnitřních	vnitřní	k2eAgInPc2d1
komisariátů	komisariát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgNnP
strategická	strategický	k2eAgNnPc1d1
raketová	raketový	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vojska	vojsko	k1gNnSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jednotky	jednotka	k1gFnPc1
všenárodní	všenárodní	k2eAgFnSc2d1
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
stály	stát	k5eAaImAgInP
na	na	k7c6
prvním	první	k4xOgInSc6
<g/>
,	,	kIx,
třetím	třetí	k4xOgMnSc6
a	a	k8xC
šestém	šestý	k4xOgInSc6
místě	místo	k1gNnSc6
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
sovětském	sovětský	k2eAgInSc6d1
systému	systém	k1gInSc6
důležitosti	důležitost	k1gFnSc2
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
byly	být	k5eAaImAgFnP
druhé	druhý	k4xOgNnSc4
<g/>
,	,	kIx,
letectvo	letectvo	k1gNnSc1
čtvrté	čtvrtá	k1gFnSc2
a	a	k8xC
námořnictvo	námořnictvo	k1gNnSc1
páté	pátá	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Armáda	armáda	k1gFnSc1
měla	mít	k5eAaImAgFnS
největší	veliký	k2eAgInSc4d3
politický	politický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
čítala	čítat	k5eAaImAgFnS
dva	dva	k4xCgInPc4
milióny	milión	k4xCgInPc4
vojáků	voják	k1gMnPc2
rozdělených	rozdělený	k2eAgMnPc2d1
mezi	mezi	k7c4
150	#num#	k4
motorizovaných	motorizovaný	k2eAgMnPc2d1
a	a	k8xC
52	#num#	k4
obrněných	obrněný	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
počátku	počátek	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
spíše	spíše	k9
méně	málo	k6eAd2
početnou	početný	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
větví	větev	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
karibské	karibský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
se	se	k3xPyFc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Sergeje	Sergej	k1gMnSc2
Gorškova	Gorškův	k2eAgInSc2d1
dočkalo	dočkat	k5eAaPmAgNnS
výrazného	výrazný	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známá	k1gFnSc6
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
díky	díky	k7c3
raketovým	raketový	k2eAgInPc3d1
křižníkům	křižník	k1gInPc3
a	a	k8xC
ponorkám	ponorka	k1gFnPc3
a	a	k8xC
sloužilo	sloužit	k5eAaImAgNnS
v	v	k7c6
něm	on	k3xPp3gInSc6
500	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
se	se	k3xPyFc4
soustřeďovalo	soustřeďovat	k5eAaImAgNnS
na	na	k7c4
flotilu	flotila	k1gFnSc4
strategických	strategický	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
ve	v	k7c6
válečné	válečný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
mělo	mít	k5eAaImAgNnS
vymýtit	vymýtit	k5eAaPmF
nepřátelskou	přátelský	k2eNgFnSc4d1
infrastrukturu	infrastruktura	k1gFnSc4
a	a	k8xC
jadernou	jaderný	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdušná	vzdušný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
měla	mít	k5eAaImAgFnS
také	také	k9
množství	množství	k1gNnSc4
stíhaček	stíhačka	k1gFnPc2
a	a	k8xC
taktických	taktický	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
armádu	armáda	k1gFnSc4
ve	v	k7c6
válce	válka	k1gFnSc6
podporovaly	podporovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategická	strategický	k2eAgNnPc4d1
raketová	raketový	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
disponovala	disponovat	k5eAaBmAgFnS
více	hodně	k6eAd2
než	než	k8xS
1400	#num#	k4
mezikontinentálními	mezikontinentální	k2eAgFnPc7d1
balistickými	balistický	k2eAgFnPc7d1
raketami	raketa	k1gFnPc7
(	(	kIx(
<g/>
ICBM	ICBM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozmístěnými	rozmístěný	k2eAgMnPc7d1
mezi	mezi	k7c7
28	#num#	k4
základnami	základna	k1gFnPc7
a	a	k8xC
300	#num#	k4
velitelskými	velitelský	k2eAgNnPc7d1
středisky	středisko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poválečné	poválečný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
Sovětská	sovětský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
přímo	přímo	k6eAd1
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
několika	několik	k4yIc6
vojenských	vojenský	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ně	on	k3xPp3gNnSc4
patřilo	patřit	k5eAaImAgNnS
potlačení	potlačení	k1gNnSc1
povstání	povstání	k1gNnSc2
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vpád	vpád	k1gInSc4
do	do	k7c2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
také	také	k9
účastnil	účastnit	k5eAaImAgInS
války	válka	k1gFnSc2
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
mezi	mezi	k7c7
lety	let	k1gInPc7
1979	#num#	k4
až	až	k9
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
platila	platit	k5eAaImAgFnS
všeobecná	všeobecný	k2eAgFnSc1d1
branná	branný	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Právní	právní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Soudnictví	soudnictví	k1gNnSc1
nebylo	být	k5eNaImAgNnS
nezávislé	závislý	k2eNgNnSc1d1
na	na	k7c6
ostatních	ostatní	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
dohlížel	dohlížet	k5eAaImAgInS
na	na	k7c4
nižší	nízký	k2eAgInSc4d2
soudy	soud	k1gInPc4
(	(	kIx(
<g/>
lidový	lidový	k2eAgInSc4d1
soud	soud	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
používal	používat	k5eAaImAgMnS
zákon	zákon	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
ústavou	ústava	k1gFnSc7
nebo	nebo	k8xC
vykládán	vykládat	k5eAaImNgInS
nejvyšším	vysoký	k2eAgInSc7d3
sovětem	sovět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbor	výbor	k1gInSc1
pro	pro	k7c4
ústavní	ústavní	k2eAgInSc4d1
dohled	dohled	k1gInSc4
přezkoumával	přezkoumávat	k5eAaImAgInS
ústavnost	ústavnost	k1gFnSc4
zákonů	zákon	k1gInPc2
a	a	k8xC
aktů	akt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
používal	používat	k5eAaImAgInS
inkviziční	inkviziční	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
soudce	soudce	k1gMnSc1
<g/>
,	,	kIx,
státní	státní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
a	a	k8xC
obhájce	obhájce	k1gMnSc1
spolupracují	spolupracovat	k5eAaImIp3nP
při	při	k7c6
určování	určování	k1gNnSc6
pravdy	pravda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyššími	vysoký	k2eAgMnPc7d3
představiteli	představitel	k1gMnPc7
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
předsedy	předseda	k1gMnSc2
prezídia	prezídium	k1gNnSc2
Nejvyššího	vysoký	k2eAgInSc2d3
sovětu	sovět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
kolektivní	kolektivní	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
<g/>
)	)	kIx)
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
byli	být	k5eAaImAgMnP
vždy	vždy	k6eAd1
zároveň	zároveň	k6eAd1
vůdcové	vůdce	k1gMnPc1
vládnoucí	vládnoucí	k2eAgFnSc2d1
strany	strana	k1gFnSc2
–	–	k?
zpravidla	zpravidla	k6eAd1
první	první	k4xOgMnSc1
nebo	nebo	k8xC
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1
obodobí	obodobit	k5eAaPmIp3nS
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Lenin	Lenin	k1gMnSc1
*	*	kIx~
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1903	#num#	k4
až	až	k9
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1924	#num#	k4
</s>
<s>
Předseda	předseda	k1gMnSc1
Rady	rada	k1gFnSc2
lidových	lidový	k2eAgMnPc2d1
komisařů	komisař	k1gMnPc2
</s>
<s>
Josif	Josif	k1gMnSc1
Vissarionovič	Vissarionovič	k1gMnSc1
Stalin	Stalin	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1922	#num#	k4
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1953	#num#	k4
</s>
<s>
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
od	od	k7c2
1922	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
1952	#num#	k4
první	první	k4xOgMnSc1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Nikita	Nikita	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
Chruščov	Chruščov	k1gInSc4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1953	#num#	k4
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
</s>
<s>
první	první	k4xOgMnSc1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Leonid	Leonid	k1gInSc1
Iljič	Iljič	k1gMnSc1
Brežněv	Brežněv	k1gMnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1982	#num#	k4
</s>
<s>
od	od	k7c2
1966	#num#	k4
generální	generální	k2eAgFnSc2d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Vladimirovič	Vladimirovič	k1gInSc4
Andropov	Andropov	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1982	#num#	k4
až	až	k9
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1984	#num#	k4
</s>
<s>
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Ustinovič	Ustinovič	k1gMnSc1
Černěnko	Černěnka	k1gFnSc5
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1984	#num#	k4
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1985	#num#	k4
</s>
<s>
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Michail	Michail	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1985	#num#	k4
až	až	k9
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1991	#num#	k4
</s>
<s>
od	od	k7c2
1990	#num#	k4
prezident	prezident	k1gMnSc1
</s>
<s>
*	*	kIx~
<g/>
)	)	kIx)
Lenin	Lenin	k1gMnSc1
byl	být	k5eAaImAgMnS
jen	jen	k9
neformálně	formálně	k6eNd1
vedoucím	vedoucí	k1gMnSc7
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
až	až	k9
1953	#num#	k4
a	a	k8xC
1966	#num#	k4
až	až	k9
1991	#num#	k4
byl	být	k5eAaImAgInS
název	název	k1gInSc1
"	"	kIx"
<g/>
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1953	#num#	k4
-	-	kIx~
1966	#num#	k4
"	"	kIx"
<g/>
první	první	k4xOgMnSc1
tajemník	tajemník	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
kosmický	kosmický	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Sovětský	sovětský	k2eAgInSc1d1
kosmický	kosmický	k2eAgInSc1d1
program	program	k1gInSc1
a	a	k8xC
Nedělinova	Nedělinův	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Popovič	Popovič	k1gMnSc1
<g/>
,	,	kIx,
Valentina	Valentina	k1gFnSc1
Těreškovová	Těreškovová	k1gFnSc1
a	a	k8xC
Nikita	Nikita	k1gFnSc1
Chruščov	Chruščov	k1gInSc1
na	na	k7c6
Leninově	Leninův	k2eAgInSc6d1
mauzoleuu	mauzoleuus	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
</s>
<s>
Raketa	raketa	k1gFnSc1
Sojuz	Sojuz	k1gInSc1
startující	startující	k2eAgInSc1d1
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
SSSR	SSSR	kA
s	s	k7c7
pomocí	pomoc	k1gFnSc7
inženýrů	inženýr	k1gMnPc2
a	a	k8xC
technologií	technologie	k1gFnPc2
dovezených	dovezený	k2eAgFnPc2d1
z	z	k7c2
poraženého	poražený	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
zkonstruovali	zkonstruovat	k5eAaPmAgMnP
Sověti	Sovět	k1gMnPc1
první	první	k4xOgFnSc4
umělou	umělý	k2eAgFnSc4d1
družici	družice	k1gFnSc4
–	–	k?
Sputnik	sputnik	k1gInSc1
1	#num#	k4
a	a	k8xC
předběhli	předběhnout	k5eAaPmAgMnP
tak	tak	k6eAd1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
následovaly	následovat	k5eAaImAgFnP
další	další	k2eAgFnPc4d1
úspěšné	úspěšný	k2eAgFnPc4d1
družice	družice	k1gFnPc4
a	a	k8xC
byli	být	k5eAaImAgMnP
vysláni	vyslat	k5eAaPmNgMnP
i	i	k8xC
pokusní	pokusný	k2eAgMnPc1d1
psi	pes	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
byl	být	k5eAaImAgMnS
vyslán	vyslán	k2eAgMnSc1d1
první	první	k4xOgMnSc1
kosmonaut	kosmonaut	k1gMnSc1
Jurij	Jurij	k1gFnSc2
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
jednou	jednou	k6eAd1
obletěl	obletět	k5eAaPmAgMnS
zemi	zem	k1gFnSc4
a	a	k8xC
úspěšně	úspěšně	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
v	v	k7c6
kazašské	kazašský	k2eAgFnSc6d1
stepi	step	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
sovětských	sovětský	k2eAgFnPc6d1
projekčních	projekční	k2eAgFnPc6d1
kancelářích	kancelář	k1gFnPc6
rýsovaly	rýsovat	k5eAaImAgInP
první	první	k4xOgInPc1
plány	plán	k1gInPc1
raketoplánů	raketoplán	k1gInPc2
a	a	k8xC
orbitálních	orbitální	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
tomu	ten	k3xDgNnSc3
však	však	k9
nakonec	nakonec	k6eAd1
zamezily	zamezit	k5eAaPmAgInP
osobní	osobní	k2eAgInPc1d1
spory	spor	k1gInPc1
mezi	mezi	k7c4
projektanty	projektant	k1gMnPc4
a	a	k8xC
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
velkým	velký	k2eAgNnSc7d1
fiaskem	fiasko	k1gNnSc7
pro	pro	k7c4
SSSR	SSSR	kA
bylo	být	k5eAaImAgNnS
přistání	přistání	k1gNnSc1
Američanů	Američan	k1gMnPc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Rusové	Rus	k1gMnPc1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
v	v	k7c6
dostatečné	dostatečný	k2eAgFnSc6d1
době	doba	k1gFnSc6
odpovědět	odpovědět	k5eAaPmF
Američanům	Američan	k1gMnPc3
stejným	stejný	k2eAgInSc7d1
projektem	projekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
rýsovat	rýsovat	k5eAaImF
konkrétnější	konkrétní	k2eAgInPc4d2
návrhy	návrh	k1gInPc4
konstrukce	konstrukce	k1gFnSc2
raketoplánu	raketoplán	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
nedostatky	nedostatek	k1gInPc4
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
elektronickém	elektronický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
rychlé	rychlý	k2eAgNnSc1d1
přehřívání	přehřívání	k1gNnSc1
elektroniky	elektronika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odsunuly	odsunout	k5eAaPmAgFnP
program	program	k1gInSc4
až	až	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
raketoplán	raketoplán	k1gInSc4
<g/>
,	,	kIx,
Buran	buran	k1gInSc4
<g/>
,	,	kIx,
letěl	letět	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
lidské	lidský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
raketoplán	raketoplán	k1gInSc1
<g/>
,	,	kIx,
Ptička	Ptička	k1gFnSc1
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgMnS
nakonec	nakonec	k6eAd1
rozestavěný	rozestavěný	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byl	být	k5eAaImAgInS
projekt	projekt	k1gInSc1
raketoplánů	raketoplán	k1gInPc2
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
vypuštění	vypuštění	k1gNnSc4
do	do	k7c2
vesmíru	vesmír	k1gInSc2
existuje	existovat	k5eAaImIp3nS
dnes	dnes	k6eAd1
nevyužívaná	využívaný	k2eNgFnSc1d1
supersilná	supersilný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
Eněrgija	Eněrgija	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
nejsilnější	silný	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
Sovětskému	sovětský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
podařilo	podařit	k5eAaPmAgNnS
vybudovat	vybudovat	k5eAaPmF
orbitální	orbitální	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Mir	Mira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
na	na	k7c6
konstrukci	konstrukce	k1gFnSc6
stanic	stanice	k1gFnPc2
Saljut	Saljut	k1gInSc1
a	a	k8xC
její	její	k3xOp3gInPc1
úkoly	úkol	k1gInPc1
byly	být	k5eAaImAgInP
čistě	čistě	k6eAd1
civilní	civilní	k2eAgInPc4d1
a	a	k8xC
výzkumné	výzkumný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
amerického	americký	k2eAgInSc2d1
Skylabu	Skylab	k1gInSc2
ukončen	ukončit	k5eAaPmNgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jedinou	jediný	k2eAgFnSc7d1
orbitální	orbitální	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
byly	být	k5eAaImAgInP
přidávány	přidáván	k2eAgInPc1d1
další	další	k2eAgInPc1d1
moduly	modul	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
americké	americký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technický	technický	k2eAgInSc1d1
stav	stav	k1gInSc1
stanice	stanice	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
hlavně	hlavně	k9
po	po	k7c6
požáru	požár	k1gInSc6
<g/>
,	,	kIx,
rychle	rychle	k6eAd1
zhoršoval	zhoršovat	k5eAaImAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
jejím	její	k3xOp3gNnSc6
navedení	navedení	k1gNnSc6
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
shořela	shořet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomika	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
DněproGES	DněproGES	k?
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mnoha	mnoho	k4c2
vodních	vodní	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
v	v	k7c6
SSSR	SSSR	kA
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
podle	podle	k7c2
(	(	kIx(
<g/>
nominálního	nominální	k2eAgNnSc2d1
<g/>
)	)	kIx)
HDP	HDP	kA
na	na	k7c4
osobu	osoba	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
západoněmecké	západoněmecký	k2eAgFnSc2d1
školní	školní	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
>	>	kIx)
5,000	5,000	k4
DM	dm	kA
</s>
<s>
2,500	2,500	k4
–	–	k?
5,000	5,000	k4
DM	dm	kA
</s>
<s>
1,000	1,000	k4
–	–	k?
2,500	2,500	k4
DM	dm	kA
</s>
<s>
500	#num#	k4
–	–	k?
1,000	1,000	k4
DM	dm	kA
</s>
<s>
250	#num#	k4
–	–	k?
500	#num#	k4
DM	dm	kA
</s>
<s>
<	<	kIx(
250	#num#	k4
DM	dm	kA
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
přijala	přijmout	k5eAaPmAgFnS
plánovanou	plánovaný	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
výrobu	výroba	k1gFnSc4
a	a	k8xC
distribuci	distribuce	k1gFnSc4
zboží	zboží	k1gNnSc2
centralizovala	centralizovat	k5eAaBmAgFnS
a	a	k8xC
řídila	řídit	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
bolševická	bolševický	k2eAgFnSc1d1
zkušenost	zkušenost	k1gFnSc1
s	s	k7c7
velitelskou	velitelský	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
byla	být	k5eAaImAgFnS
politika	politika	k1gFnSc1
válečného	válečný	k2eAgInSc2d1
komunismu	komunismus	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
znárodnění	znárodnění	k1gNnSc3
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
centralizovanou	centralizovaný	k2eAgFnSc4d1
distribuci	distribuce	k1gFnSc4
outputu	output	k1gInSc2
(	(	kIx(
<g/>
výsledek	výsledek	k1gInSc1
výrobního	výrobní	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
donucovací	donucovací	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
na	na	k7c4
zemědělskou	zemědělský	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
a	a	k8xC
pokusy	pokus	k1gInPc4
odstranit	odstranit	k5eAaPmF
peněžní	peněžní	k2eAgInSc4d1
oběh	oběh	k1gInSc4
<g/>
,	,	kIx,
soukromé	soukromý	k2eAgInPc4d1
podniky	podnik	k1gInPc4
a	a	k8xC
volný	volný	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vážném	vážný	k2eAgInSc6d1
ekonomickém	ekonomický	k2eAgInSc6d1
kolapsu	kolaps	k1gInSc6
nahradil	nahradit	k5eAaPmAgMnS
Lenin	Lenin	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
válečný	válečný	k2eAgInSc1d1
komunismus	komunismus	k1gInSc1
novou	nový	k2eAgFnSc7d1
ekonomickou	ekonomický	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
(	(	kIx(
<g/>
NEP	Nep	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
legalizoval	legalizovat	k5eAaBmAgInS
volný	volný	k2eAgInSc4d1
trh	trh	k1gInSc4
a	a	k8xC
soukromé	soukromý	k2eAgNnSc4d1
vlastnictví	vlastnictví	k1gNnSc4
malých	malý	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomika	ekonomika	k1gFnSc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
zotavila	zotavit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
debatě	debata	k1gFnSc6
mezi	mezi	k7c7
členy	člen	k1gInPc7
politbyra	politbyro	k1gNnSc2
o	o	k7c6
průběhu	průběh	k1gInSc6
hospodářského	hospodářský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1928	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
zemí	zem	k1gFnSc7
Josef	Josef	k1gMnSc1
Stalin	Stalin	k1gMnSc1
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgMnS
NEP	Nep	k1gInSc4
a	a	k8xC
prosadil	prosadit	k5eAaPmAgMnS
plné	plný	k2eAgNnSc4d1
centrální	centrální	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
násilnou	násilný	k2eAgFnSc4d1
kolektivizaci	kolektivizace	k1gFnSc4
zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
přijal	přijmout	k5eAaPmAgInS
drakonickou	drakonický	k2eAgFnSc4d1
pracovní	pracovní	k2eAgFnSc4d1
legislativu	legislativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroje	zdroj	k1gInSc2
byly	být	k5eAaImAgFnP
zmobilizovány	zmobilizovat	k5eAaPmNgInP
pro	pro	k7c4
rychlou	rychlý	k2eAgFnSc4d1
industrializaci	industrializace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
během	během	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
značně	značně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
sovětskou	sovětský	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
těžkého	těžký	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
investičního	investiční	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
motivací	motivace	k1gFnSc7
pro	pro	k7c4
industrializaci	industrializace	k1gFnSc4
byla	být	k5eAaImAgFnS
příprava	příprava	k1gFnSc1
na	na	k7c4
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
kvůli	kvůli	k7c3
nedůvěře	nedůvěra	k1gFnSc3
k	k	k7c3
"	"	kIx"
<g/>
vnejšímu	vnejší	k2eAgInSc3d1,k2eAgInSc3d2
<g/>
"	"	kIx"
kapitalistickému	kapitalistický	k2eAgInSc3d1
světu	svět	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
SSSR	SSSR	kA
přeměnil	přeměnit	k5eAaPmAgInS
z	z	k7c2
převážně	převážně	k6eAd1
agrární	agrární	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c4
velký	velký	k2eAgInSc4d1
průmyslový	průmyslový	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
razil	razit	k5eAaImAgMnS
cestu	cesta	k1gFnSc4
k	k	k7c3
vzestupu	vzestup	k1gInSc3
jako	jako	k8xC,k8xS
velmoc	velmoc	k1gFnSc1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Válka	válka	k1gFnSc1
sovětskou	sovětský	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
infrastrukturu	infrastruktura	k1gFnSc4
velmi	velmi	k6eAd1
zdevastovala	zdevastovat	k5eAaPmAgFnS
a	a	k8xC
vyžadovala	vyžadovat	k5eAaImAgFnS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
čtyřicátých	čtyřicátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
sovětská	sovětský	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
relativně	relativně	k6eAd1
soběstačnou	soběstačný	k2eAgFnSc7d1
<g/>
;	;	kIx,
po	po	k7c4
většinu	většina	k1gFnSc4
období	období	k1gNnSc2
až	až	k9
do	do	k7c2
vzniku	vznik	k1gInSc2
RVHP	RVHP	kA
se	se	k3xPyFc4
na	na	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
trhu	trh	k1gInSc6
obchodovalo	obchodovat	k5eAaImAgNnS
s	s	k7c7
pouze	pouze	k6eAd1
velmi	velmi	k6eAd1
malým	malý	k2eAgInSc7d1
podílem	podíl	k1gInSc7
sovětských	sovětský	k2eAgInPc2d1
domácích	domácí	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vzniku	vznik	k1gInSc6
Východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
zahraniční	zahraniční	k2eAgInSc1d1
obchod	obchod	k1gInSc1
rychle	rychle	k6eAd1
rostl	růst	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
byl	být	k5eAaImAgInS
vliv	vliv	k1gInSc4
světové	světový	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c6
SSSR	SSSR	kA
omezen	omezit	k5eAaPmNgInS
pevnými	pevný	k2eAgFnPc7d1
domácími	domácí	k2eAgFnPc7d1
cenami	cena	k1gFnPc7
a	a	k8xC
státním	státní	k2eAgInSc7d1
monopolem	monopol	k1gInSc7
na	na	k7c4
zahraniční	zahraniční	k2eAgInSc4d1
obchod	obchod	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Okolo	okolo	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
hlavními	hlavní	k2eAgInPc7d1
objekty	objekt	k1gInPc7
dovozu	dovoz	k1gInSc2
obilí	obilí	k1gNnSc2
a	a	k8xC
sofistikované	sofistikovaný	k2eAgInPc4d1
spotřební	spotřební	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
závodů	závod	k1gInPc2
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
ve	v	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
sovětská	sovětský	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
zatěžována	zatěžovat	k5eAaImNgFnS
vojenskými	vojenský	k2eAgInPc7d1
výdaji	výdaj	k1gInPc7
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
byly	být	k5eAaImAgFnP
silně	silně	k6eAd1
lobovány	lobovat	k5eAaBmNgFnP
mocnou	mocný	k2eAgFnSc7d1
byrokracií	byrokracie	k1gFnSc7
závislou	závislý	k2eAgFnSc7d1
na	na	k7c6
zbrojním	zbrojní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	se	k3xPyFc4
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
stal	stát	k5eAaPmAgMnS
největším	veliký	k2eAgMnSc7d3
vývozcem	vývozce	k1gMnSc7
zbraní	zbraň	k1gFnPc2
do	do	k7c2
zemí	zem	k1gFnPc2
třetího	třetí	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
až	až	k9
do	do	k7c2
rozpadu	rozpad	k1gInSc2
koncem	koncem	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
zůstalo	zůstat	k5eAaPmAgNnS
fungování	fungování	k1gNnSc1
ekonomiky	ekonomika	k1gFnSc2
v	v	k7c6
SSSR	SSSR	kA
v	v	k7c6
podstatě	podstata	k1gFnSc6
stejné	stejný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomika	ekonomika	k1gFnSc1
byla	být	k5eAaImAgFnS
formálně	formálně	k6eAd1
řízena	řídit	k5eAaImNgFnS
centrálním	centrální	k2eAgNnSc7d1
plánováním	plánování	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
prováděl	provádět	k5eAaImAgInS
Gosplan	Gosplan	k1gInSc1
a	a	k8xC
organizovaná	organizovaný	k2eAgFnSc1d1
v	v	k7c6
pětiletých	pětiletý	k2eAgInPc6d1
plánech	plán	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
však	však	k9
byly	být	k5eAaImAgInP
plány	plán	k1gInPc1
velmi	velmi	k6eAd1
"	"	kIx"
<g/>
agregované	agregovaný	k2eAgFnSc3d1
<g/>
"	"	kIx"
a	a	k8xC
provizorní	provizorní	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
výhradou	výhrada	k1gFnSc7
intervencí	intervence	k1gFnPc2
ad	ad	k7c4
hoc	hoc	k?
ze	z	k7c2
strany	strana	k1gFnSc2
nadřízených	nadřízený	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
klíčová	klíčový	k2eAgNnPc1d1
hospodářská	hospodářský	k2eAgNnPc1d1
rozhodnutí	rozhodnutí	k1gNnPc1
byla	být	k5eAaImAgNnP
přijata	přijmout	k5eAaPmNgNnP
politickým	politický	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidělené	přidělený	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
a	a	k8xC
cíle	cíl	k1gInPc1
plánu	plán	k1gInSc2
byly	být	k5eAaImAgInP
obvykle	obvykle	k6eAd1
denominovány	denominovat	k5eAaBmNgInP,k5eAaPmNgInP,k5eAaImNgInP
spíše	spíše	k9
v	v	k7c6
rublech	rubl	k1gInPc6
než	než	k8xS
ve	v	k7c6
fyzickém	fyzický	k2eAgNnSc6d1
zboží	zboží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Volžskij	Volžskít	k5eAaPmRp2nS
Avtomobilnyj	Avtomobilnyj	k1gFnSc1
Zavod	Zavod	k1gInSc1
(	(	kIx(
<g/>
VAZ	vaz	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
</s>
<s>
Doprava	doprava	k1gFnSc1
byla	být	k5eAaImAgFnS
klíčovou	klíčový	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
národního	národní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
centralizace	centralizace	k1gFnSc1
konce	konec	k1gInSc2
dvacátých	dvacátý	k4xOgNnPc2
a	a	k8xC
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
masivnímu	masivní	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
k	k	k7c3
založení	založení	k1gNnSc3
aerolinek	aerolinka	k1gFnPc2
Aeroflot	aeroflot	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Země	země	k1gFnSc1
disponovala	disponovat	k5eAaBmAgFnS
širokým	široký	k2eAgNnSc7d1
spektrem	spektrum	k1gNnSc7
dopravy	doprava	k1gFnSc2
po	po	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
vodě	voda	k1gFnSc6
a	a	k8xC
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
kvůli	kvůli	k7c3
špatné	špatný	k2eAgFnSc3d1
údržbě	údržba	k1gFnSc3
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
sovětské	sovětský	k2eAgFnSc2d1
silniční	silniční	k2eAgFnSc2d1
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnSc2d1
a	a	k8xC
civilní	civilní	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
se	s	k7c7
západním	západní	k2eAgInSc7d1
světem	svět	k1gInSc7
zastaralá	zastaralý	k2eAgFnSc1d1
a	a	k8xC
technologicky	technologicky	k6eAd1
zaostalá	zaostalý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
a	a	k8xC
nejintenzivněji	intenzivně	k6eAd3
používaná	používaný	k2eAgNnPc1d1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
a	a	k8xC
začátkem	začátkem	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
sovětští	sovětský	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
volali	volat	k5eAaImAgMnP
po	po	k7c6
výstavbě	výstavba	k1gFnSc6
více	hodně	k6eAd2
silnic	silnice	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zmírnili	zmírnit	k5eAaPmAgMnP
zatížení	zatížení	k1gNnSc4
železnic	železnice	k1gFnPc2
a	a	k8xC
zlepšili	zlepšit	k5eAaPmAgMnP
sovětský	sovětský	k2eAgInSc4d1
vládní	vládní	k2eAgInSc4d1
rozpočet	rozpočet	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Silniční	silniční	k2eAgFnSc4d1
síť	síť	k1gFnSc4
a	a	k8xC
automobilový	automobilový	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
zůstaly	zůstat	k5eAaPmAgFnP
nerozvinuté	rozvinutý	k2eNgFnPc1d1
a	a	k8xC
mimo	mimo	k6eAd1
velká	velký	k2eAgNnPc1d1
města	město	k1gNnPc1
byly	být	k5eAaImAgFnP
běžné	běžný	k2eAgFnPc4d1
polní	polní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Sovětská	sovětský	k2eAgFnSc1d1
údržba	údržba	k1gFnSc1
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
neschopná	schopný	k2eNgFnSc1d1
postarat	postarat	k5eAaPmF
se	se	k3xPyFc4
i	i	k9
o	o	k7c4
ty	ten	k3xDgFnPc4
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
země	země	k1gFnSc1
již	již	k6eAd1
měla	mít	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
do	do	k7c2
poloviny	polovina	k1gFnSc2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
sovětské	sovětský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
snažily	snažit	k5eAaImAgInP
vyřešit	vyřešit	k5eAaPmF
problém	problém	k1gInSc4
silnic	silnice	k1gFnPc2
objednáním	objednání	k1gNnSc7
výstavby	výstavba	k1gFnSc2
nových	nový	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Automobilový	automobilový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
mezitím	mezitím	k6eAd1
rostl	růst	k5eAaImAgInS
rychleji	rychle	k6eAd2
než	než	k8xS
výstavba	výstavba	k1gFnSc1
silnic	silnice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Nerozvinutá	rozvinutý	k2eNgFnSc1d1
silniční	silniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
rostoucí	rostoucí	k2eAgFnSc3d1
poptávce	poptávka	k1gFnSc3
po	po	k7c6
veřejné	veřejný	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Navzdory	navzdory	k7c3
zlepšení	zlepšení	k1gNnSc3
byly	být	k5eAaImAgInP
některé	některý	k3yIgInPc1
aspekty	aspekt	k1gInPc1
odvětví	odvětví	k1gNnSc4
dopravy	doprava	k1gFnSc2
zmítány	zmítán	k2eAgInPc1d1
problémy	problém	k1gInPc1
kvůli	kvůli	k7c3
zastaralé	zastaralý	k2eAgFnSc3d1
infrastruktuře	infrastruktura	k1gFnSc3
<g/>
,	,	kIx,
nedostatku	nedostatek	k1gInSc3
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
korupci	korupce	k1gFnSc3
a	a	k8xC
špatnému	špatný	k2eAgNnSc3d1
rozhodování	rozhodování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgInPc1d1
orgány	orgán	k1gInPc1
nebyly	být	k5eNaImAgInP
schopné	schopný	k2eAgInPc1d1
uspokojit	uspokojit	k5eAaPmF
rostoucí	rostoucí	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
po	po	k7c6
dopravní	dopravní	k2eAgFnSc6d1
infrastruktuře	infrastruktura	k1gFnSc6
a	a	k8xC
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
obchodní	obchodní	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Nadměrná	nadměrný	k2eAgNnPc1d1
úmrtí	úmrtí	k1gNnPc1
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
ruské	ruský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
poválečného	poválečný	k2eAgInSc2d1
hladomoru	hladomor	k1gInSc2
<g/>
)	)	kIx)
činila	činit	k5eAaImAgFnS
celkem	celkem	k6eAd1
18	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
přibližně	přibližně	k6eAd1
10	#num#	k4
milionů	milion	k4xCgInPc2
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
26	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
letech	let	k1gInPc6
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poválečná	poválečný	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
byla	být	k5eAaImAgFnS
o	o	k7c4
45	#num#	k4
až	až	k9
50	#num#	k4
milionů	milion	k4xCgInPc2
menší	malý	k2eAgNnPc1d2
než	než	k8xS
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
pokračoval	pokračovat	k5eAaImAgInS
předválečný	předválečný	k2eAgInSc1d1
demografický	demografický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Catherine	Catherin	k1gInSc5
Merridale	Merridala	k1gFnSc6
by	by	k9
"	"	kIx"
<g/>
rozumný	rozumný	k2eAgInSc4d1
odhad	odhad	k1gInSc4
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
nadměrných	nadměrný	k2eAgNnPc2d1
úmrtí	úmrtí	k1gNnPc2
po	po	k7c4
celé	celý	k2eAgNnSc4d1
období	období	k1gNnSc4
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
asi	asi	k9
60	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Porodnost	porodnost	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
SSSR	SSSR	kA
snížila	snížit	k5eAaPmAgFnS
z	z	k7c2
44,0	44,0	k4
na	na	k7c4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
na	na	k7c4
18,0	18,0	k4
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
kvůli	kvůli	k7c3
rostoucí	rostoucí	k2eAgFnSc3d1
urbanizaci	urbanizace	k1gFnSc3
a	a	k8xC
rostoucímu	rostoucí	k2eAgInSc3d1
průměrnému	průměrný	k2eAgInSc3d1
věku	věk	k1gInSc3
vstupu	vstup	k1gInSc2
do	do	k7c2
manželství	manželství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míra	Míra	k1gFnSc1
úmrtnosti	úmrtnost	k1gFnSc2
také	také	k9
vykazovala	vykazovat	k5eAaImAgFnS
postupný	postupný	k2eAgInSc4d1
pokles	pokles	k1gInSc4
-	-	kIx~
z	z	k7c2
23,7	23,7	k4
na	na	k7c4
tisíc	tisíc	k4xCgInSc4
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
na	na	k7c4
8,7	8,7	k4
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
byla	být	k5eAaImAgFnS
míra	míra	k1gFnSc1
porodnosti	porodnost	k1gFnSc2
jižních	jižní	k2eAgFnPc2d1
republik	republika	k1gFnPc2
v	v	k7c6
Zakavkazsku	Zakavkazsko	k1gNnSc6
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
výrazně	výrazně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
míra	míra	k1gFnSc1
porodnosti	porodnost	k1gFnSc2
v	v	k7c6
severních	severní	k2eAgFnPc6d1
částech	část	k1gFnPc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
v	v	k7c4
období	období	k1gNnSc4
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
dokonce	dokonce	k9
ještě	ještě	k6eAd1
vzrostla	vzrůst	k5eAaPmAgFnS
-	-	kIx~
fenomén	fenomén	k1gInSc1
částečně	částečně	k6eAd1
připsaný	připsaný	k2eAgInSc1d1
pomalejší	pomalý	k2eAgFnSc3d2
míře	míra	k1gFnSc3
urbanizace	urbanizace	k1gFnSc2
a	a	k8xC
tradičně	tradičně	k6eAd1
brzkým	brzký	k2eAgInPc3d1
sňatkům	sňatek	k1gInPc3
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
republikách	republika	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
byly	být	k5eAaImAgInP
svědky	svědek	k1gMnPc4
obrácení	obrácení	k1gNnSc1
klesající	klesající	k2eAgFnPc1d1
trajektorie	trajektorie	k1gFnPc1
míry	míra	k1gFnSc2
úmrtnosti	úmrtnost	k1gFnSc2
v	v	k7c6
SSSR	SSSR	kA
a	a	k8xC
byla	být	k5eAaImAgFnS
obzvláště	obzvláště	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
u	u	k7c2
mužů	muž	k1gMnPc2
v	v	k7c6
produktivním	produktivní	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
byla	být	k5eAaImAgFnS
převládající	převládající	k2eAgFnSc1d1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
převážně	převážně	k6eAd1
slovanských	slovanský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Analýza	analýza	k1gFnSc1
oficiálních	oficiální	k2eAgInPc2d1
údajů	údaj	k1gInPc2
z	z	k7c2
konce	konec	k1gInSc2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
zhoršení	zhoršení	k1gNnSc6
na	na	k7c6
konci	konec	k1gInSc6
sedmdesátých	sedmdesátý	k4xOgNnPc2
a	a	k8xC
začátku	začátek	k1gInSc2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
úmrtnost	úmrtnost	k1gFnSc1
dospělých	dospělí	k1gMnPc2
začala	začít	k5eAaPmAgFnS
znovu	znovu	k6eAd1
zlepšovat	zlepšovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Míra	Míra	k1gFnSc1
kojenecké	kojenecký	k2eAgFnSc2d1
úmrtnosti	úmrtnost	k1gFnSc2
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
z	z	k7c2
24,7	24,7	k4
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
na	na	k7c4
27,9	27,9	k4
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
výzkumníci	výzkumník	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
nárůst	nárůst	k1gInSc4
za	za	k7c4
velmi	velmi	k6eAd1
reálný	reálný	k2eAgInSc4d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
důsledek	důsledek	k1gInSc1
zhoršujících	zhoršující	k2eAgFnPc2d1
se	se	k3xPyFc4
zdravotních	zdravotní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Zvýšení	zvýšení	k1gNnSc1
jak	jak	k8xC,k8xS
dospělé	dospělý	k2eAgFnPc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
kojenecké	kojenecký	k2eAgFnSc2d1
úmrtnosti	úmrtnost	k1gFnSc2
nebyly	být	k5eNaImAgFnP
sovětskými	sovětský	k2eAgNnPc7d1
úředníky	úředník	k1gMnPc4
vysvětlovány	vysvětlován	k2eAgMnPc4d1
nebo	nebo	k8xC
obhajovány	obhajován	k2eAgMnPc4d1
a	a	k8xC
sovětská	sovětský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
prostě	prostě	k6eAd1
přestala	přestat	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
statistiky	statistika	k1gFnPc4
úmrtnosti	úmrtnost	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
deseti	deset	k4xCc2
let	léto	k1gNnPc2
publikovat	publikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětští	sovětský	k2eAgMnPc1d1
demografové	demograf	k1gMnPc1
a	a	k8xC
zdravotní	zdravotní	k2eAgMnPc1d1
specialisté	specialista	k1gMnPc1
mlčeli	mlčet	k5eAaImAgMnP
o	o	k7c4
zvýšení	zvýšení	k1gNnSc4
úmrtnosti	úmrtnost	k1gFnSc2
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
publikace	publikace	k1gFnSc1
údajů	údaj	k1gInPc2
o	o	k7c6
úmrtnosti	úmrtnost	k1gFnSc6
a	a	k8xC
vědci	vědec	k1gMnPc1
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
ponořit	ponořit	k5eAaPmF
do	do	k7c2
skutečných	skutečný	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
etnicky	etnicky	k6eAd1
rozmanitým	rozmanitý	k2eAgInSc7d1
státem	stát	k1gInSc7
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
odlišnými	odlišný	k2eAgFnPc7d1
etnickými	etnický	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
odhadován	odhadovat	k5eAaImNgMnS
na	na	k7c4
293	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odhadu	odhad	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
Rusové	Rus	k1gMnPc1
(	(	kIx(
<g/>
50,78	50,78	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
následovaná	následovaný	k2eAgFnSc1d1
Ukrajinci	Ukrajinec	k1gMnPc7
(	(	kIx(
<g/>
15,45	15,45	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Uzbeky	Uzbek	k1gMnPc7
(	(	kIx(
<g/>
5,84	5,84	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Všichni	všechen	k3xTgMnPc1
občané	občan	k1gMnPc1
SSSR	SSSR	kA
měli	mít	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
etnickou	etnický	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etnická	etnický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
osoby	osoba	k1gFnSc2
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
rodiči	rodič	k1gMnPc7
dítěte	dítě	k1gNnSc2
ve	v	k7c6
věku	věk	k1gInSc6
šestnácti	šestnáct	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
rodiče	rodič	k1gMnPc1
neshodli	shodnout	k5eNaPmAgMnP
<g/>
,	,	kIx,
dítě	dítě	k1gNnSc1
bylo	být	k5eAaImAgNnS
automaticky	automaticky	k6eAd1
přiřazeno	přiřazen	k2eAgNnSc1d1
k	k	k7c3
etnickému	etnický	k2eAgInSc3d1
původu	původ	k1gInSc3
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
kvůli	kvůli	k7c3
sovětské	sovětský	k2eAgFnSc3d1
politice	politika	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
některé	některý	k3yIgFnPc4
menší	malý	k2eAgFnPc4d2
minoritní	minoritní	k2eAgFnPc4d1
etnické	etnický	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
považovány	považován	k2eAgFnPc4d1
za	za	k7c4
součást	součást	k1gFnSc4
větších	veliký	k2eAgFnPc2d2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
gruzínští	gruzínský	k2eAgMnPc1d1
Mingrelové	Mingrel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
zařazováni	zařazován	k2eAgMnPc1d1
s	s	k7c7
jazykově	jazykově	k6eAd1
příbuznými	příbuzný	k2eAgMnPc7d1
Gruzínci	Gruzínec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
asimilovaly	asimilovat	k5eAaBmAgFnP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiné	jiný	k2eAgNnSc4d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
byly	být	k5eAaImAgFnP
přivedeny	přiveden	k2eAgFnPc1d1
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgInSc1d1
<g/>
,	,	kIx,
Bělorusové	Bělorus	k1gMnPc1
a	a	k8xC
Ukrajinci	Ukrajinec	k1gMnPc1
sdíleli	sdílet	k5eAaImAgMnP
těsné	těsný	k2eAgFnPc4d1
kulturní	kulturní	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiné	jiný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vícero	vícero	k1gNnSc1
národnostmi	národnost	k1gFnPc7
žijícími	žijící	k2eAgInPc7d1
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
území	území	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
rozvíjely	rozvíjet	k5eAaImAgInP
etnické	etnický	k2eAgInPc1d1
antagonizmy	antagonizmus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Etnografická	etnografický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
1941	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
a	a	k8xC
procentuální	procentuální	k2eAgInSc1d1
poměr	poměr	k1gInSc1
Ukrajinců	Ukrajinec	k1gMnPc2
v	v	k7c6
regionech	region	k1gInPc6
RSFSR	RSFSR	kA
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc4
lidu	lid	k1gInSc2
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ukrajinci	Ukrajinec	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
RSFSR	RSFSR	kA
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc4
lidu	lid	k1gInSc2
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Muslimské	muslimský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
v	v	k7c6
SSSR	SSSR	kA
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc4
lidu	lid	k1gInSc2
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jazyky	jazyk	k1gInPc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
jako	jako	k8xC,k8xS
mnohonárodnostní	mnohonárodnostní	k2eAgInSc1d1
stát	stát	k1gInSc1
byl	být	k5eAaImAgInS
jazykově	jazykově	k6eAd1
velmi	velmi	k6eAd1
bohatý	bohatý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
dominující	dominující	k2eAgFnSc2d1
ruštiny	ruština	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
se	se	k3xPyFc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
území	území	k1gNnSc1
hovořilo	hovořit	k5eAaImAgNnS
dalšími	další	k2eAgFnPc7d1
desítkami	desítka	k1gFnPc7
jazyků	jazyk	k1gInPc2
z	z	k7c2
mnoha	mnoho	k4c2
jazykových	jazykový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	zem	k1gFnSc2
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
neměla	mít	k5eNaImAgFnS
oficiální	oficiální	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
ruština	ruština	k1gFnSc1
jako	jako	k8xC,k8xS
řeč	řeč	k1gFnSc1
majoritní	majoritní	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
mnohdy	mnohdy	k6eAd1
preferována	preferován	k2eAgFnSc1d1
a	a	k8xC
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
se	se	k3xPyFc4
její	její	k3xOp3gInSc1
význam	význam	k1gInSc1
jako	jako	k8xC,k8xS
hlavního	hlavní	k2eAgInSc2d1
komunikačního	komunikační	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
я	я	k?
м	м	k?
о	о	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
bilingvismu	bilingvismus	k1gInSc3
a	a	k8xC
multilingvismu	multilingvismus	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
mnohými	mnohý	k2eAgMnPc7d1
národy	národ	k1gInPc4
nenáviděn	nenáviděn	k2eAgMnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
pobaltských	pobaltský	k2eAgFnPc6d1
republikách	republika	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
západní	západní	k2eAgFnSc6d1
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
ale	ale	k8xC
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
jazyky	jazyk	k1gInPc1
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
říjnové	říjnový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
bolševici	bolševik	k1gMnPc1
ustanovili	ustanovit	k5eAaPmAgMnP
základní	základní	k2eAgInPc4d1
body	bod	k1gInPc4
politiky	politika	k1gFnSc2
rozvoje	rozvoj	k1gInSc2
národnostních	národnostní	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
tehdejších	tehdejší	k2eAgMnPc2d1
marxistů	marxista	k1gMnPc2
se	se	k3xPyFc4
ale	ale	k9
proti	proti	k7c3
myšlence	myšlenka	k1gFnSc3
jednoho	jeden	k4xCgInSc2
národního	národní	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
postavila	postavit	k5eAaPmAgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
pak	pak	k6eAd1
tuto	tento	k3xDgFnSc4
myšlenku	myšlenka	k1gFnSc4
bolševický	bolševický	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
uplatnil	uplatnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
tak	tak	k6eAd1
k	k	k7c3
dočasné	dočasný	k2eAgFnSc3d1
derusifikaci	derusifikace	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
však	však	k9
ostře	ostro	k6eAd1
kontrastuje	kontrastovat	k5eAaImIp3nS
s	s	k7c7
obdobím	období	k1gNnSc7
stalinismu	stalinismus	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
celé	celý	k2eAgInPc1d1
národy	národ	k1gInPc1
stěhovány	stěhován	k2eAgInPc1d1
a	a	k8xC
rozprášeny	rozprášen	k2eAgInPc1d1
po	po	k7c4
území	území	k1gNnSc4
Svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otázka	otázka	k1gFnSc1
jazyků	jazyk	k1gInPc2
jako	jako	k8xC,k8xS
politicky	politicky	k6eAd1
citlivá	citlivý	k2eAgFnSc1d1
ještě	ještě	k9
z	z	k7c2
dob	doba	k1gFnPc2
carské	carský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
k	k	k7c3
někdy	někdy	k6eAd1
násilné	násilný	k2eAgFnSc3d1
rusifikaci	rusifikace	k1gFnSc3
také	také	k9
docházelo	docházet	k5eAaImAgNnS
a	a	k8xC
ovlivnila	ovlivnit	k5eAaPmAgFnS
mnoho	mnoho	k4c4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
docházelo	docházet	k5eAaImAgNnS
ke	k	k7c3
změnám	změna	k1gFnPc3
některých	některý	k3yIgMnPc2
jazyků	jazyk	k1gMnPc2
národů	národ	k1gInPc2
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
dosavadní	dosavadní	k2eAgNnSc4d1
arabské	arabský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
nahrazovala	nahrazovat	k5eAaImAgFnS
cyrilice	cyrilice	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
u	u	k7c2
turkických	turkický	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
nebo	nebo	k8xC
jidiš	jidiš	k6eAd1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
cizí	cizí	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
náhrada	náhrada	k1gFnSc1
cyrilicí	cyrilice	k1gFnSc7
se	se	k3xPyFc4
ovšem	ovšem	k9
většinou	většinou	k6eAd1
zachovala	zachovat	k5eAaPmAgFnS
i	i	k9
po	po	k7c4
osamostatnění	osamostatnění	k1gNnSc4
středoasijských	středoasijský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Krista	Kristus	k1gMnSc2
Spasitele	spasitel	k1gMnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
během	během	k7c2
demolice	demolice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
náboženské	náboženský	k2eAgFnSc6d1
příslušnosti	příslušnost	k1gFnSc6
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
nebyly	být	k5eNaImAgFnP
zveřejněny	zveřejnit	k5eAaPmNgFnP
a	a	k8xC
země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
ateistická	ateistický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
však	však	k9
odhadovaly	odhadovat	k5eAaImAgInP
<g/>
,	,	kIx,
že	že	k8xS
přibližně	přibližně	k6eAd1
jedna	jeden	k4xCgFnSc1
třetina	třetina	k1gFnSc1
občanů	občan	k1gMnPc2
byla	být	k5eAaImAgFnS
nábožensky	nábožensky	k6eAd1
aktivní	aktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějšími	důležitý	k2eAgNnPc7d3
náboženstvími	náboženství	k1gNnPc7
v	v	k7c6
zemi	zem	k1gFnSc6
byly	být	k5eAaImAgFnP
křesťanství	křesťanství	k1gNnSc4
a	a	k8xC
islám	islám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
křesťanství	křesťanství	k1gNnSc2
měla	mít	k5eAaImAgFnS
největší	veliký	k2eAgFnSc4d3
podporu	podpora	k1gFnSc4
Ruská	ruský	k2eAgFnSc1d1
pravoslavná	pravoslavný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
byly	být	k5eAaImAgInP
zde	zde	k6eAd1
i	i	k9
jiné	jiný	k2eAgFnSc2d1
ortodoxní	ortodoxní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
římské	římský	k2eAgFnSc2d1
<g/>
,	,	kIx,
řeckokatolické	řeckokatolický	k2eAgFnSc2d1
a	a	k8xC
různé	různý	k2eAgFnSc2d1
protestantské	protestantský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
muslimů	muslim	k1gMnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
sunnitům	sunnita	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
Ázerbájdžánu	Ázerbájdžán	k1gInSc2
náležela	náležet	k5eAaImAgFnS
k	k	k7c3
ší	ší	k?
<g/>
'	'	kIx"
<g/>
itům	itům	k1gMnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
súfismus	súfismus	k1gInSc1
byl	být	k5eAaImAgInS
zastoupen	zastoupit	k5eAaPmNgInS
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
a	a	k8xC
Čečensku	Čečensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zde	zde	k6eAd1
žila	žít	k5eAaImAgFnS
významná	významný	k2eAgFnSc1d1
židovská	židovský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ateismus	ateismus	k1gInSc1
byl	být	k5eAaImAgInS
zdůrazňován	zdůrazňovat	k5eAaImNgInS
ve	v	k7c6
školách	škola	k1gFnPc6
a	a	k8xC
stát	stát	k5eAaImF,k5eAaPmF
náboženství	náboženství	k1gNnSc3
v	v	k7c6
různé	různý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
existence	existence	k1gFnSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
potlačoval	potlačovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
dekret	dekret	k1gInSc4
Rady	rada	k1gFnSc2
lidových	lidový	k2eAgMnPc2d1
komisařů	komisař	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgInPc3,k3yRgInPc3,k3yIgInPc3
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
RSFSR	RSFSR	kA
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
sekulární	sekulární	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
také	také	k9
říkal	říkat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
učení	učení	k1gNnSc3
o	o	k7c4
náboženství	náboženství	k1gNnSc4
na	na	k7c6
všech	všecek	k3xTgNnPc6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
vyučovány	vyučován	k2eAgInPc4d1
předměty	předmět	k1gInPc4
obecné	obecný	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zakázáno	zakázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občané	občan	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vzdělávat	vzdělávat	k5eAaImF
a	a	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vyučováni	vyučovat	k5eAaImNgMnP
náboženství	náboženství	k1gNnSc3
soukromě	soukromě	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Mezi	mezi	k7c4
další	další	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
přijatá	přijatý	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
prvních	první	k4xOgMnPc2
pěti	pět	k4xCc2
let	léto	k1gNnPc2
Stalinovy	Stalinův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
patřily	patřit	k5eAaImAgInP
výslovné	výslovný	k2eAgInPc1d1
zákazy	zákaz	k1gInPc1
na	na	k7c4
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
církevních	církevní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
setkání	setkání	k1gNnSc2
organizovaného	organizovaný	k2eAgNnSc2d1
studia	studio	k1gNnSc2
Bible	bible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různá	různý	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
i	i	k8xC
nekřesťanská	křesťanský	k2eNgNnPc1d1
zařízení	zařízení	k1gNnPc1
byla	být	k5eAaImAgNnP
po	po	k7c6
tisících	tisící	k4xOgFnPc2
během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
zavírána	zavírán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
bylo	být	k5eAaImAgNnS
uzavřeno	uzavřít	k5eAaPmNgNnS
až	až	k9
90	#num#	k4
procent	procento	k1gNnPc2
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
synagog	synagoga	k1gFnPc2
a	a	k8xC
mešit	mešita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
fungovaly	fungovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
</s>
<s>
Vzdělávací	vzdělávací	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Právo	právo	k1gNnSc1
na	na	k7c4
bezplatné	bezplatný	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
na	na	k7c6
všech	všecek	k3xTgFnPc6
úrovních	úroveň	k1gFnPc6
<g/>
,	,	kIx,
od	od	k7c2
základního	základní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
až	až	k9
po	po	k7c4
vyšší	vysoký	k2eAgNnSc4d2
vzdělávání	vzdělávání	k1gNnSc4
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
občané	občan	k1gMnPc1
SSSR	SSSR	kA
a	a	k8xC
bylo	být	k5eAaImAgNnS
zakotveno	zakotvit	k5eAaPmNgNnS
v	v	k7c6
článku	článek	k1gInSc6
45	#num#	k4
Ústavy	ústava	k1gFnSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Média	médium	k1gNnPc1
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
deníku	deník	k1gInSc2
Pravda	pravda	k9
z	z	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravda	pravda	k1gFnSc1
byla	být	k5eAaImAgFnS
nejdůležitějším	důležitý	k2eAgInSc7d3
sovětským	sovětský	k2eAgInSc7d1
deníkem	deník	k1gInSc7
s	s	k7c7
20	#num#	k4
miliony	milion	k4xCgInPc7
čtenáři	čtenář	k1gMnPc7
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Masová	masový	k2eAgNnPc1d1
média	médium	k1gNnPc1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
prodloužená	prodloužený	k2eAgFnSc1d1
ruka	ruka	k1gFnSc1
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
také	také	k9
ovládat	ovládat	k5eAaImF
a	a	k8xC
mobilizovat	mobilizovat	k5eAaBmF
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
revoluce	revoluce	k1gFnSc2
byli	být	k5eAaImAgMnP
Lenin	Lenin	k1gMnSc1
a	a	k8xC
bolševici	bolševik	k1gMnPc1
závislí	závislý	k2eAgMnPc1d1
na	na	k7c6
mediální	mediální	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
získali	získat	k5eAaPmAgMnP
podporu	podpora	k1gFnSc4
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
novin	novina	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
úřady	úřad	k1gInPc1
naléhaly	naléhat	k5eAaImAgInP,k5eAaBmAgInP
na	na	k7c4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
"	"	kIx"
<g/>
budovali	budovat	k5eAaImAgMnP
socialismus	socialismus	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
propagandy	propaganda	k1gFnSc2
sovětských	sovětský	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
staly	stát	k5eAaPmAgInP
filmy	film	k1gInPc1
<g/>
,	,	kIx,
televize	televize	k1gFnSc1
a	a	k8xC
počítače	počítač	k1gInPc1
a	a	k8xC
všechna	všechen	k3xTgNnPc1
média	médium	k1gNnPc1
byla	být	k5eAaImAgNnP
používána	používán	k2eAgNnPc1d1
k	k	k7c3
šíření	šíření	k1gNnSc3
marxisticko-leninských	marxisticko-leninský	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologický	technologický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
však	však	k9
ztížil	ztížit	k5eAaPmAgInS
udržení	udržení	k1gNnSc4
přilnavosti	přilnavost	k1gFnSc2
k	k	k7c3
masové	masový	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
domácí	domácí	k2eAgInPc4d1
videosystémy	videosystém	k1gInPc4
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
znesnadňovaly	znesnadňovat	k5eAaImAgInP
kontrolu	kontrola	k1gFnSc4
cirkulace	cirkulace	k1gFnSc2
zakázaných	zakázaný	k2eAgFnPc2d1
videokazet	videokazeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
vycházelo	vycházet	k5eAaImAgNnS
v	v	k7c6
SSSR	SSSR	kA
více	hodně	k6eAd2
než	než	k8xS
8	#num#	k4
000	#num#	k4
denních	denní	k2eAgFnPc2d1
tiskovin	tiskovina	k1gFnPc2
v	v	k7c6
60	#num#	k4
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
měly	mít	k5eAaImAgFnP
celkový	celkový	k2eAgInSc4d1
oběh	oběh	k1gInSc4
asi	asi	k9
170	#num#	k4
milionů	milion	k4xCgInPc2
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitrostátní	vnitrostátní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
byly	být	k5eAaImAgFnP
vždycky	vždycky	k6eAd1
v	v	k7c6
ruštině	ruština	k1gFnSc6
<g/>
,	,	kIx,
přestože	přestože	k8xS
jen	jen	k9
polovina	polovina	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
byla	být	k5eAaImAgFnS
etnickými	etnický	k2eAgMnPc7d1
Rusy	Rus	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
představovaly	představovat	k5eAaImAgInP
přibližně	přibližně	k6eAd1
25	#num#	k4
<g/>
%	%	kIx~
celkového	celkový	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
novináři	novinář	k1gMnPc1
a	a	k8xC
redaktoři	redaktor	k1gMnPc1
novinářů	novinář	k1gMnPc2
byli	být	k5eAaImAgMnP
členy	člen	k1gInPc4
stranicky	stranicky	k6eAd1
řízené	řízený	k2eAgFnSc2d1
odborové	odborový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
novináře	novinář	k1gMnPc4
a	a	k8xC
80	#num#	k4
<g/>
%	%	kIx~
z	z	k7c2
nich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
členy	člen	k1gMnPc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
redaktoři	redaktor	k1gMnPc1
byli	být	k5eAaImAgMnP
také	také	k9
jmenováni	jmenovat	k5eAaImNgMnP,k5eAaBmNgMnP
stranou	stranou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žurnalistické	žurnalistický	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
bylo	být	k5eAaImAgNnS
také	také	k9
silně	silně	k6eAd1
politicky	politicky	k6eAd1
řízeno	řídit	k5eAaImNgNnS
a	a	k8xC
běžná	běžný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
v	v	k7c6
novinovém	novinový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
vedla	vést	k5eAaImAgFnS
přes	přes	k7c4
katedru	katedra	k1gFnSc4
žurnalistiky	žurnalistika	k1gFnSc2
na	na	k7c6
Moskevské	moskevský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
studenti	student	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
dobré	dobrý	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
ve	v	k7c6
stranické	stranický	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
deníků	deník	k1gInPc2
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
a	a	k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
Pravda	pravda	k1gFnSc1
s	s	k7c7
20	#num#	k4
miliony	milion	k4xCgInPc7
čtenáři	čtenář	k1gMnPc7
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustředila	soustředit	k5eAaPmAgFnS
se	se	k3xPyFc4
hlavně	hlavně	k9
na	na	k7c4
události	událost	k1gFnPc4
v	v	k7c6
komunistické	komunistický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
a	a	k8xC
domácí	domácí	k1gMnPc4
i	i	k9
zahraniční	zahraniční	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
novinami	novina	k1gFnPc7
byla	být	k5eAaImAgFnS
Izvestija	Izvestija	k1gFnSc1
(	(	kIx(
<g/>
vydávaná	vydávaný	k2eAgNnPc4d1
nejvyšším	vysoký	k2eAgInSc7d3
sovětem	sovět	k1gInSc7
s	s	k7c7
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
miliony	milion	k4xCgInPc7
čtenáři	čtenář	k1gMnPc7
za	za	k7c4
den	den	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Trud	trud	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Práce	práce	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vydávaná	vydávaný	k2eAgFnSc1d1
odborovými	odborový	k2eAgInPc7d1
svazy	svaz	k1gInPc1
s	s	k7c7
8	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
miliony	milion	k4xCgInPc7
čtenáři	čtenář	k1gMnPc7
za	za	k7c4
den	den	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
Komsomolskaja	Komsomolskaj	k2eAgFnSc1d1
pravda	pravda	k1gFnSc1
(	(	kIx(
<g/>
vydávaná	vydávaný	k2eAgFnSc1d1
Komsomolem	Komsomol	k1gInSc7
s	s	k7c7
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
miliony	milion	k4xCgInPc7
čtenáři	čtenář	k1gMnPc7
za	za	k7c4
den	den	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejdůležitějším	důležitý	k2eAgNnSc7d3
masovým	masový	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
ji	on	k3xPp3gFnSc4
mělo	mít	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
75	#num#	k4
milionů	milion	k4xCgInPc2
domácností	domácnost	k1gFnPc2
a	a	k8xC
odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
93	#num#	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
televizi	televize	k1gFnSc4
sledovalo	sledovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgMnPc4
televizní	televizní	k2eAgInPc4d1
kanály	kanál	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vysílaly	vysílat	k5eAaImAgInP
denně	denně	k6eAd1
pětatřicet	pětatřicet	k4xCc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
kanály	kanál	k1gInPc1
vysílaly	vysílat	k5eAaImAgInP
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
20	#num#	k4
<g/>
%	%	kIx~
času	čas	k1gInSc2
byly	být	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
program	program	k1gInSc1
Vremja	Vremjus	k1gMnSc4
byl	být	k5eAaImAgInS
vlajkovou	vlajkový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vremju	Vremju	k1gFnPc2
sledovalo	sledovat	k5eAaImAgNnS
80	#num#	k4
až	až	k8xS
90	#num#	k4
<g/>
%	%	kIx~
domácností	domácnost	k1gFnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vysílána	vysílat	k5eAaImNgFnS
v	v	k7c6
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
hodin	hodina	k1gFnPc2
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
zpráv	zpráva	k1gFnPc2
byly	být	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
z	z	k7c2
domova	domov	k1gInSc2
a	a	k8xC
zpráv	zpráva	k1gFnPc2
o	o	k7c6
sportu	sport	k1gInSc6
bylo	být	k5eAaImAgNnS
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
programem	program	k1gInSc7
pro	pro	k7c4
zahraniční	zahraniční	k2eAgNnSc4d1
zpravodajství	zpravodajství	k1gNnSc4
bylo	být	k5eAaImAgNnS
Vokrug	Vokrug	k1gInSc4
sveta	svet	k1gInSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Kolem	kolem	k7c2
světa	svět	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
dvěma	dva	k4xCgMnPc7
vysílání	vysílání	k1gNnSc4
každé	každý	k3xTgNnSc4
odpoledne	odpoledne	k1gNnSc4
a	a	k8xC
večer	večer	k6eAd1
denně	denně	k6eAd1
přilákal	přilákat	k5eAaPmAgInS
mezi	mezi	k7c7
60	#num#	k4
a	a	k8xC
90	#num#	k4
miliony	milion	k4xCgInPc1
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
televizní	televizní	k2eAgInPc4d1
pořady	pořad	k1gInPc4
byly	být	k5eAaImAgFnP
ideologického	ideologický	k2eAgInSc2d1
rázu	ráz	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
diváky	divák	k1gMnPc4
podněcovali	podněcovat	k5eAaImAgMnP
k	k	k7c3
větší	veliký	k2eAgFnSc3d2
práci	práce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Televizní	televizní	k2eAgInPc1d1
filmy	film	k1gInPc1
často	často	k6eAd1
hrály	hrát	k5eAaImAgInP
na	na	k7c4
druhoválečně	druhoválečně	k6eAd1
heroismy	heroismus	k1gInPc7
nebo	nebo	k8xC
na	na	k7c4
policejní	policejní	k2eAgFnPc4d1
a	a	k8xC
bezpečnostní	bezpečnostní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
„	„	k?
<g/>
imperialistickým	imperialistický	k2eAgFnPc3d1
hrozbám	hrozba	k1gFnPc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dětské	dětský	k2eAgInPc1d1
programy	program	k1gInPc1
byly	být	k5eAaImAgInP
vždy	vždy	k6eAd1
nenásilné	násilný	k2eNgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
zábavní	zábavní	k2eAgInPc1d1
programy	program	k1gInPc1
zdůrazňovaly	zdůrazňovat	k5eAaImAgInP
správné	správný	k2eAgNnSc4d1
společenské	společenský	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
televize	televize	k1gFnSc2
a	a	k8xC
novin	novina	k1gFnPc2
také	také	k6eAd1
rozhlasové	rozhlasový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
mělo	mít	k5eAaImAgNnS
ideologickou	ideologický	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřady	úřad	k1gInPc1
vysílaly	vysílat	k5eAaImAgInP
celkem	celkem	k6eAd1
1	#num#	k4
400	#num#	k4
hodin	hodina	k1gFnPc2
rozhlasu	rozhlas	k1gInSc2
denně	denně	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInPc1d1
vysílače	vysílač	k1gInPc1
vysílaly	vysílat	k5eAaImAgInP
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
osm	osm	k4xCc1
stanic	stanice	k1gFnPc2
vysílalo	vysílat	k5eAaImAgNnS
180	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
bylo	být	k5eAaImAgNnS
Rádio	rádio	k1gNnSc1
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
sovětským	sovětský	k2eAgInSc7d1
státním	státní	k2eAgNnPc3d1
oficiálními	oficiální	k2eAgFnPc7d1
mluvčím	mluvčí	k1gMnSc7
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vysílalo	vysílat	k5eAaImAgNnS
254	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
celkem	celkem	k6eAd1
v	v	k7c6
77	#num#	k4
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
vláda	vláda	k1gFnSc1
nad	nad	k7c7
éterem	éter	k1gInSc7
nebyla	být	k5eNaImAgFnS
taková	takový	k3xDgFnSc1
jako	jako	k9
u	u	k7c2
televize	televize	k1gFnSc2
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
v	v	k7c6
Estonsku	Estonsko	k1gNnSc6
část	část	k1gFnSc1
země	země	k1gFnSc1
chytala	chytat	k5eAaImAgFnS
finskou	finský	k2eAgFnSc4d1
televizi	televize	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
státní	státní	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
měly	mít	k5eAaImAgFnP
konkurovat	konkurovat	k5eAaImF
zahraničním	zahraniční	k2eAgFnPc3d1
stanicím	stanice	k1gFnPc3
navzdory	navzdory	k7c3
pokusu	pokus	k1gInSc3
sovětských	sovětský	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
zablokování	zablokování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitými	důležitý	k2eAgFnPc7d1
zahraničními	zahraniční	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
Svobodná	svobodný	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Hlas	hlas	k1gInSc1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
BBC	BBC	kA
a	a	k8xC
Deutsche	Deutsche	k1gFnSc1
Welle	Welle	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
dva	dva	k4xCgInPc1
až	až	k8xS
tři	tři	k4xCgInPc1
miliony	milion	k4xCgInPc1
sovětských	sovětský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
pravidelně	pravidelně	k6eAd1
poslouchali	poslouchat	k5eAaImAgMnP
zahraniční	zahraniční	k2eAgNnSc4d1
rádio	rádio	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Pionýři	pionýr	k1gMnPc1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
budově	budova	k1gFnSc6
moskevského	moskevský	k2eAgNnSc2d1
železničního	železniční	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
herec	herec	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Vysockij	Vysockij	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1
život	život	k1gInSc1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
prošel	projít	k5eAaPmAgInS
několika	několik	k4yIc7
fázemi	fáze	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1918-1929	1918-1929	k4
existovaly	existovat	k5eAaImAgFnP
poměrně	poměrně	k6eAd1
svobodné	svobodný	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
a	a	k8xC
umělci	umělec	k1gMnPc1
experimentovali	experimentovat	k5eAaImAgMnP
s	s	k7c7
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
hledání	hledání	k1gNnSc2
výrazného	výrazný	k2eAgInSc2d1
sovětského	sovětský	k2eAgInSc2d1
uměleckého	umělecký	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lenin	Lenin	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
umění	umění	k1gNnSc1
bylo	být	k5eAaImAgNnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
masám	masa	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orgány	orgán	k1gInPc1
tolerovaly	tolerovat	k5eAaImAgFnP
více	hodně	k6eAd2
trendů	trend	k1gInPc2
pokud	pokud	k8xS
nebyly	být	k5eNaImAgFnP
zjevně	zjevně	k6eAd1
proti	proti	k7c3
režimu	režim	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
umění	umění	k1gNnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
vzkvétalo	vzkvétat	k5eAaImAgNnS
několik	několik	k4yIc1
různých	různý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc4
tradiční	tradiční	k2eAgFnPc4d1
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc4d1
radikálně	radikálně	k6eAd1
experimentální	experimentální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
působili	působit	k5eAaImAgMnP
komunisté	komunista	k1gMnPc1
jako	jako	k8xC,k8xS
Maxim	Maxim	k1gMnSc1
Gorkij	Gorkij	k1gMnSc1
a	a	k8xC
Vladimir	Vladimir	k1gMnSc1
Majakovskij	Majakovskij	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
užitečný	užitečný	k2eAgInSc4d1
propagační	propagační	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
pro	pro	k7c4
získání	získání	k1gNnSc4
širokých	široký	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
povětšinou	povětšina	k1gFnSc7
negramotní	gramotný	k2eNgMnPc1d1
a	a	k8xC
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětská	sovětský	k2eAgFnSc1d1
montážní	montážní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
období	období	k1gNnSc6
1920	#num#	k4
až	až	k9
1932	#num#	k4
své	své	k1gNnSc4
zlaté	zlatý	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
filmů	film	k1gInPc2
režiséra	režisér	k1gMnSc2
Sergeje	Sergej	k1gMnSc2
Ejzenštejn	Ejzenštejn	k1gMnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
filmu	film	k1gInSc2
Křižník	křižník	k1gInSc4
Potěmkin	Potěmkin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
se	se	k3xPyFc4
také	také	k6eAd1
stalo	stát	k5eAaPmAgNnS
jednotným	jednotný	k2eAgMnSc7d1
a	a	k8xC
zaměřovalo	zaměřovat	k5eAaImAgNnS
se	se	k3xPyFc4
na	na	k7c6
ukázání	ukázání	k1gNnSc6
nadvlády	nadvláda	k1gFnSc2
komunismu	komunismus	k1gInSc2
nad	nad	k7c7
všemi	všecek	k3xTgFnPc7
ostatními	ostatní	k2eAgFnPc7d1
formami	forma	k1gFnPc7
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
během	během	k7c2
Stalinovy	Stalinův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
byla	být	k5eAaImAgFnS
sovětská	sovětský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
charakterizována	charakterizovat	k5eAaBmNgFnS
vzestupem	vzestup	k1gInSc7
a	a	k8xC
provládním	provládní	k2eAgInSc7d1
stylem	styl	k1gInSc7
socialistického	socialistický	k2eAgInSc2d1
realismu	realismus	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
všechny	všechen	k3xTgInPc1
ostatní	ostatní	k2eAgInPc1d1
trendy	trend	k1gInPc1
byly	být	k5eAaImAgInP
silně	silně	k6eAd1
potlačovány	potlačován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácně	vzácně	k6eAd1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
výjimky	výjimka	k1gFnPc1
jako	jako	k9
například	například	k6eAd1
Michail	Michail	k1gInSc1
Bulgakov	Bulgakov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
spisovatelů	spisovatel	k1gMnPc2
bylo	být	k5eAaImAgNnS
uvězněno	uvěznit	k5eAaPmNgNnS
a	a	k8xC
zabito	zabít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Chruščovovském	Chruščovovský	k2eAgNnSc6d1
tání	tání	k1gNnSc6
na	na	k7c6
přelomu	přelom	k1gInSc6
padesátých	padesátý	k4xOgNnPc2
a	a	k8xC
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
cenzury	cenzura	k1gFnSc2
ubylo	ubýt	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
zvláštní	zvláštní	k2eAgNnSc1d1
období	období	k1gNnSc1
sovětské	sovětský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
charakterizované	charakterizovaný	k2eAgNnSc1d1
konformním	konformní	k2eAgInSc7d1
veřejným	veřejný	k2eAgInSc7d1
životem	život	k1gInSc7
a	a	k8xC
intenzivním	intenzivní	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
osobní	osobní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInPc1d2
experimenty	experiment	k1gInPc1
s	s	k7c7
uměleckými	umělecký	k2eAgFnPc7d1
formami	forma	k1gFnPc7
byly	být	k5eAaImAgFnP
znovu	znovu	k6eAd1
povoleny	povolen	k2eAgFnPc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
sofistikovanější	sofistikovaný	k2eAgFnPc1d2
a	a	k8xC
jemně	jemně	k6eAd1
kritické	kritický	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režim	režim	k1gInSc1
uvolnil	uvolnit	k5eAaPmAgInS
důraz	důraz	k1gInSc4
na	na	k7c4
socialistický	socialistický	k2eAgInSc4d1
realismus	realismus	k1gInSc4
<g/>
;	;	kIx,
například	například	k6eAd1
mnoho	mnoho	k4c1
postav	postava	k1gFnPc2
románů	román	k1gInPc2
autora	autor	k1gMnSc4
Jurije	Jurije	k1gFnSc2
Trifonova	Trifonův	k2eAgInSc2d1
se	se	k3xPyFc4
zabývalo	zabývat	k5eAaImAgNnS
spíše	spíše	k9
problémy	problém	k1gInPc4
každodenního	každodenní	k2eAgInSc2d1
života	život	k1gInSc2
než	než	k8xS
budováním	budování	k1gNnSc7
socialismu	socialismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
pozdní	pozdní	k2eAgFnSc6d1
době	doba	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
podzemní	podzemní	k2eAgFnSc1d1
disidentská	disidentský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k9
samizdat	samizdat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
architektuře	architektura	k1gFnSc6
se	se	k3xPyFc4
Chruščovova	Chruščovův	k2eAgFnSc1d1
éra	éra	k1gFnSc1
soustředila	soustředit	k5eAaPmAgFnS
především	především	k9
na	na	k7c4
funkční	funkční	k2eAgInSc4d1
design	design	k1gInSc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
vysoce	vysoce	k6eAd1
zdobeného	zdobený	k2eAgInSc2d1
stylu	styl	k1gInSc2
Stalinovy	Stalinův	k2eAgFnSc2d1
epochy	epocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
Gorbačovova	Gorbačovův	k2eAgFnSc1d1
politika	politika	k1gFnSc1
perestrojky	perestrojka	k1gFnSc2
a	a	k8xC
glasnosti	glasnost	k1gFnSc2
výrazně	výrazně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
svobodu	svoboda	k1gFnSc4
projevu	projev	k1gInSc2
v	v	k7c6
celém	celý	k2eAgInSc6d1
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
médiích	médium	k1gNnPc6
i	i	k8xC
tisku	tisk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Sovětská	sovětský	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
<g/>
,	,	kIx,
Sovětská	sovětský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
a	a	k8xC
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Valerij	Valerít	k5eAaPmRp2nS
Charlamov	Charlamov	k1gInSc1
reprezentoval	reprezentovat	k5eAaImAgMnS
SSSR	SSSR	kA
na	na	k7c6
11	#num#	k4
mistrovstvích	mistrovství	k1gNnPc6
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
<g/>
;	;	kIx,
získal	získat	k5eAaPmAgMnS
8	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
stříbrné	stříbrná	k1gFnPc1
a	a	k8xC
1	#num#	k4
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
vždy	vždy	k6eAd1
existovala	existovat	k5eAaImAgFnS
jasná	jasný	k2eAgFnSc1d1
souvislost	souvislost	k1gFnSc1
mezi	mezi	k7c7
sportem	sport	k1gInSc7
a	a	k8xC
politikou	politika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
hlavní	hlavní	k2eAgFnSc1d1
rezoluce	rezoluce	k1gFnSc1
strany	strana	k1gFnSc2
o	o	k7c6
sportu	sport	k1gInSc6
to	ten	k3xDgNnSc4
jasně	jasně	k6eAd1
ukázala	ukázat	k5eAaPmAgFnS
«	«	k?
<g/>
na	na	k7c4
sportovní	sportovní	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
by	by	kYmCp3nS
se	se	k3xPyFc4
nemělo	mít	k5eNaImAgNnS
nahlížet	nahlížet	k5eAaImF
pouze	pouze	k6eAd1
z	z	k7c2
pohledu	pohled	k1gInSc2
veřejného	veřejný	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
a	a	k8xC
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
jako	jako	k9
aspektu	aspekt	k1gInSc2
kulturního	kulturní	k2eAgInSc2d1
<g/>
,	,	kIx,
ekonomického	ekonomický	k2eAgInSc2d1
a	a	k8xC
vojenského	vojenský	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
jako	jako	k9
prostředku	prostředek	k1gInSc2
socializace	socializace	k1gFnSc2
masy	masa	k1gFnSc2
<g/>
...	...	k?
<g/>
ale	ale	k8xC
jako	jako	k9
i	i	k9
na	na	k7c4
prostředek	prostředek	k1gInSc4
náboru	nábor	k1gInSc2
rolníků	rolník	k1gMnPc2
a	a	k8xC
pracovníků	pracovník	k1gMnPc2
do	do	k7c2
různých	různý	k2eAgFnPc2d1
stranických	stranický	k2eAgFnPc2d1
<g/>
,	,	kIx,
pracovních	pracovní	k2eAgFnPc2d1
a	a	k8xC
odborových	odborový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
přivedeni	přivést	k5eAaPmNgMnP
k	k	k7c3
společenským	společenský	k2eAgFnPc3d1
a	a	k8xC
politickým	politický	k2eAgFnPc3d1
činnostem	činnost	k1gFnPc3
<g/>
.	.	kIx.
<g/>
»	»	k?
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sporty	sport	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
SSSR	SSSR	kA
organizovány	organizovat	k5eAaBmNgInP
v	v	k7c6
rámci	rámec	k1gInSc6
Výboru	výbor	k1gInSc2
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
a	a	k8xC
sport	sport	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
spadal	spadat	k5eAaPmAgInS,k5eAaImAgInS
pod	pod	k7c4
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
výbor	výbor	k1gInSc1
měl	mít	k5eAaImAgInS
pod	pod	k7c7
sebou	se	k3xPyFc7
36	#num#	k4
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
a	a	k8xC
všechny	všechen	k3xTgInPc4
kromě	kromě	k7c2
dvou	dva	k4xCgMnPc6
byly	být	k5eAaImAgFnP
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
odborovými	odborový	k2eAgInPc7d1
svazy	svaz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
příklady	příklad	k1gInPc4
známých	známý	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
odborovými	odborový	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
patřily	patřit	k5eAaImAgFnP
Locomotiv	Locomotiv	k1gFnSc7
(	(	kIx(
<g/>
železniční	železniční	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Burevestnik	Burevestnik	k1gInSc1
(	(	kIx(
<g/>
studenti	student	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
Spartak	Spartak	k1gInSc4
(	(	kIx(
<g/>
osoby	osoba	k1gFnPc4
zaměstnané	zaměstnaný	k2eAgFnPc4d1
např.	např.	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc2
a	a	k8xC
kulturních	kulturní	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovaly	existovat	k5eAaImAgInP
také	také	k6eAd1
dva	dva	k4xCgInPc4
sportovní	sportovní	k2eAgInPc4d1
kluby	klub	k1gInPc4
mimo	mimo	k7c4
odbory	odbor	k1gInPc4
<g/>
;	;	kIx,
Dynamo	dynamo	k1gNnSc1
(	(	kIx(
<g/>
policie	policie	k1gFnSc1
a	a	k8xC
KGB	KGB	kA
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
pracovní	pracovní	k2eAgFnSc1d1
rezerva	rezerva	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
studenti	student	k1gMnPc1
technických	technický	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
ještě	ještě	k9
existoval	existovat	k5eAaImAgInS
armádní	armádní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
CSKA	CSKA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kluby	klub	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
CSKA	CSKA	kA
a	a	k8xC
Dynama	dynamo	k1gNnSc2
slavily	slavit	k5eAaImAgInP
nejvíce	hodně	k6eAd3,k6eAd1
sportovních	sportovní	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Až	až	k9
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
stál	stát	k5eAaImAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
mimo	mimo	k7c4
mezinárodní	mezinárodní	k2eAgNnPc4d1
sportovní	sportovní	k2eAgNnPc4d1
hnutí	hnutí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
založen	založit	k5eAaPmNgInS
sovětský	sovětský	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
a	a	k8xC
zimních	zimní	k2eAgMnPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
v	v	k7c6
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
se	se	k3xPyFc4
již	již	k9
SSSR	SSSR	kA
poprvé	poprvé	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychle	rychle	k6eAd1
získal	získat	k5eAaPmAgInS
mezinárodních	mezinárodní	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
a	a	k8xC
ve	v	k7c6
statistikách	statistika	k1gFnPc6
medailí	medaile	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
druhou	druhý	k4xOgFnSc7
nejúspěšnější	úspěšný	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
v	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
a	a	k8xC
nejúspěšnější	úspěšný	k2eAgMnSc1d3
v	v	k7c6
Cortině	Cortina	k1gFnSc6
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
letní	letní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
v	v	k7c6
Soulu	Soul	k1gInSc6
a	a	k8xC
zimní	zimní	k2eAgFnPc1d1
olympiády	olympiáda	k1gFnPc1
v	v	k7c6
Calgary	Calgary	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
získal	získat	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
dohromady	dohromady	k6eAd1
395	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
a	a	k8xC
78	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
na	na	k7c6
zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
hostila	hostit	k5eAaImAgFnS
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ze	z	k7c2
strany	strana	k1gFnSc2
USA	USA	kA
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
NSR	NSR	kA
a	a	k8xC
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
byly	být	k5eAaImAgFnP
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
sovětské	sovětský	k2eAgFnSc3d1
invazi	invaze	k1gFnSc3
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
bojkotovány	bojkotován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
a	a	k8xC
řada	řada	k1gFnSc1
zemí	zem	k1gFnPc2
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
bojkotovala	bojkotovat	k5eAaImAgFnS
letní	letní	k2eAgFnSc1d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
SSSR	SSSR	kA
byl	být	k5eAaImAgInS
také	také	k9
široce	široko	k6eAd1
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
doping	doping	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Krajina	Krajina	k1gFnSc1
nedaleko	nedaleko	k7c2
Karabaše	Karabaše	k1gFnSc2
v	v	k7c6
Čeljabinské	čeljabinský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
dříve	dříve	k6eAd2
pokryto	pokrýt	k5eAaPmNgNnS
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyselé	kyselé	k1gNnSc1
dešťové	dešťový	k2eAgFnSc2d1
srážky	srážka	k1gFnSc2
z	z	k7c2
nedaleké	daleký	k2eNgFnSc2d1
měděné	měděný	k2eAgFnSc2d1
hutě	huť	k1gFnSc2
ale	ale	k8xC
veškerou	veškerý	k3xTgFnSc4
vegetaci	vegetace	k1gFnSc4
zabily	zabít	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
vždy	vždy	k6eAd1
přikládala	přikládat	k5eAaImAgFnS
velkou	velký	k2eAgFnSc4d1
důležitost	důležitost	k1gFnSc4
akcím	akce	k1gFnPc3
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
lidské	lidský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
aktivně	aktivně	k6eAd1
zlepšují	zlepšovat	k5eAaImIp3nP
přírodu	příroda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leninův	Leninův	k2eAgInSc4d1
citát	citát	k1gInSc4
"	"	kIx"
<g/>
Komunismus	komunismus	k1gInSc1
je	být	k5eAaImIp3nS
sovětská	sovětský	k2eAgFnSc1d1
moc	moc	k1gFnSc1
a	a	k8xC
elektrifikace	elektrifikace	k1gFnSc1
země	zem	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
shrnuje	shrnovat	k5eAaImIp3nS
zaměření	zaměření	k1gNnSc1
na	na	k7c4
modernizaci	modernizace	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
prvního	první	k4xOgInSc2
pětiletého	pětiletý	k2eAgInSc2d1
plánu	plán	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
Stalin	Stalin	k1gMnSc1
přistoupil	přistoupit	k5eAaPmAgMnS
k	k	k7c3
industrializaci	industrializace	k1gFnSc3
země	zem	k1gFnSc2
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnoty	hodnota	k1gFnPc1
jako	jako	k8xC,k8xS
ochrana	ochrana	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
přírody	příroda	k1gFnSc2
byly	být	k5eAaImAgFnP
zcela	zcela	k6eAd1
ignorovány	ignorovat	k5eAaImNgInP
v	v	k7c6
boji	boj	k1gInSc6
za	za	k7c4
vytvoření	vytvoření	k1gNnSc4
moderní	moderní	k2eAgFnSc2d1
průmyslové	průmyslový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Stalinově	Stalinův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
více	hodně	k6eAd2
zaměřilo	zaměřit	k5eAaPmAgNnS
na	na	k7c4
problematiku	problematika	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
základní	základní	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
hodnoty	hodnota	k1gFnSc2
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
zůstalo	zůstat	k5eAaPmAgNnS
stejné	stejný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1
sdělovací	sdělovací	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
věnovaly	věnovat	k5eAaPmAgFnP,k5eAaImAgFnP
obrovské	obrovský	k2eAgFnSc3d1
rozloze	rozloha	k1gFnSc3
země	zem	k1gFnSc2
a	a	k8xC
prakticky	prakticky	k6eAd1
nezničitelným	zničitelný	k2eNgInPc3d1
přírodním	přírodní	k2eAgInPc3d1
zdrojům	zdroj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
kontaminace	kontaminace	k1gFnSc2
a	a	k8xC
drancování	drancování	k1gNnSc2
přírody	příroda	k1gFnSc2
nebyly	být	k5eNaImAgFnP
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
stát	stát	k1gInSc1
také	také	k9
pevně	pevně	k6eAd1
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vědecký	vědecký	k2eAgInSc1d1
a	a	k8xC
technologický	technologický	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
by	by	kYmCp3nS
vyřešil	vyřešit	k5eAaPmAgInS
všechny	všechen	k3xTgInPc4
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
říkala	říkat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
socialismu	socialismus	k1gInSc2
by	by	kYmCp3nP
se	se	k3xPyFc4
problémy	problém	k1gInPc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
mohly	moct	k5eAaImAgFnP
snadno	snadno	k6eAd1
překonat	překonat	k5eAaPmF
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
kapitalistických	kapitalistický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
zdánlivě	zdánlivě	k6eAd1
nemohly	moct	k5eNaImAgFnP
vyřešit	vyřešit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětské	sovětský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
měly	mít	k5eAaImAgFnP
téměř	téměř	k6eAd1
neochvějné	neochvějný	k2eAgNnSc4d1
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
člověk	člověk	k1gMnSc1
může	moct	k5eAaImIp3nS
přírodu	příroda	k1gFnSc4
překonat	překonat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
orgány	orgán	k1gInPc1
v	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
musely	muset	k5eAaImAgFnP
připustit	připustit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
existují	existovat	k5eAaImIp3nP
ekologické	ekologický	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
vysvětlily	vysvětlit	k5eAaPmAgInP
problémy	problém	k1gInPc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
socialismus	socialismus	k1gInSc1
ještě	ještě	k6eAd1
nebyl	být	k5eNaImAgInS
plně	plně	k6eAd1
rozvinut	rozvinut	k2eAgInSc1d1
<g/>
;	;	kIx,
znečištění	znečištění	k1gNnSc1
v	v	k7c6
socialistické	socialistický	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
bylo	být	k5eAaImAgNnS
jen	jen	k6eAd1
dočasnou	dočasný	k2eAgFnSc7d1
anomálií	anomálie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
vyřešena	vyřešit	k5eAaPmNgFnS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
socialismus	socialismus	k1gInSc1
vyvíjel	vyvíjet	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Černobylská	černobylský	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
svět	svět	k1gInSc4
šokovala	šokovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgFnSc4
velkou	velký	k2eAgFnSc4d1
nehodu	nehoda	k1gFnSc4
v	v	k7c6
civilní	civilní	k2eAgFnSc6d1
jaderné	jaderný	k2eAgFnSc6d1
elektrárně	elektrárna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
na	na	k7c6
světe	svět	k1gInSc5
neměla	mít	k5eNaImAgFnS
obdoby	obdoba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
do	do	k7c2
ovzduší	ovzduší	k1gNnSc2
uvolněno	uvolněn	k2eAgNnSc4d1
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
radioaktivních	radioaktivní	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radioaktivní	radioaktivní	k2eAgFnPc4d1
dávky	dávka	k1gFnPc4
se	se	k3xPyFc4
rozptýlily	rozptýlit	k5eAaPmAgFnP
relativně	relativně	k6eAd1
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
zdravotním	zdravotní	k2eAgInSc7d1
problémem	problém	k1gInSc7
po	po	k7c6
nehodě	nehoda	k1gFnSc6
bylo	být	k5eAaImAgNnS
4000	#num#	k4
nových	nový	k2eAgInPc2d1
případů	případ	k1gInPc2
rakoviny	rakovina	k1gFnSc2
štítné	štítný	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
relativně	relativně	k6eAd1
nízkému	nízký	k2eAgInSc3d1
počtu	počet	k1gInSc3
úmrtí	úmrtí	k1gNnSc2
(	(	kIx(
<g/>
údaje	údaj	k1gInSc2
WHO	WHO	kA
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
dlouhodobé	dlouhodobý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
nehody	nehoda	k1gFnSc2
jsou	být	k5eAaImIp3nP
neznámé	známý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnSc4d1
velkou	velký	k2eAgFnSc4d1
havárii	havárie	k1gFnSc4
patří	patřit	k5eAaImIp3nS
kyštymská	kyštymský	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pádu	pád	k1gInSc6
SSSR	SSSR	kA
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
problémy	problém	k1gInPc1
s	s	k7c7
životním	životní	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
jsou	být	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
jaké	jaký	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
sovětské	sovětský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
připouštěly	připouštět	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
místa	místo	k1gNnPc4
s	s	k7c7
jasnými	jasný	k2eAgInPc7d1
problémy	problém	k1gInPc7
patřil	patřit	k5eAaImAgInS
poloostrov	poloostrov	k1gInSc1
Kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k6eAd1
průmyslových	průmyslový	k2eAgNnPc2d1
měst	město	k1gNnPc2
Mončegorsk	Mončegorsk	k1gInSc1
a	a	k8xC
Norilsk	Norilsk	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
např.	např.	kA
nikl	nikl	k1gInSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
lesy	les	k1gInPc1
zabity	zabít	k5eAaPmNgInP
kontaminací	kontaminace	k1gFnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
severské	severský	k2eAgFnPc1d1
i	i	k8xC
jiné	jiný	k2eAgFnPc1d1
části	část	k1gFnPc1
Ruska	Rusko	k1gNnSc2
byly	být	k5eAaImAgFnP
ovlivněny	ovlivněn	k2eAgFnPc1d1
emisemi	emise	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
ze	z	k7c2
západu	západ	k1gInSc2
také	také	k9
zajímali	zajímat	k5eAaImAgMnP
o	o	k7c4
radioaktivní	radioaktivní	k2eAgNnPc4d1
nebezpečí	nebezpečí	k1gNnPc4
z	z	k7c2
jaderných	jaderný	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
vyřazených	vyřazený	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
a	a	k8xC
zpracování	zpracování	k1gNnSc4
jaderného	jaderný	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
či	či	k8xC
vyhořelého	vyhořelý	k2eAgNnSc2d1
jaderného	jaderný	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
svážel	svážet	k5eAaImAgInS
radioaktivní	radioaktivní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
do	do	k7c2
Barentsova	Barentsův	k2eAgNnSc2d1
a	a	k8xC
Karského	karský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
později	pozdě	k6eAd2
potvrdil	potvrdit	k5eAaPmAgMnS
ruský	ruský	k2eAgInSc4d1
parlament	parlament	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Havárie	havárie	k1gFnSc1
ponorky	ponorka	k1gFnSc2
K-141	K-141	k1gFnSc1
Kursk	Kursk	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
na	na	k7c6
západě	západ	k1gInSc6
dále	daleko	k6eAd2
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
obavám	obava	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nehodám	nehoda	k1gFnPc3
ponorek	ponorka	k1gFnPc2
K-	K-	k1gFnPc2
<g/>
19	#num#	k4
<g/>
,	,	kIx,
K-8	K-8	k1gFnSc1
nebo	nebo	k8xC
K-	K-	k1gFnSc1
<g/>
129	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Russia	Russia	k1gFnSc1
–	–	k?
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannicus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britannica	Britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
27	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
on	on	k3xPp3gMnSc1
29	#num#	k4
July	Jula	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Virginia	Virginium	k1gNnSc2
Thompson	Thompsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Former	Former	k1gMnSc1
Soviet	Soviet	k1gMnSc1
Union	union	k1gInSc1
<g/>
:	:	kIx,
Physical	Physical	k1gFnSc1
Geography	Geographa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Towson	Towson	k1gInSc1
University	universita	k1gFnSc2
<g/>
:	:	kIx,
Department	department	k1gInSc1
of	of	k?
Geography	Geographa	k1gFnSc2
&	&	k?
Environmental	Environmental	k1gMnSc1
Planning	Planning	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
September	September	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
720	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
597	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Ukrajina	Ukrajina	k1gFnSc1
chce	chtít	k5eAaImIp3nS
hladomor	hladomor	k1gInSc4
z	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
oficiálně	oficiálně	k6eAd1
uznat	uznat	k5eAaPmF
za	za	k7c4
genocidu	genocida	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Geoffrey	Geoffrea	k1gFnSc2
A.	A.	kA
Hosking	Hosking	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russia	Russia	k1gFnSc1
and	and	k?
the	the	k?
Russians	Russians	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
history	histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
473	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
469	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
139	#num#	k4
<g/>
-	-	kIx~
<g/>
146	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
G.	G.	kA
I.	I.	kA
Krivosheev	Krivosheva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gMnSc1
Casualties	Casualties	k1gMnSc1
and	and	k?
Combat	Combat	k1gMnSc1
Losses	Losses	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Greenhill	Greenhill	k1gInSc1
1997	#num#	k4
ISBN	ISBN	kA
978-1-85367-280-4	978-1-85367-280-4	k4
Strany	strana	k1gFnSc2
85	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
↑	↑	k?
„	„	k?
<g/>
Geografie	geografie	k1gFnSc2
krvavých	krvavý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respekt	respekt	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
↑	↑	k?
„	„	k?
<g/>
V	v	k7c6
běloruské	běloruský	k2eAgFnSc6d1
Chatyni	Chatyně	k1gFnSc6
upálili	upálit	k5eAaPmAgMnP
nacisté	nacista	k1gMnPc1
zaživa	zaživa	k6eAd1
149	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
75	#num#	k4
dětí	dítě	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
<g/>
↑	↑	k?
William	William	k1gInSc1
J.	J.	kA
Duiker	Duiker	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Contemporary	Contemporara	k1gFnSc2
World	Worldo	k1gNnPc2
History	Histor	k1gInPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wadsworth	Wadsworth	k1gMnSc1
Pub	Pub	k1gMnSc1
Co	co	k9
<g/>
,	,	kIx,
31	#num#	k4
August	August	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
495	#num#	k4
<g/>
-	-	kIx~
<g/>
57271	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
128	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Perestrojka	perestrojka	k1gFnSc1
<g/>
:	:	kIx,
Gorbačov	Gorbačov	k1gInSc1
poprvé	poprvé	k6eAd1
navštívil	navštívit	k5eAaPmAgInS
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
Čechy	Čech	k1gMnPc4
a	a	k8xC
Slováky	Slovák	k1gMnPc4
ale	ale	k8xC
zklamal	zklamat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
duban	duban	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
například	například	k6eAd1
pozici	pozice	k1gFnSc4
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
:	:	kIx,
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Resolution	Resolution	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
situation	situation	k1gInSc1
in	in	k?
Estonia	Estonium	k1gNnSc2
<g/>
,	,	kIx,
Latvia	Latvium	k1gNnSc2
<g/>
,	,	kIx,
Lithuania	Lithuanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Official	Official	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgFnSc1wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
42	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Harold	Harold	k1gMnSc1
Henry	Henry	k1gMnSc1
Fisher	Fishra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Communist	Communist	k1gMnSc1
Revolution	Revolution	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Outline	Outlin	k1gInSc5
of	of	k?
Strategy	Strateg	k1gInPc1
and	and	k?
Tactics	Tactics	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Stanford	Stanford	k1gInSc1
UP	UP	kA
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duncan	Duncan	k1gMnSc1
Hallas	Hallas	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Comintern	Comintern	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Third	Third	k1gMnSc1
International	International	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Germany	German	k1gInPc4
(	(	kIx(
<g/>
East	East	k1gInSc4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
Country	country	k2eAgInPc1d1
Study	stud	k1gInPc1
<g/>
,	,	kIx,
Appendix	Appendix	k1gInSc1
B	B	kA
<g/>
:	:	kIx,
The	The	k1gFnSc4
Council	Councila	k1gFnPc2
for	forum	k1gNnPc2
Mutual	Mutual	k1gMnSc1
Economic	Economic	k1gMnSc1
Assistance	Assistance	k1gFnSc2
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
C.	C.	kA
Kaser	Kaser	k1gMnSc1
<g/>
,	,	kIx,
Comecon	Comecon	k1gMnSc1
<g/>
:	:	kIx,
Integration	Integration	k1gInSc1
problems	problems	k1gInSc1
of	of	k?
the	the	k?
planned	planned	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
economies	economies	k1gInSc1
(	(	kIx(
<g/>
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Laurien	Laurien	k2eAgMnSc1d1
Crump	Crump	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Warsaw	Warsaw	k1gFnSc2
Pact	Pact	k1gMnSc1
Reconsidered	Reconsidered	k1gMnSc1
<g/>
:	:	kIx,
International	International	k1gMnSc1
Relations	Relationsa	k1gFnPc2
in	in	k?
Eastern	Eastern	k1gMnSc1
Europe	Europ	k1gInSc5
<g/>
,	,	kIx,
1955-1969	1955-1969	k4
(	(	kIx(
<g/>
Routledge	Routledg	k1gInSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Michał	Michał	k1gMnSc1
Jerzy	Jerza	k1gFnSc2
Zacharias	Zacharias	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
"	"	kIx"
<g/>
The	The	k1gFnSc1
Beginnings	Beginnings	k1gInSc1
of	of	k?
the	the	k?
Cominform	Cominform	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Policy	Polica	k1gFnSc2
of	of	k?
the	the	k?
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
towards	towardsa	k1gFnPc2
European	European	k1gMnSc1
Communist	Communist	k1gMnSc1
Parties	Parties	k1gMnSc1
in	in	k?
Connection	Connection	k1gInSc1
with	with	k1gMnSc1
the	the	k?
Political	Political	k1gMnSc1
Initiatives	Initiatives	k1gMnSc1
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gFnSc1
in	in	k?
1947	#num#	k4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Acta	Act	k2eAgFnSc1d1
Poloniae	Poloniae	k1gFnSc1
Historica	Historica	k1gFnSc1
78	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
161	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
200	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nikos	Nikos	k1gInSc1
Marantzidis	Marantzidis	k1gFnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gMnSc1
Greek	Greek	k1gMnSc1
Civil	civil	k1gMnSc1
War	War	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
and	and	k?
the	the	k?
International	International	k1gMnSc1
Communist	Communist	k1gMnSc1
System	Syst	k1gMnSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Journal	Journal	k1gMnSc1
of	of	k?
Cold	Cold	k1gMnSc1
War	War	k1gMnSc1
Studies	Studies	k1gMnSc1
15.4	15.4	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
25	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
54	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Heinz	Heinz	k1gMnSc1
Timmermann	Timmermann	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
cominform	cominform	k1gInSc1
effects	effects	k6eAd1
on	on	k3xPp3gMnSc1
Soviet	Soviet	k1gMnSc1
foreign	foreigna	k1gFnPc2
policy	polica	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Studies	Studies	k1gInSc1
in	in	k?
Comparative	Comparativ	k1gInSc5
Communism	Communismo	k1gNnPc2
18.1	18.1	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KUBÁT	Kubát	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorie	teorie	k1gFnSc1
nedemokratických	demokratický	k2eNgInPc2d1
režimů	režim	k1gInPc2
a	a	k8xC
východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
1944	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologický	politologický	k2eAgInSc1d1
časopis	časopis	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
139	#num#	k4
<g/>
–	–	k?
<g/>
157	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
3247	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Scott	Scott	k1gMnSc1
and	and	k?
Scott	Scott	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Armed	Armed	k1gMnSc1
Forces	Forces	k1gMnSc1
of	of	k?
the	the	k?
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
Westview	Westview	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
13	#num#	k4
<g/>
↑	↑	k?
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannicus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inquisitorial	Inquisitorial	k1gInSc1
procedure	procedur	k1gMnSc5
(	(	kIx(
<g/>
law	law	k?
<g/>
)	)	kIx)
–	–	k?
Britannica	Britannica	k1gFnSc1
Online	Onlin	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopæ	Encyclopæ	k1gNnPc1
Britannica	Britannica	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
22	#num#	k4
December	December	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mawdsley	Mawdslea	k1gFnPc1
<g/>
,	,	kIx,
Evan	Evan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Stalin	Stalin	k1gMnSc1
Years	Years	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Soviet	Soviet	k1gMnSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Manchester	Manchester	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7190	#num#	k4
<g/>
-	-	kIx~
<g/>
4600	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wheatcroft	Wheatcroft	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
G.	G.	kA
<g/>
;	;	kIx,
DAVIES	DAVIES	kA
<g/>
,	,	kIx,
R.	R.	kA
W.	W.	kA
<g/>
;	;	kIx,
COOPER	COOPER	kA
<g/>
,	,	kIx,
J.	J.	kA
M.	M.	kA
Soviet	Soviet	k1gInSc1
Industrialization	Industrialization	k1gInSc1
Reconsidered	Reconsidered	k1gInSc1
<g/>
:	:	kIx,
Some	Some	k1gInSc1
Preliminary	Preliminara	k1gFnSc2
Conclusions	Conclusionsa	k1gFnPc2
about	about	k1gMnSc1
Economic	Economic	k1gMnSc1
Development	Development	k1gMnSc1
between	between	k2eAgMnSc1d1
1926	#num#	k4
and	and	k?
1941	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Economic	Economice	k1gFnPc2
History	Histor	k1gInPc1
Review	Review	k1gFnSc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7190	#num#	k4
<g/>
-	-	kIx~
<g/>
4600	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Reconstruction	Reconstruction	k1gInSc1
and	and	k?
Cold	Cold	k1gInSc1
War	War	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Library	Librara	k1gFnSc2
of	of	k?
Congress	Congress	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Reconstruction	Reconstruction	k1gInSc1
and	and	k?
Cold	Cold	k1gInSc1
War	War	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Library	Librara	k1gFnSc2
of	of	k?
Congress	Congress	k1gInSc1
Country	country	k2eAgMnSc1d1
Studies	Studies	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
IMF	IMF	kA
and	and	k?
OECD	OECD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Study	stud	k1gInPc1
of	of	k?
the	the	k?
Soviet	Soviet	k1gInSc1
Economy	Econom	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
International	International	k1gMnSc1
Monetary	Monetara	k1gFnSc2
Fund	fund	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
103797	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Highman	Highman	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
D.	D.	kA
<g/>
S.	S.	kA
<g/>
;	;	kIx,
GREENWOOD	GREENWOOD	kA
<g/>
,	,	kIx,
JOHN	John	k1gMnSc1
T.	T.	kA
<g/>
;	;	kIx,
HARDESTY	HARDESTY	kA
<g/>
,	,	kIx,
VON	von	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russian	Russian	k1gInSc1
Aviation	Aviation	k1gInSc4
and	and	k?
Air	Air	k1gFnSc2
Power	Power	k1gMnSc1
in	in	k?
the	the	k?
Twentieth	Twentieth	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7146	#num#	k4
<g/>
-	-	kIx~
<g/>
4784	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ambler	Ambler	k1gMnSc1
<g/>
,	,	kIx,
Shaw	Shaw	k1gMnSc1
and	and	k?
Symons	Symons	k1gInSc1
1985	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
166	#num#	k4
<g/>
–	–	k?
<g/>
67.1	67.1	k4
2	#num#	k4
Ambler	Ambler	k1gMnSc1
<g/>
,	,	kIx,
Shaw	Shaw	k1gMnSc1
and	and	k?
Symons	Symons	k1gInSc1
1985	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
167	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ambler	Ambler	k1gMnSc1
<g/>
,	,	kIx,
Shaw	Shaw	k1gMnSc1
and	and	k?
Symons	Symons	k1gInSc1
1985	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
169	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
měnový	měnový	k2eAgInSc1d1
fond	fond	k1gInSc1
a	a	k8xC
OECD	OECD	kA
1991	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
56	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Central	Central	k1gMnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc4
–	–	k?
Communications	Communications	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mark	Mark	k1gMnSc1
Harrison	Harrison	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accounting	Accounting	k1gInSc1
for	forum	k1gNnPc2
War	War	k1gFnSc2
<g/>
:	:	kIx,
Soviet	Soviet	k1gInSc1
Production	Production	k1gInSc1
<g/>
,	,	kIx,
Employment	Employment	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Defence	Defenec	k1gInSc2
Burden	Burdna	k1gFnPc2
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
18	#num#	k4
July	Jula	k1gFnSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
89424	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
167	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jay	Jay	k1gMnSc1
Winter	Winter	k1gMnSc1
<g/>
;	;	kIx,
EMMANUEL	Emmanuel	k1gMnSc1
SIVAN	SIVAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
War	War	k1gFnSc1
and	and	k?
Remembrance	Remembrance	k1gFnSc2
in	in	k?
the	the	k?
Twentieth	Twentieth	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
521794366	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
64	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Government	Government	k1gInSc1
of	of	k?
the	the	k?
USSR	USSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moscow	Moscow	k1gFnSc1
<g/>
:	:	kIx,
State	status	k1gInSc5
Committee	Committee	k1gFnSc3
for	forum	k1gNnPc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Russian	Russian	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vallin	Vallin	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CHESNAIS	CHESNAIS	kA
<g/>
,	,	kIx,
J.C.	J.C.	k1gFnSc1
Recent	Recent	k1gMnSc1
Developments	Developments	k1gInSc1
of	of	k?
Mortality	mortalita	k1gFnSc2
in	in	k?
Europe	Europ	k1gMnSc5
<g/>
,	,	kIx,
English-Speaking	English-Speaking	k1gInSc1
Countries	Countries	k1gInSc1
and	and	k?
the	the	k?
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Population	Population	k1gInSc1
Studies	Studiesa	k1gFnPc2
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
861	#num#	k4
<g/>
–	–	k?
<g/>
898	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ryan	Ryan	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Life	Life	k1gNnSc7
Expectancy	Expectanca	k1gFnSc2
and	and	k?
Mortality	mortalita	k1gFnSc2
Data	datum	k1gNnSc2
from	from	k1gMnSc1
the	the	k?
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
28	#num#	k4
May	May	k1gMnSc1
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1,513	1,513	k4
<g/>
–	–	k?
<g/>
1515	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Davis	Davis	k1gFnPc2
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
Feshbach	Feshbach	k1gInSc1
<g/>
,	,	kIx,
Murray	Murray	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rising	Rising	k1gInSc4
Infant	infant	k1gMnSc1
Mortality	mortalita	k1gFnSc2
in	in	k?
the	the	k?
USSR	USSR	kA
in	in	k?
the	the	k?
1970	#num#	k4
<g/>
s.	s.	k?
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
:	:	kIx,
United	United	k1gInSc1
States	Statesa	k1gFnPc2
Census	census	k1gInSc1
Bureau	Bureaus	k1gInSc2
S.	S.	kA
95	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Krimins	Krimins	k1gInSc1
<g/>
,	,	kIx,
Juris	Juris	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Changing	Changing	k1gInSc4
Mortality	mortalita	k1gFnSc2
Patterns	Patterns	k1gInSc4
in	in	k?
Latvia	Latvium	k1gNnSc2
<g/>
,	,	kIx,
Lithuania	Lithuanium	k1gNnSc2
and	and	k?
Estonia	Estonium	k1gNnSc2
<g/>
:	:	kIx,
Experience	Experience	k1gFnSc1
of	of	k?
the	the	k?
Past	past	k1gFnSc1
Three	Thre	k1gFnSc2
Decades	Decadesa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
December	December	k1gInSc1
1990	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paper	Paper	k1gMnSc1
presented	presented	k1gMnSc1
at	at	k?
the	the	k?
International	International	k1gMnSc2
Conference	Conferenec	k1gMnSc2
on	on	k3xPp3gMnSc1
Health	Health	k1gMnSc1
<g/>
,	,	kIx,
Morbidity	morbidita	k1gFnPc1
and	and	k?
Mortality	mortalita	k1gFnSc2
by	by	kYmCp3nS
Cause	causa	k1gFnSc3
of	of	k?
Death	Death	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Central	Central	k1gMnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc4
–	–	k?
People	People	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Gorbachev	Gorbachev	k1gFnSc1
<g/>
,	,	kIx,
Mikhail	Mikhail	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopæ	Encyclopæ	k1gNnPc1
Britannica	Britannica	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
October	October	k1gInSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
RIORDAN	RIORDAN	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gInSc1
Sport	sport	k1gInSc4
and	and	k?
Soviet	Soviet	k1gInSc1
Foreign	Foreigna	k1gFnPc2
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
.	.	kIx.
1974	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
322	#num#	k4
<g/>
–	–	k?
<g/>
343	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
jstor	jstor	k1gInSc1
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BAHENSKÝ	BAHENSKÝ	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národnostní	národnostní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
na	na	k7c6
teritoriu	teritorium	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Etnologický	etnologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
219	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87112	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CROZIER	CROZIER	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzestup	vzestup	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
Sovětské	sovětský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
679	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7341	#num#	k4
<g/>
-	-	kIx~
<g/>
349	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FLORES	FLORES	kA
<g/>
,	,	kIx,
Marcello	Marcello	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunismus	komunismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Levné	levný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
185	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7309	#num#	k4
<g/>
-	-	kIx~
<g/>
388	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KOTYK	KOTYK	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
a	a	k8xC
rozpad	rozpad	k1gInSc1
sovětského	sovětský	k2eAgInSc2d1
bloku	blok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Oeconomica	Oeconomica	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
277	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
1512	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NETOPIL	topit	k5eNaImAgMnS
<g/>
,	,	kIx,
Rostislav	Rostislav	k1gMnSc1
<g/>
;	;	kIx,
SKOKAN	Skokan	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geografie	geografie	k1gFnSc1
SSSR	SSSR	kA
:	:	kIx,
celostátní	celostátní	k2eAgFnSc1d1
vysokoškolská	vysokoškolský	k2eAgFnSc1d1
učebnice	učebnice	k1gFnSc1
pro	pro	k7c4
stud	stud	k1gInSc4
<g/>
.	.	kIx.
pedag	pedag	k1gInSc4
<g/>
.	.	kIx.
a	a	k8xC
přírodověd	přírodověda	k1gFnPc2
<g/>
.	.	kIx.
fak	fak	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
stud	stud	k1gInSc1
<g/>
.	.	kIx.
oboru	obor	k1gInSc2
76-12-8	76-12-8	k4
Učitelství	učitelství	k1gNnSc2
všeobecně	všeobecně	k6eAd1
vzdělávacích	vzdělávací	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
-	-	kIx~
geografie	geografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22269	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
REIMAN	REIMAN	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrod	zrod	k1gInSc1
velmoci	velmoc	k1gFnSc2
:	:	kIx,
dějiny	dějiny	k1gFnPc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
1917	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
583	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
2266	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SERVICE	SERVICE	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc4
of	of	k?
twentieth-century	twentieth-centura	k1gFnSc2
Russia	Russium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
653	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
40348	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ŠVANKMAJER	ŠVANKMAJER	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
558	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
183	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
VEBER	VEBER	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalinovo	Stalinův	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
1924	#num#	k4
<g/>
-	-	kIx~
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
167	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
391	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
VYDRA	Vydra	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
499	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
324	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
hájí	hájit	k5eAaImIp3nS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Tiskové	tiskový	k2eAgInPc1d1
podniky	podnik	k1gInPc1
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
</s>
<s>
Ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
základní	základní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
Svazu	svaz	k1gInSc2
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
<g/>
:	:	kIx,
se	s	k7c7
změnami	změna	k1gFnPc7
a	a	k8xC
doplňky	doplněk	k1gInPc7
schválenými	schválený	k2eAgInPc7d1
I.	I.	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
X.	X.	kA
zasedáním	zasedání	k1gNnSc7
nejvyššího	vysoký	k2eAgInSc2d3
sovětu	sovět	k1gInSc2
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
</s>
<s>
ZUBOV	ZUBOV	kA
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
Borisovič	Borisovič	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Ruska	Ruska	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
921	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc7
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
–	–	k?
Mapa	mapa	k1gFnSc1
SSSR	SSSR	kA
<g/>
,	,	kIx,
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
republiky	republika	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc2
SSSR	SSSR	kA
</s>
<s>
Unione	union	k1gInSc5
Sovietica	Sovieticum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
státy	stát	k1gInPc1
s	s	k7c7
komunistickými	komunistický	k2eAgInPc7d1
režimy	režim	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
F	F	kA
–	–	k?
socialistické	socialistický	k2eAgFnSc2d1
federace	federace	k1gFnSc2
•	•	k?
V	v	k7c6
–	–	k?
členové	člen	k1gMnPc1
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
•	•	k?
X	X	kA
–	–	k?
státy	stát	k1gInPc1
většinově	většinově	k6eAd1
neuznané	uznaný	k2eNgInPc1d1
Současné	současný	k2eAgInPc1d1
socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Čína	Čína	k1gFnSc1
mimo	mimo	k7c4
Tchaj-wan	Tchaj-wan	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Číny	Čína	k1gFnSc2
•	•	k?
Kuba	Kuba	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Kuby	Kuba	k1gFnSc2
•	•	k?
Laos	Laos	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Laoská	laoský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Korejská	korejský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
sever	sever	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
,	,	kIx,
celá	celý	k2eAgFnSc1d1
země	země	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Vietnamu	Vietnam	k1gInSc2
Bývalé	bývalý	k2eAgInPc1d1
socialistické	socialistický	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Angola	Angola	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Angolská	angolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
MPLA	MPLA	kA
•	•	k?
Benin	Benin	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Benin	Benina	k1gFnPc2
/	/	kIx~
Lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Beninu	Benin	k1gInSc2
•	•	k?
Etiopie	Etiopie	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Derg	Derg	k1gInSc1
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Etiopská	etiopský	k2eAgFnSc1d1
lidově	lidově	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Kongo	Kongo	k1gNnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Konžská	konžský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Konžská	konžský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Mosambik	Mosambik	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Mosambická	mosambický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
FRELIMO	FRELIMO	kA
•	•	k?
Somálsko	Somálsko	k1gNnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Somálská	somálský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Somálská	somálský	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Grenada	Grenada	k1gFnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Grenady	Grenada	k1gFnSc2
/	/	kIx~
Nové	Nové	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
JEWEL	JEWEL	kA
Asie	Asie	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Afghánská	afghánský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Lidová	lidový	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Afghánistánu	Afghánistán	k1gInSc2
•	•	k?
Íránský	íránský	k2eAgInSc1d1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Lidová	lidový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Ázerbájdžánu	Ázerbájdžán	k1gInSc2
X	X	kA
•	•	k?
Jižní	jižní	k2eAgInSc4d1
Jemen	Jemen	k1gInSc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Jemenská	jemenský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Jemenská	jemenský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Demokratická	demokratický	k2eAgFnSc1d1
Kampučia	Kampučia	k1gFnSc1
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Kampučie	Kampučie	k1gFnSc2
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Kampučská	kampučský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Kampučská	kampučský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Mongolská	mongolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
/	/	kIx~
Mongolská	mongolský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Persie	Persie	k1gFnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Perská	perský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Tuva	Tuva	k1gMnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Tuvinská	Tuvinský	k2eAgFnSc1d1
aratská	aratský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
/	/	kIx~
Tuvinská	Tuvinský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Albánská	albánský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Albánská	albánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Alsasko	Alsasko	k1gNnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alsaská	alsaský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
•	•	k?
Bavorsko	Bavorsko	k1gNnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Bavorská	bavorský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bulharsko	Bulharsko	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Bulharská	bulharský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Bulharská	bulharský	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
F	F	kA
V	v	k7c4
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
•	•	k?
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Finská	finský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Halič	halič	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Haličská	haličský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
X	X	kA
•	•	k?
Jugoslávie	Jugoslávie	k1gFnSc2
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Socialistická	socialistický	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
F	F	kA
/	/	kIx~
Svaz	svaz	k1gInSc1
komunistů	komunista	k1gMnPc2
Jugoslávie	Jugoslávie	k1gFnSc2
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
<g/>
;	;	kIx,
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Maďarská	maďarský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
pracujících	pracující	k1gMnPc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maďarská	maďarský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Východní	východní	k2eAgInSc4d1
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Německá	německý	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Sjednocená	sjednocený	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
•	•	k?
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Polská	polský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Polská	polský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Rumunská	rumunský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
V	V	kA
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Rumunska	Rumunsko	k1gNnSc2
•	•	k?
Rusko	Rusko	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
impérium	impérium	k1gNnSc4
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
F	F	kA
V	V	kA
/	/	kIx~
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
•	•	k?
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
rad	rada	k1gFnPc2
X	X	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128583	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4077548-3	4077548-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2375	#num#	k4
1518	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80126312	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124251745	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80126312	#num#	k4
</s>
