<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
С	С	k?
<g/>
,	,	kIx,
Sovětskij	Sovětskij	k1gMnSc1
Sojuz	Sojuz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Svaz	svaz	k1gInSc1
sovětských	sovětský	k2eAgFnPc2d1
socialistických	socialistický	k2eAgFnPc2d1
republik	republika	k1gFnPc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
<g/>
́	́	k?
<g/>
з	з	k?
С	С	k?
<g/>
́	́	k?
<g/>
т	т	k?
С	С	k?
<g/>
́	́	k?
<g/>
ч	ч	k?
Р	Р	k?
<g/>
́	́	k?
<g/>
б	б	k?
<g/>
,	,	kIx,
Sojuz	Sojuz	k1gInSc1
Sovětskich	Sovětskich	k1gMnSc1
Socialističeskich	Socialističeskich	k1gMnSc1
Respublik	Respublik	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
SSSR	SSSR	kA
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
eurasijský	eurasijský	k2eAgInSc1d1
stát	stát	k1gInSc1
se	s	k7c7
socialistickým	socialistický	k2eAgNnSc7d1
zřízením	zřízení	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
existoval	existovat	k5eAaImAgInS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
1922	#num#	k4
až	až	k6eAd1
1991	#num#	k4
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
dřívějšího	dřívější	k2eAgNnSc2d1
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>