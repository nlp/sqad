<s>
Kubismus	kubismus	k1gInSc1	kubismus
-	-	kIx~	-
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
cube	cube	k1gNnSc1	cube
-	-	kIx~	-
krychle	krychle	k1gFnPc1	krychle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířský	malířský	k2eAgInSc1d1	malířský
a	a	k8xC	a
sochařský	sochařský	k2eAgInSc1d1	sochařský
směr	směr	k1gInSc1	směr
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
projevil	projevit	k5eAaPmAgInS	projevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
-	-	kIx~	-
některé	některý	k3yIgFnPc4	některý
básně	báseň	k1gFnPc4	báseň
Jeana	Jean	k1gMnSc2	Jean
Cocteaua	Cocteauus	k1gMnSc2	Cocteauus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
pojetí	pojetí	k1gNnSc2	pojetí
obrazové	obrazový	k2eAgFnSc2d1	obrazová
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
