<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
nesprávně	správně	k6eNd1	správně
ZOO	zoo	k1gFnSc1	zoo
-	-	kIx~	-
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zábavné	zábavný	k2eAgNnSc1d1	zábavné
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgNnSc1d1	vědecké
a	a	k8xC	a
osvětové	osvětový	k2eAgNnSc1d1	osvětové
zařízení	zařízení	k1gNnSc1	zařízení
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
co	co	k8xS	co
nejbližších	blízký	k2eAgInPc6d3	Nejbližší
přirozenému	přirozený	k2eAgInSc3d1	přirozený
životu	život	k1gInSc3	život
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
existenci	existence	k1gFnSc3	existence
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
vymírajících	vymírající	k2eAgInPc2d1	vymírající
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zubr	zubr	k1gMnSc1	zubr
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgNnSc2d1	převalský
<g/>
,	,	kIx,	,
berneška	berneška	k1gFnSc1	berneška
havajská	havajský	k2eAgFnSc1d1	Havajská
<g/>
,	,	kIx,	,
bažant	bažant	k1gMnSc1	bažant
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
<g/>
,	,	kIx,	,
aligátor	aligátor	k1gMnSc1	aligátor
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložen	doložit	k5eAaPmNgInS	doložit
již	již	k6eAd1	již
z	z	k7c2	z
období	období	k1gNnSc2	období
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
byla	být	k5eAaImAgNnP	být
chována	chovat	k5eAaImNgNnP	chovat
pro	pro	k7c4	pro
užitek	užitek	k1gInSc4	užitek
<g/>
,	,	kIx,	,
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
mocenské	mocenský	k2eAgFnSc2d1	mocenská
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
předvádění	předvádění	k1gNnSc2	předvádění
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
motivace	motivace	k1gFnSc1	motivace
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
vznik	vznik	k1gInSc4	vznik
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
doklady	doklad	k1gInPc1	doklad
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
některá	některý	k3yIgNnPc4	některý
zvířata	zvíře	k1gNnPc4	zvíře
chována	chován	k2eAgNnPc4d1	chováno
v	v	k7c6	v
chrámových	chrámový	k2eAgFnPc6d1	chrámová
či	či	k8xC	či
palácových	palácový	k2eAgFnPc6d1	palácová
zahradách	zahrada	k1gFnPc6	zahrada
už	už	k6eAd1	už
v	v	k7c6	v
období	období	k1gNnSc6	období
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nechen	Nechna	k1gFnPc2	Nechna
již	již	k9	již
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
3	[number]	k4	3
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
zvířata	zvíře	k1gNnPc4	zvíře
chována	chován	k2eAgNnPc4d1	chováno
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Vesetu	Veset	k1gInSc6	Veset
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
faraoni	faraon	k1gMnPc1	faraon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hatšepsut	Hatšepsut	k1gInSc1	Hatšepsut
nebo	nebo	k8xC	nebo
Thutmose	Thutmosa	k1gFnSc3	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
dováželi	dovážet	k5eAaImAgMnP	dovážet
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Penet	Peneta	k1gFnPc2	Peneta
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
chovaným	chovaný	k2eAgInPc3d1	chovaný
druhům	druh	k1gInPc3	druh
patřily	patřit	k5eAaImAgFnP	patřit
opice	opice	k1gFnPc1	opice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pavián	pavián	k1gMnSc1	pavián
pláštíkový	pláštíkový	k2eAgMnSc1d1	pláštíkový
<g/>
,	,	kIx,	,
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
,	,	kIx,	,
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
pštrosi	pštros	k1gMnPc1	pštros
nebo	nebo	k8xC	nebo
kočkovité	kočkovitý	k2eAgFnPc1d1	kočkovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
uctívaná	uctívaný	k2eAgNnPc4d1	uctívané
ve	v	k7c6	v
staroegyptském	staroegyptský	k2eAgNnSc6d1	staroegyptské
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
chov	chov	k1gInSc1	chov
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
kultickou	kultický	k2eAgFnSc4d1	kultická
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
,	,	kIx,	,
býků	býk	k1gMnPc2	býk
<g/>
,	,	kIx,	,
adaxů	adax	k1gInPc2	adax
<g/>
,	,	kIx,	,
ibisů	ibis	k1gMnPc2	ibis
či	či	k8xC	či
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
málo	málo	k1gNnSc4	málo
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
doložen	doložen	k2eAgInSc1d1	doložen
také	také	k9	také
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
středoasyrské	středoasyrský	k2eAgFnSc6d1	středoasyrský
a	a	k8xC	a
novobabylónské	novobabylónský	k2eAgFnSc6d1	Novobabylónská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Asyrští	asyrský	k2eAgMnPc1d1	asyrský
králové	král	k1gMnPc1	král
Tiglatpilesar	Tiglatpilesara	k1gFnPc2	Tiglatpilesara
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Aššurnasirpal	Aššurnasirpal	k1gMnSc1	Aššurnasirpal
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sinacherib	Sinacherib	k1gInSc1	Sinacherib
či	či	k8xC	či
Aššurbanipal	Aššurbanipal	k1gMnPc1	Aššurbanipal
chovali	chovat	k5eAaImAgMnP	chovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
palácích	palác	k1gInPc6	palác
lvy	lev	k1gInPc1	lev
<g/>
,	,	kIx,	,
levharty	levhart	k1gMnPc4	levhart
<g/>
,	,	kIx,	,
velbloudy	velbloud	k1gMnPc4	velbloud
<g/>
,	,	kIx,	,
kozorožce	kozorožec	k1gMnPc4	kozorožec
<g/>
,	,	kIx,	,
slony	slon	k1gMnPc4	slon
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
předváděna	předvádět	k5eAaImNgFnS	předvádět
poddaným	poddaný	k1gMnSc7	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Aššurnasirpala	Aššurnasirpal	k1gMnSc2	Aššurnasirpal
II	II	kA	II
<g/>
.	.	kIx.	.
pochází	pocházet	k5eAaImIp3nS	pocházet
tento	tento	k3xDgInSc1	tento
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechna	všechen	k3xTgFnSc1	všechen
kolik	kolik	k9	kolik
jen	jen	k6eAd1	jen
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc1	zvíře
nížin	nížina	k1gFnPc2	nížina
i	i	k8xC	i
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
přivedl	přivést	k5eAaPmAgMnS	přivést
a	a	k8xC	a
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
je	on	k3xPp3gInPc4	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
svým	svůj	k3xOyFgMnPc3	svůj
poddaným	poddaný	k1gMnPc3	poddaný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zvířata	zvíře	k1gNnPc1	zvíře
chovali	chovat	k5eAaImAgMnP	chovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
palácích	palác	k1gInPc6	palác
také	také	k9	také
féničtí	fénický	k2eAgMnPc1d1	fénický
či	či	k8xC	či
izraelští	izraelský	k2eAgMnPc1d1	izraelský
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Šalomoun	Šalomoun	k1gMnSc1	Šalomoun
podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
královské	královský	k2eAgFnSc2d1	královská
choval	chovat	k5eAaImAgMnS	chovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
pávy	páv	k1gMnPc4	páv
a	a	k8xC	a
opice	opice	k1gFnPc4	opice
<g/>
.	.	kIx.	.
</s>
<s>
Perští	perský	k2eAgMnPc1d1	perský
králové	král	k1gMnPc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Achajmenovců	Achajmenovec	k1gMnPc2	Achajmenovec
chovali	chovat	k5eAaImAgMnP	chovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
palácích	palác	k1gInPc6	palác
lvy	lev	k1gInPc1	lev
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
někdy	někdy	k6eAd1	někdy
předhazovali	předhazovat	k5eAaImAgMnP	předhazovat
zločince	zločinec	k1gMnSc4	zločinec
odsouzené	odsouzený	k2eAgFnSc2d1	odsouzená
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
biblická	biblický	k2eAgFnSc1d1	biblická
kniha	kniha	k1gFnSc1	kniha
Daniel	Daniela	k1gFnPc2	Daniela
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
zpráv	zpráva	k1gFnPc2	zpráva
máme	mít	k5eAaImIp1nP	mít
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
existovala	existovat	k5eAaImAgFnS	existovat
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
makedonské	makedonský	k2eAgFnSc6d1	makedonská
Pelle	Pella	k1gFnSc6	Pella
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgNnPc3d1	veliké
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
prováděl	provádět	k5eAaImAgMnS	provádět
výzkumy	výzkum	k1gInPc4	výzkum
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
používala	používat	k5eAaImAgFnS	používat
ke	k	k7c3	k
krutým	krutý	k2eAgMnPc3d1	krutý
gladiátorským	gladiátorský	k2eAgMnPc3d1	gladiátorský
zápasům	zápas	k1gInPc3	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
byla	být	k5eAaImAgNnP	být
zvířata	zvíře	k1gNnPc1	zvíře
zabíjena	zabíjet	k5eAaImNgNnP	zabíjet
speciálně	speciálně	k6eAd1	speciálně
vycvičenými	vycvičený	k2eAgMnPc7d1	vycvičený
gladiátory	gladiátor	k1gMnPc7	gladiátor
-	-	kIx~	-
venatiory	venatior	k1gMnPc7	venatior
<g/>
,	,	kIx,	,
štvána	štván	k2eAgNnPc4d1	štváno
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gInPc3	on
byli	být	k5eAaImAgMnP	být
předhazováni	předhazován	k2eAgMnPc1d1	předhazován
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
popravy	poprava	k1gFnSc2	poprava
se	se	k3xPyFc4	se
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
právu	právo	k1gNnSc6	právo
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
ad	ad	k7c4	ad
bestias	bestias	k1gInSc4	bestias
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
dovážena	dovážen	k2eAgFnSc1d1	dovážena
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
i	i	k8xC	i
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
lvy	lev	k1gMnPc4	lev
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
levharty	levhart	k1gMnPc4	levhart
<g/>
,	,	kIx,	,
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
krokodýly	krokodýl	k1gMnPc4	krokodýl
<g/>
,	,	kIx,	,
slony	slon	k1gMnPc4	slon
<g/>
,	,	kIx,	,
nosorožce	nosorožec	k1gMnPc4	nosorožec
<g/>
,	,	kIx,	,
pratury	pratur	k1gMnPc4	pratur
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
či	či	k8xC	či
zubry	zubr	k1gMnPc4	zubr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
císařském	císařský	k2eAgNnSc6d1	císařské
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
dovážena	dovážen	k2eAgFnSc1d1	dovážena
i	i	k8xC	i
neškodná	škodný	k2eNgNnPc1d1	neškodné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
žirafy	žirafa	k1gFnPc1	žirafa
<g/>
,	,	kIx,	,
pštrosi	pštros	k1gMnPc1	pštros
<g/>
,	,	kIx,	,
mufloni	muflon	k1gMnPc1	muflon
<g/>
,	,	kIx,	,
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
tuleni	tuleň	k1gMnPc1	tuleň
či	či	k8xC	či
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
venatioři	venatior	k1gMnPc1	venatior
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
pro	pro	k7c4	pro
pobavení	pobavení	k1gNnSc4	pobavení
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Dovoz	dovoz	k1gInSc4	dovoz
zvířat	zvíře	k1gNnPc2	zvíře
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nestvůrných	stvůrný	k2eNgInPc2d1	nestvůrný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Trajána	Traján	k1gMnSc2	Traján
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
v	v	k7c6	v
arénách	aréna	k1gFnPc6	aréna
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
000	[number]	k4	000
různých	různý	k2eAgNnPc2d1	různé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
neskončily	skončit	k5eNaPmAgFnP	skončit
ani	ani	k8xC	ani
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ukončil	ukončit	k5eAaPmAgInS	ukončit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
ostrogótský	ostrogótský	k2eAgMnSc1d1	ostrogótský
král	král	k1gMnSc1	král
Theoderich	Theoderich	k1gMnSc1	Theoderich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
680	[number]	k4	680
n.	n.	k?	n.
l.	l.	k?	l.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
byla	být	k5eAaImAgNnP	být
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
chována	chován	k2eAgNnPc1d1	chováno
také	také	k6eAd1	také
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
palácových	palácový	k2eAgFnPc2d1	palácová
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zřídil	zřídit	k5eAaPmAgMnS	zřídit
zakladatel	zakladatel	k1gMnSc1	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Čou	Čou	k1gMnSc1	Čou
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Wen	Wen	k1gMnSc1	Wen
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
park	park	k1gInSc1	park
zvaný	zvaný	k2eAgInSc1d1	zvaný
Zahrada	zahrada	k1gFnSc1	zahrada
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
jeřábi	jeřáb	k1gMnPc1	jeřáb
<g/>
,	,	kIx,	,
bažanti	bažant	k1gMnPc1	bažant
a	a	k8xC	a
jeleni	jelen	k1gMnPc1	jelen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
existovaly	existovat	k5eAaImAgFnP	existovat
zahrady	zahrada	k1gFnPc1	zahrada
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Maurjů	Maurj	k1gMnPc2	Maurj
ve	v	k7c6	v
3	[number]	k4	3
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
chováni	chovat	k5eAaImNgMnP	chovat
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
aztéčtí	aztécký	k2eAgMnPc1d1	aztécký
panovníci	panovník	k1gMnPc1	panovník
měli	mít	k5eAaImAgMnP	mít
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
paláce	palác	k1gInSc2	palác
v	v	k7c4	v
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
zahradu	zahrada	k1gFnSc4	zahrada
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
jaguáři	jaguár	k1gMnPc1	jaguár
<g/>
,	,	kIx,	,
pumy	puma	k1gFnPc1	puma
<g/>
,	,	kIx,	,
hadi	had	k1gMnPc1	had
nebo	nebo	k8xC	nebo
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
sbírku	sbírka	k1gFnSc4	sbírka
znetvořených	znetvořený	k2eAgFnPc2d1	znetvořená
a	a	k8xC	a
tělesně	tělesně	k6eAd1	tělesně
postižených	postižený	k2eAgMnPc2d1	postižený
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
známým	známý	k2eAgMnSc7d1	známý
chovatelem	chovatel	k1gMnSc7	chovatel
zvířat	zvíře	k1gNnPc2	zvíře
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dvoře	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
choval	chovat	k5eAaImAgMnS	chovat
velbloudy	velbloud	k1gMnPc4	velbloud
jednohrbé	jednohrbý	k2eAgMnPc4d1	jednohrbý
<g/>
,	,	kIx,	,
lvy	lev	k1gMnPc4	lev
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
i	i	k8xC	i
slona	slon	k1gMnSc4	slon
indického	indický	k2eAgInSc2d1	indický
jménem	jméno	k1gNnSc7	jméno
Abbás	Abbás	k1gInSc1	Abbás
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgInS	žít
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
tehdy	tehdy	k6eAd1	tehdy
chovali	chovat	k5eAaImAgMnP	chovat
také	také	k9	také
muslimští	muslimský	k2eAgMnPc1d1	muslimský
chalífové	chalífa	k1gMnPc1	chalífa
a	a	k8xC	a
sultáni	sultán	k1gMnPc1	sultán
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Abbásovců	Abbásovec	k1gMnPc2	Abbásovec
patřil	patřit	k5eAaImAgMnS	patřit
velký	velký	k2eAgMnSc1d1	velký
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
v	v	k7c6	v
mezopotámském	mezopotámský	k2eAgNnSc6d1	mezopotámský
městě	město	k1gNnSc6	město
Samarra	Samarro	k1gNnSc2	Samarro
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
zvěřince	zvěřinec	k1gMnPc4	zvěřinec
měli	mít	k5eAaImAgMnP	mít
také	také	k6eAd1	také
čínští	čínský	k2eAgMnPc1d1	čínský
a	a	k8xC	a
japonští	japonský	k2eAgMnPc1d1	japonský
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
spíš	spíš	k9	spíš
chovu	chov	k1gInSc3	chov
jelenů	jelen	k1gMnPc2	jelen
a	a	k8xC	a
okrasných	okrasný	k2eAgMnPc2d1	okrasný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
parku	park	k1gInSc6	park
čínských	čínský	k2eAgMnPc2d1	čínský
císařů	císař	k1gMnPc2	císař
byli	být	k5eAaImAgMnP	být
chováni	chován	k2eAgMnPc1d1	chován
jeleni	jelen	k1gMnPc1	jelen
milu	milu	k6eAd1	milu
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
admirálem	admirál	k1gMnSc7	admirál
Čeng	Čeng	k1gInSc1	Čeng
Che	che	k0	che
dovezena	dovézt	k5eAaPmNgFnS	dovézt
první	první	k4xOgFnSc1	první
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
chovali	chovat	k5eAaImAgMnP	chovat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zejména	zejména	k9	zejména
dravé	dravý	k2eAgMnPc4d1	dravý
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
medvědy	medvěd	k1gMnPc4	medvěd
a	a	k8xC	a
lvy	lev	k1gMnPc4	lev
angličtí	anglický	k2eAgMnPc1d1	anglický
<g/>
,	,	kIx,	,
francouzští	francouzský	k2eAgMnPc1d1	francouzský
i	i	k8xC	i
římskoněmečtí	římskoněmecký	k2eAgMnPc1d1	římskoněmecký
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
papežové	papež	k1gMnPc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgMnPc7d1	známý
milovníky	milovník	k1gMnPc7	milovník
zvířat	zvíře	k1gNnPc2	zvíře
byli	být	k5eAaImAgMnP	být
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
Vilém	Vilém	k1gMnSc1	Vilém
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
či	či	k8xC	či
Eduard	Eduard	k1gMnSc1	Eduard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
choval	chovat	k5eAaImAgMnS	chovat
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
ve	v	k7c6	v
zvěřincích	zvěřinec	k1gMnPc6	zvěřinec
v	v	k7c6	v
Luceře	Lucera	k1gFnSc6	Lucera
a	a	k8xC	a
Palermu	Palermo	k1gNnSc6	Palermo
např.	např.	kA	např.
velbloudy	velbloud	k1gMnPc4	velbloud
<g/>
,	,	kIx,	,
slony	slon	k1gMnPc4	slon
<g/>
,	,	kIx,	,
žirafu	žirafa	k1gFnSc4	žirafa
nebo	nebo	k8xC	nebo
ledního	lední	k2eAgMnSc4d1	lední
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1234	[number]	k4	1234
císař	císař	k1gMnSc1	císař
vypravil	vypravit	k5eAaPmAgMnS	vypravit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
potrestat	potrestat	k5eAaPmF	potrestat
odbojného	odbojný	k2eAgMnSc4d1	odbojný
syna	syn	k1gMnSc4	syn
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
ve	v	k7c6	v
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
průvodu	průvod	k1gInSc6	průvod
německými	německý	k2eAgFnPc7d1	německá
městy	město	k1gNnPc7	město
vést	vést	k5eAaImF	vést
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
včetně	včetně	k7c2	včetně
žirafy	žirafa	k1gFnSc2	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
lvy	lev	k1gInPc7	lev
chovali	chovat	k5eAaImAgMnP	chovat
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
lvech	lev	k1gInPc6	lev
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1280	[number]	k4	1280
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
benátský	benátský	k2eAgMnSc1d1	benátský
dóže	dóže	k1gMnSc1	dóže
upomíná	upomínat	k5eAaImIp3nS	upomínat
královnu	královna	k1gFnSc4	královna
Kunhutu	Kunhuta	k1gFnSc4	Kunhuta
o	o	k7c4	o
zaplacení	zaplacení	k1gNnSc4	zaplacení
dvou	dva	k4xCgInPc2	dva
lvů	lev	k1gMnPc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
lvů	lev	k1gMnPc2	lev
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
choval	chovat	k5eAaImAgMnS	chovat
kočkovité	kočkovití	k1gMnPc4	kočkovití
šelmy	šelma	k1gMnPc4	šelma
<g/>
,	,	kIx,	,
medvědy	medvěd	k1gMnPc4	medvěd
i	i	k8xC	i
dravé	dravý	k2eAgMnPc4d1	dravý
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
choval	chovat	k5eAaImAgMnS	chovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
bílého	bílý	k1gMnSc2	bílý
kakadu	kakadu	k1gMnSc2	kakadu
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
darem	dar	k1gInSc7	dar
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
Lví	lví	k2eAgInSc1d1	lví
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Stromovky	Stromovka	k1gFnSc2	Stromovka
či	či	k8xC	či
Jeleního	jelení	k2eAgInSc2d1	jelení
příkopu	příkop	k1gInSc2	příkop
byli	být	k5eAaImAgMnP	být
chováni	chován	k2eAgMnPc1d1	chován
různí	různý	k2eAgMnPc1d1	různý
kopytníci	kopytník	k1gMnPc1	kopytník
či	či	k8xC	či
exotičtí	exotický	k2eAgMnPc1d1	exotický
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
dále	daleko	k6eAd2	daleko
rozšiřovali	rozšiřovat	k5eAaImAgMnP	rozšiřovat
<g/>
,	,	kIx,	,
za	za	k7c2	za
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgInP	být
chovány	chovat	k5eAaImNgInP	chovat
i	i	k8xC	i
opice	opice	k1gFnPc1	opice
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
i	i	k9	i
dronte	dront	k1gInSc5	dront
mauricijský	mauricijský	k2eAgInSc4d1	mauricijský
<g/>
.	.	kIx.	.
</s>
<s>
Zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
přežil	přežít	k5eAaPmAgMnS	přežít
třicetiletou	třicetiletý	k2eAgFnSc4d1	třicetiletá
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
až	až	k6eAd1	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
menažerie	menažerie	k1gFnSc1	menažerie
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgNnSc1d2	pozdější
zoo	zoo	k1gNnSc1	zoo
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zámořskými	zámořský	k2eAgFnPc7d1	zámořská
plavbami	plavba	k1gFnPc7	plavba
dovážena	dovážen	k2eAgNnPc4d1	dováženo
zvířata	zvíře	k1gNnPc4	zvíře
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
i	i	k8xC	i
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zvěřince	zvěřinka	k1gFnSc3	zvěřinka
byly	být	k5eAaImAgFnP	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
panovnických	panovnický	k2eAgFnPc2d1	panovnická
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
po	po	k7c4	po
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
a	a	k8xC	a
Lisabon	Lisabon	k1gInSc4	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Chovali	chovat	k5eAaImAgMnP	chovat
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
lvi	lev	k1gMnPc1	lev
a	a	k8xC	a
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
nebo	nebo	k8xC	nebo
sloni	slon	k1gMnPc1	slon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvěřincích	zvěřinec	k1gMnPc6	zvěřinec
byly	být	k5eAaImAgFnP	být
předváděny	předvádět	k5eAaImNgInP	předvádět
i	i	k9	i
souboje	souboj	k1gInPc1	souboj
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
lvů	lev	k1gMnPc2	lev
a	a	k8xC	a
medvědů	medvěd	k1gMnPc2	medvěd
s	s	k7c7	s
velkými	velký	k2eAgMnPc7d1	velký
psy	pes	k1gMnPc7	pes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
např.	např.	kA	např.
slona	slon	k1gMnSc2	slon
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
(	(	kIx(	(
<g/>
poč	poč	k?	poč
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
či	či	k8xC	či
nosorožcem	nosorožec	k1gMnSc7	nosorožec
(	(	kIx(	(
<g/>
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
většinou	většinou	k6eAd1	většinou
nekončily	končit	k5eNaImAgInP	končit
zabitím	zabití	k1gNnSc7	zabití
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jejich	jejich	k3xOp3gFnSc2	jejich
vysoké	vysoký	k2eAgFnSc2d1	vysoká
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
obtížné	obtížný	k2eAgFnSc2d1	obtížná
dostupnosti	dostupnost	k1gFnSc2	dostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
či	či	k8xC	či
medvědi	medvěd	k1gMnPc1	medvěd
také	také	k9	také
defilovali	defilovat	k5eAaImAgMnP	defilovat
před	před	k7c7	před
panovníkem	panovník	k1gMnSc7	panovník
nebo	nebo	k8xC	nebo
předváděli	předvádět	k5eAaImAgMnP	předvádět
různé	různý	k2eAgInPc4d1	různý
kousky	kousek	k1gInPc4	kousek
<g/>
,	,	kIx,	,
právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ubikace	ubikace	k1gFnSc1	ubikace
zvěřinců	zvěřinec	k1gMnPc2	zvěřinec
nazývaly	nazývat	k5eAaImAgInP	nazývat
zwingery	zwingera	k1gFnSc2	zwingera
(	(	kIx(	(
<g/>
od	od	k7c2	od
něm.	něm.	k?	něm.
slovesa	sloveso	k1gNnSc2	sloveso
zwingen	zwingen	k1gInSc4	zwingen
-	-	kIx~	-
"	"	kIx"	"
<g/>
nutit	nutit	k5eAaImF	nutit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
zvířecí	zvířecí	k2eAgInPc4d1	zvířecí
zápasy	zápas	k1gInPc4	zápas
zakázala	zakázat	k5eAaPmAgFnS	zakázat
na	na	k7c4	na
území	území	k1gNnSc4	území
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
až	až	k8xS	až
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
či	či	k8xC	či
Rusku	Rusko	k1gNnSc6	Rusko
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
přežila	přežít	k5eAaPmAgFnS	přežít
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dovážely	dovážet	k5eAaImAgInP	dovážet
se	se	k3xPyFc4	se
také	také	k9	také
opice	opice	k1gFnSc1	opice
<g/>
,	,	kIx,	,
papoušci	papoušek	k1gMnPc1	papoušek
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Lisabonu	Lisabon	k1gInSc2	Lisabon
dovezen	dovezen	k2eAgMnSc1d1	dovezen
nosorožec	nosorožec	k1gMnSc1	nosorožec
indický	indický	k2eAgMnSc1d1	indický
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
zebra	zebra	k1gFnSc1	zebra
<g/>
,	,	kIx,	,
do	do	k7c2	do
Ferrary	Ferrara	k1gFnSc2	Ferrara
tygr	tygr	k1gMnSc1	tygr
<g/>
,	,	kIx,	,
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
krokodýl	krokodýl	k1gMnSc1	krokodýl
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
a	a	k8xC	a
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
do	do	k7c2	do
Pisy	Pisa	k1gFnPc4	Pisa
čínští	čínský	k2eAgMnPc1d1	čínský
bažanti	bažant	k1gMnPc1	bažant
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol	pola	k1gFnPc2	pola
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jeden	jeden	k4xCgMnSc1	jeden
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
nebo	nebo	k8xC	nebo
habsburský	habsburský	k2eAgMnSc1d1	habsburský
císař	císař	k1gMnSc1	císař
Leopold	Leopolda	k1gFnPc2	Leopolda
I.	I.	kA	I.
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
chovali	chovat	k5eAaImAgMnP	chovat
gepardy	gepard	k1gMnPc7	gepard
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložen	k2eAgInSc4d1	doložen
termín	termín	k1gInSc4	termín
Tiergarten	Tiergarten	k2eAgInSc4d1	Tiergarten
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
pro	pro	k7c4	pro
ZOO	zoo	k1gFnSc4	zoo
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
hrabat	hrabě	k1gNnPc2	hrabě
württemberských	württemberský	k2eAgNnPc2d1	württemberský
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
panovníků	panovník	k1gMnPc2	panovník
budovali	budovat	k5eAaImAgMnP	budovat
zvěřince	zvěřinka	k1gFnSc3	zvěřinka
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
sídlech	sídlo	k1gNnPc6	sídlo
bohatí	bohatit	k5eAaImIp3nP	bohatit
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Obyvateli	obyvatel	k1gMnPc7	obyvatel
hradních	hradní	k2eAgInPc2d1	hradní
příkopů	příkop	k1gInPc2	příkop
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
choval	chovat	k5eAaImAgMnS	chovat
jihočeský	jihočeský	k2eAgMnSc1d1	jihočeský
magnát	magnát	k1gMnSc1	magnát
Půta	Půt	k1gInSc2	Půt
Švihovský	Švihovský	k2eAgInSc4d1	Švihovský
z	z	k7c2	z
Rýznburka	Rýznburek	k1gMnSc2	Rýznburek
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Rabí	rabí	k1gMnSc1	rabí
opici	opice	k1gFnSc4	opice
<g/>
,	,	kIx,	,
snad	snad	k9	snad
paviána	pavián	k1gMnSc2	pavián
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
rozvoj	rozvoj	k1gInSc1	rozvoj
budování	budování	k1gNnSc2	budování
obor	obora	k1gFnPc2	obora
<g/>
,	,	kIx,	,
bažantnic	bažantnice	k1gFnPc2	bažantnice
a	a	k8xC	a
zámeckých	zámecký	k2eAgInPc2d1	zámecký
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
bažanti	bažant	k1gMnPc1	bažant
<g/>
,	,	kIx,	,
pávi	páv	k1gMnPc1	páv
<g/>
,	,	kIx,	,
okrasné	okrasný	k2eAgFnPc1d1	okrasná
kachny	kachna	k1gFnPc1	kachna
a	a	k8xC	a
husy	husa	k1gFnPc1	husa
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
jeleni	jelen	k1gMnPc1	jelen
nebo	nebo	k8xC	nebo
cizokrajná	cizokrajný	k2eAgNnPc1d1	cizokrajné
plemena	plemeno	k1gNnPc1	plemeno
domácích	domácí	k2eAgFnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
okrasných	okrasný	k2eAgNnPc2d1	okrasné
i	i	k8xC	i
užitkových	užitkový	k2eAgNnPc2d1	užitkové
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označovala	označovat	k5eAaImAgFnS	označovat
francouzským	francouzský	k2eAgInSc7d1	francouzský
termínem	termín	k1gInSc7	termín
la	la	k1gNnSc2	la
menageérie	menageérie	k1gFnSc2	menageérie
"	"	kIx"	"
<g/>
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
šlechticů	šlechtic	k1gMnPc2	šlechtic
je	být	k5eAaImIp3nS	být
zřizovaly	zřizovat	k5eAaImAgInP	zřizovat
také	také	k9	také
kláštery	klášter	k1gInPc1	klášter
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
panovnických	panovnický	k2eAgMnPc2d1	panovnický
zvěřinců	zvěřinec	k1gMnPc2	zvěřinec
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
nebyla	být	k5eNaImAgFnS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
zvěřinec	zvěřinec	k1gMnSc1	zvěřinec
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Toweru	Towero	k1gNnSc6	Towero
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jej	on	k3xPp3gMnSc4	on
mohl	moct	k5eAaImAgInS	moct
navštívit	navštívit	k5eAaPmF	navštívit
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesl	přinést	k5eAaPmAgMnS	přinést
slepici	slepice	k1gFnSc4	slepice
<g/>
,	,	kIx,	,
psa	pes	k1gMnSc4	pes
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
zvíře	zvíře	k1gNnSc4	zvíře
jako	jako	k8xC	jako
krmení	krmení	k1gNnSc4	krmení
pro	pro	k7c4	pro
šelmy	šelma	k1gMnPc4	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc2	veřejnost
byla	být	k5eAaImAgNnP	být
zvířata	zvíře	k1gNnPc1	zvíře
předváděna	předváděn	k2eAgNnPc1d1	předváděno
v	v	k7c6	v
kočovných	kočovný	k2eAgMnPc6d1	kočovný
zvěřincích	zvěřinec	k1gMnPc6	zvěřinec
a	a	k8xC	a
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
v	v	k7c6	v
cirkusech	cirkus	k1gInPc6	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
měla	mít	k5eAaImAgNnP	mít
zábavní	zábavní	k2eAgInSc4d1	zábavní
a	a	k8xC	a
komerční	komerční	k2eAgInSc4d1	komerční
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
předváděla	předvádět	k5eAaImAgFnS	předvádět
nejen	nejen	k6eAd1	nejen
exotická	exotický	k2eAgNnPc4d1	exotické
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
kuriozity	kuriozita	k1gFnPc4	kuriozita
<g/>
,	,	kIx,	,
abnormality	abnormalita	k1gFnPc4	abnormalita
a	a	k8xC	a
zrůdy	zrůda	k1gFnPc4	zrůda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
srostlé	srostlý	k2eAgMnPc4d1	srostlý
jedince	jedinec	k1gMnPc4	jedinec
domácích	domácí	k1gFnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
s	s	k7c7	s
nadbytečnými	nadbytečný	k2eAgFnPc7d1	nadbytečná
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
též	též	k9	též
trpaslíky	trpaslík	k1gMnPc4	trpaslík
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
postižené	postižený	k2eAgMnPc4d1	postižený
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
byl	být	k5eAaImAgInS	být
barvitý	barvitý	k2eAgInSc1d1	barvitý
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
skandálními	skandální	k2eAgFnPc7d1	skandální
podrobnostmi	podrobnost	k1gFnPc7	podrobnost
<g/>
,	,	kIx,	,
zvěřince	zvěřinec	k1gMnPc4	zvěřinec
předváděly	předvádět	k5eAaImAgFnP	předvádět
lidožravé	lidožravý	k2eAgMnPc4d1	lidožravý
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
třistaleté	třistaletý	k2eAgMnPc4d1	třistaletý
krokodýly	krokodýl	k1gMnPc4	krokodýl
<g/>
,	,	kIx,	,
dvacetimetrové	dvacetimetrový	k2eAgMnPc4d1	dvacetimetrový
hroznýše	hroznýš	k1gMnPc4	hroznýš
<g/>
,	,	kIx,	,
mumifikované	mumifikovaný	k2eAgFnPc1d1	mumifikovaná
mořské	mořský	k2eAgFnPc1d1	mořská
panny	panna	k1gFnPc1	panna
<g/>
,	,	kIx,	,
neškodní	škodní	k2eNgMnPc1d1	neškodní
kaloni	kaloň	k1gMnPc1	kaloň
byli	být	k5eAaImAgMnP	být
vydáváni	vydáván	k2eAgMnPc1d1	vydáván
za	za	k7c7	za
upíry	upír	k1gMnPc7	upír
atd.	atd.	kA	atd.
Životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
zařízeních	zařízení	k1gNnPc6	zařízení
byly	být	k5eAaImAgFnP	být
otřesné	otřesný	k2eAgFnPc1d1	otřesná
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
byla	být	k5eAaImAgFnS	být
chována	chován	k2eAgFnSc1d1	chována
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
pojízdných	pojízdný	k2eAgFnPc6d1	pojízdná
klecích	klec	k1gFnPc6	klec
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
přivázaná	přivázaný	k2eAgFnSc1d1	přivázaná
na	na	k7c6	na
řetěze	řetěz	k1gInSc6	řetěz
a	a	k8xC	a
neodborně	odborně	k6eNd1	odborně
krmena	krmit	k5eAaImNgFnS	krmit
různými	různý	k2eAgInPc7d1	různý
zbytky	zbytek	k1gInPc7	zbytek
a	a	k8xC	a
odpadky	odpadek	k1gInPc7	odpadek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
medvědářích	medvědář	k1gMnPc6	medvědář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
předváděli	předvádět	k5eAaImAgMnP	předvádět
ochočené	ochočený	k2eAgFnPc4d1	ochočená
"	"	kIx"	"
<g/>
tancující	tancující	k2eAgFnPc4d1	tancující
<g/>
"	"	kIx"	"
medvědy	medvěd	k1gMnPc7	medvěd
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
opice	opice	k1gFnSc2	opice
či	či	k8xC	či
velbloudy	velbloud	k1gMnPc7	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
nebo	nebo	k8xC	nebo
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
medvědi	medvěd	k1gMnPc1	medvěd
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
metody	metoda	k1gFnPc4	metoda
drezury	drezura	k1gFnSc2	drezura
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
týrání	týrání	k1gNnSc2	týrání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
zoologickou	zoologický	k2eAgFnSc4d1	zoologická
zahradu	zahrada	k1gFnSc4	zahrada
je	být	k5eAaImIp3nS	být
odborníky	odborník	k1gMnPc4	odborník
považována	považován	k2eAgFnSc1d1	považována
zoo	zoo	k1gFnSc1	zoo
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1752	[number]	k4	1752
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
však	však	k9	však
byla	být	k5eAaImAgFnS	být
určená	určený	k2eAgFnSc1d1	určená
především	především	k9	především
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
habsburského	habsburský	k2eAgInSc2d1	habsburský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
zoo	zoo	k1gNnSc3	zoo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Konventu	konvent	k1gInSc2	konvent
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
Jardin	Jardin	k2eAgInSc4d1	Jardin
des	des	k1gNnSc4	des
plantes	plantesa	k1gFnPc2	plantesa
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgNnP	být
přivezena	přivezen	k2eAgNnPc1d1	přivezeno
zvířata	zvíře	k1gNnPc1	zvíře
z	z	k7c2	z
královského	královský	k2eAgMnSc2d1	královský
zvěřince	zvěřinec	k1gMnSc2	zvěřinec
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
pro	pro	k7c4	pro
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
londýnskou	londýnský	k2eAgFnSc4d1	londýnská
zoo	zoo	k1gFnSc4	zoo
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
v	v	k7c6	v
Regent	regent	k1gMnSc1	regent
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
a	a	k8xC	a
populární	populární	k2eAgFnSc1d1	populární
zkratka	zkratka	k1gFnSc1	zkratka
ZOO	zoo	k1gFnSc2	zoo
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
založeno	založit	k5eAaPmNgNnS	založit
buďto	buďto	k8xC	buďto
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
bohatých	bohatý	k2eAgMnPc2d1	bohatý
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
,	,	kIx,	,
spolky	spolek	k1gInPc1	spolek
nadšenců	nadšenec	k1gMnPc2	nadšenec
a	a	k8xC	a
milovníků	milovník	k1gMnPc2	milovník
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
hostinskými	hostinská	k1gFnPc7	hostinská
z	z	k7c2	z
reklamních	reklamní	k2eAgInPc2d1	reklamní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zřízení	zřízení	k1gNnSc1	zřízení
zoo	zoo	k1gNnSc2	zoo
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
sponzoroval	sponzorovat	k5eAaImAgMnS	sponzorovat
filozof	filozof	k1gMnSc1	filozof
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
zahradu	zahrada	k1gFnSc4	zahrada
ve	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
založil	založit	k5eAaPmAgInS	založit
spolek	spolek	k1gInSc1	spolek
chovatelů	chovatel	k1gMnPc2	chovatel
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
zoo	zoo	k1gFnSc1	zoo
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
reklama	reklama	k1gFnSc1	reklama
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
hostinci	hostinec	k1gInSc6	hostinec
atd.	atd.	kA	atd.
Pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
byly	být	k5eAaImAgFnP	být
zoologické	zoologický	k2eAgFnPc4d1	zoologická
zahrady	zahrada	k1gFnPc4	zahrada
zakládány	zakládán	k2eAgFnPc4d1	zakládána
vědci	vědec	k1gMnPc7	vědec
jako	jako	k9	jako
odborné	odborný	k2eAgFnSc2d1	odborná
instituce	instituce	k1gFnSc2	instituce
nebo	nebo	k8xC	nebo
na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
fenoménem	fenomén	k1gInSc7	fenomén
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
aklimatizační	aklimatizační	k2eAgFnSc2d1	aklimatizační
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgInP	provádět
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
aklimatizací	aklimatizace	k1gFnSc7	aklimatizace
cizokrajných	cizokrajný	k2eAgInPc2d1	cizokrajný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
tropických	tropický	k2eAgMnPc2d1	tropický
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgInP	podobat
oborám	obora	k1gFnPc3	obora
nebo	nebo	k8xC	nebo
pozdějším	pozdní	k2eAgFnPc3d2	pozdější
safari	safari	k1gNnSc3	safari
parkům	park	k1gInPc3	park
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
byla	být	k5eAaImAgNnP	být
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kopytníci	kopytník	k1gMnPc1	kopytník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
např.	např.	kA	např.
pštrosi	pštros	k1gMnPc1	pštros
či	či	k8xC	či
klokani	klokan	k1gMnPc1	klokan
<g/>
,	,	kIx,	,
chována	chovat	k5eAaImNgFnS	chovat
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgFnSc1d1	vynikající
úroveň	úroveň	k1gFnSc1	úroveň
měla	mít	k5eAaImAgFnS	mít
např.	např.	kA	např.
zahrada	zahrada	k1gFnSc1	zahrada
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Derby	derby	k1gNnSc2	derby
v	v	k7c4	v
Knowsley	Knowslea	k1gFnPc4	Knowslea
u	u	k7c2	u
Liverpoolu	Liverpool	k1gInSc2	Liverpool
nebo	nebo	k8xC	nebo
park	park	k1gInSc1	park
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
Woburn	Woburna	k1gFnPc2	Woburna
Abbey	Abbea	k1gFnSc2	Abbea
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sehrál	sehrát	k5eAaPmAgInS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
záchraně	záchrana	k1gFnSc6	záchrana
jelenů	jelen	k1gMnPc2	jelen
milu	milus	k1gInSc2	milus
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
chovu	chov	k1gInSc2	chov
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
zoo	zoo	k1gFnPc6	zoo
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
neodlišovaly	odlišovat	k5eNaImAgFnP	odlišovat
od	od	k7c2	od
zvěřinců	zvěřinec	k1gMnPc2	zvěřinec
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
byla	být	k5eAaImAgNnP	být
chována	chovat	k5eAaImNgNnP	chovat
v	v	k7c6	v
klecích	klec	k1gFnPc6	klec
nebo	nebo	k8xC	nebo
úzkých	úzký	k2eAgInPc6d1	úzký
výbězích	výběh	k1gInPc6	výběh
zvaných	zvaný	k2eAgFnPc2d1	zvaná
zwingery	zwingera	k1gFnSc2	zwingera
<g/>
.	.	kIx.	.
</s>
<s>
Ubikace	ubikace	k1gFnPc1	ubikace
byly	být	k5eAaImAgFnP	být
řazeny	řadit	k5eAaImNgFnP	řadit
systematicky	systematicky	k6eAd1	systematicky
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
patrech	patro	k1gNnPc6	patro
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
móda	móda	k1gFnSc1	móda
chovu	chov	k1gInSc2	chov
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
ubikacích	ubikace	k1gFnPc6	ubikace
napodobujících	napodobující	k2eAgFnPc6d1	napodobující
exotické	exotický	k2eAgNnSc4d1	exotické
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sloni	slon	k1gMnPc1	slon
byli	být	k5eAaImAgMnP	být
chováni	chován	k2eAgMnPc1d1	chován
v	v	k7c6	v
indických	indický	k2eAgFnPc6d1	indická
pagodách	pagoda	k1gFnPc6	pagoda
<g/>
,	,	kIx,	,
velbloudi	velbloud	k1gMnPc1	velbloud
v	v	k7c6	v
mešitách	mešita	k1gFnPc6	mešita
<g/>
,	,	kIx,	,
žirafy	žirafa	k1gFnPc1	žirafa
ve	v	k7c6	v
staroegyptských	staroegyptský	k2eAgInPc6d1	staroegyptský
chrámech	chrám	k1gInPc6	chrám
atd.	atd.	kA	atd.
Podobné	podobný	k2eAgFnPc4d1	podobná
budovy	budova	k1gFnPc4	budova
lze	lze	k6eAd1	lze
dodnes	dodnes	k6eAd1	dodnes
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
,	,	kIx,	,
Wroclawi	Wroclawi	k1gNnSc6	Wroclawi
či	či	k8xC	či
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
zoo	zoo	k1gFnPc6	zoo
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
např.	např.	kA	např.
výběhy	výběh	k1gInPc4	výběh
bizonů	bizon	k1gMnPc2	bizon
vyzdobeny	vyzdoben	k2eAgInPc4d1	vyzdoben
indiánskými	indiánský	k2eAgInPc7d1	indiánský
totemy	totem	k1gInPc7	totem
<g/>
,	,	kIx,	,
budují	budovat	k5eAaImIp3nP	budovat
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
africké	africký	k2eAgFnPc1d1	africká
vesničky	vesnička	k1gFnPc1	vesnička
či	či	k8xC	či
japonské	japonský	k2eAgFnPc1d1	japonská
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
atraktivní	atraktivní	k2eAgInPc1d1	atraktivní
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
spíš	spíš	k9	spíš
o	o	k7c4	o
dekorace	dekorace	k1gFnPc4	dekorace
než	než	k8xS	než
zařízení	zařízení	k1gNnPc1	zařízení
určená	určený	k2eAgNnPc1d1	určené
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
chovu	chov	k1gInSc2	chov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
bývali	bývat	k5eAaImAgMnP	bývat
jako	jako	k8xS	jako
atrakce	atrakce	k1gFnPc1	atrakce
vystavováni	vystavovat	k5eAaImNgMnP	vystavovat
také	také	k9	také
domorodci	domorodec	k1gMnPc1	domorodec
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
či	či	k8xC	či
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
folklóru	folklór	k1gInSc2	folklór
a	a	k8xC	a
tradičního	tradiční	k2eAgInSc2d1	tradiční
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vzácná	vzácný	k2eAgNnPc1d1	vzácné
a	a	k8xC	a
exotická	exotický	k2eAgNnPc1d1	exotické
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgNnP	těšit
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
přízni	přízeň	k1gFnSc6	přízeň
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Úplnou	úplný	k2eAgFnSc4d1	úplná
senzaci	senzace	k1gFnSc4	senzace
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
žirafa	žirafa	k1gFnSc1	žirafa
jménem	jméno	k1gNnSc7	jméno
Zarafa	Zaraf	k1gMnSc2	Zaraf
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
věnoval	věnovat	k5eAaImAgMnS	věnovat
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
egyptský	egyptský	k2eAgInSc1d1	egyptský
místokrál	místokrál	k1gMnSc1	místokrál
Muhammad	Muhammad	k1gInSc1	Muhammad
Alí	Alí	k1gMnSc1	Alí
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
Karlu	Karla	k1gFnSc4	Karla
X.	X.	kA	X.
<g/>
.	.	kIx.	.
</s>
<s>
Vlna	vlna	k1gFnSc1	vlna
obdivu	obdiv	k1gInSc2	obdiv
a	a	k8xC	a
nadšení	nadšení	k1gNnSc2	nadšení
ze	z	k7c2	z
žirafy	žirafa	k1gFnSc2	žirafa
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
odrazila	odrazit	k5eAaPmAgFnS	odrazit
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
démské	démský	k2eAgFnSc6d1	démský
módě	móda	k1gFnSc6	móda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
slavný	slavný	k2eAgMnSc1d1	slavný
byl	být	k5eAaImAgMnS	být
hroch	hroch	k1gMnSc1	hroch
Obaysch	Obaysch	k1gMnSc1	Obaysch
<g/>
,	,	kIx,	,
dovezený	dovezený	k2eAgInSc1d1	dovezený
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
obdivovaný	obdivovaný	k2eAgInSc1d1	obdivovaný
královnou	královna	k1gFnSc7	královna
Viktorií	Viktoria	k1gFnPc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnSc4d1	revoluční
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
koncepci	koncepce	k1gFnSc6	koncepce
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
zavedl	zavést	k5eAaPmAgMnS	zavést
Karl	Karl	k1gMnSc1	Karl
Hagenbeck	Hagenbeck	k1gMnSc1	Hagenbeck
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
exotickými	exotický	k2eAgNnPc7d1	exotické
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
milovník	milovník	k1gMnSc1	milovník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
při	při	k7c6	při
zakládání	zakládání	k1gNnSc6	zakládání
nové	nový	k2eAgFnSc2d1	nová
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
ve	v	k7c6	v
Stellingenu	Stellingen	k1gInSc6	Stellingen
u	u	k7c2	u
Hamburku	Hamburk	k1gInSc2	Hamburk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
započal	započnout	k5eAaPmAgInS	započnout
s	s	k7c7	s
chovem	chov	k1gInSc7	chov
zvířat	zvíře	k1gNnPc2	zvíře
ve	v	k7c6	v
volných	volný	k2eAgInPc6d1	volný
výbězích	výběh	k1gInPc6	výběh
<g/>
,	,	kIx,	,
ohrazených	ohrazený	k2eAgFnPc6d1	ohrazená
pouze	pouze	k6eAd1	pouze
příkopy	příkop	k1gInPc7	příkop
<g/>
,	,	kIx,	,
výstavbou	výstavba	k1gFnSc7	výstavba
umělých	umělý	k2eAgFnPc2d1	umělá
skal	skála	k1gFnPc2	skála
pro	pro	k7c4	pro
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
kozorožce	kozorožec	k1gMnPc4	kozorožec
či	či	k8xC	či
paviány	pavián	k1gMnPc4	pavián
a	a	k8xC	a
budováním	budování	k1gNnSc7	budování
velkých	velký	k2eAgFnPc2d1	velká
voliér	voliéra	k1gFnPc2	voliéra
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Hagenbeck	Hagenbeck	k1gMnSc1	Hagenbeck
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
výstavbu	výstavba	k1gFnSc4	výstavba
podobných	podobný	k2eAgInPc2d1	podobný
výběhů	výběh	k1gInPc2	výběh
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
či	či	k8xC	či
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
zoo	zoo	k1gFnSc6	zoo
je	být	k5eAaImIp3nS	být
památkou	památka	k1gFnSc7	památka
na	na	k7c4	na
Hagenbecka	Hagenbecko	k1gNnPc4	Hagenbecko
výběh	výběh	k1gInSc1	výběh
pand	panda	k1gFnPc2	panda
červených	červená	k1gFnPc2	červená
umístěný	umístěný	k2eAgInSc4d1	umístěný
naproti	naproti	k7c3	naproti
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
vchodu	vchod	k1gInSc3	vchod
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
určený	určený	k2eAgMnSc1d1	určený
pro	pro	k7c4	pro
vlky	vlk	k1gMnPc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
USA	USA	kA	USA
zřizovány	zřizován	k2eAgInPc1d1	zřizován
safari	safari	k1gNnPc2	safari
parky	park	k1gInPc7	park
<g/>
,	,	kIx,	,
chovající	chovající	k2eAgNnPc4d1	chovající
zvířata	zvíře	k1gNnPc4	zvíře
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
společných	společný	k2eAgInPc6d1	společný
výbězích	výběh	k1gInPc6	výběh
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgFnPc1d1	nabízející
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
bezprostřední	bezprostřední	k2eAgInSc4d1	bezprostřední
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohou	moct	k5eAaImIp3nP	moct
přes	přes	k7c4	přes
prostor	prostor	k1gInSc4	prostor
safari	safari	k1gNnSc4	safari
projíždět	projíždět	k5eAaImF	projíždět
buďto	buďto	k8xC	buďto
svými	svůj	k3xOyFgInPc7	svůj
vlastními	vlastní	k2eAgInPc7d1	vlastní
automobily	automobil	k1gInPc7	automobil
nebo	nebo	k8xC	nebo
specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
(	(	kIx(	(
<g/>
autobus	autobus	k1gInSc1	autobus
<g/>
,	,	kIx,	,
vláček	vláček	k1gInSc1	vláček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hluk	hluk	k1gInSc1	hluk
automobilových	automobilový	k2eAgInPc2d1	automobilový
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
výfukové	výfukový	k2eAgInPc1d1	výfukový
plyny	plyn	k1gInPc1	plyn
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
zdraví	zdraví	k1gNnSc4	zdraví
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
bezpečnostního	bezpečnostní	k2eAgNnSc2d1	bezpečnostní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
využití	využití	k1gNnSc1	využití
specializovaných	specializovaný	k2eAgNnPc2d1	specializované
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
zvlášť	zvlášť	k6eAd1	zvlášť
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
osvědčují	osvědčovat	k5eAaImIp3nP	osvědčovat
elektromobily	elektromobila	k1gFnPc1	elektromobila
<g/>
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgInSc7d1	obdobný
trendem	trend	k1gInSc7	trend
jsou	být	k5eAaImIp3nP	být
průchozí	průchozí	k2eAgFnPc4d1	průchozí
expozice	expozice	k1gFnPc4	expozice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
procházet	procházet	k5eAaImF	procházet
přímo	přímo	k6eAd1	přímo
prostorem	prostor	k1gInSc7	prostor
výběhu	výběh	k1gInSc2	výběh
nebo	nebo	k8xC	nebo
voliéry	voliéra	k1gFnSc2	voliéra
<g/>
.	.	kIx.	.
</s>
<s>
Průchozí	průchozí	k2eAgFnPc1d1	průchozí
expozice	expozice	k1gFnPc1	expozice
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
atraktivní	atraktivní	k2eAgMnPc1d1	atraktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
riskantní	riskantní	k2eAgMnSc1d1	riskantní
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
návštěvníků	návštěvník	k1gMnPc2	návštěvník
i	i	k8xC	i
chovaných	chovaný	k2eAgNnPc2d1	chované
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
chovat	chovat	k5eAaImF	chovat
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
proto	proto	k8xC	proto
hlavně	hlavně	k6eAd1	hlavně
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
např.	např.	kA	např.
klokani	klokan	k1gMnPc1	klokan
<g/>
,	,	kIx,	,
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
mara	mara	k6eAd1	mara
stepní	stepní	k2eAgFnSc1d1	stepní
<g/>
,	,	kIx,	,
psoun	psoun	k1gMnSc1	psoun
prériový	prériový	k2eAgMnSc1d1	prériový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letouni	letoun	k1gMnPc1	letoun
(	(	kIx(	(
<g/>
noční	noční	k2eAgInPc1d1	noční
pavilony	pavilon	k1gInPc1	pavilon
netopýrů	netopýr	k1gMnPc2	netopýr
a	a	k8xC	a
kaloňů	kaloň	k1gMnPc2	kaloň
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
drobné	drobný	k2eAgFnPc1d1	drobná
šelmy	šelma	k1gFnPc1	šelma
(	(	kIx(	(
<g/>
mangusty	mangusta	k1gFnPc1	mangusta
<g/>
,	,	kIx,	,
surikaty	surikata	k1gFnPc1	surikata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
do	do	k7c2	do
podobných	podobný	k2eAgFnPc2d1	podobná
ubikací	ubikace	k1gFnPc2	ubikace
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
i	i	k9	i
opice	opice	k1gFnPc1	opice
či	či	k8xC	či
lemuři	lemur	k1gMnPc1	lemur
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
rizik	riziko	k1gNnPc2	riziko
(	(	kIx(	(
<g/>
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
pokousání	pokousání	k1gNnSc2	pokousání
<g/>
,	,	kIx,	,
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
přikrmování	přikrmování	k1gNnSc4	přikrmování
"	"	kIx"	"
<g/>
žebrajících	žebrající	k2eAgFnPc2d1	žebrající
<g/>
"	"	kIx"	"
opic	opice	k1gFnPc2	opice
<g/>
,	,	kIx,	,
krádeže	krádež	k1gFnPc4	krádež
předmětů	předmět	k1gInPc2	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
chovaných	chovaný	k2eAgNnPc2d1	chované
zvířat	zvíře	k1gNnPc2	zvíře
před	před	k7c7	před
týráním	týrání	k1gNnSc7	týrání
<g/>
,	,	kIx,	,
krádeží	krádež	k1gFnSc7	krádež
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
,	,	kIx,	,
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
želv	želva	k1gFnPc2	želva
<g/>
)	)	kIx)	)
či	či	k8xC	či
nepovoleným	povolený	k2eNgNnSc7d1	nepovolené
krmením	krmení	k1gNnSc7	krmení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
návštěvníků	návštěvník	k1gMnPc2	návštěvník
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
průchozí	průchozí	k2eAgFnPc1d1	průchozí
expozice	expozice	k1gFnPc1	expozice
opatřeny	opatřen	k2eAgFnPc1d1	opatřena
dozorem	dozor	k1gInSc7	dozor
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
monitorovány	monitorovat	k5eAaImNgInP	monitorovat
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Změnilo	změnit	k5eAaPmAgNnS	změnit
se	se	k3xPyFc4	se
i	i	k9	i
poslání	poslání	k1gNnSc2	poslání
zoologickách	zoologicko	k1gNnPc6	zoologicko
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
vystavování	vystavování	k1gNnSc2	vystavování
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
prezentovat	prezentovat	k5eAaBmF	prezentovat
celé	celý	k2eAgFnPc4d1	celá
skupiny	skupina	k1gFnPc4	skupina
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
ubikacích	ubikace	k1gFnPc6	ubikace
<g/>
,	,	kIx,	,
napodobujícch	napodobujícch	k1gInSc1	napodobujícch
jejich	jejich	k3xOp3gInSc2	jejich
přirozené	přirozený	k2eAgNnSc1d1	přirozené
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zábavní	zábavní	k2eAgFnSc2d1	zábavní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
funkce	funkce	k1gFnSc2	funkce
azhrad	azhrad	k1gInSc1	azhrad
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
i	i	k8xC	i
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
záchranu	záchrana	k1gFnSc4	záchrana
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
moderních	moderní	k2eAgFnPc2d1	moderní
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
patří	patřit	k5eAaImIp3nS	patřit
chov	chov	k1gInSc1	chov
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
živočíchů	živočích	k1gInPc2	živočích
a	a	k8xC	a
programy	program	k1gInPc1	program
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
zpětné	zpětný	k2eAgNnSc1d1	zpětné
vysazování	vysazování	k1gNnSc1	vysazování
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
propagátorem	propagátor	k1gMnSc7	propagátor
této	tento	k3xDgFnSc2	tento
úlohy	úloha	k1gFnSc2	úloha
zoo	zoo	k1gFnSc4	zoo
byl	být	k5eAaImAgMnS	být
Gerald	Gerald	k1gMnSc1	Gerald
Durrell	Durrell	k1gMnSc1	Durrell
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
úspěchy	úspěch	k1gInPc4	úspěch
zoologických	zoologický	k2eAgFnPc2d1	zoologická
azhrad	azhrada	k1gFnPc2	azhrada
patří	patřit	k5eAaImIp3nS	patřit
záchrana	záchrana	k1gFnSc1	záchrana
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
vyhubených	vyhubený	k2eAgMnPc2d1	vyhubený
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
jeź	jeź	k?	jeź
se	se	k3xPyFc4	se
podAřilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
jen	jen	k9	jen
díky	díky	k7c3	díky
chovu	chov	k1gInSc3	chov
v	v	k7c6	v
zajeí	zajeí	k1gFnSc6	zajeí
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgMnSc4d1	převalský
<g/>
,	,	kIx,	,
zubr	zubr	k1gMnSc1	zubr
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
milu	milus	k1gInSc2	milus
<g/>
,	,	kIx,	,
přímorožec	přímorožec	k1gMnSc1	přímorožec
arabský	arabský	k2eAgMnSc1d1	arabský
a	a	k8xC	a
šavlorohý	šavlorohý	k2eAgMnSc1d1	šavlorohý
<g/>
,	,	kIx,	,
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
berneška	berneška	k1gFnSc1	berneška
havajská	havajský	k2eAgFnSc1d1	Havajská
<g/>
,	,	kIx,	,
bažant	bažant	k1gMnSc1	bažant
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
<g/>
,	,	kIx,	,
ibis	ibis	k1gMnSc1	ibis
skalní	skalní	k2eAgMnPc1d1	skalní
nebo	nebo	k8xC	nebo
hrdilčka	hrdilčka	k6eAd1	hrdilčka
sokorská	sokorský	k2eAgFnSc1d1	sokorský
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
vedení	vedení	k1gNnSc1	vedení
plemenných	plemenný	k2eAgFnPc2d1	plemenná
knih	kniha	k1gFnPc2	kniha
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc1	zahrada
ovšem	ovšem	k9	ovšem
nadále	nadále	k6eAd1	nadále
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
ke	k	k7c3	k
vzdělávacím	vzdělávací	k2eAgMnPc3d1	vzdělávací
<g/>
,	,	kIx,	,
výzkumným	výzkumný	k2eAgMnPc3d1	výzkumný
<g/>
,	,	kIx,	,
rekreačním	rekreační	k2eAgMnPc3d1	rekreační
a	a	k8xC	a
estetickým	estetický	k2eAgMnPc3d1	estetický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
českých	český	k2eAgFnPc2d1	Česká
zoo	zoo	k1gFnPc2	zoo
je	být	k5eAaImIp3nS	být
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
přes	přes	k7c4	přes
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgMnSc2d1	převalský
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
má	můj	k3xOp1gFnSc1	můj
zahrada	zahrada	k1gFnSc1	zahrada
i	i	k9	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc4	jehož
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
plemennou	plemenný	k2eAgFnSc4d1	plemenná
knihu	kniha	k1gFnSc4	kniha
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
atraktivními	atraktivní	k2eAgMnPc7d1	atraktivní
zvířaty	zvíře	k1gNnPc7	zvíře
jsou	být	k5eAaImIp3nP	být
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
,	,	kIx,	,
tučňáci	tučňák	k1gMnPc1	tučňák
a	a	k8xC	a
gorily	gorila	k1gFnPc1	gorila
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
i	i	k9	i
varani	varan	k1gMnPc1	varan
komodští	komodský	k2eAgMnPc1d1	komodský
nebo	nebo	k8xC	nebo
gaviálové	gaviál	k1gMnPc1	gaviál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věhlasu	věhlas	k1gInSc6	věhlas
pražské	pražský	k2eAgFnSc2d1	Pražská
zoo	zoo	k1gFnSc2	zoo
mají	mít	k5eAaImIp3nP	mít
zásluhy	zásluha	k1gFnPc1	zásluha
především	především	k9	především
tři	tři	k4xCgMnPc1	tři
ředitelé	ředitel	k1gMnPc1	ředitel
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Janda	Janda	k1gMnSc1	Janda
-	-	kIx~	-
její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	s	k7c7	s
30	[number]	k4	30
let	léto	k1gNnPc2	léto
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
zoo	zoo	k1gFnSc2	zoo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
-	-	kIx~	-
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
těžké	těžký	k2eAgFnPc4d1	těžká
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
socialismu	socialismus	k1gInSc2	socialismus
zasloužil	zasloužit	k5eAaPmAgInS	zasloužit
zejména	zejména	k9	zejména
o	o	k7c4	o
její	její	k3xOp3gInSc4	její
ohlas	ohlas	k1gInSc4	ohlas
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
světě	svět	k1gInSc6	svět
Petr	Petr	k1gMnSc1	Petr
Fejk	Fejk	k1gMnSc1	Fejk
-	-	kIx~	-
zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
výraznou	výrazný	k2eAgFnSc4d1	výrazná
modernizací	modernizace	k1gFnSc7	modernizace
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
zatraktivnění	zatraktivnění	k1gNnSc4	zatraktivnění
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
(	(	kIx(	(
<g/>
nové	nový	k2eAgFnPc4d1	nová
typy	typa	k1gFnPc4	typa
výběhů	výběh	k1gInPc2	výběh
<g/>
,	,	kIx,	,
zlepšený	zlepšený	k2eAgInSc4d1	zlepšený
vzhled	vzhled	k1gInSc4	vzhled
pavilónů	pavilón	k1gInPc2	pavilón
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
zoo	zoo	k1gFnSc6	zoo
je	být	k5eAaImIp3nS	být
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
zoo	zoo	k1gFnSc1	zoo
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
atraktivním	atraktivní	k2eAgNnSc6d1	atraktivní
prostředí	prostředí	k1gNnSc6	prostředí
anglického	anglický	k2eAgInSc2d1	anglický
parku	park	k1gInSc2	park
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
Lešná	Lešná	k1gFnSc1	Lešná
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
nově	nově	k6eAd1	nově
zrekonstruována	zrekonstruován	k2eAgNnPc1d1	zrekonstruováno
zoo	zoo	k1gNnPc1	zoo
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
zoo	zoo	k1gFnSc6	zoo
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
chová	chovat	k5eAaImIp3nS	chovat
nejvíce	hodně	k6eAd3	hodně
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
7000	[number]	k4	7000
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
1210	[number]	k4	1210
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
je	být	k5eAaImIp3nS	být
zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
její	její	k3xOp3gMnSc1	její
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
zoolog	zoolog	k1gMnSc1	zoolog
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
unikátní	unikátní	k2eAgMnSc1d1	unikátní
africké	africký	k2eAgNnSc4d1	africké
safari	safari	k1gNnSc4	safari
<g/>
:	:	kIx,	:
30	[number]	k4	30
ha	ha	kA	ha
velké	velký	k2eAgInPc4d1	velký
výběhy	výběh	k1gInPc4	výběh
s	s	k7c7	s
volně	volně	k6eAd1	volně
vypuštěnými	vypuštěný	k2eAgFnPc7d1	vypuštěná
stády	stádo	k1gNnPc7	stádo
afrických	africký	k2eAgMnPc2d1	africký
kopytníků	kopytník	k1gMnPc2	kopytník
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníky	návštěvník	k1gMnPc4	návštěvník
vozí	vozit	k5eAaImIp3nP	vozit
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c4	mezi
zvířata	zvíře	k1gNnPc4	zvíře
safaribusy	safaribus	k1gMnPc4	safaribus
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
safari	safari	k1gNnPc2	safari
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
také	také	k9	také
v	v	k7c6	v
chomutovském	chomutovský	k2eAgInSc6d1	chomutovský
zooparku	zoopark	k1gInSc6	zoopark
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
největší	veliký	k2eAgFnSc7d3	veliký
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
ZOO	zoo	k1gFnPc2	zoo
<g/>
,	,	kIx,	,
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
112	[number]	k4	112
hektarech	hektar	k1gInPc6	hektar
a	a	k8xC	a
specializuje	specializovat	k5eAaBmIp3nS	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
chov	chov	k1gInSc4	chov
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Safari	safari	k1gNnSc1	safari
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
zoo	zoo	k1gFnSc1	zoo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
také	také	k9	také
zoo	zoo	k1gFnSc1	zoo
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc4d3	nejmenší
rozlohu	rozloha	k1gFnSc4	rozloha
má	mít	k5eAaImIp3nS	mít
Zoo	zoo	k1gFnSc1	zoo
Hodonín	Hodonín	k1gInSc1	Hodonín
a	a	k8xC	a
zoo	zoo	k1gFnPc1	zoo
Chleby	chléb	k1gInPc4	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
celosvětově	celosvětově	k6eAd1	celosvětově
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
a	a	k8xC	a
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
chová	chovat	k5eAaImIp3nS	chovat
téměř	téměř	k6eAd1	téměř
250	[number]	k4	250
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
malá	malá	k1gFnSc1	malá
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
(	(	kIx(	(
<g/>
u	u	k7c2	u
Hluboké	Hluboká	k1gFnSc2	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
50	[number]	k4	50
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
chov	chov	k1gInSc4	chov
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
fauny	fauna	k1gFnSc2	fauna
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
také	také	k9	také
Zoo	zoo	k1gFnSc1	zoo
Děčín	Děčín	k1gInSc1	Děčín
a	a	k8xC	a
Podkrušnohorský	podkrušnohorský	k2eAgInSc1d1	podkrušnohorský
zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Zoopark	zoopark	k1gInSc1	zoopark
ve	v	k7c6	v
Vyškově	Vyškov	k1gInSc6	Vyškov
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
specializací	specializace	k1gFnSc7	specializace
na	na	k7c4	na
chov	chov	k1gInSc4	chov
exotických	exotický	k2eAgNnPc2d1	exotické
a	a	k8xC	a
krajových	krajový	k2eAgNnPc2d1	krajové
plemen	plemeno	k1gNnPc2	plemeno
domácích	domácí	k1gMnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
mají	mít	k5eAaImIp3nP	mít
Zoo	zoo	k1gNnPc1	zoo
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Zoo	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
především	především	k9	především
vinou	vinou	k7c2	vinou
zastaralých	zastaralý	k2eAgFnPc2d1	zastaralá
<g/>
,	,	kIx,	,
dosluhujících	dosluhující	k2eAgFnPc2d1	dosluhující
ubikací	ubikace	k1gFnPc2	ubikace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tyto	tento	k3xDgFnPc1	tento
zahrady	zahrada	k1gFnPc1	zahrada
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
zajímavými	zajímavý	k2eAgFnPc7d1	zajímavá
expozicemi	expozice	k1gFnPc7	expozice
a	a	k8xC	a
pozoruhodnými	pozoruhodný	k2eAgInPc7d1	pozoruhodný
chovatelskými	chovatelský	k2eAgInPc7d1	chovatelský
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zoo	zoo	k1gFnPc6	zoo
projevuje	projevovat	k5eAaImIp3nS	projevovat
trend	trend	k1gInSc1	trend
rozčlenění	rozčlenění	k1gNnSc2	rozčlenění
prostoru	prostor	k1gInSc2	prostor
zahrady	zahrada	k1gFnSc2	zahrada
podle	podle	k7c2	podle
geografických	geografický	k2eAgFnPc2d1	geografická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
tendence	tendence	k1gFnSc1	tendence
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
plzeňské	plzeňský	k2eAgFnSc6d1	Plzeňská
a	a	k8xC	a
zlínské	zlínský	k2eAgFnSc6d1	zlínská
zoo	zoo	k1gFnSc6	zoo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
plánuje	plánovat	k5eAaImIp3nS	plánovat
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
koncepci	koncepce	k1gFnSc4	koncepce
i	i	k8xC	i
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Zoologickou	zoologický	k2eAgFnSc7d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
(	(	kIx(	(
<g/>
Zoo	zoo	k1gFnSc1	zoo
<g/>
)	)	kIx)	)
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
označováno	označovat	k5eAaImNgNnS	označovat
pouze	pouze	k6eAd1	pouze
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
od	od	k7c2	od
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ji	on	k3xPp3gFnSc4	on
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
162	[number]	k4	162
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
provozování	provozování	k1gNnSc2	provozování
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
směrnice	směrnice	k1gFnSc2	směrnice
č.	č.	k?	č.
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
licence	licence	k1gFnSc2	licence
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zařízení	zařízení	k1gNnSc1	zařízení
má	mít	k5eAaImIp3nS	mít
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c6	na
ustájení	ustájení	k1gNnSc6	ustájení
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jejich	jejich	k3xOp3gNnSc2	jejich
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
vhodných	vhodný	k2eAgFnPc2d1	vhodná
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
má	mít	k5eAaImIp3nS	mít
zavedené	zavedený	k2eAgNnSc4d1	zavedené
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
úniků	únik	k1gInPc2	únik
živočichů	živočich	k1gMnPc2	živočich
má	mít	k5eAaImIp3nS	mít
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
veterinární	veterinární	k2eAgFnSc4d1	veterinární
péči	péče	k1gFnSc4	péče
provádí	provádět	k5eAaImIp3nS	provádět
výchovu	výchova	k1gFnSc4	výchova
veřejnosti	veřejnost	k1gFnSc2	veřejnost
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
poskytováním	poskytování	k1gNnSc7	poskytování
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
vystavených	vystavený	k2eAgInPc6d1	vystavený
druzích	druh	k1gInPc6	druh
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc6	jejich
přírodních	přírodní	k2eAgInPc6d1	přírodní
stanovištích	stanoviště	k1gNnPc6	stanoviště
a	a	k8xC	a
úloze	úloha	k1gFnSc6	úloha
v	v	k7c6	v
ekosystémech	ekosystém	k1gInPc6	ekosystém
<g/>
)	)	kIx)	)
účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
výzkumu	výzkum	k1gInSc2	výzkum
prospěšného	prospěšný	k2eAgMnSc4d1	prospěšný
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
dostatek	dostatek	k1gInSc1	dostatek
odborného	odborný	k2eAgInSc2d1	odborný
personálu	personál	k1gInSc2	personál
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
chovaných	chovaný	k2eAgMnPc2d1	chovaný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
veřejný	veřejný	k2eAgInSc4d1	veřejný
zvířecí	zvířecí	k2eAgInSc4d1	zvířecí
výběh	výběh	k1gInSc4	výběh
licenci	licence	k1gFnSc4	licence
a	a	k8xC	a
podléhá	podléhat	k5eAaImIp3nS	podléhat
inspekci	inspekce	k1gFnSc4	inspekce
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
těchto	tento	k3xDgInPc2	tento
úřadů	úřad	k1gInPc2	úřad
<g/>
:	:	kIx,	:
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Department	department	k1gInSc1	department
of	of	k?	of
Agriculture	Agricultur	k1gMnSc5	Agricultur
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc4	Protection
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
Drug	Drug	k1gMnSc1	Drug
Enforcement	Enforcement	k1gMnSc1	Enforcement
Administration	Administration	k1gInSc1	Administration
<g/>
,	,	kIx,	,
Occupational	Occupational	k1gMnSc1	Occupational
Safety	Safeta	k1gFnSc2	Safeta
and	and	k?	and
Health	Health	k1gInSc1	Health
Administration	Administration	k1gInSc1	Administration
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgNnPc1	jaký
zvířata	zvíře	k1gNnPc1	zvíře
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
mohou	moct	k5eAaImIp3nP	moct
podléhat	podléhat	k5eAaImF	podléhat
zákonům	zákon	k1gInPc3	zákon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Endangered	Endangered	k1gMnSc1	Endangered
Species	species	k1gFnSc2	species
Act	Act	k1gMnSc1	Act
<g/>
,	,	kIx,	,
the	the	k?	the
Animal	animal	k1gMnSc1	animal
Welfare	Welfar	k1gMnSc5	Welfar
Act	Act	k1gMnSc5	Act
<g/>
,	,	kIx,	,
the	the	k?	the
Migratory	Migrator	k1gInPc4	Migrator
Bird	Bird	k1gMnSc1	Bird
Treaty	Treata	k1gFnSc2	Treata
Act	Act	k1gMnSc1	Act
of	of	k?	of
1918	[number]	k4	1918
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
mohou	moct	k5eAaImIp3nP	moct
zoo	zoo	k1gFnSc4	zoo
zažádat	zažádat	k5eAaPmF	zažádat
o	o	k7c4	o
akreditaci	akreditace	k1gFnSc4	akreditace
vydávanou	vydávaný	k2eAgFnSc7d1	vydávaná
Asociací	asociace	k1gFnSc7	asociace
zoo	zoo	k1gFnPc2	zoo
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
AZA	AZA	kA	AZA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
získání	získání	k1gNnSc3	získání
musí	muset	k5eAaImIp3nP	muset
splnit	splnit	k5eAaPmF	splnit
požadavky	požadavek	k1gInPc7	požadavek
AZA	AZA	kA	AZA
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
a	a	k8xC	a
blaho	blaho	k1gNnSc4	blaho
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
financování	financování	k1gNnSc1	financování
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc1	vzdělání
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
globálních	globální	k2eAgFnPc2d1	globální
konzervačních	konzervační	k2eAgFnPc2d1	konzervační
snah	snaha	k1gFnPc2	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Inspekci	inspekce	k1gFnSc4	inspekce
provádí	provádět	k5eAaImIp3nP	provádět
tři	tři	k4xCgMnPc1	tři
odborníci	odborník	k1gMnPc1	odborník
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
veterinář	veterinář	k1gMnSc1	veterinář
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c6	na
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
management	management	k1gInSc4	management
zoo	zoo	k1gFnSc2	zoo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
její	její	k3xOp3gInSc4	její
výstup	výstup	k1gInSc4	výstup
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
panel	panel	k1gInSc1	panel
dvanácti	dvanáct	k4xCc2	dvanáct
dalších	další	k2eAgMnPc2d1	další
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
akreditace	akreditace	k1gFnSc2	akreditace
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
opakován	opakovat	k5eAaImNgInS	opakovat
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
AZA	AZA	kA	AZA
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
2400	[number]	k4	2400
vydaných	vydaný	k2eAgFnPc2d1	vydaná
licencí	licence	k1gFnPc2	licence
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akreditováno	akreditovat	k5eAaBmNgNnS	akreditovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Biolog	biolog	k1gMnSc1	biolog
Marc	Marc	k1gInSc1	Marc
Bekoff	Bekoff	k1gMnSc1	Bekoff
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgNnPc4d1	následující
fakta	faktum	k1gNnPc4	faktum
jako	jako	k8xS	jako
protiargumenty	protiargument	k1gInPc4	protiargument
proti	proti	k7c3	proti
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
zoo	zoo	k1gFnSc1	zoo
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Kruger	Kruger	k1gMnSc1	Kruger
z	z	k7c2	z
Informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
pohody	pohoda	k1gFnSc2	pohoda
zvířat	zvíře	k1gNnPc2	zvíře
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
stráví	strávit	k5eAaPmIp3nS	strávit
průměrný	průměrný	k2eAgMnSc1d1	průměrný
návštěvník	návštěvník	k1gMnSc1	návštěvník
zoo	zoo	k1gFnSc2	zoo
před	před	k7c7	před
jednou	jeden	k4xCgFnSc7	jeden
expozicí	expozice	k1gFnSc7	expozice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
do	do	k7c2	do
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgInPc4d1	informační
panely	panel	k1gInPc4	panel
čtou	číst	k5eAaImIp3nP	číst
návštěvníci	návštěvník	k1gMnPc1	návštěvník
jen	jen	k9	jen
některé	některý	k3yIgNnSc4	některý
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
průzkumů	průzkum	k1gInPc2	průzkum
je	být	k5eAaImIp3nS	být
převážným	převážný	k2eAgInSc7d1	převážný
důvodem	důvod	k1gInSc7	důvod
návštěvy	návštěva	k1gFnSc2	návštěva
zoo	zoo	k1gFnSc1	zoo
touha	touha	k1gFnSc1	touha
pobavit	pobavit	k5eAaPmF	pobavit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
studie	studie	k1gFnSc2	studie
provedené	provedený	k2eAgFnSc2d1	provedená
v	v	k7c6	v
Edinburské	Edinburský	k2eAgFnSc6d1	Edinburská
zoo	zoo	k1gFnSc6	zoo
pouhá	pouhý	k2eAgFnSc1d1	pouhá
4	[number]	k4	4
<g/>
%	%	kIx~	%
návštěvníků	návštěvník	k1gMnPc2	návštěvník
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
zoo	zoo	k1gFnSc2	zoo
kvůli	kvůli	k7c3	kvůli
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
vysloveně	vysloveně	k6eAd1	vysloveně
neuvedl	uvést	k5eNaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
zoo	zoo	k1gFnSc2	zoo
kvůli	kvůli	k7c3	kvůli
podpoření	podpoření	k1gNnSc3	podpoření
myšlenky	myšlenka	k1gFnSc2	myšlenka
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Dokladů	doklad	k1gInPc2	doklad
pro	pro	k7c4	pro
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zoo	zoo	k1gNnSc6	zoo
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
něčemu	něco	k3yInSc3	něco
naučí	naučit	k5eAaPmIp3nP	naučit
nebo	nebo	k8xC	nebo
že	že	k8xS	že
návštěva	návštěva	k1gFnSc1	návštěva
zoo	zoo	k1gFnSc2	zoo
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
postoj	postoj	k1gInSc4	postoj
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
poněkud	poněkud	k6eAd1	poněkud
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
a	a	k8xC	a
návštěvnicky	návštěvnicky	k6eAd1	návštěvnicky
atraktivních	atraktivní	k2eAgInPc2d1	atraktivní
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kočkovité	kočkovitý	k2eAgFnPc1d1	kočkovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
žirafy	žirafa	k1gFnPc1	žirafa
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
např.	např.	kA	např.
papoušci	papoušek	k1gMnPc1	papoušek
a	a	k8xC	a
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zoo	zoo	k1gFnSc6	zoo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
chována	chován	k2eAgNnPc4d1	chováno
i	i	k8xC	i
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
moc	moc	k6eAd1	moc
atraktivní	atraktivní	k2eAgFnPc1d1	atraktivní
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
antilop	antilopa	k1gFnPc2	antilopa
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnSc2d1	divoká
kozy	koza	k1gFnSc2	koza
a	a	k8xC	a
ovce	ovce	k1gFnSc2	ovce
nebo	nebo	k8xC	nebo
vrubozobí	vrubozobý	k2eAgMnPc1d1	vrubozobý
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
husy	husa	k1gFnPc4	husa
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc4	kachna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
velmi	velmi	k6eAd1	velmi
cenná	cenný	k2eAgFnSc1d1	cenná
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
záchranných	záchranný	k2eAgInPc2d1	záchranný
programů	program	k1gInPc2	program
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
