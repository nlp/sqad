<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
svěřil	svěřit	k5eAaPmAgInS	svěřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1464	[number]	k4	1464
hrad	hrad	k1gInSc4	hrad
Bouzov	Bouzov	k1gInSc4	Bouzov
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
svému	svůj	k3xOyFgMnSc3	svůj
věrnému	věrný	k2eAgMnSc3d1	věrný
stoupenci	stoupenec	k1gMnSc3	stoupenec
Zdeňku	Zdeněk	k1gMnSc3	Zdeněk
Kostkovi	Kostka	k1gMnSc3	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
jej	on	k3xPp3gMnSc4	on
velitelem	velitel	k1gMnSc7	velitel
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
