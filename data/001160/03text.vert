<s>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Jan	Jan	k1gMnSc1	Jan
Tomáš	Tomáš	k1gMnSc1	Tomáš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1932	[number]	k4	1932
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
dvou	dva	k4xCgFnPc2	dva
Oscarů	Oscar	k1gInPc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
tří	tři	k4xCgInPc2	tři
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
a	a	k8xC	a
ceny	cena	k1gFnSc2	cena
BAFTA	BAFTA	kA	BAFTA
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Křesadlovou	křesadlový	k2eAgFnSc7d1	Křesadlová
jsou	být	k5eAaImIp3nP	být
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
divadelníci	divadelník	k1gMnPc1	divadelník
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Matěj	Matěj	k1gMnSc1	Matěj
Formanovi	Forman	k1gMnSc3	Forman
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
mladší	mladý	k2eAgFnSc7d2	mladší
Martinou	Martin	k2eAgFnSc7d1	Martina
Zbořilovou	Zbořilová	k1gFnSc7	Zbořilová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
seznámení	seznámení	k1gNnSc6	seznámení
vydala	vydat	k5eAaPmAgFnS	vydat
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
další	další	k2eAgMnPc4d1	další
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc1	dvojče
Andyho	Andy	k1gMnSc2	Andy
a	a	k8xC	a
Jima	Jimus	k1gMnSc2	Jimus
Formanovy	Formanův	k2eAgFnSc2d1	Formanova
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
polovičním	poloviční	k2eAgMnSc7d1	poloviční
bratrem	bratr	k1gMnSc7	bratr
je	být	k5eAaImIp3nS	být
matematik	matematik	k1gMnSc1	matematik
Joseph	Joseph	k1gMnSc1	Joseph
Kohn	Kohn	k1gMnSc1	Kohn
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
USA	USA	kA	USA
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
protestantské	protestantský	k2eAgFnSc6d1	protestantská
rodině	rodina	k1gFnSc6	rodina
Anně	Anna	k1gFnSc6	Anna
Formanové	Formanová	k1gFnSc2	Formanová
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Švábové	Švábová	k1gFnSc2	Švábová
<g/>
)	)	kIx)	)
provozující	provozující	k2eAgInSc1d1	provozující
hotel	hotel	k1gInSc1	hotel
u	u	k7c2	u
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
Rudolfovi	Rudolf	k1gMnSc6	Rudolf
Formanovi	Forman	k1gMnSc6	Forman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pravým	pravý	k2eAgMnSc7d1	pravý
otcem	otec	k1gMnSc7	otec
však	však	k8xC	však
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
pražský	pražský	k2eAgMnSc1d1	pražský
architekt	architekt	k1gMnSc1	architekt
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
Otto	Otto	k1gMnSc1	Otto
Kohn	Kohn	k1gMnSc1	Kohn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
pravém	pravý	k2eAgInSc6d1	pravý
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
však	však	k9	však
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
až	až	k6eAd1	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mládí	mládí	k1gNnSc4	mládí
prožil	prožít	k5eAaPmAgMnS	prožít
jednak	jednak	k8xC	jednak
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Čáslavi	Čáslav	k1gFnSc6	Čáslav
a	a	k8xC	a
střídavě	střídavě	k6eAd1	střídavě
i	i	k9	i
v	v	k7c6	v
penzionu	penzion	k1gInSc6	penzion
Rut	rout	k5eAaImNgInS	rout
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
i	i	k9	i
s	s	k7c7	s
penzionem	penzion	k1gInSc7	penzion
fungoval	fungovat	k5eAaImAgInS	fungovat
jen	jen	k9	jen
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
vracela	vracet	k5eAaImAgFnS	vracet
do	do	k7c2	do
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
mladý	mladý	k2eAgMnSc1d1	mladý
osiřel	osiřet	k5eAaPmAgMnS	osiřet
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
odbojové	odbojový	k2eAgFnSc6d1	odbojová
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pro	pro	k7c4	pro
pouhé	pouhý	k2eAgNnSc4d1	pouhé
falešné	falešný	k2eAgNnSc4d1	falešné
udání	udání	k1gNnSc4	udání
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
s	s	k7c7	s
protinacistickými	protinacistický	k2eAgInPc7d1	protinacistický
letáky	leták	k1gInPc7	leták
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
zatkli	zatknout	k5eAaPmAgMnP	zatknout
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
Starých	Starých	k2eAgInPc2d1	Starých
Splavů	splav	k1gInPc2	splav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
jiná	jiná	k1gFnSc1	jiná
tu	tu	k6eAd1	tu
nebyla	být	k5eNaImAgFnS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neuměl	umět	k5eNaImAgMnS	umět
německy	německy	k6eAd1	německy
a	a	k8xC	a
spolužáci	spolužák	k1gMnPc1	spolužák
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
dávali	dávat	k5eAaImAgMnP	dávat
natolik	natolik	k6eAd1	natolik
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
raději	rád	k6eAd2	rád
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
roků	rok	k1gInPc2	rok
vězněni	věznit	k5eAaImNgMnP	věznit
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Petschkově	Petschkův	k2eAgInSc6d1	Petschkův
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
poté	poté	k6eAd1	poté
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
zahynula	zahynout	k5eAaPmAgFnS	zahynout
na	na	k7c4	na
tyfus	tyfus	k1gInSc4	tyfus
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
Buchenwaldu	Buchenwald	k1gInSc6	Buchenwald
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
u	u	k7c2	u
příbuzných	příbuzný	k1gMnPc2	příbuzný
a	a	k8xC	a
v	v	k7c6	v
poděbradské	poděbradský	k2eAgFnSc6d1	Poděbradská
internátní	internátní	k2eAgFnSc6d1	internátní
škole	škola	k1gFnSc6	škola
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnPc4	jeho
spolužáky	spolužák	k1gMnPc4	spolužák
byli	být	k5eAaImAgMnP	být
mj.	mj.	kA	mj.
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
Mašínové	Mašínová	k1gFnSc2	Mašínová
a	a	k8xC	a
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Janata	Janata	k1gMnSc1	Janata
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
osiřelí	osiřelý	k2eAgMnPc1d1	osiřelý
chlapci	chlapec	k1gMnPc1	chlapec
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
o	o	k7c4	o
12	[number]	k4	12
let	léto	k1gNnPc2	léto
starší	starší	k1gMnSc1	starší
bratr	bratr	k1gMnSc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
)	)	kIx)	)
do	do	k7c2	do
penzionu	penzion	k1gInSc2	penzion
Rut	rout	k5eAaImNgInS	rout
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
jej	on	k3xPp3gNnSc4	on
opravovat	opravovat	k5eAaImF	opravovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
znárodnění	znárodnění	k1gNnSc6	znárodnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
bydlet	bydlet	k5eAaImF	bydlet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
plnoletosti	plnoletost	k1gFnSc2	plnoletost
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Formanovi	Forman	k1gMnSc3	Forman
penzion	penzion	k1gInSc1	penzion
ve	v	k7c6	v
Starých	Starých	k2eAgInPc6d1	Starých
Splavech	splav	k1gInPc6	splav
získali	získat	k5eAaPmAgMnP	získat
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
v	v	k7c6	v
restituci	restituce	k1gFnSc6	restituce
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
scenáristiku	scenáristika	k1gFnSc4	scenáristika
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Filmové	filmový	k2eAgFnSc6d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc6d1	televizní
fakultě	fakulta	k1gFnSc6	fakulta
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
FAMU	FAMU	kA	FAMU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
asistent	asistent	k1gMnSc1	asistent
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
společně	společně	k6eAd1	společně
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Ondříčkem	Ondříček	k1gMnSc7	Ondříček
natočil	natočit	k5eAaBmAgMnS	natočit
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
snímků	snímek	k1gInPc2	snímek
vyznačujících	vyznačující	k2eAgInPc2d1	vyznačující
se	se	k3xPyFc4	se
sžíravým	sžíravý	k2eAgMnSc7d1	sžíravý
<g/>
,	,	kIx,	,
až	až	k9	až
černohumorným	černohumorný	k2eAgInSc7d1	černohumorný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
získal	získat	k5eAaPmAgMnS	získat
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
svého	svůj	k1gMnSc2	svůj
tvůrce	tvůrce	k1gMnSc2	tvůrce
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Locarnu	Locarno	k1gNnSc6	Locarno
(	(	kIx(	(
<g/>
za	za	k7c7	za
Formanem	Forman	k1gMnSc7	Forman
tehdy	tehdy	k6eAd1	tehdy
zůstali	zůstat	k5eAaPmAgMnP	zůstat
režiséři	režisér	k1gMnPc1	režisér
zvučných	zvučný	k2eAgNnPc2d1	zvučné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Antonioni	Antonioň	k1gFnSc3	Antonioň
a	a	k8xC	a
Jean-Luc	Jean-Luc	k1gFnSc1	Jean-Luc
Godard	Godarda	k1gFnPc2	Godarda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
nominován	nominován	k2eAgMnSc1d1	nominován
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
legálně	legálně	k6eAd1	legálně
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
filmové	filmový	k2eAgFnSc6d1	filmová
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
tedy	tedy	k9	tedy
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
filmaři	filmař	k1gMnPc7	filmař
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
kameramanem	kameraman	k1gMnSc7	kameraman
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Ondříčkem	Ondříček	k1gMnSc7	Ondříček
a	a	k8xC	a
Amadea	Amadeus	k1gMnSc4	Amadeus
natočit	natočit	k5eAaBmF	natočit
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
"	"	kIx"	"
<g/>
formanovský	formanovský	k2eAgMnSc1d1	formanovský
<g/>
"	"	kIx"	"
první	první	k4xOgInSc4	první
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
Taking	Taking	k1gInSc1	Taking
Off	Off	k1gFnSc1	Off
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
remake	remake	k1gFnSc1	remake
<g/>
"	"	kIx"	"
Konkursu	konkurs	k1gInSc2	konkurs
<g/>
)	)	kIx)	)
-	-	kIx~	-
aplikující	aplikující	k2eAgInSc4d1	aplikující
jeho	jeho	k3xOp3gInSc4	jeho
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
<g/>
,	,	kIx,	,
na	na	k7c4	na
české	český	k2eAgMnPc4d1	český
diváky	divák	k1gMnPc4	divák
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
americké	americký	k2eAgNnSc4d1	americké
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
doznívající	doznívající	k2eAgFnSc6d1	doznívající
éře	éra	k1gFnSc6	éra
hippies	hippies	k1gInSc1	hippies
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
první	první	k4xOgFnSc7	první
cenou	cena	k1gFnSc7	cena
poroty	porota	k1gFnSc2	porota
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komerčně	komerčně	k6eAd1	komerčně
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Významného	významný	k2eAgInSc2d1	významný
úspěchu	úspěch	k1gInSc2	úspěch
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
svým	svůj	k3xOyFgInSc7	svůj
dalším	další	k2eAgInSc7d1	další
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
adaptací	adaptace	k1gFnSc7	adaptace
románu	román	k1gInSc2	román
Kena	Kenus	k1gMnSc2	Kenus
Keseyho	Kesey	k1gMnSc2	Kesey
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
americká	americký	k2eAgFnSc1d1	americká
Akademie	akademie	k1gFnSc1	akademie
filmových	filmový	k2eAgNnPc2d1	filmové
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
ocenila	ocenit	k5eAaPmAgFnS	ocenit
pěti	pět	k4xCc7	pět
Oscary	Oscar	k1gInPc7	Oscar
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
více	hodně	k6eAd2	hodně
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
než	než	k8xS	než
divácky	divácky	k6eAd1	divácky
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Valmont	Valmont	k1gInSc1	Valmont
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Amadeus	Amadeus	k1gMnSc1	Amadeus
získal	získat	k5eAaPmAgMnS	získat
dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
Oscarů	Oscar	k1gInPc2	Oscar
včetně	včetně	k7c2	včetně
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
-	-	kIx~	-
a	a	k8xC	a
významných	významný	k2eAgMnPc2d1	významný
amerických	americký	k2eAgMnPc2d1	americký
a	a	k8xC	a
světových	světový	k2eAgMnPc2d1	světový
-	-	kIx~	-
filmových	filmový	k2eAgMnPc2d1	filmový
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
jak	jak	k8xC	jak
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
kritiků	kritik	k1gMnPc2	kritik
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Slovanském	slovanský	k2eAgInSc6d1	slovanský
domě	dům	k1gInSc6	dům
slavnostně	slavnostně	k6eAd1	slavnostně
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
premiéře	premiéra	k1gFnSc6	premiéra
jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
Goyovy	Goyův	k2eAgInPc1d1	Goyův
přízraky	přízrak	k1gInPc1	přízrak
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Natalie	Natalie	k1gFnSc2	Natalie
Portmanové	Portmanová	k1gFnSc2	Portmanová
a	a	k8xC	a
Javiera	Javier	k1gMnSc4	Javier
Bardema	Bardem	k1gMnSc4	Bardem
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
premiéru	premiéra	k1gFnSc4	premiéra
nové	nový	k2eAgNnSc1d1	nové
nastudování	nastudování	k1gNnSc1	nastudování
jazzové	jazzový	k2eAgFnSc2d1	jazzová
opery	opera	k1gFnSc2	opera
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchý	k1gMnSc2	Suchý
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
Dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
spolurežíroval	spolurežírovat	k5eAaImAgMnS	spolurežírovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Petrem	Petr	k1gMnSc7	Petr
Formanem	Forman	k1gMnSc7	Forman
<g/>
.	.	kIx.	.
</s>
<s>
Konkurs	konkurs	k1gInSc1	konkurs
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Černý	Černý	k1gMnSc1	Černý
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Lásky	láska	k1gFnSc2	láska
jedné	jeden	k4xCgFnSc2	jeden
plavovlásky	plavovláska	k1gFnSc2	plavovláska
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Odcházím	odcházet	k5eAaImIp1nS	odcházet
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
-	-	kIx~	-
adaptace	adaptace	k1gFnSc1	adaptace
knihy	kniha	k1gFnSc2	kniha
Vyhoďme	vyhodit	k5eAaPmRp1nP	vyhodit
<g />
.	.	kIx.	.
</s>
<s>
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
ven	ven	k6eAd1	ven
Kena	Kenus	k1gMnSc2	Kenus
Keseyho	Kesey	k1gMnSc2	Kesey
Vlasy	vlas	k1gInPc1	vlas
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
Ragtime	ragtime	k1gInSc1	ragtime
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
-	-	kIx~	-
adaptace	adaptace	k1gFnSc1	adaptace
knihy	kniha	k1gFnSc2	kniha
Ragtime	ragtime	k1gInSc1	ragtime
(	(	kIx(	(
<g/>
Edgar	Edgar	k1gMnSc1	Edgar
Lawrence	Lawrence	k1gFnSc2	Lawrence
Doctorow	Doctorow	k1gMnSc1	Doctorow
<g/>
)	)	kIx)	)
Amadeus	Amadeus	k1gMnSc1	Amadeus
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Petera	Peter	k1gMnSc2	Peter
Shaffera	Shaffer	k1gMnSc2	Shaffer
<g/>
.	.	kIx.	.
</s>
<s>
Valmont	Valmont	k1gInSc1	Valmont
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
-	-	kIx~	-
adaptace	adaptace	k1gFnSc1	adaptace
knihy	kniha	k1gFnSc2	kniha
Nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
známosti	známost	k1gFnSc2	známost
(	(	kIx(	(
<g/>
Choderlos	Choderlos	k1gInSc1	Choderlos
de	de	k?	de
Laclos	Laclos	k1gInSc1	Laclos
<g/>
)	)	kIx)	)
Lid	lid	k1gInSc1	lid
versus	versus	k7c1	versus
Larry	Larra	k1gMnSc2	Larra
Flynt	Flynta	k1gFnPc2	Flynta
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspirováno	inspirován	k2eAgNnSc1d1	inspirováno
životem	život	k1gInSc7	život
Larryho	Larry	k1gMnSc2	Larry
Flynta	Flynt	k1gMnSc2	Flynt
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
magazínu	magazín	k1gInSc2	magazín
Hustler	Hustler	k1gMnSc1	Hustler
<g/>
)	)	kIx)	)
Muž	muž	k1gMnSc1	muž
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
životem	život	k1gInSc7	život
<g />
.	.	kIx.	.
</s>
<s>
komika	komika	k1gFnSc1	komika
Andyho	Andy	k1gMnSc2	Andy
Kaufmana	Kaufman	k1gMnSc2	Kaufman
Goyovy	Goyův	k2eAgInPc1d1	Goyův
přízraky	přízrak	k1gInPc7	přízrak
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
procházka	procházka	k1gFnSc1	procházka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
záznam	záznam	k1gInSc1	záznam
představení	představení	k1gNnPc2	představení
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Dědeček	dědeček	k1gMnSc1	dědeček
automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Heartburn	Heartburna	k1gFnPc2	Heartburna
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
New	New	k1gMnSc1	New
Year	Year	k1gMnSc1	Year
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Day	Day	k1gFnSc7	Day
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Rabín	rabín	k1gMnSc1	rabín
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
krásná	krásný	k2eAgFnSc1d1	krásná
blondýna	blondýna	k1gFnSc1	blondýna
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
farář	farář	k1gMnSc1	farář
Havel	Havel	k1gMnSc1	Havel
Peklo	peklo	k1gNnSc4	peklo
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
čert	čert	k1gMnSc1	čert
Milovaní	milovaný	k2eAgMnPc1d1	milovaný
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
