<s>
Korjačtina	Korjačtina	k1gFnSc1	Korjačtina
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čavčuvenský	čavčuvenský	k2eAgMnSc1d1	čavčuvenský
<g/>
,	,	kIx,	,
apukinský	apukinský	k2eAgMnSc1d1	apukinský
<g/>
,	,	kIx,	,
itkanský	itkanský	k2eAgMnSc1d1	itkanský
<g/>
,	,	kIx,	,
kamenský	kamenský	k2eAgMnSc1d1	kamenský
<g/>
,	,	kIx,	,
parenský	parenský	k2eAgMnSc1d1	parenský
<g/>
,	,	kIx,	,
karaginský	karaginský	k2eAgMnSc1d1	karaginský
nebo	nebo	k8xC	nebo
palanský	palanský	k2eAgMnSc1d1	palanský
<g/>
;	;	kIx,	;
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
