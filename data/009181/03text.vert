<p>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
hudební	hudební	k2eAgFnSc1d1	hudební
forma	forma	k1gFnSc1	forma
sonátového	sonátový	k2eAgInSc2d1	sonátový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
komponovaná	komponovaný	k2eAgFnSc1d1	komponovaná
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
až	až	k8xS	až
velké	velký	k2eAgNnSc4d1	velké
nástrojové	nástrojový	k2eAgNnSc4d1	nástrojové
obsazení	obsazení	k1gNnSc4	obsazení
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
původně	původně	k6eAd1	původně
instrumentální	instrumentální	k2eAgFnSc3d1	instrumentální
symfonii	symfonie	k1gFnSc3	symfonie
později	pozdě	k6eAd2	pozdě
přibyla	přibýt	k5eAaPmAgFnS	přibýt
možnost	možnost	k1gFnSc1	možnost
současného	současný	k2eAgNnSc2d1	současné
uplatnění	uplatnění	k1gNnSc2	uplatnění
dalších	další	k2eAgInPc2d1	další
komponentů	komponent	k1gInPc2	komponent
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sólového	sólový	k2eAgInSc2d1	sólový
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
pak	pak	k6eAd1	pak
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
dalších	další	k2eAgInPc2d1	další
přidružených	přidružený	k2eAgInPc2d1	přidružený
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
souboru	soubor	k1gInSc2	soubor
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc1	symfonie
Miloslava	Miloslav	k1gMnSc2	Miloslav
Kabeláče	Kabeláč	k1gMnSc2	Kabeláč
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnSc2	skladba
Krzystofa	Krzystof	k1gMnSc2	Krzystof
Pendereckého	Penderecký	k2eAgMnSc2d1	Penderecký
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
tradičních	tradiční	k2eAgFnPc2d1	tradiční
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
např.	např.	kA	např.
elektronických	elektronický	k2eAgInPc2d1	elektronický
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
Philip	Philip	k1gInSc1	Philip
Glass	Glass	k1gInSc1	Glass
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nehudebních	hudební	k2eNgMnPc2d1	nehudební
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Cage	Cage	k1gFnSc1	Cage
<g/>
,	,	kIx,	,
György	Györg	k1gInPc1	Györg
Ligeti	Lige	k1gNnSc6	Lige
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
industriálních	industriální	k2eAgInPc2d1	industriální
<g/>
,	,	kIx,	,
hlukových	hlukový	k2eAgInPc2d1	hlukový
a	a	k8xC	a
ambientních	ambientní	k2eAgInPc2d1	ambientní
zdrojů	zdroj	k1gInPc2	zdroj
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
digitálně	digitálně	k6eAd1	digitálně
zpracovaných	zpracovaný	k2eAgInPc2d1	zpracovaný
zvuků	zvuk	k1gInPc2	zvuk
nástrojů	nástroj	k1gInPc2	nástroj
z	z	k7c2	z
obsazení	obsazení	k1gNnSc2	obsazení
klasického	klasický	k2eAgInSc2d1	klasický
orchestru	orchestr	k1gInSc2	orchestr
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hirsch	Hirsch	k1gMnSc1	Hirsch
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
<s>
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
forma	forma	k1gFnSc1	forma
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejnáročnějším	náročný	k2eAgInPc3d3	nejnáročnější
hudebním	hudební	k2eAgInPc3d1	hudební
útvarům	útvar	k1gInPc3	útvar
jak	jak	k9	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kompozičního	kompoziční	k2eAgNnSc2d1	kompoziční
<g/>
,	,	kIx,	,
tak	tak	k9	tak
interpretačního	interpretační	k2eAgMnSc2d1	interpretační
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
právem	právo	k1gNnSc7	právo
perlou	perla	k1gFnSc7	perla
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
skladatelského	skladatelský	k2eAgNnSc2d1	skladatelské
umění	umění	k1gNnSc2	umění
i	i	k8xC	i
umění	umění	k1gNnSc2	umění
koncertní	koncertní	k2eAgFnSc2d1	koncertní
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
a	a	k8xC	a
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
i	i	k8xC	i
slova	slovo	k1gNnSc2	slovo
symfonie	symfonie	k1gFnSc2	symfonie
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
<g/>
:	:	kIx,	:
souzvuk	souzvuk	k1gInSc1	souzvuk
<g/>
,	,	kIx,	,
současné	současný	k2eAgNnSc4d1	současné
znění	znění	k1gNnSc4	znění
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
zvuku	zvuk	k1gInSc2	zvuk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
hledat	hledat	k5eAaImF	hledat
již	již	k9	již
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
výrazu	výraz	k1gInSc2	výraz
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
mnoho	mnoho	k4c1	mnoho
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
řecké	řecký	k2eAgFnSc6d1	řecká
hudbě	hudba	k1gFnSc6	hudba
představoval	představovat	k5eAaImAgMnS	představovat
tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
tedy	tedy	k9	tedy
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dnes	dnes	k6eAd1	dnes
-konsonance	onsonanka	k1gFnSc3	-konsonanka
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
každá	každý	k3xTgFnSc1	každý
vokální	vokální	k2eAgFnSc1d1	vokální
skladba	skladba	k1gFnSc1	skladba
s	s	k7c7	s
instrumentálním	instrumentální	k2eAgInSc7d1	instrumentální
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
sinfonia	sinfonia	k1gFnSc1	sinfonia
předehrou	předehra	k1gFnSc7	předehra
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
předehra	předehra	k1gFnSc1	předehra
<g/>
"	"	kIx"	"
-	-	kIx~	-
sinfonia	sinfonia	k1gFnSc1	sinfonia
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
současné	současný	k2eAgFnSc2d1	současná
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
proměňovala	proměňovat	k5eAaImAgFnS	proměňovat
(	(	kIx(	(
<g/>
počet	počet	k1gInSc4	počet
vět	věta	k1gFnPc2	věta
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc1	dělení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
rychlá-pomalá-rychlá	rychláomaláychlat	k5eAaPmIp3nS	rychlá-pomalá-rychlat
<g/>
,	,	kIx,	,
zaváděním	zavádění	k1gNnSc7	zavádění
sborů	sbor	k1gInPc2	sbor
či	či	k8xC	či
sólistů	sólista	k1gMnPc2	sólista
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
vývojovým	vývojový	k2eAgInSc7d1	vývojový
článkem	článek	k1gInSc7	článek
pro	pro	k7c4	pro
symfonii	symfonie	k1gFnSc4	symfonie
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
chápeme	chápat	k5eAaImIp1nP	chápat
víceméně	víceméně	k9	víceméně
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skladby	skladba	k1gFnPc4	skladba
tzv.	tzv.	kA	tzv.
mannheimské	mannheimský	k2eAgFnPc4d1	Mannheimská
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
založené	založený	k2eAgFnPc4d1	založená
českými	český	k2eAgMnPc7d1	český
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
především	především	k9	především
J.	J.	kA	J.
V.	V.	kA	V.
Stamicem	Stamic	k1gMnSc7	Stamic
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
sonátové	sonátový	k2eAgFnSc2d1	sonátová
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc4	otec
<g/>
"	"	kIx"	"
klasické	klasický	k2eAgFnSc2d1	klasická
symfonie	symfonie	k1gFnSc2	symfonie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
;	;	kIx,	;
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
symfonie	symfonie	k1gFnSc1	symfonie
obvykle	obvykle	k6eAd1	obvykle
sonátovou	sonátový	k2eAgFnSc4d1	sonátová
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
formu	forma	k1gFnSc4	forma
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
větách	věta	k1gFnPc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
bylo	být	k5eAaImAgNnS	být
víceméně	víceméně	k9	víceméně
respektováno	respektován	k2eAgNnSc1d1	respektováno
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
moderní	moderní	k2eAgFnPc4d1	moderní
symfonie	symfonie	k1gFnPc4	symfonie
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
nejsou	být	k5eNaImIp3nP	být
stanovena	stanoven	k2eAgNnPc1d1	stanoveno
žádná	žádný	k3yNgNnPc4	žádný
předem	předem	k6eAd1	předem
daná	daný	k2eAgNnPc4d1	dané
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
každém	každý	k3xTgMnSc6	každý
skladateli	skladatel	k1gMnSc6	skladatel
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
si	se	k3xPyFc3	se
při	při	k7c6	při
skladbě	skladba	k1gFnSc6	skladba
symfonie	symfonie	k1gFnSc2	symfonie
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skladatelé	skladatel	k1gMnPc5	skladatel
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
skladatele	skladatel	k1gMnPc4	skladatel
symfonické	symfonický	k2eAgFnSc2d1	symfonická
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
řazeno	řadit	k5eAaImNgNnS	řadit
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
</s>
</p>
<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Bruckner	Bruckner	k1gMnSc1	Bruckner
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
</s>
</p>
<p>
<s>
Camille	Camille	k1gFnSc1	Camille
Saint-Saëns	Saint-Saënsa	k1gFnPc2	Saint-Saënsa
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
</s>
</p>
<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
</s>
</p>
<p>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
</s>
</p>
<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
</s>
</p>
<p>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Šostakovič	Šostakovič	k1gInSc4	Šostakovič
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kabeláč	Kabeláč	k1gMnSc1	Kabeláč
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
nejdokonalejší	dokonalý	k2eAgNnPc4d3	nejdokonalejší
symfonická	symfonický	k2eAgNnPc4d1	symfonické
díla	dílo	k1gNnPc4	dílo
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
skladby	skladba	k1gFnSc2	skladba
Haydnovy	Haydnův	k2eAgFnSc2d1	Haydnova
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
100	[number]	k4	100
programních	programní	k2eAgFnPc2d1	programní
symfonií	symfonie	k1gFnPc2	symfonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mozartovy	Mozartův	k2eAgInPc1d1	Mozartův
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
25	[number]	k4	25
g-moll	goll	k1gInSc1	g-moll
<g/>
,	,	kIx,	,
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
40	[number]	k4	40
g-moll	goll	k1gInSc1	g-moll
<g/>
,	,	kIx,	,
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
41	[number]	k4	41
C	C	kA	C
dur	dur	k1gNnSc2	dur
"	"	kIx"	"
<g/>
Jupiterská	jupiterský	k2eAgFnSc1d1	Jupiterská
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Beethovenovy	Beethovenův	k2eAgInPc1d1	Beethovenův
(	(	kIx(	(
<g/>
především	především	k9	především
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Eroica	Eroica	k1gMnSc1	Eroica
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Osudová	osudový	k2eAgFnSc1d1	osudová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pastorální	pastorální	k2eAgMnSc1d1	pastorální
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
9	[number]	k4	9
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
S	s	k7c7	s
ódou	óda	k1gFnSc7	óda
na	na	k7c4	na
radost	radost	k1gFnSc4	radost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
symfonie	symfonie	k1gFnSc2	symfonie
č.	č.	k?	č.
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Novosvětská	novosvětský	k2eAgFnSc1d1	Novosvětská
<g/>
"	"	kIx"	"
a	a	k8xC	a
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
"	"	kIx"	"
<g/>
Novosvětská	novosvětský	k2eAgFnSc1d1	Novosvětská
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
vůbec	vůbec	k9	vůbec
nejhranějšími	hraný	k2eAgFnPc7d3	nejhranější
symfoniemi	symfonie	k1gFnPc7	symfonie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
symfonie	symfonie	k1gFnPc1	symfonie
Brucknerovy	Brucknerův	k2eAgFnPc1d1	Brucknerova
<g/>
,	,	kIx,	,
Brahmsovy	Brahmsův	k2eAgFnPc1d1	Brahmsova
a	a	k8xC	a
Mahlerovy	Mahlerův	k2eAgFnPc1d1	Mahlerova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Koncertantní	koncertantní	k2eAgFnPc1d1	koncertantní
symfonie	symfonie	k1gFnPc1	symfonie
</s>
</p>
<p>
<s>
Sinfonia	sinfonia	k1gFnSc1	sinfonia
</s>
</p>
<p>
<s>
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
</s>
</p>
<p>
<s>
Symfonieta	symfonieta	k1gFnSc1	symfonieta
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
symfonických	symfonický	k2eAgNnPc2d1	symfonické
děl	dělo	k1gNnPc2	dělo
</s>
</p>
<p>
<s>
Polyfonie	polyfonie	k1gFnSc1	polyfonie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
symfonie	symfonie	k1gFnSc2	symfonie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
