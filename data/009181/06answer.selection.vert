<s>
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
forma	forma	k1gFnSc1	forma
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejnáročnějším	náročný	k2eAgInPc3d3	nejnáročnější
hudebním	hudební	k2eAgInPc3d1	hudební
útvarům	útvar	k1gInPc3	útvar
jak	jak	k9	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kompozičního	kompoziční	k2eAgNnSc2d1	kompoziční
<g/>
,	,	kIx,	,
tak	tak	k9	tak
interpretačního	interpretační	k2eAgMnSc2d1	interpretační
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
právem	právo	k1gNnSc7	právo
perlou	perla	k1gFnSc7	perla
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
skladatelského	skladatelský	k2eAgNnSc2d1	skladatelské
umění	umění	k1gNnSc2	umění
i	i	k8xC	i
umění	umění	k1gNnSc2	umění
koncertní	koncertní	k2eAgFnSc2d1	koncertní
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
