<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc4d1
geomorfologický	geomorfologický	k2eAgInSc4d1
celek	celek	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Podkrušnohorské	podkrušnohorský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Sokolov	Sokolov	k1gInSc1
a	a	k8xC
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>