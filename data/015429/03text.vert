<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
Jezero	jezero	k1gNnSc1
Medard	Medard	k1gMnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
569,7	569,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
Zelený	zelený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
36	#num#	k4
km	km	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
9	#num#	k4
km	km	kA
Rozloha	rozloha	k1gFnSc1
</s>
<s>
312	#num#	k4
km²	km²	k?
Střední	střední	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
451,8	451,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Nadřazená	nadřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Podkrušnohorská	podkrušnohorský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Sousedníjednotky	Sousedníjednotka	k1gFnSc2
</s>
<s>
Doupovské	Doupovské	k2eAgInSc1d1
horySlavkovský	horySlavkovský	k2eAgInSc1d1
lesChebská	lesChebský	k2eAgFnSc1d1
pánevKrušné	pánevKrušný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
</s>
<s>
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Česka	Česko	k1gNnSc2
</s>
<s>
Horniny	hornina	k1gFnPc1
</s>
<s>
terciérní	terciérní	k2eAgInPc4d1
sedimenty	sediment	k1gInPc4
<g/>
,	,	kIx,
hnědé	hnědý	k2eAgNnSc4d1
uhlí	uhlí	k1gNnSc4
Povodí	povodí	k1gNnSc2
</s>
<s>
Ohře	Ohře	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Identifikátory	identifikátor	k1gInPc1
Kód	kód	k1gInSc1
geomorf	geomorf	k1gInSc1
<g/>
.	.	kIx.
jednotky	jednotka	k1gFnPc1
</s>
<s>
IIIB-2	IIIB-2	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc4d1
geomorfologický	geomorfologický	k2eAgInSc4d1
celek	celek	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Podkrušnohorské	podkrušnohorský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Sokolov	Sokolov	k1gInSc1
a	a	k8xC
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
Zelený	zelený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
569,7	569,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
starších	starý	k2eAgFnPc6d2
mapách	mapa	k1gFnPc6
ještě	ještě	k6eAd1
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
bývalý	bývalý	k2eAgInSc4d1
nejvyšší	vysoký	k2eAgInSc4d3
vrch	vrch	k1gInSc4
v	v	k7c6
Sokolovské	sokolovský	k2eAgFnSc6d1
pánvi	pánev	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
Dvorský	dvorský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
původně	původně	k6eAd1
573	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vrcholová	vrcholový	k2eAgFnSc1d1
partie	partie	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
odtěžena	odtěžit	k5eAaPmNgFnS
lomem	lom	k1gInSc7
na	na	k7c4
kámen	kámen	k1gInSc4
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
nejvyšší	vysoký	k2eAgInSc1d3
bodem	bod	k1gInSc7
stal	stát	k5eAaPmAgInS
Zelený	zelený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vymezení	vymezení	k1gNnSc1
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
terciérní	terciérní	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
s	s	k7c7
vrásově	vrásově	k6eAd1
zlomovou	zlomový	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
oboustranně	oboustranně	k6eAd1
tektonicky	tektonicky	k6eAd1
ohraničený	ohraničený	k2eAgInSc1d1
<g/>
,	,	kIx,
stupňovitý	stupňovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
příčně	příčně	k6eAd1
asymetrický	asymetrický	k2eAgInSc1d1
příkop	příkop	k1gInSc1
<g/>
,	,	kIx,
protažený	protažený	k2eAgInSc1d1
ve	v	k7c6
směru	směr	k1gInSc6
západ	západ	k1gInSc1
–	–	k?
východ	východ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pánev	pánev	k1gFnSc1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
36	#num#	k4
km	km	kA
<g/>
,	,	kIx,
šířku	šířka	k1gFnSc4
9	#num#	k4
km	km	kA
a	a	k8xC
rozlohu	rozloha	k1gFnSc4
312	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
je	být	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
oherským	oherský	k2eAgInSc7d1
okrajovým	okrajový	k2eAgInSc7d1
zlomem	zlom	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
odděluje	oddělovat	k5eAaImIp3nS
od	od	k7c2
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
je	být	k5eAaImIp3nS
pánev	pánev	k1gFnSc1
ohraničena	ohraničit	k5eAaPmNgFnS
stupňovitým	stupňovitý	k2eAgNnSc7d1
zlomovým	zlomový	k2eAgNnSc7d1
krušnohorským	krušnohorský	k2eAgNnSc7d1
pásmem	pásmo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
je	být	k5eAaImIp3nS
oddělena	oddělen	k2eAgFnSc1d1
od	od	k7c2
chebské	chebský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
krystalinickým	krystalinický	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
u	u	k7c2
Chlumu	chlum	k1gInSc2
sv.	sv.	kA
Maří	mařit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
krystalinický	krystalinický	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
oherského	oherský	k2eAgNnSc2d1
krystalinika	krystalinikum	k1gNnSc2
<g/>
,	,	kIx,
překrytý	překrytý	k2eAgMnSc1d1
vulkanity	vulkanit	k1gInPc7
Doupovských	Doupovských	k2eAgInSc2d1
hor.	hor.	k?
Na	na	k7c6
území	území	k1gNnSc6
se	se	k3xPyFc4
z	z	k7c2
větších	veliký	k2eAgNnPc2d2
sídel	sídlo	k1gNnPc2
nacházejí	nacházet	k5eAaImIp3nP
města	město	k1gNnSc2
Sokolov	Sokolov	k1gInSc1
<g/>
,	,	kIx,
Chodov	Chodov	k1gInSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Sedlo	sedlo	k1gNnSc1
a	a	k8xC
Habartov	Habartov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
nemá	mít	k5eNaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
českého	český	k2eAgNnSc2d1
geomorfologického	geomorfologický	k2eAgNnSc2d1
členění	členění	k1gNnSc2
žádný	žádný	k3yNgInSc1
podcelek	podcelek	k1gInSc4
<g/>
,	,	kIx,
dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
okrsky	okrsek	k1gInPc4
<g/>
,	,	kIx,
kterými	který	k3yRgMnPc7,k3yIgMnPc7,k3yQgMnPc7
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Chlumský	chlumský	k2eAgInSc1d1
práh	práh	k1gInSc1
</s>
<s>
Svatavská	Svatavský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Chodovská	chodovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Ostrovská	ostrovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Podloží	podloží	k1gNnSc1
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
pánve	pánev	k1gFnSc2
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
krystalinikem	krystalinikum	k1gNnSc7
<g/>
,	,	kIx,
zastoupeným	zastoupený	k2eAgNnSc7d1
metamorfovanými	metamorfovaný	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
rulami	rula	k1gFnPc7
a	a	k8xC
svory	svora	k1gFnSc2
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
pánve	pánev	k1gFnSc2
žulami	žula	k1gFnPc7
karlovarského	karlovarský	k2eAgInSc2d1
plutonu	pluton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
vyplněna	vyplnit	k5eAaPmNgFnS
třetihorními	třetihorní	k2eAgInPc7d1
(	(	kIx(
<g/>
kenozoickými	kenozoický	k2eAgInPc7d1
<g/>
)	)	kIx)
jezerními	jezerní	k2eAgInPc7d1
sedimenty	sediment	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Geneze	geneze	k1gFnSc1
pánve	pánev	k1gFnSc2
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
příkopovou	příkopový	k2eAgFnSc4d1
propadlinu	propadlina	k1gFnSc4
vzniklou	vzniklý	k2eAgFnSc4d1
v	v	k7c6
ose	osa	k1gFnSc6
podkrušnohorského	podkrušnohorský	k2eAgInSc2d1
prolomu	prolom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
vysokými	vysoký	k2eAgInPc7d1
a	a	k8xC
příkrými	příkrý	k2eAgInPc7d1
svahy	svah	k1gInPc7
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
a	a	k8xC
její	její	k3xOp3gInSc1
reliéf	reliéf	k1gInSc1
má	mít	k5eAaImIp3nS
mírně	mírně	k6eAd1
zvlněný	zvlněný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
vyplněna	vyplnit	k5eAaPmNgFnS
jezerními	jezerní	k2eAgInPc7d1
sedimenty	sediment	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
prostorově	prostorově	k6eAd1
a	a	k8xC
geneticky	geneticky	k6eAd1
úzce	úzko	k6eAd1
svázána	svázat	k5eAaPmNgFnS
s	s	k7c7
vulkanismem	vulkanismus	k1gInSc7
<g/>
,	,	kIx,
vulkanogenní	vulkanogenní	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
složení	složení	k1gNnSc6
terciérní	terciérní	k2eAgFnSc2d1
výplně	výplň	k1gFnSc2
pánve	pánev	k1gFnSc2
podílejí	podílet	k5eAaImIp3nP
asi	asi	k9
55	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
vulkanického	vulkanický	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
byly	být	k5eAaImAgInP
vulkanické	vulkanický	k2eAgInPc1d1
systémy	systém	k1gInPc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Doupovských	Doupovský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
izolovaná	izolovaný	k2eAgNnPc1d1
vulkanická	vulkanický	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
na	na	k7c6
území	území	k1gNnSc6
sokolovské	sokolovský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
a	a	k8xC
přilehlé	přilehlý	k2eAgFnSc2d1
části	část	k1gFnSc2
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Litostratigrafie	Litostratigrafie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
přilehlých	přilehlý	k2eAgInPc2d1
svahů	svah	k1gInPc2
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
-	-	kIx~
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
přilehlých	přilehlý	k2eAgInPc2d1
svahů	svah	k1gInPc2
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
-	-	kIx~
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Krušné	krušný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
povrchový	povrchový	k2eAgInSc4d1
lom	lom	k1gInSc4
Jiří	Jiří	k1gMnSc1
</s>
<s>
Za	za	k7c7
poznáním	poznání	k1gNnSc7
stratigrafie	stratigrafie	k1gFnSc2
sokolovské	sokolovský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
stojí	stát	k5eAaImIp3nS
zkušenost	zkušenost	k1gFnSc1
mnoha	mnoho	k4c2
generací	generace	k1gFnPc2
odborníků	odborník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
stratigrafické	stratigrafický	k2eAgNnSc4d1
členění	členění	k1gNnSc4
na	na	k7c6
základě	základ	k1gInSc6
vrstevního	vrstevní	k2eAgInSc2d1
sledu	sled	k1gInSc2
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
Českou	český	k2eAgFnSc7d1
stratigrafickou	stratigrafický	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Litostratigraficky	Litostratigraficky	k6eAd1
se	se	k3xPyFc4
sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
člení	členit	k5eAaImIp3nS
na	na	k7c4
4	#num#	k4
základní	základní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
–	–	k?
souvrství	souvrství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Starosedelské	Starosedelský	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
</s>
<s>
Starosedelské	Starosedelský	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
nejstarší	starý	k2eAgInPc4d3
terciérní	terciérní	k2eAgInPc4d1
sedimenty	sediment	k1gInPc4
a	a	k8xC
nemá	mít	k5eNaImIp3nS
úzký	úzký	k2eAgInSc4d1
vztah	vztah	k1gInSc4
k	k	k7c3
tektonické	tektonický	k2eAgFnSc3d1
struktuře	struktura	k1gFnSc3
sokolovské	sokolovský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
uložení	uložení	k1gNnSc3
sedimentů	sediment	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
před	před	k7c7
koncem	konec	k1gInSc7
eocénu	eocén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvrství	souvrství	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
kaolinitickými	kaolinitický	k2eAgInPc7d1
jíly	jíl	k1gInPc7
<g/>
,	,	kIx,
písky	písek	k1gInPc7
<g/>
,	,	kIx,
štěrky	štěrk	k1gInPc7
<g/>
,	,	kIx,
pískovci	pískovec	k1gInPc7
<g/>
,	,	kIx,
slepenci	slepenec	k1gInPc7
a	a	k8xC
křemenci	křemenec	k1gInPc7
<g/>
,	,	kIx,
mocnost	mocnost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
40	#num#	k4
m.	m.	k?
V	v	k7c6
sedimentech	sediment	k1gInPc6
starosedelského	starosedelský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
se	se	k3xPyFc4
se	se	k3xPyFc4
hojně	hojně	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
fosilní	fosilní	k2eAgFnSc1d1
flóra	flóra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
fosilní	fosilní	k2eAgInPc4d1
pozůstatky	pozůstatek	k1gInPc4
třetihorní	třetihorní	k2eAgFnSc2d1
flóry	flóra	k1gFnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vidět	vidět	k5eAaImF
zejména	zejména	k9
v	v	k7c6
evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnSc3d1
lokalitě	lokalita	k1gFnSc3
Pískovna	pískovna	k1gFnSc1
Erika	Erika	k1gFnSc1
a	a	k8xC
přírodní	přírodní	k2eAgFnSc3d1
památce	památka	k1gFnSc3
Údolí	údolí	k1gNnSc2
Ohře	Ohře	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
zajímavé	zajímavý	k2eAgInPc4d1
pseudokrasové	pseudokrasový	k2eAgInPc4d1
útvary	útvar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Novosedelské	novosedelský	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
oligocenního	oligocenní	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
a	a	k8xC
ukládání	ukládání	k1gNnSc6
sedimentů	sediment	k1gInPc2
probíhalo	probíhat	k5eAaImAgNnS
již	již	k9
za	za	k7c4
tektonické	tektonický	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocnost	mocnost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
85	#num#	k4
m.	m.	k?
V	v	k7c6
souvrství	souvrství	k1gNnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
uhelné	uhelný	k2eAgFnSc2d1
lávky	lávka	k1gFnSc2
sloje	sloj	k1gFnSc2
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
sedimentaci	sedimentace	k1gFnSc4
provázela	provázet	k5eAaImAgFnS
stupňující	stupňující	k2eAgFnSc1d1
se	se	k3xPyFc4
vulkanická	vulkanický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dokládají	dokládat	k5eAaImIp3nP
vložky	vložka	k1gFnPc4
tufů	tuf	k1gInPc2
a	a	k8xC
tufitů	tufit	k1gInPc2
i	i	k9
složení	složení	k1gNnSc4
popelovin	popelovina	k1gFnPc2
v	v	k7c6
uhlí	uhlí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sokolovské	sokolovský	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
</s>
<s>
Odráží	odrážet	k5eAaImIp3nS
období	období	k1gNnSc4
intenzivní	intenzivní	k2eAgFnSc2d1
extenze	extenze	k1gFnSc2
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
spojené	spojený	k2eAgInPc4d1
s	s	k7c7
vulkanismem	vulkanismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukládání	ukládání	k1gNnSc1
vrstev	vrstva	k1gFnPc2
již	již	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
v	v	k7c6
tektonických	tektonický	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
sokolovské	sokolovský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáří	stáří	k1gNnSc1
sokolovského	sokolovský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
22,8	22,8	k4
<g/>
–	–	k?
<g/>
21,3	21,3	k4
Ma	Ma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společným	společný	k2eAgInSc7d1
znakem	znak	k1gInSc7
sokolovského	sokolovský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
je	být	k5eAaImIp3nS
mnohonásobné	mnohonásobný	k2eAgNnSc4d1
opakování	opakování	k1gNnSc4
hornin	hornina	k1gFnPc2
vulkanického	vulkanický	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
sedimentů	sediment	k1gInPc2
ukládaných	ukládaný	k2eAgInPc2d1
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
tektonicky	tektonicky	k6eAd1
vyvolané	vyvolaný	k2eAgFnPc1d1
subsidence	subsidence	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvrství	souvrství	k1gNnSc1
dosahuje	dosahovat	k5eAaImIp3nS
mocnosti	mocnost	k1gFnPc4
až	až	k9
300	#num#	k4
m	m	kA
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
hlavní	hlavní	k2eAgInPc4d1
uhelné	uhelný	k2eAgInPc4d1
sloje	sloj	k1gInPc4
Anežka	Anežka	k1gFnSc1
a	a	k8xC
Antonín	Antonín	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
od	od	k7c2
sebe	se	k3xPyFc2
odděleny	oddělit	k5eAaPmNgInP
habartovskými	habartovský	k2eAgFnPc7d1
nebo	nebo	k8xC
těšovickými	těšovický	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Cyprisové	Cyprisový	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
</s>
<s>
Téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc1d1
cyprisové	cyprisový	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
bitumenními	bitumenní	k2eAgInPc7d1
jílovci	jílovec	k1gInPc7
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
světle	světle	k6eAd1
hnědé	hnědý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
lístkovitou	lístkovitý	k2eAgFnSc7d1
odlučností	odlučnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezeně	omezeně	k6eAd1
<g/>
,	,	kIx,
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
písky	písek	k1gInPc1
<g/>
,	,	kIx,
pískovce	pískovec	k1gInPc1
a	a	k8xC
slepence	slepenec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
cyprisové	cyprisový	k2eAgNnSc4d1
souvrství	souvrství	k1gNnSc4
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
jemně	jemně	k6eAd1
rozptýlená	rozptýlený	k2eAgFnSc1d1
příměs	příměs	k1gFnSc1
karbonátů	karbonát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
organického	organický	k2eAgInSc2d1
uhlíku	uhlík	k1gInSc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
několik	několik	k4yIc4
procent	procento	k1gNnPc2
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
až	až	k9
18	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocnost	mocnost	k1gFnSc1
souvrství	souvrství	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
180	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hydrologie	hydrologie	k1gFnSc1
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3
vodním	vodní	k2eAgInSc7d1
tokem	tok	k1gInSc7
řeka	řeka	k1gFnSc1
Ohře	Ohře	k1gFnSc1
<g/>
,	,	kIx,
pramenící	pramenící	k2eAgFnSc1d1
ve	v	k7c6
Smrčinách	Smrčiny	k1gFnPc6
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
protéká	protékat	k5eAaImIp3nS
územím	území	k1gNnSc7
přibližně	přibližně	k6eAd1
od	od	k7c2
západu	západ	k1gInSc2
k	k	k7c3
východu	východ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
nejzápadnějšího	západní	k2eAgInSc2d3
úseku	úsek	k1gInSc2
Ohře	Ohře	k1gFnSc2
v	v	k7c6
pánvi	pánev	k1gFnSc6
zasahuje	zasahovat	k5eAaImIp3nS
evropsky	evropsky	k6eAd1
významná	významný	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
Ramena	rameno	k1gNnSc2
Ohře	Ohře	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
Sokolovem	Sokolovo	k1gNnSc7
byla	být	k5eAaImAgFnS
řeka	řeka	k1gFnSc1
svedena	sveden	k2eAgFnSc1d1
do	do	k7c2
uměle	uměle	k6eAd1
upravených	upravený	k2eAgNnPc2d1
a	a	k8xC
přeložených	přeložený	k2eAgNnPc2d1
říčních	říční	k2eAgNnPc2d1
koryt	koryto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
význačnější	význačný	k2eAgInPc4d2
přítoky	přítok	k1gInPc4
patří	patřit	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Svatava	Svatava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
a	a	k8xC
Lobezský	Lobezský	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
pramenící	pramenící	k2eAgFnSc4d1
ve	v	k7c6
Slavkovském	slavkovský	k2eAgInSc6d1
lese	les	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
rekultivací	rekultivace	k1gFnSc7
a	a	k8xC
revitalizací	revitalizace	k1gFnSc7
území	území	k1gNnSc2
zasaženého	zasažený	k2eAgNnSc2d1
povrchovou	povrchový	k2eAgFnSc7d1
těžbou	těžba	k1gFnSc7
uhlí	uhlí	k1gNnSc1
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
několik	několik	k4yIc1
významných	významný	k2eAgFnPc2d1
nových	nový	k2eAgFnPc2d1
vodních	vodní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostoru	prostor	k1gInSc6
vytěženého	vytěžený	k2eAgInSc2d1
lomu	lom	k1gInSc2
Medard	Medard	k1gMnSc1
a	a	k8xC
lomu	lom	k1gInSc2
Libík	Libík	k1gInSc4
vzniklo	vzniknout	k5eAaPmAgNnS
jezero	jezero	k1gNnSc1
Medard	Medard	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
prostoru	prostor	k1gInSc6
bývalého	bývalý	k2eAgInSc2d1
lomu	lom	k1gInSc2
Michal	Michal	k1gMnSc1
pak	pak	k8xC
jezero	jezero	k1gNnSc1
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hornická	hornický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
První	první	k4xOgFnPc1
písemné	písemný	k2eAgFnPc1d1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
těžbě	těžba	k1gFnSc6
uhlí	uhlí	k1gNnSc2
v	v	k7c6
Sokolovské	sokolovský	k2eAgFnSc6d1
pánvi	pánev	k1gFnSc6
jsou	být	k5eAaImIp3nP
z	z	k7c2
roku	rok	k1gInSc2
1760	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
menší	malý	k2eAgFnPc1d2
těžby	těžba	k1gFnPc1
na	na	k7c6
okraji	okraj	k1gInSc6
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
vázány	vázat	k5eAaImNgFnP
na	na	k7c4
výchozy	výchoz	k1gInPc4
sloje	sloj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
většímu	veliký	k2eAgInSc3d2
rozsahu	rozsah	k1gInSc3
dobývání	dobývání	k1gNnSc2
uhlí	uhlí	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
prodloužena	prodloužit	k5eAaPmNgFnS
páteřní	páteřní	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
–	–	k?
Buštěhradská	buštěhradský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
z	z	k7c2
Chomutova	Chomutov	k1gInSc2
do	do	k7c2
Chebu	Cheb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
opravdovými	opravdový	k2eAgInPc7d1
začátky	začátek	k1gInPc7
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
byl	být	k5eAaImAgMnS
spjat	spjat	k2eAgMnSc1d1
Johann	Johann	k1gMnSc1
David	David	k1gMnSc1
Starck	Starck	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
provozu	provoz	k1gInSc6
celkem	celkem	k6eAd1
39	#num#	k4
hlubinných	hlubinný	k2eAgInPc2d1
dolů	dol	k1gInPc2
a	a	k8xC
15	#num#	k4
malolomů	malolom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
však	však	k9
těžba	těžba	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
k	k	k7c3
povrchovému	povrchový	k2eAgNnSc3d1
dobývání	dobývání	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
nabylo	nabýt	k5eAaPmAgNnS
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
uhelného	uhelný	k2eAgNnSc2d1
ložiska	ložisko	k1gNnSc2
je	být	k5eAaImIp3nS
již	již	k6eAd1
vytěžena	vytěžen	k2eAgFnSc1d1
<g/>
,	,	kIx,
těžba	těžba	k1gFnSc1
po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
uskutečňuje	uskutečňovat	k5eAaImIp3nS
lomem	lom	k1gInSc7
Jiří	Jiří	k1gMnSc2
a	a	k8xC
lomem	lom	k1gInSc7
Družba	družba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lom	lom	k1gInSc1
Družba	družba	k1gFnSc1
však	však	k9
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
dočasně	dočasně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
těžbu	těžba	k1gFnSc4
z	z	k7c2
důvodu	důvod	k1gInSc2
sesuvu	sesuv	k1gInSc2
vnitřní	vnitřní	k2eAgFnSc2d1
výsypky	výsypka	k1gFnSc2
lomu	lom	k1gInSc2
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
po	po	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
těžba	těžba	k1gFnSc1
uhlí	uhlí	k1gNnSc2
pokračovat	pokračovat	k5eAaImF
až	až	k9
do	do	k7c2
vyuhlení	vyuhlení	k1gNnSc2
zásob	zásoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JISKRA	jiskra	k1gFnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
hornictví	hornictví	k1gNnSc2
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Bodrov	Bodrov	k1gInSc1
–	–	k?
studio	studio	k1gNnSc1
OKO	oko	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
7338	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ROJÍK	rojík	k1gInSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
stratigraphic	stratigraphice	k1gFnPc2
subdivision	subdivision	k1gInSc4
of	of	k?
the	the	k?
Tertiary	Tertiara	k1gFnSc2
in	in	k?
the	the	k?
Sokolov	Sokolov	k1gInSc1
Basin	Basin	k1gInSc1
in	in	k?
Northwestern	Northwestern	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Geological	Geological	k1gMnPc3
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
176	#num#	k4
<g/>
-	-	kIx~
<g/>
184	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEŠEK	Pešek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terciérní	terciérní	k2eAgFnPc4d1
pánve	pánev	k1gFnPc4
a	a	k8xC
ložiska	ložisko	k1gNnPc4
hnědého	hnědý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7075	#num#	k4
<g/>
-	-	kIx~
<g/>
759	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rojík	rojík	k1gInSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Návrh	návrh	k1gInSc1
stratigrafického	stratigrafický	k2eAgNnSc2d1
členění	členění	k1gNnSc2
terciéru	terciér	k1gInSc2
sokolovské	sokolovský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gMnSc1
Hnědé	hnědý	k2eAgNnSc4d1
uhlí	uhlí	k1gNnSc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
Most	most	k1gInSc1
<g/>
↑	↑	k?
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Sokolovské	sokolovský	k2eAgInPc4d1
uhelné	uhelný	k2eAgInPc4d1
a.s.	a.s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
Česka	Česko	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
geomorfologických	geomorfologický	k2eAgInPc2d1
celků	celek	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Turistický	turistický	k2eAgInSc4d1
portál	portál	k1gInSc4
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
–	–	k?
Sokolovská	sokolovský	k2eAgFnSc1d1
uhelná	uhelný	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
