<s>
Souostroví	souostroví	k1gNnSc1	souostroví
Madeira	Madeira	k1gFnSc1	Madeira
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
starým	starý	k2eAgMnPc3d1	starý
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
náhodou	náhodou	k6eAd1	náhodou
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
znovuobjeveno	znovuobjeven	k2eAgNnSc1d1	znovuobjeven
a	a	k8xC	a
osídleno	osídlen	k2eAgNnSc1d1	osídleno
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
námořníky	námořník	k1gMnPc7	námořník
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
<g/>
.	.	kIx.	.
</s>
