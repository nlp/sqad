<s>
Skotsko	Skotsko	k1gNnSc1	Skotsko
(	(	kIx(	(
<g/>
skotsky	skotsky	k6eAd1	skotsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
Scotland	Scotlando	k1gNnPc2	Scotlando
<g/>
,	,	kIx,	,
skotskou	skotský	k2eAgFnSc7d1	skotská
gaelštinou	gaelština	k1gFnSc7	gaelština
Alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
konstituční	konstituční	k2eAgFnSc2d1	konstituční
monarchie	monarchie	k1gFnSc2	monarchie
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
suchozemskou	suchozemský	k2eAgFnSc4d1	suchozemská
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
Skotsko	Skotsko	k1gNnSc1	Skotsko
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Severním	severní	k2eAgInSc6d1	severní
průlivem	průliv	k1gInSc7	průliv
a	a	k8xC	a
Irským	irský	k2eAgNnSc7d1	irské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
pevninu	pevnina	k1gFnSc4	pevnina
se	se	k3xPyFc4	se
Skotsko	Skotsko	k1gNnSc1	Skotsko
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
790	[number]	k4	790
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Orkneje	Orkneje	k1gFnPc1	Orkneje
<g/>
,	,	kIx,	,
Shetlandy	Shetlanda	k1gFnPc1	Shetlanda
a	a	k8xC	a
Hebridy	Hebridy	k1gFnPc1	Hebridy
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
městem	město	k1gNnSc7	město
Skotska	Skotsko	k1gNnSc2	Skotsko
je	být	k5eAaImIp3nS	být
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
evropských	evropský	k2eAgNnPc2d1	Evropské
finančních	finanční	k2eAgNnPc2d1	finanční
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
centrem	centrum	k1gNnSc7	centrum
skotského	skotský	k1gInSc2	skotský
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
Skotsko	Skotsko	k1gNnSc1	Skotsko
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
obchodních	obchodní	k2eAgInPc2d1	obchodní
<g/>
,	,	kIx,	,
intelektuálních	intelektuální	k2eAgInPc2d1	intelektuální
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
motorů	motor	k1gInPc2	motor
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
skotským	skotský	k2eAgNnSc7d1	skotské
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Glasgow	Glasgow	k1gNnSc1	Glasgow
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kdysi	kdysi	k6eAd1	kdysi
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
světových	světový	k2eAgFnPc2d1	světová
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
metropolí	metropol	k1gFnPc2	metropol
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
velké	velký	k2eAgFnSc2d1	velká
glasgowské	glasgowský	k2eAgFnSc2d1	Glasgowská
konurbace	konurbace	k1gFnSc2	konurbace
dominující	dominující	k2eAgInSc1d1	dominující
Lowlands	Lowlands	k1gInSc1	Lowlands
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
žije	žít	k5eAaImIp3nS	žít
5	[number]	k4	5
144	[number]	k4	144
200	[number]	k4	200
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
88	[number]	k4	88
%	%	kIx~	%
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
Skotové	Skot	k1gMnPc1	Skot
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
skotština	skotština	k1gFnSc1	skotština
a	a	k8xC	a
skotská	skotský	k2eAgFnSc1d1	skotská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
.	.	kIx.	.
</s>
<s>
Skotské	skotský	k2eAgNnSc1d1	skotské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1707	[number]	k4	1707
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
Anglickým	anglický	k2eAgNnSc7d1	anglické
královstvím	království	k1gNnSc7	království
(	(	kIx(	(
<g/>
v	v	k7c6	v
pouze	pouze	k6eAd1	pouze
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Anglickým	anglický	k2eAgNnSc7d1	anglické
královstvím	království	k1gNnSc7	království
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
unie	unie	k1gFnSc1	unie
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
Unii	unie	k1gFnSc4	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
schválením	schválení	k1gNnSc7	schválení
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
Unii	unie	k1gFnSc4	unie
parlamenty	parlament	k1gInPc1	parlament
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
protestům	protest	k1gInPc3	protest
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
je	být	k5eAaImIp3nS	být
Skotsko	Skotsko	k1gNnSc4	Skotsko
součástí	součást	k1gFnPc2	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
nezávislý	závislý	k2eNgInSc4d1	nezávislý
politický	politický	k2eAgInSc4d1	politický
systém	systém	k1gInSc4	systém
a	a	k8xC	a
vlastní	vlastní	k2eAgNnSc4d1	vlastní
soukromé	soukromý	k2eAgNnSc4d1	soukromé
i	i	k8xC	i
veřejné	veřejný	k2eAgNnSc4d1	veřejné
právo	právo	k1gNnSc4	právo
(	(	kIx(	(
<g/>
skotské	skotský	k2eAgNnSc4d1	skotské
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
skotského	skotský	k1gInSc2	skotský
právního	právní	k2eAgInSc2d1	právní
a	a	k8xC	a
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
systému	systém	k1gInSc2	systém
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
i	i	k9	i
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
Unie	unie	k1gFnSc2	unie
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
pokračování	pokračování	k1gNnSc3	pokračování
skotské	skotský	k2eAgFnSc2d1	skotská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
skotské	skotský	k2eAgFnSc2d1	skotská
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
a	a	k8xC	a
část	část	k1gFnSc1	část
Skotů	Skot	k1gMnPc2	Skot
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Skotskou	skotský	k2eAgFnSc7d1	skotská
národní	národní	k2eAgFnSc7d1	národní
stranou	strana	k1gFnSc7	strana
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
státní	státní	k2eAgFnSc4d1	státní
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
osadníci	osadník	k1gMnPc1	osadník
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Skotska	Skotsko	k1gNnSc2	Skotsko
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc4d1	ledová
asi	asi	k9	asi
před	před	k7c7	před
11000	[number]	k4	11000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vesnice	vesnice	k1gFnSc1	vesnice
podle	podle	k7c2	podle
vykopávek	vykopávka	k1gFnPc2	vykopávka
vznikaly	vznikat	k5eAaImAgFnP	vznikat
cca	cca	kA	cca
okolo	okolo	k7c2	okolo
6000	[number]	k4	6000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Průkazné	průkazný	k2eAgFnPc4d1	průkazná
písemnosti	písemnost	k1gFnPc4	písemnost
o	o	k7c6	o
Skotsku	Skotsko	k1gNnSc6	Skotsko
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
příchodu	příchod	k1gInSc2	příchod
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemi	zem	k1gFnSc4	zem
nazývali	nazývat	k5eAaImAgMnP	nazývat
Kaledonie	Kaledonie	k1gFnSc2	Kaledonie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
nebylo	být	k5eNaImAgNnS	být
díky	díky	k7c3	díky
silnému	silný	k2eAgInSc3d1	silný
odporu	odpor	k1gInSc3	odpor
zdejších	zdejší	k2eAgInPc2d1	zdejší
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
nikdy	nikdy	k6eAd1	nikdy
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
načas	načas	k6eAd1	načas
připojila	připojit	k5eAaPmAgFnS	připojit
pouze	pouze	k6eAd1	pouze
území	území	k1gNnSc4	území
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Hadriánova	Hadriánův	k2eAgInSc2d1	Hadriánův
valu	val	k1gInSc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
Skotska	Skotsko	k1gNnSc2	Skotsko
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
dalriadský	dalriadský	k2eAgMnSc1d1	dalriadský
král	král	k1gMnSc1	král
Kenneth	Kenneth	k1gMnSc1	Kenneth
Mac	Mac	kA	Mac
Alpin	alpinum	k1gNnPc2	alpinum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
843	[number]	k4	843
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Říši	říše	k1gFnSc4	říše
Piktů	Pikt	k1gInPc2	Pikt
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
království	království	k1gNnSc4	království
Alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
bezprostředního	bezprostřední	k2eAgMnSc2d1	bezprostřední
předchůdce	předchůdce	k1gMnSc2	předchůdce
Skotského	skotský	k2eAgNnSc2d1	skotské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
šesti	šest	k4xCc7	šest
severními	severní	k2eAgFnPc7d1	severní
provinciemi	provincie	k1gFnPc7	provincie
<g/>
,	,	kIx,	,
spravovanými	spravovaný	k2eAgInPc7d1	spravovaný
mormaery	mormaer	k1gInPc7	mormaer
a	a	k8xC	a
několika	několik	k4yIc7	několik
královstvími	království	k1gNnPc7	království
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Mormaerové	Mormaer	k1gMnPc1	Mormaer
i	i	k8xC	i
králové	král	k1gMnPc1	král
byli	být	k5eAaImAgMnP	být
podřízeni	podřídit	k5eAaPmNgMnP	podřídit
skotskému	skotský	k1gInSc3	skotský
velekráli	velekrále	k1gFnSc4	velekrále
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
skotského	skotský	k1gInSc2	skotský
velekrále	velekrála	k1gFnSc3	velekrála
Malcolma	Malcolm	k1gMnSc2	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
majetkových	majetkový	k2eAgInPc2d1	majetkový
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
v	v	k7c6	v
systému	systém	k1gInSc6	systém
následnictví	následnictví	k1gNnSc2	následnictví
<g/>
.	.	kIx.	.
</s>
<s>
Reorganizována	reorganizován	k2eAgFnSc1d1	reorganizována
byla	být	k5eAaImAgFnS	být
také	také	k9	také
skotská	skotský	k2eAgFnSc1d1	skotská
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
převzaté	převzatý	k2eAgInPc4d1	převzatý
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
anglického	anglický	k2eAgNnSc2d1	anglické
království	království	k1gNnSc2	království
muselo	muset	k5eAaImAgNnS	muset
Skotsko	Skotsko	k1gNnSc1	Skotsko
čelit	čelit	k5eAaImF	čelit
sílícímu	sílící	k2eAgInSc3d1	sílící
tlaku	tlak	k1gInSc3	tlak
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
značně	značně	k6eAd1	značně
vyčerpávaly	vyčerpávat	k5eAaImAgFnP	vyčerpávat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
William	William	k1gInSc1	William
Wallace	Wallace	k1gFnSc2	Wallace
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
táhl	táhnout	k5eAaImAgMnS	táhnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgInS	dobýt
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zvedl	zvednout	k5eAaPmAgMnS	zvednout
morálku	morálka	k1gFnSc4	morálka
vyčerpaným	vyčerpaný	k2eAgInPc3d1	vyčerpaný
Skotům	skot	k1gInPc3	skot
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
film	film	k1gInSc4	film
Statečné	statečný	k2eAgNnSc1d1	statečné
srdce	srdce	k1gNnSc1	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
osobou	osoba	k1gFnSc7	osoba
skotské	skotský	k2eAgFnSc2d1	skotská
historie	historie	k1gFnSc2	historie
byl	být	k5eAaImAgMnS	být
bezesporu	bezesporu	k9	bezesporu
Robert	Robert	k1gMnSc1	Robert
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
pokračující	pokračující	k2eAgMnSc1d1	pokračující
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
popraveného	popravený	k2eAgInSc2d1	popravený
Wallace	Wallace	k1gFnSc5	Wallace
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jinými	jiný	k2eAgInPc7d1	jiný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1707	[number]	k4	1707
došlo	dojít	k5eAaPmAgNnS	dojít
Aktem	akt	k1gInSc7	akt
o	o	k7c4	o
unii	unie	k1gFnSc4	unie
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
kombinací	kombinace	k1gFnSc7	kombinace
anglické	anglický	k2eAgFnSc2d1	anglická
a	a	k8xC	a
skotské	skotský	k2eAgFnSc2d1	skotská
vlajky	vlajka	k1gFnSc2	vlajka
vlajka	vlajka	k1gFnSc1	vlajka
britská	britský	k2eAgFnSc1d1	britská
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
Jack	Jack	k1gMnSc1	Jack
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1801	[number]	k4	1801
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Skotsko	Skotsko	k1gNnSc1	Skotsko
stalo	stát	k5eAaPmAgNnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
k	k	k7c3	k
několika	několik	k4yIc3	několik
povstáním	povstání	k1gNnPc3	povstání
proti	proti	k7c3	proti
vládnoucím	vládnoucí	k2eAgMnPc3d1	vládnoucí
králům	král	k1gMnPc3	král
z	z	k7c2	z
hannoverské	hannoverský	k2eAgFnSc2d1	hannoverská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgFnPc2	tento
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
dosadit	dosadit	k5eAaPmF	dosadit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
členy	člen	k1gMnPc4	člen
katolické	katolický	k2eAgFnSc2d1	katolická
stuartovské	stuartovský	k2eAgFnSc2d1	stuartovská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Jakuba	Jakub	k1gMnSc2	Jakub
Františka	František	k1gMnSc2	František
Stuarta	Stuart	k1gMnSc2	Stuart
(	(	kIx(	(
<g/>
povstání	povstání	k1gNnSc2	povstání
1715	[number]	k4	1715
-	-	kIx~	-
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Eduarda	Eduard	k1gMnSc2	Eduard
Stuarta	Stuart	k1gMnSc2	Stuart
(	(	kIx(	(
<g/>
1745	[number]	k4	1745
-	-	kIx~	-
1746	[number]	k4	1746
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnPc1	povstání
byla	být	k5eAaImAgNnP	být
krutě	krutě	k6eAd1	krutě
potlačena	potlačit	k5eAaPmNgNnP	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
vliv	vliv	k1gInSc4	vliv
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
na	na	k7c4	na
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
vzrůst	vzrůst	k1gInSc4	vzrůst
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Obchody	obchod	k1gInPc1	obchod
se	s	k7c7	s
zámořím	zámoří	k1gNnSc7	zámoří
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zboží	zboží	k1gNnSc2	zboží
napomohly	napomoct	k5eAaPmAgFnP	napomoct
Skotsku	Skotsko	k1gNnSc6	Skotsko
k	k	k7c3	k
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
prosperitě	prosperita	k1gFnSc3	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
také	také	k9	také
objevení	objevení	k1gNnSc1	objevení
ropných	ropný	k2eAgNnPc2d1	ropné
nalezišť	naleziště	k1gNnPc2	naleziště
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
znovuobnovení	znovuobnovení	k1gNnSc6	znovuobnovení
Skotského	skotský	k2eAgInSc2d1	skotský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Skotsko	Skotsko	k1gNnSc1	Skotsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
třetině	třetina	k1gFnSc6	třetina
ostrova	ostrov	k1gInSc2	ostrov
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c4	na
78	[number]	k4	78
772	[number]	k4	772
čtverečních	čtvereční	k2eAgInPc6d1	čtvereční
kilometrech	kilometr	k1gInPc6	kilometr
(	(	kIx(	(
<g/>
30	[number]	k4	30
414	[number]	k4	414
mil2	mil2	k4	mil2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevninskou	pevninský	k2eAgFnSc4d1	pevninská
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
96	[number]	k4	96
kilometrovou	kilometrový	k2eAgFnSc7d1	kilometrová
mezi	mezi	k7c7	mezi
řekou	řeka	k1gFnSc7	řeka
Tweed	tweed	k1gInSc1	tweed
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
zálivem	záliv	k1gInSc7	záliv
Solway	Solwaa	k1gFnSc2	Solwaa
Firth	Firth	k1gInSc1	Firth
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
po	po	k7c6	po
Irském	irský	k2eAgNnSc6d1	irské
moři	moře	k1gNnSc6	moře
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
400	[number]	k4	400
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
přes	přes	k7c4	přes
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Islandem	Island	k1gInSc7	Island
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
souostroví	souostroví	k1gNnSc2	souostroví
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
západ	západ	k1gInSc4	západ
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
skotského	skotský	k2eAgNnSc2d1	skotské
území	území	k1gNnSc2	území
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgInPc1d3	veliký
Shetlandské	Shetlandský	k2eAgInPc1d1	Shetlandský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Orkneje	Orkneje	k1gFnPc1	Orkneje
a	a	k8xC	a
Hebridy	Hebridy	k1gFnPc1	Hebridy
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
ostrovy	ostrov	k1gInPc7	ostrov
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
Mainland	Mainland	k1gInSc4	Mainland
(	(	kIx(	(
<g/>
Shetlandy	Shetlanda	k1gFnSc2	Shetlanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gInSc1	Lewis
<g/>
,	,	kIx,	,
Skye	Skye	k1gInSc1	Skye
a	a	k8xC	a
Mull	Mull	k1gInSc1	Mull
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
je	být	k5eAaImIp3nS	být
Skotská	skotský	k2eAgFnSc1d1	skotská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
Highlands	Highlands	k1gInSc1	Highlands
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celého	celý	k2eAgNnSc2d1	celé
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Highlands	Highlands	k6eAd1	Highlands
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kamenitý	kamenitý	k2eAgInSc1d1	kamenitý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
horských	horský	k2eAgNnPc2d1	horské
údolí	údolí	k1gNnPc2	údolí
(	(	kIx(	(
<g/>
glens	glens	k6eAd1	glens
<g/>
)	)	kIx)	)
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
lochs	lochs	k1gInSc1	lochs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgNnSc1d1	ledové
pod	pod	k7c7	pod
souvislou	souvislý	k2eAgFnSc7d1	souvislá
vrstvou	vrstva	k1gFnSc7	vrstva
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Highland	Highlanda	k1gFnPc2	Highlanda
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
jih	jih	k1gInSc4	jih
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
úrodnější	úrodný	k2eAgFnSc2d2	úrodnější
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
usazováním	usazování	k1gNnSc7	usazování
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
nahromaděné	nahromaděný	k2eAgInPc1d1	nahromaděný
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
částech	část	k1gFnPc6	část
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
částech	část	k1gFnPc6	část
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
území	území	k1gNnSc4	území
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kolem	kolem	k7c2	kolem
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Edinburgu	Edinburg	k1gInSc2	Edinburg
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
anglickým	anglický	k2eAgFnPc3d1	anglická
hranicím	hranice	k1gFnPc3	hranice
je	být	k5eAaImIp3nS	být
hojnější	hojný	k2eAgNnSc1d2	hojnější
výskyt	výskyt	k1gInSc4	výskyt
lesního	lesní	k2eAgInSc2d1	lesní
porostu	porost	k1gInSc2	porost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
důsledkem	důsledek	k1gInSc7	důsledek
mýcení	mýcení	k1gNnSc2	mýcení
hlavně	hlavně	k6eAd1	hlavně
během	během	k7c2	během
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
země	zem	k1gFnSc2	zem
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Greenock	Greenock	k1gInSc1	Greenock
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gNnSc1	Glasgow
<g/>
,	,	kIx,	,
Stirling	Stirling	k1gInSc1	Stirling
a	a	k8xC	a
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Střední	střední	k2eAgInSc1d1	střední
pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
řeky	řeka	k1gFnPc4	řeka
patří	patřit	k5eAaImIp3nS	patřit
Clyde	Clyd	k1gInSc5	Clyd
a	a	k8xC	a
Forth	Fortha	k1gFnPc2	Fortha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
horských	horský	k2eAgFnPc6d1	horská
řekách	řeka	k1gFnPc6	řeka
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
lososům	losos	k1gMnPc3	losos
-	-	kIx~	-
národní	národní	k2eAgFnSc6d1	národní
rybě	ryba	k1gFnSc6	ryba
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jezer	jezero	k1gNnPc2	jezero
Skotska	Skotsko	k1gNnSc2	Skotsko
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgInSc4d3	veliký
Loch	loch	k1gInSc4	loch
Lomond	Lomonda	k1gFnPc2	Lomonda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
,	,	kIx,	,
Loch	loch	k1gInSc4	loch
Ness	Nessa	k1gFnPc2	Nessa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
hlubinách	hlubina	k1gFnPc6	hlubina
prý	prý	k9	prý
žije	žít	k5eAaImIp3nS	žít
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
příšera	příšera	k1gFnSc1	příšera
(	(	kIx(	(
<g/>
Lochneska	lochneska	k1gFnSc1	lochneska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
existenci	existence	k1gFnSc4	existence
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
Loch	loch	k1gInSc1	loch
Awe	Awe	k1gFnSc2	Awe
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Kategorie	kategorie	k1gFnSc2	kategorie
<g/>
:	:	kIx,	:
<g/>
Jezera	jezero	k1gNnSc2	jezero
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ovlivněno	ovlivněn	k2eAgNnSc1d1	ovlivněno
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
panují	panovat	k5eAaImIp3nP	panovat
mírné	mírný	k2eAgFnPc1d1	mírná
ale	ale	k8xC	ale
vlhké	vlhký	k2eAgFnPc1d1	vlhká
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
chladnější	chladný	k2eAgNnPc1d2	chladnější
léta	léto	k1gNnPc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
(	(	kIx(	(
<g/>
podzim-jaro	podzimara	k1gFnSc5	podzim-jara
<g/>
)	)	kIx)	)
s	s	k7c7	s
jasným	jasný	k2eAgInSc7d1	jasný
svitem	svit	k1gInSc7	svit
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
kroupami	kroupa	k1gFnPc7	kroupa
i	i	k8xC	i
sněhem	sníh	k1gInSc7	sníh
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
maximální	maximální	k2eAgFnPc1d1	maximální
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
okolo	okolo	k7c2	okolo
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
okolo	okolo	k7c2	okolo
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Skotsko	Skotsko	k1gNnSc1	Skotsko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
32	[number]	k4	32
správních	správní	k2eAgFnPc2d1	správní
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
unitary	unitar	k1gInPc1	unitar
authorities	authoritiesa	k1gFnPc2	authoritiesa
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
Skotska	Skotsko	k1gNnSc2	Skotsko
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
tradici	tradice	k1gFnSc4	tradice
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
průmysl	průmysl	k1gInSc4	průmysl
těžký	těžký	k2eAgInSc4d1	těžký
-	-	kIx~	-
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
rud	ruda	k1gFnPc2	ruda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dřevařským	dřevařský	k2eAgInSc7d1	dřevařský
a	a	k8xC	a
textilním	textilní	k2eAgInSc7d1	textilní
průmyslem	průmysl	k1gInSc7	průmysl
byly	být	k5eAaImAgInP	být
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
z	z	k7c2	z
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
byl	být	k5eAaImAgInS	být
chod	chod	k1gInSc1	chod
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
omezen	omezit	k5eAaPmNgInS	omezit
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
objevena	objeven	k2eAgNnPc1d1	objeveno
naleziště	naleziště	k1gNnPc1	naleziště
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
rozkvět	rozkvět	k1gInSc1	rozkvět
těžebního	těžební	k2eAgInSc2d1	těžební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těžařství	těžařství	k1gNnSc2	těžařství
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rychle	rychle	k6eAd1	rychle
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
lehký	lehký	k2eAgInSc4d1	lehký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
finančnictví	finančnictví	k1gNnSc4	finančnictví
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gNnSc4	on
Skotsko	Skotsko	k1gNnSc4	Skotsko
významným	významný	k2eAgMnSc7d1	významný
výrobcem	výrobce	k1gMnSc7	výrobce
elektronických	elektronický	k2eAgInPc2d1	elektronický
komponentů	komponent	k1gInPc2	komponent
a	a	k8xC	a
města	město	k1gNnSc2	město
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
a	a	k8xC	a
Glasgow	Glasgow	k1gInSc4	Glasgow
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
finančních	finanční	k2eAgFnPc2d1	finanční
center	centrum	k1gNnPc2	centrum
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřuje	směřovat	k5eAaImIp3nS	směřovat
nejvíc	hodně	k6eAd3	hodně
exportu	export	k1gInSc2	export
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
turistika	turistika	k1gFnSc1	turistika
a	a	k8xC	a
rozkvetly	rozkvetnout	k5eAaPmAgFnP	rozkvetnout
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
služby	služba	k1gFnSc2	služba
související	související	k2eAgInPc1d1	související
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
25	[number]	k4	25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
74	[number]	k4	74
%	%	kIx~	%
a	a	k8xC	a
zbylé	zbylý	k2eAgInPc1d1	zbylý
1	[number]	k4	1
%	%	kIx~	%
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc3d1	vysoká
úrovni	úroveň	k1gFnSc3	úroveň
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
farmářském	farmářský	k2eAgInSc6d1	farmářský
způsobu	způsob	k1gInSc6	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Fife	Fif	k1gInSc2	Fif
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
daří	dařit	k5eAaImIp3nS	dařit
bramborám	brambora	k1gFnPc3	brambora
<g/>
,	,	kIx,	,
obilninám	obilnina	k1gFnPc3	obilnina
<g/>
,	,	kIx,	,
mrkvi	mrkev	k1gFnSc3	mrkev
a	a	k8xC	a
kořenové	kořenový	k2eAgFnSc3d1	kořenová
zelenině	zelenina	k1gFnSc3	zelenina
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
kapustě	kapusta	k1gFnSc6	kapusta
<g/>
,	,	kIx,	,
brukvi	brukev	k1gFnSc6	brukev
<g/>
,	,	kIx,	,
i	i	k9	i
další	další	k2eAgFnSc3d1	další
košťálové	košťálový	k2eAgFnSc3d1	košťálová
zelenině	zelenina	k1gFnSc3	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
četné	četný	k2eAgFnPc1d1	četná
jsou	být	k5eAaImIp3nP	být
farmy	farma	k1gFnPc1	farma
specializující	specializující	k2eAgFnPc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
jahody	jahoda	k1gFnPc4	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
výrobou	výroba	k1gFnSc7	výroba
je	být	k5eAaImIp3nS	být
neodlučitelně	odlučitelně	k6eNd1	odlučitelně
spojena	spojit	k5eAaPmNgFnS	spojit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
tradičních	tradiční	k2eAgInPc2d1	tradiční
skotských	skotský	k1gInPc2	skotský
plemen	plemeno	k1gNnPc2	plemeno
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgFnPc1d1	bohatá
zkušenosti	zkušenost	k1gFnPc1	zkušenost
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
oblastmi	oblast	k1gFnPc7	oblast
bohatými	bohatý	k2eAgFnPc7d1	bohatá
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
Skotsko	Skotsko	k1gNnSc1	Skotsko
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
odvětví	odvětví	k1gNnSc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
hlavně	hlavně	k9	hlavně
lososi	losos	k1gMnPc1	losos
<g/>
,	,	kIx,	,
pstruzi	pstruh	k1gMnPc1	pstruh
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc1	štika
a	a	k8xC	a
úhoři	úhoř	k1gMnPc1	úhoř
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
pak	pak	k6eAd1	pak
celá	celý	k2eAgFnSc1d1	celá
škála	škála	k1gFnSc1	škála
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
treska	treska	k1gFnSc1	treska
<g/>
,	,	kIx,	,
sleď	sleď	k1gMnSc1	sleď
<g/>
,	,	kIx,	,
šprot	šprot	k1gInSc1	šprot
<g/>
,	,	kIx,	,
štikozubec	štikozubec	k1gMnSc1	štikozubec
a	a	k8xC	a
platýs	platýs	k1gMnSc1	platýs
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
rybářské	rybářský	k2eAgInPc1d1	rybářský
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
Peterhead	Peterhead	k1gInSc4	Peterhead
a	a	k8xC	a
Fraseburgh	Fraseburgh	k1gInSc4	Fraseburgh
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
hlavně	hlavně	k9	hlavně
Aberdeen	Aberdeen	k1gInSc1	Aberdeen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Glasgow	Glasgow	k1gNnSc2	Glasgow
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
Aberdeen	Aberdeen	k1gInSc1	Aberdeen
<g/>
,	,	kIx,	,
Prestwick	Prestwick	k1gInSc1	Prestwick
(	(	kIx(	(
<g/>
letiště	letiště	k1gNnSc1	letiště
Glasgow-Prestwick	Glasgow-Prestwicka	k1gFnPc2	Glasgow-Prestwicka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Inverness	Inverness	k1gInSc4	Inverness
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgNnPc2	tento
letišť	letiště	k1gNnPc2	letiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dalších	další	k2eAgInPc2d1	další
deset	deset	k4xCc1	deset
lokálních	lokální	k2eAgInPc2d1	lokální
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
Flyglobespan	Flyglobespana	k1gFnPc2	Flyglobespana
<g/>
,	,	kIx,	,
Loganair	Loganaira	k1gFnPc2	Loganaira
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
Star	star	k1gFnSc2	star
Airlines	Airlinesa	k1gFnPc2	Airlinesa
a	a	k8xC	a
ScotAirways	ScotAirwaysa	k1gFnPc2	ScotAirwaysa
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
a	a	k8xC	a
spravována	spravovat	k5eAaImNgFnS	spravovat
samostatně	samostatně	k6eAd1	samostatně
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Společného	společný	k2eAgNnSc2d1	společné
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hustou	hustý	k2eAgFnSc4d1	hustá
síť	síť	k1gFnSc4	síť
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
dopravy	doprava	k1gFnSc2	doprava
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
i	i	k9	i
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Propojení	propojení	k1gNnSc1	propojení
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nejdůležitějšími	důležitý	k2eAgNnPc7d3	nejdůležitější
městy	město	k1gNnPc7	město
je	být	k5eAaImIp3nS	být
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
soukromých	soukromý	k2eAgFnPc2d1	soukromá
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
silnic	silnice	k1gFnPc2	silnice
je	být	k5eAaImIp3nS	být
nejvytíženější	vytížený	k2eAgFnSc1d3	nejvytíženější
dálnice	dálnice	k1gFnSc1	dálnice
M8	M8	k1gFnSc2	M8
mezi	mezi	k7c7	mezi
Glasgow	Glasgow	k1gNnSc7	Glasgow
a	a	k8xC	a
Edinburgem	Edinburg	k1gInSc7	Edinburg
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
M	M	kA	M
<g/>
74	[number]	k4	74
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
spojnicí	spojnice	k1gFnSc7	spojnice
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
lodních	lodní	k2eAgFnPc2d1	lodní
linek	linka	k1gFnPc2	linka
funguje	fungovat	k5eAaImIp3nS	fungovat
jak	jak	k6eAd1	jak
mezinárodně	mezinárodně	k6eAd1	mezinárodně
(	(	kIx(	(
<g/>
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Faerskými	Faerský	k2eAgInPc7d1	Faerský
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
Islandem	Island	k1gInSc7	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
lokálně	lokálně	k6eAd1	lokálně
(	(	kIx(	(
<g/>
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lokálních	lokální	k2eAgInPc2d1	lokální
spojů	spoj	k1gInPc2	spoj
funguje	fungovat	k5eAaImIp3nS	fungovat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Skotská	skotský	k2eAgFnSc1d1	skotská
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
Skotská	skotský	k2eAgFnSc1d1	skotská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
nejvíce	nejvíce	k6eAd1	nejvíce
vyjímá	vyjímat	k5eAaImIp3nS	vyjímat
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
gaelskou	gaelský	k2eAgFnSc7d1	gaelská
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
skotské	skotský	k2eAgFnPc4d1	skotská
dudy	dudy	k1gFnPc4	dudy
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
přesněji	přesně	k6eAd2	přesně
Great	Great	k2eAgInSc4d1	Great
Highland	Highland	k1gInSc4	Highland
Bagpipe	Bagpip	k1gInSc5	Bagpip
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
známým	známý	k2eAgInSc7d1	známý
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
ovšem	ovšem	k9	ovšem
ke	k	k7c3	k
klasickým	klasický	k2eAgFnPc3d1	klasická
skotským	skotský	k2eAgFnPc3d1	skotská
kapelám	kapela	k1gFnPc3	kapela
hrajícím	hrající	k2eAgFnPc3d1	hrající
Scottish	Scottish	k1gInSc4	Scottish
country	country	k2eAgInPc1d1	country
dance	danec	k1gInPc1	danec
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc4	akordeon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
harfa	harfa	k1gFnSc1	harfa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
modernějších	moderní	k2eAgNnPc2d2	modernější
seskupení	seskupení	k1gNnPc2	seskupení
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
elektronické	elektronický	k2eAgFnPc4d1	elektronická
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skotské	skotský	k2eAgFnSc3d1	skotská
hudbě	hudba	k1gFnSc3	hudba
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
skotský	skotský	k2eAgInSc1d1	skotský
tanec	tanec	k1gInSc1	tanec
(	(	kIx(	(
<g/>
scottish	scottish	k1gInSc1	scottish
dancing	dancing	k1gInSc1	dancing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezřídka	nezřídka	k6eAd1	nezřídka
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Skotska	Skotsko	k1gNnSc2	Skotsko
pořádány	pořádán	k2eAgInPc4d1	pořádán
festivaly	festival	k1gInPc4	festival
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
kulturní	kulturní	k2eAgInPc4d1	kulturní
večery	večer	k1gInPc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skotskou	skotský	k2eAgFnSc7d1	skotská
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
tancem	tanec	k1gInSc7	tanec
a	a	k8xC	a
tradičními	tradiční	k2eAgFnPc7d1	tradiční
skotskými	skotský	k2eAgFnPc7d1	skotská
sportovními	sportovní	k2eAgFnPc7d1	sportovní
disciplínami	disciplína	k1gFnPc7	disciplína
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
na	na	k7c4	na
Highland	Highland	k1gInSc4	Highland
games	gamesa	k1gFnPc2	gamesa
(	(	kIx(	(
<g/>
Skotské	skotský	k2eAgFnSc2d1	skotská
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skotské	skotský	k2eAgFnSc6d1	skotská
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
několik	několik	k4yIc4	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
skotská	skotský	k2eAgFnSc1d1	skotská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
skotština	skotština	k1gFnSc1	skotština
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
skotské	skotský	k2eAgMnPc4d1	skotský
literáty	literát	k1gMnPc4	literát
patří	patřit	k5eAaImIp3nS	patřit
Robert	Robert	k1gMnSc1	Robert
Burns	Burns	k1gInSc1	Burns
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Conan	Conany	k1gInPc2	Conany
Doyle	Doyle	k1gInSc1	Doyle
a	a	k8xC	a
Irvine	Irvin	k1gInSc5	Irvin
Welsh	Welsh	k1gInSc1	Welsh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhlasových	rozhlasový	k2eAgFnPc6d1	rozhlasová
vlnách	vlna	k1gFnPc6	vlna
lze	lze	k6eAd1	lze
naladit	naladit	k5eAaPmF	naladit
mnoho	mnoho	k4c4	mnoho
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
ryze	ryze	k6eAd1	ryze
skotské	skotský	k2eAgMnPc4d1	skotský
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc4	radio
Scotland	Scotland	k1gInSc1	Scotland
a	a	k8xC	a
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
nan	nan	k?	nan
Gaidheal	Gaidheal	k1gMnSc1	Gaidheal
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
kombinovaně	kombinovaně	k6eAd1	kombinovaně
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
ve	v	k7c6	v
skotské	skotský	k2eAgFnSc6d1	skotská
gaelštině	gaelština	k1gFnSc6	gaelština
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rozhlasu	rozhlas	k1gInSc2	rozhlas
vysílá	vysílat	k5eAaImIp3nS	vysílat
i	i	k9	i
televize	televize	k1gFnSc1	televize
BBC	BBC	kA	BBC
Scotland	Scotlanda	k1gFnPc2	Scotlanda
a	a	k8xC	a
komerční	komerční	k2eAgFnSc2d1	komerční
STV	STV	kA	STV
<g/>
.	.	kIx.	.
</s>
<s>
Skotské	skotský	k2eAgInPc1d1	skotský
deníky	deník	k1gInPc1	deník
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
Daily	Dail	k1gMnPc7	Dail
Record	Recorda	k1gFnPc2	Recorda
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Herald	Herald	k1gMnSc1	Herald
a	a	k8xC	a
The	The	k1gMnSc1	The
Scotsman	Scotsman	k1gMnSc1	Scotsman
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kultuře	kultura	k1gFnSc3	kultura
patří	patřit	k5eAaImIp3nS	patřit
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
i	i	k9	i
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
platí	platit	k5eAaImIp3nS	platit
dvojnásob	dvojnásobit	k5eAaImRp2nS	dvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Soutěživost	soutěživost	k1gFnSc1	soutěživost
Skotů	Skot	k1gMnPc2	Skot
je	být	k5eAaImIp3nS	být
veliká	veliký	k2eAgFnSc1d1	veliká
a	a	k8xC	a
tradiční	tradiční	k2eAgInPc4d1	tradiční
sporty	sport	k1gInPc4	sport
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
zemí	zem	k1gFnSc7	zem
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgInPc1d1	známý
<g/>
:	:	kIx,	:
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
ragby	ragby	k1gNnSc1	ragby
<g/>
,	,	kIx,	,
golf	golf	k1gInSc1	golf
<g/>
,	,	kIx,	,
koňské	koňský	k2eAgInPc1d1	koňský
dostihy	dostih	k1gInPc1	dostih
<g/>
,	,	kIx,	,
automobilové	automobilový	k2eAgInPc1d1	automobilový
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
tradiční	tradiční	k2eAgFnSc1d1	tradiční
rivalita	rivalita	k1gFnSc1	rivalita
klubů	klub	k1gInPc2	klub
Celtic	Celtice	k1gFnPc2	Celtice
Glasgow	Glasgow	k1gNnSc1	Glasgow
a	a	k8xC	a
Glasgow	Glasgow	k1gNnSc1	Glasgow
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
ragby	ragby	k1gNnSc2	ragby
pravidelně	pravidelně	k6eAd1	pravidelně
bojuje	bojovat	k5eAaImIp3nS	bojovat
skotský	skotský	k2eAgInSc1d1	skotský
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
o	o	k7c4	o
nejcennější	cenný	k2eAgFnPc4d3	nejcennější
trofeje	trofej	k1gFnPc4	trofej
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
golfová	golfový	k2eAgNnPc4d1	golfové
hřiště	hřiště	k1gNnPc4	hřiště
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
školství	školství	k1gNnSc2	školství
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
se	se	k3xPyFc4	se
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
bylo	být	k5eAaImAgNnS	být
Skotsko	Skotsko	k1gNnSc1	Skotsko
první	první	k4xOgFnSc2	první
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zavedlo	zavést	k5eAaPmAgNnS	zavést
školství	školství	k1gNnSc1	školství
do	do	k7c2	do
veřejného	veřejný	k2eAgInSc2d1	veřejný
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
nařizoval	nařizovat	k5eAaImAgInS	nařizovat
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
pro	pro	k7c4	pro
nejstarší	starý	k2eAgMnPc4d3	nejstarší
syny	syn	k1gMnPc4	syn
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
vlastníků	vlastník	k1gMnPc2	vlastník
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
se	se	k3xPyFc4	se
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
i	i	k9	i
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
dnech	den	k1gInPc6	den
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
mládež	mládež	k1gFnSc4	mládež
základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
v	v	k7c6	v
15	[number]	k4	15
až	až	k8xS	až
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
sérií	série	k1gFnSc7	série
zkoušek	zkouška	k1gFnPc2	zkouška
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgInSc1d1	maximální
počet	počet	k1gInSc1	počet
předmětů	předmět	k1gInPc2	předmět
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
povinné	povinný	k2eAgFnSc6d1	povinná
školní	školní	k2eAgFnSc6d1	školní
docházce	docházka	k1gFnSc6	docházka
si	se	k3xPyFc3	se
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
další	další	k2eAgInPc4d1	další
studijní	studijní	k2eAgInPc4d1	studijní
obory	obor	k1gInPc4	obor
na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
typech	typ	k1gInPc6	typ
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
zakončit	zakončit	k5eAaPmF	zakončit
studia	studio	k1gNnPc4	studio
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zkoušek	zkouška	k1gFnPc2	zkouška
:	:	kIx,	:
Access	Access	k1gInSc1	Access
exams	examsa	k1gFnPc2	examsa
<g/>
,	,	kIx,	,
Intermediate	Intermediat	k1gInSc5	Intermediat
exams	examsa	k1gFnPc2	examsa
nebo	nebo	k8xC	nebo
Higher	Highra	k1gFnPc2	Highra
Grade	grad	k1gInSc5	grad
a	a	k8xC	a
Advanced	Advanced	k1gInSc4	Advanced
Higher	Highra	k1gFnPc2	Highra
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
nebo	nebo	k8xC	nebo
používat	používat	k5eAaImF	používat
anglický	anglický	k2eAgInSc4d1	anglický
model	model	k1gInSc4	model
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
studia	studio	k1gNnPc1	studio
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
St.	st.	kA	st.
Andrews	Andrews	k1gInSc1	Andrews
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Glasgow	Glasgow	k1gNnSc1	Glasgow
a	a	k8xC	a
University	universita	k1gFnPc1	universita
of	of	k?	of
Aberdeen	Aberdeen	k1gInSc1	Aberdeen
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
dosažený	dosažený	k2eAgInSc1d1	dosažený
úspěšným	úspěšný	k2eAgNnSc7d1	úspěšné
složením	složení	k1gNnSc7	složení
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
po	po	k7c6	po
čtyřletém	čtyřletý	k2eAgNnSc6d1	čtyřleté
studiu	studio	k1gNnSc6	studio
je	být	k5eAaImIp3nS	být
Artium	Artium	k1gNnSc1	Artium
Baccalaureus	Baccalaureus	k1gInSc1	Baccalaureus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k9	jako
B.A.	B.A.	k1gFnSc1	B.A.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
A.B.	A.B.	k1gFnSc1	A.B.
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
lze	lze	k6eAd1	lze
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skotských	skotský	k2eAgFnPc6d1	skotská
univerzitách	univerzita	k1gFnPc6	univerzita
studuje	studovat	k5eAaImIp3nS	studovat
mnoho	mnoho	k4c1	mnoho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
.	.	kIx.	.
</s>
