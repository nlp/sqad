<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
městem	město	k1gNnSc7	město
Skotska	Skotsko	k1gNnSc2	Skotsko
je	být	k5eAaImIp3nS	být
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
evropských	evropský	k2eAgNnPc2d1	Evropské
finančních	finanční	k2eAgNnPc2d1	finanční
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
