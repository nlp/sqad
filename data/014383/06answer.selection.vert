<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
správní	správní	k2eAgInPc1d1	správní
sídla	sídlo	k1gNnSc2	sídlo
těchto	tento	k3xDgInPc2	tento
regionů	region	k1gInPc2	region
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nordjylland	Nordjylland	k1gInSc1	Nordjylland
(	(	kIx(	(
<g/>
Aalborg	Aalborg	k1gInSc1	Aalborg
<g/>
)	)	kIx)	)
Midtjylland	Midtjylland	k1gInSc1	Midtjylland
(	(	kIx(	(
<g/>
Viborg	Viborg	k1gInSc1	Viborg
<g/>
)	)	kIx)	)
Syddanmark	Syddanmark	k1gInSc1	Syddanmark
(	(	kIx(	(
<g/>
Vejle	vejl	k1gInSc5	vejl
<g/>
)	)	kIx)	)
Hovedstaden	Hovedstaden	k2eAgMnSc1d1	Hovedstaden
(	(	kIx(	(
<g/>
Hillerø	Hillerø	k1gMnSc1	Hillerø
<g/>
)	)	kIx)	)
Sjæ	Sjæ	k1gMnSc1	Sjæ
<g />
.	.	kIx.	.
</s>
