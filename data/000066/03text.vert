<s>
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
František	František	k1gMnSc1	František
Serafínský	serafínský	k2eAgMnSc1d1	serafínský
Werich	Werich	k1gMnSc1	Werich
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1905	[number]	k4	1905
Smíchov	Smíchov	k1gInSc1	Smíchov
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1980	[number]	k4	1980
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
v	v	k7c6	v
autorské	autorský	k2eAgFnSc6d1	autorská
trojici	trojice	k1gFnSc6	trojice
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Ježkem	Ježek	k1gMnSc7	Ježek
významný	významný	k2eAgMnSc1d1	významný
představitel	představitel	k1gMnSc1	představitel
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
avantgardy	avantgarda	k1gFnSc2	avantgarda
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
poválečné	poválečný	k2eAgFnSc2d1	poválečná
české	český	k2eAgFnSc2d1	Česká
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
byl	být	k5eAaImAgMnS	být
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Vratislava	Vratislav	k1gMnSc2	Vratislav
Wericha	Werich	k1gMnSc2	Werich
<g/>
,	,	kIx,	,
úředníka	úředník	k1gMnSc2	úředník
První	první	k4xOgFnSc2	první
české	český	k2eAgFnSc2d1	Česká
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
(	(	kIx(	(
<g/>
syna	syn	k1gMnSc2	syn
Františka	František	k1gMnSc2	František
Wericha	Werich	k1gMnSc2	Werich
<g/>
,	,	kIx,	,
obchodníka	obchodník	k1gMnSc2	obchodník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Reginy	Regina	k1gFnSc2	Regina
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Duchoňové	Duchoňová	k1gFnPc1	Duchoňová
z	z	k7c2	z
Hostomic	Hostomice	k1gFnPc2	Hostomice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Gabriely	Gabriela	k1gFnSc2	Gabriela
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Choděrové	Choděra	k1gMnPc1	Choděra
(	(	kIx(	(
<g/>
dcery	dcera	k1gFnSc2	dcera
Jana	Jan	k1gMnSc2	Jan
Choděry	Choděra	k1gFnSc2	Choděra
<g/>
,	,	kIx,	,
hostinského	hostinský	k2eAgInSc2d1	hostinský
v	v	k7c6	v
Žižkově	Žižkov	k1gInSc6	Žižkov
č.	č.	k?	č.
970	[number]	k4	970
<g/>
,	,	kIx,	,
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Zounkové	Zounek	k1gMnPc1	Zounek
z	z	k7c2	z
Malých	Malých	k2eAgFnPc2d1	Malých
Nehvizd	Nehvizda	k1gFnPc2	Nehvizda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
kmotry	kmotra	k1gFnSc2	kmotra
mu	on	k3xPp3gMnSc3	on
byli	být	k5eAaImAgMnP	být
prarodiče	prarodič	k1gMnPc1	prarodič
Regina	Regina	k1gFnSc1	Regina
Benešová-Werichová	Benešová-Werichová	k1gFnSc1	Benešová-Werichová
a	a	k8xC	a
František	František	k1gMnSc1	František
Choděra	Choděra	k1gFnSc1	Choděra
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
brzo	brzo	k6eAd1	brzo
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
připadl	připadnout	k5eAaPmAgMnS	připadnout
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
(	(	kIx(	(
<g/>
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
čp.	čp.	k?	čp.
403	[number]	k4	403
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
matčini	matčin	k2eAgMnPc1d1	matčin
rodiče	rodič	k1gMnPc1	rodič
hostinec	hostinec	k1gInSc1	hostinec
<g/>
;	;	kIx,	;
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
otec	otec	k1gMnSc1	otec
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
švadlenou	švadlena	k1gFnSc7	švadlena
a	a	k8xC	a
návrhářkou	návrhářka	k1gFnSc7	návrhářka
divadelních	divadelní	k2eAgInPc2d1	divadelní
kostýmů	kostým	k1gInPc2	kostým
Zdeňkou	Zdeňka	k1gFnSc7	Zdeňka
(	(	kIx(	(
<g/>
též	též	k9	též
Zdenou	Zdena	k1gFnSc7	Zdena
<g/>
)	)	kIx)	)
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Houskovou	houskový	k2eAgFnSc7d1	Housková
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přeložila	přeložit	k5eAaPmAgFnS	přeložit
dvě	dva	k4xCgFnPc4	dva
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
Jana	Jana	k1gFnSc1	Jana
Werichová	Werichová	k1gFnSc1	Werichová
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Fanča	Fanča	k1gFnSc1	Fanča
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Hulíková	Hulíková	k1gFnSc1	Hulíková
<g/>
,	,	kIx,	,
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
pedagogiku	pedagogika	k1gFnSc4	pedagogika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
psycholožkou	psycholožka	k1gFnSc7	psycholožka
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
studoval	studovat	k5eAaImAgMnS	studovat
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
Křemencově	křemencově	k6eAd1	křemencově
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
chemická	chemický	k2eAgFnSc1d1	chemická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
divadelním	divadelní	k2eAgMnSc7d1	divadelní
partnerem	partner	k1gMnSc7	partner
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
ročník	ročník	k1gInSc1	ročník
víceletého	víceletý	k2eAgNnSc2d1	víceleté
gymnázia	gymnázium	k1gNnSc2	gymnázium
však	však	k9	však
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
na	na	k7c6	na
smíchovském	smíchovský	k2eAgNnSc6d1	Smíchovské
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
dvojici	dvojice	k1gFnSc4	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
časopisu	časopis	k1gInSc2	časopis
Přerod	přerod	k1gInSc1	přerod
a	a	k8xC	a
brzo	brzo	k6eAd1	brzo
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
divadelní	divadelní	k2eAgFnSc1d1	divadelní
spolupráce	spolupráce	k1gFnSc1	spolupráce
v	v	k7c6	v
Osvobozeném	osvobozený	k2eAgNnSc6d1	osvobozené
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyústila	vyústit	k5eAaPmAgFnS	vyústit
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
první	první	k4xOgFnSc2	první
hry	hra	k1gFnSc2	hra
Vest	vesta	k1gFnPc2	vesta
pocket	pocket	k1gMnSc1	pocket
revue	revue	k1gFnSc2	revue
-	-	kIx~	-
názvem	název	k1gInSc7	název
mysleli	myslet	k5eAaImAgMnP	myslet
malou	malý	k2eAgFnSc4d1	malá
revui	revue	k1gFnSc4	revue
do	do	k7c2	do
kapsičky	kapsička	k1gFnSc2	kapsička
u	u	k7c2	u
vesty	vesta	k1gFnSc2	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Werich	Werich	k1gMnSc1	Werich
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
plakátech	plakát	k1gInPc6	plakát
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k9	jako
J.	J.	kA	J.
W.	W.	kA	W.
Rich	Rich	k1gMnSc1	Rich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
jejich	jejich	k3xOp3gFnSc2	jejich
divadelní	divadelní	k2eAgFnSc2d1	divadelní
spolupráce	spolupráce	k1gFnSc2	spolupráce
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Osvobozeném	osvobozený	k2eAgNnSc6d1	osvobozené
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
působili	působit	k5eAaImAgMnP	působit
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
tzv.	tzv.	kA	tzv.
Spoutaného	spoutaný	k2eAgNnSc2d1	spoutané
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedli	uvést	k5eAaPmAgMnP	uvést
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejodvážnějších	odvážný	k2eAgFnPc2d3	nejodvážnější
her	hra	k1gFnPc2	hra
Balada	balada	k1gFnSc1	balada
z	z	k7c2	z
hadrů	hadr	k1gInPc2	hadr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
k	k	k7c3	k
Osvobozenému	osvobozený	k2eAgNnSc3d1	osvobozené
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
a	a	k8xC	a
J.	J.	kA	J.
Werich	Werich	k1gMnSc1	Werich
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
hudební	hudební	k2eAgMnSc1d1	hudební
autor	autor	k1gMnSc1	autor
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
opustit	opustit	k5eAaPmF	opustit
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
strávili	strávit	k5eAaPmAgMnP	strávit
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
jako	jako	k9	jako
herci	herec	k1gMnPc1	herec
<g/>
;	;	kIx,	;
především	především	k6eAd1	především
mezi	mezi	k7c7	mezi
tamní	tamní	k2eAgFnSc7d1	tamní
českou	český	k2eAgFnSc7d1	Česká
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Proslavili	proslavit	k5eAaPmAgMnP	proslavit
se	se	k3xPyFc4	se
též	též	k9	též
svými	svůj	k3xOyFgInPc7	svůj
protinacistickými	protinacistický	k2eAgInPc7d1	protinacistický
pořady	pořad	k1gInPc7	pořad
pro	pro	k7c4	pro
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
stanici	stanice	k1gFnSc4	stanice
Hlas	hlas	k1gInSc4	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
vysílanými	vysílaný	k2eAgInPc7d1	vysílaný
česky	česky	k6eAd1	česky
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
osvobozeného	osvobozený	k2eAgNnSc2d1	osvobozené
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
se	se	k3xPyFc4	se
navrátil	navrátit	k5eAaPmAgMnS	navrátit
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
Divadlo	divadlo	k1gNnSc1	divadlo
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dělat	dělat	k5eAaImF	dělat
politickou	politický	k2eAgFnSc4d1	politická
satiru	satira	k1gFnSc4	satira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Werichův	Werichův	k2eAgMnSc1d1	Werichův
partner	partner	k1gMnSc1	partner
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
do	do	k7c2	do
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
pražských	pražský	k2eAgNnPc2d1	Pražské
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Divadla	divadlo	k1gNnSc2	divadlo
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Werich	Werich	k1gMnSc1	Werich
založil	založit	k5eAaPmAgMnS	založit
Divadlo	divadlo	k1gNnSc4	divadlo
Voskovce	Voskovec	k1gMnSc2	Voskovec
a	a	k8xC	a
Wericha	Werich	k1gMnSc2	Werich
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
novým	nový	k2eAgMnSc7d1	nový
partnerem	partner	k1gMnSc7	partner
stal	stát	k5eAaPmAgMnS	stát
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
divadelní	divadelní	k2eAgFnSc2d1	divadelní
tvorby	tvorba	k1gFnSc2	tvorba
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nejznámější	známý	k2eAgFnSc2d3	nejznámější
tzv.	tzv.	kA	tzv.
forbíny	forbína	k1gFnSc2	forbína
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
předscény	předscéna	k1gFnSc2	předscéna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poválečná	poválečný	k2eAgFnSc1d1	poválečná
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
režimem	režim	k1gInSc7	režim
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnPc1	jeho
kritici	kritik	k1gMnPc1	kritik
mu	on	k3xPp3gMnSc3	on
vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
nezastal	zastat	k5eNaPmAgMnS	zastat
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgInPc2	svůj
pronásledovaných	pronásledovaný	k2eAgInPc2d1	pronásledovaný
a	a	k8xC	a
vězněných	vězněný	k2eAgMnPc2d1	vězněný
kolegů	kolega	k1gMnPc2	kolega
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gMnPc1	jeho
obránci	obránce	k1gMnPc1	obránce
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
Werichův	Werichův	k2eAgInSc4d1	Werichův
podpis	podpis	k1gInSc4	podpis
petice	petice	k1gFnSc2	petice
Dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
podnikl	podniknout	k5eAaPmAgMnS	podniknout
J.	J.	kA	J.
Werich	Werich	k1gMnSc1	Werich
několik	několik	k4yIc4	několik
individuálních	individuální	k2eAgFnPc2d1	individuální
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
;	;	kIx,	;
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
i	i	k9	i
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Janou	Jana	k1gFnSc7	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Italské	italský	k2eAgFnPc1d1	italská
prázdniny	prázdniny	k1gFnPc1	prázdniny
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Českosl	Českosl	k1gInSc1	Českosl
<g/>
.	.	kIx.	.
spis	spis	k1gInSc1	spis
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
...	...	k?	...
9	[number]	k4	9
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
&	&	k?	&
Moravec	Moravec	k1gMnSc1	Moravec
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
bral	brát	k5eAaImAgMnS	brát
trochu	trochu	k6eAd1	trochu
s	s	k7c7	s
humorem	humor	k1gInSc7	humor
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
s	s	k7c7	s
despektem	despekt	k1gInSc7	despekt
<g/>
;	;	kIx,	;
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Werichovu	Werichův	k2eAgFnSc4d1	Werichova
průpovídku	průpovídka	k1gFnSc4	průpovídka
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jsem	být	k5eAaImIp1nS	být
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oni	onen	k3xDgMnPc1	onen
mě	já	k3xPp1nSc2	já
nenechají	nechat	k5eNaPmIp3nP	nechat
to	ten	k3xDgNnSc4	ten
národní	národní	k2eAgNnSc4d1	národní
umělectví	umělectví	k1gNnSc4	umělectví
provozovat	provozovat	k5eAaImF	provozovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
období	období	k1gNnSc6	období
normalizace	normalizace	k1gFnSc2	normalizace
nesměl	smět	k5eNaImAgInS	smět
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
ani	ani	k8xC	ani
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
zákaz	zákaz	k1gInSc1	zákaz
veřejně	veřejně	k6eAd1	veřejně
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
také	také	k9	také
pod	pod	k7c4	pod
Manifest	manifest	k1gInSc4	manifest
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInSc4	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
vyšel	vyjít	k5eAaPmAgInS	vyjít
paradoxně	paradoxně	k6eAd1	paradoxně
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
právu	právo	k1gNnSc6	právo
seznam	seznam	k1gInSc1	seznam
signatářů	signatář	k1gMnPc2	signatář
Anticharty	anticharta	k1gFnSc2	anticharta
s	s	k7c7	s
Werichovým	Werichův	k2eAgNnSc7d1	Werichovo
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
vysílala	vysílat	k5eAaImAgFnS	vysílat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
záběry	záběr	k1gInPc4	záběr
J.	J.	kA	J.
Wericha	Werich	k1gMnSc2	Werich
sedícího	sedící	k2eAgMnSc2d1	sedící
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgMnPc7d1	ostatní
shromážděnými	shromážděný	k2eAgMnPc7d1	shromážděný
umělci	umělec	k1gMnPc7	umělec
poslouchajícími	poslouchající	k2eAgInPc7d1	poslouchající
projevy	projev	k1gInPc7	projev
J.	J.	kA	J.
Švorcové	švorcový	k2eAgFnSc2d1	Švorcová
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
tehdy	tehdy	k6eAd1	tehdy
komunisticky	komunisticky	k6eAd1	komunisticky
angažovaných	angažovaný	k2eAgMnPc2d1	angažovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádal	žádat	k5eAaImAgMnS	žádat
komunistické	komunistický	k2eAgInPc4d1	komunistický
úřady	úřad	k1gInPc4	úřad
o	o	k7c4	o
vyškrtnutí	vyškrtnutí	k1gNnSc4	vyškrtnutí
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
signatářů	signatář	k1gMnPc2	signatář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pouze	pouze	k6eAd1	pouze
prezenční	prezenční	k2eAgFnSc4d1	prezenční
listinu	listina	k1gFnSc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
protesty	protest	k1gInPc1	protest
nebyly	být	k5eNaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	on	k3xPp3gInPc4	on
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
bratři	bratr	k1gMnPc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
Suchý	Suchý	k1gMnSc1	Suchý
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Pan	Pan	k1gMnSc1	Pan
Werich	Werich	k1gMnSc1	Werich
z	z	k7c2	z
Kampy	Kampa	k1gFnSc2	Kampa
(	(	kIx(	(
<g/>
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Werich	Werich	k1gMnSc1	Werich
šel	jít	k5eAaImAgMnS	jít
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
nemohl	moct	k5eNaImAgMnS	moct
vycestovat	vycestovat	k5eAaPmF	vycestovat
na	na	k7c4	na
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
do	do	k7c2	do
USA	USA	kA	USA
<g/>
;	;	kIx,	;
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voskovce	Voskovec	k1gMnSc2	Voskovec
uvidí	uvidět	k5eAaPmIp3nS	uvidět
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Kampě	Kampa	k1gFnSc6	Kampa
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
domě	dům	k1gInSc6	dům
jako	jako	k8xC	jako
básník	básník	k1gMnSc1	básník
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
označovaném	označovaný	k2eAgMnSc6d1	označovaný
jako	jako	k8xC	jako
dům	dům	k1gInSc4	dům
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
Werichova	Werichův	k2eAgFnSc1d1	Werichova
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
U	u	k7c2	u
Sovových	Sovových	k2eAgInPc2d1	Sovových
mlýnů	mlýn	k1gInPc2	mlýn
501	[number]	k4	501
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Werich	Werich	k1gMnSc1	Werich
s	s	k7c7	s
Holanem	Holan	k1gMnSc7	Holan
neměli	mít	k5eNaImAgMnP	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
(	(	kIx(	(
<g/>
I.	I.	kA	I.
občanský	občanský	k2eAgInSc1d1	občanský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
,	,	kIx,	,
hrob	hrob	k1gInSc1	hrob
267	[number]	k4	267
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
stého	stý	k4xOgInSc2	stý
výročí	výročí	k1gNnSc3	výročí
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
pamětní	pamětní	k2eAgFnSc4d1	pamětní
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
minci	mince	k1gFnSc4	mince
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
200	[number]	k4	200
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
především	především	k6eAd1	především
vyprávěních	vyprávění	k1gNnPc6	vyprávění
proslul	proslout	k5eAaPmAgMnS	proslout
mnoha	mnoho	k4c3	mnoho
převážně	převážně	k6eAd1	převážně
politickými	politický	k2eAgFnPc7d1	politická
narážkami	narážka	k1gFnPc7	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
o	o	k7c6	o
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
říkal	říkat	k5eAaImAgMnS	říkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
televizních	televizní	k2eAgInPc6d1	televizní
rozhovorech	rozhovor	k1gInPc6	rozhovor
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Škutinou	Škutina	k1gFnSc7	Škutina
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
takže	takže	k8xS	takže
když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
shrneme	shrnout	k5eAaPmIp1nP	shrnout
<g/>
...	...	k?	...
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
báli	bát	k5eAaImAgMnP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
bejt	bejt	k?	bejt
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
bejt	bejt	k?	bejt
moc	moc	k6eAd1	moc
špatný	špatný	k2eAgInSc4d1	špatný
<g/>
...	...	k?	...
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
hlídat	hlídat	k5eAaImF	hlídat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
První	první	k4xOgFnSc2	první
hry	hra	k1gFnSc2	hra
dvojice	dvojice	k1gFnSc2	dvojice
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
zábavné	zábavný	k2eAgInPc1d1	zábavný
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
neangažované	angažovaný	k2eNgFnPc1d1	neangažovaná
<g/>
.	.	kIx.	.
</s>
<s>
Vest	vesta	k1gFnPc2	vesta
pocket	pocketa	k1gFnPc2	pocketa
revue	revue	k1gFnSc2	revue
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
-	-	kIx~	-
představení	představení	k1gNnSc1	představení
mělo	mít	k5eAaImAgNnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
(	(	kIx(	(
<g/>
208	[number]	k4	208
repríz	repríza	k1gFnPc2	repríza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
zde	zde	k6eAd1	zde
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc4d1	nové
pojetí	pojetí	k1gNnSc4	pojetí
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jako	jako	k9	jako
bíle	bíle	k6eAd1	bíle
nalíčení	nalíčený	k2eAgMnPc1d1	nalíčený
klauni	klaun	k1gMnPc1	klaun
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
povídají	povídat	k5eAaImIp3nP	povídat
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
obecenstvem	obecenstvo	k1gNnSc7	obecenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
předem	předem	k6eAd1	předem
domluvené	domluvený	k2eAgNnSc4d1	domluvené
téma	téma	k1gNnSc4	téma
i	i	k8xC	i
směr	směr	k1gInSc4	směr
dialogu	dialog	k1gInSc2	dialog
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstal	zůstat	k5eAaPmAgInS	zůstat
dost	dost	k6eAd1	dost
velký	velký	k2eAgInSc1d1	velký
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
improvizaci	improvizace	k1gFnSc4	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
Skafandr	skafandr	k1gInSc1	skafandr
Ostrov	ostrov	k1gInSc1	ostrov
Dynamit	dynamit	k1gInSc1	dynamit
Sever	sever	k1gInSc1	sever
proti	proti	k7c3	proti
jihu	jih	k1gInSc3	jih
Golem	Golem	k1gMnSc1	Golem
Hej	hej	k6eAd1	hej
rup	rupět	k5eAaImRp2nS	rupět
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
psal	psát	k5eAaImAgInS	psát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
J.	J.	kA	J.
Voskovcem	Voskovec	k1gMnSc7	Voskovec
politicky	politicky	k6eAd1	politicky
angažované	angažovaný	k2eAgFnSc2d1	angažovaná
revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
levicově	levicově	k6eAd1	levicově
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
<g/>
,	,	kIx,	,
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
a	a	k8xC	a
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnPc1d2	pozdější
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
zcela	zcela	k6eAd1	zcela
nepokrytě	pokrytě	k6eNd1	pokrytě
antifašistická	antifašistický	k2eAgFnSc1d1	Antifašistická
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
dialogy	dialog	k1gInPc1	dialog
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
proti	proti	k7c3	proti
maloměšťáctví	maloměšťáctví	k1gNnSc3	maloměšťáctví
<g/>
,	,	kIx,	,
hlouposti	hloupost	k1gFnSc3	hloupost
<g/>
,	,	kIx,	,
totalitě	totalita	k1gFnSc6	totalita
atd.	atd.	kA	atd.
Parodie	parodie	k1gFnPc1	parodie
byly	být	k5eAaImAgFnP	být
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
-	-	kIx~	-
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
Benita	Benita	k1gFnSc1	Benita
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
jako	jako	k8xS	jako
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
politické	politický	k2eAgNnSc4d1	politické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Osel	osel	k1gMnSc1	osel
a	a	k8xC	a
stín	stín	k1gInSc1	stín
-	-	kIx~	-
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
hádají	hádat	k5eAaImIp3nP	hádat
o	o	k7c4	o
nesmyslné	smyslný	k2eNgFnPc4d1	nesmyslná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Antická	antický	k2eAgFnSc1d1	antická
historka	historka	k1gFnSc1	historka
-	-	kIx~	-
spor	spor	k1gInSc1	spor
o	o	k7c4	o
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
odpočinek	odpočinek	k1gInSc4	odpočinek
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
osla	osel	k1gMnSc2	osel
<g/>
.	.	kIx.	.
</s>
<s>
Kat	kat	k1gMnSc1	kat
a	a	k8xC	a
blázen	blázen	k1gMnSc1	blázen
-	-	kIx~	-
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
německého	německý	k2eAgNnSc2d1	německé
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
stěžovalo	stěžovat	k5eAaImAgNnS	stěžovat
na	na	k7c4	na
urážku	urážka	k1gFnSc4	urážka
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
V	V	kA	V
+	+	kIx~	+
W	W	kA	W
vykázáni	vykázat	k5eAaPmNgMnP	vykázat
z	z	k7c2	z
paláce	palác	k1gInSc2	palác
U	u	k7c2	u
Nováků	Novák	k1gMnPc2	Novák
a	a	k8xC	a
sezónu	sezóna	k1gFnSc4	sezóna
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
odehráli	odehrát	k5eAaPmAgMnP	odehrát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Spoutané	spoutaný	k2eAgNnSc1d1	spoutané
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
sálu	sál	k1gInSc3	sál
Rokoka	rokoko	k1gNnSc2	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
divácky	divácky	k6eAd1	divácky
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
hra	hra	k1gFnSc1	hra
této	tento	k3xDgFnSc2	tento
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
:	:	kIx,	:
Balada	balada	k1gFnSc1	balada
z	z	k7c2	z
hadrů	hadr	k1gInPc2	hadr
-	-	kIx~	-
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
her	hra	k1gFnPc2	hra
není	být	k5eNaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
(	(	kIx(	(
<g/>
Kritika	kritika	k1gFnSc1	kritika
skrze	skrze	k?	skrze
Tři	tři	k4xCgMnPc4	tři
košile	košile	k1gFnPc4	košile
Děda	děd	k1gMnSc2	děd
Nezamysla	Nezamysl	k1gMnSc2	Nezamysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
sociální	sociální	k2eAgInSc1d1	sociální
kontext	kontext	k1gInSc1	kontext
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
poctou	pocta	k1gFnSc7	pocta
Villonovi	Villonův	k2eAgMnPc1d1	Villonův
-	-	kIx~	-
svobodomyslnému	svobodomyslný	k2eAgMnSc3d1	svobodomyslný
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
buřiči	buřič	k1gMnSc3	buřič
a	a	k8xC	a
velkému	velký	k2eAgMnSc3d1	velký
básníku	básník	k1gMnSc3	básník
středověké	středověký	k2eAgFnSc2d1	středověká
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
postoje	postoj	k1gInPc1	postoj
byly	být	k5eAaImAgInP	být
autorům	autor	k1gMnPc3	autor
jistě	jistě	k6eAd1	jistě
blízké	blízký	k2eAgInPc4d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1936	[number]	k4	1936
se	s	k7c7	s
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
paláce	palác	k1gInSc2	palác
U	u	k7c2	u
Nováků	Novák	k1gMnPc2	Novák
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
k	k	k7c3	k
názvu	název	k1gInSc3	název
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nebe	nebe	k1gNnSc1	nebe
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
Těžká	těžký	k2eAgFnSc1d1	těžká
Barbora	Barbora	k1gFnSc1	Barbora
Svět	svět	k1gInSc1	svět
za	za	k7c7	za
mřížemi	mříž	k1gFnPc7	mříž
Pěst	pěst	k1gFnSc1	pěst
na	na	k7c4	na
oko	oko	k1gNnSc4	oko
-	-	kIx~	-
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
odmítají	odmítat	k5eAaImIp3nP	odmítat
poučit	poučit	k5eAaPmF	poučit
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
opakují	opakovat	k5eAaImIp3nP	opakovat
jednu	jeden	k4xCgFnSc4	jeden
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
myšlena	myslet	k5eAaImNgFnS	myslet
jako	jako	k8xS	jako
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
zabránili	zabránit	k5eAaPmAgMnP	zabránit
mocným	mocný	k2eAgInSc7d1	mocný
jim	on	k3xPp3gMnPc3	on
ubližovat	ubližovat	k5eAaImF	ubližovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
J.	J.	kA	J.
Ježka	Ježek	k1gMnSc2	Ježek
napsali	napsat	k5eAaPmAgMnP	napsat
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
texty	text	k1gInPc4	text
mnoha	mnoho	k4c2	mnoho
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Nebe	nebe	k1gNnSc1	nebe
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
stihli	stihnout	k5eAaPmAgMnP	stihnout
Voskovec	Voskovec	k1gMnSc1	Voskovec
s	s	k7c7	s
Werichem	Werich	k1gMnSc7	Werich
ještě	ještě	k9	ještě
podle	podle	k7c2	podle
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
Finian	Finian	k1gMnSc1	Finian
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rainbow	Rainbow	k1gFnSc7	Rainbow
od	od	k7c2	od
'	'	kIx"	'
<g/>
Yip	Yip	k1gMnSc2	Yip
<g/>
'	'	kIx"	'
Harburga	Harburg	k1gMnSc2	Harburg
a	a	k8xC	a
Freda	Fred	k1gMnSc2	Fred
Saidyho	Saidy	k1gMnSc2	Saidy
napsat	napsat	k5eAaBmF	napsat
hru	hra	k1gFnSc4	hra
<g/>
:	:	kIx,	:
Divotvorný	divotvorný	k2eAgInSc4d1	divotvorný
hrnec	hrnec	k1gInSc4	hrnec
-	-	kIx~	-
originální	originální	k2eAgFnSc4d1	originální
hudbu	hudba	k1gFnSc4	hudba
Burtona	Burton	k1gMnSc2	Burton
Lane	Lan	k1gMnSc2	Lan
upravil	upravit	k5eAaPmAgMnS	upravit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Pudr	pudr	k1gInSc1	pudr
a	a	k8xC	a
benzín	benzín	k1gInSc1	benzín
-	-	kIx~	-
1931	[number]	k4	1931
Peníze	peníz	k1gInPc1	peníz
nebo	nebo	k8xC	nebo
život	život	k1gInSc1	život
-	-	kIx~	-
1932	[number]	k4	1932
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
v	v	k7c6	v
Kocourkově	Kocourkov	k1gInSc6	Kocourkov
-	-	kIx~	-
1932	[number]	k4	1932
Hej	hej	k6eAd1	hej
rup	rupět	k5eAaImRp2nS	rupět
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
1934	[number]	k4	1934
Svět	svět	k1gInSc1	svět
patří	patřit	k5eAaImIp3nS	patřit
nám	my	k3xPp1nPc3	my
-	-	kIx~	-
1937	[number]	k4	1937
Pád	Pád	k1gInSc1	Pád
Berlína	Berlín	k1gInSc2	Berlín
-	-	kIx~	-
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
Hermanna	Hermann	k1gMnSc2	Hermann
Göringa	Göring	k1gMnSc2	Göring
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Císařův	Císařův	k2eAgMnSc1d1	Císařův
pekař	pekař	k1gMnSc1	pekař
a	a	k8xC	a
pekařův	pekařův	k2eAgMnSc1d1	pekařův
císař	císař	k1gMnSc1	císař
-	-	kIx~	-
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
dvojrole	dvojrole	k1gFnSc1	dvojrole
(	(	kIx(	(
<g/>
zachráněná	zachráněný	k2eAgFnSc1d1	zachráněná
kopie	kopie	k1gFnSc1	kopie
cenzurované	cenzurovaný	k2eAgFnSc2d1	cenzurovaná
verze	verze	k1gFnSc2	verze
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
krve	krev	k1gFnSc2	krev
-	-	kIx~	-
1953	[number]	k4	1953
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
král	král	k1gMnSc1	král
-	-	kIx~	-
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
s	s	k7c7	s
Vlastou	Vlasta	k1gMnSc7	Vlasta
Burianem	Burian	k1gMnSc7	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
mrazíci	mrazík	k1gMnPc1	mrazík
-	-	kIx~	-
1954	[number]	k4	1954
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
(	(	kIx(	(
<g/>
voiceover	voiceover	k1gMnSc1	voiceover
<g/>
)	)	kIx)	)
s	s	k7c7	s
Vlastou	Vlasta	k1gMnSc7	Vlasta
Burianem	Burian	k1gMnSc7	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Vzorný	vzorný	k2eAgInSc1d1	vzorný
kinematograf	kinematograf	k1gInSc1	kinematograf
Haška	Hašek	k1gMnSc2	Hašek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
-	-	kIx~	-
1955	[number]	k4	1955
Psohlavci	psohlavec	k1gMnPc1	psohlavec
-	-	kIx~	-
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
drobná	drobný	k2eAgFnSc1d1	drobná
epizodní	epizodní	k2eAgFnSc1d1	epizodní
role	role	k1gFnSc1	role
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
žalujících	žalující	k2eAgMnPc2d1	žalující
rychtářů	rychtář	k1gMnPc2	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
-	-	kIx~	-
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
drobná	drobný	k2eAgFnSc1d1	drobná
epizodní	epizodní	k2eAgFnSc1d1	epizodní
role	role	k1gFnSc1	role
posluchače	posluchač	k1gMnSc2	posluchač
v	v	k7c6	v
publiku	publikum	k1gNnSc6	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1957	[number]	k4	1957
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
hlas	hlas	k1gInSc1	hlas
-	-	kIx~	-
voiceover	voiceover	k1gInSc1	voiceover
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baron	baron	k1gMnSc1	baron
Prášil	Prášil	k1gMnSc1	Prášil
-	-	kIx~	-
1961	[number]	k4	1961
-	-	kIx~	-
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
přijde	přijít	k5eAaPmIp3nS	přijít
kocour	kocour	k1gMnSc1	kocour
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
dvojrole	dvojrole	k1gFnSc1	dvojrole
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
otázek	otázka	k1gFnPc2	otázka
pro	pro	k7c4	pro
Jana	Jan	k1gMnSc4	Jan
Wericha	Werich	k1gMnSc2	Werich
-	-	kIx~	-
1964	[number]	k4	1964
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnPc2	tau
a	a	k8xC	a
Claudie	Claudia	k1gFnSc2	Claudia
-	-	kIx~	-
1970	[number]	k4	1970
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
smích	smích	k1gInSc4	smích
-	-	kIx~	-
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
vzpomínkový	vzpomínkový	k2eAgInSc1d1	vzpomínkový
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc2	tau
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1972	[number]	k4	1972
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
pan	pan	k1gMnSc1	pan
Tau	tau	k1gNnSc2	tau
-	-	kIx~	-
1972	[number]	k4	1972
25	[number]	k4	25
<g/>
.	.	kIx.	.
hodina	hodina	k1gFnSc1	hodina
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
<g/>
/	/	kIx~	/
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
/	/	kIx~	/
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
posmrtně	posmrtně	k6eAd1	posmrtně
Fimfárum	Fimfárum	k1gNnSc1	Fimfárum
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
hlas	hlas	k1gInSc1	hlas
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
nahrávky	nahrávka	k1gFnSc2	nahrávka
autorského	autorský	k2eAgNnSc2d1	autorské
čtení	čtení	k1gNnSc2	čtení
knihy	kniha	k1gFnSc2	kniha
Fimfárum	Fimfárum	k1gInSc1	Fimfárum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fimfárum	Fimfárum	k1gInSc1	Fimfárum
2	[number]	k4	2
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
hlas	hlas	k1gInSc1	hlas
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
nahrávky	nahrávka	k1gFnSc2	nahrávka
autorského	autorský	k2eAgNnSc2d1	autorské
čtení	čtení	k1gNnSc2	čtení
knihy	kniha	k1gFnSc2	kniha
Fimfárum	Fimfárum	k1gInSc1	Fimfárum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fimfárum	Fimfárum	k1gNnSc1	Fimfárum
-	-	kIx~	-
Do	do	k7c2	do
třetice	třetice	k1gFnSc2	třetice
všeho	všecek	k3xTgNnSc2	všecek
dobrého	dobré	k1gNnSc2	dobré
-	-	kIx~	-
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
hlas	hlas	k1gInSc1	hlas
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
nahrávky	nahrávka	k1gFnSc2	nahrávka
autorského	autorský	k2eAgNnSc2d1	autorské
čtení	čtení	k1gNnSc2	čtení
knihy	kniha	k1gFnSc2	kniha
Fimfárum	Fimfárum	k1gInSc1	Fimfárum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
-	-	kIx~	-
1961	[number]	k4	1961
Slzy	slza	k1gFnPc4	slza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
svět	svět	k1gInSc1	svět
nevidí	vidět	k5eNaImIp3nS	vidět
-	-	kIx~	-
1962	[number]	k4	1962
Kočár	kočár	k1gInSc1	kočár
nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
svátosti	svátost	k1gFnSc2	svátost
-	-	kIx~	-
1962	[number]	k4	1962
Uspořená	uspořený	k2eAgFnSc1d1	uspořená
libra	libra	k1gFnSc1	libra
-	-	kIx~	-
1963	[number]	k4	1963
Drahý	drahý	k1gMnSc1	drahý
zesnulý	zesnulý	k1gMnSc1	zesnulý
-	-	kIx~	-
1964	[number]	k4	1964
Magnetické	magnetický	k2eAgFnSc2d1	magnetická
vlny	vlna	k1gFnSc2	vlna
léčí	léč	k1gFnPc2	léč
-	-	kIx~	-
1965	[number]	k4	1965
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
-	-	kIx~	-
1967	[number]	k4	1967
Co	co	k8xS	co
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
říkáte	říkat	k5eAaImIp2nP	říkat
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Werichu	Werich	k1gMnSc5	Werich
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
1968	[number]	k4	1968
Fimfárum	Fimfárum	k1gInSc1	Fimfárum
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Italské	italský	k2eAgFnPc1d1	italská
prázdniny	prázdniny	k1gFnPc1	prázdniny
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Potlach	potlach	k1gInSc4	potlach
Falstaffovo	Falstaffův	k2eAgNnSc4d1	Falstaffovo
babí	babí	k2eAgNnSc4d1	babí
léto	léto	k1gNnSc4	léto
Lincoln	Lincoln	k1gMnSc1	Lincoln
1933	[number]	k4	1933
Listování	listování	k1gNnPc2	listování
Korespondence	korespondence	k1gFnSc2	korespondence
s	s	k7c7	s
Voskovcem	Voskovec	k1gMnSc7	Voskovec
I	I	kA	I
<g/>
-	-	kIx~	-
<g/>
III	III	kA	III
Deoduši	Deoduše	k1gFnSc6	Deoduše
Všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
píseň	píseň	k1gFnSc1	píseň
-	-	kIx~	-
interpret	interpret	k1gMnSc1	interpret
-	-	kIx~	-
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
text	text	k1gInSc4	text
<g/>
)	)	kIx)	)
-	-	kIx~	-
rok	rok	k1gInSc1	rok
Babička	babička	k1gFnSc1	babička
Mary	Mary	k1gFnSc1	Mary
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Bleška	bleška	k1gFnSc1	bleška
a	a	k8xC	a
veška	veška	k1gFnSc1	veška
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Baur	Baur	k1gMnSc1	Baur
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Civilizace	civilizace	k1gFnSc1	civilizace
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
a	a	k8xC	a
Goliáš	Goliáš	k1gMnSc1	Goliáš
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Divotvorný	divotvorný	k2eAgInSc1d1	divotvorný
hrnec	hrnec	k1gInSc1	hrnec
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Burt	Burt	k2eAgInSc1d1	Burt
Lane	Lane	k1gInSc1	Lane
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Ezop	Ezop	k1gMnSc1	Ezop
a	a	k8xC	a
brabenec	brabenec	k?	brabenec
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Funiculí	Funicule	k1gFnPc2	Funicule
funiculá	funiculat	k5eAaImIp3nS	funiculat
-	-	kIx~	-
Allanovy	Allanův	k2eAgFnPc1d1	Allanova
sestry	sestra	k1gFnPc1	sestra
-	-	kIx~	-
(	(	kIx(	(
<g/>
Luigi	Luigi	k1gNnSc1	Luigi
Denza	Denza	k1gFnSc1	Denza
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Hodné	hodný	k2eAgNnSc1d1	hodné
štěně	štěně	k1gNnSc1	štěně
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Kalná	kalný	k2eAgFnSc1d1	kalná
řeka	řeka	k1gFnSc1	řeka
(	(	kIx(	(
<g/>
Ol	Ola	k1gFnPc2	Ola
<g/>
'	'	kIx"	'
Man	Man	k1gMnSc1	Man
River	River	k1gMnSc1	River
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jerome	Jerom	k1gInSc5	Jerom
Kern	Kern	k1gNnSc1	Kern
/	/	kIx~	/
Hammerstein	Hammerstein	k2eAgInSc1d1	Hammerstein
Oscar	Oscar	k1gInSc1	Oscar
II	II	kA	II
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
text	text	k1gInSc1	text
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
)	)	kIx)	)
Klobouk	klobouk	k1gInSc1	klobouk
ve	v	k7c6	v
křoví	křoví	k1gNnSc6	křoví
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Nebe	nebe	k1gNnSc1	nebe
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Nikdy	nikdy	k6eAd1	nikdy
nic	nic	k6eAd1	nic
nikdo	nikdo	k3yNnSc1	nikdo
nemá	mít	k5eNaImIp3nS	mít
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
O	o	k7c6	o
Španělsku	Španělsko	k1gNnSc6	Španělsko
si	se	k3xPyFc3	se
zpívám	zpívat	k5eAaImIp1nS	zpívat
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Píseň	píseň	k1gFnSc1	píseň
strašlivá	strašlivý	k2eAgFnSc1d1	strašlivá
o	o	k7c6	o
Golemovi	Golem	k1gMnSc3	Golem
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc2	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Potopa	potopa	k1gFnSc1	potopa
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc2	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Prodám	prodat	k5eAaPmIp1nS	prodat
srdce	srdce	k1gNnSc4	srdce
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jiří	Jiří	k1gMnSc2	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
)	)	kIx)	)
Půl	půl	k6eAd1	půl
párku	párek	k1gInSc3	párek
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Lou	Lou	k1gFnSc4	Lou
Singer	Singra	k1gFnPc2	Singra
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Stonožka	stonožka	k1gFnSc1	stonožka
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
Příhoda	Příhoda	k1gMnSc1	Příhoda
(	(	kIx(	(
<g/>
Casey	Casea	k1gMnSc2	Casea
Jones	Jones	k1gMnSc1	Jones
<g/>
)	)	kIx)	)
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Svět	svět	k1gInSc1	svět
patří	patřit	k5eAaImIp3nS	patřit
nám	my	k3xPp1nPc3	my
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Svítá	svítat	k5eAaImIp3nS	svítat
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Šůr	Šůr	k1gFnSc1	Šůr
polka	polka	k1gFnSc1	polka
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
strážníci	strážník	k1gMnPc1	strážník
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
V	v	k7c6	v
domě	dům	k1gInSc6	dům
straší	strašit	k5eAaImIp3nS	strašit
duch	duch	k1gMnSc1	duch
<g/>
(	(	kIx(	(
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
náhoda	náhoda	k1gFnSc1	náhoda
-	-	kIx~	-
-	-	kIx~	-
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
/	/	kIx~	/
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
)	)	kIx)	)
Pozn	pozn	kA	pozn
<g/>
:	:	kIx,	:
diskografie	diskografie	k1gFnSc1	diskografie
není	být	k5eNaImIp3nS	být
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
Civilisace	Civilisace	k1gFnSc1	Civilisace
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
(	(	kIx(	(
<g/>
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Osel	osít	k5eAaPmAgInS	osít
a	a	k8xC	a
stín	stín	k1gInSc4	stín
upravil	upravit	k5eAaPmAgMnS	upravit
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brázda	Brázda	k1gMnSc1	Brázda
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
Vlach	Vlach	k1gMnSc1	Vlach
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
)	)	kIx)	)
/	/	kIx~	/
Kalná	kalný	k2eAgFnSc1d1	kalná
řeka	řeka	k1gFnSc1	řeka
-	-	kIx~	-
Rudolf	Rudolf	k1gMnSc1	Rudolf
Cortés	Cortésa	k1gFnPc2	Cortésa
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
51566-	[number]	k4	51566-
<g/>
M.	M.	kA	M.
2008	[number]	k4	2008
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
-	-	kIx~	-
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
-	-	kIx~	-
četba	četba	k1gFnSc1	četba
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
CD	CD	kA	CD
disků	disk	k1gInPc2	disk
<g/>
.	.	kIx.	.
</s>
