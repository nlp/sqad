<s>
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ČHS	ČHS	kA
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Klub	klub	k1gInSc1
alpistů	alpista	k1gMnPc2
československých	československý	k2eAgMnPc2d1
(	(	kIx(
<g/>
KAČs	KAČs	k1gInSc1
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
1993	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
národní	národní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
spolek	spolek	k1gInSc1
Účel	účel	k1gInSc1
</s>
<s>
Horolezectví	horolezectví	k1gNnSc1
<g/>
,	,	kIx,
Skialpinismus	Skialpinismus	k1gInSc1
<g/>
,	,	kIx,
Sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
,	,	kIx,
Závodní	závodní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
,	,	kIx,
Drytooling	Drytooling	k1gInSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Nádražní	nádražní	k2eAgNnSc4d1
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
5	#num#	k4
Působnost	působnost	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Úřední	úřední	k2eAgFnSc1d1
jazyk	jazyk	k1gInSc4
</s>
<s>
čeština	čeština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
18	#num#	k4
000	#num#	k4
Generální	generální	k2eAgInSc4d1
tajemník	tajemník	k1gInSc4
</s>
<s>
Božena	Božena	k1gFnSc1
Valentová	Valentová	k1gFnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Bloudek	bloudek	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Klíma	Klíma	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Resch	Resch	k1gMnSc1
Klíčové	klíčový	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
</s>
<s>
výkonný	výkonný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Hlavní	hlavní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
UIAA	UIAA	kA
Přidružení	přidružení	k1gNnSc1
</s>
<s>
IFSC	IFSC	kA
<g/>
,	,	kIx,
ISMF	ISMF	kA
<g/>
,	,	kIx,
ČUS	ČUS	k?
<g/>
,	,	kIx,
ČOV	ČOV	kA
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
8	#num#	k4
000	#num#	k4
000	#num#	k4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
horosvaz	horosvaz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
mdbqgbk	mdbqgbk	k6eAd1
Dřívější	dřívější	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
IČO	IČO	kA
</s>
<s>
00460001	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Czech	Czech	k1gInSc1
Mountaineering	Mountaineering	k1gInSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
existence	existence	k1gFnSc2
je	být	k5eAaImIp3nS
podpora	podpora	k1gFnSc1
horolezectví	horolezectví	k1gNnSc2
ve	v	k7c6
všech	všecek	k3xTgFnPc6
jeho	jeho	k3xOp3gFnPc6
formách	forma	k1gFnPc6
a	a	k8xC
disciplínách	disciplína	k1gFnPc6
včetně	včetně	k7c2
skialpinismu	skialpinismus	k1gInSc2
<g/>
,	,	kIx,
prosazování	prosazování	k1gNnSc4
a	a	k8xC
zastupování	zastupování	k1gNnSc4
zájmů	zájem	k1gInPc2
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
jiným	jiný	k2eAgFnPc3d1
horolezeckým	horolezecký	k2eAgFnPc3d1
organizacím	organizace	k1gFnPc3
<g/>
,	,	kIx,
orgánům	orgán	k1gInPc3
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
apod.	apod.	kA
a	a	k8xC
celkové	celkový	k2eAgNnSc1d1
vytváření	vytváření	k1gNnSc1
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
sport	sport	k1gInSc1
mohl	moct	k5eAaImAgInS
v	v	k7c6
ČR	ČR	kA
rozvíjet	rozvíjet	k5eAaImF
na	na	k7c6
rekreační	rekreační	k2eAgFnSc6d1
i	i	k8xC
špičkové	špičkový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
disciplíny	disciplína	k1gFnPc4
horolezectví	horolezectví	k1gNnPc2
patří	patřit	k5eAaImIp3nS
alpinismus	alpinismus	k1gInSc1
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgNnPc1d1
lezení	lezení	k1gNnPc1
<g/>
,	,	kIx,
soutěžní	soutěžní	k2eAgNnPc1d1
lezení	lezení	k1gNnPc1
<g/>
,	,	kIx,
tradiční	tradiční	k2eAgNnPc1d1
lezení	lezení	k1gNnPc1
<g/>
,	,	kIx,
ledolezení	ledolezení	k1gNnSc1
/	/	kIx~
drytooling	drytooling	k1gInSc1
a	a	k8xC
bouldering	bouldering	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
ČHS	ČHS	kA
každoročně	každoročně	k6eAd1
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
nejlepší	dobrý	k2eAgInPc4d3
výkony	výkon	k1gInPc4
českých	český	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
ve	v	k7c6
výše	vysoce	k6eAd2
zmíněných	zmíněný	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
a	a	k8xC
uděluje	udělovat	k5eAaImIp3nS
ocenění	ocenění	k1gNnSc4
Výstupy	výstup	k1gInPc4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
členů	člen	k1gMnPc2
ČHS	ČHS	kA
<g/>
,	,	kIx,
kterých	který	k3yQgMnPc2,k3yIgMnPc2,k3yRgMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
téměř	téměř	k6eAd1
18	#num#	k4
000	#num#	k4
a	a	k8xC
jsou	být	k5eAaImIp3nP
sdruženi	sdružen	k2eAgMnPc1d1
převážně	převážně	k6eAd1
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
400	#num#	k4
horolezeckých	horolezecký	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
horolezectví	horolezectví	k1gNnSc1
a	a	k8xC
dalším	další	k2eAgFnPc3d1
blízkým	blízký	k2eAgFnPc3d1
disciplínám	disciplína	k1gFnPc3
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
ve	v	k7c6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionálové	profesionál	k1gMnPc1
jako	jako	k9
Adam	Adam	k1gMnSc1
Ondra	Ondra	k1gMnSc1
patří	patřit	k5eAaImIp3nS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
disciplínách	disciplína	k1gFnPc6
k	k	k7c3
absolutní	absolutní	k2eAgFnSc3d1
světové	světový	k2eAgFnSc3d1
špičce	špička	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
a	a	k8xC
navázal	navázat	k5eAaPmAgInS
tak	tak	k9
na	na	k7c4
činnost	činnost	k1gFnSc4
horolezeckých	horolezecký	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
působily	působit	k5eAaImAgFnP
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchůdci	předchůdce	k1gMnPc1
ČHS	ČHS	kA
byly	být	k5eAaImAgInP
například	například	k6eAd1
Český	český	k2eAgInSc4d1
odbor	odbor	k1gInSc4
Slovinského	slovinský	k2eAgNnSc2d1
planinského	planinský	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Klub	klub	k1gInSc1
alpistů	alpista	k1gMnPc2
československých	československý	k2eAgMnPc2d1
(	(	kIx(
<g/>
KAČs	KAČs	k1gInSc4
<g/>
,	,	kIx,
založen	založen	k2eAgInSc4d1
1924	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
KAČs	KAČs	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
horolezeckými	horolezecký	k2eAgInPc7d1
spolky	spolek	k1gInPc7
(	(	kIx(
<g/>
horolezecký	horolezecký	k2eAgInSc1d1
odbor	odbor	k1gInSc1
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
...	...	k?
<g/>
)	)	kIx)
a	a	k8xC
slovenským	slovenský	k2eAgInSc7d1
spolkem	spolek	k1gInSc7
IAMES	IAMES	kA
(	(	kIx(
<g/>
JAMES	JAMES	kA
<g/>
)	)	kIx)
"	"	kIx"
<g/>
včleněn	včleněn	k2eAgInSc1d1
<g/>
"	"	kIx"
po	po	k7c6
různých	různý	k2eAgFnPc6d1
peripetiích	peripetie	k1gFnPc6
do	do	k7c2
ČSTV	ČSTV	kA
(	(	kIx(
<g/>
Československého	československý	k2eAgInSc2d1
svazu	svaz	k1gInSc2
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
Českého	český	k2eAgInSc2d1
svazu	svaz	k1gInSc2
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Československa	Československo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Oblasti	oblast	k1gFnPc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Činnost	činnost	k1gFnSc4
ČHS	ČHS	kA
zajišťují	zajišťovat	k5eAaImIp3nP
především	především	k6eAd1
odborné	odborný	k2eAgFnPc1d1
komise	komise	k1gFnPc1
a	a	k8xC
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
:	:	kIx,
sport	sport	k1gInSc1
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc1
<g/>
,	,	kIx,
skalní	skalní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
vrcholová	vrcholový	k2eAgFnSc1d1
komise	komise	k1gFnSc1
</s>
<s>
Metodická	metodický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
</s>
<s>
Komise	komise	k1gFnSc1
soutěžního	soutěžní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
</s>
<s>
Komise	komise	k1gFnSc1
soutěžního	soutěžní	k2eAgNnSc2d1
ledolezení	ledolezení	k1gNnSc2
</s>
<s>
Komise	komise	k1gFnSc1
závodního	závodní	k2eAgInSc2d1
skialpinismu	skialpinismus	k1gInSc2
</s>
<s>
Komise	komise	k1gFnSc1
tradičního	tradiční	k2eAgInSc2d1
skialpinismu	skialpinismus	k1gInSc2
</s>
<s>
Komise	komise	k1gFnSc1
alpinismu	alpinismus	k1gInSc2
</s>
<s>
Komise	komise	k1gFnSc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
patří	patřit	k5eAaImIp3nS
organizace	organizace	k1gFnSc1
lezeckých	lezecký	k2eAgFnPc2d1
/	/	kIx~
skialpinisických	skialpinisický	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
sestavení	sestavení	k1gNnSc4
a	a	k8xC
zajištění	zajištění	k1gNnSc4
reprezentací	reprezentace	k1gFnPc2
ČR	ČR	kA
(	(	kIx(
<g/>
reprezentace	reprezentace	k1gFnSc1
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
,	,	kIx,
reprezentace	reprezentace	k1gFnSc1
závodního	závodní	k2eAgInSc2d1
skialpinismu	skialpinismus	k1gInSc2
<g/>
,	,	kIx,
reprezentace	reprezentace	k1gFnSc2
soutěžního	soutěžní	k2eAgNnSc2d1
ledolození	ledolození	k1gNnSc2
/	/	kIx~
drytoolingu	drytooling	k1gInSc2
<g/>
,	,	kIx,
reprezentace	reprezentace	k1gFnSc2
alpinismu	alpinismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disciplínami	disciplína	k1gFnPc7
soutěžního	soutěžní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
jsou	být	k5eAaImIp3nP
lezení	lezený	k2eAgMnPc1d1
na	na	k7c4
obtížnost	obtížnost	k1gFnSc4
<g/>
,	,	kIx,
lezení	lezení	k1gNnSc4
na	na	k7c4
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
bouldering	bouldering	k1gInSc4
a	a	k8xC
kombinace	kombinace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinace	kombinace	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
rok	rok	k1gInSc4
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
zahrnuta	zahrnout	k5eAaPmNgFnS
mezi	mezi	k7c4
olympijské	olympijský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
soutěžní	soutěžní	k2eAgNnSc4d1
lezení	lezení	k1gNnSc4
zastřešuje	zastřešovat	k5eAaImIp3nS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
-	-	kIx~
IFSC	IFSC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skialpinismus	Skialpinismus	k1gInSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
turistickou	turistický	k2eAgFnSc4d1
<g/>
,	,	kIx,
extrémní	extrémní	k2eAgFnSc4d1
a	a	k8xC
závodní	závodní	k2eAgFnSc4d1
formu	forma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodní	závodní	k2eAgInSc1d1
skialpinismus	skialpinismus	k1gInSc1
zastřešuje	zastřešovat	k5eAaImIp3nS
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
Mezinárodní	mezinárodní	k2eAgFnSc1d1
skialpinistická	skialpinistický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
-	-	kIx~
ISMF	ISMF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěžní	soutěžní	k2eAgInSc4d1
ledolezení	ledolezení	k1gNnSc1
/	/	kIx~
drytooling	drytooling	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
disciplíny	disciplína	k1gFnPc4
-	-	kIx~
lezení	lezení	k1gNnSc1
na	na	k7c4
obtížnost	obtížnost	k1gFnSc4
a	a	k8xC
lezení	lezení	k1gNnSc4
na	na	k7c4
rychlost	rychlost	k1gFnSc4
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
zastřešeno	zastřešit	k5eAaPmNgNnS
Mezinárodní	mezinárodní	k2eAgFnSc7d1
federací	federace	k1gFnSc7
alpinismu	alpinismus	k1gInSc2
-	-	kIx~
UIAA	UIAA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejnou	stejný	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
je	být	k5eAaImIp3nS
zastřešen	zastřešen	k2eAgInSc4d1
také	také	k9
alpinismus	alpinismus	k1gInSc4
zahrnující	zahrnující	k2eAgNnSc4d1
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
činností	činnost	k1gFnPc2
v	v	k7c6
horský	horský	k2eAgInSc1d1
terénu	terén	k1gInSc3
velehorského	velehorský	k2eAgInSc2d1
rázu	ráz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČHS	ČHS	kA
vede	vést	k5eAaImIp3nS
tříletý	tříletý	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
program	program	k1gInSc1
Sokolíci	sokolík	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vychovává	vychovávat	k5eAaImIp3nS
mladé	mladý	k2eAgMnPc4d1
talentované	talentovaný	k2eAgMnPc4d1
lezce	lezec	k1gMnPc4
se	s	k7c7
zájmem	zájem	k1gInSc7
o	o	k7c4
big	big	k?
wally	walla	k1gFnPc4
a	a	k8xC
lezení	lezení	k1gNnPc4
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Skály	skála	k1gFnPc1
</s>
<s>
ČHS	ČHS	kA
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
vhodné	vhodný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
provozování	provozování	k1gNnSc4
horolezectví	horolezectví	k1gNnSc2
na	na	k7c6
skalách	skála	k1gFnPc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
vyjednává	vyjednávat	k5eAaImIp3nS
povolení	povolení	k1gNnPc4
lezení	lezení	k1gNnSc2
s	s	k7c7
orgány	orgán	k1gInPc7
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
s	s	k7c7
vlastníky	vlastník	k1gMnPc7
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
dodržování	dodržování	k1gNnSc4
pravidel	pravidlo	k1gNnPc2
a	a	k8xC
podmínek	podmínka	k1gFnPc2
lezení	lezení	k1gNnPc2
<g/>
,	,	kIx,
eviduje	evidovat	k5eAaImIp3nS
lezecké	lezecký	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
a	a	k8xC
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
školení	školení	k1gNnSc4
osazování	osazování	k1gNnSc2
jištění	jištění	k1gNnSc2
a	a	k8xC
dle	dle	k7c2
možností	možnost	k1gFnPc2
provádí	provádět	k5eAaImIp3nS
namátkovou	namátkový	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
případnou	případný	k2eAgFnSc4d1
údržbu	údržba	k1gFnSc4
jištění	jištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
databáze	databáze	k1gFnSc2
Skály	skála	k1gFnSc2
ČR	ČR	kA
informuje	informovat	k5eAaBmIp3nS
veřejnost	veřejnost	k1gFnSc4
o	o	k7c6
existujících	existující	k2eAgInPc6d1
horolezeckých	horolezecký	k2eAgInPc6d1
výstupech	výstup	k1gInPc6
a	a	k8xC
aktuálních	aktuální	k2eAgNnPc6d1
pravidlech	pravidlo	k1gNnPc6
a	a	k8xC
omezeních	omezení	k1gNnPc6
horolezecké	horolezecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnosti	činnost	k1gFnSc6
týkající	týkající	k2eAgFnSc1d1
se	se	k3xPyFc4
skalních	skalní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
Centrální	centrální	k2eAgFnSc1d1
vrcholová	vrcholový	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
oblastní	oblastní	k2eAgFnSc2d1
vrcholové	vrcholový	k2eAgFnSc2d1
komise	komise	k1gFnSc2
a	a	k8xC
dobrovolní	dobrovolný	k2eAgMnPc1d1
správci	správce	k1gMnPc1
skal	skála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
komise	komise	k1gFnPc1
ČHS	ČHS	kA
organizují	organizovat	k5eAaBmIp3nP
otevřené	otevřený	k2eAgFnPc4d1
vzdělávací	vzdělávací	k2eAgFnPc4d1
a	a	k8xC
metodické	metodický	k2eAgFnPc4d1
akce	akce	k1gFnPc4
pro	pro	k7c4
členy	člen	k1gInPc4
ČHS	ČHS	kA
i	i	k9
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1
lezeckou	lezecký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
školení	školení	k1gNnPc1
a	a	k8xC
výcviky	výcvik	k1gInPc1
instruktorů	instruktor	k1gMnPc2
pro	pro	k7c4
oblast	oblast	k1gFnSc4
horolezectví	horolezectví	k1gNnSc2
a	a	k8xC
stavění	stavění	k1gNnSc2
lezeckých	lezecký	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČHS	ČHS	kA
dále	daleko	k6eAd2
uděluje	udělovat	k5eAaImIp3nS
certifikát	certifikát	k1gInSc4
Garantované	garantovaný	k2eAgFnSc2d1
horoškoly	horoškola	k1gFnSc2
ČHS	ČHS	kA
horoškolám	horoškolat	k5eAaImIp1nS,k5eAaPmIp1nS
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
výuka	výuka	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
standardům	standard	k1gInPc3
ČHS	ČHS	kA
<g/>
.	.	kIx.
</s>
<s>
Mládež	mládež	k1gFnSc1
</s>
<s>
ČHS	ČHS	kA
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
podpoře	podpora	k1gFnSc3
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
zájem	zájem	k1gInSc4
o	o	k7c4
horolezectví	horolezectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
práce	práce	k1gFnSc2
s	s	k7c7
mládeží	mládež	k1gFnSc7
je	být	k5eAaImIp3nS
Síť	síť	k1gFnSc1
klubů	klub	k1gInPc2
mládeže	mládež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
horolezců	horolezec	k1gMnPc2
</s>
<s>
ČHS	ČHS	kA
</s>
<s>
Čestný	čestný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
ČHS	ČHS	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Horolezec	horolezec	k1gMnSc1
roku	rok	k1gInSc2
</s>
<s>
Výstup	výstup	k1gInSc1
roku	rok	k1gInSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
cepín	cepín	k1gInSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pražský	pražský	k2eAgInSc1d1
křišťálový	křišťálový	k2eAgInSc1d1
cepín	cepín	k1gInSc1
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Salewa	Salewa	k6eAd1
Rock	rock	k1gInSc1
Award	Award	k1gInSc1
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
IFSC	IFSC	kA
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc7
Sportiva	Sportivum	k1gNnSc2
Competition	Competition	k1gInSc1
Award	Awardo	k1gNnPc2
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
IFSC	IFSC	kA
<g/>
)	)	kIx)
</s>
<s>
dříve	dříve	k6eAd2
</s>
<s>
Horolezec	horolezec	k1gMnSc1
ČSSR	ČSSR	kA
-	-	kIx~
stříbrný	stříbrný	k2eAgInSc4d1
<g/>
,	,	kIx,
bronzový	bronzový	k2eAgInSc4d1
a	a	k8xC
zlatý	zlatý	k2eAgInSc4d1
odznak	odznak	k1gInSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
po	po	k7c6
splnění	splnění	k1gNnSc6
výkonnostních	výkonnostní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
</s>
<s>
Mistr	mistr	k1gMnSc1
sportu	sport	k1gInSc2
ČSSR	ČSSR	kA
(	(	kIx(
<g/>
v	v	k7c6
horolezectví	horolezectví	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Seznam	seznam	k1gInSc4
čestných	čestný	k2eAgMnPc2d1
členů	člen	k1gMnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
ČHS	ČHS	kA
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ročenka	ročenka	k1gFnSc1
ČHS	ČHS	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Web	web	k1gInSc1
Horolezeckého	horolezecký	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
historie	historie	k1gFnSc1
českého	český	k2eAgNnSc2d1
horolezectví	horolezectví	k1gNnSc2
</s>
<s>
Skalní	skalní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
ČR	ČR	kA
–	–	k?
Oficiální	oficiální	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
skal	skála	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Závody	závod	k1gInPc1
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
mládeže	mládež	k1gFnSc2
•	•	k?
Světové	světový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Asijské	asijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Zimní	zimní	k2eAgFnSc2d1
armádní	armádní	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
<g/>
;	;	kIx,
Amerika	Amerika	k1gFnSc1
<g/>
;	;	kIx,
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Asie	Asie	k1gFnSc2
juniorů	junior	k1gMnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
•	•	k?
Evropský	evropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
juniorů	junior	k1gMnPc2
•	•	k?
Rheintal	Rheintal	k1gMnSc1
Cup	cup	k0
</s>
<s>
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
•	•	k?
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
•	•	k?
Akademické	akademický	k2eAgFnSc2d1
MČR	MČR	kA
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Sportroccia	Sportroccia	k1gFnSc1
•	•	k?
Arco	Arco	k1gNnSc1
Rock	rock	k1gInSc1
Master	master	k1gMnSc1
•	•	k?
Melloblocco	Melloblocco	k1gMnSc1
•	•	k?
Adidas	Adidas	k1gInSc1
Rockstars	Rockstars	k1gInSc1
•	•	k?
Salewa	Salew	k1gInSc2
rock	rock	k1gInSc1
show	show	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Francie	Francie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Itálie	Itálie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Německa	Německo	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Polska	Polsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Rakouska	Rakousko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Slovenska	Slovensko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Španělska	Španělsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Švýcarska	Švýcarsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
USA	USA	kA
</s>
<s>
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
MČR	MČR	kA
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
ČP	ČP	kA
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
Petrohradské	petrohradský	k2eAgNnSc4d1
Padání	padání	k1gNnSc4
•	•	k?
Mejcup	Mejcup	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
boulderingová	boulderingový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
MČR	MČR	kA
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
ČP	ČP	kA
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
U14	U14	k1gFnSc4
•	•	k?
Překližka	překližka	k1gFnSc1
cup	cup	k1gInSc1
•	•	k?
Moravský	moravský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
(	(	kIx(
<g/>
IFSC	IFSC	kA
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
)	)	kIx)
•	•	k?
Slovenský	slovenský	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
JAMES	JAMES	kA
</s>
<s>
Závody	závod	k1gInPc1
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
•	•	k?
Světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
•	•	k?
Evropský	evropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
v	v	k7c4
ledolezení	ledolezení	k1gNnSc4
•	•	k?
Mistrovství	mistrovství	k1gNnSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČR	ČR	kA
v	v	k7c6
drytoolingu	drytooling	k1gInSc6
•	•	k?
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
horolezecká	horolezecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
UIAA	UIAA	kA
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
)	)	kIx)
</s>
<s>
Závody	závod	k1gInPc1
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
</s>
<s>
MS	MS	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
SP	SP	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
ME	ME	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
<g/>
;	;	kIx,
Asie	Asie	k1gFnSc1
<g/>
;	;	kIx,
Amerika	Amerika	k1gFnSc1
<g/>
;	;	kIx,
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
MSJ	MSJ	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
MEJ	MEJ	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
EPJ	EPJ	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
</s>
<s>
MČR	MČR	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
ČP	ČP	kA
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
</s>
<s>
MČR	MČR	kA
juniorů	junior	k1gMnPc2
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
•	•	k?
ČP	ČP	kA
juniorů	junior	k1gMnPc2
ve	v	k7c6
skialpinismu	skialpinismus	k1gInSc6
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
skialpinistická	skialpinistický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
ISMF	ISMF	kA
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
)	)	kIx)
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
svazy	svaz	k1gInPc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
olympijském	olympijský	k2eAgInSc6d1
výboru	výbor	k1gInSc6
(	(	kIx(
<g/>
ČOV	ČOV	kA
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
LOH	LOH	kA
</s>
<s>
ČAS	čas	k1gInSc1
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBaS	ČBaS	k?
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČBF	ČBF	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FAČR	FAČR	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
ČSH	ČSH	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČJF	ČJF	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJu	ČSJu	k5eAaPmIp1nS
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČLS	ČLS	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMP	ČSMP	kA
(	(	kIx(
<g/>
moderního	moderní	k2eAgNnSc2d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČSPS	ČSPS	kA
(	(	kIx(
<g/>
plavání	plavání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pozemního	pozemní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSRU	ČSRU	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSS	ČSS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČAST	ČAST	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČŠS	ČŠS	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČST	ČST	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČTS	ČTS	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČTA	číst	k5eAaImSgMnS
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSV	ČSV	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
SZČR	SZČR	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
ZOH	ZOH	kA
</s>
<s>
ČSB	ČSB	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSBS	ČSBS	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
krasobruslení	krasobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSR	ČSR	kA
(	(	kIx(
<g/>
rychlobruslení	rychlobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSLH	ČSLH	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
SLČR	SLČR	kA
(	(	kIx(
<g/>
lyžování	lyžování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
saně	saně	k1gFnPc1
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
uznané	uznaný	k2eAgInPc1d1
MOV	MOV	kA
</s>
<s>
ČSAE	ČSAE	kA
(	(	kIx(
<g/>
aerobik	aerobik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
AČR	AČR	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc4d1
a	a	k8xC
motocyklový	motocyklový	k2eAgInSc4d1
sport	sport	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČAB	ČAB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČMBS	ČMBS	kA
(	(	kIx(
<g/>
billiard	billiard	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBS	ČBS	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČF	ČF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČHS	ČHS	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
a	a	k8xC
sportovní	sportovní	k2eAgNnPc1d1
lezení	lezení	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
ČSKe	ČSKe	k1gNnSc1
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČUKB	ČUKB	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČMKS	ČMKS	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKBF	ČKBF	kA
(	(	kIx(
<g/>
kuželky	kuželka	k1gFnPc1
a	a	k8xC
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
AeČR	AeČR	k?
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
C.	C.	kA
<g/>
M.	M.	kA
<g/>
T.A.	T.A.	k1gFnPc2
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
netball	netball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSOS	ČSOS	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČFP	ČFP	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
SPČR	SPČR	kA
(	(	kIx(
<g/>
potápění	potápění	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASQ	ČASQ	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSS	ČSS	kA
(	(	kIx(
<g/>
sumó	sumó	k?
<g/>
)	)	kIx)
</s>
<s>
AČSF	AČSF	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ŠSČR	ŠSČR	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČSTS	ČSTS	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVLW	ČSVLW	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVM	ČSVM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
???	???	k?
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
VZS	VZS	kA
ČČK	ČČK	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
SportAccord	SportAccord	k6eAd1
</s>
<s>
MOV	MOV	kA
</s>
<s>
ASOIF	ASOIF	kA
</s>
<s>
AIOWF	AIOWF	kA
</s>
<s>
ARISF	ARISF	kA
</s>
<s>
IWGA	IWGA	kA
</s>
<s>
IPC	IPC	kA
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
svazy	svaz	k1gInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
unii	unie	k1gFnSc6
sportu	sport	k1gInSc2
(	(	kIx(
<g/>
ČUS	ČUS	k?
<g/>
)	)	kIx)
Sportovní	sportovní	k2eAgFnSc6d1
svazysdružené	svazysdružený	k2eAgFnSc6d1
v	v	k7c6
ČUS	ČUS	k?
</s>
<s>
ČSAE	ČSAE	kA
(	(	kIx(
<g/>
aerobik	aerobik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSAR	ČSAR	kA
(	(	kIx(
<g/>
akrobatický	akrobatický	k2eAgInSc1d1
rock	rock	k1gInSc1
and	and	k?
roll	roll	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAAF	ČAAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAS	čas	k1gInSc1
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBaS	ČBaS	k?
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČBF	ČBF	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČMBS	ČMBS	kA
(	(	kIx(
<g/>
billiard	billiard	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSBS	ČSBS	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČUBU	čuba	k1gFnSc4
(	(	kIx(
<g/>
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČAES	ČAES	kA
(	(	kIx(
<g/>
extrémní	extrémní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČF	ČF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAČR	FAČR	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
ČSH	ČSH	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČMSHb	ČMSHb	k1gInSc1
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČHS	ČHS	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
HS	HS	kA
ČR	ČR	kA
(	(	kIx(
<g/>
Horská	horský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČACH	ČACH	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČJF	ČJF	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jóga	jóga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJu	ČSJu	k5eAaPmIp1nS
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČUKB	ČUKB	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgFnSc2d1
brusle	brusle	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
krasobruslení	krasobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
SKFČR	SKFČR	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kuše	kuše	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČKBF	ČKBF	kA
(	(	kIx(
<g/>
kuželky	kuželka	k1gFnPc1
a	a	k8xC
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČLU	ČLU	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSLH	ČSLH	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČLS	ČLS	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
SLČR	SLČR	kA
(	(	kIx(
<g/>
lyžování	lyžování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
ČSM	ČSM	kA
(	(	kIx(
<g/>
metaná	metaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
ČMGS	ČMGS	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMG	ČSMG	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgFnSc1d1
gymnastika	gymnastika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMP	ČSMP	kA
(	(	kIx(
<g/>
moderního	moderní	k2eAgNnSc2d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
C.	C.	kA
<g/>
M.	M.	kA
<g/>
T.A.	T.A.	k1gFnPc2
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
SNH	SNH	kA
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČNS	ČNS	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSOS	ČSOS	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAPEK	Čapek	k1gMnSc1
(	(	kIx(
<g/>
pétanque	pétanque	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSPS	ČSPS	kA
(	(	kIx(
<g/>
plavání	plavání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pozemního	pozemní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASS	ČASS	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
AROB	AROB	kA
(	(	kIx(
<g/>
rádiový	rádiový	k2eAgInSc1d1
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSRU	ČSRU	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSR	ČSR	kA
(	(	kIx(
<g/>
rychlobruslení	rychlobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
saně	saně	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSST	ČSST	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
skibobistů	skibobista	k1gMnPc2
(	(	kIx(
<g/>
skiboby	skibob	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASQ	ČASQ	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAST	ČAST	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ŠSČR	ŠSČR	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČŠS	ČŠS	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČST	ČST	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČTS	ČTS	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČTA	číst	k5eAaImSgMnS
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČAUS	ČAUS	kA
(	(	kIx(
<g/>
univerzitní	univerzitní	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVLW	ČSVLW	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVM	ČSVM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVP	ČSVP	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSV	ČSV	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
SZČR	SZČR	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UZPS	UZPS	kA
(	(	kIx(
<g/>
paralympijské	paralympijský	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
Sportovní	sportovní	k2eAgFnPc1d1
svazypřidružené	svazypřidružený	k2eAgFnPc1d1
k	k	k7c3
ČUS	ČUS	k?
</s>
<s>
Asociace	asociace	k1gFnSc1
bazénů	bazén	k1gInPc2
a	a	k8xC
saun	sauna	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
plaveckých	plavecký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
regeneraci	regenerace	k1gFnSc6
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Go	Go	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
foosballová	foosballový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
bridžový	bridžový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
racketlonu	racketlon	k1gInSc2
</s>
<s>
Český	český	k2eAgInSc1d1
rybářský	rybářský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
koloběhu	koloběh	k1gInSc2
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
mariáše	mariáš	k1gInSc2
</s>
<s>
Unie	unie	k1gFnSc1
šipkových	šipkový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Paintball	paintball	k1gInSc1
Games	Gamesa	k1gFnPc2
Bohemia	bohemia	k1gFnSc1
Association	Association	k1gInSc1
</s>
<s>
Sdružení	sdružení	k1gNnSc1
zimních	zimní	k2eAgInPc2d1
stadionů	stadion	k1gInPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Unie	unie	k1gFnSc1
armádních	armádní	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
ČR	ČR	kA
</s>
<s>
Unie	unie	k1gFnSc1
hráčů	hráč	k1gMnPc2
stolního	stolní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
unie	unie	k1gFnSc1
sportu	sport	k1gInSc2
v	v	k7c6
přetahování	přetahování	k1gNnSc6
lanem	lano	k1gNnSc7
</s>
<s>
Česká	český	k2eAgFnSc1d1
Bowls	Bowls	k1gInSc1
asociace	asociace	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
ricochetová	ricochetový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Nordic	Nordic	k1gMnSc1
Walking	Walking	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
šipková	šipkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Stiga	Stiga	k1gFnSc1
Game	game	k1gInSc4
</s>
<s>
Asociace	asociace	k1gFnSc1
českomoravského	českomoravský	k2eAgInSc2d1
kroketu	kroket	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2007416565	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2018026300	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124816378	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2018026300	#num#	k4
</s>
