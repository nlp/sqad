<p>
<s>
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
(	(	kIx(	(
<g/>
tádžicky	tádžicky	k6eAd1	tádžicky
Ҷ	Ҷ	k?	Ҷ
Т	Т	k?	Т
<g/>
/	/	kIx~	/
<g/>
Džumhúrí-ji	Džumhúrí	k1gFnSc3	Džumhúrí-j
Tódžíkistón	Tódžíkistón	k1gInSc1	Tódžíkistón
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asijský	asijský	k2eAgInSc1d1	asijský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
státu	stát	k1gInSc2	stát
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
hory	hora	k1gFnPc1	hora
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
až	až	k9	až
7	[number]	k4	7
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
tyčí	tyč	k1gFnPc2	tyč
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Pamír	Pamír	k1gInSc1	Pamír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
území	území	k1gNnSc4	území
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
4	[number]	k4	4
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgNnSc4d1	bronzové
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
lidé	člověk	k1gMnPc1	člověk
kultury	kultura	k1gFnSc2	kultura
Bactria	Bactrium	k1gNnSc2	Bactrium
<g/>
–	–	k?	–
<g/>
Margiana	Margiana	k1gFnSc1	Margiana
<g/>
,	,	kIx,	,
Oxus	Oxus	k1gInSc1	Oxus
a	a	k8xC	a
Andronovské	Andronovský	k2eAgFnSc2d1	Andronovský
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
i	i	k9	i
starověké	starověký	k2eAgNnSc1d1	starověké
předmětské	předmětský	k2eAgNnSc1d1	předmětský
osídlení	osídlení	k1gNnSc1	osídlení
Sarazm	Sarazma	k1gFnPc2	Sarazma
(	(	kIx(	(
<g/>
3	[number]	k4	3
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
2	[number]	k4	2
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
st.	st.	kA	st.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
současného	současný	k2eAgInSc2d1	současný
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
součástí	součást	k1gFnPc2	součást
starověkého	starověký	k2eAgNnSc2d1	starověké
království	království	k1gNnSc2	království
Kambódža	Kambódžum	k1gNnSc2	Kambódžum
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
částí	část	k1gFnSc7	část
Achaimenovské	Achaimenovský	k2eAgFnSc2d1	Achaimenovský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začali	začít	k5eAaPmAgMnP	začít
na	na	k7c6	na
území	území	k1gNnSc6	území
přicházet	přicházet	k5eAaImF	přicházet
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
pronásledováním	pronásledování	k1gNnSc7	pronásledování
z	z	k7c2	z
Babylónu	babylón	k1gInSc2	babylón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgFnPc3d1	veliká
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
krátkodobé	krátkodobý	k2eAgFnSc3d1	krátkodobá
okupaci	okupace	k1gFnSc3	okupace
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
helenistické	helenistický	k2eAgNnSc1d1	helenistické
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Seleukovská	Seleukovský	k2eAgFnSc1d1	Seleukovská
říše	říše	k1gFnSc1	říše
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
Řecko-baktrijské	Řeckoaktrijský	k2eAgNnSc1d1	Řecko-baktrijský
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Jüe-č	Jüe-č	k1gFnSc1	Jüe-č
<g/>
'	'	kIx"	'
<g/>
ové	ové	k0wR	ové
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
90	[number]	k4	90
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
30	[number]	k4	30
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
zničili	zničit	k5eAaPmAgMnP	zničit
poslední	poslední	k2eAgNnPc4d1	poslední
nástupnická	nástupnický	k2eAgNnPc4d1	nástupnické
helenistická	helenistický	k2eAgNnPc4d1	helenistické
království	království	k1gNnPc4	království
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Tochary	Tochar	k1gMnPc7	Tochar
založili	založit	k5eAaPmAgMnP	založit
Kušánskou	Kušánský	k2eAgFnSc4d1	Kušánská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Kušánská	Kušánský	k2eAgFnSc1d1	Kušánská
říše	říše	k1gFnSc1	říše
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
region	region	k1gInSc4	region
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
úpadku	úpadek	k1gInSc2	úpadek
v	v	k7c4	v
3	[number]	k4	3
st.	st.	kA	st.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
současného	současný	k2eAgInSc2d1	současný
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
většinou	většinou	k6eAd1	většinou
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Sásánovské	sásánovský	k2eAgFnSc2d1	sásánovská
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
krátkodobým	krátkodobý	k2eAgNnSc7d1	krátkodobé
ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
nomádskou	nomádský	k2eAgFnSc7d1	nomádská
říší	říš	k1gFnSc7	říš
Hephthalitů	Hephthalit	k1gInPc2	Hephthalit
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
650	[number]	k4	650
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
680	[number]	k4	680
n.	n.	k?	n.
l	l	kA	l
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přišly	přijít	k5eAaPmAgFnP	přijít
nájezdy	nájezd	k1gInPc4	nájezd
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
šíření	šíření	k1gNnSc1	šíření
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Politicko-kulturně-obchodními	Politickoulturněbchodní	k2eAgMnPc7d1	Politicko-kulturně-obchodní
centry	centr	k1gMnPc7	centr
celé	celý	k2eAgFnSc2d1	celá
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stala	stát	k5eAaPmAgFnS	stát
města	město	k1gNnSc2	město
Samarkand	Samarkand	k1gInSc1	Samarkand
a	a	k8xC	a
Buchara	Buchara	k1gFnSc1	Buchara
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
obývaná	obývaný	k2eAgFnSc1d1	obývaná
Tádžiky	Tádžik	k1gMnPc7	Tádžik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Arabech	Arab	k1gMnPc6	Arab
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
ovládnuta	ovládnout	k5eAaPmNgFnS	ovládnout
Chórezmskou	Chórezmský	k2eAgFnSc7d1	Chórezmský
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
a	a	k8xC	a
zpustošili	zpustošit	k5eAaPmAgMnP	zpustošit
Mongolové	Mongol	k1gMnPc1	Mongol
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Čingischána	Čingischán	k1gMnSc2	Čingischán
<g/>
.	.	kIx.	.
</s>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
založili	založit	k5eAaPmAgMnP	založit
Čagatajský	Čagatajský	k2eAgInSc4d1	Čagatajský
chanát	chanát	k1gInSc4	chanát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
území	území	k1gNnSc6	území
ovládala	ovládat	k5eAaImAgFnS	ovládat
Tímúrovská	Tímúrovský	k2eAgFnSc1d1	Tímúrovský
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
turkický	turkický	k2eAgMnSc1d1	turkický
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Tamerlán	Tamerlán	k2eAgMnSc1d1	Tamerlán
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
Bucharský	bucharský	k2eAgInSc4d1	bucharský
chanát	chanát	k1gInSc4	chanát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ruské	ruský	k2eAgNnSc1d1	ruské
a	a	k8xC	a
sovětské	sovětský	k2eAgNnSc1d1	sovětské
impérium	impérium	k1gNnSc1	impérium
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
carské	carský	k2eAgNnSc1d1	carské
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
zabralo	zabrat	k5eAaPmAgNnS	zabrat
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
muslimští	muslimský	k2eAgMnPc1d1	muslimský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
proti	proti	k7c3	proti
Rusům	Rus	k1gMnPc3	Rus
začali	začít	k5eAaPmAgMnP	začít
vést	vést	k5eAaImF	vést
partyzánskou	partyzánský	k2eAgFnSc4d1	Partyzánská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
boj	boj	k1gInSc1	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
zintenzivnil	zintenzivnit	k5eAaPmAgMnS	zintenzivnit
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
ovládnout	ovládnout	k5eAaPmF	ovládnout
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
a	a	k8xC	a
nastolit	nastolit	k5eAaPmF	nastolit
bolševický	bolševický	k2eAgInSc4d1	bolševický
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
partyzány	partyzána	k1gFnPc4	partyzána
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Tádžická	tádžický	k2eAgFnSc1d1	tádžická
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hranice	hranice	k1gFnSc1	hranice
vytyčila	vytyčit	k5eAaPmAgFnS	vytyčit
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vůbec	vůbec	k9	vůbec
nebrala	brát	k5eNaImAgFnS	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tádžická	tádžický	k2eAgNnPc1d1	tádžické
historická	historický	k2eAgNnPc1d1	historické
města	město	k1gNnPc1	město
Samarkand	Samarkanda	k1gFnPc2	Samarkanda
a	a	k8xC	a
Buchara	Buchara	k1gFnSc1	Buchara
připadla	připadnout	k5eAaPmAgFnS	připadnout
sousední	sousední	k2eAgFnSc6d1	sousední
Uzbecké	uzbecký	k2eAgFnSc6d1	Uzbecká
sovětské	sovětský	k2eAgFnSc6d1	sovětská
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
obydleny	obydlet	k5eAaPmNgFnP	obydlet
výhradně	výhradně	k6eAd1	výhradně
Uzbeky	Uzbek	k1gMnPc7	Uzbek
<g/>
,	,	kIx,	,
připadly	připadnout	k5eAaPmAgInP	připadnout
Tádžické	tádžický	k2eAgInPc1d1	tádžický
sovětské	sovětský	k2eAgFnSc6d1	sovětská
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
začali	začít	k5eAaPmAgMnP	začít
silně	silně	k6eAd1	silně
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
místním	místní	k2eAgNnPc3d1	místní
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
emigrace	emigrace	k1gFnSc1	emigrace
sovětských	sovětský	k2eAgMnPc2d1	sovětský
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
nastala	nastat	k5eAaPmAgFnS	nastat
masivní	masivní	k2eAgFnSc1d1	masivní
emigrace	emigrace	k1gFnSc1	emigrace
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Emigrace	emigrace	k1gFnSc1	emigrace
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
po	po	k7c4	po
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
místní	místní	k2eAgFnSc1d1	místní
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
tzv.	tzv.	kA	tzv.
bucharských	bucharský	k2eAgMnPc2d1	bucharský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
už	už	k6eAd1	už
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
de	de	k?	de
facto	facto	k1gNnSc1	facto
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
komunita	komunita	k1gFnSc1	komunita
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
probudil	probudit	k5eAaPmAgInS	probudit
boj	boj	k1gInSc1	boj
za	za	k7c4	za
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc4	nezávislost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc1	nezávislost
a	a	k8xC	a
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
po	po	k7c6	po
kolapsu	kolaps	k1gInSc6	kolaps
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
prezidenta	prezident	k1gMnSc4	prezident
Emómalí-ji	Emómalíe	k1gFnSc4	Emómalí-je
Rahmóna	Rahmóna	k1gFnSc1	Rahmóna
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zcela	zcela	k6eAd1	zcela
eliminoval	eliminovat	k5eAaBmAgMnS	eliminovat
opozici	opozice	k1gFnSc3	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
opozice	opozice	k1gFnSc1	opozice
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
islámskými	islámský	k2eAgInPc7d1	islámský
radikály	radikál	k1gInPc7	radikál
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
menšinami	menšina	k1gFnPc7	menšina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bojovaly	bojovat	k5eAaImAgFnP	bojovat
za	za	k7c4	za
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
oficiální	oficiální	k2eAgFnSc3d1	oficiální
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgNnPc1d1	vládní
vojska	vojsko	k1gNnPc1	vojsko
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
ruské	ruský	k2eAgFnSc2d1	ruská
a	a	k8xC	a
uzbecké	uzbecký	k2eAgFnSc2d1	Uzbecká
armády	armáda	k1gFnSc2	armáda
měla	mít	k5eAaImAgFnS	mít
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porazit	porazit	k5eAaPmF	porazit
opoziční	opoziční	k2eAgNnPc4d1	opoziční
vojska	vojsko	k1gNnPc4	vojsko
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
zvrhla	zvrhnout	k5eAaPmAgFnS	zvrhnout
v	v	k7c4	v
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Statisíce	statisíce	k1gInPc1	statisíce
lidí	člověk	k1gMnPc2	člověk
musely	muset	k5eAaImAgInP	muset
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
a	a	k8xC	a
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
jich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgFnP	být
povražděny	povraždit	k5eAaPmNgFnP	povraždit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
nové	nový	k2eAgFnPc1d1	nová
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgFnP	být
opozicí	opozice	k1gFnSc7	opozice
označeny	označen	k2eAgFnPc1d1	označena
za	za	k7c4	za
nesvobodné	svobodný	k2eNgNnSc4d1	nesvobodné
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ale	ale	k8xC	ale
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
základny	základna	k1gFnSc2	základna
islámských	islámský	k2eAgMnPc2d1	islámský
radikálů	radikál	k1gMnPc2	radikál
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
převážně	převážně	k6eAd1	převážně
severozápadní	severozápadní	k2eAgNnSc4d1	severozápadní
území	území	k1gNnSc4	území
obydlené	obydlený	k2eAgNnSc4d1	obydlené
Uzbeky	Uzbek	k1gMnPc7	Uzbek
pro	pro	k7c4	pro
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
uzbecké	uzbecký	k2eAgFnSc3d1	Uzbecká
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tádžická	tádžický	k2eAgFnSc1d1	tádžická
vláda	vláda	k1gFnSc1	vláda
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
proti	proti	k7c3	proti
radikálům	radikál	k1gInPc3	radikál
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poslání	poslání	k1gNnSc4	poslání
vojska	vojsko	k1gNnSc2	vojsko
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
obydlené	obydlený	k2eAgFnSc2d1	obydlená
Uzbeky	Uzbek	k1gMnPc4	Uzbek
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
zažila	zažít	k5eAaPmAgFnS	zažít
další	další	k2eAgInSc4d1	další
šok	šok	k1gInSc4	šok
po	po	k7c4	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
začalo	začít	k5eAaPmAgNnS	začít
proudit	proudit	k5eAaImF	proudit
mnoho	mnoho	k4c4	mnoho
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k6eAd1	právě
probíhala	probíhat	k5eAaImAgFnS	probíhat
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Tálibánem	Tálibán	k1gInSc7	Tálibán
a	a	k8xC	a
vojsky	vojsky	k6eAd1	vojsky
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
jednotky	jednotka	k1gFnPc1	jednotka
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
bojkotované	bojkotovaný	k2eAgFnPc1d1	bojkotovaná
většinou	většinou	k6eAd1	většinou
hlavních	hlavní	k2eAgFnPc2d1	hlavní
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
však	však	k9	však
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
méně	málo	k6eAd2	málo
represivní	represivní	k2eAgInPc1d1	represivní
než	než	k8xS	než
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
desetiletí	desetiletí	k1gNnSc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
není	být	k5eNaImIp3nS	být
náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
zákaz	zákaz	k1gInSc1	zákaz
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgFnPc2d1	Jehovova
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
začali	začít	k5eAaPmAgMnP	začít
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
bývalém	bývalý	k2eAgInSc6d1	bývalý
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc4	hora
tvoří	tvořit	k5eAaImIp3nS	tvořit
93	[number]	k4	93
%	%	kIx~	%
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
%	%	kIx~	%
země	zem	k1gFnPc1	zem
leží	ležet	k5eAaImIp3nP	ležet
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
horská	horský	k2eAgNnPc1d1	horské
pásma	pásmo	k1gNnPc1	pásmo
na	na	k7c6	na
území	území	k1gNnSc6	území
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
jsou	být	k5eAaImIp3nP	být
Pamír	Pamír	k1gInSc4	Pamír
a	a	k8xC	a
Zaalajský	Zaalajský	k2eAgInSc4d1	Zaalajský
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Zeravšanský	Zeravšanský	k2eAgInSc4d1	Zeravšanský
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
Turkestánský	turkestánský	k2eAgInSc4d1	turkestánský
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc4	součást
soustavy	soustava	k1gFnSc2	soustava
Pamíro-Alaj	Pamíro-Alaj	k1gFnSc4	Pamíro-Alaj
<g/>
,	,	kIx,	,
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
dvě	dva	k4xCgFnPc1	dva
nížiny	nížina	k1gFnPc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nížina	nížina	k1gFnSc1	nížina
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Pandž	Pandž	k1gFnSc4	Pandž
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Ferganská	Ferganský	k2eAgFnSc1d1	Ferganská
kotlina	kotlina	k1gFnSc1	kotlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
rozvinuta	rozvinout	k5eAaPmNgFnS	rozvinout
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Nejhustší	hustý	k2eAgFnSc1d3	nejhustší
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
nejřidší	řídký	k2eAgMnPc1d3	řídký
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
řeky	řeka	k1gFnPc1	řeka
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
povodím	povodit	k5eAaPmIp1nS	povodit
Amúdarji	Amúdarj	k1gFnSc3	Amúdarj
<g/>
,	,	kIx,	,
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
a	a	k8xC	a
Zeravšánu	Zeravšán	k1gInSc3	Zeravšán
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Pamíru	Pamír	k1gInSc6	Pamír
nevelké	velký	k2eNgFnSc2d1	nevelká
řeky	řeka	k1gFnSc2	řeka
Karadžilga	Karadžilga	k1gFnSc1	Karadžilga
<g/>
,	,	kIx,	,
Akdžilga	Akdžilga	k1gFnSc1	Akdžilga
a	a	k8xC	a
Muzkol	Muzkol	k1gInSc1	Muzkol
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
bezodtokého	bezodtoký	k2eAgNnSc2d1	bezodtoké
jezera	jezero	k1gNnSc2	jezero
Karakul	Karakul	k1gInSc1	Karakul
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Markansu	Markans	k1gInSc2	Markans
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
řeky	řeka	k1gFnSc2	řeka
Tarim	Tarima	k1gFnPc2	Tarima
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodit	k5eAaPmIp3nS	povodit
Amúdarji	Amúdarje	k1gFnSc4	Amúdarje
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
3⁄	3⁄	k?	3⁄
rozlohy	rozloha	k1gFnSc2	rozloha
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
náleží	náležet	k5eAaImIp3nS	náležet
zdrojnice	zdrojnice	k1gFnSc2	zdrojnice
Amúdarji	Amúdarje	k1gFnSc4	Amúdarje
Pandž	Pandž	k1gFnSc4	Pandž
(	(	kIx(	(
<g/>
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
Ghunt	Ghunta	k1gFnPc2	Ghunta
<g/>
,	,	kIx,	,
Bartang	Bartanga	k1gFnPc2	Bartanga
<g/>
,	,	kIx,	,
Jazghulóm	Jazghulóma	k1gFnPc2	Jazghulóma
<g/>
,	,	kIx,	,
Vandž	Vandž	k1gFnSc4	Vandž
<g/>
,	,	kIx,	,
Kyzylsu	Kyzylsa	k1gFnSc4	Kyzylsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vachš	Vachš	k1gMnSc1	Vachš
(	(	kIx(	(
<g/>
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
nad	nad	k7c7	nad
ústím	ústí	k1gNnSc7	ústí
Óbichingou	Óbichinga	k1gFnSc7	Óbichinga
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Surchob	Surchoba	k1gFnPc2	Surchoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kófirnihón	Kófirnihón	k1gInSc4	Kófirnihón
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
povodí	povodit	k5eAaPmIp3nS	povodit
Syrdarji	Syrdarja	k1gFnSc3	Syrdarja
(	(	kIx(	(
<g/>
protéká	protékat	k5eAaImIp3nS	protékat
severní	severní	k2eAgFnSc7d1	severní
částí	část	k1gFnSc7	část
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
195	[number]	k4	195
km	km	kA	km
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
stékají	stékat	k5eAaImIp3nP	stékat
ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
svahu	svah	k1gInSc2	svah
Turkestánského	turkestánský	k2eAgInSc2d1	turkestánský
hřbetu	hřbet	k1gInSc2	hřbet
(	(	kIx(	(
<g/>
Isfara	Isfara	k1gFnSc1	Isfara
<g/>
,	,	kIx,	,
Chodžabakirgan	Chodžabakirgan	k1gMnSc1	Chodžabakirgan
<g/>
,	,	kIx,	,
Karasú	Karasú	k1gMnSc1	Karasú
<g/>
,	,	kIx,	,
Áksú	Áksú	k1gMnSc1	Áksú
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
Zeravšánu	Zeravšán	k1gInSc2	Zeravšán
(	(	kIx(	(
<g/>
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
Fóndarjó	Fóndarjó	k1gMnPc2	Fóndarjó
<g/>
,	,	kIx,	,
Kštut	Kštut	k1gMnSc1	Kštut
a	a	k8xC	a
Móghijón	Móghijón	k1gMnSc1	Móghijón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
řek	řeka	k1gFnPc2	řeka
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hor	hora	k1gFnPc2	hora
má	mít	k5eAaImIp3nS	mít
ledovcový	ledovcový	k2eAgInSc1d1	ledovcový
zdroj	zdroj	k1gInSc1	zdroj
(	(	kIx(	(
<g/>
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
průtokem	průtok	k1gInSc7	průtok
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sněhovo-ledovcový	sněhovoedovcový	k2eAgMnSc1d1	sněhovo-ledovcový
(	(	kIx(	(
<g/>
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
průtokem	průtok	k1gInSc7	průtok
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pramení	pramenit	k5eAaImIp3nP	pramenit
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
výškách	výška	k1gFnPc6	výška
a	a	k8xC	a
níže	nízce	k6eAd2	nízce
mají	mít	k5eAaImIp3nP	mít
zdroj	zdroj	k1gInSc4	zdroj
především	především	k9	především
v	v	k7c6	v
tajícím	tající	k2eAgInSc6d1	tající
sněhu	sníh	k1gInSc6	sníh
<g/>
,	,	kIx,	,
dešti	dešť	k1gInSc6	dešť
a	a	k8xC	a
podzemních	podzemní	k2eAgFnPc6d1	podzemní
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
největší	veliký	k2eAgInSc4d3	veliký
průtok	průtok	k1gInSc4	průtok
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
zavlažování	zavlažování	k1gNnSc4	zavlažování
a	a	k8xC	a
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
hydroenergie	hydroenergie	k1gFnSc2	hydroenergie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
v	v	k7c6	v
Pamíru	Pamír	k1gInSc6	Pamír
a	a	k8xC	a
v	v	k7c6	v
Hisóro-Álajském	Hisóro-Álajský	k2eAgNnSc6d1	Hisóro-Álajský
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Karakul	Karakul	k1gInSc1	Karakul
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
Sarezské	Sarezský	k2eAgFnSc2d1	Sarezský
a	a	k8xC	a
Jašilkul	Jašilkula	k1gFnPc2	Jašilkula
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zavalením	zavalení	k1gNnSc7	zavalení
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
jezer	jezero	k1gNnPc2	jezero
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
závalem	zával	k1gInSc7	zával
Iskanderkul	Iskanderkul	k1gInSc1	Iskanderkul
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Hisórském	Hisórský	k2eAgInSc6d1	Hisórský
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
velké	velký	k2eAgFnPc1d1	velká
přehrady	přehrada	k1gFnPc1	přehrada
<g/>
:	:	kIx,	:
Kajrakkumská	Kajrakkumský	k2eAgNnPc1d1	Kajrakkumský
<g/>
,	,	kIx,	,
Nurecká	Nurecký	k2eAgNnPc1d1	Nurecký
<g/>
,	,	kIx,	,
Farchadská	Farchadský	k2eAgNnPc1d1	Farchadský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
horským	horský	k2eAgInSc7d1	horský
ledovcem	ledovec	k1gInSc7	ledovec
Asie	Asie	k1gFnSc2	Asie
je	být	k5eAaImIp3nS	být
Fedčenkův	Fedčenkův	k2eAgInSc1d1	Fedčenkův
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Pamír	Pamír	k1gInSc4	Pamír
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
71	[number]	k4	71
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Róghun	Róghuna	k1gFnPc2	Róghuna
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc1d3	veliký
hráz	hráz	k1gFnSc1	hráz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
306	[number]	k4	306
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
s	s	k7c7	s
horkým	horký	k2eAgNnSc7d1	horké
suchým	suchý	k2eAgNnSc7d1	suché
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
chladnou	chladný	k2eAgFnSc7d1	chladná
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Dušanbe	Dušanb	k1gInSc5	Dušanb
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
-25	-25	k4	-25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červencová	červencový	k2eAgFnSc1d1	červencová
28	[number]	k4	28
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
1	[number]	k4	1
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Tádžikové	Tádžik	k1gMnPc1	Tádžik
jsou	být	k5eAaImIp3nP	být
indoárijského	indoárijský	k2eAgMnSc4d1	indoárijský
původu	původa	k1gMnSc4	původa
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
okolních	okolní	k2eAgNnPc2d1	okolní
etnik	etnikum	k1gNnPc2	etnikum
jsou	být	k5eAaImIp3nP	být
původu	původ	k1gInSc2	původ
mongolského	mongolský	k2eAgMnSc2d1	mongolský
<g/>
.	.	kIx.	.
</s>
<s>
Tádžičtina	tádžičtina	k1gFnSc1	tádžičtina
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
íránské	íránský	k2eAgFnSc2d1	íránská
větve	větev	k1gFnSc2	větev
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
65	[number]	k4	65
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Tádžikové	Tádžik	k1gMnPc1	Tádžik
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Uzbeci	Uzbek	k1gMnPc1	Uzbek
(	(	kIx(	(
<g/>
24	[number]	k4	24
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
tvoří	tvořit	k5eAaImIp3nS	tvořit
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
tvoří	tvořit	k5eAaImIp3nP	tvořit
nyní	nyní	k6eAd1	nyní
pouze	pouze	k6eAd1	pouze
1,1	[number]	k4	1,1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
emigraci	emigrace	k1gFnSc3	emigrace
stále	stále	k6eAd1	stále
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
tvořili	tvořit	k5eAaImAgMnP	tvořit
Rusové	Rus	k1gMnPc1	Rus
7,6	[number]	k4	7,6
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
etniky	etnikum	k1gNnPc7	etnikum
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Tataři	Tatar	k1gMnPc1	Tatar
či	či	k8xC	či
Kyrgyzové	Kyrgyz	k1gMnPc1	Kyrgyz
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
silně	silně	k6eAd1	silně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
muslimští	muslimský	k2eAgMnPc1d1	muslimský
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žijí	žít	k5eAaImIp3nP	žít
také	také	k9	také
menšiny	menšina	k1gFnPc1	menšina
ismailitů	ismailit	k1gInPc2	ismailit
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
obviňována	obviňovat	k5eAaImNgFnS	obviňovat
z	z	k7c2	z
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
Bourají	bourat	k5eAaImIp3nP	bourat
se	se	k3xPyFc4	se
historické	historický	k2eAgFnPc1d1	historická
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patřily	patřit	k5eAaImAgFnP	patřit
bucharským	bucharský	k2eAgMnSc7d1	bucharský
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c4	na
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
je	být	k5eAaImIp3nS	být
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nejslabší	slabý	k2eAgFnPc1d3	nejslabší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
postsovětských	postsovětský	k2eAgFnPc2d1	postsovětská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
42,1	[number]	k4	42,1
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
HDP	HDP	kA	HDP
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
remitencí	remitence	k1gFnPc2	remitence
(	(	kIx(	(
<g/>
peněžních	peněžní	k2eAgInPc2d1	peněžní
převodů	převod	k1gInPc2	převod
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
Tádžiků	Tádžik	k1gMnPc2	Tádžik
pracujících	pracující	k2eAgMnPc2d1	pracující
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
podílů	podíl	k1gInPc2	podíl
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
hnědé	hnědý	k2eAgNnSc1d1	hnědé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
rtuť	rtuť	k1gFnSc1	rtuť
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
těžební	těžební	k2eAgInSc4d1	těžební
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
hutnický	hutnický	k2eAgInSc4d1	hutnický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
jaci	jak	k1gMnPc1	jak
a	a	k8xC	a
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Využívány	využíván	k2eAgInPc1d1	využíván
jsou	být	k5eAaImIp3nP	být
hydroenergetické	hydroenergetický	k2eAgInPc1d1	hydroenergetický
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Т	Т	k?	Т
с	с	k?	с
с	с	k?	с
р	р	k?	р
(	(	kIx(	(
<g/>
В	В	k?	В
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROUX	ROUX	kA	ROUX
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
867	[number]	k4	867
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgFnPc1d1	cestovní
informace	informace	k1gFnPc1	informace
</s>
</p>
<p>
<s>
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
zahyne	zahynout	k5eAaPmIp3nS	zahynout
na	na	k7c6	na
endogamii	endogamie	k1gFnSc6	endogamie
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
South	South	k1gMnSc1	South
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-26	[number]	k4	2011-07-26
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Tajikistan	Tajikistan	k1gInSc4	Tajikistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2007-01-19	[number]	k4	2007-01-19
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Taškentu	Taškent	k1gInSc6	Taškent
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008-01-07	[number]	k4	2008-01-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ALLWORTH	ALLWORTH	kA	ALLWORTH
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tajikistan	Tajikistan	k1gInSc1	Tajikistan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
