<s>
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
či	či	k8xC	či
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchou-č	-čchou-č	k1gMnSc1	-čchou-č
<g/>
'	'	kIx"	'
<g/>
-lu	u	k?	-lu
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gMnSc7	pchin-jin
Sī	Sī	k1gMnSc2	Sī
zhī	zhī	k?	zhī
Lù	Lù	k1gMnSc2	Lù
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
丝	丝	k?	丝
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Silk	silk	k1gInSc1	silk
Road	Roada	k1gFnPc2	Roada
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Seidenstraße	Seidenstraße	k1gFnSc1	Seidenstraße
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověká	starověký	k2eAgFnSc1d1	starověká
a	a	k8xC	a
středověká	středověký	k2eAgFnSc1d1	středověká
trasa	trasa	k1gFnSc1	trasa
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
