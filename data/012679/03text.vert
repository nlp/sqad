<p>
<s>
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
či	či	k8xC	či
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchou-č	-čchou-č	k1gMnSc1	-čchou-č
<g/>
'	'	kIx"	'
<g/>
-lu	u	k?	-lu
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gMnSc7	pchin-jin
Sī	Sī	k1gMnSc2	Sī
zhī	zhī	k?	zhī
Lù	Lù	k1gMnSc2	Lù
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
丝	丝	k?	丝
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Silk	silk	k1gInSc1	silk
Road	Roada	k1gFnPc2	Roada
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Seidenstraße	Seidenstraße	k1gFnSc1	Seidenstraße
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověká	starověký	k2eAgFnSc1d1	starověká
a	a	k8xC	a
středověká	středověký	k2eAgFnSc1d1	středověká
trasa	trasa	k1gFnSc1	trasa
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavní	hlavní	k2eAgFnSc6d1	hlavní
větvi	větev	k1gFnSc6	větev
spojující	spojující	k2eAgNnSc1d1	spojující
čínské	čínský	k2eAgNnSc1d1	čínské
město	město	k1gNnSc1	město
Čchang-an	Čchangn	k1gInSc1	Čchang-an
(	(	kIx(	(
<g/>
Si-an	Sin	k1gInSc1	Si-an
<g/>
)	)	kIx)	)
s	s	k7c7	s
Malou	malý	k2eAgFnSc7d1	malá
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
a	a	k8xC	a
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
8	[number]	k4	8
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obchod	obchod	k1gInSc1	obchod
po	po	k7c6	po
hedvábné	hedvábný	k2eAgFnSc6d1	hedvábná
stezce	stezka	k1gFnSc6	stezka
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
rozvoji	rozvoj	k1gInSc6	rozvoj
velkých	velký	k2eAgFnPc2d1	velká
starověkých	starověký	k2eAgFnPc2d1	starověká
civilizací	civilizace	k1gFnPc2	civilizace
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
Persii	Persie	k1gFnSc6	Persie
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
položit	položit	k5eAaPmF	položit
základy	základ	k1gInPc4	základ
moderního	moderní	k2eAgInSc2d1	moderní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
použil	použít	k5eAaPmAgInS	použít
název	název	k1gInSc1	název
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
zeměpisec	zeměpisec	k1gMnSc1	zeměpisec
baron	baron	k1gMnSc1	baron
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Richthofen	Richthofen	k1gInSc1	Richthofen
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
existovaly	existovat	k5eAaImAgInP	existovat
kontakty	kontakt	k1gInPc1	kontakt
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
kontakty	kontakt	k1gInPc1	kontakt
obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
a	a	k8xC	a
i	i	k9	i
jejich	jejich	k3xOp3gFnSc7	jejich
zásluhou	zásluha	k1gFnSc7	zásluha
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
kultury	kultura	k1gFnPc1	kultura
začaly	začít	k5eAaPmAgFnP	začít
postupně	postupně	k6eAd1	postupně
poznávat	poznávat	k5eAaImF	poznávat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
však	však	k9	však
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
styky	styk	k1gInPc1	styk
musely	muset	k5eAaImAgInP	muset
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
pomocí	pomocí	k7c2	pomocí
prostředníků	prostředník	k1gInPc2	prostředník
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
odpověď	odpověď	k1gFnSc1	odpověď
trvala	trvat	k5eAaImAgFnS	trvat
velice	velice	k6eAd1	velice
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
však	však	k9	však
byli	být	k5eAaImAgMnP	být
překážkou	překážka	k1gFnSc7	překážka
obchodního	obchodní	k2eAgInSc2d1	obchodní
styku	styk	k1gInSc2	styk
výbojní	výbojný	k2eAgMnPc1d1	výbojný
Parthové	Parth	k1gMnPc1	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Augustus	Augustus	k1gMnSc1	Augustus
dokázal	dokázat	k5eAaPmAgMnS	dokázat
na	na	k7c4	na
čas	čas	k1gInSc4	čas
s	s	k7c7	s
Parthy	Parth	k1gInPc7	Parth
sjednat	sjednat	k5eAaPmF	sjednat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
cesty	cesta	k1gFnPc4	cesta
na	na	k7c4	na
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
bezpečnými	bezpečný	k2eAgFnPc7d1	bezpečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
dovážela	dovážet	k5eAaImAgFnS	dovážet
spousta	spousta	k1gFnSc1	spousta
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
milovali	milovat	k5eAaImAgMnP	milovat
tyto	tento	k3xDgFnPc4	tento
jemné	jemný	k2eAgFnPc4d1	jemná
tkaniny	tkanina	k1gFnPc4	tkanina
a	a	k8xC	a
náležitě	náležitě	k6eAd1	náležitě
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
platili	platit	k5eAaImAgMnP	platit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikal	vznikat	k5eAaImAgInS	vznikat
veliký	veliký	k2eAgInSc1d1	veliký
obchodní	obchodní	k2eAgInSc1d1	obchodní
deficit	deficit	k1gInSc1	deficit
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
následně	následně	k6eAd1	následně
omezovali	omezovat	k5eAaImAgMnP	omezovat
tyto	tento	k3xDgInPc4	tento
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ekonomicky	ekonomicky	k6eAd1	ekonomicky
Římskou	římský	k2eAgFnSc4d1	římská
říši	říše	k1gFnSc4	říše
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
její	její	k3xOp3gFnSc1	její
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
devastovaly	devastovat	k5eAaBmAgFnP	devastovat
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
však	však	k9	však
používali	používat	k5eAaImAgMnP	používat
hedvábí	hedvábí	k1gNnSc4	hedvábí
také	také	k6eAd1	také
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
luxusních	luxusní	k2eAgInPc2d1	luxusní
darů	dar	k1gInPc2	dar
pro	pro	k7c4	pro
náčelníky	náčelník	k1gInPc4	náčelník
Germánů	Germán	k1gMnPc2	Germán
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
loajalitu	loajalita	k1gFnSc4	loajalita
si	se	k3xPyFc3	se
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Parthové	Parthové	k2eAgFnSc1d1	Parthové
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
sasánovští	sasánovský	k2eAgMnPc1d1	sasánovský
Peršané	Peršan	k1gMnPc1	Peršan
bohatli	bohatnout	k5eAaImAgMnP	bohatnout
především	především	k6eAd1	především
na	na	k7c6	na
clech	clo	k1gNnPc6	clo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
hranicích	hranice	k1gFnPc6	hranice
od	od	k7c2	od
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
hedvábím	hedvábí	k1gNnSc7	hedvábí
vybírali	vybírat	k5eAaImAgMnP	vybírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Říma	Řím	k1gInSc2	Řím
nastal	nastat	k5eAaPmAgInS	nastat
nový	nový	k2eAgInSc1d1	nový
rozvoj	rozvoj	k1gInSc1	rozvoj
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchang	k1gInSc1	Tchang
(	(	kIx(	(
<g/>
618	[number]	k4	618
<g/>
–	–	k?	–
<g/>
907	[number]	k4	907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
vzala	vzít	k5eAaPmAgFnS	vzít
Persii	Persie	k1gFnSc4	Persie
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
těmito	tento	k3xDgFnPc7	tento
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Li	li	k8xS	li
Šimin	Šimin	k1gMnSc1	Šimin
(	(	kIx(	(
<g/>
Tchaj-cung	Tchajung	k1gMnSc1	Tchaj-cung
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládali	ovládat	k5eAaImAgMnP	ovládat
stezku	stezka	k1gFnSc4	stezka
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
;	;	kIx,	;
i	i	k9	i
přes	přes	k7c4	přes
pověsti	pověst	k1gFnPc4	pověst
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
dobyvačnosti	dobyvačnost	k1gFnSc6	dobyvačnost
a	a	k8xC	a
agresivitě	agresivita	k1gFnSc6	agresivita
se	se	k3xPyFc4	se
k	k	k7c3	k
Evropanům	Evropan	k1gMnPc3	Evropan
putujícím	putující	k2eAgMnPc3d1	putující
touto	tento	k3xDgFnSc7	tento
stezkou	stezka	k1gFnSc7	stezka
chovali	chovat	k5eAaImAgMnP	chovat
přívětivě	přívětivě	k6eAd1	přívětivě
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mohlo	moct	k5eAaImAgNnS	moct
množství	množství	k1gNnSc1	množství
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tudy	tudy	k6eAd1	tudy
proudilo	proudit	k5eAaImAgNnS	proudit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
však	však	k9	však
postupně	postupně	k6eAd1	postupně
oslabovalo	oslabovat	k5eAaImAgNnS	oslabovat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1368	[number]	k4	1368
získala	získat	k5eAaPmAgFnS	získat
znovu	znovu	k6eAd1	znovu
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
cestou	cesta	k1gFnSc7	cesta
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
intenzita	intenzita	k1gFnSc1	intenzita
obchodů	obchod	k1gInPc2	obchod
prudce	prudko	k6eAd1	prudko
klesla	klesnout	k5eAaPmAgFnS	klesnout
a	a	k8xC	a
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
už	už	k6eAd1	už
stavu	stav	k1gInSc2	stav
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchanga	k1gFnPc2	Tchanga
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
zámořského	zámořský	k2eAgNnSc2d1	zámořské
putování	putování	k1gNnSc2	putování
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
cesty	cesta	k1gFnSc2	cesta
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1514	[number]	k4	1514
Portugalci	Portugalec	k1gMnPc1	Portugalec
založili	založit	k5eAaPmAgMnP	založit
první	první	k4xOgInSc4	první
zámořský	zámořský	k2eAgInSc4d1	zámořský
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
spoj	spoj	k1gInSc4	spoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
později	pozdě	k6eAd2	pozdě
převzali	převzít	k5eAaPmAgMnP	převzít
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nová	nový	k2eAgFnSc1d1	nová
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
čínský	čínský	k2eAgMnSc1d1	čínský
prezident	prezident	k1gMnSc1	prezident
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
záměr	záměr	k1gInSc4	záměr
vybudování	vybudování	k1gNnSc4	vybudování
Nové	Nová	k1gFnSc2	Nová
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
vhodného	vhodný	k2eAgNnSc2d1	vhodné
zboží	zboží	k1gNnSc2	zboží
v	v	k7c6	v
kontejnerech	kontejner	k1gInPc6	kontejner
využívány	využívat	k5eAaPmNgInP	využívat
již	již	k9	již
existující	existující	k2eAgFnPc1d1	existující
železniční	železniční	k2eAgFnPc1d1	železniční
trati	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
samotné	samotný	k2eAgFnSc6d1	samotná
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
a	a	k8xC	a
tratě	trať	k1gFnPc1	trať
přes	přes	k7c4	přes
evropské	evropský	k2eAgNnSc4d1	Evropské
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
,	,	kIx,	,
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
Duisburg	Duisburg	k1gInSc1	Duisburg
důležitým	důležitý	k2eAgNnSc7d1	důležité
překladištěm	překladiště	k1gNnSc7	překladiště
u	u	k7c2	u
splavné	splavný	k2eAgFnSc2d1	splavná
řeky	řeka	k1gFnSc2	řeka
Rýn	rýna	k1gFnPc2	rýna
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
hedvábnou	hedvábný	k2eAgFnSc4d1	hedvábná
stezku	stezka	k1gFnSc4	stezka
napojeny	napojen	k2eAgFnPc4d1	napojena
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
přijel	přijet	k5eAaPmAgMnS	přijet
první	první	k4xOgInSc4	první
vlak	vlak	k1gInSc4	vlak
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
čínské	čínský	k2eAgFnSc2d1	čínská
provincie	provincie	k1gFnSc2	provincie
Če-ťiang	Če-ťianga	k1gFnPc2	Če-ťianga
do	do	k7c2	do
Teheránu	Teherán	k1gInSc2	Teherán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
vypraven	vypravit	k5eAaPmNgInS	vypravit
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
až	až	k9	až
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
celou	celý	k2eAgFnSc4d1	celá
trasu	trasa	k1gFnSc4	trasa
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
12	[number]	k4	12
000	[number]	k4	000
km	km	kA	km
ujel	ujet	k5eAaPmAgMnS	ujet
za	za	k7c4	za
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
18	[number]	k4	18
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zboží	zboží	k1gNnSc1	zboží
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
několikrát	několikrát	k6eAd1	několikrát
překládáno	překládán	k2eAgNnSc1d1	překládáno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kvůli	kvůli	k7c3	kvůli
rozdílným	rozdílný	k2eAgInPc3d1	rozdílný
rozchodům	rozchod	k1gInPc3	rozchod
kolejí	kolej	k1gFnPc2	kolej
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
měněny	měnit	k5eAaImNgFnP	měnit
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
i	i	k8xC	i
vagóny	vagón	k1gInPc1	vagón
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
napojení	napojení	k1gNnSc4	napojení
Rakouska	Rakousko	k1gNnSc2	Rakousko
přes	přes	k7c4	přes
Slovensko	Slovensko	k1gNnSc4	Slovensko
na	na	k7c4	na
čínský	čínský	k2eAgInSc4d1	čínský
projekt	projekt	k1gInSc4	projekt
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nová	nový	k2eAgFnSc1d1	nová
asi	asi	k9	asi
450	[number]	k4	450
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
širokorozchodná	širokorozchodný	k2eAgFnSc1d1	širokorozchodná
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Košic	Košice	k1gInPc2	Košice
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
již	již	k9	již
taková	takový	k3xDgFnSc1	takový
trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Užhorodu	Užhorod	k1gInSc2	Užhorod
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Bratislavu	Bratislava	k1gFnSc4	Bratislava
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
mají	mít	k5eAaImIp3nP	mít
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
dráhy	dráha	k1gFnSc2	dráha
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
mu	on	k3xPp3gInSc3	on
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
válka	válka	k1gFnSc1	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Silk	silk	k1gInSc1	silk
Road	Road	k1gInSc1	Road
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Liščák	Liščák	k1gMnSc1	Liščák
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
–	–	k?	–
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
Hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
styků	styk	k1gInPc2	styk
Východ	východ	k1gInSc4	východ
–	–	k?	–
Západ	západ	k1gInSc1	západ
<g/>
.	.	kIx.	.
</s>
<s>
Set	set	k1gInSc1	set
out	out	k?	out
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86277-11-9	[number]	k4	80-86277-11-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
stezky	stezka	k1gFnSc2	stezka
(	(	kIx(	(
<g/>
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
stezky	stezka	k1gFnSc2	stezka
</s>
</p>
<p>
<s>
The	The	k?	The
Silk	silk	k1gInSc1	silk
Road	Road	k1gMnSc1	Road
Project	Project	k1gMnSc1	Project
</s>
</p>
