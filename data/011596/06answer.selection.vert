<s>
Její	její	k3xOp3gFnSc1	její
podskupina	podskupina	k1gFnSc1	podskupina
A1	A1	k1gFnSc2	A1
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
podskupina	podskupina	k1gFnSc1	podskupina
A2	A2	k1gFnSc1	A2
pak	pak	k6eAd1	pak
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
haploskupin	haploskupina	k1gFnPc2	haploskupina
nalezených	nalezený	k2eAgFnPc2d1	nalezená
mezi	mezi	k7c7	mezi
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
