<p>
<s>
Haploskupina	Haploskupina	k1gFnSc1	Haploskupina
A	a	k9	a
je	být	k5eAaImIp3nS	být
haploskupina	haploskupina	k1gFnSc1	haploskupina
lidské	lidský	k2eAgFnSc2d1	lidská
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haploskupina	Haploskupina	k1gFnSc1	Haploskupina
A	a	k9	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c4	před
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
předhůdkyní	předhůdkyně	k1gFnSc7	předhůdkyně
byla	být	k5eAaImAgFnS	být
haploskupina	haploskupina	k1gFnSc1	haploskupina
N.	N.	kA	N.
</s>
</p>
<p>
<s>
Haploskupina	Haploskupina	k1gFnSc1	Haploskupina
A	A	kA	A
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
podskupina	podskupina	k1gFnSc1	podskupina
A1	A1	k1gFnSc2	A1
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
podskupina	podskupina	k1gFnSc1	podskupina
A2	A2	k1gFnSc1	A2
pak	pak	k6eAd1	pak
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
haploskupin	haploskupina	k1gFnPc2	haploskupina
nalezených	nalezený	k2eAgFnPc2d1	nalezená
mezi	mezi	k7c7	mezi
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bryan	Bryan	k1gMnSc1	Bryan
Sykes	Sykes	k1gMnSc1	Sykes
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Sedm	sedm	k4xCc4	sedm
dcer	dcera	k1gFnPc2	dcera
Eviných	Evin	k2eAgFnPc2d1	Evina
pro	pro	k7c4	pro
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
haploskupiny	haploskupina	k1gFnSc2	haploskupina
A	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Aiyana	Aiyan	k1gMnSc2	Aiyan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Haploskupiny	Haploskupin	k1gInPc1	Haploskupin
lidské	lidský	k2eAgFnSc2d1	lidská
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
</s>
</p>
<p>
<s>
Genealogický	genealogický	k2eAgInSc1d1	genealogický
test	test	k1gInSc1	test
DNAV	DNAV	kA	DNAV
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Haplogroup	Haplogroup	k1gInSc1	Haplogroup
A	A	kA	A
(	(	kIx(	(
<g/>
mtDNA	mtDNA	k?	mtDNA
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
