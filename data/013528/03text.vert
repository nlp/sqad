<s>
Pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
</s>
<s>
Pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Autor	autor	k1gMnSc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
ZoulaČeněk	ZoulaČeňka	k1gFnPc2
Vosmík	Vosmík	k1gMnSc1
Rok	rok	k1gInSc4
vzniku	vznik	k1gInSc2
</s>
<s>
1898	#num#	k4
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
40070	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1206	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Popis	popis	k1gInSc1
Materiál	materiál	k1gInSc1
</s>
<s>
bronz	bronz	k1gInSc4
Umístění	umístění	k1gNnSc2
Umístění	umístění	k1gNnPc2
</s>
<s>
Karlovo	Karlův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
25,5	25,5	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
9,54	9,54	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
popis	popis	k1gInSc1
</s>
<s>
Pomník	pomník	k1gInSc1
botaniku	botanik	k1gMnSc3
Benediktu	Benedikt	k1gMnSc3
Roezlovi	Roezl	k1gMnSc3
dal	dát	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
mezinárodní	mezinárodní	k2eAgInSc4d1
komitét	komitét	k1gInSc4
ustavený	ustavený	k2eAgInSc4d1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
poskytl	poskytnout	k5eAaPmAgMnS
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalo	trvat	k5eAaImAgNnS
třináct	třináct	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
pražští	pražský	k2eAgMnPc1d1
radní	radní	k1gMnPc1
pod	pod	k7c7
tlakem	tlak	k1gInSc7
zahraniční	zahraniční	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
souhlasili	souhlasit	k5eAaImAgMnP
s	s	k7c7
umístěním	umístění	k1gNnSc7
pomníku	pomník	k1gInSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Karlova	Karlův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
poblíž	poblíž	k7c2
tzv.	tzv.	kA
Faustova	Faustův	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bronzové	bronzový	k2eAgNnSc1d1
sousoší	sousoší	k1gNnSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
vytvořili	vytvořit	k5eAaPmAgMnP
sochaři	sochař	k1gMnPc1
Gustav	Gustav	k1gMnSc1
Zoula	Zoula	k1gMnSc1
(	(	kIx(
<g/>
socha	socha	k1gFnSc1
B.	B.	kA
Roezla	Roezla	k1gFnSc2
v	v	k7c6
pískovci	pískovec	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
Čeněk	Čeněk	k1gMnSc1
Vosmík	Vosmík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
architektonického	architektonický	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
pomníku	pomník	k1gInSc2
byl	být	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
Sochor	Sochor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1
pískovcový	pískovcový	k2eAgInSc1d1
sokl	sokl	k1gInSc1
s	s	k7c7
bronzovým	bronzový	k2eAgNnSc7d1
sousoším	sousoší	k1gNnSc7
a	a	k8xC
nápisy	nápis	k1gInPc7
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
na	na	k7c6
pětistupňovém	pětistupňový	k2eAgInSc6d1
polokruhovém	polokruhový	k2eAgInSc6d1
stylobatu	stylobat	k1gInSc6
<g/>
,	,	kIx,
uzavřeném	uzavřený	k2eAgInSc6d1
po	po	k7c6
stranách	strana	k1gFnPc6
nízkou	nízký	k2eAgFnSc7d1
římsou	římsa	k1gFnSc7
a	a	k8xC
dvěma	dva	k4xCgFnPc7
kamennými	kamenný	k2eAgFnPc7d1
vázami	váza	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstup	vstup	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
uzavřen	uzavřít	k5eAaPmNgInS
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
chybějícím	chybějící	k2eAgInSc7d1
<g/>
)	)	kIx)
ozdobným	ozdobný	k2eAgInSc7d1
bronzovým	bronzový	k2eAgInSc7d1
řetězem	řetěz	k1gInSc7
zavěšeným	zavěšený	k2eAgInSc7d1
na	na	k7c6
dvou	dva	k4xCgInPc6
mosazných	mosazný	k2eAgInPc6d1
sloupcích	sloupec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
<g/>
,	,	kIx,
oddělenou	oddělený	k2eAgFnSc4d1
dvěma	dva	k4xCgInPc7
stupni	stupeň	k1gInPc7
<g/>
,	,	kIx,
obklopuje	obklopovat	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
podkovovitá	podkovovitý	k2eAgFnSc1d1
zídka	zídka	k1gFnSc1
z	z	k7c2
opracovaného	opracovaný	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
zakončená	zakončený	k2eAgFnSc1d1
v	v	k7c6
čele	čelo	k1gNnSc6
vyšším	vysoký	k2eAgInSc7d2
kamennou	kamenný	k2eAgFnSc7d1
nadstavbou	nadstavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc1d1
piedestal	piedestal	k1gInSc1
sochy	socha	k1gFnSc2
má	mít	k5eAaImIp3nS
čtvercový	čtvercový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
konkávně	konkávně	k6eAd1
zužuje	zužovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
profilovaná	profilovaný	k2eAgFnSc1d1
římsa	římsa	k1gFnSc1
a	a	k8xC
ustupující	ustupující	k2eAgInSc1d1
vlastní	vlastní	k2eAgInSc1d1
sokl	sokl	k1gInSc1
sousoší	sousoší	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piedestal	piedestal	k1gInSc1
obklopují	obklopovat	k5eAaImIp3nP
na	na	k7c6
třech	tři	k4xCgFnPc6
stranách	strana	k1gFnPc6
kamenné	kamenný	k2eAgFnSc2d1
lavice	lavice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
jsou	být	k5eAaImIp3nP
nápisy	nápis	k1gInPc1
BENEDIKTU	benedikt	k1gInSc2
ROEZLOVI	ROEZLOVI	kA
(	(	kIx(
<g/>
12.8	12.8	k4
<g/>
.1824	.1824	k4
<g/>
,	,	kIx,
14.10	14.10	k4
<g/>
.1885	.1885	k4
<g/>
)	)	kIx)
/	/	kIx~
SLAVNÉMU	slavný	k2eAgInSc3d1
/	/	kIx~
BOTANIKU	botanik	k1gMnSc3
A	a	k8xC
CESTOVATELI	cestovatel	k1gMnSc3
/	/	kIx~
VĚNUJÍ	věnovat	k5eAaPmIp3nP,k5eAaImIp3nP
/	/	kIx~
JEHO	jeho	k3xOp3gMnPc1
CTITELÉ	ctitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
římsou	římsa	k1gFnSc7
je	být	k5eAaImIp3nS
kovový	kovový	k2eAgInSc1d1
znak	znak	k1gInSc1
Prahy	Praha	k1gFnSc2
a	a	k8xC
pás	pás	k1gInSc1
s	s	k7c7
květy	květ	k1gInPc7
orchidejí	orchidea	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sousoší	sousoší	k1gNnSc1
představuje	představovat	k5eAaImIp3nS
stojícího	stojící	k2eAgMnSc4d1
botanika	botanik	k1gMnSc4
a	a	k8xC
cestovatele	cestovatel	k1gMnPc4
v	v	k7c6
klobouku	klobouk	k1gInSc6
a	a	k8xC
rozepjatém	rozepjatý	k2eAgInSc6d1
kabátě	kabát	k1gInSc6
s	s	k7c7
knihou	kniha	k1gFnSc7
v	v	k7c6
pravé	pravá	k1gFnSc6
a	a	k8xC
bronzovou	bronzový	k2eAgFnSc7d1
květinou	květina	k1gFnSc7
v	v	k7c6
levé	levý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
nakročenou	nakročený	k2eAgFnSc7d1
levou	levý	k2eAgFnSc7d1
nohou	noha	k1gFnSc7
sedí	sedit	k5eAaImIp3nS
nahý	nahý	k2eAgMnSc1d1
indiánský	indiánský	k2eAgMnSc1d1
chlapec	chlapec	k1gMnSc1
s	s	k7c7
mačetou	mačeta	k1gFnSc7
v	v	k7c6
pravé	pravý	k2eAgFnSc6d1
ruce	ruka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
evidovaná	evidovaný	k2eAgFnSc1d1
v	v	k7c6
Ústředním	ústřední	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
pod	pod	k7c7
rejstříkovým	rejstříkový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
:	:	kIx,
40070	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1206	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Záměna	záměna	k1gFnSc1
osob	osoba	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s>
V	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
čísle	číslo	k1gNnSc6
15	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
1897	#num#	k4
<g/>
)	)	kIx)
vyšel	vyjít	k5eAaPmAgInS
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
časopisu	časopis	k1gInSc2
Zlatá	zlatá	k1gFnSc1
Praha	Praha	k1gFnSc1
text	text	k1gInSc1
<g/>
,	,	kIx,
podepsaný	podepsaný	k2eAgInSc1d1
jen	jen	k9
písmenem	písmeno	k1gNnSc7
M.	M.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Autor	autor	k1gMnSc1
v	v	k7c6
textu	text	k1gInSc6
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
plánování	plánování	k1gNnSc6
a	a	k8xC
výstavbě	výstavba	k1gFnSc3
pomníku	pomník	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
záměně	záměna	k1gFnSc3
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
dvojí	dvojit	k5eAaImIp3nS
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
Presl	Presl	k1gInSc1
/	/	kIx~
Josef	Josef	k1gMnSc1
Ressel	Ressel	k1gMnSc1
/	/	kIx~
Benedikt	Benedikt	k1gMnSc1
Roezl	Roezl	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
však	však	k9
zřejmě	zřejmě	k6eAd1
jen	jen	k9
o	o	k7c4
dobový	dobový	k2eAgInSc4d1
hoax	hoax	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
k	k	k7c3
tomuto	tento	k3xDgNnSc3
tvrzení	tvrzení	k1gNnSc3
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
jakékoli	jakýkoli	k3yIgInPc4
doklady	doklad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
často	často	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
fakt	fakt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
ale	ale	k9
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
místo	místo	k1gNnSc1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
Spolku	spolek	k1gInSc6
českých	český	k2eAgMnPc2d1
chemiků	chemik	k1gMnPc2
pro	pro	k7c4
vybudování	vybudování	k1gNnSc4
Preslova	Preslův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
přislíbeno	přislíben	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Článek	článek	k1gInSc1
byl	být	k5eAaImAgInS
sestaven	sestavit	k5eAaPmNgInS
s	s	k7c7
využitím	využití	k1gNnSc7
podkladů	podklad	k1gInPc2
NPÚ	NPÚ	kA
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://archiv.ucl.cas.cz/index.php?path=ZlataPrahaII/15.1897-1898/2/24.png	http://archiv.ucl.cas.cz/index.php?path=ZlataPrahaII/15.1897-1898/2/24.png	k1gInSc1
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Geneze	geneze	k1gFnSc1
pomníku	pomník	k1gInSc2
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Za	za	k7c4
starou	starý	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
.	.	kIx.
https://www.zastarouprahu.cz/webdata/D7B70C39-53BF-428E-AD43-A97619D02ABA_02.pdf	https://www.zastarouprahu.cz/webdata/D7B70C39-53BF-428E-AD43-A97619D02ABA_02.pdf	k1gInSc1
<g/>
↑	↑	k?
ČRO	ČRO	kA
2012	#num#	k4
<g/>
,	,	kIx,
Toulky	toulka	k1gFnSc2
českou	český	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
<g/>
,	,	kIx,
882	#num#	k4
<g/>
.	.	kIx.
schůzka	schůzka	k1gFnSc1
<g/>
:	:	kIx,
Muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dostal	dostat	k5eAaPmAgMnS
pomník	pomník	k1gInSc4
omylem	omylem	k6eAd1
<g/>
↑	↑	k?
S.	S.	kA
370	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgInPc1d1
listy	list	k1gInPc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
průmysl	průmysl	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
370	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Hrubešová	Hrubešová	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Hrubeš	Hrubeš	k1gMnSc1
<g/>
,	,	kIx,
Pražské	pražský	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
a	a	k8xC
pomníky	pomník	k1gInPc1
<g/>
,	,	kIx,
nakl	nakl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrklíč	petrklíč	k1gInSc1
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
88	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7229-076-2	80-7229-076-2	k4
</s>
<s>
Coats	Coats	k1gInSc1
A.	A.	kA
M.	M.	kA
<g/>
,	,	kIx,
The	The	k1gFnSc1
Plant	planta	k1gFnPc2
Hunters	Huntersa	k1gFnPc2
<g/>
,	,	kIx,
McGraw-Hill	McGraw-Hilla	k1gFnPc2
<g/>
,	,	kIx,
1970	#num#	k4
</s>
<s>
Kline	klinout	k5eAaImIp3nS
<g/>
,	,	kIx,
Mary	Mary	k1gFnSc1
C.	C.	kA
<g/>
,	,	kIx,
Benedict	Benedict	k2eAgMnSc1d1
Roezl	Roezl	k1gMnSc1
–	–	k?
Famous	Famous	k1gInSc1
orchid	orchida	k1gFnPc2
collectors	collectors	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amer.	Amer.	k1gMnSc1
Orch	Orch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soc	soc	kA
<g/>
.	.	kIx.
Bull	bulla	k1gFnPc2
<g/>
.	.	kIx.
32	#num#	k4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
8	#num#	k4
</s>
<s>
TLUSTÝ	tlustý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geneze	geneze	k1gFnSc1
pomníku	pomník	k1gInSc2
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
starou	starý	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XLIX	XLIX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
ČT	ČT	kA
2004	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
v	v	k7c6
rostlinné	rostlinný	k2eAgFnSc6d1
říši	říš	k1gFnSc6
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Král	Král	k1gMnSc1
orchidejí	orchidea	k1gFnPc2
</s>
<s>
IPNI	IPNI	kA
<g/>
:	:	kIx,
Roezl	Roezl	k1gMnSc1
<g/>
,	,	kIx,
Benedikt	Benedikt	k1gMnSc1
(	(	kIx(
<g/>
1824	#num#	k4
<g/>
-	-	kIx~
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hrady	hrad	k1gInPc1
CZ	CZ	kA
<g/>
:	:	kIx,
pomník	pomník	k1gInSc1
Benedikta	Benedikt	k1gMnSc2
Roezla	Roezla	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
