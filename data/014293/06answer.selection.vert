<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
mokřadní	mokřadní	k2eAgInPc1d1
ekosystémy	ekosystém	k1gInPc1
podél	podél	k7c2
pramenného	pramenný	k2eAgInSc2d1
úseku	úsek	k1gInSc2
Růžového	růžový	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgInPc1d1
druhy	druh	k1gInPc1
živočichů	živočich	k1gMnPc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>