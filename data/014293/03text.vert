<s>
Růžová	růžový	k2eAgFnSc1d1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
rezervaceRůžováIUCN	rezervaceRůžováIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2002	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
672	#num#	k4
-	-	kIx~
736	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
24,81	24,81	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Bruntál	Bruntál	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Rýmařov	Rýmařov	k1gInSc1
(	(	kIx(
<g/>
k.ú.	k.ú.	k?
Janušov	Janušov	k1gInSc1
<g/>
)	)	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
57,2	57,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
27,4	27,4	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
2245	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
ev.	ev.	kA
č.	č.	kA
2245	#num#	k4
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Rýmařov	Rýmařov	k1gInSc1
v	v	k7c6
okrese	okres	k1gInSc6
Bruntál	Bruntál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
spravuje	spravovat	k5eAaImIp3nS
AOPK	AOPK	kA
ČR	ČR	kA
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jeseníky	Jeseník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
mokřadní	mokřadní	k2eAgInPc1d1
ekosystémy	ekosystém	k1gInPc1
podél	podél	k7c2
pramenného	pramenný	k2eAgInSc2d1
úseku	úsek	k1gInSc2
Růžového	růžový	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgInPc1d1
druhy	druh	k1gInPc1
živočichů	živočich	k1gMnPc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Bruntál	Bruntál	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Bruntál	Bruntál	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Jeseníky	Jeseník	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
Pavlovický	pavlovický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Sovinecko	Sovinecko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Praděd	praděd	k1gMnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Skřítek	skřítek	k1gMnSc1
•	•	k?
Rešovské	Rešovský	k2eAgInPc4d1
vodopády	vodopád	k1gInPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Javorový	javorový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Ptačí	ptačit	k5eAaImIp3nS
hora	hora	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Roudný	Roudný	k2eAgInSc1d1
Přírodní	přírodní	k2eAgInSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Břidličná	břidličný	k2eAgFnSc1d1
•	•	k?
Džungle	džungle	k1gFnSc1
•	•	k?
Franz-Franz	Franz-Franz	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Karlovice	Karlovice	k1gFnPc1
-	-	kIx~
sever	sever	k1gInSc1
•	•	k?
Krasovský	Krasovský	k2eAgInSc1d1
kotel	kotel	k1gInSc1
•	•	k?
Kunov	Kunov	k1gInSc1
•	•	k?
Mokřiny	mokřina	k1gFnSc2
u	u	k7c2
Krahulčí	krahulčí	k2eAgFnSc2d1
•	•	k?
Niva	niva	k1gFnSc1
Moravice	Moravice	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
studánkou	studánka	k1gFnSc7
•	•	k?
Pstruží	pstruží	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Pustá	pustý	k2eAgFnSc1d1
Rudná	rudný	k2eAgFnSc1d1
•	•	k?
Radim	Radim	k1gMnSc1
•	•	k?
Růžová	růžový	k2eAgFnSc1d1
•	•	k?
Skalní	skalní	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Skalské	Skalská	k1gFnSc2
rašeliniště	rašeliniště	k1gNnSc2
•	•	k?
Suchý	suchý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
U	u	k7c2
Slatinného	slatinný	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Pavlovický	pavlovický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Lávový	lávový	k2eAgInSc1d1
proud	proud	k1gInSc1
u	u	k7c2
Meziny	Mezina	k1gFnSc2
•	•	k?
Liptaňský	Liptaňský	k2eAgInSc1d1
bludný	bludný	k2eAgInSc1d1
balvan	balvan	k1gInSc1
•	•	k?
Morgenland	Morgenland	k1gInSc1
•	•	k?
Oblík	oblík	k1gInSc1
u	u	k7c2
Dívčího	dívčí	k2eAgInSc2d1
hradu	hrad	k1gInSc2
•	•	k?
Osoblažský	osoblažský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
•	•	k?
Razovské	Razovský	k2eAgInPc4d1
tufity	tufit	k1gInPc4
•	•	k?
Staré	Stará	k1gFnSc2
hliniště	hliniště	k1gNnSc2
•	•	k?
Štola	štola	k1gFnSc1
pod	pod	k7c7
Jelení	jelení	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Uhlířský	uhlířský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
