<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
římské	římský	k2eAgFnSc6d1	římská
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
planetu	planeta	k1gFnSc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
ženě	žena	k1gFnSc6	žena
<g/>
.	.	kIx.	.
</s>

