<s>
Hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
(	(	kIx(	(
<g/>
Sagittarius	Sagittarius	k1gMnSc1	Sagittarius
serpentarius	serpentarius	k1gMnSc1	serpentarius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
žijící	žijící	k2eAgMnSc1d1	žijící
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
125	[number]	k4	125
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
asi	asi	k9	asi
130	[number]	k4	130
cm	cm	kA	cm
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
okolo	okolo	k6eAd1	okolo
2	[number]	k4	2
m	m	kA	m
(	(	kIx(	(
<g/>
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
Hmotnost	hmotnost	k1gFnSc1	hmotnost
2,3	[number]	k4	2,3
<g/>
–	–	k?	–
<g/>
4,3	[number]	k4	4,3
kg	kg	kA	kg
Zbarvení	zbarvení	k1gNnSc4	zbarvení
těla	tělo	k1gNnSc2	tělo
bílo-šedé	bílo-šedý	k2eAgNnSc1d1	bílo-šedé
s	s	k7c7	s
černě	černě	k6eAd1	černě
zbarvenými	zbarvený	k2eAgNnPc7d1	zbarvené
místy	místo	k1gNnPc7	místo
(	(	kIx(	(
<g/>
nohy	noh	k1gMnPc7	noh
<g/>
,	,	kIx,	,
chocholka	chocholka	k1gFnSc1	chocholka
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
<g/>
)	)	kIx)	)
Hadilov	hadilov	k1gMnSc1	hadilov
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
tenké	tenký	k2eAgFnPc4d1	tenká
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
s	s	k7c7	s
pařáty	pařát	k1gInPc7	pařát
uzpůsobenými	uzpůsobený	k2eAgInPc7d1	uzpůsobený
lovu	lov	k1gInSc3	lov
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
neohebné	ohebný	k2eNgInPc1d1	neohebný
<g/>
,	,	kIx,	,
drápy	dráp	k1gInPc1	dráp
tupé	tupý	k2eAgFnSc2d1	tupá
a	a	k8xC	a
krátké	krátký	k2eAgFnSc2d1	krátká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
týle	týl	k1gInSc6	týl
má	mít	k5eAaImIp3nS	mít
volnou	volný	k2eAgFnSc4d1	volná
chocholku	chocholka	k1gFnSc4	chocholka
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
při	při	k7c6	při
rozrušení	rozrušení	k1gNnSc6	rozrušení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
centrální	centrální	k2eAgNnPc4d1	centrální
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
při	při	k7c6	při
letu	let	k1gInSc6	let
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
tvářích	tvář	k1gFnPc6	tvář
červené	červený	k2eAgFnSc2d1	červená
skvrny	skvrna	k1gFnSc2	skvrna
bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
žluto-oranžové	žlutoranž	k1gMnPc1	žluto-oranž
Sexuální	sexuální	k2eAgInPc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
rozvinut	rozvinout	k5eAaPmNgInS	rozvinout
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
delší	dlouhý	k2eAgNnPc4d2	delší
ocasní	ocasní	k2eAgNnPc4d1	ocasní
pera	pero	k1gNnPc4	pero
i	i	k8xC	i
chocholku	chocholka	k1gFnSc4	chocholka
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
hadilov	hadilov	k1gMnSc1	hadilov
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
obratně	obratně	k6eAd1	obratně
lovit	lovit	k5eAaImF	lovit
hady	had	k1gMnPc4	had
(	(	kIx(	(
<g/>
i	i	k9	i
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
schopnost	schopnost	k1gFnSc4	schopnost
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
i	i	k9	i
latinské	latinský	k2eAgFnSc2d1	Latinská
serpentarius	serpentarius	k1gInSc4	serpentarius
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
písař	písař	k1gMnSc1	písař
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vysvětlováno	vysvětlovat	k5eAaImNgNnS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chocholka	chocholka	k1gFnSc1	chocholka
v	v	k7c6	v
klidové	klidový	k2eAgFnSc6d1	klidová
poloze	poloha	k1gFnSc6	poloha
připomíná	připomínat	k5eAaImIp3nS	připomínat
písaře	písař	k1gMnSc4	písař
s	s	k7c7	s
perem	pero	k1gNnSc7	pero
za	za	k7c7	za
uchem	ucho	k1gNnSc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
zvou	zvát	k5eAaImIp3nP	zvát
hadilova	hadilov	k1gMnSc4	hadilov
sekretářem	sekretář	k1gInSc7	sekretář
(	(	kIx(	(
<g/>
secretary	secretara	k1gFnSc2	secretara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
Sagittarius	Sagittarius	k1gInSc1	Sagittarius
<g/>
)	)	kIx)	)
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
též	též	k9	též
na	na	k7c4	na
hlavová	hlavový	k2eAgNnPc4d1	hlavové
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
lukostřelec	lukostřelec	k1gMnSc1	lukostřelec
(	(	kIx(	(
<g/>
pera	pero	k1gNnSc2	pero
připomínají	připomínat	k5eAaImIp3nP	připomínat
šípy	šíp	k1gInPc4	šíp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadilov	hadilov	k1gMnSc1	hadilov
žije	žít	k5eAaImIp3nS	žít
většinou	většinou	k6eAd1	většinou
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
páru	pár	k1gInSc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
společně	společně	k6eAd1	společně
loví	lovit	k5eAaImIp3nP	lovit
a	a	k8xC	a
společně	společně	k6eAd1	společně
také	také	k9	také
stavějí	stavět	k5eAaImIp3nP	stavět
velké	velký	k2eAgNnSc4d1	velké
ploché	plochý	k2eAgNnSc4d1	ploché
hnízdo	hnízdo	k1gNnSc4	hnízdo
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
akácie	akácie	k1gFnPc1	akácie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
pak	pak	k6eAd1	pak
používají	používat	k5eAaImIp3nP	používat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
specializaci	specializace	k1gFnSc3	specializace
na	na	k7c4	na
lov	lov	k1gInSc4	lov
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
jinak	jinak	k6eAd1	jinak
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
pařáty	pařát	k1gInPc1	pařát
a	a	k8xC	a
do	do	k7c2	do
korun	koruna	k1gFnPc2	koruna
stromů	strom	k1gInPc2	strom
musí	muset	k5eAaImIp3nS	muset
všechen	všechen	k3xTgInSc1	všechen
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
přemístit	přemístit	k5eAaPmF	přemístit
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
pár	pár	k1gInSc1	pár
si	se	k3xPyFc3	se
brání	bránit	k5eAaImIp3nS	bránit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
teritorium	teritorium	k1gNnSc4	teritorium
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
až	až	k9	až
45	[number]	k4	45
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
malými	malý	k2eAgMnPc7d1	malý
obratlovci	obratlovec	k1gMnPc7	obratlovec
(	(	kIx(	(
<g/>
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
ještěrkami	ještěrka	k1gFnPc7	ještěrka
<g/>
,	,	kIx,	,
hady	had	k1gMnPc7	had
<g/>
)	)	kIx)	)
a	a	k8xC	a
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
ptačími	ptačí	k2eAgNnPc7d1	ptačí
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
živou	živý	k2eAgFnSc4d1	živá
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
kořist	kořist	k1gFnSc4	kořist
požírá	požírat	k5eAaImIp3nS	požírat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
velkých	velký	k2eAgInPc2d1	velký
požárů	požár	k1gInPc2	požár
(	(	kIx(	(
<g/>
takže	takže	k8xS	takže
vlastně	vlastně	k9	vlastně
upečenou	upečený	k2eAgFnSc7d1	upečená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
loví	lovit	k5eAaImIp3nS	lovit
pomocí	pomocí	k7c2	pomocí
zobáku	zobák	k1gInSc2	zobák
nebo	nebo	k8xC	nebo
spárů	spár	k1gInPc2	spár
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
extrémně	extrémně	k6eAd1	extrémně
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pták	pták	k1gMnSc1	pták
cupitá	cupitat	k5eAaImIp3nS	cupitat
drobným	drobná	k1gFnPc3	drobná
krůčky	krůček	k1gInPc4	krůček
a	a	k8xC	a
plaší	plašit	k5eAaImIp3nP	plašit
lovenou	lovený	k2eAgFnSc4d1	lovená
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
opakovaně	opakovaně	k6eAd1	opakovaně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
pařáty	pařát	k1gInPc7	pařát
a	a	k8xC	a
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
kořist	kořist	k1gFnSc4	kořist
vyhlédnutou	vyhlédnutý	k2eAgFnSc4d1	vyhlédnutá
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
tiše	tiš	k1gFnPc1	tiš
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
jako	jako	k8xC	jako
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
na	na	k7c4	na
lov	lov	k1gInSc4	lov
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
denně	denně	k6eAd1	denně
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
až	až	k9	až
dvacet	dvacet	k4xCc4	dvacet
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
před	před	k7c7	před
setměním	setmění	k1gNnSc7	setmění
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
vracejí	vracet	k5eAaImIp3nP	vracet
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
hnízdu	hnízdo	k1gNnSc3	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
světle	světle	k6eAd1	světle
modrozelená	modrozelený	k2eAgNnPc1d1	modrozelené
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
vejce	vejce	k1gNnPc4	vejce
v	v	k7c6	v
odstupu	odstup	k1gInSc6	odstup
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snůšce	snůška	k1gFnSc6	snůška
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
asi	asi	k9	asi
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
světle	světle	k6eAd1	světle
šedá	šedý	k2eAgNnPc1d1	šedé
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Krmí	krmit	k5eAaImIp3nS	krmit
je	on	k3xPp3gMnPc4	on
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
natrávenou	natrávený	k2eAgFnSc7d1	natrávená
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
celými	celý	k2eAgInPc7d1	celý
mrtvými	mrtvý	k2eAgInPc7d1	mrtvý
kusy	kus	k1gInPc7	kus
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
jim	on	k3xPp3gMnPc3	on
nosí	nosit	k5eAaImIp3nS	nosit
živé	živý	k2eAgMnPc4d1	živý
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
mláďata	mládě	k1gNnPc4	mládě
sama	sám	k3xTgFnSc1	sám
ulovit	ulovit	k5eAaPmF	ulovit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
starají	starat	k5eAaImIp3nP	starat
až	až	k9	až
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
péči	péče	k1gFnSc4	péče
v	v	k7c6	v
početnějších	početní	k2eAgInPc6d2	početnější
vrzích	vrh	k1gInPc6	vrh
třetí	třetí	k4xOgNnSc4	třetí
mládě	mládě	k1gNnSc4	mládě
často	často	k6eAd1	často
nevychovají	vychovat	k5eNaPmIp3nP	vychovat
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
hadilovové	hadilov	k1gMnPc1	hadilov
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
velmi	velmi	k6eAd1	velmi
nenasytní	nasytný	k2eNgMnPc1d1	nenasytný
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
nestíhají	stíhat	k5eNaImIp3nP	stíhat
uživit	uživit	k5eAaPmF	uživit
<g/>
.	.	kIx.	.
</s>
<s>
Hadilov	hadilov	k1gMnSc1	hadilov
umí	umět	k5eAaImIp3nS	umět
létat	létat	k5eAaImF	létat
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pěkně	pěkně	k6eAd1	pěkně
vysoko	vysoko	k6eAd1	vysoko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nečiní	činit	k5eNaImIp3nS	činit
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgMnSc7d1	dobrý
běžcem	běžec	k1gMnSc7	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgMnSc7d1	dobrý
běžcem	běžec	k1gMnSc7	běžec
<g/>
,	,	kIx,	,
běhá	běhat	k5eAaImIp3nS	běhat
dokonce	dokonce	k9	dokonce
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
obletuje	obletovat	k5eAaImIp3nS	obletovat
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
vznese	vznést	k5eAaPmIp3nS	vznést
a	a	k8xC	a
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
příležitostí	příležitost	k1gFnSc7	příležitost
je	být	k5eAaImIp3nS	být
útok	útok	k1gInSc1	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
velké	velký	k2eAgFnSc2d1	velká
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Hadilov	hadilov	k1gMnSc1	hadilov
se	se	k3xPyFc4	se
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
a	a	k8xC	a
vznese	vznést	k5eAaPmIp3nS	vznést
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
rozbíhá	rozbíhat	k5eAaImIp3nS	rozbíhat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
<g/>
,	,	kIx,	,
za	za	k7c2	za
bezvětří	bezvětří	k1gNnSc2	bezvětří
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
delší	dlouhý	k2eAgInSc4d2	delší
rozběh	rozběh	k1gInSc4	rozběh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
usedá	usedat	k5eAaImIp3nS	usedat
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
ohrožení	ohrožení	k1gNnSc2	ohrožení
opět	opět	k6eAd1	opět
snáší	snášet	k5eAaImIp3nS	snášet
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
specifický	specifický	k2eAgInSc1d1	specifický
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
větve	větev	k1gFnSc2	větev
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
v	v	k7c6	v
oligocénu	oligocén	k1gInSc6	oligocén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhu	druh	k1gInSc2	druh
ho	on	k3xPp3gInSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
ztráta	ztráta	k1gFnSc1	ztráta
obývaných	obývaný	k2eAgFnPc2d1	obývaná
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
aktivity	aktivita	k1gFnSc2	aktivita
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
má	mít	k5eAaImIp3nS	mít
málo	málo	k6eAd1	málo
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gInPc7	on
hlavně	hlavně	k6eAd1	hlavně
velké	velká	k1gFnPc1	velká
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
a	a	k8xC	a
dravci	dravec	k1gMnPc7	dravec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
hojnost	hojnost	k1gFnSc4	hojnost
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgMnSc1d1	chráněn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Africkou	africký	k2eAgFnSc4d1	africká
konvencí	konvence	k1gFnSc7	konvence
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Hadilov	hadilov	k1gMnSc1	hadilov
je	být	k5eAaImIp3nS	být
endemit	endemit	k1gInSc4	endemit
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
savanách	savana	k1gFnPc6	savana
a	a	k8xC	a
polopouštích	polopoušť	k1gFnPc6	polopoušť
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
až	až	k9	až
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
stále	stále	k6eAd1	stále
běžný	běžný	k2eAgInSc4d1	běžný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
chovaný	chovaný	k2eAgMnSc1d1	chovaný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hadilov	hadilov	k1gMnSc1	hadilov
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
lovit	lovit	k5eAaImF	lovit
hady	had	k1gMnPc4	had
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
občas	občas	k6eAd1	občas
ochočován	ochočovat	k5eAaImNgMnS	ochočovat
a	a	k8xC	a
zemědělci	zemědělec	k1gMnPc1	zemědělec
cvičen	cvičen	k2eAgMnSc1d1	cvičen
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
symbolem	symbol	k1gInSc7	symbol
Súdánu	Súdán	k1gInSc2	Súdán
(	(	kIx(	(
<g/>
hadilov	hadilov	k1gMnSc1	hadilov
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
pečeti	pečeť	k1gFnSc6	pečeť
i	i	k8xC	i
na	na	k7c6	na
vojenských	vojenský	k2eAgInPc6d1	vojenský
odznacích	odznak	k1gInPc6	odznak
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc2	galerie
Hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hadilov	hadilov	k1gMnSc1	hadilov
písař	písař	k1gMnSc1	písař
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Sagittarius_serpentarius	Sagittarius_serpentarius	k1gInSc1	Sagittarius_serpentarius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Hadilov	hadilov	k1gMnSc1	hadilov
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
