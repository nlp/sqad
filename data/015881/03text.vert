<s>
Troja	Troja	k1gFnSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Troja	Troj	k2eAgFnSc1d1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
40	#num#	k4
km	km	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Troja	Troja	k1gFnSc1
je	být	k5eAaImIp3nS
nevelká	velký	k2eNgFnSc1d1
řeka	řeka	k1gFnSc1
ve	v	k7c6
vojvodstvích	vojvodství	k1gNnPc6
opolském	opolský	k2eAgInSc6d1
i	i	k8xC
slezském	slezský	k2eAgInSc6d1
na	na	k7c6
jihu	jih	k1gInSc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
pravý	pravý	k2eAgInSc1d1
nejdelší	dlouhý	k2eAgInSc1d3
přítok	přítok	k1gInSc1
Psiny	psina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Troje	troje	k4xRgFnPc4
leží	ležet	k5eAaImIp3nP
město	město	k1gNnSc4
Ketř	Ketř	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Řeka	řeka	k1gFnSc1
pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
Powiatu	Powiat	k1gMnSc6
głubczyckém	głubczycký	k1gMnSc6
v	v	k7c6
Přírodním	přírodní	k2eAgInSc6d1
parku	park	k1gInSc6
Rajón	rajón	k1gInSc1
Mokré	Mokrá	k1gFnSc2
–	–	k?
Levice	levice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
protéká	protékat	k5eAaImIp3nS
řadou	řada	k1gFnSc7
vesnic	vesnice	k1gFnPc2
Hlubčického	Hlubčický	k2eAgInSc2d1
okresu	okres	k1gInSc2
a	a	k8xC
částí	část	k1gFnPc2
Slezského	slezský	k2eAgNnSc2d1
vojvodství	vojvodství	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
svoje	svůj	k3xOyFgNnPc4
ústí	ústí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
periodickou	periodický	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
<g/>
,	,	kIx,
napájenou	napájený	k2eAgFnSc4d1
podzemními	podzemní	k2eAgFnPc7d1
vodami	voda	k1gFnPc7
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
hladina	hladina	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
zvedá	zvedat	k5eAaImIp3nS
v	v	k7c6
důsledku	důsledek	k1gInSc6
srážek	srážka	k1gFnPc2
a	a	k8xC
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramen	pramen	k1gInSc1
Troji	troje	k4xRgMnPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
u	u	k7c2
Petrovic	Petrovice	k1gFnPc2
poblíž	poblíž	k7c2
Krnova	Krnov	k1gInSc2
<g/>
,	,	kIx,
řeka	řeka	k1gFnSc1
sama	sám	k3xTgMnSc4
nemá	mít	k5eNaImIp3nS
významných	významný	k2eAgInPc2d1
přítoků	přítok	k1gInPc2
<g/>
;	;	kIx,
jde	jít	k5eAaImIp3nS
většinou	většina	k1gFnSc7
o	o	k7c4
větší	veliký	k2eAgInPc4d2
potoky	potok	k1gInPc4
a	a	k8xC
strouhy	strouha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
bylo	být	k5eAaImAgNnS
provedeno	proveden	k2eAgNnSc1d1
prohloubení	prohloubení	k1gNnSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
koryta	koryto	k1gNnSc2
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
při	při	k7c6
tisícileté	tisíciletý	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
řeka	řeka	k1gFnSc1
nevystoupila	vystoupit	k5eNaPmAgFnS
z	z	k7c2
břehů	břeh	k1gInPc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
dříve	dříve	k6eAd2
opakovaně	opakovaně	k6eAd1
zaplavovala	zaplavovat	k5eAaImAgFnS
část	část	k1gFnSc1
města	město	k1gNnSc2
Ketř	Ketř	k1gFnSc1
(	(	kIx(
<g/>
naposledy	naposledy	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1995	#num#	k4
–	–	k?
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
vsi	ves	k1gFnSc2
Włodzienin	Włodzienin	k1gFnSc1
(	(	kIx(
<g/>
Vladěnín	Vladěnín	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
vybudována	vybudován	k2eAgFnSc1d1
retenční	retenční	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Włodzienin	Włodzienina	k1gFnPc2
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
zbiornik	zbiornik	k1gInSc1
retencyjny	retencyjna	k1gFnSc2
Włodzienin	Włodzienina	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
při	při	k7c6
velkých	velký	k2eAgFnPc6d1
dešťových	dešťový	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
nebo	nebo	k8xC
náhlých	náhlý	k2eAgFnPc6d1
záplavách	záplava	k1gFnPc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
prudkému	prudký	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
množství	množství	k1gNnSc2
vody	voda	k1gFnSc2
v	v	k7c6
korytě	koryto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nádrž	nádrž	k1gFnSc1
bude	být	k5eAaImBp3nS
chránit	chránit	k5eAaImF
před	před	k7c7
povodněmi	povodeň	k1gFnPc7
území	území	k1gNnSc2
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
Troji	troje	k4xRgMnPc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
bezpečí	bezpečí	k1gNnSc4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
a	a	k8xC
zamezit	zamezit	k5eAaPmF
přílivu	příliv	k1gInSc3
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
vod	voda	k1gFnPc2
do	do	k7c2
Odry	Odra	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
vysokého	vysoký	k2eAgInSc2d1
stavu	stav	k1gInSc2
vody	voda	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
nádrži	nádrž	k1gFnSc6
se	se	k3xPyFc4
dnes	dnes	k6eAd1
chovají	chovat	k5eAaImIp3nP
ryby	ryba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezanedbatelné	zanedbatelný	k2eNgFnPc4d1
bude	být	k5eAaImBp3nS
i	i	k9
využití	využití	k1gNnSc1
turistické	turistický	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
koupání	koupání	k1gNnSc4
zakázáno	zakázán	k2eAgNnSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
okolní	okolní	k2eAgFnPc1d1
vsi	ves	k1gFnPc1
nemají	mít	k5eNaImIp3nP
vybudované	vybudovaný	k2eAgFnPc1d1
kanalizace	kanalizace	k1gFnPc1
s	s	k7c7
čističkami	čistička	k1gFnPc7
a	a	k8xC
splašky	splašky	k1gInPc7
tečou	téct	k5eAaImIp3nP
přímo	přímo	k6eAd1
do	do	k7c2
retenční	retenční	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
pravé	pravá	k1gFnPc4
–	–	k?
Wierzbiec	Wierzbiec	k1gMnSc1
<g/>
,	,	kIx,
Glinik	Glinik	k1gMnSc1
<g/>
,	,	kIx,
Kałuża	Kałuża	k1gMnSc1
(	(	kIx(
<g/>
Dopływ	Dopływ	k1gMnSc1
z	z	k7c2
Posucic	Posucice	k1gFnPc2
<g/>
,	,	kIx,
něm.	něm.	k?
Kaluscha	Kaluscha	k1gFnSc1
<g/>
)	)	kIx)
+	+	kIx~
Potok	potok	k1gInSc1
Jędrychowicki	Jędrychowicki	k1gNnSc2
<g/>
,	,	kIx,
Morawka	Morawko	k1gNnSc2
<g/>
,	,	kIx,
Potok	potok	k1gInSc4
Rozumicki	Rozumick	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Řeka	řeka	k1gFnSc1
Troja	Troj	k1gInSc2
v	v	k7c6
Ketři	Ketř	k1gFnSc6
po	po	k7c6
intenzivních	intenzivní	k2eAgFnPc6d1
dešťových	dešťový	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
v	v	k7c6
září	září	k1gNnSc6
2007	#num#	k4
</s>
<s>
Troja	Troja	k6eAd1
v	v	k7c6
Ketři	Ketř	k1gInSc6
</s>
<s>
Troja	Troja	k6eAd1
v	v	k7c6
Ketři	Ketř	k1gInSc6
2	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Troja	Troj	k1gInSc2
(	(	kIx(
<g/>
rzeka	rzeka	k6eAd1
<g/>
)	)	kIx)
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zbiornik	Zbiornik	k1gInSc1
retencyjny	retencyjna	k1gFnSc2
Włodzienin	Włodzienina	k1gFnPc2
<g/>
,	,	kIx,
URL	URL	kA
<g/>
:	:	kIx,
http://www.wlodzienin.cba.pl/galerie/zbw/index.php	http://www.wlodzienin.cba.pl/galerie/zbw/index.php	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Poláci	Polák	k1gMnPc1
zfušovanou	zfušovaný	k2eAgFnSc4d1
přehradu	přehrada	k1gFnSc4
vypustili	vypustit	k5eAaPmAgMnP
</s>
<s>
Ocena	Ocena	k6eAd1
stanu	stanout	k5eAaPmIp1nS
jakości	jakośce	k1gFnSc4
wód	wód	k?
powierzchniowych	powierzchniowycha	k1gFnPc2
w	w	k?
zlewni	zlewn	k1gMnPc1
rzeky	rzeka	k1gFnSc2
Troi	Tro	k1gFnSc2
w	w	k?
2007	#num#	k4
r.	r.	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
</s>
