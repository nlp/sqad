<s>
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Baden	Baden	k1gInSc1	Baden
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Burg	Burg	k1gMnSc1	Burg
im	im	k?	im
Leimental	Leimental	k1gMnSc1	Leimental
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vědec	vědec	k1gMnSc1	vědec
známý	známý	k1gMnSc1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
LSD	LSD	kA	LSD
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c6	na
Universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgInS	zajímat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
chemii	chemie	k1gFnSc4	chemie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
prováděl	provádět	k5eAaImAgInS	provádět
výzkum	výzkum	k1gInSc1	výzkum
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
běžné	běžný	k2eAgFnSc2d1	běžná
živočišné	živočišný	k2eAgFnSc2d1	živočišná
látky	látka	k1gFnSc2	látka
chitinu	chitin	k1gInSc2	chitin
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
dostal	dostat	k5eAaPmAgInS	dostat
doktorát	doktorát	k1gInSc1	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Hofmann	Hofmann	k1gInSc1	Hofmann
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
farmaceutickochemickému	farmaceutickochemický	k2eAgNnSc3d1	farmaceutickochemický
oddělení	oddělení	k1gNnSc3	oddělení
laboratoří	laboratoř	k1gFnPc2	laboratoř
Sandoz	Sandoz	k1gInSc1	Sandoz
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Novartis	Novartis	k1gFnSc1	Novartis
<g/>
)	)	kIx)	)
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
léčivou	léčivý	k2eAgFnSc4d1	léčivá
bylinu	bylina	k1gFnSc4	bylina
ladoňku	ladoňka	k1gFnSc4	ladoňka
a	a	k8xC	a
houbu	houba	k1gFnSc4	houba
paličkovici	paličkovice	k1gFnSc4	paličkovice
nachovou	nachový	k2eAgFnSc4d1	nachová
(	(	kIx(	(
<g/>
námel	námel	k1gInSc1	námel
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
rafinovat	rafinovat	k5eAaImF	rafinovat
a	a	k8xC	a
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
jejich	jejich	k3xOp3gFnPc4	jejich
aktivní	aktivní	k2eAgFnPc4d1	aktivní
složky	složka	k1gFnPc4	složka
k	k	k7c3	k
farmaceutickým	farmaceutický	k2eAgInPc3d1	farmaceutický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
bádání	bádání	k1gNnSc4	bádání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kyseliny	kyselina	k1gFnSc2	kyselina
lysergové	lysergový	k2eAgFnSc2d1	lysergová
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc2d1	centrální
součásti	součást	k1gFnSc2	součást
námelových	námelový	k2eAgInPc2d1	námelový
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
LSD-	LSD-	k1gFnSc1	LSD-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
při	při	k7c6	při
opakování	opakování	k1gNnSc6	opakování
syntézy	syntéza	k1gFnSc2	syntéza
téměř	téměř	k6eAd1	téměř
zapomenuté	zapomenutý	k2eAgFnSc2d1	zapomenutá
látky	látka	k1gFnSc2	látka
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hofmann	Hofmann	k1gMnSc1	Hofmann
objevil	objevit	k5eAaPmAgMnS	objevit
psychedelické	psychedelický	k2eAgInPc4d1	psychedelický
účinky	účinek	k1gInPc4	účinek
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
nedopatřením	nedopatření	k1gNnSc7	nedopatření
potřísnil	potřísnit	k5eAaPmAgInS	potřísnit
kůži	kůže	k1gFnSc4	kůže
na	na	k7c6	na
zápěstí	zápěstí	k1gNnSc6	zápěstí
roztokem	roztok	k1gInSc7	roztok
LSD-	LSD-	k1gFnSc7	LSD-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Hofmann	Hofmann	k1gNnSc4	Hofmann
záměrně	záměrně	k6eAd1	záměrně
požil	požít	k5eAaPmAgInS	požít
250	[number]	k4	250
mikrogramů	mikrogram	k1gInPc2	mikrogram
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
dávku	dávka	k1gFnSc4	dávka
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yQgFnSc2	který
očekával	očekávat	k5eAaImAgInS	očekávat
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgInSc1d2	nižší
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yIgMnSc1	jaký
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
dostavil	dostavit	k5eAaPmAgMnS	dostavit
(	(	kIx(	(
<g/>
pověstný	pověstný	k2eAgInSc4d1	pověstný
"	"	kIx"	"
<g/>
Cyklistický	cyklistický	k2eAgInSc4d1	cyklistický
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
-	-	kIx~	-
cesta	cesta	k1gFnSc1	cesta
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
z	z	k7c2	z
laboratoře	laboratoř	k1gFnSc2	laboratoř
domů	dům	k1gInPc2	dům
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
nechtěně	chtěně	k6eNd1	chtěně
několikanásobně	několikanásobně	k6eAd1	několikanásobně
předávkovaného	předávkovaný	k2eAgMnSc2d1	předávkovaný
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
své	svůj	k3xOyFgFnSc2	svůj
asistentky	asistentka	k1gFnSc2	asistentka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
rovněž	rovněž	k9	rovněž
kolegové	kolega	k1gMnPc1	kolega
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hofmanna	Hofmann	k1gMnSc2	Hofmann
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
pak	pak	k6eAd1	pak
napsal	napsat	k5eAaBmAgMnS	napsat
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
experimentech	experiment	k1gInPc6	experiment
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ředitelem	ředitel	k1gMnSc7	ředitel
oddělení	oddělení	k1gNnSc2	oddělení
přírodních	přírodní	k2eAgInPc2d1	přírodní
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
firmy	firma	k1gFnSc2	firma
Sandoz	Sandoz	k1gInSc1	Sandoz
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
studovat	studovat	k5eAaImF	studovat
halucinogenní	halucinogenní	k2eAgFnSc2d1	halucinogenní
látky	látka	k1gFnSc2	látka
objevené	objevený	k2eAgFnSc2d1	objevená
v	v	k7c6	v
mexických	mexický	k2eAgFnPc6d1	mexická
houbách	houba	k1gFnPc6	houba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
rostlinách	rostlina	k1gFnPc6	rostlina
užívaných	užívaný	k2eAgFnPc6d1	užívaná
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
psilocybinu	psilocybina	k1gFnSc4	psilocybina
-	-	kIx~	-
aktivního	aktivní	k2eAgNnSc2d1	aktivní
činidla	činidlo	k1gNnSc2	činidlo
mnoha	mnoho	k4c3	mnoho
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
magických	magický	k2eAgFnPc2d1	magická
hub	houba	k1gFnPc2	houba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hofmann	Hofmann	k1gMnSc1	Hofmann
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
semínka	semínko	k1gNnPc4	semínko
mexických	mexický	k2eAgFnPc2d1	mexická
rostlin	rostlina	k1gFnPc2	rostlina
druhu	druh	k1gInSc2	druh
Rivea	Rive	k2eAgFnSc1d1	Rivea
corymbosa	corymbosa	k1gFnSc1	corymbosa
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
domorodci	domorodec	k1gMnPc1	domorodec
nazývali	nazývat	k5eAaImAgMnP	nazývat
Ololiuhqui	Ololiuhque	k1gFnSc4	Ololiuhque
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
našel	najít	k5eAaPmAgInS	najít
aktivní	aktivní	k2eAgFnSc4d1	aktivní
sloučeninu	sloučenina	k1gFnSc4	sloučenina
ergin	ergina	k1gFnPc2	ergina
(	(	kIx(	(
<g/>
amid	amid	k1gInSc1	amid
kyseliny	kyselina	k1gFnSc2	kyselina
lysergové	lysergový	k2eAgFnSc2d1	lysergová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úzce	úzko	k6eAd1	úzko
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
odjel	odjet	k5eAaPmAgInS	odjet
společně	společně	k6eAd1	společně
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
Anitou	Anita	k1gFnSc7	Anita
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Gordonem	Gordon	k1gMnSc7	Gordon
Wassonem	Wasson	k1gMnSc7	Wasson
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Mexika	Mexiko	k1gNnSc2	Mexiko
hledat	hledat	k5eAaImF	hledat
rostlinu	rostlina	k1gFnSc4	rostlina
"	"	kIx"	"
<g/>
Ska	Ska	k1gFnSc4	Ska
Maria	Mario	k1gMnSc2	Mario
Pastora	pastor	k1gMnSc2	pastor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
Salvia	Salvia	k1gFnSc1	Salvia
divinorum	divinorum	k1gInSc1	divinorum
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
rovněž	rovněž	k9	rovněž
vzorky	vzorek	k1gInPc4	vzorek
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
identifikovat	identifikovat	k5eAaBmF	identifikovat
jejich	jejich	k3xOp3gFnPc4	jejich
aktivní	aktivní	k2eAgFnPc4d1	aktivní
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Nazýval	nazývat	k5eAaImAgMnS	nazývat
LSD	LSD	kA	LSD
"	"	kIx"	"
<g/>
lékem	lék	k1gInSc7	lék
pro	pro	k7c4	pro
duši	duše	k1gFnSc4	duše
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znechucený	znechucený	k2eAgInSc1d1	znechucený
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
prohibicí	prohibice	k1gFnSc7	prohibice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jeho	jeho	k3xOp3gFnSc1	jeho
objev	objev	k1gInSc4	objev
zašlapala	zašlapat	k5eAaPmAgFnS	zašlapat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
LSD	LSD	kA	LSD
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
velmi	velmi	k6eAd1	velmi
úspěšně	úspěšně	k6eAd1	úspěšně
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
Hippies	Hippiesa	k1gFnPc2	Hippiesa
<g/>
,	,	kIx,	,
neférově	férově	k6eNd1	férově
démonizována	démonizován	k2eAgFnSc1d1	démonizována
vládami	vláda	k1gFnPc7	vláda
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
hnutí	hnutí	k1gNnSc1	hnutí
oponovalo	oponovat	k5eAaImAgNnS	oponovat
<g/>
.	.	kIx.	.
</s>
<s>
Hofmann	Hofmann	k1gMnSc1	Hofmann
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
LSD	LSD	kA	LSD
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
v	v	k7c6	v
nesprávných	správný	k2eNgFnPc6d1	nesprávná
rukách	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
stovek	stovka	k1gFnPc2	stovka
vědeckých	vědecký	k2eAgInPc2d1	vědecký
článků	článek	k1gInPc2	článek
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
podílel	podílet	k5eAaImAgInS	podílet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
LSD	LSD	kA	LSD
<g/>
:	:	kIx,	:
My	my	k3xPp1nPc1	my
Problem	Probl	k1gMnSc7	Probl
Child	Child	k1gMnSc1	Child
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
LSD	LSD	kA	LSD
<g/>
:	:	kIx,	:
Moje	můj	k3xOp1gNnSc1	můj
problémové	problémový	k2eAgNnSc1d1	problémové
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částečnou	částečný	k2eAgFnSc7d1	částečná
autobiografií	autobiografie	k1gFnSc7	autobiografie
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
Hofmannovu	Hofmannův	k2eAgFnSc4d1	Hofmannova
slavnou	slavný	k2eAgFnSc4d1	slavná
cestu	cesta	k1gFnSc4	cesta
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
jeho	jeho	k3xOp3gFnPc2	jeho
stých	stý	k4xOgFnPc2	stý
narozenin	narozeniny	k1gFnPc2	narozeniny
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
středem	středem	k7c2	středem
pozornosti	pozornost	k1gFnSc2	pozornost
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
sympoziu	sympozion	k1gNnSc6	sympozion
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
jako	jako	k9	jako
připomínka	připomínka	k1gFnSc1	připomínka
objevu	objev	k1gInSc2	objev
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
evoluci	evoluce	k1gFnSc6	evoluce
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
důležité	důležitý	k2eAgNnSc1d1	důležité
objevení	objevení	k1gNnSc1	objevení
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
-	-	kIx~	-
LSD	LSD	kA	LSD
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Hofmann	Hofmann	k1gMnSc1	Hofmann
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
nástroj	nástroj	k1gInSc1	nástroj
k	k	k7c3	k
přeměnění	přeměnění	k1gNnSc3	přeměnění
nás	my	k3xPp1nPc2	my
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
102	[number]	k4	102
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
selhání	selhání	k1gNnSc6	selhání
srdce	srdce	k1gNnSc2	srdce
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
zastával	zastávat	k5eAaImAgMnS	zastávat
zásadu	zásada	k1gFnSc4	zásada
žití	žití	k1gNnSc2	žití
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Albert	Alberta	k1gFnPc2	Alberta
Hofmann	Hofmann	k1gNnSc4	Hofmann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nadace	nadace	k1gFnSc1	nadace
Alberta	Albert	k1gMnSc2	Albert
Hofmanna	Hofmann	k1gMnSc2	Hofmann
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
International	International	k1gFnSc1	International
Symposium	symposium	k1gNnSc1	symposium
on	on	k3xPp3gMnSc1	on
the	the	k?	the
occasion	occasion	k1gInSc1	occasion
of	of	k?	of
the	the	k?	the
100	[number]	k4	100
<g/>
th	th	k?	th
Birthday	Birthdaa	k1gFnSc2	Birthdaa
of	of	k?	of
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
"	"	kIx"	"
<g/>
LSD	LSD	kA	LSD
<g/>
:	:	kIx,	:
Problem	Problo	k1gNnSc7	Problo
Child	Child	k1gMnSc1	Child
and	and	k?	and
Wonder	Wonder	k1gMnSc1	Wonder
Drug	Drug	k1gMnSc1	Drug
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Basel	Basel	k1gMnSc1	Basel
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
13-15	[number]	k4	13-15
January	Januara	k1gFnSc2	Januara
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Internationales	Internationales	k1gMnSc1	Internationales
Symposium	symposium	k1gNnSc1	symposium
zum	zum	k?	zum
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Geburtstag	Geburtstag	k1gInSc1	Geburtstag
von	von	k1gInSc1	von
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
"	"	kIx"	"
<g/>
LSD	LSD	kA	LSD
<g/>
:	:	kIx,	:
Sorgenkind	Sorgenkind	k1gMnSc1	Sorgenkind
und	und	k?	und
Wunderdroge	Wunderdroge	k1gInSc1	Wunderdroge
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Basel	Basel	k1gMnSc1	Basel
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
13-15	[number]	k4	13-15
Januar	Januar	k1gInSc1	Januar
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Albert	Albert	k1gMnSc1	Albert
Hoffman	Hoffman	k1gMnSc1	Hoffman
(	(	kIx(	(
<g/>
NNDB	NNDB	kA	NNDB
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
<g/>
:	:	kIx,	:
LSD	LSD	kA	LSD
-	-	kIx~	-
mé	můj	k3xOp1gNnSc1	můj
problémové	problémový	k2eAgNnSc1d1	problémové
dítě	dítě	k1gNnSc1	dítě
-	-	kIx~	-
plný	plný	k2eAgInSc1d1	plný
online	onlinout	k5eAaPmIp3nS	onlinout
text	text	k1gInSc1	text
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
<g/>
:	:	kIx,	:
LSD	LSD	kA	LSD
-	-	kIx~	-
My	my	k3xPp1nPc1	my
problem	probl	k1gMnSc7	probl
Child	Child	k1gMnSc1	Child
<g/>
.	.	kIx.	.
-	-	kIx~	-
plný	plný	k2eAgInSc1d1	plný
online	onlinout	k5eAaPmIp3nS	onlinout
text	text	k1gInSc1	text
</s>
