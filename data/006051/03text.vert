<s>
Ionizace	ionizace	k1gFnSc1	ionizace
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
z	z	k7c2	z
elektricky	elektricky	k6eAd1	elektricky
neutrálního	neutrální	k2eAgInSc2d1	neutrální
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnSc2	molekula
stává	stávat	k5eAaImIp3nS	stávat
iont	iont	k1gInSc1	iont
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
ionizace	ionizace	k1gFnSc1	ionizace
<g/>
"	"	kIx"	"
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
stav	stav	k1gInSc1	stav
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
dějem	děj	k1gInSc7	děj
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
je	být	k5eAaImIp3nS	být
rekombinace	rekombinace	k1gFnSc1	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
záporných	záporný	k2eAgInPc2d1	záporný
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
aniontů	anion	k1gInPc2	anion
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
způsoben	způsobit	k5eAaPmNgInS	způsobit
dodáním	dodání	k1gNnSc7	dodání
záporného	záporný	k2eAgInSc2d1	záporný
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
-	-	kIx~	-
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
-	-	kIx~	-
do	do	k7c2	do
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
kladných	kladný	k2eAgMnPc2d1	kladný
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
kationtů	kation	k1gInPc2	kation
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
podmíněn	podmínit	k5eAaPmNgInS	podmínit
odtržením	odtržení	k1gNnSc7	odtržení
jednoho	jeden	k4xCgMnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
částici	částice	k1gFnSc4	částice
dodat	dodat	k5eAaPmF	dodat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
-	-	kIx~	-
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
jednoho	jeden	k4xCgInSc2	jeden
elektronu	elektron	k1gInSc2	elektron
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ionizační	ionizační	k2eAgInSc1d1	ionizační
potenciál	potenciál	k1gInSc1	potenciál
(	(	kIx(	(
<g/>
ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
minimální	minimální	k2eAgFnSc1d1	minimální
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
dopadající	dopadající	k2eAgFnPc1d1	dopadající
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ionizaci	ionizace	k1gFnSc3	ionizace
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc3	vytvoření
kationtu	kation	k1gInSc2	kation
<g/>
.	.	kIx.	.
</s>
<s>
Ionizační	ionizační	k2eAgInSc1d1	ionizační
potenciál	potenciál	k1gInSc1	potenciál
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
(	(	kIx(	(
<g/>
1	[number]	k4	1
eV	eV	k?	eV
=	=	kIx~	=
1,6	[number]	k4	1,6
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vodík	vodík	k1gInSc4	vodík
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
13,53	[number]	k4	13,53
eV	eV	k?	eV
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
15,8	[number]	k4	15,8
eV	eV	k?	eV
a	a	k8xC	a
rtuť	rtuť	k1gFnSc1	rtuť
10,4	[number]	k4	10,4
eV.	eV.	k?	eV.
Přidáním	přidání	k1gNnSc7	přidání
elektronu	elektron	k1gInSc2	elektron
k	k	k7c3	k
atomu	atom	k1gInSc3	atom
určitého	určitý	k2eAgInSc2d1	určitý
prvku	prvek	k1gInSc2	prvek
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
jisté	jistý	k2eAgFnSc2d1	jistá
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k8xC	jako
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
afinitu	afinita	k1gFnSc4	afinita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
funguje	fungovat	k5eAaImIp3nS	fungovat
trochu	trochu	k6eAd1	trochu
odlišně	odlišně	k6eAd1	odlišně
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
iont	iont	k1gInSc1	iont
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
s	s	k7c7	s
kladně	kladně	k6eAd1	kladně
nebo	nebo	k8xC	nebo
záporně	záporně	k6eAd1	záporně
nabitým	nabitý	k2eAgInSc7d1	nabitý
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Kladně	kladně	k6eAd1	kladně
nabité	nabitý	k2eAgInPc1d1	nabitý
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
<g/>
,	,	kIx,	,
když	když	k8xS	když
elektrony	elektron	k1gInPc1	elektron
vázané	vázaný	k2eAgInPc1d1	vázaný
na	na	k7c4	na
atom	atom	k1gInSc4	atom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
molekulu	molekula	k1gFnSc4	molekula
<g/>
)	)	kIx)	)
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
z	z	k7c2	z
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgInPc1d1	nabitý
ionty	ion	k1gInPc1	ion
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
volný	volný	k2eAgInSc1d1	volný
elektron	elektron	k1gInSc1	elektron
srazí	srazit	k5eAaPmIp3nS	srazit
s	s	k7c7	s
atomem	atom	k1gInSc7	atom
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
zachytí	zachytit	k5eAaPmIp3nP	zachytit
uvnitř	uvnitř	k7c2	uvnitř
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
ionizace	ionizace	k1gFnPc4	ionizace
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
sekvenční	sekvenční	k2eAgFnPc4d1	sekvenční
ionizace	ionizace	k1gFnPc4	ionizace
a	a	k8xC	a
ne-sekvenční	ekvenční	k2eNgFnPc4d1	-sekvenční
ionizace	ionizace	k1gFnPc4	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
fyzice	fyzika	k1gFnSc6	fyzika
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
jen	jen	k9	jen
sekvenční	sekvenční	k2eAgFnPc4d1	sekvenční
ionizace	ionizace	k1gFnPc4	ionizace
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
Klasická	klasický	k2eAgFnSc1d1	klasická
ionizace	ionizace	k1gFnSc1	ionizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ne-sekvenční	Neekvenční	k2eAgFnSc1d1	Ne-sekvenční
ionizace	ionizace	k1gFnSc1	ionizace
porušuje	porušovat	k5eAaImIp3nS	porušovat
několik	několik	k4yIc4	několik
zákonů	zákon	k1gInPc2	zákon
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
najdete	najít	k5eAaPmIp2nP	najít
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
ionizace	ionizace	k1gFnSc1	ionizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
Bohrův	Bohrův	k2eAgInSc4d1	Bohrův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dělá	dělat	k5eAaImIp3nS	dělat
jak	jak	k6eAd1	jak
atomovou	atomový	k2eAgFnSc4d1	atomová
tak	tak	k8xC	tak
i	i	k9	i
molekulární	molekulární	k2eAgFnSc4d1	molekulární
ionizaci	ionizace	k1gFnSc4	ionizace
zcela	zcela	k6eAd1	zcela
deterministickou	deterministický	k2eAgFnSc7d1	deterministická
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
problém	problém	k1gInSc1	problém
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
určitou	určitý	k2eAgFnSc4d1	určitá
a	a	k8xC	a
vypočitatelnou	vypočitatelný	k2eAgFnSc4d1	vypočitatelná
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
energetický	energetický	k2eAgInSc4d1	energetický
rozdíl	rozdíl	k1gInSc4	rozdíl
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Analogie	analogie	k1gFnSc1	analogie
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
:	:	kIx,	:
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
nemůže	moct	k5eNaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
skočit	skočit	k5eAaPmF	skočit
přes	přes	k7c4	přes
zeď	zeď	k1gFnSc4	zeď
vysokou	vysoká	k1gFnSc4	vysoká
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
toho	ten	k3xDgInSc2	ten
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c4	nad
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
elektron	elektron	k1gInSc1	elektron
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
přes	přes	k7c4	přes
13,6	[number]	k4	13,6
<g/>
-eV	V	k?	-eV
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
méně	málo	k6eAd2	málo
jak	jak	k8xC	jak
13,6	[number]	k4	13,6
eV	eV	k?	eV
energie	energie	k1gFnSc2	energie
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
nebo	nebo	k8xC	nebo
rovna	roven	k2eAgFnSc1d1	rovna
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
současnou	současný	k2eAgFnSc7d1	současná
atomovou	atomový	k2eAgFnSc7d1	atomová
vazbou	vazba	k1gFnSc7	vazba
nebo	nebo	k8xC	nebo
molekulárním	molekulární	k2eAgInSc7d1	molekulární
orbitalem	orbital	k1gInSc7	orbital
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
možným	možný	k2eAgInSc7d1	možný
orbitalem	orbital	k1gInSc7	orbital
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
absorbovaná	absorbovaný	k2eAgFnSc1d1	absorbovaná
energie	energie	k1gFnSc1	energie
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
tento	tento	k3xDgInSc4	tento
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
elektron	elektron	k1gInSc1	elektron
vydává	vydávat	k5eAaImIp3nS	vydávat
jako	jako	k9	jako
volný	volný	k2eAgInSc1d1	volný
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
elektron	elektron	k1gInSc1	elektron
krátce	krátce	k6eAd1	krátce
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
absorbovaná	absorbovaný	k2eAgFnSc1d1	absorbovaná
energie	energie	k1gFnSc1	energie
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
a	a	k8xC	a
elektron	elektron	k1gInSc1	elektron
znovu	znovu	k6eAd1	znovu
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
možného	možný	k2eAgInSc2d1	možný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tvaru	tvar	k1gInSc3	tvar
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
volný	volný	k2eAgInSc1d1	volný
elektron	elektron	k1gInSc1	elektron
energii	energie	k1gFnSc4	energie
vyšší	vysoký	k2eAgFnSc4d2	vyšší
nebo	nebo	k8xC	nebo
rovnu	roven	k2eAgFnSc4d1	rovna
energii	energie	k1gFnSc4	energie
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
schopný	schopný	k2eAgMnSc1d1	schopný
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
volný	volný	k2eAgInSc1d1	volný
elektron	elektron	k1gInSc1	elektron
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgInS	učinit
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
možného	možný	k2eAgInSc2d1	možný
energetického	energetický	k2eAgInSc2d1	energetický
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
zbývající	zbývající	k2eAgFnSc2d1	zbývající
energie	energie	k1gFnSc2	energie
bude	být	k5eAaImBp3nS	být
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
elektron	elektron	k1gInSc1	elektron
nemá	mít	k5eNaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
elektrostatickou	elektrostatický	k2eAgFnSc7d1	elektrostatická
silou	síla	k1gFnSc7	síla
zahnán	zahnán	k2eAgInSc1d1	zahnán
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
v	v	k7c6	v
Coulombově	Coulombův	k2eAgInSc6d1	Coulombův
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Sekvenční	sekvenční	k2eAgFnSc1d1	sekvenční
ionizace	ionizace	k1gFnSc1	ionizace
je	být	k5eAaImIp3nS	být
popis	popis	k1gInSc4	popis
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
probíhá	probíhat	k5eAaImIp3nS	probíhat
ionizace	ionizace	k1gFnSc1	ionizace
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
iont	iont	k1gInSc1	iont
s	s	k7c7	s
+2	+2	k4	+2
nábojem	náboj	k1gInSc7	náboj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
iontů	ion	k1gInPc2	ion
s	s	k7c7	s
+1	+1	k4	+1
nábojem	náboj	k1gInSc7	náboj
nebo	nebo	k8xC	nebo
+3	+3	k4	+3
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
numerické	numerický	k2eAgInPc1d1	numerický
náboj	náboj	k1gInSc4	náboj
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnSc2	molekula
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
měnit	měnit	k5eAaImF	měnit
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
z	z	k7c2	z
čísla	číslo	k1gNnSc2	číslo
na	na	k7c4	na
číslo	číslo	k1gNnSc4	číslo
předchozí	předchozí	k2eAgNnSc4d1	předchozí
nebo	nebo	k8xC	nebo
následující	následující	k2eAgNnSc4d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ionizace	ionizace	k1gFnSc1	ionizace
probíhat	probíhat	k5eAaImF	probíhat
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
elektron	elektron	k1gInSc1	elektron
má	mít	k5eAaImIp3nS	mít
dost	dost	k6eAd1	dost
energie	energie	k1gFnSc1	energie
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
další	další	k2eAgFnSc4d1	další
možnost	možnost	k1gFnSc4	možnost
tunelové	tunelový	k2eAgFnSc2d1	tunelová
ionizace	ionizace	k1gFnSc2	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
ionizace	ionizace	k1gFnSc2	ionizace
probíhá	probíhat	k5eAaImIp3nS	probíhat
důsledkem	důsledek	k1gInSc7	důsledek
ionizace	ionizace	k1gFnSc2	ionizace
kvantového	kvantový	k2eAgNnSc2d1	kvantové
tunelování	tunelování	k1gNnSc2	tunelování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
ionizaci	ionizace	k1gFnSc6	ionizace
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
elektron	elektron	k1gInSc4	elektron
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvantové	kvantový	k2eAgNnSc1d1	kvantové
tunelování	tunelování	k1gNnSc1	tunelování
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
elektronu	elektron	k1gInSc2	elektron
jednoduše	jednoduše	k6eAd1	jednoduše
projít	projít	k5eAaPmF	projít
přes	přes	k7c4	přes
potenciálovou	potenciálový	k2eAgFnSc4d1	potenciálová
barieru	bariera	k1gFnSc4	bariera
díky	díky	k7c3	díky
vlnovému	vlnový	k2eAgInSc3d1	vlnový
charakteru	charakter	k1gInSc3	charakter
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
průchodu	průchod	k1gInSc2	průchod
elektronu	elektron	k1gInSc2	elektron
přes	přes	k7c4	přes
bariéru	bariéra	k1gFnSc4	bariéra
exponenciálně	exponenciálně	k6eAd1	exponenciálně
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
šířkou	šířka	k1gFnSc7	šířka
potenciálové	potenciálový	k2eAgFnSc2d1	potenciálová
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
kombinován	kombinován	k2eAgInSc1d1	kombinován
s	s	k7c7	s
tunelem	tunel	k1gInSc7	tunel
ionizace	ionizace	k1gFnSc2	ionizace
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
jevu	jev	k1gInSc3	jev
ne-sekvenční	ekvenční	k2eNgFnSc2d1	-sekvenční
ionizace	ionizace	k1gFnSc2	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnSc2	molekula
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaslán	zaslat	k5eAaPmNgInS	zaslat
zpátky	zpátky	k6eAd1	zpátky
vlivem	vliv	k1gInSc7	vliv
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
zkombinován	zkombinovat	k5eAaPmNgMnS	zkombinovat
s	s	k7c7	s
atomem	atom	k1gInSc7	atom
nebo	nebo	k8xC	nebo
molekulou	molekula	k1gFnSc7	molekula
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
přebytečné	přebytečný	k2eAgFnSc2d1	přebytečná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
možnost	možnost	k1gFnSc4	možnost
dále	daleko	k6eAd2	daleko
ionizovat	ionizovat	k5eAaBmF	ionizovat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vysokoenergetickým	vysokoenergetický	k2eAgFnPc3d1	vysokoenergetická
srážkám	srážka	k1gFnPc3	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dodatečná	dodatečný	k2eAgFnSc1d1	dodatečná
ionizace	ionizace	k1gFnSc1	ionizace
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
ne-sekvenční	ekvenční	k2eNgFnSc1d1	-sekvenční
ionizace	ionizace	k1gFnSc1	ionizace
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádné	žádný	k3yNgNnSc1	žádný
pravidlo	pravidlo	k1gNnSc1	pravidlo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
elektron	elektron	k1gInSc1	elektron
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
<g/>
,	,	kIx,	,
atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
s	s	k7c7	s
+2	+2	k4	+2
nábojem	náboj	k1gInSc7	náboj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnSc2	molekula
s	s	k7c7	s
neutrálním	neutrální	k2eAgInSc7d1	neutrální
nábojem	náboj	k1gInSc7	náboj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
čísla	číslo	k1gNnPc1	číslo
nábojů	náboj	k1gInPc2	náboj
nejsou	být	k5eNaImIp3nP	být
sekvenční	sekvenční	k2eAgFnPc1d1	sekvenční
<g/>
.	.	kIx.	.
</s>
<s>
Ne-sekvenční	Neekvenční	k2eAgFnSc1d1	Ne-sekvenční
ionizace	ionizace	k1gFnSc1	ionizace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
na	na	k7c6	na
laserovém	laserový	k2eAgNnSc6d1	laserové
poli	pole	k1gNnSc6	pole
o	o	k7c6	o
nižší	nízký	k2eAgFnSc6d2	nižší
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
míře	míra	k1gFnSc6	míra
ionizace	ionizace	k1gFnSc2	ionizace
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
ionizačních	ionizační	k2eAgInPc2d1	ionizační
události	událost	k1gFnSc3	událost
sekvenční	sekvenční	k2eAgMnPc1d1	sekvenční
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
ionizován	ionizovat	k5eAaBmNgInS	ionizovat
účinkem	účinek	k1gInSc7	účinek
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c4	v
1	[number]	k4	1
cm	cm	kA	cm
<g/>
3	[number]	k4	3
vzduchu	vzduch	k1gInSc2	vzduch
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
přibližně	přibližně	k6eAd1	přibližně
deset	deset	k4xCc4	deset
kladných	kladný	k2eAgInPc2d1	kladný
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
mohou	moct	k5eAaImIp3nP	moct
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
neutrální	neutrální	k2eAgFnSc7d1	neutrální
molekulou	molekula	k1gFnSc7	molekula
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
záporný	záporný	k2eAgInSc4d1	záporný
iont	iont	k1gInSc4	iont
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
ionizace	ionizace	k1gFnSc1	ionizace
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
např.	např.	kA	např.
el.	el.	k?	el.
výbojem	výboj	k1gInSc7	výboj
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
či	či	k8xC	či
srážkami	srážka	k1gFnPc7	srážka
molekul	molekula	k1gFnPc2	molekula
s	s	k7c7	s
velkou	velká	k1gFnSc7	velká
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
Fotoionizace	fotoionizace	k1gFnSc2	fotoionizace
-	-	kIx~	-
vznik	vznik	k1gInSc1	vznik
iontů	ion	k1gInPc2	ion
absorpcí	absorpce	k1gFnPc2	absorpce
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
<g/>
,	,	kIx,	,
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
či	či	k8xC	či
ʏ	ʏ	k?	ʏ
Násobná	násobný	k2eAgFnSc1d1	násobná
ionizace	ionizace	k1gFnSc1	ionizace
-	-	kIx~	-
odtržení	odtržení	k1gNnSc1	odtržení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgInSc2	jeden
elektronu	elektron	k1gInSc2	elektron
od	od	k7c2	od
neutrální	neutrální	k2eAgFnSc2d1	neutrální
molekuly	molekula	k1gFnSc2	molekula
(	(	kIx(	(
<g/>
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
http://en.wikipedia.org/wiki/Ionization	[url]	k1gInSc1	http://en.wikipedia.org/wiki/Ionization
Plazma	plazma	k1gFnSc1	plazma
Ionosféra	ionosféra	k1gFnSc1	ionosféra
Rekombinace	rekombinace	k1gFnSc2	rekombinace
Ionizující	ionizující	k2eAgNnSc4d1	ionizující
záření	záření	k1gNnSc4	záření
</s>
