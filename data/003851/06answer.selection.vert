<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c4	v
hodnocení	hodnocení	k1gNnSc4	hodnocení
podle	podle	k7c2	podle
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
10.	[number]	k4	10.
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
a	a	k8xC	a
3.	[number]	k4	3.
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
v	v	k7c6	v
EU	EU	kA	EU
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ukazateli	ukazatel	k1gInSc6	ukazatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
39 500	[number]	k4	39 500
USD	USD	kA	USD
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
MMF	MMF	kA	MMF
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
