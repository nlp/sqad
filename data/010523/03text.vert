<p>
<s>
Terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
především	především	k6eAd1	především
z	z	k7c2	z
křemičitanových	křemičitanový	k2eAgFnPc2d1	křemičitanová
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Terra	Terra	k1gFnSc1	Terra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
alternativní	alternativní	k2eAgFnSc7d1	alternativní
definicí	definice	k1gFnSc7	definice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
rysy	rys	k1gInPc1	rys
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
"	"	kIx"	"
<g/>
podobné	podobný	k2eAgFnSc6d1	podobná
Zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
složeni	složen	k2eAgMnPc1d1	složen
především	především	k6eAd1	především
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kombinacích	kombinace	k1gFnPc6	kombinace
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
skupenstvích	skupenství	k1gNnPc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
podobnou	podobný	k2eAgFnSc7d1	podobná
strukturu	struktura	k1gFnSc4	struktura
<g/>
:	:	kIx,	:
centrální	centrální	k2eAgNnSc4d1	centrální
kovové	kovový	k2eAgNnSc4d1	kovové
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgNnSc1d1	obklopené
křemičitanovým	křemičitanový	k2eAgInSc7d1	křemičitanový
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
kůru	kůr	k1gInSc6	kůr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
přirozený	přirozený	k2eAgInSc4d1	přirozený
satelit	satelit	k1gInSc4	satelit
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Terestrická	terestrický	k2eAgNnPc1d1	terestrické
tělesa	těleso	k1gNnPc1	těleso
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
různorodý	různorodý	k2eAgInSc4d1	různorodý
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
topografii	topografie	k1gFnSc4	topografie
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kaňony	kaňon	k1gInPc1	kaňon
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
sopky	sopka	k1gFnPc1	sopka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Planety	planeta	k1gFnPc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
==	==	k?	==
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
terestrické	terestrický	k2eAgFnPc4d1	terestrická
planety	planeta	k1gFnPc4	planeta
<g/>
:	:	kIx,	:
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
terrestrických	terrestrický	k2eAgInPc2d1	terrestrický
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
Io	Io	k1gFnSc2	Io
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátečních	počáteční	k2eAgNnPc6d1	počáteční
obdobích	období	k1gNnPc6	období
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
terestrických	terestrický	k2eAgNnPc2d1	terestrické
těles	těleso	k1gNnPc2	těleso
tzv.	tzv.	kA	tzv.
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
diferenciovaných	diferenciovaný	k2eAgFnPc2d1	diferenciovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
finálního	finální	k2eAgInSc2d1	finální
procesu	proces	k1gInSc2	proces
tvorby	tvorba	k1gFnSc2	tvorba
dnešních	dnešní	k2eAgFnPc2d1	dnešní
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vznik	vznik	k1gInSc1	vznik
našeho	náš	k3xOp1gInSc2	náš
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
migrování	migrování	k1gNnPc2	migrování
planet	planeta	k1gFnPc2	planeta
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vymrštění	vymrštění	k1gNnSc3	vymrštění
některého	některý	k3yIgInSc2	některý
takového	takový	k3xDgNnSc2	takový
tělesa	těleso	k1gNnSc2	těleso
mimo	mimo	k7c4	mimo
Sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
(	(	kIx(	(
<g/>
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
důležitou	důležitý	k2eAgFnSc4d1	důležitá
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fungování	fungování	k1gNnSc2	fungování
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
Země	zem	k1gFnSc2	zem
také	také	k9	také
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
potom	potom	k6eAd1	potom
již	již	k6eAd1	již
jen	jen	k9	jen
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
ledový	ledový	k2eAgInSc4d1	ledový
měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Exoplanety	Exoplanet	k1gInPc4	Exoplanet
==	==	k?	==
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
plánují	plánovat	k5eAaImIp3nP	plánovat
projekty	projekt	k1gInPc1	projekt
Terrestrial	Terrestrial	k1gInSc1	Terrestrial
Planet	planeta	k1gFnPc2	planeta
Finder	Findra	k1gFnPc2	Findra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hledač	hledač	k1gInSc1	hledač
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
umožnit	umožnit	k5eAaPmF	umožnit
detekci	detekce	k1gFnSc4	detekce
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
vně	vně	k7c2	vně
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
zatím	zatím	k6eAd1	zatím
objevené	objevený	k2eAgInPc1d1	objevený
zřejmě	zřejmě	k6eAd1	zřejmě
terestrické	terestrický	k2eAgInPc1d1	terestrický
exoplanety	exoplanet	k1gInPc1	exoplanet
obíhají	obíhat	k5eAaImIp3nP	obíhat
hvězdy	hvězda	k1gFnPc4	hvězda
μ	μ	k?	μ
Arae	Arae	k1gNnSc1	Arae
<g/>
,	,	kIx,	,
55	[number]	k4	55
Cancri	Cancri	k1gNnSc2	Cancri
a	a	k8xC	a
GJ	GJ	kA	GJ
436	[number]	k4	436
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
známé	známý	k2eAgInPc1d1	známý
exoplanety	exoplanet	k1gInPc1	exoplanet
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
planeta	planeta	k1gFnSc1	planeta
typu	typ	k1gInSc2	typ
plynný	plynný	k2eAgMnSc1d1	plynný
obr	obr	k1gMnSc1	obr
</s>
</p>
<p>
<s>
planeta	planeta	k1gFnSc1	planeta
typu	typ	k1gInSc2	typ
horký	horký	k2eAgInSc1d1	horký
Jupiter	Jupiter	k1gInSc1	Jupiter
</s>
</p>
<p>
<s>
Obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
zóna	zóna	k1gFnSc1	zóna
</s>
</p>
