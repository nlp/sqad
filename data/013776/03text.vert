<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Grišin	Grišin	k2eAgMnSc1d1
(	(	kIx(
<g/>
rychlobruslař	rychlobruslař	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Grišin	Grišin	k2eAgInSc1d1
Jevgenij	Jevgenij	k1gFnSc7
Grišin	Grišina	k1gFnPc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1931	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Tula	Tula	k1gFnSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
SFSR	SFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
74	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
Rychlobruslařská	rychlobruslařský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
Reprezentace	reprezentace	k1gFnSc2
</s>
<s>
od	od	k7c2
1954	#num#	k4
Konec	konec	k1gInSc1
kariéry	kariéra	k1gFnSc2
</s>
<s>
1970	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Rychlobruslení	rychlobruslení	k1gNnSc1
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1956	#num#	k4
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
</s>
<s>
500	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1956	#num#	k4
Cortina	Cortina	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
</s>
<s>
1500	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1960	#num#	k4
Squaw	squaw	k1gFnSc1
Valley	Vallea	k1gFnSc2
</s>
<s>
500	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1960	#num#	k4
Squaw	squaw	k1gFnSc1
Valley	Vallea	k1gFnSc2
</s>
<s>
1500	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1964	#num#	k4
Innsbruck	Innsbruck	k1gInSc1
</s>
<s>
500	#num#	k4
m	m	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1954	#num#	k4
Sapporo	Sappora	k1gFnSc5
</s>
<s>
víceboj	víceboj	k1gInSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
1956	#num#	k4
Oslo	Oslo	k1gNnSc1
</s>
<s>
víceboj	víceboj	k1gInSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1956	#num#	k4
Helsinky	Helsinky	k1gFnPc1
</s>
<s>
víceboj	víceboj	k1gInSc1
</s>
<s>
Jevgenij	Jevgenít	k1gMnSc1
Romanovič	Romanovič	k1gMnSc1
Grišin	Grišin	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Е	Е	k?
<g/>
́	́	k?
<g/>
н	н	k?
Р	Р	k?
<g/>
́	́	k?
<g/>
н	н	k?
Г	Г	k?
<g/>
́	́	k?
<g/>
ш	ш	k?
<g/>
;	;	kIx,
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1931	#num#	k4
Tula	Tula	k1gFnSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
SFSR	SFSR	kA
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
rychlobruslař	rychlobruslař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Prvního	první	k4xOgNnSc2
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
na	na	k7c4
11	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
startoval	startovat	k5eAaBmAgInS
i	i	k9
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
si	se	k3xPyFc3
přivezl	přivézt	k5eAaPmAgMnS
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největších	veliký	k2eAgMnPc2d3
úspěchů	úspěch	k1gInPc2
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
<g/>
,	,	kIx,
vybojoval	vybojovat	k5eAaPmAgMnS
bronz	bronz	k1gInSc4
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
a	a	k8xC
vyhrál	vyhrát	k5eAaPmAgMnS
v	v	k7c6
závodech	závod	k1gInPc6
na	na	k7c4
500	#num#	k4
m	m	kA
a	a	k8xC
1500	#num#	k4
m	m	kA
na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
nejlépe	dobře	k6eAd3
na	na	k7c6
pátém	pátý	k4xOgNnSc6
místě	místo	k1gNnSc6
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
startoval	startovat	k5eAaBmAgMnS
pouze	pouze	k6eAd1
na	na	k7c6
menších	malý	k2eAgInPc6d2
závodech	závod	k1gInPc6
a	a	k8xC
sovětských	sovětský	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
<g/>
,	,	kIx,
po	po	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
velké	velký	k2eAgFnPc4d1
mezinárodní	mezinárodní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
však	však	k9
obhájil	obhájit	k5eAaPmAgMnS
na	na	k7c6
zimní	zimní	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
obě	dva	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
světových	světový	k2eAgInPc6d1
a	a	k8xC
kontinentálních	kontinentální	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
příliš	příliš	k6eAd1
nedařilo	dařit	k5eNaImAgNnS
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
se	se	k3xPyFc4
umisťoval	umisťovat	k5eAaImAgInS
na	na	k7c6
konci	konec	k1gInSc6
druhé	druhý	k4xOgFnSc2
desítky	desítka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
cenný	cenný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
na	na	k7c6
ZOH	ZOH	kA
1964	#num#	k4
na	na	k7c6
sprinterské	sprinterský	k2eAgFnSc6d1
pětistovce	pětistovka	k1gFnSc6
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
byl	být	k5eAaImAgMnS
jedenáctý	jedenáctý	k4xOgMnSc1
na	na	k7c6
trojnásobné	trojnásobný	k2eAgFnSc6d1
distanci	distance	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
již	již	k6eAd1
na	na	k7c6
mistrovstvích	mistrovství	k1gNnPc6
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
neobjevoval	objevovat	k5eNaImAgMnS
<g/>
,	,	kIx,
absolvoval	absolvovat	k5eAaPmAgMnS
pouze	pouze	k6eAd1
menší	malý	k2eAgInPc4d2
mezinárodní	mezinárodní	k2eAgInPc4d1
či	či	k8xC
domácí	domácí	k2eAgInPc4d1
závody	závod	k1gInPc4
nebo	nebo	k8xC
sovětské	sovětský	k2eAgInPc4d1
šampionáty	šampionát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
byly	být	k5eAaImAgFnP
Zimní	zimní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
1968	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
startoval	startovat	k5eAaBmAgInS
pouze	pouze	k6eAd1
na	na	k7c6
trati	trať	k1gFnSc6
500	#num#	k4
m	m	kA
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
těsně	těsně	k6eAd1
pod	pod	k7c7
stupni	stupeň	k1gInPc7
vítězů	vítěz	k1gMnPc2
na	na	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledního	poslední	k2eAgInSc2d1
závodu	závod	k1gInSc2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jevgenij	Jevgenij	k1gMnSc1
Grišin	Grišin	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Grišin	Grišin	k2eAgInSc4d1
na	na	k7c4
speedskatingnews	speedskatingnews	k1gInSc4
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Grišin	Grišin	k2eAgMnSc1d1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
–	–	k?
500	#num#	k4
m	m	kA
</s>
<s>
1924	#num#	k4
Charles	Charles	k1gMnSc1
Jewtraw	Jewtraw	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1928	#num#	k4
Bernt	Bernt	k1gInSc1
Evensen	Evensen	k1gInSc4
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
1928	#num#	k4
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc4
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1932	#num#	k4
Jack	Jack	k1gMnSc1
Shea	Shea	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1936	#num#	k4
Ivar	Ivar	k1gMnSc1
Ballangrud	Ballangrud	k1gMnSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
Finn	Finn	k1gInSc1
Helgesen	Helgesen	k2eAgInSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
Ken	Ken	k1gMnSc1
Henry	Henry	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1956	#num#	k4
Jevgenij	Jevgenij	k1gFnSc1
Grišin	Grišina	k1gFnPc2
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1960	#num#	k4
Jevgenij	Jevgenij	k1gFnSc1
Grišin	Grišina	k1gFnPc2
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
Richard	Richard	k1gMnSc1
McDermott	McDermott	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1968	#num#	k4
Erhard	Erhard	k1gMnSc1
Keller	Keller	k1gMnSc1
(	(	kIx(
<g/>
FRG	FRG	kA
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
Erhard	Erhard	k1gMnSc1
Keller	Keller	k1gMnSc1
(	(	kIx(
<g/>
FRG	FRG	kA
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
Jevgenij	Jevgenij	k1gMnSc1
Kulikov	Kulikov	k1gInSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
Eric	Eric	k1gFnSc1
Heiden	Heidna	k1gFnPc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Sergej	Sergej	k1gMnSc1
Fokičev	Fokičev	k1gMnSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
Uwe-Jens	Uwe-Jens	k1gInSc1
Mey	Mey	k1gFnSc2
(	(	kIx(
<g/>
GDR	GDR	kA
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
Uwe-Jens	Uwe-Jens	k1gInSc1
Mey	Mey	k1gFnSc2
(	(	kIx(
<g/>
GER	Gera	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
Alexandr	Alexandr	k1gMnSc1
Golubev	Golubev	k1gFnSc4
(	(	kIx(
<g/>
RUS	Rus	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Hirojasu	Hirojas	k1gInSc2
Šimizu	Šimiz	k1gInSc2
(	(	kIx(
<g/>
JPN	JPN	kA
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Casey	Casea	k1gFnSc2
FitzRandolph	FitzRandolpha	k1gFnPc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Joey	Joea	k1gFnSc2
Cheek	Cheky	k1gFnPc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
Mo	Mo	k1gFnSc1
Tche-pom	Tche-pom	k1gInSc1
(	(	kIx(
<g/>
KOR	KOR	kA
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Michel	Michel	k1gMnSc1
Mulder	Mulder	k1gMnSc1
(	(	kIx(
<g/>
NED	NED	kA
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
Hå	Hå	k1gInSc1
Lorentzen	Lorentzen	k2eAgInSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
–	–	k?
1500	#num#	k4
m	m	kA
</s>
<s>
1924	#num#	k4
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc4
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1928	#num#	k4
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc4
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1932	#num#	k4
Jack	Jack	k1gMnSc1
Shea	Shea	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1936	#num#	k4
Charles	Charles	k1gMnSc1
Mathiesen	Mathiesen	k2eAgMnSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
Sverre	Sverr	k1gInSc5
Farstad	Farstad	k1gInSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
Hjalmar	Hjalmar	k1gMnSc1
Andersen	Andersen	k1gMnSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1956	#num#	k4
Jevgenij	Jevgenij	k1gFnSc1
Grišin	Grišina	k1gFnPc2
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
<g/>
1956	#num#	k4
Jurij	Jurij	k1gFnSc2
Michajlov	Michajlovo	k1gNnPc2
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1960	#num#	k4
Roald	Roald	k1gMnSc1
Aas	Aas	k1gMnSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
1960	#num#	k4
Jevgenij	Jevgenij	k1gFnPc2
Grišin	Grišina	k1gFnPc2
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
Ants	Ants	k1gInSc1
Antson	Antson	k1gInSc4
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1968	#num#	k4
Kees	Kees	k1gInSc1
Verkerk	Verkerk	k1gInSc4
(	(	kIx(
<g/>
NED	NED	kA
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
Ard	Ard	k1gMnSc1
Schenk	Schenk	k1gMnSc1
(	(	kIx(
<g/>
NED	NED	kA
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
Jan	Jan	k1gMnSc1
Egil	Egil	k1gMnSc1
Storholt	Storholt	k1gMnSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
Eric	Eric	k1gFnSc1
Heiden	Heidna	k1gFnPc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Gaétan	Gaétan	k1gInSc1
Boucher	Bouchra	k1gFnPc2
(	(	kIx(
<g/>
CAN	CAN	kA
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
André	André	k1gMnSc1
Hoffmann	Hoffmann	k1gMnSc1
(	(	kIx(
<g/>
GDR	GDR	kA
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
Johann	Johann	k1gNnSc1
Olav	Olava	k1gFnPc2
Koss	Kossa	k1gFnPc2
(	(	kIx(
<g/>
NOR	Nora	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
Johann	Johann	k1gNnSc1
Olav	Olava	k1gFnPc2
Koss	Kossa	k1gFnPc2
(	(	kIx(
<g/>
NOR	Nora	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Å	Å	k5eAaPmIp3nS
Sø	Sø	k1gMnSc1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Derek	Derek	k1gMnSc1
Parra	Parra	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Enrico	Enrico	k1gNnSc1
Fabris	Fabris	k1gFnPc2
(	(	kIx(
<g/>
ITA	ITA	kA
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
Mark	Mark	k1gMnSc1
Tuitert	Tuitert	k1gMnSc1
(	(	kIx(
<g/>
NED	NED	kA
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Zbigniew	Zbigniew	k1gFnSc1
Bródka	Bródka	k1gFnSc1
(	(	kIx(
<g/>
POL	pola	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
Kjeld	Kjeld	k1gInSc1
Nuis	Nuis	k1gInSc4
(	(	kIx(
<g/>
NED	NED	kA
<g/>
)	)	kIx)
</s>
<s>
Mistři	mistr	k1gMnPc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
rychlobruslení	rychlobruslení	k1gNnSc6
–	–	k?
klasický	klasický	k2eAgInSc4d1
víceboj	víceboj	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
:	:	kIx,
titul	titul	k1gInSc4
neudělen	udělen	k2eNgInSc4d1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
:	:	kIx,
titul	titul	k1gInSc4
neudělen	udělen	k2eNgInSc4d1
<g/>
)	)	kIx)
•	•	k?
1893	#num#	k4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Ericson	Ericsona	k1gFnPc2
•	•	k?
1894	#num#	k4
<g/>
:	:	kIx,
titul	titul	k1gInSc4
neudělen	udělen	k2eNgInSc4d1
•	•	k?
1895	#num#	k4
<g/>
:	:	kIx,
Alfred	Alfred	k1gMnSc1
Næ	Næ	k1gFnPc2
•	•	k?
1896	#num#	k4
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Seyler	Seyler	k1gMnSc1
•	•	k?
1897	#num#	k4
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Seyler	Seyler	k1gMnSc1
•	•	k?
1898	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Gustaf	Gustaf	k1gInSc1
Estlander	Estlander	k1gInSc1
•	•	k?
1899	#num#	k4
<g/>
:	:	kIx,
Peder	Peder	k1gInSc1
Ø	Ø	k1gInSc1
•	•	k?
1900	#num#	k4
<g/>
:	:	kIx,
Peder	Peder	k1gInSc1
Ø	Ø	k1gInSc1
•	•	k?
1901	#num#	k4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Gundersen	Gundersna	k1gFnPc2
•	•	k?
1902	#num#	k4
<g/>
:	:	kIx,
Johan	Johana	k1gFnPc2
Schwartz	Schwartz	k1gInSc1
•	•	k?
1903	#num#	k4
<g/>
:	:	kIx,
titul	titul	k1gInSc4
neudělen	udělen	k2eNgInSc4d1
•	•	k?
1904	#num#	k4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Gundersen	Gundersna	k1gFnPc2
•	•	k?
1905	#num#	k4
<g/>
:	:	kIx,
titul	titul	k1gInSc4
neudělen	udělen	k2eNgInSc4d1
•	•	k?
1906	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Rudolf	Rudolf	k1gMnSc1
Gundersen	Gundersen	k1gInSc1
•	•	k?
1907	#num#	k4
<g/>
:	:	kIx,
Moje	můj	k3xOp1gFnPc1
Öholm	Öholm	k1gInSc1
•	•	k?
1908	#num#	k4
<g/>
:	:	kIx,
Moje	můj	k3xOp1gFnPc1
Öholm	Öholm	k1gInSc1
•	•	k?
1909	#num#	k4
<g/>
:	:	kIx,
Oscar	Oscar	k1gInSc1
Mathisen	Mathisen	k1gInSc1
•	•	k?
1910	#num#	k4
<g/>
:	:	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Strunnikov	Strunnikov	k1gInSc1
•	•	k?
1911	#num#	k4
<g/>
:	:	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Strunnikov	Strunnikov	k1gInSc1
•	•	k?
1912	#num#	k4
<g/>
:	:	kIx,
Oscar	Oscar	k1gInSc1
Mathisen	Mathisen	k1gInSc1
•	•	k?
1913	#num#	k4
<g/>
:	:	kIx,
Vasilij	Vasilij	k1gFnSc1
Ippolitov	Ippolitov	k1gInSc1
•	•	k?
1914	#num#	k4
<g/>
:	:	kIx,
Oscar	Oscar	k1gInSc1
Mathisen	Mathisen	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1915	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
:	:	kIx,
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc1
•	•	k?
1923	#num#	k4
<g/>
:	:	kIx,
Harald	Harald	k1gInSc1
Strø	Strø	k1gInSc1
•	•	k?
1924	#num#	k4
<g/>
:	:	kIx,
Roald	Roaldo	k1gNnPc2
Larsen	larsena	k1gFnPc2
•	•	k?
1925	#num#	k4
<g/>
:	:	kIx,
Otto	Otto	k1gMnSc1
Polacsek	Polacska	k1gFnPc2
•	•	k?
1926	#num#	k4
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Skutnabb	Skutnabba	k1gFnPc2
•	•	k?
1927	#num#	k4
<g/>
:	:	kIx,
Bernt	Bernt	k1gInSc1
Evensen	Evensen	k1gInSc1
•	•	k?
1928	#num#	k4
<g/>
:	:	kIx,
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc1
•	•	k?
1929	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Ivar	Ivar	k1gInSc1
Ballangrud	Ballangrud	k1gInSc1
•	•	k?
1930	#num#	k4
<g/>
:	:	kIx,
Ivar	Ivar	k1gInSc1
Ballangrud	Ballangrud	k1gInSc1
•	•	k?
1931	#num#	k4
<g/>
:	:	kIx,
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc1
•	•	k?
1932	#num#	k4
<g/>
:	:	kIx,
Clas	Clas	k1gInSc1
Thunberg	Thunberg	k1gInSc1
•	•	k?
1933	#num#	k4
<g/>
:	:	kIx,
Ivar	Ivar	k1gInSc1
Ballangrud	Ballangrud	k1gInSc1
•	•	k?
1934	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Staksrud	Staksruda	k1gFnPc2
•	•	k?
1935	#num#	k4
<g/>
:	:	kIx,
Karl	Karla	k1gFnPc2
Wazulek	Wazulka	k1gFnPc2
•	•	k?
1936	#num#	k4
<g/>
:	:	kIx,
Ivar	Ivar	k1gInSc1
Ballangrud	Ballangrud	k1gInSc1
•	•	k?
1937	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Michael	Michael	k1gMnSc1
Staksrud	Staksrud	k1gMnSc1
•	•	k?
1938	#num#	k4
<g/>
:	:	kIx,
Charles	Charles	k1gMnSc1
Mathiesen	Mathiesen	k2eAgMnSc1d1
•	•	k?
1939	#num#	k4
<g/>
:	:	kIx,
Alfons	Alfons	k1gMnSc1
Bē	Bē	k1gMnSc1
•	•	k?
1940	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
•	•	k?
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Göthe	Göth	k1gInSc2
Hedlund	Hedlund	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1947	#num#	k4
<g/>
:	:	kIx,
Å	Å	k1gInSc2
Seyffarth	Seyffarth	k1gInSc1
•	•	k?
1948	#num#	k4
<g/>
:	:	kIx,
Reidar	Reidar	k1gInSc1
Liaklev	Liaklev	k1gFnSc2
•	•	k?
1949	#num#	k4
<g/>
:	:	kIx,
Sverre	Sverr	k1gInSc5
Farstad	Farstad	k1gInSc4
•	•	k?
1950	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Hjalmar	Hjalmar	k1gMnSc1
Andersen	Andersen	k1gMnSc1
•	•	k?
1951	#num#	k4
<g/>
:	:	kIx,
Hjalmar	Hjalmar	k1gMnSc1
Andersen	Andersen	k1gMnSc1
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Hjalmar	Hjalmar	k1gMnSc1
Andersen	Andersen	k1gMnSc1
•	•	k?
1953	#num#	k4
<g/>
:	:	kIx,
Kees	Kees	k1gInSc1
Broekman	Broekman	k1gMnSc1
•	•	k?
1954	#num#	k4
<g/>
:	:	kIx,
Boris	Boris	k1gMnSc1
Šilkov	Šilkov	k1gInSc1
•	•	k?
1955	#num#	k4
<g/>
:	:	kIx,
Sigvard	Sigvard	k1gInSc1
Ericsson	Ericsson	kA
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Jevgenij	Jevgenij	k1gMnSc1
Grišin	Grišina	k1gFnPc2
•	•	k?
1957	#num#	k4
<g/>
:	:	kIx,
Oleg	Oleg	k1gMnSc1
Gončarenko	Gončarenka	k1gFnSc5
•	•	k?
1958	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Oleg	Oleg	k1gMnSc1
Gončarenko	Gončarenka	k1gFnSc5
•	•	k?
1959	#num#	k4
<g/>
:	:	kIx,
Knut	knuta	k1gFnPc2
Johannesen	Johannesen	k2eAgInSc1d1
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Knut	knuta	k1gFnPc2
Johannesen	Johannesen	k2eAgInSc1d1
•	•	k?
1961	#num#	k4
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Kosičkin	Kosičkina	k1gFnPc2
•	•	k?
1962	#num#	k4
<g/>
:	:	kIx,
Robert	Roberta	k1gFnPc2
Merkulov	Merkulov	k1gInSc1
•	•	k?
1963	#num#	k4
<g/>
:	:	kIx,
Nils	Nils	k1gInSc1
Aaness	Aaness	k1gInSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Ants	Ants	k1gInSc1
Antson	Antson	k1gInSc1
•	•	k?
1965	#num#	k4
<g/>
:	:	kIx,
Eduard	Eduard	k1gMnSc1
Matusevič	Matusevič	k1gMnSc1
•	•	k?
1966	#num#	k4
<g/>
:	:	kIx,
Ard	Ard	k1gMnSc1
Schenk	Schenk	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1967	#num#	k4
<g/>
:	:	kIx,
Kees	Kees	k1gInSc1
Verkerk	Verkerk	k1gInSc1
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
Fred	Fred	k1gMnSc1
Anton	Anton	k1gMnSc1
Maier	Maier	k1gMnSc1
•	•	k?
1969	#num#	k4
<g/>
:	:	kIx,
Dag	dag	kA
Fornæ	Fornæ	k1gInSc1
•	•	k?
1970	#num#	k4
<g/>
:	:	kIx,
Ard	Ard	k1gFnSc1
Schenk	Schenk	k1gInSc1
•	•	k?
1971	#num#	k4
<g/>
:	:	kIx,
Dag	dag	kA
Fornæ	Fornæ	k1gInSc1
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Ard	Ard	k1gFnSc1
Schenk	Schenk	k1gInSc1
•	•	k?
1973	#num#	k4
<g/>
:	:	kIx,
Göran	Göran	k1gInSc1
Claeson	Claeson	k1gInSc1
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Göran	Göran	k1gMnSc1
Claeson	Claeson	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1975	#num#	k4
<g/>
:	:	kIx,
Sten	sten	k1gInSc1
Stensen	Stensen	k1gInSc1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Kay	Kay	k1gFnSc1
Stenshjemmet	Stenshjemmet	k1gInSc1
•	•	k?
1977	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Egil	Egil	k1gMnSc1
Storholt	Storholt	k1gMnSc1
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Sergej	Sergej	k1gMnSc1
Marčuk	Marčuka	k1gFnPc2
•	•	k?
1979	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Egil	Egil	k1gMnSc1
Storholt	Storholt	k1gMnSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Kay	Kay	k1gFnSc1
Stenshjemmet	Stenshjemmet	k1gInSc1
•	•	k?
1981	#num#	k4
<g/>
:	:	kIx,
Amund	Amund	k1gInSc1
Sjø	Sjø	k1gInSc1
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Tomas	Tomas	k1gMnSc1
Gustafson	Gustafson	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1983	#num#	k4
<g/>
:	:	kIx,
Hilbert	Hilbert	k1gInSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Duim	Duim	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Hilbert	Hilbert	k1gInSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Duim	Duim	k1gInSc1
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Hein	Hein	k1gInSc1
Vergeer	Vergeer	k1gInSc1
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Hein	Hein	k1gInSc1
Vergeer	Vergeer	k1gInSc1
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Guljajev	Guljajev	k1gFnSc2
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Tomas	Tomas	k1gInSc1
Gustafson	Gustafson	k1gInSc1
•	•	k?
1989	#num#	k4
<g/>
:	:	kIx,
Leo	Leo	k1gMnSc1
Visser	Vissra	k1gFnPc2
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Bart	Bart	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Veldkamp	Veldkamp	k1gInSc1
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Johann	Johann	k1gInSc1
Olav	Olav	k1gInSc1
Koss	Koss	k1gInSc4
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Falko	Falko	k1gNnSc1
Zandstra	Zandstrum	k1gNnSc2
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Falko	Falko	k1gNnSc1
Zandstra	Zandstrum	k1gNnSc2
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
Ids	Ids	k1gFnSc1
Postma	Postma	k1gFnSc1
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Rintje	Rintj	k1gInSc2
Ritsma	Ritsma	k1gFnSc1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Dmitrij	Dmitrij	k1gFnSc1
Šepel	Šepel	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Jochem	Jochem	k?
Uytdehaage	Uytdehaage	k1gInSc1
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Gianni	Gianň	k1gMnSc5
Romme	Romm	k1gMnSc5
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Mark	Mark	k1gMnSc1
Tuitert	Tuitert	k1gMnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Jochem	Jochem	k?
Uytdehaage	Uytdehaage	k1gInSc1
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Enrico	Enrico	k6eAd1
Fabris	Fabris	k1gInSc1
•	•	k?
2007	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Skobrev	Skobrva	k1gFnPc2
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Blokhuijsen	Blokhuijsna	k1gFnPc2
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Sven	Sven	k1gInSc1
Kramer	Kramer	k1gInSc1
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
<g/>
:	:	kIx,
Patrick	Patrick	k1gMnSc1
Roest	Roest	k1gFnSc4
Šampionáty	šampionát	k1gInPc1
v	v	k7c6
letech	léto	k1gNnPc6
1891	#num#	k4
<g/>
,	,	kIx,
1892	#num#	k4
a	a	k8xC
1946	#num#	k4
byly	být	k5eAaImAgFnP
neoficiální	neoficiální	k2eAgFnPc1d1,k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
