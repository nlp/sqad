<s>
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
ochranný	ochranný	k2eAgInSc4d1	ochranný
symbol	symbol	k1gInSc4	symbol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
Ženevskou	ženevský	k2eAgFnSc7d1	Ženevská
úmluvou	úmluva	k1gFnSc7	úmluva
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
osudu	osud	k1gInSc2	osud
raněných	raněný	k1gMnPc2	raněný
a	a	k8xC	a
nemocných	mocný	k2eNgMnPc2d1	nemocný
příslušníků	příslušník	k1gMnPc2	příslušník
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
