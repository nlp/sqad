<s>
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
[	[	kIx(	[
<g/>
rolan	rolan	k1gMnSc1	rolan
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Clamecy	Clameca	k1gFnPc1	Clameca
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Vézelay	Vézelay	k1gInPc4	Vézelay
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
