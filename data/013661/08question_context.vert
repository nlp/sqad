<s>
Termodynamická	termodynamický	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
absolutní	absolutní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
teplota	teplota	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fyzikální	fyzikální	k2eAgFnSc1d1
stavová	stavový	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
dobře	dobře	k6eAd1
definovatelná	definovatelný	k2eAgFnSc1d1
pro	pro	k7c4
termodynamické	termodynamický	k2eAgInPc4d1
systémy	systém	k1gInPc4
ve	v	k7c6
stavu	stav	k1gInSc6
termodynamické	termodynamický	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgInSc4d1
s	s	k7c7
růstem	růst	k1gInSc7
vnitřní	vnitřní	k2eAgFnSc2d1
energie	energie	k1gFnSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>