<s>
Termodynamická	termodynamický	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
Termodynamická	termodynamický	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
absolutní	absolutní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
teplota	teplota	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fyzikální	fyzikální	k2eAgFnSc1d1
stavová	stavový	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
dobře	dobře	k6eAd1
definovatelná	definovatelný	k2eAgFnSc1d1
pro	pro	k7c4
termodynamické	termodynamický	k2eAgInPc4d1
systémy	systém	k1gInPc4
ve	v	k7c6
stavu	stav	k1gInSc6
termodynamické	termodynamický	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgInSc4d1
s	s	k7c7
růstem	růst	k1gInSc7
vnitřní	vnitřní	k2eAgFnSc2d1
energie	energie	k1gFnSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
nerovnost	nerovnost	k1gFnSc4
určuje	určovat	k5eAaImIp3nS
směr	směr	k1gInSc1
samovolného	samovolný	k2eAgMnSc2d1
(	(	kIx(
<g/>
tedy	tedy	k8xC
bez	bez	k7c2
konání	konání	k1gNnSc2
práce	práce	k1gFnSc2
<g/>
)	)	kIx)
přestupu	přestup	k1gInSc2
tepla	teplo	k1gNnSc2
od	od	k7c2
teplejšího	teplý	k2eAgInSc2d2
systému	systém	k1gInSc2
k	k	k7c3
systému	systém	k1gInSc3
chladnějšímu	chladný	k2eAgInSc3d2
<g/>
,	,	kIx,
uvedou	uvést	k5eAaPmIp3nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
do	do	k7c2
tepelného	tepelný	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
termodynamickém	termodynamický	k2eAgNnSc6d1
definování	definování	k1gNnSc6
teploty	teplota	k1gFnSc2
a	a	k8xC
vztahu	vztah	k1gInSc2
termodynamické	termodynamický	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
k	k	k7c3
ostatním	ostatní	k2eAgFnPc3d1
definicím	definice	k1gFnPc3
blíže	blízce	k6eAd2
pojednává	pojednávat	k5eAaImIp3nS
samostatný	samostatný	k2eAgInSc1d1
článek	článek	k1gInSc1
Teplota	teplota	k1gFnSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
jeho	jeho	k3xOp3gInSc1
oddíl	oddíl	k1gInSc1
Definice	definice	k1gFnSc2
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Značení	značení	k1gNnSc1
</s>
<s>
Symbol	symbol	k1gInSc1
veličiny	veličina	k1gFnSc2
<g/>
:	:	kIx,
T	T	kA
(	(	kIx(
<g/>
angl.	angl.	k?
temperature	temperatur	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	SI	kA
<g/>
:	:	kIx,
kelvin	kelvin	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
jednotky	jednotka	k1gFnSc2
K	K	kA
</s>
<s>
Další	další	k2eAgFnPc1d1
používané	používaný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
stupeň	stupeň	k1gInSc1
Rankina	Rankina	k1gMnSc1
°	°	k?
<g/>
R	R	kA
</s>
<s>
Měřící	měřící	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
</s>
<s>
teploměr	teploměr	k1gInSc4
kapalinový	kapalinový	k2eAgInSc4d1
(	(	kIx(
<g/>
rtuťový	rtuťový	k2eAgInSc4d1
<g/>
,	,	kIx,
lihový	lihový	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plynový	plynový	k2eAgInSc1d1
<g/>
,	,	kIx,
teploměr	teploměr	k1gInSc1
bimetalový	bimetalový	k2eAgInSc1d1
<g/>
,	,	kIx,
teploměr	teploměr	k1gInSc1
elektrický	elektrický	k2eAgInSc1d1
(	(	kIx(
<g/>
termoelektrický	termoelektrický	k2eAgInSc1d1
<g/>
,	,	kIx,
odporový	odporový	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
teploměr	teploměr	k1gInSc1
radiační	radiační	k2eAgInSc1d1
(	(	kIx(
<g/>
pyrometr	pyrometr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Teploměr	teploměr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
</s>
<s>
Barevná	barevný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4141118-3	4141118-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
3203	#num#	k4
</s>
