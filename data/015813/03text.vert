<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gFnSc2
KV	KV	kA
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugge	k1gNnSc1
Název	název	k1gInSc1
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gFnSc2
Koninklijke	Koninklijk	k1gFnSc2
Voetbalvereniging	Voetbalvereniging	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Belgie	Belgie	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Bruggy	Bruggy	k1gFnPc1
Založen	založit	k5eAaPmNgMnS
</s>
<s>
1891	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcb	_fcb	k1gInSc1
<g/>
1516	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
belgická	belgický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Breydel	Breydlo	k1gNnPc2
Stadion	stadion	k1gInSc4
Bruggy	Bruggy	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
29.042	29.042	k4
Vedení	vedení	k1gNnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Bart	Bart	k1gMnSc1
Verhaeghe	Verhaeghe	k1gNnSc2
Trenér	trenér	k1gMnSc1
</s>
<s>
Philippe	Philippat	k5eAaPmIp3nS
Clement	Clement	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gFnSc2
KV	KV	kA
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gFnSc2
KV	KV	kA
</s>
<s>
Nezaměňovat	zaměňovat	k5eNaImF
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
klubem	klub	k1gInSc7
z	z	k7c2
Brugg	Bruggy	k1gFnPc2
-	-	kIx~
Cercle	cercle	k1gInSc1
Brugge	Brugg	k1gInSc2
KSV	KSV	kA
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Breydel	Breydlo	k1gNnPc2
Stadion	stadion	k1gInSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
klubový	klubový	k2eAgInSc1d1
emblém	emblém	k1gInSc1
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gInSc2
KV	KV	kA
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
Club	club	k1gInSc1
Brugge	Brugge	k1gFnPc2
Koninklijke	Koninklijke	k1gFnPc2
Voetbalvereniging	Voetbalvereniging	k1gInSc1
-	-	kIx~
Klub	klub	k1gInSc1
bruggská	bruggský	k2eAgFnSc1d1
královská	královský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
jako	jako	k9
FC	FC	kA
Bruggy	Bruggy	k1gFnPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
belgický	belgický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Bruggy	Bruggy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
roku	rok	k1gInSc2
1891	#num#	k4
jako	jako	k8xC,k8xS
Brugsche	Brugsche	k1gFnSc3
Football	Footballum	k1gNnPc2
Club	club	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Domácím	domácí	k2eAgNnSc7d1
hřištěm	hřiště	k1gNnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Breydel	Breydlo	k1gNnPc2
Stadion	stadion	k1gInSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
29	#num#	k4
000	#num#	k4
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
sdílí	sdílet	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
městským	městský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
Cercle	cercle	k1gInSc4
Brugge	Brugg	k1gFnSc2
KSV	KSV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jediný	jediný	k2eAgInSc1d1
belgický	belgický	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
hrál	hrát	k5eAaImAgInS
finále	finále	k1gNnSc7
PMEZ	PMEZ	kA
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
podlehl	podlehnout	k5eAaPmAgMnS
anglickému	anglický	k2eAgMnSc3d1
Liverpoolu	Liverpool	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1891	#num#	k4
jako	jako	k8xC,k8xS
Brugsche	Brugsche	k1gInSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
část	část	k1gFnSc1
členů	člen	k1gInPc2
klubu	klub	k1gInSc2
odchází	odcházet	k5eAaImIp3nS
a	a	k8xC
zakládá	zakládat	k5eAaImIp3nS
Football	Football	k1gInSc1
Club	club	k1gInSc4
Brugeois	Brugeois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
se	se	k3xPyFc4
oba	dva	k4xCgInPc1
kluby	klub	k1gInPc1
slučují	slučovat	k5eAaImIp3nP
pod	pod	k7c7
názvem	název	k1gInSc7
Football	Football	k1gInSc1
Club	club	k1gInSc4
Brugeois	Brugeois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
slučuje	slučovat	k5eAaImIp3nS
s	s	k7c7
Vlaamsche	Vlaamsche	k1gNnSc7
Football	Football	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Bruges	Bruges	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
stěhuje	stěhovat	k5eAaImIp3nS
na	na	k7c4
stadion	stadion	k1gInSc4
De	De	k?
Klokke	Klokke	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
stává	stávat	k5eAaImIp3nS
mistrem	mistr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
se	se	k3xPyFc4
přejmenovává	přejmenovávat	k5eAaImIp3nS
na	na	k7c4
Royal	Royal	k1gInSc4
Football	Football	k1gInSc1
Club	club	k1gInSc1
Brugeois	Brugeois	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
</s>
<s>
Od	od	k7c2
návratu	návrat	k1gInSc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
už	už	k9
klub	klub	k1gInSc1
nikdy	nikdy	k6eAd1
nesestoupil	sestoupit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
belgický	belgický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
už	už	k9
patří	patřit	k5eAaImIp3nS
natrvalo	natrvalo	k6eAd1
k	k	k7c3
předním	přední	k2eAgMnPc3d1
týmům	tým	k1gInPc3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
se	se	k3xPyFc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c4
Club	club	k1gInSc4
Brugge	Brugge	k1gNnSc2
Koninklijke	Koninklijk	k1gFnSc2
Voetbalvereniging	Voetbalvereniging	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
po	po	k7c4
půl	půl	k1xP
století	století	k1gNnSc2
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
druhý	druhý	k4xOgInSc4
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ernst	Ernst	k1gMnSc1
Happel	Happel	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
-	-	kIx~
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgMnPc1d3
jsou	být	k5eAaImIp3nP
léta	léto	k1gNnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
trénoval	trénovat	k5eAaImAgMnS
Ernst	Ernst	k1gMnSc1
Happel	Happel	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
-	-	kIx~
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
přestěhoval	přestěhovat	k5eAaPmAgInS
na	na	k7c4
Olympiastadion	Olympiastadion	k1gInSc4
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Jan	Jan	k1gMnSc1
Breydelstadion	Breydelstadion	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
hraje	hrát	k5eAaImIp3nS
finále	finále	k1gNnSc4
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
přemožitelem	přemožitel	k1gMnSc7
je	být	k5eAaImIp3nS
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
hraje	hrát	k5eAaImIp3nS
finále	finále	k1gNnSc4
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
přemožitelem	přemožitel	k1gMnSc7
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Domácí	domácí	k1gMnPc1
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1919	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
–	–	k?
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1967	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
–	–	k?
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
-	-	kIx~
Finále	finále	k1gNnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
-	-	kIx~
Finále	finále	k1gNnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
1975	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
-	-	kIx~
Semifinále	semifinále	k1gNnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
-	-	kIx~
Semifinále	semifinále	k1gNnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
92	#num#	k4
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgMnPc1d1
k	k	k7c3
datu	datum	k1gNnSc3
<g/>
:	:	kIx,
19	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Eduard	Eduard	k1gMnSc1
Sobol	Sobol	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Éder	Éder	k1gInSc1
Balanta	Balant	k1gMnSc2
</s>
<s>
5	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Odilon	Odilon	k1gInSc1
Kossounou	Kossouna	k1gFnSc7
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Michael	Michael	k1gMnSc1
Krmenčík	Krmenčík	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Mbaye	Mbayus	k1gMnSc5
Diagne	Diagn	k1gMnSc5
(	(	kIx(
<g/>
na	na	k7c4
hostování	hostování	k1gNnSc4
z	z	k7c2
Galatasaraye	Galatasaray	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Krépin	Krépin	k1gMnSc1
Diatta	Diatta	k1gMnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
David	David	k1gMnSc1
Okereke	Okerek	k1gFnSc2
</s>
<s>
15	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Matej	Matej	k1gInSc1
Mitrović	Mitrović	k1gFnSc2
</s>
<s>
16	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Siebe	Siebat	k5eAaPmIp3nS
Schrijvers	Schrijvers	k1gInSc1
</s>
<s>
17	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Simon	Simon	k1gMnSc1
Deli	Del	k1gFnSc2
</s>
<s>
18	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Federico	Federico	k1gMnSc1
Ricca	Ricca	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Thibault	Thibault	k2eAgInSc1d1
Vlietinck	Vlietinck	k1gInSc1
</s>
<s>
20	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Hans	Hans	k1gMnSc1
Vanaken	Vanaken	k2eAgMnSc1d1
(	(	kIx(
<g/>
zástupce	zástupce	k1gMnSc4
kapitána	kapitán	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Ethan	ethan	k1gInSc1
Horvath	Horvatha	k1gFnPc2
</s>
<s>
25	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ruud	Ruud	k1gMnSc1
Vormer	Vormer	k1gMnSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
26	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Mats	Mats	k6eAd1
Rits	Rits	k1gInSc1
</s>
<s>
27	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Youssouph	Youssouph	k1gMnSc1
Badji	Badj	k1gMnSc3
</s>
<s>
33	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Nick	Nick	k6eAd1
Shinton	Shinton	k1gInSc1
</s>
<s>
35	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Percy	Percy	k1gInPc1
Tau	tau	k1gNnSc2
(	(	kIx(
<g/>
na	na	k7c6
hostování	hostování	k1gNnSc6
z	z	k7c2
Brightonu	Brighton	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
42	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Emmanuel	Emmanuel	k1gMnSc1
Dennis	Dennis	k1gFnSc2
</s>
<s>
44	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Brandon	Brandon	k1gNnSc1
Mechele	Mechel	k1gInSc2
</s>
<s>
77	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Clinton	Clinton	k1gMnSc1
Mata	mást	k5eAaImSgInS
</s>
<s>
80	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Loï	Loï	k6eAd1
Openda	Openda	k1gFnSc1
</s>
<s>
88	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Simon	Simon	k1gMnSc1
Mignolet	Mignolet	k1gInSc4
</s>
<s>
90	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Charles	Charles	k1gMnSc1
De	De	k?
Ketelaere	Ketelaer	k1gInSc5
</s>
<s>
92	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ignace	Ignace	k1gFnSc1
Van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Brempt	Brempt	k1gMnSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Raoul	Raoul	k1gInSc1
Lambert	Lambert	k1gInSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
-	-	kIx~
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Ceulemans	Ceulemansa	k1gFnPc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Franky	Franky	k1gInPc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Elst	Elst	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Belgium	Belgium	k1gNnSc1
-	-	kIx~
Overview	Overview	k1gFnSc1
of	of	k?
teams	teams	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
8	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
na	na	k7c4
transfermarkt	transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Club	club	k1gInSc1
Brugge	Brugg	k1gMnSc2
KV	KV	kA
–	–	k?
Detailed	Detailed	k1gInSc1
squad	squad	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
transfermarkt	transfermarkt	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Club	club	k1gInSc1
Brugge	Brugge	k1gNnSc4
KV	KV	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
na	na	k7c4
transfermarkt	transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
21	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
na	na	k7c6
weltfussballarchiv	weltfussballarchiva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Jupiler	Jupiler	k1gMnSc1
Pro	pro	k7c4
League	League	k1gFnSc4
–	–	k?
Belgická	belgický	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ročníky	ročník	k1gInPc1
<g/>
)	)	kIx)
Kluby	klub	k1gInPc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
RSC	RSC	kA
Anderlecht	Anderlecht	k1gMnSc1
•	•	k?
Cercle	cercle	k1gInSc1
Brugge	Brugg	k1gMnSc2
KSV	KSV	kA
•	•	k?
Club	club	k1gInSc1
Brugge	Brugg	k1gMnSc2
KV	KV	kA
•	•	k?
R.	R.	kA
Charleroi	Charlero	k1gFnSc2
SC	SC	kA
•	•	k?
KRC	KRC	kA
Genk	Genk	k1gInSc1
•	•	k?
KAA	KAA	kA
Gent	Gent	k1gInSc1
•	•	k?
KV	KV	kA
Kortrijk	Kortrijk	k1gInSc1
•	•	k?
KAS	kasa	k1gFnPc2
Eupen	Eupno	k1gNnPc2
•	•	k?
Royal	Royal	k1gMnSc1
Excel	Excel	kA
Mouscron	Mouscron	k1gMnSc1
•	•	k?
Royal	Royal	k1gMnSc1
Antwerp	Antwerp	k1gMnSc1
FC	FC	kA
•	•	k?
K.	K.	kA
Sint-Truidense	Sint-Truidense	k1gFnSc2
VV	VV	kA
•	•	k?
KV	KV	kA
Oostende	Oostend	k1gInSc5
•	•	k?
Standard	standard	k1gInSc1
Liè	Liè	k1gFnSc1
•	•	k?
Waasland-Beveren	Waasland-Beverna	k1gFnPc2
•	•	k?
SV	sv	kA
Zulte-Waregem	Zulte-Warego	k1gNnSc7
•	•	k?
KV	KV	kA
Mechelen	Mechelen	k2eAgInSc1d1
•	•	k?
Beerschot	Beerschot	k1gInSc1
Wilrijk	Wilrijk	k1gInSc1
•	•	k?
Oud-Heverlee	Oud-Heverlee	k1gInSc1
Leuven	Leuvna	k1gFnPc2
Sezóny	sezóna	k1gFnSc2
Jupiler	Jupiler	k1gInSc1
Pro	pro	k7c4
League	League	k1gFnSc4
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
<g/>
:	:	kIx,
Belgický	belgický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Belgický	belgický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
•	•	k?
Belgický	belgický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7743319-1	7743319-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2012000922	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
224133935	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2012000922	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Belgie	Belgie	k1gFnSc1
</s>
