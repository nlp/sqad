<s>
Sumerština	sumerština	k1gFnSc1	sumerština
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgInSc4d1	starověký
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Kolem	kolem	k7c2	kolem
1900	[number]	k4	1900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
akkadštinou	akkadština	k1gFnSc7	akkadština
<g/>
,	,	kIx,	,
jazykem	jazyk	k1gInSc7	jazyk
náboženským	náboženský	k2eAgInSc7d1	náboženský
a	a	k8xC	a
vědeckým	vědecký	k2eAgInSc7d1	vědecký
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c4	po
dvě	dva	k4xCgNnPc4	dva
další	další	k2eAgNnPc4d1	další
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Sumerština	sumerština	k1gFnSc1	sumerština
není	být	k5eNaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
jiným	jiný	k2eAgInSc7d1	jiný
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
například	například	k6eAd1	například
baskičtina	baskičtina	k1gFnSc1	baskičtina
mezi	mezi	k7c4	mezi
izolované	izolovaný	k2eAgInPc4d1	izolovaný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Sumerština	sumerština	k1gFnSc1	sumerština
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
známým	známý	k2eAgInSc7d1	známý
psaným	psaný	k2eAgInSc7d1	psaný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Používané	používaný	k2eAgNnSc4d1	používané
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc4d1	zvaná
klínové	klínový	k2eAgFnPc4d1	klínová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
převzaly	převzít	k5eAaPmAgInP	převzít
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
jako	jako	k8xC	jako
akkadština	akkadština	k1gFnSc1	akkadština
<g/>
,	,	kIx,	,
ugaritština	ugaritština	k1gFnSc1	ugaritština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
chetitština	chetitština	k1gFnSc1	chetitština
<g/>
.	.	kIx.	.
</s>
<s>
Sumerština	sumerština	k1gFnSc1	sumerština
je	být	k5eAaImIp3nS	být
aglutinační	aglutinační	k2eAgInSc4d1	aglutinační
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
jedné	jeden	k4xCgFnSc2	jeden
gramatické	gramatický	k2eAgFnSc2d1	gramatická
funkce	funkce	k1gFnSc2	funkce
požívá	požívat	k5eAaImIp3nS	požívat
jeden	jeden	k4xCgInSc4	jeden
afix	afix	k1gInSc4	afix
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
od	od	k7c2	od
sebe	se	k3xPyFc2	se
dají	dát	k5eAaPmIp3nP	dát
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
sumerština	sumerština	k1gFnSc1	sumerština
i	i	k8xC	i
ergativní	ergativní	k2eAgInSc1d1	ergativní
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
systému	systém	k1gInSc6	systém
pád	pád	k1gInSc1	pád
ergativ	ergativ	k1gInSc1	ergativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevovaly	objevovat	k5eAaImAgFnP	objevovat
se	se	k3xPyFc4	se
snahy	snaha	k1gFnSc2	snaha
přiřadit	přiřadit	k5eAaPmF	přiřadit
původem	původ	k1gInSc7	původ
sumerštinu	sumerština	k1gFnSc4	sumerština
snad	snad	k9	snad
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
známému	známý	k2eAgInSc3d1	známý
aglutinačnímu	aglutinační	k2eAgInSc3d1	aglutinační
jazyku	jazyk	k1gInSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c6	o
příbuznosti	příbuznost	k1gFnSc6	příbuznost
s	s	k7c7	s
baskičtinou	baskičtina	k1gFnSc7	baskičtina
nebo	nebo	k8xC	nebo
drávidskými	drávidský	k2eAgInPc7d1	drávidský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dohady	dohad	k1gInPc1	dohad
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
naprosto	naprosto	k6eAd1	naprosto
nepodložené	podložený	k2eNgInPc1d1	nepodložený
a	a	k8xC	a
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
komunita	komunita	k1gFnSc1	komunita
je	on	k3xPp3gNnSc4	on
neuznává	uznávat	k5eNaImIp3nS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
zastoupení	zastoupení	k1gNnSc2	zastoupení
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
nostratické	nostratický	k2eAgFnSc6d1	nostratický
jazykové	jazykový	k2eAgFnSc6d1	jazyková
rodině	rodina	k1gFnSc6	rodina
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
dené-kavkazskými	denéavkazský	k2eAgInPc7d1	dené-kavkazský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
existence	existence	k1gFnSc1	existence
skupin	skupina	k1gFnPc2	skupina
samotných	samotný	k2eAgFnPc2d1	samotná
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
