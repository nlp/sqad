<s>
Sumerština	sumerština	k1gFnSc1	sumerština
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgInSc4d1	starověký
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
