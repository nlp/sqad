<s>
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
objevem	objev	k1gInSc7	objev
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
teorii	teorie	k1gFnSc4	teorie
samoplození	samoplození	k1gNnSc2	samoplození
</s>
