<p>
<s>
Barbados	Barbados	k1gMnSc1	Barbados
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgNnPc4d1	ostrovní
království	království	k1gNnPc4	království
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
hranici	hranice	k1gFnSc6	hranice
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
Návětrné	návětrný	k2eAgInPc4d1	návětrný
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gInSc4	on
izolovaně	izolovaně	k6eAd1	izolovaně
umístěný	umístěný	k2eAgInSc4d1	umístěný
a	a	k8xC	a
korálovými	korálový	k2eAgInPc7d1	korálový
útesy	útes	k1gInPc7	útes
obklopený	obklopený	k2eAgInSc4d1	obklopený
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
168	[number]	k4	168
km	km	kA	km
od	od	k7c2	od
ostrovního	ostrovní	k2eAgInSc2d1	ostrovní
státu	stát	k1gInSc2	stát
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnPc1	grenadina
a	a	k8xC	a
400	[number]	k4	400
km	km	kA	km
od	od	k7c2	od
státu	stát	k1gInSc2	stát
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
34	[number]	k4	34
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
23	[number]	k4	23
km	km	kA	km
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
zabírající	zabírající	k2eAgFnSc4d1	zabírající
plochu	plocha	k1gFnSc4	plocha
432	[number]	k4	432
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
432	[number]	k4	432
km2	km2	k4	km2
a	a	k8xC	a
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
jej	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
nížiny	nížina	k1gFnPc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gInSc1	Mount
Hillaby	Hillaba	k1gFnSc2	Hillaba
(	(	kIx(	(
<g/>
337	[number]	k4	337
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Původ	původ	k1gInSc1	původ
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
nevulkanický	vulkanický	k2eNgInSc1d1	vulkanický
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
horninu	hornina	k1gFnSc4	hornina
tvoří	tvořit	k5eAaImIp3nS	tvořit
vápenec	vápenec	k1gInSc1	vápenec
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
činností	činnost	k1gFnSc7	činnost
korálů	korál	k1gInPc2	korál
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
<g/>
,	,	kIx,	,
nadzvedaném	nadzvedaný	k2eAgInSc6d1	nadzvedaný
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
podsouvání	podsouvání	k1gNnSc2	podsouvání
Jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
desky	deska	k1gFnSc2	deska
pod	pod	k7c7	pod
Karibskou	karibský	k2eAgFnSc7d1	karibská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
pasáty	pasát	k1gInPc1	pasát
udržují	udržovat	k5eAaImIp3nP	udržovat
relativně	relativně	k6eAd1	relativně
mírné	mírný	k2eAgFnPc1d1	mírná
teploty	teplota	k1gFnPc1	teplota
nad	nad	k7c7	nad
ostrovem	ostrov	k1gInSc7	ostrov
a	a	k8xC	a
přinášejí	přinášet	k5eAaImIp3nP	přinášet
sezónní	sezónní	k2eAgInPc4d1	sezónní
deště	dešť	k1gInPc4	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
pokryv	pokryv	k1gInSc1	pokryv
tvoří	tvořit	k5eAaImIp3nS	tvořit
tropický	tropický	k2eAgInSc4d1	tropický
deštný	deštný	k2eAgInSc4d1	deštný
les	les	k1gInSc4	les
a	a	k8xC	a
mangrovníkové	mangrovníkový	k2eAgInPc4d1	mangrovníkový
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
vymýceny	vymýtit	k5eAaPmNgInP	vymýtit
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
plantážemi	plantáž	k1gFnPc7	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
pastvinami	pastvina	k1gFnPc7	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
svázaný	svázaný	k2eAgInSc1d1	svázaný
průmysl	průmysl	k1gInSc1	průmysl
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
ostrov	ostrov	k1gInSc4	ostrov
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
živobytí	živobytí	k1gNnSc2	živobytí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přidal	přidat	k5eAaPmAgInS	přidat
i	i	k9	i
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Barbados	Barbados	k1gInSc1	Barbados
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
ostrovů	ostrov	k1gInPc2	ostrov
tzv.	tzv.	kA	tzv.
Západní	západní	k2eAgFnSc2d1	západní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
ostrov	ostrov	k1gInSc1	ostrov
Malých	Malých	k2eAgFnPc2d1	Malých
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rovinný	rovinný	k2eAgMnSc1d1	rovinný
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
západními	západní	k2eAgMnPc7d1	západní
sousedy	soused	k1gMnPc7	soused
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
ostrova	ostrov	k1gInSc2	ostrov
tvoří	tvořit	k5eAaImIp3nP	tvořit
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
vyvýšených	vyvýšený	k2eAgFnPc2d1	vyvýšená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Hillaby	Hillaba	k1gFnSc2	Hillaba
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
Saint	Sainta	k1gFnPc2	Sainta
Andrew	Andrew	k1gFnSc2	Andrew
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
zdroje	zdroj	k1gInPc1	zdroj
udávají	udávat	k5eAaImIp3nP	udávat
336	[number]	k4	336
<g/>
–	–	k?	–
<g/>
340	[number]	k4	340
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Původ	původ	k1gInSc1	původ
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
nevulkanický	vulkanický	k2eNgInSc1d1	vulkanický
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nadzvednutím	nadzvednutí	k1gNnSc7	nadzvednutí
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
podsouvání	podsouvání	k1gNnSc2	podsouvání
Jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
desky	deska	k1gFnSc2	deska
pod	pod	k7c7	pod
Karibskou	karibský	k2eAgFnSc7d1	karibská
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
geologicky	geologicky	k6eAd1	geologicky
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Trinidad	Trinidad	k1gInSc4	Trinidad
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
horninu	hornina	k1gFnSc4	hornina
vápenec	vápenec	k1gInSc4	vápenec
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
činností	činnost	k1gFnSc7	činnost
korálů	korál	k1gInPc2	korál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
pasáty	pasát	k1gInPc1	pasát
udržují	udržovat	k5eAaImIp3nP	udržovat
relativně	relativně	k6eAd1	relativně
mírné	mírný	k2eAgFnPc1d1	mírná
teploty	teplota	k1gFnPc1	teplota
nad	nad	k7c7	nad
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
června	červen	k1gInSc2	červen
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
činí	činit	k5eAaImIp3nP	činit
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
2300	[number]	k4	2300
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c4	v
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
tj.	tj.	kA	tj.
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
května	květen	k1gInSc2	květen
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
hodnoty	hodnota	k1gFnPc4	hodnota
mezi	mezi	k7c7	mezi
21	[number]	k4	21
až	až	k9	až
31	[number]	k4	31
°	°	k?	°
<g/>
C.	C.	kA	C.
Mezi	mezi	k7c7	mezi
měsíci	měsíc	k1gInSc6	měsíc
červen	červen	k1gInSc1	červen
a	a	k8xC	a
listopad	listopad	k1gInSc1	listopad
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
23	[number]	k4	23
až	až	k9	až
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Podle	podle	k7c2	podle
Köppenovy	Köppenův	k2eAgFnSc2d1	Köppenova
klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
spadá	spadat	k5eAaImIp3nS	spadat
Barbados	Barbados	k1gInSc1	Barbados
do	do	k7c2	do
tropického	tropický	k2eAgNnSc2d1	tropické
monsunového	monsunový	k2eAgNnSc2d1	monsunový
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mírné	mírný	k2eAgInPc1d1	mírný
větry	vítr	k1gInPc1	vítr
vanoucí	vanoucí	k2eAgFnSc2d1	vanoucí
12	[number]	k4	12
–	–	k?	–
16	[number]	k4	16
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
ostrov	ostrov	k1gInSc4	ostrov
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc4	klima
spíše	spíše	k9	spíše
mírně	mírně	k6eAd1	mírně
tropické	tropický	k2eAgFnPc1d1	tropická
<g/>
.	.	kIx.	.
<g/>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
sužován	sužovat	k5eAaImNgInS	sužovat
tropickými	tropický	k2eAgFnPc7d1	tropická
bouřemi	bouř	k1gFnPc7	bouř
a	a	k8xC	a
hurikány	hurikán	k1gInPc7	hurikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
pokryv	pokryv	k1gInSc1	pokryv
tvoří	tvořit	k5eAaImIp3nS	tvořit
tropický	tropický	k2eAgInSc4d1	tropický
deštný	deštný	k2eAgInSc4d1	deštný
les	les	k1gInSc4	les
a	a	k8xC	a
mangrovníkové	mangrovníkový	k2eAgInPc4d1	mangrovníkový
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
vymýceny	vymýtit	k5eAaPmNgInP	vymýtit
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
plantážemi	plantáž	k1gFnPc7	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
pastvinami	pastvina	k1gFnPc7	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
tvoří	tvořit	k5eAaImIp3nS	tvořit
37,2	[number]	k4	37,2
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zalesněno	zalesněn	k2eAgNnSc1d1	zalesněno
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
za	za	k7c4	za
původní	původní	k2eAgInSc4d1	původní
les	les	k1gInSc4	les
lze	lze	k6eAd1	lze
však	však	k9	však
považovat	považovat	k5eAaImF	považovat
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
20	[number]	k4	20
ha	ha	kA	ha
porostu	porost	k1gInSc2	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgMnSc1d1	objeven
Portugalci	Portugalec	k1gMnPc1	Portugalec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgInS	dostat
zřejmě	zřejmě	k6eAd1	zřejmě
podle	podle	k7c2	podle
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
liján	lijána	k1gFnPc2	lijána
fíkovníků	fíkovník	k1gInPc2	fíkovník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
vousy	vous	k1gInPc4	vous
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
je	být	k5eAaImIp3nS	být
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
sem	sem	k6eAd1	sem
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
doplulo	doplout	k5eAaPmAgNnS	doplout
prvních	první	k4xOgNnPc6	první
80	[number]	k4	80
osadníků	osadník	k1gMnPc2	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Barbados	Barbados	k1gInSc1	Barbados
měl	mít	k5eAaImAgInS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
House	house	k1gNnSc1	house
of	of	k?	of
Assembly	Assembly	k1gFnPc2	Assembly
<g/>
,	,	kIx,	,
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1639	[number]	k4	1639
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
nejstarší	starý	k2eAgFnSc1d3	nejstarší
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
ostrov	ostrov	k1gInSc1	ostrov
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
a	a	k8xC	a
strašlivě	strašlivě	k6eAd1	strašlivě
zpustošil	zpustošit	k5eAaPmAgInS	zpustošit
Velký	velký	k2eAgInSc1d1	velký
hurikán	hurikán	k1gInSc1	hurikán
<g/>
,	,	kIx,	,
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
4500	[number]	k4	4500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
získává	získávat	k5eAaImIp3nS	získávat
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958-1962	[number]	k4	1958-1962
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnPc2	součást
federace	federace	k1gFnSc2	federace
Západoindických	západoindický	k2eAgInPc2d1	západoindický
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
získal	získat	k5eAaPmAgInS	získat
Barbados	Barbados	k1gInSc1	Barbados
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Barbados	Barbados	k1gInSc1	Barbados
je	být	k5eAaImIp3nS	být
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
zemí	zem	k1gFnSc7	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Státoprávní	státoprávní	k2eAgNnSc1d1	státoprávní
uspořádání	uspořádání	k1gNnSc1	uspořádání
Barbadosu	Barbados	k1gInSc2	Barbados
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zastupována	zastupován	k2eAgFnSc1d1	zastupována
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
guvernér	guvernér	k1gMnSc1	guvernér
dává	dávat	k5eAaImIp3nS	dávat
např.	např.	kA	např.
souhlas	souhlas	k1gInSc1	souhlas
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
předloženého	předložený	k2eAgInSc2d1	předložený
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
;	;	kIx,	;
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
11	[number]	k4	11
farností	farnost	k1gFnPc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Barbados	Barbados	k1gInSc1	Barbados
je	být	k5eAaImIp3nS	být
zapojen	zapojit	k5eAaPmNgInS	zapojit
do	do	k7c2	do
několika	několik	k4yIc2	několik
regionálních	regionální	k2eAgFnPc2d1	regionální
karibských	karibský	k2eAgFnPc2d1	karibská
organizací	organizace	k1gFnPc2	organizace
např.	např.	kA	např.
Karibské	karibský	k2eAgNnSc1d1	Karibské
společenství	společenství	k1gNnSc1	společenství
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Regionální	regionální	k2eAgInSc1d1	regionální
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Barbados	Barbados	k1gInSc1	Barbados
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
11	[number]	k4	11
samosprávních	samosprávní	k2eAgFnPc2d1	samosprávní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
George	Georg	k1gFnSc2	Georg
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Lucy	Luca	k1gFnSc2	Luca
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Philip	Philip	k1gMnSc1	Philip
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
St.	st.	kA	st.
George	Georg	k1gFnSc2	Georg
a	a	k8xC	a
St.	st.	kA	st.
Thomas	Thomas	k1gMnSc1	Thomas
,	,	kIx,	,
které	který	k3yIgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pobřežními	pobřežní	k2eAgFnPc7d1	pobřežní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
30.11	[number]	k4	30.11
<g/>
.1966	.1966	k4	.1966
<g/>
-	-	kIx~	-
<g/>
18.5	[number]	k4	18.5
<g/>
.1967	.1967	k4	.1967
–	–	k?	–
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Montague	Montague	k1gFnPc2	Montague
Stow	Stow	k1gMnSc1	Stow
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
18.5	[number]	k4	18.5
<g/>
.1967	.1967	k4	.1967
<g/>
-	-	kIx~	-
<g/>
9.8	[number]	k4	9.8
<g/>
.1976	.1976	k4	.1976
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Arleigh	Arleigha	k1gFnPc2	Arleigha
Winston	Winston	k1gInSc1	Winston
Scott	Scott	k1gMnSc1	Scott
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
9.8	[number]	k4	9.8
<g/>
.1976	.1976	k4	.1976
<g/>
-	-	kIx~	-
<g/>
17.11	[number]	k4	17.11
<g/>
.1976	.1976	k4	.1976
–	–	k?	–
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
Randolph	Randolph	k1gMnSc1	Randolph
Douglas	Douglas	k1gMnSc1	Douglas
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
17.11	[number]	k4	17.11
<g/>
.1976	.1976	k4	.1976
<g/>
-	-	kIx~	-
<g/>
9.1	[number]	k4	9.1
<g/>
.1984	.1984	k4	.1984
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Deighton	Deighton	k1gInSc4	Deighton
Harcourt	Harcourt	k1gInSc1	Harcourt
Lisle	Lisle	k1gInSc1	Lisle
Ward	Ward	k1gInSc4	Ward
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
10.1	[number]	k4	10.1
<g/>
.1984	.1984	k4	.1984
<g/>
-	-	kIx~	-
<g/>
24.2	[number]	k4	24.2
<g/>
.1984	.1984	k4	.1984
–	–	k?	–
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
Randolph	Randolph	k1gMnSc1	Randolph
Douglas	Douglas	k1gMnSc1	Douglas
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
24.2	[number]	k4	24.2
<g/>
.1984	.1984	k4	.1984
<g/>
-	-	kIx~	-
<g/>
6.6	[number]	k4	6.6
<g/>
.1990	.1990	k4	.1990
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Hugh	Hugh	k1gMnSc1	Hugh
Worrell	Worrell	k1gMnSc1	Worrell
Springer	Springer	k1gMnSc1	Springer
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
6.6	[number]	k4	6.6
<g/>
.1990	.1990	k4	.1990
<g/>
-	-	kIx~	-
<g/>
19.12	[number]	k4	19.12
<g/>
.1995	.1995	k4	.1995
–	–	k?	–
Dame	Dame	k1gFnSc1	Dame
Ruth	Ruth	k1gFnSc1	Ruth
Nita	Nita	k1gFnSc1	Nita
Barrowová	Barrowová	k1gFnSc1	Barrowová
–	–	k?	–
generální	generální	k2eAgFnSc1d1	generální
guvernérka	guvernérka	k1gFnSc1	guvernérka
</s>
</p>
<p>
<s>
19.12	[number]	k4	19.12
<g/>
.1995	.1995	k4	.1995
<g/>
-	-	kIx~	-
<g/>
1.6	[number]	k4	1.6
<g/>
.1996	.1996	k4	.1996
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Denys	Denys	k1gInSc4	Denys
Ambrose	Ambrosa	k1gFnSc3	Ambrosa
Williams	Williams	k1gInSc1	Williams
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
1.6	[number]	k4	1.6
<g/>
.1996	.1996	k4	.1996
<g/>
-	-	kIx~	-
<g/>
31.10	[number]	k4	31.10
<g/>
.2011	.2011	k4	.2011
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Clifford	Clifford	k1gMnSc1	Clifford
Straughn	Straughna	k1gFnPc2	Straughna
Husbands	Husbands	k1gInSc4	Husbands
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
1.11	[number]	k4	1.11
<g/>
.2011	.2011	k4	.2011
<g/>
-	-	kIx~	-
<g/>
30.5	[number]	k4	30.5
<g/>
.2012	.2012	k4	.2012
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Elliott	Elliott	k1gMnSc1	Elliott
Belgrave	Belgrav	k1gInSc5	Belgrav
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
30.5	[number]	k4	30.5
<g/>
.2012	.2012	k4	.2012
<g/>
-	-	kIx~	-
<g/>
1.6	[number]	k4	1.6
<g/>
.2012	.2012	k4	.2012
–	–	k?	–
Dame	Dame	k1gFnSc1	Dame
Sandra	Sandra	k1gFnSc1	Sandra
Masonová	Masonová	k1gFnSc1	Masonová
–	–	k?	–
úřadující	úřadující	k2eAgFnSc1d1	úřadující
generální	generální	k2eAgFnSc1d1	generální
guvernérka	guvernérka	k1gFnSc1	guvernérka
</s>
</p>
<p>
<s>
1.6	[number]	k4	1.6
<g/>
.2012	.2012	k4	.2012
<g/>
-	-	kIx~	-
<g/>
30.6	[number]	k4	30.6
<g/>
.2017	.2017	k4	.2017
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Elliott	Elliott	k1gMnSc1	Elliott
Belgrave	Belgrav	k1gInSc5	Belgrav
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
1.7	[number]	k4	1.7
<g/>
.2017	.2017	k4	.2017
<g/>
-	-	kIx~	-
<g/>
8.1	[number]	k4	8.1
<g/>
.2018	.2018	k4	.2018
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Philip	Philip	k1gMnSc1	Philip
Greaves	Greaves	k1gMnSc1	Greaves
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
</s>
</p>
<p>
<s>
od	od	k7c2	od
8.1	[number]	k4	8.1
<g/>
.2018	.2018	k4	.2018
–	–	k?	–
Dame	Dame	k1gFnSc1	Dame
Sandra	Sandra	k1gFnSc1	Sandra
Masonová	Masonová	k1gFnSc1	Masonová
–	–	k?	–
generální	generální	k2eAgFnSc1d1	generální
guvernérka	guvernérka	k1gFnSc1	guvernérka
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
286	[number]	k4	286
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
90	[number]	k4	90
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asiaté	asiat	k1gMnPc1	asiat
a	a	k8xC	a
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
Barbadosu	Barbados	k1gInSc2	Barbados
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
státní	státní	k2eAgFnSc7d1	státní
správou	správa	k1gFnSc7	správa
a	a	k8xC	a
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
službách	služba	k1gFnPc6	služba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
britské	britský	k2eAgFnSc3d1	britská
angličtině	angličtina	k1gFnSc3	angličtina
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
výslovnost	výslovnost	k1gFnSc1	výslovnost
a	a	k8xC	a
hláskování	hláskování	k1gNnSc1	hláskování
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
barbadoské	barbadoský	k2eAgFnSc2d1	barbadoská
angličtiny	angličtina	k1gFnSc2	angličtina
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Bajan	bajan	k1gMnSc1	bajan
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
angličtiny	angličtina	k1gFnSc2	angličtina
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
v	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
neformální	formální	k2eNgFnSc6d1	neformální
mluvě	mluva	k1gFnSc6	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Bajan	bajan	k1gMnSc1	bajan
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
ostatními	ostatní	k2eAgInPc7d1	ostatní
karibskými	karibský	k2eAgInPc7d1	karibský
dialekty	dialekt	k1gInPc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
anglického	anglický	k2eAgMnSc4d1	anglický
mluvčího	mluvčí	k1gMnSc4	mluvčí
problém	problém	k1gInSc4	problém
tomuto	tento	k3xDgNnSc3	tento
nářečí	nářečí	k1gNnSc3	nářečí
porozumět	porozumět	k5eAaPmF	porozumět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
protestanti	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
67	[number]	k4	67
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
anglikánům	anglikán	k1gMnPc3	anglikán
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
letniční	letniční	k2eAgInPc4d1	letniční
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
metodisté	metodista	k1gMnPc1	metodista
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
17	[number]	k4	17
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
nehlásí	hlásit	k5eNaImIp3nS	hlásit
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
náboženských	náboženský	k2eAgInPc2d1	náboženský
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
vytyčuje	vytyčovat	k5eAaImIp3nS	vytyčovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
diecézi	diecéze	k1gFnSc3	diecéze
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
biskup	biskup	k1gMnSc1	biskup
Barbadosu	Barbados	k1gInSc2	Barbados
<g/>
,	,	kIx,	,
současným	současný	k2eAgMnPc3d1	současný
je	on	k3xPp3gNnPc4	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
John	John	k1gMnSc1	John
Walder	Walder	k1gMnSc1	Walder
Dunlop	Dunlop	k1gInSc4	Dunlop
Holder	Holdra	k1gFnPc2	Holdra
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
i	i	k8xC	i
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
diecéze	diecéze	k1gFnSc1	diecéze
Bridgetown	Bridgetowna	k1gFnPc2	Bridgetowna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Provincie	provincie	k1gFnSc2	provincie
Port	port	k1gInSc1	port
of	of	k?	of
Spain	Spain	k1gInSc1	Spain
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
biskup	biskup	k1gMnSc1	biskup
zasedá	zasedat	k5eAaImIp3nS	zasedat
v	v	k7c6	v
Antilské	antilský	k2eAgFnSc6d1	antilská
biskupské	biskupský	k2eAgFnSc6d1	biskupská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
biskupský	biskupský	k2eAgInSc1d1	biskupský
stolec	stolec	k1gInSc1	stolec
neobsazen	obsazen	k2eNgInSc1d1	neobsazen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
je	být	k5eAaImIp3nS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
export	export	k1gInSc1	export
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
elektrozařízení	elektrozařízení	k1gNnSc1	elektrozařízení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
zisky	zisk	k1gInPc1	zisk
plynou	plynout	k5eAaImIp3nP	plynout
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
bílé	bílý	k2eAgFnPc4d1	bílá
korálové	korálový	k2eAgFnPc4d1	korálová
pláže	pláž	k1gFnPc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
zisk	zisk	k1gInSc1	zisk
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnSc3	osobnost
Barbadosu	Barbados	k1gInSc2	Barbados
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgMnPc1d1	národní
hrdinové	hrdina	k1gMnPc1	hrdina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1998	[number]	k4	1998
přijal	přijmout	k5eAaPmAgInS	přijmout
Parlament	parlament	k1gInSc4	parlament
Barbadosu	Barbados	k1gInSc2	Barbados
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
zavedl	zavést	k5eAaPmAgInS	zavést
Řád	řád	k1gInSc1	řád
národních	národní	k2eAgMnPc2d1	národní
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
Den	dna	k1gFnPc2	dna
národních	národní	k2eAgMnPc2d1	národní
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
připadající	připadající	k2eAgInSc4d1	připadající
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zároveň	zároveň	k6eAd1	zároveň
určil	určit	k5eAaPmAgInS	určit
deset	deset	k4xCc4	deset
prvních	první	k4xOgInPc2	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediných	jediný	k2eAgMnPc2d1	jediný
nositelů	nositel	k1gMnPc2	nositel
řádu	řád	k1gInSc2	řád
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bussa	Bussa	k1gFnSc1	Bussa
(	(	kIx(	(
<g/>
†	†	k?	†
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
povstání	povstání	k1gNnSc2	povstání
barbadoských	barbadoský	k2eAgMnPc2d1	barbadoský
otroků	otrok	k1gMnPc2	otrok
</s>
</p>
<p>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Ann	Ann	k1gMnSc1	Ann
Gill	Gill	k1gMnSc1	Gill
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
-	-	kIx~	-
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metodistická	metodistický	k2eAgFnSc1d1	metodistická
aktivistka	aktivistka	k1gFnSc1	aktivistka
</s>
</p>
<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Jackman	Jackman	k1gMnSc1	Jackman
Prescod	Prescoda	k1gFnPc2	Prescoda
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
barbadoský	barbadoský	k2eAgMnSc1d1	barbadoský
poslanec	poslanec	k1gMnSc1	poslanec
s	s	k7c7	s
černošskými	černošský	k2eAgInPc7d1	černošský
předky	předek	k1gInPc7	předek
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Duncan	Duncan	k1gMnSc1	Duncan
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neal	Neal	k1gInSc4	Neal
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Grantley	Grantle	k2eAgFnPc1d1	Grantle
Herbert	Herbert	k1gInSc4	Herbert
Adams	Adamsa	k1gFnPc2	Adamsa
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
premiér	premiéra	k1gFnPc2	premiéra
Barbadosu	Barbados	k1gInSc2	Barbados
a	a	k8xC	a
první	první	k4xOgFnSc1	první
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
premiér	premiér	k1gMnSc1	premiér
Západoindické	západoindický	k2eAgFnSc2d1	Západoindická
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
Clement	Clement	k1gMnSc1	Clement
Osbourne	Osbourn	k1gInSc5	Osbourn
Payne	Payn	k1gInSc5	Payn
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
odborového	odborový	k2eAgNnSc2d1	odborové
hnutí	hnutí	k1gNnSc2	hnutí
na	na	k7c6	na
Barbadosu	Barbados	k1gInSc6	Barbados
</s>
</p>
<p>
<s>
Hugh	Hugh	k1gMnSc1	Hugh
Worrell	Worrell	k1gMnSc1	Worrell
Springer	Springer	k1gMnSc1	Springer
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
odborář	odborář	k1gMnSc1	odborář
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Leslie	Leslie	k1gFnSc2	Leslie
Walcott	Walcott	k1gMnSc1	Walcott
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborář	odborář	k1gMnSc1	odborář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
</s>
</p>
<p>
<s>
Errol	Errol	k1gInSc1	Errol
Walton	Walton	k1gInSc1	Walton
Barrow	Barrow	k1gFnSc1	Barrow
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
premiér	premiér	k1gMnSc1	premiér
nezávislého	závislý	k2eNgInSc2d1	nezávislý
Barbadosu	Barbados	k1gInSc2	Barbados
</s>
</p>
<p>
<s>
Garfield	Garfield	k1gInSc1	Garfield
St	St	kA	St
Auburn	Auburn	k1gInSc1	Auburn
Sobers	Sobers	k1gInSc1	Sobers
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
hráč	hráč	k1gMnSc1	hráč
kriketu	kriket	k1gInSc2	kriket
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Worrell	Worrell	k1gMnSc1	Worrell
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
kriketu	kriket	k1gInSc2	kriket
</s>
</p>
<p>
<s>
Everton	Everton	k1gInSc1	Everton
DeCourcey	DeCourcea	k1gFnSc2	DeCourcea
Weekes	Weekesa	k1gFnPc2	Weekesa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
kriketu	kriket	k1gInSc2	kriket
</s>
</p>
<p>
<s>
George	George	k1gInSc1	George
Lamming	Lamming	k1gInSc1	Lamming
(	(	kIx(	(
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Austin	Austin	k2eAgInSc1d1	Austin
Clarke	Clarke	k1gInSc1	Clarke
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
</s>
</p>
<p>
<s>
Robyn	Robyn	k1gInSc1	Robyn
Rihanna	Rihann	k1gMnSc2	Rihann
Fenty	Fenta	k1gMnSc2	Fenta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Cox	Cox	k1gMnSc1	Cox
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
557	[number]	k4	557
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barbados	Barbadosa	k1gFnPc2	Barbadosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Barbados	Barbadosa	k1gFnPc2	Barbadosa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Barbados	Barbados	k1gInSc1	Barbados
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Barbados	Barbados	k1gInSc1	Barbados
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-08-12	[number]	k4	2011-08-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Barbados	Barbados	k1gInSc1	Barbados
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-31	[number]	k4	2011-01-31
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JACKSON	JACKSON	kA	JACKSON
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Barbados	Barbados	k1gInSc1	Barbados
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
