<p>
<s>
PhDr.	PhDr.	kA	PhDr.
Pavel	Pavel	k1gMnSc1	Pavel
Bergmann	Bergmann	k1gMnSc1	Bergmann
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Jana	Jan	k1gMnSc2	Jan
Patočky	Patočka	k1gMnSc2	Patočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Bergmanna	Bergmanna	k1gFnSc1	Bergmanna
a	a	k8xC	a
Karoliny	Karolinum	k1gNnPc7	Karolinum
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
Steinové	Steinové	k2eAgFnSc1d1	Steinové
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
poručníkem	poručník	k1gMnSc7	poručník
chlapce	chlapec	k1gMnSc4	chlapec
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
stal	stát	k5eAaPmAgMnS	stát
manžel	manžel	k1gMnSc1	manžel
její	její	k3xOp3gFnSc2	její
starší	starý	k2eAgFnSc2d2	starší
sestry	sestra	k1gFnSc2	sestra
Luisy	Luisa	k1gFnSc2	Luisa
Otta	Otta	k1gMnSc1	Otta
Boitler	Boitler	k1gMnSc1	Boitler
<g/>
.	.	kIx.	.
</s>
<s>
Vychováván	vychovávat	k5eAaImNgInS	vychovávat
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
svého	svůj	k3xOyFgMnSc2	svůj
dědečka	dědeček	k1gMnSc2	dědeček
Mořice	Mořic	k1gMnSc2	Mořic
Steina	Stein	k1gMnSc2	Stein
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
patřily	patřit	k5eAaImAgFnP	patřit
bývalé	bývalý	k2eAgInPc4d1	bývalý
valdštejnské	valdštejnský	k2eAgInPc4d1	valdštejnský
statky	statek	k1gInPc4	statek
v	v	k7c6	v
Zabrušanech	Zabrušan	k1gInPc6	Zabrušan
a	a	k8xC	a
Všechlapech	Všechlap	k1gInPc6	Všechlap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
záboru	zábor	k1gInSc6	zábor
Sudet	Sudety	k1gFnPc2	Sudety
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
rodina	rodina	k1gFnSc1	rodina
nucena	nutit	k5eAaImNgFnS	nutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Pavel	Pavel	k1gMnSc1	Pavel
Bergmann	Bergmann	k1gMnSc1	Bergmann
deportován	deportovat	k5eAaBmNgMnS	deportovat
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1943	[number]	k4	1943
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc6	Auschwitz-Birkenaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
pochodu	pochod	k1gInSc3	pochod
smrti	smrt	k1gFnSc2	smrt
do	do	k7c2	do
Mauthausenu	Mauthausen	k1gInSc2	Mauthausen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
a	a	k8xC	a
nezbytném	nezbytný	k2eAgNnSc6d1	nezbytný
léčení	léčení	k1gNnSc6	léčení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
války	válka	k1gFnSc2	válka
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
tyfem	tyf	k1gInSc7	tyf
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
studentem	student	k1gMnSc7	student
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Duchcově	Duchcův	k2eAgNnSc6d1	Duchcův
a	a	k8xC	a
po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
katedře	katedra	k1gFnSc6	katedra
historie	historie	k1gFnSc2	historie
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
sovětských	sovětský	k2eAgNnPc2d1	sovětské
okupačních	okupační	k2eAgNnPc2d1	okupační
vojsk	vojsko	k1gNnPc2	vojsko
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
dána	dán	k2eAgFnSc1d1	dána
výpověď	výpověď	k1gFnSc1	výpověď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
disidentskými	disidentský	k2eAgFnPc7d1	disidentská
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
signatářem	signatář	k1gMnSc7	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
spoluzakládal	spoluzakládat	k5eAaImAgMnS	spoluzakládat
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
a	a	k8xC	a
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
jako	jako	k8xS	jako
zprostředkovatel	zprostředkovatel	k1gMnSc1	zprostředkovatel
česko-německých	českoěmecký	k2eAgInPc2d1	česko-německý
i	i	k8xC	i
česko-izraelských	českozraelský	k2eAgInPc2d1	česko-izraelský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Battěkem	Battěk	k1gMnSc7	Battěk
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
Asociaci	asociace	k1gFnSc6	asociace
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakožto	jakožto	k8xS	jakožto
historik	historik	k1gMnSc1	historik
Pavel	Pavel	k1gMnSc1	Pavel
Bergmann	Bergmann	k1gMnSc1	Bergmann
poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
zejména	zejména	k9	zejména
na	na	k7c4	na
historické	historický	k2eAgFnPc4d1	historická
křivdy	křivda	k1gFnPc4	křivda
i	i	k8xC	i
způsob	způsob	k1gInSc4	způsob
jednostranného	jednostranný	k2eAgInSc2d1	jednostranný
historického	historický	k2eAgInSc2d1	historický
výkladu	výklad	k1gInSc2	výklad
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ignorování	ignorování	k1gNnSc4	ignorování
úlohy	úloha	k1gFnSc2	úloha
německých	německý	k2eAgMnPc2d1	německý
antifašistů	antifašista	k1gMnPc2	antifašista
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Karla	Karel	k1gMnSc2	Karel
Syřištěho	Syřiště	k1gMnSc2	Syřiště
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
II	II	kA	II
<g/>
.	.	kIx.	.
odboje	odboj	k1gInSc2	odboj
ve	v	k7c6	v
Všechlapech	Všechlap	k1gInPc6	Všechlap
i	i	k8xC	i
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
padlým	padlý	k1gMnPc3	padlý
odbojářům	odbojář	k1gMnPc3	odbojář
<g/>
,	,	kIx,	,
především	především	k9	především
německým	německý	k2eAgMnPc3d1	německý
sociálním	sociální	k2eAgMnPc3d1	sociální
demokratům	demokrat	k1gMnPc3	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Autorsky	autorsky	k6eAd1	autorsky
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Leo	Leo	k1gMnSc1	Leo
Pavlátem	Pavlát	k1gInSc7	Pavlát
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Daníčkem	Daníček	k1gMnSc7	Daníček
<g/>
,	,	kIx,	,
Arno	Arno	k6eAd1	Arno
Paříkem	Pařík	k1gInSc7	Pařík
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
dokumentu	dokument	k1gInSc6	dokument
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
Kritika	kritika	k1gFnSc1	kritika
devastace	devastace	k1gFnSc2	devastace
židovských	židovská	k1gFnPc2	židovská
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
zamlčování	zamlčování	k1gNnSc2	zamlčování
úlohy	úloha	k1gFnSc2	úloha
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
čs	čs	kA	čs
<g/>
.	.	kIx.	.
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Dcera	dcera	k1gFnSc1	dcera
Pavla	Pavla	k1gFnSc1	Pavla
Bergmanna	Bergmanna	k1gFnSc1	Bergmanna
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
Bergmannová	Bergmannová	k1gFnSc1	Bergmannová
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vzala	vzít	k5eAaPmAgFnS	vzít
sochaře	sochař	k1gMnSc4	sochař
Pavla	Pavel	k1gMnSc4	Pavel
Opočenského	opočenský	k2eAgMnSc4d1	opočenský
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
FREUND	FREUND	kA	FREUND
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
After	After	k1gInSc1	After
those	those	k1gFnSc2	those
fifty	fifta	k1gFnSc2	fifta
years	years	k1gInSc1	years
-	-	kIx~	-
Memoirs	Memoirs	k1gInSc1	Memoirs
of	of	k?	of
the	the	k?	the
Birkenau	Birkenaus	k1gInSc2	Birkenaus
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
226	[number]	k4	226
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0-9696	[number]	k4	0-9696
660	[number]	k4	660
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Duchcov	Duchcov	k1gInSc4	Duchcov
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
Holocaust	holocaust	k1gInSc4	holocaust
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
Lidovky	Lidovky	k1gFnPc4	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Záznam	záznam	k1gInSc1	záznam
svědectví	svědectví	k1gNnSc2	svědectví
u	u	k7c2	u
Frankfurtského	frankfurtský	k2eAgInSc2d1	frankfurtský
procesu	proces	k1gInSc2	proces
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FREUND	FREUND	kA	FREUND
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
After	After	k1gInSc1	After
those	those	k1gFnSc2	those
fifty	fifta	k1gFnSc2	fifta
years	years	k1gInSc1	years
-	-	kIx~	-
Memoirs	Memoirs	k1gInSc1	Memoirs
of	of	k?	of
the	the	k?	the
Birkenau	Birkenaus	k1gInSc2	Birkenaus
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
226	[number]	k4	226
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0-9696	[number]	k4	0-9696
660	[number]	k4	660
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Terezínský	terezínský	k2eAgInSc1d1	terezínský
rodinný	rodinný	k2eAgInSc1d1	rodinný
tábor	tábor	k1gInSc1	tábor
</s>
</p>
<p>
<s>
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
</s>
</p>
