<s>
PhDr.	PhDr.	kA	PhDr.
Pavel	Pavel	k1gMnSc1	Pavel
Bergmann	Bergmann	k1gMnSc1	Bergmann
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Jana	Jan	k1gMnSc2	Jan
Patočky	Patočka	k1gMnSc2	Patočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Bergmanna	Bergmanna	k1gFnSc1	Bergmanna
a	a	k8xC	a
Karoliny	Karolinum	k1gNnPc7	Karolinum
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
Steinové	Steinové	k2eAgFnSc1d1	Steinové
<g/>
.	.	kIx.	.
</s>
