<s>
Sgrafito	sgrafito	k1gNnSc1	sgrafito
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
grafické	grafický	k2eAgFnSc2d1	grafická
výzdoby	výzdoba	k1gFnSc2	výzdoba
omítkových	omítkový	k2eAgInPc2d1	omítkový
povrchů	povrch	k1gInPc2	povrch
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
prodělala	prodělat	k5eAaPmAgFnS	prodělat
největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
novorenesanci	novorenesance	k1gFnSc6	novorenesance
<g/>
.	.	kIx.	.
</s>
