<p>
<s>
Sgrafito	sgrafito	k1gNnSc1	sgrafito
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
grafické	grafický	k2eAgFnSc2d1	grafická
výzdoby	výzdoba	k1gFnSc2	výzdoba
omítkových	omítkový	k2eAgInPc2d1	omítkový
povrchů	povrch	k1gInPc2	povrch
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
prodělala	prodělat	k5eAaPmAgFnS	prodělat
největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
novorenesanci	novorenesance	k1gFnSc6	novorenesance
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
nanesení	nanesení	k1gNnSc6	nanesení
dvou	dva	k4xCgFnPc2	dva
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
různobarevných	různobarevný	k2eAgFnPc2d1	různobarevná
vrstev	vrstva	k1gFnPc2	vrstva
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
části	část	k1gFnPc4	část
horní	horní	k2eAgFnPc4d1	horní
ještě	ještě	k6eAd1	ještě
vlhké	vlhký	k2eAgFnPc4d1	vlhká
vrstvy	vrstva	k1gFnPc4	vrstva
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
škrabáním	škrabání	k1gNnSc7	škrabání
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
kontrastující	kontrastující	k2eAgFnSc1d1	kontrastující
spodní	spodní	k2eAgFnSc1d1	spodní
vrstva	vrstva	k1gFnSc1	vrstva
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
dekoraci	dekorace	k1gFnSc4	dekorace
vnější	vnější	k2eAgFnSc2d1	vnější
fasády	fasáda	k1gFnSc2	fasáda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
stěn	stěn	k1gInSc1	stěn
interiérů	interiér	k1gInPc2	interiér
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
sgrafita	sgrafito	k1gNnSc2	sgrafito
bývá	bývat	k5eAaImIp3nS	bývat
figurální	figurální	k2eAgFnSc1d1	figurální
nebo	nebo	k8xC	nebo
ornamentální	ornamentální	k2eAgNnPc1d1	ornamentální
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgNnPc1d1	charakteristické
jsou	být	k5eAaImIp3nP	být
obdélníková	obdélníkový	k2eAgNnPc1d1	obdélníkové
tzv.	tzv.	kA	tzv.
psaníčka	psaníčko	k1gNnSc2	psaníčko
(	(	kIx(	(
<g/>
psaníčkové	psaníčkový	k2eAgNnSc1d1	psaníčkové
sgrafito	sgrafito	k1gNnSc1	sgrafito
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
techniky	technika	k1gFnSc2	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Dvoubarevného	dvoubarevný	k2eAgInSc2d1	dvoubarevný
sgrafitového	sgrafitový	k2eAgInSc2d1	sgrafitový
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
spodní	spodní	k2eAgFnSc1d1	spodní
vrstva	vrstva	k1gFnSc1	vrstva
tmavé	tmavý	k2eAgFnSc2d1	tmavá
omítky	omítka	k1gFnSc2	omítka
obarvená	obarvený	k2eAgFnSc1d1	obarvená
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
renesančního	renesanční	k2eAgInSc2d1	renesanční
způsobu	způsob	k1gInSc2	způsob
jemně	jemně	k6eAd1	jemně
mletým	mletý	k2eAgNnSc7d1	mleté
uhlím	uhlí	k1gNnSc7	uhlí
nebo	nebo	k8xC	nebo
popelem	popel	k1gInSc7	popel
ze	z	k7c2	z
spálené	spálený	k2eAgFnSc2d1	spálená
slámy	sláma	k1gFnSc2	sláma
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
zaschnout	zaschnout	k5eAaPmF	zaschnout
a	a	k8xC	a
do	do	k7c2	do
svrchní	svrchní	k2eAgFnSc2d1	svrchní
tenké	tenký	k2eAgFnSc2d1	tenká
vrstvy	vrstva	k1gFnSc2	vrstva
omítky	omítka	k1gFnSc2	omítka
světlé	světlý	k2eAgFnSc2d1	světlá
barvy	barva	k1gFnSc2	barva
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
hlazeným	hlazený	k2eAgInSc7d1	hlazený
povrchem	povrch	k1gInSc7	povrch
se	se	k3xPyFc4	se
za	za	k7c4	za
vlhka	vlhko	k1gNnPc4	vlhko
proškrabují	proškrabovat	k5eAaImIp3nP	proškrabovat
vzory	vzor	k1gInPc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
postup	postup	k1gInSc1	postup
opačný	opačný	k2eAgInSc1d1	opačný
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
spodní	spodní	k2eAgFnSc1d1	spodní
omítka	omítka	k1gFnSc1	omítka
je	být	k5eAaImIp3nS	být
světlá	světlý	k2eAgFnSc1d1	světlá
a	a	k8xC	a
vzory	vzor	k1gInPc1	vzor
jsou	být	k5eAaImIp3nP	být
proškrabovány	proškrabován	k2eAgInPc1d1	proškrabován
do	do	k7c2	do
svrchní	svrchní	k2eAgFnSc2d1	svrchní
tmavé	tmavý	k2eAgFnSc2d1	tmavá
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
kontrasgrafito	kontrasgrafít	k5eAaPmNgNnS	kontrasgrafít
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc1	postup
zjednodušován	zjednodušován	k2eAgInSc1d1	zjednodušován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzory	vzor	k1gInPc7	vzor
proškrabují	proškrabovat	k5eAaImIp3nP	proškrabovat
do	do	k7c2	do
jednobarevné	jednobarevný	k2eAgFnSc2d1	jednobarevná
světlé	světlý	k2eAgFnSc2d1	světlá
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdrsnění	zdrsnění	k1gNnSc6	zdrsnění
hladké	hladký	k2eAgFnSc2d1	hladká
plochy	plocha	k1gFnSc2	plocha
fasády	fasáda	k1gFnSc2	fasáda
vyrytím	vyrytí	k1gNnSc7	vyrytí
vzorů	vzor	k1gInPc2	vzor
se	se	k3xPyFc4	se
zakrátko	zakrátko	k6eAd1	zakrátko
vlivem	vliv	k1gInSc7	vliv
povětrnosti	povětrnost	k1gFnSc2	povětrnost
škrábané	škrábaný	k2eAgFnSc2d1	škrábaná
části	část	k1gFnSc2	část
v	v	k7c6	v
jednobarevné	jednobarevný	k2eAgFnSc6d1	jednobarevná
omítce	omítka	k1gFnSc6	omítka
barevně	barevně	k6eAd1	barevně
odliší	odlišit	k5eAaPmIp3nS	odlišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
sgrafit	sgrafito	k1gNnPc2	sgrafito
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Psaníčko	psaníčko	k1gNnSc1	psaníčko
===	===	k?	===
</s>
</p>
<p>
<s>
Forma	forma	k1gFnSc1	forma
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
obdélníků	obdélník	k1gInPc2	obdélník
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
dvěma	dva	k4xCgFnPc7	dva
úhlopříčkami	úhlopříčka	k1gFnPc7	úhlopříčka
připomíná	připomínat	k5eAaImIp3nS	připomínat
zadní	zadní	k2eAgFnSc4d1	zadní
stranu	strana	k1gFnSc4	strana
dopisní	dopisní	k2eAgFnSc2d1	dopisní
obálky	obálka	k1gFnSc2	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
zdobená	zdobený	k2eAgFnSc1d1	zdobená
psaníčkovým	psaníčkový	k2eAgNnSc7d1	psaníčkové
sgrafitem	sgrafito	k1gNnSc7	sgrafito
<g/>
,	,	kIx,	,
napodobujícím	napodobující	k2eAgMnSc7d1	napodobující
iluzívně	iluzívně	k6eAd1	iluzívně
diamantový	diamantový	k2eAgInSc4d1	diamantový
řez	řez	k1gInSc4	řez
nebo	nebo	k8xC	nebo
pyramidovitě	pyramidovitě	k6eAd1	pyramidovitě
zašpičatělé	zašpičatělý	k2eAgInPc4d1	zašpičatělý
stavební	stavební	k2eAgInPc4d1	stavební
kvádry	kvádr	k1gInPc4	kvádr
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
dojem	dojem	k1gInSc4	dojem
plasticity	plasticita	k1gFnSc2	plasticita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ornamenty	ornament	k1gInPc4	ornament
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
vzoru	vzor	k1gInSc2	vzor
psaníček	psaníčko	k1gNnPc2	psaníčko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sgrafito	sgrafito	k1gNnSc1	sgrafito
geometricky	geometricky	k6eAd1	geometricky
členěno	členit	k5eAaImNgNnS	členit
přímkami	přímka	k1gFnPc7	přímka
<g/>
,	,	kIx,	,
kružnicemi	kružnice	k1gFnPc7	kružnice
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
křivkami	křivka	k1gFnPc7	křivka
a	a	k8xC	a
rostlinnými	rostlinný	k2eAgInPc7d1	rostlinný
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
často	často	k6eAd1	často
používané	používaný	k2eAgInPc1d1	používaný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
ornamentální	ornamentální	k2eAgInPc4d1	ornamentální
<g/>
,	,	kIx,	,
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
nebo	nebo	k8xC	nebo
figurální	figurální	k2eAgInPc4d1	figurální
motivy	motiv	k1gInPc4	motiv
sgrafita	sgrafito	k1gNnSc2	sgrafito
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc7d1	obsahující
většinou	většina	k1gFnSc7	většina
téma	téma	k1gNnSc4	téma
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
řecké	řecký	k2eAgFnSc2d1	řecká
či	či	k8xC	či
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
především	především	k9	především
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc1d1	různá
alegorické	alegorický	k2eAgFnPc1d1	alegorická
postavy	postava	k1gFnPc1	postava
či	či	k8xC	či
výjevy	výjev	k1gInPc1	výjev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sgrafito	sgrafito	k1gNnSc1	sgrafito
v	v	k7c6	v
novorenesanci	novorenesance	k1gFnSc6	novorenesance
==	==	k?	==
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
výzdoby	výzdoba	k1gFnSc2	výzdoba
novorenesenačních	novorenesenační	k2eAgInPc2d1	novorenesenační
domů	dům	k1gInPc2	dům
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
sgrafita	sgrafito	k1gNnPc1	sgrafito
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgNnPc1	první
sgrafita	sgrafito	k1gNnPc1	sgrafito
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Ignác	Ignác	k1gMnSc1	Ignác
Ullmann	Ullmann	k1gMnSc1	Ullmann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
dívčí	dívčí	k2eAgFnSc2d1	dívčí
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Vodičkově	Vodičkův	k2eAgFnSc6d1	Vodičkova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
rozšíření	rozšíření	k1gNnSc6	rozšíření
sgrafit	sgrafito	k1gNnPc2	sgrafito
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
novorenesanční	novorenesanční	k2eAgFnSc6d1	novorenesanční
architektuře	architektura	k1gFnSc6	architektura
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
architekt	architekt	k1gMnSc1	architekt
Josef	Josef	k1gMnSc1	Josef
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jehož	jenž	k3xRgInSc2	jenž
návrhu	návrh	k1gInSc2	návrh
se	se	k3xPyFc4	se
rekonstruoval	rekonstruovat	k5eAaBmAgInS	rekonstruovat
Schwarzenberský	schwarzenberský	k2eAgInSc1d1	schwarzenberský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
prakticky	prakticky	k6eAd1	prakticky
ověřoval	ověřovat	k5eAaImAgMnS	ověřovat
metody	metoda	k1gFnPc4	metoda
tvorby	tvorba	k1gFnSc2	tvorba
sgrafitové	sgrafitový	k2eAgFnSc2d1	sgrafitová
výzdoby	výzdoba	k1gFnSc2	výzdoba
fasád	fasáda	k1gFnPc2	fasáda
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
receptur	receptura	k1gFnPc2	receptura
<g/>
.	.	kIx.	.
</s>
<s>
Schulzovou	Schulzův	k2eAgFnSc7d1	Schulzova
zásluhou	zásluha	k1gFnSc7	zásluha
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
sgrafita	sgrafito	k1gNnPc4	sgrafito
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
českých	český	k2eAgMnPc2d1	český
architektů	architekt	k1gMnPc2	architekt
je	být	k5eAaImIp3nS	být
začala	začít	k5eAaPmAgFnS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
ve	v	k7c6	v
výzdobě	výzdoba	k1gFnSc6	výzdoba
novorenesančních	novorenesanční	k2eAgInPc2d1	novorenesanční
činžovních	činžovní	k2eAgInPc2d1	činžovní
domů	dům	k1gInPc2	dům
a	a	k8xC	a
hledala	hledat	k5eAaImAgFnS	hledat
inspiraci	inspirace	k1gFnSc4	inspirace
ve	v	k7c6	v
výzdobě	výzdoba	k1gFnSc6	výzdoba
renesančních	renesanční	k2eAgInPc2d1	renesanční
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
uplatnění	uplatnění	k1gNnSc4	uplatnění
sgrafit	sgrafito	k1gNnPc2	sgrafito
například	například	k6eAd1	například
referoval	referovat	k5eAaBmAgInS	referovat
Jan	Jan	k1gMnSc1	Jan
Koula	Koula	k1gMnSc1	Koula
v	v	k7c6	v
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
Domy	dům	k1gInPc4	dům
pp	pp	k?	pp
<g/>
.	.	kIx.	.
<g/>
architektů	architekt	k1gMnPc2	architekt
V.	V.	kA	V.
<g/>
Skučka	Skučka	k1gFnSc1	Skučka
a	a	k8xC	a
J.	J.	kA	J.
<g/>
Zeyera	Zeyer	k1gMnSc2	Zeyer
<g/>
"	"	kIx"	"
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
se	s	k7c7	s
sgrafity	sgrafito	k1gNnPc7	sgrafito
publikoval	publikovat	k5eAaBmAgMnS	publikovat
i	i	k9	i
malíř	malíř	k1gMnSc1	malíř
Láďa	Láďa	k1gMnSc1	Láďa
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
často	často	k6eAd1	často
na	na	k7c6	na
fasádách	fasáda	k1gFnPc6	fasáda
domů	dům	k1gInPc2	dům
tvořil	tvořit	k5eAaImAgInS	tvořit
výzdobu	výzdoba	k1gFnSc4	výzdoba
podle	podle	k7c2	podle
Alšových	Alšových	k2eAgInPc2d1	Alšových
kartonů	karton	k1gInPc2	karton
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
uplatnění	uplatnění	k1gNnSc4	uplatnění
sgrafit	sgrafito	k1gNnPc2	sgrafito
na	na	k7c6	na
fasádách	fasáda	k1gFnPc6	fasáda
nájemních	nájemní	k2eAgInPc2d1	nájemní
domů	dům	k1gInPc2	dům
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
architekt	architekt	k1gMnSc1	architekt
Antonín	Antonín	k1gMnSc1	Antonín
Wiehl	Wiehl	k1gMnSc1	Wiehl
<g/>
.	.	kIx.	.
</s>
<s>
Mezníkem	mezník	k1gInSc7	mezník
Wiehlovy	Wiehlův	k2eAgFnSc2d1	Wiehlova
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1035	[number]	k4	1035
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
v	v	k7c6	v
Poštovské	poštovský	k2eAgFnSc6d1	Poštovská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Wiehl	Wiehl	k1gMnSc1	Wiehl
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ve	v	k7c4	v
spolupráce	spolupráce	k1gFnPc4	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
umělci	umělec	k1gMnPc7	umělec
<g/>
:	:	kIx,	:
sgrafita	sgrafito	k1gNnPc1	sgrafito
jsou	být	k5eAaImIp3nP	být
navržena	navržen	k2eAgFnSc1d1	navržena
Františkem	František	k1gMnSc7	František
Ženíškem	Ženíšek	k1gMnSc7	Ženíšek
<g/>
.	.	kIx.	.
</s>
<s>
Sgrafitová	sgrafitový	k2eAgFnSc1d1	sgrafitová
fasáda	fasáda	k1gFnSc1	fasáda
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
ukončená	ukončený	k2eAgFnSc1d1	ukončená
lunetovou	lunetový	k2eAgFnSc7d1	lunetová
římsou	římsa	k1gFnSc7	římsa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ojedinělé	ojedinělý	k2eAgNnSc4d1	ojedinělé
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgInS	Wiehnout
lunetovou	lunetový	k2eAgFnSc4d1	lunetová
římsu	římsa	k1gFnSc4	římsa
zapracoval	zapracovat	k5eAaPmAgMnS	zapracovat
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
až	až	k6eAd1	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	s	k7c7	s
Schwarzenberským	schwarzenberský	k2eAgInSc7d1	schwarzenberský
palácem	palác	k1gInSc7	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
po	po	k7c6	po
Schulzově	Schulzův	k2eAgFnSc6d1	Schulzova
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
palácem	palác	k1gInSc7	palác
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
u	u	k7c2	u
atiky	atika	k1gFnSc2	atika
a	a	k8xC	a
lunet	luneta	k1gFnPc2	luneta
se	s	k7c7	s
grafity	grafit	k1gInPc7	grafit
Fary	fara	k1gFnSc2	fara
u	u	k7c2	u
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Wiehlův	Wiehlův	k2eAgMnSc1d1	Wiehlův
kolega	kolega	k1gMnSc1	kolega
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Koula	Koula	k1gMnSc1	Koula
jeho	jeho	k3xOp3gNnSc4	jeho
úsilí	úsilí	k1gNnSc4	úsilí
definoval	definovat	k5eAaBmAgInS	definovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
ve	v	k7c6	v
Zprávách	zpráva	k1gFnPc6	zpráva
Spolku	spolek	k1gInSc2	spolek
architektů	architekt	k1gMnPc2	architekt
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
výklad	výklad	k1gInSc1	výklad
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
stylu	styl	k1gInSc3	styl
A.	A.	kA	A.
Wiehla	Wiehla	k1gFnSc2	Wiehla
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
........	........	k?	........
<g/>
Wiehl	Wiehl	k1gFnSc1	Wiehl
bojuje	bojovat	k5eAaImIp3nS	bojovat
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
vyjádření	vyjádření	k1gNnSc4	vyjádření
architektonické	architektonický	k2eAgNnSc4d1	architektonické
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzorů	vzor	k1gInPc2	vzor
,	,	kIx,	,
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Čechy	Čechy	k1gFnPc4	Čechy
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
typických	typický	k2eAgFnPc2d1	typická
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
po	po	k7c6	po
prvé	prvý	k4xOgFnSc6	prvý
<g/>
,	,	kIx,	,
když	když	k8xS	když
postavil	postavit	k5eAaPmAgInS	postavit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
sgrafitový	sgrafitový	k2eAgInSc4d1	sgrafitový
domek	domek	k1gInSc4	domek
<g/>
"	"	kIx"	"
v	v	k7c6	v
Poštovské	poštovský	k2eAgFnSc6d1	Poštovská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pilně	pilně	k6eAd1	pilně
sbíral	sbírat	k5eAaImAgMnS	sbírat
památky	památka	k1gFnPc4	památka
naší	náš	k3xOp1gFnSc2	náš
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
studovala	studovat	k5eAaImAgFnS	studovat
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
hleděl	hledět	k5eAaImAgMnS	hledět
jich	on	k3xPp3gMnPc2	on
užíti	užít	k5eAaPmF	užít
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Wiehlovým	Wiehlův	k2eAgNnSc7d1	Wiehlův
přičiněním	přičinění	k1gNnSc7	přičinění
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
"	"	kIx"	"
<g/>
české	český	k2eAgFnSc3d1	Česká
renesanci	renesance	k1gFnSc3	renesance
<g/>
;	;	kIx,	;
cítíme	cítit	k5eAaImIp1nP	cítit
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
dosud	dosud	k6eAd1	dosud
nestanovil	stanovit	k5eNaPmAgInS	stanovit
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
ráz	ráz	k1gInSc4	ráz
těch	ten	k3xDgFnPc2	ten
staveb	stavba	k1gFnPc2	stavba
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
....	....	k?	....
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Novorenesační	Novorenesační	k2eAgInPc1d1	Novorenesační
domy	dům	k1gInPc1	dům
se	s	k7c7	s
sgrafity	sgrafito	k1gNnPc7	sgrafito
navržené	navržený	k2eAgFnSc2d1	navržená
Antonínem	Antonín	k1gMnSc7	Antonín
Wiehlem	Wiehl	k1gMnSc7	Wiehl
===	===	k?	===
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Schnircha	Schnirch	k1gMnSc2	Schnirch
v	v	k7c6	v
Mikovcově	Mikovcův	k2eAgFnSc6d1	Mikovcova
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
548	[number]	k4	548
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
autory	autor	k1gMnPc7	autor
výzdoby	výzdoba	k1gFnSc2	výzdoba
jsou	být	k5eAaImIp3nP	být
Schnirch	Schnirch	k1gMnSc1	Schnirch
a	a	k8xC	a
Fr.	Fr.	k1gMnSc1	Fr.
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1035	[number]	k4	1035
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Karolíny	Karolína	k1gFnSc2	Karolína
Světlé	světlý	k2eAgFnSc2d1	světlá
Wiehl	Wiehl	k1gFnSc2	Wiehl
stavěl	stavět	k5eAaImAgMnS	stavět
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
J.	J.	kA	J.
Zeyerem	Zeyer	k1gMnSc7	Zeyer
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
výzdoby	výzdoba	k1gFnSc2	výzdoba
jsou	být	k5eAaImIp3nP	být
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
a	a	k8xC	a
Myslbek	Myslbek	k1gMnSc1	Myslbek
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nárožní	nárožní	k2eAgInSc1d1	nárožní
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Mladých	mladý	k2eAgInPc2d1	mladý
Goliášů	goliáš	k1gInPc2	goliáš
čp.	čp.	k?	čp.
527	[number]	k4	527
<g/>
/	/	kIx~	/
<g/>
I	i	k9	i
ve	v	k7c6	v
Skořepce	skořepka	k1gFnSc6	skořepka
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Staré	Stará	k1gFnSc6	Stará
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Jilská	jilský	k2eAgFnSc1d1	Jilská
2	[number]	k4	2
<g/>
,	,	kIx,	,
Skořepka	skořepka	k1gFnSc1	skořepka
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
postavil	postavit	k5eAaPmAgMnS	postavit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gemperlem	Gemperl	k1gMnSc7	Gemperl
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
sgrafit	sgrafito	k1gNnPc2	sgrafito
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nárožní	nárožní	k2eAgInSc1d1	nárožní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
542	[number]	k4	542
ve	v	k7c6	v
Zborovské	Zborovský	k2eAgFnSc6d1	Zborovská
ulici	ulice	k1gFnSc6	ulice
42	[number]	k4	42
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
postavil	postavit	k5eAaPmAgMnS	postavit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gemperlem	Gemperl	k1gMnSc7	Gemperl
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
plastik	plastika	k1gFnPc2	plastika
J.V.	J.V.	k1gFnPc2	J.V.
Myslbek	Myslbek	k1gInSc1	Myslbek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nárožní	nárožní	k2eAgInSc1d1	nárožní
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1682	[number]	k4	1682
Na	na	k7c6	na
Poříčí	Poříčí	k1gNnSc6	Poříčí
(	(	kIx(	(
<g/>
or	or	k?	or
<g/>
.	.	kIx.	.
č.	č.	k?	č.
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
č.	č.	k?	č.
o.	o.	k?	o.
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wiehl	Wiehnout	k5eAaPmAgMnS	Wiehnout
postavil	postavit	k5eAaPmAgMnS	postavit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gemperlem	Gemperl	k1gMnSc7	Gemperl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlastní	vlastní	k2eAgInSc1d1	vlastní
nárožní	nárožní	k2eAgInSc1d1	nárožní
Wiehlův	Wiehlův	k2eAgInSc1d1	Wiehlův
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
<g/>
792	[number]	k4	792
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
autoři	autor	k1gMnPc1	autor
výzdoby	výzdoba	k1gFnSc2	výzdoba
Aleš	Aleš	k1gMnSc1	Aleš
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Fanta	Fanta	k1gMnSc1	Fanta
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Vladislava	Vladislava	k1gFnSc1	Vladislava
Říhová	Říhová	k1gFnSc1	Říhová
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Sgrafito	sgrafito	k1gNnSc1	sgrafito
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
restaurování	restaurování	k1gNnSc1	restaurování
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7395-228-0	[number]	k4	978-80-7395-228-0
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Waisser	Waisser	k1gMnSc1	Waisser
<g/>
:	:	kIx,	:
Renesanční	renesanční	k2eAgNnSc1d1	renesanční
figurální	figurální	k2eAgNnSc1d1	figurální
sgrafito	sgrafito	k1gNnSc1	sgrafito
na	na	k7c6	na
průčelích	průčelí	k1gNnPc6	průčelí
moravských	moravský	k2eAgInPc2d1	moravský
městských	městský	k2eAgInPc2d1	městský
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-244-4305-8	[number]	k4	978-80-244-4305-8
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sgrafito	sgrafito	k1gNnSc4	sgrafito
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Panoch	Panoch	k1gMnSc1	Panoch
<g/>
,	,	kIx,	,
Mluvící	mluvící	k2eAgFnSc4d1	mluvící
průčelí	průčelí	k1gNnSc3	průčelí
<g/>
,	,	kIx,	,
Novověké	novověký	k2eAgFnSc3d1	novověká
sgrafitové	sgrafitový	k2eAgFnPc4d1	sgrafitová
a	a	k8xC	a
malované	malovaný	k2eAgFnPc4d1	malovaná
fasády	fasáda	k1gFnPc4	fasáda
jako	jako	k8xC	jako
pramen	pramen	k1gInSc4	pramen
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
kulturní	kulturní	k2eAgFnSc2d1	kulturní
historie	historie	k1gFnSc2	historie
</s>
</p>
