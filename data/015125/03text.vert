<s>
Chrëschtlech	Chrëschtl	k1gInPc6
Sozial	Sozial	k1gMnSc1
Vollekspartei	Vollekspartei	k1gNnSc4
</s>
<s>
Chrëschtlech	Chrëschtl	k1gInPc6
Sozial	Sozial	k1gInSc4
Vollekspartei	Volleksparte	k1gFnSc2
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
1944	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Engel	Engel	k1gMnSc1
(	(	kIx(
<g/>
Politiker	Politiker	k1gInSc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Lucemburk	Lucemburk	k1gInSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
www.csv.lu	www.csv.lat	k5eAaPmIp1nS
</s>
<s>
Chrëschtlech	Chrëschtl	k1gInPc6
Sozial	Sozial	k1gInSc4
Vollekspartei	Vollekspartei	k1gNnPc7
(	(	kIx(
<g/>
CSV	CSV	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Křesťanskosociální	křesťanskosociální	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nebo	nebo	k8xC
Sociálně-křesťanská	Sociálně-křesťanský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
lucemburská	lucemburský	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
sleduje	sledovat	k5eAaImIp3nS
středovou	středový	k2eAgFnSc4d1
křesťansko-demokratickou	křesťansko-demokratický	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
Michel	Michel	k1gMnSc1
Wolter	Wolter	k1gMnSc1
<g/>
,	,	kIx,
současně	současně	k6eAd1
též	též	k9
lucemburský	lucemburský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
pro	pro	k7c4
zaměstnanost	zaměstnanost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gInSc7
CSV	CSV	kA
je	být	k5eAaImIp3nS
také	také	k9
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
expremiér	expremiér	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
Jean-Claude	Jean-Claud	k1gInSc5
Juncker	Junckero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
CSV	CSV	kA
koalici	koalice	k1gFnSc4
s	s	k7c7
LSAP	LSAP	kA
a	a	k8xC
od	od	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
má	mít	k5eAaImIp3nS
v	v	k7c6
parlamentu	parlament	k1gInSc6
24	#num#	k4
ze	z	k7c2
60	#num#	k4
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strana	strana	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
let	léto	k1gNnPc2
1975	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
účastnila	účastnit	k5eAaImAgFnS
všech	všecek	k3xTgFnPc2
lucemburských	lucemburský	k2eAgFnPc2d1
koaličních	koaliční	k2eAgFnPc2d1
vlád	vláda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chrëschtlech	Chrëschtl	k1gInPc6
Sozial	Sozial	k1gMnSc1
Vollekspartei	Vollekspartei	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
↑	↑	k?
Parlamentní	parlamentní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
Lucembursku	Lucembursko	k1gNnSc6
<g/>
,	,	kIx,
stránka	stránka	k1gFnSc1
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
..	..	k?
www.kdu.cz	www.kdu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
strany	strana	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
2017408-1	2017408-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2009140004	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124969559	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2009140004	#num#	k4
</s>
