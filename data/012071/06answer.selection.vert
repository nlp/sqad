<s>
Smíšená	smíšený	k2eAgNnPc1d1	smíšené
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mixed	Mixed	k1gMnSc1	Mixed
Martial	Martial	k1gMnSc1	Martial
Arts	Arts	k1gInSc4	Arts
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
MMA	MMA	kA	MMA
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Artes	Artes	k1gMnSc1	Artes
marciais	marciais	k1gFnPc2	marciais
mistas	mistas	k1gMnSc1	mistas
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
AMM	AMM	kA	AMM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plnokontaktní	plnokontaktní	k2eAgInSc1d1	plnokontaktní
bojový	bojový	k2eAgInSc1d1	bojový
sport	sport	k1gInSc1	sport
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInPc4d1	umožňující
údery	úder	k1gInPc4	úder
i	i	k8xC	i
chvaty	chvat	k1gInPc4	chvat
<g/>
,	,	kIx,	,
boj	boj	k1gInSc4	boj
ve	v	k7c6	v
stoje	stoje	k6eAd1	stoje
i	i	k8xC	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
spojující	spojující	k2eAgFnPc1d1	spojující
různé	různý	k2eAgFnPc1d1	různá
techniky	technika	k1gFnPc1	technika
jiných	jiný	k2eAgInPc2d1	jiný
bojových	bojový	k2eAgInPc2d1	bojový
sportů	sport	k1gInPc2	sport
i	i	k8xC	i
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
