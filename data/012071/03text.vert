<p>
<s>
Smíšená	smíšený	k2eAgNnPc1d1	smíšené
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mixed	Mixed	k1gMnSc1	Mixed
Martial	Martial	k1gMnSc1	Martial
Arts	Arts	k1gInSc4	Arts
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
MMA	MMA	kA	MMA
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Artes	Artes	k1gMnSc1	Artes
marciais	marciais	k1gFnPc2	marciais
mistas	mistas	k1gMnSc1	mistas
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
AMM	AMM	kA	AMM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plnokontaktní	plnokontaktní	k2eAgInSc1d1	plnokontaktní
bojový	bojový	k2eAgInSc1d1	bojový
sport	sport	k1gInSc1	sport
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInPc4d1	umožňující
údery	úder	k1gInPc4	úder
i	i	k8xC	i
chvaty	chvat	k1gInPc4	chvat
<g/>
,	,	kIx,	,
boj	boj	k1gInSc4	boj
ve	v	k7c6	v
stoje	stoje	k6eAd1	stoje
i	i	k8xC	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
spojující	spojující	k2eAgFnPc1d1	spojující
různé	různý	k2eAgFnPc1d1	různá
techniky	technika	k1gFnPc1	technika
jiných	jiný	k2eAgInPc2d1	jiný
bojových	bojový	k2eAgInPc2d1	bojový
sportů	sport	k1gInPc2	sport
i	i	k8xC	i
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
nalézt	nalézt	k5eAaPmF	nalézt
mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
bojovými	bojový	k2eAgNnPc7d1	bojové
uměními	umění	k1gNnPc7	umění
nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
způsob	způsob	k1gInSc4	způsob
boje	boj	k1gInSc2	boj
ve	v	k7c6	v
skutečných	skutečný	k2eAgFnPc6d1	skutečná
situacích	situace	k1gFnPc6	situace
neozbrojeného	ozbrojený	k2eNgInSc2d1	neozbrojený
střetu	střet	k1gInSc2	střet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
bojovníky	bojovník	k1gMnPc4	bojovník
světa	svět	k1gInSc2	svět
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Dustin	Dustin	k1gMnSc1	Dustin
Poirier	Poirier	k1gMnSc1	Poirier
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Jones	Jones	k1gMnSc1	Jones
Khabib	Khabib	k1gMnSc1	Khabib
Nurmagomedov	Nurmagomedov	k1gInSc1	Nurmagomedov
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Ferguson	Ferguson	k1gMnSc1	Ferguson
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Holloway	Hollowaa	k1gFnSc2	Hollowaa
<g/>
,	,	kIx,	,
<g/>
Conor	Conor	k1gMnSc1	Conor
McGregor	McGregor	k1gMnSc1	McGregor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
bojovníků	bojovník	k1gMnPc2	bojovník
bojují	bojovat	k5eAaImIp3nP	bojovat
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
nejprestižnější	prestižní	k2eAgFnSc2d3	nejprestižnější
organizace	organizace	k1gFnSc2	organizace
Ultimate	Ultimat	k1gInSc5	Ultimat
Fighting	Fighting	k1gInSc1	Fighting
Championship	Championship	k1gInSc1	Championship
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
soupeře	soupeř	k1gMnPc4	soupeř
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
těžké	těžký	k2eAgInPc1d1	těžký
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
neporazitelný	porazitelný	k2eNgMnSc1d1	neporazitelný
bojovník	bojovník	k1gMnSc1	bojovník
s	s	k7c7	s
perfektní	perfektní	k2eAgFnSc7d1	perfektní
bilancí	bilance	k1gFnSc7	bilance
trénující	trénující	k2eAgInSc4d1	trénující
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lehce	lehko	k6eAd1	lehko
poráží	porážet	k5eAaImIp3nS	porážet
své	svůj	k3xOyFgMnPc4	svůj
soupeře	soupeř	k1gMnPc4	soupeř
jako	jako	k8xC	jako
na	na	k7c6	na
běžícím	běžící	k2eAgInSc6d1	běžící
páse	pás	k1gInSc6	pás
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
UFC	UFC	kA	UFC
zápasit	zápasit	k5eAaImF	zápasit
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
nejslabšími	slabý	k2eAgMnPc7d3	nejslabší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
==	==	k?	==
</s>
</p>
<p>
<s>
MMA	MMA	kA	MMA
(	(	kIx(	(
<g/>
či	či	k8xC	či
Vale-tudo	Valeudo	k1gNnSc1	Vale-tudo
<g/>
,	,	kIx,	,
Free-Fight	Free-Fight	k1gInSc1	Free-Fight
<g/>
,	,	kIx,	,
Ultimate-Fighting	Ultimate-Fighting	k1gInSc1	Ultimate-Fighting
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fenomén	fenomén	k1gNnSc1	fenomén
posledních	poslední	k2eAgNnPc2d1	poslední
dvou	dva	k4xCgNnPc2	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
kdy	kdy	k6eAd1	kdy
Řekové	Řek	k1gMnPc1	Řek
zavedli	zavést	k5eAaPmAgMnP	zavést
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
Pankration	Pankration	k1gInSc4	Pankration
<g/>
,	,	kIx,	,
drsnou	drsný	k2eAgFnSc4d1	drsná
směs	směs	k1gFnSc4	směs
starověkého	starověký	k2eAgInSc2d1	starověký
boxu	box	k1gInSc2	box
a	a	k8xC	a
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
povolené	povolený	k2eAgInPc1d1	povolený
všechny	všechen	k3xTgInPc1	všechen
údery	úder	k1gInPc1	úder
a	a	k8xC	a
zápasnické	zápasnický	k2eAgFnPc1d1	zápasnická
techniky	technika	k1gFnPc1	technika
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
lámání	lámání	k1gNnSc2	lámání
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
vypichování	vypichování	k1gNnSc3	vypichování
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
úderů	úder	k1gInPc2	úder
na	na	k7c4	na
genitálie	genitálie	k1gFnPc4	genitálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
poražení	poražení	k1gNnSc3	poražení
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
trvaly	trvat	k5eAaImAgInP	trvat
často	často	k6eAd1	často
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
nezřídka	nezřídka	k6eAd1	nezřídka
končily	končit	k5eAaImAgInP	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pankratisty	Pankratista	k1gMnPc4	Pankratista
využíval	využívat	k5eAaPmAgMnS	využívat
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
legendární	legendární	k2eAgFnPc4d1	legendární
schopnosti	schopnost	k1gFnPc4	schopnost
např.	např.	kA	např.
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
taženích	tažení	k1gNnPc6	tažení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
boje	boj	k1gInSc2	boj
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
jasnými	jasný	k2eAgNnPc7d1	jasné
a	a	k8xC	a
bezpečnými	bezpečný	k2eAgNnPc7d1	bezpečné
pravidly	pravidlo	k1gNnPc7	pravidlo
chránícími	chránící	k2eAgFnPc7d1	chránící
oba	dva	k4xCgMnPc4	dva
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zrod	zrod	k1gInSc1	zrod
moderní	moderní	k2eAgFnSc2d1	moderní
podoby	podoba	k1gFnSc2	podoba
MMA	MMA	kA	MMA
==	==	k?	==
</s>
</p>
<p>
<s>
Zrod	zrod	k1gInSc1	zrod
moderních	moderní	k2eAgMnPc2d1	moderní
MMA	MMA	kA	MMA
je	on	k3xPp3gInPc4	on
možno	možno	k6eAd1	možno
vystopovat	vystopovat	k5eAaPmF	vystopovat
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc4	první
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Helio	Helio	k1gMnSc1	Helio
Gracie	Gracie	k1gFnSc2	Gracie
(	(	kIx(	(
<g/>
rodina	rodina	k1gFnSc1	rodina
Gracie	Gracie	k1gFnSc2	Gracie
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Brazilské	brazilský	k2eAgNnSc4d1	brazilské
Jiu-jitsu	Jiuitsa	k1gFnSc4	Jiu-jitsa
<g/>
)	)	kIx)	)
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
boji	boj	k1gInSc3	boj
zástupce	zástupce	k1gMnPc4	zástupce
různých	různý	k2eAgInPc2d1	různý
bojových	bojový	k2eAgInPc2d1	bojový
stylů	styl	k1gInPc2	styl
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
Vale	val	k1gInSc6	val
tudo	tudo	k1gNnSc1	tudo
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
vše	všechen	k3xTgNnSc1	všechen
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
bojích	boj	k1gInPc6	boj
většinou	většinou	k6eAd1	většinou
vítězil	vítězit	k5eAaImAgMnS	vítězit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
proti	proti	k7c3	proti
mnohem	mnohem	k6eAd1	mnohem
těžším	těžký	k2eAgMnPc3d2	těžší
bojovníkům	bojovník	k1gMnPc3	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učít	k5eAaPmAgMnS	učít
BJJ	BJJ	kA	BJJ
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
poté	poté	k6eAd1	poté
vydali	vydat	k5eAaPmAgMnP	vydat
proslulou	proslulý	k2eAgFnSc4d1	proslulá
Gracieovskou	Gracieovský	k2eAgFnSc4d1	Gracieovský
výzvu	výzva	k1gFnSc4	výzva
(	(	kIx(	(
<g/>
odměnu	odměna	k1gFnSc4	odměna
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	on	k3xPp3gMnPc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
porazit	porazit	k5eAaPmF	porazit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zajistili	zajistit	k5eAaPmAgMnP	zajistit
svému	svůj	k3xOyFgInSc3	svůj
rodinnému	rodinný	k2eAgInSc3d1	rodinný
stylu	styl	k1gInSc3	styl
nehynoucí	hynoucí	k2eNgFnSc1d1	nehynoucí
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
k	k	k7c3	k
soutěžím	soutěž	k1gFnPc3	soutěž
ve	v	k7c4	v
Vale-Tudo	Vale-Tudo	k1gNnSc4	Vale-Tudo
množství	množství	k1gNnSc2	množství
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bojovníky	bojovník	k1gMnPc4	bojovník
vždycky	vždycky	k6eAd1	vždycky
zajímalo	zajímat	k5eAaImAgNnS	zajímat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
styl	styl	k1gInSc1	styl
boje	boj	k1gInSc2	boj
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
zvědavost	zvědavost	k1gFnSc1	zvědavost
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
v	v	k7c4	v
amatérský	amatérský	k2eAgInSc4d1	amatérský
šampionát	šampionát	k1gInSc4	šampionát
UFAC	UFAC	kA	UFAC
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Fighting	Fighting	k1gInSc1	Fighting
amateur	amateur	k1gMnSc1	amateur
Championship	Championship	k1gMnSc1	Championship
<g/>
)	)	kIx)	)
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
tu	tu	k6eAd1	tu
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
zástupci	zástupce	k1gMnPc1	zástupce
různých	různý	k2eAgInPc2d1	různý
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Savate	Savat	k1gInSc5	Savat
<g/>
,	,	kIx,	,
Box	box	k1gInSc4	box
<g/>
,	,	kIx,	,
Karate	karate	k1gNnSc4	karate
<g/>
,	,	kIx,	,
Jiu-jitsu	Jiuitsa	k1gFnSc4	Jiu-jitsa
<g/>
,	,	kIx,	,
Zápas	zápas	k1gInSc4	zápas
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
několik	několik	k4yIc4	několik
následujících	následující	k2eAgFnPc2d1	následující
<g/>
)	)	kIx)	)
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zástupce	zástupce	k1gMnSc1	zástupce
Brazilského	brazilský	k2eAgInSc2d1	brazilský
Jiu-jitsu	Jiuits	k1gInSc2	Jiu-jits
Royce	Royce	k1gFnSc2	Royce
Gracie	Gracie	k1gFnSc2	Gracie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgMnS	porazit
Petr	Petr	k1gMnSc1	Petr
Muab	Muab	k1gMnSc1	Muab
Baumrukr	Baumrukr	k1gMnSc1	Baumrukr
(	(	kIx(	(
<g/>
Savate	Savat	k1gInSc5	Savat
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
technice	technika	k1gFnSc3	technika
boje	boj	k1gInSc2	boj
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ukázal	ukázat	k5eAaPmAgInS	ukázat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
stylům	styl	k1gInPc3	styl
boje	boj	k1gInSc2	boj
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jim	on	k3xPp3gMnPc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
nebyly	být	k5eNaImAgFnP	být
schopné	schopný	k2eAgFnPc1d1	schopná
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
MMA	MMA	kA	MMA
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
MMA	MMA	kA	MMA
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bojovníci	bojovník	k1gMnPc1	bojovník
připravovaní	připravovaný	k2eAgMnPc1d1	připravovaný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
úrovních	úroveň	k1gFnPc6	úroveň
boje	boj	k1gInSc2	boj
–	–	k?	–
boj	boj	k1gInSc1	boj
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
boj	boj	k1gInSc4	boj
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
MMA	MMA	kA	MMA
nemá	mít	k5eNaImIp3nS	mít
byť	byť	k8xS	byť
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednodimenzionální	jednodimenzionální	k2eAgMnSc1d1	jednodimenzionální
bojovník	bojovník	k1gMnSc1	bojovník
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
elitních	elitní	k2eAgFnPc2d1	elitní
profesionálních	profesionální	k2eAgFnPc2d1	profesionální
soutěží	soutěž	k1gFnPc2	soutěž
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
americká	americký	k2eAgFnSc1d1	americká
UFC	UFC	kA	UFC
a	a	k8xC	a
Bellator	Bellator	k1gInSc1	Bellator
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
organizace	organizace	k1gFnSc1	organizace
Rizin	Rizin	k1gInSc4	Rizin
Fighting	Fighting	k1gInSc1	Fighting
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
M-1	M-1	k1gMnPc4	M-1
Global	globat	k5eAaImAgMnS	globat
a	a	k8xC	a
Absolute	Absolut	k1gInSc5	Absolut
Championship	Championship	k1gMnSc1	Championship
Berkut	Berkut	k1gMnSc1	Berkut
(	(	kIx(	(
<g/>
ACB	ACB	kA	ACB
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
asijská	asijský	k2eAgFnSc1d1	asijská
ONE	ONE	kA	ONE
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
bezpočet	bezpočet	k1gInSc1	bezpočet
menších	malý	k2eAgFnPc2d2	menší
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
automaticky	automaticky	k6eAd1	automaticky
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
horší	zlý	k2eAgInPc1d2	horší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnPc1d3	veliký
organizace	organizace	k1gFnPc1	organizace
XFN	XFN	kA	XFN
a	a	k8xC	a
Oktagon	Oktagon	k1gMnSc1	Oktagon
MMA	MMA	kA	MMA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bojuje	bojovat	k5eAaImIp3nS	bojovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oktagonu	oktagon	k1gInSc6	oktagon
(	(	kIx(	(
<g/>
osmiúhelník	osmiúhelník	k1gInSc4	osmiúhelník
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
pletivem	pletivo	k1gNnSc7	pletivo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
klec	klec	k1gFnSc1	klec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
občas	občas	k6eAd1	občas
v	v	k7c6	v
boxerském	boxerský	k2eAgInSc6d1	boxerský
ringu	ring	k1gInSc6	ring
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
profesionálních	profesionální	k2eAgInPc6d1	profesionální
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
bojuje	bojovat	k5eAaImIp3nS	bojovat
na	na	k7c4	na
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
kol	kolo	k1gNnPc2	kolo
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
daným	daný	k2eAgInSc7d1	daný
časovým	časový	k2eAgInSc7d1	časový
limitem	limit	k1gInSc7	limit
-	-	kIx~	-
3	[number]	k4	3
kola	kolo	k1gNnSc2	kolo
po	po	k7c6	po
5	[number]	k4	5
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
titulové	titulový	k2eAgInPc1d1	titulový
zápasy	zápas	k1gInPc1	zápas
5	[number]	k4	5
kol	kolo	k1gNnPc2	kolo
po	po	k7c6	po
5	[number]	k4	5
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
na	na	k7c6	na
amatérské	amatérský	k2eAgFnSc6d1	amatérská
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
bojují	bojovat	k5eAaImIp3nP	bojovat
3	[number]	k4	3
kola	kolo	k1gNnSc2	kolo
po	po	k7c6	po
3	[number]	k4	3
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Povinné	povinný	k2eAgInPc1d1	povinný
chrániče	chránič	k1gInPc1	chránič
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
speciální	speciální	k2eAgFnPc4d1	speciální
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
chránič	chránič	k1gInSc4	chránič
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
suspenzor	suspenzor	k1gInSc4	suspenzor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
–	–	k?	–
knockout	knockout	k1gMnSc1	knockout
(	(	kIx(	(
<g/>
K.O.	K.O.	k1gMnSc1	K.O.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdáním	vzdání	k1gNnSc7	vzdání
soupeře	soupeř	k1gMnSc2	soupeř
(	(	kIx(	(
<g/>
submission	submission	k1gInSc1	submission
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastavení	zastavení	k1gNnSc2	zastavení
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
-	-	kIx~	-
technické	technický	k2eAgFnSc2d1	technická
K.O.	K.O.	k1gFnSc2	K.O.
(	(	kIx(	(
<g/>
T.K.O.	T.K.O.	k1gMnSc1	T.K.O.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážnější	vážní	k2eAgNnSc1d2	vážnější
zranění	zranění	k1gNnSc1	zranění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
zápasu	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
injury	injura	k1gFnSc2	injura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
výhra	výhra	k1gFnSc1	výhra
na	na	k7c4	na
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
unanimous	unanimous	k1gInSc1	unanimous
decesion	decesion	k1gInSc1	decesion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
pravidla	pravidlo	k1gNnPc1	pravidlo
od	od	k7c2	od
každé	každý	k3xTgFnSc2	každý
profesionální	profesionální	k2eAgFnSc2d1	profesionální
soutěže	soutěž	k1gFnSc2	soutěž
mění	měnit	k5eAaImIp3nP	měnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skoro	skoro	k6eAd1	skoro
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
pravidlech	pravidlo	k1gNnPc6	pravidlo
zakázány	zakázat	k5eAaPmNgInP	zakázat
údery	úder	k1gInPc7	úder
do	do	k7c2	do
genitálií	genitálie	k1gFnPc2	genitálie
<g/>
,	,	kIx,	,
píchání	píchání	k1gNnPc2	píchání
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
údery	úder	k1gInPc4	úder
do	do	k7c2	do
temene	temeno	k1gNnSc2	temeno
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kolena	koleno	k1gNnSc2	koleno
a	a	k8xC	a
kopy	kopa	k1gFnSc2	kopa
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
soupeř	soupeř	k1gMnSc1	soupeř
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
země	zem	k1gFnPc4	zem
třemi	tři	k4xCgNnPc7	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
body	bod	k1gInPc4	bod
nebo	nebo	k8xC	nebo
páky	páka	k1gFnPc4	páka
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
klouby	kloub	k1gInPc4	kloub
(	(	kIx(	(
<g/>
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
zápěstí	zápěstí	k1gNnSc4	zápěstí
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
MMA	MMA	kA	MMA
bojovníci	bojovník	k1gMnPc1	bojovník
například	například	k6eAd1	například
nesmějí	smát	k5eNaImIp3nP	smát
držet	držet	k5eAaImF	držet
pletiva	pletivo	k1gNnSc2	pletivo
nebo	nebo	k8xC	nebo
soupeřových	soupeřův	k2eAgFnPc2d1	soupeřova
trenek	trenky	k1gFnPc2	trenky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dodržování	dodržování	k1gNnSc6	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
ringový	ringový	k2eAgMnSc1d1	ringový
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgMnSc1d1	přítomný
v	v	k7c6	v
oktagonu	oktagon	k1gInSc6	oktagon
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Zpoza	zpoza	k7c2	zpoza
oktagonu	oktagon	k1gInSc2	oktagon
vše	všechen	k3xTgNnSc4	všechen
sledují	sledovat	k5eAaImIp3nP	sledovat
bodoví	bodový	k2eAgMnPc1d1	bodový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ukončení	ukončení	k1gNnSc4	ukončení
zápasu	zápas	k1gInSc2	zápas
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
limitu	limit	k1gInSc2	limit
<g/>
,	,	kIx,	,
bodují	bodovat	k5eAaImIp3nP	bodovat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
zápas	zápas	k1gInSc4	zápas
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
světové	světový	k2eAgInPc4d1	světový
MMA	MMA	kA	MMA
bojovníky	bojovník	k1gMnPc4	bojovník
momentálně	momentálně	k6eAd1	momentálně
patří	patřit	k5eAaImIp3nS	patřit
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xC	jako
například	například	k6eAd1	například
Conor	Conor	k1gMnSc1	Conor
McGregor	McGregor	k1gMnSc1	McGregor
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
"	"	kIx"	"
<g/>
Bones	Bones	k1gMnSc1	Bones
<g/>
"	"	kIx"	"
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Ronda	ronda	k1gFnSc1	ronda
Rousey	Rousea	k1gFnSc2	Rousea
<g/>
,	,	kIx,	,
Cain	Cain	k1gMnSc1	Cain
Velasquez	Velasquez	k1gMnSc1	Velasquez
<g/>
,	,	kIx,	,
Anderson	Anderson	k1gMnSc1	Anderson
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
Alistair	Alistair	k1gInSc4	Alistair
Overeem	Overeum	k1gNnSc7	Overeum
nebo	nebo	k8xC	nebo
Robbie	Robbie	k1gFnSc1	Robbie
Lawler	Lawler	k1gInSc1	Lawler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc7	druh
bojovníků	bojovník	k1gMnPc2	bojovník
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Striker	Striker	k1gInSc1	Striker
–	–	k?	–
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
porazům	poraz	k1gInPc3	poraz
(	(	kIx(	(
<g/>
takedownům	takedowno	k1gNnPc3	takedowno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
bojuje	bojovat	k5eAaImIp3nS	bojovat
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
zápas	zápas	k1gInSc4	zápas
ukončit	ukončit	k5eAaPmF	ukončit
K.O	K.O	k1gFnSc4	K.O
nebo	nebo	k8xC	nebo
TKO	TKO	kA	TKO
(	(	kIx(	(
<g/>
technical	technicat	k5eAaPmAgMnS	technicat
knockout	knockout	k1gMnSc1	knockout
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Conor	Conor	k1gMnSc1	Conor
McGregor	McGregor	k1gMnSc1	McGregor
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Hunt	hunt	k1gInSc1	hunt
<g/>
,	,	kIx,	,
Stephen	Stephen	k1gInSc1	Stephen
Thompson	Thompsona	k1gFnPc2	Thompsona
nebo	nebo	k8xC	nebo
Darren	Darrna	k1gFnPc2	Darrna
Till	Tilla	k1gFnPc2	Tilla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Wrestler	Wrestler	k1gInSc1	Wrestler
–	–	k?	–
který	který	k3yRgMnSc1	který
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
porazy	poraz	k1gInPc4	poraz
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
soupeře	soupeř	k1gMnSc2	soupeř
dobít	dobít	k5eAaPmF	dobít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
Tyron	Tyron	k1gInSc4	Tyron
Woodley	Woodlea	k1gFnSc2	Woodlea
Cain	Caina	k1gFnPc2	Caina
Velasquez	Velasquez	k1gInSc1	Velasquez
<g/>
,	,	kIx,	,
Colby	Colba	k1gFnPc1	Colba
Covington	Covington	k1gInSc1	Covington
<g/>
,	,	kIx,	,
Demetrious	Demetrious	k1gMnSc1	Demetrious
Johnson	Johnson	k1gMnSc1	Johnson
nebo	nebo	k8xC	nebo
Henry	henry	k1gInSc1	henry
Cejudo	Cejudo	k1gNnSc1	Cejudo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Groundfighter	Groundfighter	k1gMnSc1	Groundfighter
/	/	kIx~	/
Grappler	Grappler	k1gMnSc1	Grappler
/	/	kIx~	/
Jiu-Jitsu	Jiu-Jits	k1gInSc2	Jiu-Jits
fighter	fighter	k1gMnSc1	fighter
–	–	k?	–
který	který	k3yRgInSc4	který
boj	boj	k1gInSc4	boj
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
vynutí	vynutit	k5eAaPmIp3nS	vynutit
vzdání	vzdání	k1gNnSc4	vzdání
soupeře	soupeř	k1gMnSc2	soupeř
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pákou	páka	k1gFnSc7	páka
nebo	nebo	k8xC	nebo
škrcením	škrcení	k1gNnSc7	škrcení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Anderson	Anderson	k1gMnSc1	Anderson
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
Royce	Royce	k1gFnSc1	Royce
Gracie	Gracie	k1gFnSc1	Gracie
<g/>
,	,	kIx,	,
Demian	Demian	k1gMnSc1	Demian
Maia	Maia	k1gMnSc1	Maia
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Ortega	Ortega	k1gFnSc1	Ortega
nebo	nebo	k8xC	nebo
Chabib	Chabib	k1gInSc1	Chabib
Nurmagomedov	Nurmagomedovo	k1gNnPc2	Nurmagomedovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
==	==	k?	==
</s>
</p>
<p>
<s>
Zápasí	zápasit	k5eAaImIp3nS	zápasit
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
váhových	váhový	k2eAgFnPc6d1	váhová
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
MMA	MMA	kA	MMA
jako	jako	k8xS	jako
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
jiném	jiný	k2eAgInSc6d1	jiný
bojovém	bojový	k2eAgInSc6d1	bojový
sportu	sport	k1gInSc6	sport
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
udržování	udržování	k1gNnSc4	udržování
si	se	k3xPyFc3	se
váhu	váha	k1gFnSc4	váha
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kategorie	kategorie	k1gFnSc1	kategorie
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
šampiona	šampion	k1gMnSc2	šampion
<g/>
,	,	kIx,	,
následovaného	následovaný	k2eAgMnSc2d1	následovaný
zástupem	zástupem	k6eAd1	zástupem
vyzyvatelů	vyzyvatelů	k?	vyzyvatelů
(	(	kIx(	(
<g/>
contenders	contenders	k1gInSc1	contenders
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
zápasí	zápasit	k5eAaImIp3nS	zápasit
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
vyzvat	vyzvat	k5eAaPmF	vyzvat
právě	právě	k9	právě
šampiona	šampion	k1gMnSc4	šampion
na	na	k7c4	na
titulový	titulový	k2eAgInSc4d1	titulový
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
výhry	výhra	k1gFnSc2	výhra
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
stát	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
mužské	mužský	k2eAgFnPc1d1	mužská
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
muší	muší	k2eAgFnSc1d1	muší
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
flyweight	flyweight	k1gInSc1	flyweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
56	[number]	k4	56
kg	kg	kA	kg
</s>
</p>
<p>
<s>
bantamová	bantamový	k2eAgFnSc1d1	bantamová
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
bantamweight	bantamweight	k1gInSc1	bantamweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
61	[number]	k4	61
kg	kg	kA	kg
</s>
</p>
<p>
<s>
pérová	pérový	k2eAgFnSc1d1	pérová
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
featherweight	featherweight	k1gInSc1	featherweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
66	[number]	k4	66
kg	kg	kA	kg
</s>
</p>
<p>
<s>
lehká	lehký	k2eAgFnSc1d1	lehká
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
lightweight	lightweight	k1gInSc1	lightweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
70	[number]	k4	70
kg	kg	kA	kg
</s>
</p>
<p>
<s>
welterová	welterový	k2eAgFnSc1d1	welterová
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
welterweight	welterweight	k1gInSc1	welterweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
77	[number]	k4	77
kg	kg	kA	kg
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
middleweight	middleweight	k1gInSc1	middleweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
84	[number]	k4	84
kg	kg	kA	kg
</s>
</p>
<p>
<s>
polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
light-heavyweight	lighteavyweight	k1gInSc1	light-heavyweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
93	[number]	k4	93
kg	kg	kA	kg
</s>
</p>
<p>
<s>
těžká	těžký	k2eAgFnSc1d1	těžká
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
heavyweight	heavyweight	k1gInSc1	heavyweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
120	[number]	k4	120
kgLze	kgLze	k6eAd1	kgLze
také	také	k9	také
zápasit	zápasit	k5eAaImF	zápasit
ve	v	k7c6	v
smluvní	smluvní	k2eAgFnSc6d1	smluvní
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
catchweight	catchweight	k1gInSc1	catchweight
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
soupeři	soupeř	k1gMnPc1	soupeř
domluví	domluvit	k5eAaPmIp3nP	domluvit
na	na	k7c6	na
požadované	požadovaný	k2eAgFnSc6d1	požadovaná
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženské	ženská	k1gFnPc1	ženská
váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
women	women	k2eAgInSc1d1	women
strawweight	strawweight	k1gInSc1	strawweight
-	-	kIx~	-
do	do	k7c2	do
52	[number]	k4	52
kg	kg	kA	kg
</s>
</p>
<p>
<s>
bantamová	bantamový	k2eAgFnSc1d1	bantamová
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
women	women	k2eAgInSc1d1	women
bantamweight	bantamweight	k1gInSc1	bantamweight
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
61	[number]	k4	61
kg	kg	kA	kg
</s>
</p>
<p>
<s>
featherweight	featherweight	k1gInSc1	featherweight
(	(	kIx(	(
<g/>
pérová	pérový	k2eAgFnSc1d1	pérová
váha	váha	k1gFnSc1	váha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Oblečení	oblečení	k1gNnSc1	oblečení
a	a	k8xC	a
vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
MMA	MMA	kA	MMA
bojovníci	bojovník	k1gMnPc1	bojovník
standardně	standardně	k6eAd1	standardně
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
organizací	organizace	k1gFnPc2	organizace
nosí	nosit	k5eAaImIp3nS	nosit
šortky	šortky	k1gFnPc4	šortky
nebo	nebo	k8xC	nebo
trenky	trenky	k1gFnPc4	trenky
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
i	i	k8xC	i
kraťasy	kraťas	k1gInPc4	kraťas
thaiboxerského	thaiboxerský	k2eAgInSc2d1	thaiboxerský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
menších	malý	k2eAgFnPc6d2	menší
organizacích	organizace	k1gFnPc6	organizace
jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgInPc1d1	povolen
slipy	slip	k1gInPc1	slip
nebo	nebo	k8xC	nebo
přiléhavé	přiléhavý	k2eAgNnSc4d1	přiléhavé
trička	tričko	k1gNnPc1	tričko
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rash	rash	k1gMnSc1	rash
guard	guard	k1gMnSc1	guard
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
navíc	navíc	k6eAd1	navíc
nosí	nosit	k5eAaImIp3nP	nosit
sportovní	sportovní	k2eAgFnSc4d1	sportovní
podprsenku	podprsenka	k1gFnSc4	podprsenka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
povinné	povinný	k2eAgFnPc4d1	povinná
ochranné	ochranný	k2eAgFnPc4d1	ochranná
pomůcky	pomůcka	k1gFnPc4	pomůcka
jako	jako	k8xC	jako
suspenzor	suspenzor	k1gInSc4	suspenzor
a	a	k8xC	a
chránič	chránič	k1gInSc4	chránič
na	na	k7c4	na
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zápasení	zápasení	k1gNnSc6	zápasení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgFnSc2d1	speciální
bezprsté	bezprstý	k2eAgFnSc2d1	bezprstý
MMA	MMA	kA	MMA
graplingové	graplingový	k2eAgFnSc2d1	graplingový
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lepší	dobrý	k2eAgInSc4d2	lepší
úchop	úchop	k1gInSc4	úchop
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
organizacích	organizace	k1gFnPc6	organizace
jsou	být	k5eAaImIp3nP	být
povolené	povolený	k2eAgInPc1d1	povolený
i	i	k8xC	i
thaiboxerské	thaiboxerský	k2eAgInPc1d1	thaiboxerský
návleky	návlek	k1gInPc1	návlek
na	na	k7c4	na
kotníky	kotník	k1gInPc4	kotník
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc2d1	Česká
MMA	MMA	kA	MMA
==	==	k?	==
</s>
</p>
<p>
<s>
Známí	známit	k5eAaImIp3nP	známit
čeští	český	k2eAgMnPc1d1	český
MMA	MMA	kA	MMA
bojovníciKarlos	bojovníciKarlos	k1gMnSc1	bojovníciKarlos
"	"	kIx"	"
<g/>
Terminátor	terminátor	k1gMnSc1	terminátor
<g/>
"	"	kIx"	"
Vémola	Vémola	k1gFnSc1	Vémola
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Shootfighters	Shootfightersa	k1gFnPc2	Shootfightersa
<g/>
)	)	kIx)	)
-	-	kIx~	-
mistr	mistr	k1gMnSc1	mistr
tří	tři	k4xCgFnPc2	tři
organizací	organizace	k1gFnPc2	organizace
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
váze	váha	k1gFnSc6	váha
(	(	kIx(	(
<g/>
GCF	GCF	kA	GCF
<g/>
,	,	kIx,	,
MMAA	MMAA	kA	MMAA
<g/>
,	,	kIx,	,
UCMMA	UCMMA	kA	UCMMA
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
v	v	k7c6	v
polotěžké	polotěžký	k2eAgInPc1d1	polotěžký
(	(	kIx(	(
<g/>
OKTAGON	OKTAGON	kA	OKTAGON
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
UFC	UFC	kA	UFC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Pešta	Pešta	k1gMnSc1	Pešta
(	(	kIx(	(
<g/>
Penta	Penta	k1gFnSc1	Penta
Gym	Gym	k1gFnSc1	Gym
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
-	-	kIx~	-
bojovník	bojovník	k1gMnSc1	bojovník
těžké	těžký	k2eAgFnSc2d1	těžká
váhy	váha	k1gFnSc2	váha
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
UFC	UFC	kA	UFC
<g/>
.	.	kIx.	.
</s>
<s>
Čelil	čelit	k5eAaImAgMnS	čelit
těm	ten	k3xDgMnPc3	ten
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
bojovníkům	bojovník	k1gMnPc3	bojovník
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Derrick	Derrick	k1gInSc4	Derrick
Lewis	Lewis	k1gFnSc2	Lewis
který	který	k3yRgInSc4	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
těžké	těžký	k2eAgFnSc2d1	těžká
váhy	váha	k1gFnSc2	váha
UFC	UFC	kA	UFC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
"	"	kIx"	"
<g/>
Denisa	Denisa	k1gFnSc1	Denisa
<g/>
"	"	kIx"	"
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
Jetsaam	Jetsaam	k1gInSc1	Jetsaam
Gym	Gym	k1gFnSc2	Gym
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
-	-	kIx~	-
bojovník	bojovník	k1gMnSc1	bojovník
polotěžké	polotěžký	k2eAgFnSc2d1	polotěžká
váhy	váha	k1gFnSc2	váha
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
zápasí	zápasit	k5eAaImIp3nS	zápasit
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
organizaci	organizace	k1gFnSc6	organizace
Rizin	Rizin	k2eAgInSc1d1	Rizin
Fighting	Fighting	k1gInSc1	Fighting
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
šampionem	šampion	k1gMnSc7	šampion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Pudilová	Pudilová	k1gFnSc1	Pudilová
(	(	kIx(	(
<g/>
KBC	KBC	kA	KBC
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
v	v	k7c6	v
UFC	UFC	kA	UFC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Šormová	Šormová	k1gFnSc1	Šormová
(	(	kIx(	(
<g/>
Penta	Penta	k1gFnSc1	Penta
Gym	Gym	k1gFnSc1	Gym
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Běle	běle	k6eAd1wB	běle
(	(	kIx(	(
<g/>
Penta	Penta	k1gFnSc1	Penta
Gym	Gym	k1gFnSc1	Gym
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
-	-	kIx~	-
finalista	finalista	k1gMnSc1	finalista
Okragon	Okragon	k1gMnSc1	Okragon
výzva	výzva	k1gFnSc1	výzva
a	a	k8xC	a
XFN	XFN	kA	XFN
veterán	veterán	k1gMnSc1	veterán
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ohledně	ohledně	k7c2	ohledně
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
MMA	MMA	kA	MMA
panuje	panovat	k5eAaImIp3nS	panovat
stále	stále	k6eAd1	stále
kontroverze	kontroverze	k1gFnSc1	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
nedostatek	nedostatek	k1gInSc1	nedostatek
sběru	sběr	k1gInSc2	sběr
a	a	k8xC	a
přístupnosti	přístupnost	k1gFnSc2	přístupnost
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
zraněních	zranění	k1gNnPc6	zranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
lidí	člověk	k1gMnPc2	člověk
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
MMA	MMA	kA	MMA
sport	sport	k1gInSc1	sport
jen	jen	k9	jen
pro	pro	k7c4	pro
hloupější	hloupý	k2eAgMnPc4d2	hloupější
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
tréninku	trénink	k1gInSc6	trénink
MMA	MMA	kA	MMA
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
učitele	učitel	k1gMnPc4	učitel
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
právníky	právník	k1gMnPc7	právník
nebo	nebo	k8xC	nebo
studenty	student	k1gMnPc7	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zranění	zranění	k1gNnSc4	zranění
tu	tu	k6eAd1	tu
samozřejmě	samozřejmě	k6eAd1	samozřejmě
najdeme	najít	k5eAaPmIp1nP	najít
ale	ale	k9	ale
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
čii	čii	k?	čii
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
čím	čí	k3xOyRgNnSc6	čí
dál	daleko	k6eAd2	daleko
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
kvůli	kvůli	k7c3	kvůli
vzrůstající	vzrůstající	k2eAgFnSc3d1	vzrůstající
oblibě	obliba	k1gFnSc3	obliba
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc4	vylepšení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
i	i	k9	i
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
přípravky	přípravek	k1gInPc1	přípravek
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
(	(	kIx(	(
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
MMA	MMA	kA	MMA
<g/>
)	)	kIx)	)
dělá	dělat	k5eAaImIp3nS	dělat
s	s	k7c7	s
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
dbáme	dbát	k5eAaImIp1nP	dbát
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
trenéra	trenér	k1gMnSc2	trenér
zranění	zranění	k1gNnSc2	zranění
jsou	být	k5eAaImIp3nP	být
minimální	minimální	k2eAgFnPc1d1	minimální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Četnost	četnost	k1gFnSc1	četnost
zranění	zranění	k1gNnSc2	zranění
===	===	k?	===
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
poslední	poslední	k2eAgFnSc2d1	poslední
meta-analýzy	metanalýza	k1gFnSc2	meta-analýza
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
zraněních	zranění	k1gNnPc6	zranění
v	v	k7c6	v
MMA	MMA	kA	MMA
je	být	k5eAaImIp3nS	být
četnost	četnost	k1gFnSc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
228.7	[number]	k4	228.7
zranění	zranění	k1gNnPc2	zranění
na	na	k7c4	na
1000	[number]	k4	1000
vystoupení	vystoupení	k1gNnPc2	vystoupení
atleta	atlet	k1gMnSc2	atlet
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
atleta	atlet	k1gMnSc2	atlet
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
atlet	atlet	k1gMnSc1	atlet
vystupující	vystupující	k2eAgMnSc1d1	vystupující
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
boji	boj	k1gInSc6	boj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc1	druh
zranění	zranění	k1gNnSc1	zranění
===	===	k?	===
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc1	druh
zranění	zranění	k1gNnSc1	zranění
podobné	podobný	k2eAgFnPc1d1	podobná
zraněním	zranění	k1gNnSc7	zranění
v	v	k7c6	v
boxu	box	k1gInSc6	box
<g/>
,	,	kIx,	,
kickboxu	kickbox	k1gInSc6	kickbox
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc6	karate
<g/>
,	,	kIx,	,
taekwondu	taekwondo	k1gNnSc6	taekwondo
ale	ale	k8xC	ale
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
bojových	bojový	k2eAgInPc2d1	bojový
sportů	sport	k1gInPc2	sport
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
judo	judo	k1gNnSc4	judo
a	a	k8xC	a
šerm	šerm	k1gInSc4	šerm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Neoficiální	neoficiální	k2eAgInPc1d1	neoficiální
zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
Známá	známá	k1gFnSc1	známá
úmrtí	úmrtí	k1gNnSc2	úmrtí
při	při	k7c6	při
neoficiálních	oficiální	k2eNgNnPc6d1	neoficiální
kláních	klání	k1gNnPc6	klání
MMA	MMA	kA	MMA
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
</s>
</p>
<p>
<s>
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
bez	bez	k7c2	bez
vypsaných	vypsaný	k2eAgNnPc2d1	vypsané
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
pauz	pauza	k1gFnPc2	pauza
atletů	atlet	k1gMnPc2	atlet
které	který	k3yIgFnPc4	který
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
MMA	MMA	kA	MMA
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alfredo	Alfredo	k1gNnSc1	Alfredo
Castro	Castro	k1gNnSc1	Castro
Herrera	Herrera	k1gFnSc1	Herrera
<g/>
,	,	kIx,	,
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
po	po	k7c6	po
knock-outu	knockut	k1gMnSc6	knock-out
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Douglas	Douglas	k1gInSc1	Douglas
Dedge	Dedg	k1gFnSc2	Dedg
<g/>
,	,	kIx,	,
31	[number]	k4	31
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
květen	květen	k1gInSc4	květen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Samsong-dong	Samsongong	k1gInSc1	Samsong-dong
(	(	kIx(	(
<g/>
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mike	Mike	k1gFnSc1	Mike
Mittelmeier	Mittelmeira	k1gFnPc2	Mittelmeira
<g/>
,	,	kIx,	,
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dustin	Dustin	k1gMnSc1	Dustin
Jenson	Jenson	k1gMnSc1	Jenson
<g/>
,	,	kIx,	,
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podlebeční	podlebeční	k2eAgNnSc4d1	podlebeční
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Pablo	Pablo	k1gNnSc4	Pablo
Elochukwu	Elochukwus	k1gInSc2	Elochukwus
<g/>
,	,	kIx,	,
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Nigerijec	Nigerijec	k1gMnSc1	Nigerijec
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Port	port	k1gInSc1	port
Huron	Huron	k1gInSc1	Huron
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hypoglykemický	hypoglykemický	k2eAgInSc1d1	hypoglykemický
šok	šok	k1gInSc1	šok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ramin	Ramin	k2eAgInSc1d1	Ramin
Zeynalov	Zeynalov	k1gInSc1	Zeynalov
<g/>
,	,	kIx,	,
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Ázerbájdžánec	Ázerbájdžánec	k1gMnSc1	Ázerbájdžánec
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jameston	Jameston	k1gInSc1	Jameston
Lee-Yaw	Lee-Yaw	k1gFnSc2	Lee-Yaw
<g/>
,	,	kIx,	,
47	[number]	k4	47
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
Trinidadu	Trinidad	k1gInSc2	Trinidad
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Aberdeen	Aberdeen	k1gInSc1	Aberdeen
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc1	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Joã	Joã	k?	Joã
Carvalho	Carval	k1gMnSc2	Carval
<g/>
,	,	kIx,	,
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Portugalec	Portugalec	k1gMnSc1	Portugalec
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
úmrtí	úmrtí	k1gNnSc3	úmrtí
2	[number]	k4	2
dny	den	k1gInPc4	den
po	po	k7c6	po
knock-outu	knockut	k1gMnSc6	knock-out
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nebyla	být	k5eNaImAgFnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pořadateli	pořadatel	k1gMnSc3	pořadatel
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
ošetřovatelská	ošetřovatelský	k2eAgFnSc1d1	ošetřovatelská
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
zápasy	zápas	k1gInPc1	zápas
neměly	mít	k5eNaImAgInP	mít
striktní	striktní	k2eAgNnPc1d1	striktní
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
povinných	povinný	k2eAgFnPc2d1	povinná
pauz	pauza	k1gFnPc2	pauza
atletů	atlet	k1gMnPc2	atlet
nebo	nebo	k8xC	nebo
také	také	k9	také
byly	být	k5eAaImAgInP	být
povoleny	povolen	k2eAgInPc1d1	povolen
zakázané	zakázaný	k2eAgInPc1d1	zakázaný
údery	úder	k1gInPc1	úder
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
které	který	k3yIgFnSc3	který
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
MMA	MMA	kA	MMA
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mixed	mixed	k1gMnSc1	mixed
martial	martiat	k5eAaImAgInS	martiat
arts	arts	k1gInSc4	arts
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c4	o
MMA	MMA	kA	MMA
extraround	extraround	k1gInSc4	extraround
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c6	o
MMA	MMA	kA	MMA
fightnews	fightnews	k6eAd1	fightnews
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c6	o
MMA	MMA	kA	MMA
mma	mma	k?	mma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
web	web	k1gInSc1	web
o	o	k7c4	o
Československej	Československej	k?	Československej
MMA	MMA	kA	MMA
súťaži	súťazat	k5eAaPmIp1nS	súťazat
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
<g/>
aktuality	aktualita	k1gFnPc1	aktualita
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
