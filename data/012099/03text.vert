<p>
<s>
Malvice	malvice	k1gFnSc1	malvice
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
pomum	pomum	k1gNnSc1	pomum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
nepravého	pravý	k2eNgInSc2d1	nepravý
dužnatého	dužnatý	k2eAgInSc2d1	dužnatý
nepukavého	pukavý	k2eNgInSc2d1	nepukavý
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Malvice	malvice	k1gFnSc1	malvice
vzniká	vznikat	k5eAaImIp3nS	vznikat
ze	z	k7c2	z
spodního	spodní	k2eAgInSc2d1	spodní
nebo	nebo	k8xC	nebo
výjimečně	výjimečně	k6eAd1	výjimečně
z	z	k7c2	z
polospodního	polospodní	k2eAgInSc2d1	polospodní
gynecea	gynece	k1gInSc2	gynece
tvořeného	tvořený	k2eAgInSc2d1	tvořený
několika	několik	k4yIc7	několik
do	do	k7c2	do
různé	různý	k2eAgFnSc2d1	různá
míry	míra	k1gFnSc2	míra
srostlými	srostlý	k2eAgInPc7d1	srostlý
plodolisty	plodolist	k1gInPc7	plodolist
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
morfologického	morfologický	k2eAgNnSc2d1	morfologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
jádřince	jádřinec	k1gInSc2	jádřinec
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
z	z	k7c2	z
pouzder	pouzdro	k1gNnPc2	pouzdro
semeníku	semeník	k1gInSc2	semeník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dužnaté	dužnatý	k2eAgFnPc1d1	dužnatá
části	část	k1gFnPc1	část
plodu	plod	k1gInSc2	plod
se	se	k3xPyFc4	se
formují	formovat	k5eAaImIp3nP	formovat
až	až	k9	až
po	po	k7c6	po
opylení	opylení	k1gNnSc2	opylení
dužnatěním	dužnatění	k1gNnSc7	dužnatění
češule	češule	k1gFnSc2	češule
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
<g/>
Semena	semeno	k1gNnSc2	semeno
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
gynecea	gynece	k1gInSc2	gynece
jsou	být	k5eAaImIp3nP	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
suchomázdřitým	suchomázdřitý	k2eAgInSc7d1	suchomázdřitý
endokarpem	endokarp	k1gInSc7	endokarp
tvořícím	tvořící	k2eAgInSc7d1	tvořící
tzv.	tzv.	kA	tzv.
jaderník	jaderník	k1gInSc1	jaderník
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mezokarpem	mezokarp	k1gInSc7	mezokarp
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytnělým	zbytnělý	k2eAgNnSc7d1	zbytnělé
květním	květní	k2eAgNnSc7d1	květní
lůžkem	lůžko	k1gNnSc7	lůžko
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
tvořícími	tvořící	k2eAgFnPc7d1	tvořící
dužninu	dužnina	k1gFnSc4	dužnina
a	a	k8xC	a
povrch	povrch	k1gInSc4	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
exokarp	exokarp	k1gInSc1	exokarp
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
tuhé	tuhý	k2eAgFnSc2d1	tuhá
blány	blána	k1gFnSc2	blána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
plodu	plod	k1gInSc2	plod
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
stopka	stopka	k1gFnSc1	stopka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zaschlé	zaschlý	k2eAgInPc1d1	zaschlý
zbytky	zbytek	k1gInPc1	zbytek
kalichu	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malvice	malvice	k1gFnPc1	malvice
jsou	být	k5eAaImIp3nP	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
plodem	plod	k1gInSc7	plod
některých	některý	k3yIgInPc2	některý
rodů	rod	k1gInPc2	rod
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgInPc2d1	růžovitý
(	(	kIx(	(
<g/>
Rosaceae	Rosaceae	k1gNnPc2	Rosaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
řazených	řazený	k2eAgFnPc2d1	řazená
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
Maloideae	Maloidea	k1gFnSc2	Maloidea
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
taxonomii	taxonomie	k1gFnSc6	taxonomie
součásti	součást	k1gFnSc2	součást
podčeledi	podčeleď	k1gFnSc2	podčeleď
Spiraeoideae	Spiraeoidea	k1gFnSc2	Spiraeoidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
známých	známý	k2eAgFnPc2d1	známá
ovocných	ovocný	k2eAgFnPc2d1	ovocná
dřevin	dřevina	k1gFnPc2	dřevina
(	(	kIx(	(
<g/>
jabloň	jabloň	k1gFnSc1	jabloň
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
malvice	malvice	k1gFnPc1	malvice
např.	např.	kA	např.
jeřáb	jeřáb	k1gInSc1	jeřáb
<g/>
,	,	kIx,	,
hloh	hloh	k1gInSc1	hloh
<g/>
,	,	kIx,	,
mišpule	mišpule	k1gFnSc1	mišpule
<g/>
,	,	kIx,	,
mišpulník	mišpulník	k1gMnSc1	mišpulník
<g/>
,	,	kIx,	,
temnoplodec	temnoplodec	k1gMnSc1	temnoplodec
<g/>
,	,	kIx,	,
kdouloň	kdouloň	k1gFnSc1	kdouloň
<g/>
,	,	kIx,	,
kdoulovec	kdoulovec	k1gInSc1	kdoulovec
<g/>
,	,	kIx,	,
hlohyně	hlohyně	k1gFnSc1	hlohyně
<g/>
,	,	kIx,	,
blýskalka	blýskalka	k1gFnSc1	blýskalka
(	(	kIx(	(
<g/>
Photinia	Photinium	k1gNnSc2	Photinium
<g/>
)	)	kIx)	)
a	a	k8xC	a
muchovník	muchovník	k1gInSc4	muchovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jablko	jablko	k1gNnSc1	jablko
</s>
</p>
<p>
<s>
Generativní	generativní	k2eAgInPc1d1	generativní
orgány	orgán	k1gInPc1	orgán
</s>
</p>
<p>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
</s>
</p>
<p>
<s>
Ovocnářství	ovocnářství	k1gNnSc1	ovocnářství
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
(	(	kIx(	(
<g/>
botanika	botanika	k1gFnSc1	botanika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Malvice	malvice	k1gFnSc2	malvice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
slovník	slovník	k1gInSc1	slovník
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
anatomie	anatomie	k1gFnSc2	anatomie
PřF	PřF	k1gFnSc2	PřF
UK	UK	kA	UK
</s>
</p>
<p>
<s>
Generativní	generativní	k2eAgInPc1d1	generativní
orgány	orgán	k1gInPc1	orgán
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
VŠCHT	VŠCHT	kA	VŠCHT
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Farabee	Farabee	k1gFnSc1	Farabee
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
J.	J.	kA	J.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Plants	Plants	k1gInSc1	Plants
and	and	k?	and
their	their	k1gInSc1	their
structure	structur	k1gMnSc5	structur
<g/>
"	"	kIx"	"
Estrella	Estrella	k1gMnSc1	Estrella
Mountain	Mountain	k1gMnSc1	Mountain
Community	Communit	k2eAgFnPc4d1	Communit
College	Colleg	k1gFnPc4	Colleg
<g/>
,	,	kIx,	,
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
</s>
</p>
<p>
<s>
Botanical	Botanicat	k5eAaPmAgMnS	Botanicat
Visual	Visual	k1gMnSc1	Visual
Glossary	Glossara	k1gFnSc2	Glossara
</s>
</p>
