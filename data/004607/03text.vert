<s>
Pelyněk	pelyněk	k1gInSc1	pelyněk
estragon	estragon	k1gInSc1	estragon
(	(	kIx(	(
<g/>
Artemisia	Artemisia	k1gFnSc1	Artemisia
dracunculus	dracunculus	k1gMnSc1	dracunculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
pelyněk	pelyněk	k1gInSc1	pelyněk
kozalec	kozalec	k1gInSc1	kozalec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
(	(	kIx(	(
<g/>
Asteraceae	Asteracea	k1gFnSc2	Asteracea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
jako	jako	k8xS	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
též	též	k9	též
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
60	[number]	k4	60
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přímou	přímý	k2eAgFnSc4d1	přímá
lodyhu	lodyha	k1gFnSc4	lodyha
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
prutnatě	prutnatě	k6eAd1	prutnatě
větvenou	větvený	k2eAgFnSc4d1	větvená
a	a	k8xC	a
celokrajnými	celokrajný	k2eAgFnPc7d1	celokrajná
<g/>
,	,	kIx,	,
nedělenými	dělený	k2eNgInPc7d1	nedělený
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
čárkovitými	čárkovitý	k2eAgInPc7d1	čárkovitý
kopinatými	kopinatý	k2eAgInPc7d1	kopinatý
přisedlými	přisedlý	k2eAgInPc7d1	přisedlý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
jsou	být	k5eAaImIp3nP	být
žlutokvěté	žlutokvětý	k2eAgInPc1d1	žlutokvětý
<g/>
,	,	kIx,	,
stopkaté	stopkatý	k2eAgInPc1d1	stopkatý
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Estragon	estragon	k1gInSc1	estragon
se	se	k3xPyFc4	se
vysévá	vysévat	k5eAaImIp3nS	vysévat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
do	do	k7c2	do
truhlíčků	truhlíček	k1gInPc2	truhlíček
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
okenní	okenní	k2eAgFnSc6d1	okenní
římse	římsa	k1gFnSc6	římsa
a	a	k8xC	a
sazenice	sazenice	k1gFnPc4	sazenice
se	se	k3xPyFc4	se
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
do	do	k7c2	do
záhonu	záhon	k1gInSc2	záhon
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
na	na	k7c4	na
slunné	slunný	k2eAgNnSc4d1	slunné
a	a	k8xC	a
teplé	teplý	k2eAgNnSc4d1	teplé
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
v	v	k7c4	v
humózní	humózní	k2eAgNnSc4d1	humózní
a	a	k8xC	a
živinami	živina	k1gFnPc7	živina
bohaté	bohatý	k2eAgFnSc3d1	bohatá
půdě	půda	k1gFnSc3	půda
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
a	a	k8xC	a
chráněných	chráněný	k2eAgFnPc6d1	chráněná
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
vegetativně	vegetativně	k6eAd1	vegetativně
dělením	dělení	k1gNnSc7	dělení
starých	starý	k2eAgInPc2d1	starý
trsů	trs	k1gInPc2	trs
nebo	nebo	k8xC	nebo
odnožemi	odnož	k1gFnPc7	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sklizni	sklizeň	k1gFnSc6	sklizeň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
provádět	provádět	k5eAaImF	provádět
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
uřezáváme	uřezávat	k5eAaImIp1nP	uřezávat
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vrcholky	vrcholek	k1gInPc4	vrcholek
trsu	trs	k1gInSc2	trs
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
rozvijí	rozvít	k5eAaPmIp3nP	rozvít
poupata	poupě	k1gNnPc4	poupě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvíce	hodně	k6eAd3	hodně
silice	silice	k1gFnSc1	silice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
koření	kořenit	k5eAaImIp3nS	kořenit
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
aromatickou	aromatický	k2eAgFnSc4d1	aromatická
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
ostrou	ostrý	k2eAgFnSc4d1	ostrá
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
nahořklou	nahořklý	k2eAgFnSc4d1	nahořklá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
záhonu	záhon	k1gInSc6	záhon
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vydatnou	vydatný	k2eAgFnSc4d1	vydatná
zálivku	zálivka	k1gFnSc4	zálivka
<g/>
,	,	kIx,	,
v	v	k7c6	v
květináči	květináč	k1gInSc6	květináč
ale	ale	k8xC	ale
nesnáší	snášet	k5eNaImIp3nS	snášet
přelití	přelití	k1gNnSc1	přelití
<g/>
.	.	kIx.	.
</s>
<s>
Prospívá	prospívat	k5eAaImIp3nS	prospívat
mu	on	k3xPp3gMnSc3	on
přihnojení	přihnojení	k1gNnSc1	přihnojení
kopřivovým	kopřivový	k2eAgInSc7d1	kopřivový
výluhem	výluh	k1gInSc7	výluh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
chráníme	chránit	k5eAaImIp1nP	chránit
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
seřezané	seřezaný	k2eAgInPc4d1	seřezaný
asi	asi	k9	asi
6	[number]	k4	6
cm	cm	kA	cm
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
přikrytím	přikrytí	k1gNnSc7	přikrytí
slámou	sláma	k1gFnSc7	sláma
nebo	nebo	k8xC	nebo
listím	listí	k1gNnSc7	listí
před	před	k7c7	před
vymrzáním	vymrzání	k1gNnSc7	vymrzání
<g/>
.	.	kIx.	.
</s>
<s>
Sušení	sušení	k1gNnPc1	sušení
musíme	muset	k5eAaImIp1nP	muset
provádět	provádět	k5eAaImF	provádět
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
nesmí	smět	k5eNaImIp3nS	smět
přestoupit	přestoupit	k5eAaPmF	přestoupit
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
aroma	aroma	k1gNnSc1	aroma
a	a	k8xC	a
rostlina	rostlina	k1gFnSc1	rostlina
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
zahrádce	zahrádka	k1gFnSc6	zahrádka
nebo	nebo	k8xC	nebo
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
používat	používat	k5eAaImF	používat
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Estragon	estragon	k1gInSc1	estragon
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
častou	častý	k2eAgFnSc7d1	častá
součástí	součást	k1gFnSc7	součást
pokrmů	pokrm	k1gInPc2	pokrm
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
Arménů	Armén	k1gMnPc2	Armén
a	a	k8xC	a
Turků	Turek	k1gMnPc2	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
estragonu	estragon	k1gInSc2	estragon
mají	mít	k5eAaImIp3nP	mít
jemnou	jemný	k2eAgFnSc4d1	jemná
hořkosladkou	hořkosladký	k2eAgFnSc4d1	hořkosladká
kořeněnou	kořeněný	k2eAgFnSc4d1	kořeněná
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
aromatizování	aromatizování	k1gNnSc3	aromatizování
vinných	vinný	k2eAgInPc2d1	vinný
nebo	nebo	k8xC	nebo
ovocných	ovocný	k2eAgInPc2d1	ovocný
octů	ocet	k1gInPc2	ocet
<g/>
,	,	kIx,	,
bylinkových	bylinkový	k2eAgNnPc2d1	bylinkové
másel	máslo	k1gNnPc2	máslo
<g/>
,	,	kIx,	,
do	do	k7c2	do
nádivek	nádivka	k1gFnPc2	nádivka
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
k	k	k7c3	k
pečeným	pečený	k2eAgNnPc3d1	pečené
a	a	k8xC	a
dušeným	dušený	k2eAgNnPc3d1	dušené
masům	maso	k1gNnPc3	maso
<g/>
,	,	kIx,	,
do	do	k7c2	do
omáček	omáčka	k1gFnPc2	omáčka
<g/>
,	,	kIx,	,
při	při	k7c6	při
nakládání	nakládání	k1gNnSc6	nakládání
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
estragonové	estragonový	k2eAgFnSc2d1	estragonová
hořčice	hořčice	k1gFnSc2	hořčice
<g/>
,	,	kIx,	,
při	při	k7c6	při
nakládání	nakládání	k1gNnSc6	nakládání
okurek	okurka	k1gFnPc2	okurka
<g/>
,	,	kIx,	,
do	do	k7c2	do
čínských	čínský	k2eAgInPc2d1	čínský
a	a	k8xC	a
francouzských	francouzský	k2eAgInPc2d1	francouzský
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
k	k	k7c3	k
rýži	rýže	k1gFnSc3	rýže
a	a	k8xC	a
vařeným	vařený	k2eAgFnPc3d1	vařená
rybám	ryba	k1gFnPc3	ryba
<g/>
,	,	kIx,	,
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
polévek	polévka	k1gFnPc2	polévka
a	a	k8xC	a
marinád	marináda	k1gFnPc2	marináda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
koření	koření	k1gNnSc2	koření
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
čerstvé	čerstvý	k2eAgInPc1d1	čerstvý
i	i	k8xC	i
sušené	sušený	k2eAgInPc1d1	sušený
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
stonky	stonka	k1gFnPc4	stonka
<g/>
.	.	kIx.	.
</s>
<s>
Pokrmům	pokrm	k1gInPc3	pokrm
dodává	dodávat	k5eAaImIp3nS	dodávat
pikantně	pikantně	k6eAd1	pikantně
trpkou	trpký	k2eAgFnSc4d1	trpká
příchuť	příchuť	k1gFnSc4	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Estragon	estragon	k1gInSc1	estragon
významně	významně	k6eAd1	významně
podporuje	podporovat	k5eAaImIp3nS	podporovat
tvorbu	tvorba	k1gFnSc4	tvorba
žluči	žluč	k1gFnSc2	žluč
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
mírně	mírně	k6eAd1	mírně
močopudně	močopudně	k6eAd1	močopudně
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
trávení	trávení	k1gNnSc1	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
prokazatelný	prokazatelný	k2eAgInSc1d1	prokazatelný
vliv	vliv	k1gInSc1	vliv
antisklerotický	antisklerotický	k2eAgInSc1d1	antisklerotický
a	a	k8xC	a
tonizační	tonizační	k2eAgInSc1d1	tonizační
<g/>
.	.	kIx.	.
</s>
<s>
Tlumí	tlumit	k5eAaImIp3nP	tlumit
křečové	křečový	k2eAgFnPc1d1	křečová
bolesti	bolest	k1gFnPc1	bolest
při	při	k7c6	při
dyspepsii	dyspepsie	k1gFnSc6	dyspepsie
a	a	k8xC	a
podobných	podobný	k2eAgFnPc6d1	podobná
poruchách	porucha	k1gFnPc6	porucha
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
střevní	střevní	k2eAgMnPc4d1	střevní
parazity	parazit	k1gMnPc4	parazit
(	(	kIx(	(
<g/>
roupy	roup	k1gMnPc4	roup
<g/>
,	,	kIx,	,
škrkavky	škrkavka	k1gMnPc4	škrkavka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
i	i	k8xC	i
sušeném	sušený	k2eAgInSc6d1	sušený
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
botanik	botanik	k1gMnSc1	botanik
Ibn	Ibn	k1gMnSc1	Ibn
Baithar	Baithar	k1gMnSc1	Baithar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
čerstvé	čerstvý	k2eAgInPc1d1	čerstvý
výhonky	výhonek	k1gInPc1	výhonek
estragonu	estragon	k1gInSc2	estragon
byly	být	k5eAaImAgInP	být
vařeny	vařit	k5eAaImNgInP	vařit
v	v	k7c6	v
zelenině	zelenina	k1gFnSc6	zelenina
a	a	k8xC	a
šťáva	šťáva	k1gFnSc1	šťáva
z	z	k7c2	z
estragonu	estragon	k1gInSc2	estragon
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
k	k	k7c3	k
ochucování	ochucování	k1gNnSc3	ochucování
nápojů	nápoj	k1gInPc2	nápoj
<g/>
;	;	kIx,	;
poučuje	poučovat	k5eAaImIp3nS	poučovat
nás	my	k3xPp1nPc4	my
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
estragon	estragon	k1gInSc1	estragon
oslazuje	oslazovat	k5eAaImIp3nS	oslazovat
dech	dech	k1gInSc4	dech
<g/>
,	,	kIx,	,
otupuje	otupovat	k5eAaImIp3nS	otupovat
chuť	chuť	k1gFnSc4	chuť
hořkého	hořký	k2eAgInSc2d1	hořký
léku	lék	k1gInSc2	lék
a	a	k8xC	a
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Název	název	k1gInSc1	název
Artemisia	Artemisium	k1gNnSc2	Artemisium
pochází	pocházet	k5eAaImIp3nS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
řecké	řecký	k2eAgFnSc2d1	řecká
bohyně	bohyně	k1gFnSc2	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
–	–	k?	–
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
panen	panna	k1gFnPc2	panna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
mají	mít	k5eAaImIp3nP	mít
abortivní	abortivní	k2eAgInSc4d1	abortivní
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Druhový	druhový	k2eAgInSc1d1	druhový
název	název	k1gInSc1	název
dracunculus	dracunculus	k1gInSc1	dracunculus
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
malý	malý	k2eAgInSc1d1	malý
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
saň	saň	k1gFnSc1	saň
nebo	nebo	k8xC	nebo
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
čárkovité	čárkovitý	k2eAgInPc4d1	čárkovitý
listy	list	k1gInPc4	list
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
