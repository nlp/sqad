<p>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
digneská	digneský	k2eAgFnSc1d1	digneský
(	(	kIx(	(
<g/>
-riezská-sisteronská	iezskáisteronský	k2eAgFnSc1d1	-riezská-sisteronský
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Dioecesis	Dioecesis	k1gInSc1	Dioecesis
Diniensis	Diniensis	k1gFnSc1	Diniensis
(	(	kIx(	(
<g/>
-Reiensis-Sistariensis	-Reiensis-Sistariensis	k1gInSc1	-Reiensis-Sistariensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
franc	franc	k1gFnSc4	franc
<g/>
.	.	kIx.	.
</s>
<s>
Diocè	Diocè	k?	Diocè
de	de	k?	de
Digne	Dign	k1gMnSc5	Dign
<g/>
,	,	kIx,	,
Riez	Riez	k1gInSc1	Riez
et	et	k?	et
Sisteron	Sisteron	k1gInSc1	Sisteron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
diecéze	diecéze	k1gFnSc1	diecéze
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
území	území	k1gNnSc4	území
departementu	departement	k1gInSc2	departement
Alpes-de-Haute-Provence	Alpese-Haute-Provenec	k1gMnSc2	Alpes-de-Haute-Provenec
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
území	území	k1gNnSc1	území
přesně	přesně	k6eAd1	přesně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
biskupství	biskupství	k1gNnSc1	biskupství
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
Saint-Jérôme	Saint-Jérôm	k1gInSc5	Saint-Jérôm
de	de	k?	de
Digne	Dign	k1gInSc5	Dign
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Digne-les-Bains	Dignees-Bainsa	k1gFnPc2	Digne-les-Bainsa
<g/>
.	.	kIx.	.
</s>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
marseillské	marseillský	k2eAgFnSc2d1	marseillská
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
diecézním	diecézní	k2eAgInSc7d1	diecézní
biskupem	biskup	k1gInSc7	biskup
Mons	Monsa	k1gFnPc2	Monsa
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Philippe	Jean-Philippat	k5eAaPmIp3nS	Jean-Philippat
Nault	Nault	k2eAgMnSc1d1	Nault
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Biskupství	biskupství	k1gNnSc1	biskupství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Digne	Dign	k1gInSc5	Dign
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Patrony	patron	k1gInPc1	patron
diecéze	diecéze	k1gFnSc2	diecéze
jsou	být	k5eAaImIp3nP	být
svatí	svatý	k1gMnPc1	svatý
Domnin	Domnin	k2eAgMnSc1d1	Domnin
a	a	k8xC	a
Vincent	Vincent	k1gMnSc1	Vincent
z	z	k7c2	z
Digne	Dign	k1gInSc5	Dign
<g/>
,	,	kIx,	,
biskupové	biskup	k1gMnPc1	biskup
z	z	k7c2	z
Digne	Dign	k1gInSc5	Dign
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konkordátu	konkordát	k1gInSc2	konkordát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1801	[number]	k4	1801
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
francouzských	francouzský	k2eAgFnPc2d1	francouzská
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
diecézí	diecéze	k1gFnPc2	diecéze
aptské	aptská	k1gFnSc2	aptská
<g/>
,	,	kIx,	,
gapské	gapská	k1gFnSc2	gapská
<g/>
,	,	kIx,	,
glandè	glandè	k?	glandè
<g/>
,	,	kIx,	,
riezské	riezský	k2eAgFnSc2d1	riezský
<g/>
,	,	kIx,	,
senezské	senezský	k2eAgFnSc2d1	senezský
<g/>
,	,	kIx,	,
sisteronské	sisteronský	k2eAgFnSc2d1	sisteronský
a	a	k8xC	a
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
embrunské	embrunská	k1gFnSc2	embrunská
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zčásti	zčásti	k6eAd1	zčásti
včleněno	včleněn	k2eAgNnSc1d1	včleněno
do	do	k7c2	do
gigneské	gigneský	k2eAgFnSc2d1	gigneský
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
1916	[number]	k4	1916
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
diecéze	diecéze	k1gFnSc2	diecéze
na	na	k7c6	na
dignesko-riezsko-sisteronská	digneskoiezskoisteronský	k2eAgNnPc1d1	dignesko-riezsko-sisteronský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
digneská	digneský	k2eAgFnSc1d1	digneský
diecéze	diecéze	k1gFnSc1	diecéze
sufragánem	sufragán	k1gMnSc7	sufragán
marseillské	marseillský	k2eAgFnSc2d1	marseillská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
;	;	kIx,	;
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
sufragánní	sufragánní	k2eAgFnSc7d1	sufragánní
diecézí	diecéze	k1gFnSc7	diecéze
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
aixské	aixská	k1gFnSc2	aixská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Diocè	Diocè	k1gFnSc2	Diocè
de	de	k?	de
Digne	Dign	k1gMnSc5	Dign
<g/>
,	,	kIx,	,
Riez	Riez	k1gMnSc1	Riez
et	et	k?	et
Sisteron	Sisteron	k1gMnSc1	Sisteron
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
diecéze	diecéze	k1gFnSc2	diecéze
digneská	digneskat	k5eAaPmIp3nS	digneskat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
Digne	Dign	k1gInSc5	Dign
na	na	k7c4	na
Catholic	Catholice	k1gFnPc2	Catholice
hiearchy	hiearcha	k1gFnSc2	hiearcha
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
