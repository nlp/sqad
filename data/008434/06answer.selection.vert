<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konkordátu	konkordát	k1gInSc2	konkordát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1801	[number]	k4	1801
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
francouzských	francouzský	k2eAgFnPc2d1	francouzská
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
diecézí	diecéze	k1gFnPc2	diecéze
aptské	aptská	k1gFnSc2	aptská
<g/>
,	,	kIx,	,
gapské	gapská	k1gFnSc2	gapská
<g/>
,	,	kIx,	,
glandè	glandè	k?	glandè
<g/>
,	,	kIx,	,
riezské	riezský	k2eAgFnSc2d1	riezský
<g/>
,	,	kIx,	,
senezské	senezský	k2eAgFnSc2d1	senezský
<g/>
,	,	kIx,	,
sisteronské	sisteronský	k2eAgFnSc2d1	sisteronský
a	a	k8xC	a
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
embrunské	embrunská	k1gFnSc2	embrunská
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zčásti	zčásti	k6eAd1	zčásti
včleněno	včleněn	k2eAgNnSc1d1	včleněno
do	do	k7c2	do
gigneské	gigneský	k2eAgFnSc2d1	gigneský
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
