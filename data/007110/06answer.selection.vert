<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Avignonu	Avignon	k1gInSc2	Avignon
(	(	kIx(	(
<g/>
Avignonský	avignonský	k2eAgInSc1d1	avignonský
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
papežský	papežský	k2eAgInSc1d1	papežský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
biskupský	biskupský	k2eAgInSc1d1	biskupský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
hradby	hradba	k1gFnPc1	hradba
<g/>
)	)	kIx)	)
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
