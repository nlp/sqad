<s>
Avignon	Avignon	k1gInSc1	Avignon
[	[	kIx(	[
<g/>
aviňon	aviňon	k1gInSc1	aviňon
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
starobylé	starobylý	k2eAgNnSc1d1	starobylé
město	město	k1gNnSc1	město
v	v	k7c4	v
Provence	Provence	k1gFnPc4	Provence
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
departementu	departement	k1gInSc2	departement
Vaucluse	Vaucluse	k1gFnSc2	Vaucluse
v	v	k7c6	v
regionu	region	k1gInSc6	region
Provence-Alpes-Côte	Provence-Alpes-Côt	k1gInSc5	Provence-Alpes-Côt
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Azur	azur	k1gInSc1	azur
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Rhôny	Rhôna	k1gFnSc2	Rhôna
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Durance	durance	k1gFnSc2	durance
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
SZ	SZ	kA	SZ
od	od	k7c2	od
Marseille	Marseille	k1gFnSc2	Marseille
a	a	k8xC	a
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
SV	sv	kA	sv
od	od	k7c2	od
Montpellieru	Montpellier	k1gInSc2	Montpellier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1309-1377	[number]	k4	1309-1377
zde	zde	k6eAd1	zde
sídlili	sídlit	k5eAaImAgMnP	sídlit
papežové	papež	k1gMnPc1	papež
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Avignonu	Avignon	k1gInSc2	Avignon
(	(	kIx(	(
<g/>
Avignonský	avignonský	k2eAgInSc1d1	avignonský
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
papežský	papežský	k2eAgInSc1d1	papežský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
biskupský	biskupský	k2eAgInSc1d1	biskupský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
hradby	hradba	k1gFnPc1	hradba
<g/>
)	)	kIx)	)
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
<g/>
:	:	kIx,	:
Villeneuve-lè	Villeneuveè	k1gFnSc4	Villeneuve-lè
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Angles	Anglesa	k1gFnPc2	Anglesa
<g/>
,	,	kIx,	,
Barbentane	Barbentan	k1gInSc5	Barbentan
<g/>
,	,	kIx,	,
Rognonas	Rognonasa	k1gFnPc2	Rognonasa
<g/>
,	,	kIx,	,
Châteaurenard	Châteaurenarda	k1gFnPc2	Châteaurenarda
a	a	k8xC	a
Noves	Novesa	k1gFnPc2	Novesa
<g/>
.	.	kIx.	.
</s>
<s>
Avignon	Avignon	k1gInSc1	Avignon
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Rhôny	Rhôna	k1gFnSc2	Rhôna
a	a	k8xC	a
Durance	durance	k1gFnSc2	durance
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
osídlení	osídlení	k1gNnSc4	osídlení
místa	místo	k1gNnSc2	místo
již	již	k6eAd1	již
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
kelto-ligurský	keltoigurský	k2eAgInSc4d1	kelto-ligurský
kmen	kmen	k1gInSc4	kmen
Kavarů	Kavar	k1gMnPc2	Kavar
založil	založit	k5eAaPmAgMnS	založit
opevněné	opevněný	k2eAgNnSc4d1	opevněné
sídlo	sídlo	k1gNnSc4	sídlo
Auenion	Auenion	k1gInSc4	Auenion
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
postavili	postavit	k5eAaPmAgMnP	postavit
fokajští	fokajský	k2eAgMnPc1d1	fokajský
kolonisté	kolonista	k1gMnPc1	kolonista
z	z	k7c2	z
Marseille	Marseille	k1gFnSc2	Marseille
opevněný	opevněný	k2eAgInSc4d1	opevněný
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
sklad	sklad	k1gInSc4	sklad
zboží	zboží	k1gNnSc2	zboží
jménem	jméno	k1gNnSc7	jméno
Avenio	Avenio	k1gMnSc1	Avenio
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
120	[number]	k4	120
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
římští	římský	k2eAgMnPc1d1	římský
legionáři	legionář	k1gMnPc1	legionář
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
součástí	součást	k1gFnPc2	součást
provincie	provincie	k1gFnSc2	provincie
Gallia	Gallium	k1gNnSc2	Gallium
Narbonensis	Narbonensis	k1gFnSc2	Narbonensis
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
a	a	k8xC	a
kvetoucí	kvetoucí	k2eAgNnSc1d1	kvetoucí
město	město	k1gNnSc1	město
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Colonia	Colonium	k1gNnPc4	Colonium
iulia	iulius	k1gMnSc2	iulius
augusta	august	k1gMnSc2	august
avenionesium	avenionesium	k1gNnSc4	avenionesium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
malá	malý	k2eAgFnSc1d1	malá
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
osada	osada	k1gFnSc1	osada
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
první	první	k4xOgInSc1	první
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
500	[number]	k4	500
město	město	k1gNnSc1	město
obléhal	obléhat	k5eAaImAgMnS	obléhat
francký	francký	k2eAgMnSc1d1	francký
král	král	k1gMnSc1	král
Chlodvík	Chlodvík	k1gMnSc1	Chlodvík
I.	I.	kA	I.
<g/>
,	,	kIx,	,
městu	město	k1gNnSc3	město
však	však	k8xC	však
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
římští	římský	k2eAgMnPc1d1	římský
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Avignon	Avignon	k1gInSc1	Avignon
pak	pak	k6eAd1	pak
připadl	připadnout	k5eAaPmAgInS	připadnout
Ostrogótům	Ostrogót	k1gMnPc3	Ostrogót
a	a	k8xC	a
Merovejcům	Merovejec	k1gMnPc3	Merovejec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
736	[number]	k4	736
je	on	k3xPp3gMnPc4	on
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Saracéni	Saracén	k1gMnPc1	Saracén
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
zradu	zrada	k1gFnSc4	zrada
potrestal	potrestat	k5eAaPmAgMnS	potrestat
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
Martel	Martel	k1gMnSc1	Martel
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
částí	část	k1gFnSc7	část
Provensálského	provensálský	k2eAgNnSc2d1	provensálské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přechodně	přechodně	k6eAd1	přechodně
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
za	za	k7c4	za
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1303	[number]	k4	1303
zde	zde	k6eAd1	zde
papež	papež	k1gMnSc1	papež
založil	založit	k5eAaPmAgMnS	založit
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
hlavně	hlavně	k9	hlavně
studiem	studio	k1gNnSc7	studio
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1309	[number]	k4	1309
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Avignon	Avignon	k1gInSc4	Avignon
sídlem	sídlo	k1gNnSc7	sídlo
papežského	papežský	k2eAgInSc2d1	papežský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papežové	k2eAgNnSc4d1	Papežové
město	město	k1gNnSc4	město
zvelebili	zvelebit	k5eAaPmAgMnP	zvelebit
a	a	k8xC	a
nově	nově	k6eAd1	nově
ohradili	ohradit	k5eAaPmAgMnP	ohradit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
je	on	k3xPp3gNnPc4	on
koupili	koupit	k5eAaPmAgMnP	koupit
od	od	k7c2	od
neapolské	neapolský	k2eAgFnSc2d1	neapolská
královny	královna	k1gFnSc2	královna
Joanny	Joanna	k1gFnSc2	Joanna
<g/>
.	.	kIx.	.
</s>
<s>
Papežské	papežský	k2eAgFnSc3d1	Papežská
kurii	kurie	k1gFnSc3	kurie
pak	pak	k6eAd1	pak
město	město	k1gNnSc1	město
patřilo	patřit	k5eAaImAgNnS	patřit
až	až	k9	až
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
papežského	papežský	k2eAgInSc2d1	papežský
dvora	dvůr	k1gInSc2	dvůr
žila	žít	k5eAaImAgFnS	žít
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
i	i	k8xC	i
učenců	učenec	k1gMnPc2	učenec
a	a	k8xC	a
papežská	papežský	k2eAgFnSc1d1	Papežská
knihovna	knihovna	k1gFnSc1	knihovna
s	s	k7c7	s
2	[number]	k4	2
000	[number]	k4	000
svazky	svazek	k1gInPc1	svazek
byla	být	k5eAaImAgNnP	být
tehdy	tehdy	k6eAd1	tehdy
největší	veliký	k2eAgNnPc1d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
sídlilo	sídlit	k5eAaImAgNnS	sídlit
sedm	sedm	k4xCc1	sedm
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
XI	XI	kA	XI
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1376	[number]	k4	1376
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
sídlili	sídlit	k5eAaImAgMnP	sídlit
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
dva	dva	k4xCgMnPc1	dva
vzdoropapežové	vzdoropapež	k1gMnPc1	vzdoropapež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1721	[number]	k4	1721
postihla	postihnout	k5eAaPmAgFnS	postihnout
Avignon	Avignon	k1gInSc4	Avignon
morová	morový	k2eAgFnSc1d1	morová
rána	rána	k1gFnSc1	rána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabila	zabít	k5eAaPmAgFnS	zabít
asi	asi	k9	asi
18	[number]	k4	18
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1791	[number]	k4	1791
patřilo	patřit	k5eAaImAgNnS	patřit
město	město	k1gNnSc1	město
papežům	papež	k1gMnPc3	papež
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
anektováno	anektovat	k5eAaBmNgNnS	anektovat
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
papežského	papežský	k2eAgInSc2d1	papežský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
pobýval	pobývat	k5eAaImAgMnS	pobývat
krátce	krátce	k6eAd1	krátce
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Raňkův	Raňkův	k2eAgMnSc1d1	Raňkův
z	z	k7c2	z
Ježova	Ježův	k2eAgInSc2d1	Ježův
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Dražic	Dražice	k1gFnPc2	Dražice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochován	pochován	k2eAgMnSc1d1	pochován
Jan	Jan	k1gMnSc1	Jan
Milíč	Milíč	k1gMnSc1	Milíč
z	z	k7c2	z
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1374	[number]	k4	1374
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
1309	[number]	k4	1309
<g/>
–	–	k?	–
<g/>
1316	[number]	k4	1316
–	–	k?	–
Klement	Klement	k1gMnSc1	Klement
V.	V.	kA	V.
1316	[number]	k4	1316
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1334	[number]	k4	1334
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
1334	[number]	k4	1334
<g/>
–	–	k?	–
<g/>
1342	[number]	k4	1342
–	–	k?	–
Benedikt	Benedikt	k1gMnSc1	Benedikt
XII	XII	kA	XII
<g/>
.	.	kIx.	.
1342	[number]	k4	1342
<g/>
–	–	k?	–
<g/>
1352	[number]	k4	1352
–	–	k?	–
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
1352	[number]	k4	1352
<g/>
–	–	k?	–
<g/>
1362	[number]	k4	1362
–	–	k?	–
Inocenc	Inocenc	k1gMnSc1	Inocenc
VI	VI	kA	VI
<g/>
.	.	kIx.	.
1362	[number]	k4	1362
<g/>
–	–	k?	–
<g/>
1370	[number]	k4	1370
–	–	k?	–
Urban	Urban	k1gMnSc1	Urban
V.	V.	kA	V.
1370	[number]	k4	1370
<g/>
–	–	k?	–
<g/>
1378	[number]	k4	1378
–	–	k?	–
Řehoř	Řehoř	k1gMnSc1	Řehoř
XI	XI	kA	XI
<g/>
.	.	kIx.	.
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1394	[number]	k4	1394
–	–	k?	–
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
vzdoropapež	vzdoropapež	k1gMnSc1	vzdoropapež
<g/>
)	)	kIx)	)
1394	[number]	k4	1394
<g/>
–	–	k?	–
<g/>
1423	[number]	k4	1423
–	–	k?	–
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
vzdoropapež	vzdoropapež	k1gMnSc1	vzdoropapež
<g/>
)	)	kIx)	)
Avignon	Avignon	k1gInSc1	Avignon
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
dobře	dobře	k6eAd1	dobře
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
téměř	téměř	k6eAd1	téměř
úplných	úplný	k2eAgFnPc2d1	úplná
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Avignonský	avignonský	k2eAgInSc1d1	avignonský
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Pont	Pont	k1gInSc1	Pont
Saint-Bénezet	Saint-Bénezet	k1gFnSc2	Saint-Bénezet
<g/>
)	)	kIx)	)
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgInSc7d3	nejznámější
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
původně	původně	k6eAd1	původně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
900	[number]	k4	900
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
22	[number]	k4	22
oblouky	oblouk	k1gInPc7	oblouk
strhla	strhnout	k5eAaPmAgFnS	strhnout
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gNnSc1	jeho
zbylé	zbylý	k2eAgInPc1d1	zbylý
čtyři	čtyři	k4xCgInPc1	čtyři
oblouky	oblouk	k1gInPc1	oblouk
končí	končit	k5eAaImIp3nP	končit
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
řeky	řeka	k1gFnSc2	řeka
Rhôny	Rhôna	k1gFnSc2	Rhôna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
most	most	k1gInSc4	most
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
věží	věž	k1gFnPc2	věž
Tour	Tour	k1gMnSc1	Tour
du	du	k?	du
Châtelet	Châtelet	k1gInSc1	Châtelet
a	a	k8xC	a
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
je	být	k5eAaImIp3nS	být
goticko-románská	gotickoománský	k2eAgFnSc1d1	goticko-románský
patrová	patrový	k2eAgFnSc1d1	patrová
kaple	kaple	k1gFnSc1	kaple
Chapelle	Chapelle	k1gFnSc2	Chapelle
Saint-Nicolas	Saint-Nicolasa	k1gFnPc2	Saint-Nicolasa
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
Papežský	papežský	k2eAgInSc1d1	papežský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Palais	Palais	k1gInSc1	Palais
des	des	k1gNnSc1	des
Papes	Papes	k1gInSc1	Papes
<g/>
)	)	kIx)	)
na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
etapách	etapa	k1gFnPc6	etapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1335-1342	[number]	k4	1335-1342
dal	dát	k5eAaPmAgMnS	dát
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XII	XII	kA	XII
<g/>
.	.	kIx.	.
postavit	postavit	k5eAaPmF	postavit
čtyři	čtyři	k4xCgNnPc4	čtyři
křídla	křídlo	k1gNnPc4	křídlo
tak	tak	k6eAd1	tak
zvaného	zvaný	k2eAgInSc2d1	zvaný
Starého	Starého	k2eAgInSc2d1	Starého
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
Palais-vieux	Palaisieux	k1gInSc1	Palais-vieux
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
rajského	rajský	k2eAgInSc2d1	rajský
dvora	dvůr	k1gInSc2	dvůr
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
palác	palác	k1gInSc1	palác
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1342-1370	[number]	k4	1342-1370
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
užitkovou	užitkový	k2eAgFnSc7d1	užitková
plochou	plocha	k1gFnSc7	plocha
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
m	m	kA	m
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
staveb	stavba	k1gFnPc2	stavba
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
stojí	stát	k5eAaImIp3nS	stát
románská	románský	k2eAgFnSc1d1	románská
katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame-des-Doms	Notre-Damees-Domsa	k1gFnPc2	Notre-Dame-des-Domsa
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mohutná	mohutný	k2eAgFnSc1d1	mohutná
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
masivní	masivní	k2eAgFnSc7d1	masivní
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
postranní	postranní	k2eAgFnPc4d1	postranní
kaple	kaple	k1gFnPc4	kaple
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
upraven	upraven	k2eAgMnSc1d1	upraven
preskbytář	preskbytář	k1gMnSc1	preskbytář
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
románského	románský	k2eAgNnSc2d1	románské
opatství	opatství	k1gNnSc2	opatství
sv.	sv.	kA	sv.
Rufa	Rufa	k1gMnSc1	Rufa
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
Rhony	Rhona	k1gFnSc2	Rhona
Gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
St-Didier	St-Didira	k1gFnPc2	St-Didira
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
Gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
řádu	řád	k1gInSc2	řád
celestinů	celestin	k1gMnPc2	celestin
<g/>
,	,	kIx,	,
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
z	z	k7c2	z
původně	původně	k6eAd1	původně
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
s	s	k7c7	s
rajským	rajský	k2eAgInSc7d1	rajský
dvorem	dvůr	k1gInSc7	dvůr
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
historických	historický	k2eAgInPc2d1	historický
domů	dům	k1gInPc2	dům
Muzea	muzeum	k1gNnSc2	muzeum
Muzeum	muzeum	k1gNnSc1	muzeum
Angladon	Angladon	k1gInSc1	Angladon
(	(	kIx(	(
<g/>
Musée	Musée	k1gInSc1	Musée
Angladon	Angladon	k1gInSc1	Angladon
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
od	od	k7c2	od
zimní	zimní	k2eAgFnSc2d1	zimní
sezóny	sezóna	k1gFnSc2	sezóna
1996-1997	[number]	k4	1996-1997
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
panském	panský	k2eAgNnSc6d1	panské
sídle	sídlo	k1gNnSc6	sídlo
přístupná	přístupný	k2eAgFnSc1d1	přístupná
soukromá	soukromý	k2eAgFnSc1d1	soukromá
sbírka	sbírka	k1gFnSc1	sbírka
obrazů	obraz	k1gInPc2	obraz
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díla	dílo	k1gNnPc4	dílo
od	od	k7c2	od
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
S.	S.	kA	S.
Chardina	Chardina	k1gFnSc1	Chardina
po	po	k7c4	po
van	van	k1gInSc4	van
Gogha	Gogha	k1gFnSc1	Gogha
<g/>
,	,	kIx,	,
Degase	Degasa	k1gFnSc6	Degasa
<g/>
,	,	kIx,	,
Cézanna	Cézann	k1gMnSc4	Cézann
<g/>
,	,	kIx,	,
Sisleyho	Sisley	k1gMnSc4	Sisley
<g/>
,	,	kIx,	,
Picassa	Picass	k1gMnSc4	Picass
a	a	k8xC	a
Modiglianiho	Modigliani	k1gMnSc4	Modigliani
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Aubanel	Aubanela	k1gFnPc2	Aubanela
(	(	kIx(	(
<g/>
Musée	Musée	k1gFnSc1	Musée
Aubanel	Aubanela	k1gFnPc2	Aubanela
<g/>
)	)	kIx)	)
vystavovalo	vystavovat	k5eAaImAgNnS	vystavovat
staré	starý	k2eAgNnSc1d1	staré
tiskařské	tiskařský	k2eAgNnSc1d1	tiskařské
stroje	stroj	k1gInSc2	stroj
Muzeum	muzeum	k1gNnSc1	muzeum
Calvet	Calvet	k1gInSc1	Calvet
(	(	kIx(	(
<g/>
Musée	Musée	k1gInSc1	Musée
Calvet	Calvet	k1gInSc1	Calvet
<g/>
)	)	kIx)	)
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
sbírky	sbírka	k1gFnPc4	sbírka
archeologie	archeologie	k1gFnSc2	archeologie
<g/>
,	,	kIx,	,
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
Muzeum	muzeum	k1gNnSc4	muzeum
Malý	malý	k2eAgInSc1d1	malý
Palác	palác	k1gInSc1	palác
-	-	kIx~	-
Avignon	Avignon	k1gInSc1	Avignon
(	(	kIx(	(
<g/>
Musée	Musée	k1gInSc1	Musée
du	du	k?	du
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc1	Palais
-	-	kIx~	-
Avignon	Avignon	k1gInSc1	Avignon
<g/>
)	)	kIx)	)
Bývalé	bývalý	k2eAgFnSc2d1	bývalá
biskupské	biskupský	k2eAgFnSc2d1	biskupská
<g />
.	.	kIx.	.
</s>
<s>
sídlo	sídlo	k1gNnSc1	sídlo
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
<g/>
,	,	kIx,	,
průčelí	průčelí	k1gNnSc4	průčelí
konec	konec	k1gInSc1	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
dnes	dnes	k6eAd1	dnes
hostí	hostit	k5eAaImIp3nS	hostit
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
děl	dělo	k1gNnPc2	dělo
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Giana	Gian	k1gMnSc2	Gian
Pietra	Pietr	k1gMnSc2	Pietr
Campany	Campana	k1gFnSc2	Campana
z	z	k7c2	z
období	období	k1gNnSc2	období
italských	italský	k2eAgMnPc2d1	italský
mistrů	mistr	k1gMnPc2	mistr
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
díly	díl	k1gInPc7	díl
malířské	malířský	k2eAgFnSc2d1	malířská
školy	škola	k1gFnSc2	škola
z	z	k7c2	z
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
malíři	malíř	k1gMnSc3	malíř
Botticelli	Botticell	k1gMnSc3	Botticell
<g/>
,	,	kIx,	,
Carpaccio	Carpaccio	k6eAd1	Carpaccio
<g/>
,	,	kIx,	,
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
di	di	k?	di
Paolo	Paolo	k1gNnSc1	Paolo
a.j.	a.j.	k?	a.j.
Muzeum	muzeum	k1gNnSc1	muzeum
Mont	Mont	k1gMnSc1	Mont
de	de	k?	de
Piété	Piétý	k2eAgNnSc1d1	Piétý
Muzeum	muzeum	k1gNnSc4	muzeum
Requien	Requina	k1gFnPc2	Requina
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnSc2	sbírka
místních	místní	k2eAgFnPc2d1	místní
přírodnin	přírodnina	k1gFnPc2	přírodnina
<g/>
,	,	kIx,	,
herbáře	herbář	k1gInSc2	herbář
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Lapidárium	lapidárium	k1gNnSc1	lapidárium
(	(	kIx(	(
<g/>
Musée	Muséus	k1gMnSc5	Muséus
Lapidaire	Lapidair	k1gMnSc5	Lapidair
<g/>
)	)	kIx)	)
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
jezuitském	jezuitský	k2eAgInSc6d1	jezuitský
kostele	kostel	k1gInSc6	kostel
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
řecké	řecký	k2eAgNnSc1d1	řecké
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc4d1	římské
a	a	k8xC	a
středověké	středověký	k2eAgNnSc4d1	středověké
sochařství	sochařství	k1gNnSc4	sochařství
a	a	k8xC	a
keramiku	keramika	k1gFnSc4	keramika
Muzeum	muzeum	k1gNnSc1	muzeum
Louise	Louis	k1gMnSc2	Louis
Voulanda	Voulanda	k1gFnSc1	Voulanda
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
keramiku	keramika	k1gFnSc4	keramika
<g/>
,	,	kIx,	,
umělecká	umělecký	k2eAgNnPc4d1	umělecké
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
Muzeum	muzeum	k1gNnSc1	muzeum
lidového	lidový	k2eAgNnSc2d1	lidové
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
Provence	Provence	k1gFnSc2	Provence
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1304	[number]	k4	1304
–	–	k?	–
1374	[number]	k4	1374
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
Laura	Laura	k1gFnSc1	Laura
de	de	k?	de
Noves	Noves	k1gInSc1	Noves
(	(	kIx(	(
<g/>
1310	[number]	k4	1310
-	-	kIx~	-
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
Francesca	Francesc	k1gInSc2	Francesc
Petrarky	Petrarka	k1gFnSc2	Petrarka
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Raňkův	Raňkův	k2eAgMnSc1d1	Raňkův
z	z	k7c2	z
Ježova	Ježův	k2eAgInSc2d1	Ježův
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1320	[number]	k4	1320
-	-	kIx~	-
1388	[number]	k4	1388
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Josef	Josef	k1gMnSc1	Josef
ha-Kohen	ha-Kohen	k2eAgMnSc1d1	ha-Kohen
(	(	kIx(	(
<g/>
1496	[number]	k4	1496
–	–	k?	–
1558	[number]	k4	1558
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Alexandre	Alexandr	k1gInSc5	Alexandr
de	de	k?	de
Rhodes	Rhodes	k1gInSc1	Rhodes
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
nebo	nebo	k8xC	nebo
1593	[number]	k4	1593
-	-	kIx~	-
1660	[number]	k4	1660
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezuita	jezuita	k1gMnSc1	jezuita
a	a	k8xC	a
misionář	misionář	k1gMnSc1	misionář
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
(	(	kIx(	(
<g/>
*	*	kIx~	*
1806	[number]	k4	1806
-	-	kIx~	-
†	†	k?	†
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Édouard	Édouard	k1gMnSc1	Édouard
Daladier	Daladier	k1gMnSc1	Daladier
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1884	[number]	k4	1884
-	-	kIx~	-
†	†	k?	†
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
Pierre	Pierr	k1gInSc5	Pierr
Boulle	Boulle	k1gNnPc7	Boulle
(	(	kIx(	(
<g/>
*	*	kIx~	*
1912	[number]	k4	1912
-	-	kIx~	-
†	†	k?	†
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
René	René	k1gMnSc1	René
Girard	Girard	k1gMnSc1	Girard
(	(	kIx(	(
<g/>
*	*	kIx~	*
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
historik	historik	k1gMnSc1	historik
Bernard	Bernard	k1gMnSc1	Bernard
Kouchner	Kouchner	k1gMnSc1	Kouchner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
)	)	kIx)	)
Mireille	Mireille	k1gInSc1	Mireille
Mathieu	Mathieus	k1gInSc2	Mathieus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Daniel	Daniel	k1gMnSc1	Daniel
Auteuil	Auteuil	k1gMnSc1	Auteuil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
Colchester	Colchester	k1gMnSc1	Colchester
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Diourbel	Diourbel	k1gInSc4	Diourbel
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc4	Senegal
Guanajuato	Guanajuat	k2eAgNnSc1d1	Guanajuato
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
New	New	k1gFnPc2	New
Haven	Havna	k1gFnPc2	Havna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Siena	Siena	k1gFnSc1	Siena
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Tarragona	Tarragona	k1gFnSc1	Tarragona
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Tortosa	Tortosa	k1gFnSc1	Tortosa
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Wetzlar	Wetzlara	k1gFnPc2	Wetzlara
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Wichita	Wichita	k1gFnSc1	Wichita
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Seznam	seznam	k1gInSc4	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Vaucluse	Vaucluse	k1gFnSc2	Vaucluse
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Avignon	Avignon	k1gInSc4	Avignon
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc1	Commons
Historic	Historice	k1gInPc2	Historice
Centre	centr	k1gInSc5	centr
of	of	k?	of
Avignon	Avignon	k1gInSc1	Avignon
(	(	kIx(	(
<g/>
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Papežského	papežský	k2eAgInSc2d1	papežský
paláce	palác	k1gInSc2	palác
festival	festival	k1gInSc1	festival
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Avignon	Avignon	k1gInSc1	Avignon
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
ville	ville	k6eAd1	ville
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Avignon	Avignon	k1gInSc1	Avignon
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Arles-Avignon	Arles-Avignona	k1gFnPc2	Arles-Avignona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Avinion	Avinion	k1gInSc1	Avinion
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Galerie	galerie	k1gFnSc1	galerie
Avignon	Avignon	k1gInSc1	Avignon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
