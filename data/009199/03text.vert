<p>
<s>
Avon	Avon	k1gInSc1	Avon
je	být	k5eAaImIp3nS	být
kosmetická	kosmetický	k2eAgFnSc1d1	kosmetická
společnost	společnost	k1gFnSc1	společnost
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
světovým	světový	k2eAgFnPc3d1	světová
firmám	firma	k1gFnPc3	firma
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
prodává	prodávat	k5eAaImIp3nS	prodávat
formou	forma	k1gFnSc7	forma
přímého	přímý	k2eAgInSc2d1	přímý
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
o	o	k7c4	o
zákazníky	zákazník	k1gMnPc4	zákazník
stará	starat	k5eAaImIp3nS	starat
konkrétní	konkrétní	k2eAgMnSc1d1	konkrétní
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
pořádá	pořádat	k5eAaImIp3nS	pořádat
také	také	k9	také
Avon	Avon	k1gInSc4	Avon
pochody	pochod	k1gInPc7	pochod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
podporovat	podporovat	k5eAaImF	podporovat
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
i	i	k9	i
společensky	společensky	k6eAd1	společensky
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
modelky	modelka	k1gFnPc1	modelka
Adriana	Adrian	k1gMnSc2	Adrian
Sklenaříková	Sklenaříková	k1gFnSc1	Sklenaříková
Karembeu	Karembeus	k1gInSc2	Karembeus
a	a	k8xC	a
Daniela	Daniela	k1gFnSc1	Daniela
Peštová	Peštová	k1gFnSc1	Peštová
<g/>
,	,	kIx,	,
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Lucie	Lucie	k1gFnSc2	Lucie
Bílá	bílý	k2eAgFnSc1d1	bílá
či	či	k8xC	či
Monika	Monika	k1gFnSc1	Monika
Absolonová	Absolonová	k1gFnSc1	Absolonová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
muži	muž	k1gMnPc1	muž
jako	jako	k8xS	jako
například	například	k6eAd1	například
Paľo	Paľo	k6eAd1	Paľo
Habera	Habera	k1gFnSc1	Habera
nebo	nebo	k8xC	nebo
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
.	.	kIx.	.
</s>
<s>
Pochody	pochod	k1gInPc1	pochod
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
asi	asi	k9	asi
šedesáti	šedesát	k4xCc6	šedesát
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
pražského	pražský	k2eAgInSc2d1	pražský
pochodu	pochod	k1gInSc2	pochod
účastnilo	účastnit	k5eAaImAgNnS	účastnit
asi	asi	k9	asi
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
jako	jako	k9	jako
vstupenku	vstupenka	k1gFnSc4	vstupenka
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
tričko	tričko	k1gNnSc4	tričko
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Avon	Avona	k1gFnPc2	Avona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
