<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
spektrální	spektrální	k2eAgFnPc1d1	spektrální
třídy	třída	k1gFnPc1	třída
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
V.	V.	kA	V.
Obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
25	[number]	k4	25
000	[number]	k4	000
do	do	k7c2	do
28	[number]	k4	28
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oběh	oběh	k1gInSc1	oběh
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
226	[number]	k4	226
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
centrum	centrum	k1gNnSc1	centrum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
1	[number]	k4	1
AU	au	k0	au
(	(	kIx(	(
<g/>
asi	asi	k9	asi
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
hvězdu	hvězda	k1gFnSc4	hvězda
Zemi	zem	k1gFnSc3	zem
nejbližší	blízký	k2eAgFnSc3d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
330	[number]	k4	330
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc1	hmotnost
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
99,8	[number]	k4	99,8
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
jejího	její	k3xOp3gInSc2	její
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
koule	koule	k1gFnSc1	koule
žhavého	žhavý	k2eAgNnSc2d1	žhavé
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
produkuje	produkovat	k5eAaImIp3nS	produkovat
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
W	W	kA	W
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
dopadá	dopadat	k5eAaImIp3nS	dopadat
asi	asi	k9	asi
0,45	[number]	k4	0,45
miliardtiny	miliardtina	k1gFnPc4	miliardtina
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
energie	energie	k1gFnSc2	energie
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
konstanta	konstanta	k1gFnSc1	konstanta
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
1,4	[number]	k4	1,4
kW	kW	kA	kW
m	m	kA	m
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc1d1	staré
přibližně	přibližně	k6eAd1	přibližně
4,6	[number]	k4	4,6
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	on	k3xPp3gMnPc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
hvězdy	hvězda	k1gFnPc4	hvězda
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
svítit	svítit	k5eAaImF	svítit
ještě	ještě	k9	ještě
asi	asi	k9	asi
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
5	[number]	k4	5
800	[number]	k4	800
K	K	kA	K
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	on	k3xPp3gMnPc4	on
lidé	člověk	k1gMnPc1	člověk
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
žluté	žlutý	k2eAgFnPc1d1	žlutá
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
maximum	maximum	k1gNnSc1	maximum
jeho	on	k3xPp3gNnSc2	on
vyzařování	vyzařování	k1gNnSc2	vyzařování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
části	část	k1gFnSc6	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
400	[number]	k4	400
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
109	[number]	k4	109
průměrů	průměr	k1gInPc2	průměr
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
asi	asi	k9	asi
1,3	[number]	k4	1,3
milionkrát	milionkrát	k6eAd1	milionkrát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
objem	objem	k1gInSc4	objem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
Slunce	slunce	k1gNnSc2	slunce
činí	činit	k5eAaImIp3nS	činit
průměrně	průměrně	k6eAd1	průměrně
1	[number]	k4	1
400	[number]	k4	400
kg	kg	kA	kg
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
jinou	jiný	k2eAgFnSc7d1	jiná
rychlostí	rychlost	k1gFnSc7	rychlost
u	u	k7c2	u
pólů	pól	k1gInPc2	pól
a	a	k8xC	a
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
25	[number]	k4	25
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
pólu	pól	k1gInSc6	pól
za	za	k7c4	za
36	[number]	k4	36
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
je	být	k5eAaImIp3nS	být
+4,1	+4,1	k4	+4,1
<g/>
,	,	kIx,	,
relativní	relativní	k2eAgInSc4d1	relativní
pak	pak	k6eAd1	pak
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tak	tak	k9	tak
o	o	k7c4	o
nejjasnější	jasný	k2eAgNnSc4d3	nejjasnější
těleso	těleso	k1gNnSc4	těleso
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
Slunce	slunce	k1gNnSc4	slunce
je	být	k5eAaImIp3nS	být
kruh	kruh	k1gInSc1	kruh
s	s	k7c7	s
bodem	bod	k1gInSc7	bod
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
☉	☉	k?	☉
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2609	[number]	k4	2609
SUN	Sun	kA	Sun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
povrch	povrch	k6eAd1wR	povrch
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
teplem	teplo	k1gNnSc7	teplo
a	a	k8xC	a
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
povrchu	povrch	k1gInSc3	povrch
Země	zem	k1gFnSc2	zem
za	za	k7c4	za
8	[number]	k4	8
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
19	[number]	k4	19
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
hvězdy	hvězda	k1gFnSc2	hvězda
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
světlo	světlo	k1gNnSc1	světlo
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
za	za	k7c4	za
4,22	[number]	k4	4,22
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
147	[number]	k4	147
097	[number]	k4	097
000	[number]	k4	000
km	km	kA	km
(	(	kIx(	(
<g/>
perihélium	perihélium	k1gNnSc1	perihélium
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
152	[number]	k4	152
099	[number]	k4	099
000	[number]	k4	000
km	km	kA	km
(	(	kIx(	(
<g/>
afélium	afélium	k1gNnSc1	afélium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
střídání	střídání	k1gNnSc2	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
pohybu	pohyb	k1gInSc2	pohyb
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
i	i	k9	i
pravý	pravý	k2eAgInSc1d1	pravý
sluneční	sluneční	k2eAgInSc1d1	sluneční
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
upravená	upravený	k2eAgFnSc1d1	upravená
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
pohání	pohánět	k5eAaImIp3nS	pohánět
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
závislé	závislý	k2eAgNnSc1d1	závislé
podnebí	podnebí	k1gNnSc3	podnebí
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
počasí	počasí	k1gNnSc2	počasí
i	i	k8xC	i
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
přílivu	příliv	k1gInSc6	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc6	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
živočichům	živočich	k1gMnPc3	živočich
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
propouští	propouštět	k5eAaImIp3nS	propouštět
jen	jen	k9	jen
část	část	k1gFnSc1	část
spektra	spektrum	k1gNnSc2	spektrum
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
–	–	k?	–
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
ultrafialového	ultrafialový	k2eAgMnSc2d1	ultrafialový
<g/>
,	,	kIx,	,
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
a	a	k8xC	a
radiového	radiový	k2eAgNnSc2d1	radiové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
tvorbu	tvorba	k1gFnSc4	tvorba
vitaminu	vitamin	k1gInSc2	vitamin
D	D	kA	D
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobějším	dlouhodobý	k2eAgNnSc6d2	dlouhodobější
působení	působení	k1gNnSc6	působení
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
i	i	k9	i
nepříznivé	příznivý	k2eNgInPc4d1	nepříznivý
efekty	efekt	k1gInPc4	efekt
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mutací	mutace	k1gFnPc2	mutace
a	a	k8xC	a
vzniků	vznik	k1gInPc2	vznik
nádorových	nádorový	k2eAgNnPc2d1	nádorové
onemocnění	onemocnění	k1gNnPc2	onemocnění
či	či	k8xC	či
slepoty	slepota	k1gFnSc2	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kulturách	kultura	k1gFnPc6	kultura
uctíváno	uctíván	k2eAgNnSc1d1	uctíváno
jako	jako	k9	jako
božstvo	božstvo	k1gNnSc1	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
byl	být	k5eAaImAgMnS	být
bohem	bůh	k1gMnSc7	bůh
Slunce	slunce	k1gNnSc2	slunce
Helios	Helios	k1gMnSc1	Helios
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
cestoval	cestovat	k5eAaImAgMnS	cestovat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
zlatém	zlatý	k2eAgInSc6d1	zlatý
voze	vůz	k1gInSc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
Sol	sol	k1gInSc1	sol
a	a	k8xC	a
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
pak	pak	k6eAd1	pak
Ré	ré	k1gNnPc7	ré
<g/>
,	,	kIx,	,
Ra	ra	k0	ra
či	či	k8xC	či
Amon	Amono	k1gNnPc2	Amono
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
symbolem	symbol	k1gInSc7	symbol
vitality	vitalita	k1gFnSc2	vitalita
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kultur	kultura	k1gFnPc2	kultura
považovala	považovat	k5eAaImAgFnS	považovat
Slunce	slunce	k1gNnSc4	slunce
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
života	život	k1gInSc2	život
a	a	k8xC	a
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
objevováním	objevování	k1gNnSc7	objevování
se	se	k3xPyFc4	se
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
každé	každý	k3xTgInPc4	každý
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
pozorování	pozorování	k1gNnSc6	pozorování
Slunce	slunce	k1gNnSc2	slunce
pocházejí	pocházet	k5eAaImIp3nP	pocházet
období	období	k1gNnSc4	období
2	[number]	k4	2
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
762	[number]	k4	762
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
první	první	k4xOgNnPc1	první
zatmění	zatmění	k1gNnPc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hliněné	hliněný	k2eAgFnSc2d1	hliněná
destičky	destička	k1gFnSc2	destička
psané	psaný	k2eAgFnSc2d1	psaná
klínovým	klínový	k2eAgNnSc7d1	klínové
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Anaxagoras	Anaxagoras	k1gMnSc1	Anaxagoras
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
434	[number]	k4	434
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hromady	hromada	k1gFnSc2	hromada
hořícího	hořící	k2eAgNnSc2d1	hořící
kamení	kamení	k1gNnSc2	kamení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
představ	představa	k1gFnPc2	představa
mnohých	mnohý	k2eAgFnPc2d1	mnohá
civilizací	civilizace	k1gFnPc2	civilizace
Slunce	slunce	k1gNnSc2	slunce
obíhalo	obíhat	k5eAaImAgNnS	obíhat
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
Země	zem	k1gFnPc1	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
modelu	model	k1gInSc6	model
vesmíru	vesmír	k1gInSc2	vesmír
umístil	umístit	k5eAaPmAgMnS	umístit
Slunce	slunce	k1gNnSc4	slunce
mezi	mezi	k7c4	mezi
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
.	.	kIx.	.
</s>
<s>
Aristarchos	Aristarchos	k1gInSc1	Aristarchos
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
předvedl	předvést	k5eAaPmAgMnS	předvést
současně	současně	k6eAd1	současně
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
že	že	k8xS	že
Země	země	k1gFnSc1	země
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gMnSc2	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
raná	raný	k2eAgFnSc1d1	raná
heliocentrická	heliocentrický	k2eAgFnSc1d1	heliocentrická
představa	představa	k1gFnSc1	představa
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neuchytila	uchytit	k5eNaPmAgFnS	uchytit
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1507	[number]	k4	1507
převažoval	převažovat	k5eAaImAgInS	převažovat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
středem	střed	k1gInSc7	střed
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1543	[number]	k4	1543
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svoje	svůj	k3xOyFgFnPc4	svůj
teze	teze	k1gFnPc4	teze
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
revolutionibus	revolutionibus	k1gInSc1	revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelestium	coelestium	k1gNnSc1	coelestium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pro	pro	k7c4	pro
heliocentrickou	heliocentrický	k2eAgFnSc4d1	heliocentrická
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
prvního	první	k4xOgInSc2	první
dalekohledu	dalekohled	k1gInSc2	dalekohled
značně	značně	k6eAd1	značně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
možnosti	možnost	k1gFnPc4	možnost
zkoumání	zkoumání	k1gNnSc2	zkoumání
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
využil	využít	k5eAaPmAgMnS	využít
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gInSc6	Galile
a	a	k8xC	a
David	David	k1gMnSc1	David
Fabricius	Fabricius	k1gMnSc1	Fabricius
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
značně	značně	k6eAd1	značně
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
katolickou	katolický	k2eAgFnSc4d1	katolická
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
z	z	k7c2	z
"	"	kIx"	"
<g/>
dokonale	dokonale	k6eAd1	dokonale
čistého	čistý	k2eAgInSc2d1	čistý
éteru	éter	k1gInSc2	éter
<g/>
"	"	kIx"	"
a	a	k8xC	a
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
nacházely	nacházet	k5eAaImAgFnP	nacházet
tmavší	tmavý	k2eAgFnPc1d2	tmavší
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
minimálně	minimálně	k6eAd1	minimálně
čtyřem	čtyři	k4xCgMnPc3	čtyři
dalším	další	k2eAgMnPc3d1	další
pozorovatelům	pozorovatel	k1gMnPc3	pozorovatel
pozorovat	pozorovat	k5eAaImF	pozorovat
sluneční	sluneční	k2eAgFnPc4d1	sluneční
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
Galileovo	Galileův	k2eAgNnSc1d1	Galileovo
pozorování	pozorování	k1gNnSc1	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
jezuita	jezuita	k1gMnSc1	jezuita
Christoph	Christoph	k1gMnSc1	Christoph
Scheiner	scheiner	k1gInSc4	scheiner
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
rotuje	rotovat	k5eAaImIp3nS	rotovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Země	země	k1gFnSc1	země
okolo	okolo	k7c2	okolo
svojí	svůj	k3xOyFgFnSc2	svůj
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
učinil	učinit	k5eAaPmAgInS	učinit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnPc2	pozorování
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
pozorování	pozorování	k1gNnSc2	pozorování
nápadně	nápadně	k6eAd1	nápadně
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
okraje	okraj	k1gInSc2	okraj
ke	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
krokem	krok	k1gInSc7	krok
pro	pro	k7c4	pro
porozumění	porozumění	k1gNnSc4	porozumění
významu	význam	k1gInSc2	význam
a	a	k8xC	a
pozice	pozice	k1gFnSc1	pozice
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
objevení	objevení	k1gNnSc1	objevení
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
Newtonova	Newtonův	k2eAgInSc2d1	Newtonův
gravitačního	gravitační	k2eAgInSc2d1	gravitační
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnPc1d1	hmotná
a	a	k8xC	a
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
tělesa	těleso	k1gNnPc1	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
kolem	kolem	k6eAd1	kolem
něho	on	k3xPp3gMnSc4	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
přesně	přesně	k6eAd1	přesně
změřeny	změřit	k5eAaPmNgFnP	změřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1672	[number]	k4	1672
díky	díky	k7c3	díky
přesným	přesný	k2eAgNnPc3d1	přesné
měřením	měření	k1gNnPc3	měření
italského	italský	k2eAgMnSc2d1	italský
astronoma	astronom	k1gMnSc2	astronom
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Cassiniho	Cassini	k1gMnSc2	Cassini
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Flamsteeda	Flamsteed	k1gMnSc2	Flamsteed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
použil	použít	k5eAaPmAgMnS	použít
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Joseph	Josepha	k1gFnPc2	Josepha
von	von	k1gInSc1	von
Fraunhofer	Fraunhofer	k1gMnSc1	Fraunhofer
spektroskop	spektroskop	k1gInSc1	spektroskop
pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spektrum	spektrum	k1gNnSc1	spektrum
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
přerušované	přerušovaný	k2eAgNnSc1d1	přerušované
tmavými	tmavý	k2eAgFnPc7d1	tmavá
absorpčními	absorpční	k2eAgFnPc7d1	absorpční
čárami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
čáry	čára	k1gFnPc1	čára
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
jako	jako	k9	jako
Fraunhoferovy	Fraunhoferův	k2eAgFnPc1d1	Fraunhoferova
čáry	čára	k1gFnPc1	čára
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgMnSc7d1	důležitý
pomocníkem	pomocník	k1gMnSc7	pomocník
při	při	k7c6	při
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
určování	určování	k1gNnSc6	určování
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
hvězdy	hvězda	k1gFnPc1	hvězda
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
studovány	studovat	k5eAaImNgInP	studovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
platila	platit	k5eAaImAgFnS	platit
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
provázanost	provázanost	k1gFnSc1	provázanost
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc4d1	Nové
objevy	objev	k1gInPc4	objev
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
vědcům	vědec	k1gMnPc3	vědec
pochopit	pochopit	k5eAaPmF	pochopit
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
hvězdách	hvězda	k1gFnPc6	hvězda
a	a	k8xC	a
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
záření	záření	k1gNnSc2	záření
ale	ale	k8xC	ale
přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
námahu	námaha	k1gFnSc4	námaha
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hypotéz	hypotéza	k1gFnPc2	hypotéza
vyslovená	vyslovený	k2eAgFnSc1d1	vyslovená
skotským	skotský	k1gInSc7	skotský
inženýrem	inženýr	k1gMnSc7	inženýr
Johnem	John	k1gMnSc7	John
Waterstonem	Waterston	k1gInSc7	Waterston
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzářená	vyzářený	k2eAgFnSc1d1	vyzářená
energie	energie	k1gFnSc1	energie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
gravitační	gravitační	k2eAgFnSc2d1	gravitační
kontrakce	kontrakce	k1gFnSc2	kontrakce
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
hypotéza	hypotéza	k1gFnSc1	hypotéza
vyslovená	vyslovený	k2eAgFnSc1d1	vyslovená
J.	J.	kA	J.
Mayerem	Mayer	k1gMnSc7	Mayer
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
udržována	udržovat	k5eAaImNgFnS	udržovat
dopady	dopad	k1gInPc7	dopad
meteoritů	meteorit	k1gInPc2	meteorit
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
mezníkem	mezník	k1gInSc7	mezník
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
objev	objev	k1gInSc1	objev
spektrometrie	spektrometrie	k1gFnSc2	spektrometrie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
energetickým	energetický	k2eAgInSc7d1	energetický
zdrojem	zdroj	k1gInSc7	zdroj
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vést	vést	k5eAaImF	vést
debaty	debata	k1gFnPc4	debata
o	o	k7c6	o
formě	forma	k1gFnSc6	forma
této	tento	k3xDgFnSc2	tento
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slučování	slučování	k1gNnSc4	slučování
(	(	kIx(	(
<g/>
fúzi	fúze	k1gFnSc4	fúze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
štěpení	štěpení	k1gNnSc4	štěpení
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Hans	Hans	k1gMnSc1	Hans
Bethe	Beth	k1gFnSc2	Beth
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
jako	jako	k8xS	jako
energetický	energetický	k2eAgInSc4d1	energetický
zdroj	zdroj	k1gInSc4	zdroj
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
největší	veliký	k2eAgNnSc4d3	veliký
nebeské	nebeský	k2eAgNnSc4d1	nebeské
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
109	[number]	k4	109
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc1d2	veliký
průměr	průměr	k1gInSc1	průměr
než	než	k8xS	než
Země	země	k1gFnSc1	země
a	a	k8xC	a
1	[number]	k4	1
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
násobně	násobně	k6eAd1	násobně
větší	veliký	k2eAgInSc4d2	veliký
objem	objem	k1gInSc4	objem
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
99,8	[number]	k4	99,8
%	%	kIx~	%
hmoty	hmota	k1gFnSc2	hmota
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
obrovská	obrovský	k2eAgFnSc1d1	obrovská
plazmová	plazmový	k2eAgFnSc1d1	plazmová
koule	koule	k1gFnSc1	koule
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
hustotou	hustota	k1gFnSc7	hustota
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
hustota	hustota	k1gFnSc1	hustota
i	i	k9	i
teplota	teplota	k1gFnSc1	teplota
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
hvězdami	hvězda	k1gFnPc7	hvězda
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
středně	středně	k6eAd1	středně
staré	starý	k2eAgFnSc2d1	stará
skupiny	skupina	k1gFnSc2	skupina
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
svítivost	svítivost	k1gFnSc1	svítivost
je	být	k5eAaImIp3nS	být
však	však	k9	však
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
průměr	průměr	k1gInSc1	průměr
hvězd	hvězda	k1gFnPc2	hvězda
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
asi	asi	k9	asi
na	na	k7c4	na
polovičku	polovička	k1gFnSc4	polovička
hodnot	hodnota	k1gFnPc2	hodnota
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
svítivosti	svítivost	k1gFnSc2	svítivost
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
tvořen	tvořit	k5eAaImNgInS	tvořit
červenými	červený	k2eAgInPc7d1	červený
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
netvoří	tvořit	k5eNaImIp3nS	tvořit
vícenásobný	vícenásobný	k2eAgInSc4d1	vícenásobný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
či	či	k8xC	či
dvojhvězdu	dvojhvězda	k1gFnSc4	dvojhvězda
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
nepovedené	povedený	k2eNgFnSc6d1	nepovedená
dvojhvězdě	dvojhvězda	k1gFnSc6	dvojhvězda
Slunce	slunce	k1gNnSc2	slunce
–	–	k?	–
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
také	také	k9	také
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgMnSc1d1	žlutý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1	elektromagnetické
spektru	spektrum	k1gNnSc6	spektrum
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
spojité	spojitý	k2eAgNnSc4d1	spojité
spektrum	spektrum	k1gNnSc4	spektrum
s	s	k7c7	s
absorpčními	absorpční	k2eAgFnPc7d1	absorpční
spektrálními	spektrální	k2eAgFnPc7d1	spektrální
čárami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
800	[number]	k4	800
K	K	kA	K
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
maximum	maximum	k1gNnSc4	maximum
vyzařování	vyzařování	k1gNnSc2	vyzařování
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
501	[number]	k4	501
nm	nm	k?	nm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
(	(	kIx(	(
<g/>
žluto	žluto	k1gNnSc4	žluto
<g/>
)	)	kIx)	)
<g/>
zelené	zelený	k2eAgFnPc1d1	zelená
části	část	k1gFnPc1	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
ho	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
žluté	žlutý	k2eAgFnPc1d1	žlutá
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
modré	modrý	k2eAgFnSc2d1	modrá
části	část	k1gFnSc2	část
spektra	spektrum	k1gNnSc2	spektrum
záření	záření	k1gNnSc2	záření
ubývá	ubývat	k5eAaImIp3nS	ubývat
rychleji	rychle	k6eAd2	rychle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
však	však	k9	však
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vidět	vidět	k5eAaImF	vidět
mimo	mimo	k7c4	mimo
zemskou	zemský	k2eAgFnSc4d1	zemská
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
jako	jako	k9	jako
oranžové	oranžový	k2eAgNnSc1d1	oranžové
nebo	nebo	k8xC	nebo
červené	červený	k2eAgNnSc1d1	červené
kvůli	kvůli	k7c3	kvůli
Rayleighovu	Rayleighův	k2eAgInSc3d1	Rayleighův
rozptylu	rozptyl	k1gInSc3	rozptyl
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
mocnině	mocnina	k1gFnSc6	mocnina
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
modré	modrý	k2eAgNnSc1d1	modré
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
poloviční	poloviční	k2eAgFnSc4d1	poloviční
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
než	než	k8xS	než
červené	červený	k2eAgInPc4d1	červený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
vykonalo	vykonat	k5eAaPmAgNnS	vykonat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
nižší	nízký	k2eAgFnSc7d2	nižší
a	a	k8xC	a
hustší	hustý	k2eAgFnSc7d2	hustší
vrstvou	vrstva	k1gFnSc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
oblohy	obloha	k1gFnSc2	obloha
je	být	k5eAaImIp3nS	být
také	také	k9	také
způsobena	způsobit	k5eAaPmNgFnS	způsobit
Rayleighovým	Rayleighův	k2eAgInSc7d1	Rayleighův
rozptylem	rozptyl	k1gInSc7	rozptyl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
Slunce	slunce	k1gNnSc1	slunce
zdát	zdát	k5eAaImF	zdát
šišaté	šišatý	k2eAgNnSc4d1	šišaté
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
zkreslen	zkreslit	k5eAaPmNgInS	zkreslit
atmosférickou	atmosférický	k2eAgFnSc7d1	atmosférická
refrakcí	refrakce	k1gFnSc7	refrakce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
obzoru	obzor	k1gInSc2	obzor
hodnotu	hodnota	k1gFnSc4	hodnota
0,5	[number]	k4	0,5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
když	když	k8xS	když
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
obzoru	obzor	k1gInSc2	obzor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
optický	optický	k2eAgInSc4d1	optický
klam	klam	k1gInSc4	klam
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
se	s	k7c7	s
zploštěním	zploštění	k1gNnSc7	zploštění
přibližně	přibližně	k6eAd1	přibližně
pouhých	pouhý	k2eAgFnPc2d1	pouhá
10	[number]	k4	10
km	km	kA	km
polárního	polární	k2eAgInSc2d1	polární
průměru	průměr	k1gInSc2	průměr
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rovníkovému	rovníkový	k2eAgInSc3d1	rovníkový
průměru	průměr	k1gInSc3	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
téměř	téměř	k6eAd1	téměř
ideální	ideální	k2eAgInSc1d1	ideální
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
částečně	částečně	k6eAd1	částečně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
je	být	k5eAaImIp3nS	být
odstředivý	odstředivý	k2eAgInSc1d1	odstředivý
efekt	efekt	k1gInSc1	efekt
sluneční	sluneční	k2eAgFnSc2d1	sluneční
rotace	rotace	k1gFnSc2	rotace
asi	asi	k9	asi
18	[number]	k4	18
<g/>
milionkrát	milionkrát	k6eAd1	milionkrát
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
gravitační	gravitační	k2eAgFnSc1d1	gravitační
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgFnSc1	všechen
energie	energie	k1gFnSc1	energie
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
vyzařována	vyzařován	k2eAgFnSc1d1	vyzařována
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
formy	forma	k1gFnPc4	forma
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
reakce	reakce	k1gFnSc2	reakce
pp-řetězce	pp-řetězka	k1gFnSc3	pp-řetězka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
hélium	hélium	k1gNnSc4	hélium
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
uvolňování	uvolňování	k1gNnSc2	uvolňování
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
Slunce	slunce	k1gNnSc2	slunce
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
a	a	k8xC	a
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
700	[number]	k4	700
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
695	[number]	k4	695
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
4,5	[number]	k4	4,5
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
je	být	k5eAaImIp3nS	být
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
96	[number]	k4	96
%	%	kIx~	%
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
4	[number]	k4	4
%	%	kIx~	%
elektronová	elektronový	k2eAgNnPc4d1	elektronové
neutrina	neutrino	k1gNnPc4	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
včetně	včetně	k7c2	včetně
viditelného	viditelný	k2eAgNnSc2d1	viditelné
záření	záření	k1gNnSc2	záření
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
fotosféry	fotosféra	k1gFnSc2	fotosféra
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
Slunce	slunce	k1gNnSc1	slunce
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
tolik	tolik	k6eAd1	tolik
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
stačilo	stačit	k5eAaBmAgNnS	stačit
pokrýt	pokrýt	k5eAaPmF	pokrýt
potřeby	potřeba	k1gFnPc4	potřeba
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Slunce	slunce	k1gNnSc2	slunce
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fotonů	foton	k1gInPc2	foton
gama	gama	k1gNnSc4	gama
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
konvekce	konvekce	k1gFnSc2	konvekce
<g/>
,	,	kIx,	,
absorpce	absorpce	k1gFnSc2	absorpce
a	a	k8xC	a
emise	emise	k1gFnSc2	emise
<g/>
,	,	kIx,	,
opouští	opouštět	k5eAaImIp3nS	opouštět
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
radiace	radiace	k1gFnSc2	radiace
a	a	k8xC	a
neutrin	neutrino	k1gNnPc2	neutrino
(	(	kIx(	(
<g/>
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
také	také	k9	také
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kinetické	kinetický	k2eAgFnSc6d1	kinetická
energii	energie	k1gFnSc6	energie
a	a	k8xC	a
termální	termální	k2eAgFnSc1d1	termální
energie	energie	k1gFnSc1	energie
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
jako	jako	k9	jako
energie	energie	k1gFnSc1	energie
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
a	a	k8xC	a
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
přitahovány	přitahovat	k5eAaImNgInP	přitahovat
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
hydrostatické	hydrostatický	k2eAgNnSc4d1	hydrostatické
a	a	k8xC	a
energetické	energetický	k2eAgNnSc4d1	energetické
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnPc4d1	sluneční
neutrina	neutrino	k1gNnPc4	neutrino
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
detekovat	detekovat	k5eAaImF	detekovat
pomocí	pomocí	k7c2	pomocí
neutrinového	utrinový	k2eNgInSc2d1	neutrinový
detektoru	detektor	k1gInSc2	detektor
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
slunečních	sluneční	k2eAgNnPc2d1	sluneční
neutrin	neutrino	k1gNnPc2	neutrino
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
může	moct	k5eAaImIp3nS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jádře	jádro	k1gNnSc6	jádro
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
putují	putovat	k5eAaImIp3nP	putovat
tisíce	tisíc	k4xCgInPc1	tisíc
až	až	k9	až
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc4d1	současný
počet	počet	k1gInSc4	počet
pozorovaných	pozorovaný	k2eAgNnPc2d1	pozorované
slunečných	slunečný	k2eAgNnPc2d1	slunečné
neutrin	neutrino	k1gNnPc2	neutrino
je	být	k5eAaImIp3nS	být
však	však	k9	však
asi	asi	k9	asi
třikrát	třikrát	k6eAd1	třikrát
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
počet	počet	k1gInSc1	počet
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
předpovídán	předpovídat	k5eAaImNgInS	předpovídat
modelem	model	k1gInSc7	model
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
předpokládaným	předpokládaný	k2eAgInSc7d1	předpokládaný
a	a	k8xC	a
skutečným	skutečný	k2eAgInSc7d1	skutečný
počtem	počet	k1gInSc7	počet
neutrin	neutrino	k1gNnPc2	neutrino
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nepodařilo	podařit	k5eNaPmAgNnS	podařit
uspokojivě	uspokojivě	k6eAd1	uspokojivě
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
pomocí	pomocí	k7c2	pomocí
neutrinového	utrinový	k2eNgInSc2d1	neutrinový
detektoru	detektor	k1gInSc2	detektor
Subdury	Subdura	k1gFnSc2	Subdura
Neutrino	neutrino	k1gNnSc1	neutrino
Observatory	Observator	k1gInPc4	Observator
však	však	k9	však
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
neutrina	neutrino	k1gNnPc1	neutrino
mají	mít	k5eAaImIp3nP	mít
nenulovou	nulový	k2eNgFnSc4d1	nenulová
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
že	že	k8xS	že
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
zevnitř	zevnitř	k6eAd1	zevnitř
Slunce	slunce	k1gNnSc2	slunce
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
oscilují	oscilovat	k5eAaImIp3nP	oscilovat
mezi	mezi	k7c7	mezi
elektronovým	elektronový	k2eAgNnSc7d1	elektronové
neutrinem	neutrino	k1gNnSc7	neutrino
<g/>
,	,	kIx,	,
mionovým	mionův	k2eAgNnSc7d1	mionův
neutrinem	neutrino	k1gNnSc7	neutrino
a	a	k8xC	a
tauónovým	tauónový	k2eAgNnSc7d1	tauónový
neutrinem	neutrino	k1gNnSc7	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
detektory	detektor	k1gInPc1	detektor
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
chlóru	chlór	k1gInSc6	chlór
a	a	k8xC	a
galiu	galium	k1gNnSc6	galium
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
zachytit	zachytit	k5eAaPmF	zachytit
jen	jen	k9	jen
elektronová	elektronový	k2eAgNnPc4d1	elektronové
neutrina	neutrino	k1gNnPc4	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
už	už	k6eAd1	už
Slunce	slunce	k1gNnSc1	slunce
spotřebovalo	spotřebovat	k5eAaPmAgNnS	spotřebovat
polovinu	polovina	k1gFnSc4	polovina
svých	svůj	k3xOyFgFnPc2	svůj
zásob	zásoba	k1gFnPc2	zásoba
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
bude	být	k5eAaImBp3nS	být
ještě	ještě	k9	ještě
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
probíhat	probíhat	k5eAaImF	probíhat
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
většina	většina	k1gFnSc1	většina
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
vodík	vodík	k1gInSc4	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
naruší	narušit	k5eAaPmIp3nS	narušit
se	se	k3xPyFc4	se
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
hydrostatická	hydrostatický	k2eAgFnSc1d1	hydrostatická
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
povede	povést	k5eAaPmIp3nS	povést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
červeným	červený	k2eAgMnSc7d1	červený
obrem	obr	k1gMnSc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšováním	zvětšování	k1gNnSc7	zvětšování
průměru	průměr	k1gInSc2	průměr
Slunce	slunce	k1gNnSc2	slunce
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
planety	planeta	k1gFnPc1	planeta
budou	být	k5eAaImBp3nP	být
pohlceny	pohltit	k5eAaPmNgFnP	pohltit
rozšiřujícím	rozšiřující	k2eAgFnPc3d1	rozšiřující
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
i	i	k9	i
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
Slunce	slunce	k1gNnSc2	slunce
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
zcela	zcela	k6eAd1	zcela
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
složení	složení	k1gNnSc6	složení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
výzkumu	výzkum	k1gInSc2	výzkum
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
homogenně	homogenně	k6eAd1	homogenně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
hloubce	hloubka	k1gFnSc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
vlivem	vlivem	k7c2	vlivem
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc4d2	veliký
obsah	obsah	k1gInSc4	obsah
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc4	vodík
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
již	již	k9	již
34	[number]	k4	34
%	%	kIx~	%
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
64	[number]	k4	64
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
současně	současně	k6eAd1	současně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
většina	většina	k1gFnSc1	většina
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Metalicita	Metalicita	k1gFnSc1	Metalicita
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
poměr	poměr	k1gInSc1	poměr
obsahu	obsah	k1gInSc2	obsah
těžších	těžký	k2eAgInPc2d2	těžší
a	a	k8xC	a
lehčích	lehký	k2eAgInPc2d2	lehčí
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hmotnostních	hmotnostní	k2eAgInPc6d1	hmotnostní
poměrech	poměr	k1gInPc6	poměr
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
zhruba	zhruba	k6eAd1	zhruba
3⁄	3⁄	k?	3⁄
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
1⁄	1⁄	k?	1⁄
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
měla	mít	k5eAaImAgFnS	mít
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Genesis	Genesis	k1gFnSc1	Genesis
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
výzkum	výzkum	k1gInSc4	výzkum
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
odebrání	odebrání	k1gNnSc2	odebrání
jeho	jeho	k3xOp3gInPc2	jeho
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
návratového	návratový	k2eAgInSc2d1	návratový
modulu	modul	k1gInSc2	modul
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
však	však	k9	však
neotevřely	otevřít	k5eNaPmAgInP	otevřít
padáky	padák	k1gInPc1	padák
a	a	k8xC	a
pouzdro	pouzdro	k1gNnSc1	pouzdro
se	se	k3xPyFc4	se
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vzorků	vzorek	k1gInPc2	vzorek
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgNnSc1d1	sluneční
těleso	těleso	k1gNnSc1	těleso
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
zvanou	zvaný	k2eAgFnSc7d1	zvaná
heliosféra	heliosféra	k1gFnSc1	heliosféra
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jádro	jádro	k1gNnSc1	jádro
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
175	[number]	k4	175
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,5	[number]	k4	1,5
<g/>
×	×	k?	×
<g/>
107	[number]	k4	107
K	K	kA	K
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
plazmatu	plazma	k1gNnSc2	plazma
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
130	[number]	k4	130
000	[number]	k4	000
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
fúze	fúze	k1gFnSc1	fúze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostředí	prostředí	k1gNnSc6	prostředí
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
atomy	atom	k1gInPc1	atom
rozloženy	rozložit	k5eAaPmNgInP	rozložit
na	na	k7c4	na
volná	volný	k2eAgNnPc4d1	volné
jádra	jádro	k1gNnPc4	jádro
a	a	k8xC	a
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
za	za	k7c4	za
uvolnění	uvolnění	k1gNnSc4	uvolnění
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
fúze	fúze	k1gFnSc1	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
spálí	spálit	k5eAaPmIp3nS	spálit
700	[number]	k4	700
000	[number]	k4	000
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvnitř	uvnitř	k6eAd1	uvnitř
Slunce	slunce	k1gNnSc1	slunce
děj	děj	k1gInSc1	děj
probíhá	probíhat	k5eAaImIp3nS	probíhat
nějak	nějak	k6eAd1	nějak
překotně	překotně	k6eAd1	překotně
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
výkonu	výkon	k1gInSc2	výkon
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
pouhých	pouhý	k2eAgInPc2d1	pouhý
0,19	[number]	k4	0,19
mW	mW	k?	mW
<g/>
.	.	kIx.	.
<g/>
kg	kg	kA	kg
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
mezistupňů	mezistupeň	k1gInPc2	mezistupeň
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
proton-protonovém	protonrotonový	k2eAgNnSc6d1	proton-protonový
cyklu	cyklus	k1gInSc6	cyklus
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
čtyř	čtyři	k4xCgFnPc2	čtyři
protonů	proton	k1gInPc2	proton
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
částici	částice	k1gFnSc4	částice
alfa	alfa	k1gNnSc2	alfa
–	–	k?	–
jádra	jádro	k1gNnSc2	jádro
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
protonů	proton	k1gInPc2	proton
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
neutrony	neutron	k1gInPc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Řetězec	řetězec	k1gInSc1	řetězec
těchto	tento	k3xDgFnPc2	tento
reakcí	reakce	k1gFnPc2	reakce
produkuje	produkovat	k5eAaImIp3nS	produkovat
mnoho	mnoho	k6eAd1	mnoho
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fotonů	foton	k1gInPc2	foton
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
gama	gama	k1gNnSc2	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
1	[number]	k4	1
gramu	gram	k1gInSc2	gram
vodíku	vodík	k1gInSc2	vodík
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
hélium	hélium	k1gNnSc4	hélium
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
1012	[number]	k4	1012
J	J	kA	J
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
pronikají	pronikat	k5eAaImIp3nP	pronikat
k	k	k7c3	k
chladnějšímu	chladný	k2eAgInSc3d2	chladnější
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
trvá	trvat	k5eAaImIp3nS	trvat
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
odhadů	odhad	k1gInPc2	odhad
od	od	k7c2	od
asi	asi	k9	asi
17	[number]	k4	17
tisíců	tisíc	k4xCgInPc2	tisíc
po	po	k7c4	po
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
předají	předat	k5eAaPmIp3nP	předat
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
energie	energie	k1gFnSc2	energie
hmotě	hmota	k1gFnSc3	hmota
Slunce	slunka	k1gFnSc3	slunka
a	a	k8xC	a
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
fotony	foton	k1gInPc1	foton
o	o	k7c6	o
mnohem	mnohem	k6eAd1	mnohem
delších	dlouhý	k2eAgFnPc6d2	delší
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
fotony	foton	k1gInPc1	foton
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
dostanou	dostat	k5eAaPmIp3nP	dostat
vzniklá	vzniklý	k2eAgNnPc4d1	vzniklé
neutrina	neutrino	k1gNnPc4	neutrino
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgMnPc4	který
hmota	hmota	k1gFnSc1	hmota
Slunce	slunce	k1gNnSc2	slunce
prakticky	prakticky	k6eAd1	prakticky
není	být	k5eNaImIp3nS	být
překážkou	překážka	k1gFnSc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vrstva	vrstva	k1gFnSc1	vrstva
v	v	k7c6	v
zářivé	zářivý	k2eAgFnSc6d1	zářivá
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zářivá	zářivý	k2eAgFnSc1d1	zářivá
rovnováha	rovnováha	k1gFnSc1	rovnováha
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k9	co
atomy	atom	k1gInPc1	atom
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
také	také	k9	také
později	pozdě	k6eAd2	pozdě
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
gravitační	gravitační	k2eAgInSc1d1	gravitační
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
výrazné	výrazný	k2eAgNnSc1d1	výrazné
zpomalení	zpomalení	k1gNnSc1	zpomalení
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každý	každý	k3xTgInSc1	každý
foton	foton	k1gInSc1	foton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pohlcen	pohltit	k5eAaPmNgInS	pohltit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
vyzářen	vyzářit	k5eAaPmNgInS	vyzářit
v	v	k7c6	v
náhodném	náhodný	k2eAgInSc6d1	náhodný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotony	foton	k1gInPc4	foton
touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
projdou	projít	k5eAaPmIp3nP	projít
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
7	[number]	k4	7
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
20	[number]	k4	20
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
0,2	[number]	k4	0,2
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tenkou	tenký	k2eAgFnSc4d1	tenká
mezivrstvu	mezivrstva	k1gFnSc4	mezivrstva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
měřením	měření	k1gNnSc7	měření
americké	americký	k2eAgFnSc2d1	americká
družice	družice	k1gFnSc2	družice
SOHO	SOHO	kA	SOHO
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
generaci	generace	k1gFnSc3	generace
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mění	měnit	k5eAaImIp3nS	měnit
rychlost	rychlost	k1gFnSc4	rychlost
proudů	proud	k1gInPc2	proud
plazmatu	plazma	k1gNnSc2	plazma
a	a	k8xC	a
rotační	rotační	k2eAgFnSc4d1	rotační
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
asi	asi	k9	asi
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
je	být	k5eAaImIp3nS	být
nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
hrnci	hrnec	k1gInPc7	hrnec
s	s	k7c7	s
vroucí	vroucí	k2eAgFnSc7d1	vroucí
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
způsob	způsob	k1gInSc4	způsob
předávání	předávání	k1gNnSc2	předávání
energie	energie	k1gFnSc2	energie
pomocí	pomocí	k7c2	pomocí
záření	záření	k1gNnSc2	záření
málo	málo	k6eAd1	málo
účinný	účinný	k2eAgInSc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ionty	ion	k1gInPc1	ion
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
schopny	schopen	k2eAgInPc1d1	schopen
za	za	k7c2	za
nižších	nízký	k2eAgFnPc2d2	nižší
teplot	teplota	k1gFnPc2	teplota
fotony	foton	k1gInPc4	foton
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
neemitovat	emitovat	k5eNaBmF	emitovat
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
absorpci	absorpce	k1gFnSc3	absorpce
<g/>
.	.	kIx.	.
</s>
<s>
Studenější	studený	k2eAgFnSc1d2	studenější
hmota	hmota	k1gFnSc1	hmota
padá	padat	k5eAaImIp3nS	padat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ohřátá	ohřátý	k2eAgFnSc1d1	ohřátá
se	se	k3xPyFc4	se
dere	drát	k5eAaImIp3nS	drát
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
značné	značný	k2eAgFnPc4d1	značná
turbulence	turbulence	k1gFnPc4	turbulence
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
a	a	k8xC	a
promíchávání	promíchávání	k1gNnSc6	promíchávání
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přenosem	přenos	k1gInSc7	přenos
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
proudění	proudění	k1gNnSc1	proudění
čili	čili	k8xC	čili
konvekce	konvekce	k1gFnSc1	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
konvekce	konvekce	k1gFnSc2	konvekce
se	se	k3xPyFc4	se
přenášený	přenášený	k2eAgInSc1d1	přenášený
plyn	plyn	k1gInSc1	plyn
rychle	rychle	k6eAd1	rychle
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
a	a	k8xC	a
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
<g/>
.	.	kIx.	.
</s>
<s>
Výstupy	výstup	k1gInPc1	výstup
konvektivních	konvektivní	k2eAgInPc2d1	konvektivní
proudů	proud	k1gInPc2	proud
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zóně	zóna	k1gFnSc6	zóna
pozorovat	pozorovat	k5eAaImF	pozorovat
jako	jako	k9	jako
granuly	granula	k1gFnPc4	granula
či	či	k8xC	či
supergranuly	supergranout	k5eAaPmAgInP	supergranout
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
do	do	k7c2	do
6	[number]	k4	6
000	[number]	k4	000
K.	K.	kA	K.
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fotosféra	fotosféra	k1gFnSc1	fotosféra
<g/>
.	.	kIx.	.
</s>
<s>
Fotosféra	fotosféra	k1gFnSc1	fotosféra
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc4d1	viditelný
povrch	povrch	k1gInSc4	povrch
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
jako	jako	k9	jako
sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
viditelný	viditelný	k2eAgInSc1d1	viditelný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
střed	střed	k1gInSc1	střed
Slunce	slunce	k1gNnSc2	slunce
jasnější	jasný	k2eAgFnSc2d2	jasnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
okraje	okraj	k1gInPc1	okraj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
Slunce	slunce	k1gNnSc2	slunce
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
chladnější	chladný	k2eAgFnPc1d2	chladnější
oblasti	oblast	k1gFnPc1	oblast
fotosféry	fotosféra	k1gFnSc2	fotosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotosféře	fotosféra	k1gFnSc6	fotosféra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
vrcholky	vrcholek	k1gInPc4	vrcholek
vystupujících	vystupující	k2eAgInPc2d1	vystupující
proudů	proud	k1gInPc2	proud
z	z	k7c2	z
konvektivní	konvektivní	k2eAgFnSc2d1	konvektivní
zóny	zóna	k1gFnSc2	zóna
dosahující	dosahující	k2eAgFnSc7d1	dosahující
velikostí	velikost	k1gFnSc7	velikost
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
granulace	granulace	k1gFnSc2	granulace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nápadné	nápadný	k2eAgInPc1d1	nápadný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
sluneční	sluneční	k2eAgFnPc1d1	sluneční
skvrny	skvrna	k1gFnPc1	skvrna
a	a	k8xC	a
protuberance	protuberance	k1gFnPc1	protuberance
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1023	[number]	k4	1023
částic	částice	k1gFnPc2	částice
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
zápisu	zápis	k1gInSc6	zápis
3,5	[number]	k4	3,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
7	[number]	k4	7
do	do	k7c2	do
4,5	[number]	k4	4,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
5	[number]	k4	5
800	[number]	k4	800
K.	K.	kA	K.
Fotosféra	fotosféra	k1gFnSc1	fotosféra
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
oblastí	oblast	k1gFnSc7	oblast
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
200	[number]	k4	200
až	až	k9	až
400	[number]	k4	400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
jevem	jev	k1gInSc7	jev
ve	v	k7c6	v
fotosféře	fotosféra	k1gFnSc6	fotosféra
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc4	přítomnost
granulí	granule	k1gFnPc2	granule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc4d1	různé
zrna	zrno	k1gNnPc4	zrno
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
od	od	k7c2	od
200	[number]	k4	200
do	do	k7c2	do
1	[number]	k4	1
800	[number]	k4	800
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výstupné	výstupný	k2eAgInPc4d1	výstupný
konvektivní	konvektivní	k2eAgInPc4d1	konvektivní
proudy	proud	k1gInPc4	proud
ze	z	k7c2	z
svrchních	svrchní	k2eAgFnPc2d1	svrchní
oblastí	oblast	k1gFnPc2	oblast
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
než	než	k8xS	než
okolní	okolní	k2eAgFnPc1d1	okolní
fotosféra	fotosféra	k1gFnSc1	fotosféra
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chromosféra	chromosféra	k1gFnSc1	chromosféra
<g/>
.	.	kIx.	.
</s>
<s>
Chromosféra	chromosféra	k1gFnSc1	chromosféra
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
tenká	tenký	k2eAgFnSc1d1	tenká
a	a	k8xC	a
řídká	řídký	k2eAgFnSc1d1	řídká
vrstva	vrstva	k1gFnSc1	vrstva
nad	nad	k7c7	nad
fotosférou	fotosféra	k1gFnSc7	fotosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgNnSc1d1	červené
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
teplota	teplota	k1gFnSc1	teplota
stoupá	stoupat	k5eAaImIp3nS	stoupat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
není	být	k5eNaImIp3nS	být
všude	všude	k6eAd1	všude
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
3	[number]	k4	3
000	[number]	k4	000
km	km	kA	km
pozvolna	pozvolna	k6eAd1	pozvolna
stoupá	stoupat	k5eAaImIp3nS	stoupat
asi	asi	k9	asi
k	k	k7c3	k
hodnotě	hodnota	k1gFnSc3	hodnota
6	[number]	k4	6
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
narůstá	narůstat	k5eAaImIp3nS	narůstat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nestabilitou	nestabilita	k1gFnSc7	nestabilita
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
chromosférické	chromosférický	k2eAgFnPc4d1	chromosférická
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrstva	vrstva	k1gFnSc1	vrstva
silně	silně	k6eAd1	silně
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
12	[number]	k4	12
000	[number]	k4	000
do	do	k7c2	do
15	[number]	k4	15
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
viditelná	viditelný	k2eAgFnSc1d1	viditelná
jako	jako	k9	jako
načervenalý	načervenalý	k2eAgInSc4d1	načervenalý
světelný	světelný	k2eAgInSc4d1	světelný
úkaz	úkaz	k1gInSc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximum	maximum	k1gNnSc1	maximum
jejího	její	k3xOp3gNnSc2	její
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
čáře	čára	k1gFnSc6	čára
H-alfa	Hlf	k1gMnSc2	H-alf
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
světla	světlo	k1gNnSc2	světlo
656,7	[number]	k4	656,7
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
hustotě	hustota	k1gFnSc3	hustota
částic	částice	k1gFnPc2	částice
75	[number]	k4	75
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Přechodová	přechodový	k2eAgFnSc1d1	přechodová
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Přechodová	přechodový	k2eAgFnSc1d1	přechodová
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc4	některý
zdroje	zdroj	k1gInPc4	zdroj
jí	on	k3xPp3gFnSc3	on
samostatně	samostatně	k6eAd1	samostatně
nevyčleňují	vyčleňovat	k5eNaImIp3nP	vyčleňovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
vrstva	vrstva	k1gFnSc1	vrstva
sluneční	sluneční	k2eAgFnSc2d1	sluneční
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
korónu	koróna	k1gFnSc4	koróna
od	od	k7c2	od
chladnější	chladný	k2eAgFnSc2d2	chladnější
fotosféry	fotosféra	k1gFnSc2	fotosféra
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
náhle	náhle	k6eAd1	náhle
mění	měnit	k5eAaImIp3nS	měnit
z	z	k7c2	z
20	[number]	k4	20
000	[number]	k4	000
K	k	k7c3	k
(	(	kIx(	(
<g/>
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
chromosférou	chromosféra	k1gFnSc7	chromosféra
<g/>
)	)	kIx)	)
až	až	k9	až
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
K	k	k7c3	k
(	(	kIx(	(
<g/>
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
korónou	koróna	k1gFnSc7	koróna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
převážně	převážně	k6eAd1	převážně
přes	přes	k7c4	přes
ultrafialovou	ultrafialový	k2eAgFnSc4d1	ultrafialová
část	část	k1gFnSc4	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koróna	koróna	k1gFnSc1	koróna
<g/>
.	.	kIx.	.
</s>
<s>
Koróna	koróna	k1gFnSc1	koróna
nemá	mít	k5eNaImIp3nS	mít
vnější	vnější	k2eAgFnSc4d1	vnější
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
udáváno	udáván	k2eAgNnSc1d1	udáváno
<g/>
,	,	kIx,	,
že	že	k8xS	že
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
koróně	koróna	k1gFnSc6	koróna
o	o	k7c6	o
tří	tři	k4xCgFnPc2	tři
řády	řád	k1gInPc7	řád
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
teplotu	teplota	k1gFnSc4	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
K	k	k7c3	k
až	až	k8xS	až
6	[number]	k4	6
000	[number]	k4	000
000	[number]	k4	000
K.	K.	kA	K.
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ohřev	ohřev	k1gInSc4	ohřev
pomocí	pomocí	k7c2	pomocí
Alfénových	Alfénový	k2eAgFnPc2d1	Alfénový
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Koróna	koróna	k1gFnSc1	koróna
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
řídká	řídký	k2eAgFnSc1d1	řídká
(	(	kIx(	(
<g/>
hustota	hustota	k1gFnSc1	hustota
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1011	[number]	k4	1011
částic	částice	k1gFnPc2	částice
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
normálně	normálně	k6eAd1	normálně
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
přezářena	přezářit	k5eAaPmNgFnS	přezářit
spodnější	spodní	k2eAgFnSc7d2	spodnější
fotosférou	fotosféra	k1gFnSc7	fotosféra
<g/>
;	;	kIx,	;
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
zatměních	zatmění	k1gNnPc6	zatmění
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
koronografu	koronograf	k1gInSc2	koronograf
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
koróně	koróna	k1gFnSc6	koróna
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
erupce	erupce	k1gFnPc1	erupce
a	a	k8xC	a
protuberance	protuberance	k1gFnPc1	protuberance
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgNnSc1d1	silné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
hodnotu	hodnota	k1gFnSc4	hodnota
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
tesla	tesla	k1gFnSc1	tesla
<g/>
,	,	kIx,	,
lokálně	lokálně	k6eAd1	lokálně
pole	pole	k1gNnSc2	pole
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
tesla	tesla	k1gFnSc1	tesla
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
polem	pole	k1gNnSc7	pole
souvisí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
magneticky	magneticky	k6eAd1	magneticky
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
polarita	polarita	k1gFnSc1	polarita
jeho	jeho	k3xOp3gInPc2	jeho
pólů	pól	k1gInPc2	pól
a	a	k8xC	a
orientace	orientace	k1gFnSc1	orientace
jeho	jeho	k3xOp3gFnPc2	jeho
siločar	siločára	k1gFnPc2	siločára
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
22	[number]	k4	22
ročním	roční	k2eAgInSc7d1	roční
slunečním	sluneční	k2eAgInSc7d1	sluneční
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
maximu	maximum	k1gNnSc6	maximum
slunečního	sluneční	k2eAgInSc2d1	sluneční
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
vnímat	vnímat	k5eAaImF	vnímat
zastoupený	zastoupený	k2eAgInSc4d1	zastoupený
dvojpólový	dvojpólový	k2eAgInSc4d1	dvojpólový
moment	moment	k1gInSc4	moment
<g/>
.	.	kIx.	.
</s>
<s>
Silokřivky	silokřivka	k1gFnPc1	silokřivka
jsou	být	k5eAaImIp3nP	být
vlivem	vliv	k1gInSc7	vliv
rotace	rotace	k1gFnSc2	rotace
Slunce	slunce	k1gNnSc2	slunce
tvarovány	tvarován	k2eAgFnPc4d1	tvarována
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
Archimedových	Archimedův	k2eAgFnPc2d1	Archimedova
spirál	spirála	k1gFnPc2	spirála
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
obíhající	obíhající	k2eAgNnPc1d1	obíhající
tělesa	těleso	k1gNnPc1	těleso
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
procházejí	procházet	k5eAaImIp3nP	procházet
střídavě	střídavě	k6eAd1	střídavě
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
směry	směr	k1gInPc7	směr
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
plyno-prachové	plynorachový	k2eAgFnSc2d1	plyno-prachový
sluneční	sluneční	k2eAgFnSc2d1	sluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
objekty	objekt	k1gInPc1	objekt
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
posledních	poslední	k2eAgNnPc2d1	poslední
měření	měření	k1gNnPc2	měření
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
všude	všude	k6eAd1	všude
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
složka	složka	k1gFnSc1	složka
celkového	celkový	k2eAgNnSc2d1	celkové
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
lokální	lokální	k2eAgFnSc1d1	lokální
magnetická	magnetický	k2eAgFnSc1d1	magnetická
pole	pole	k1gFnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
aktivních	aktivní	k2eAgFnPc2d1	aktivní
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgNnSc2	tento
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gInSc4	jeho
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
fotosférických	fotosférický	k2eAgInPc2d1	fotosférický
<g/>
,	,	kIx,	,
chromosférických	chromosférický	k2eAgInPc2d1	chromosférický
a	a	k8xC	a
koronálních	koronální	k2eAgInPc2d1	koronální
objektů	objekt	k1gInPc2	objekt
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
zcela	zcela	k6eAd1	zcela
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celou	celý	k2eAgFnSc4d1	celá
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sluneční	sluneční	k2eAgInSc4d1	sluneční
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc4	komplex
dynamických	dynamický	k2eAgInPc2d1	dynamický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
slunečním	sluneční	k2eAgInSc6d1	sluneční
povrchu	povrch	k1gInSc6	povrch
nebo	nebo	k8xC	nebo
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
množství	množství	k1gNnSc2	množství
vyvrhovaných	vyvrhovaný	k2eAgFnPc2d1	vyvrhovaná
částic	částice	k1gFnPc2	částice
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgFnPc1d1	nabitá
a	a	k8xC	a
neutrální	neutrální	k2eAgFnPc1d1	neutrální
částice	částice	k1gFnPc1	částice
opouštějící	opouštějící	k2eAgFnSc4d1	opouštějící
korónu	koróna	k1gFnSc4	koróna
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
unikající	unikající	k2eAgNnSc1d1	unikající
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
zakřivených	zakřivený	k2eAgFnPc6d1	zakřivená
spirálovitých	spirálovitý	k2eAgFnPc6d1	spirálovitá
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
sledují	sledovat	k5eAaImIp3nP	sledovat
siločáry	siločára	k1gFnPc1	siločára
slunečního	sluneční	k2eAgNnSc2d1	sluneční
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
svojí	svůj	k3xOyFgFnSc2	svůj
rotace	rotace	k1gFnSc2	rotace
deformují	deformovat	k5eAaImIp3nP	deformovat
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
tzv.	tzv.	kA	tzv.
Parkerových	Parkerův	k2eAgFnPc2d1	Parkerova
spirál	spirála	k1gFnPc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
částic	částice	k1gFnPc2	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odklánějí	odklánět	k5eAaImIp3nP	odklánět
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
závisí	záviset	k5eAaImIp3nS	záviset
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
sluneční	sluneční	k2eAgFnPc1d1	sluneční
částice	částice	k1gFnPc1	částice
unikají	unikat	k5eAaImIp3nP	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
skrz	skrz	k7c4	skrz
koronální	koronální	k2eAgFnPc4d1	koronální
díry	díra	k1gFnPc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc7	každý
sekundou	sekunda	k1gFnSc7	sekunda
Slunce	slunce	k1gNnSc2	slunce
opustí	opustit	k5eAaPmIp3nS	opustit
asi	asi	k9	asi
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
tun	tuna	k1gFnPc2	tuna
slunečního	sluneční	k2eAgNnSc2d1	sluneční
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
až	až	k6eAd1	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
tak	tak	k8xS	tak
Slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
vyvrhlo	vyvrhnout	k5eAaPmAgNnS	vyvrhnout
přibližně	přibližně	k6eAd1	přibližně
okolo	okolo	k7c2	okolo
0,01	[number]	k4	0,01
%	%	kIx~	%
svojí	svůj	k3xOyFgFnSc2	svůj
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
periodě	perioda	k1gFnSc6	perioda
slunečního	sluneční	k2eAgInSc2d1	sluneční
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
též	též	k9	též
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
jeho	jeho	k3xOp3gNnSc2	jeho
záření	záření	k1gNnSc2	záření
–	–	k?	–
celkové	celkový	k2eAgNnSc4d1	celkové
vyzařování	vyzařování	k1gNnSc4	vyzařování
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
též	též	k9	též
nesprávně	správně	k6eNd1	správně
jako	jako	k8xC	jako
sluneční	sluneční	k2eAgFnSc1d1	sluneční
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pozvolným	pozvolný	k2eAgFnPc3d1	pozvolná
změnám	změna	k1gFnPc3	změna
vyzařované	vyzařovaný	k2eAgFnSc2d1	vyzařovaná
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
metr	metr	k1gInSc1	metr
slunečního	sluneční	k2eAgInSc2d1	sluneční
povrchu	povrch	k1gInSc2	povrch
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
přibližně	přibližně	k6eAd1	přibližně
62,86	[number]	k4	62,86
<g/>
×	×	k?	×
<g/>
106	[number]	k4	106
J	J	kA	J
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
Slunce	slunce	k1gNnSc2	slunce
pak	pak	k6eAd1	pak
3,826	[number]	k4	3,826
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
J.	J.	kA	J.
Na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
dopadá	dopadat	k5eAaImIp3nS	dopadat
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
1017	[number]	k4	1017
J	J	kA	J
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
polovička	polovička	k1gFnSc1	polovička
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
zpět	zpět	k6eAd1	zpět
o	o	k7c4	o
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Země	zem	k1gFnSc2	zem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
rychlosti	rychlost	k1gFnSc2	rychlost
od	od	k7c2	od
300	[number]	k4	300
do	do	k7c2	do
800	[number]	k4	800
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
výronem	výron	k1gInSc7	výron
koronální	koronální	k2eAgFnSc2d1	koronální
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sluneční	sluneční	k2eAgFnSc2d1	sluneční
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Výron	výron	k1gInSc1	výron
koronální	koronální	k2eAgFnSc2d1	koronální
hmoty	hmota	k1gFnSc2	hmota
má	mít	k5eAaImIp3nS	mít
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Země	zem	k1gFnSc2	zem
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
družice	družice	k1gFnPc4	družice
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
astronauti	astronaut	k1gMnPc1	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
i	i	k9	i
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
geomagnetické	geomagnetický	k2eAgFnSc2d1	geomagnetická
bouřky	bouřka	k1gFnSc2	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gInPc7	jejich
projevy	projev	k1gInPc7	projev
patří	patřit	k5eAaImIp3nS	patřit
občasné	občasný	k2eAgNnSc1d1	občasné
narušení	narušení	k1gNnSc1	narušení
navigačních	navigační	k2eAgInPc2d1	navigační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
výpadky	výpadek	k1gInPc1	výpadek
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
či	či	k8xC	či
případně	případně	k6eAd1	případně
vyřazení	vyřazení	k1gNnSc1	vyřazení
elektrických	elektrický	k2eAgInPc2d1	elektrický
rozvodů	rozvod	k1gInPc2	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
aktivita	aktivita	k1gFnSc1	aktivita
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
slunečním	sluneční	k2eAgInSc6d1	sluneční
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
střední	střední	k2eAgFnSc4d1	střední
délku	délka	k1gFnSc4	délka
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
tvar	tvar	k1gInSc1	tvar
<g/>
:	:	kIx,	:
náběh	náběh	k1gInSc1	náběh
cyklu	cyklus	k1gInSc2	cyklus
do	do	k7c2	do
maxima	maximum	k1gNnSc2	maximum
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
pokles	pokles	k1gInSc1	pokles
k	k	k7c3	k
minimu	minimum	k1gNnSc3	minimum
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejviditelnějším	viditelný	k2eAgInSc7d3	nejviditelnější
projevem	projev	k1gInSc7	projev
jsou	být	k5eAaImIp3nP	být
sluneční	sluneční	k2eAgFnPc1d1	sluneční
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
postupně	postupně	k6eAd1	postupně
objevovat	objevovat	k5eAaImF	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
slunečního	sluneční	k2eAgNnSc2d1	sluneční
minima	minimum	k1gNnSc2	minimum
se	se	k3xPyFc4	se
sluneční	sluneční	k2eAgFnPc1d1	sluneční
skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
téměř	téměř	k6eAd1	téměř
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
maxima	maximum	k1gNnSc2	maximum
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Maxima	Maxima	k1gFnSc1	Maxima
výskytu	výskyt	k1gInSc2	výskyt
skvrn	skvrna	k1gFnPc2	skvrna
nejsou	být	k5eNaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
současně	současně	k6eAd1	současně
s	s	k7c7	s
80	[number]	k4	80
<g/>
ročním	roční	k2eAgInSc7d1	roční
slunečním	sluneční	k2eAgInSc7d1	sluneční
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
projevy	projev	k1gInPc4	projev
patří	patřit	k5eAaImIp3nP	patřit
prototurbulence	prototurbulence	k1gFnPc1	prototurbulence
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
gigantické	gigantický	k2eAgInPc4d1	gigantický
výrony	výron	k1gInPc4	výron
plynu	plyn	k1gInSc2	plyn
do	do	k7c2	do
sluneční	sluneční	k2eAgFnSc2d1	sluneční
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tělesem	těleso	k1gNnSc7	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
745	[number]	k4	745
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
udržuje	udržovat	k5eAaImIp3nS	udržovat
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
dominanci	dominance	k1gFnSc4	dominance
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
blízko	blízko	k7c2	blízko
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
působení	působení	k1gNnSc2	působení
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
tělesa	těleso	k1gNnPc1	těleso
soustavy	soustava	k1gFnSc2	soustava
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgNnSc2	tento
těžiště	těžiště	k1gNnSc2	těžiště
v	v	k7c6	v
o	o	k7c6	o
mnoho	mnoho	k4c4	mnoho
řádů	řád	k1gInPc2	řád
větších	veliký	k2eAgFnPc6d2	veliký
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
korektní	korektní	k2eAgNnSc1d1	korektní
označit	označit	k5eAaPmF	označit
jejich	jejich	k3xOp3gInSc4	jejich
oběh	oběh	k1gInSc4	oběh
za	za	k7c4	za
oběh	oběh	k1gInSc4	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
to	ten	k3xDgNnSc1	ten
tělesy	těleso	k1gNnPc7	těleso
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
trpasličí	trpasličí	k2eAgFnPc1d1	trpasličí
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
planetky	planetka	k1gFnPc1	planetka
<g/>
,	,	kIx,	,
meteoroidy	meteoroid	k1gInPc1	meteoroid
<g/>
,	,	kIx,	,
komety	kometa	k1gFnPc1	kometa
a	a	k8xC	a
kosmický	kosmický	k2eAgInSc1d1	kosmický
prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
těleso	těleso	k1gNnSc1	těleso
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
překonat	překonat	k5eAaPmF	překonat
tzv.	tzv.	kA	tzv.
třetí	třetí	k4xOgFnSc4	třetí
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
tělesa	těleso	k1gNnSc2	těleso
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
–	–	k?	–
např.	např.	kA	např.
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
42,1	[number]	k4	42,1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
ze	z	k7c2	z
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
globule	globule	k1gFnSc2	globule
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
gravitačním	gravitační	k2eAgFnPc3d1	gravitační
kontrakcím	kontrakce	k1gFnPc3	kontrakce
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
zahušťovat	zahušťovat	k5eAaImF	zahušťovat
<g/>
.	.	kIx.	.
</s>
<s>
Odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
zrychlovala	zrychlovat	k5eAaImAgFnS	zrychlovat
rotaci	rotace	k1gFnSc4	rotace
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zploštění	zploštění	k1gNnSc3	zploštění
původně	původně	k6eAd1	původně
kulaté	kulatý	k2eAgFnPc4d1	kulatá
globule	globule	k1gFnPc4	globule
do	do	k7c2	do
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
utvořila	utvořit	k5eAaPmAgFnS	utvořit
protohvězda	protohvězda	k1gFnSc1	protohvězda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
které	který	k3yQgInPc4	který
rychle	rychle	k6eAd1	rychle
začala	začít	k5eAaPmAgFnS	začít
narůstat	narůstat	k5eAaImF	narůstat
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zažehnutí	zažehnutí	k1gNnSc3	zažehnutí
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
skupiny	skupina	k1gFnPc1	skupina
do	do	k7c2	do
které	který	k3yRgFnSc2	který
spadá	spadat	k5eAaImIp3nS	spadat
i	i	k9	i
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
ještě	ještě	k6eAd1	ještě
minimálně	minimálně	k6eAd1	minimálně
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
stabilní	stabilní	k2eAgFnSc2d1	stabilní
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
reakce	reakce	k1gFnSc1	reakce
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vnitru	vnitro	k1gNnSc6	vnitro
na	na	k7c6	na
krátko	krátko	k6eAd1	krátko
ustane	ustat	k5eAaPmIp3nS	ustat
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
přestane	přestat	k5eAaPmIp3nS	přestat
působit	působit	k5eAaImF	působit
proti	proti	k7c3	proti
vlastní	vlastní	k2eAgFnSc3d1	vlastní
gravitaci	gravitace	k1gFnSc3	gravitace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naruší	narušit	k5eAaPmIp3nS	narušit
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
rovnováhu	rovnováha	k1gFnSc4	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
smrští	smrštit	k5eAaPmIp3nS	smrštit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
hélia	hélium	k1gNnSc2	hélium
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
chemické	chemický	k2eAgInPc4d1	chemický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Slunce	slunce	k1gNnSc4	slunce
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
opět	opět	k6eAd1	opět
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
však	však	k9	však
začnou	začít	k5eAaPmIp3nP	začít
rozpínat	rozpínat	k5eAaImF	rozpínat
<g/>
,	,	kIx,	,
řídnout	řídnout	k5eAaImF	řídnout
a	a	k8xC	a
chladnout	chladnout	k5eAaImF	chladnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
zvětšujícím	zvětšující	k2eAgInSc6d1	zvětšující
se	se	k3xPyFc4	se
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
do	do	k7c2	do
stádia	stádium	k1gNnSc2	stádium
rudého	rudý	k1gMnSc2	rudý
obra	obr	k1gMnSc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpínající	rozpínající	k2eAgNnSc1d1	rozpínající
Slunce	slunce	k1gNnSc1	slunce
následně	následně	k6eAd1	následně
pohltí	pohltit	k5eAaPmIp3nS	pohltit
Merkur	Merkur	k1gInSc4	Merkur
<g/>
,	,	kIx,	,
Venuši	Venuše	k1gFnSc4	Venuše
a	a	k8xC	a
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
scénářů	scénář	k1gInPc2	scénář
i	i	k8xC	i
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
dojde	dojít	k5eAaPmIp3nS	dojít
opětovně	opětovně	k6eAd1	opětovně
k	k	k7c3	k
pozastavení	pozastavení	k1gNnSc3	pozastavení
termojaderných	termojaderný	k2eAgFnPc2d1	termojaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k9	již
žádná	žádný	k3yNgFnSc1	žádný
síla	síla	k1gFnSc1	síla
nebude	být	k5eNaImBp3nS	být
působit	působit	k5eAaImF	působit
proti	proti	k7c3	proti
gravitačnímu	gravitační	k2eAgNnSc3d1	gravitační
působení	působení	k1gNnSc3	působení
a	a	k8xC	a
Slunce	slunka	k1gFnSc3	slunka
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
smršťovat	smršťovat	k5eAaImF	smršťovat
do	do	k7c2	do
malého	malý	k2eAgNnSc2d1	malé
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
zkolabuje	zkolabovat	k5eAaPmIp3nS	zkolabovat
<g/>
,	,	kIx,	,
scvrkne	scvrknout	k5eAaPmIp3nS	scvrknout
se	se	k3xPyFc4	se
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
budou	být	k5eAaImBp3nP	být
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
pochodu	pochod	k1gInSc2	pochod
odmrštěny	odmrštit	k5eAaPmNgFnP	odmrštit
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
–	–	k?	–
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
planetární	planetární	k2eAgFnSc1d1	planetární
mlhovina	mlhovina	k1gFnSc1	mlhovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
rozšiřujících	rozšiřující	k2eAgInPc2d1	rozšiřující
se	se	k3xPyFc4	se
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
bude	být	k5eAaImBp3nS	být
pozvolna	pozvolna	k6eAd1	pozvolna
chladnout	chladnout	k5eAaImF	chladnout
<g/>
,	,	kIx,	,
až	až	k8xS	až
vychladne	vychladnout	k5eAaPmIp3nS	vychladnout
zcela	zcela	k6eAd1	zcela
<g/>
.	.	kIx.	.
</s>
<s>
Hypotézu	hypotéza	k1gFnSc4	hypotéza
rotace	rotace	k1gFnSc2	rotace
Slunce	slunce	k1gNnSc2	slunce
poprvé	poprvé	k6eAd1	poprvé
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Astronomia	Astronomia	k1gFnSc1	Astronomia
nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgFnSc1	všechen
hmota	hmota	k1gFnSc1	hmota
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
extrémní	extrémní	k2eAgFnSc3d1	extrémní
teplotě	teplota	k1gFnSc3	teplota
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Slunce	slunce	k1gNnSc1	slunce
rotovalo	rotovat	k5eAaImAgNnS	rotovat
rychleji	rychle	k6eAd2	rychle
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
než	než	k8xS	než
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
rozdílu	rozdíl	k1gInSc3	rozdíl
je	být	k5eAaImIp3nS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
deformované	deformovaný	k2eAgFnSc2d1	deformovaná
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
silotrubici	silotrubice	k1gFnSc3	silotrubice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deformace	deformace	k1gFnSc1	deformace
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
erupce	erupce	k1gFnSc1	erupce
a	a	k8xC	a
spouští	spouštět	k5eAaImIp3nS	spouštět
vznik	vznik	k1gInSc4	vznik
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
a	a	k8xC	a
protuberancí	protuberance	k1gFnPc2	protuberance
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
rotuje	rotovat	k5eAaImIp3nS	rotovat
okolo	okolo	k7c2	okolo
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
hvězdami	hvězda	k1gFnPc7	hvězda
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc2	rotace
není	být	k5eNaImIp3nS	být
všude	všude	k6eAd1	všude
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
25,38	[number]	k4	25,38
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
za	za	k7c4	za
36	[number]	k4	36
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rotace	rotace	k1gFnSc1	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
jako	jako	k9	jako
tuhé	tuhý	k2eAgNnSc1d1	tuhé
těleso	těleso	k1gNnSc1	těleso
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
rychlostí	rychlost	k1gFnSc7	rychlost
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
27	[number]	k4	27
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
synodická	synodický	k2eAgFnSc1d1	synodická
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
počítána	počítán	k2eAgFnSc1d1	počítána
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
okolním	okolní	k2eAgInPc3d1	okolní
nehybným	hybný	k2eNgInPc3d1	nehybný
objektům	objekt	k1gInPc3	objekt
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
otočí	otočit	k5eAaPmIp3nS	otočit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
25,38	[number]	k4	25,38
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Galaxie	galaxie	k1gFnSc2	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
ostatním	ostatní	k2eAgNnPc3d1	ostatní
tělesům	těleso	k1gNnPc3	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
téměř	téměř	k6eAd1	téměř
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Galaxie	galaxie	k1gFnSc2	galaxie
však	však	k8xC	však
Slunce	slunce	k1gNnSc2	slunce
není	být	k5eNaImIp3nS	být
stacionárním	stacionární	k2eAgNnSc7d1	stacionární
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
jádra	jádro	k1gNnSc2	jádro
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
30	[number]	k4	30
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
km	km	kA	km
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
střed	střed	k1gInSc4	střed
Galaxie	galaxie	k1gFnSc2	galaxie
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
25	[number]	k4	25
000	[number]	k4	000
až	až	k9	až
28	[number]	k4	28
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
jednou	jednou	k9	jednou
za	za	k7c4	za
226	[number]	k4	226
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oběh	oběh	k1gInSc1	oběh
nemá	mít	k5eNaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
kružnice	kružnice	k1gFnSc2	kružnice
a	a	k8xC	a
ani	ani	k9	ani
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
galaktických	galaktický	k2eAgInPc6d1	galaktický
epicyklech	epicykl	k1gInPc6	epicykl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
elipsu	elipsa	k1gFnSc4	elipsa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
střed	střed	k1gInSc4	střed
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
středu	střed	k1gInSc2	střed
Galaxie	galaxie	k1gFnSc2	galaxie
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
Slunce	slunce	k1gNnSc2	slunce
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
Galaxie	galaxie	k1gFnSc2	galaxie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
galaktický	galaktický	k2eAgInSc4d1	galaktický
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rotaci	rotace	k1gFnSc3	rotace
Země	zem	k1gFnSc2	zem
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc2	slunce
zdánlivě	zdánlivě	k6eAd1	zdánlivě
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Azimut	azimut	k1gInSc1	azimut
jeho	on	k3xPp3gInSc2	on
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejný	stejný	k2eAgInSc4d1	stejný
jen	jen	k9	jen
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgNnSc7	který
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
48	[number]	k4	48
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
Slunce	slunce	k1gNnSc1	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
42	[number]	k4	42
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníků	rovník	k1gInPc2	rovník
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
úhel	úhel	k1gInSc1	úhel
roven	roven	k2eAgInSc1d1	roven
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pólech	pól	k1gInPc6	pól
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc1	úhel
východu	východ	k1gInSc2	východ
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
a	a	k8xC	a
pod	pod	k7c4	pod
obzor	obzor	k1gInSc4	obzor
ho	on	k3xPp3gMnSc4	on
vynáší	vynášet	k5eAaImIp3nS	vynášet
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
ekliptice	ekliptika	k1gFnSc6	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úhel	úhel	k1gInSc1	úhel
současně	současně	k6eAd1	současně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
délku	délka	k1gFnSc4	délka
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
a	a	k8xC	a
nejmenší	malý	k2eAgFnPc4d3	nejmenší
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Oběh	oběh	k1gInSc1	oběh
Země	zem	k1gFnSc2	zem
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
ekliptice	ekliptika	k1gFnSc6	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
slunečný	slunečný	k2eAgInSc4d1	slunečný
tzv.	tzv.	kA	tzv.
synodický	synodický	k2eAgInSc4d1	synodický
den	den	k1gInSc4	den
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
minuty	minuta	k1gFnPc4	minuta
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
tzv.	tzv.	kA	tzv.
siderický	siderický	k2eAgInSc1d1	siderický
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
zdánlivými	zdánlivý	k2eAgNnPc7d1	zdánlivé
souhvězdími	souhvězdí	k1gNnPc7	souhvězdí
po	po	k7c6	po
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
znameními	znamení	k1gNnPc7	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
rok	rok	k1gInSc4	rok
přejde	přejít	k5eAaPmIp3nS	přejít
Slunce	slunce	k1gNnSc1	slunce
světovým	světový	k2eAgInSc7d1	světový
rovníkem	rovník	k1gInSc7	rovník
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
světového	světový	k2eAgInSc2d1	světový
rovníku	rovník	k1gInSc2	rovník
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdálí	vzdálit	k5eNaPmIp3nS	vzdálit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
sklon	sklon	k1gInSc4	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tedy	tedy	k9	tedy
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
maximální	maximální	k2eAgFnSc1d1	maximální
výška	výška	k1gFnSc1	výška
Slunce	slunce	k1gNnSc2	slunce
nad	nad	k7c7	nad
jižním	jižní	k2eAgInSc7d1	jižní
bodem	bod	k1gInSc7	bod
horizontu	horizont	k1gInSc2	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
48	[number]	k4	48
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
výška	výška	k1gFnSc1	výška
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
18,5	[number]	k4	18,5
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
zimní	zimní	k2eAgInSc1d1	zimní
slunovrat	slunovrat	k1gInSc1	slunovrat
<g/>
)	)	kIx)	)
do	do	k7c2	do
65,5	[number]	k4	65,5
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
letní	letní	k2eAgInSc1d1	letní
slunovrat	slunovrat	k1gInSc1	slunovrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oběh	oběh	k1gInSc1	oběh
Země	zem	k1gFnSc2	zem
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
pomocí	pomocí	k7c2	pomocí
ekliptikálních	ekliptikální	k2eAgFnPc2d1	ekliptikální
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
nerovnoměrnou	rovnoměrný	k2eNgFnSc7d1	nerovnoměrná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
nekulminuje	kulminovat	k5eNaImIp3nS	kulminovat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
ve	v	k7c4	v
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
pravým	pravý	k2eAgInSc7d1	pravý
slunečním	sluneční	k2eAgInSc7d1	sluneční
časem	čas	k1gInSc7	čas
a	a	k8xC	a
středním	střední	k2eAgInSc7d1	střední
slunečným	slunečný	k2eAgInSc7d1	slunečný
časem	čas	k1gInSc7	čas
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
časová	časový	k2eAgFnSc1d1	časová
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Zatmění	zatmění	k1gNnSc1	zatmění
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
astronomický	astronomický	k2eAgInSc1d1	astronomický
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Měsíc	měsíc	k1gInSc1	měsíc
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
mezi	mezi	k7c4	mezi
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gMnSc4	on
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
zakryje	zakrýt	k5eAaPmIp3nS	zakrýt
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
novu	nov	k1gInSc6	nov
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
i	i	k8xC	i
Měsíc	měsíc	k1gInSc1	měsíc
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
části	část	k1gFnSc6	část
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zatmění	zatmění	k1gNnSc1	zatmění
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
setmění	setmění	k1gNnSc3	setmění
<g/>
,	,	kIx,	,
ochlazení	ochlazení	k1gNnSc3	ochlazení
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
černého	černý	k2eAgInSc2d1	černý
středu	střed	k1gInSc2	střed
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
výrazná	výrazný	k2eAgFnSc1d1	výrazná
záře	záře	k1gFnSc1	záře
sluneční	sluneční	k2eAgFnSc2d1	sluneční
koróny	koróna	k1gFnSc2	koróna
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nP	objevit
se	se	k3xPyFc4	se
hvězdy	hvězda	k1gFnPc1	hvězda
i	i	k8xC	i
některé	některý	k3yIgFnPc1	některý
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
reakce	reakce	k1gFnPc4	reakce
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
průvodní	průvodní	k2eAgInPc1d1	průvodní
jevy	jev	k1gInPc1	jev
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
spojování	spojování	k1gNnSc3	spojování
události	událost	k1gFnSc2	událost
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
přisuzování	přisuzování	k1gNnSc1	přisuzování
mystických	mystický	k2eAgInPc2d1	mystický
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
duchovní	duchovní	k2eAgInPc1d1	duchovní
významy	význam	k1gInPc1	význam
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
většinou	většina	k1gFnSc7	většina
odmítány	odmítat	k5eAaImNgInP	odmítat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
snadnosti	snadnost	k1gFnSc2	snadnost
pochopení	pochopení	k1gNnSc1	pochopení
jeho	jeho	k3xOp3gFnPc2	jeho
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
obloze	obloha	k1gFnSc6	obloha
velmi	velmi	k6eAd1	velmi
jasné	jasný	k2eAgNnSc1d1	jasné
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
pozorovat	pozorovat	k5eAaImF	pozorovat
nechráněným	chráněný	k2eNgNnSc7d1	nechráněné
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gNnSc4	jeho
delší	dlouhý	k2eAgNnSc4d2	delší
pozorování	pozorování	k1gNnSc4	pozorování
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
Slunce	slunce	k1gNnSc2	slunce
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
fosfenové	fosfenový	k2eAgInPc4d1	fosfenový
vizuální	vizuální	k2eAgInPc4d1	vizuální
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
částečnou	částečný	k2eAgFnSc4d1	částečná
slepotu	slepota	k1gFnSc4	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
pohledu	pohled	k1gInSc6	pohled
působí	působit	k5eAaImIp3nS	působit
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
výkonem	výkon	k1gInSc7	výkon
asi	asi	k9	asi
4	[number]	k4	4
miliwatty	miliwatt	k1gInPc4	miliwatt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zahřívání	zahřívání	k1gNnSc3	zahřívání
sítnice	sítnice	k1gFnSc2	sítnice
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
možnému	možný	k2eAgNnSc3d1	možné
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
zeslabeno	zeslaben	k2eAgNnSc1d1	zeslabeno
rozptylem	rozptyl	k1gInSc7	rozptyl
světla	světlo	k1gNnSc2	světlo
díky	díky	k7c3	díky
obzvláště	obzvláště	k6eAd1	obzvláště
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
průchodu	průchod	k1gInSc3	průchod
zemskou	zemský	k2eAgFnSc4d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
;	;	kIx,	;
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
lze	lze	k6eAd1	lze
Slunce	slunce	k1gNnSc1	slunce
bez	bez	k7c2	bez
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Mlha	mlha	k1gFnSc1	mlha
<g/>
,	,	kIx,	,
atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
prach	prach	k1gInSc1	prach
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vlhkost	vlhkost	k1gFnSc1	vlhkost
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
atmosférickému	atmosférický	k2eAgNnSc3d1	atmosférické
zředění	zředění	k1gNnSc3	zředění
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pozorování	pozorování	k1gNnSc3	pozorování
Slunce	slunce	k1gNnSc2	slunce
optikou	optika	k1gFnSc7	optika
soustřeďující	soustřeďující	k2eAgNnSc1d1	soustřeďující
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
ochranného	ochranný	k2eAgInSc2d1	ochranný
filtru	filtr	k1gInSc2	filtr
tlumícího	tlumící	k2eAgNnSc2d1	tlumící
záření	záření	k1gNnSc2	záření
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
použít	použít	k5eAaPmF	použít
vhodný	vhodný	k2eAgInSc4d1	vhodný
filtr	filtr	k1gInSc4	filtr
<g/>
;	;	kIx,	;
improvizované	improvizovaný	k2eAgInPc1d1	improvizovaný
filtry	filtr	k1gInPc1	filtr
mohou	moct	k5eAaImIp3nP	moct
propustit	propustit	k5eAaPmF	propustit
UV	UV	kA	UV
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
jasnosti	jasnost	k1gFnSc6	jasnost
poškodit	poškodit	k5eAaPmF	poškodit
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Nefiltrovaný	filtrovaný	k2eNgInSc1d1	nefiltrovaný
dalekohled	dalekohled	k1gInSc1	dalekohled
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
doručit	doručit	k5eAaPmF	doručit
500	[number]	k4	500
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
než	než	k8xS	než
prosté	prostý	k2eAgNnSc4d1	prosté
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
buňky	buňka	k1gFnPc4	buňka
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
krátký	krátký	k2eAgInSc4d1	krátký
pohled	pohled	k1gInSc4	pohled
do	do	k7c2	do
poledního	polední	k2eAgNnSc2d1	polední
Slunce	slunce	k1gNnSc2	slunce
přes	přes	k7c4	přes
nefiltrovaný	filtrovaný	k2eNgInSc4d1	nefiltrovaný
dalekohled	dalekohled	k1gInSc4	dalekohled
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
trvalou	trvalý	k2eAgFnSc4d1	trvalá
slepotu	slepota	k1gFnSc4	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pozorovat	pozorovat	k5eAaImF	pozorovat
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
promítnutí	promítnutí	k1gNnSc4	promítnutí
jeho	on	k3xPp3gInSc2	on
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
či	či	k8xC	či
papír	papír	k1gInSc4	papír
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
nebo	nebo	k8xC	nebo
malého	malý	k2eAgInSc2d1	malý
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
byl	být	k5eAaImAgMnS	být
vybaven	vybavit	k5eAaPmNgMnS	vybavit
speciálními	speciální	k2eAgFnPc7d1	speciální
ochrannými	ochranný	k2eAgFnPc7d1	ochranná
pomůckami	pomůcka	k1gFnPc7	pomůcka
i	i	k8xC	i
během	běh	k1gInSc7	běh
pozorování	pozorování	k1gNnSc2	pozorování
slunečního	sluneční	k2eAgNnSc2d1	sluneční
zatmění	zatmění	k1gNnSc2	zatmění
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k9	jak
celkového	celkový	k2eAgInSc2d1	celkový
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
částečného	částečný	k2eAgInSc2d1	částečný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
ochrana	ochrana	k1gFnSc1	ochrana
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
používat	používat	k5eAaImF	používat
speciální	speciální	k2eAgFnPc4d1	speciální
brýle	brýle	k1gFnPc4	brýle
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
satelit	satelit	k1gInSc4	satelit
navržený	navržený	k2eAgInSc4d1	navržený
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
Slunce	slunce	k1gNnSc2	slunce
byly	být	k5eAaImAgFnP	být
americké	americký	k2eAgFnPc1d1	americká
sondy	sonda	k1gFnPc1	sonda
Pioneer	Pioneer	kA	Pioneer
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
9	[number]	k4	9
vypuštěné	vypuštěný	k2eAgFnSc2d1	vypuštěná
během	během	k7c2	během
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1959	[number]	k4	1959
až	až	k6eAd1	až
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
sond	sonda	k1gFnPc2	sonda
nebylo	být	k5eNaImAgNnS	být
přiblížit	přiblížit	k5eAaPmF	přiblížit
se	se	k3xPyFc4	se
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
provádět	provádět	k5eAaImF	provádět
pozorování	pozorování	k1gNnPc4	pozorování
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
přibližné	přibližný	k2eAgFnSc3d1	přibližná
oběžné	oběžný	k2eAgFnSc3d1	oběžná
dráze	dráha	k1gFnSc3	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
sondy	sonda	k1gFnSc2	sonda
poprvé	poprvé	k6eAd1	poprvé
podrobně	podrobně	k6eAd1	podrobně
měřily	měřit	k5eAaImAgFnP	měřit
sluneční	sluneční	k2eAgInSc4d1	sluneční
vítr	vítr	k1gInSc4	vítr
a	a	k8xC	a
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vyslána	vyslán	k2eAgFnSc1d1	vyslána
sonda	sonda	k1gFnSc1	sonda
Helios	Helios	k1gInSc1	Helios
1	[number]	k4	1
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Apollo	Apollo	k1gMnSc1	Apollo
Telescope	Telescop	k1gInSc5	Telescop
Mount	Mounto	k1gNnPc2	Mounto
byly	být	k5eAaImAgInP	být
prováděny	prováděn	k2eAgFnPc4d1	prováděna
nová	nový	k2eAgNnPc4d1	nové
pozorování	pozorování	k1gNnPc4	pozorování
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
sluneční	sluneční	k2eAgFnSc2d1	sluneční
korony	korona	k1gFnSc2	korona
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Helios	Helios	k1gInSc1	Helios
1	[number]	k4	1
byla	být	k5eAaImAgFnS	být
společným	společný	k2eAgInSc7d1	společný
americko-německým	americkoěmecký	k2eAgInSc7d1	americko-německý
projektem	projekt	k1gInSc7	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
studovat	studovat	k5eAaImF	studovat
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
z	z	k7c2	z
orbity	orbita	k1gFnSc2	orbita
uvnitř	uvnitř	k7c2	uvnitř
dráhy	dráha	k1gFnSc2	dráha
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Solar	Solar	k1gInSc1	Solar
Maximum	maximum	k1gNnSc1	maximum
Mission	Mission	k1gInSc1	Mission
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
gamma	gammum	k1gNnSc2	gammum
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
měření	měření	k1gNnSc2	měření
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
ze	z	k7c2	z
slunečních	sluneční	k2eAgFnPc2d1	sluneční
erupcí	erupce	k1gFnPc2	erupce
během	během	k7c2	během
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
sluneční	sluneční	k2eAgFnSc2d1	sluneční
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
startu	start	k1gInSc6	start
vlivem	vlivem	k7c2	vlivem
elektronického	elektronický	k2eAgNnSc2d1	elektronické
selhání	selhání	k1gNnSc2	selhání
přestala	přestat	k5eAaPmAgFnS	přestat
fungovat	fungovat	k5eAaImF	fungovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přepnutí	přepnutí	k1gNnSc3	přepnutí
sondy	sonda	k1gFnSc2	sonda
do	do	k7c2	do
záložního	záložní	k2eAgInSc2d1	záložní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
setrvala	setrvat	k5eAaPmAgFnS	setrvat
3	[number]	k4	3
roky	rok	k1gInPc4	rok
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
opravena	opravit	k5eAaPmNgFnS	opravit
během	během	k7c2	během
mise	mise	k1gFnSc2	mise
STS-	STS-	k1gFnSc2	STS-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
C.	C.	kA	C.
Sonda	sonda	k1gFnSc1	sonda
následně	následně	k6eAd1	následně
zaslala	zaslat	k5eAaPmAgFnS	zaslat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tisíce	tisíc	k4xCgInSc2	tisíc
snímků	snímek	k1gInPc2	snímek
sluneční	sluneční	k2eAgFnSc2d1	sluneční
korony	korona	k1gFnSc2	korona
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1989	[number]	k4	1989
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
sondy	sonda	k1gFnPc4	sonda
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
:	:	kIx,	:
TRACE	TRACE	kA	TRACE
SOHO	SOHO	kA	SOHO
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
Genesis	Genesis	k1gFnSc1	Genesis
dvojice	dvojice	k1gFnSc2	dvojice
sond	sonda	k1gFnPc2	sonda
STEREO	stereo	k6eAd1	stereo
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sluneční	sluneční	k2eAgFnPc4d1	sluneční
božstva	božstvo	k1gNnPc4	božstvo
<g/>
.	.	kIx.	.
</s>
