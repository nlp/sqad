<s>
Jádro	jádro	k1gNnSc1
Slunce	slunce	k1gNnSc2
</s>
<s>
Schematické	schematický	k2eAgNnSc1d1
znázornění	znázornění	k1gNnSc1
vnitřní	vnitřní	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
-	-	kIx~
jádro	jádro	k1gNnSc1
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
vrstva	vrstva	k1gFnSc1
v	v	k7c6
zářivé	zářivý	k2eAgFnSc6d1
rovnováze	rovnováha	k1gFnSc6
<g/>
,	,	kIx,
3	#num#	k4
-	-	kIx~
konvektivní	konvektivní	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
-	-	kIx~
fotosféra	fotosféra	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
-	-	kIx~
chromosféra	chromosféra	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
-	-	kIx~
koróna	koróna	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
-	-	kIx~
sluneční	sluneční	k2eAgFnSc1d1
skvrna	skvrna	k1gFnSc1
<g/>
,	,	kIx,
8	#num#	k4
-	-	kIx~
granulace	granulace	k1gFnSc2
<g/>
,	,	kIx,
9	#num#	k4
-	-	kIx~
protuberace	protuberace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jádro	jádro	k1gNnSc1
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
část	část	k1gFnSc1
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6
probíhají	probíhat	k5eAaImIp3nP
termonukleární	termonukleární	k2eAgFnPc1d1
reakce	reakce	k1gFnPc1
a	a	k8xC
vzniká	vznikat	k5eAaImIp3nS
zde	zde	k6eAd1
všechna	všechen	k3xTgFnSc1
energie	energie	k1gFnSc1
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poloměr	poloměr	k1gInSc1
slunečního	sluneční	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
175	#num#	k4
tisíc	tisíc	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplota	teplota	k1gFnSc1
v	v	k7c6
jádru	jádro	k1gNnSc6
dosahuje	dosahovat	k5eAaImIp3nS
1,5	1,5	k4
<g/>
×	×	k?
<g/>
107	#num#	k4
K	k	k7c3
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
na	na	k7c6
okraji	okraj	k1gInSc6
asi	asi	k9
7	#num#	k4
000	#num#	k4
000	#num#	k4
K.	K.	kA
Vnitřní	vnitřní	k2eAgInSc1d1
tlak	tlak	k1gInSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
2,5	2,5	k4
<g/>
×	×	k?
<g/>
1016	#num#	k4
Pa	Pa	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hustota	hustota	k1gFnSc1
vnější	vnější	k2eAgFnSc2d1
části	část	k1gFnSc2
jádra	jádro	k1gNnSc2
asi	asi	k9
20	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vnitřní	vnitřní	k2eAgFnPc1d1
části	část	k1gFnPc1
až	až	k9
130	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvořeno	tvořit	k5eAaImNgNnS
je	být	k5eAaImIp3nS
hlavně	hlavně	k9
volnými	volný	k2eAgNnPc7d1
jádry	jádro	k1gNnPc7
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
hélia	hélium	k1gNnSc2
a	a	k8xC
volnými	volný	k2eAgInPc7d1
elektrony	elektron	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
teorií	teorie	k1gFnPc2
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
doba	doba	k1gFnSc1
rotace	rotace	k1gFnSc1
slunečního	sluneční	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
je	být	k5eAaImIp3nS
11	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
přesně	přesně	k6eAd1
známo	znám	k2eAgNnSc1d1
jakou	jaký	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
rychlostí	rychlost	k1gFnSc7
jádro	jádro	k1gNnSc1
rotuje	rotovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ani	ani	k8xC
zda	zda	k8xS
má	mít	k5eAaImIp3nS
Slunce	slunce	k1gNnSc1
diferenciální	diferenciální	k2eAgFnSc4d1
rotaci	rotace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jádru	jádro	k1gNnSc6
Slunce	slunce	k1gNnSc2
dochází	docházet	k5eAaImIp3nS
během	během	k7c2
proton-protonového	proton-protonový	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
k	k	k7c3
fúzi	fúze	k1gFnSc3
jader	jádro	k1gNnPc2
lehkého	lehký	k2eAgInSc2d1
vodíku	vodík	k1gInSc2
na	na	k7c4
hélium	hélium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
čtyř	čtyři	k4xCgNnPc2
jader	jádro	k1gNnPc2
vodíku	vodík	k1gInSc2
vzniká	vznikat	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
jádro	jádro	k1gNnSc1
hélia	hélium	k1gNnSc2
<g/>
.	.	kIx.
0,7	0,7	k4
%	%	kIx~
původních	původní	k2eAgInPc2d1
protonů	proton	k1gInPc2
se	se	k3xPyFc4
přemění	přeměnit	k5eAaPmIp3nS
na	na	k7c4
energii	energie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každou	každý	k3xTgFnSc4
sekundu	sekunda	k1gFnSc4
se	se	k3xPyFc4
tak	tak	k9
na	na	k7c4
energii	energie	k1gFnSc4
přemění	přeměnit	k5eAaPmIp3nS
5	#num#	k4
milionů	milion	k4xCgInPc2
tun	tuna	k1gFnPc2
hmoty	hmota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlak	tlak	k1gInSc1
takto	takto	k6eAd1
vznikajícího	vznikající	k2eAgNnSc2d1
záření	záření	k1gNnSc2
působí	působit	k5eAaImIp3nS
proti	proti	k7c3
tlaku	tlak	k1gInSc3
vznikajícího	vznikající	k2eAgInSc2d1
vlivem	vlivem	k7c2
velké	velký	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
vnějších	vnější	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
Slunce	slunce	k1gNnSc2
a	a	k8xC
udržují	udržovat	k5eAaImIp3nP
tak	tak	k6eAd1
sluneční	sluneční	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
v	v	k7c6
hydrostatické	hydrostatický	k2eAgFnSc6d1
rovnováze	rovnováha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Energie	energie	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
ve	v	k7c6
formě	forma	k1gFnSc6
fotonů	foton	k1gInPc2
gama	gama	k1gNnSc4
záření	záření	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c4
povrch	povrch	k1gInSc4
Slunce	slunce	k1gNnSc2
dostává	dostávat	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
konvekce	konvekce	k1gFnSc2
<g/>
,	,	kIx,
absorpce	absorpce	k1gFnSc2
a	a	k8xC
emise	emise	k1gFnSc2
<g/>
,	,	kIx,
opouští	opouštět	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
elektromagnetické	elektromagnetický	k2eAgFnSc2d1
radiace	radiace	k1gFnSc2
a	a	k8xC
neutrin	neutrino	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KLEZCEK	KLEZCEK	kA
<g/>
,	,	kIx,
Josip	Josip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
906	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
455	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
