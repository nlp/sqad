<s>
Selen	selen	k1gInSc1	selen
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Selenium	Selenium	k1gNnSc1	Selenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polokov	polokov	k1gInSc4	polokov
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc4d1	významný
svými	svůj	k3xOyFgFnPc7	svůj
fotoelektrickými	fotoelektrický	k2eAgFnPc7d1	fotoelektrická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
