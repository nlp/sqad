<s>
První	první	k4xOgInSc1
kostel	kostel	k1gInSc1
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
dal	dát	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
císař	císař	k1gMnSc1
Theodosius	Theodosius	k1gInSc4
Veliký	veliký	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
379	#num#	k4
<g/>
–	–	k?
<g/>
395	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
614	#num#	k4
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
armádou	armáda	k1gFnSc7
perských	perský	k2eAgMnPc2d1
Sásánovců	Sásánovec	k1gMnPc2
a	a	k8xC
následným	následný	k2eAgNnSc7d1
zemětřesením	zemětřesení	k1gNnSc7
roku	rok	k1gInSc2
750	#num#	k4
<g/>
.	.	kIx.
</s>