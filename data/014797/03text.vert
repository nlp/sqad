<s>
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
</s>
<s>
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
Místo	místo	k7c2
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
čtvrť	čtvrť	k1gFnSc1
At-Tur	At-Tura	k1gFnPc2
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Getsemanská	getsemanský	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
45,22	45,22	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
22,66	22,66	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Církev	církev	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
Diecéze	diecéze	k1gFnSc2
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
patriarchát	patriarchát	k1gInSc1
jeruzalémský	jeruzalémský	k2eAgInSc1d1
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Architekt	architekt	k1gMnSc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Barluzzi	Barluzh	k1gMnPc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
(	(	kIx(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1
také	také	k9
bazilika	bazilika	k1gFnSc1
Agónie	agónie	k1gFnSc2
Páně	páně	k2eAgFnSc2d1
<g/>
,	,	kIx,
Basilica	Basilica	k1gMnSc1
Agoniæ	Agoniæ	k1gMnSc1
Domini	Domin	k2eAgMnPc1d1
<g/>
)	)	kIx)
v	v	k7c6
Getsemanské	getsemanský	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
leží	ležet	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
úpatí	úpatí	k1gNnSc6
Olivové	Olivové	k2eAgFnSc2d1
hory	hora	k1gFnSc2
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Cedron	Cedron	k1gInSc4
a	a	k8xC
náleží	náležet	k5eAaImIp3nS
františkánské	františkánský	k2eAgFnSc3d1
Kustodii	Kustodie	k1gFnSc3
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Interiér	interiér	k1gInSc1
chrámu	chrám	k1gInSc2
</s>
<s>
První	první	k4xOgInSc1
kostel	kostel	k1gInSc1
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
dal	dát	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
císař	císař	k1gMnSc1
Theodosius	Theodosius	k1gInSc4
Veliký	veliký	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
379	#num#	k4
<g/>
–	–	k?
<g/>
395	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
614	#num#	k4
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
armádou	armáda	k1gFnSc7
perských	perský	k2eAgMnPc2d1
Sásánovců	Sásánovec	k1gMnPc2
a	a	k8xC
následným	následný	k2eAgNnSc7d1
zemětřesením	zemětřesení	k1gNnSc7
roku	rok	k1gInSc2
750	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
křižáckého	křižácký	k2eAgInSc2d1
státu	stát	k1gInSc2
byl	být	k5eAaImAgInS
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
troskách	troska	k1gFnPc6
postaven	postaven	k2eAgInSc1d1
kostel	kostel	k1gInSc1
nazvaný	nazvaný	k2eAgInSc1d1
Bazilika	bazilika	k1gFnSc1
Agónie	agónie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zanikl	zaniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1345	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františkáni	františkán	k1gMnPc1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
již	již	k6eAd1
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
získat	získat	k5eAaPmF
tato	tento	k3xDgNnPc1
místa	místo	k1gNnPc1
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc1
až	až	k6eAd1
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
se	s	k7c7
stavbou	stavba	k1gFnSc7
kostela	kostel	k1gInSc2
mohli	moct	k5eAaImAgMnP
začít	začít	k5eAaPmF
až	až	k9
po	po	k7c6
provedení	provedení	k1gNnSc6
vykopávek	vykopávka	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
nalezli	nalézt	k5eAaBmAgMnP,k5eAaPmAgMnP
zbytky	zbytek	k1gInPc4
předchozích	předchozí	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
stavbu	stavba	k1gFnSc4
kostela	kostel	k1gInSc2
podle	podle	k7c2
projektu	projekt	k1gInSc2
italského	italský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Antonia	Antonio	k1gMnSc2
Barluzziho	Barluzzi	k1gMnSc2
se	se	k3xPyFc4
vybíraly	vybírat	k5eAaImAgInP
finanční	finanční	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
zván	zvát	k5eAaImNgInS
také	také	k9
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MAJERNÍK	MAJERNÍK	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
;	;	kIx,
SCHEIDOVÁ	SCHEIDOVÁ	kA
<g/>
,	,	kIx,
Leopoldína	Leopoldína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spoznávajme	Spoznávajme	k1gFnSc1
Svätú	Svätú	k1gFnSc1
zem	zem	k1gFnSc1
pohľadom	pohľadom	k1gInSc4
archológov	archológov	k1gInSc1
a	a	k8xC
biblistov	biblistov	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Rímskokatolícky	Rímskokatolícka	k1gFnSc2
farský	farský	k2eAgMnSc1d1
úrad	úrada	k1gFnPc2
v	v	k7c4
Zákamennom	Zákamennom	k1gInSc4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
801	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
968861	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bazilika	bazilika	k1gFnSc1
Agónie	agónie	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
197	#num#	k4
<g/>
-	-	kIx~
<g/>
199	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KROLL	KROLL	kA
<g/>
,	,	kIx,
Gerhard	Gerhard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stopách	stopa	k1gFnPc6
Ježíšových	Ježíšův	k2eAgMnPc2d1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgInSc1d1
Vydří	vydří	k2eAgInSc1d1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
486	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7192	#num#	k4
<g/>
-	-	kIx~
<g/>
711	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
59	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatčení	zatčení	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
320	#num#	k4
<g/>
-	-	kIx~
<g/>
326	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnSc2d1
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
Virtual	Virtual	k1gMnSc1
Tour	Tour	k1gMnSc1
of	of	k?
inside	insid	k1gInSc5
Church	Church	k1gMnSc1
of	of	k?
All	All	k1gFnSc2
Nations	Nationsa	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Izrael	Izrael	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1079636730	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
200145067430266631085	#num#	k4
</s>
