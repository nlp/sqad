<s>
Kostel	kostel	k1gInSc1
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
(	(	kIx(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1
také	také	k9
bazilika	bazilika	k1gFnSc1
Agónie	agónie	k1gFnSc2
Páně	páně	k2eAgFnSc2d1
<g/>
,	,	kIx,
Basilica	Basilica	k1gMnSc1
Agoniæ	Agoniæ	k1gMnSc1
Domini	Domin	k2eAgMnPc1d1
<g/>
)	)	kIx)
v	v	k7c6
Getsemanské	getsemanský	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
leží	ležet	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
úpatí	úpatí	k1gNnSc6
Olivové	Olivové	k2eAgFnSc2d1
hory	hora	k1gFnSc2
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Cedron	Cedron	k1gInSc4
a	a	k8xC
náleží	náležet	k5eAaImIp3nS
františkánské	františkánský	k2eAgFnSc3d1
Kustodii	Kustodie	k1gFnSc3
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>