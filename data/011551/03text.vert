<p>
<s>
Radisson	Radissona	k1gFnPc2	Radissona
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
27	[number]	k4	27
stanic	stanice	k1gFnPc2	stanice
zelené	zelený	k2eAgFnSc2d1	zelená
linky	linka	k1gFnSc2	linka
montrealského	montrealský	k2eAgNnSc2d1	montrealské
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
Angrignon	Angrignon	k1gInSc1	Angrignon
–	–	k?	–
Honoré-Beaugrand	Honoré-Beaugrand	k1gInSc1	Honoré-Beaugrand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
22,1	[number]	k4	22,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dvacátá	dvacátý	k4xOgNnPc4	dvacátý
šestá	šestý	k4xOgFnSc1	šestý
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
17,4	[number]	k4	17,4
m.	m.	k?	m.
Její	její	k3xOp3gFnSc4	její
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
stanice	stanice	k1gFnSc2	stanice
Langelier	Langelira	k1gFnPc2	Langelira
je	být	k5eAaImIp3nS	být
621,79	[number]	k4	621,79
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
od	od	k7c2	od
následující	následující	k2eAgFnSc2d1	následující
Honoré-Beaugrand	Honoré-Beaugranda	k1gFnPc2	Honoré-Beaugranda
716,99	[number]	k4	716,99
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
Radisson	Radissona	k1gFnPc2	Radissona
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
ji	on	k3xPp3gFnSc4	on
architekti	architekt	k1gMnPc1	architekt
Papineau	Papineaus	k1gInSc2	Papineaus
<g/>
,	,	kIx,	,
Gérin-Lajoie	Gérin-Lajoie	k1gFnSc2	Gérin-Lajoie
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Blanc	Blanc	k1gMnSc1	Blanc
a	a	k8xC	a
Edwards	Edwards	k1gInSc1	Edwards
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historie	historie	k1gFnSc2	historie
stavby	stavba	k1gFnSc2	stavba
linky	linka	k1gFnSc2	linka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
druhé	druhý	k4xOgNnSc4	druhý
nejmladší	mladý	k2eAgNnSc4d3	nejmladší
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejsevernější	severní	k2eAgFnPc1d3	nejsevernější
<g/>
)	)	kIx)	)
části	část	k1gFnPc1	část
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgFnPc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
stanic	stanice	k1gFnPc2	stanice
mezi	mezi	k7c7	mezi
Préfontaine	Préfontain	k1gInSc5	Préfontain
a	a	k8xC	a
Honoré-Beaugrand	Honoré-Beaugrando	k1gNnPc2	Honoré-Beaugrando
<g/>
.	.	kIx.	.
<g/>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Atwater	Atwatra	k1gFnPc2	Atwatra
až	až	k9	až
po	po	k7c4	po
Frontenac	Frontenac	k1gFnSc4	Frontenac
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
zelené	zelený	k2eAgFnSc2d1	zelená
linky	linka	k1gFnSc2	linka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
úseku	úsek	k1gInSc6	úsek
Angrignon	Angrignon	k1gInSc1	Angrignon
až	až	k8xS	až
Lionel-Groulx	Lionel-Groulx	k1gInSc1	Lionel-Groulx
tvoří	tvořit	k5eAaImIp3nS	tvořit
služebně	služebně	k6eAd1	služebně
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
část	část	k1gFnSc1	část
zelené	zelený	k2eAgFnSc2d1	zelená
linky	linka	k1gFnSc2	linka
montrealského	montrealský	k2eAgNnSc2d1	montrealské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radisson	Radissona	k1gFnPc2	Radissona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
montrealského	montrealský	k2eAgNnSc2d1	montrealské
metra	metro	k1gNnSc2	metro
</s>
</p>
