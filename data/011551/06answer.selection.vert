<s>
Stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
úseku	úsek	k1gInSc6	úsek
Angrignon	Angrignon	k1gInSc1	Angrignon
až	až	k8xS	až
Lionel-Groulx	Lionel-Groulx	k1gInSc1	Lionel-Groulx
tvoří	tvořit	k5eAaImIp3nS	tvořit
služebně	služebně	k6eAd1	služebně
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
část	část	k1gFnSc1	část
zelené	zelený	k2eAgFnSc2d1	zelená
linky	linka	k1gFnSc2	linka
montrealského	montrealský	k2eAgNnSc2d1	montrealské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
