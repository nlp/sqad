<s>
Semiokluzivy	semiokluziva	k1gFnPc1	semiokluziva
–	–	k?	–
polozávěrové	polozávěrový	k2eAgFnPc1d1	polozávěrový
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
prvotní	prvotní	k2eAgFnSc7d1	prvotní
krátkou	krátký	k2eAgFnSc7d1	krátká
okluzí	okluze	k1gFnSc7	okluze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vzápětí	vzápětí	k6eAd1	vzápětí
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
následována	následovat	k5eAaImNgFnS	následovat
konstrikcí	konstrikce	k1gFnSc7	konstrikce
<g/>
.	.	kIx.	.
</s>
