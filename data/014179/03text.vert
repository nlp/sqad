<s>
VOX	VOX	kA
(	(	kIx(
<g/>
fotografická	fotografický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Fotografická	fotografický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
VOX	VOX	kA
působila	působit	k5eAaImAgFnS
v	v	k7c6
Brně	Brno	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
členy	člen	k1gInPc7
byli	být	k5eAaImAgMnP
fotografové	fotograf	k1gMnPc1
Jan	Jan	k1gMnSc1
Beran	Beran	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Budík	budík	k1gInSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Hinšt	Hinšt	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Otto	Otto	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Skoupil	skoupit	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
Soňa	Soňa	k1gFnSc1
Skoupilová	Skoupilová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Tichý	Tichý	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
scházeli	scházet	k5eAaImAgMnP
ve	v	k7c6
fotostředisku	fotostředisek	k1gInSc6
při	při	k7c6
Krajském	krajský	k2eAgInSc6d1
domu	dům	k1gInSc6wR
osvěty	osvěta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
úspěšně	úspěšně	k6eAd1
obesílali	obesílat	k5eAaImAgMnP
mezinárodní	mezinárodní	k2eAgFnPc4d1
výstavy	výstava	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
International	International	k1gFnSc1
Photographic	Photographic	k1gMnSc1
Salon	salon	k1gInSc1
of	of	k?
Japan	japan	k1gInSc1
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
vznikem	vznik	k1gInSc7
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autorem	autor	k1gMnSc7
názvu	název	k1gInSc2
VOX	VOX	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
Otto	Otto	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
neměla	mít	k5eNaImAgFnS
žádný	žádný	k3yNgInSc4
pevný	pevný	k2eAgInSc4d1
program	program	k1gInSc4
nebo	nebo	k8xC
výtvarné	výtvarný	k2eAgNnSc4d1
směrování	směrování	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pořádala	pořádat	k5eAaImAgFnS
společné	společný	k2eAgInPc4d1
výstavy	výstav	k1gInPc4
zaměřené	zaměřený	k2eAgInPc4d1
na	na	k7c4
určité	určitý	k2eAgNnSc4d1
téma	téma	k1gNnSc4
(	(	kIx(
<g/>
např.	např.	kA
Variace	variace	k1gFnSc1
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
absurdit	absurdita	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
sedmileté	sedmiletý	k2eAgFnSc2d1
existence	existence	k1gFnSc2
uspářadala	uspářadat	k5eAaImAgFnS,k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
VOX	VOX	kA
devět	devět	k4xCc4
domácích	domácí	k2eAgFnPc2d1
výstav	výstava	k1gFnPc2
a	a	k8xC
jednu	jeden	k4xCgFnSc4
zahraniční	zahraniční	k2eAgFnSc4d1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografická	fotografický	k2eAgFnSc1d1
skupima	skupima	k1gFnSc1
VOX	VOX	kA
zanikla	zaniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BÁRTL	Bártl	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VOX	VOX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magisterská	magisterský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BÁRTL	Bártl	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
VOX	VOX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
/	/	kIx~
Art	Art	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
469	#num#	k4
<g/>
–	–	k?
<g/>
476	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
5123	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BÁRTL	Bártl	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poezie	poezie	k1gFnSc1
všedního	všední	k2eAgInSc2d1
dne	den	k1gInSc2
ve	v	k7c6
fotografii	fotografia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disertační	disertační	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
