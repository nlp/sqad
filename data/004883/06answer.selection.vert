<s>
Současné	současný	k2eAgNnSc4d1	současné
elektrické	elektrický	k2eAgNnSc4d1	elektrické
křeslo	křeslo	k1gNnSc4	křeslo
nicméně	nicméně	k8xC	nicméně
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
zubař	zubař	k1gMnSc1	zubař
Alfred	Alfred	k1gMnSc1	Alfred
P.	P.	kA	P.
Southwick	Southwick	k1gMnSc1	Southwick
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
popravovat	popravovat	k5eAaImF	popravovat
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
.	.	kIx.	.
</s>
