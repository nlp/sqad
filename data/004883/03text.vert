<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
křeslo	křeslo	k1gNnSc1	křeslo
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
pomocí	pomocí	k7c2	pomocí
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
jen	jen	k9	jen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
hledán	hledán	k2eAgInSc1d1	hledán
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
humánnější	humánní	k2eAgMnSc1d2	humánnější
než	než	k8xS	než
oběšení	oběšení	k1gNnSc1	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
elektrického	elektrický	k2eAgNnSc2d1	elektrické
křesla	křeslo	k1gNnSc2	křeslo
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
bojem	boj	k1gInSc7	boj
Thomase	Thomas	k1gMnSc2	Thomas
Alva	Alvus	k1gMnSc2	Alvus
Edisona	Edison	k1gMnSc2	Edison
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Edison	Edison	k1gMnSc1	Edison
chtěl	chtít	k5eAaImAgMnS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
od	od	k7c2	od
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc4d1	současné
elektrické	elektrický	k2eAgNnSc4d1	elektrické
křeslo	křeslo	k1gNnSc4	křeslo
nicméně	nicméně	k8xC	nicméně
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
zubař	zubař	k1gMnSc1	zubař
Alfred	Alfred	k1gMnSc1	Alfred
P.	P.	kA	P.
Southwick	Southwick	k1gMnSc1	Southwick
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
popravovat	popravovat	k5eAaImF	popravovat
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
elektrické	elektrický	k2eAgNnSc1d1	elektrické
křeslo	křeslo	k1gNnSc1	křeslo
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
popravený	popravený	k2eAgInSc1d1	popravený
byl	být	k5eAaImAgInS	být
William	William	k1gInSc1	William
Kemmler	Kemmler	k1gInSc1	Kemmler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
známí	známý	k2eAgMnPc1d1	známý
vězni	vězeň	k1gMnPc1	vězeň
popravení	popravení	k1gNnSc1	popravení
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
křeslem	křeslo	k1gNnSc7	křeslo
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Ted	Ted	k1gMnSc1	Ted
Bundy	bunda	k1gFnSc2	bunda
<g/>
,	,	kIx,	,
Leon	Leona	k1gFnPc2	Leona
Czolgosz	Czolgosza	k1gFnPc2	Czolgosza
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Fish	Fish	k1gMnSc1	Fish
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Starkweather	Starkweathra	k1gFnPc2	Starkweathra
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Joubert	Joubert	k1gMnSc1	Joubert
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Rosenberg	Rosenberg	k1gMnSc1	Rosenberg
či	či	k8xC	či
Donald	Donald	k1gMnSc1	Donald
Henry	Henry	k1gMnSc1	Henry
Gaskins	Gaskins	k1gInSc4	Gaskins
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
být	být	k5eAaImF	být
popraven	popravit	k5eAaPmNgInS	popravit
i	i	k9	i
Oscar	Oscar	k1gInSc1	Oscar
Collazo	Collaza	k1gFnSc5	Collaza
<g/>
,	,	kIx,	,
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
atentátník	atentátník	k1gMnSc1	atentátník
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Trumana	Truman	k1gMnSc4	Truman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
USA	USA	kA	USA
elektrické	elektrický	k2eAgNnSc1d1	elektrické
křeslo	křeslo	k1gNnSc1	křeslo
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
smrtící	smrtící	k2eAgFnSc3d1	smrtící
injekci	injekce	k1gFnSc3	injekce
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
a	a	k8xC	a
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jediným	jediný	k2eAgInSc7d1	jediný
státem	stát	k1gInSc7	stát
jiným	jiný	k2eAgInSc7d1	jiný
než	než	k8xS	než
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
využíván	využívat	k5eAaPmNgInS	využívat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Filipíny	Filipíny	k1gFnPc1	Filipíny
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
1924	[number]	k4	1924
za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
je	být	k5eAaImIp3nS	být
připoután	připoután	k2eAgMnSc1d1	připoután
ke	k	k7c3	k
dřevěné	dřevěný	k2eAgFnSc3d1	dřevěná
židli	židle	k1gFnSc3	židle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
mu	on	k3xPp3gMnSc3	on
jsou	být	k5eAaImIp3nP	být
přiloženy	přiložen	k2eAgFnPc4d1	přiložena
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navlhčeny	navlhčen	k2eAgMnPc4d1	navlhčen
slaným	slaný	k2eAgInSc7d1	slaný
roztokem	roztok	k1gInSc7	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnSc2	místo
dotyku	dotyk	k1gInSc2	dotyk
s	s	k7c7	s
elektrodami	elektroda	k1gFnPc7	elektroda
jsou	být	k5eAaImIp3nP	být
oholena	oholen	k2eAgFnSc1d1	oholena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Popravčí	popravčí	k2eAgInSc1d1	popravčí
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
může	moct	k5eAaImIp3nS	moct
popravu	poprava	k1gFnSc4	poprava
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Dozorce	dozorce	k1gMnSc1	dozorce
dá	dát	k5eAaPmIp3nS	dát
znamení	znamení	k1gNnSc4	znamení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zapojen	zapojen	k2eAgInSc1d1	zapojen
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
500	[number]	k4	500
-	-	kIx~	-
2300	[number]	k4	2300
voltů	volt	k1gInPc2	volt
(	(	kIx(	(
<g/>
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státě	stát	k1gInSc6	stát
jinak	jinak	k6eAd1	jinak
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
asi	asi	k9	asi
třiceti	třicet	k4xCc6	třicet
sekundách	sekunda	k1gFnPc6	sekunda
je	být	k5eAaImIp3nS	být
zdroj	zdroj	k1gInSc1	zdroj
vypojen	vypojit	k5eAaPmNgInS	vypojit
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
čeká	čekat	k5eAaImIp3nS	čekat
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
ochladí	ochladit	k5eAaPmIp3nS	ochladit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zkontroluje	zkontrolovat	k5eAaPmIp3nS	zkontrolovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
vězňovo	vězňův	k2eAgNnSc1d1	vězňovo
srdce	srdce	k1gNnSc1	srdce
stále	stále	k6eAd1	stále
bije	bít	k5eAaImIp3nS	bít
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
připojen	připojit	k5eAaPmNgInS	připojit
zdroj	zdroj	k1gInSc1	zdroj
a	a	k8xC	a
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státě	stát	k1gInSc6	stát
jiný	jiný	k1gMnSc1	jiný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
automatický	automatický	k2eAgInSc1d1	automatický
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
napětí	napětí	k1gNnSc1	napětí
2300	[number]	k4	2300
V	V	kA	V
<g/>
,	,	kIx,	,
proud	proud	k1gInSc1	proud
9,5	[number]	k4	9,5
A	A	kA	A
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
8	[number]	k4	8
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
<g/>
:	:	kIx,	:
1000	[number]	k4	1000
V	V	kA	V
<g/>
,	,	kIx,	,
4	[number]	k4	4
A	A	kA	A
<g/>
,	,	kIx,	,
22	[number]	k4	22
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc2	třetí
<g/>
:	:	kIx,	:
2300	[number]	k4	2300
V	V	kA	V
<g/>
,	,	kIx,	,
9,5	[number]	k4	9,5
A	A	kA	A
<g/>
,	,	kIx,	,
8	[number]	k4	8
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgFnPc1d1	skutečná
hodnoty	hodnota	k1gFnPc1	hodnota
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
proudů	proud	k1gInPc2	proud
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
při	při	k7c6	při
popravě	poprava	k1gFnSc6	poprava
jiné	jiný	k2eAgFnSc6d1	jiná
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
elektrickém	elektrický	k2eAgInSc6d1	elektrický
odporu	odpor	k1gInSc6	odpor
těla	tělo	k1gNnSc2	tělo
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popravě	poprava	k1gFnSc6	poprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
cítit	cítit	k5eAaImF	cítit
spálená	spálený	k2eAgFnSc1d1	spálená
kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
kouřem	kouř	k1gInSc7	kouř
nebo	nebo	k8xC	nebo
párou	pára	k1gFnSc7	pára
<g/>
,	,	kIx,	,
krvácením	krvácení	k1gNnSc7	krvácení
(	(	kIx(	(
<g/>
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
nebo	nebo	k8xC	nebo
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
urinací	urinace	k1gFnSc7	urinace
nebo	nebo	k8xC	nebo
defekací	defekace	k1gFnSc7	defekace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
vězni	vězeň	k1gMnPc1	vězeň
vypadnou	vypadnout	k5eAaPmIp3nP	vypadnout
oční	oční	k2eAgFnPc4d1	oční
bulvy	bulva	k1gFnPc4	bulva
<g/>
.	.	kIx.	.
</s>
<s>
Vězeň	vězeň	k1gMnSc1	vězeň
otéká	otékat	k5eAaImIp3nS	otékat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
začne	začít	k5eAaPmIp3nS	začít
hořet	hořet	k5eAaImF	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
zástavou	zástava	k1gFnSc7	zástava
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
paralýzou	paralýza	k1gFnSc7	paralýza
respiračního	respirační	k2eAgInSc2d1	respirační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elektrické	elektrický	k2eAgFnSc2d1	elektrická
křeslo	křeslo	k1gNnSc4	křeslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
První	první	k4xOgFnSc1	první
poprava	poprava	k1gFnSc1	poprava
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
v	v	k7c6	v
periodiku	periodikum	k1gNnSc6	periodikum
Budivoj	Budivoj	k1gInSc1	Budivoj
<g/>
.	.	kIx.	.
</s>
<s>
Budivoj	Budivoj	k1gInSc1	Budivoj
<g/>
;	;	kIx,	;
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
politický	politický	k2eAgInSc1d1	politický
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Č.	Č.	kA	Č.
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
,	,	kIx,	,
č.	č.	k?	č.
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
2353	[number]	k4	2353
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://kramerius.nkp.cz/kramerius/handle/ABA001/4925150	[url]	k4	http://kramerius.nkp.cz/kramerius/handle/ABA001/4925150
</s>
