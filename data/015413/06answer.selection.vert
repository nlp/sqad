<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1252	#num#	k4
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
náhrobku	náhrobek	k1gInSc6
v	v	k7c6
sevillské	sevillský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
byly	být	k5eAaImAgInP
nápisy	nápis	k1gInPc1
v	v	k7c6
kastilštině	kastilština	k1gFnSc6
<g/>
,	,	kIx,
latině	latina	k1gFnSc6
<g/>
,	,	kIx,
hebrejštině	hebrejština	k1gFnSc6
a	a	k8xC
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1671	#num#	k4
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
svatého	svatý	k1gMnSc4
a	a	k8xC
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
jej	on	k3xPp3gInSc4
uctívají	uctívat	k5eAaImIp3nP
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Fernando	Fernanda	k1gFnSc5
el	ela	k1gFnPc2
Santo	Santo	k1gNnSc1
nebo	nebo	k8xC
San	San	k1gFnSc1
Fernando	Fernanda	k1gFnSc5
<g/>
.	.	kIx.
</s>