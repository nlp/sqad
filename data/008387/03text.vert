<p>
<s>
Varhany	varhany	k1gFnPc1	varhany
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnPc1d1	označovaná
za	za	k7c4	za
"	"	kIx"	"
<g/>
královský	královský	k2eAgInSc4d1	královský
nástroj	nástroj	k1gInSc4	nástroj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnPc1d3	veliký
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
nejsložitější	složitý	k2eAgInSc1d3	nejsložitější
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jsou	být	k5eAaImIp3nP	být
strojem	stroj	k1gInSc7	stroj
užívaným	užívaný	k2eAgInSc7d1	užívaný
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
(	(	kIx(	(
<g/>
varhanní	varhanní	k2eAgInSc1d1	varhanní
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
vícehlasé	vícehlasý	k2eAgInPc4d1	vícehlasý
aerofony	aerofon	k1gInPc4	aerofon
<g/>
.	.	kIx.	.
</s>
<s>
Tóny	Tóny	k1gMnSc1	Tóny
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
nebo	nebo	k8xC	nebo
kovových	kovový	k2eAgFnPc6d1	kovová
píšťalách	píšťala	k1gFnPc6	píšťala
buď	buď	k8xC	buď
chvěním	chvění	k1gNnSc7	chvění
vzduchových	vzduchový	k2eAgInPc2d1	vzduchový
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chvěním	chvění	k1gNnSc7	chvění
kovových	kovový	k2eAgInPc2d1	kovový
jazýčků	jazýček	k1gInPc2	jazýček
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stlačený	stlačený	k2eAgInSc1d1	stlačený
vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
do	do	k7c2	do
píšťal	píšťala	k1gFnPc2	píšťala
vhání	vhánět	k5eAaImIp3nS	vhánět
obvykle	obvykle	k6eAd1	obvykle
měchem	měch	k1gInSc7	měch
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
sedm	sedm	k4xCc4	sedm
manuálů	manuál	k1gInPc2	manuál
(	(	kIx(	(
<g/>
klaviatur	klaviatura	k1gFnPc2	klaviatura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
pedálnice	pedálnice	k1gFnPc1	pedálnice
(	(	kIx(	(
<g/>
klaviatury	klaviatura	k1gFnPc1	klaviatura
pro	pro	k7c4	pro
nohy	noha	k1gFnPc4	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
desítky	desítka	k1gFnPc4	desítka
rejstříků	rejstřík	k1gInPc2	rejstřík
a	a	k8xC	a
až	až	k9	až
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
píšťal	píšťala	k1gFnPc2	píšťala
velikosti	velikost	k1gFnSc2	velikost
od	od	k7c2	od
11	[number]	k4	11
mm	mm	kA	mm
do	do	k7c2	do
20	[number]	k4	20
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předchůdci	předchůdce	k1gMnPc1	předchůdce
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
byly	být	k5eAaImAgInP	být
Panova	Panův	k2eAgFnSc1d1	Panova
flétna	flétna	k1gFnSc1	flétna
a	a	k8xC	a
dudy	dudy	k1gFnPc1	dudy
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
usilovného	usilovný	k2eAgNnSc2d1	usilovné
zvětšování	zvětšování	k1gNnSc2	zvětšování
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc2	zdokonalování
dala	dát	k5eAaPmAgFnS	dát
vznik	vznik	k1gInSc4	vznik
"	"	kIx"	"
<g/>
královskému	královský	k2eAgMnSc3d1	královský
<g/>
"	"	kIx"	"
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zdobí	zdobit	k5eAaImIp3nS	zdobit
největší	veliký	k2eAgFnPc4d3	veliký
katedrály	katedrála	k1gFnPc4	katedrála
světa	svět	k1gInSc2	svět
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
malý	malý	k2eAgInSc1d1	malý
venkovský	venkovský	k2eAgInSc1d1	venkovský
kostelík	kostelík	k1gInSc1	kostelík
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nadále	nadále	k6eAd1	nadále
budovány	budovat	k5eAaImNgFnP	budovat
i	i	k9	i
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
koncertních	koncertní	k2eAgInPc6d1	koncertní
sálech	sál	k1gInPc6	sál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
službu	služba	k1gFnSc4	služba
umění	umění	k1gNnSc2	umění
vykonaly	vykonat	k5eAaPmAgFnP	vykonat
varhany	varhany	k1gFnPc1	varhany
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hudebního	hudební	k2eAgNnSc2d1	hudební
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
provozovaly	provozovat	k5eAaImAgInP	provozovat
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
hlas	hlas	k1gInSc1	hlas
varhan	varhany	k1gFnPc2	varhany
neztratil	ztratit	k5eNaPmAgInS	ztratit
své	svůj	k3xOyFgNnSc4	svůj
kouzlo	kouzlo	k1gNnSc4	kouzlo
ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
–	–	k?	–
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
vyprodané	vyprodaný	k2eAgNnSc1d1	vyprodané
varhanní	varhanní	k2eAgInPc1d1	varhanní
koncerty	koncert	k1gInPc1	koncert
ať	ať	k8xC	ať
v	v	k7c6	v
koncertních	koncertní	k2eAgFnPc6d1	koncertní
síních	síň	k1gFnPc6	síň
s	s	k7c7	s
moderními	moderní	k2eAgInPc7d1	moderní
elektrifikovanými	elektrifikovaný	k2eAgInPc7d1	elektrifikovaný
nástroji	nástroj	k1gInPc7	nástroj
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
s	s	k7c7	s
historickými	historický	k2eAgInPc7d1	historický
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
takovou	takový	k3xDgFnSc7	takový
historickou	historický	k2eAgFnSc7d1	historická
koncertní	koncertní	k2eAgFnSc7d1	koncertní
síní	síň	k1gFnSc7	síň
např.	např.	kA	např.
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
psali	psát	k5eAaImAgMnP	psát
hudbu	hudba	k1gFnSc4	hudba
skladatelé	skladatel	k1gMnPc1	skladatel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
v	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
v	v	k7c6	v
době	doba	k1gFnSc6	doba
romantismu	romantismus	k1gInSc2	romantismus
či	či	k8xC	či
Petr	Petr	k1gMnSc1	Petr
Eben	eben	k1gInSc1	eben
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hudbě	hudba	k1gFnSc6	hudba
<g/>
)	)	kIx)	)
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
doba	doba	k1gFnSc1	doba
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
své	svůj	k3xOyFgMnPc4	svůj
slavné	slavný	k2eAgMnPc4d1	slavný
varhaníky	varhaník	k1gMnPc4	varhaník
a	a	k8xC	a
stavitele	stavitel	k1gMnPc4	stavitel
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
varhanářství	varhanářství	k1gNnSc1	varhanářství
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
varhanářstvím	varhanářství	k1gNnSc7	varhanářství
německým	německý	k2eAgNnSc7d1	německé
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
občas	občas	k6eAd1	občas
uvádíme	uvádět	k5eAaImIp1nP	uvádět
i	i	k9	i
německé	německý	k2eAgInPc1d1	německý
pojmy	pojem	k1gInPc1	pojem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgMnPc4d1	tradiční
české	český	k2eAgMnPc4d1	český
výrobce	výrobce	k1gMnPc4	výrobce
varhan	varhany	k1gFnPc2	varhany
patří	patřit	k5eAaImIp3nP	patřit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
krnovská	krnovský	k2eAgFnSc1d1	Krnovská
firma	firma	k1gFnSc1	firma
RIEGER-KLOSS	RIEGER-KLOSS	k1gFnSc2	RIEGER-KLOSS
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
RIEGER	RIEGER	kA	RIEGER
-	-	kIx~	-
KLOSS	KLOSS	kA	KLOSS
VARHANY	varhany	k1gInPc1	varhany
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
3732	[number]	k4	3732
varhan	varhany	k1gFnPc2	varhany
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Různorodosti	různorodost	k1gFnPc1	různorodost
barvy	barva	k1gFnSc2	barva
tónů	tón	k1gInPc2	tón
varhan	varhany	k1gFnPc2	varhany
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
akustickým	akustický	k2eAgNnSc7d1	akustické
spoluzněním	spoluznění	k1gNnSc7	spoluznění
alikvotních	alikvotní	k2eAgInPc2d1	alikvotní
tónů	tón	k1gInPc2	tón
vznikajících	vznikající	k2eAgInPc2d1	vznikající
v	v	k7c6	v
několika	několik	k4yIc6	několik
píšťalách	píšťala	k1gFnPc6	píšťala
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
elektrofonické	elektrofonický	k2eAgFnSc3d1	elektrofonická
aditivní	aditivní	k2eAgFnSc3d1	aditivní
syntéze	syntéza	k1gFnSc3	syntéza
používané	používaný	k2eAgFnSc2d1	používaná
u	u	k7c2	u
elektronických	elektronický	k2eAgInPc2d1	elektronický
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc6	část
varhan	varhany	k1gFnPc2	varhany
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stroje	stroj	k1gInPc4	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Varhany	varhany	k1gFnPc1	varhany
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
stroje	stroj	k1gInPc1	stroj
mívají	mívat	k5eAaImIp3nP	mívat
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
bývají	bývat	k5eAaImIp3nP	bývat
umístěny	umístit	k5eAaPmNgFnP	umístit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
mívají	mívat	k5eAaImIp3nP	mívat
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
největší	veliký	k2eAgInSc4d3	veliký
stroj	stroj	k1gInSc4	stroj
</s>
</p>
<p>
<s>
Horní	horní	k2eAgInSc1d1	horní
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
nad	nad	k7c7	nad
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
</s>
</p>
<p>
<s>
Prsní	prsní	k2eAgInSc1d1	prsní
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
před	před	k7c7	před
hráčem	hráč	k1gMnSc7	hráč
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zpravidla	zpravidla	k6eAd1	zpravidla
pod	pod	k7c7	pod
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
umístěn	umístit	k5eAaPmNgInS	umístit
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Positiv	positiv	k1gInSc1	positiv
–	–	k?	–
menší	malý	k2eAgInSc1d2	menší
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
Zadní	zadní	k2eAgInSc1d1	zadní
positiv	positiv	k1gInSc1	positiv
–	–	k?	–
positiv	positiv	k1gInSc1	positiv
umístěný	umístěný	k2eAgInSc1d1	umístěný
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
hráče	hráč	k1gMnSc2	hráč
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
zábradlí	zábradlí	k1gNnSc6	zábradlí
kruchty	kruchta	k1gFnSc2	kruchta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žaluziový	žaluziový	k2eAgInSc1d1	žaluziový
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
<g/>
,	,	kIx,	,
uzavíratelný	uzavíratelný	k2eAgInSc1d1	uzavíratelný
žaluziemi	žaluzie	k1gFnPc7	žaluzie
pro	pro	k7c4	pro
plynulé	plynulý	k2eAgNnSc4d1	plynulé
tlumení	tlumení	k1gNnSc4	tlumení
zvuku	zvuk	k1gInSc2	zvuk
</s>
</p>
<p>
<s>
Pedál	pedál	k1gInSc1	pedál
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
basovými	basový	k2eAgFnPc7d1	basová
píšťalami	píšťala	k1gFnPc7	píšťala
<g/>
,	,	kIx,	,
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
zpravidla	zpravidla	k6eAd1	zpravidla
pedálnicí	pedálnice	k1gFnSc7	pedálnice
(	(	kIx(	(
<g/>
pedálem	pedál	k1gInSc7	pedál
<g/>
)	)	kIx)	)
<g/>
Zejména	zejména	k9	zejména
v	v	k7c6	v
romantické	romantický	k2eAgFnSc6d1	romantická
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
stroje	stroj	k1gInPc1	stroj
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spodní	spodní	k2eAgInSc1d1	spodní
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
zpravidla	zpravidla	k6eAd1	zpravidla
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
hracího	hrací	k2eAgInSc2d1	hrací
stolu	stol	k1gInSc2	stol
</s>
</p>
<p>
<s>
Zadní	zadní	k2eAgInSc1d1	zadní
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
za	za	k7c7	za
hlavním	hlavní	k2eAgInSc7d1	hlavní
strojem	stroj	k1gInSc7	stroj
</s>
</p>
<p>
<s>
Vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
stroj	stroj	k1gInSc1	stroj
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
umístěný	umístěný	k2eAgInSc1d1	umístěný
ve	v	k7c4	v
větší	veliký	k2eAgFnPc4d2	veliký
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
až	až	k9	až
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
kostelaKaždý	kostelaKaždý	k2eAgInSc4d1	kostelaKaždý
ze	z	k7c2	z
strojů	stroj	k1gInPc2	stroj
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
umístěn	umístit	k5eAaPmNgInS	umístit
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
varhanní	varhanní	k2eAgFnSc6d1	varhanní
skříni	skříň	k1gFnSc6	skříň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěno	umístit	k5eAaPmNgNnS	umístit
i	i	k9	i
více	hodně	k6eAd2	hodně
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skříni	skříň	k1gFnSc6	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
jeden	jeden	k4xCgInSc4	jeden
stroj	stroj	k1gInSc4	stroj
rozdělen	rozdělen	k2eAgInSc4d1	rozdělen
do	do	k7c2	do
více	hodně	k6eAd2	hodně
skříní	skříň	k1gFnSc7	skříň
–	–	k?	–
např.	např.	kA	např.
pedál	pedál	k1gInSc1	pedál
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skříní	skříň	k1gFnPc2	skříň
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hlavního	hlavní	k2eAgInSc2d1	hlavní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
strojů	stroj	k1gInPc2	stroj
ovládán	ovládat	k5eAaImNgInS	ovládat
jedním	jeden	k4xCgInSc7	jeden
manuálem	manuál	k1gInSc7	manuál
(	(	kIx(	(
<g/>
klaviaturou	klaviatura	k1gFnSc7	klaviatura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pedálnicí	pedálnice	k1gFnSc7	pedálnice
(	(	kIx(	(
<g/>
pedálem	pedál	k1gInSc7	pedál
<g/>
)	)	kIx)	)
hracího	hrací	k2eAgInSc2d1	hrací
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
počet	počet	k1gInSc1	počet
strojů	stroj	k1gInPc2	stroj
obvykle	obvykle	k6eAd1	obvykle
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
manuálů	manuál	k1gInPc2	manuál
a	a	k8xC	a
pedálů	pedál	k1gInPc2	pedál
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Pedál	pedál	k1gInSc4	pedál
mají	mít	k5eAaImIp3nP	mít
varhany	varhany	k1gFnPc1	varhany
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
španělské	španělský	k2eAgFnPc1d1	španělská
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mívají	mívat	k5eAaImIp3nP	mívat
pedály	pedál	k1gInPc4	pedál
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
kostelích	kostel	k1gInPc6	kostel
nejčastěji	často	k6eAd3	často
nacházíme	nacházet	k5eAaImIp1nP	nacházet
dvoumanuálové	dvoumanuálový	k2eAgFnPc1d1	dvoumanuálový
varhany	varhany	k1gFnPc1	varhany
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
hlavní	hlavní	k2eAgInSc4d1	hlavní
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
positiv	positiv	k1gInSc4	positiv
a	a	k8xC	a
pedál	pedál	k1gInSc4	pedál
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
kostelích	kostel	k1gInPc6	kostel
bývají	bývat	k5eAaImIp3nP	bývat
trojmanuálové	trojmanuálový	k2eAgFnPc1d1	trojmanuálový
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
čtyřmanuálové	čtyřmanuálový	k2eAgFnSc2d1	čtyřmanuálový
–	–	k?	–
i	i	k8xC	i
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koncertních	koncertní	k2eAgFnPc6d1	koncertní
síních	síň	k1gFnPc6	síň
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
malé	malý	k2eAgFnPc1d1	malá
varhany	varhany	k1gFnPc1	varhany
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
jen	jen	k6eAd1	jen
jednomanuálové	jednomanuálový	k2eAgInPc1d1	jednomanuálový
s	s	k7c7	s
pedálem	pedál	k1gInSc7	pedál
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
varhánky	varhánka	k1gFnPc1	varhánka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
manuál	manuál	k1gInSc4	manuál
bez	bez	k7c2	bez
pedálu	pedál	k1gInSc2	pedál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nenazývají	nazývat	k5eNaImIp3nP	nazývat
varhanami	varhany	k1gFnPc7	varhany
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
positivem	positiv	k1gInSc7	positiv
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
přenosný	přenosný	k2eAgInSc1d1	přenosný
positiv	positiv	k1gInSc1	positiv
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
portativem	portativ	k1gInSc7	portativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
stroj	stroj	k1gInSc1	stroj
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vzdušnici	vzdušnice	k1gFnSc4	vzdušnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
píšťaly	píšťala	k1gFnPc4	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
stroji	stroj	k1gInSc3	stroj
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
hracího	hrací	k2eAgInSc2d1	hrací
stolu	stol	k1gInSc2	stol
traktura	traktura	k1gFnSc1	traktura
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vzduchu	vzduch	k1gInSc2	vzduch
naopak	naopak	k6eAd1	naopak
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
stroje	stroj	k1gInPc4	stroj
společný	společný	k2eAgInSc1d1	společný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
stroje	stroj	k1gInPc1	stroj
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
vyrovnávací	vyrovnávací	k2eAgInSc4d1	vyrovnávací
měch	měch	k1gInSc4	měch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Píšťaly	píšťala	k1gFnSc2	píšťala
====	====	k?	====
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
zvuku	zvuk	k1gInSc2	zvuk
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
píšťaly	píšťala	k1gFnPc1	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
skrz	skrz	k7c4	skrz
ně	on	k3xPp3gMnPc4	on
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vibracemi	vibrace	k1gFnPc7	vibrace
kovového	kovový	k2eAgInSc2d1	kovový
jazýčku	jazýček	k1gInSc2	jazýček
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
píšťaly	píšťala	k1gFnSc2	píšťala
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
frekvenci	frekvence	k1gFnSc4	frekvence
zvuku	zvuk	k1gInSc2	zvuk
<g/>
;	;	kIx,	;
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
píšťala	píšťala	k1gFnSc1	píšťala
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
hlubší	hluboký	k2eAgInSc4d2	hlubší
tón	tón	k1gInSc4	tón
vydává	vydávat	k5eAaPmIp3nS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
píšťala	píšťala	k1gFnSc1	píšťala
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
tón	tón	k1gInSc4	tón
-	-	kIx~	-
zvuk	zvuk	k1gInSc1	zvuk
varhan	varhany	k1gFnPc2	varhany
vzniká	vznikat	k5eAaImIp3nS	vznikat
simultánním	simultánní	k2eAgNnSc7d1	simultánní
hraním	hraní	k1gNnSc7	hraní
více	hodně	k6eAd2	hodně
píšťal	píšťala	k1gFnPc2	píšťala
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Hlasitost	hlasitost	k1gFnSc1	hlasitost
tónu	tón	k1gInSc2	tón
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Píšťaly	píšťala	k1gFnPc4	píšťala
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kriterií	kriterium	k1gNnPc2	kriterium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dle	dle	k7c2	dle
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
způsobu	způsob	k1gInSc2	způsob
vzniku	vznik	k1gInSc2	vznik
zvuku	zvuk	k1gInSc2	zvuk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
retné	retný	k2eAgFnPc4d1	retný
(	(	kIx(	(
<g/>
labiální	labiální	k2eAgFnPc4d1	labiální
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zvuk	zvuk	k1gInSc1	zvuk
zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
chvěním	chvění	k1gNnSc7	chvění
vzduchového	vzduchový	k2eAgInSc2d1	vzduchový
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
zobcové	zobcový	k2eAgFnSc2d1	zobcová
flétny	flétna	k1gFnSc2	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
především	především	k6eAd1	především
délkou	délka	k1gFnSc7	délka
kmitajícího	kmitající	k2eAgInSc2d1	kmitající
vzduchového	vzduchový	k2eAgInSc2d1	vzduchový
sloupce	sloupec	k1gInSc2	sloupec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
otevřené	otevřený	k2eAgNnSc1d1	otevřené
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
konec	konec	k1gInSc1	konec
píšťaly	píšťala	k1gFnSc2	píšťala
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
kmitajícího	kmitající	k2eAgInSc2d1	kmitající
sloupce	sloupec	k1gInSc2	sloupec
rovná	rovnat	k5eAaImIp3nS	rovnat
délce	délka	k1gFnSc3	délka
akustické	akustický	k2eAgFnPc4d1	akustická
půlvlny	půlvlna	k1gFnPc4	půlvlna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
píšťaly	píšťala	k1gFnPc1	píšťala
produkují	produkovat	k5eAaImIp3nP	produkovat
kromě	kromě	k7c2	kromě
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
sudé	sudý	k2eAgFnSc2d1	sudá
i	i	k8xC	i
liché	lichý	k2eAgFnSc2d1	lichá
alikvoty	alikvota	k1gFnSc2	alikvota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
válcovité	válcovitý	k2eAgInPc1d1	válcovitý
–	–	k?	–
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
tvar	tvar	k1gInSc1	tvar
píšťal	píšťala	k1gFnPc2	píšťala
</s>
</p>
<p>
<s>
kónické	kónický	k2eAgFnPc1d1	kónická
–	–	k?	–
nahoru	nahoru	k6eAd1	nahoru
se	s	k7c7	s
zužující	zužující	k2eAgFnSc7d1	zužující
</s>
</p>
<p>
<s>
trychtýřovité	trychtýřovitý	k2eAgFnPc1d1	trychtýřovitá
–	–	k?	–
nahoru	nahoru	k6eAd1	nahoru
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgFnSc3d1	rozšiřující
</s>
</p>
<p>
<s>
kryté	krytý	k2eAgInPc4d1	krytý
(	(	kIx(	(
<g/>
kryty	kryt	k1gInPc4	kryt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
konec	konec	k1gInSc1	konec
píšťaly	píšťala	k1gFnSc2	píšťala
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
pohyblivou	pohyblivý	k2eAgFnSc7d1	pohyblivá
zátkou	zátka	k1gFnSc7	zátka
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
ladění	ladění	k1gNnSc3	ladění
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
kmitajícího	kmitající	k2eAgInSc2d1	kmitající
sloupce	sloupec	k1gInSc2	sloupec
rovná	rovnat	k5eAaImIp3nS	rovnat
délce	délka	k1gFnSc3	délka
akustické	akustický	k2eAgFnPc4d1	akustická
čtvrtvlny	čtvrtvlna	k1gFnPc4	čtvrtvlna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
píšťaly	píšťala	k1gFnPc1	píšťala
produkují	produkovat	k5eAaImIp3nP	produkovat
kromě	kromě	k7c2	kromě
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
jen	jen	k9	jen
liché	lichý	k2eAgInPc4d1	lichý
alikvoty	alikvot	k1gInPc4	alikvot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
polokryté	polokrytý	k2eAgFnPc1d1	polokrytá
<g/>
:	:	kIx,	:
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
otevřenými	otevřený	k2eAgInPc7d1	otevřený
a	a	k8xC	a
krytými	krytý	k2eAgInPc7d1	krytý
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc1	jejich
základní	základní	k2eAgInSc1d1	základní
kmitočet	kmitočet	k1gInSc1	kmitočet
leží	ležet	k5eAaImIp3nS	ležet
také	také	k9	také
mezi	mezi	k7c7	mezi
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruován	k2eAgInPc1d1	konstruován
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
trubicové	trubicový	k2eAgNnSc1d1	trubicové
<g/>
:	:	kIx,	:
zátka	zátka	k1gFnSc1	zátka
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
ještě	ještě	k9	ještě
se	s	k7c7	s
vsazenou	vsazený	k2eAgFnSc7d1	vsazená
trubicí	trubice	k1gFnSc7	trubice
</s>
</p>
<p>
<s>
kónické	kónický	k2eAgNnSc1d1	kónické
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gInSc1	jejich
průřez	průřez	k1gInSc1	průřez
se	se	k3xPyFc4	se
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
otvor	otvor	k1gInSc1	otvor
</s>
</p>
<p>
<s>
jazykové	jazykový	k2eAgInPc4d1	jazykový
(	(	kIx(	(
<g/>
jazyky	jazyk	k1gInPc4	jazyk
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zvuk	zvuk	k1gInSc1	zvuk
vzniká	vznikat	k5eAaImIp3nS	vznikat
chvěním	chvění	k1gNnSc7	chvění
kovového	kovový	k2eAgInSc2d1	kovový
jazýčku	jazýček	k1gInSc2	jazýček
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
rozměry	rozměr	k1gInPc1	rozměr
a	a	k8xC	a
materiál	materiál	k1gInSc1	materiál
určuje	určovat	k5eAaImIp3nS	určovat
výšku	výška	k1gFnSc4	výška
tónu	tón	k1gInSc2	tón
<g/>
;	;	kIx,	;
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
píšťaly	píšťala	k1gFnSc2	píšťala
(	(	kIx(	(
<g/>
rezonátor	rezonátor	k1gInSc1	rezonátor
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
zabarvení	zabarvení	k1gNnSc6	zabarvení
výsledného	výsledný	k2eAgInSc2d1	výsledný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
výšku	výška	k1gFnSc4	výška
podstatně	podstatně	k6eAd1	podstatně
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jazykové	jazykový	k2eAgFnPc1d1	jazyková
píšťaly	píšťala	k1gFnPc1	píšťala
nárazné	nárazný	k2eAgFnPc1d1	nárazný
<g/>
:	:	kIx,	:
Jazyk	jazyk	k1gInSc1	jazyk
zde	zde	k6eAd1	zde
naráží	narážet	k5eAaImIp3nS	narážet
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
např.	např.	kA	např.
plátek	plátek	k1gInSc1	plátek
u	u	k7c2	u
klarinetu	klarinet	k1gInSc2	klarinet
nebo	nebo	k8xC	nebo
u	u	k7c2	u
hoboje	hoboj	k1gFnSc2	hoboj
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
takové	takový	k3xDgFnSc2	takový
píšťaly	píšťala	k1gFnSc2	píšťala
je	být	k5eAaImIp3nS	být
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
harmonických	harmonický	k2eAgInPc2d1	harmonický
(	(	kIx(	(
<g/>
alikvotních	alikvotní	k2eAgInPc2d1	alikvotní
<g/>
)	)	kIx)	)
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jazykové	jazykový	k2eAgFnPc1d1	jazyková
píšťaly	píšťala	k1gFnPc1	píšťala
průrazné	průrazný	k2eAgFnPc1d1	průrazná
<g/>
:	:	kIx,	:
Jazyk	jazyk	k1gInSc1	jazyk
zde	zde	k6eAd1	zde
prokmitává	prokmitávat	k5eAaImIp3nS	prokmitávat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
např.	např.	kA	např.
plátek	plátek	k1gInSc1	plátek
u	u	k7c2	u
harmoniky	harmonika	k1gFnSc2	harmonika
nebo	nebo	k8xC	nebo
u	u	k7c2	u
harmonia	harmonium	k1gNnSc2	harmonium
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
takové	takový	k3xDgFnSc2	takový
píšťaly	píšťala	k1gFnSc2	píšťala
je	být	k5eAaImIp3nS	být
kulatější	kulatý	k2eAgNnSc1d2	kulatější
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
harmonických	harmonický	k2eAgInPc2d1	harmonický
(	(	kIx(	(
<g/>
alikvotních	alikvotní	k2eAgInPc2d1	alikvotní
<g/>
)	)	kIx)	)
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dle	dle	k7c2	dle
materiálu	materiál	k1gInSc2	materiál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dřevěné	dřevěný	k2eAgMnPc4d1	dřevěný
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
obdélným	obdélný	k2eAgInSc7d1	obdélný
průřezem	průřez	k1gInSc7	průřez
</s>
</p>
<p>
<s>
kovové	kovový	k2eAgInPc1d1	kovový
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
kruhovým	kruhový	k2eAgInSc7d1	kruhový
průřezem	průřez	k1gInSc7	průřez
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zpravidla	zpravidla	k6eAd1	zpravidla
cínové	cínový	k2eAgInPc1d1	cínový
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
gotické	gotický	k2eAgFnSc6d1	gotická
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
měděné	měděný	k2eAgFnPc1d1	měděná
</s>
</p>
<p>
<s>
u	u	k7c2	u
levných	levný	k2eAgFnPc2d1	levná
varhan	varhany	k1gFnPc2	varhany
nebo	nebo	k8xC	nebo
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
cínu	cín	k1gInSc2	cín
nedostatek	nedostatek	k1gInSc1	nedostatek
(	(	kIx(	(
<g/>
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
)	)	kIx)	)
zinkové	zinkový	k2eAgNnSc1d1	zinkové
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
rekvírování	rekvírování	k?	rekvírování
varhanních	varhanní	k2eAgFnPc2d1	varhanní
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
kostelních	kostelní	k2eAgInPc2d1	kostelní
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
na	na	k7c4	na
strategickou	strategický	k2eAgFnSc4d1	strategická
surovinu	surovina	k1gFnSc4	surovina
pro	pro	k7c4	pro
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Zinkové	zinkový	k2eAgFnPc1d1	zinková
píšťaly	píšťala	k1gFnPc1	píšťala
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
používaly	používat	k5eAaImAgInP	používat
jako	jako	k8xC	jako
náhražka	náhražka	k1gFnSc1	náhražka
cínových	cínový	k2eAgMnPc2d1	cínový
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výjimečně	výjimečně	k6eAd1	výjimečně
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
amatérské	amatérský	k2eAgFnPc1d1	amatérská
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
levné	levný	k2eAgFnPc1d1	levná
domácí	domácí	k2eAgFnPc4d1	domácí
varhánky	varhánka	k1gFnPc4	varhánka
<g/>
,	,	kIx,	,
portativy	portativ	k1gInPc4	portativ
<g/>
,	,	kIx,	,
hračky	hračka	k1gFnPc4	hračka
<g/>
,	,	kIx,	,
extravagantní	extravagantní	k2eAgInPc4d1	extravagantní
nápady	nápad	k1gInPc4	nápad
atd	atd	kA	atd
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
sololit	sololit	k1gInSc1	sololit
<g/>
,	,	kIx,	,
překližka	překližka	k1gFnSc1	překližka
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgFnPc1d1	umělá
hmoty	hmota	k1gFnPc1	hmota
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
dle	dle	k7c2	dle
mensury	mensura	k1gFnSc2	mensura
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
píšťaly	píšťala	k1gFnPc1	píšťala
s	s	k7c7	s
normální	normální	k2eAgFnSc7d1	normální
mensurou	mensura	k1gFnSc7	mensura
</s>
</p>
<p>
<s>
píšťaly	píšťala	k1gFnPc1	píšťala
široké	široký	k2eAgFnPc1d1	široká
</s>
</p>
<p>
<s>
píšťaly	píšťala	k1gFnPc4	píšťala
úzkéPodle	úzkéPodle	k6eAd1	úzkéPodle
uvedených	uvedený	k2eAgNnPc2d1	uvedené
kriterií	kriterium	k1gNnPc2	kriterium
pak	pak	k8xC	pak
celé	celý	k2eAgFnSc2d1	celá
píšťalové	píšťalový	k2eAgFnSc2d1	píšťalová
řady	řada	k1gFnSc2	řada
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
rejstříky	rejstřík	k1gInPc1	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rejstříku	rejstřík	k1gInSc2	rejstřík
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
jak	jak	k6eAd1	jak
mensura	mensura	k1gFnSc1	mensura
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
použitý	použitý	k2eAgInSc4d1	použitý
materiál	materiál	k1gInSc4	materiál
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
v	v	k7c4	v
horní	horní	k2eAgInSc4d1	horní
cín	cín	k1gInSc4	cín
<g/>
)	)	kIx)	)
či	či	k8xC	či
stavba	stavba	k1gFnSc1	stavba
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
kryté	krytý	k2eAgFnSc6d1	krytá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
polokryté	polokrytý	k2eAgFnSc6d1	polokrytá
a	a	k8xC	a
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
otevřené	otevřený	k2eAgFnSc6d1	otevřená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rejstříky	rejstřík	k1gInPc4	rejstřík
====	====	k?	====
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
stroj	stroj	k1gInSc1	stroj
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
několika	několik	k4yIc2	několik
řad	řada	k1gFnPc2	řada
píšťal	píšťala	k1gFnPc2	píšťala
čili	čili	k8xC	čili
rejstříků	rejstřík	k1gInPc2	rejstřík
(	(	kIx(	(
<g/>
registrů	registr	k1gInPc2	registr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
možno	možno	k6eAd1	možno
nezávisle	závisle	k6eNd1	závisle
zapínat	zapínat	k5eAaImF	zapínat
a	a	k8xC	a
vypínat	vypínat	k5eAaImF	vypínat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vymoženost	vymoženost	k1gFnSc1	vymoženost
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
renesance	renesance	k1gFnSc2	renesance
<g/>
;	;	kIx,	;
v	v	k7c6	v
gotických	gotický	k2eAgFnPc6d1	gotická
varhanách	varhany	k1gFnPc6	varhany
zpravidla	zpravidla	k6eAd1	zpravidla
hrály	hrát	k5eAaImAgInP	hrát
všechny	všechen	k3xTgInPc1	všechen
rejstříky	rejstřík	k1gInPc1	rejstřík
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
rejstřík	rejstřík	k1gInSc1	rejstřík
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
,	,	kIx,	,
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
půltónové	půltónový	k2eAgFnSc6d1	půltónová
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
shodnou	shodný	k2eAgFnSc4d1	shodná
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
parametry	parametr	k1gInPc4	parametr
(	(	kIx(	(
<g/>
hlasitost	hlasitost	k1gFnSc1	hlasitost
<g/>
)	)	kIx)	)
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Píšťaly	píšťala	k1gFnPc1	píšťala
hlubokých	hluboký	k2eAgInPc2d1	hluboký
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgInPc2d1	vysoký
tónů	tón	k1gInPc2	tón
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
materiál	materiál	k1gInSc4	materiál
i	i	k8xC	i
konstrukci	konstrukce	k1gFnSc4	konstrukce
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
)	)	kIx)	)
budou	být	k5eAaImBp3nP	být
znít	znít	k5eAaImF	znít
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Kolika	kolika	k1gFnSc1	kolika
a	a	k8xC	a
jakými	jaký	k3yRgInPc7	jaký
rejstříky	rejstřík	k1gInPc7	rejstřík
varhany	varhany	k1gInPc7	varhany
disponují	disponovat	k5eAaBmIp3nP	disponovat
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
disposice	disposice	k1gFnSc1	disposice
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stroj	stroj	k1gInSc1	stroj
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jiný	jiný	k2eAgInSc4d1	jiný
počet	počet	k1gInSc4	počet
rejstříků	rejstřík	k1gInPc2	rejstřík
a	a	k8xC	a
rejstříky	rejstřík	k1gInPc1	rejstřík
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
různé	různý	k2eAgFnSc2d1	různá
síly	síla	k1gFnSc2	síla
i	i	k8xC	i
charakteru	charakter	k1gInSc2	charakter
zvuku	zvuk	k1gInSc2	zvuk
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
manuály	manuál	k1gInPc4	manuál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
rejstříku	rejstřík	k1gInSc2	rejstřík
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yRgFnSc6	jaký
výšce	výška	k1gFnSc6	výška
píšťaly	píšťala	k1gFnSc2	píšťala
znějí	znět	k5eAaImIp3nP	znět
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
délku	délka	k1gFnSc4	délka
má	mít	k5eAaImIp3nS	mít
otevřená	otevřený	k2eAgFnSc1d1	otevřená
labiální	labiální	k2eAgFnSc1d1	labiální
píšťala	píšťala	k1gFnSc1	píšťala
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
klávese	klávesa	k1gFnSc6	klávesa
velké	velká	k1gFnSc2	velká
C.	C.	kA	C.
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
také	také	k9	také
čárkou	čárka	k1gFnSc7	čárka
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celočíselnou	celočíselný	k2eAgFnSc4d1	celočíselná
mocninu	mocnina	k1gFnSc4	mocnina
čísla	číslo	k1gNnSc2	číslo
2	[number]	k4	2
(	(	kIx(	(
<g/>
takže	takže	k8xS	takže
klasické	klasický	k2eAgInPc4d1	klasický
rejstříky	rejstřík	k1gInPc4	rejstřík
mají	mít	k5eAaImIp3nP	mít
výšky	výška	k1gFnPc1	výška
1	[number]	k4	1
<g/>
'	'	kIx"	'
,	,	kIx,	,
2	[number]	k4	2
<g/>
'	'	kIx"	'
,	,	kIx,	,
4	[number]	k4	4
<g/>
'	'	kIx"	'
,	,	kIx,	,
8	[number]	k4	8
<g/>
'	'	kIx"	'
a	a	k8xC	a
16	[number]	k4	16
<g/>
'	'	kIx"	'
-	-	kIx~	-
vyšší	vysoký	k2eAgInPc1d2	vyšší
rejstříky	rejstřík	k1gInPc1	rejstřík
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
jen	jen	k9	jen
na	na	k7c6	na
velikých	veliký	k2eAgFnPc6d1	veliká
varhanách	varhany	k1gFnPc6	varhany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Názvů	název	k1gInPc2	název
rejstříků	rejstřík	k1gInPc2	rejstřík
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
jich	on	k3xPp3gInPc2	on
být	být	k5eAaImF	být
stovky	stovka	k1gFnPc4	stovka
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
takových	takový	k3xDgInPc2	takový
až	až	k6eAd1	až
exotických	exotický	k2eAgInPc2d1	exotický
názvů	název	k1gInPc2	název
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
romantice	romantika	k1gFnSc6	romantika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
varhanáři	varhanář	k1gMnPc1	varhanář
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
módy	móda	k1gFnSc2	móda
snažili	snažit	k5eAaImAgMnP	snažit
napodobit	napodobit	k5eAaPmF	napodobit
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ptačí	ptačí	k2eAgNnSc4d1	ptačí
cvrlikání	cvrlikání	k1gNnSc4	cvrlikání
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc4d1	lidský
hlas	hlas	k1gInSc4	hlas
–	–	k?	–
vox	vox	k?	vox
humana	humana	k1gFnSc1	humana
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
uvedeme	uvést	k5eAaPmIp1nP	uvést
jen	jen	k9	jen
několik	několik	k4yIc4	několik
základních	základní	k2eAgFnPc2d1	základní
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
píšťaly	píšťala	k1gFnPc1	píšťala
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Normální	normální	k2eAgFnSc1d1	normální
mensura	mensura	k1gFnSc1	mensura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Principál	principál	k1gInSc1	principál
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
základním	základní	k2eAgInSc7d1	základní
rejstříkem	rejstřík	k1gInSc7	rejstřík
téměř	téměř	k6eAd1	téměř
každých	každý	k3xTgFnPc2	každý
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
,	,	kIx,	,
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
délce	délka	k1gFnSc6	délka
u	u	k7c2	u
hlavního	hlavní	k2eAgInSc2d1	hlavní
stroje	stroj	k1gInSc2	stroj
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
strojů	stroj	k1gInPc2	stroj
nebo	nebo	k8xC	nebo
u	u	k7c2	u
menších	malý	k2eAgFnPc2d2	menší
varhan	varhany	k1gFnPc2	varhany
a	a	k8xC	a
positivů	positiv	k1gInPc2	positiv
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
4	[number]	k4	4
<g/>
stopý	stopý	k2eAgInSc4d1	stopý
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oktáva	oktáva	k1gFnSc1	oktáva
<g/>
:	:	kIx,	:
zpravidla	zpravidla	k6eAd1	zpravidla
stejné	stejný	k2eAgFnPc1d1	stejná
konstrukce	konstrukce	k1gFnPc1	konstrukce
a	a	k8xC	a
mensury	mensura	k1gFnPc1	mensura
jako	jako	k8xS	jako
principál	principál	k1gInSc1	principál
<g/>
,	,	kIx,	,
posazená	posazený	k2eAgFnSc1d1	posazená
o	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
výš	výš	k1gFnSc1wB	výš
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
např.	např.	kA	např.
pro	pro	k7c4	pro
4	[number]	k4	4
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
principál	principál	k1gInSc1	principál
bude	být	k5eAaImBp3nS	být
oktáva	oktáva	k1gFnSc1	oktáva
2	[number]	k4	2
<g/>
stopá	stopat	k5eAaPmIp3nS	stopat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Superoktáva	Superoktáva	k1gFnSc1	Superoktáva
<g/>
:	:	kIx,	:
další	další	k2eAgFnSc1d1	další
oktáva	oktáva	k1gFnSc1	oktáva
nad	nad	k7c7	nad
oktávou	oktáva	k1gFnSc7	oktáva
</s>
</p>
<p>
<s>
Široká	široký	k2eAgFnSc1d1	široká
mensura	mensura	k1gFnSc1	mensura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Úzká	úzký	k2eAgFnSc1d1	úzká
mensura	mensura	k1gFnSc1	mensura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Salicionál	salicionál	k1gInSc1	salicionál
</s>
</p>
<p>
<s>
Gamba	gamba	k1gFnSc1	gamba
</s>
</p>
<p>
<s>
Mixtura	mixtura	k1gFnSc1	mixtura
<g/>
:	:	kIx,	:
Rejstřík	rejstřík	k1gInSc4	rejstřík
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
několika	několik	k4yIc2	několik
řad	řada	k1gFnPc2	řada
malých	malý	k2eAgFnPc2d1	malá
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
korunu	koruna	k1gFnSc4	koruna
(	(	kIx(	(
<g/>
posiluje	posilovat	k5eAaImIp3nS	posilovat
alikvotní	alikvotní	k2eAgInPc4d1	alikvotní
tóny	tón	k1gInPc4	tón
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
principály	principál	k1gMnPc7	principál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kryté	krytý	k2eAgFnPc1d1	krytá
píšťaly	píšťala	k1gFnPc1	píšťala
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kryt	kryt	k1gInSc1	kryt
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
Copula	Copula	k1gFnSc1	Copula
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zpravidla	zpravidla	k6eAd1	zpravidla
osmistopý	osmistopý	k2eAgInSc1d1	osmistopý
základní	základní	k2eAgInSc1d1	základní
rejstřík	rejstřík	k1gInSc1	rejstřík
u	u	k7c2	u
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
principál	principál	k1gInSc1	principál
na	na	k7c6	na
osmistopé	osmistopý	k2eAgFnSc6d1	osmistopý
posici	posice	k1gFnSc6	posice
není	být	k5eNaImIp3nS	být
disponován	disponován	k2eAgInSc1d1	disponován
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
partiích	partie	k1gFnPc6	partie
<g/>
)	)	kIx)	)
dřevěný	dřevěný	k2eAgMnSc1d1	dřevěný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kryt	kryt	k1gInSc1	kryt
malý	malý	k2eAgInSc1d1	malý
(	(	kIx(	(
<g/>
Copula	Copula	k1gFnSc1	Copula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
O	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
výše	vysoce	k6eAd2	vysoce
posazený	posazený	k2eAgInSc1d1	posazený
nad	nad	k7c7	nad
velkým	velký	k2eAgInSc7d1	velký
krytem	kryt	k1gInSc7	kryt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
čtyřstopý	čtyřstopý	k2eAgInSc1d1	čtyřstopý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bourdon	bourdon	k1gInSc1	bourdon
<g/>
:	:	kIx,	:
Zpravidla	zpravidla	k6eAd1	zpravidla
16	[number]	k4	16
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
<g/>
,	,	kIx,	,
či	či	k8xC	či
8	[number]	k4	8
<g/>
stopý	stopý	k2eAgInSc4d1	stopý
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
rejstřík	rejstřík	k1gInSc4	rejstřík
disponovaný	disponovaný	k2eAgInSc4d1	disponovaný
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
stroji	stroj	k1gInSc6	stroj
středně	středně	k6eAd1	středně
velkých	velký	k2eAgFnPc6d1	velká
či	či	k8xC	či
větších	veliký	k2eAgFnPc6d2	veliký
varhanách	varhany	k1gFnPc6	varhany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Subbas	Subbas	k1gInSc1	Subbas
<g/>
:	:	kIx,	:
16	[number]	k4	16
<g/>
stopý	stopý	k2eAgInSc4d1	stopý
zpravidla	zpravidla	k6eAd1	zpravidla
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
rejstřík	rejstřík	k1gInSc4	rejstřík
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
základní	základní	k2eAgInSc1d1	základní
rejstřík	rejstřík	k1gInSc1	rejstřík
disponovaný	disponovaný	k2eAgInSc1d1	disponovaný
v	v	k7c6	v
pedálu	pedál	k1gInSc6	pedál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Echobas	Echobas	k1gInSc1	Echobas
<g/>
:	:	kIx,	:
Zpravidla	zpravidla	k6eAd1	zpravidla
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
subbas	subbas	k1gInSc4	subbas
o	o	k7c6	o
nižším	nízký	k2eAgInSc6d2	nižší
tlaku	tlak	k1gInSc6	tlak
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
snížen	snížit	k5eAaPmNgInS	snížit
asi	asi	k9	asi
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
píšťaly	píšťala	k1gFnPc1	píšťala
(	(	kIx(	(
<g/>
u	u	k7c2	u
českých	český	k2eAgFnPc2d1	Česká
varhan	varhany	k1gFnPc2	varhany
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
,	,	kIx,	,
disponují	disponovat	k5eAaBmIp3nP	disponovat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
jen	jen	k9	jen
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
varhan	varhany	k1gFnPc2	varhany
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trubka	trubka	k1gFnSc1	trubka
(	(	kIx(	(
<g/>
Trompeta	trompeta	k1gFnSc1	trompeta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
rejstřík	rejstřík	k1gInSc1	rejstřík
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hoboj	hoboj	k1gInSc1	hoboj
<g/>
:	:	kIx,	:
Disponován	disponovat	k5eAaBmNgInS	disponovat
spíše	spíše	k9	spíše
na	na	k7c6	na
slabším	slabý	k2eAgInSc6d2	slabší
manuálu	manuál	k1gInSc6	manuál
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bombard	bombarda	k1gFnPc2	bombarda
<g/>
:	:	kIx,	:
Zpravidla	zpravidla	k6eAd1	zpravidla
16	[number]	k4	16
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
rejstřík	rejstřík	k1gInSc1	rejstřík
v	v	k7c6	v
pedálu	pedál	k1gInSc6	pedál
<g/>
,	,	kIx,	,
či	či	k8xC	či
manuálu	manuál	k1gInSc2	manuál
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejhlasitějších	hlasitý	k2eAgInPc2d3	nejhlasitější
rejstříků	rejstřík	k1gInPc2	rejstřík
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozoun	pozoun	k1gInSc1	pozoun
<g/>
:	:	kIx,	:
Nejčastěji	často	k6eAd3	často
16	[number]	k4	16
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
rejstřík	rejstřík	k1gInSc1	rejstřík
v	v	k7c6	v
pedálu	pedál	k1gInSc6	pedál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regál	regál	k1gInSc1	regál
<g/>
:	:	kIx,	:
Často	často	k6eAd1	často
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
základního	základní	k2eAgInSc2d1	základní
rejstříků	rejstřík	k1gInPc2	rejstřík
u	u	k7c2	u
malých	malý	k2eAgInPc2d1	malý
portativů	portativ	k1gInPc2	portativ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
varhan	varhany	k1gFnPc2	varhany
disponován	disponovat	k5eAaBmNgInS	disponovat
v	v	k7c4	v
32	[number]	k4	32
<g/>
stopé	stopý	k2eAgInPc4d1	stopý
až	až	k9	až
4	[number]	k4	4
<g/>
stopé	stopý	k2eAgFnSc6d1	stopá
poloze	poloha	k1gFnSc6	poloha
na	na	k7c6	na
slabších	slabý	k2eAgInPc6d2	slabší
manuálech	manuál	k1gInPc6	manuál
či	či	k8xC	či
v	v	k7c6	v
pedálu	pedál	k1gInSc6	pedál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ophicleide	Ophicleid	k1gMnSc5	Ophicleid
<g/>
:	:	kIx,	:
Výkonný	výkonný	k2eAgMnSc1d1	výkonný
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
16	[number]	k4	16
<g/>
stopý	stopý	k2eAgInSc1d1	stopý
rejstřík	rejstřík	k1gInSc1	rejstřík
v	v	k7c6	v
pedálu	pedál	k1gInSc6	pedál
(	(	kIx(	(
<g/>
výjmku	výjmek	k1gInSc2	výjmek
tvoří	tvořit	k5eAaImIp3nS	tvořit
Contra	Contra	k1gFnSc1	Contra
Ophicleide	Ophicleid	k1gInSc5	Ophicleid
-	-	kIx~	-
32	[number]	k4	32
<g/>
stopá	stopý	k2eAgFnSc1d1	stopá
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
varianta	varianta	k1gFnSc1	varianta
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Grand	grand	k1gMnSc1	grand
Ophicleide	Ophicleid	k1gInSc5	Ophicleid
je	být	k5eAaImIp3nS	být
nejhlasitějším	hlasitý	k2eAgInSc7d3	nejhlasitější
varhanním	varhanní	k2eAgInSc7d1	varhanní
rejstříkem	rejstřík	k1gInSc7	rejstřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vzdušnice	vzdušnice	k1gFnSc2	vzdušnice
====	====	k?	====
</s>
</p>
<p>
<s>
Řady	řada	k1gFnPc1	řada
píšťal	píšťala	k1gFnPc2	píšťala
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c4	na
vzdušnici	vzdušnice	k1gFnSc4	vzdušnice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
proudí	proudit	k5eAaImIp3nP	proudit
do	do	k7c2	do
píšťal	píšťala	k1gFnPc2	píšťala
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stroj	stroj	k1gInSc1	stroj
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
varhanní	varhanní	k2eAgFnSc6d1	varhanní
skříni	skříň	k1gFnSc6	skříň
zpravidla	zpravidla	k6eAd1	zpravidla
jednu	jeden	k4xCgFnSc4	jeden
vzdušnici	vzdušnice	k1gFnSc4	vzdušnice
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgNnSc4d1	nesoucí
příslušné	příslušný	k2eAgFnPc4d1	příslušná
řady	řada	k1gFnPc4	řada
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
varhan	varhany	k1gFnPc2	varhany
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
píšťaly	píšťala	k1gFnPc1	píšťala
rozděleny	rozdělen	k2eAgFnPc1d1	rozdělena
i	i	k9	i
mezi	mezi	k7c4	mezi
více	hodně	k6eAd2	hodně
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ventily	ventil	k1gInPc4	ventil
<g/>
,	,	kIx,	,
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
hráčem	hráč	k1gMnSc7	hráč
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hracího	hrací	k2eAgInSc2d1	hrací
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
pohyb	pohyb	k1gInSc1	pohyb
kláves	klávesa	k1gFnPc2	klávesa
přenáší	přenášet	k5eAaImIp3nS	přenášet
do	do	k7c2	do
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
pomocí	pomocí	k7c2	pomocí
traktury	traktura	k1gFnSc2	traktura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hráči	hráč	k1gMnSc3	hráč
zvolit	zvolit	k5eAaPmF	zvolit
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
řady	řada	k1gFnPc1	řada
píšťal	píšťala	k1gFnPc2	píšťala
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
rejstříkovat	rejstříkovat	k5eAaImF	rejstříkovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zásuvková	zásuvkový	k2eAgFnSc1d1	zásuvková
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
<g/>
:	:	kIx,	:
Hrací	hrací	k2eAgInPc1d1	hrací
ventily	ventil	k1gInPc1	ventil
vpouští	vpouštět	k5eAaImIp3nP	vpouštět
vzduch	vzduch	k1gInSc4	vzduch
do	do	k7c2	do
tónových	tónový	k2eAgMnPc2d1	tónový
kancel	kancet	k5eAaPmAgMnS	kancet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
komůrky	komůrka	k1gFnPc1	komůrka
nad	nad	k7c7	nad
hracími	hrací	k2eAgInPc7d1	hrací
neboli	neboli	k8xC	neboli
tónovými	tónový	k2eAgInPc7d1	tónový
ventily	ventil	k1gInPc7	ventil
-	-	kIx~	-
každá	každý	k3xTgFnSc1	každý
klávesa	klávesa	k1gFnSc1	klávesa
totiž	totiž	k9	totiž
ovládá	ovládat	k5eAaImIp3nS	ovládat
jeden	jeden	k4xCgInSc1	jeden
takový	takový	k3xDgInSc4	takový
ventil	ventil	k1gInSc4	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kancel	kancet	k5eAaPmAgMnS	kancet
pak	pak	k8xC	pak
proudí	proudit	k5eAaImIp3nS	proudit
vzduch	vzduch	k1gInSc1	vzduch
otvory	otvor	k1gInPc7	otvor
do	do	k7c2	do
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
stojí	stát	k5eAaImIp3nS	stát
zásuvky	zásuvka	k1gFnSc2	zásuvka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
posuvné	posuvný	k2eAgFnPc1d1	posuvná
lišty	lišta	k1gFnPc1	lišta
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
ovládané	ovládaný	k2eAgFnSc3d1	ovládaná
manubrii	manubrie	k1gFnSc3	manubrie
od	od	k7c2	od
hracího	hrací	k2eAgInSc2d1	hrací
stolu	stol	k1gInSc2	stol
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
rejstříkové	rejstříkový	k2eAgFnSc2d1	rejstříková
traktury	traktura	k1gFnSc2	traktura
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
otvory	otvor	k1gInPc7	otvor
v	v	k7c6	v
lištách	lišta	k1gFnPc6	lišta
kryjí	krýt	k5eAaImIp3nP	krýt
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
ve	v	k7c6	v
vzdušnici	vzdušnice	k1gFnSc6	vzdušnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rejstřík	rejstřík	k1gInSc1	rejstřík
zapnut	zapnout	k5eAaPmNgInS	zapnout
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
může	moct	k5eAaImIp3nS	moct
proudit	proudit	k5eAaImF	proudit
do	do	k7c2	do
příslušných	příslušný	k2eAgFnPc2d1	příslušná
píšťalových	píšťalový	k2eAgFnPc2d1	píšťalová
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
byla	být	k5eAaImAgFnS	být
typickou	typický	k2eAgFnSc7d1	typická
pro	pro	k7c4	pro
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
trakturu	traktura	k1gFnSc4	traktura
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc3d1	barokní
éře	éra	k1gFnSc3	éra
stavby	stavba	k1gFnSc2	stavba
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kuželková	kuželkový	k2eAgFnSc1d1	Kuželková
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
<g/>
:	:	kIx,	:
Píšťaly	píšťala	k1gFnPc1	píšťala
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
rejstříkových	rejstříkový	k2eAgFnPc6d1	rejstříková
kancelách	kancela	k1gFnPc6	kancela
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
komory	komora	k1gFnPc1	komora
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnPc1d1	probíhající
pod	pod	k7c7	pod
celými	celý	k2eAgFnPc7d1	celá
řadami	řada	k1gFnPc7	řada
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
pouští	pouštět	k5eAaImIp3nS	pouštět
vzduch	vzduch	k1gInSc1	vzduch
přes	přes	k7c4	přes
rejstříkové	rejstříkový	k2eAgInPc4d1	rejstříkový
ventily	ventil	k1gInPc4	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rejstříkových	rejstříkový	k2eAgMnPc2d1	rejstříkový
kancel	kancet	k5eAaImAgMnS	kancet
se	se	k3xPyFc4	se
pouští	poušť	k1gFnSc7	poušť
vzduch	vzduch	k1gInSc1	vzduch
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
píšťal	píšťala	k1gFnPc2	píšťala
vlastními	vlastní	k2eAgInPc7d1	vlastní
kuželkovými	kuželkový	k2eAgInPc7d1	kuželkový
ventily	ventil	k1gInPc7	ventil
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
píšťala	píšťala	k1gFnSc1	píšťala
má	mít	k5eAaImIp3nS	mít
svojí	svojit	k5eAaImIp3nS	svojit
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
kuželku	kuželka	k1gFnSc4	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Tónová	tónový	k2eAgFnSc1d1	tónová
traktura	traktura	k1gFnSc1	traktura
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
najednou	najednou	k6eAd1	najednou
zdvihat	zdvihat	k5eAaImF	zdvihat
kuželky	kuželka	k1gFnPc4	kuželka
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
rejstříky	rejstřík	k1gInPc4	rejstřík
daného	daný	k2eAgInSc2d1	daný
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vzdušnice	vzdušnice	k1gFnPc1	vzdušnice
našly	najít	k5eAaPmAgFnP	najít
rozšíření	rozšíření	k1gNnSc4	rozšíření
za	za	k7c4	za
romantické	romantický	k2eAgFnPc4d1	romantická
éry	éra	k1gFnPc4	éra
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
pneumatickou	pneumatický	k2eAgFnSc7d1	pneumatická
trakturou	traktura	k1gFnSc7	traktura
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
nejznámějších	známý	k2eAgInPc2d3	nejznámější
způsobů	způsob	k1gInPc2	způsob
uspořádání	uspořádání	k1gNnSc2	uspořádání
můžou	můžou	k?	můžou
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
různé	různý	k2eAgFnPc4d1	různá
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Varhanní	varhanní	k2eAgFnSc1d1	varhanní
skříň	skříň	k1gFnSc1	skříň
====	====	k?	====
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgFnSc1d1	varhanní
skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
každých	každý	k3xTgFnPc6	každý
varhanách	varhany	k1gFnPc6	varhany
nejvíce	nejvíce	k6eAd1	nejvíce
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
skříní	skříň	k1gFnPc2	skříň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
skříň	skříň	k1gFnSc4	skříň
dělenou	dělený	k2eAgFnSc4d1	dělená
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
skříň	skříň	k1gFnSc1	skříň
skrývá	skrývat	k5eAaImIp3nS	skrývat
varhanní	varhanní	k2eAgInSc1d1	varhanní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsme	být	k5eAaImIp1nP	být
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnPc1d1	nesoucí
řady	řada	k1gFnPc1	řada
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Někdy	někdy	k6eAd1	někdy
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc1	jeden
stroj	stroj	k1gInSc1	stroj
např.	např.	kA	např.
symetricky	symetricky	k6eAd1	symetricky
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skříní	skříň	k1gFnPc2	skříň
anebo	anebo	k8xC	anebo
naopak	naopak	k6eAd1	naopak
jedna	jeden	k4xCgFnSc1	jeden
skříň	skříň	k1gFnSc1	skříň
může	moct	k5eAaImIp3nS	moct
ukrývat	ukrývat	k5eAaImF	ukrývat
více	hodně	k6eAd2	hodně
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Z	z	k7c2	z
varhanního	varhanní	k2eAgInSc2d1	varhanní
stroje	stroj	k1gInSc2	stroj
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
vidět	vidět	k5eAaImF	vidět
pouze	pouze	k6eAd1	pouze
přední	přední	k2eAgFnPc4d1	přední
píšťaly	píšťala	k1gFnPc4	píšťala
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
bývá	bývat	k5eAaImIp3nS	bývat
věnována	věnován	k2eAgFnSc1d1	věnována
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
;	;	kIx,	;
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
předních	přední	k2eAgFnPc2d1	přední
<g/>
,	,	kIx,	,
viditelných	viditelný	k2eAgFnPc2d1	viditelná
píšťal	píšťala	k1gFnPc2	píšťala
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prospekt	prospekt	k1gInSc1	prospekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebývá	bývat	k5eNaImIp3nS	bývat
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
varhanní	varhanní	k2eAgFnSc1d1	varhanní
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vlastních	vlastní	k2eAgFnPc2d1	vlastní
varhan	varhany	k1gFnPc2	varhany
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
či	či	k8xC	či
restauruje	restaurovat	k5eAaBmIp3nS	restaurovat
jiný	jiný	k2eAgMnSc1d1	jiný
řemeslník	řemeslník	k1gMnSc1	řemeslník
či	či	k8xC	či
jiná	jiný	k2eAgFnSc1d1	jiná
firma	firma	k1gFnSc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
varhanní	varhanní	k2eAgFnSc6d1	varhanní
skříni	skříň	k1gFnSc6	skříň
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
akustických	akustický	k2eAgInPc2d1	akustický
a	a	k8xC	a
technických	technický	k2eAgInPc2d1	technický
parametrů	parametr	k1gInPc2	parametr
významná	významný	k2eAgFnSc1d1	významná
i	i	k8xC	i
stránka	stránka	k1gFnSc1	stránka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
provedení	provedení	k1gNnSc2	provedení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
provedením	provedení	k1gNnSc7	provedení
interiéru	interiér	k1gInSc2	interiér
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
architekturou	architektura	k1gFnSc7	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
by	by	kYmCp3nS	by
ale	ale	k8xC	ale
styl	styl	k1gInSc1	styl
varhanní	varhanní	k2eAgFnSc2d1	varhanní
skříně	skříň	k1gFnSc2	skříň
měl	mít	k5eAaImAgInS	mít
korespondovat	korespondovat	k5eAaImF	korespondovat
i	i	k9	i
se	s	k7c7	s
stylem	styl	k1gInSc7	styl
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Traktura	traktura	k1gFnSc1	traktura
====	====	k?	====
</s>
</p>
<p>
<s>
Rozvádí	rozvádět	k5eAaImIp3nP	rozvádět
pohyby	pohyb	k1gInPc1	pohyb
kláves	klávesa	k1gFnPc2	klávesa
a	a	k8xC	a
rejstříkových	rejstříkový	k2eAgFnPc2d1	rejstříková
manubrií	manubrie	k1gFnPc2	manubrie
či	či	k8xC	či
klapek	klapka	k1gFnPc2	klapka
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Traktura	traktura	k1gFnSc1	traktura
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mechanická	mechanický	k2eAgFnSc1d1	mechanická
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
zpravidla	zpravidla	k6eAd1	zpravidla
pomocí	pomocí	k7c2	pomocí
táhel	táhlo	k1gNnPc2	táhlo
(	(	kIx(	(
<g/>
abstrakty	abstrakt	k1gInPc4	abstrakt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pák	páka	k1gFnPc2	páka
<g/>
,	,	kIx,	,
úhelníků	úhelník	k1gInPc2	úhelník
<g/>
,	,	kIx,	,
hřídelí	hřídel	k1gFnPc2	hřídel
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
mechanických	mechanický	k2eAgInPc2d1	mechanický
elementů	element	k1gInPc2	element
</s>
</p>
<p>
<s>
pneumatická	pneumatický	k2eAgFnSc1d1	pneumatická
–	–	k?	–
pomocí	pomoc	k1gFnSc7	pomoc
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
trubiček	trubička	k1gFnPc2	trubička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
vzduchová	vzduchový	k2eAgNnPc4d1	vzduchové
relé	relé	k1gNnPc4	relé
</s>
</p>
<p>
<s>
tlakový	tlakový	k2eAgInSc1d1	tlakový
systém	systém	k1gInSc1	systém
–	–	k?	–
při	při	k7c6	při
stlačení	stlačení	k1gNnSc6	stlačení
klávesy	klávesa	k1gFnSc2	klávesa
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
pneumatické	pneumatický	k2eAgFnSc6d1	pneumatická
traktuře	traktura	k1gFnSc6	traktura
</s>
</p>
<p>
<s>
výpustný	výpustný	k2eAgInSc1d1	výpustný
systém	systém	k1gInSc1	systém
–	–	k?	–
při	při	k7c6	při
stlačení	stlačení	k1gNnSc6	stlačení
klávesy	klávesa	k1gFnSc2	klávesa
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
pneumatické	pneumatický	k2eAgFnSc6d1	pneumatická
traktuře	traktura	k1gFnSc6	traktura
</s>
</p>
<p>
<s>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
–	–	k?	–
pomocí	pomocí	k7c2	pomocí
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
elektrických	elektrický	k2eAgInPc2d1	elektrický
kabelů	kabel	k1gInPc2	kabel
se	se	k3xPyFc4	se
přivádí	přivádět	k5eAaImIp3nS	přivádět
el.	el.	k?	el.
proud	proud	k1gInSc1	proud
do	do	k7c2	do
elektromagnetů	elektromagnet	k1gInPc2	elektromagnet
<g/>
,	,	kIx,	,
ovládajících	ovládající	k2eAgInPc2d1	ovládající
ventily	ventil	k1gInPc7	ventil
</s>
</p>
<p>
<s>
mechanicko-elektrická	mechanickolektrický	k2eAgFnSc1d1	mechanicko-elektrický
–	–	k?	–
kombinace	kombinace	k1gFnSc1	kombinace
obojího	obojí	k4xRgMnSc2	obojí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrací	hrací	k2eAgInSc4d1	hrací
stůl	stůl	k1gInSc4	stůl
===	===	k?	===
</s>
</p>
<p>
<s>
Hrací	hrací	k2eAgInSc1d1	hrací
stůl	stůl	k1gInSc1	stůl
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
varhaníkovi	varhaník	k1gMnSc3	varhaník
ovládat	ovládat	k5eAaImF	ovládat
všechny	všechen	k3xTgInPc1	všechen
varhanní	varhanní	k2eAgInPc1d1	varhanní
stroje	stroj	k1gInPc1	stroj
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
varhanní	varhanní	k2eAgInSc1d1	varhanní
stroj	stroj	k1gInSc1	stroj
ovládán	ovládat	k5eAaImNgInS	ovládat
jedním	jeden	k4xCgInSc7	jeden
manuálem	manuál	k1gInSc7	manuál
<g/>
,	,	kIx,	,
sestávajícím	sestávající	k2eAgFnPc3d1	sestávající
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
tastaturou	tastatura	k1gFnSc7	tastatura
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
klaviaturou	klaviatura	k1gFnSc7	klaviatura
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
klavírem	klavír	k1gInSc7	klavír
<g/>
,	,	kIx,	,
podobným	podobný	k2eAgInSc7d1	podobný
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
známe	znát	k5eAaImIp1nP	znát
např.	např.	kA	např.
u	u	k7c2	u
harmonia	harmonium	k1gNnSc2	harmonium
nebo	nebo	k8xC	nebo
u	u	k7c2	u
fortepiana	fortepiano	k1gNnSc2	fortepiano
<g/>
;	;	kIx,	;
basový	basový	k2eAgInSc1d1	basový
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
ovládán	ovládat	k5eAaImNgInS	ovládat
pedálem	pedál	k1gInSc7	pedál
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
pedálnicí	pedálnice	k1gFnSc7	pedálnice
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc7d1	sestávající
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
pedálových	pedálový	k2eAgFnPc2d1	pedálová
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hry	hra	k1gFnSc2	hra
může	moct	k5eAaImIp3nS	moct
varhaník	varhaník	k1gMnSc1	varhaník
využívat	využívat	k5eAaPmF	využívat
různé	různý	k2eAgInPc4d1	různý
manuály	manuál	k1gInPc4	manuál
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
měnit	měnit	k5eAaImF	měnit
charakter	charakter	k1gInSc4	charakter
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
manuálů	manuál	k1gInPc2	manuál
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
hracím	hrací	k2eAgInSc6d1	hrací
stole	stol	k1gInSc6	stol
umístěna	umístit	k5eAaPmNgFnS	umístit
rejstříková	rejstříkový	k2eAgFnSc1d1	rejstříková
táhla	táhlo	k1gNnSc2	táhlo
(	(	kIx(	(
<g/>
manubria	manubrium	k1gNnSc2	manubrium
<g/>
)	)	kIx)	)
či	či	k8xC	či
klapky	klapka	k1gFnSc2	klapka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
i	i	k9	i
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
zapínat	zapínat	k5eAaImF	zapínat
a	a	k8xC	a
vypínat	vypínat	k5eAaImF	vypínat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
rejstříky	rejstřík	k1gInPc4	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
komplikovaných	komplikovaný	k2eAgFnPc2d1	komplikovaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
časté	častý	k2eAgNnSc4d1	časté
rejstříkování	rejstříkování	k1gNnSc4	rejstříkování
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
varhaníkovi	varhaník	k1gMnSc3	varhaník
může	moct	k5eAaImIp3nS	moct
vypomáhat	vypomáhat	k5eAaImF	vypomáhat
druhá	druhý	k4xOgFnSc1	druhý
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
registrátor	registrátor	k1gMnSc1	registrátor
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
vhodných	vhodný	k2eAgInPc2d1	vhodný
rejstříků	rejstřík	k1gInPc2	rejstřík
je	být	k5eAaImIp3nS	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
či	či	k8xC	či
ona	onen	k3xDgFnSc1	onen
varhanní	varhanní	k2eAgFnSc1d1	varhanní
skladba	skladba	k1gFnSc1	skladba
interpretována	interpretován	k2eAgFnSc1d1	interpretována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
autor	autor	k1gMnSc1	autor
varhanní	varhanní	k2eAgFnSc2d1	varhanní
skladby	skladba	k1gFnSc2	skladba
použití	použití	k1gNnSc2	použití
vhodných	vhodný	k2eAgInPc2d1	vhodný
rejstříků	rejstřík	k1gInPc2	rejstřík
předepíše	předepsat	k5eAaPmIp3nS	předepsat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nS	ležet
volba	volba	k1gFnSc1	volba
registrace	registrace	k1gFnSc1	registrace
na	na	k7c6	na
interpretovi	interpret	k1gMnSc6	interpret
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
volit	volit	k5eAaImF	volit
rejstříky	rejstřík	k1gInPc4	rejstřík
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
rejstříky	rejstřík	k1gInPc4	rejstřík
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
kterých	který	k3yQgFnPc2	který
varhan	varhany	k1gFnPc2	varhany
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroj	zdroj	k1gInSc1	zdroj
vzduchu	vzduch	k1gInSc2	vzduch
===	===	k?	===
</s>
</p>
<p>
<s>
Stlačenému	stlačený	k2eAgInSc3d1	stlačený
vzduchu	vzduch	k1gInSc3	vzduch
<g/>
,	,	kIx,	,
určenému	určený	k2eAgMnSc3d1	určený
k	k	k7c3	k
rozeznívání	rozeznívání	k1gNnSc3	rozeznívání
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
pneumatické	pneumatický	k2eAgFnSc2d1	pneumatická
traktury	traktura	k1gFnSc2	traktura
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
varhanářské	varhanářský	k2eAgFnSc6d1	varhanářská
terminologii	terminologie	k1gFnSc6	terminologie
říká	říkat	k5eAaImIp3nS	říkat
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měchy	měch	k1gInPc1	měch
–	–	k?	–
tradiční	tradiční	k2eAgInSc4d1	tradiční
zdroj	zdroj	k1gInSc4	zdroj
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
poháněný	poháněný	k2eAgInSc4d1	poháněný
kalkantem	kalkantem	k?	kalkantem
(	(	kIx(	(
<g/>
měchy	měch	k1gInPc1	měch
jsou	být	k5eAaImIp3nP	být
poháněny	pohánět	k5eAaImNgInP	pohánět
ručním	ruční	k2eAgInSc7d1	ruční
či	či	k8xC	či
nožním	nožní	k2eAgInSc7d1	nožní
pohonem	pohon	k1gInSc7	pohon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ventilátor	ventilátor	k1gInSc1	ventilátor
–	–	k?	–
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používaný	používaný	k2eAgInSc4d1	používaný
zdroj	zdroj	k1gInSc4	zdroj
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
poháněný	poháněný	k2eAgInSc1d1	poháněný
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
,	,	kIx,	,
s	s	k7c7	s
regulátorem	regulátor	k1gInSc7	regulátor
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
zdroj	zdroj	k1gInSc1	zdroj
vzduchu	vzduch	k1gInSc2	vzduch
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stavbu	stavba	k1gFnSc4	stavba
větších	veliký	k2eAgFnPc2d2	veliký
varhan	varhany	k1gFnPc2	varhany
-	-	kIx~	-
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
největších	veliký	k2eAgFnPc2d3	veliký
by	by	kYmCp3nS	by
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
stovky	stovka	k1gFnPc4	stovka
kalkantů	kalkantů	k?	kalkantů
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
stlačený	stlačený	k2eAgInSc1d1	stlačený
vzduch	vzduch	k1gInSc1	vzduch
vede	vést	k5eAaImIp3nS	vést
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
zásobního	zásobní	k2eAgInSc2d1	zásobní
měchu	měch	k1gInSc2	měch
(	(	kIx(	(
<g/>
hlavního	hlavní	k2eAgInSc2d1	hlavní
měchu	měch	k1gInSc2	měch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
rozváděn	rozvádět	k5eAaImNgInS	rozvádět
vzduchovými	vzduchový	k2eAgInPc7d1	vzduchový
kanály	kanál	k1gInPc7	kanál
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jiných	jiný	k2eAgInPc6d1	jiný
tlacích	tlak	k1gInPc6	tlak
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
manuálech	manuál	k1gInPc6	manuál
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ještě	ještě	k9	ještě
"	"	kIx"	"
<g/>
pomocné	pomocný	k2eAgInPc1d1	pomocný
měchy	měch	k1gInPc1	měch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Používaný	používaný	k2eAgInSc1d1	používaný
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
50	[number]	k4	50
mm	mm	kA	mm
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
až	až	k6eAd1	až
do	do	k7c2	do
2500	[number]	k4	2500
mm	mm	kA	mm
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tónový	tónový	k2eAgInSc4d1	tónový
rozsah	rozsah	k1gInSc4	rozsah
==	==	k?	==
</s>
</p>
<p>
<s>
Tónový	tónový	k2eAgInSc1d1	tónový
rozsah	rozsah	k1gInSc1	rozsah
varhan	varhany	k1gFnPc2	varhany
<g/>
,	,	kIx,	,
nahlížený	nahlížený	k2eAgInSc1d1	nahlížený
podle	podle	k7c2	podle
rozsahu	rozsah	k1gInSc2	rozsah
manuálu	manuál	k1gInSc2	manuál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
klávesovými	klávesový	k2eAgInPc7d1	klávesový
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
klavírem	klavír	k1gInSc7	klavír
<g/>
,	,	kIx,	,
čembalem	čembal	k1gInSc7	čembal
nebo	nebo	k8xC	nebo
harmoniem	harmonium	k1gNnSc7	harmonium
<g/>
)	)	kIx)	)
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zpravidla	zpravidla	k6eAd1	zpravidla
obnáší	obnášet	k5eAaImIp3nP	obnášet
čtyři	čtyři	k4xCgInPc1	čtyři
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
oktávy	oktáva	k1gFnSc2	oktáva
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
varhany	varhany	k1gInPc1	varhany
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
sedmioktávový	sedmioktávový	k2eAgInSc4d1	sedmioktávový
manuál	manuál	k1gInSc4	manuál
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
od	od	k7c2	od
velkého	velký	k2eAgNnSc2d1	velké
C	C	kA	C
do	do	k7c2	do
tříčárkovaného	tříčárkovaný	k2eAgInSc2d1	tříčárkovaný
c	c	k0	c
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tříčárkovaného	tříčárkovaný	k2eAgMnSc2d1	tříčárkovaný
d	d	k?	d
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
až	až	k9	až
čtyřčárkovaného	čtyřčárkovaný	k2eAgMnSc2d1	čtyřčárkovaný
c.	c.	k?	c.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
rozsah	rozsah	k1gInSc1	rozsah
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
rejstříku	rejstřík	k1gInSc2	rejstřík
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
varhany	varhany	k1gFnPc1	varhany
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
rejstříky	rejstřík	k1gInPc4	rejstřík
od	od	k7c2	od
64	[number]	k4	64
stop	stopa	k1gFnPc2	stopa
až	až	k9	až
po	po	k7c4	po
1	[number]	k4	1
stopu	stopa	k1gFnSc4	stopa
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gInSc4	jejich
celkový	celkový	k2eAgInSc4d1	celkový
tónový	tónový	k2eAgInSc4d1	tónový
rozsah	rozsah	k1gInSc4	rozsah
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
oktáv	oktáva	k1gFnPc2	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
varhany	varhany	k1gFnPc1	varhany
jsou	být	k5eAaImIp3nP	být
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
tónový	tónový	k2eAgInSc1d1	tónový
rozsah	rozsah	k1gInSc1	rozsah
(	(	kIx(	(
<g/>
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
elektronické	elektronický	k2eAgInPc1d1	elektronický
zdroje	zdroj	k1gInPc1	zdroj
zvuku	zvuk	k1gInSc2	zvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Varhanám	varhany	k1gFnPc3	varhany
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
královský	královský	k2eAgInSc4d1	královský
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
opravdu	opravdu	k6eAd1	opravdu
král	král	k1gMnSc1	král
mezi	mezi	k7c7	mezi
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nej	Nej	k1gFnSc6	Nej
<g/>
...	...	k?	...
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Největší	veliký	k2eAgFnPc4d3	veliký
varhany	varhany	k1gFnPc4	varhany
===	===	k?	===
</s>
</p>
<p>
<s>
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
Boardwalk	Boardwalk	k1gMnSc1	Boardwalk
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Convention	Convention	k1gInSc1	Convention
Hall	Halla	k1gFnPc2	Halla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
:	:	kIx,	:
7	[number]	k4	7
manuálů	manuál	k1gInPc2	manuál
<g/>
,	,	kIx,	,
1	[number]	k4	1
pedál	pedál	k1gInSc1	pedál
<g/>
,	,	kIx,	,
1255	[number]	k4	1255
rejstříků	rejstřík	k1gInPc2	rejstřík
<g/>
,	,	kIx,	,
33	[number]	k4	33
112	[number]	k4	112
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
píšťala	píšťala	k1gFnSc1	píšťala
měří	měřit	k5eAaImIp3nS	měřit
přes	přes	k7c4	přes
19	[number]	k4	19
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
zvaný	zvaný	k2eAgInSc1d1	zvaný
Diaphone-Dulzian	Diaphone-Dulzian	k1gInSc4	Diaphone-Dulzian
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
skutečných	skutečný	k2eAgInPc2d1	skutečný
64	[number]	k4	64
<g/>
stopých	stopý	k2eAgInPc2d1	stopý
rejstříků	rejstřík	k1gInPc2	rejstřík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
váží	vážit	k5eAaImIp3nS	vážit
1	[number]	k4	1
tunu	tuna	k1gFnSc4	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Hala	hala	k1gFnSc1	hala
má	mít	k5eAaImIp3nS	mít
41	[number]	k4	41
000	[number]	k4	000
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
píšťaly	píšťala	k1gFnPc1	píšťala
vydávají	vydávat	k5eAaPmIp3nP	vydávat
tóny	tón	k1gInPc4	tón
pod	pod	k7c4	pod
subkontraoktávovou	subkontraoktávový	k2eAgFnSc4d1	subkontraoktávový
polohu	poloha	k1gFnSc4	poloha
s	s	k7c7	s
osmi	osm	k4xCc7	osm
kmity	kmit	k1gInPc7	kmit
za	za	k7c2	za
sekundu	sekund	k1gInSc2	sekund
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc3d1	jiná
zase	zase	k9	zase
vydávají	vydávat	k5eAaPmIp3nP	vydávat
zvuk	zvuk	k1gInSc4	zvuk
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
130	[number]	k4	130
dB	db	kA	db
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
1	[number]	k4	1
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
Grand	grand	k1gMnSc1	grand
Ophicleide	Ophicleid	k1gInSc5	Ophicleid
-	-	kIx~	-
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
100	[number]	k4	100
<g/>
''	''	k?	''
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
m	m	kA	m
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
-	-	kIx~	-
vysokotlaké	vysokotlaký	k2eAgInPc1d1	vysokotlaký
rejstříky	rejstřík	k1gInPc1	rejstřík
většinou	většinou	k6eAd1	většinou
nepoužívají	používat	k5eNaImIp3nP	používat
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
větší	veliký	k2eAgFnSc3d2	veliký
než	než	k8xS	než
35	[number]	k4	35
cm	cm	kA	cm
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tyto	tento	k3xDgInPc4	tento
varhany	varhany	k1gInPc4	varhany
činí	činit	k5eAaImIp3nP	činit
nejhlasitějším	hlasitý	k2eAgInSc7d3	nejhlasitější
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
největším	veliký	k2eAgInSc7d3	veliký
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kdy	kdy	k6eAd1	kdy
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gInPc1	varhany
byly	být	k5eAaImAgInP	být
ovšem	ovšem	k9	ovšem
poškozeny	poškozen	k2eAgInPc1d1	poškozen
různými	různý	k2eAgFnPc7d1	různá
událostmi	událost	k1gFnPc7	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hurikán	hurikán	k1gInSc1	hurikán
nebo	nebo	k8xC	nebo
nepozornost	nepozornost	k1gFnSc1	nepozornost
dělníků	dělník	k1gMnPc2	dělník
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
asi	asi	k9	asi
jen	jen	k9	jen
30	[number]	k4	30
%	%	kIx~	%
varhan	varhany	k1gFnPc2	varhany
funkčních	funkční	k2eAgFnPc2d1	funkční
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
komor	komora	k1gFnPc2	komora
-	-	kIx~	-
Right	Right	k2eAgInSc1d1	Right
Stage	Stage	k1gInSc1	Stage
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
plně	plně	k6eAd1	plně
provozuschopné	provozuschopný	k2eAgNnSc1d1	provozuschopné
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Wanamaker	Wanamaker	k1gInSc1	Wanamaker
Grand	grand	k1gMnSc1	grand
Court	Court	k1gInSc1	Court
Organ	organon	k1gNnPc2	organon
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
:	:	kIx,	:
6	[number]	k4	6
manuálů	manuál	k1gInPc2	manuál
<g/>
,	,	kIx,	,
1	[number]	k4	1
pedál	pedál	k1gInSc1	pedál
<g/>
,	,	kIx,	,
28	[number]	k4	28
604	[number]	k4	604
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
největší	veliký	k2eAgFnPc1d3	veliký
chrámové	chrámový	k2eAgFnPc1d1	chrámová
varhany	varhany	k1gFnPc1	varhany
-	-	kIx~	-
Passau	Passaa	k1gFnSc4	Passaa
<g/>
,	,	kIx,	,
dóm	dóm	k1gInSc4	dóm
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
:	:	kIx,	:
5	[number]	k4	5
manuálů	manuál	k1gInPc2	manuál
<g/>
,	,	kIx,	,
233	[number]	k4	233
rejstříků	rejstřík	k1gInPc2	rejstřík
<g/>
,	,	kIx,	,
17	[number]	k4	17
974	[number]	k4	974
píšťal	píšťala	k1gFnPc2	píšťala
</s>
</p>
<p>
<s>
====	====	k?	====
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
o	o	k7c4	o
post	post	k1gInSc4	post
největších	veliký	k2eAgFnPc2d3	veliký
varhan	varhany	k1gFnPc2	varhany
dělí	dělit	k5eAaImIp3nP	dělit
dva	dva	k4xCgInPc4	dva
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
píšťal	píšťala	k1gFnPc2	píšťala
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
nástroj	nástroj	k1gInSc1	nástroj
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
staršího	starý	k2eAgMnSc2d2	starší
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
se	s	k7c7	s
4	[number]	k4	4
manuály	manuál	k1gInPc7	manuál
<g/>
,	,	kIx,	,
1	[number]	k4	1
pedál	pedál	k1gInSc1	pedál
<g/>
,	,	kIx,	,
91	[number]	k4	91
rejstříkem	rejstřík	k1gInSc7	rejstřík
(	(	kIx(	(
<g/>
95	[number]	k4	95
při	při	k7c6	při
započtení	započtení	k1gNnSc6	započtení
transmisí	transmise	k1gFnPc2	transmise
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
277	[number]	k4	277
píšťalami	píšťala	k1gFnPc7	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
rejstříků	rejstřík	k1gInPc2	rejstřík
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
největší	veliký	k2eAgFnPc4d3	veliký
považovány	považován	k2eAgFnPc4d1	považována
varhany	varhany	k1gFnPc4	varhany
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Mořice	Mořic	k1gMnSc2	Mořic
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
5	[number]	k4	5
manuály	manuál	k1gInPc7	manuál
<g/>
,	,	kIx,	,
1	[number]	k4	1
pedálem	pedál	k1gInSc7	pedál
<g/>
,	,	kIx,	,
95	[number]	k4	95
rejstříky	rejstřík	k1gInPc1	rejstřík
a	a	k8xC	a
8	[number]	k4	8
054	[number]	k4	054
píšťalami	píšťala	k1gFnPc7	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Některými	některý	k3yIgInPc7	některý
zdroji	zdroj	k1gInPc7	zdroj
uváděný	uváděný	k2eAgInSc4d1	uváděný
údaj	údaj	k1gInSc4	údaj
135	[number]	k4	135
rejstříků	rejstřík	k1gInPc2	rejstřík
a	a	k8xC	a
10	[number]	k4	10
400	[number]	k4	400
píšťal	píšťala	k1gFnPc2	píšťala
nelze	lze	k6eNd1	lze
srovnávat	srovnávat	k5eAaImF	srovnávat
se	s	k7c7	s
specifikacemi	specifikace	k1gFnPc7	specifikace
ostatních	ostatní	k2eAgMnPc2d1	ostatní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
znějících	znějící	k2eAgInPc2d1	znějící
rejstříků	rejstřík	k1gInPc2	rejstřík
číslovány	číslován	k2eAgFnPc1d1	číslována
i	i	k8xC	i
spojky	spojka	k1gFnPc1	spojka
(	(	kIx(	(
<g/>
ovládací	ovládací	k2eAgInPc1d1	ovládací
prvky	prvek	k1gInPc1	prvek
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
píšťal	píšťala	k1gFnPc2	píšťala
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zvuková	zvukový	k2eAgNnPc4d1	zvukové
centra	centrum	k1gNnPc4	centrum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejstarší	starý	k2eAgInPc1d3	nejstarší
varhany	varhany	k1gInPc1	varhany
===	===	k?	===
</s>
</p>
<p>
<s>
dochované	dochovaný	k2eAgInPc1d1	dochovaný
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
funkční	funkční	k2eAgNnPc1d1	funkční
<g/>
:	:	kIx,	:
v	v	k7c6	v
Sionu	Sion	k1gInSc6	Sion
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Notre-Dame-de-Valè	Notre-Damee-Valè	k1gFnSc2	Notre-Dame-de-Valè
<g/>
:	:	kIx,	:
1	[number]	k4	1
manuál	manuál	k1gInSc1	manuál
<g/>
,	,	kIx,	,
1	[number]	k4	1
pedál	pedál	k1gInSc1	pedál
<g/>
,	,	kIx,	,
8	[number]	k4	8
rejstříků	rejstřík	k1gInPc2	rejstřík
(	(	kIx(	(
<g/>
postaveny	postavit	k5eAaPmNgInP	postavit
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
funkční	funkční	k2eAgNnSc1d1	funkční
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
:	:	kIx,	:
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
ve	v	k7c6	v
Smečně	Smečně	k1gFnSc1	Smečně
(	(	kIx(	(
<g/>
postaveny	postavit	k5eAaPmNgInP	postavit
1587	[number]	k4	1587
<g/>
,	,	kIx,	,
přestavěny	přestavěn	k2eAgFnPc1d1	přestavěna
1621	[number]	k4	1621
<g/>
,	,	kIx,	,
zrekonstruovány	zrekonstruovat	k5eAaPmNgInP	zrekonstruovat
1999	[number]	k4	1999
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
chrám	chrám	k1gInSc1	chrám
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
před	před	k7c7	před
Týnem	Týn	k1gInSc7	Týn
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
<g/>
:	:	kIx,	:
2	[number]	k4	2
manuály	manuál	k1gInPc7	manuál
</s>
</p>
<p>
<s>
===	===	k?	===
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgFnPc1d3	veliký
dosud	dosud	k6eAd1	dosud
fungující	fungující	k2eAgFnPc1d1	fungující
varhanářské	varhanářský	k2eAgFnPc1d1	varhanářská
firmy	firma	k1gFnPc1	firma
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Rieger	Rieger	k1gInSc1	Rieger
–	–	k?	–
Kloss	Kloss	k1gInSc1	Kloss
<g/>
,	,	kIx,	,
Krnov	Krnov	k1gInSc1	Krnov
–	–	k?	–
od	od	k7c2	od
r.	r.	kA	r.
1873	[number]	k4	1873
</s>
</p>
<p>
<s>
Organa	organon	k1gNnPc1	organon
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
–	–	k?	–
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
(	(	kIx(	(
<g/>
Tuček	Tuček	k1gMnSc1	Tuček
&	&	k?	&
Melzer	Melzer	k1gMnSc1	Melzer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
cca	cca	kA	cca
10	[number]	k4	10
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
</s>
</p>
<p>
<s>
===	===	k?	===
Unikátní	unikátní	k2eAgFnPc1d1	unikátní
varhany	varhany	k1gFnPc1	varhany
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Plaské	Plaský	k2eAgInPc1d1	Plaský
varhany	varhany	k1gInPc1	varhany
–	–	k?	–
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgInPc1d1	barokní
varhany	varhany	k1gInPc1	varhany
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1688	[number]	k4	1688
v	v	k7c6	v
Plaském	Plaské	k1gNnSc6	Plaské
klášteřeKamenná	klášteřeKamenný	k2eAgNnPc1d1	klášteřeKamenný
divadla	divadlo	k1gNnPc1	divadlo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
stálé	stálý	k2eAgInPc4d1	stálý
divadelní	divadelní	k2eAgInPc4d1	divadelní
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Příbramské	příbramský	k2eAgNnSc1d1	příbramské
divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Janáčkovo	Janáčkův	k2eAgNnSc1d1	Janáčkovo
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Stálá	stálý	k2eAgFnSc1d1	stálá
divadelní	divadelní	k2eAgFnSc1d1	divadelní
scéna	scéna	k1gFnSc1	scéna
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SYCHRA	Sychra	k1gMnSc1	Sychra
<g/>
,	,	kIx,	,
Method	Method	k1gInSc1	Method
Lumír	Lumír	k1gInSc1	Lumír
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
varhan	varhany	k1gFnPc2	varhany
a	a	k8xC	a
varhanní	varhanní	k2eAgFnSc2d1	varhanní
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Obecná	obecný	k2eAgFnSc1d1	obecná
jednota	jednota	k1gFnSc1	jednota
cyrillská	cyrillský	k2eAgFnSc1d1	cyrillská
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
36	[number]	k4	36
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
studie	studie	k1gFnSc1	studie
sleduje	sledovat	k5eAaImIp3nS	sledovat
téma	téma	k1gNnSc4	téma
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
uplatnění	uplatnění	k1gNnSc4	uplatnění
i	i	k9	i
osobnosti	osobnost	k1gFnSc3	osobnost
varhaníků	varhaník	k1gMnPc2	varhaník
a	a	k8xC	a
skladatelů	skladatel	k1gMnPc2	skladatel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
poznámkovým	poznámkový	k2eAgInSc7d1	poznámkový
aparátem	aparát	k1gInSc7	aparát
<g/>
..	..	k?	..
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
KOUKAL	Koukal	k1gMnSc1	Koukal
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
rozladěné	rozladěný	k2eAgInPc1d1	rozladěný
varhany	varhany	k1gInPc1	varhany
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
hudebního	hudební	k2eAgNnSc2d1	hudební
ladění	ladění	k1gNnSc2	ladění
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
NPÚ	NPÚ	kA	NPÚ
<g/>
,	,	kIx,	,
Telč	Telč	k1gFnSc1	Telč
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
176	[number]	k4	176
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905631	[number]	k4	905631
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
recenze	recenze	k1gFnPc1	recenze
knihy	kniha	k1gFnSc2	kniha
on-line	onin	k1gInSc5	on-lin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
varhaníků	varhaník	k1gMnPc2	varhaník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
varhanních	varhanní	k2eAgInPc2d1	varhanní
festivalů	festival	k1gInPc2	festival
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
píšťalové	píšťalový	k2eAgInPc1d1	píšťalový
varhany	varhany	k1gInPc1	varhany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
varhany	varhany	k1gFnPc1	varhany
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
varhany	varhany	k1gInPc1	varhany
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Petr	Petr	k1gMnSc1	Petr
Bernat	Bernat	k1gMnSc1	Bernat
<g/>
:	:	kIx,	:
Anatomie	anatomie	k1gFnSc1	anatomie
varhan	varhany	k1gFnPc2	varhany
</s>
</p>
<p>
<s>
ITEM	ITEM	kA	ITEM
<g/>
:	:	kIx,	:
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
varhan	varhany	k1gFnPc2	varhany
</s>
</p>
<p>
<s>
Varhany	varhany	k1gInPc1	varhany
na	na	k7c4	na
Varhaníci	varhaník	k1gMnPc5	varhaník
online	onlin	k1gInSc5	onlin
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
:	:	kIx,	:
Přehled	přehled	k1gInSc1	přehled
varham	varham	k1gInSc1	varham
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
:	:	kIx,	:
Varhanní	varhanní	k2eAgInPc4d1	varhanní
články	článek	k1gInPc4	článek
</s>
</p>
<p>
<s>
Moravští	moravský	k2eAgMnPc1d1	moravský
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
varhaníci	varhaník	k1gMnPc1	varhaník
<g/>
:	:	kIx,	:
varhaníci	varhaník	k1gMnPc1	varhaník
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
:	:	kIx,	:
Varhany	varhany	k1gFnPc1	varhany
a	a	k8xC	a
varhanáři	varhanář	k1gMnPc1	varhanář
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgFnSc1d1	varhanní
Hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
nahrávky	nahrávka	k1gFnSc2	nahrávka
mp	mp	k?	mp
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fórum	fórum	k1gNnSc1	fórum
pro	pro	k7c4	pro
varhaníky	varhaník	k1gMnPc4	varhaník
<g/>
:	:	kIx,	:
forum	forum	k1gNnSc1	forum
<g/>
.	.	kIx.	.
<g/>
organistae	organistae	k1gFnSc1	organistae
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Varhany	varhany	k1gFnPc1	varhany
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
