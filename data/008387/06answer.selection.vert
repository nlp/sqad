<s>
Varhany	varhany	k1gFnPc1	varhany
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnPc1d1	označovaná
za	za	k7c4	za
"	"	kIx"	"
<g/>
královský	královský	k2eAgInSc4d1	královský
nástroj	nástroj	k1gInSc4	nástroj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnPc1d3	veliký
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
nejsložitější	složitý	k2eAgInSc1d3	nejsložitější
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jsou	být	k5eAaImIp3nP	být
strojem	stroj	k1gInSc7	stroj
užívaným	užívaný	k2eAgInSc7d1	užívaný
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
(	(	kIx(	(
<g/>
varhanní	varhanní	k2eAgInSc1d1	varhanní
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
