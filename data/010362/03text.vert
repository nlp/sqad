<p>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgMnSc1d1	literární
hrdina	hrdina	k1gMnSc1	hrdina
z	z	k7c2	z
knižní	knižní	k2eAgFnSc2d1	knižní
trilogie	trilogie	k1gFnSc2	trilogie
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
Petera	Peter	k1gMnSc2	Peter
Jacksona	Jackson	k1gMnSc2	Jackson
jej	on	k3xPp3gNnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Viggo	Viggo	k6eAd1	Viggo
Mortensen	Mortensen	k2eAgMnSc1d1	Mortensen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2931	[number]	k4	2931
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Arathorna	Arathorno	k1gNnSc2	Arathorno
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Gilraen	Gilraen	k1gInSc4	Gilraen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
rodičích	rodič	k1gMnPc6	rodič
potomkem	potomek	k1gMnSc7	potomek
králů	král	k1gMnPc2	král
Isildura	Isildur	k1gMnSc2	Isildur
a	a	k8xC	a
Elendila	Elendil	k1gMnSc2	Elendil
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
pak	pak	k6eAd1	pak
prvního	první	k4xOgMnSc4	první
krále	král	k1gMnSc4	král
Númenoru	Númenor	k1gInSc2	Númenor
Elrose	Elrosa	k1gFnSc6	Elrosa
Tar-Minyatura	Tar-Minyatura	k1gFnSc1	Tar-Minyatura
a	a	k8xC	a
skrze	skrze	k?	skrze
něj	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
i	i	k9	i
všech	všecek	k3xTgInPc2	všecek
Tří	tři	k4xCgInPc2	tři
vládnoucích	vládnoucí	k2eAgInPc2d1	vládnoucí
rodů	rod	k1gInPc2	rod
Edain	Edaina	k1gFnPc2	Edaina
i	i	k8xC	i
Eldar	Eldara	k1gFnPc2	Eldara
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ženy	žena	k1gFnSc2	žena
krále	král	k1gMnSc2	král
Thingola	Thingola	k1gFnSc1	Thingola
Melian	Melian	k1gMnSc1	Melian
i	i	k8xC	i
božského	božský	k2eAgInSc2d1	božský
řádu	řád	k1gInSc2	řád
Maiar	Maiara	k1gFnPc2	Maiara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aragornův	Aragornův	k2eAgMnSc1d1	Aragornův
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
chlapci	chlapec	k1gMnSc6	chlapec
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gInSc1	Aragorn
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc4d1	nazván
Estel	Estel	k1gInSc4	Estel
(	(	kIx(	(
<g/>
sindarský	sindarský	k2eAgInSc4d1	sindarský
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
naději	naděje	k1gFnSc4	naděje
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
2951	[number]	k4	2951
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mu	on	k3xPp3gMnSc3	on
Elrond	Elrond	k1gInSc1	Elrond
prozradil	prozradit	k5eAaPmAgInS	prozradit
jeho	jeho	k3xOp3gFnSc4	jeho
pravou	pravý	k2eAgFnSc4d1	pravá
totožnost	totožnost	k1gFnSc4	totožnost
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
mu	on	k3xPp3gMnSc3	on
úlomky	úlomek	k1gInPc7	úlomek
Narsilu	Narsil	k1gInSc2	Narsil
a	a	k8xC	a
Barahirův	Barahirův	k2eAgInSc4d1	Barahirův
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Žezlo	žezlo	k1gNnSc4	žezlo
Annúmiasu	Annúmias	k1gInSc2	Annúmias
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedospěje	dochvít	k5eNaPmIp3nS	dochvít
k	k	k7c3	k
právu	právo	k1gNnSc3	právo
držet	držet	k5eAaImF	držet
je	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Elrondovy	Elrondův	k2eAgFnSc2d1	Elrondova
dcery	dcera	k1gFnSc2	dcera
Arwen	Arwna	k1gFnPc2	Arwna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgInS	žít
Aragorn	Aragorn	k1gInSc1	Aragorn
s	s	k7c7	s
Hraničáři	hraničář	k1gMnPc7	hraničář
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
a	a	k8xC	a
střežil	střežit	k5eAaImAgMnS	střežit
hranice	hranice	k1gFnPc4	hranice
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
mír	mír	k1gInSc4	mír
v	v	k7c6	v
Eriadoru	Eriador	k1gInSc6	Eriador
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2956	[number]	k4	2956
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Gandalfem	Gandalf	k1gInSc7	Gandalf
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
svou	svůj	k3xOyFgFnSc4	svůj
identitu	identita	k1gFnSc4	identita
tajil	tajit	k5eAaImAgMnS	tajit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nedoslechli	doslechnout	k5eNaPmAgMnP	doslechnout
sluhové	sluha	k1gMnPc1	sluha
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
v	v	k7c6	v
Eriadoru	Eriador	k1gInSc6	Eriador
záhadnou	záhadný	k2eAgFnSc4d1	záhadná
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
v	v	k7c6	v
Hůrce	hůrka	k1gFnSc6	hůrka
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
přezdívku	přezdívka	k1gFnSc4	přezdívka
Chodec	chodec	k1gMnSc1	chodec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hraničář	hraničář	k1gMnSc1	hraničář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
2957	[number]	k4	2957
<g/>
–	–	k?	–
<g/>
2980	[number]	k4	2980
Aragorn	Aragorn	k1gInSc1	Aragorn
podnikal	podnikat	k5eAaImAgInS	podnikat
výpravy	výprava	k1gFnPc4	výprava
za	za	k7c7	za
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
pod	pod	k7c7	pod
praporem	prapor	k1gInSc7	prapor
Thengela	Thengela	k1gFnSc1	Thengela
Rohanského	Rohanský	k2eAgInSc2d1	Rohanský
a	a	k8xC	a
Ectheliona	Ecthelion	k1gMnSc2	Ecthelion
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
gondorského	gondorský	k2eAgMnSc4d1	gondorský
správce	správce	k1gMnSc4	správce
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
tehdy	tehdy	k6eAd1	tehdy
používal	používat	k5eAaImAgMnS	používat
pseudonym	pseudonym	k1gInSc4	pseudonym
Thorongil	Thorongil	k1gMnSc1	Thorongil
(	(	kIx(	(
<g/>
Orel	Orel	k1gMnSc1	Orel
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2980	[number]	k4	2980
s	s	k7c7	s
gondorským	gondorský	k2eAgNnSc7d1	gondorské
loďstvem	loďstvo	k1gNnSc7	loďstvo
spálil	spálit	k5eAaPmAgMnS	spálit
loďstvo	loďstvo	k1gNnSc4	loďstvo
Umbaru	Umbar	k1gInSc2	Umbar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
Gondor	Gondor	k1gMnSc1	Gondor
opustil	opustit	k5eAaPmAgMnS	opustit
-	-	kIx~	-
k	k	k7c3	k
lítosti	lítost	k1gFnSc3	lítost
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nikoliv	nikoliv	k9	nikoliv
Denethora	Denethora	k1gFnSc1	Denethora
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
konkurenci	konkurence	k1gFnSc4	konkurence
v	v	k7c6	v
přízni	přízeň	k1gFnSc6	přízeň
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
gondorského	gondorský	k2eAgInSc2d1	gondorský
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Osaměle	osaměle	k6eAd1	osaměle
putoval	putovat	k5eAaImAgInS	putovat
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
a	a	k8xC	a
daleký	daleký	k2eAgInSc4d1	daleký
jih	jih	k1gInSc4	jih
a	a	k8xC	a
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
lidská	lidský	k2eAgNnPc4d1	lidské
srdce	srdce	k1gNnPc4	srdce
a	a	k8xC	a
Sauronovy	Sauronův	k2eAgMnPc4d1	Sauronův
služebníky	služebník	k1gMnPc4	služebník
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
největším	veliký	k2eAgMnSc7d3	veliký
cestovatelem	cestovatel	k1gMnSc7	cestovatel
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
Středozemě	Středozem	k1gFnSc2	Středozem
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gMnSc1	Aragorn
znovu	znovu	k6eAd1	znovu
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Arwen	Arwen	k1gInSc4	Arwen
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
jí	jíst	k5eAaImIp3nS	jíst
dědictví	dědictví	k1gNnSc4	dědictví
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
–	–	k?	–
Barahirův	Barahirův	k2eAgInSc1d1	Barahirův
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
později	pozdě	k6eAd2	pozdě
požádal	požádat	k5eAaPmAgMnS	požádat
Elronda	Elrond	k1gMnSc4	Elrond
o	o	k7c4	o
Arweninu	Arwenin	k2eAgFnSc4d1	Arwenin
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
Elrond	Elrond	k1gMnSc1	Elrond
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
chráněnec	chráněnec	k1gMnSc1	chráněnec
stane	stanout	k5eAaPmIp3nS	stanout
králem	král	k1gMnSc7	král
Gondoru	Gondor	k1gInSc2	Gondor
a	a	k8xC	a
Arnoru	Arnor	k1gInSc2	Arnor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
3009	[number]	k4	3009
Gandalf	Gandalf	k1gInSc4	Gandalf
a	a	k8xC	a
Aragorn	Aragorn	k1gInSc4	Aragorn
společně	společně	k6eAd1	společně
hledali	hledat	k5eAaImAgMnP	hledat
v	v	k7c6	v
Rhovanionu	Rhovanion	k1gInSc6	Rhovanion
Gluma	Glumum	k1gNnSc2	Glumum
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Aragorn	Aragorn	k1gMnSc1	Aragorn
nalezl	nalézt	k5eAaBmAgMnS	nalézt
v	v	k7c6	v
Mrtvých	mrtvý	k2eAgInPc6d1	mrtvý
močálech	močál	k1gInPc6	močál
poblíž	poblíž	k7c2	poblíž
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgMnS	přinést
jako	jako	k9	jako
vězně	vězeň	k1gMnPc4	vězeň
do	do	k7c2	do
síní	síň	k1gFnPc2	síň
elfího	elfí	k1gMnSc2	elfí
krále	král	k1gMnSc2	král
Thranduila	Thranduil	k1gMnSc2	Thranduil
v	v	k7c6	v
Temném	temný	k2eAgInSc6d1	temný
hvozdě	hvozd	k1gInSc6	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Bilbem	Bilb	k1gInSc7	Bilb
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
-	-	kIx~	-
Bilbo	Bilba	k1gFnSc5	Bilba
složil	složit	k5eAaPmAgMnS	složit
báseň	báseň	k1gFnSc4	báseň
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
jménu	jméno	k1gNnSc3	jméno
(	(	kIx(	(
<g/>
Ne	ne	k9	ne
každé	každý	k3xTgNnSc1	každý
zlato	zlato	k1gNnSc1	zlato
třpytívá	třpytívat	k5eAaImIp3nS	třpytívat
se	s	k7c7	s
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
3018	[number]	k4	3018
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gInSc1	Aragorn
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Frodově	Frodův	k2eAgFnSc3d1	Frodova
výpravě	výprava	k1gFnSc3	výprava
v	v	k7c6	v
Hůrce	hůrka	k1gFnSc6	hůrka
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
U	u	k7c2	u
Skákavého	skákavý	k2eAgMnSc2d1	skákavý
poníka	poník	k1gMnSc2	poník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Aragornově	Aragornův	k2eAgFnSc3d1	Aragornova
pomoci	pomoc	k1gFnSc3	pomoc
hobiti	hobit	k1gMnPc1	hobit
utekli	utéct	k5eAaPmAgMnP	utéct
nazgû	nazgû	k?	nazgû
a	a	k8xC	a
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
v	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
opravili	opravit	k5eAaPmAgMnP	opravit
zlomený	zlomený	k2eAgInSc4d1	zlomený
Elendilův	Elendilův	k2eAgInSc4d1	Elendilův
meč	meč	k1gInSc4	meč
Narsil	Narsil	k1gMnSc1	Narsil
a	a	k8xC	a
Aragorn	Aragorn	k1gMnSc1	Aragorn
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
nosil	nosit	k5eAaImAgInS	nosit
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Andúril	Andúrila	k1gFnPc2	Andúrila
(	(	kIx(	(
<g/>
Plamen	plamen	k1gInSc1	plamen
Západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Elrondově	Elrondův	k2eAgFnSc6d1	Elrondova
Radě	rada	k1gFnSc6	rada
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Aragorn	Aragorn	k1gMnSc1	Aragorn
členem	člen	k1gInSc7	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
směřoval	směřovat	k5eAaImAgInS	směřovat
do	do	k7c2	do
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgMnS	pomoct
svému	svůj	k3xOyFgInSc3	svůj
lidu	lid	k1gInSc3	lid
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
výpravu	výprava	k1gFnSc4	výprava
provázet	provázet	k5eAaImF	provázet
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
Gandalf	Gandalf	k1gInSc1	Gandalf
svržen	svrhnout	k5eAaPmNgInS	svrhnout
s	s	k7c7	s
balrogem	balrog	k1gInSc7	balrog
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gMnSc1	Aragorn
vůdcem	vůdce	k1gMnSc7	vůdce
Společenstva	společenstvo	k1gNnSc2	společenstvo
a	a	k8xC	a
nastalo	nastat	k5eAaPmAgNnS	nastat
mu	on	k3xPp3gMnSc3	on
těžké	těžký	k2eAgNnSc1d1	těžké
dilema	dilema	k1gNnSc1	dilema
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
provázet	provázet	k5eAaImF	provázet
Froda	Froda	k1gFnSc1	Froda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
držet	držet	k5eAaImF	držet
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odkládal	odkládat	k5eAaImAgMnS	odkládat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dlouho	dlouho	k6eAd1	dlouho
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
Společenstvo	společenstvo	k1gNnSc4	společenstvo
Lórienem	Lórien	k1gInSc7	Lórien
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
plavil	plavit	k5eAaImAgInS	plavit
až	až	k9	až
k	k	k7c3	k
Rauroskému	Rauroský	k2eAgInSc3d1	Rauroský
vodopádu	vodopád	k1gInSc3	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
mezi	mezi	k7c7	mezi
Frodem	Frodo	k1gNnSc7	Frodo
a	a	k8xC	a
Boromirem	Boromir	k1gInSc7	Boromir
a	a	k8xC	a
Společenstvo	společenstvo	k1gNnSc1	společenstvo
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
jen	jen	k9	jen
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
Samem	Sam	k1gMnSc7	Sam
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Boromir	Boromir	k1gMnSc1	Boromir
padl	padnout	k5eAaImAgMnS	padnout
a	a	k8xC	a
Smíšek	Smíšek	k1gMnSc1	Smíšek
s	s	k7c7	s
Pipinem	Pipin	k1gMnSc7	Pipin
byli	být	k5eAaImAgMnP	být
zajati	zajmout	k5eAaPmNgMnP	zajmout
Sarumanovými	Sarumanův	k2eAgMnPc7d1	Sarumanův
skřety	skřet	k1gMnPc7	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
po	po	k7c6	po
těžkém	těžký	k2eAgNnSc6d1	těžké
rozhodování	rozhodování	k1gNnSc6	rozhodování
nechal	nechat	k5eAaPmAgMnS	nechat
Froda	Froda	k1gMnSc1	Froda
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
už	už	k6eAd1	už
nemůže	moct	k5eNaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
s	s	k7c7	s
Legolasem	Legolas	k1gInSc7	Legolas
a	a	k8xC	a
Gimlim	Gimlim	k1gInSc4	Gimlim
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
skřety	skřet	k1gMnPc4	skřet
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
zajaté	zajatý	k2eAgMnPc4d1	zajatý
hobity	hobit	k1gMnPc4	hobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
jejich	jejich	k3xOp3gFnSc4	jejich
hrdinskou	hrdinský	k2eAgFnSc4d1	hrdinská
pouť	pouť	k1gFnSc4	pouť
přes	přes	k7c4	přes
Rohan	Rohan	k1gMnSc1	Rohan
však	však	k9	však
skřety	skřet	k1gMnPc4	skřet
nedostihli	dostihnout	k5eNaPmAgMnP	dostihnout
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
pobiti	pobít	k5eAaPmNgMnP	pobít
rohanskými	rohanský	k2eAgMnPc7d1	rohanský
jezdci	jezdec	k1gMnPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gMnSc1	Aragorn
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
jejich	jejich	k3xOp3gMnSc7	jejich
vůdcem	vůdce	k1gMnSc7	vůdce
Éomerem	Éomer	k1gMnSc7	Éomer
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInPc4d1	počáteční
rozpaky	rozpak	k1gInPc4	rozpak
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
rychle	rychle	k6eAd1	rychle
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
jejich	jejich	k3xOp3gInPc2	jejich
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
se	se	k3xPyFc4	se
s	s	k7c7	s
Legolasem	Legolas	k1gInSc7	Legolas
a	a	k8xC	a
Gimlim	Gimli	k1gNnSc7	Gimli
vydal	vydat	k5eAaPmAgMnS	vydat
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
hobitů	hobit	k1gMnPc2	hobit
do	do	k7c2	do
Fangornu	Fangorn	k1gInSc2	Fangorn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Gandalfem	Gandalf	k1gInSc7	Gandalf
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
pomoci	pomoct	k5eAaPmF	pomoct
rohanskému	rohanský	k2eAgMnSc3d1	rohanský
králi	král	k1gMnSc3	král
Théodenovi	Théoden	k1gMnSc3	Théoden
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
Sarumanem	Saruman	k1gInSc7	Saruman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
===	===	k?	===
</s>
</p>
<p>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
významně	významně	k6eAd1	významně
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Rohirům	Rohir	k1gMnPc3	Rohir
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Helmově	helmově	k6eAd1	helmově
žlebu	žleb	k1gInSc2	žleb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazili	porazit	k5eAaPmAgMnP	porazit
Sarumanovo	Sarumanův	k2eAgNnSc4d1	Sarumanovo
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rohanu	Rohan	k1gMnSc6	Rohan
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gMnSc1	Aragorn
seznámil	seznámit	k5eAaPmAgMnS	seznámit
také	také	k9	také
s	s	k7c7	s
Éomerovou	Éomerův	k2eAgFnSc7d1	Éomerův
sestrou	sestra	k1gFnSc7	sestra
Éowyn	Éowyna	k1gFnPc2	Éowyna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
viděla	vidět	k5eAaImAgFnS	vidět
svou	svůj	k3xOyFgFnSc4	svůj
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
slavný	slavný	k2eAgInSc4d1	slavný
a	a	k8xC	a
zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
život	život	k1gInSc4	život
a	a	k8xC	a
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Aragornovo	Aragornův	k2eAgNnSc1d1	Aragornovo
srdce	srdce	k1gNnSc1	srdce
však	však	k9	však
patřilo	patřit	k5eAaImAgNnS	patřit
Arwen	Arwen	k1gInSc4	Arwen
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
Éowyn	Éowyn	k1gMnSc1	Éowyn
cítil	cítit	k5eAaImAgMnS	cítit
lítost	lítost	k1gFnSc4	lítost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
výpravě	výprava	k1gFnSc6	výprava
do	do	k7c2	do
Železného	železný	k2eAgInSc2d1	železný
pasu	pas	k1gInSc2	pas
získal	získat	k5eAaPmAgInS	získat
orthancký	orthancký	k2eAgInSc1d1	orthancký
palantír	palantír	k1gInSc1	palantír
a	a	k8xC	a
protože	protože	k8xS	protože
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
Mordoru	Mordor	k1gInSc3	Mordor
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
odpoutat	odpoutat	k5eAaPmF	odpoutat
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
velkého	velký	k2eAgNnSc2d1	velké
rizika	riziko	k1gNnSc2	riziko
pohlédl	pohlédnout	k5eAaPmAgMnS	pohlédnout
do	do	k7c2	do
palantíru	palantír	k1gInSc2	palantír
<g/>
,	,	kIx,	,
vyrval	vyrvat	k5eAaPmAgMnS	vyrvat
jej	on	k3xPp3gInSc4	on
ze	z	k7c2	z
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
odhalil	odhalit	k5eAaPmAgMnS	odhalit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
jako	jako	k9	jako
Isildurův	Isildurův	k2eAgMnSc1d1	Isildurův
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
rozeběhla	rozeběhnout	k5eAaPmAgFnS	rozeběhnout
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Mordoru	Mordor	k1gInSc2	Mordor
vůči	vůči	k7c3	vůči
Minas	Minas	k1gInSc4	Minas
Tirith	Tiritha	k1gFnPc2	Tiritha
a	a	k8xC	a
Aragorn	Aragorna	k1gFnPc2	Aragorna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránil	zachránit	k5eAaPmAgInS	zachránit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
Stezkami	stezka	k1gFnPc7	stezka
mrtvých	mrtvý	k1gMnPc2	mrtvý
ke	k	k7c3	k
Kameni	kámen	k1gInSc3	kámen
Erech	Erecha	k1gFnPc2	Erecha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
armáda	armáda	k1gFnSc1	armáda
Mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
znovu	znovu	k6eAd1	znovu
jako	jako	k9	jako
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
lety	léto	k1gNnPc7	léto
porazil	porazit	k5eAaPmAgMnS	porazit
umbarské	umbarský	k2eAgMnPc4d1	umbarský
korzáry	korzár	k1gMnPc4	korzár
poblíž	poblíž	k7c2	poblíž
Pelargiru	Pelargir	k1gInSc2	Pelargir
a	a	k8xC	a
s	s	k7c7	s
gondorským	gondorský	k2eAgNnSc7d1	gondorské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
pomocí	pomoc	k1gFnSc7	pomoc
Rohirů	Rohir	k1gMnPc2	Rohir
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dále	daleko	k6eAd2	daleko
odpoutávat	odpoutávat	k5eAaImF	odpoutávat
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
gondorského	gondorský	k2eAgNnSc2d1	gondorské
vojska	vojsko	k1gNnSc2	vojsko
proti	proti	k7c3	proti
Mordoru	Mordor	k1gInSc3	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gFnPc7	jeho
branami	brána	k1gFnPc7	brána
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
beznadějná	beznadějný	k2eAgFnSc1d1	beznadějná
bitva	bitva	k1gFnSc1	bitva
proti	proti	k7c3	proti
obrovské	obrovský	k2eAgFnSc3d1	obrovská
převaze	převaha	k1gFnSc3	převaha
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
však	však	k9	však
Frodo	Frodo	k1gNnSc4	Frodo
dokončil	dokončit	k5eAaPmAgMnS	dokončit
své	svůj	k3xOyFgNnSc4	svůj
poslání	poslání	k1gNnSc4	poslání
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgInS	zničit
Prsten	prsten	k1gInSc1	prsten
a	a	k8xC	a
mordorská	mordorský	k2eAgFnSc1d1	mordorská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
rozprchla	rozprchnout	k5eAaPmAgFnS	rozprchnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
Krále	Král	k1gMnSc2	Král
===	===	k?	===
</s>
</p>
<p>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
3019	[number]	k4	3019
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
korunován	korunován	k2eAgMnSc1d1	korunován
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
Elessar	Elessar	k1gMnSc1	Elessar
Telcontar	Telcontar	k1gMnSc1	Telcontar
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
Arnoru	Arnor	k1gInSc2	Arnor
a	a	k8xC	a
35	[number]	k4	35
<g/>
.	.	kIx.	.
králem	král	k1gMnSc7	král
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Arwen	Arwen	k1gInSc1	Arwen
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Eldariona	Eldarion	k1gMnSc4	Eldarion
a	a	k8xC	a
několik	několik	k4yIc4	několik
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Elessar	Elessara	k1gFnPc2	Elessara
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Elfkam	Elfkam	k1gInSc1	Elfkam
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
klenotu	klenot	k1gInSc2	klenot
s	s	k7c7	s
ozdravnou	ozdravný	k2eAgFnSc7d1	ozdravná
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
darovala	darovat	k5eAaPmAgFnS	darovat
Galadriel	Galadriel	k1gInSc4	Galadriel
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
Lórienu	Lórien	k1gInSc2	Lórien
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
ruce	ruka	k1gFnPc4	ruka
uzdravitele	uzdravitel	k1gMnSc2	uzdravitel
<g/>
.	.	kIx.	.
</s>
<s>
Přízvisko	přízvisko	k1gNnSc1	přízvisko
Telcontar	Telcontara	k1gFnPc2	Telcontara
je	být	k5eAaImIp3nS	být
quenijským	quenijský	k2eAgInSc7d1	quenijský
překladem	překlad	k1gInSc7	překlad
označení	označení	k1gNnSc2	označení
Chodec	chodec	k1gMnSc1	chodec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
spíše	spíše	k9	spíše
posměšně	posměšně	k6eAd1	posměšně
hůreckými	hůrecký	k2eAgInPc7d1	hůrecký
sedláky	sedlák	k1gInPc7	sedlák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
představen	představit	k5eAaPmNgInS	představit
i	i	k9	i
hobitům	hobit	k1gMnPc3	hobit
ze	z	k7c2	z
Společenstva	společenstvo	k1gNnSc2	společenstvo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
oslovení	oslovení	k1gNnSc4	oslovení
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
oslovovali	oslovovat	k5eAaImAgMnP	oslovovat
jej	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
a	a	k8xC	a
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
ještě	ještě	k6eAd1	ještě
mnoho	mnoho	k4c4	mnoho
bitev	bitva	k1gFnPc2	bitva
s	s	k7c7	s
nepřátelskými	přátelský	k2eNgFnPc7d1	nepřátelská
říšemi	říš	k1gFnPc7	říš
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přežily	přežít	k5eAaPmAgFnP	přežít
Sauronův	Sauronův	k2eAgInSc4d1	Sauronův
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
120	[number]	k4	120
Č.	Č.	kA	Č.
v.	v.	k?	v.
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
209	[number]	k4	209
letech	léto	k1gNnPc6	léto
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
postavy	postava	k1gFnSc2	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Tolkien	Tolkien	k1gInSc4	Tolkien
zpočátku	zpočátku	k6eAd1	zpočátku
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
Aragorna	Aragorno	k1gNnSc2	Aragorno
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
hobita	hobit	k1gMnSc4	hobit
-	-	kIx~	-
hraničáře	hraničář	k1gMnSc4	hraničář
jménem	jméno	k1gNnSc7	jméno
Klusák	Klusák	k1gMnSc1	Klusák
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
však	však	k9	však
postava	postava	k1gFnSc1	postava
"	"	kIx"	"
<g/>
vymkla	vymknout	k5eAaPmAgFnS	vymknout
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
zdál	zdát	k5eAaImAgInS	zdát
být	být	k5eAaImF	být
otrhaným	otrhaný	k2eAgMnSc7d1	otrhaný
tulákem	tulák	k1gMnSc7	tulák
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
náčelníkem	náčelník	k1gInSc7	náčelník
Dúnadanů	Dúnadan	k1gMnPc2	Dúnadan
Severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
Isildurovým	Isildurův	k2eAgMnSc7d1	Isildurův
dědicem	dědic	k1gMnSc7	dědic
a	a	k8xC	a
králem	král	k1gMnSc7	král
Obnoveného	obnovený	k2eAgNnSc2d1	obnovené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
popisuje	popisovat	k5eAaImIp3nS	popisovat
Aragorna	Aragorna	k1gFnSc1	Aragorna
jako	jako	k9	jako
vysokého	vysoký	k2eAgNnSc2d1	vysoké
<g/>
,	,	kIx,	,
tmavovlasého	tmavovlasý	k2eAgMnSc2d1	tmavovlasý
<g/>
,	,	kIx,	,
s	s	k7c7	s
šedýma	šedý	k2eAgNnPc7d1	šedé
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
vážnou	vážný	k2eAgFnSc7d1	vážná
tváří	tvář	k1gFnSc7	tvář
a	a	k8xC	a
bledou	bledý	k2eAgFnSc7d1	bledá
pletí	pleť	k1gFnSc7	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
statečný	statečný	k2eAgMnSc1d1	statečný
bojovník	bojovník	k1gMnSc1	bojovník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
také	také	k9	také
zběhlý	zběhlý	k2eAgInSc1d1	zběhlý
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výchově	výchova	k1gFnSc3	výchova
i	i	k9	i
v	v	k7c6	v
uzdravování	uzdravování	k1gNnSc6	uzdravování
a	a	k8xC	a
elfských	elfský	k2eAgFnPc6d1	elfská
učenostech	učenost	k1gFnPc6	učenost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
númenorského	númenorský	k2eAgInSc2d1	númenorský
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gMnSc3	on
dán	dát	k5eAaPmNgInS	dát
třikrát	třikrát	k6eAd1	třikrát
delší	dlouhý	k2eAgInSc4d2	delší
život	život	k1gInSc4	život
než	než	k8xS	než
běžným	běžný	k2eAgInSc7d1	běžný
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
postavu	postava	k1gFnSc4	postava
statečného	statečný	k2eAgMnSc4d1	statečný
rytíře	rytíř	k1gMnSc4	rytíř
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
např.	např.	kA	např.
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
-	-	kIx~	-
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nezradí	zradit	k5eNaPmIp3nS	zradit
ušlechtilé	ušlechtilý	k2eAgInPc4d1	ušlechtilý
ideály	ideál	k1gInPc4	ideál
<g/>
,	,	kIx,	,
nezradí	zradit	k5eNaPmIp3nP	zradit
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
zasvětí	zasvětit	k5eAaPmIp3nS	zasvětit
život	život	k1gInSc1	život
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
zlu	zlo	k1gNnSc3	zlo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
samotná	samotný	k2eAgFnSc1d1	samotná
osobnost	osobnost	k1gFnSc1	osobnost
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
v	v	k7c6	v
nepřátelích	nepřítel	k1gMnPc6	nepřítel
děs	děs	k1gInSc4	děs
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aragorn	Aragorn	k1gNnSc1	Aragorn
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vyprávění	vyprávění	k1gNnSc2	vyprávění
nebyl	být	k5eNaImAgInS	být
nepřáteli	nepřítel	k1gMnPc7	nepřítel
zraněn	zranit	k5eAaPmNgMnS	zranit
a	a	k8xC	a
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
podléhá	podléhat	k5eAaImIp3nS	podléhat
únavě	únava	k1gFnSc3	únava
a	a	k8xC	a
skepsi	skepse	k1gFnSc3	skepse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
Jacksona	Jackson	k1gMnSc2	Jackson
Aragorna	Aragorno	k1gNnSc2	Aragorno
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Viggo	Viggo	k6eAd1	Viggo
Mortensen	Mortensen	k2eAgMnSc1d1	Mortensen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
zpravidla	zpravidla	k6eAd1	zpravidla
drží	držet	k5eAaImIp3nS	držet
literární	literární	k2eAgFnPc4d1	literární
předlohy	předloha	k1gFnPc4	předloha
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Arwen	Arwen	k1gInSc1	Arwen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
objevuje	objevovat	k5eAaImIp3nS	objevovat
spíše	spíše	k9	spíše
okrajově	okrajově	k6eAd1	okrajově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podrobně	podrobně	k6eAd1	podrobně
je	být	k5eAaImIp3nS	být
rozebrán	rozebrán	k2eAgMnSc1d1	rozebrán
v	v	k7c6	v
Dodatcích	dodatek	k1gInPc6	dodatek
k	k	k7c3	k
Návratu	návrat	k1gInSc3	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
se	s	k7c7	s
Sarumanovými	Sarumanův	k2eAgMnPc7d1	Sarumanův
skřety	skřet	k1gMnPc7	skřet
ve	v	k7c6	v
Dvou	dva	k4xCgFnPc6	dva
věžích	věž	k1gFnPc6	věž
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vůbec	vůbec	k9	vůbec
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Aragorn	Aragorn	k1gMnSc1	Aragorn
těžce	těžce	k6eAd1	těžce
raněn	ranit	k5eAaPmNgMnS	ranit
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
knižním	knižní	k2eAgNnSc7d1	knižní
pojetím	pojetí	k1gNnSc7	pojetí
Aragorna	Aragorno	k1gNnSc2	Aragorno
jako	jako	k8xC	jako
nezlomného	zlomný	k2eNgMnSc2d1	nezlomný
bojovníka	bojovník	k1gMnSc2	bojovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
přicházejí	přicházet	k5eAaImIp3nP	přicházet
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Aragornovi	Aragorn	k1gMnSc3	Aragorn
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc1	desítka
Dúnadanů	Dúnadan	k1gMnPc2	Dúnadan
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gNnSc3	jeho
lidu	lido	k1gNnSc3	lido
z	z	k7c2	z
Eriadoru	Eriador	k1gInSc2	Eriador
<g/>
)	)	kIx)	)
a	a	k8xC	a
Elrondovi	Elrondův	k2eAgMnPc1d1	Elrondův
synové	syn	k1gMnPc1	syn
Elladan	Elladan	k1gMnSc1	Elladan
a	a	k8xC	a
Elrohir	Elrohir	k1gMnSc1	Elrohir
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
sám	sám	k3xTgMnSc1	sám
Elrond	Elrond	k1gMnSc1	Elrond
a	a	k8xC	a
donese	donést	k5eAaPmIp3nS	donést
Aragornovi	Aragorn	k1gMnSc3	Aragorn
znovu	znovu	k6eAd1	znovu
zkutý	zkutý	k2eAgInSc1d1	zkutý
meč	meč	k1gInSc1	meč
Andúril	Andúrila	k1gFnPc2	Andúrila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
použije	použít	k5eAaPmIp3nS	použít
Aragorn	Aragorn	k1gInSc4	Aragorn
armádu	armáda	k1gFnSc4	armáda
Mrtvých	mrtvý	k1gMnPc2	mrtvý
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
umbarských	umbarský	k2eAgMnPc2d1	umbarský
korzárů	korzár	k1gMnPc2	korzár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
samotnému	samotný	k2eAgNnSc3d1	samotné
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
