<s>
Genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
virů	vir	k1gInPc2	vir
v	v	k7c6	v
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
