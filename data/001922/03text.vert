<s>
Genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
virů	vir	k1gInPc2	vir
v	v	k7c6	v
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgInPc4	všechen
geny	gen	k1gInPc4	gen
a	a	k8xC	a
nekódující	kódující	k2eNgFnPc4d1	nekódující
sekvence	sekvence	k1gFnPc4	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
genom	genom	k1gInSc1	genom
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
sekvence	sekvence	k1gFnSc1	sekvence
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
jedné	jeden	k4xCgFnSc2	jeden
sady	sada	k1gFnSc2	sada
chromozómů	chromozóm	k1gInPc2	chromozóm
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
například	například	k6eAd1	například
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
sad	sada	k1gFnPc2	sada
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
diploidní	diploidní	k2eAgMnSc1d1	diploidní
jedinec	jedinec	k1gMnSc1	jedinec
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
somatické	somatický	k2eAgFnSc6d1	somatická
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
genom	genom	k1gInSc1	genom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápán	chápat	k5eAaImNgMnS	chápat
úžeji	úzko	k6eAd2	úzko
jako	jako	k8xC	jako
kompletní	kompletní	k2eAgInSc4d1	kompletní
výčet	výčet	k1gInSc4	výčet
veškeré	veškerý	k3xTgFnSc2	veškerý
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
navíc	navíc	k6eAd1	navíc
i	i	k9	i
veškerá	veškerý	k3xTgNnPc4	veškerý
DNA	dno	k1gNnPc4	dno
těch	ten	k3xDgFnPc2	ten
organel	organela	k1gFnPc2	organela
(	(	kIx(	(
<g/>
semiautonomní	semiautonomní	k2eAgFnPc1d1	semiautonomní
organely	organela	k1gFnPc1	organela
-	-	kIx~	-
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
<g/>
,	,	kIx,	,
chloroplasty	chloroplast	k1gInPc1	chloroplast
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dědí	dědit	k5eAaImIp3nS	dědit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc1d1	vlastní
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tedy	tedy	k9	tedy
soubor	soubor	k1gInSc1	soubor
veškeré	veškerý	k3xTgFnSc2	veškerý
buněčné	buněčný	k2eAgFnSc2d1	buněčná
DNA	DNA	kA	DNA
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vedle	vedle	k7c2	vedle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
také	také	k6eAd1	také
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
a	a	k8xC	a
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
plastidovou	plastidový	k2eAgFnSc7d1	plastidová
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Genom	genom	k1gInSc1	genom
člověka	člověk	k1gMnSc2	člověk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
20488	[number]	k4	20488
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
sekvenování	sekvenování	k1gNnSc2	sekvenování
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
výčtu	výčet	k1gInSc6	výčet
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
přečteno	přečíst	k5eAaPmNgNnS	přečíst
mnoho	mnoho	k4c1	mnoho
genomů	genom	k1gInPc2	genom
mnoha	mnoho	k4c2	mnoho
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
prvoků	prvok	k1gMnPc2	prvok
i	i	k8xC	i
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
a	a	k8xC	a
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
genomy	genom	k1gInPc1	genom
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejkratším	krátký	k2eAgInPc3d3	nejkratší
a	a	k8xC	a
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
sekvenovaným	sekvenovaný	k2eAgInPc3d1	sekvenovaný
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
přečteným	přečtený	k2eAgInSc7d1	přečtený
genomem	genom	k1gInSc7	genom
vůbec	vůbec	k9	vůbec
byl	být	k5eAaImAgInS	být
genom	genom	k1gInSc1	genom
bakterie	bakterie	k1gFnSc2	bakterie
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenzae	k1gFnSc1	influenzae
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sekvenování	sekvenování	k1gNnSc4	sekvenování
byla	být	k5eAaImAgFnS	být
vybrána	vybrán	k2eAgFnSc1d1	vybrána
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
výjimečně	výjimečně	k6eAd1	výjimečně
malému	malý	k2eAgInSc3d1	malý
genomu	genom	k1gInSc3	genom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
jen	jen	k9	jen
1	[number]	k4	1
830	[number]	k4	830
140	[number]	k4	140
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
DNA	DNA	kA	DNA
a	a	k8xC	a
1740	[number]	k4	1740
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
osekvenovaným	osekvenovaný	k2eAgMnPc3d1	osekvenovaný
jednobuněčným	jednobuněčný	k2eAgMnPc3d1	jednobuněčný
patří	patřit	k5eAaImIp3nS	patřit
bakterie	bakterie	k1gFnPc4	bakterie
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
<g/>
,	,	kIx,	,
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
například	například	k6eAd1	například
virus	virus	k1gInSc1	virus
SARS	SARS	kA	SARS
<g/>
.	.	kIx.	.
</s>
<s>
Přečtené	přečtený	k2eAgInPc1d1	přečtený
genomy	genom	k1gInPc1	genom
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
databázi	databáze	k1gFnSc4	databáze
genů	gen	k1gInPc2	gen
od	od	k7c2	od
"	"	kIx"	"
<g/>
průměrného	průměrný	k2eAgMnSc2d1	průměrný
jedince	jedinec	k1gMnSc2	jedinec
<g/>
"	"	kIx"	"
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
analýze	analýza	k1gFnSc3	analýza
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
vždy	vždy	k6eAd1	vždy
vzorky	vzorek	k1gInPc1	vzorek
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
octomilka	octomilka	k1gFnSc1	octomilka
(	(	kIx(	(
<g/>
Drosophila	Drosophila	k1gFnSc1	Drosophila
melanogaster	melanogaster	k1gMnSc1	melanogaster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
modelových	modelový	k2eAgInPc2d1	modelový
organismů	organismus	k1gInPc2	organismus
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
.	.	kIx.	.
komár	komár	k1gMnSc1	komár
rodu	rod	k1gInSc2	rod
Anopheles	Anopheles	k1gMnSc1	Anopheles
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
-	-	kIx~	-
leden	leden	k1gInSc1	leden
2004	[number]	k4	2004
-	-	kIx~	-
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
odlišností	odlišnost	k1gFnSc7	odlišnost
tohoto	tento	k3xDgInSc2	tento
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
eusociálnost	eusociálnost	k1gFnSc4	eusociálnost
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významným	významný	k2eAgMnSc7d1	významný
druhem	druh	k1gMnSc7	druh
<g/>
.	.	kIx.	.
bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
-	-	kIx~	-
(	(	kIx(	(
<g/>
91	[number]	k4	91
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
-	-	kIx~	-
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
dánio	dánio	k6eAd1	dánio
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
(	(	kIx(	(
<g/>
Danio	Danio	k1gMnSc1	Danio
rerio	rerio	k1gMnSc1	rerio
<g/>
)	)	kIx)	)
čtverzubec	čtverzubec	k1gMnSc1	čtverzubec
rudoploutvý	rudoploutvý	k2eAgMnSc1d1	rudoploutvý
(	(	kIx(	(
<g/>
Takifugu	Takifuga	k1gFnSc4	Takifuga
rubripes	rubripesa	k1gFnPc2	rubripesa
<g/>
)	)	kIx)	)
čtverzubec	čtverzubec	k1gInSc1	čtverzubec
černozelený	černozelený	k2eAgInSc1d1	černozelený
(	(	kIx(	(
<g/>
Tetraodon	Tetraodon	k1gInSc1	Tetraodon
nigroviridis	nigroviridis	k1gInSc1	nigroviridis
<g/>
)	)	kIx)	)
říjen	říjen	k1gInSc1	říjen
2004	[number]	k4	2004
-	-	kIx~	-
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Přečtení	přečtení	k1gNnSc1	přečtení
dvou	dva	k4xCgMnPc2	dva
příbuzných	příbuzný	k1gMnPc2	příbuzný
genomů	genom	k1gInPc2	genom
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
při	při	k7c6	při
porozumění	porozumění	k1gNnSc6	porozumění
procesu	proces	k1gInSc2	proces
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
drápatka	drápatka	k1gFnSc1	drápatka
Xenopus	Xenopus	k1gInSc1	Xenopus
tropicalis	tropicalis	k1gFnSc1	tropicalis
kur	kur	k1gMnSc1	kur
bankivský	bankivský	k2eAgMnSc1d1	bankivský
(	(	kIx(	(
<g/>
Gallus	Gallus	k1gMnSc1	Gallus
gallus	gallus	k1gMnSc1	gallus
<g/>
)	)	kIx)	)
-	-	kIx~	-
březen	březen	k1gInSc1	březen
2004	[number]	k4	2004
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
bankivský	bankivský	k2eAgMnSc1d1	bankivský
je	být	k5eAaImIp3nS	být
divokým	divoký	k2eAgInSc7d1	divoký
předkem	předek	k1gInSc7	předek
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
Rozluštění	rozluštění	k1gNnSc1	rozluštění
lidského	lidský	k2eAgInSc2d1	lidský
genomu	genom	k1gInSc2	genom
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
osekvenováno	osekvenován	k2eAgNnSc1d1	osekvenován
již	již	k9	již
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
myš	myš	k1gFnSc1	myš
-	-	kIx~	-
Myš	myš	k1gFnSc1	myš
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgNnSc7	první
savcem	savec	k1gMnSc7	savec
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
genom	genom	k1gInSc1	genom
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
porovnáván	porovnávat	k5eAaImNgInS	porovnávat
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
<g/>
.	.	kIx.	.
</s>
<s>
Myš	myš	k1gFnSc1	myš
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
modelovým	modelový	k2eAgInSc7d1	modelový
organismem	organismus	k1gInSc7	organismus
lékařského	lékařský	k2eAgInSc2d1	lékařský
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
potkan	potkan	k1gMnSc1	potkan
-	-	kIx~	-
duben	duben	k1gInSc1	duben
2004	[number]	k4	2004
-	-	kIx~	-
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
modelovým	modelový	k2eAgInSc7d1	modelový
organismem	organismus	k1gInSc7	organismus
v	v	k7c6	v
lékařském	lékařský	k2eAgInSc6d1	lékařský
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
skot	skot	k1gInSc1	skot
-	-	kIx~	-
nahrubo	nahrubo	k6eAd1	nahrubo
přečten	přečíst	k5eAaPmNgInS	přečíst
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
-	-	kIx~	-
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
zejména	zejména	k9	zejména
z	z	k7c2	z
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
a	a	k8xC	a
šlechtitelských	šlechtitelský	k2eAgInPc2d1	šlechtitelský
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
prase	prase	k1gNnSc1	prase
-	-	kIx~	-
červen	červen	k1gInSc1	červen
2005	[number]	k4	2005
-	-	kIx~	-
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
jak	jak	k8xC	jak
z	z	k7c2	z
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
a	a	k8xC	a
šlechtitelských	šlechtitelský	k2eAgInPc2d1	šlechtitelský
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
zároveň	zároveň	k6eAd1	zároveň
některé	některý	k3yIgFnPc4	některý
tkáně	tkáň	k1gFnPc4	tkáň
či	či	k8xC	či
orgány	orgán	k1gInPc1	orgán
prasete	prase	k1gNnSc2	prase
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
transplantační	transplantační	k2eAgFnSc6d1	transplantační
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
znát	znát	k5eAaImF	znát
genom	genom	k1gInSc4	genom
prasete	prase	k1gNnSc2	prase
<g/>
.	.	kIx.	.
šimpanz	šimpanz	k1gMnSc1	šimpanz
-	-	kIx~	-
srpen	srpen	k1gInSc1	srpen
2005	[number]	k4	2005
-	-	kIx~	-
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
významné	významný	k2eAgInPc1d1	významný
pro	pro	k7c4	pro
porovnávatelnost	porovnávatelnost	k1gFnSc4	porovnávatelnost
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
genomem	genom	k1gInSc7	genom
<g/>
.	.	kIx.	.
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
plemeno	plemeno	k1gNnSc1	plemeno
boxer	boxer	k1gInSc1	boxer
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
některých	některý	k3yIgNnPc2	některý
dědičných	dědičný	k2eAgNnPc2d1	dědičné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
psy	pes	k1gMnPc7	pes
sdílíme	sdílet	k5eAaImIp1nP	sdílet
<g/>
.	.	kIx.	.
makak	makak	k1gMnSc1	makak
huseníček	huseníčko	k1gNnPc2	huseníčko
rolní	rolní	k2eAgFnPc1d1	rolní
(	(	kIx(	(
<g/>
Arabidopsis	Arabidopsis	k1gInSc1	Arabidopsis
thalliana	thallian	k1gMnSc2	thallian
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Arabidopsis	Arabidopsis	k1gInSc1	Arabidopsis
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
modelový	modelový	k2eAgInSc1d1	modelový
organismus	organismus	k1gInSc1	organismus
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
kukuřice	kukuřice	k1gFnSc1	kukuřice
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
Zea	Zea	k1gFnSc1	Zea
mays	mays	k1gInSc1	mays
<g/>
)	)	kIx)	)
Topol	topol	k1gInSc1	topol
chlupatoplodý	chlupatoplodý	k2eAgInSc1d1	chlupatoplodý
(	(	kIx(	(
<g/>
Populus	Populus	k1gInSc1	Populus
trichocarpa	trichocarp	k1gMnSc2	trichocarp
<g/>
)	)	kIx)	)
-	-	kIx~	-
Anotovaný	Anotovaný	k2eAgInSc1d1	Anotovaný
genom	genom	k1gInSc1	genom
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejnit	k5eAaPmNgInS	uveřejnit
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Topol	topol	k1gInSc1	topol
chlupatoplodý	chlupatoplodý	k2eAgInSc1d1	chlupatoplodý
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
genom	genom	k1gInSc1	genom
byl	být	k5eAaImAgInS	být
osekvenován	osekvenovat	k5eAaPmNgInS	osekvenovat
<g/>
.	.	kIx.	.
</s>
<s>
Physcomitrella	Physcomitrella	k6eAd1	Physcomitrella
patens	patens	k6eAd1	patens
(	(	kIx(	(
<g/>
modelový	modelový	k2eAgInSc1d1	modelový
druh	druh	k1gInSc1	druh
mechu	mech	k1gInSc2	mech
<g/>
)	)	kIx)	)
-	-	kIx~	-
Anotovaný	Anotovaný	k2eAgInSc1d1	Anotovaný
a	a	k8xC	a
kompletní	kompletní	k2eAgInSc1d1	kompletní
genom	genom	k1gInSc1	genom
mechu	mech	k1gInSc2	mech
Physcomitrella	Physcomitrello	k1gNnSc2	Physcomitrello
patens	patens	k6eAd1	patens
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejnit	k5eAaPmNgInS	uveřejnit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Physcomitrella	Physcomitrella	k1gFnSc1	Physcomitrella
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
osekvenovaným	osekvenovaný	k2eAgInSc7d1	osekvenovaný
mechorostem	mechorost	k1gInSc7	mechorost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
první	první	k4xOgFnSc7	první
sekvenovanou	sekvenovaný	k2eAgFnSc7d1	sekvenovaný
ne-semennou	emenný	k2eNgFnSc7d1	-semenný
rostlinou	rostlina	k1gFnSc7	rostlina
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
tak	tak	k6eAd1	tak
cenný	cenný	k2eAgInSc4d1	cenný
modelový	modelový	k2eAgInSc4d1	modelový
organizmus	organizmus	k1gInSc4	organizmus
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
evolučních	evoluční	k2eAgInPc2d1	evoluční
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
gen	gen	k1gInSc1	gen
supercontigy	supercontiga	k1gFnSc2	supercontiga
přehledné	přehledný	k2eAgNnSc4d1	přehledné
zobrazení	zobrazení	k1gNnSc4	zobrazení
přečtených	přečtený	k2eAgInPc2d1	přečtený
genomů	genom	k1gInPc2	genom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
