<p>
<s>
Blouznivci	blouznivec	k1gMnPc1	blouznivec
našich	náš	k3xOp1gFnPc2	náš
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Antala	Antal	k1gMnSc2	Antal
Staška	Stašek	k1gMnSc2	Stašek
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
a	a	k8xC	a
spiritismus	spiritismus	k1gInSc4	spiritismus
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
název	název	k1gInSc4	název
blouznivci	blouznivec	k1gMnPc1	blouznivec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydán	vydat	k5eAaPmNgInS	vydat
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dvě	dva	k4xCgFnPc1	dva
horské	horský	k2eAgFnPc1d1	horská
dívky	dívka	k1gFnPc1	dívka
==	==	k?	==
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
Dvě	dva	k4xCgFnPc4	dva
horské	horský	k2eAgFnPc4d1	horská
dívky	dívka	k1gFnPc4	dívka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
nešťastný	šťastný	k2eNgInSc1d1	nešťastný
příběh	příběh	k1gInSc1	příběh
dvou	dva	k4xCgFnPc2	dva
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
poodhaluje	poodhalovat	k5eAaImIp3nS	poodhalovat
duchověrce	duchověrec	k1gMnSc4	duchověrec
(	(	kIx(	(
<g/>
spiritismus	spiritismus	k1gInSc1	spiritismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
nedaleko	nedaleko	k7c2	nedaleko
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
jejich	jejich	k3xOp3gFnSc7	jejich
společnou	společný	k2eAgFnSc7d1	společná
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
dohazovač	dohazovač	k1gMnSc1	dohazovač
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
,	,	kIx,	,
věčně	věčně	k6eAd1	věčně
opilý	opilý	k2eAgMnSc1d1	opilý
a	a	k8xC	a
negramotný	gramotný	k2eNgMnSc1d1	negramotný
povaleč	povaleč	k1gMnSc1	povaleč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
snaží	snažit	k5eAaImIp3nP	snažit
spojit	spojit	k5eAaPmF	spojit
dohromady	dohromady	k6eAd1	dohromady
Růženku	Růženka	k1gFnSc4	Růženka
s	s	k7c7	s
Petříkem	Petřík	k1gMnSc7	Petřík
kvůli	kvůli	k7c3	kvůli
vidině	vidina	k1gFnSc3	vidina
dvouset	dvouset	k2eAgInSc1d1	dvouset
zlatých	zlatý	k1gInPc6	zlatý
a	a	k8xC	a
honbě	honba	k1gFnSc6	honba
rodičů	rodič	k1gMnPc2	rodič
za	za	k7c7	za
penězi	peníze	k1gInPc7	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Bobek	Bobek	k1gMnSc1	Bobek
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
také	také	k9	také
členem	člen	k1gMnSc7	člen
tamějšího	tamější	k2eAgInSc2d1	tamější
spiritistického	spiritistický	k2eAgInSc2d1	spiritistický
"	"	kIx"	"
<g/>
sboru	sbor	k1gInSc2	sbor
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spiritismus	spiritismus	k1gInSc1	spiritismus
(	(	kIx(	(
<g/>
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
příchod	příchod	k1gInSc4	příchod
nového	nový	k2eAgMnSc2d1	nový
mesiáše	mesiáš	k1gMnSc2	mesiáš
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
přejat	přejmout	k5eAaPmNgInS	přejmout
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
brán	brán	k2eAgInSc1d1	brán
jako	jako	k8xC	jako
určitá	určitý	k2eAgFnSc1d1	určitá
reforma	reforma	k1gFnSc1	reforma
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nelíbilo	líbit	k5eNaImAgNnS	líbit
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
prvního	první	k4xOgInSc2	první
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
,	,	kIx,	,
milé	milý	k2eAgNnSc1d1	milé
a	a	k8xC	a
pěkné	pěkný	k2eAgNnSc1d1	pěkné
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
vychováváno	vychovávat	k5eAaImNgNnS	vychovávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
panskosti	panskost	k1gFnSc3	panskost
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
mlynářky	mlynářka	k1gFnSc2	mlynářka
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
ji	on	k3xPp3gFnSc4	on
matka	matka	k1gFnSc1	matka
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ale	ale	k9	ale
četla	číst	k5eAaImAgFnS	číst
mnoho	mnoho	k4c4	mnoho
světových	světový	k2eAgMnPc2d1	světový
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
především	především	k9	především
Byrona	Byrona	k1gFnSc1	Byrona
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
nudila	nudit	k5eAaImAgFnS	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
studenta	student	k1gMnSc2	student
práv	právo	k1gNnPc2	právo
Čeňka	Čeněk	k1gMnSc2	Čeněk
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
už	už	k6eAd1	už
se	se	k3xPyFc4	se
vdávat	vdávat	k5eAaImF	vdávat
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
Čeněk	Čeněk	k1gMnSc1	Čeněk
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
ošetřovatel	ošetřovatel	k1gMnSc1	ošetřovatel
doktor	doktor	k1gMnSc1	doktor
Boukal	boukat	k5eAaImAgMnS	boukat
<g/>
,	,	kIx,	,
pragmatický	pragmatický	k2eAgMnSc1d1	pragmatický
a	a	k8xC	a
introvertní	introvertní	k2eAgMnSc1d1	introvertní
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ale	ale	k9	ale
moc	moc	k6eAd1	moc
nelíbil	líbit	k5eNaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
přijel	přijet	k5eAaPmAgMnS	přijet
domů	domů	k6eAd1	domů
syn	syn	k1gMnSc1	syn
chudé	chudý	k2eAgFnSc2d1	chudá
sousedky	sousedka	k1gFnSc2	sousedka
kněz	kněz	k1gMnSc1	kněz
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
Růženka	Růženka	k1gFnSc1	Růženka
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
přátelila	přátelit	k5eAaImAgFnS	přátelit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
teď	teď	k6eAd1	teď
zbláznil	zbláznit	k5eAaPmAgInS	zbláznit
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
říkal	říkat	k5eAaImAgMnS	říkat
z	z	k7c2	z
blouznění	blouznění	k1gNnSc2	blouznění
její	její	k3xOp3gNnPc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
jí	jíst	k5eAaImIp3nS	jíst
jeho	jeho	k3xOp3gFnSc4	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neuměla	umět	k5eNaImAgFnS	umět
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
přečetla	přečíst	k5eAaPmAgFnS	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Jeník	Jeník	k1gMnSc1	Jeník
tajně	tajně	k6eAd1	tajně
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc4	ten
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dočetla	dočíst	k5eAaPmAgFnS	dočíst
o	o	k7c6	o
ráně	rána	k1gFnSc6	rána
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
a	a	k8xC	a
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příčinnou	příčinný	k2eAgFnSc4d1	příčinná
jeho	jeho	k3xOp3gNnSc1	jeho
šílenství	šílenství	k1gNnSc4	šílenství
a	a	k8xC	a
paranoii	paranoia	k1gFnSc4	paranoia
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
jí	on	k3xPp3gFnSc3	on
však	však	k9	však
zakázala	zakázat	k5eAaPmAgFnS	zakázat
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
a	a	k8xC	a
Jeník	Jeník	k1gMnSc1	Jeník
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
spiritistických	spiritistický	k2eAgFnPc6d1	spiritistická
schůzkách	schůzka	k1gFnPc6	schůzka
a	a	k8xC	a
poslala	poslat	k5eAaPmAgFnS	poslat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
trest	trest	k1gInSc4	trest
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jí	on	k3xPp3gFnSc3	on
po	po	k7c6	po
několika	několik	k4yIc2	několik
měsících	měsíc	k1gInPc6	měsíc
přišel	přijít	k5eAaPmAgInS	přijít
dopis	dopis	k1gInSc1	dopis
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	domů	k6eAd1	domů
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
rodičům	rodič	k1gMnPc3	rodič
peníze	peníz	k1gInPc4	peníz
z	z	k7c2	z
věna	věno	k1gNnSc2	věno
<g/>
,	,	kIx,	,
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tak	tak	k9	tak
zadluženého	zadlužený	k2eAgMnSc2d1	zadlužený
<g/>
,	,	kIx,	,
mlýna	mlýn	k1gInSc2	mlýn
po	po	k7c6	po
povodni	povodeň	k1gFnSc6	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Teta	Teta	k1gFnSc1	Teta
i	i	k8xC	i
strýc	strýc	k1gMnSc1	strýc
jí	on	k3xPp3gFnSc2	on
radili	radit	k5eAaImAgMnP	radit
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
nedělala	dělat	k5eNaImAgFnS	dělat
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
alespoň	alespoň	k9	alespoň
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neposlechla	poslechnout	k5eNaPmAgFnS	poslechnout
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
soudních	soudní	k2eAgFnPc2d1	soudní
přenic	přenice	k1gFnPc2	přenice
a	a	k8xC	a
pronásledování	pronásledování	k1gNnPc2	pronásledování
věřiteli	věřitel	k1gMnPc7	věřitel
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
vše	všechen	k3xTgNnSc1	všechen
ztraceno	ztraceno	k1gNnSc1	ztraceno
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
chaloupky	chaloupka	k1gFnSc2	chaloupka
tety	teta	k1gFnSc2	teta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
příběh	příběh	k1gInSc1	příběh
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Růženka	Růženka	k1gFnSc1	Růženka
se	se	k3xPyFc4	se
probudila	probudit	k5eAaPmAgFnS	probudit
ze	z	k7c2	z
snového	snový	k2eAgInSc2d1	snový
světa	svět	k1gInSc2	svět
knížek	knížka	k1gFnPc2	knížka
<g/>
,	,	kIx,	,
stržena	stržen	k2eAgFnSc1d1	stržena
vírem	vír	k1gInSc7	vír
tvrdé	tvrdá	k1gFnSc2	tvrdá
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
dokázala	dokázat	k5eAaPmAgFnS	dokázat
smířit	smířit	k5eAaPmF	smířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
odchodem	odchod	k1gInSc7	odchod
Petříka	Petřík	k1gMnSc2	Petřík
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
sedláka	sedlák	k1gMnSc2	sedlák
<g/>
,	,	kIx,	,
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
rodiče	rodič	k1gMnPc1	rodič
nechtějí	chtít	k5eNaImIp3nP	chtít
povolit	povolit	k5eAaPmF	povolit
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
chudou	chudý	k2eAgFnSc7d1	chudá
dcerou	dcera	k1gFnSc7	dcera
kováře	kovář	k1gMnSc2	kovář
Františkou	Františka	k1gFnSc7	Františka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
totiž	totiž	k9	totiž
blázen	blázen	k1gMnSc1	blázen
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgMnS	mít
nablízku	nablízku	k6eAd1	nablízku
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
měla	mít	k5eAaImAgFnS	mít
plně	plně	k6eAd1	plně
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
čekal	čekat	k5eAaImAgMnS	čekat
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Petříkovi	Petřík	k1gMnSc3	Petřík
ještě	ještě	k9	ještě
nebylo	být	k5eNaImAgNnS	být
dost	dost	k6eAd1	dost
(	(	kIx(	(
<g/>
jenom	jenom	k9	jenom
22	[number]	k4	22
<g/>
)	)	kIx)	)
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
Františka	Františka	k1gFnSc1	Františka
usmyslela	usmyslet	k5eAaPmAgFnS	usmyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
u	u	k7c2	u
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
zabila	zabít	k5eAaPmAgFnS	zabít
už	už	k6eAd1	už
v	v	k7c6	v
kovárně	kovárna	k1gFnSc6	kovárna
a	a	k8xC	a
zdrcenému	zdrcený	k2eAgMnSc3d1	zdrcený
Petříkovi	Petřík	k1gMnSc3	Petřík
to	ten	k3xDgNnSc1	ten
pak	pak	k9	pak
naštěstí	naštěstí	k6eAd1	naštěstí
rozmluvili	rozmluvit	k5eAaPmAgMnP	rozmluvit
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Prací	práce	k1gFnSc7	práce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snažil	snažit	k5eAaImAgInS	snažit
zahnat	zahnat	k5eAaPmF	zahnat
žal	žal	k1gInSc1	žal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
smícháním	smíchání	k1gNnSc7	smíchání
různých	různý	k2eAgInPc2d1	různý
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
rozvádění	rozvádění	k1gNnSc2	rozvádění
mnoha	mnoho	k4c2	mnoho
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
příhod	příhoda	k1gFnPc2	příhoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
ke	k	k7c3	k
ztracení	ztracení	k1gNnSc3	ztracení
čtenáře	čtenář	k1gMnSc2	čtenář
v	v	k7c6	v
ději	děj	k1gInSc6	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
