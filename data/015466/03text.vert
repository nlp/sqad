<s>
WLAN	WLAN	kA
</s>
<s>
Bezdrátová	bezdrátový	k2eAgFnSc1d1
lokální	lokální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
(	(	kIx(
<g/>
WLAN	WLAN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezdrátová	bezdrátový	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spojuje	spojovat	k5eAaImIp3nS
dvě	dva	k4xCgNnPc1
nebo	nebo	k8xC
více	hodně	k6eAd2
zařízení	zařízení	k1gNnPc1
pomocí	pomocí	k7c2
bezdrátové	bezdrátový	k2eAgFnSc2d1
distribuční	distribuční	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
(	(	kIx(
<g/>
často	často	k6eAd1
rozprostřené	rozprostřený	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
nebo	nebo	k8xC
OFDM	OFDM	kA
rádio	rádio	k1gNnSc4
<g/>
)	)	kIx)
v	v	k7c6
omezeném	omezený	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
doma	doma	k6eAd1
<g/>
,	,	kIx,
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
počítačové	počítačový	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
nebo	nebo	k8xC
kancelářské	kancelářský	k2eAgFnSc3d1
budově	budova	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dává	dávat	k5eAaImIp3nS
uživatelům	uživatel	k1gMnPc3
možnost	možnost	k1gFnSc4
stálého	stálý	k2eAgNnSc2d1
připojení	připojení	k1gNnSc2
k	k	k7c3
síti	síť	k1gFnSc3
při	při	k7c6
pohybu	pohyb	k1gInSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
pokryté	pokrytý	k2eAgFnSc6d1
signálem	signál	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
zpravidla	zpravidla	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
připojení	připojení	k1gNnSc4
k	k	k7c3
Internetu	Internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
moderních	moderní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
WLAN	WLAN	kA
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
na	na	k7c6
standardech	standard	k1gInPc6
IEEE	IEEE	kA
802.11	802.11	k4
<g/>
,	,	kIx,
uváděných	uváděný	k2eAgFnPc2d1
na	na	k7c4
trh	trh	k1gInSc4
pod	pod	k7c7
značkou	značka	k1gFnSc7
Wi-Fi	Wi-Fi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
snadné	snadný	k2eAgFnSc3d1
instalaci	instalace	k1gFnSc3
a	a	k8xC
použití	použití	k1gNnSc1
se	se	k3xPyFc4
bezdrátové	bezdrátový	k2eAgFnSc2d1
LAN	lano	k1gNnPc2
staly	stát	k5eAaPmAgFnP
oblíbené	oblíbený	k2eAgFnPc1d1
v	v	k7c6
domácnostech	domácnost	k1gFnPc6
i	i	k8xC
u	u	k7c2
komerčních	komerční	k2eAgInPc2d1
komplexů	komplex	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nabízejí	nabízet	k5eAaImIp3nP
bezdrátový	bezdrátový	k2eAgInSc4d1
přístup	přístup	k1gInSc4
svým	svůj	k3xOyFgMnPc3
zákazníkům	zákazník	k1gMnPc3
často	často	k6eAd1
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
město	město	k1gNnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
zahájilo	zahájit	k5eAaPmAgNnS
pilotní	pilotní	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožní	umožnit	k5eAaPmIp3nS
pracovníkům	pracovník	k1gMnPc3
města	město	k1gNnSc2
ve	v	k7c6
všech	všecek	k3xTgNnPc6
pěti	pět	k4xCc6
městských	městský	k2eAgFnPc6d1
částech	část	k1gFnPc6
bezdrátové	bezdrátový	k2eAgNnSc1d1
připojení	připojení	k1gNnSc1
k	k	k7c3
internetu	internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
54	#num#	k4
Mbit	Mbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
WLAN	WLAN	kA
PCI	PCI	kA
Card	Card	k1gMnSc1
(	(	kIx(
<g/>
802.11	802.11	k4
<g/>
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgFnSc4
bezdrátovou	bezdrátový	k2eAgFnSc4d1
počítačovou	počítačový	k2eAgFnSc4d1
komunikační	komunikační	k2eAgFnSc4d1
síť	síť	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
zprovoznil	zprovoznit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
profesor	profesor	k1gMnSc1
Havajské	havajský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Norman	Norman	k1gMnSc1
Abramson	Abramson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
nazývaná	nazývaný	k2eAgFnSc1d1
ALOHAnet	ALOHAnet	k1gInSc1
obcházela	obcházet	k5eAaImAgFnS
používání	používání	k1gNnSc4
telefonních	telefonní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
levnějším	levný	k2eAgFnPc3d2
spojením	spojení	k1gNnSc7
pomocí	pomoc	k1gFnPc2
radioamatérských	radioamatérský	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
sítě	síť	k1gFnSc2
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
sedm	sedm	k4xCc1
počítačů	počítač	k1gMnPc2
na	na	k7c6
čtyřech	čtyři	k4xCgInPc6
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
komunikovaly	komunikovat	k5eAaImAgInP
s	s	k7c7
centrálním	centrální	k2eAgInSc7d1
počítačem	počítač	k1gInSc7
na	na	k7c6
ostrově	ostrov	k1gInSc6
Oahu	Oahus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
WLAN	WLAN	kA
(	(	kIx(
<g/>
Wireless	Wireless	k1gInSc1
Local	Local	k1gInSc1
Area	area	k1gFnSc1
Network	network	k1gInSc1
<g/>
)	)	kIx)
stálo	stát	k5eAaImAgNnS
zpočátku	zpočátku	k6eAd1
tolik	tolik	k4xDc1,k4yIc1
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
používáno	používat	k5eAaImNgNnS
jen	jen	k6eAd1
jako	jako	k8xC,k8xS
alternativa	alternativa	k1gFnSc1
LAN	lano	k1gNnPc2
kabelu	kabel	k1gInSc2
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
kabeláž	kabeláž	k1gFnSc1
byla	být	k5eAaImAgFnS
obtížná	obtížný	k2eAgFnSc1d1
nebo	nebo	k8xC
nemožná	možný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnPc1
bezdrátové	bezdrátový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
používaly	používat	k5eAaImAgFnP
oborově	oborově	k6eAd1
specifická	specifický	k2eAgNnPc1d1
řešení	řešení	k1gNnPc1
a	a	k8xC
proprietární	proprietární	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
koncem	koncem	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byly	být	k5eAaImAgInP
tyto	tento	k3xDgInPc1
protokoly	protokol	k1gInPc1
nahrazeny	nahradit	k5eAaPmNgInP
normami	norma	k1gFnPc7
<g/>
,	,	kIx,
především	především	k6eAd1
různými	různý	k2eAgFnPc7d1
verzemi	verze	k1gFnPc7
IEEE	IEEE	kA
802.11	802.11	k4
(	(	kIx(
<g/>
v	v	k7c6
produktech	produkt	k1gInPc6
používajících	používající	k2eAgInPc2d1
značku	značka	k1gFnSc4
Wi-Fi	Wi-F	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
pod	pod	k7c7
evropskou	evropský	k2eAgFnSc7d1
alternativou	alternativa	k1gFnSc7
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
HiperLAN	HiperLAN	k1gMnSc1
<g/>
/	/	kIx~
<g/>
1	#num#	k4
byl	být	k5eAaImAgInS
podporovaný	podporovaný	k2eAgInSc1d1
Evropským	evropský	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgFnPc4d1
normy	norma	k1gFnPc4
(	(	kIx(
<g/>
ETSI	ETSI	kA
<g/>
)	)	kIx)
s	s	k7c7
první	první	k4xOgFnSc7
verzí	verze	k1gFnSc7
schválenou	schválený	k2eAgFnSc7d1
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
následováno	následovat	k5eAaImNgNnS
HiperLAN	HiperLAN	k1gFnSc7
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
funkční	funkční	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
s	s	k7c7
vlivy	vliv	k1gInPc7
ATM	ATM	kA
vydanou	vydaný	k2eAgFnSc4d1
v	v	k7c6
únoru	únor	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
Evropská	evropský	k2eAgFnSc1d1
norma	norma	k1gFnSc1
nedosáhla	dosáhnout	k5eNaPmAgFnS
komerčního	komerční	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
802.11	802.11	k4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
práce	práce	k1gFnSc2
na	na	k7c6
HiperLAN	HiperLAN	k1gFnSc6
<g/>
/	/	kIx~
<g/>
2	#num#	k4
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
ve	v	k7c4
specifikaci	specifikace	k1gFnSc4
PHY	PHY	kA
pro	pro	k7c4
IEEE	IEEE	kA
802.11	802.11	k4
<g/>
a	a	k8xC
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
totožné	totožný	k2eAgFnSc3d1
s	s	k7c7
PHY	PHY	kA
z	z	k7c2
HiperLAN	HiperLAN	k1gFnSc2
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
standard	standard	k1gInSc1
IEEE	IEEE	kA
802.11	802.11	k4
rozšířen	rozšířen	k2eAgInSc4d1
o	o	k7c4
IEEE	IEEE	kA
802.11	802.11	k4
<g/>
n	n	k0
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
přenosovou	přenosový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
až	až	k9
600	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
ve	v	k7c6
dvou	dva	k4xCgNnPc6
pásmech	pásmo	k1gNnPc6
–	–	k?
2,4	2,4	k4
GHz	GHz	k1gFnPc2
a	a	k8xC
5	#num#	k4
GHz	GHz	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
novějších	nový	k2eAgInPc2d2
routerů	router	k1gInPc2
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
využívat	využívat	k5eAaImF,k5eAaPmF
obě	dva	k4xCgNnPc4
bezdrátová	bezdrátový	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
(	(	kIx(
<g/>
dualband	dualbando	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zabraňuje	zabraňovat	k5eAaImIp3nS
přeplnění	přeplnění	k1gNnSc1
pásma	pásmo	k1gNnSc2
2,4	2,4	k4
GHz	GHz	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
také	také	k9
sdílen	sdílen	k2eAgInSc1d1
zařízeními	zařízení	k1gNnPc7
Bluetooth	Bluetootha	k1gFnPc2
a	a	k8xC
mikrovlnnými	mikrovlnný	k2eAgFnPc7d1
troubami	trouba	k1gFnPc7
<g/>
.	.	kIx.
5	#num#	k4
GHz	GHz	k1gFnPc2
je	být	k5eAaImIp3nS
širší	široký	k2eAgFnSc4d2
než	než	k8xS
2,4	2,4	k4
GHz	GHz	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
více	hodně	k6eAd2
kanály	kanál	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
větší	veliký	k2eAgInSc1d2
počet	počet	k1gInSc1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
sdílí	sdílet	k5eAaImIp3nS
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
v	v	k7c6
některých	některý	k3yIgInPc6
regionech	region	k1gInPc6
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
všechny	všechen	k3xTgInPc4
kanály	kanál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
</s>
<s>
Všechny	všechen	k3xTgFnPc1
komponenty	komponenta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
připojit	připojit	k5eAaPmF
do	do	k7c2
bezdrátového	bezdrátový	k2eAgNnSc2d1
média	médium	k1gNnSc2
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
jako	jako	k8xC,k8xS
stanice	stanice	k1gFnPc1
(	(	kIx(
<g/>
STA	sto	k4xCgNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
stanice	stanice	k1gFnPc1
jsou	být	k5eAaImIp3nP
vybaveny	vybavit	k5eAaPmNgFnP
bezdrátovou	bezdrátový	k2eAgFnSc7d1
síťovou	síťový	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
(	(	kIx(
<g/>
wi-fi	wi-fi	k6eAd1
karta	karta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezdrátová	bezdrátový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
bezdrátové	bezdrátový	k2eAgInPc4d1
přístupové	přístupový	k2eAgInPc4d1
body	bod	k1gInPc4
a	a	k8xC
klienti	klient	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístupové	přístupový	k2eAgInPc1d1
body	bod	k1gInPc1
(	(	kIx(
<g/>
AP	ap	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
bezdrátové	bezdrátový	k2eAgInPc4d1
routery	router	k1gInPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
základní	základní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
pro	pro	k7c4
bezdrátovou	bezdrátový	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
vysílají	vysílat	k5eAaImIp3nP
a	a	k8xC
přijímají	přijímat	k5eAaImIp3nP
rádiové	rádiový	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
povolenými	povolený	k2eAgNnPc7d1
bezdrátovými	bezdrátový	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezdrátoví	bezdrátový	k2eAgMnPc1d1
klienti	klient	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
mobilní	mobilní	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
notebooky	notebook	k1gInPc4
<g/>
,	,	kIx,
osobní	osobní	k2eAgMnPc1d1
digitální	digitální	k2eAgMnPc1d1
asistenti	asistent	k1gMnPc1
<g/>
,	,	kIx,
IP	IP	kA
telefony	telefon	k1gInPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
smartphony	smartphona	k1gFnPc4
nebo	nebo	k8xC
pevná	pevný	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
stolní	stolní	k2eAgInPc4d1
počítače	počítač	k1gInPc4
a	a	k8xC
pracovní	pracovní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
vybaveny	vybavit	k5eAaPmNgInP
bezdrátovým	bezdrátový	k2eAgNnSc7d1
síťovým	síťový	k2eAgNnSc7d1
rozhraním	rozhraní	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
sada	sada	k1gFnSc1
služeb	služba	k1gFnPc2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
sada	sada	k1gFnSc1
služeb	služba	k1gFnPc2
(	(	kIx(
<g/>
BSS	BSS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
množina	množina	k1gFnSc1
všech	všecek	k3xTgFnPc2
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
komunikovat	komunikovat	k5eAaImF
mezi	mezi	k7c7
sebou	se	k3xPyFc7
na	na	k7c6
PHY	PHY	kA
vrstvě	vrstva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
BSS	BSS	kA
má	mít	k5eAaImIp3nS
identifikační	identifikační	k2eAgInSc4d1
kód	kód	k1gInSc4
(	(	kIx(
<g/>
ID	Ida	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nazvaný	nazvaný	k2eAgInSc1d1
BSSID	BSSID	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
MAC	Mac	kA
adresa	adresa	k1gFnSc1
přístupového	přístupový	k2eAgInSc2d1
bodu	bod	k1gInSc2
obsluhující	obsluhující	k2eAgFnSc2d1
BSS	BSS	kA
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
BSS	BSS	kA
<g/>
:	:	kIx,
Nezávislé	závislý	k2eNgNnSc1d1
BSS	BSS	kA
(	(	kIx(
<g/>
označovaný	označovaný	k2eAgMnSc1d1
také	také	k9
jako	jako	k9
IBSS	IBSS	kA
<g/>
)	)	kIx)
a	a	k8xC
infrastrukturní	infrastrukturní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezávislé	závislý	k2eNgInPc1d1
BSS	BSS	kA
(	(	kIx(
<g/>
IBSS	IBSS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
ad	ad	k7c4
hoc	hoc	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgInPc4
přístupové	přístupový	k2eAgInPc4d1
body	bod	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nemůže	moct	k5eNaImIp3nS
připojit	připojit	k5eAaPmF
k	k	k7c3
jiné	jiný	k2eAgFnSc3d1
základní	základní	k2eAgFnSc3d1
sadě	sada	k1gFnSc3
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1
sada	sada	k1gFnSc1
služba	služba	k1gFnSc1
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1
sada	sada	k1gFnSc1
služba	služba	k1gFnSc1
(	(	kIx(
<g/>
ESS	ESS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
propojených	propojený	k2eAgFnPc2d1
základních	základní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístupové	přístupový	k2eAgInPc1d1
body	bod	k1gInPc1
v	v	k7c6
ESS	ESS	kA
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
distribučním	distribuční	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
ESS	ESS	kA
má	mít	k5eAaImIp3nS
ID	Ida	k1gFnPc2
nazvané	nazvaný	k2eAgFnSc2d1
SSID	SSID	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
řetězec	řetězec	k1gInSc1
ASCII	ascii	kA
znaků	znak	k1gInPc2
dlouhý	dlouhý	k2eAgInSc4d1
maximálně	maximálně	k6eAd1
32	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Distribuční	distribuční	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Distribuční	distribuční	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
DS	DS	kA
<g/>
)	)	kIx)
připojuje	připojovat	k5eAaImIp3nS
přístupové	přístupový	k2eAgInPc4d1
body	bod	k1gInPc4
v	v	k7c6
rozšířené	rozšířený	k2eAgFnSc6d1
sadě	sada	k1gFnSc6
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
DS	DS	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
pokrytí	pokrytí	k1gNnSc4
sítě	síť	k1gFnSc2
přes	přes	k7c4
roaming	roaming	k1gInSc4
mezi	mezi	k7c7
buňkami	buňka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
DS	DS	kA
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
kabelové	kabelový	k2eAgInPc4d1
nebo	nebo	k8xC
bezdrátové	bezdrátový	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgInPc1d1
distribuční	distribuční	k2eAgInPc1d1
systémy	systém	k1gInPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
založeny	založit	k5eAaPmNgInP
na	na	k7c6
WDS	WDS	kA
nebo	nebo	k8xC
MESH	MESH	kA
protokolech	protokol	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
bezdrátových	bezdrátový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
LAN	lano	k1gNnPc2
</s>
<s>
IEEE	IEEE	kA
802.11	802.11	k4
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
základní	základní	k2eAgInPc4d1
provozní	provozní	k2eAgInPc4d1
režimy	režim	k1gInPc4
<g/>
:	:	kIx,
infrastrukturní	infrastrukturní	k2eAgInSc4d1
a	a	k8xC
ad	ad	k7c4
hoc	hoc	k?
režim	režim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
režimu	režim	k1gInSc6
ad	ad	k7c4
hoc	hoc	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
mobilní	mobilní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
připojují	připojovat	k5eAaImIp3nP
přímo	přímo	k6eAd1
peer-to-peer	peer-to-peer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
infrastrukturním	infrastrukturní	k2eAgInSc6d1
režimu	režim	k1gInSc6
komunikují	komunikovat	k5eAaImIp3nP
mobilní	mobilní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
i	i	k9
přes	přes	k7c4
přístupový	přístupový	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
most	most	k1gInSc4
do	do	k7c2
dalších	další	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
internet	internet	k1gInSc4
nebo	nebo	k8xC
LAN	lano	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
bezdrátová	bezdrátový	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
více	hodně	k6eAd2
otevřené	otevřený	k2eAgNnSc1d1
médium	médium	k1gNnSc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
kabelovou	kabelový	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
LAN	lano	k1gNnPc2
<g/>
,	,	kIx,
designéři	designér	k1gMnPc1
802.11	802.11	k4
také	také	k9
zahrnuli	zahrnout	k5eAaPmAgMnP
šifrovací	šifrovací	k2eAgInSc4d1
mechanismus	mechanismus	k1gInSc4
<g/>
:	:	kIx,
Wired	Wired	k1gMnSc1
Equivalent	Equivalent	k1gMnSc1
Privacy	Privaca	k1gFnSc2
(	(	kIx(
<g/>
WEP	WEP	kA
<g/>
)	)	kIx)
a	a	k8xC
Wi-Fi	Wi-Fi	k1gNnSc7
Protected	Protected	k1gInSc1
Access	Access	k1gInSc1
(	(	kIx(
<g/>
WPA	WPA	kA
<g/>
,	,	kIx,
WPA	WPA	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
zabezpečení	zabezpečení	k1gNnSc3
bezdrátových	bezdrátový	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
přístupových	přístupový	k2eAgInPc2d1
bodů	bod	k1gInPc2
bude	být	k5eAaImBp3nS
také	také	k9
nabízet	nabízet	k5eAaImF
Wi-Fi	Wi-F	k1gMnPc1
Protected	Protected	k1gMnSc1
Setup	Setup	k1gMnSc1
<g/>
,	,	kIx,
rychlý	rychlý	k2eAgInSc1d1
způsob	způsob	k1gInSc1
připojení	připojení	k1gNnSc2
nového	nový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
do	do	k7c2
šifrované	šifrovaný	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Režim	režim	k1gInSc1
infrastruktury	infrastruktura	k1gFnSc2
</s>
<s>
Většina	většina	k1gFnSc1
sítí	síť	k1gFnPc2
Wi-Fi	Wi-F	k1gFnSc2
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
režimu	režim	k1gInSc6
infrastruktury	infrastruktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
režimu	režim	k1gInSc6
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
základní	základní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
bezdrátový	bezdrátový	k2eAgInSc1d1
přístupový	přístupový	k2eAgInSc1d1
bod	bod	k1gInSc1
rozbočovače	rozbočovač	k1gInSc2
a	a	k8xC
uzly	uzel	k1gInPc1
komunikují	komunikovat	k5eAaImIp3nP
přes	přes	k7c4
rozbočovač	rozbočovač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozbočovač	rozbočovač	k1gInSc1
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
kabelové	kabelový	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
k	k	k7c3
síti	síť	k1gFnSc3
<g/>
(	(	kIx(
<g/>
ne	ne	k9
vždy	vždy	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
trvalé	trvalý	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
k	k	k7c3
bezdrátové	bezdrátový	k2eAgFnSc3d1
síti	síť	k1gFnSc3
jiných	jiný	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bezdrátové	bezdrátový	k2eAgInPc1d1
přístupové	přístupový	k2eAgInPc1d1
body	bod	k1gInPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
pevné	pevný	k2eAgInPc1d1
a	a	k8xC
poskytují	poskytovat	k5eAaImIp3nP
servis	servis	k1gInSc4
jejich	jejich	k3xOp3gInPc3
klientským	klientský	k2eAgInPc3d1
uzlům	uzel	k1gInPc3
v	v	k7c6
dosahu	dosah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
síť	síť	k1gFnSc4
více	hodně	k6eAd2
přístupových	přístupový	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
se	s	k7c7
stejným	stejný	k2eAgNnSc7d1
'	'	kIx"
<g/>
SSID	SSID	kA
<g/>
'	'	kIx"
a	a	k8xC
bezpečnostním	bezpečnostní	k2eAgNnSc7d1
uspořádáním	uspořádání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdykoliv	kdykoliv	k6eAd1
je	být	k5eAaImIp3nS
klient	klient	k1gMnSc1
připojen	připojit	k5eAaPmNgMnS
k	k	k7c3
přístupovému	přístupový	k2eAgInSc3d1
bodu	bod	k1gInSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
připojen	připojit	k5eAaPmNgInS
i	i	k9
do	do	k7c2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
klientský	klientský	k2eAgInSc1d1
software	software	k1gInSc1
snažit	snažit	k5eAaImF
vybrat	vybrat	k5eAaPmF
přístupový	přístupový	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
poskytnout	poskytnout	k5eAaPmF
nejlepší	dobrý	k2eAgFnPc4d3
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
přístupový	přístupový	k2eAgInSc4d1
bod	bod	k1gInSc4
s	s	k7c7
nejsilnějším	silný	k2eAgInSc7d3
signálem	signál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Peer-to-peer	Peer-to-peer	k1gMnSc1
</s>
<s>
Peer-to-Peer	Peer-to-Peer	k1gInSc1
or	or	k?
ad	ad	k7c4
hoc	hoc	k?
wireless	wireless	k1gInSc4
LAN	lano	k1gNnPc2
</s>
<s>
Síť	síť	k1gFnSc1
ad	ad	k7c4
hoc	hoc	k?
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
samé	samý	k3xTgNnSc1
jako	jako	k9
Wi-Fi	Wi-F	k1gFnPc1
Direct	Direct	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
stanice	stanice	k1gFnPc1
komunikují	komunikovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
peer	peer	k1gMnSc1
to	ten	k3xDgNnSc4
peer	peer	k1gMnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
2	#num#	k4
<g/>
P	P	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
základna	základna	k1gFnSc1
a	a	k8xC
nikdo	nikdo	k3yNnSc1
nedává	dávat	k5eNaImIp3nS
svolení	svolení	k1gNnSc4
ke	k	k7c3
komunikaci	komunikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
dosahuje	dosahovat	k5eAaImIp3nS
pomocí	pomoc	k1gFnSc7
Independent	independent	k1gMnSc1
Basic	Basic	kA
Service	Service	k1gFnSc1
Set	set	k1gInSc1
(	(	kIx(
<g/>
IBSS	IBSS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Wi-Fi	Wi-Fi	k6eAd1
Direct	Direct	k1gInSc1
je	být	k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
typ	typ	k1gInSc1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
komunikují	komunikovat	k5eAaImIp3nP
stanice	stanice	k1gFnPc4
peer	peer	k1gMnSc1
to	ten	k3xDgNnSc1
peer	peer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
skupině	skupina	k1gFnSc6
Wi-Fi	Wi-F	k1gFnSc2
P	P	kA
<g/>
2	#num#	k4
<g/>
P	P	kA
<g/>
,	,	kIx,
vlastník	vlastník	k1gMnSc1
skupiny	skupina	k1gFnSc2
působí	působit	k5eAaImIp3nS
jako	jako	k9
přístupový	přístupový	k2eAgInSc4d1
bod	bod	k1gInSc4
a	a	k8xC
všechna	všechen	k3xTgNnPc1
ostatní	ostatní	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
jsou	být	k5eAaImIp3nP
klienty	klient	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zřídit	zřídit	k5eAaPmF
majitele	majitel	k1gMnPc4
ve	v	k7c6
Wi-Fi	Wi-F	k1gFnSc6
Direct	Direct	k2eAgMnSc1d1
skupině	skupina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
přístupu	přístup	k1gInSc6
uživatel	uživatel	k1gMnSc1
nastaví	nastavět	k5eAaBmIp3nS,k5eAaPmIp3nS
majitele	majitel	k1gMnPc4
skupiny	skupina	k1gFnSc2
P2P	P2P	k1gFnSc2
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
autonomní	autonomní	k2eAgMnSc1d1
vlastník	vlastník	k1gMnSc1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
autonomní	autonomní	k2eAgFnSc1d1
GO	GO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
metodě	metoda	k1gFnSc6
<g/>
,	,	kIx,
také	také	k9
nazývané	nazývaný	k2eAgInPc1d1
tvorba	tvorba	k1gFnSc1
skupin	skupina	k1gFnPc2
založená	založený	k2eAgFnSc1d1
na	na	k7c6
vyjednávání	vyjednávání	k1gNnSc6
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
dvě	dva	k4xCgNnPc1
zařízení	zařízení	k1gNnPc1
soupeří	soupeřit	k5eAaImIp3nP
na	na	k7c6
základě	základ	k1gInSc6
hodnoty	hodnota	k1gFnSc2
záměru	záměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zařízení	zařízení	k1gNnSc2
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
hodnotou	hodnota	k1gFnSc7
záměru	záměr	k1gInSc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
majitel	majitel	k1gMnSc1
skupiny	skupina	k1gFnSc2
a	a	k8xC
druhé	druhý	k4xOgNnSc1
zařízení	zařízení	k1gNnPc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
klientem	klient	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
záměru	záměr	k1gInSc2
vlastníka	vlastník	k1gMnSc2
může	moct	k5eAaImIp3nS
záviset	záviset	k5eAaImF
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
bezdrátové	bezdrátový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
provádí	provádět	k5eAaImIp3nS
křížové	křížový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
mezi	mezi	k7c7
WLAN	WLAN	kA
infrastrukturami	infrastruktura	k1gFnPc7
a	a	k8xC
P2P	P2P	k1gFnSc7
skupinou	skupina	k1gFnSc7
<g/>
,	,	kIx,
zda	zda	k8xS
bezdrátové	bezdrátový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
majitel	majitel	k1gMnSc1
jiné	jiný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
zbývající	zbývající	k2eAgFnSc6d1
síle	síla	k1gFnSc6
v	v	k7c6
bezdrátovém	bezdrátový	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
přijaté	přijatý	k2eAgFnSc3d1
síle	síla	k1gFnSc3
signálu	signál	k1gInSc2
prvního	první	k4xOgNnSc2
bezdrátového	bezdrátový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Síť	síť	k1gFnSc1
peer-to-peer	peer-to-pera	k1gFnPc2
umožňuje	umožňovat	k5eAaImIp3nS
bezdrátovým	bezdrátový	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
komunikovat	komunikovat	k5eAaImF
přímo	přímo	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezdrátová	bezdrátový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
v	v	k7c6
dosahu	dosah	k1gInSc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
objevovat	objevovat	k5eAaImF
a	a	k8xC
komunikovat	komunikovat	k5eAaImF
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
bez	bez	k7c2
zapojení	zapojení	k1gNnSc2
centrálních	centrální	k2eAgInPc2d1
přístupových	přístupový	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
obvykle	obvykle	k6eAd1
používá	používat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
počítače	počítač	k1gInPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
připojit	připojit	k5eAaPmF
k	k	k7c3
sobě	se	k3xPyFc3
navzájem	navzájem	k6eAd1
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
nastat	nastat	k5eAaPmF
u	u	k7c2
zařízení	zařízení	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
uzavřeného	uzavřený	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
IEEE	IEEE	kA
802.11	802.11	k4
definuje	definovat	k5eAaBmIp3nS
fyzické	fyzický	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
(	(	kIx(
<g/>
PHY	PHY	kA
<g/>
)	)	kIx)
a	a	k8xC
MAC	Mac	kA
(	(	kIx(
<g/>
Media	medium	k1gNnPc1
Access	Accessa	k1gFnPc2
Control	Control	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrstvy	vrstva	k1gFnPc4
založené	založený	k2eAgFnPc4d1
na	na	k7c6
CSMA	CSMA	kA
<g/>
/	/	kIx~
<g/>
CA	ca	kA
(	(	kIx(
<g/>
csma	csma	k1gMnSc1
s	s	k7c7
prevencí	prevence	k1gFnSc7
střetů	střet	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specifikace	specifikace	k1gFnSc1
802.11	802.11	k4
obsahují	obsahovat	k5eAaImIp3nP
ustanovení	ustanovení	k1gNnSc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
minimalizovat	minimalizovat	k5eAaBmF
kolize	kolize	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
dvě	dva	k4xCgFnPc1
mobilní	mobilní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
dosahu	dosah	k1gInSc6
společného	společný	k2eAgInSc2d1
přístupového	přístupový	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mimo	mimo	k7c4
dosah	dosah	k1gInSc4
sebe	sebe	k3xPyFc4
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Most	most	k1gInSc1
</s>
<s>
Most	most	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
k	k	k7c3
propojení	propojení	k1gNnSc3
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezdrátový	bezdrátový	k2eAgInSc1d1
most	most	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
připojení	připojení	k1gNnSc4
zařízení	zařízení	k1gNnPc2
v	v	k7c6
kabelové	kabelový	k2eAgFnSc6d1
síti	síť	k1gFnSc6
Ethernet	Ethernet	k1gInSc4
k	k	k7c3
bezdrátové	bezdrátový	k2eAgFnSc3d1
síti	síť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Most	most	k1gInSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
bod	bod	k1gInSc1
připojení	připojení	k1gNnSc2
do	do	k7c2
bezdrátové	bezdrátový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
LAN	lano	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Bezdrátový	bezdrátový	k2eAgInSc1d1
distribuční	distribuční	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Bezdrátové	bezdrátový	k2eAgInPc1d1
distribuční	distribuční	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
(	(	kIx(
<g/>
WDS	WDS	kA
<g/>
)	)	kIx)
umožňují	umožňovat	k5eAaImIp3nP
bezdrátové	bezdrátový	k2eAgNnSc1d1
propojení	propojení	k1gNnSc1
přístupových	přístupový	k2eAgInPc2d1
bodů	bod	k1gInPc2
v	v	k7c6
síti	síť	k1gFnSc6
IEEE	IEEE	kA
802.11	802.11	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
bezdrátové	bezdrátový	k2eAgNnSc1d1
síti	sít	k5eAaImF
rozšíření	rozšíření	k1gNnSc4
několika	několik	k4yIc7
přístupovými	přístupový	k2eAgInPc7d1
body	bod	k1gInPc7
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
použití	použití	k1gNnSc2
kabelové	kabelový	k2eAgFnSc2d1
páteřní	páteřní	k2eAgFnSc2d1
linky	linka	k1gFnSc2
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
propojení	propojení	k1gNnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
požadováno	požadován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
WDS	WDS	kA
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
zachovává	zachovávat	k5eAaImIp3nS
MAC	Mac	kA
adresy	adresa	k1gFnSc2
paketů	paket	k1gInPc2
klienta	klient	k1gMnSc2
prostřednictvím	prostřednictvím	k7c2
propojení	propojení	k1gNnSc2
mezi	mezi	k7c7
přístupovými	přístupový	k2eAgInPc7d1
body	bod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Přístupový	přístupový	k2eAgInSc1d1
bod	bod	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
buď	buď	k8xC
hlavní	hlavní	k2eAgFnSc1d1
<g/>
,	,	kIx,
přenosová	přenosový	k2eAgFnSc1d1
nebo	nebo	k8xC
vzdálená	vzdálený	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
připojena	připojit	k5eAaPmNgFnS
ke	k	k7c3
kabelové	kabelový	k2eAgFnSc3d1
síti	síť	k1gFnSc3
Ethernet	Ethernet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenosová	přenosový	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
přenáší	přenášet	k5eAaImIp3nS
data	datum	k1gNnPc4
mezi	mezi	k7c7
vzdálenými	vzdálený	k2eAgFnPc7d1
základními	základní	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
<g/>
,	,	kIx,
bezdrátovým	bezdrátový	k2eAgMnPc3d1
klientům	klient	k1gMnPc3
nebo	nebo	k8xC
jiným	jiný	k2eAgFnPc3d1
přenosovým	přenosový	k2eAgFnPc3d1
stanicím	stanice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálená	vzdálený	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
přijímá	přijímat	k5eAaImIp3nS
připojení	připojení	k1gNnSc4
od	od	k7c2
bezdrátových	bezdrátový	k2eAgMnPc2d1
klientů	klient	k1gMnPc2
a	a	k8xC
předává	předávat	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
přenosovou	přenosový	k2eAgFnSc4d1
nebo	nebo	k8xC
hlavní	hlavní	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
mezi	mezi	k7c7
klienty	klient	k1gMnPc7
jsou	být	k5eAaImIp3nP
spíše	spíše	k9
prováděna	prováděn	k2eAgNnPc1d1
pomocí	pomocí	k7c2
MAC	Mac	kA
adresy	adresa	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
přiřazením	přiřazení	k1gNnSc7
specifické	specifický	k2eAgFnSc2d1
IP	IP	kA
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc4
základní	základní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
v	v	k7c6
bezdrátovém	bezdrátový	k2eAgInSc6d1
distribučním	distribuční	k2eAgInSc6d1
systému	systém	k1gInSc6
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
nakonfigurovány	nakonfigurovat	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
používaly	používat	k5eAaImAgFnP
stejný	stejný	k2eAgInSc4d1
rádiový	rádiový	k2eAgInSc4d1
kanál	kanál	k1gInSc4
a	a	k8xC
sdílely	sdílet	k5eAaImAgFnP
WEP	WEP	kA
nebo	nebo	k8xC
WPA	WPA	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
WDS	WDS	kA
také	také	k9
vyžaduje	vyžadovat	k5eAaImIp3nS
nakonfigurování	nakonfigurování	k1gNnSc4
základní	základní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
předala	předat	k5eAaPmAgFnS
ostatním	ostatní	k1gNnSc7
informace	informace	k1gFnSc2
v	v	k7c6
systému	systém	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
výše	vysoce	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
WDS	WDS	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k9
opakovač	opakovač	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přemosťuje	přemosťovat	k5eAaImIp3nS
a	a	k8xC
přijímá	přijímat	k5eAaImIp3nS
bezdrátové	bezdrátový	k2eAgMnPc4d1
klienty	klient	k1gMnPc4
současně	současně	k6eAd1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
tradičního	tradiční	k2eAgNnSc2d1
přemostění	přemostění	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
kapacita	kapacita	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
metodě	metoda	k1gFnSc6
se	se	k3xPyFc4
snižuje	snižovat	k5eAaImIp3nS
na	na	k7c4
polovinu	polovina	k1gFnSc4
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
klienty	klient	k1gMnPc4
připojené	připojený	k2eAgMnPc4d1
bezdrátově	bezdrátově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Bezdrátové	bezdrátový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
LAN	lano	k1gNnPc2
mají	mít	k5eAaImIp3nP
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
použití	použití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc1d1
implementace	implementace	k1gFnSc1
sítí	síť	k1gFnPc2
WLAN	WLAN	kA
sahají	sahat	k5eAaImIp3nP
od	od	k7c2
malé	malý	k2eAgFnSc2d1
domácí	domácí	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
až	až	k9
po	po	k7c6
velké	velká	k1gFnSc6
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
mobilní	mobilní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
na	na	k7c6
letadlech	letadlo	k1gNnPc6
a	a	k8xC
vlacích	vlak	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
přistupovat	přistupovat	k5eAaImF
k	k	k7c3
internetu	internet	k1gInSc3
z	z	k7c2
WLAN	WLAN	kA
hotspotů	hotspot	k1gMnPc2
v	v	k7c6
restauracích	restaurace	k1gFnPc6
<g/>
,	,	kIx,
hotelech	hotel	k1gInPc6
a	a	k8xC
nyní	nyní	k6eAd1
i	i	k9
s	s	k7c7
přenosnými	přenosný	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
připojují	připojovat	k5eAaImIp3nP
k	k	k7c3
sítím	sítí	k1gNnSc7
3G	3G	k4
nebo	nebo	k8xC
4	#num#	k4
<g/>
G.	G.	kA
Často	často	k6eAd1
tyto	tento	k3xDgInPc1
typy	typ	k1gInPc1
veřejných	veřejný	k2eAgInPc2d1
přístupových	přístupový	k2eAgInPc2d1
bodů	bod	k1gInPc2
nevyžadují	vyžadovat	k5eNaImIp3nP
žádnou	žádný	k3yNgFnSc4
registraci	registrace	k1gFnSc4
nebo	nebo	k8xC
heslo	heslo	k1gNnSc4
pro	pro	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
síti	síť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přístupné	přístupný	k2eAgInPc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
již	již	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
registraci	registrace	k1gFnSc3
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
již	již	k6eAd1
hrazen	hradit	k5eAaImNgInS
poplatek	poplatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Wi-Fi	Wi-Fi	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Wireless	Wirelessa	k1gFnPc2
LAN	lano	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4633975-9	4633975-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2002000555	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2002000555	#num#	k4
</s>
