<s>
Bezdrátová	bezdrátový	k2eAgFnSc1d1
lokální	lokální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
(	(	kIx(
<g/>
WLAN	WLAN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezdrátová	bezdrátový	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spojuje	spojovat	k5eAaImIp3nS
dvě	dva	k4xCgNnPc1
nebo	nebo	k8xC
více	hodně	k6eAd2
zařízení	zařízení	k1gNnPc1
pomocí	pomocí	k7c2
bezdrátové	bezdrátový	k2eAgFnSc2d1
distribuční	distribuční	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
(	(	kIx(
<g/>
často	často	k6eAd1
rozprostřené	rozprostřený	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
nebo	nebo	k8xC
OFDM	OFDM	kA
rádio	rádio	k1gNnSc4
<g/>
)	)	kIx)
v	v	k7c6
omezeném	omezený	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
doma	doma	k6eAd1
<g/>
,	,	kIx,
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
počítačové	počítačový	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
nebo	nebo	k8xC
kancelářské	kancelářský	k2eAgFnSc3d1
budově	budova	k1gFnSc3
<g/>
.	.	kIx.
</s>