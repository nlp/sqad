<p>
<s>
Karel	Karel	k1gMnSc1	Karel
August	August	k1gMnSc1	August
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1685	[number]	k4	1685
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nasavsko-weilburským	nasavskoeilburský	k2eAgNnSc7d1	nasavsko-weilburský
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
August	August	k1gMnSc1	August
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
Arnošta	Arnošt	k1gMnSc2	Arnošt
Nasavsko-Weilburského	Nasavsko-Weilburský	k2eAgMnSc2d1	Nasavsko-Weilburský
a	a	k8xC	a
Marie	Maria	k1gFnPc4	Maria
Polyxeny	Polyxena	k1gFnSc2	Polyxena
Leiningensko-Dagsbursko-Hartenburské	Leiningensko-Dagsbursko-Hartenburský	k2eAgFnSc2d1	Leiningensko-Dagsbursko-Hartenburský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
saský	saský	k2eAgMnSc1d1	saský
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
saským	saský	k2eAgMnSc7d1	saský
vyslancem	vyslanec	k1gMnSc7	vyslanec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1719	[number]	k4	1719
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1733	[number]	k4	1733
a	a	k8xC	a
1734	[number]	k4	1734
velel	velet	k5eAaImAgInS	velet
císařským	císařský	k2eAgInSc7d1	císařský
vojskům	vojsko	k1gNnPc3	vojsko
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
jako	jako	k8xC	jako
císařský	císařský	k2eAgMnSc1d1	císařský
generál	generál	k1gMnSc1	generál
kavalérie	kavalérie	k1gFnSc2	kavalérie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1737	[number]	k4	1737
převzal	převzít	k5eAaPmAgMnS	převzít
titul	titul	k1gInSc4	titul
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gFnSc3	jeho
rodině	rodina	k1gFnSc3	rodina
udělen	udělen	k2eAgInSc1d1	udělen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
však	však	k9	však
rodina	rodina	k1gFnSc1	rodina
nezískala	získat	k5eNaPmAgFnS	získat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
a	a	k8xC	a
na	na	k7c4	na
protest	protest	k1gInSc4	protest
titul	titul	k1gInSc4	titul
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1737	[number]	k4	1737
bylo	být	k5eAaImAgNnS	být
Karlu	Karel	k1gMnSc3	Karel
Augustovi	August	k1gMnSc3	August
uděleno	udělen	k2eAgNnSc1d1	uděleno
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
sněmu	sněm	k1gInSc6	sněm
a	a	k8xC	a
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
svůj	svůj	k3xOyFgInSc4	svůj
knížecí	knížecí	k2eAgInSc4d1	knížecí
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
August	August	k1gMnSc1	August
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
ve	v	k7c6	v
Weilburgu	Weilburg	k1gInSc6	Weilburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
potomci	potomek	k1gMnPc1	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
August	August	k1gMnSc1	August
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1723	[number]	k4	1723
ve	v	k7c6	v
Wiesbadenu	Wiesbaden	k1gInSc6	Wiesbaden
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Augustou	Augusta	k1gMnSc7	Augusta
Bedřiškou	Bedřiška	k1gFnSc7	Bedřiška
Nasavsko-Idsteinskou	Nasavsko-Idsteinský	k2eAgFnSc7d1	Nasavsko-Idsteinský
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
několik	několik	k4yIc4	několik
dětíː	dětíː	k?	dětíː
</s>
</p>
<p>
<s>
Henrieta	Henriet	k2eAgFnSc1d1	Henrieta
Maria	Maria	k1gFnSc1	Maria
Dorotea	Dorotea	k1gFnSc1	Dorotea
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
-	-	kIx~	-
<g/>
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Henrieta	Henrieta	k1gFnSc1	Henrieta
Augusta	August	k1gMnSc2	August
Frederika	Frederik	k1gMnSc2	Frederik
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
–	–	k?	–
<g/>
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kristýna	Kristýna	k1gFnSc1	Kristýna
Luisa	Luisa	k1gFnSc1	Luisa
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
-	-	kIx~	-
<g/>
1727	[number]	k4	1727
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polyxena	Polyxena	k1gFnSc1	Polyxena
Luisa	Luisa	k1gFnSc1	Luisa
Vilemína	Vilemína	k1gFnSc1	Vilemína
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1728	[number]	k4	1728
<g/>
–	–	k?	–
<g/>
1732	[number]	k4	1732
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kristýna	Kristýna	k1gFnSc1	Kristýna
Luisa	Luisa	k1gFnSc1	Luisa
Šarlota	Šarlota	k1gFnSc1	Šarlota
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
1732	[number]	k4	1732
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Luisa	Luisa	k1gFnSc1	Luisa
Polyxena	Polyxena	k1gFnSc1	Polyxena
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kristián	Kristián	k1gMnSc1	Kristián
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1	Nasavsko-Weilburský
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Charles	Charles	k1gMnSc1	Charles
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Nassau-Weilburg	Nassau-Weilburg	k1gMnSc1	Nassau-Weilburg
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
