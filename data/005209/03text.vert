<s>
Termín	termín	k1gInSc1	termín
starověk	starověk	k1gInSc1	starověk
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
dějepisectví	dějepisectví	k1gNnSc6	dějepisectví
historické	historický	k2eAgNnSc4d1	historické
období	období	k1gNnSc4	období
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
prvních	první	k4xOgFnPc2	první
civilizací	civilizace	k1gFnPc2	civilizace
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
lze	lze	k6eAd1	lze
tuto	tento	k3xDgFnSc4	tento
epochu	epocha	k1gFnSc4	epocha
ohraničit	ohraničit	k5eAaPmF	ohraničit
koncem	koncem	k7c2	koncem
pravěku	pravěk	k1gInSc2	pravěk
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
počátkem	počátek	k1gInSc7	počátek
středověku	středověk	k1gInSc2	středověk
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
pojem	pojem	k1gInSc1	pojem
antika	antika	k1gFnSc1	antika
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
výlučně	výlučně	k6eAd1	výlučně
na	na	k7c4	na
řecko-římskou	řecko-římský	k2eAgFnSc4d1	řecko-římská
etapu	etapa	k1gFnSc4	etapa
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlejší	rozsáhlý	k2eAgInSc1d2	rozsáhlejší
pojem	pojem	k1gInSc1	pojem
starověk	starověk	k1gInSc1	starověk
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tedy	tedy	k9	tedy
rovněž	rovněž	k9	rovněž
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
dějin	dějiny	k1gFnPc2	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
starověku	starověk	k1gInSc2	starověk
lze	lze	k6eAd1	lze
vymezit	vymezit	k5eAaPmF	vymezit
dobou	doba	k1gFnSc7	doba
rozšíření	rozšíření	k1gNnSc2	rozšíření
písma	písmo	k1gNnSc2	písmo
na	na	k7c6	na
území	území	k1gNnSc6	území
staroorientálních	staroorientální	k2eAgFnPc2d1	staroorientální
říší	říš	k1gFnPc2	říš
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
–	–	k?	–
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
(	(	kIx(	(
<g/>
Sumer	Sumer	k1gInSc1	Sumer
<g/>
,	,	kIx,	,
Akkad	Akkad	k1gInSc1	Akkad
<g/>
,	,	kIx,	,
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
Mitanni	Mitann	k1gMnPc1	Mitann
<g/>
,	,	kIx,	,
Asýrie	Asýrie	k1gFnPc1	Asýrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Íránské	íránský	k2eAgFnSc2d1	íránská
vysočiny	vysočina	k1gFnSc2	vysočina
(	(	kIx(	(
<g/>
Elam	Elam	k1gInSc1	Elam
<g/>
,	,	kIx,	,
Médie	Médie	k1gFnSc1	Médie
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc1	Persie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
(	(	kIx(	(
<g/>
Ugarit	Ugarit	k1gInSc1	Ugarit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Malé	Malé	k2eAgFnSc4d1	Malé
Asii	Asie	k1gFnSc4	Asie
(	(	kIx(	(
<g/>
Chetité	Chetita	k1gMnPc1	Chetita
<g/>
,	,	kIx,	,
Frýgie	Frýgie	k1gFnPc1	Frýgie
<g/>
,	,	kIx,	,
Lýdie	Lýdia	k1gFnPc1	Lýdia
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
starověku	starověk	k1gInSc2	starověk
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
rozpadu	rozpad	k1gInSc2	rozpad
římské	římský	k2eAgFnSc2d1	římská
resp.	resp.	kA	resp.
sásánovské	sásánovský	k2eAgFnSc2d1	sásánovská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
arabské	arabský	k2eAgFnSc2d1	arabská
expanze	expanze	k1gFnSc2	expanze
(	(	kIx(	(
<g/>
zánik	zánik	k1gInSc1	zánik
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
565	[number]	k4	565
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
v	v	k7c6	v
roce	rok	k1gInSc6	rok
632	[number]	k4	632
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
němečtí	německý	k2eAgMnPc1d1	německý
historikové	historik	k1gMnPc1	historik
označují	označovat	k5eAaImIp3nP	označovat
období	období	k1gNnSc4	období
přerodu	přerod	k1gInSc2	přerod
starověku	starověk	k1gInSc2	starověk
ve	v	k7c4	v
středověk	středověk	k1gInSc4	středověk
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
za	za	k7c4	za
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
etapu	etapa	k1gFnSc4	etapa
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověký	starověký	k2eAgInSc4d1	starověký
Orient	Orient	k1gInSc4	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doba	doba	k1gFnSc1	doba
měděná	měděný	k2eAgFnSc1d1	měděná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
starověké	starověký	k2eAgFnPc1d1	starověká
civilizace	civilizace	k1gFnPc1	civilizace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
eneolitu	eneolit	k1gInSc2	eneolit
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
(	(	kIx(	(
<g/>
polní	polní	k2eAgNnSc1d1	polní
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenskou	společenský	k2eAgFnSc7d1	společenská
dělbou	dělba	k1gFnSc7	dělba
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
základními	základní	k2eAgFnPc7d1	základní
technikami	technika	k1gFnPc7	technika
zpracování	zpracování	k1gNnSc2	zpracování
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
skladováním	skladování	k1gNnSc7	skladování
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
založením	založení	k1gNnSc7	založení
prvních	první	k4xOgFnPc2	první
stálých	stálý	k2eAgFnPc2d1	stálá
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
kultury	kultura	k1gFnSc2	kultura
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
znalosti	znalost	k1gFnSc2	znalost
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
doposud	doposud	k6eAd1	doposud
užívané	užívaný	k2eAgInPc1d1	užívaný
piktogramy	piktogram	k1gInPc1	piktogram
a	a	k8xC	a
ideogramy	ideogram	k1gInPc1	ideogram
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
grafických	grafický	k2eAgInPc2d1	grafický
symbolů	symbol	k1gInPc2	symbol
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
již	již	k6eAd1	již
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jejich	jejich	k3xOp3gFnPc1	jejich
podoba	podoba	k1gFnSc1	podoba
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
byly	být	k5eAaImAgInP	být
nepochybně	pochybně	k6eNd1	pochybně
silně	silně	k6eAd1	silně
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
klínovým	klínový	k2eAgNnSc7d1	klínové
písmem	písmo	k1gNnSc7	písmo
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
a	a	k8xC	a
hieroglyfy	hieroglyf	k1gInPc4	hieroglyf
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
grafické	grafický	k2eAgFnPc1d1	grafická
značky	značka	k1gFnPc1	značka
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Féničanů	Féničan	k1gMnPc2	Féničan
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
v	v	k7c4	v
naši	náš	k3xOp1gFnSc4	náš
dnešní	dnešní	k2eAgFnSc4d1	dnešní
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
písma	písmo	k1gNnSc2	písmo
umožnil	umožnit	k5eAaPmAgInS	umožnit
rodícím	rodící	k2eAgInPc3d1	rodící
se	se	k3xPyFc4	se
státům	stát	k1gInPc3	stát
zavést	zavést	k5eAaPmF	zavést
stálý	stálý	k2eAgInSc4d1	stálý
správní	správní	k2eAgInSc4d1	správní
a	a	k8xC	a
náboženský	náboženský	k2eAgInSc4d1	náboženský
aparát	aparát	k1gInSc4	aparát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
počátku	počátek	k1gInSc2	počátek
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sumerové	Sumer	k1gMnPc1	Sumer
a	a	k8xC	a
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
civilizace	civilizace	k1gFnSc1	civilizace
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
v	v	k7c6	v
sumerských	sumerský	k2eAgInPc6d1	sumerský
městských	městský	k2eAgInPc6d1	městský
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
Uruk	Uruk	k1gInSc4	Uruk
<g/>
,	,	kIx,	,
Ur	Ur	k1gInSc4	Ur
<g/>
,	,	kIx,	,
Eridu	Eris	k1gFnSc4	Eris
<g/>
,	,	kIx,	,
Larsa	Larsa	k1gFnSc1	Larsa
<g/>
,	,	kIx,	,
Lagaš	Lagaš	k1gInSc1	Lagaš
a	a	k8xC	a
Kiš	kiš	k0	kiš
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
Uruk	Uruk	k1gInSc1	Uruk
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
okolním	okolní	k2eAgInPc3d1	okolní
městům	město	k1gNnPc3	město
velikou	veliký	k2eAgFnSc7d1	veliká
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
těchto	tento	k3xDgNnPc2	tento
sídel	sídlo	k1gNnPc2	sídlo
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
důmyslným	důmyslný	k2eAgInSc7d1	důmyslný
a	a	k8xC	a
složitým	složitý	k2eAgInSc7d1	složitý
systémem	systém	k1gInSc7	systém
výstavby	výstavba	k1gFnSc2	výstavba
zavlažování	zavlažování	k1gNnSc2	zavlažování
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
organizován	organizovat	k5eAaBmNgInS	organizovat
a	a	k8xC	a
řízen	řídit	k5eAaImNgInS	řídit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
chrámového	chrámový	k2eAgNnSc2d1	chrámové
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hliněných	hliněný	k2eAgInPc2d1	hliněný
střepů	střep	k1gInPc2	střep
<g/>
,	,	kIx,	,
pečetí	pečeť	k1gFnPc2	pečeť
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
jiných	jiný	k2eAgInPc2d1	jiný
artefaktů	artefakt	k1gInPc2	artefakt
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
obchodní	obchodní	k2eAgNnSc1d1	obchodní
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Arábií	Arábie	k1gFnSc7	Arábie
a	a	k8xC	a
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
impulzem	impulz	k1gInSc7	impulz
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozmachu	rozmach	k1gInSc2	rozmach
sumerských	sumerský	k2eAgInPc2d1	sumerský
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
vynalezení	vynalezení	k1gNnSc4	vynalezení
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
hrnčířského	hrnčířský	k2eAgInSc2d1	hrnčířský
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
probíhající	probíhající	k2eAgInSc4d1	probíhající
vzrůst	vzrůst	k1gInSc4	vzrůst
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
vývoje	vývoj	k1gInSc2	vývoj
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
nejprve	nejprve	k6eAd1	nejprve
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
evidenci	evidence	k1gFnSc3	evidence
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
:	:	kIx,	:
z	z	k7c2	z
nástroje	nástroj	k1gInSc2	nástroj
vedení	vedení	k1gNnSc2	vedení
záznamů	záznam	k1gInPc2	záznam
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prostředek	prostředek	k1gInSc1	prostředek
osobního	osobní	k2eAgInSc2d1	osobní
nebo	nebo	k8xC	nebo
kolektivního	kolektivní	k2eAgInSc2d1	kolektivní
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
nejstaršími	starý	k2eAgInPc7d3	nejstarší
literárními	literární	k2eAgInPc7d1	literární
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInPc3	jaký
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
pojednávající	pojednávající	k2eAgInSc4d1	pojednávající
o	o	k7c6	o
mýtickém	mýtický	k2eAgMnSc6d1	mýtický
králi	král	k1gMnSc6	král
města	město	k1gNnSc2	město
Uruku	Uruk	k1gInSc2	Uruk
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Politické	politický	k2eAgNnSc1d1	politické
vedení	vedení	k1gNnSc1	vedení
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
náleželo	náležet	k5eAaImAgNnS	náležet
kněžím	kněz	k1gMnPc3	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
třímali	třímat	k5eAaImAgMnP	třímat
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Architektonickým	architektonický	k2eAgInSc7d1	architektonický
projevem	projev	k1gInSc7	projev
sumerského	sumerský	k2eAgNnSc2d1	sumerské
polyteistického	polyteistický	k2eAgNnSc2d1	polyteistické
náboženství	náboženství	k1gNnSc2	náboženství
byly	být	k5eAaImAgInP	být
až	až	k6eAd1	až
padesát	padesát	k4xCc4	padesát
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
stupňovité	stupňovitý	k2eAgFnSc2d1	stupňovitá
pyramidy	pyramida	k1gFnSc2	pyramida
–	–	k?	–
zikkuraty	zikkurat	k1gInPc1	zikkurat
–	–	k?	–
které	který	k3yRgFnPc1	který
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
paláce	palác	k1gInPc1	palác
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
plnily	plnit	k5eAaImAgInP	plnit
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
poddaným	poddaný	k1gMnPc3	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
města	město	k1gNnPc4	město
sice	sice	k8xC	sice
vedla	vést	k5eAaImAgFnS	vést
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
sousedy	soused	k1gMnPc7	soused
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
války	válka	k1gFnSc2	válka
o	o	k7c4	o
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnPc1d1	obchodní
stezky	stezka	k1gFnPc1	stezka
a	a	k8xC	a
tribut	tribut	k1gInSc1	tribut
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tyto	tento	k3xDgInPc1	tento
konflikty	konflikt	k1gInPc1	konflikt
nabývaly	nabývat	k5eAaImAgInP	nabývat
jen	jen	k6eAd1	jen
lokální	lokální	k2eAgInSc4d1	lokální
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nestaly	stát	k5eNaPmAgFnP	stát
impulzem	impulz	k1gInSc7	impulz
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
větších	veliký	k2eAgFnPc2d2	veliký
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začalo	začít	k5eAaPmAgNnS	začít
bohatství	bohatství	k1gNnSc4	bohatství
Sumerů	Sumer	k1gInPc2	Sumer
přitahovat	přitahovat	k5eAaImF	přitahovat
pozornost	pozornost	k1gFnSc4	pozornost
nomádských	nomádský	k2eAgInPc2d1	nomádský
kmenů	kmen	k1gInPc2	kmen
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
hojný	hojný	k2eAgInSc1d1	hojný
výskyt	výskyt	k1gInSc1	výskyt
semitských	semitský	k2eAgNnPc2d1	semitské
jmen	jméno	k1gNnPc2	jméno
v	v	k7c6	v
sumerských	sumerský	k2eAgInPc6d1	sumerský
seznamech	seznam	k1gInPc6	seznam
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
sumerština	sumerština	k1gFnSc1	sumerština
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
jazykové	jazykový	k2eAgFnSc3d1	jazyková
větvi	větev	k1gFnSc3	větev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
zničením	zničení	k1gNnSc7	zničení
monumentálních	monumentální	k2eAgInPc2d1	monumentální
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
doposud	doposud	k6eAd1	doposud
jednotné	jednotný	k2eAgFnSc6d1	jednotná
světské	světský	k2eAgFnSc3d1	světská
a	a	k8xC	a
duchovní	duchovní	k2eAgFnSc3d1	duchovní
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vládcové	vládce	k1gMnPc1	vládce
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
stavět	stavět	k5eAaImF	stavět
obrovské	obrovský	k2eAgInPc4d1	obrovský
paláce	palác	k1gInPc4	palác
s	s	k7c7	s
hroby	hrob	k1gInPc7	hrob
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
byli	být	k5eAaImAgMnP	být
pohřbíváni	pohřbíván	k2eAgMnPc1d1	pohřbíván
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgNnSc2	tento
protodynastického	protodynastický	k2eAgNnSc2d1	protodynastický
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
sumerský	sumerský	k2eAgInSc1d1	sumerský
vliv	vliv	k1gInSc1	vliv
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
při	při	k7c6	při
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Sumeru	Sumer	k1gInSc2	Sumer
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
došlo	dojít	k5eAaPmAgNnS	dojít
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
sjednocení	sjednocení	k1gNnSc3	sjednocení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
napomohla	napomoct	k5eAaPmAgFnS	napomoct
geograficky	geograficky	k6eAd1	geograficky
daná	daný	k2eAgFnSc1d1	daná
izolovanost	izolovanost	k1gFnSc1	izolovanost
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
poskytující	poskytující	k2eAgFnSc4d1	poskytující
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Četná	četný	k2eAgNnPc1d1	četné
malá	malý	k2eAgNnPc1d1	malé
království	království	k1gNnPc1	království
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
seskupila	seskupit	k5eAaPmAgNnP	seskupit
ve	v	k7c4	v
dva	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nechen	Nechna	k1gFnPc2	Nechna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dolní	dolní	k2eAgInSc1d1	dolní
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Peruadžet	Peruadžet	k1gFnSc2	Peruadžet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
trvajícího	trvající	k2eAgInSc2d1	trvající
procesu	proces	k1gInSc2	proces
sjednocena	sjednotit	k5eAaPmNgFnS	sjednotit
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
32	[number]	k4	32
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
hornoegyptského	hornoegyptský	k2eAgMnSc2d1	hornoegyptský
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
dochovaných	dochovaný	k2eAgInPc6d1	dochovaný
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
z	z	k7c2	z
archaického	archaický	k2eAgNnSc2d1	archaické
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
různá	různý	k2eAgNnPc4d1	různé
jména	jméno	k1gNnPc4	jméno
vládců	vládce	k1gMnPc2	vládce
jako	jako	k9	jako
Škorpión	škorpión	k1gMnSc1	škorpión
<g/>
,	,	kIx,	,
Narmer	Narmer	k1gMnSc1	Narmer
a	a	k8xC	a
Aha	aha	k1gNnSc1	aha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
pozdějších	pozdní	k2eAgInPc2d2	pozdější
pramenů	pramen	k1gInPc2	pramen
označují	označovat	k5eAaImIp3nP	označovat
jedinou	jediný	k2eAgFnSc4d1	jediná
postavu	postava	k1gFnSc4	postava
–	–	k?	–
faraóna	faraón	k1gMnSc4	faraón
Meniho	Meni	k1gMnSc4	Meni
<g/>
,	,	kIx,	,
sjednotitele	sjednotitel	k1gMnSc4	sjednotitel
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
archaického	archaický	k2eAgNnSc2d1	archaické
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
28	[number]	k4	28
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
sahala	sahat	k5eAaImAgFnS	sahat
moc	moc	k1gFnSc1	moc
faraonů	faraon	k1gMnPc2	faraon
až	až	k9	až
na	na	k7c4	na
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
tehdy	tehdy	k6eAd1	tehdy
disponoval	disponovat	k5eAaBmAgInS	disponovat
přímým	přímý	k2eAgNnSc7d1	přímé
obchodním	obchodní	k2eAgNnSc7d1	obchodní
spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
městem	město	k1gNnSc7	město
Byblos	Byblosa	k1gFnPc2	Byblosa
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
dalekém	daleký	k2eAgInSc6d1	daleký
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
panovníci	panovník	k1gMnPc1	panovník
2	[number]	k4	2
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
opírali	opírat	k5eAaImAgMnP	opírat
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
o	o	k7c4	o
soustavu	soustava	k1gFnSc4	soustava
centrálně	centrálně	k6eAd1	centrálně
řízených	řízený	k2eAgInPc2d1	řízený
správních	správní	k2eAgInPc2d1	správní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
kult	kult	k1gInSc1	kult
vlastního	vlastní	k2eAgNnSc2d1	vlastní
božství	božství	k1gNnSc2	božství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
prohlašovali	prohlašovat	k5eAaImAgMnP	prohlašovat
za	za	k7c4	za
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
náboženství	náboženství	k1gNnSc1	náboženství
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
rovněž	rovněž	k9	rovněž
uctíváním	uctívání	k1gNnSc7	uctívání
božských	božský	k2eAgNnPc2d1	božské
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
býci	býk	k1gMnPc1	býk
a	a	k8xC	a
krávy	kráva	k1gFnPc1	kráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
silná	silný	k2eAgFnSc1d1	silná
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
posmrtný	posmrtný	k2eAgInSc4d1	posmrtný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Sakkáře	Sakkář	k1gMnPc4	Sakkář
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
Mennoferu	Mennofera	k1gFnSc4	Mennofera
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Abydu	Abyd	k1gInSc6	Abyd
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
ohromné	ohromný	k2eAgFnPc4d1	ohromná
náboženské	náboženský	k2eAgFnPc4d1	náboženská
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
hrobky	hrobka	k1gFnPc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
Egypt	Egypt	k1gInSc1	Egypt
tedy	tedy	k9	tedy
již	již	k6eAd1	již
v	v	k7c6	v
archaickém	archaický	k2eAgNnSc6d1	archaické
období	období	k1gNnSc6	období
disponoval	disponovat	k5eAaBmAgInS	disponovat
vysoce	vysoce	k6eAd1	vysoce
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hieroglyfické	hieroglyfický	k2eAgNnSc1d1	hieroglyfické
písmo	písmo	k1gNnSc1	písmo
nalezlo	nalézt	k5eAaBmAgNnS	nalézt
své	své	k1gNnSc4	své
uplatnění	uplatnění	k1gNnSc2	uplatnění
v	v	k7c6	v
náboženských	náboženský	k2eAgFnPc6d1	náboženská
a	a	k8xC	a
správních	správní	k2eAgFnPc6d1	správní
záležitostech	záležitost	k1gFnPc6	záležitost
(	(	kIx(	(
<g/>
daňový	daňový	k2eAgInSc4d1	daňový
registr	registr	k1gInSc4	registr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
měrou	míra	k1gFnSc7wR	míra
také	také	k9	také
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2707	[number]	k4	2707
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgInS	znamenat
počátek	počátek	k1gInSc4	počátek
období	období	k1gNnSc2	období
staré	starý	k2eAgFnSc2d1	stará
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dále	daleko	k6eAd2	daleko
nerušeně	nerušeně	k6eAd1	nerušeně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
a	a	k8xC	a
po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
delty	delta	k1gFnSc2	delta
řeky	řeka	k1gFnSc2	řeka
Nilu	Nil	k1gInSc2	Nil
expandovala	expandovat	k5eAaImAgFnS	expandovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
–	–	k?	–
do	do	k7c2	do
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnPc1d1	vládní
a	a	k8xC	a
správní	správní	k2eAgFnPc1d1	správní
struktury	struktura	k1gFnPc1	struktura
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
egyptská	egyptský	k2eAgFnSc1d1	egyptská
kultura	kultura	k1gFnSc1	kultura
získaly	získat	k5eAaPmAgFnP	získat
svoji	svůj	k3xOyFgFnSc4	svůj
definitivní	definitivní	k2eAgFnSc4d1	definitivní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
etapa	etapa	k1gFnSc1	etapa
vývoje	vývoj	k1gInSc2	vývoj
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
především	především	k9	především
stavbou	stavba	k1gFnSc7	stavba
monumentálních	monumentální	k2eAgFnPc2d1	monumentální
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgMnSc2	svůj
vrcholu	vrchol	k1gInSc6	vrchol
budováním	budování	k1gNnSc7	budování
pyramid	pyramida	k1gFnPc2	pyramida
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
staviteli	stavitel	k1gMnPc7	stavitel
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byli	být	k5eAaImAgMnP	být
faraoni	faraon	k1gMnPc1	faraon
Džoser	Džoser	k1gMnSc1	Džoser
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
stupňovitou	stupňovitý	k2eAgFnSc4d1	stupňovitá
pyramidu	pyramida	k1gFnSc4	pyramida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Snofru	Snofr	k1gInSc2	Snofr
<g/>
,	,	kIx,	,
Chufu	Chuf	k1gInSc2	Chuf
a	a	k8xC	a
Rachef	Rachef	k1gInSc4	Rachef
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dokonalosti	dokonalost	k1gFnSc6	dokonalost
těchto	tento	k3xDgFnPc2	tento
gigantických	gigantický	k2eAgFnPc2d1	gigantická
architektonických	architektonický	k2eAgFnPc2d1	architektonická
staveb	stavba	k1gFnPc2	stavba
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
umělecká	umělecký	k2eAgFnSc1d1	umělecká
výzdoba	výzdoba	k1gFnSc1	výzdoba
jejich	jejich	k3xOp3gInSc2	jejich
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
své	svůj	k3xOyFgFnPc4	svůj
vrcholné	vrcholný	k2eAgFnPc4d1	vrcholná
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
nabyl	nabýt	k5eAaPmAgMnS	nabýt
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
sluneční	sluneční	k2eAgMnSc1d1	sluneční
bůh	bůh	k1gMnSc1	bůh
Re	re	k9	re
<g/>
.	.	kIx.	.
</s>
<s>
Faraon	faraon	k1gMnSc1	faraon
Radžedef	Radžedef	k1gMnSc1	Radžedef
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
synem	syn	k1gMnSc7	syn
Re	re	k9	re
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
5	[number]	k4	5
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
Re	re	k9	re
plně	plně	k6eAd1	plně
zastínil	zastínit	k5eAaPmAgInS	zastínit
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
kult	kult	k1gInSc1	kult
boha	bůh	k1gMnSc2	bůh
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
měď	měď	k1gFnSc1	měď
pozvolna	pozvolna	k6eAd1	pozvolna
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
bronzem	bronz	k1gInSc7	bronz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
bylo	být	k5eAaImAgNnS	být
vedle	vedle	k7c2	vedle
všudypřítomné	všudypřítomný	k2eAgFnSc2d1	všudypřítomná
mědi	měď	k1gFnSc2	měď
zapotřebí	zapotřebí	k6eAd1	zapotřebí
také	také	k9	také
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
dovážen	dovážit	k5eAaPmNgInS	dovážit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
z	z	k7c2	z
území	území	k1gNnSc2	území
nynějšího	nynější	k2eAgInSc2d1	nynější
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
používání	používání	k1gNnSc1	používání
mědi	měď	k1gFnSc2	měď
i	i	k8xC	i
bronzu	bronz	k1gInSc2	bronz
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
relativně	relativně	k6eAd1	relativně
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
zde	zde	k6eAd1	zde
nikdy	nikdy	k6eAd1	nikdy
nezískala	získat	k5eNaPmAgFnS	získat
na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
tolik	tolik	k4xDc1	tolik
jako	jako	k8xC	jako
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
zřejmě	zřejmě	k6eAd1	zřejmě
vůbec	vůbec	k9	vůbec
nevyráběl	vyrábět	k5eNaImAgMnS	vyrábět
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dovážel	dovážet	k5eAaImAgMnS	dovážet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jak	jak	k6eAd1	jak
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
představovalo	představovat	k5eAaImAgNnS	představovat
vážný	vážný	k2eAgInSc4d1	vážný
problém	problém	k1gInSc4	problém
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Egypt	Egypt	k1gInSc1	Egypt
postrádal	postrádat	k5eAaImAgInS	postrádat
nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
spoléhat	spoléhat	k5eAaImF	spoléhat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
dovoz	dovoz	k1gInSc4	dovoz
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
železa	železo	k1gNnSc2	železo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
šířilo	šířit	k5eAaImAgNnS	šířit
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
relativní	relativní	k2eAgInSc4d1	relativní
zaostávání	zaostávání	k1gNnSc4	zaostávání
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Akkad	Akkad	k1gInSc1	Akkad
<g/>
,	,	kIx,	,
Babylonie	Babylonie	k1gFnSc1	Babylonie
a	a	k8xC	a
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sumerské	sumerský	k2eAgFnSc2d1	sumerská
kultury	kultura	k1gFnSc2	kultura
nastaly	nastat	k5eAaPmAgFnP	nastat
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pronikavé	pronikavý	k2eAgFnPc4d1	pronikavá
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Sargon	Sargon	k1gMnSc1	Sargon
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
města	město	k1gNnSc2	město
Akkadu	Akkad	k1gInSc2	Akkad
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
sumerského	sumerský	k2eAgNnSc2d1	sumerské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
mocnou	mocný	k2eAgFnSc4d1	mocná
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
když	když	k8xS	když
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
celou	celý	k2eAgFnSc4d1	celá
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Sargon	Sargon	k1gMnSc1	Sargon
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vynikající	vynikající	k2eAgMnSc1d1	vynikající
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
moci	moc	k1gFnSc3	moc
mu	on	k3xPp3gNnSc3	on
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
také	také	k9	také
vojenská	vojenský	k2eAgFnSc1d1	vojenská
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
bylo	být	k5eAaImAgNnS	být
akkadské	akkadský	k2eAgNnSc1d1	akkadské
vojsko	vojsko	k1gNnSc1	vojsko
mobilnější	mobilní	k2eAgFnSc2d2	mobilnější
než	než	k8xS	než
vojska	vojsko	k1gNnSc2	vojsko
ostatních	ostatní	k2eAgNnPc2d1	ostatní
sumerských	sumerský	k2eAgNnPc2d1	sumerské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
převyšovalo	převyšovat	k5eAaImAgNnS	převyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
Sargon	Sargona	k1gFnPc2	Sargona
vládl	vládnout	k5eAaImAgMnS	vládnout
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
od	od	k7c2	od
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
až	až	k9	až
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisováno	připisován	k2eAgNnSc1d1	připisováno
rovněž	rovněž	k9	rovněž
první	první	k4xOgNnSc4	první
nasazení	nasazení	k1gNnSc4	nasazení
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
krále	král	k1gMnSc2	král
Sargona	Sargon	k1gMnSc2	Sargon
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
nestabilita	nestabilita	k1gFnSc1	nestabilita
a	a	k8xC	a
vnější	vnější	k2eAgNnSc1d1	vnější
ohrožení	ohrožení	k1gNnSc1	ohrožení
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
někdejších	někdejší	k2eAgInPc2d1	někdejší
sumerských	sumerský	k2eAgInPc2d1	sumerský
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
však	však	k9	však
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
pronikaly	pronikat	k5eAaImAgFnP	pronikat
okolní	okolní	k2eAgInPc4d1	okolní
kmeny	kmen	k1gInPc4	kmen
Elamitů	Elamita	k1gMnPc2	Elamita
a	a	k8xC	a
Amorejců	Amorejec	k1gMnPc2	Amorejec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Sumeru	Sumer	k1gInSc2	Sumer
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
mocné	mocný	k2eAgFnPc1d1	mocná
říše	říš	k1gFnPc1	říš
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
sumerské	sumerský	k2eAgFnSc2d1	sumerská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
byl	být	k5eAaImAgMnS	být
přesto	přesto	k8xC	přesto
pozvolný	pozvolný	k2eAgMnSc1d1	pozvolný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
sumerští	sumerský	k2eAgMnPc1d1	sumerský
vládci	vládce	k1gMnPc1	vládce
se	se	k3xPyFc4	se
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Sargona	Sargon	k1gMnSc2	Sargon
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
obnovit	obnovit	k5eAaPmF	obnovit
jednotu	jednota	k1gFnSc4	jednota
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Nejmocnějším	mocný	k2eAgNnSc7d3	nejmocnější
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejprve	nejprve	k6eAd1	nejprve
Larsa	Larsa	k1gFnSc1	Larsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
dobyta	dobýt	k5eAaPmNgFnS	dobýt
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepříliš	příliš	k6eNd1	příliš
významným	významný	k2eAgNnSc7d1	významné
městem	město	k1gNnSc7	město
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgMnS	být
Chammurapi	Chammurap	k1gFnSc2	Chammurap
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
přednostního	přednostní	k2eAgNnSc2d1	přednostní
mocenského	mocenský	k2eAgNnSc2d1	mocenské
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
zde	zde	k6eAd1	zde
druhou	druhý	k4xOgFnSc4	druhý
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
velikosti	velikost	k1gFnPc4	velikost
někdejší	někdejší	k2eAgFnSc2d1	někdejší
říše	říš	k1gFnSc2	říš
akkadské	akkadský	k2eAgFnSc2d1	akkadská
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Chammurapiho	Chammurapize	k6eAd1	Chammurapize
výjimečnost	výjimečnost	k1gFnSc1	výjimečnost
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechal	nechat	k5eAaPmAgMnS	nechat
vytvořit	vytvořit	k5eAaPmF	vytvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
zákoníků	zákoník	k1gInPc2	zákoník
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
zákoník	zákoník	k1gInSc4	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
následný	následný	k2eAgInSc4d1	následný
zánik	zánik	k1gInSc4	zánik
babylonské	babylonský	k2eAgFnSc2d1	Babylonská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nepostihl	postihnout	k5eNaPmAgInS	postihnout
samotný	samotný	k2eAgInSc1d1	samotný
Babylon	Babylon	k1gInSc1	Babylon
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Akkadu	Akkad	k1gInSc2	Akkad
úpadek	úpadek	k1gInSc1	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
povznesl	povznést	k5eAaPmAgInS	povznést
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
největšího	veliký	k2eAgMnSc2d3	veliký
a	a	k8xC	a
nejmocnějšího	mocný	k2eAgNnSc2d3	nejmocnější
města	město	k1gNnSc2	město
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
íránského	íránský	k2eAgNnSc2d1	íránské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ke	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
kmene	kmen	k1gInSc2	kmen
Kassitů	Kassit	k1gInPc2	Kassit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podnikali	podnikat	k5eAaImAgMnP	podnikat
stále	stále	k6eAd1	stále
častější	častý	k2eAgInPc4d2	častější
vpády	vpád	k1gInPc4	vpád
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
smrtící	smrtící	k2eAgInSc1d1	smrtící
úder	úder	k1gInSc1	úder
přišel	přijít	k5eAaPmAgInS	přijít
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Chetité	Chetita	k1gMnPc1	Chetita
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mocnou	mocný	k2eAgFnSc4d1	mocná
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
Babylon	Babylon	k1gInSc4	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
strašlivě	strašlivě	k6eAd1	strašlivě
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Slabosti	slabost	k1gFnPc1	slabost
babylonské	babylonský	k2eAgFnSc2d1	Babylonská
říše	říš	k1gFnSc2	říš
však	však	k9	však
využili	využít	k5eAaPmAgMnP	využít
Kassité	Kassita	k1gMnPc1	Kassita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ustavili	ustavit	k5eAaPmAgMnP	ustavit
pány	pan	k1gMnPc7	pan
Babylonu	babylon	k1gInSc2	babylon
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Kassité	Kassita	k1gMnPc1	Kassita
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Babylon	Babylon	k1gInSc4	Babylon
<g/>
,	,	kIx,	,
moc	moc	k1gFnSc4	moc
chetitské	chetitský	k2eAgFnSc2d1	Chetitská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Chetité	Chetita	k1gMnPc1	Chetita
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
říší	říš	k1gFnSc7	říš
Mitanni	Mitann	k1gMnPc1	Mitann
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
území	území	k1gNnSc4	území
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
až	až	k9	až
k	k	k7c3	k
syrskému	syrský	k2eAgNnSc3d1	syrské
pobřeží	pobřeží	k1gNnSc3	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
rozmachu	rozmach	k1gInSc3	rozmach
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
počátky	počátek	k1gInPc1	počátek
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
akkadské	akkadský	k2eAgFnSc2d1	akkadská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
zde	zde	k6eAd1	zde
jednotný	jednotný	k2eAgInSc1d1	jednotný
<g/>
,	,	kIx,	,
centralizovaný	centralizovaný	k2eAgInSc1d1	centralizovaný
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
několik	několik	k4yIc1	několik
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Ugarit	Ugarit	k1gInSc1	Ugarit
<g/>
,	,	kIx,	,
Arados	Arados	k1gMnSc1	Arados
<g/>
,	,	kIx,	,
Byblos	Byblos	k1gMnSc1	Byblos
a	a	k8xC	a
Týros	Týros	k1gMnSc1	Týros
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
obchodem	obchod	k1gInSc7	obchod
domohla	domoct	k5eAaPmAgFnS	domoct
značného	značný	k2eAgNnSc2d1	značné
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
23	[number]	k4	23
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
první	první	k4xOgFnSc1	první
přechodná	přechodný	k2eAgFnSc1d1	přechodná
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgFnSc1d1	stávající
moc	moc	k1gFnSc1	moc
faraónů	faraón	k1gMnPc2	faraón
dočasně	dočasně	k6eAd1	dočasně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
do	do	k7c2	do
několika	několik	k4yIc2	několik
menších	malý	k2eAgInPc2d2	menší
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Nástupem	nástup	k1gInSc7	nástup
11	[number]	k4	11
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
o	o	k7c4	o
200	[number]	k4	200
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zahájil	zahájit	k5eAaPmAgInS	zahájit
Egypt	Egypt	k1gInSc1	Egypt
etapu	etapa	k1gFnSc4	etapa
střední	střední	k2eAgFnSc1d1	střední
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Faraón	faraón	k1gMnSc1	faraón
Amenemhet	Amenemhet	k1gMnSc1	Amenemhet
I.	I.	kA	I.
podporoval	podporovat	k5eAaImAgMnS	podporovat
vzestup	vzestup	k1gInSc4	vzestup
kultu	kult	k1gInSc2	kult
boha	bůh	k1gMnSc2	bůh
Amona	Amon	k1gMnSc2	Amon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
egyptským	egyptský	k2eAgNnSc7d1	egyptské
božstvem	božstvo	k1gNnSc7	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
říše	říše	k1gFnSc1	říše
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
posunuty	posunut	k2eAgFnPc1d1	posunuta
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
prožíval	prožívat	k5eAaImAgInS	prožívat
období	období	k1gNnSc4	období
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispěly	přispět	k5eAaPmAgInP	přispět
jak	jak	k9	jak
intenzivní	intenzivní	k2eAgInPc1d1	intenzivní
obchodní	obchodní	k2eAgInPc1d1	obchodní
styky	styk	k1gInPc1	styk
s	s	k7c7	s
Féničany	Féničan	k1gMnPc7	Féničan
<g/>
,	,	kIx,	,
tak	tak	k9	tak
odvodnění	odvodnění	k1gNnSc4	odvodnění
fajjúmské	fajjúmský	k2eAgFnSc2d1	fajjúmský
oázy	oáza	k1gFnSc2	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Faraón	faraón	k1gMnSc1	faraón
Senusret	Senusret	k1gMnSc1	Senusret
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
doposud	doposud	k6eAd1	doposud
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
vládcem	vládce	k1gMnSc7	vládce
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Epochu	epocha	k1gFnSc4	epocha
rozkvětu	rozkvět	k1gInSc2	rozkvět
a	a	k8xC	a
prosperity	prosperita	k1gFnSc2	prosperita
však	však	k9	však
ukončil	ukončit	k5eAaPmAgInS	ukončit
vpád	vpád	k1gInSc1	vpád
semitských	semitský	k2eAgMnPc2d1	semitský
Hyksósů	Hyksós	k1gMnPc2	Hyksós
do	do	k7c2	do
delty	delta	k1gFnSc2	delta
Nilu	Nil	k1gInSc2	Nil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Egyptu	Egypt	k1gInSc3	Egypt
trvalo	trvat	k5eAaImAgNnS	trvat
další	další	k2eAgNnSc4d1	další
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
než	než	k8xS	než
se	se	k3xPyFc4	se
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
cizí	cizí	k2eAgFnSc7d1	cizí
mocí	moc	k1gFnSc7	moc
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c4	nad
Hyksósy	Hyksós	k1gMnPc4	Hyksós
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
nová	nový	k2eAgFnSc1d1	nová
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
expanzi	expanze	k1gFnSc6	expanze
do	do	k7c2	do
Núbie	Núbie	k1gFnSc2	Núbie
nasměrovala	nasměrovat	k5eAaPmAgFnS	nasměrovat
své	svůj	k3xOyFgInPc4	svůj
další	další	k2eAgInPc4d1	další
výboje	výboj	k1gInPc4	výboj
do	do	k7c2	do
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
oplývající	oplývající	k2eAgFnSc1d1	oplývající
nerostnými	nerostný	k2eAgFnPc7d1	nerostná
surovinami	surovina	k1gFnPc7	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Kanaán	Kanaán	k1gInSc1	Kanaán
<g/>
,	,	kIx,	,
Fénicie	Fénicie	k1gFnSc1	Fénicie
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
syrská	syrský	k2eAgNnPc1d1	syrské
města	město	k1gNnPc1	město
byla	být	k5eAaImAgNnP	být
podrobena	podrobit	k5eAaPmNgNnP	podrobit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
dobyvačná	dobyvačný	k2eAgFnSc1d1	dobyvačná
egyptská	egyptský	k2eAgFnSc1d1	egyptská
politika	politika	k1gFnSc1	politika
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
faraóna	faraón	k1gMnSc2	faraón
Thutmose	Thutmosa	k1gFnSc3	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1479	[number]	k4	1479
<g/>
-	-	kIx~	-
<g/>
1425	[number]	k4	1425
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
posunul	posunout	k5eAaPmAgMnS	posunout
hranice	hranice	k1gFnPc4	hranice
Egypta	Egypt	k1gInSc2	Egypt
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Eufratu	Eufrat	k1gInSc2	Eufrat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
faraónů	faraón	k1gMnPc2	faraón
18	[number]	k4	18
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Egypt	Egypt	k1gInSc1	Egypt
svého	svůj	k3xOyFgInSc2	svůj
historicky	historicky	k6eAd1	historicky
největšího	veliký	k2eAgInSc2d3	veliký
rozsahu	rozsah	k1gInSc2	rozsah
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Amenhotep	Amenhotep	k1gMnSc1	Amenhotep
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
spíše	spíše	k9	spíše
jako	jako	k9	jako
Achnaton	Achnaton	k1gInSc4	Achnaton
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zavést	zavést	k5eAaPmF	zavést
náboženské	náboženský	k2eAgFnPc4d1	náboženská
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
značný	značný	k2eAgInSc4d1	značný
odpor	odpor	k1gInSc4	odpor
především	především	k9	především
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
vlivných	vlivný	k2eAgMnPc2d1	vlivný
kněží	kněz	k1gMnPc2	kněz
boha	bůh	k1gMnSc2	bůh
Amona	Amon	k1gMnSc2	Amon
<g/>
.	.	kIx.	.
</s>
<s>
Achnaton	Achnaton	k1gInSc1	Achnaton
chtěl	chtít	k5eAaImAgInS	chtít
prosadit	prosadit	k5eAaPmF	prosadit
monoteistické	monoteistický	k2eAgNnSc4d1	monoteistické
náboženství	náboženství	k1gNnSc4	náboženství
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
Atona	Aton	k1gMnSc2	Aton
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Vesetu	Veseto	k1gNnSc6	Veseto
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
nově	nově	k6eAd1	nově
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
Achetatonu	Achetaton	k1gInSc2	Achetaton
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
král	král	k1gMnSc1	král
udržoval	udržovat	k5eAaImAgMnS	udržovat
výtečné	výtečný	k2eAgInPc4d1	výtečný
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c6	o
nastolení	nastolení	k1gNnSc6	nastolení
jediného	jediný	k2eAgNnSc2d1	jediné
božstva	božstvo	k1gNnSc2	božstvo
však	však	k9	však
přišlo	přijít	k5eAaPmAgNnS	přijít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
vniveč	vniveč	k6eAd1	vniveč
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
navrátil	navrátit	k5eAaPmAgMnS	navrátit
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
starých	starý	k2eAgMnPc2d1	starý
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ústřední	ústřední	k2eAgNnSc4d1	ústřední
postavení	postavení	k1gNnPc4	postavení
zaujímal	zaujímat	k5eAaImAgInS	zaujímat
kult	kult	k1gInSc1	kult
boha	bůh	k1gMnSc4	bůh
Amona	Amon	k1gInSc2	Amon
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Kréta	Kréta	k1gFnSc1	Kréta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
centrem	centrum	k1gNnSc7	centrum
vyspělé	vyspělý	k2eAgFnSc2d1	vyspělá
kultury	kultura	k1gFnSc2	kultura
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
mínojská	mínojský	k2eAgNnPc4d1	mínojské
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jen	jen	k9	jen
omezenou	omezený	k2eAgFnSc4d1	omezená
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
této	tento	k3xDgFnSc2	tento
vysoce	vysoce	k6eAd1	vysoce
rozvinuté	rozvinutý	k2eAgFnSc2d1	rozvinutá
krétské	krétský	k2eAgFnSc2d1	krétská
palácové	palácový	k2eAgFnSc2d1	palácová
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
struktury	struktura	k1gFnPc1	struktura
tamější	tamější	k2eAgFnSc2d1	tamější
společnosti	společnost	k1gFnSc2	společnost
nám	my	k3xPp1nPc3	my
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
dosud	dosud	k6eAd1	dosud
skryty	skryt	k2eAgFnPc1d1	skryta
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
nabyla	nabýt	k5eAaPmAgFnS	nabýt
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kréťané	Kréťan	k1gMnPc1	Kréťan
vedli	vést	k5eAaImAgMnP	vést
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koloniální	koloniální	k2eAgFnSc4d1	koloniální
expanzi	expanze	k1gFnSc4	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Kréta	Kréta	k1gFnSc1	Kréta
přední	přední	k2eAgFnSc1d1	přední
mocností	mocnost	k1gFnSc7	mocnost
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tato	tento	k3xDgFnSc1	tento
kultura	kultura	k1gFnSc1	kultura
náhle	náhle	k6eAd1	náhle
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
doklad	doklad	k1gInSc4	doklad
o	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Kréta	Kréta	k1gFnSc1	Kréta
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
oblasti	oblast	k1gFnSc2	oblast
působení	působení	k1gNnSc4	působení
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
především	především	k6eAd1	především
na	na	k7c6	na
území	území	k1gNnSc6	území
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
malých	malý	k2eAgNnPc2d1	malé
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
Mykény	Mykény	k1gFnPc1	Mykény
<g/>
,	,	kIx,	,
Pylos	Pylos	k1gInSc1	Pylos
a	a	k8xC	a
Athény	Athéna	k1gFnPc1	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
svého	své	k1gNnSc2	své
doposud	doposud	k6eAd1	doposud
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
byli	být	k5eAaImAgMnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
agresivními	agresivní	k2eAgMnPc7d1	agresivní
a	a	k8xC	a
krutými	krutý	k2eAgMnPc7d1	krutý
nájezdníky	nájezdník	k1gMnPc7	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
romantizující	romantizující	k2eAgNnSc4d1	romantizující
vylíčení	vylíčení	k1gNnSc4	vylíčení
mykénských	mykénský	k2eAgFnPc2d1	mykénská
loupeživých	loupeživý	k2eAgFnPc2d1	loupeživá
výprav	výprava	k1gFnPc2	výprava
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začalo	začít	k5eAaPmAgNnS	začít
nabírat	nabírat	k5eAaImF	nabírat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
zpracování	zpracování	k1gNnSc2	zpracování
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
však	však	k9	však
probíhal	probíhat	k5eAaImAgInS	probíhat
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
železo	železo	k1gNnSc4	železo
zcela	zcela	k6eAd1	zcela
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
používání	používání	k1gNnSc1	používání
bronzu	bronz	k1gInSc2	bronz
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chetité	Chetita	k1gMnPc1	Chetita
<g/>
.	.	kIx.	.
</s>
<s>
Chetitské	chetitský	k2eAgInPc1d1	chetitský
výboje	výboj	k1gInPc1	výboj
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
Chetité	Chetita	k1gMnPc1	Chetita
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
syrských	syrský	k2eAgNnPc2d1	syrské
měst	město	k1gNnPc2	město
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1274	[number]	k4	1274
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Kadeše	Kadeše	k1gFnSc2	Kadeše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
událost	událost	k1gFnSc4	událost
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
neměla	mít	k5eNaImAgFnS	mít
jasného	jasný	k2eAgMnSc4d1	jasný
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
je	být	k5eAaImIp3nS	být
významnější	významný	k2eAgFnSc7d2	významnější
událostí	událost	k1gFnSc7	událost
následná	následný	k2eAgFnSc1d1	následná
první	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doložená	doložený	k2eAgFnSc1d1	doložená
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zakotvovala	zakotvovat	k5eAaImAgFnS	zakotvovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c7	mezi
Egypťany	Egypťan	k1gMnPc7	Egypťan
a	a	k8xC	a
Chetity	Chetit	k1gMnPc7	Chetit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
úsilím	úsilí	k1gNnSc7	úsilí
Asyřanů	Asyřan	k1gMnPc2	Asyřan
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
vlastní	vlastní	k2eAgFnSc2d1	vlastní
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Válečná	válečný	k2eAgNnPc4d1	válečné
tažení	tažení	k1gNnPc4	tažení
asyrských	asyrský	k2eAgMnPc2d1	asyrský
králů	král	k1gMnPc2	král
ze	z	k7c2	z
svého	své	k1gNnSc2	své
původního	původní	k2eAgNnSc2d1	původní
území	území	k1gNnSc2	území
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Aššúr	Aššúr	k1gMnSc1	Aššúr
přivedla	přivést	k5eAaPmAgFnS	přivést
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1300	[number]	k4	1300
–	–	k?	–
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pod	pod	k7c4	pod
jejich	jejich	k3xOp3gFnSc4	jejich
vládu	vláda	k1gFnSc4	vláda
celou	celý	k2eAgFnSc4d1	celá
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Asyřané	Asyřan	k1gMnPc1	Asyřan
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přijmout	přijmout	k5eAaPmF	přijmout
kulturu	kultura	k1gFnSc4	kultura
místního	místní	k2eAgNnSc2d1	místní
mezopotamského	mezopotamský	k2eAgNnSc2d1	Mezopotamské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
zbavit	zbavit	k5eAaPmF	zbavit
nálepky	nálepka	k1gFnPc4	nálepka
cizích	cizí	k2eAgMnPc2d1	cizí
vládců	vládce	k1gMnPc2	vládce
a	a	k8xC	a
dodat	dodat	k5eAaPmF	dodat
tím	ten	k3xDgNnSc7	ten
legitimitu	legitimita	k1gFnSc4	legitimita
svým	svůj	k3xOyFgInSc7	svůj
nárokům	nárok	k1gInPc3	nárok
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
asyrská	asyrský	k2eAgFnSc1d1	Asyrská
říše	říše	k1gFnSc1	říše
řeky	řeka	k1gFnSc2	řeka
Eufratu	Eufrat	k1gInSc2	Eufrat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	on	k3xPp3gFnSc3	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
Chetity	Chetit	k1gMnPc7	Chetit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Egypťané	Egypťan	k1gMnPc1	Egypťan
dokázali	dokázat	k5eAaPmAgMnP	dokázat
i	i	k9	i
Asyřané	Asyřan	k1gMnPc1	Asyřan
urovnávat	urovnávat	k5eAaImF	urovnávat
konflikty	konflikt	k1gInPc4	konflikt
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jejich	jejich	k3xOp3gFnSc2	jejich
obratné	obratný	k2eAgFnSc2d1	obratná
diplomacie	diplomacie	k1gFnSc2	diplomacie
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
mocenské	mocenský	k2eAgFnSc2d1	mocenská
rovnováhy	rovnováha	k1gFnSc2	rovnováha
mezi	mezi	k7c7	mezi
Asýrií	Asýrie	k1gFnSc7	Asýrie
<g/>
,	,	kIx,	,
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Chetity	Chetit	k1gMnPc7	Chetit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
mělo	mít	k5eAaImAgNnS	mít
však	však	k9	však
jen	jen	k6eAd1	jen
krátkého	krátký	k2eAgNnSc2d1	krátké
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
východního	východní	k2eAgNnSc2d1	východní
Středomoří	středomoří	k1gNnSc2	středomoří
objevily	objevit	k5eAaPmAgFnP	objevit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
mořské	mořský	k2eAgInPc1d1	mořský
národy	národ	k1gInPc1	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
menších	malý	k2eAgInPc2d2	menší
kmenových	kmenový	k2eAgInPc2d1	kmenový
svazů	svaz	k1gInPc2	svaz
patrně	patrně	k6eAd1	patrně
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
připisovala	připisovat	k5eAaImAgFnS	připisovat
vina	vina	k1gFnSc1	vina
za	za	k7c4	za
zhroucení	zhroucení	k1gNnSc4	zhroucení
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
chetitské	chetitský	k2eAgFnSc2d1	Chetitská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novějších	nový	k2eAgInPc2d2	novější
názorů	názor	k1gInPc2	názor
však	však	k9	však
byl	být	k5eAaImAgInS	být
pád	pád	k1gInSc4	pád
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgFnPc2	tento
civilizací	civilizace	k1gFnPc2	civilizace
zapříčiněn	zapříčinit	k5eAaPmNgMnS	zapříčinit
spíše	spíše	k9	spíše
lokálními	lokální	k2eAgInPc7d1	lokální
konflikty	konflikt	k1gInPc7	konflikt
a	a	k8xC	a
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozbroji	rozbroj	k1gInPc7	rozbroj
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
útoky	útok	k1gInPc4	útok
mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
zřejmě	zřejmě	k6eAd1	zřejmě
napomohly	napomoct	k5eAaPmAgFnP	napomoct
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgFnPc2	tento
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
příslušníkům	příslušník	k1gMnPc3	příslušník
mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Pelištejcům	Pelištejec	k1gMnPc3	Pelištejec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
usadit	usadit	k5eAaPmF	usadit
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
(	(	kIx(	(
<g/>
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
usídlili	usídlit	k5eAaPmAgMnP	usídlit
také	také	k9	také
Židé	Žid	k1gMnPc1	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
stihli	stihnout	k5eAaPmAgMnP	stihnout
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
faraón	faraón	k1gMnSc1	faraón
Ramesse	Ramesse	k1gFnSc2	Ramesse
III	III	kA	III
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
dokázal	dokázat	k5eAaPmAgMnS	dokázat
navzdory	navzdory	k7c3	navzdory
těžkým	těžký	k2eAgFnPc3d1	těžká
ztrátám	ztráta	k1gFnPc3	ztráta
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
museli	muset	k5eAaImAgMnP	muset
Chetité	Chetita	k1gMnPc1	Chetita
čelit	čelit	k5eAaImF	čelit
invazi	invaze	k1gFnSc3	invaze
Frygů	Fryg	k1gMnPc2	Fryg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
uspíšilo	uspíšit	k5eAaPmAgNnS	uspíšit
násilný	násilný	k2eAgInSc4d1	násilný
zánik	zánik	k1gInSc4	zánik
jejich	jejich	k3xOp3gFnSc2	jejich
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Asyrská	asyrský	k2eAgFnSc1d1	Asyrská
říše	říše	k1gFnSc1	říše
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
neklidu	neklid	k1gInSc2	neklid
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
dočasně	dočasně	k6eAd1	dočasně
vzdát	vzdát	k5eAaPmF	vzdát
pozice	pozice	k1gFnSc1	pozice
hegemona	hegemon	k1gMnSc2	hegemon
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
sílícího	sílící	k2eAgInSc2d1	sílící
Babylonu	babylon	k1gInSc2	babylon
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začaly	začít	k5eAaPmAgFnP	začít
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
sílit	sílit	k5eAaImF	sílit
kmeny	kmen	k1gInPc4	kmen
Aramejců	Aramejec	k1gMnPc2	Aramejec
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
státní	státní	k2eAgNnSc4d1	státní
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
mocenské	mocenský	k2eAgFnPc4d1	mocenská
struktury	struktura	k1gFnPc4	struktura
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
zachovat	zachovat	k5eAaPmF	zachovat
pouze	pouze	k6eAd1	pouze
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ze	z	k7c2	z
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
mořskými	mořský	k2eAgInPc7d1	mořský
národy	národ	k1gInPc7	národ
vyšel	vyjít	k5eAaPmAgMnS	vyjít
značně	značně	k6eAd1	značně
oslabený	oslabený	k2eAgMnSc1d1	oslabený
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Asýrie	Asýrie	k1gFnSc2	Asýrie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
říše	říš	k1gFnSc2	říš
Chetitů	Chetit	k1gMnPc2	Chetit
a	a	k8xC	a
rozkladu	rozklad	k1gInSc2	rozklad
kassitské	kassitský	k2eAgFnSc2d1	kassitský
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Babylonu	Babylon	k1gInSc6	Babylon
<g/>
,	,	kIx,	,
vyvolaném	vyvolaný	k2eAgMnSc6d1	vyvolaný
Aramejci	Aramejec	k1gMnSc6	Aramejec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
předoasijském	předoasijský	k2eAgInSc6d1	předoasijský
prostoru	prostor	k1gInSc6	prostor
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
mocenské	mocenský	k2eAgNnSc1d1	mocenské
vakuum	vakuum	k1gNnSc1	vakuum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
vyplněno	vyplnit	k5eAaPmNgNnS	vyplnit
až	až	k9	až
Asyřany	Asyřan	k1gMnPc4	Asyřan
za	za	k7c4	za
Tiglatpilesara	Tiglatpilesar	k1gMnSc4	Tiglatpilesar
I.	I.	kA	I.
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tomuto	tento	k3xDgMnSc3	tento
panovníkovi	panovník	k1gMnSc3	panovník
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
znovu	znovu	k6eAd1	znovu
rozšířit	rozšířit	k5eAaPmF	rozšířit
asyrskou	asyrský	k2eAgFnSc4d1	Asyrská
říši	říše	k1gFnSc4	říše
až	až	k9	až
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
asyrská	asyrský	k2eAgFnSc1d1	Asyrská
moc	moc	k1gFnSc1	moc
rychle	rychle	k6eAd1	rychle
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
vlivem	vlivem	k7c2	vlivem
pokračujících	pokračující	k2eAgInPc2d1	pokračující
aramejských	aramejský	k2eAgInPc2d1	aramejský
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Asyřané	Asyřan	k1gMnPc1	Asyřan
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
zatlačeni	zatlačen	k2eAgMnPc1d1	zatlačen
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Aššúr	Aššúra	k1gFnPc2	Aššúra
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
bránit	bránit	k5eAaImF	bránit
výbojným	výbojný	k2eAgMnPc3d1	výbojný
aramejským	aramejský	k2eAgMnPc3d1	aramejský
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Tiglatpilesarově	Tiglatpilesarův	k2eAgFnSc6d1	Tiglatpilesarův
smrti	smrt	k1gFnSc6	smrt
dokázal	dokázat	k5eAaPmAgMnS	dokázat
král	král	k1gMnSc1	král
Aššurdán	Aššurdán	k2eAgMnSc1d1	Aššurdán
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obnovit	obnovit	k5eAaPmF	obnovit
asyrskou	asyrský	k2eAgFnSc4d1	Asyrská
expanzi	expanze	k1gFnSc4	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Asyřané	Asyřan	k1gMnPc1	Asyřan
opět	opět	k6eAd1	opět
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výsadního	výsadní	k2eAgNnSc2d1	výsadní
postavení	postavení	k1gNnSc3	postavení
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
podrobili	podrobit	k5eAaPmAgMnP	podrobit
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
od	od	k7c2	od
Urmijského	Urmijský	k2eAgNnSc2d1	Urmijský
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
pohoří	pohoří	k1gNnSc2	pohoří
Zagros	Zagrosa	k1gFnPc2	Zagrosa
až	až	k9	až
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
podmanili	podmanit	k5eAaPmAgMnP	podmanit
Babylon	Babylon	k1gInSc4	Babylon
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc4	Sýrie
a	a	k8xC	a
Palestinu	Palestina	k1gFnSc4	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Zastavili	zastavit	k5eAaPmAgMnP	zastavit
také	také	k9	také
vzestup	vzestup	k1gInSc4	vzestup
říše	říš	k1gFnSc2	říš
Urartu	Urart	k1gInSc2	Urart
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
663	[number]	k4	663
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
Egypt	Egypt	k1gInSc4	Egypt
až	až	k9	až
k	k	k7c3	k
Vesetu	Veseto	k1gNnSc3	Veseto
<g/>
.	.	kIx.	.
</s>
<s>
Novoasyrská	Novoasyrský	k2eAgFnSc1d1	Novoasyrská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
problémy	problém	k1gInPc1	problém
a	a	k8xC	a
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
destabilizovaly	destabilizovat	k5eAaBmAgFnP	destabilizovat
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Babylonu	Babylon	k1gInSc6	Babylon
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vůči	vůči	k7c3	vůči
dobyvatelům	dobyvatel	k1gMnPc3	dobyvatel
obrovská	obrovský	k2eAgFnSc1d1	obrovská
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Médů	Méd	k1gMnPc2	Méd
přivodili	přivodit	k5eAaPmAgMnP	přivodit
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
úplný	úplný	k2eAgInSc1d1	úplný
rozklad	rozklad	k1gInSc4	rozklad
asyrské	asyrský	k2eAgFnSc2d1	Asyrská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Asyrská	asyrský	k2eAgNnPc1d1	asyrské
sídelní	sídelní	k2eAgNnPc1d1	sídelní
města	město	k1gNnPc1	město
Aššúr	Aššúra	k1gFnPc2	Aššúra
a	a	k8xC	a
Ninive	Ninive	k1gNnSc2	Ninive
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zničena	zničit	k5eAaPmNgFnS	zničit
do	do	k7c2	do
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
posledního	poslední	k2eAgMnSc2d1	poslední
asyrského	asyrský	k2eAgMnSc2d1	asyrský
krále	král	k1gMnSc2	král
Aššur-uballita	Aššurballita	k1gFnSc1	Aššur-uballita
II	II	kA	II
<g/>
.	.	kIx.	.
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Babylonu	Babylon	k1gInSc6	Babylon
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Chaldejci	Chaldejec	k1gMnPc1	Chaldejec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Médy	Méd	k1gMnPc7	Méd
dokázali	dokázat	k5eAaPmAgMnP	dokázat
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
na	na	k7c4	na
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
asyrského	asyrský	k2eAgNnSc2d1	asyrské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Nebukadnesara	Nebukadnesar	k1gMnSc2	Nebukadnesar
II	II	kA	II
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
babylonská	babylonský	k2eAgFnSc1d1	Babylonská
říše	říše	k1gFnSc1	říše
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
bezmála	bezmála	k6eAd1	bezmála
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
někdejší	někdejší	k2eAgFnSc7d1	někdejší
říší	říš	k1gFnSc7	říš
asyrských	asyrský	k2eAgMnPc2d1	asyrský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Babylon	Babylon	k1gInSc1	Babylon
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
kulturního	kulturní	k2eAgInSc2d1	kulturní
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Okázalé	okázalý	k2eAgFnPc1d1	okázalá
Nebukadnesarovy	Nebukadnesarův	k2eAgFnPc1d1	Nebukadnesarův
stavby	stavba	k1gFnPc1	stavba
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
zahrnuty	zahrnut	k2eAgFnPc1d1	zahrnuta
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
antických	antický	k2eAgInPc2d1	antický
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
existující	existující	k2eAgInSc1d1	existující
kult	kult	k1gInSc1	kult
boha	bůh	k1gMnSc2	bůh
Marduka	Marduk	k1gMnSc2	Marduk
zažil	zažít	k5eAaPmAgMnS	zažít
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
stavbou	stavba	k1gFnSc7	stavba
devadesát	devadesát	k4xCc4	devadesát
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
zikkuratu	zikkurat	k1gInSc2	zikkurat
–	–	k?	–
biblické	biblický	k2eAgFnSc2d1	biblická
Babylonské	babylonský	k2eAgFnSc2d1	Babylonská
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
tehdy	tehdy	k6eAd1	tehdy
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgMnSc7d3	veliký
a	a	k8xC	a
nejskvělejším	skvělý	k2eAgNnSc7d3	nejskvělejší
městem	město	k1gNnSc7	město
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
skvostného	skvostný	k2eAgInSc2d1	skvostný
rozmachu	rozmach	k1gInSc2	rozmach
však	však	k9	však
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
jen	jen	k9	jen
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
podrobených	podrobený	k2eAgInPc2d1	podrobený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
do	do	k7c2	do
babylonského	babylonský	k2eAgNnSc2d1	babylonské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
říše	říš	k1gFnSc2	říš
Chetitů	Chetit	k1gMnPc2	Chetit
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Gordion	Gordion	k1gInSc1	Gordion
říše	říš	k1gFnSc2	říš
Frygů	Fryg	k1gInPc2	Fryg
<g/>
.	.	kIx.	.
</s>
<s>
Chetité	Chetita	k1gMnPc1	Chetita
samotní	samotný	k2eAgMnPc1d1	samotný
si	se	k3xPyFc3	se
ještě	ještě	k6eAd1	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
udrželi	udržet	k5eAaPmAgMnP	udržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Frygové	Fryg	k1gMnPc1	Fryg
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
museli	muset	k5eAaImAgMnP	muset
utkat	utkat	k5eAaPmF	utkat
se	se	k3xPyFc4	se
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
pronikajícími	pronikající	k2eAgInPc7d1	pronikající
Kimmerii	Kimmerie	k1gFnSc4	Kimmerie
a	a	k8xC	a
Skythy	Skyth	k1gMnPc4	Skyth
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
padli	padnout	k5eAaPmAgMnP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
moci	moc	k1gFnSc2	moc
lýdské	lýdský	k2eAgFnSc2d1	lýdský
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Sardách	Sardy	k1gFnPc6	Sardy
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Nilu	Nil	k1gInSc6	Nil
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
vyvolané	vyvolaný	k2eAgFnPc4d1	vyvolaná
rozkladem	rozklad	k1gInSc7	rozklad
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
následnými	následný	k2eAgInPc7d1	následný
politickými	politický	k2eAgInPc7d1	politický
zmatky	zmatek	k1gInPc7	zmatek
a	a	k8xC	a
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
konflikty	konflikt	k1gInPc7	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
kněží	kněz	k1gMnPc2	kněz
boha	bůh	k1gMnSc2	bůh
Amona	Amon	k1gMnSc2	Amon
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Egyptě	Egypt	k1gInSc6	Egypt
dokonce	dokonce	k9	dokonce
založili	založit	k5eAaPmAgMnP	založit
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
teokratického	teokratický	k2eAgInSc2d1	teokratický
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
upadl	upadnout	k5eAaPmAgMnS	upadnout
Egypt	Egypt	k1gInSc4	Egypt
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Kušitů	Kušita	k1gMnPc2	Kušita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zadržet	zadržet	k5eAaPmF	zadržet
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
moc	moc	k1gFnSc4	moc
Asýrie	Asýrie	k1gFnSc2	Asýrie
<g/>
.	.	kIx.	.
</s>
<s>
Slabosti	slabost	k1gFnPc4	slabost
Egypta	Egypt	k1gInSc2	Egypt
využili	využít	k5eAaPmAgMnP	využít
Asyřané	Asyřan	k1gMnPc1	Asyřan
k	k	k7c3	k
dobytí	dobytí	k1gNnSc3	dobytí
údolí	údolí	k1gNnSc2	údolí
Nilu	Nil	k1gInSc2	Nil
a	a	k8xC	a
k	k	k7c3	k
vyhnání	vyhnání	k1gNnSc3	vyhnání
Kušitů	Kušit	k1gInPc2	Kušit
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
asyrské	asyrský	k2eAgFnSc2d1	Asyrská
moci	moc	k1gFnSc2	moc
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
nastolena	nastolen	k2eAgFnSc1d1	nastolena
vláda	vláda	k1gFnSc1	vláda
domácích	domácí	k2eAgMnPc2d1	domácí
egyptských	egyptský	k2eAgMnPc2d1	egyptský
faraónů	faraón	k1gMnPc2	faraón
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Egypťané	Egypťan	k1gMnPc1	Egypťan
byli	být	k5eAaImAgMnP	být
Asyřanům	Asyřan	k1gMnPc3	Asyřan
zavázáni	zavázat	k5eAaPmNgMnP	zavázat
za	za	k7c4	za
vyhnání	vyhnání	k1gNnSc4	vyhnání
Kušitů	Kušit	k1gInPc2	Kušit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Asýrie	Asýrie	k1gFnSc2	Asýrie
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
Babylonem	Babylon	k1gInSc7	Babylon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
však	však	k9	však
Egypt	Egypt	k1gInSc1	Egypt
poražen	poražen	k2eAgInSc1d1	poražen
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávající	přetrvávající	k2eAgFnSc1d1	přetrvávající
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
labilita	labilita	k1gFnSc1	labilita
země	zem	k1gFnSc2	zem
posléze	posléze	k6eAd1	posléze
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
dobytí	dobytí	k1gNnSc6	dobytí
země	země	k1gFnSc1	země
Peršany	peršan	k1gInPc1	peršan
na	na	k7c6	na
konci	konec	k1gInSc6	konec
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
,	,	kIx,	,
Kartágo	Kartágo	k1gNnSc1	Kartágo
a	a	k8xC	a
Etruskové	Etrusk	k1gMnPc1	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dalekosáhlé	dalekosáhlý	k2eAgFnSc2d1	dalekosáhlá
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začala	začít	k5eAaPmAgFnS	začít
fénická	fénický	k2eAgFnSc1d1	fénická
kolonizace	kolonizace	k1gFnSc1	kolonizace
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgNnPc1d1	nezávislé
fénická	fénický	k2eAgNnPc1d1	fénické
města	město	k1gNnPc1	město
dosáhla	dosáhnout	k5eAaPmAgNnP	dosáhnout
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obchodu	obchod	k1gInSc2	obchod
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
nárůstu	nárůst	k1gInSc3	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
uživit	uživit	k5eAaPmF	uživit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hrozilo	hrozit	k5eAaImAgNnS	hrozit
přelidnění	přelidnění	k1gNnSc1	přelidnění
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
představovala	představovat	k5eAaImAgFnS	představovat
kolonizace	kolonizace	k1gFnSc1	kolonizace
oblastí	oblast	k1gFnPc2	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
motivována	motivovat	k5eAaBmNgFnS	motivovat
také	také	k9	také
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
hledisky	hledisko	k1gNnPc7	hledisko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
území	území	k1gNnSc3	území
byla	být	k5eAaImAgFnS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
cenné	cenný	k2eAgFnPc4d1	cenná
nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Úrodnost	úrodnost	k1gFnSc1	úrodnost
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
dostupnost	dostupnost	k1gFnSc1	dostupnost
různých	různý	k2eAgFnPc2d1	různá
obchodních	obchodní	k2eAgFnPc2d1	obchodní
komodit	komodita	k1gFnPc2	komodita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fénické	fénický	k2eAgFnPc1d1	fénická
kolonie	kolonie	k1gFnPc1	kolonie
byly	být	k5eAaImAgFnP	být
záhy	záhy	k6eAd1	záhy
stejně	stejně	k6eAd1	stejně
bohaté	bohatý	k2eAgInPc1d1	bohatý
jako	jako	k8xC	jako
mateřská	mateřský	k2eAgNnPc4d1	mateřské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
význačným	význačný	k2eAgInPc3d1	význačný
tehdy	tehdy	k6eAd1	tehdy
založeným	založený	k2eAgNnPc3d1	založené
městům	město	k1gNnPc3	město
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
Utica	Utica	k1gMnSc1	Utica
<g/>
,	,	kIx,	,
Gades	Gades	k1gMnSc1	Gades
nebo	nebo	k8xC	nebo
Panormos	Panormos	k1gMnSc1	Panormos
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
byl	být	k5eAaImAgMnS	být
založeno	založen	k2eAgNnSc4d1	založeno
zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
čilým	čilý	k2eAgFnPc3d1	čilá
obchodním	obchodní	k2eAgFnPc3d1	obchodní
aktivitám	aktivita	k1gFnPc3	aktivita
významného	významný	k2eAgNnSc2d1	významné
mocenského	mocenský	k2eAgNnSc2d1	mocenské
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
Kartágo	Kartágo	k1gNnSc4	Kartágo
samo	sám	k3xTgNnSc1	sám
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
kolonií	kolonie	k1gFnSc7	kolonie
města	město	k1gNnSc2	město
Tyru	Tyrus	k1gInSc2	Tyrus
<g/>
,	,	kIx,	,
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
brzy	brzy	k6eAd1	brzy
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
všemi	všecek	k3xTgFnPc7	všecek
okolními	okolní	k2eAgFnPc7d1	okolní
punskými	punský	k2eAgNnPc7d1	punské
sídly	sídlo	k1gNnPc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginské	kartáginský	k2eAgInPc4d1	kartáginský
zájmy	zájem	k1gInPc4	zájem
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
zkřížily	zkřížit	k5eAaPmAgInP	zkřížit
se	s	k7c7	s
zájmy	zájem	k1gInPc7	zájem
tamějších	tamější	k2eAgFnPc2d1	tamější
řeckých	řecký	k2eAgFnPc2d1	řecká
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
mocnými	mocný	k2eAgFnPc7d1	mocná
Syrákúsami	Syrákúsa	k1gFnPc7	Syrákúsa
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
Kartáginci	Kartáginec	k1gInPc7	Kartáginec
vedli	vést	k5eAaImAgMnP	vést
neustálé	neustálý	k2eAgInPc4d1	neustálý
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přes	přes	k7c4	přes
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Peršany	Peršan	k1gMnPc7	Peršan
a	a	k8xC	a
Etrusky	etrusky	k6eAd1	etrusky
se	se	k3xPyFc4	se
Kartágincům	Kartáginec	k1gMnPc3	Kartáginec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
domoci	domoct	k5eAaPmF	domoct
se	se	k3xPyFc4	se
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
převahy	převaha	k1gFnSc2	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
nevyznačovalo	vyznačovat	k5eNaImAgNnS	vyznačovat
tak	tak	k6eAd1	tak
silným	silný	k2eAgInSc7d1	silný
duchovním	duchovní	k2eAgInSc7d1	duchovní
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc7d1	kulturní
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
panoval	panovat	k5eAaImAgInS	panovat
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obchodních	obchodní	k2eAgInPc2d1	obchodní
kontaktů	kontakt	k1gInPc2	kontakt
pronikaly	pronikat	k5eAaImAgInP	pronikat
do	do	k7c2	do
města	město	k1gNnSc2	město
kulturní	kulturní	k2eAgInPc4d1	kulturní
vlivy	vliv	k1gInPc4	vliv
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
poznání	poznání	k1gNnSc6	poznání
velkolepějších	velkolepý	k2eAgInPc2d2	velkolepější
úspěchů	úspěch	k1gInPc2	úspěch
slavili	slavit	k5eAaImAgMnP	slavit
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
někdejší	někdejší	k2eAgFnSc3d1	někdejší
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c4	na
Tyru	Tyra	k1gFnSc4	Tyra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
rozvoji	rozvoj	k1gInSc3	rozvoj
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
stali	stát	k5eAaPmAgMnP	stát
první	první	k4xOgFnSc4	první
mediteránní	mediteránní	k2eAgFnSc4d1	mediteránní
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podnikala	podnikat	k5eAaImAgFnS	podnikat
expedice	expedice	k1gFnPc4	expedice
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
objevné	objevný	k2eAgFnPc1d1	objevná
plavby	plavba	k1gFnPc1	plavba
až	až	k9	až
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
dnešního	dnešní	k2eAgInSc2d1	dnešní
Gabunu	Gabun	k1gInSc2	Gabun
a	a	k8xC	a
do	do	k7c2	do
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Řek	Řek	k1gMnSc1	Řek
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
připisoval	připisovat	k5eAaImAgMnS	připisovat
Kartágincům	Kartáginec	k1gMnPc3	Kartáginec
obeplutí	obeplutí	k1gNnSc2	obeplutí
celého	celý	k2eAgInSc2d1	celý
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
již	již	k6eAd1	již
Féničané	Féničan	k1gMnPc1	Féničan
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
egyptského	egyptský	k2eAgMnSc4d1	egyptský
faraóna	faraón	k1gMnSc4	faraón
Nekoa	Neko	k1gInSc2	Neko
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
blízkého	blízký	k2eAgInSc2d1	blízký
řeckého	řecký	k2eAgInSc2d1	řecký
vlivu	vliv	k1gInSc2	vliv
k	k	k7c3	k
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
změnám	změna	k1gFnPc3	změna
zdejších	zdejší	k2eAgInPc2d1	zdejší
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
900	[number]	k4	900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
etruská	etruský	k2eAgFnSc1d1	etruská
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
Etruskové	Etrusk	k1gMnPc1	Etrusk
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c4	nad
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
poloostrova	poloostrov	k1gInSc2	poloostrov
včetně	včetně	k7c2	včetně
oblastí	oblast	k1gFnPc2	oblast
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Pádu	Pád	k1gInSc2	Pád
a	a	k8xC	a
do	do	k7c2	do
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
.	.	kIx.	.
</s>
<s>
Etruskové	Etrusk	k1gMnPc1	Etrusk
však	však	k9	však
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
pouze	pouze	k6eAd1	pouze
kulturní	kulturní	k2eAgFnPc4d1	kulturní
jednoty	jednota	k1gFnPc4	jednota
nikoli	nikoli	k9	nikoli
státní	státní	k2eAgMnSc1d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Řekové	Řek	k1gMnPc1	Řek
nebo	nebo	k8xC	nebo
Féničané	Féničan	k1gMnPc1	Féničan
byli	být	k5eAaImAgMnP	být
roztříštěni	roztříštit	k5eAaPmNgMnP	roztříštit
v	v	k7c4	v
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
soupeřící	soupeřící	k2eAgInPc4d1	soupeřící
městské	městský	k2eAgInPc4d1	městský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
navenek	navenek	k6eAd1	navenek
vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
jednotně	jednotně	k6eAd1	jednotně
vůči	vůči	k7c3	vůči
Řekům	Řek	k1gMnPc3	Řek
a	a	k8xC	a
různým	různý	k2eAgMnPc3d1	různý
italským	italský	k2eAgMnPc3d1	italský
kmenovým	kmenový	k2eAgMnPc3d1	kmenový
svazům	svaz	k1gInPc3	svaz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Latinové	Latin	k1gMnPc1	Latin
<g/>
,	,	kIx,	,
Umbrové	Umber	k1gMnPc1	Umber
nebo	nebo	k8xC	nebo
Venetové	Venet	k1gMnPc1	Venet
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gNnSc4	jejich
písmo	písmo	k1gNnSc4	písmo
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
rozluštěno	rozluštit	k5eAaPmNgNnS	rozluštit
<g/>
,	,	kIx,	,
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
etruské	etruský	k2eAgFnSc6d1	etruská
kultuře	kultura	k1gFnSc6	kultura
jsou	být	k5eAaImIp3nP	být
čerpány	čerpat	k5eAaImNgInP	čerpat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
archeologických	archeologický	k2eAgFnPc2d1	archeologická
vykopávek	vykopávka	k1gFnPc2	vykopávka
jejich	jejich	k3xOp3gInPc2	jejich
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Perská	perský	k2eAgFnSc1d1	perská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Achaimenovci	Achaimenovec	k1gMnSc3	Achaimenovec
<g/>
.	.	kIx.	.
</s>
<s>
Babylonská	babylonský	k2eAgFnSc1d1	Babylonská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Mezopotámií	Mezopotámie	k1gFnSc7	Mezopotámie
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
se	se	k3xPyFc4	se
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
nastolení	nastolení	k1gNnSc6	nastolení
začala	začít	k5eAaPmAgFnS	začít
drolit	drolit	k5eAaImF	drolit
<g/>
.	.	kIx.	.
</s>
<s>
Nebukadnesarovým	Nebukadnesarův	k2eAgMnPc3d1	Nebukadnesarův
nástupcům	nástupce	k1gMnPc3	nástupce
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
dobytými	dobytý	k2eAgFnPc7d1	dobytá
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
obliba	obliba	k1gFnSc1	obliba
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
značně	značně	k6eAd1	značně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
i	i	k9	i
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Babylonii	Babylonie	k1gFnSc6	Babylonie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vzestup	vzestup	k1gInSc4	vzestup
nové	nový	k2eAgFnSc2d1	nová
dominantní	dominantní	k2eAgFnSc2d1	dominantní
mocnosti	mocnost	k1gFnSc2	mocnost
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
perská	perský	k2eAgFnSc1d1	perská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
Kýros	Kýros	k1gMnSc1	Kýros
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
vládcem	vládce	k1gMnSc7	vládce
Persidy	Persida	k1gFnSc2	Persida
<g/>
,	,	kIx,	,
vazalského	vazalský	k2eAgInSc2d1	vazalský
státu	stát	k1gInSc2	stát
médské	médský	k2eAgFnSc2d1	Médská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
porazil	porazit	k5eAaPmAgMnS	porazit
médského	médský	k2eAgMnSc4d1	médský
krále	král	k1gMnSc4	král
Astyaga	Astyag	k1gMnSc4	Astyag
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
jeho	jeho	k3xOp3gNnSc4	jeho
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ustavil	ustavit	k5eAaPmAgMnS	ustavit
se	s	k7c7	s
králem	král	k1gMnSc7	král
Médů	Méd	k1gMnPc2	Méd
a	a	k8xC	a
Peršanů	Peršan	k1gMnPc2	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
lýdskou	lýdský	k2eAgFnSc4d1	lýdský
říši	říše	k1gFnSc4	říše
krále	král	k1gMnSc2	král
Kroisa	Kroisos	k1gMnSc2	Kroisos
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
maloasijském	maloasijský	k2eAgNnSc6d1	maloasijské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
539	[number]	k4	539
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mu	on	k3xPp3gInSc3	on
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
padl	padnout	k5eAaPmAgInS	padnout
samotný	samotný	k2eAgInSc1d1	samotný
Babylon	Babylon	k1gInSc1	Babylon
<g/>
.	.	kIx.	.
</s>
<s>
Kýros	Kýros	k1gMnSc1	Kýros
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Massagetům	Massaget	k1gMnPc3	Massaget
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Kambýsés	Kambýsésa	k1gFnPc2	Kambýsésa
II	II	kA	II
<g/>
.	.	kIx.	.
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
výbojích	výboj	k1gInPc6	výboj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
525	[number]	k4	525
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgInS	dobýt
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Kambýsově	Kambýsův	k2eAgFnSc6d1	Kambýsův
nenadálé	nenadálý	k2eAgFnSc6d1	nenadálá
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
následným	následný	k2eAgInPc3d1	následný
zmatkům	zmatek	k1gInPc3	zmatek
hrozil	hrozit	k5eAaImAgInS	hrozit
říši	říš	k1gFnSc3	říš
zánik	zánik	k1gInSc4	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Dáreiovi	Dáreius	k1gMnSc3	Dáreius
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
vedlejších	vedlejší	k2eAgMnPc2d1	vedlejší
členů	člen	k1gMnPc2	člen
dynastie	dynastie	k1gFnSc2	dynastie
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
záhy	záhy	k6eAd1	záhy
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
další	další	k2eAgFnSc1d1	další
vážná	vážná	k1gFnSc1	vážná
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zdolání	zdolání	k1gNnSc1	zdolání
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohl	moct	k5eAaImAgInS	moct
Dáreios	Dáreios	k1gInSc4	Dáreios
konečně	konečně	k6eAd1	konečně
upevnit	upevnit	k5eAaPmF	upevnit
svoje	svůj	k3xOyFgNnSc4	svůj
panování	panování	k1gNnSc4	panování
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgFnSc4d1	počáteční
nestabilitu	nestabilita	k1gFnSc4	nestabilita
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Dáreios	Dáreios	k1gMnSc1	Dáreios
říši	říše	k1gFnSc4	říše
sjednotit	sjednotit	k5eAaPmF	sjednotit
a	a	k8xC	a
posunout	posunout	k5eAaPmF	posunout
její	její	k3xOp3gFnPc4	její
hranice	hranice	k1gFnPc4	hranice
až	až	k6eAd1	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Indu	Indus	k1gInSc2	Indus
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c4	na
poloostrov	poloostrov	k1gInSc4	poloostrov
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
proti	proti	k7c3	proti
vzbouřeným	vzbouřený	k2eAgMnPc3d1	vzbouřený
maloasijským	maloasijský	k2eAgMnPc3d1	maloasijský
Řekům	Řek	k1gMnPc3	Řek
a	a	k8xC	a
následně	následně	k6eAd1	následně
proti	proti	k7c3	proti
Athénám	Athéna	k1gFnPc3	Athéna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Athéňanům	Athéňan	k1gMnPc3	Athéňan
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
Miltiadés	Miltiadés	k1gInSc1	Miltiadés
přivodil	přivodit	k5eAaPmAgInS	přivodit
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Marathónu	Marathón	k1gInSc2	Marathón
v	v	k7c6	v
roce	rok	k1gInSc6	rok
490	[number]	k4	490
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jeho	jeho	k3xOp3gMnSc7	jeho
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
Xerxés	Xerxésa	k1gFnPc2	Xerxésa
I.	I.	kA	I.
sice	sice	k8xC	sice
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
během	během	k7c2	během
řecko-perských	řeckoerský	k2eAgFnPc2d1	řecko-perský
válek	válka	k1gFnPc2	válka
nad	nad	k7c4	nad
Řeky	řeka	k1gFnPc4	řeka
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Thermopyl	Thermopyly	k1gFnPc2	Thermopyly
v	v	k7c6	v
roce	rok	k1gInSc6	rok
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Salamíny	Salamína	k1gFnSc2	Salamína
a	a	k8xC	a
u	u	k7c2	u
Platají	Platají	k1gFnSc2	Platají
byli	být	k5eAaImAgMnP	být
Peršané	Peršan	k1gMnPc1	Peršan
zásluhou	zásluhou	k7c2	zásluhou
Themistokla	Themistokla	k1gFnSc2	Themistokla
a	a	k8xC	a
Pausania	Pausanium	k1gNnSc2	Pausanium
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
perská	perský	k2eAgFnSc1d1	perská
expanze	expanze	k1gFnSc1	expanze
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
Dáreia	Dáreium	k1gNnSc2	Dáreium
I.	I.	kA	I.
byly	být	k5eAaImAgFnP	být
zaváděny	zaváděn	k2eAgFnPc4d1	zaváděna
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgInPc1d1	perský
způsoby	způsob	k1gInPc1	způsob
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgInP	prosadit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
i	i	k8xC	i
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nelze	lze	k6eNd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
naprosté	naprostý	k2eAgFnSc6d1	naprostá
dominanci	dominance	k1gFnSc6	dominance
perské	perský	k2eAgFnSc2d1	perská
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
zacházeli	zacházet	k5eAaImAgMnP	zacházet
s	s	k7c7	s
tradicemi	tradice	k1gFnPc7	tradice
<g/>
,	,	kIx,	,
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
podmaněných	podmaněný	k2eAgInPc2d1	podmaněný
národů	národ	k1gInPc2	národ
i	i	k9	i
nadále	nadále	k6eAd1	nadále
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
respektem	respekt	k1gInSc7	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
velkokrál	velkokrál	k1gInSc1	velkokrál
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
proto	proto	k8xC	proto
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
označení	označení	k1gNnSc1	označení
krále	král	k1gMnSc2	král
perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
spíše	spíše	k9	spíše
jako	jako	k9	jako
"	"	kIx"	"
<g/>
krále	král	k1gMnSc4	král
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
krále	král	k1gMnSc2	král
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
kmenů	kmen	k1gInPc2	kmen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pocit	pocit	k1gInSc1	pocit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
i	i	k9	i
na	na	k7c6	na
četných	četný	k2eAgInPc6d1	četný
královských	královský	k2eAgInPc6d1	královský
nápisech	nápis	k1gInPc6	nápis
a	a	k8xC	a
reliéfech	reliéf	k1gInPc6	reliéf
v	v	k7c6	v
královských	královský	k2eAgFnPc6d1	královská
rezidencích	rezidence	k1gFnPc6	rezidence
<g/>
:	:	kIx,	:
v	v	k7c6	v
Susách	Susy	k1gFnPc6	Susy
<g/>
,	,	kIx,	,
v	v	k7c6	v
Persepoli	Persepolis	k1gFnSc6	Persepolis
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pasargádách	Pasargáda	k1gFnPc6	Pasargáda
a	a	k8xC	a
v	v	k7c6	v
Ekbatáně	Ekbatána	k1gFnSc6	Ekbatána
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c6	na
náhrobcích	náhrobek	k1gInPc6	náhrobek
v	v	k7c6	v
Nakš-e	Nakš	k1gFnSc6	Nakš-e
Rustamu	Rustam	k1gInSc2	Rustam
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
byla	být	k5eAaImAgFnS	být
perská	perský	k2eAgFnSc1d1	perská
nadvláda	nadvláda	k1gFnSc1	nadvláda
některými	některý	k3yIgInPc7	některý
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
Egypťany	Egypťan	k1gMnPc4	Egypťan
<g/>
,	,	kIx,	,
pociťována	pociťován	k2eAgMnSc4d1	pociťován
jako	jako	k8xC	jako
útlak	útlak	k1gInSc1	útlak
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
společně	společně	k6eAd1	společně
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
slabostí	slabost	k1gFnSc7	slabost
perské	perský	k2eAgFnSc2d1	perská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
využívali	využívat	k5eAaImAgMnP	využívat
někteří	některý	k3yIgMnPc1	některý
správci	správce	k1gMnPc1	správce
provincií	provincie	k1gFnPc2	provincie
–	–	k?	–
satrapové	satrap	k1gMnPc1	satrap
–	–	k?	–
k	k	k7c3	k
povstáním	povstání	k1gNnPc3	povstání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
značným	značný	k2eAgFnPc3d1	značná
územním	územní	k2eAgFnPc3d1	územní
ztrátám	ztráta	k1gFnPc3	ztráta
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Artaxerxovi	Artaxerxův	k2eAgMnPc1d1	Artaxerxův
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
359	[number]	k4	359
<g/>
–	–	k?	–
<g/>
338	[number]	k4	338
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
podařilo	podařit	k5eAaPmAgNnS	podařit
upevnit	upevnit	k5eAaPmF	upevnit
moc	moc	k1gFnSc4	moc
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
znovu	znovu	k6eAd1	znovu
připojil	připojit	k5eAaPmAgMnS	připojit
většinu	většina	k1gFnSc4	většina
odpadlých	odpadlý	k2eAgNnPc2d1	odpadlé
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
především	především	k9	především
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlubší	hluboký	k2eAgFnSc4d2	hlubší
konsolidaci	konsolidace	k1gFnSc4	konsolidace
říše	říš	k1gFnSc2	říš
ale	ale	k8xC	ale
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
jeho	jeho	k3xOp3gFnSc4	jeho
zavraždění	zavraždění	k1gNnSc1	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
perská	perský	k2eAgFnSc1d1	perská
říše	říše	k1gFnSc1	říše
dobyta	dobyt	k2eAgFnSc1d1	dobyta
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Parthská	Parthský	k2eAgFnSc1d1	Parthský
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
<g/>
.	.	kIx.	.
</s>
<s>
Helénismus	helénismus	k1gInSc1	helénismus
se	se	k3xPyFc4	se
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vliv	vliv	k1gInSc1	vliv
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
reprezentovaný	reprezentovaný	k2eAgInSc1d1	reprezentovaný
seleukovskou	seleukovský	k2eAgFnSc7d1	seleukovská
říší	říš	k1gFnSc7	říš
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vzestupem	vzestup	k1gInSc7	vzestup
Parthů	Parth	k1gInPc2	Parth
vedených	vedený	k2eAgFnPc2d1	vedená
dynastií	dynastie	k1gFnPc2	dynastie
Arsakovců	Arsakovec	k1gMnPc2	Arsakovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
založili	založit	k5eAaPmAgMnP	založit
parthskou	parthský	k2eAgFnSc4d1	parthský
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
arsakovských	arsakovský	k2eAgMnPc2d1	arsakovský
králů	král	k1gMnPc2	král
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
decentralizovaná	decentralizovaný	k2eAgFnSc1d1	decentralizovaná
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jejich	jejich	k3xOp3gMnPc2	jejich
vazalů	vazal	k1gMnPc2	vazal
<g/>
.	.	kIx.	.
</s>
<s>
Parthská	Parthský	k2eAgFnSc1d1	Parthský
říše	říše	k1gFnSc1	říše
působila	působit	k5eAaImAgFnS	působit
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
jako	jako	k8xC	jako
prostředník	prostředník	k1gMnSc1	prostředník
mezi	mezi	k7c7	mezi
západním	západní	k2eAgInSc7d1	západní
řecko-římským	řecko-římský	k2eAgInSc7d1	řecko-římský
světem	svět	k1gInSc7	svět
a	a	k8xC	a
východními	východní	k2eAgFnPc7d1	východní
civilizacemi	civilizace	k1gFnPc7	civilizace
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Parthové	Parth	k1gMnPc1	Parth
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
neustále	neustále	k6eAd1	neustále
bránit	bránit	k5eAaImF	bránit
jak	jak	k8xS	jak
proti	proti	k7c3	proti
římské	římský	k2eAgFnSc3d1	římská
hrozbě	hrozba	k1gFnSc3	hrozba
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
proti	proti	k7c3	proti
nomádským	nomádský	k2eAgInPc3d1	nomádský
kmenům	kmen	k1gInPc3	kmen
útočícím	útočící	k2eAgInPc3d1	útočící
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Obranu	obrana	k1gFnSc4	obrana
říše	říš	k1gFnSc2	říš
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
především	především	k6eAd1	především
nestabilní	stabilní	k2eNgNnSc1d1	nestabilní
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
uspořádání	uspořádání	k1gNnSc1	uspořádání
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
časté	častý	k2eAgInPc1d1	častý
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Rozepře	rozepře	k1gFnSc1	rozepře
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
dynastie	dynastie	k1gFnSc2	dynastie
znemožnily	znemožnit	k5eAaPmAgFnP	znemožnit
Parthům	Parth	k1gInPc3	Parth
využít	využít	k5eAaPmF	využít
jejich	jejich	k3xOp3gNnSc3	jejich
vítězství	vítězství	k1gNnSc3	vítězství
nad	nad	k7c7	nad
Římany	Říman	k1gMnPc7	Říman
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Karrh	Karrha	k1gFnPc2	Karrha
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgInSc3	tento
úspěchu	úspěch	k1gInSc3	úspěch
představoval	představovat	k5eAaImAgMnS	představovat
však	však	k9	však
Řím	Řím	k1gInSc4	Řím
pro	pro	k7c4	pro
Parthy	Partha	k1gFnPc4	Partha
vážného	vážný	k2eAgMnSc2d1	vážný
a	a	k8xC	a
nebezpečného	bezpečný	k2eNgMnSc2d1	nebezpečný
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
pravidelně	pravidelně	k6eAd1	pravidelně
uštědřoval	uštědřovat	k5eAaImAgMnS	uštědřovat
ničivé	ničivý	k2eAgFnPc4d1	ničivá
porážky	porážka	k1gFnPc4	porážka
a	a	k8xC	a
odnímal	odnímat	k5eAaImAgMnS	odnímat
značné	značný	k2eAgFnPc4d1	značná
části	část	k1gFnPc4	část
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
114	[number]	k4	114
napadl	napadnout	k5eAaPmAgMnS	napadnout
parthskou	parthský	k2eAgFnSc4d1	parthský
říši	říše	k1gFnSc4	říše
Traianus	Traianus	k1gMnSc1	Traianus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
postupu	postup	k1gInSc2	postup
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc4	centrum
parthské	parthský	k2eAgFnSc2d1	parthský
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Arsakovcům	Arsakovec	k1gMnPc3	Arsakovec
přivodil	přivodit	k5eAaPmAgMnS	přivodit
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
117	[number]	k4	117
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
velkoryse	velkoryse	k6eAd1	velkoryse
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
dokázali	dokázat	k5eAaPmAgMnP	dokázat
Parthové	Parthové	k2eAgMnPc1d1	Parthové
pozvednout	pozvednout	k5eAaPmF	pozvednout
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stali	stát	k5eAaPmAgMnP	stát
odhodlaným	odhodlaný	k2eAgMnSc7d1	odhodlaný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
161	[number]	k4	161
napadli	napadnout	k5eAaPmAgMnP	napadnout
Arménii	Arménie	k1gFnSc4	Arménie
(	(	kIx(	(
<g/>
předmět	předmět	k1gInSc4	předmět
neustálých	neustálý	k2eAgInPc2d1	neustálý
svárů	svár	k1gInPc2	svár
mezi	mezi	k7c7	mezi
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Parthy	Parth	k1gMnPc7	Parth
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
vojsky	vojsky	k6eAd1	vojsky
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Avidia	Avidium	k1gNnSc2	Avidium
Cassia	Cassium	k1gNnSc2	Cassium
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
dobyli	dobýt	k5eAaPmAgMnP	dobýt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
165	[number]	k4	165
parthské	parthské	k2eAgFnSc1d1	parthské
hlavní	hlavní	k2eAgFnSc1d1	hlavní
město	město	k1gNnSc4	město
Ktésifón	Ktésifón	k1gInSc1	Ktésifón
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
římští	římský	k2eAgMnPc1d1	římský
legionáři	legionář	k1gMnPc1	legionář
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
zárodky	zárodek	k1gInPc4	zárodek
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
pádu	pád	k1gInSc2	pád
parthské	parthský	k2eAgFnSc2d1	parthský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Parthský	Parthský	k1gMnSc1	Parthský
vazal	vazal	k1gMnSc1	vazal
a	a	k8xC	a
vládce	vládce	k1gMnSc1	vládce
Persidy	Persida	k1gFnSc2	Persida
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
využíval	využívat	k5eAaImAgInS	využívat
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
s	s	k7c7	s
parthskou	parthský	k2eAgFnSc7d1	parthský
nadvládou	nadvláda	k1gFnSc7	nadvláda
a	a	k8xC	a
nově	nově	k6eAd1	nově
probuzené	probuzený	k2eAgFnSc6d1	probuzená
perské	perský	k2eAgFnSc6d1	perská
národní	národní	k2eAgFnSc6d1	národní
hrdosti	hrdost	k1gFnSc6	hrdost
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Tažení	tažení	k1gNnSc1	tažení
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Septimia	Septimius	k1gMnSc2	Septimius
Severa	Severa	k1gMnSc1	Severa
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
vypleněn	vypleněn	k2eAgInSc1d1	vypleněn
Ktésifón	Ktésifón	k1gInSc1	Ktésifón
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
parthskou	parthský	k2eAgFnSc4d1	parthský
říši	říše	k1gFnSc4	říše
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevzpamatovala	vzpamatovat	k5eNaPmAgFnS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Persidě	Persida	k1gFnSc6	Persida
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
rozhořelo	rozhořet	k5eAaPmAgNnS	rozhořet
povstání	povstání	k1gNnSc1	povstání
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
lokálního	lokální	k2eAgMnSc2d1	lokální
vládce	vládce	k1gMnSc2	vládce
Ardašíra	Ardašír	k1gMnSc2	Ardašír
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
224	[number]	k4	224
porazil	porazit	k5eAaPmAgMnS	porazit
parthského	parthský	k2eAgMnSc4d1	parthský
krále	král	k1gMnSc4	král
Artabana	Artaban	k1gMnSc2	Artaban
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
dynastie	dynastie	k1gFnSc1	dynastie
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
chopila	chopit	k5eAaPmAgFnS	chopit
moci	moct	k5eAaImF	moct
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
sásánovskou	sásánovský	k2eAgFnSc4d1	sásánovská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Ardašír	Ardašír	k1gMnSc1	Ardašír
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Šápúr	Šápúra	k1gFnPc2	Šápúra
I.	I.	kA	I.
získali	získat	k5eAaPmAgMnP	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celým	celý	k2eAgMnSc7d1	celý
někdejším	někdejší	k2eAgMnSc7d1	někdejší
parthským	parthský	k1gMnSc7	parthský
územím	území	k1gNnSc7	území
a	a	k8xC	a
napadli	napadnout	k5eAaPmAgMnP	napadnout
římskou	římský	k2eAgFnSc4d1	římská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Arménii	Arménie	k1gFnSc4	Arménie
a	a	k8xC	a
přivodili	přivodit	k5eAaPmAgMnP	přivodit
římským	římský	k2eAgFnPc3d1	římská
legiím	legie	k1gFnPc3	legie
řadu	řada	k1gFnSc4	řada
porážek	porážka	k1gFnPc2	porážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
260	[number]	k4	260
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Edesse	Edess	k1gInSc6	Edess
zajat	zajat	k2eAgMnSc1d1	zajat
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Valerianus	Valerianus	k1gMnSc1	Valerianus
<g/>
.	.	kIx.	.
</s>
<s>
Ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
obnovil	obnovit	k5eAaPmAgInS	obnovit
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Šápúr	Šápúra	k1gFnPc2	Šápúra
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Julianus	Julianus	k1gMnSc1	Julianus
sice	sice	k8xC	sice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
363	[number]	k4	363
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
s	s	k7c7	s
početným	početný	k2eAgNnSc7d1	početné
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
menší	malý	k2eAgFnSc6d2	menší
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaImAgInS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
museli	muset	k5eAaImAgMnP	muset
následně	následně	k6eAd1	následně
uzavřít	uzavřít	k5eAaPmF	uzavřít
potupný	potupný	k2eAgInSc4d1	potupný
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Peršanů	peršan	k1gInPc2	peršan
vzdali	vzdát	k5eAaPmAgMnP	vzdát
některých	některý	k3yIgNnPc6	některý
svých	svůj	k3xOyFgNnPc2	svůj
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
hranici	hranice	k1gFnSc6	hranice
Persie	Persie	k1gFnSc2	Persie
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgMnSc1d1	nový
nepřítel	nepřítel	k1gMnSc1	nepřítel
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
–	–	k?	–
Hefthalité	Hefthalitý	k2eAgFnPc1d1	Hefthalitý
<g/>
,	,	kIx,	,
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
také	také	k9	také
Bílí	bílý	k2eAgMnPc1d1	bílý
Hunové	Hun	k1gMnPc1	Hun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
484	[number]	k4	484
padl	padnout	k5eAaPmAgMnS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
velkokrál	velkokrál	k1gMnSc1	velkokrál
Péróz	Péróza	k1gFnPc2	Péróza
I.	I.	kA	I.
Peršané	Peršan	k1gMnPc1	Peršan
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
porážky	porážka	k1gFnSc2	porážka
přinuceni	přinucen	k2eAgMnPc1d1	přinucen
Hefthalitům	Hefthalit	k1gInPc3	Hefthalit
platit	platit	k5eAaImF	platit
tribut	tribut	k1gInSc1	tribut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hladomor	hladomor	k1gMnSc1	hladomor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyústil	vyústit	k5eAaPmAgMnS	vyústit
v	v	k7c4	v
četná	četný	k2eAgNnPc4d1	četné
povstání	povstání	k1gNnPc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vzpour	vzpoura	k1gFnPc2	vzpoura
a	a	k8xC	a
hladomorů	hladomor	k1gInPc2	hladomor
využil	využít	k5eAaPmAgMnS	využít
Husrav	Husrav	k1gMnSc1	Husrav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
sásánovský	sásánovský	k2eAgMnSc1d1	sásánovský
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
zásadních	zásadní	k2eAgFnPc2d1	zásadní
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
532	[number]	k4	532
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
540	[number]	k4	540
sám	sám	k3xTgMnSc1	sám
porušil	porušit	k5eAaPmAgMnS	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgNnSc1d1	perské
vojsko	vojsko	k1gNnSc1	vojsko
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
dobylo	dobýt	k5eAaPmAgNnS	dobýt
a	a	k8xC	a
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
Antiochii	Antiochie	k1gFnSc4	Antiochie
nad	nad	k7c7	nad
Orontem	Oront	k1gMnSc7	Oront
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
město	město	k1gNnSc1	město
římského	římský	k2eAgInSc2d1	římský
Orientu	Orient	k1gInSc2	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Husravovi	Husrava	k1gMnSc3	Husrava
I.	I.	kA	I.
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zničit	zničit	k5eAaPmF	zničit
rovněž	rovněž	k9	rovněž
říši	říše	k1gFnSc4	říše
Hefthalitů	Hefthalit	k1gInPc2	Hefthalit
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
až	až	k6eAd1	až
do	do	k7c2	do
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Husrav	Husrav	k1gFnSc2	Husrav
II	II	kA	II
<g/>
.	.	kIx.	.
posunul	posunout	k5eAaPmAgMnS	posunout
hranice	hranice	k1gFnPc4	hranice
perské	perský	k2eAgFnSc2d1	perská
moci	moc	k1gFnSc2	moc
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
když	když	k8xS	když
dobyl	dobýt	k5eAaPmAgMnS	dobýt
římské	římský	k2eAgFnPc4d1	římská
provincie	provincie	k1gFnPc4	provincie
Sýrii	Sýrie	k1gFnSc4	Sýrie
a	a	k8xC	a
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
626	[number]	k4	626
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Avary	Avar	k1gMnPc7	Avar
neúspěšně	úspěšně	k6eNd1	úspěšně
obléhal	obléhat	k5eAaImAgMnS	obléhat
samotnou	samotný	k2eAgFnSc4d1	samotná
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Protiofenzíva	protiofenzíva	k1gFnSc1	protiofenzíva
císaře	císař	k1gMnSc2	císař
Herakleia	Herakleius	k1gMnSc2	Herakleius
však	však	k9	však
přivodila	přivodit	k5eAaBmAgFnS	přivodit
Peršanům	Peršan	k1gMnPc3	Peršan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
627	[number]	k4	627
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Husrav	Husrat	k5eAaPmDgInS	Husrat
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
během	během	k7c2	během
palácového	palácový	k2eAgNnSc2d1	palácové
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Persie	Persie	k1gFnSc1	Persie
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
propadla	propadlo	k1gNnSc2	propadlo
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
anarchie	anarchie	k1gFnSc2	anarchie
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
říši	říš	k1gFnSc3	říš
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předchozími	předchozí	k2eAgFnPc7d1	předchozí
dlouholetými	dlouholetý	k2eAgFnPc7d1	dlouholetá
válkami	válka	k1gFnPc7	válka
značně	značně	k6eAd1	značně
vyčerpaly	vyčerpat	k5eAaPmAgFnP	vyčerpat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
expanzi	expanze	k1gFnSc4	expanze
Arabů	Arab	k1gMnPc2	Arab
po	po	k7c6	po
roce	rok	k1gInSc6	rok
634	[number]	k4	634
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
posledního	poslední	k2eAgMnSc2d1	poslední
sásánovského	sásánovský	k2eAgMnSc2d1	sásánovský
panovníka	panovník	k1gMnSc2	panovník
Jazdkarta	Jazdkart	k1gMnSc2	Jazdkart
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
651	[number]	k4	651
perská	perský	k2eAgFnSc1d1	perská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
nastaly	nastat	k5eAaPmAgFnP	nastat
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
zásadní	zásadní	k2eAgFnPc4d1	zásadní
sociální	sociální	k2eAgFnPc4d1	sociální
proměny	proměna	k1gFnPc4	proměna
a	a	k8xC	a
zvraty	zvrat	k1gInPc4	zvrat
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
novou	nový	k2eAgFnSc7d1	nová
invazí	invaze	k1gFnSc7	invaze
kmenů	kmen	k1gInPc2	kmen
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
chybějícím	chybějící	k2eAgInPc3d1	chybějící
soudobým	soudobý	k2eAgInPc3d1	soudobý
pramenům	pramen	k1gInPc3	pramen
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
temným	temný	k2eAgNnSc7d1	temné
obdobím	období	k1gNnSc7	období
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
započala	započnout	k5eAaPmAgFnS	započnout
na	na	k7c6	na
Peloponéském	peloponéský	k2eAgInSc6d1	peloponéský
poloostrově	poloostrov	k1gInSc6	poloostrov
expanze	expanze	k1gFnSc1	expanze
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
zdejších	zdejší	k2eAgInPc2d1	zdejší
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
–	–	k?	–
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
řeckých	řecký	k2eAgFnPc6d1	řecká
obcích	obec	k1gFnPc6	obec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
královské	královský	k2eAgFnSc2d1	královská
vlády	vláda	k1gFnSc2	vláda
progresivnějším	progresivní	k2eAgNnPc3d2	progresivnější
společenským	společenský	k2eAgNnPc3d1	společenské
uspořádáním	uspořádání	k1gNnPc3	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
poleis	poleis	k1gFnPc2	poleis
<g/>
)	)	kIx)	)
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
demokratické	demokratický	k2eAgNnSc4d1	demokratické
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
autoritativnější	autoritativní	k2eAgInPc1d2	autoritativnější
politické	politický	k2eAgInPc1d1	politický
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
buď	buď	k8xC	buď
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
v	v	k7c6	v
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
setrvaly	setrvat	k5eAaPmAgInP	setrvat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tyranie	tyranie	k1gFnSc2	tyranie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
jediný	jediný	k2eAgMnSc1d1	jediný
vládce	vládce	k1gMnSc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
stala	stát	k5eAaPmAgFnS	stát
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
zámořským	zámořský	k2eAgFnPc3d1	zámořská
výpravám	výprava	k1gFnPc3	výprava
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
důsledkem	důsledek	k1gInSc7	důsledek
byla	být	k5eAaImAgFnS	být
řecká	řecký	k2eAgFnSc1d1	řecká
kolonizace	kolonizace	k1gFnSc1	kolonizace
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
kolonizace	kolonizace	k1gFnSc1	kolonizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1	kolonizace
vycházela	vycházet	k5eAaImAgFnS	vycházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
měst	město	k1gNnPc2	město
Korint	Korinta	k1gFnPc2	Korinta
<g/>
,	,	kIx,	,
Megara	Megara	k1gFnSc1	Megara
<g/>
,	,	kIx,	,
Chalkis	Chalkis	k1gFnSc1	Chalkis
a	a	k8xC	a
Mílétos	Mílétos	k1gInSc1	Mílétos
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
kolonistů	kolonista	k1gMnPc2	kolonista
patřila	patřit	k5eAaImAgFnS	patřit
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Magna	Magen	k2eAgFnSc1d1	Magna
Graecia	Graecia	k1gFnSc1	Graecia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
Černomoří	Černomoří	k1gNnSc4	Černomoří
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kolonizace	kolonizace	k1gFnSc1	kolonizace
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Afriky	Afrika	k1gFnSc2	Afrika
dotkla	dotknout	k5eAaPmAgFnS	dotknout
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
omývaných	omývaný	k2eAgInPc2d1	omývaný
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
řeckých	řecký	k2eAgMnPc2d1	řecký
kolonistů	kolonista	k1gMnPc2	kolonista
s	s	k7c7	s
již	již	k6eAd1	již
existujícími	existující	k2eAgFnPc7d1	existující
fénickými	fénický	k2eAgFnPc7d1	fénická
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byly	být	k5eAaImAgInP	být
trvalé	trvalý	k2eAgInPc1d1	trvalý
konflikty	konflikt	k1gInPc1	konflikt
mezi	mezi	k7c4	mezi
Řeky	Řek	k1gMnPc4	Řek
a	a	k8xC	a
Puny	Pun	k1gMnPc4	Pun
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
ostrova	ostrov	k1gInSc2	ostrov
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
až	až	k6eAd1	až
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kulturní	kulturní	k2eAgInSc1d1	kulturní
vývoj	vývoj	k1gInSc1	vývoj
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
epochy	epocha	k1gFnSc2	epocha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
dokonalostí	dokonalost	k1gFnSc7	dokonalost
filozofických	filozofický	k2eAgFnPc2d1	filozofická
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
řecké	řecký	k2eAgFnPc1d1	řecká
poleis	poleis	k1gFnPc1	poleis
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
perskou	perský	k2eAgFnSc4d1	perská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Řecko-perské	řeckoerský	k2eAgFnPc1d1	řecko-perský
války	válka	k1gFnPc1	válka
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
mezi	mezi	k7c4	mezi
Helény	Helén	k1gMnPc4	Helén
silný	silný	k2eAgInSc4d1	silný
pocit	pocit	k1gInSc4	pocit
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
se	se	k3xPyFc4	se
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
obránce	obránce	k1gMnPc4	obránce
civilizace	civilizace	k1gFnSc2	civilizace
proti	proti	k7c3	proti
východním	východní	k2eAgMnPc3d1	východní
barbarům	barbar	k1gMnPc3	barbar
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
ještě	ještě	k9	ještě
neměl	mít	k5eNaImAgInS	mít
dnešní	dnešní	k2eAgInSc1d1	dnešní
pejorativní	pejorativní	k2eAgInSc1d1	pejorativní
nádech	nádech	k1gInSc1	nádech
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
neřecké	řecký	k2eNgInPc4d1	řecký
národy	národ	k1gInPc4	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
řecký	řecký	k2eAgInSc1d1	řecký
svět	svět	k1gInSc1	svět
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
nejednotností	nejednotnost	k1gFnSc7	nejednotnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
překonána	překonat	k5eAaPmNgFnS	překonat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vnějšího	vnější	k2eAgNnSc2d1	vnější
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
završení	završení	k1gNnSc6	završení
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
Peršanům	Peršan	k1gMnPc3	Peršan
Helénové	Helén	k1gMnPc1	Helén
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
477	[number]	k4	477
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
řecké	řecký	k2eAgFnPc4d1	řecká
obce	obec	k1gFnPc4	obec
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
soupeřením	soupeření	k1gNnSc7	soupeření
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
Sparty	Sparta	k1gFnSc2	Sparta
o	o	k7c4	o
přednostní	přednostní	k2eAgNnSc4d1	přednostní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
Athény	Athéna	k1gFnPc1	Athéna
zažívaly	zažívat	k5eAaImAgFnP	zažívat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Perikla	Perikla	k1gFnSc4	Perikla
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
epochu	epocha	k1gFnSc4	epocha
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
řevnivost	řevnivost	k1gFnSc1	řevnivost
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
obcemi	obec	k1gFnPc7	obec
přerostla	přerůst	k5eAaPmAgFnS	přerůst
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
431	[number]	k4	431
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c4	v
peloponéskou	peloponéský	k2eAgFnSc4d1	Peloponéská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
antickou	antický	k2eAgFnSc7d1	antická
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
náročné	náročný	k2eAgNnSc1d1	náročné
střetnutí	střetnutí	k1gNnSc1	střetnutí
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
404	[number]	k4	404
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vítězstvím	vítězství	k1gNnSc7	vítězství
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
řecké	řecký	k2eAgFnPc1d1	řecká
obce	obec	k1gFnPc1	obec
střetly	střetnout	k5eAaPmAgFnP	střetnout
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
korintské	korintský	k2eAgFnSc6d1	Korintská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
395	[number]	k4	395
<g/>
-	-	kIx~	-
<g/>
386	[number]	k4	386
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
teprve	teprve	k9	teprve
královským	královský	k2eAgInSc7d1	královský
mírem	mír	k1gInSc7	mír
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mír	mír	k1gInSc1	mír
znamenal	znamenat	k5eAaImAgInS	znamenat
potvrzení	potvrzení	k1gNnSc4	potvrzení
mocenské	mocenský	k2eAgFnSc2d1	mocenská
převahy	převaha	k1gFnSc2	převaha
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
řecké	řecký	k2eAgInPc1d1	řecký
státy	stát	k1gInPc1	stát
nebyly	být	k5eNaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
naleznout	naleznout	k5eAaPmF	naleznout
přijatelný	přijatelný	k2eAgInSc1d1	přijatelný
modus	modus	k1gInSc1	modus
vivendi	vivend	k1gMnPc1	vivend
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
také	také	k9	také
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
řecké	řecký	k2eAgInPc1d1	řecký
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
obce	obec	k1gFnPc4	obec
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
deklarace	deklarace	k1gFnSc1	deklarace
jen	jen	k6eAd1	jen
formální	formální	k2eAgFnSc4d1	formální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
Athény	Athéna	k1gFnPc4	Athéna
<g/>
,	,	kIx,	,
Spartu	Sparta	k1gFnSc4	Sparta
nebo	nebo	k8xC	nebo
Théby	Théby	k1gFnPc4	Théby
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
byla	být	k5eAaImAgFnS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
garanta	garant	k1gMnSc4	garant
perské	perský	k2eAgFnSc2d1	perská
dominance	dominance	k1gFnSc2	dominance
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
díky	díky	k7c3	díky
perské	perský	k2eAgFnSc3d1	perská
podpoře	podpora	k1gFnSc3	podpora
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
hegemonní	hegemonní	k2eAgNnSc4d1	hegemonní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Prosadit	prosadit	k5eAaPmF	prosadit
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
koiné	koiné	k1gFnSc4	koiné
eiréné	eiréná	k1gFnSc2	eiréná
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ale	ale	k9	ale
Sparťanům	Sparťan	k1gMnPc3	Sparťan
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
postavily	postavit	k5eAaPmAgInP	postavit
Athény	Athéna	k1gFnPc4	Athéna
a	a	k8xC	a
Théby	Théby	k1gFnPc4	Théby
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Théby	Théby	k1gFnPc1	Théby
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Epameinóndás	Epameinóndása	k1gFnPc2	Epameinóndása
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
porazily	porazit	k5eAaPmAgFnP	porazit
Spartu	Sparta	k1gFnSc4	Sparta
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Leukter	Leuktra	k1gFnPc2	Leuktra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Sparťané	Sparťan	k1gMnPc1	Sparťan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
své	svůj	k3xOyFgNnSc4	svůj
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Thébská	thébský	k2eAgFnSc1d1	Thébská
hegemonie	hegemonie	k1gFnSc1	hegemonie
ale	ale	k9	ale
netrvala	trvat	k5eNaImAgFnS	trvat
ani	ani	k8xC	ani
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
rozvrácené	rozvrácený	k2eAgInPc1d1	rozvrácený
neustálými	neustálý	k2eAgFnPc7d1	neustálá
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
politickém	politický	k2eAgNnSc6d1	politické
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Helénů	Helén	k1gMnPc2	Helén
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
cítilo	cítit	k5eAaImAgNnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdejší	někdejší	k2eAgInSc4d1	někdejší
lesk	lesk	k1gInSc4	lesk
jejich	jejich	k3xOp3gFnSc2	jejich
civilizace	civilizace	k1gFnSc2	civilizace
kvůli	kvůli	k7c3	kvůli
přetrvávajícímu	přetrvávající	k2eAgInSc3d1	přetrvávající
chaosu	chaos	k1gInSc3	chaos
upadá	upadat	k5eAaImIp3nS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
řečníci	řečník	k1gMnPc1	řečník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k9	třeba
Ísokratés	Ísokratés	k1gInSc1	Ísokratés
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odvetného	odvetný	k2eAgNnSc2d1	odvetné
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
Peršanům	peršan	k1gInPc3	peršan
<g/>
.	.	kIx.	.
</s>
<s>
Realizace	realizace	k1gFnSc1	realizace
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
makedonský	makedonský	k2eAgMnSc1d1	makedonský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
359	[number]	k4	359
až	až	k9	až
338	[number]	k4	338
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgInS	dobýt
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Makedonie	Makedonie	k1gFnSc2	Makedonie
ležících	ležící	k2eAgFnPc2d1	ležící
území	území	k1gNnPc2	území
Ilýrie	Ilýrie	k1gFnSc2	Ilýrie
a	a	k8xC	a
Thrákie	Thrákie	k1gFnSc2	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
338	[number]	k4	338
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
navzdory	navzdory	k6eAd1	navzdory
Démosthenově	Démosthenův	k2eAgNnSc6d1	Démosthenův
úsilí	úsilí	k1gNnSc6	úsilí
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chairóneie	Chairóneie	k1gFnSc2	Chairóneie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ničivě	ničivě	k6eAd1	ničivě
porazil	porazit	k5eAaPmAgMnS	porazit
spojené	spojený	k2eAgNnSc4d1	spojené
athénské	athénský	k2eAgNnSc4d1	athénské
a	a	k8xC	a
thébské	thébský	k2eAgNnSc4d1	thébské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nový	nový	k2eAgMnSc1d1	nový
řecký	řecký	k2eAgMnSc1d1	řecký
hegemon	hegemon	k1gMnSc1	hegemon
plánoval	plánovat	k5eAaImAgMnS	plánovat
válečné	válečný	k2eAgNnSc4d1	válečné
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
úmysl	úmysl	k1gInSc1	úmysl
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zmařen	zmařen	k2eAgInSc1d1	zmařen
rukou	ruka	k1gFnSc7	ruka
vraha	vrah	k1gMnSc2	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
vývoje	vývoj	k1gInSc2	vývoj
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
řecké	řecký	k2eAgFnSc2d1	řecká
literatury	literatura	k1gFnSc2	literatura
byly	být	k5eAaImAgFnP	být
Homérovy	Homérův	k2eAgInPc4d1	Homérův
eposy	epos	k1gInPc4	epos
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odysseia	Odysseia	k1gFnSc1	Odysseia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
již	již	k6eAd1	již
Helénové	Helén	k1gMnPc1	Helén
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
posvátné	posvátný	k2eAgNnSc4d1	posvátné
<g/>
.	.	kIx.	.
</s>
<s>
Nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
soudobou	soudobý	k2eAgFnSc4d1	soudobá
zábavu	zábava	k1gFnSc4	zábava
širokých	široký	k2eAgFnPc2d1	široká
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
představovalo	představovat	k5eAaImAgNnS	představovat
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dramatická	dramatický	k2eAgNnPc1d1	dramatické
díla	dílo	k1gNnPc1	dílo
Aischyla	Aischyla	k1gMnSc2	Aischyla
<g/>
,	,	kIx,	,
Aristofana	Aristofan	k1gMnSc2	Aristofan
<g/>
,	,	kIx,	,
Sofokla	Sofokles	k1gMnSc2	Sofokles
nebo	nebo	k8xC	nebo
Eurípida	Eurípid	k1gMnSc2	Eurípid
udávala	udávat	k5eAaImAgFnS	udávat
směr	směr	k1gInSc4	směr
veškeré	veškerý	k3xTgFnSc3	veškerý
pozdější	pozdní	k2eAgFnSc3d2	pozdější
evropské	evropský	k2eAgFnSc3d1	Evropská
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
dílo	dílo	k1gNnSc1	dílo
Hérodota	Hérodot	k1gMnSc2	Hérodot
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
znamenala	znamenat	k5eAaImAgFnS	znamenat
počátek	počátek	k1gInSc4	počátek
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
poprvé	poprvé	k6eAd1	poprvé
detailně	detailně	k6eAd1	detailně
a	a	k8xC	a
věrně	věrně	k6eAd1	věrně
rekonstruovalo	rekonstruovat	k5eAaBmAgNnS	rekonstruovat
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Thúkýdidés	Thúkýdidés	k1gInSc1	Thúkýdidés
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgInS	stát
zakladatelem	zakladatel	k1gMnSc7	zakladatel
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
důsledkem	důsledek	k1gInSc7	důsledek
duchovního	duchovní	k2eAgInSc2d1	duchovní
života	život	k1gInSc2	život
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
se	se	k3xPyFc4	se
však	však	k9	však
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
klasická	klasický	k2eAgFnSc1d1	klasická
řecká	řecký	k2eAgFnSc1d1	řecká
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
a	a	k8xC	a
dílech	dílo	k1gNnPc6	dílo
Sókrata	Sókrata	k1gFnSc1	Sókrata
<g/>
,	,	kIx,	,
Platóna	Platón	k1gMnSc4	Platón
a	a	k8xC	a
Aristotela	Aristoteles	k1gMnSc4	Aristoteles
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
antická	antický	k2eAgFnSc1d1	antická
filozofie	filozofie	k1gFnSc1	filozofie
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
principech	princip	k1gInPc6	princip
a	a	k8xC	a
zásadách	zásada	k1gFnPc6	zásada
v	v	k7c6	v
nich	on	k3xPp3gFnPc2	on
obsažených	obsažený	k2eAgFnPc2d1	obsažená
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
postavena	postaven	k2eAgFnSc1d1	postavena
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
evropská	evropský	k2eAgFnSc1d1	Evropská
etika	etika	k1gFnSc1	etika
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
rozkvět	rozkvět	k1gInSc4	rozkvět
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
rovněž	rovněž	k9	rovněž
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
a	a	k8xC	a
malířství	malířství	k1gNnSc1	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc1	úspěch
dosažené	dosažený	k2eAgInPc1d1	dosažený
Helény	Helén	k1gMnPc4	Helén
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
tvořily	tvořit	k5eAaImAgFnP	tvořit
základní	základní	k2eAgInPc4d1	základní
kameny	kámen	k1gInPc4	kámen
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgInSc4d2	pozdější
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Helénismus	helénismus	k1gInSc1	helénismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
převzal	převzít	k5eAaPmAgInS	převzít
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
upevnil	upevnit	k5eAaPmAgInS	upevnit
makedonskou	makedonský	k2eAgFnSc4d1	makedonská
hegemonii	hegemonie	k1gFnSc4	hegemonie
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
334	[number]	k4	334
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zahájil	zahájit	k5eAaPmAgInS	zahájit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
perské	perský	k2eAgFnSc3d1	perská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
řecké	řecký	k2eAgFnSc2d1	řecká
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
poté	poté	k6eAd1	poté
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
perského	perský	k2eAgNnSc2d1	perské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
všech	všecek	k3xTgNnPc2	všecek
perských	perský	k2eAgNnPc2d1	perské
území	území	k1gNnPc2	území
podél	podél	k7c2	podél
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Issu	Issus	k1gInSc2	Issus
(	(	kIx(	(
<g/>
333	[number]	k4	333
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
perský	perský	k2eAgMnSc1d1	perský
velkokrál	velkokrál	k1gMnSc1	velkokrál
Dáreios	Dáreios	k1gMnSc1	Dáreios
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Makedoňané	Makedoňan	k1gMnPc1	Makedoňan
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Dáreios	Dáreios	k1gMnSc1	Dáreios
unikl	uniknout	k5eAaPmAgMnS	uniknout
a	a	k8xC	a
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
nové	nový	k2eAgNnSc4d1	nové
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
Alexandr	Alexandr	k1gMnSc1	Alexandr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
utkal	utkat	k5eAaPmAgMnS	utkat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gaugamél	Gaugaméla	k1gFnPc2	Gaugaméla
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Alexandr	Alexandr	k1gMnSc1	Alexandr
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
překážek	překážka	k1gFnPc2	překážka
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
Babylonu	Babylon	k1gInSc3	Babylon
a	a	k8xC	a
Sus	Sus	k1gFnSc3	Sus
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
pronikl	proniknout	k5eAaPmAgInS	proniknout
až	až	k9	až
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
samotné	samotný	k2eAgFnSc2d1	samotná
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Persepolis	Persepolis	k1gFnSc4	Persepolis
a	a	k8xC	a
Ekbatánu	Ekbatán	k2eAgFnSc4d1	Ekbatán
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
Dáreia	Dáreium	k1gNnSc2	Dáreium
prchajícího	prchající	k2eAgNnSc2d1	prchající
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
očekávané	očekávaný	k2eAgFnSc3d1	očekávaná
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
ovšem	ovšem	k9	ovšem
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Dáreios	Dáreios	k1gMnSc1	Dáreios
byl	být	k5eAaImAgMnS	být
zrádně	zrádně	k6eAd1	zrádně
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
satrapou	satrapa	k1gMnSc7	satrapa
Béssem	Béss	k1gMnSc7	Béss
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
titul	titul	k1gInSc1	titul
velkokrále	velkokrála	k1gFnSc3	velkokrála
uzurpoval	uzurpovat	k5eAaBmAgInS	uzurpovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pomstít	pomstít	k5eAaPmF	pomstít
tuto	tento	k3xDgFnSc4	tento
zradu	zrada	k1gFnSc4	zrada
a	a	k8xC	a
postoupil	postoupit	k5eAaPmAgMnS	postoupit
až	až	k6eAd1	až
do	do	k7c2	do
Baktrie	Baktrie	k1gFnSc2	Baktrie
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Béssu	Béssa	k1gFnSc4	Béssa
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
popravil	popravit	k5eAaPmAgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
proměnily	proměnit	k5eAaPmAgFnP	proměnit
v	v	k7c4	v
gerilovou	gerilový	k2eAgFnSc4d1	gerilová
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vzdaly	vzdát	k5eAaPmAgInP	vzdát
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
327	[number]	k4	327
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Alexandr	Alexandr	k1gMnSc1	Alexandr
poté	poté	k6eAd1	poté
podnikl	podniknout	k5eAaPmAgMnS	podniknout
kampaň	kampaň	k1gFnSc4	kampaň
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
domorodých	domorodý	k2eAgMnPc2d1	domorodý
indických	indický	k2eAgMnPc2d1	indický
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
vojáci	voják	k1gMnPc1	voják
nakonec	nakonec	k6eAd1	nakonec
donutili	donutit	k5eAaPmAgMnP	donutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Persie	Persie	k1gFnSc2	Persie
Alexandrovo	Alexandrův	k2eAgNnSc1d1	Alexandrovo
vojsko	vojsko	k1gNnSc1	vojsko
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
při	při	k7c6	při
pochodu	pochod	k1gInSc6	pochod
přes	přes	k7c4	přes
poušť	poušť	k1gFnSc4	poušť
v	v	k7c6	v
Gedrosii	Gedrosie	k1gFnSc6	Gedrosie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
Alexandr	Alexandr	k1gMnSc1	Alexandr
usadil	usadit	k5eAaPmAgMnS	usadit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
v	v	k7c6	v
Babylonu	Babylon	k1gInSc6	Babylon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
plánování	plánování	k1gNnSc2	plánování
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
na	na	k7c4	na
západ	západ	k1gInSc4	západ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
323	[number]	k4	323
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
války	válka	k1gFnPc1	válka
diadochů	diadoch	k1gMnPc2	diadoch
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Alexandrovy	Alexandrův	k2eAgFnSc2d1	Alexandrova
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíchž	jejíž	k3xOyRp3gFnPc6	jejíž
troskách	troska	k1gFnPc6	troska
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
říše	říš	k1gFnPc1	říš
Seleukovců	Seleukovec	k1gMnPc2	Seleukovec
<g/>
,	,	kIx,	,
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
a	a	k8xC	a
Antigonovců	Antigonovec	k1gMnPc2	Antigonovec
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc4d1	původní
motiv	motiv	k1gInSc4	motiv
a	a	k8xC	a
záminku	záminka	k1gFnSc4	záminka
tažení	tažení	k1gNnSc2	tažení
na	na	k7c4	na
východ	východ	k1gInSc4	východ
–	–	k?	–
pomstu	pomsta	k1gFnSc4	pomsta
Řeků	Řek	k1gMnPc2	Řek
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
postupem	postup	k1gInSc7	postup
časem	časem	k6eAd1	časem
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
naopak	naopak	k6eAd1	naopak
prosazovat	prosazovat	k5eAaImF	prosazovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
splynutí	splynutí	k1gNnPc2	splynutí
Řeků	Řek	k1gMnPc2	Řek
s	s	k7c7	s
barbary	barbar	k1gMnPc7	barbar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
zakládal	zakládat	k5eAaImAgMnS	zakládat
Alexandr	Alexandr	k1gMnSc1	Alexandr
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
četná	četný	k2eAgNnPc4d1	četné
nová	nový	k2eAgNnPc4d1	nové
města	město	k1gNnPc4	město
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
svatbu	svatba	k1gFnSc4	svatba
mezi	mezi	k7c7	mezi
makedonskými	makedonský	k2eAgMnPc7d1	makedonský
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
urozenými	urozený	k2eAgFnPc7d1	urozená
Peršankami	Peršanka	k1gFnPc7	Peršanka
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrova	Alexandrův	k2eAgFnSc1d1	Alexandrova
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
vystupování	vystupování	k1gNnSc2	vystupování
nabývaly	nabývat	k5eAaImAgInP	nabývat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
orientálních	orientální	k2eAgInPc2d1	orientální
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
u	u	k7c2	u
Makedoňanů	Makedoňan	k1gMnPc2	Makedoňan
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
nelibost	nelibost	k1gFnSc4	nelibost
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
spojení	spojení	k1gNnSc2	spojení
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
barbarů	barbar	k1gMnPc2	barbar
tudíž	tudíž	k8xC	tudíž
vzala	vzít	k5eAaPmAgFnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
s	s	k7c7	s
Alexandrovou	Alexandrův	k2eAgFnSc7d1	Alexandrova
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
však	však	k9	však
přetrvávala	přetrvávat	k5eAaImAgFnS	přetrvávat
snaha	snaha	k1gFnSc1	snaha
prosadit	prosadit	k5eAaPmF	prosadit
řeckou	řecký	k2eAgFnSc4d1	řecká
kulturu	kultura	k1gFnSc4	kultura
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naráželo	narážet	k5eAaPmAgNnS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
podrobených	podrobený	k2eAgInPc2d1	podrobený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrovi	Alexandrův	k2eAgMnPc1d1	Alexandrův
nástupci	nástupce	k1gMnPc1	nástupce
byli	být	k5eAaImAgMnP	být
proto	proto	k8xC	proto
pozvolna	pozvolna	k6eAd1	pozvolna
vytlačováni	vytlačovat	k5eAaImNgMnP	vytlačovat
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
zachovaly	zachovat	k5eAaPmAgInP	zachovat
především	především	k9	především
v	v	k7c6	v
Baktrii	Baktrie	k1gFnSc6	Baktrie
a	a	k8xC	a
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řecká	řecký	k2eAgFnSc1d1	řecká
přítomnost	přítomnost	k1gFnSc1	přítomnost
udržela	udržet	k5eAaPmAgFnS	udržet
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Západněji	západně	k6eAd2	západně
položené	položený	k2eAgFnPc1d1	položená
země	zem	k1gFnPc1	zem
jako	jako	k8xS	jako
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
byly	být	k5eAaImAgInP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
mnohem	mnohem	k6eAd1	mnohem
silnějšímu	silný	k2eAgNnSc3d2	silnější
působení	působení	k1gNnSc3	působení
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
egyptském	egyptský	k2eAgNnSc6d1	egyptské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Alexandrii	Alexandrie	k1gFnSc3	Alexandrie
místní	místní	k2eAgFnSc3d1	místní
a	a	k8xC	a
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
úplně	úplně	k6eAd1	úplně
splynuly	splynout	k5eAaPmAgFnP	splynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
se	se	k3xPyFc4	se
řecká	řecký	k2eAgFnSc1d1	řecká
civilizace	civilizace	k1gFnSc1	civilizace
plně	plně	k6eAd1	plně
prosadila	prosadit	k5eAaPmAgFnS	prosadit
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
původních	původní	k2eAgFnPc2d1	původní
domorodých	domorodý	k2eAgFnPc2d1	domorodá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Nesvornost	Nesvornost	k1gFnSc1	Nesvornost
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
slabost	slabost	k1gFnSc1	slabost
helénistických	helénistický	k2eAgInPc2d1	helénistický
říši	říš	k1gFnSc3	říš
nicméně	nicméně	k8xC	nicméně
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerý	veškerý	k3xTgInSc1	veškerý
řecký	řecký	k2eAgInSc1d1	řecký
svět	svět	k1gInSc1	svět
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
pohlcen	pohlcen	k2eAgInSc1d1	pohlcen
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Parthy	Parth	k1gMnPc7	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Římanů	Říman	k1gMnPc2	Říman
vliv	vliv	k1gInSc4	vliv
helénistické	helénistický	k2eAgFnSc2d1	helénistická
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
východě	východ	k1gInSc6	východ
přesto	přesto	k8xC	přesto
ještě	ještě	k6eAd1	ještě
zesílil	zesílit	k5eAaPmAgMnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
byl	být	k5eAaImAgInS	být
Řím	Řím	k1gInSc4	Řím
městským	městský	k2eAgInSc7d1	městský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vládli	vládnout	k5eAaImAgMnP	vládnout
králové	král	k1gMnPc1	král
etruského	etruský	k2eAgInSc2d1	etruský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
však	však	k8xC	však
Římané	Říman	k1gMnPc1	Říman
tyto	tento	k3xDgMnPc4	tento
krále	král	k1gMnPc4	král
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
římské	římský	k2eAgFnSc2d1	římská
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
509	[number]	k4	509
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
475	[number]	k4	475
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
etruská	etruský	k2eAgFnSc1d1	etruská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
res	res	k?	res
publica	publicum	k1gNnSc2	publicum
–	–	k?	–
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
každoročně	každoročně	k6eAd1	každoročně
voleni	volit	k5eAaImNgMnP	volit
dva	dva	k4xCgMnPc1	dva
konzulové	konzul	k1gMnPc1	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Následnou	následný	k2eAgFnSc4d1	následná
pozvolnou	pozvolný	k2eAgFnSc4d1	pozvolná
římskou	římský	k2eAgFnSc4d1	římská
expanzi	expanze	k1gFnSc4	expanze
nezastavil	zastavit	k5eNaPmAgMnS	zastavit
ani	ani	k8xC	ani
vpád	vpád	k1gInSc4	vpád
Galů	Gal	k1gMnPc2	Gal
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Řím	Řím	k1gInSc4	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
387	[number]	k4	387
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
zpočátku	zpočátku	k6eAd1	zpočátku
obranných	obranný	k2eAgFnPc2d1	obranná
válek	válka	k1gFnPc2	válka
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
kmeny	kmen	k1gInPc7	kmen
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
,	,	kIx,	,
Latinů	Latin	k1gMnPc2	Latin
<g/>
,	,	kIx,	,
Samnitů	Samnit	k1gInPc2	Samnit
<g/>
,	,	kIx,	,
Umbrů	Umber	k1gMnPc2	Umber
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
jihoitalskými	jihoitalský	k2eAgMnPc7d1	jihoitalský
Řeky	Řek	k1gMnPc7	Řek
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
vedl	vést	k5eAaImAgMnS	vést
Pyrrhos	Pyrrhos	k1gMnSc1	Pyrrhos
<g/>
,	,	kIx,	,
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
264	[number]	k4	264
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Kartáginci	Kartáginec	k1gMnPc7	Kartáginec
první	první	k4xOgFnSc1	první
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
241	[number]	k4	241
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
Sicílie	Sicílie	k1gFnSc2	Sicílie
vítěznými	vítězný	k2eAgMnPc7d1	vítězný
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
desetiletí	desetiletí	k1gNnSc6	desetiletí
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
Římané	Říman	k1gMnPc1	Říman
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
na	na	k7c4	na
dřívější	dřívější	k2eAgInPc4d1	dřívější
kartáginské	kartáginský	k2eAgInPc4d1	kartáginský
ostrovy	ostrov	k1gInPc4	ostrov
Sardinii	Sardinie	k1gFnSc4	Sardinie
a	a	k8xC	a
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
si	se	k3xPyFc3	se
vykompenzovali	vykompenzovat	k5eAaPmAgMnP	vykompenzovat
své	svůj	k3xOyFgFnPc4	svůj
ztráty	ztráta	k1gFnPc4	ztráta
dobytím	dobytí	k1gNnSc7	dobytí
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Opětovný	opětovný	k2eAgInSc1d1	opětovný
vzrůst	vzrůst	k1gInSc1	vzrůst
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
druhou	druhý	k4xOgFnSc4	druhý
punskou	punský	k2eAgFnSc4d1	punská
válku	válka	k1gFnSc4	válka
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
kartáginský	kartáginský	k2eAgMnSc1d1	kartáginský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Hannibal	Hannibal	k1gInSc4	Hannibal
dobyl	dobýt	k5eAaPmAgMnS	dobýt
římské	římský	k2eAgNnSc4d1	římské
spojenecké	spojenecký	k2eAgNnSc4d1	spojenecké
město	město	k1gNnSc4	město
Saguntum	Saguntum	k1gNnSc1	Saguntum
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
vedl	vést	k5eAaImAgInS	vést
pak	pak	k6eAd1	pak
tažení	tažení	k1gNnSc4	tažení
na	na	k7c4	na
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
drtivým	drtivý	k2eAgNnSc7d1	drtivé
kartáginským	kartáginský	k2eAgNnSc7d1	kartáginské
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c4	nad
Římany	Říman	k1gMnPc4	Říman
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
216	[number]	k4	216
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hannibal	Hannibal	k1gInSc1	Hannibal
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
Říma	Řím	k1gInSc2	Řím
zmocnit	zmocnit	k5eAaPmF	zmocnit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
izolován	izolovat	k5eAaBmNgMnS	izolovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Římanům	Říman	k1gMnPc3	Říman
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
obnovit	obnovit	k5eAaPmF	obnovit
své	svůj	k3xOyFgFnPc4	svůj
legie	legie	k1gFnPc4	legie
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
válečné	válečný	k2eAgNnSc4d1	válečné
úsilí	úsilí	k1gNnSc4	úsilí
Římané	Říman	k1gMnPc1	Říman
nasměrovali	nasměrovat	k5eAaPmAgMnP	nasměrovat
proti	proti	k7c3	proti
kartáginským	kartáginský	k2eAgFnPc3d1	kartáginská
državám	država	k1gFnPc3	država
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
úplně	úplně	k6eAd1	úplně
podmanili	podmanit	k5eAaPmAgMnP	podmanit
a	a	k8xC	a
vyrvali	vyrvat	k5eAaPmAgMnP	vyrvat
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Metauru	Metaur	k1gInSc2	Metaur
selhal	selhat	k5eAaPmAgInS	selhat
poslední	poslední	k2eAgInSc1d1	poslední
kartáginský	kartáginský	k2eAgInSc1d1	kartáginský
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zvrat	zvrat	k1gInSc4	zvrat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Hannibal	Hannibal	k1gInSc1	Hannibal
přinucen	přinucen	k2eAgInSc1d1	přinucen
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
Scipionem	Scipion	k1gInSc7	Scipion
Africanem	African	k1gInSc7	African
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
v	v	k7c6	v
roce	rok	k1gInSc6	rok
202	[number]	k4	202
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
definitivně	definitivně	k6eAd1	definitivně
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
stali	stát	k5eAaPmAgMnP	stát
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
mocností	mocnost	k1gFnSc7	mocnost
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
149	[number]	k4	149
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
úplným	úplný	k2eAgNnSc7d1	úplné
zničením	zničení	k1gNnSc7	zničení
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
srovnáno	srovnat	k5eAaPmNgNnS	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
půda	půda	k1gFnSc1	půda
prokleta	proklet	k2eAgFnSc1d1	prokleta
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
provincie	provincie	k1gFnSc1	provincie
Afrika	Afrika	k1gFnSc1	Afrika
anektováno	anektovat	k5eAaBmNgNnS	anektovat
Římany	Říman	k1gMnPc4	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
začali	začít	k5eAaPmAgMnP	začít
angažovat	angažovat	k5eAaBmF	angažovat
také	také	k9	také
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jako	jako	k9	jako
osvoboditelé	osvoboditel	k1gMnPc1	osvoboditel
řeckých	řecký	k2eAgFnPc2d1	řecká
obcí	obec	k1gFnPc2	obec
od	od	k7c2	od
makedonské	makedonský	k2eAgFnSc2d1	makedonská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
během	během	k7c2	během
následných	následný	k2eAgFnPc2d1	následná
válek	válka	k1gFnPc2	válka
však	však	k9	však
získali	získat	k5eAaPmAgMnP	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
zbavili	zbavit	k5eAaPmAgMnP	zbavit
Helény	Helén	k1gMnPc7	Helén
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
133	[number]	k4	133
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zdědili	zdědit	k5eAaPmAgMnP	zdědit
Římané	Říman	k1gMnPc1	Říman
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc4	král
Attala	Attal	k1gMnSc2	Attal
III	III	kA	III
<g/>
.	.	kIx.	.
pergamské	pergamský	k2eAgNnSc1d1	Pergamské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
zajistili	zajistit	k5eAaPmAgMnP	zajistit
pevnou	pevný	k2eAgFnSc4d1	pevná
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
výboje	výboj	k1gInPc4	výboj
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
Řím	Řím	k1gInSc1	Řím
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
vpádem	vpád	k1gInSc7	vpád
germánských	germánský	k2eAgMnPc2d1	germánský
Kimbrů	Kimbr	k1gMnPc2	Kimbr
a	a	k8xC	a
Teutonů	Teuton	k1gMnPc2	Teuton
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
původních	původní	k2eAgNnPc2d1	původní
sídlišť	sídliště	k1gNnPc2	sídliště
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Evropy	Evropa	k1gFnSc2	Evropa
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
vážných	vážný	k2eAgFnPc6d1	vážná
porážkách	porážka	k1gFnPc6	porážka
Římanů	Říman	k1gMnPc2	Říman
provedl	provést	k5eAaPmAgMnS	provést
Gaius	Gaius	k1gMnSc1	Gaius
Marius	Marius	k1gMnSc1	Marius
zásadní	zásadní	k2eAgFnSc4d1	zásadní
reformu	reforma	k1gFnSc4	reforma
římské	římský	k2eAgFnSc2d1	římská
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
dokázal	dokázat	k5eAaPmAgMnS	dokázat
útočící	útočící	k2eAgInPc4d1	útočící
kmeny	kmen	k1gInPc4	kmen
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
pontským	pontský	k2eAgMnSc7d1	pontský
králem	král	k1gMnSc7	král
Mithridatem	Mithridat	k1gMnSc7	Mithridat
Římané	Říman	k1gMnPc1	Říman
nově	nově	k6eAd1	nově
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
poměry	poměr	k1gInPc4	poměr
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1	sociální
napětí	napětí	k1gNnSc1	napětí
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
však	však	k9	však
zapříčinily	zapříčinit	k5eAaPmAgInP	zapříčinit
politické	politický	k2eAgInPc4d1	politický
zmatky	zmatek	k1gInPc4	zmatek
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
bratrů	bratr	k1gMnPc2	bratr
Gracchů	Graccha	k1gMnPc2	Graccha
vyřešit	vyřešit	k5eAaPmF	vyřešit
tyto	tento	k3xDgInPc4	tento
problémy	problém	k1gInPc4	problém
cestou	cestou	k7c2	cestou
agrární	agrární	k2eAgFnSc2d1	agrární
reformy	reforma	k1gFnSc2	reforma
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
oba	dva	k4xCgMnPc1	dva
nakonec	nakonec	k6eAd1	nakonec
skončili	skončit	k5eAaPmAgMnP	skončit
násilnou	násilný	k2eAgFnSc7d1	násilná
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgNnSc1d1	politické
uspořádání	uspořádání	k1gNnSc1	uspořádání
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nevyhovující	vyhovující	k2eNgNnSc1d1	nevyhovující
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
tak	tak	k8xC	tak
obrovské	obrovský	k2eAgFnSc2d1	obrovská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
v	v	k7c4	v
jakou	jaký	k3yRgFnSc4	jaký
se	se	k3xPyFc4	se
Řím	Řím	k1gInSc1	Řím
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
zhoršována	zhoršovat	k5eAaImNgFnS	zhoršovat
řadou	řada	k1gFnSc7	řada
ambiciózních	ambiciózní	k2eAgMnPc2d1	ambiciózní
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sledovali	sledovat	k5eAaImAgMnP	sledovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
byl	být	k5eAaImAgInS	být
rozpolcený	rozpolcený	k2eAgInSc1d1	rozpolcený
bojem	boj	k1gInSc7	boj
soupeřících	soupeřící	k2eAgFnPc2d1	soupeřící
frakcí	frakce	k1gFnPc2	frakce
–	–	k?	–
optimátů	optimát	k1gMnPc2	optimát
a	a	k8xC	a
populárů	populár	k1gMnPc2	populár
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
optimátů	optimát	k1gMnPc2	optimát
<g/>
,	,	kIx,	,
Lucius	Lucius	k1gMnSc1	Lucius
Cornelius	Cornelius	k1gMnSc1	Cornelius
Sulla	Sulla	k1gMnSc1	Sulla
<g/>
,	,	kIx,	,
uchvátil	uchvátit	k5eAaPmAgMnS	uchvátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
82	[number]	k4	82
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
moc	moc	k6eAd1	moc
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
spory	spor	k1gInPc1	spor
byly	být	k5eAaImAgInP	být
dočasně	dočasně	k6eAd1	dočasně
urovnány	urovnat	k5eAaPmNgInP	urovnat
nastolením	nastolení	k1gNnSc7	nastolení
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
odstoupení	odstoupení	k1gNnSc2	odstoupení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Obnovit	obnovit	k5eAaPmF	obnovit
stabilní	stabilní	k2eAgNnSc4d1	stabilní
politické	politický	k2eAgNnSc4d1	politické
prostředí	prostředí	k1gNnSc4	prostředí
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
povstání	povstání	k1gNnSc1	povstání
otroků	otrok	k1gMnPc2	otrok
vedené	vedený	k2eAgNnSc1d1	vedené
Spartakem	Spartak	k1gInSc7	Spartak
poukázalo	poukázat	k5eAaPmAgNnS	poukázat
na	na	k7c4	na
přetrvávající	přetrvávající	k2eAgFnSc4d1	přetrvávající
slabost	slabost	k1gFnSc4	slabost
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
60	[number]	k4	60
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
tři	tři	k4xCgMnPc1	tři
význační	význačný	k2eAgMnPc1d1	význačný
vojevůdci	vojevůdce	k1gMnPc1	vojevůdce
Crassus	Crassus	k1gMnSc1	Crassus
<g/>
,	,	kIx,	,
Pompeius	Pompeius	k1gMnSc1	Pompeius
a	a	k8xC	a
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
tzv.	tzv.	kA	tzv.
první	první	k4xOgInSc4	první
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
dokázal	dokázat	k5eAaPmAgMnS	dokázat
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
spojenectví	spojenectví	k1gNnSc2	spojenectví
nejvíce	hodně	k6eAd3	hodně
vytěžit	vytěžit	k5eAaPmF	vytěžit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
59	[number]	k4	59
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
během	během	k7c2	během
několika	několik	k4yIc3	několik
let	léto	k1gNnPc2	léto
celou	celý	k2eAgFnSc4d1	celá
dobyl	dobýt	k5eAaPmAgInS	dobýt
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Římské	římský	k2eAgFnSc3d1	římská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Crassus	Crassus	k1gMnSc1	Crassus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
53	[number]	k4	53
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Karrh	Karrha	k1gFnPc2	Karrha
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
<g/>
,	,	kIx,	,
triumvirát	triumvirát	k1gInSc1	triumvirát
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
Pompeia	Pompeium	k1gNnSc2	Pompeium
postavil	postavit	k5eAaPmAgInS	postavit
senát	senát	k1gInSc1	senát
Caesara	Caesar	k1gMnSc2	Caesar
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
Caesar	Caesar	k1gMnSc1	Caesar
vyprovokován	vyprovokovat	k5eAaPmNgMnS	vyprovokovat
ke	k	k7c3	k
vpádu	vpád	k1gInSc3	vpád
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
započala	započnout	k5eAaPmAgFnS	započnout
další	další	k2eAgFnSc1d1	další
fáze	fáze	k1gFnSc1	fáze
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
Pompeius	Pompeius	k1gMnSc1	Pompeius
poražen	porazit	k5eAaPmNgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Farsalu	Farsal	k1gInSc2	Farsal
a	a	k8xC	a
Caesar	Caesar	k1gMnSc1	Caesar
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
pánem	pán	k1gMnSc7	pán
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
a	a	k8xC	a
římský	římský	k2eAgInSc1d1	římský
stát	stát	k1gInSc1	stát
opět	opět	k6eAd1	opět
upadl	upadnout	k5eAaPmAgInS	upadnout
do	do	k7c2	do
vnitropolitických	vnitropolitický	k2eAgInPc2d1	vnitropolitický
zmatků	zmatek	k1gInPc2	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
<g/>
,	,	kIx,	,
Octavianus	Octavianus	k1gMnSc1	Octavianus
a	a	k8xC	a
Marcus	Marcus	k1gMnSc1	Marcus
Lepidus	Lepidus	k1gMnSc1	Lepidus
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
druhý	druhý	k4xOgInSc4	druhý
triumvirát	triumvirát	k1gInSc4	triumvirát
<g/>
,	,	kIx,	,
namířený	namířený	k2eAgInSc4d1	namířený
proti	proti	k7c3	proti
Caesarovým	Caesarův	k2eAgMnPc3d1	Caesarův
vrahům	vrah	k1gMnPc3	vrah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
42	[number]	k4	42
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	on	k3xPp3gNnSc4	on
triumvirové	triumvir	k1gMnPc1	triumvir
porazili	porazit	k5eAaPmAgMnP	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Filipp	Filippy	k1gInPc2	Filippy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
obrátili	obrátit	k5eAaPmAgMnP	obrátit
zbraně	zbraň	k1gFnPc4	zbraň
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Markem	Marek	k1gMnSc7	Marek
Antoniem	Antonio	k1gMnSc7	Antonio
a	a	k8xC	a
Kleopatrou	Kleopatra	k1gFnSc7	Kleopatra
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Actia	Actium	k1gNnSc2	Actium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
31	[number]	k4	31
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
získal	získat	k5eAaPmAgMnS	získat
Octavianus	Octavianus	k1gMnSc1	Octavianus
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vzbudit	vzbudit	k5eAaPmF	vzbudit
dojem	dojem	k1gInSc4	dojem
znovuobnovení	znovuobnovení	k1gNnSc2	znovuobnovení
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nastolil	nastolit	k5eAaPmAgInS	nastolit
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
vlády	vláda	k1gFnSc2	vláda
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
principát	principát	k1gInSc1	principát
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
augustus	augustus	k1gInSc4	augustus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
připojena	připojen	k2eAgFnSc1d1	připojena
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
říše	říše	k1gFnSc1	říše
byly	být	k5eAaImAgFnP	být
posunuty	posunout	k5eAaPmNgFnP	posunout
až	až	k9	až
k	k	k7c3	k
řekám	řeka	k1gFnPc3	řeka
Dunaji	Dunaj	k1gInSc3	Dunaj
a	a	k8xC	a
Rýnu	Rýn	k1gInSc3	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Egypt	Egypt	k1gInSc1	Egypt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
dobýt	dobýt	k5eAaPmF	dobýt
Germánii	Germánie	k1gFnSc4	Germánie
ovšem	ovšem	k9	ovšem
selhal	selhat	k5eAaPmAgMnS	selhat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc1	tři
římské	římský	k2eAgFnPc1d1	římská
legie	legie	k1gFnPc1	legie
zničeny	zničit	k5eAaPmNgFnP	zničit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Teutoburském	Teutoburský	k2eAgInSc6d1	Teutoburský
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
Tiberia	Tiberium	k1gNnSc2	Tiberium
<g/>
,	,	kIx,	,
Caliguly	Caligula	k1gFnSc2	Caligula
<g/>
,	,	kIx,	,
Claudia	Claudia	k1gFnSc1	Claudia
a	a	k8xC	a
Nerona	Nero	k1gMnSc2	Nero
byl	být	k5eAaImAgInS	být
principát	principát	k1gInSc1	principát
dále	daleko	k6eAd2	daleko
upevněn	upevnit	k5eAaPmNgInS	upevnit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Tiberia	Tiberium	k1gNnSc2	Tiberium
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Judeji	Judea	k1gFnSc6	Judea
ukřižován	ukřižován	k2eAgMnSc1d1	ukřižován
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
získala	získat	k5eAaPmAgFnS	získat
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
Augustovi	Augustův	k2eAgMnPc1d1	Augustův
následovníci	následovník	k1gMnPc1	následovník
byli	být	k5eAaImAgMnP	být
jen	jen	k9	jen
průměrně	průměrně	k6eAd1	průměrně
schopní	schopný	k2eAgMnPc1d1	schopný
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Caliguly	Caligula	k1gFnSc2	Caligula
a	a	k8xC	a
Nerona	Nero	k1gMnSc2	Nero
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nenáviděné	nenáviděný	k2eAgMnPc4d1	nenáviděný
tyrany	tyran	k1gMnPc4	tyran
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Neronově	Neronův	k2eAgNnSc6d1	Neronovo
zavraždění	zavraždění	k1gNnSc6	zavraždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
68	[number]	k4	68
nastala	nastat	k5eAaPmAgFnS	nastat
krátká	krátký	k2eAgFnSc1d1	krátká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
vítězem	vítěz	k1gMnSc7	vítěz
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
reorganizoval	reorganizovat	k5eAaBmAgMnS	reorganizovat
státní	státní	k2eAgFnPc4d1	státní
finance	finance	k1gFnPc4	finance
a	a	k8xC	a
stabilizoval	stabilizovat	k5eAaBmAgInS	stabilizovat
správu	správa	k1gFnSc4	správa
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Titus	Titus	k1gInSc4	Titus
a	a	k8xC	a
Domitianus	Domitianus	k1gInSc4	Domitianus
si	se	k3xPyFc3	se
počínali	počínat	k5eAaImAgMnP	počínat
celkem	celkem	k6eAd1	celkem
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Domitianus	Domitianus	k1gInSc1	Domitianus
padl	padnout	k5eAaImAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
96	[number]	k4	96
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
spiknutí	spiknutí	k1gNnSc2	spiknutí
vyvolanému	vyvolaný	k2eAgInSc3d1	vyvolaný
jeho	jeho	k3xOp3gInSc7	jeho
autoritativním	autoritativní	k2eAgInSc7d1	autoritativní
způsobem	způsob	k1gInSc7	způsob
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Flaviovce	Flaviovec	k1gMnPc4	Flaviovec
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
tzv.	tzv.	kA	tzv.
adoptivní	adoptivní	k2eAgMnPc1d1	adoptivní
císaři	císař	k1gMnPc1	císař
<g/>
,	,	kIx,	,
za	za	k7c2	za
jejichž	jejichž	k3xOyRp3gFnSc2	jejichž
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Traiana	Traian	k1gMnSc4	Traian
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
říše	říše	k1gFnSc1	říše
dobytím	dobytí	k1gNnSc7	dobytí
Dácie	Dácie	k1gFnSc2	Dácie
<g/>
,	,	kIx,	,
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
,	,	kIx,	,
Asýrie	Asýrie	k1gFnSc1	Asýrie
a	a	k8xC	a
Arménie	Arménie	k1gFnSc1	Arménie
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
územního	územní	k2eAgInSc2d1	územní
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Traianův	Traianův	k2eAgMnSc1d1	Traianův
nástupce	nástupce	k1gMnSc1	nástupce
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
většiny	většina	k1gFnSc2	většina
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozkvět	rozkvět	k1gInSc4	rozkvět
zažili	zažít	k5eAaPmAgMnP	zažít
Římané	Říman	k1gMnPc1	Říman
za	za	k7c2	za
Antonina	Antonin	k2eAgMnSc2d1	Antonin
Pia	Pius	k1gMnSc2	Pius
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
doba	doba	k1gFnSc1	doba
vlády	vláda	k1gFnSc2	vláda
Marca	Marc	k2eAgFnSc1d1	Marca
Aurelia	Aurelia	k1gFnSc1	Aurelia
se	se	k3xPyFc4	se
už	už	k6eAd1	už
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
masivní	masivní	k2eAgFnSc7d1	masivní
invazí	invaze	k1gFnSc7	invaze
Germánů	Germán	k1gMnPc2	Germán
a	a	k8xC	a
Sarmatů	Sarmat	k1gMnPc2	Sarmat
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
s	s	k7c7	s
vynaložením	vynaložení	k1gNnSc7	vynaložení
velkého	velký	k2eAgNnSc2d1	velké
úsilí	úsilí	k1gNnSc2	úsilí
se	se	k3xPyFc4	se
císaři	císař	k1gMnSc3	císař
podařilo	podařit	k5eAaPmAgNnS	podařit
zdolat	zdolat	k5eAaPmF	zdolat
jejich	jejich	k3xOp3gInSc4	jejich
příval	příval	k1gInSc4	příval
během	během	k7c2	během
náročných	náročný	k2eAgNnPc2d1	náročné
tažení	tažení	k1gNnPc2	tažení
markomanských	markomanský	k2eAgFnPc2d1	markomanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
násilné	násilný	k2eAgFnSc6d1	násilná
smrti	smrt	k1gFnSc6	smrt
despotického	despotický	k2eAgMnSc2d1	despotický
Marcova	Marcův	k2eAgMnSc2d1	Marcův
syna	syn	k1gMnSc2	syn
Commoda	Commod	k1gMnSc2	Commod
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
192	[number]	k4	192
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vítězně	vítězně	k6eAd1	vítězně
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
zajistit	zajistit	k5eAaPmF	zajistit
říši	říše	k1gFnSc4	říše
trvalou	trvalý	k2eAgFnSc4d1	trvalá
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
po	po	k7c4	po
zavraždění	zavraždění	k1gNnSc4	zavraždění
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Caracally	Caracalla	k1gMnSc2	Caracalla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
217	[number]	k4	217
se	se	k3xPyFc4	se
římský	římský	k2eAgInSc1d1	římský
stát	stát	k1gInSc1	stát
začal	začít	k5eAaPmAgInS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
propadat	propadat	k5eAaPmF	propadat
do	do	k7c2	do
anarchie	anarchie	k1gFnSc2	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
naplno	naplno	k6eAd1	naplno
propukla	propuknout	k5eAaPmAgFnS	propuknout
po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
235	[number]	k4	235
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
byl	být	k5eAaImAgInS	být
svými	svůj	k3xOyFgMnPc7	svůj
vlastními	vlastní	k2eAgMnPc7d1	vlastní
vojáky	voják	k1gMnPc7	voják
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Alexander	Alexandra	k1gFnPc2	Alexandra
Severus	Severus	k1gMnSc1	Severus
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
císař	císař	k1gMnSc1	císař
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Severovců	Severovec	k1gMnPc2	Severovec
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
kritické	kritický	k2eAgNnSc4d1	kritické
období	období	k1gNnSc4	období
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
vojenských	vojenský	k2eAgMnPc2d1	vojenský
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
třetího	třetí	k4xOgNnSc2	třetí
století	století	k1gNnSc2	století
přivodila	přivodit	k5eAaPmAgFnS	přivodit
Římské	římský	k2eAgInPc1d1	římský
říši	říše	k1gFnSc4	říše
téměř	téměř	k6eAd1	téměř
úplnou	úplný	k2eAgFnSc4d1	úplná
zkázu	zkáza	k1gFnSc4	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgInPc4d1	pokračující
útoky	útok	k1gInPc4	útok
germánských	germánský	k2eAgMnPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
zesílený	zesílený	k2eAgInSc4d1	zesílený
tlak	tlak	k1gInSc4	tlak
sásánovské	sásánovský	k2eAgFnSc2d1	sásánovská
Persie	Persie	k1gFnSc2	Persie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
znamenaly	znamenat	k5eAaImAgFnP	znamenat
pro	pro	k7c4	pro
Řím	Řím	k1gInSc4	Řím
vážnou	vážný	k2eAgFnSc4d1	vážná
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
současně	současně	k6eAd1	současně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
uzurpací	uzurpace	k1gFnPc2	uzurpace
císařského	císařský	k2eAgInSc2d1	císařský
trůnu	trůn	k1gInSc2	trůn
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
různých	různý	k2eAgMnPc2d1	různý
velitelů	velitel	k1gMnPc2	velitel
římských	římský	k2eAgNnPc2d1	římské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
území	území	k1gNnPc1	území
říše	říš	k1gFnSc2	říš
byla	být	k5eAaImAgFnS	být
dočasně	dočasně	k6eAd1	dočasně
ztracena	ztratit	k5eAaPmNgFnS	ztratit
(	(	kIx(	(
<g/>
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
provincie	provincie	k1gFnSc2	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
území	území	k1gNnSc1	území
v	v	k7c6	v
Germánii	Germánie	k1gFnSc6	Germánie
a	a	k8xC	a
v	v	k7c6	v
Dácii	Dácie	k1gFnSc6	Dácie
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
opuštěna	opustit	k5eAaPmNgFnS	opustit
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
za	za	k7c2	za
Diocletiana	Diocletian	k1gMnSc2	Diocletian
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
reforem	reforma	k1gFnPc2	reforma
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
nastolit	nastolit	k5eAaPmF	nastolit
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Impérium	impérium	k1gNnSc1	impérium
bylo	být	k5eAaImAgNnS	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
celkem	celkem	k6eAd1	celkem
čtyřmi	čtyři	k4xCgInPc7	čtyři
císaři	císař	k1gMnPc7	císař
<g/>
,	,	kIx,	,
vládnoucími	vládnoucí	k2eAgInPc7d1	vládnoucí
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
shodě	shoda	k1gFnSc6	shoda
(	(	kIx(	(
<g/>
tetrarchie	tetrarchie	k1gFnSc1	tetrarchie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Diocletiana	Diocletian	k1gMnSc2	Diocletian
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
poslední	poslední	k2eAgNnSc1d1	poslední
velké	velký	k2eAgNnSc1d1	velké
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
Diocletianově	Diocletianův	k2eAgFnSc6d1	Diocletianova
abdikaci	abdikace	k1gFnSc6	abdikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
305	[number]	k4	305
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
opět	opět	k6eAd1	opět
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
324	[number]	k4	324
převzal	převzít	k5eAaPmAgMnS	převzít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
římským	římský	k2eAgNnSc7d1	římské
impériem	impérium	k1gNnSc7	impérium
Konstantin	Konstantin	k1gMnSc1	Konstantin
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
Bosporu	Bospor	k1gInSc6	Bospor
vybudovat	vybudovat	k5eAaPmF	vybudovat
nové	nový	k2eAgNnSc4d1	nové
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
neslo	nést	k5eAaImAgNnS	nést
jméno	jméno	k1gNnSc1	jméno
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
313	[number]	k4	313
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Mediolanu	Mediolan	k1gInSc6	Mediolan
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
povoloval	povolovat	k5eAaImAgMnS	povolovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Julianus	Julianus	k1gMnSc1	Julianus
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zvrátit	zvrátit	k5eAaPmF	zvrátit
vzestup	vzestup	k1gInSc4	vzestup
křesťánství	křesťánství	k1gNnSc2	křesťánství
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgInS	podporovat
původní	původní	k2eAgInPc4d1	původní
kulty	kult	k1gInPc4	kult
(	(	kIx(	(
<g/>
pročež	pročež	k6eAd1	pročež
si	se	k3xPyFc3	se
od	od	k7c2	od
křesťanů	křesťan	k1gMnPc2	křesťan
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
řecký	řecký	k2eAgInSc4d1	řecký
přídomek	přídomek	k1gInSc4	přídomek
Apostata	apostata	k1gMnSc1	apostata
<g/>
,	,	kIx,	,
odpadlík	odpadlík	k1gMnSc1	odpadlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nešel	jít	k5eNaImAgMnS	jít
však	však	k9	však
cestou	cesta	k1gFnSc7	cesta
teroru	teror	k1gInSc2	teror
či	či	k8xC	či
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
porazit	porazit	k5eAaPmF	porazit
křesťanství	křesťanství	k1gNnSc4	křesťanství
ideologicky	ideologicky	k6eAd1	ideologicky
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
jeho	on	k3xPp3gInSc4	on
spis	spis	k1gInSc4	spis
Contra	Contrum	k1gNnSc2	Contrum
Galileos	Galileosa	k1gFnPc2	Galileosa
<g/>
)	)	kIx)	)
a	a	k8xC	a
skrytým	skrytý	k2eAgNnSc7d1	skryté
podporováním	podporování	k1gNnSc7	podporování
rozepří	rozepře	k1gFnPc2	rozepře
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
předčasná	předčasný	k2eAgFnSc1d1	předčasná
smrt	smrt	k1gFnSc1	smrt
při	při	k7c6	při
perském	perský	k2eAgNnSc6d1	perské
tažení	tažení	k1gNnSc6	tažení
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgInS	vládnout
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gInPc4	jeho
záměry	záměr	k1gInPc4	záměr
zmařila	zmařit	k5eAaPmAgFnS	zmařit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gNnPc2	jeho
pro-pohanských	proohanský	k2eAgNnPc2d1	pro-pohanský
nařízení	nařízení	k1gNnPc2	nařízení
opět	opět	k6eAd1	opět
zrušili	zrušit	k5eAaPmAgMnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
christianizaci	christianizace	k1gFnSc4	christianizace
celé	celý	k2eAgFnSc2d1	celá
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
završena	završen	k2eAgFnSc1d1	završena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
391	[number]	k4	391
<g/>
,	,	kIx,	,
když	když	k8xS	když
Theodosius	Theodosius	k1gMnSc1	Theodosius
I.	I.	kA	I.
zakázal	zakázat	k5eAaPmAgMnS	zakázat
vykonávání	vykonávání	k1gNnSc4	vykonávání
veškerých	veškerý	k3xTgInPc2	veškerý
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
narůstala	narůstat	k5eAaImAgFnS	narůstat
intenzita	intenzita	k1gFnSc1	intenzita
vpádů	vpád	k1gInPc2	vpád
Germánů	Germán	k1gMnPc2	Germán
na	na	k7c4	na
území	území	k1gNnSc4	území
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
byli	být	k5eAaImAgMnP	být
Gótové	Gót	k1gMnPc1	Gót
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
Huny	Hun	k1gMnPc4	Hun
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
sídlišť	sídliště	k1gNnPc2	sídliště
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
na	na	k7c4	na
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
se	se	k3xPyFc4	se
však	však	k9	však
zanedlouho	zanedlouho	k6eAd1	zanedlouho
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
porazili	porazit	k5eAaPmAgMnP	porazit
Římany	Říman	k1gMnPc7	Říman
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
období	období	k1gNnSc2	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
200	[number]	k4	200
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jemuž	jenž	k3xRgMnSc3	jenž
padla	padnout	k5eAaPmAgFnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
západní	západní	k2eAgFnSc1d1	západní
polovina	polovina	k1gFnSc1	polovina
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Germáni	Germán	k1gMnPc1	Germán
pronikající	pronikající	k2eAgMnPc1d1	pronikající
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
byli	být	k5eAaImAgMnP	být
usazováni	usazovat	k5eAaImNgMnP	usazovat
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
půdě	půda	k1gFnSc6	půda
jako	jako	k9	jako
foederati	foederat	k5eAaImF	foederat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
pokračující	pokračující	k2eAgInPc1d1	pokračující
útoky	útok	k1gInPc1	útok
však	však	k9	však
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
utváření	utváření	k1gNnSc3	utváření
germánských	germánský	k2eAgInPc2d1	germánský
států	stát	k1gInPc2	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
celých	celý	k2eAgFnPc2d1	celá
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
Římané	Říman	k1gMnPc1	Říman
dokázali	dokázat	k5eAaPmAgMnP	dokázat
ubránit	ubránit	k5eAaPmF	ubránit
svou	svůj	k3xOyFgFnSc4	svůj
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
proti	proti	k7c3	proti
výbojné	výbojný	k2eAgFnSc3d1	výbojná
sásánovské	sásánovský	k2eAgFnSc3d1	sásánovská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
byla	být	k5eAaImAgFnS	být
někdejší	někdejší	k2eAgFnSc1d1	někdejší
římská	římský	k2eAgFnSc1d1	římská
mocenská	mocenský	k2eAgFnSc1d1	mocenská
převaha	převaha	k1gFnSc1	převaha
nenávratně	návratně	k6eNd1	návratně
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Theodosia	Theodosium	k1gNnSc2	Theodosium
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
byla	být	k5eAaImAgFnS	být
říše	říše	k1gFnSc1	říše
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
Arcadia	Arcadium	k1gNnSc2	Arcadium
a	a	k8xC	a
Honoria	Honorium	k1gNnSc2	Honorium
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
formální	formální	k2eAgNnSc4d1	formální
dělení	dělení	k1gNnSc4	dělení
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
jedinou	jediný	k2eAgFnSc7d1	jediná
celistvou	celistvý	k2eAgFnSc7d1	celistvá
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
efektivnější	efektivní	k2eAgFnSc2d2	efektivnější
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
obrany	obrana	k1gFnSc2	obrana
rozlehlého	rozlehlý	k2eAgNnSc2d1	rozlehlé
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
ukázat	ukázat	k5eAaPmF	ukázat
trvalým	trvalá	k1gFnPc3	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
vystavená	vystavený	k2eAgFnSc1d1	vystavená
silnějšímu	silný	k2eAgInSc3d2	silnější
tlaku	tlak	k1gInSc3	tlak
Germánů	Germán	k1gMnPc2	Germán
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
uhájit	uhájit	k5eAaPmF	uhájit
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vedení	vedení	k1gNnSc2	vedení
náčelníkem	náčelník	k1gInSc7	náčelník
Alarichem	Alarich	k1gInSc7	Alarich
samotný	samotný	k2eAgInSc1d1	samotný
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
císaře	císař	k1gMnSc2	císař
Valentiniana	Valentinian	k1gMnSc2	Valentinian
III	III	kA	III
<g/>
.	.	kIx.	.
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Aetius	Aetius	k1gMnSc1	Aetius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Germány	Germán	k1gMnPc7	Germán
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
451	[number]	k4	451
nad	nad	k7c4	nad
Huny	Hun	k1gMnPc4	Hun
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Katalaunských	Katalaunský	k2eAgNnPc6d1	Katalaunský
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
západořímská	západořímský	k2eAgFnSc1d1	Západořímská
obrana	obrana	k1gFnSc1	obrana
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
posledního	poslední	k2eAgMnSc2d1	poslední
císaře	císař	k1gMnSc2	císař
Romula	Romulus	k1gMnSc2	Romulus
Augusta	August	k1gMnSc2	August
západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
antické	antický	k2eAgFnPc1d1	antická
tradice	tradice	k1gFnPc1	tradice
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
přetrvaly	přetrvat	k5eAaPmAgFnP	přetrvat
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
východní	východní	k2eAgFnPc4d1	východní
říše	říš	k1gFnPc4	říš
disponující	disponující	k2eAgFnPc4d1	disponující
příznivějšími	příznivý	k2eAgInPc7d2	příznivější
geografickými	geografický	k2eAgInPc7d1	geografický
a	a	k8xC	a
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
podmínkami	podmínka	k1gFnPc7	podmínka
se	se	k3xPyFc4	se
germánským	germánský	k2eAgInPc3d1	germánský
a	a	k8xC	a
perským	perský	k2eAgInPc3d1	perský
úderům	úder	k1gInPc3	úder
ubránila	ubránit	k5eAaPmAgFnS	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
znovu	znovu	k6eAd1	znovu
dobyta	dobyt	k2eAgFnSc1d1	dobyta
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
někdejší	někdejší	k2eAgFnSc2d1	někdejší
západní	západní	k2eAgFnSc2d1	západní
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vzrůstající	vzrůstající	k2eAgInSc1d1	vzrůstající
vliv	vliv	k1gInSc1	vliv
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
islámu	islám	k1gInSc2	islám
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
způsobily	způsobit	k5eAaPmAgInP	způsobit
postupnou	postupný	k2eAgFnSc4d1	postupná
změnu	změna	k1gFnSc4	změna
charakteru	charakter	k1gInSc2	charakter
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
antické	antický	k2eAgNnSc4d1	antické
východořímské	východořímský	k2eAgNnSc4d1	Východořímské
impérium	impérium	k1gNnSc4	impérium
se	se	k3xPyFc4	se
tak	tak	k9	tak
transformovalo	transformovat	k5eAaBmAgNnS	transformovat
ve	v	k7c4	v
středověkou	středověký	k2eAgFnSc4d1	středověká
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Harappská	Harappský	k2eAgFnSc1d1	Harappská
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
Árjové	Árjové	k2eAgFnSc1d1	Árjové
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
starověkém	starověký	k2eAgInSc6d1	starověký
sociálním	sociální	k2eAgMnSc6d1	sociální
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgMnSc6d1	kulturní
a	a	k8xC	a
politickém	politický	k2eAgInSc6d1	politický
vývoji	vývoj	k1gInSc6	vývoj
na	na	k7c6	na
území	území	k1gNnSc6	území
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
vycházejí	vycházet	k5eAaImIp3nP	vycházet
kvůli	kvůli	k7c3	kvůli
chybějícím	chybějící	k2eAgInPc3d1	chybějící
písemným	písemný	k2eAgInPc3d1	písemný
pramenům	pramen	k1gInPc3	pramen
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
neolitu	neolit	k1gInSc2	neolit
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
jen	jen	k9	jen
řídce	řídce	k6eAd1	řídce
osídlena	osídlen	k2eAgFnSc1d1	osídlena
<g/>
.	.	kIx.	.
</s>
<s>
Nejranější	raný	k2eAgInPc1d3	nejranější
doklady	doklad	k1gInPc1	doklad
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
7000	[number]	k4	7000
až	až	k6eAd1	až
3200	[number]	k4	3200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Méhrgarh	Méhrgarh	k1gInSc1	Méhrgarh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
západně	západně	k6eAd1	západně
od	od	k7c2	od
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
pákistánského	pákistánský	k2eAgNnSc2d1	pákistánské
města	město	k1gNnSc2	město
Kvéta	Kvét	k1gInSc2	Kvét
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
trvale	trvale	k6eAd1	trvale
osídleno	osídlit	k5eAaPmNgNnS	osídlit
asi	asi	k9	asi
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2600	[number]	k4	2600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
postupně	postupně	k6eAd1	postupně
asimilováno	asimilován	k2eAgNnSc1d1	asimilováno
kmeny	kmen	k1gInPc1	kmen
pronikajícími	pronikající	k2eAgMnPc7d1	pronikající
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
příslušníci	příslušník	k1gMnPc1	příslušník
hovořili	hovořit	k5eAaImAgMnP	hovořit
zřejmě	zřejmě	k6eAd1	zřejmě
drávidskými	drávidský	k2eAgInPc7d1	drávidský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
nově	nově	k6eAd1	nově
příchozí	příchozí	k1gMnPc1	příchozí
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Indu	Indus	k1gInSc2	Indus
velice	velice	k6eAd1	velice
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
harappská	harappský	k2eAgFnSc1d1	harappská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
3300	[number]	k4	3300
–	–	k?	–
1700	[number]	k4	1700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
území	území	k1gNnSc6	území
nynějšího	nynější	k2eAgInSc2d1	nynější
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
nalezeného	nalezený	k2eAgNnSc2d1	nalezené
města	město	k1gNnSc2	město
této	tento	k3xDgFnSc2	tento
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byla	být	k5eAaImAgFnS	být
Harappa	Harappa	k1gFnSc1	Harappa
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
pákistánské	pákistánský	k2eAgFnSc6d1	pákistánská
provincii	provincie	k1gFnSc6	provincie
Paňdžáb	Paňdžáb	k1gInSc1	Paňdžáb
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
centrem	centr	k1gInSc7	centr
bylo	být	k5eAaImAgNnS	být
Mohendžodaro	Mohendžodara	k1gFnSc5	Mohendžodara
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
silným	silný	k2eAgNnSc7d1	silné
opevněním	opevnění	k1gNnSc7	opevnění
<g/>
,	,	kIx,	,
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
lázněmi	lázeň	k1gFnPc7	lázeň
a	a	k8xC	a
propracovaným	propracovaný	k2eAgNnSc7d1	propracované
kanalizačním	kanalizační	k2eAgNnSc7d1	kanalizační
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Indie	Indie	k1gFnSc2	Indie
udržovali	udržovat	k5eAaImAgMnP	udržovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
styky	styk	k1gInPc4	styk
se	s	k7c7	s
Sumeřany	Sumeřan	k1gMnPc7	Sumeřan
a	a	k8xC	a
navázali	navázat	k5eAaPmAgMnP	navázat
obchodní	obchodní	k2eAgInPc4d1	obchodní
kontakty	kontakt	k1gInPc4	kontakt
i	i	k9	i
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sérii	série	k1gFnSc3	série
invazí	invaze	k1gFnPc2	invaze
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgInP	nazývat
Árjové	Árjová	k1gFnSc2	Árjová
(	(	kIx(	(
<g/>
Arya	Ary	k1gInSc2	Ary
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
řečí	řeč	k1gFnSc7	řeč
byl	být	k5eAaImAgInS	být
sanskrt	sanskrt	k1gInSc1	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
polonomádi	polonomád	k1gMnPc1	polonomád
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
přes	přes	k7c4	přes
horské	horský	k2eAgInPc4d1	horský
průsmyky	průsmyk	k1gInPc4	průsmyk
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
podrobili	podrobit	k5eAaPmAgMnP	podrobit
si	se	k3xPyFc3	se
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
harappská	harappský	k2eAgFnSc1d1	harappská
kultura	kultura	k1gFnSc1	kultura
těmito	tento	k3xDgMnPc7	tento
bojovnými	bojovný	k2eAgMnPc7d1	bojovný
nájezdníky	nájezdník	k1gMnPc7	nájezdník
zničena	zničit	k5eAaPmNgFnS	zničit
nebo	nebo	k8xC	nebo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
splynutí	splynutí	k1gNnSc3	splynutí
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
byli	být	k5eAaImAgMnP	být
jako	jako	k8xS	jako
zemědělci	zemědělec	k1gMnPc1	zemědělec
absorbováni	absorbovat	k5eAaBmNgMnP	absorbovat
árjskými	árjský	k2eAgMnPc7d1	árjský
dobyvateli	dobyvatel	k1gMnPc7	dobyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létech	léto	k1gNnPc6	léto
1500	[number]	k4	1500
až	až	k9	až
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikaly	vznikat	k5eAaImAgInP	vznikat
Védy	véd	k1gInPc1	véd
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc1	sbírka
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
předávaných	předávaný	k2eAgFnPc2d1	předávaná
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
pouze	pouze	k6eAd1	pouze
ústní	ústní	k2eAgFnSc7d1	ústní
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
rituály	rituál	k1gInPc4	rituál
uctívání	uctívání	k1gNnSc3	uctívání
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc4	provádění
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
různé	různý	k2eAgFnSc2d1	různá
filozofické	filozofický	k2eAgFnSc2d1	filozofická
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
pramen	pramen	k1gInSc1	pramen
raných	raný	k2eAgFnPc2d1	raná
indických	indický	k2eAgFnPc2d1	indická
dějin	dějiny	k1gFnPc2	dějiny
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
podstatu	podstata	k1gFnSc4	podstata
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
upanišadami	upanišada	k1gFnPc7	upanišada
a	a	k8xC	a
eposy	epos	k1gInPc4	epos
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
a	a	k8xC	a
Rámájana	Rámájana	k1gFnSc1	Rámájana
představují	představovat	k5eAaImIp3nP	představovat
Védy	véd	k1gInPc1	véd
základ	základ	k1gInSc1	základ
veškeré	veškerý	k3xTgFnSc2	veškerý
epické	epický	k2eAgFnSc2d1	epická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
tradic	tradice	k1gFnPc2	tradice
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Árjové	Árjus	k1gMnPc1	Árjus
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Gangy	Ganga	k1gFnSc2	Ganga
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
opustili	opustit	k5eAaPmAgMnP	opustit
svůj	svůj	k3xOyFgInSc4	svůj
dřívější	dřívější	k2eAgInSc4d1	dřívější
nomádský	nomádský	k2eAgInSc4d1	nomádský
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgInSc2	ten
nastaly	nastat	k5eAaPmAgFnP	nastat
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
a	a	k8xC	a
politické	politický	k2eAgFnSc6d1	politická
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
se	se	k3xPyFc4	se
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
koncem	koncem	k7c2	koncem
védského	védský	k2eAgNnSc2d1	védské
období	období	k1gNnSc2	období
prosadil	prosadit	k5eAaPmAgInS	prosadit
kastovní	kastovní	k2eAgInSc1d1	kastovní
systém	systém	k1gInSc1	systém
uspořádání	uspořádání	k1gNnSc2	uspořádání
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
postavenou	postavený	k2eAgFnSc7d1	postavená
kastou	kasta	k1gFnSc7	kasta
byli	být	k5eAaImAgMnP	být
kněží	kněz	k1gMnPc1	kněz
(	(	kIx(	(
<g/>
brahmáni	brahmán	k1gMnPc1	brahmán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
následovali	následovat	k5eAaImAgMnP	následovat
vládci	vládce	k1gMnPc1	vládce
a	a	k8xC	a
válečníci	válečník	k1gMnPc1	válečník
(	(	kIx(	(
<g/>
kšátrijové	kšátrij	k1gMnPc1	kšátrij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
a	a	k8xC	a
rolníci	rolník	k1gMnPc1	rolník
(	(	kIx(	(
<g/>
vajšijové	vajšij	k1gMnPc1	vajšij
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
kasta	kasta	k1gFnSc1	kasta
otroků	otrok	k1gMnPc2	otrok
(	(	kIx(	(
<g/>
šúdrové	šúdrové	k2eAgMnPc3d1	šúdrové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
táhnoucího	táhnoucí	k2eAgNnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
dnešního	dnešní	k2eAgInSc2d1	dnešní
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
až	až	k9	až
k	k	k7c3	k
indickému	indický	k2eAgInSc3d1	indický
státu	stát	k1gInSc3	stát
Bihár	Bihár	k1gInSc4	Bihár
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
šestnáct	šestnáct	k4xCc4	šestnáct
velkých	velký	k2eAgNnPc2d1	velké
království	království	k1gNnPc2	království
a	a	k8xC	a
oligarchických	oligarchický	k2eAgFnPc2d1	oligarchická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
mahádžanapady	mahádžanapada	k1gFnSc2	mahádžanapada
<g/>
.	.	kIx.	.
</s>
<s>
Nejvlivnější	vlivný	k2eAgMnPc1d3	nejvlivnější
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
království	království	k1gNnSc2	království
Kóšalí	Kóšalý	k2eAgMnPc1d1	Kóšalý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dalšími	další	k2eAgInPc7d1	další
důležitými	důležitý	k2eAgInPc7d1	důležitý
byly	být	k5eAaImAgFnP	být
Avanti	avanti	k0	avanti
a	a	k8xC	a
Gandhára	Gandhár	k1gMnSc4	Gandhár
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mocné	mocný	k2eAgNnSc1d1	mocné
království	království	k1gNnSc1	království
Magadha	Magadha	k1gFnSc1	Magadha
podrobilo	podrobit	k5eAaPmAgNnS	podrobit
své	svůj	k3xOyFgMnPc4	svůj
sousedy	soused	k1gMnPc4	soused
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc4	jádro
indické	indický	k2eAgFnSc2d1	indická
civilizace	civilizace	k1gFnSc2	civilizace
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
Buddha	Buddha	k1gMnSc1	Buddha
a	a	k8xC	a
Mahávíra	Mahávír	k1gMnSc2	Mahávír
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
učení	učení	k1gNnSc2	učení
vzešla	vzejít	k5eAaPmAgFnS	vzejít
nová	nový	k2eAgFnSc1d1	nová
náboženství	náboženství	k1gNnSc4	náboženství
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
džinismus	džinismus	k1gInSc1	džinismus
<g/>
.	.	kIx.	.
</s>
<s>
Poříčí	Poříčí	k1gNnSc1	Poříčí
Indu	Indus	k1gInSc2	Indus
ovládali	ovládat	k5eAaImAgMnP	ovládat
místní	místní	k2eAgFnPc4d1	místní
králové	králová	k1gFnPc4	králová
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
520	[number]	k4	520
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
podmanil	podmanit	k5eAaPmAgMnS	podmanit
perský	perský	k2eAgMnSc1d1	perský
král	král	k1gMnSc1	král
Dareios	Dareios	k1gMnSc1	Dareios
I.	I.	kA	I.
Ovšem	ovšem	k9	ovšem
Magadha	Magadha	k1gFnSc1	Magadha
disponovala	disponovat	k5eAaBmAgFnS	disponovat
silnou	silný	k2eAgFnSc7d1	silná
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
expandovala	expandovat	k5eAaImAgFnS	expandovat
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
326	[number]	k4	326
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
překročil	překročit	k5eAaPmAgMnS	překročit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
svojí	svojit	k5eAaImIp3nS	svojit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
výpravou	výprava	k1gFnSc7	výprava
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Hindúkuš	Hindúkuš	k1gInSc4	Hindúkuš
a	a	k8xC	a
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Indu	Indus	k1gInSc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
<g/>
,	,	kIx,	,
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
Alexandr	Alexandr	k1gMnSc1	Alexandr
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Magadhou	Magadha	k1gFnSc7	Magadha
zakladatel	zakladatel	k1gMnSc1	zakladatel
maurjovské	maurjovská	k1gFnSc2	maurjovská
říše	říš	k1gFnSc2	říš
Čandragupta	Čandragupt	k1gInSc2	Čandragupt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
během	během	k7c2	během
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
učinil	učinit	k5eAaPmAgInS	učinit
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
Patáliputry	Patáliputr	k1gInPc4	Patáliputr
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
říše	říš	k1gFnSc2	říš
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
makedonskými	makedonský	k2eAgMnPc7d1	makedonský
satrapy	satrap	k1gMnPc7	satrap
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
a	a	k8xC	a
Balúčistánu	Balúčistán	k1gInSc2	Balúčistán
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
zisky	zisk	k1gInPc4	zisk
stvrdil	stvrdit	k5eAaPmAgInS	stvrdit
uzavřením	uzavření	k1gNnSc7	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
se	s	k7c7	s
Seleukem	Seleuk	k1gInSc7	Seleuk
I.	I.	kA	I.
Rozmach	rozmach	k1gInSc1	rozmach
maurjovské	maurjovský	k2eAgFnSc2d1	maurjovský
říše	říš	k1gFnSc2	říš
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ašóky	Ašóka	k1gMnSc2	Ašóka
<g/>
,	,	kIx,	,
Čandraguptova	Čandraguptův	k2eAgMnSc2d1	Čandraguptův
vnuka	vnuk	k1gMnSc2	vnuk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgInS	dobýt
jih	jih	k1gInSc4	jih
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
opanoval	opanovat	k5eAaPmAgInS	opanovat
tak	tak	k9	tak
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Indii	Indie	k1gFnSc4	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
letech	let	k1gInPc6	let
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
Ašóka	Ašóka	k1gMnSc1	Ašóka
zcela	zcela	k6eAd1	zcela
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
buddhismu	buddhismus	k1gInSc3	buddhismus
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
tolerance	tolerance	k1gFnSc2	tolerance
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgNnPc3d1	ostatní
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
se	se	k3xPyFc4	se
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
především	především	k9	především
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Nálanda	Nálando	k1gNnSc2	Nálando
a	a	k8xC	a
Takšašíla	Takšašílo	k1gNnSc2	Takšašílo
se	se	k3xPyFc4	se
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
učenci	učenec	k1gMnPc1	učenec
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Ašókově	Ašókův	k2eAgFnSc6d1	Ašókův
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
maurjovská	maurjovský	k2eAgFnSc1d1	maurjovský
říše	říše	k1gFnSc1	říše
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
přispělo	přispět	k5eAaPmAgNnS	přispět
dobytí	dobytí	k1gNnSc4	dobytí
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Indie	Indie	k1gFnSc2	Indie
řecko-baktrijským	řeckoaktrijský	k2eAgMnSc7d1	řecko-baktrijský
králem	král	k1gMnSc7	král
Démétriem	Démétrium	k1gNnSc7	Démétrium
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
úder	úder	k1gInSc4	úder
zasadili	zasadit	k5eAaPmAgMnP	zasadit
Maurjům	Maurj	k1gMnPc3	Maurj
asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
185	[number]	k4	185
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Šungové	Šung	k1gMnPc1	Šung
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jedno	jeden	k4xCgNnSc1	jeden
století	století	k1gNnSc2	století
ovládali	ovládat	k5eAaImAgMnP	ovládat
východní	východní	k2eAgFnSc4d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
Indii	Indie	k1gFnSc4	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
spadá	spadat	k5eAaPmIp3nS	spadat
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
buddhismu	buddhismus	k1gInSc2	buddhismus
a	a	k8xC	a
opětné	opětný	k2eAgNnSc4d1	opětné
nastolení	nastolení	k1gNnSc4	nastolení
bráhmanismu	bráhmanismus	k1gInSc2	bráhmanismus
<g/>
.	.	kIx.	.
</s>
<s>
Severozápad	severozápad	k1gInSc1	severozápad
byl	být	k5eAaImAgInS	být
vystavován	vystavován	k2eAgMnSc1d1	vystavován
dalším	další	k2eAgFnPc3d1	další
invazím	invaze	k1gFnPc3	invaze
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
zrodu	zrod	k1gInSc3	zrod
indo-řeckého	indo-řecký	k2eAgNnSc2d1	indo-řecký
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
si	se	k3xPyFc3	se
západní	západní	k2eAgFnSc4d1	západní
Indii	Indie	k1gFnSc4	Indie
pomanili	pomanit	k5eAaPmAgMnP	pomanit
Šakové	Šakus	k1gMnPc1	Šakus
neboli	neboli	k8xC	neboli
Indoskythové	Indoskythová	k1gFnPc1	Indoskythová
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
válčili	válčit	k5eAaImAgMnP	válčit
s	s	k7c7	s
dynastií	dynastie	k1gFnSc7	dynastie
Andhra	Andhra	k1gFnSc1	Andhra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ovládala	ovládat	k5eAaImAgFnS	ovládat
centrální	centrální	k2eAgFnSc1d1	centrální
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
říše	říše	k1gFnSc1	říše
Indoskythů	Indoskyth	k1gInPc2	Indoskyth
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
nomádskými	nomádský	k2eAgInPc7d1	nomádský
kmeny	kmen	k1gInPc7	kmen
Jüe-č	Jüe-č	k1gInSc1	Jüe-č
<g/>
'	'	kIx"	'
<g/>
i.	i.	k?	i.
Ty	ten	k3xDgFnPc1	ten
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgInP	usadit
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založily	založit	k5eAaPmAgFnP	založit
kušánskou	kušánský	k2eAgFnSc4d1	kušánský
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
udržovala	udržovat	k5eAaImAgFnS	udržovat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Kanišky	Kanišek	k1gInPc4	Kanišek
<g/>
,	,	kIx,	,
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
vládce	vládce	k1gMnSc4	vládce
Kušánů	Kušán	k1gInPc2	Kušán
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
velkého	velký	k2eAgMnSc4d1	velký
patrona	patron	k1gMnSc4	patron
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
sahala	sahat	k5eAaImAgFnS	sahat
jejich	jejich	k3xOp3gFnSc1	jejich
říše	říše	k1gFnSc1	říše
od	od	k7c2	od
Turkestánu	Turkestán	k1gInSc2	Turkestán
až	až	k6eAd1	až
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Gudžarátu	Gudžarát	k1gInSc2	Gudžarát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tlaku	tlak	k1gInSc2	tlak
Kušánů	Kušán	k1gMnPc2	Kušán
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
království	království	k1gNnSc2	království
Andhra	Andhra	k1gFnSc1	Andhra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
tamilských	tamilský	k2eAgInPc2d1	tamilský
států	stát	k1gInPc2	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
pádu	pád	k1gInSc6	pád
Maurjů	Maurj	k1gMnPc2	Maurj
byla	být	k5eAaImAgFnS	být
Indie	Indie	k1gFnSc1	Indie
důležitým	důležitý	k2eAgNnSc7d1	důležité
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
obchodovala	obchodovat	k5eAaImAgFnS	obchodovat
i	i	k9	i
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Indické	indický	k2eAgNnSc1d1	indické
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
sochařství	sochařství	k1gNnPc1	sochařství
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
jedinečné	jedinečný	k2eAgFnPc4d1	jedinečná
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Kušánská	Kušánský	k2eAgFnSc1d1	Kušánská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
království	království	k1gNnSc6	království
Andhra	Andhra	k1gFnSc1	Andhra
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
320	[number]	k4	320
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
Čandragupta	Čandragupta	k1gMnSc1	Čandragupta
I.	I.	kA	I.
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc2	vládce
Magadhy	Magadha	k1gMnSc2	Magadha
<g/>
,	,	kIx,	,
území	území	k1gNnSc1	území
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Gangy	Ganga	k1gFnSc2	Ganga
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
guptovskou	guptovský	k2eAgFnSc4d1	guptovský
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
<g/>
,	,	kIx,	,
Samudragupta	Samudragupta	k1gMnSc1	Samudragupta
a	a	k8xC	a
Čandragupta	Čandragupta	k1gMnSc1	Čandragupta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nabyli	nabýt	k5eAaPmAgMnP	nabýt
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dříve	dříve	k6eAd2	dříve
podařilo	podařit	k5eAaPmAgNnS	podařit
Maurjům	Maurj	k1gMnPc3	Maurj
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
od	od	k7c2	od
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
po	po	k7c4	po
Ásám	Ásám	k1gInSc4	Ásám
<g/>
,	,	kIx,	,
zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
obratným	obratný	k2eAgNnSc7d1	obratné
uzavíráním	uzavírání	k1gNnSc7	uzavírání
sňatků	sňatek	k1gInPc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konsolidaci	konsolidace	k1gFnSc3	konsolidace
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
přispívala	přispívat	k5eAaImAgFnS	přispívat
i	i	k9	i
tolerantní	tolerantní	k2eAgFnSc1d1	tolerantní
náboženská	náboženský	k2eAgFnSc1d1	náboženská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Guptovci	Guptovec	k1gMnPc1	Guptovec
přímo	přímo	k6eAd1	přímo
ovládali	ovládat	k5eAaImAgMnP	ovládat
pouze	pouze	k6eAd1	pouze
jádro	jádro	k1gNnSc4	jádro
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
nynějších	nynější	k2eAgInPc6d1	nynější
státech	stát	k1gInPc6	stát
Uttarpradéš	Uttarpradéš	k1gMnSc1	Uttarpradéš
a	a	k8xC	a
Bihár	Bihár	k1gMnSc1	Bihár
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dobyté	dobytý	k2eAgFnPc1d1	dobytá
oblasti	oblast	k1gFnPc1	oblast
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
spravovali	spravovat	k5eAaImAgMnP	spravovat
jim	on	k3xPp3gMnPc3	on
podřízení	podřízený	k1gMnPc1	podřízený
vládci	vládce	k1gMnPc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Guptovců	Guptovec	k1gMnPc2	Guptovec
zažívala	zažívat	k5eAaImAgFnS	zažívat
Indie	Indie	k1gFnSc1	Indie
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
své	svůj	k3xOyFgFnSc2	svůj
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
značného	značný	k2eAgInSc2d1	značný
rozkvětu	rozkvět	k1gInSc2	rozkvět
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Velkému	velký	k2eAgInSc3d1	velký
věhlasu	věhlas	k1gInSc3	věhlas
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
těší	těšit	k5eAaImIp3nS	těšit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
indický	indický	k2eAgMnSc1d1	indický
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Kálidása	Kálidása	k1gFnSc1	Kálidása
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
éry	éra	k1gFnSc2	éra
Guptovců	Guptovec	k1gInPc2	Guptovec
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
dokonalejší	dokonalý	k2eAgFnSc6d2	dokonalejší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
nuly	nula	k1gFnSc2	nula
a	a	k8xC	a
desítkové	desítkový	k2eAgFnSc2d1	desítková
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
přenesli	přenést	k5eAaPmAgMnP	přenést
Arabové	Arab	k1gMnPc1	Arab
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
představovalo	představovat	k5eAaImAgNnS	představovat
největší	veliký	k2eAgInSc4d3	veliký
soudobý	soudobý	k2eAgInSc4d1	soudobý
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
museli	muset	k5eAaImAgMnP	muset
Guptovci	Guptovec	k1gMnPc1	Guptovec
čelit	čelit	k5eAaImF	čelit
nájezdům	nájezd	k1gInPc3	nájezd
Hefthalitů	Hefthalit	k1gInPc2	Hefthalit
<g/>
,	,	kIx,	,
známých	známý	k2eAgFnPc2d1	známá
také	také	k9	také
jako	jako	k9	jako
Bílí	bílý	k2eAgMnPc1d1	bílý
Hunové	Hun	k1gMnPc1	Hun
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
opakované	opakovaný	k2eAgInPc1d1	opakovaný
útoky	útok	k1gInPc1	útok
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
guptovské	guptovský	k2eAgFnSc2d1	guptovský
říše	říš	k1gFnSc2	říš
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
550	[number]	k4	550
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řek	k1gMnSc1	řek
Huang	Huang	k1gMnSc1	Huang
He	he	k0	he
a	a	k8xC	a
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Číně	Čína	k1gFnSc6	Čína
začaly	začít	k5eAaPmAgFnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
neolitické	neolitický	k2eAgFnSc2d1	neolitická
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaPmAgMnP	věnovat
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
3000	[number]	k4	3000
–	–	k?	–
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jsou	být	k5eAaImIp3nP	být
archeologicky	archeologicky	k6eAd1	archeologicky
doložena	doložit	k5eAaPmNgFnS	doložit
první	první	k4xOgNnPc4	první
stálá	stálý	k2eAgNnPc4d1	stálé
lidská	lidský	k2eAgNnPc4d1	lidské
sídla	sídlo	k1gNnPc4	sídlo
s	s	k7c7	s
obrannými	obranný	k2eAgFnPc7d1	obranná
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
velice	velice	k6eAd1	velice
skromné	skromný	k2eAgInPc4d1	skromný
historické	historický	k2eAgInPc4d1	historický
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
počátky	počátek	k1gInPc1	počátek
čínských	čínský	k2eAgFnPc2d1	čínská
dějin	dějiny	k1gFnPc2	dějiny
jsou	být	k5eAaImIp3nP	být
zahaleny	zahalit	k5eAaPmNgInP	zahalit
pověstmi	pověst	k1gFnPc7	pověst
o	o	k7c6	o
řadě	řada	k1gFnSc6	řada
legendárních	legendární	k2eAgFnPc2d1	legendární
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
2100	[number]	k4	2100
–	–	k?	–
1600	[number]	k4	1600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
měla	mít	k5eAaImAgFnS	mít
vládnout	vládnout	k5eAaImF	vládnout
dynastie	dynastie	k1gFnSc1	dynastie
Sia	Sia	k1gFnSc2	Sia
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
panování	panování	k1gNnSc1	panování
se	se	k3xPyFc4	se
časově	časově	k6eAd1	časově
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
objevy	objev	k1gInPc4	objev
bronzových	bronzový	k2eAgInPc2d1	bronzový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Sia	Sia	k?	Sia
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dynastií	dynastie	k1gFnSc7	dynastie
Šang	Šang	k1gInSc1	Šang
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
území	území	k1gNnSc3	území
střední	střední	k2eAgFnSc2d1	střední
Číny	Čína	k1gFnSc2	Čína
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
i	i	k9	i
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgInPc4	první
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
formu	forma	k1gFnSc4	forma
nápisů	nápis	k1gInPc2	nápis
na	na	k7c6	na
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kosterních	kosterní	k2eAgFnPc6d1	kosterní
schránkách	schránka	k1gFnPc6	schránka
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgFnPc6d1	sloužící
k	k	k7c3	k
věštění	věštění	k1gNnSc3	věštění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
používaných	používaný	k2eAgInPc2d1	používaný
symbolů	symbol	k1gInPc2	symbol
znázorňujících	znázorňující	k2eAgNnPc2d1	znázorňující
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
moderní	moderní	k2eAgNnSc1d1	moderní
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Šang	Šang	k1gInSc1	Šang
nastal	nastat	k5eAaPmAgInS	nastat
růst	růst	k1gInSc4	růst
měst	město	k1gNnPc2	město
se	s	k7c7	s
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
řemeslnou	řemeslný	k2eAgFnSc7d1	řemeslná
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
charakter	charakter	k1gInSc1	charakter
společnosti	společnost	k1gFnSc2	společnost
zůstával	zůstávat	k5eAaImAgInS	zůstávat
převážně	převážně	k6eAd1	převážně
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
vládci	vládce	k1gMnPc1	vládce
Šang	Šanga	k1gFnPc2	Šanga
sídlili	sídlit	k5eAaImAgMnP	sídlit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
An-jang	Ananga	k1gFnPc2	An-janga
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
provincii	provincie	k1gFnSc6	provincie
Che-nan	Cheana	k1gFnPc2	Che-nana
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Huang	Huang	k1gMnSc1	Huang
He	he	k0	he
polobarbarský	polobarbarský	k2eAgInSc1d1	polobarbarský
národ	národ	k1gInSc1	národ
Čou	Čou	k1gFnSc2	Čou
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jeho	jeho	k3xOp3gMnSc1	jeho
král	král	k1gMnSc1	král
Wu	Wu	k1gMnSc1	Wu
Wang	Wang	k1gMnSc1	Wang
získal	získat	k5eAaPmAgMnS	získat
území	území	k1gNnSc4	území
dynastie	dynastie	k1gFnSc2	dynastie
Šang	Šanga	k1gFnPc2	Šanga
<g/>
.	.	kIx.	.
</s>
<s>
Čou	Čou	k?	Čou
vládli	vládnout	k5eAaImAgMnP	vládnout
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
města	město	k1gNnSc2	město
Čchang-an	Čchangny	k1gInPc2	Čchang-any
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
poté	poté	k6eAd1	poté
přenesli	přenést	k5eAaPmAgMnP	přenést
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Luo-jangu	Luoang	k1gInSc2	Luo-jang
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
ospravedlnili	ospravedlnit	k5eAaPmAgMnP	ospravedlnit
svoji	svůj	k3xOyFgFnSc4	svůj
nadvládu	nadvláda	k1gFnSc4	nadvláda
zavedli	zavést	k5eAaPmAgMnP	zavést
mandát	mandát	k1gInSc1	mandát
Nebes	nebesa	k1gNnPc2	nebesa
(	(	kIx(	(
<g/>
Tian	Tian	k1gMnSc1	Tian
Ming	Ming	k1gMnSc1	Ming
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
ideologický	ideologický	k2eAgInSc4d1	ideologický
koncept	koncept	k1gInSc4	koncept
přejala	přejmout	k5eAaPmAgFnS	přejmout
většina	většina	k1gFnSc1	většina
po	po	k7c6	po
nich	on	k3xPp3gFnPc2	on
panujících	panující	k2eAgFnPc2d1	panující
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
králové	králová	k1gFnPc1	králová
Čou	Čou	k1gFnSc2	Čou
se	se	k3xPyFc4	se
nepokusili	pokusit	k5eNaPmAgMnP	pokusit
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
si	se	k3xPyFc3	se
přímou	přímý	k2eAgFnSc4d1	přímá
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
jimi	on	k3xPp3gMnPc7	on
dobytým	dobytý	k2eAgNnSc7d1	dobyté
teritoriem	teritorium	k1gNnSc7	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
vybrané	vybraný	k2eAgMnPc4d1	vybraný
stoupence	stoupenec	k1gMnPc4	stoupenec
a	a	k8xC	a
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spravovali	spravovat	k5eAaImAgMnP	spravovat
kraje	kraj	k1gInSc2	kraj
obklopující	obklopující	k2eAgFnSc4d1	obklopující
královskou	královský	k2eAgFnSc4d1	královská
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
Vládcům	vládce	k1gMnPc3	vládce
dynastie	dynastie	k1gFnSc2	dynastie
Čou	Čou	k1gMnSc2	Čou
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
udržovat	udržovat	k5eAaImF	udržovat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
svými	svůj	k3xOyFgMnPc7	svůj
vazaly	vazal	k1gMnPc7	vazal
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
staletí	staletí	k1gNnPc4	staletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
770	[number]	k4	770
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
těchto	tento	k3xDgMnPc2	tento
vazalů	vazal	k1gMnPc2	vazal
vzbouřilo	vzbouřit	k5eAaPmAgNnS	vzbouřit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
kmenů	kmen	k1gInPc2	kmen
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
autorita	autorita	k1gFnSc1	autorita
Čou	Čou	k1gMnPc4	Čou
značně	značně	k6eAd1	značně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
nastal	nastat	k5eAaPmAgInS	nastat
výrazný	výrazný	k2eAgInSc4d1	výrazný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc4d1	sociální
vzestup	vzestup	k1gInSc4	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
používání	používání	k1gNnSc2	používání
železných	železný	k2eAgMnPc2d1	železný
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
vylepšených	vylepšený	k2eAgFnPc2d1	vylepšená
zavlažovacích	zavlažovací	k2eAgFnPc2d1	zavlažovací
technik	technika	k1gFnPc2	technika
byly	být	k5eAaImAgInP	být
zvýšeny	zvýšen	k2eAgInPc1d1	zvýšen
výnosy	výnos	k1gInPc1	výnos
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgInSc1d1	pokračující
rozklad	rozklad	k1gInSc1	rozklad
centrální	centrální	k2eAgFnSc2d1	centrální
moci	moc	k1gFnSc2	moc
vedl	vést	k5eAaImAgInS	vést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
k	k	k7c3	k
počátku	počátek	k1gInSc2	počátek
období	období	k1gNnSc2	období
Jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
Podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
místních	místní	k2eAgMnPc2d1	místní
vojenských	vojenský	k2eAgMnPc2d1	vojenský
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
potomky	potomek	k1gMnPc4	potomek
vazalů	vazal	k1gMnPc2	vazal
dosazených	dosazený	k2eAgFnPc2d1	dosazená
Čou	Čou	k1gFnPc2	Čou
<g/>
,	,	kIx,	,
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c6	o
nastolení	nastolení	k1gNnSc6	nastolení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hegemonie	hegemonie	k1gFnSc2	hegemonie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
stability	stabilita	k1gFnPc4	stabilita
uzavíráním	uzavírání	k1gNnSc7	uzavírání
spojenectví	spojenectví	k1gNnSc2	spojenectví
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
státy	stát	k1gInPc1	stát
těchto	tento	k3xDgMnPc2	tento
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
aliancí	aliance	k1gFnSc7	aliance
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
začala	začít	k5eAaPmAgFnS	začít
poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
Čou	Čou	k1gFnSc1	Čou
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgNnPc4d1	nazývané
období	období	k1gNnPc4	období
válčících	válčící	k2eAgInPc2d1	válčící
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
častým	častý	k2eAgInPc3d1	častý
a	a	k8xC	a
krvavým	krvavý	k2eAgInPc3d1	krvavý
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
sedmi	sedm	k4xCc7	sedm
dominujícími	dominující	k2eAgInPc7d1	dominující
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Čchu	Čch	k2eAgFnSc4d1	Čch
<g/>
,	,	kIx,	,
Chan	Chan	k1gNnSc4	Chan
<g/>
,	,	kIx,	,
Čchi	Čchi	k1gNnSc4	Čchi
<g/>
,	,	kIx,	,
Čchin	Čchin	k1gMnSc1	Čchin
<g/>
,	,	kIx,	,
Wej	Wej	k1gMnSc1	Wej
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Čchao	Čchao	k1gMnSc1	Čchao
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
válčení	válčení	k1gNnSc2	válčení
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
věnovaly	věnovat	k5eAaImAgInP	věnovat
i	i	k9	i
vojenské	vojenský	k2eAgFnSc3d1	vojenská
expanzi	expanze	k1gFnSc3	expanze
mimo	mimo	k7c4	mimo
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
hranice	hranice	k1gFnPc4	hranice
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
k	k	k7c3	k
dobytí	dobytí	k1gNnSc3	dobytí
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Sečuánu	Sečuán	k1gInSc2	Sečuán
a	a	k8xC	a
Liao-ningu	Liaoing	k1gInSc2	Liao-ning
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
jižní	jižní	k2eAgInSc1d1	jižní
stát	stát	k1gInSc1	stát
Čchu	Čchus	k1gInSc2	Čchus
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svoji	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
dynastie	dynastie	k1gFnSc1	dynastie
Čou	Čou	k1gFnSc1	Čou
zcela	zcela	k6eAd1	zcela
postrádala	postrádat	k5eAaImAgFnS	postrádat
reálnou	reálný	k2eAgFnSc4d1	reálná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
formální	formální	k2eAgFnSc1d1	formální
vláda	vláda	k1gFnSc1	vláda
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
256	[number]	k4	256
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
doba	doba	k1gFnSc1	doba
pozdní	pozdní	k2eAgFnSc1d1	pozdní
neboli	neboli	k8xC	neboli
Východní	východní	k2eAgFnSc1d1	východní
Čou	Čou	k1gFnSc1	Čou
byla	být	k5eAaImAgFnS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
společenskými	společenský	k2eAgInPc7d1	společenský
otřesy	otřes	k1gInPc7	otřes
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
intelektuálním	intelektuální	k2eAgInSc7d1	intelektuální
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc7d1	kulturní
rozkvětem	rozkvět	k1gInSc7	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
tehdy	tehdy	k6eAd1	tehdy
působilo	působit	k5eAaImAgNnS	působit
mnoho	mnoho	k4c1	mnoho
filozofů	filozof	k1gMnPc2	filozof
a	a	k8xC	a
teoretiků	teoretik	k1gMnPc2	teoretik
a	a	k8xC	a
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
také	také	k9	také
koncepce	koncepce	k1gFnSc1	koncepce
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
a	a	k8xC	a
současně	současně	k6eAd1	současně
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
doplňujících	doplňující	k2eAgFnPc2d1	doplňující
sil	síla	k1gFnPc2	síla
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
Jang	Jang	k1gMnSc1	Jang
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Sun-	Sun-	k1gMnSc1	Sun-
c	c	k0	c
<g/>
'	'	kIx"	'
dílo	dílo	k1gNnSc1	dílo
Umění	umění	k1gNnSc1	umění
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
filozofických	filozofický	k2eAgInPc2d1	filozofický
směrů	směr	k1gInPc2	směr
vynikají	vynikat	k5eAaImIp3nP	vynikat
především	především	k9	především
konfucianismus	konfucianismus	k1gInSc4	konfucianismus
<g/>
,	,	kIx,	,
taoismus	taoismus	k1gInSc4	taoismus
a	a	k8xC	a
legismus	legismus	k1gInSc4	legismus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
nejvlivnějším	vlivný	k2eAgMnPc3d3	nejvlivnější
filozofům	filozof	k1gMnPc3	filozof
této	tento	k3xDgFnSc2	tento
epochy	epocha	k1gFnSc2	epocha
náleželi	náležet	k5eAaImAgMnP	náležet
Konfucius	Konfucius	k1gInSc4	Konfucius
<g/>
,	,	kIx,	,
Lao-c	Lao	k1gFnSc4	Lao-c
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgNnSc2	jenž
učení	učení	k1gNnSc2	učení
vzešel	vzejít	k5eAaPmAgInS	vzejít
taoismus	taoismus	k1gInSc1	taoismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Konfuciův	Konfuciův	k2eAgMnSc1d1	Konfuciův
žák	žák	k1gMnSc1	žák
Mencius	Mencius	k1gMnSc1	Mencius
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
vliv	vliv	k1gInSc4	vliv
Čchin	Čchin	k2eAgInSc4d1	Čchin
<g/>
,	,	kIx,	,
nejzápadnějšího	západní	k2eAgMnSc2d3	nejzápadnější
z	z	k7c2	z
čínských	čínský	k2eAgInPc2d1	čínský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
provedl	provést	k5eAaPmAgInS	provést
reformy	reforma	k1gFnPc4	reforma
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
filozofie	filozofie	k1gFnSc2	filozofie
legalismu	legalismus	k1gInSc2	legalismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zničil	zničit	k5eAaPmAgInS	zničit
Čchin	Čchin	k1gInSc1	Čchin
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
zbývající	zbývající	k2eAgMnPc4d1	zbývající
soupeře	soupeř	k1gMnPc4	soupeř
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
221	[number]	k4	221
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přijal	přijmout	k5eAaPmAgMnS	přijmout
jeho	jeho	k3xOp3gMnSc1	jeho
král	král	k1gMnSc1	král
titul	titul	k1gInSc4	titul
císaře	císař	k1gMnSc4	císař
(	(	kIx(	(
<g/>
chuang-ti	chuang	k5eAaImF	chuang-t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazýval	nazývat	k5eAaImAgInS	nazývat
Čchin	Čchin	k1gInSc1	Čchin
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-chuang-ti	huang	k2eAgMnPc1d1	-chuang-t
(	(	kIx(	(
<g/>
První	první	k4xOgMnSc1	první
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
další	další	k2eAgNnSc1d1	další
panování	panování	k1gNnSc1	panování
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
agresivními	agresivní	k2eAgInPc7d1	agresivní
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
výboji	výboj	k1gInPc7	výboj
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
posunul	posunout	k5eAaPmAgMnS	posunout
hranice	hranice	k1gFnPc4	hranice
Číny	Čína	k1gFnSc2	Čína
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
zahájil	zahájit	k5eAaPmAgMnS	zahájit
budování	budování	k1gNnSc4	budování
Velké	velký	k2eAgFnSc2d1	velká
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
nájezdy	nájezd	k1gInPc7	nájezd
nomádů	nomád	k1gMnPc2	nomád
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
prosadil	prosadit	k5eAaPmAgMnS	prosadit
císař	císař	k1gMnSc1	císař
silně	silně	k6eAd1	silně
restriktivní	restriktivní	k2eAgNnSc4d1	restriktivní
zákonodárství	zákonodárství	k1gNnSc4	zákonodárství
a	a	k8xC	a
centralizaci	centralizace	k1gFnSc4	centralizace
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
opírající	opírající	k2eAgFnPc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
efektivní	efektivní	k2eAgInSc4d1	efektivní
správní	správní	k2eAgInSc4d1	správní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
však	však	k9	však
charakterizována	charakterizován	k2eAgFnSc1d1	charakterizována
taktéž	taktéž	k?	taktéž
brutálním	brutální	k2eAgInSc7d1	brutální
postupem	postup	k1gInSc7	postup
vůči	vůči	k7c3	vůči
politické	politický	k2eAgFnSc3d1	politická
opozici	opozice	k1gFnSc3	opozice
a	a	k8xC	a
ostatním	ostatní	k2eAgFnPc3d1	ostatní
filozofickým	filozofický	k2eAgFnPc3d1	filozofická
školám	škola	k1gFnPc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
císařově	císařův	k2eAgFnSc6d1	císařova
smrti	smrt	k1gFnSc6	smrt
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vzešla	vzejít	k5eAaPmAgFnS	vzejít
vítězně	vítězně	k6eAd1	vítězně
nová	nový	k2eAgFnSc1d1	nová
dynastie	dynastie	k1gFnSc1	dynastie
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
k	k	k7c3	k
filozofii	filozofie	k1gFnSc3	filozofie
konfucianismu	konfucianismus	k1gInSc2	konfucianismus
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
Kao-cu	Kaous	k1gInSc2	Kao-cus
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc4	první
císaře	císař	k1gMnSc4	císař
Chan	Chan	k1gInSc4	Chan
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispívaly	přispívat	k5eAaImAgInP	přispívat
také	také	k9	také
vpády	vpád	k1gInPc1	vpád
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
Hsiung-nu	Hsiung	k1gInSc2	Hsiung-n
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Kao-cu	Kaou	k6eAd1	Kao-cu
proto	proto	k8xC	proto
nechal	nechat	k5eAaPmAgMnS	nechat
snížit	snížit	k5eAaPmF	snížit
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
enormní	enormní	k2eAgFnSc2d1	enormní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
správu	správa	k1gFnSc4	správa
státu	stát	k1gInSc2	stát
a	a	k8xC	a
přerozdělil	přerozdělit	k5eAaPmAgMnS	přerozdělit
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
reformy	reforma	k1gFnPc1	reforma
přivodily	přivodit	k5eAaBmAgFnP	přivodit
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc4d1	kulturní
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc4d1	vojenský
rozmach	rozmach	k1gInSc4	rozmach
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
podnikl	podniknout	k5eAaPmAgMnS	podniknout
"	"	kIx"	"
<g/>
vojácký	vojácký	k2eAgMnSc1d1	vojácký
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
Wu-ti	Wu	k1gMnPc1	Wu-t
velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
expedici	expedice	k1gFnSc4	expedice
proti	proti	k7c3	proti
Hsiung-nu	Hsiungo	k1gNnSc3	Hsiung-no
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
a	a	k8xC	a
zahnal	zahnat	k5eAaPmAgMnS	zahnat
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Wu-ti	Wu	k5eAaImF	Wu-t
si	se	k3xPyFc3	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
kmeny	kmen	k1gInPc4	kmen
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
následně	následně	k6eAd1	následně
konsolidoval	konsolidovat	k5eAaBmAgMnS	konsolidovat
dobytá	dobytý	k2eAgNnPc4d1	dobyté
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
začala	začít	k5eAaPmAgFnS	začít
takřka	takřka	k6eAd1	takřka
celé	celý	k2eAgInPc4d1	celý
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
trvající	trvající	k2eAgFnSc1d1	trvající
éra	éra	k1gFnSc1	éra
míru	mír	k1gInSc2	mír
a	a	k8xC	a
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispívalo	přispívat	k5eAaImAgNnS	přispívat
i	i	k9	i
obchodní	obchodní	k2eAgNnSc1d1	obchodní
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
zajišťované	zajišťovaný	k2eAgNnSc1d1	zajišťované
hedvábnou	hedvábný	k2eAgFnSc7d1	hedvábná
stezkou	stezka	k1gFnSc7	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
Západní	západní	k2eAgMnSc1d1	západní
Chan	Chan	k1gMnSc1	Chan
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
9	[number]	k4	9
n.	n.	k?	n.
l.	l.	k?	l.
uzurpace	uzurpace	k1gFnSc2	uzurpace
Wang	Wanga	k1gFnPc2	Wanga
Manga	mango	k1gNnSc2	mango
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
dynastie	dynastie	k1gFnSc2	dynastie
Hsin	Hsin	k1gMnSc1	Hsin
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
provést	provést	k5eAaPmF	provést
zásadní	zásadní	k2eAgFnPc4d1	zásadní
pozemkové	pozemkový	k2eAgFnPc4d1	pozemková
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
omezit	omezit	k5eAaPmF	omezit
velké	velký	k2eAgMnPc4d1	velký
vlastníky	vlastník	k1gMnPc4	vlastník
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
23	[number]	k4	23
byl	být	k5eAaImAgInS	být
však	však	k9	však
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
císař	císař	k1gMnSc1	císař
Kuang-wu	Kuangus	k1gInSc2	Kuang-wus
nastolil	nastolit	k5eAaPmAgMnS	nastolit
vládu	vláda	k1gFnSc4	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Východní	východní	k2eAgInSc1d1	východní
Chan	Chan	k1gInSc1	Chan
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
do	do	k7c2	do
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obnovila	obnovit	k5eAaPmAgFnS	obnovit
někdejší	někdejší	k2eAgFnSc1d1	někdejší
hranice	hranice	k1gFnSc1	hranice
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
započal	započnout	k5eAaPmAgInS	započnout
úpadek	úpadek	k1gInSc1	úpadek
Chanů	Chan	k1gMnPc2	Chan
zapříčiněný	zapříčiněný	k2eAgMnSc1d1	zapříčiněný
slabostí	slabost	k1gFnSc7	slabost
a	a	k8xC	a
nevýkonností	nevýkonnost	k1gFnSc7	nevýkonnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
rozbroji	rozbroj	k1gInSc3	rozbroj
různých	různý	k2eAgMnPc2d1	různý
mocných	mocný	k2eAgMnPc2d1	mocný
klanů	klan	k1gInPc2	klan
a	a	k8xC	a
intrikami	intrika	k1gFnPc7	intrika
eunuchů	eunuch	k1gMnPc2	eunuch
v	v	k7c6	v
císařském	císařský	k2eAgInSc6d1	císařský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pozdní	pozdní	k2eAgMnSc1d1	pozdní
Chan	Chan	k1gMnSc1	Chan
zesílil	zesílit	k5eAaPmAgMnS	zesílit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
vliv	vliv	k1gInSc4	vliv
buddhismu	buddhismus	k1gInSc2	buddhismus
šířeného	šířený	k2eAgInSc2d1	šířený
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
již	jenž	k3xRgFnSc4	jenž
říši	říše	k1gFnSc4	říše
zcela	zcela	k6eAd1	zcela
pohltily	pohltit	k5eAaPmAgInP	pohltit
politické	politický	k2eAgInPc1d1	politický
zmatky	zmatek	k1gInPc1	zmatek
a	a	k8xC	a
opakovaná	opakovaný	k2eAgNnPc1d1	opakované
povstání	povstání	k1gNnPc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
vzpoura	vzpoura	k1gFnSc1	vzpoura
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
189	[number]	k4	189
vládu	vláda	k1gFnSc4	vláda
eunuchů	eunuch	k1gMnPc2	eunuch
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
poté	poté	k6eAd1	poté
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
naprostému	naprostý	k2eAgInSc3d1	naprostý
rozkladu	rozklad	k1gInSc3	rozklad
panství	panství	k1gNnSc2	panství
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
moc	moc	k1gFnSc4	moc
drželi	držet	k5eAaImAgMnP	držet
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
rukou	ruka	k1gFnSc7	ruka
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
provincie	provincie	k1gFnPc4	provincie
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Abdikací	abdikace	k1gFnSc7	abdikace
posledního	poslední	k2eAgMnSc2d1	poslední
císaře	císař	k1gMnSc2	císař
Chan	Chana	k1gFnPc2	Chana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
220	[number]	k4	220
započalo	započnout	k5eAaPmAgNnS	započnout
období	období	k1gNnSc2	období
tří	tři	k4xCgNnPc2	tři
království	království	k1gNnPc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
země	zem	k1gFnSc2	zem
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
království	království	k1gNnSc1	království
Wu	Wu	k1gFnSc2	Wu
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
království	království	k1gNnSc1	království
Šu-chan	Šuhana	k1gFnPc2	Šu-chana
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
dominovalo	dominovat	k5eAaImAgNnS	dominovat
království	království	k1gNnSc1	království
Wej	Wej	k1gFnSc2	Wej
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
potýkat	potýkat	k5eAaImF	potýkat
se	s	k7c7	s
zásadními	zásadní	k2eAgInPc7d1	zásadní
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
ale	ale	k9	ale
nebránilo	bránit	k5eNaImAgNnS	bránit
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
rozhořčených	rozhořčený	k2eAgFnPc2d1	rozhořčená
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
této	tento	k3xDgFnSc2	tento
neklidné	klidný	k2eNgFnSc2d1	neklidná
éry	éra	k1gFnSc2	éra
nastal	nastat	k5eAaPmAgInS	nastat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
260	[number]	k4	260
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
ujala	ujmout	k5eAaPmAgFnS	ujmout
dynastie	dynastie	k1gFnSc1	dynastie
Ťin	Ťin	k1gFnSc1	Ťin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
krátkodobě	krátkodobě	k6eAd1	krátkodobě
znovu	znovu	k6eAd1	znovu
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
však	však	k9	však
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
jejími	její	k3xOp3gMnPc7	její
princi	princ	k1gMnPc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
slabosti	slabost	k1gFnSc3	slabost
říše	říš	k1gFnSc2	říš
využili	využít	k5eAaPmAgMnP	využít
Hsiung-nu	Hsiunga	k1gFnSc4	Hsiung-na
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ke	k	k7c3	k
vpádu	vpád	k1gInSc3	vpád
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zajali	zajmout	k5eAaPmAgMnP	zajmout
a	a	k8xC	a
popravili	popravit	k5eAaPmAgMnP	popravit
poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
císaře	císař	k1gMnSc2	císař
Západní	západní	k2eAgFnPc1d1	západní
Ťin	Ťin	k1gFnPc1	Ťin
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
dynastie	dynastie	k1gFnSc2	dynastie
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Nankingu	Nanking	k1gInSc2	Nanking
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
dynastii	dynastie	k1gFnSc4	dynastie
Východní	východní	k2eAgFnSc2d1	východní
Ťin	Ťin	k1gFnSc2	Ťin
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
říše	říš	k1gFnSc2	říš
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
ovšem	ovšem	k9	ovšem
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
nomádských	nomádský	k2eAgInPc2d1	nomádský
kmenů	kmen	k1gInPc2	kmen
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
589	[number]	k4	589
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
stabilní	stabilní	k2eAgFnSc4d1	stabilní
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
celkem	celkem	k6eAd1	celkem
šestnáct	šestnáct	k4xCc4	šestnáct
království	království	k1gNnPc2	království
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
panovníky	panovník	k1gMnPc7	panovník
nečínského	čínský	k2eNgInSc2d1	čínský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Ťin	Ťin	k1gFnSc2	Ťin
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
svržena	svrhnout	k5eAaPmNgFnS	svrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
420	[number]	k4	420
<g/>
.	.	kIx.	.
</s>
