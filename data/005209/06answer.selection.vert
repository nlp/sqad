<s>
První	první	k4xOgFnPc1	první
starověké	starověký	k2eAgFnPc1d1	starověká
civilizace	civilizace	k1gFnPc1	civilizace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
eneolitu	eneolit	k1gInSc2	eneolit
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
(	(	kIx(	(
<g/>
polní	polní	k2eAgNnSc1d1	polní
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenskou	společenský	k2eAgFnSc7d1	společenská
dělbou	dělba	k1gFnSc7	dělba
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
základními	základní	k2eAgFnPc7d1	základní
technikami	technika	k1gFnPc7	technika
zpracování	zpracování	k1gNnSc2	zpracování
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
skladováním	skladování	k1gNnSc7	skladování
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
založením	založení	k1gNnSc7	založení
prvních	první	k4xOgFnPc2	první
stálých	stálý	k2eAgFnPc2d1	stálá
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
