<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
počas	počasa	k1gFnPc2	počasa
kterého	který	k3yRgMnSc4	který
vznikali	vznikat	k5eAaImAgMnP	vznikat
první	první	k4xOgFnPc4	první
starověké	starověký	k2eAgFnPc4d1	starověká
civilizace	civilizace	k1gFnPc4	civilizace
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
mědi	měď	k1gFnSc2	měď
<g/>
?	?	kIx.	?
</s>
