<p>
<s>
Úsměv	úsměv	k1gInSc1	úsměv
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
grimasa	grimasa	k1gFnSc1	grimasa
<g/>
,	,	kIx,	,
provázející	provázející	k2eAgFnSc4d1	provázející
radostné	radostný	k2eAgNnSc4d1	radostné
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Úsměvem	úsměv	k1gInSc7	úsměv
reagujeme	reagovat	k5eAaBmIp1nP	reagovat
na	na	k7c4	na
vtip	vtip	k1gInSc4	vtip
<g/>
,	,	kIx,	,
radostnou	radostný	k2eAgFnSc4d1	radostná
novinu	novina	k1gFnSc4	novina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
nebo	nebo	k8xC	nebo
pozdrav	pozdrav	k1gInSc4	pozdrav
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgInSc1d1	specifický
úsměv	úsměv	k1gInSc1	úsměv
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
také	také	k6eAd1	také
např.	např.	kA	např.
ironii	ironie	k1gFnSc4	ironie
nebo	nebo	k8xC	nebo
výsměch	výsměch	k1gInSc4	výsměch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
úsměvu	úsměv	k1gInSc6	úsměv
člověk	člověk	k1gMnSc1	člověk
pozvedne	pozvednout	k5eAaPmIp3nS	pozvednout
ústní	ústní	k2eAgInPc4d1	ústní
koutky	koutek	k1gInPc4	koutek
směrem	směr	k1gInSc7	směr
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
poodhalí	poodhalit	k5eAaPmIp3nP	poodhalit
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smíchu	smích	k1gInSc6	smích
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
úsměvu	úsměv	k1gInSc2	úsměv
zapojí	zapojit	k5eAaPmIp3nS	zapojit
také	také	k9	také
bránice	bránice	k1gFnSc1	bránice
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
psychologové	psycholog	k1gMnPc1	psycholog
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
úsměv	úsměv	k1gInSc4	úsměv
na	na	k7c4	na
upřímný	upřímný	k2eAgInSc4d1	upřímný
a	a	k8xC	a
hraný	hraný	k2eAgInSc4d1	hraný
(	(	kIx(	(
<g/>
křečovitý	křečovitý	k2eAgInSc4d1	křečovitý
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
aktivace	aktivace	k1gFnSc2	aktivace
určitých	určitý	k2eAgInPc2d1	určitý
mimických	mimický	k2eAgInPc2d1	mimický
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
problematika	problematika	k1gFnSc1	problematika
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
škála	škála	k1gFnSc1	škála
úsměvů	úsměv	k1gInPc2	úsměv
u	u	k7c2	u
každého	každý	k3xTgMnSc4	každý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
nesmírně	smírně	k6eNd1	smírně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Duchenneův	Duchenneův	k2eAgInSc4d1	Duchenneův
úsměv	úsměv	k1gInSc4	úsměv
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
objevil	objevit	k5eAaPmAgMnS	objevit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
neurolog	neurolog	k1gMnSc1	neurolog
Guillaume	Guillaum	k1gInSc5	Guillaum
Duchenne	Duchenn	k1gInSc5	Duchenn
během	během	k7c2	během
výzkumu	výzkum	k1gInSc2	výzkum
fyziologie	fyziologie	k1gFnSc2	fyziologie
výrazů	výraz	k1gInPc2	výraz
obličeje	obličej	k1gInSc2	obličej
dva	dva	k4xCgInPc1	dva
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
typy	typ	k1gInPc1	typ
úsměvů	úsměv	k1gInPc2	úsměv
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
Duchenneově	Duchenneův	k2eAgInSc6d1	Duchenneův
úsměvu	úsměv	k1gInSc6	úsměv
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
stahu	stah	k1gInSc2	stah
jak	jak	k8xC	jak
hlavního	hlavní	k2eAgNnSc2d1	hlavní
zygomatického	zygomatický	k2eAgNnSc2d1	zygomatický
(	(	kIx(	(
<g/>
lícního	lícní	k2eAgMnSc2d1	lícní
<g/>
)	)	kIx)	)
svalu	sval	k1gInSc2	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gMnSc1	musculus
zygomaticus	zygomaticus	k1gMnSc1	zygomaticus
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zvedá	zvedat	k5eAaImIp3nS	zvedat
koutky	koutek	k1gInPc4	koutek
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
tak	tak	k9	tak
kruhového	kruhový	k2eAgInSc2d1	kruhový
očního	oční	k2eAgInSc2d1	oční
svalu	sval	k1gInSc2	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gInSc1	musculus
orbicularis	orbicularis	k1gFnSc2	orbicularis
oculi	ocule	k1gFnSc4	ocule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svalu	sval	k1gInSc2	sval
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvedá	zvedat	k5eAaImIp3nS	zvedat
tváře	tvář	k1gFnPc4	tvář
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
vějířky	vějířek	k1gInPc1	vějířek
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
jiný	jiný	k2eAgInSc4d1	jiný
úsměv	úsměv	k1gInSc4	úsměv
než	než	k8xS	než
Duchenneův	Duchenneův	k2eAgInSc4d1	Duchenneův
<g/>
,	,	kIx,	,
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
pouze	pouze	k6eAd1	pouze
hlavní	hlavní	k2eAgInSc4d1	hlavní
zygomatický	zygomatický	k2eAgInSc4d1	zygomatický
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
dřívějších	dřívější	k2eAgInPc2d1	dřívější
výzkumů	výzkum	k1gInPc2	výzkum
provedených	provedený	k2eAgInPc2d1	provedený
na	na	k7c6	na
dospělých	dospělý	k2eAgMnPc6d1	dospělý
jedincích	jedinec	k1gMnPc6	jedinec
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocit	pocit	k1gInSc1	pocit
radosti	radost	k1gFnSc2	radost
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
úsměvem	úsměv	k1gInSc7	úsměv
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgMnSc2	který
jsou	být	k5eAaImIp3nP	být
pomocí	pomocí	k7c2	pomocí
hlavního	hlavní	k2eAgInSc2d1	hlavní
zygomatického	zygomatický	k2eAgInSc2d1	zygomatický
svalu	sval	k1gInSc2	sval
zdviženy	zdvižen	k2eAgInPc4d1	zdvižen
koutky	koutek	k1gInPc4	koutek
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
studie	studie	k1gFnPc1	studie
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
pozitivními	pozitivní	k2eAgFnPc7d1	pozitivní
emocemi	emoce	k1gFnPc7	emoce
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
výhradně	výhradně	k6eAd1	výhradně
úsměv	úsměv	k1gInSc1	úsměv
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
stahují	stahovat	k5eAaImIp3nP	stahovat
svaly	sval	k1gInPc1	sval
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
zvedají	zvedat	k5eAaImIp3nP	zvedat
tváře	tvář	k1gFnPc4	tvář
(	(	kIx(	(
<g/>
Duchenneův	Duchenneův	k2eAgInSc4d1	Duchenneův
úsměv	úsměv	k1gInSc4	úsměv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Falešný	falešný	k2eAgInSc1d1	falešný
úsměv	úsměv	k1gInSc1	úsměv
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
záměrně	záměrně	k6eAd1	záměrně
stažen	stáhnout	k5eAaPmNgInS	stáhnout
pouze	pouze	k6eAd1	pouze
hlavní	hlavní	k2eAgInSc1d1	hlavní
<g />
.	.	kIx.	.
</s>
<s>
zygomatický	zygomatický	k2eAgInSc1d1	zygomatický
sval	sval	k1gInSc1	sval
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gFnSc2	Am
úsměv	úsměv	k1gInSc1	úsměv
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
botoxový	botoxový	k2eAgInSc4d1	botoxový
úsměv	úsměv	k1gInSc4	úsměv
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc2d1	neexistující
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Pan	Pan	k1gMnSc1	Pan
American	American	k1gMnSc1	American
World	World	k1gMnSc1	World
Airways	Airwaysa	k1gFnPc2	Airwaysa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
letušky	letuška	k1gFnPc1	letuška
a	a	k8xC	a
stewardi	steward	k1gMnPc1	steward
vždy	vždy	k6eAd1	vždy
každého	každý	k3xTgMnSc4	každý
pasažéra	pasažér	k1gMnSc4	pasažér
obdařili	obdařit	k5eAaPmAgMnP	obdařit
stejným	stejný	k2eAgInSc7d1	stejný
povrchním	povrchní	k2eAgInSc7d1	povrchní
úsměvem	úsměv	k1gInSc7	úsměv
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
vžitý	vžitý	k2eAgInSc1d1	vžitý
název	název	k1gInSc1	název
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
botoxu	botox	k1gInSc3	botox
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
kosmetickém	kosmetický	k2eAgInSc6d1	kosmetický
průmyslu	průmysl	k1gInSc6	průmysl
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
aplikace	aplikace	k1gFnSc1	aplikace
botoxových	botoxový	k2eAgFnPc2d1	botoxová
injekcí	injekce	k1gFnPc2	injekce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vyhladit	vyhladit	k5eAaPmF	vyhladit
oční	oční	k2eAgFnPc4d1	oční
vrásky	vráska	k1gFnPc4	vráska
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
až	až	k9	až
v	v	k7c4	v
paralýzu	paralýza	k1gFnSc4	paralýza
malých	malý	k2eAgInPc2d1	malý
svalů	sval	k1gInPc2	sval
v	v	k7c6	v
očním	oční	k2eAgNnSc6d1	oční
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
znemožnit	znemožnit	k5eAaPmF	znemožnit
tak	tak	k9	tak
vznik	vznik	k1gInSc4	vznik
Duchenneova	Duchenneův	k2eAgInSc2d1	Duchenneův
úsměvu	úsměv	k1gInSc2	úsměv
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Smile	smil	k1gInSc5	smil
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Smích	smích	k1gInSc1	smích
</s>
</p>
<p>
<s>
Radost	radost	k1gFnSc1	radost
</s>
</p>
<p>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
</s>
</p>
<p>
<s>
Pláč	pláč	k1gInSc1	pláč
</s>
</p>
<p>
<s>
Mimika	mimika	k1gFnSc1	mimika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
úsměv	úsměv	k1gInSc4	úsměv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
smích	smích	k1gInSc1	smích
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
úsměv	úsměv	k1gInSc1	úsměv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
