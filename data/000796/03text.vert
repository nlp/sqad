<s>
Cordelia	Cordelia	k1gFnSc1	Cordelia
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Obíhá	obíhat	k5eAaImIp3nS	obíhat
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
49750	[number]	k4	49750
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgMnSc1d1	veliký
13	[number]	k4	13
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
planeta	planeta	k1gFnSc1	planeta
navštívena	navštívit	k5eAaPmNgFnS	navštívit
americkou	americký	k2eAgFnSc7d1	americká
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyager	k1gMnSc1	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
~	~	kIx~	~
<g/>
4,4	[number]	k4	4,4
<g/>
×	×	k?	×
<g/>
1016	[number]	k4	1016
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
mu	on	k3xPp3gMnSc3	on
trvá	trvat	k5eAaImIp3nS	trvat
0,335033	[number]	k4	0,335033
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Cordelia	Cordelia	k1gFnSc1	Cordelia
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Ophelia	Ophelius	k1gMnSc2	Ophelius
tzv.	tzv.	kA	tzv.
pastýřským	pastýřský	k2eAgInSc7d1	pastýřský
měsícem	měsíc	k1gInSc7	měsíc
k	k	k7c3	k
Uranově	Uranův	k2eAgFnSc3d1	Uranova
prstenci	prstenec	k1gInSc3	prstenec
Epsilon	epsilon	k1gNnSc1	epsilon
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
částice	částice	k1gFnSc1	částice
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
tak	tak	k9	tak
prstenec	prstenec	k1gInSc1	prstenec
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
