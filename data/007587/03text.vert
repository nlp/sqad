<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
ve	v	k7c6	v
spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
i	i	k8xC	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
je	být	k5eAaImIp3nS	být
pořádána	pořádat	k5eAaImNgFnS	pořádat
společností	společnost	k1gFnSc7	společnost
Ernst	Ernst	k1gMnSc1	Ernst
&	&	k?	&
Young	Young	k1gInSc1	Young
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
jsou	být	k5eAaImIp3nP	být
vyhlašovány	vyhlašován	k2eAgFnPc1d1	vyhlašována
různé	různý	k2eAgFnPc1d1	různá
kategorie	kategorie	k1gFnPc1	kategorie
včetně	včetně	k7c2	včetně
regionálních	regionální	k2eAgInPc2d1	regionální
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
–	–	k?	–
Mohed	Mohed	k1gInSc1	Mohed
Altrad	Altrad	k1gInSc1	Altrad
<g/>
,	,	kIx,	,
Groupe	Group	k1gInSc5	Group
Altrad	Altrad	k1gInSc1	Altrad
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
GZ	GZ	kA	GZ
Media	medium	k1gNnSc2	medium
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
–	–	k?	–
Viktor	Viktor	k1gMnSc1	Viktor
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
BioVendor	BioVendor	k1gMnSc1	BioVendor
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Wallner	Wallner	k1gMnSc1	Wallner
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Huber	Huber	k1gMnSc1	Huber
<g/>
,	,	kIx,	,
Mixit	Mixit	k2eAgMnSc1d1	Mixit
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
–	–	k?	–
Markéta	Markéta	k1gFnSc1	Markéta
Královcová	Královcový	k2eAgFnSc1d1	Královcová
<g/>
,	,	kIx,	,
Klíček	klíček	k1gInSc1	klíček
Regionální	regionální	k2eAgInSc1d1	regionální
kola	kolo	k1gNnSc2	kolo
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Jiřina	Jiřina	k1gFnSc1	Jiřina
Nepalová	Nepalová	k1gFnSc1	Nepalová
<g/>
,	,	kIx,	,
RENOMIA	RENOMIA	kA	RENOMIA
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
GZ	GZ	kA	GZ
Media	medium	k1gNnSc2	medium
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Viktor	Viktor	k1gMnSc1	Viktor
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
BioVendor	BioVendor	k1gMnSc1	BioVendor
-	-	kIx~	-
Laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
medicína	medicína	k1gFnSc1	medicína
a.s.	a.s.	k?	a.s.
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Penn	Penn	k1gMnSc1	Penn
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnPc1d1	Moravská
kovárny	kovárna	k1gFnPc1	kovárna
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
-	-	kIx~	-
Luboš	Luboš	k1gMnSc1	Luboš
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Mega	mega	k1gNnSc1	mega
a.s.	a.s.	k?	a.s.
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Gregořica	Gregořica	k1gMnSc1	Gregořica
<g/>
,	,	kIx,	,
Lanex	Lanex	k1gInSc1	Lanex
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Gross	Gross	k1gMnSc1	Gross
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Gross	Gross	k1gMnSc1	Gross
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Zlínský	zlínský	k2eAgInSc4d1	zlínský
kraj	kraj	k1gInSc4	kraj
–	–	k?	–
Michele	Michel	k1gInSc2	Michel
Taiariol	Taiariola	k1gFnPc2	Taiariola
<g/>
,	,	kIx,	,
TAJMAC-ZPS	TAJMAC-ZPS	k1gFnPc2	TAJMAC-ZPS
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g />
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
–	–	k?	–
Uday	Udaa	k1gFnSc2	Udaa
Kotak	Kotak	k1gMnSc1	Kotak
<g/>
,	,	kIx,	,
Kotak	Kotak	k1gMnSc1	Kotak
Mahindra	Mahindra	k1gFnSc1	Mahindra
Bank	bank	k1gInSc1	bank
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
–	–	k?	–
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Bříza	Bříza	k1gMnSc1	Bříza
<g/>
,	,	kIx,	,
Koh-i-noor	Kohoor	k1gMnSc1	Koh-i-noor
holding	holding	k1gInSc1	holding
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Velebný	velebný	k2eAgMnSc1d1	velebný
<g/>
,	,	kIx,	,
Contipro	Contipro	k1gNnSc1	Contipro
holding	holding	k1gInSc1	holding
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Mahdalíček	Mahdalíček	k1gMnSc1	Mahdalíček
<g/>
,	,	kIx,	,
Simplity	Simplit	k1gInPc1	Simplit
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2014	[number]	k4	2014
–	–	k?	–
Tereza	Tereza	k1gFnSc1	Tereza
Jurečková	Jurečková	k1gFnSc1	Jurečková
<g/>
,	,	kIx,	,
Pragulic	Pragulice	k1gFnPc2	Pragulice
Regionální	regionální	k2eAgFnSc1d1	regionální
kola	kola	k1gFnSc1	kola
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
Media	medium	k1gNnSc2	medium
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Martin	Martin	k1gInSc1	Martin
Vohánka	Vohánka	k1gFnSc1	Vohánka
<g/>
,	,	kIx,	,
W.A.G.	W.A.G.	k1gFnSc1	W.A.G.
payment	payment	k1gInSc1	payment
solutions	solutions	k6eAd1	solutions
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Bříza	Bříza	k1gMnSc1	Bříza
<g/>
,	,	kIx,	,
Koh-i-noor	Kohoor	k1gInSc1	Koh-i-noor
holding	holding	k1gInSc1	holding
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Životský	Životský	k2eAgMnSc1d1	Životský
<g/>
,	,	kIx,	,
OKAY	OKAY	kA	OKAY
Holding	holding	k1gInSc1	holding
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
rodinný	rodinný	k2eAgInSc1d1	rodinný
pivovar	pivovar	k1gInSc1	pivovar
BERNARD	Bernard	k1gMnSc1	Bernard
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Gevorg	Gevorg	k1gMnSc1	Gevorg
Avetisjan	Avetisjan	k1gMnSc1	Avetisjan
<g/>
,	,	kIx,	,
Marlenka	Marlenka	k1gFnSc1	Marlenka
International	International	k1gFnPc2	International
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
David	David	k1gMnSc1	David
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
,	,	kIx,	,
PAPCEL	PAPCEL	kA	PAPCEL
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Velebný	velebný	k2eAgMnSc1d1	velebný
<g/>
,	,	kIx,	,
Contipro	Contipro	k1gNnSc1	Contipro
holding	holding	k1gInSc1	holding
Zlínský	zlínský	k2eAgInSc4d1	zlínský
kraj	kraj	k1gInSc4	kraj
–	–	k?	–
Pavel	Pavla	k1gFnPc2	Pavla
Stodůlka	stodůlka	k1gFnSc1	stodůlka
–	–	k?	–
Gemini	Gemin	k1gMnPc1	Gemin
oční	oční	k2eAgFnSc2d1	oční
klinika	klinik	k1gMnSc4	klinik
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
–	–	k?	–
Hamdi	Hamd	k1gMnPc1	Hamd
Ulukaya	Ulukaya	k1gMnSc1	Ulukaya
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Chobani	Chobaň	k1gFnSc3	Chobaň
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
,	,	kIx,	,
Juta	juta	k1gFnSc1	juta
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
–	–	k?	–
František	František	k1gMnSc1	František
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Kudelovi	Kudelův	k2eAgMnPc1d1	Kudelův
<g/>
,	,	kIx,	,
Chropyňské	chropyňský	k2eAgFnSc2d1	Chropyňská
strojírny	strojírna	k1gFnSc2	strojírna
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
–	–	k?	–
Veronika	Veronika	k1gFnSc1	Veronika
Mouchová	Mouchová	k1gFnSc1	Mouchová
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
YesFresh	YesFresh	k1gMnSc1	YesFresh
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Školník	školník	k1gMnSc1	školník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
Broumovska	Broumovsko	k1gNnSc2	Broumovsko
Regionální	regionální	k2eAgFnSc2d1	regionální
kola	kolo	k1gNnSc2	kolo
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Valová	Valová	k1gFnSc1	Valová
<g/>
,	,	kIx,	,
SIKO	sika	k1gMnSc5	sika
koupelny	koupelna	k1gFnSc2	koupelna
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Libor	Libor	k1gMnSc1	Libor
Bittner	Bittner	k1gMnSc1	Bittner
<g/>
,	,	kIx,	,
Bioveta	Biovet	k1gMnSc2	Biovet
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
,	,	kIx,	,
Juta	juta	k1gFnSc1	juta
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj	kraj	k1gInSc4	kraj
–	–	k?	–
Gevorg	Gevorg	k1gInSc1	Gevorg
Avetisjan	Avetisjan	k1gInSc1	Avetisjan
<g/>
,	,	kIx,	,
Marlenka	Marlenka	k1gFnSc1	Marlenka
International	International	k1gFnPc2	International
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Cyril	Cyril	k1gMnSc1	Cyril
<g />
.	.	kIx.	.
</s>
<s>
Svozil	Svozil	k1gMnSc1	Svozil
<g/>
,	,	kIx,	,
FENIX	FENIX	kA	FENIX
Trading	Trading	k1gInSc4	Trading
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
František	František	k1gMnSc1	František
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Kudelovi	Kudelův	k2eAgMnPc1d1	Kudelův
<g/>
,	,	kIx,	,
Chropyňské	chropyňský	k2eAgFnSc2d1	Chropyňská
strojírny	strojírna	k1gFnSc2	strojírna
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
–	–	k?	–
James	James	k1gMnSc1	James
Mwangi	Mwangi	k1gNnSc1	Mwangi
<g/>
,	,	kIx,	,
Kenya	Kenya	k1gFnSc1	Kenya
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Equity	Equita	k1gMnPc7	Equita
Bank	bank	k1gInSc1	bank
Chobani	Chobaň	k1gFnSc3	Chobaň
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
–	–	k?	–
František	František	k1gMnSc1	František
Piškanin	Piškanin	k2eAgMnSc1d1	Piškanin
<g/>
,	,	kIx,	,
HOPI	HOPI	kA	HOPI
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Volenec	volenec	k1gMnSc1	volenec
<g/>
,	,	kIx,	,
ELLA	ELLA	kA	ELLA
–	–	k?	–
CS	CS	kA	CS
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Schönfeld	Schönfeld	k1gMnSc1	Schönfeld
<g/>
,	,	kIx,	,
International	International	k1gMnSc1	International
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Music	Music	k1gMnSc1	Music
and	and	k?	and
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc4	Arts
Regionální	regionální	k2eAgNnPc4d1	regionální
kola	kolo	k1gNnPc4	kolo
hlavní	hlavní	k2eAgNnPc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
GZ	GZ	kA	GZ
Digital	Digital	kA	Digital
Media	medium	k1gNnPc4	medium
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Frentech	Frent	k1gInPc6	Frent
Aerospace	Aerospace	k1gFnPc1	Aerospace
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g />
.	.	kIx.	.
</s>
<s>
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
VEBA	Veba	k1gFnSc1	Veba
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInPc4d1	textilní
závody	závod	k1gInPc4	závod
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj	kraj	k1gInSc4	kraj
–	–	k?	–
Mieczyslaw	Mieczyslaw	k1gMnSc2	Mieczyslaw
Molenda	Molend	k1gMnSc2	Molend
<g/>
,	,	kIx,	,
GASCONTROL	GASCONTROL	kA	GASCONTROL
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Žáček	Žáček	k1gMnSc1	Žáček
<g/>
,	,	kIx,	,
Česko-slezská	českolezský	k2eAgFnSc1d1	česko-slezský
výrobní	výrobní	k2eAgInSc1d1	výrobní
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
Replast	Replast	k1gInSc1	Replast
Holding	holding	k1gInSc1	holding
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
–	–	k?	–
Olivia	Olivia	k1gFnSc1	Olivia
Lum	Lum	k1gFnSc1	Lum
<g/>
,	,	kIx,	,
Hyflux	Hyflux	k1gInSc1	Hyflux
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
<g />
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
–	–	k?	–
Janis	Janis	k1gInSc1	Janis
Samaras	Samaras	k1gInSc1	Samaras
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Tescan	Tescan	k1gMnSc1	Tescan
Orsay	Orsaa	k1gFnSc2	Orsaa
Holding	holding	k1gInSc1	holding
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
–	–	k?	–
Marek	Marek	k1gMnSc1	Marek
Potysz	Potysz	k1gMnSc1	Potysz
<g/>
,	,	kIx,	,
Moje	můj	k3xOp1gFnSc1	můj
Ambulance	ambulance	k1gFnPc4	ambulance
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
–	–	k?	–
Dolores	Doloresa	k1gFnPc2	Doloresa
Czudková	Czudkový	k2eAgFnSc1d1	Czudková
<g/>
,	,	kIx,	,
Ergon	Ergon	k1gInSc1	Ergon
–	–	k?	–
chráněná	chráněný	k2eAgFnSc1d1	chráněná
dílna	dílna	k1gFnSc1	dílna
Regionální	regionální	k2eAgFnSc1d1	regionální
kola	kola	k1gFnSc1	kola
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Bouška	Bouška	k1gMnSc1	Bouška
<g/>
,	,	kIx,	,
VAFO	VAFO	kA	VAFO
Praha	Praha	k1gFnSc1	Praha
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Tescan	Tescan	k1gMnSc1	Tescan
Orsay	Orsaa	k1gFnSc2	Orsaa
Holding	holding	k1gInSc1	holding
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
DETOA	DETOA	kA	DETOA
Albrechtice	Albrechtice	k1gFnPc4	Albrechtice
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jannis	Jannis	k1gInSc1	Jannis
Samaras	Samaras	k1gInSc1	Samaras
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Mencl	Mencl	k1gMnSc1	Mencl
<g/>
,	,	kIx,	,
SEV	SEV	kA	SEV
Litovel	Litovel	k1gFnSc4	Litovel
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Pavlas	Pavlas	k1gMnSc1	Pavlas
<g/>
,	,	kIx,	,
KOVOLIS	KOVOLIS	kA	KOVOLIS
Hedvikov	Hedvikov	k1gInSc4	Hedvikov
Ústecký	ústecký	k2eAgInSc1d1	ústecký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Hausenblas	Hausenblas	k1gMnSc1	Hausenblas
<g/>
,	,	kIx,	,
ADLER	Adler	k1gMnSc1	Adler
Czech	Czech	k1gMnSc1	Czech
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
–	–	k?	–
Michael	Michael	k1gMnSc1	Michael
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
,	,	kIx,	,
ICAP	ICAP	kA	ICAP
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
–	–	k?	–
Mariusz	Mariusz	k1gMnSc1	Mariusz
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Valdemar	Valdemara	k1gFnPc2	Valdemara
Walachové	Walachová	k1gFnSc2	Walachová
<g/>
,	,	kIx,	,
WALMARK	WALMARK	kA	WALMARK
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Muchna	Muchn	k1gInSc2	Muchn
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Y	Y	kA	Y
Soft	Soft	k?	Soft
Corporation	Corporation	k1gInSc1	Corporation
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Křížek	Křížek	k1gMnSc1	Křížek
<g/>
,	,	kIx,	,
Ochrana	ochrana	k1gFnSc1	ochrana
fauny	fauna	k1gFnSc2	fauna
ČR	ČR	kA	ČR
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
–	–	k?	–
Cho	cho	k0	cho
Tak	tak	k9	tak
Wong	Wonga	k1gFnPc2	Wonga
<g/>
,	,	kIx,	,
Fuyao	Fuyao	k6eAd1	Fuyao
Glass	Glass	k1gInSc1	Glass
Industry	Industra	k1gFnSc2	Industra
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
AVAST	AVAST	kA	AVAST
Software	software	k1gInSc1	software
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
AVAST	AVAST	kA	AVAST
Software	software	k1gInSc1	software
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Biopekárna	Biopekárna	k1gFnSc1	Biopekárna
Zemanka	zemanka	k1gFnSc1	zemanka
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
–	–	k?	–
Emilie	Emilie	k1gFnSc1	Emilie
Smrčková	Smrčková	k1gFnSc1	Smrčková
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Křídly	křídlo	k1gNnPc7	křídlo
Středočeský	středočeský	k2eAgInSc4d1	středočeský
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
AVAST	AVAST	kA	AVAST
Software	software	k1gInSc4	software
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
TESCAN	TESCAN	kA	TESCAN
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
<g />
.	.	kIx.	.
</s>
<s>
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jozef	Jozef	k1gMnSc1	Jozef
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
,	,	kIx,	,
HOPAX	HOPAX	kA	HOPAX
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Rozhoň	Rozhoň	k1gMnSc1	Rozhoň
<g/>
,	,	kIx,	,
VIVANTIS	VIVANTIS	kA	VIVANTIS
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
FITE	FITE	kA	FITE
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
–	–	k?	–
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Clozel	Clozel	k1gFnSc1	Clozel
<g/>
,	,	kIx,	,
Actelion	Actelion	k1gInSc1	Actelion
Pharmaceuticals	Pharmaceuticalsa	k1gFnPc2	Pharmaceuticalsa
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Unicorn	Unicorn	k1gMnSc1	Unicorn
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
–	–	k?	–
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
SEKO	seko	k1gNnSc1	seko
EDM	EDM	kA	EDM
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
–	–	k?	–
Linda	Linda	k1gFnSc1	Linda
Vavříková	Vavříková	k1gFnSc1	Vavříková
<g/>
,	,	kIx,	,
Firma	firma	k1gFnSc1	firma
na	na	k7c4	na
zážitky	zážitek	k1gInPc4	zážitek
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Zejda	Zejda	k1gMnSc1	Zejda
<g/>
,	,	kIx,	,
KAZETO	kazeta	k1gFnSc5	kazeta
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
–	–	k?	–
Drahoslava	Drahoslava	k1gFnSc1	Drahoslava
Kabátová	Kabátová	k1gFnSc1	Kabátová
<g/>
,	,	kIx,	,
Letohrádek	letohrádek	k1gInSc1	letohrádek
Vendula	Vendula	k1gFnSc1	Vendula
Střední	střední	k2eAgFnPc1d1	střední
Čechy	Čechy	k1gFnPc1	Čechy
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Unicorn	Unicorn	k1gMnSc1	Unicorn
<g />
.	.	kIx.	.
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Žďárský	Žďárský	k1gMnSc1	Žďárský
<g/>
,	,	kIx,	,
Farmet	Farmet	k1gMnSc1	Farmet
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Oulický	Oulický	k2eAgMnSc1d1	Oulický
<g/>
,	,	kIx,	,
ASAVET	ASAVET	kA	ASAVET
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Sázavský	sázavský	k2eAgMnSc1d1	sázavský
<g/>
,	,	kIx,	,
TENZA	TENZA	kA	TENZA
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Libor	Libor	k1gMnSc1	Libor
Suchánek	Suchánek	k1gMnSc1	Suchánek
<g/>
,	,	kIx,	,
SULKO	sulka	k1gFnSc5	sulka
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
,	,	kIx,	,
H.	H.	kA	H.
R.	R.	kA	R.
G.	G.	kA	G.
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
–	–	k?	–
Roman	Roman	k1gMnSc1	Roman
Stryk	stryk	k1gMnSc1	stryk
<g/>
,	,	kIx,	,
ROSS	ROSS	kA	ROSS
<g />
.	.	kIx.	.
</s>
<s>
Holding	holding	k1gInSc1	holding
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Canibal	Canibal	k1gInSc1	Canibal
<g/>
,	,	kIx,	,
Karvinská	karvinský	k2eAgFnSc1d1	karvinská
hornická	hornický	k2eAgFnSc1d1	hornická
nemocnice	nemocnice	k1gFnSc2	nemocnice
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
–	–	k?	–
Guy	Guy	k1gMnSc1	Guy
Laliberté	Laliberta	k1gMnPc1	Laliberta
<g/>
,	,	kIx,	,
Cirque	Cirqu	k1gFnPc1	Cirqu
du	du	k?	du
Soleil	Soleil	k1gInSc1	Soleil
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Březina	Březina	k1gMnSc1	Březina
<g/>
,	,	kIx,	,
BEST	BEST	kA	BEST
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Suska	Suska	k1gMnSc1	Suska
<g/>
,	,	kIx,	,
HOKAMI	HOKAMI	kA	HOKAMI
CZ	CZ	kA	CZ
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Rychtar	Rychtar	k1gMnSc1	Rychtar
<g/>
,	,	kIx,	,
ROLOFOL	ROLOFOL	kA	ROLOFOL
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
poroty	porota	k1gFnSc2	porota
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
BIOMAC	BIOMAC	kA	BIOMAC
Ing.	ing.	kA	ing.
Černý	Černý	k1gMnSc1	Černý
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
–	–	k?	–
Jozef	Jozef	k1gMnSc1	Jozef
Baláž	Baláž	k1gMnSc1	Baláž
<g/>
,	,	kIx,	,
LIGA	liga	k1gFnSc1	liga
Bruntál	Bruntál	k1gInSc4	Bruntál
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Mičánek	Mičánek	k1gMnSc1	Mičánek
<g/>
,	,	kIx,	,
LESS	LESS	kA	LESS
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ondřej	Ondřej	k1gMnSc1	Ondřej
Fryc	Fryc	k1gFnSc4	Fryc
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
Mall	Mall	k1gInSc1	Mall
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Březina	Březina	k1gMnSc1	Březina
<g/>
,	,	kIx,	,
BEST	BEST	kA	BEST
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Fusek	Fusek	k1gMnSc1	Fusek
<g/>
,	,	kIx,	,
Pivovar	pivovar	k1gInSc1	pivovar
Černá	černat	k5eAaImIp3nS	černat
Hora	Hora	k1gMnSc1	Hora
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
BIOMAC	BIOMAC	kA	BIOMAC
Ing.	ing.	kA	ing.
Černý	černý	k2eAgInSc1d1	černý
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Samek	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
Pacific	Pacific	k1gMnSc1	Pacific
Direct	Direct	k2eAgInSc4d1	Direct
Kraj	kraj	k1gInSc4	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Jeleček	jeleček	k1gMnSc1	jeleček
<g/>
,	,	kIx,	,
TEDOM	TEDOM	kA	TEDOM
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Franek	Franek	k1gMnSc1	Franek
<g/>
,	,	kIx,	,
REFRASIL	REFRASIL	kA	REFRASIL
<g />
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
–	–	k?	–
Bill	Bill	k1gMnSc1	Bill
Lynch	Lynch	k1gMnSc1	Lynch
<g/>
,	,	kIx,	,
Imperial	Imperial	k1gMnSc1	Imperial
Holdings	Holdings	k1gInSc1	Holdings
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Juříček	Juříček	k1gMnSc1	Juříček
<g/>
,	,	kIx,	,
BRANO	BRANO	kA	BRANO
GROUP	GROUP	kA	GROUP
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Červinka	Červinka	k1gMnSc1	Červinka
<g/>
,	,	kIx,	,
ADASTRA	ADASTRA	kA	ADASTRA
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Muchna	Muchn	k1gMnSc2	Muchn
<g/>
,	,	kIx,	,
Y	Y	kA	Y
Soft	Soft	k?	Soft
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
poroty	porota	k1gFnPc1	porota
–	–	k?	–
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Kasal	Kasal	k1gMnSc1	Kasal
<g/>
,	,	kIx,	,
Kasalova	Kasalův	k2eAgFnSc1d1	Kasalova
pila	pila	k1gFnSc1	pila
Sociálně	sociálně	k6eAd1	sociálně
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
–	–	k?	–
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
AGENTURA	agentura	k1gFnSc1	agentura
PRO	pro	k7c4	pro
VÁS	vy	k3xPp2nPc4	vy
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Roman	Roman	k1gMnSc1	Roman
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc4	Publishing
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Kout	kout	k5eAaImF	kout
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Maštálka	maštálka	k1gFnSc1	maštálka
<g/>
,	,	kIx,	,
PEKASS	PEKASS	kA	PEKASS
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Kaláb	Kaláb	k1gMnSc1	Kaláb
<g/>
,	,	kIx,	,
KALÁB-stavební	KALÁBtavební	k2eAgFnSc1d1	KALÁB-stavební
firma	firma	k1gFnSc1	firma
<g />
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Študent	Študent	k?	Študent
<g/>
,	,	kIx,	,
SIWATEC	SIWATEC	kA	SIWATEC
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Brýdl	Brýdl	k1gMnSc1	Brýdl
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Brýdl	Brýdl	k1gMnSc1	Brýdl
<g/>
,	,	kIx,	,
STORY	story	k1gFnSc1	story
DESIGN	design	k1gInSc1	design
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
SAPELI	SAPELI	kA	SAPELI
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Juříček	Juříček	k1gMnSc1	Juříček
<g/>
,	,	kIx,	,
BRANO	BRANO	kA	BRANO
GROUP	GROUP	kA	GROUP
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
–	–	k?	–
Wayne	Wayn	k1gInSc5	Wayn
Huizenga	Huizenga	k1gFnSc1	Huizenga
<g/>
,	,	kIx,	,
Huizenga	Huizenga	k1gFnSc1	Huizenga
Holdings	Holdings	k1gInSc1	Holdings
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
GRAPO	GRAPO	k?	GRAPO
Začínající	začínající	k2eAgMnSc1d1	začínající
podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Řežáb	řežáb	k1gInSc1	řežáb
<g/>
,	,	kIx,	,
REDBOSS	REDBOSS	kA	REDBOSS
Speciální	speciální	k2eAgNnSc1d1	speciální
ocenění	ocenění	k1gNnSc1	ocenění
poroty	porota	k1gFnSc2	porota
–	–	k?	–
Jana	Jana	k1gFnSc1	Jana
Procházková	procházkový	k2eAgFnSc1d1	Procházková
<g/>
,	,	kIx,	,
GLOBAL	globat	k5eAaImAgInS	globat
EXPRESS	express	k1gInSc1	express
GROUP	GROUP	kA	GROUP
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
cz	cz	k?	cz
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Zábranský	Zábranský	k1gMnSc1	Zábranský
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
chodská	chodský	k2eAgFnSc1d1	Chodská
stavební	stavební	k2eAgFnSc1d1	stavební
společnost	společnost	k1gFnSc1	společnost
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
BOCHEMIE	BOCHEMIE	kA	BOCHEMIE
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
–	–	k?	–
Tony	Tony	k1gMnSc1	Tony
Tan	Tan	k1gMnSc1	Tan
Caktiong	Caktiong	k1gMnSc1	Caktiong
<g/>
,	,	kIx,	,
Jolibee	Jolibee	k1gFnSc1	Jolibee
Foods	Foods	k1gInSc1	Foods
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Řihák	Řihák	k1gMnSc1	Řihák
<g/>
,	,	kIx,	,
ANECT	ANECT	kA	ANECT
Technologický	technologický	k2eAgMnSc1d1	technologický
podnikatel	podnikatel	k1gMnSc1	podnikatel
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
ELKO	ELKO	kA	ELKO
EP	EP	kA	EP
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
–	–	k?	–
Narayana	Narayan	k1gMnSc2	Narayan
Murthy	Murtha	k1gMnSc2	Murtha
<g/>
,	,	kIx,	,
Infosys	Infosys	k1gInSc1	Infosys
Technologies	Technologies	k1gInSc1	Technologies
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
–	–	k?	–
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Frolík	Frolík	k1gMnSc1	Frolík
<g/>
,	,	kIx,	,
L	L	kA	L
I	i	k8xC	i
N	N	kA	N
E	E	kA	E
T	T	kA	T
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
–	–	k?	–
Stefan	Stefan	k1gMnSc1	Stefan
Vilsmeier	Vilsmeier	k1gMnSc1	Vilsmeier
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
BrainLAB	BrainLAB	k1gFnPc4	BrainLAB
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
–	–	k?	–
Kvido	Kvido	k1gMnSc1	Kvido
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Isolit-Bravo	Isolit-Brava	k1gFnSc5	Isolit-Brava
Světový	světový	k2eAgMnSc1d1	světový
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
–	–	k?	–
Paolo	Paolo	k1gNnSc1	Paolo
della	della	k1gFnSc1	della
Porta	porta	k1gFnSc1	porta
<g/>
,	,	kIx,	,
Saes	Saes	k1gInSc1	Saes
Getters	Getters	k1gInSc1	Getters
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jandejsek	Jandejsek	k1gMnSc1	Jandejsek
<g/>
,	,	kIx,	,
RABBIT	RABBIT	kA	RABBIT
Trhový	trhový	k2eAgInSc4d1	trhový
Štěpánov	Štěpánov	k1gInSc4	Štěpánov
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
–	–	k?	–
Kateřina	Kateřina	k1gFnSc1	Kateřina
Forstingerová	Forstingerová	k1gFnSc1	Forstingerová
<g/>
,	,	kIx,	,
Moravia	Moravia	k1gFnSc1	Moravia
IT	IT	kA	IT
</s>
