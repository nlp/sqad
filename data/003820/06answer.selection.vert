<s>
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1822	[number]	k4	1822
Dole	dole	k6eAd1	dole
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1895	[number]	k4	1895
Villeneuve-l	Villeneuveum	k1gNnPc2	Villeneuve-lum
<g/>
'	'	kIx"	'
<g/>
Etang	Etang	k1gInSc1	Etang
<g/>
,	,	kIx,	,
Marnes-la-Coquette	Marnesa-Coquett	k1gMnSc5	Marnes-la-Coquett
<g/>
,	,	kIx,	,
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
vědců	vědec	k1gMnPc2	vědec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
