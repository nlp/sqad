<s>
Kurt	Kurt	k1gMnSc1	Kurt
Moll	moll	k1gNnSc2	moll
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Buir	Buir	k1gInSc1	Buir
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Buir	Buir	k1gInSc4	Buir
poblíž	poblíž	k7c2	poblíž
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
dirigent	dirigent	k1gMnSc1	dirigent
mu	on	k3xPp3gMnSc3	on
doporučil	doporučit	k5eAaPmAgMnS	doporučit
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
na	na	k7c4	na
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Hlasový	hlasový	k2eAgInSc1d1	hlasový
obor	obor	k1gInSc1	obor
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
na	na	k7c4	na
Köln	Köln	k1gInSc4	Köln
Hochschule	Hochschule	k1gFnSc1	Hochschule
für	für	k?	für
Musik	musika	k1gFnPc2	musika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958-1961	[number]	k4	1958-1961
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Kolínské	kolínský	k2eAgFnSc6d1	Kolínská
opeře	opera	k1gFnSc6	opera
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
a	a	k8xC	a
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Wuppertalu	Wuppertal	k1gMnSc6	Wuppertal
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
souboru	soubor	k1gInSc2	soubor
Státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Prorazil	prorazit	k5eAaPmAgMnS	prorazit
také	také	k9	také
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Francisc	k2eAgFnSc4d1	Francisca
postavu	postava	k1gFnSc4	postava
Gurnemanze	Gurnemanze	k1gFnSc2	Gurnemanze
ve	v	k7c6	v
Wagnerově	Wagnerův	k2eAgMnSc6d1	Wagnerův
Parsifalovi	Parsifal	k1gMnSc6	Parsifal
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
v	v	k7c6	v
Metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
opeře	opera	k1gFnSc6	opera
Landgrafa	Landgraf	k1gMnSc2	Landgraf
<g/>
,	,	kIx,	,
Rocca	Roccus	k1gMnSc2	Roccus
a	a	k8xC	a
Sparafucila	Sparafucil	k1gMnSc2	Sparafucil
<g/>
.	.	kIx.	.
</s>
<s>
Nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
mnoho	mnoho	k4c4	mnoho
nahrávek	nahrávka	k1gFnPc2	nahrávka
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
dirigenty	dirigent	k1gMnPc7	dirigent
a	a	k8xC	a
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mollův	Mollův	k2eAgInSc1d1	Mollův
temný	temný	k2eAgInSc1d1	temný
a	a	k8xC	a
hluboký	hluboký	k2eAgInSc1d1	hluboký
bas	bas	k1gInSc1	bas
se	se	k3xPyFc4	se
výborně	výborně	k6eAd1	výborně
hodil	hodit	k5eAaImAgInS	hodit
pro	pro	k7c4	pro
vážné	vážný	k2eAgFnPc4d1	vážná
role	role	k1gFnPc4	role
ve	v	k7c6	v
Wagnerovi	Wagner	k1gMnSc6	Wagner
či	či	k8xC	či
Verdim	Verdim	k1gInSc4	Verdim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
výkony	výkon	k1gInPc4	výkon
patří	patřit	k5eAaImIp3nS	patřit
komické	komický	k2eAgNnSc1d1	komické
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
velmi	velmi	k6eAd1	velmi
technicky	technicky	k6eAd1	technicky
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
)	)	kIx)	)
role	role	k1gFnSc1	role
jako	jako	k8xS	jako
Osmin	osmina	k1gFnPc2	osmina
či	či	k8xC	či
Baron	baron	k1gMnSc1	baron
Ochs	Ochsa	k1gFnPc2	Ochsa
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Moll	moll	k1gNnSc2	moll
ukončil	ukončit	k5eAaPmAgMnS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
držitelem	držitel	k1gMnSc7	držitel
Grammy	Gramma	k1gFnSc2	Gramma
Award	Awarda	k1gFnPc2	Awarda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
