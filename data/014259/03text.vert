<s>
Colours	Colours	k6eAd1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2018	#num#	k4
</s>
<s>
Colours	Colours	k6eAd1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2018	#num#	k4
<g/>
Téma	téma	k1gNnSc4
</s>
<s>
hudba	hudba	k1gFnSc1
Místo	místo	k7c2
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Vítkovice	Vítkovice	k1gInPc4
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Pořadatelé	pořadatel	k1gMnPc1
</s>
<s>
Colour	Colour	k1gMnSc1
Production	Production	k1gInSc1
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
Datum	datum	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
až	až	k9
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
Účast	účast	k1gFnSc1
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	a	k9
Předchozí	předchozí	k2eAgFnSc1d1
</s>
<s>
Colours	Colours	k6eAd1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2017	#num#	k4
Následující	následující	k2eAgInPc1d1
</s>
<s>
Colours	Colours	k6eAd1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2019	#num#	k4
Web	web	k1gInSc1
</s>
<s>
https://www.colours.cz/	https://www.colours.cz/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Colours	Colours	k6eAd1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2018	#num#	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
Barvy	barva	k1gFnPc1
Ostravy	Ostrava	k1gFnSc2
2018	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
17	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
multižánrového	multižánrový	k2eAgInSc2d1
hudebního	hudební	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Colours	Colours	k1gInSc1
of	of	k7
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Konal	konat	k5eAaImAgInS
se	se	k3xPyFc4
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
v	v	k7c6
Dolní	dolní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Vítkovice	Vítkovice	k1gInPc1
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Festival	festival	k1gInSc4
opět	opět	k6eAd1
pořádala	pořádat	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Colour	Colour	k1gMnSc1
Production	Production	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
na	na	k7c6
21	#num#	k4
scénách	scéna	k1gFnPc6
a	a	k8xC
měl	mít	k5eAaImAgMnS
přes	přes	k7c4
450	#num#	k4
programových	programový	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
61	#num#	k4
českých	český	k2eAgMnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
a	a	k8xC
80	#num#	k4
zahraničních	zahraniční	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Účinkující	účinkující	k1gFnSc1
</s>
<s>
Neúplný	úplný	k2eNgInSc4d1
seznam	seznam	k1gInSc4
účastníků	účastník	k1gMnPc2
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
N.	N.	kA
<g/>
E.	E.	kA
<g/>
R.	R.	kA
<g/>
D.	D.	kA
</s>
<s>
Kygo	Kygo	k6eAd1
</s>
<s>
George	Georg	k1gMnSc4
Ezra	Ezrus	k1gMnSc4
</s>
<s>
Paul	Paul	k1gMnSc1
Kalkbrenner	Kalkbrenner	k1gMnSc1
</s>
<s>
GusGus	GusGus	k1gMnSc1
</s>
<s>
Jon	Jon	k?
Hopkins	Hopkins	k1gInSc1
</s>
<s>
Beth	Beth	k1gInSc1
Ditto	Ditto	k1gNnSc1
</s>
<s>
Algiers	Algiers	k6eAd1
</s>
<s>
Jacob	Jacoba	k1gFnPc2
Banks	Banksa	k1gFnPc2
</s>
<s>
Beránci	Beránek	k1gMnPc1
a	a	k8xC
vlci	vlk	k1gMnPc1
</s>
<s>
Diego	Diego	k6eAd1
</s>
<s>
Lenny	Lenny	k?
</s>
<s>
Mig	mig	k1gInSc1
21	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Colours	Colours	k1gInSc1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
2019	#num#	k4
|	|	kIx~
Areál	areál	k1gInSc1
Dolní	dolní	k2eAgInPc1d1
Vítkovice	Vítkovice	k1gInPc1
-	-	kIx~
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informuji	informovat	k5eAaBmIp1nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
IREPORT	IREPORT	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kam	kam	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
na	na	k7c4
festivaly	festival	k1gInPc4
(	(	kIx(
<g/>
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Na	na	k7c4
Colours	Colours	k1gInSc4
míří	mířit	k5eAaImIp3nS
The	The	k1gFnSc1
Cure	Cur	k1gFnSc2
a	a	k8xC
Florence	Florenc	k1gFnSc2
And	Anda	k1gFnPc2
The	The	k1gMnSc5
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
Benátská	benátský	k2eAgNnPc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
veze	vézt	k5eAaImIp3nS
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
iREPORT	iREPORT	k?
–	–	k?
music	music	k1gMnSc1
<g/>
&	&	k?
<g/>
style	styl	k1gInSc5
magazine	magazin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Colours	Colours	k1gInSc1
of	of	k?
Ostrava	Ostrava	k1gFnSc1
-	-	kIx~
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
