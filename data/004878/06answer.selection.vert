<s>
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
po	po	k7c6	po
místě	místo	k1gNnSc6	místo
objevu	objev	k1gInSc2	objev
–	–	k?	–
první	první	k4xOgMnSc1	první
popsaný	popsaný	k2eAgInSc1d1	popsaný
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
městě	město	k1gNnSc6	město
Lassa	Lassa	k1gFnSc1	Lassa
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Čadského	čadský	k2eAgNnSc2d1	Čadské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
zemřely	zemřít	k5eAaPmAgFnP	zemřít
dvě	dva	k4xCgFnPc1	dva
misionářky	misionářka	k1gFnPc1	misionářka
<g/>
.	.	kIx.	.
</s>
