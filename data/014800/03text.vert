<s>
Honky	Honky	k6eAd1
Château	château	k1gNnSc1
</s>
<s>
Honky	Honky	k6eAd1
ChâteauInterpretElton	ChâteauInterpretElton	k1gInSc1
JohnPruh	JohnPruha	k1gFnPc2
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
<g/>
Nahránoleden	Nahránoleden	k2eAgInSc4d1
1972	#num#	k4
<g/>
ŽánrrockDélka	ŽánrrockDélka	k1gFnSc1
<g/>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
JazykangličtinaVydavatelstvíUni	JazykangličtinaVydavatelstvíUn	k1gMnPc1
Records	Recordsa	k1gFnPc2
<g/>
,	,	kIx,
DJM	DJM	kA
RecordsProducentGus	RecordsProducentGus	k1gInSc1
DudgeonProfesionální	DudgeonProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
Christgau	Christgaus	k1gInSc2
(	(	kIx(
<g/>
A	a	k9
<g/>
−	−	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Madman	Madman	k1gMnSc1
Across	Acrossa	k1gFnPc2
the	the	k?
Water	Water	k1gMnSc1
<g/>
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Honky	Honky	k6eAd1
Château	château	k1gNnSc1
<g/>
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Shoot	Shoot	k1gInSc1
Me	Me	k1gMnSc1
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Only	Onla	k1gMnSc2
the	the	k?
Piano	piano	k1gNnSc1
Player	Player	k1gMnSc1
<g/>
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Honky	Honky	k6eAd1
Château	château	k1gNnSc1
je	být	k5eAaImIp3nS
páté	pátý	k4xOgNnSc4
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
anglického	anglický	k2eAgMnSc2d1
hudebníka	hudebník	k1gMnSc2
Eltona	Elton	k1gMnSc4
Johna	John	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydáno	vydán	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
květnu	květen	k1gInSc6
1972	#num#	k4
společnostmi	společnost	k1gFnPc7
Uni	Uni	k1gFnSc1
Records	Records	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
a	a	k8xC
DJM	DJM	kA
Records	Records	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
bylo	být	k5eAaImAgNnS
v	v	k7c6
lednu	leden	k1gInSc6
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
na	na	k7c6
zámku	zámek	k1gInSc6
Château	château	k1gNnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Hérouville	Hérouvill	k1gMnSc2
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Hérouville	Hérouvill	k1gInSc6
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
název	název	k1gInSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Producentem	producent	k1gMnSc7
alba	album	k1gNnSc2
byl	být	k5eAaImAgMnS
Gus	Gus	k1gMnSc1
Dudgeon	Dudgeon	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
s	s	k7c7
Johnem	John	k1gMnSc7
spolupracoval	spolupracovat	k5eAaImAgMnS
již	již	k6eAd1
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Rolling	Rolling	k1gInSc4
Stone	ston	k1gInSc5
desku	deska	k1gFnSc4
zařadil	zařadit	k5eAaPmAgInS
na	na	k7c4
359	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
žebříčku	žebříček	k1gInSc2
500	#num#	k4
nejlepších	dobrý	k2eAgFnPc2d3
alb	alba	k1gFnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Autory	autor	k1gMnPc7
všech	všecek	k3xTgFnPc2
skladeb	skladba	k1gFnPc2
jsou	být	k5eAaImIp3nP
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
a	a	k8xC
Bernie	Bernie	k1gFnSc1
Taupin	Taupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Honky	Honka	k1gFnPc4
Cat	Cat	k1gFnSc2
<g/>
“	“	k?
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
„	„	k?
<g/>
Mellow	Mellow	k1gFnSc1
<g/>
“	“	k?
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
</s>
<s>
„	„	k?
<g/>
I	i	k9
Think	Think	k1gMnSc1
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Going	Going	k1gMnSc1
to	ten	k3xDgNnSc4
Kill	Kill	k1gMnSc1
Myself	Myself	k1gMnSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
</s>
<s>
„	„	k?
<g/>
Susie	Susie	k1gFnSc1
(	(	kIx(
<g/>
Dramas	Dramas	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
„	„	k?
<g/>
Rocket	Rocket	k1gMnSc1
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
I	I	kA
Think	Think	k1gMnSc1
It	It	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Going	Going	k1gInSc1
to	ten	k3xDgNnSc1
Be	Be	k1gMnSc1
a	a	k8xC
Long	Long	k1gMnSc1
<g/>
,	,	kIx,
Long	Long	k1gMnSc1
Time	Tim	k1gFnSc2
<g/>
)	)	kIx)
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
„	„	k?
<g/>
Salvation	Salvation	k1gInSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
„	„	k?
<g/>
Slave	Slaev	k1gFnSc2
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
</s>
<s>
„	„	k?
<g/>
Amy	Amy	k1gFnSc1
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
„	„	k?
<g/>
Mona	Mona	k1gMnSc1
Lisas	Lisas	k1gMnSc1
and	and	k?
Mad	Mad	k1gFnSc2
Hatters	Hattersa	k1gFnPc2
<g/>
“	“	k?
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
„	„	k?
<g/>
Hercules	Hercules	k1gInSc1
<g/>
“	“	k?
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
</s>
<s>
„	„	k?
<g/>
Slave	Slaev	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
alternativní	alternativní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
(	(	kIx(
<g/>
bonus	bonus	k1gInSc1
na	na	k7c6
reedici	reedice	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgNnSc1d1
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
varhany	varhany	k1gInPc1
<g/>
,	,	kIx,
harmonium	harmonium	k1gNnSc1
</s>
<s>
Davey	Davea	k1gFnPc1
Johnstone	Johnston	k1gInSc5
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
banjo	banjo	k1gNnSc1
<g/>
,	,	kIx,
steel	steel	k1gInSc1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
mandolína	mandolína	k1gFnSc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Dee	Dee	k?
Murray	Murraa	k1gFnPc4
–	–	k?
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Nigel	Nigel	k1gMnSc1
Olsson	Olsson	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
<g/>
,	,	kIx,
konga	konga	k1gFnSc1
<g/>
,	,	kIx,
tamburína	tamburína	k1gFnSc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Jason	Jason	k1gInSc1
Barnhart	Barnhart	k1gInSc1
–	–	k?
trubka	trubka	k1gFnSc1
</s>
<s>
Jacques	Jacques	k1gMnSc1
Bolognesi	Bolognese	k1gFnSc3
–	–	k?
pozoun	pozoun	k1gInSc1
</s>
<s>
Jean-Louis	Jean-Louis	k1gInSc1
Chautemps	Chautemps	k1gInSc1
–	–	k?
saxofon	saxofon	k1gInSc1
</s>
<s>
Alain	Alain	k2eAgInSc1d1
Hatot	Hatot	k1gInSc1
–	–	k?
saxofon	saxofon	k1gInSc1
</s>
<s>
Jean-Luc	Jean-Luc	k6eAd1
Ponty	Pont	k1gInPc1
–	–	k?
elektrické	elektrický	k2eAgFnPc4d1
housle	housle	k1gFnPc4
</s>
<s>
„	„	k?
<g/>
Legs	Legs	k1gInSc1
<g/>
“	“	k?
Larry	Larra	k1gFnSc2
Smith	Smith	k1gMnSc1
–	–	k?
step	step	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Hentschel	Hentschel	k1gMnSc1
–	–	k?
syntezátor	syntezátor	k1gInSc1
</s>
<s>
Ray	Ray	k?
Cooper	Cooper	k1gMnSc1
–	–	k?
konga	konga	k1gFnSc1
</s>
<s>
Gus	Gus	k?
Dudgeon	Dudgeon	k1gInSc1
–	–	k?
píšťalka	píšťalka	k1gFnSc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Madeline	Madelin	k1gInSc5
Bell	bell	k1gInSc4
–	–	k?
doprovodné	doprovodný	k2eAgInPc4d1
vokály	vokál	k1gInPc4
</s>
<s>
Liza	Liza	k6eAd1
Strike	Strik	k1gInPc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Larry	Larr	k1gInPc1
Steel	Steela	k1gFnPc2
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Tony	Tony	k1gMnSc1
Hazzard	Hazzard	k1gMnSc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ERLEWINE	ERLEWINE	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgMnSc1d1
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
<g/>
:	:	kIx,
Honky	Honk	k1gInPc1
Château	château	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHRISTGAU	CHRISTGAU	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
Christgau	Christgaus	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
•	•	k?
Davey	Davea	k1gFnSc2
Johnstone	Johnston	k1gInSc5
•	•	k?
John	John	k1gMnSc1
Mahon	mahon	k1gInSc1
•	•	k?
Kim	Kim	k1gFnSc2
Bullard	Bullard	k1gMnSc1
•	•	k?
Nigel	Nigel	k1gMnSc1
OlssonBob	OlssonBoba	k1gFnPc2
Birch	Birch	k1gMnSc1
•	•	k?
Tony	Tony	k1gMnSc1
Murray	Murraa	k1gFnSc2
•	•	k?
Roger	Roger	k1gMnSc1
Pope	pop	k1gInSc5
•	•	k?
Barry	Barra	k1gFnSc2
Morgan	morgan	k1gMnSc1
•	•	k?
Dee	Dee	k1gMnSc1
Murray	Murraa	k1gFnSc2
•	•	k?
Ray	Ray	k1gMnSc1
Cooper	Cooper	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Newton	Newton	k1gMnSc1
Howard	Howarda	k1gFnPc2
•	•	k?
Caleb	Calba	k1gFnPc2
Quaye	Quaye	k1gFnSc1
•	•	k?
Kenny	Kenna	k1gFnSc2
Passarelli	Passarell	k1gMnSc3
•	•	k?
Charlie	Charlie	k1gMnSc1
Morgan	morgan	k1gMnSc1
•	•	k?
Guy	Guy	k1gMnSc4
Babylon	Babylon	k1gInSc1
Studiová	studiový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Empty	Empt	k1gInPc1
Sky	Sky	k1gFnSc2
•	•	k?
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
•	•	k?
Tumbleweed	Tumbleweed	k1gInSc1
Connection	Connection	k1gInSc1
•	•	k?
Madman	Madman	k1gMnSc1
Across	Acrossa	k1gFnPc2
the	the	k?
Water	Water	k1gMnSc1
•	•	k?
Honky	Honka	k1gFnSc2
Château	château	k1gNnSc2
•	•	k?
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Shoot	Shoot	k1gInSc1
Me	Me	k1gMnSc1
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Only	Onla	k1gMnSc2
the	the	k?
Piano	piano	k1gNnSc1
Player	Player	k1gMnSc1
•	•	k?
Goodbye	Goodby	k1gFnSc2
Yellow	Yellow	k1gMnSc1
Brick	Brick	k1gMnSc1
Road	Road	k1gMnSc1
•	•	k?
Caribou	Cariba	k1gMnSc7
•	•	k?
Captain	Captain	k1gInSc1
Fantastic	Fantastice	k1gFnPc2
and	and	k?
the	the	k?
Brown	Brown	k1gMnSc1
Dirt	Dirt	k1gMnSc1
Cowboy	Cowboa	k1gFnSc2
•	•	k?
Rock	rock	k1gInSc1
of	of	k?
the	the	k?
Westies	Westies	k1gInSc1
•	•	k?
Blue	Blue	k1gInSc1
Moves	Moves	k1gMnSc1
•	•	k?
A	a	k9
Single	singl	k1gInSc5
Man	Man	k1gMnSc1
•	•	k?
Victim	Victim	k1gMnSc1
of	of	k?
Love	lov	k1gInSc5
•	•	k?
21	#num#	k4
at	at	k?
33	#num#	k4
•	•	k?
The	The	k1gFnSc1
Fox	fox	k1gInSc1
•	•	k?
Jump	Jump	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Too	Too	k1gMnSc1
Low	Low	k1gMnSc1
for	forum	k1gNnPc2
Zero	Zero	k6eAd1
•	•	k?
Breaking	Breaking	k1gInSc1
Hearts	Hearts	k1gInSc1
•	•	k?
Ice	Ice	k1gFnSc2
on	on	k3xPp3gInSc1
Fire	Fire	k1gInSc1
•	•	k?
Leather	Leathra	k1gFnPc2
Jackets	Jacketsa	k1gFnPc2
•	•	k?
Reg	Reg	k1gMnSc1
Strikes	Strikes	k1gMnSc1
Back	Back	k1gMnSc1
•	•	k?
Sleeping	Sleeping	k1gInSc1
with	with	k1gInSc1
the	the	k?
Past	past	k1gFnSc1
•	•	k?
The	The	k1gMnSc1
One	One	k1gMnSc1
•	•	k?
Duets	Duets	k1gInSc1
•	•	k?
Made	Made	k1gInSc1
in	in	k?
England	England	k1gInSc1
•	•	k?
The	The	k1gMnSc5
Big	Big	k1gMnSc5
Picture	Pictur	k1gMnSc5
•	•	k?
Songs	Songs	k1gInSc1
from	from	k1gMnSc1
the	the	k?
West	West	k1gMnSc1
Coast	Coast	k1gMnSc1
•	•	k?
Peachtree	Peachtree	k1gInSc1
Road	Road	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Captain	Captain	k1gMnSc1
&	&	k?
the	the	k?
Kid	Kid	k1gMnSc7
•	•	k?
The	The	k1gFnSc1
Union	union	k1gInSc1
•	•	k?
The	The	k1gFnSc1
Diving	Diving	k1gInSc1
Board	Board	k1gInSc1
•	•	k?
Wonderful	Wonderful	k1gInSc4
Crazy	Craza	k1gFnSc2
Night	Night	k1gInSc4
Koncertní	koncertní	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
17-11-70	17-11-70	k4
•	•	k?
Here	Here	k1gInSc1
and	and	k?
There	Ther	k1gInSc5
•	•	k?
Live	Live	k1gFnSc6
in	in	k?
Australia	Australium	k1gNnSc2
with	witha	k1gFnPc2
the	the	k?
Melbourne	Melbourne	k1gNnSc4
Symphony	Symphona	k1gFnSc2
Orchestra	orchestra	k1gFnSc1
•	•	k?
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
One	One	k1gMnSc1
Night	Night	k1gMnSc1
Only	Onla	k1gFnSc2
–	–	k?
The	The	k1gMnSc1
Greatest	Greatest	k1gMnSc1
Hits	Hitsa	k1gFnPc2
•	•	k?
Elton	Elton	k1gNnSc1
60	#num#	k4
–	–	k?
Live	Live	k1gInSc1
at	at	k?
Madison	Madison	k1gInSc1
Square	square	k1gInSc1
Garden	Gardna	k1gFnPc2
Soundtracky	soundtrack	k1gInPc1
</s>
<s>
Friends	Friends	k6eAd1
•	•	k?
The	The	k1gMnSc1
Lion	Lion	k1gMnSc1
King	King	k1gMnSc1
•	•	k?
The	The	k1gFnSc2
Muse	Musa	k1gFnSc3
•	•	k?
Elton	Elton	k1gMnSc1
John	John	k1gMnSc1
and	and	k?
Tim	Tim	k?
Rice	Rice	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Aida	Aida	k1gFnSc1
•	•	k?
The	The	k1gMnSc1
Road	Road	k1gMnSc1
to	ten	k3xDgNnSc4
El	Ela	k1gFnPc2
Dorado	Dorada	k1gFnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
