<p>
<s>
Krtek	krtek	k1gMnSc1	krtek
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
europaea	europaeus	k1gMnSc4	europaeus
<g/>
,	,	kIx,	,
Linné	Linný	k1gMnPc4	Linný
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
černý	černý	k2eAgMnSc1d1	černý
hmyzožravec	hmyzožravec	k1gMnSc1	hmyzožravec
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
v	v	k7c6	v
parcích	park	k1gInPc6	park
a	a	k8xC	a
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
decimetrů	decimetr	k1gInPc2	decimetr
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Buduje	budovat	k5eAaImIp3nS	budovat
chodby	chodba	k1gFnPc4	chodba
-	-	kIx~	-
jednak	jednak	k8xC	jednak
obytné	obytný	k2eAgInPc1d1	obytný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
okružní	okružní	k2eAgFnSc2d1	okružní
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
typické	typický	k2eAgFnSc2d1	typická
hliněné	hliněný	k2eAgFnSc2d1	hliněná
hromádky	hromádka	k1gFnSc2	hromádka
zvané	zvaný	k2eAgFnPc1d1	zvaná
krtinec	krtinec	k1gInSc4	krtinec
či	či	k8xC	či
krtina	krtina	k1gFnSc1	krtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Krtek	krtek	k1gMnSc1	krtek
má	mít	k5eAaImIp3nS	mít
válcovité	válcovitý	k2eAgNnSc4d1	válcovité
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
12	[number]	k4	12
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
a	a	k8xC	a
skryté	skrytý	k2eAgNnSc1d1	skryté
za	za	k7c4	za
chlupy	chlup	k1gInPc4	chlup
<g/>
,	,	kIx,	,
zrak	zrak	k1gInSc1	zrak
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sluch	sluch	k1gInSc1	sluch
a	a	k8xC	a
čich	čich	k1gInSc1	čich
má	mít	k5eAaImIp3nS	mít
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
malými	malý	k2eAgInPc7d1	malý
výstupky	výstupek	k1gInPc7	výstupek
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
díky	díky	k7c3	díky
životu	život	k1gInSc3	život
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
barvy	barva	k1gFnPc1	barva
znevýhodněny	znevýhodněn	k2eAgFnPc1d1	znevýhodněna
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krtek	krtek	k1gMnSc1	krtek
má	mít	k5eAaImIp3nS	mít
přední	přední	k2eAgFnPc4d1	přední
tlapky	tlapka	k1gFnPc4	tlapka
dobře	dobře	k6eAd1	dobře
uzpůsobené	uzpůsobený	k2eAgFnPc1d1	uzpůsobená
k	k	k7c3	k
hrabání	hrabání	k1gNnSc3	hrabání
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
svalnaté	svalnatý	k2eAgFnPc1d1	svalnatá
<g/>
,	,	kIx,	,
vyvrácené	vyvrácený	k2eAgFnPc1d1	vyvrácená
a	a	k8xC	a
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
drápky	drápek	k1gInPc7	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
tlapkách	tlapka	k1gFnPc6	tlapka
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
"	"	kIx"	"
<g/>
prstů	prst	k1gInPc2	prst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
jen	jen	k9	jen
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hloubí	hloubit	k5eAaImIp3nP	hloubit
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
tunelů	tunel	k1gInPc2	tunel
-	-	kIx~	-
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
samičkách	samička	k1gFnPc6	samička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
5	[number]	k4	5
-	-	kIx~	-
20	[number]	k4	20
cm	cm	kA	cm
pak	pak	k6eAd1	pak
tunely	tunel	k1gInPc1	tunel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
skladuje	skladovat	k5eAaImIp3nS	skladovat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
tunelů	tunel	k1gInPc2	tunel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	s	k7c7	s
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
bezobratlými	bezobratlý	k2eAgMnPc7d1	bezobratlý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pomocí	pomocí	k7c2	pomocí
sluchu	sluch	k1gInSc2	sluch
a	a	k8xC	a
čichu	čich	k1gInSc2	čich
a	a	k8xC	a
vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
je	být	k5eAaImIp3nS	být
ostrým	ostrý	k2eAgInSc7d1	ostrý
čumáčkem	čumáček	k1gInSc7	čumáček
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnSc4	jeho
potravu	potrava	k1gFnSc4	potrava
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
larvy	larva	k1gFnPc4	larva
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
žáby	žába	k1gFnSc2	žába
a	a	k8xC	a
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Žížaly	Žížala	k1gMnPc4	Žížala
ochromuje	ochromovat	k5eAaImIp3nS	ochromovat
kousnutím	kousnutí	k1gNnSc7	kousnutí
do	do	k7c2	do
nervového	nervový	k2eAgNnSc2d1	nervové
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
skladuje	skladovat	k5eAaImIp3nS	skladovat
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
zásoby	zásoba	k1gFnPc4	zásoba
na	na	k7c4	na
horší	zlý	k2eAgInPc4d2	horší
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
dokáže	dokázat	k5eAaPmIp3nS	dokázat
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
téměř	téměř	k6eAd1	téměř
tolik	tolik	k4yIc4	tolik
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
sám	sám	k3xTgMnSc1	sám
váží	vážit	k5eAaImIp3nS	vážit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
mívá	mívat	k5eAaImIp3nS	mívat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
<g/>
)	)	kIx)	)
po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
3-7	[number]	k4	3-7
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
neosrstěná	osrstěný	k2eNgFnSc1d1	neosrstěná
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
asi	asi	k9	asi
jako	jako	k8xC	jako
fazole	fazole	k1gFnSc1	fazole
<g/>
;	;	kIx,	;
prohlédnou	prohlédnout	k5eAaPmIp3nP	prohlédnout
za	za	k7c7	za
3	[number]	k4	3
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
pijí	pít	k5eAaImIp3nP	pít
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
po	po	k7c6	po
3	[number]	k4	3
týdnech	týden	k1gInPc6	týden
sama	sám	k3xTgMnSc4	sám
loví	lovit	k5eAaImIp3nP	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
již	již	k6eAd1	již
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
obvykle	obvykle	k6eAd1	obvykle
hned	hned	k6eAd1	hned
následující	následující	k2eAgNnSc4d1	následující
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
European	Europeana	k1gFnPc2	Europeana
Mole	mol	k1gInSc5	mol
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krtek	krtek	k1gMnSc1	krtek
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
krtek	krtek	k1gMnSc1	krtek
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Talpa	Talp	k1gMnSc2	Talp
europaea	europaeus	k1gMnSc2	europaeus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
