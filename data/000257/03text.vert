<s>
Paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
para	para	k2eAgFnSc1d1	para
<g/>
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
lympiáda	lympiáda	k1gFnSc1	lympiáda
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vícesportovní	vícesportovní	k2eAgFnSc1d1	vícesportovní
událost	událost	k1gFnSc1	událost
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
sportovce	sportovec	k1gMnSc4	sportovec
s	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
tělesným	tělesný	k2eAgNnSc7d1	tělesné
<g/>
,	,	kIx,	,
mentálním	mentální	k2eAgNnSc7d1	mentální
a	a	k8xC	a
senzorickým	senzorický	k2eAgNnSc7d1	senzorické
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
to	ten	k3xDgNnSc1	ten
sportovce	sportovec	k1gMnSc4	sportovec
se	s	k7c7	s
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
amputacemi	amputace	k1gFnPc7	amputace
<g/>
,	,	kIx,	,
oslepnutím	oslepnutí	k1gNnSc7	oslepnutí
a	a	k8xC	a
mentální	mentální	k2eAgFnSc7d1	mentální
retardací	retardace	k1gFnSc7	retardace
<g/>
.	.	kIx.	.
</s>
<s>
Paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
je	on	k3xPp3gMnPc4	on
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
paralympijský	paralympijský	k2eAgInSc1d1	paralympijský
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
občas	občas	k6eAd1	občas
zaměňovány	zaměňován	k2eAgInPc1d1	zaměňován
se	s	k7c7	s
Světovými	světový	k2eAgFnPc7d1	světová
hrami	hra	k1gFnPc7	hra
speciálních	speciální	k2eAgFnPc2d1	speciální
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
určeny	určen	k2eAgInPc1d1	určen
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
duševním	duševní	k2eAgNnSc7d1	duševní
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
aktivity	aktivita	k1gFnPc4	aktivita
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
nominací	nominace	k1gFnSc7	nominace
na	na	k7c4	na
paralympiádu	paralympiáda	k1gFnSc4	paralympiáda
Český	český	k2eAgInSc4d1	český
paralympijský	paralympijský	k2eAgInSc4d1	paralympijský
výbor	výbor	k1gInSc4	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
získaných	získaný	k2eAgFnPc2d1	získaná
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
paralympiád	paralympiáda	k1gFnPc2	paralympiáda
drží	držet	k5eAaImIp3nS	držet
Ragnhild	Ragnhild	k1gMnSc1	Ragnhild
Myklebustová	Myklebustová	k1gFnSc1	Myklebustová
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
paralympijských	paralympijský	k2eAgFnPc6d1	paralympijská
hrách	hra	k1gFnPc6	hra
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
a	a	k8xC	a
2002	[number]	k4	2002
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
severském	severský	k2eAgNnSc6d1	severské
lyžování	lyžování	k1gNnSc6	lyžování
dohromady	dohromady	k6eAd1	dohromady
22	[number]	k4	22
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
17	[number]	k4	17
bylo	být	k5eAaImAgNnS	být
zlatých	zlatý	k2eAgNnPc2d1	Zlaté
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
sporty	sport	k1gInPc4	sport
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
programu	program	k1gInSc6	program
letních	letní	k2eAgFnPc2d1	letní
paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
<g/>
:	:	kIx,	:
Lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
Atletika	atletika	k1gFnSc1	atletika
Boccia	boccia	k1gFnSc1	boccia
Cyklistika	cyklistika	k1gFnSc1	cyklistika
Paradrezura	Paradrezura	k1gFnSc1	Paradrezura
Fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-a-side	id	k1gInSc5	-a-sid
<g/>
)	)	kIx)	)
Fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
-a-side	id	k1gInSc5	-a-sid
<g/>
)	)	kIx)	)
Goalball	Goalball	k1gInSc1	Goalball
Judo	judo	k1gNnSc1	judo
Powerlifting	Powerlifting	k1gInSc1	Powerlifting
Veslování	veslování	k1gNnSc2	veslování
Vodáctví	vodáctví	k1gNnSc2	vodáctví
Střelba	střelba	k1gFnSc1	střelba
Plavání	plavání	k1gNnSc6	plavání
Stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
Volejbal	volejbal	k1gInSc1	volejbal
(	(	kIx(	(
<g/>
sezení	sezení	k1gNnSc1	sezení
<g/>
)	)	kIx)	)
Basketbal	basketbal	k1gInSc1	basketbal
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
vozíčkáři	vozíčkář	k1gMnPc1	vozíčkář
<g/>
)	)	kIx)	)
Šerm	šerm	k1gInSc1	šerm
(	(	kIx(	(
<g/>
vozíčkáři	vozíčkář	k1gMnPc5	vozíčkář
<g/>
)	)	kIx)	)
Ragby	ragby	k1gNnSc4	ragby
(	(	kIx(	(
<g/>
vozíčkáři	vozíčkář	k1gMnPc5	vozíčkář
<g/>
)	)	kIx)	)
zvedání	zvedání	k1gNnSc4	zvedání
činek	činka	k1gFnPc2	činka
Tenis	tenis	k1gInSc1	tenis
(	(	kIx(	(
<g/>
vozíčkáři	vozíčkář	k1gMnPc1	vozíčkář
<g/>
)	)	kIx)	)
Následující	následující	k2eAgInPc1d1	následující
sporty	sport	k1gInPc1	sport
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
programu	program	k1gInSc6	program
zimních	zimní	k2eAgFnPc2d1	zimní
paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
<g/>
:	:	kIx,	:
Alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc1	lyžování
Hokej	hokej	k1gInSc1	hokej
(	(	kIx(	(
<g/>
lední	lední	k2eAgFnPc1d1	lední
sáně	sáně	k1gFnPc1	sáně
<g/>
)	)	kIx)	)
Severské	severský	k2eAgNnSc1d1	severské
lyžování	lyžování	k1gNnSc1	lyžování
Biatlon	biatlon	k1gInSc1	biatlon
Běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Curling	Curling	k1gInSc1	Curling
(	(	kIx(	(
<g/>
vozíčkáři	vozíčkář	k1gMnPc1	vozíčkář
<g/>
)	)	kIx)	)
</s>
