<s>
Zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
živočichům	živočich	k1gMnPc3	živočich
vnímat	vnímat	k5eAaImF	vnímat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
smysl	smysl	k1gInSc1	smysl
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
informací	informace	k1gFnPc2	informace
vnímáme	vnímat	k5eAaImIp1nP	vnímat
zrakem	zrak	k1gInSc7	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
především	především	k9	především
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
kontrastu	kontrast	k1gInSc2	kontrast
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vidění	vidění	k1gNnSc4	vidění
kontur	kontura	k1gFnPc2	kontura
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
vnímání	vnímání	k1gNnSc1	vnímání
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
citlivosti	citlivost	k1gFnSc6	citlivost
zrakových	zrakový	k2eAgInPc2d1	zrakový
pigmentů	pigment	k1gInPc2	pigment
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rodopsin	rodopsin	k2eAgInSc1d1	rodopsin
<g/>
)	)	kIx)	)
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Světlem	světlo	k1gNnSc7	světlo
se	se	k3xPyFc4	se
zrakové	zrakový	k2eAgInPc1d1	zrakový
pigmenty	pigment	k1gInPc1	pigment
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zahájí	zahájit	k5eAaPmIp3nS	zahájit
řetěz	řetěz	k1gInSc1	řetěz
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
převedení	převedení	k1gNnSc3	převedení
signálu	signál	k1gInSc2	signál
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
vzruch	vzruch	k1gInSc4	vzruch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
informaci	informace	k1gFnSc4	informace
do	do	k7c2	do
zrakových	zrakový	k2eAgNnPc2d1	zrakové
center	centrum	k1gNnPc2	centrum
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Fotoreceptory	Fotoreceptor	k1gInPc1	Fotoreceptor
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
jsou	být	k5eAaImIp3nP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
světelné	světelný	k2eAgFnPc4d1	světelná
vlny	vlna	k1gFnPc4	vlna
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgInSc1d1	absolutní
práh	práh	k1gInSc1	práh
citlivosti	citlivost	k1gFnSc2	citlivost
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
J	J	kA	J
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
energii	energie	k1gFnSc6	energie
jednoho	jeden	k4xCgInSc2	jeden
jediného	jediný	k2eAgInSc2d1	jediný
fotonu	foton	k1gInSc2	foton
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rodopsin	Rodopsin	k1gInSc1	Rodopsin
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
oční	oční	k2eAgInSc1d1	oční
pigment	pigment	k1gInSc1	pigment
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
vnímání	vnímání	k1gNnSc4	vnímání
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čípkách	čípkách	k?	čípkách
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
tzv.	tzv.	kA	tzv.
jodopsiny	jodopsina	k1gFnPc1	jodopsina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
barevné	barevný	k2eAgNnSc4d1	barevné
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
však	však	k9	však
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Rodopsin	Rodopsin	k1gInSc1	Rodopsin
je	být	k5eAaImIp3nS	být
transmembránový	transmembránový	k2eAgInSc4d1	transmembránový
protein	protein	k1gInSc4	protein
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
proteinové	proteinový	k2eAgFnSc2d1	proteinová
složky	složka	k1gFnSc2	složka
opsinu	opsin	k1gInSc2	opsin
a	a	k8xC	a
karotenové	karotenový	k2eAgFnSc2d1	karotenový
složky	složka	k1gFnSc2	složka
retinalu	retinal	k1gInSc2	retinal
<g/>
.	.	kIx.	.
</s>
<s>
Retinal	retinal	k1gInSc1	retinal
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
cis-trans	cisrans	k1gInSc1	cis-trans
izomerie	izomerie	k1gFnSc2	izomerie
<g/>
.	.	kIx.	.
</s>
<s>
Cis	cis	k1gNnSc1	cis
konfigurace	konfigurace	k1gFnSc2	konfigurace
retinalu	retinal	k1gInSc2	retinal
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
lysinový	lysinový	k2eAgInSc4d1	lysinový
zbytek	zbytek	k1gInSc4	zbytek
opsinového	opsinový	k2eAgInSc2d1	opsinový
proteinu	protein	k1gInSc2	protein
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
klidový	klidový	k2eAgInSc4d1	klidový
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Retinal	retinal	k1gInSc1	retinal
je	být	k5eAaImIp3nS	být
však	však	k9	však
schopen	schopen	k2eAgMnSc1d1	schopen
prudce	prudko	k6eAd1	prudko
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
dopadající	dopadající	k2eAgInPc4d1	dopadající
fotony	foton	k1gInPc4	foton
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
totiž	totiž	k9	totiž
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
absorpci	absorpce	k1gFnSc3	absorpce
světla	světlo	k1gNnSc2	světlo
cis-retinalem	cisetinal	k1gInSc7	cis-retinal
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
all-trans	allrans	k1gInSc4	all-trans
izomer	izomer	k1gInSc1	izomer
a	a	k8xC	a
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
se	se	k3xPyFc4	se
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
konformace	konformace	k1gFnSc2	konformace
rodopsinu	rodopsina	k1gFnSc4	rodopsina
se	se	k3xPyFc4	se
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
G	G	kA	G
protein	protein	k1gInSc1	protein
transducin	transducin	k1gInSc1	transducin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
cGMP	cGMP	k?	cGMP
fosfodiesterázu	fosfodiesteráza	k1gFnSc4	fosfodiesteráza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
cGMP	cGMP	k?	cGMP
na	na	k7c4	na
otevřený	otevřený	k2eAgInSc4d1	otevřený
GMP	GMP	kA	GMP
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
tyčinkách	tyčinka	k1gFnPc6	tyčinka
není	být	k5eNaImIp3nS	být
cGMP	cGMP	k?	cGMP
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
sodíkových	sodíkový	k2eAgInPc2d1	sodíkový
iontových	iontový	k2eAgInPc2d1	iontový
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hyperpolarizaci	hyperpolarizace	k1gFnSc3	hyperpolarizace
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
synapse	synapse	k1gFnSc1	synapse
na	na	k7c6	na
tyčinkových	tyčinkový	k2eAgFnPc6d1	tyčinková
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
produkce	produkce	k1gFnSc2	produkce
jistých	jistý	k2eAgInPc2d1	jistý
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
nedostatek	nedostatek	k1gInSc1	nedostatek
způsobí	způsobit	k5eAaPmIp3nS	způsobit
depolarizaci	depolarizace	k1gFnSc3	depolarizace
membrány	membrána	k1gFnSc2	membrána
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
akčního	akční	k2eAgInSc2d1	akční
potenciálu	potenciál	k1gInSc2	potenciál
v	v	k7c6	v
očním	oční	k2eAgInSc6d1	oční
nervu	nerv	k1gInSc6	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
dodání	dodání	k1gNnSc3	dodání
této	tento	k3xDgFnSc2	tento
informace	informace	k1gFnSc2	informace
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Retinal	retinal	k1gInSc1	retinal
je	být	k5eAaImIp3nS	být
derivát	derivát	k1gInSc1	derivát
vitamínu	vitamín	k1gInSc2	vitamín
A.	A.	kA	A.
Při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
nedostatku	nedostatek	k1gInSc6	nedostatek
se	se	k3xPyFc4	se
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
tmu	tma	k1gFnSc4	tma
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
syntéza	syntéza	k1gFnSc1	syntéza
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
pigmentů	pigment	k1gInPc2	pigment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
šeroslepost	šeroslepost	k1gFnSc1	šeroslepost
<g/>
.	.	kIx.	.
</s>
<s>
Zraková	zrakový	k2eAgFnSc1d1	zraková
ostrost	ostrost	k1gFnSc1	ostrost
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
odlišit	odlišit	k5eAaPmF	odlišit
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
schopnosti	schopnost	k1gFnPc4	schopnost
optického	optický	k2eAgInSc2d1	optický
aparátu	aparát	k1gInSc2	aparát
zaostřit	zaostřit	k5eAaPmF	zaostřit
paprsky	paprsek	k1gInPc1	paprsek
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
průhlednosti	průhlednost	k1gFnSc6	průhlednost
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
intenzity	intenzita	k1gFnSc2	intenzita
osvětlení	osvětlení	k1gNnPc2	osvětlení
a	a	k8xC	a
hustotě	hustota	k1gFnSc3	hustota
a	a	k8xC	a
zapojení	zapojení	k1gNnSc3	zapojení
fotoreceptorů	fotoreceptor	k1gInPc2	fotoreceptor
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
skvrně	skvrna	k1gFnSc6	skvrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
čípků	čípek	k1gInPc2	čípek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
úhlová	úhlový	k2eAgFnSc1d1	úhlová
minuta	minuta	k1gFnSc1	minuta
-	-	kIx~	-
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
obrazů	obraz	k1gInPc2	obraz
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pouhých	pouhý	k2eAgFnPc2d1	pouhá
5	[number]	k4	5
μ	μ	k?	μ
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
podrážděnými	podrážděný	k2eAgFnPc7d1	podrážděná
světločivými	světločivý	k2eAgFnPc7d1	světločivá
buňkami	buňka	k1gFnPc7	buňka
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
nepodrážděná	podrážděný	k2eNgFnSc1d1	nepodrážděná
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Smyslovým	smyslový	k2eAgInSc7d1	smyslový
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
oko	oko	k1gNnSc1	oko
(	(	kIx(	(
<g/>
oculus	oculus	k1gInSc1	oculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
oční	oční	k2eAgFnSc2d1	oční
koule	koule	k1gFnSc2	koule
a	a	k8xC	a
přídatných	přídatný	k2eAgInPc2d1	přídatný
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
světločivná	světločivný	k2eAgFnSc1d1	světločivná
vrstva	vrstva	k1gFnSc1	vrstva
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
fotoreceptory	fotoreceptor	k1gInPc4	fotoreceptor
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
světločivé	světločivý	k2eAgFnPc1d1	světločivá
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
tyčinky	tyčinka	k1gFnPc1	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc1	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
zanořeny	zanořit	k5eAaPmNgFnP	zanořit
v	v	k7c6	v
pigmentovém	pigmentový	k2eAgInSc6d1	pigmentový
epitelu	epitel	k1gInSc6	epitel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
jejich	jejich	k3xOp3gFnSc4	jejich
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
světelnou	světelný	k2eAgFnSc4d1	světelná
izolaci	izolace	k1gFnSc4	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
oku	oko	k1gNnSc6	oko
přes	přes	k7c4	přes
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
světločivých	světločivý	k2eAgFnPc2d1	světločivá
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
zrakového	zrakový	k2eAgNnSc2d1	zrakové
vnímání	vnímání	k1gNnSc2	vnímání
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
části	část	k1gFnPc1	část
oka	oko	k1gNnSc2	oko
tvořící	tvořící	k2eAgFnPc1d1	tvořící
jeho	jeho	k3xOp3gFnSc1	jeho
optický	optický	k2eAgInSc4d1	optický
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
rohovka	rohovka	k1gFnSc1	rohovka
<g/>
,	,	kIx,	,
komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
sklivec	sklivec	k1gInSc1	sklivec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
paprsky	paprsek	k1gInPc4	paprsek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc1	jejich
ohnisko	ohnisko	k1gNnSc1	ohnisko
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barevné	barevný	k2eAgNnSc4d1	barevné
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Zraková	zrakový	k2eAgFnSc1d1	zraková
ostrost	ostrost	k1gFnSc1	ostrost
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
jenom	jenom	k9	jenom
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
tmu	tma	k1gFnSc4	tma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
úplně	úplně	k6eAd1	úplně
slepí	slepý	k2eAgMnPc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
barev	barva	k1gFnPc2	barva
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
čípky	čípek	k1gInPc1	čípek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
normálním	normální	k2eAgNnSc6d1	normální
lidském	lidský	k2eAgNnSc6d1	lidské
oku	oko	k1gNnSc6	oko
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgMnPc4d1	lišící
se	se	k3xPyFc4	se
barevnými	barevný	k2eAgInPc7d1	barevný
pigmenty	pigment	k1gInPc7	pigment
a	a	k8xC	a
citlivostí	citlivost	k1gFnSc7	citlivost
k	k	k7c3	k
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Čípky	čípek	k1gInPc1	čípek
lidí	člověk	k1gMnPc2	člověk
vnímají	vnímat	k5eAaImIp3nP	vnímat
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgNnSc1d1	normální
lidské	lidský	k2eAgNnSc1d1	lidské
vidění	vidění	k1gNnSc1	vidění
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
trichromatické	trichromatický	k2eAgNnSc1d1	trichromatický
<g/>
,	,	kIx,	,
vidění	vidění	k1gNnSc3	vidění
barvoslepých	barvoslepý	k2eAgMnPc2d1	barvoslepý
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
dichromatické	dichromatický	k2eAgInPc1d1	dichromatický
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k6eAd1	málo
známým	známý	k2eAgInSc7d1	známý
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
opak	opak	k1gInSc1	opak
barvosleposti	barvoslepost	k1gFnSc2	barvoslepost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
živočichové	živočich	k1gMnPc1	živočich
nevnímají	vnímat	k5eNaImIp3nP	vnímat
barvy	barva	k1gFnPc4	barva
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Trichromatické	trichromatický	k2eAgNnSc1d1	trichromatický
vidění	vidění	k1gNnSc1	vidění
je	být	k5eAaImIp3nS	být
výsada	výsada	k1gFnSc1	výsada
primátů	primát	k1gMnPc2	primát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Většina	většina	k1gFnSc1	většina
savců	savec	k1gMnPc2	savec
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dichromatické	dichromatický	k2eAgNnSc1d1	dichromatický
vidění	vidění	k1gNnSc3	vidění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známým	známý	k2eAgInSc7d1	známý
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
barvoslepý	barvoslepý	k2eAgMnSc1d1	barvoslepý
-	-	kIx~	-
ale	ale	k8xC	ale
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
dobře	dobře	k6eAd1	dobře
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
nemá	mít	k5eNaImIp3nS	mít
čípky	čípek	k1gInPc1	čípek
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
plazi	plaz	k1gMnPc1	plaz
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
tetrachromatické	tetrachromatický	k2eAgNnSc4d1	tetrachromatický
vidění	vidění	k1gNnSc4	vidění
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
primáti	primát	k1gMnPc1	primát
vnímají	vnímat	k5eAaImIp3nP	vnímat
barvy	barva	k1gFnPc4	barva
od	od	k7c2	od
modré	modrý	k2eAgFnSc2d1	modrá
po	po	k7c4	po
červenou	červená	k1gFnSc4	červená
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
světlo	světlo	k1gNnSc1	světlo
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
400	[number]	k4	400
do	do	k7c2	do
700	[number]	k4	700
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
citlivost	citlivost	k1gFnSc1	citlivost
mírně	mírně	k6eAd1	mírně
posunuta	posunut	k2eAgFnSc1d1	posunuta
k	k	k7c3	k
modrým	modrý	k2eAgFnPc3d1	modrá
barvám	barva	k1gFnPc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
Hlubinné	hlubinný	k2eAgFnPc1d1	hlubinná
ryby	ryba	k1gFnPc1	ryba
mají	mít	k5eAaImIp3nP	mít
citlivost	citlivost	k1gFnSc4	citlivost
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proniká	pronikat	k5eAaImIp3nS	pronikat
pod	pod	k7c4	pod
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
nejhlouběji	hluboko	k6eAd3	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
vidí	vidět	k5eAaImIp3nS	vidět
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
světlo	světlo	k1gNnSc4	světlo
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kratší	krátký	k2eAgFnSc7d2	kratší
než	než	k8xS	než
400	[number]	k4	400
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevidí	vidět	k5eNaImIp3nS	vidět
naopak	naopak	k6eAd1	naopak
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
hadi	had	k1gMnPc1	had
vidí	vidět	k5eAaImIp3nS	vidět
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
barev	barva	k1gFnPc2	barva
od	od	k7c2	od
ultrafialové	ultrafialový	k2eAgFnSc2d1	ultrafialová
až	až	k9	až
po	po	k7c4	po
infračervenou	infračervený	k2eAgFnSc4d1	infračervená
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
700	[number]	k4	700
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
tepla	teplo	k1gNnSc2	teplo
oběti	oběť	k1gFnSc2	oběť
mají	mít	k5eAaImIp3nP	mít
někteří	některý	k3yIgMnPc1	některý
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
i	i	k9	i
další	další	k2eAgInSc4d1	další
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setmění	setmění	k1gNnSc6	setmění
(	(	kIx(	(
<g/>
šeru	šer	k1gInSc2wB	šer
<g/>
)	)	kIx)	)
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
zornice	zornice	k1gFnSc2	zornice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
dostalo	dostat	k5eAaPmAgNnS	dostat
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
oka	oko	k1gNnSc2	oko
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
čípky	čípek	k1gInPc1	čípek
méně	málo	k6eAd2	málo
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
přestáváme	přestávat	k5eAaImIp1nP	přestávat
vidět	vidět	k5eAaImF	vidět
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
šelmy	šelma	k1gFnPc4	šelma
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
žralok	žralok	k1gMnSc1	žralok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kráva	kráva	k1gFnSc1	kráva
nebo	nebo	k8xC	nebo
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
za	za	k7c7	za
sítnicí	sítnice	k1gFnSc7	sítnice
vrstvu	vrstva	k1gFnSc4	vrstva
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vláken	vlákno	k1gNnPc2	vlákno
<g/>
)	)	kIx)	)
schopných	schopný	k2eAgMnPc2d1	schopný
odrážet	odrážet	k5eAaImF	odrážet
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lepší	dobrý	k2eAgNnPc1d2	lepší
vidění	vidění	k1gNnPc1	vidění
za	za	k7c4	za
šera	šero	k1gNnPc4	šero
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
světelné	světelný	k2eAgInPc1d1	světelný
paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
projdou	projít	k5eAaPmIp3nP	projít
sítnicí	sítnice	k1gFnSc7	sítnice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odrazí	odrazit	k5eAaPmIp3nP	odrazit
a	a	k8xC	a
procházejí	procházet	k5eAaImIp3nP	procházet
sítnicí	sítnice	k1gFnSc7	sítnice
zase	zase	k9	zase
nazpět	nazpět	k6eAd1	nazpět
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
podráždit	podráždit	k5eAaPmF	podráždit
fotoreceptory	fotoreceptor	k1gMnPc7	fotoreceptor
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Odražené	odražený	k2eAgNnSc1d1	odražené
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
"	"	kIx"	"
<g/>
svítících	svítící	k2eAgNnPc2d1	svítící
očí	oko	k1gNnPc2	oko
<g/>
"	"	kIx"	"
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zraku	zrak	k1gInSc2	zrak
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k9	především
k	k	k7c3	k
poruchám	porucha	k1gFnPc3	porucha
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
poruchám	porucha	k1gFnPc3	porucha
vnímání	vnímání	k1gNnSc6	vnímání
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
celkovému	celkový	k2eAgNnSc3d1	celkové
oslabení	oslabení	k1gNnSc3	oslabení
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	poruch	k1gInPc1	poruch
ostrosti	ostrost	k1gFnSc2	ostrost
jsou	být	k5eAaImIp3nP	být
způsobeny	způsoben	k2eAgInPc1d1	způsoben
špatnou	špatný	k2eAgFnSc7d1	špatná
akomodací	akomodace	k1gFnSc7	akomodace
oční	oční	k2eAgFnSc2d1	oční
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
krátkozrakost	krátkozrakost	k1gFnSc1	krátkozrakost
dalekozrakost	dalekozrakost	k1gFnSc1	dalekozrakost
astigmatismus	astigmatismus	k1gInSc4	astigmatismus
Špatné	špatný	k2eAgNnSc1d1	špatné
rozlišování	rozlišování	k1gNnSc1	rozlišování
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
chybnou	chybný	k2eAgFnSc7d1	chybná
činností	činnost	k1gFnSc7	činnost
čípků	čípek	k1gInPc2	čípek
nebo	nebo	k8xC	nebo
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
špatné	špatný	k2eAgFnSc2d1	špatná
činnosti	činnost	k1gFnSc2	činnost
tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
šeroslepost	šeroslepost	k1gFnSc1	šeroslepost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
omezená	omezený	k2eAgFnSc1d1	omezená
schopnost	schopnost	k1gFnSc1	schopnost
vidění	vidění	k1gNnSc2	vidění
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
špatné	špatný	k2eAgFnSc6d1	špatná
činnosti	činnost	k1gFnSc6	činnost
čípků	čípek	k1gInPc2	čípek
člověk	člověk	k1gMnSc1	člověk
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
schopnost	schopnost	k1gFnSc4	schopnost
vidět	vidět	k5eAaImF	vidět
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
jen	jen	k9	jen
asi	asi	k9	asi
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
ze	z	k7c2	z
100.000	[number]	k4	100.000
vidí	vidět	k5eAaImIp3nS	vidět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
šedé	šedá	k1gFnSc2	šedá
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
pouze	pouze	k6eAd1	pouze
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
některé	některý	k3yIgInPc4	některý
odstíny	odstín	k1gInPc4	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
zaměňování	zaměňování	k1gNnSc1	zaměňování
červené	červený	k2eAgNnSc1d1	červené
se	s	k7c7	s
zelenou	zelená	k1gFnSc7	zelená
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
zaměňování	zaměňování	k1gNnSc1	zaměňování
žluté	žlutý	k2eAgNnSc1d1	žluté
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zrak	zrak	k1gInSc4	zrak
oslaben	oslaben	k2eAgMnSc1d1	oslaben
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
slabozrakosti	slabozrakost	k1gFnSc6	slabozrakost
<g/>
,	,	kIx,	,
při	při	k7c6	při
úplné	úplný	k2eAgFnSc6d1	úplná
ztrátě	ztráta	k1gFnSc6	ztráta
zraku	zrak	k1gInSc2	zrak
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
slepotě	slepota	k1gFnSc6	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Binokulární	binokulární	k2eAgNnSc4d1	binokulární
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
polohou	poloha	k1gFnSc7	poloha
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
každého	každý	k3xTgNnSc2	každý
oka	oko	k1gNnSc2	oko
dopadá	dopadat	k5eAaImIp3nS	dopadat
mírně	mírně	k6eAd1	mírně
odlišný	odlišný	k2eAgInSc1d1	odlišný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
skládá	skládat	k5eAaImIp3nS	skládat
prostorový	prostorový	k2eAgInSc1d1	prostorový
obraz	obraz	k1gInSc1	obraz
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
ho	on	k3xPp3gMnSc4	on
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
zaostřování	zaostřování	k1gNnSc1	zaostřování
oční	oční	k2eAgFnSc2d1	oční
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
odhadnout	odhadnout	k5eAaPmF	odhadnout
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
jednooký	jednooký	k2eAgMnSc1d1	jednooký
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
živočich	živočich	k1gMnSc1	živočich
oči	oko	k1gNnPc4	oko
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
tím	ten	k3xDgNnSc7	ten
široký	široký	k2eAgInSc4d1	široký
přehled	přehled	k1gInSc4	přehled
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
uvidět	uvidět	k5eAaPmF	uvidět
včas	včas	k6eAd1	včas
hrozící	hrozící	k2eAgNnSc1d1	hrozící
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
plochý	plochý	k2eAgMnSc1d1	plochý
(	(	kIx(	(
<g/>
kopytníci	kopytník	k1gMnPc1	kopytník
<g/>
,	,	kIx,	,
kytovci	kytovec	k1gMnPc1	kytovec
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
poloha	poloha	k1gFnSc1	poloha
očí	oko	k1gNnPc2	oko
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
prostorový	prostorový	k2eAgInSc1d1	prostorový
přehled	přehled	k1gInSc1	přehled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užší	úzký	k2eAgInSc1d2	užší
rozhled	rozhled	k1gInSc1	rozhled
(	(	kIx(	(
<g/>
šelmy	šelma	k1gFnPc1	šelma
<g/>
,	,	kIx,	,
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
dravci	dravec	k1gMnPc1	dravec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
Lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
Slepota	slepota	k1gFnSc1	slepota
Slepá	slepý	k2eAgFnSc1d1	slepá
skvrna	skvrna	k1gFnSc1	skvrna
Smysl	smysl	k1gInSc1	smysl
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
Optický	optický	k2eAgInSc1d1	optický
klam	klam	k1gInSc1	klam
Oční	oční	k2eAgInSc1d1	oční
kontakt	kontakt	k1gInSc4	kontakt
Zrakový	zrakový	k2eAgInSc4d1	zrakový
systém	systém	k1gInSc4	systém
Barvoslepost	barvoslepost	k1gFnSc1	barvoslepost
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zrak	zrak	k1gInSc4	zrak
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc1	Wikidat
<g/>
:	:	kIx,	:
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Vision	vision	k1gInSc1	vision
Wikidata	Wikidat	k1gMnSc2	Wikidat
<g/>
:	:	kIx,	:
Visual	Visual	k1gMnSc1	Visual
perception	perception	k1gInSc4	perception
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zrak	zrak	k1gInSc1	zrak
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
