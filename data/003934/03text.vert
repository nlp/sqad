<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
Zvolen	Zvolen	k1gInSc1	Zvolen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
oponenty	oponent	k1gMnPc4	oponent
často	často	k6eAd1	často
označovaný	označovaný	k2eAgMnSc1d1	označovaný
za	za	k7c4	za
autoritářského	autoritářský	k2eAgMnSc4d1	autoritářský
politika	politik	k1gMnSc4	politik
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
vedl	vést	k5eAaImAgInS	vést
Hnutí	hnutí	k1gNnPc4	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1962	[number]	k4	1962
až	až	k9	až
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Komenského	Komenského	k2eAgFnSc6d1	Komenského
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
ho	on	k3xPp3gInSc4	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nestraníka	nestraník	k1gMnSc4	nestraník
a	a	k8xC	a
neznámého	známý	k2eNgMnSc4d1	neznámý
právníka	právník	k1gMnSc4	právník
<g/>
,	,	kIx,	,
nominovalo	nominovat	k5eAaBmAgNnS	nominovat
hnutí	hnutí	k1gNnSc1	hnutí
Verejnosť	Verejnosť	k1gFnSc2	Verejnosť
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
(	(	kIx(	(
<g/>
VPN	VPN	kA	VPN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
obdoba	obdoba	k1gFnSc1	obdoba
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
koalici	koalice	k1gFnSc4	koalice
VPN	VPN	kA	VPN
s	s	k7c7	s
KDH	KDH	kA	KDH
a	a	k8xC	a
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
až	až	k9	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
VPN	VPN	kA	VPN
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Mečiar	Mečiar	k1gMnSc1	Mečiar
následovně	následovně	k6eAd1	následovně
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ve	v	k7c6	v
VPN	VPN	kA	VPN
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
symptizanty	symptizant	k1gMnPc7	symptizant
křídlo	křídlo	k1gNnSc4	křídlo
VPN-Za	VPN-Za	k1gFnSc1	VPN-Za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
předchůkyní	předchůkyně	k1gFnSc7	předchůkyně
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
Hnutie	Hnutie	k1gFnSc2	Hnutie
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
HZDS	HZDS	kA	HZDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
post	post	k1gInSc4	post
člena	člen	k1gMnSc2	člen
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
VPN	VPN	kA	VPN
do	do	k7c2	do
slovenské	slovenský	k2eAgFnSc2d1	slovenská
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Středoslovenský	středoslovenský	k2eAgInSc1d1	středoslovenský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozkladu	rozklad	k1gInSc6	rozklad
VPN	VPN	kA	VPN
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
HZDS	HZDS	kA	HZDS
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
HZDS	HZDS	kA	HZDS
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
<g/>
,	,	kIx,	,
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
stal	stát	k5eAaPmAgInS	stát
znovu	znovu	k6eAd1	znovu
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
premiérem	premiér	k1gMnSc7	premiér
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úřad	úřad	k1gInSc1	úřad
zastával	zastávat	k5eAaImAgInS	zastávat
až	až	k6eAd1	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
odvolána	odvolat	k5eAaPmNgFnS	odvolat
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
kabinet	kabinet	k1gInSc1	kabinet
Jozefa	Jozef	k1gMnSc2	Jozef
Moravčíka	Moravčík	k1gMnSc2	Moravčík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
Mečiar	Mečiar	k1gMnSc1	Mečiar
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
unesen	unést	k5eAaPmNgMnS	unést
syn	syn	k1gMnSc1	syn
slovenského	slovenský	k2eAgMnSc2d1	slovenský
prezidenta	prezident	k1gMnSc2	prezident
Michal	Michal	k1gMnSc1	Michal
Kováč	Kováč	k1gMnSc1	Kováč
ml.	ml.	kA	ml.
do	do	k7c2	do
Rakouského	rakouský	k2eAgInSc2d1	rakouský
Hainburgu	Hainburg	k1gInSc2	Hainburg
a	a	k8xC	a
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
podezřelé	podezřelý	k2eAgFnPc4d1	podezřelá
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
slovenská	slovenský	k2eAgFnSc1d1	slovenská
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
SIS	SIS	kA	SIS
<g/>
.	.	kIx.	.
</s>
<s>
Spojka	spojka	k1gFnSc1	spojka
korunního	korunní	k2eAgMnSc2d1	korunní
svědka	svědek	k1gMnSc2	svědek
únosu	únos	k1gInSc2	únos
Róbert	Róbert	k1gMnSc1	Róbert
Remiáš	Remiáš	k1gMnSc1	Remiáš
následně	následně	k6eAd1	následně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1996	[number]	k4	1996
uhořel	uhořet	k5eAaPmAgMnS	uhořet
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
auta	auto	k1gNnSc2	auto
v	v	k7c6	v
Karlovej	Karlovej	k1gFnSc6	Karlovej
Vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Pachatele	pachatel	k1gMnSc2	pachatel
Mečiar	Mečiar	k1gMnSc1	Mečiar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
amnestoval	amnestovat	k5eAaBmAgMnS	amnestovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
protiústavnímu	protiústavní	k2eAgNnSc3d1	protiústavní
odvolání	odvolání	k1gNnSc3	odvolání
poslance	poslanec	k1gMnSc2	poslanec
Františka	František	k1gMnSc2	František
Gauliedera	Gaulieder	k1gMnSc2	Gaulieder
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
pomalu	pomalu	k6eAd1	pomalu
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
nepočítalo	počítat	k5eNaImAgNnS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
SR	SR	kA	SR
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
ani	ani	k8xC	ani
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Mečiar	Mečiar	k1gMnSc1	Mečiar
byl	být	k5eAaImAgMnS	být
západními	západní	k2eAgFnPc7d1	západní
médii	médium	k1gNnPc7	médium
označovan	označovan	k1gMnSc1	označovan
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
HZDS	HZDS	kA	HZDS
uspělo	uspět	k5eAaPmAgNnS	uspět
i	i	k8xC	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
sestavit	sestavit	k5eAaPmF	sestavit
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
naopak	naopak	k6eAd1	naopak
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc2	Dzurind
z	z	k7c2	z
opoziční	opoziční	k2eAgFnSc2d1	opoziční
strany	strana	k1gFnSc2	strana
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
a	a	k8xC	a
kresťanská	kresťanský	k2eAgFnSc1d1	kresťanská
únia	únia	k1gFnSc1	únia
(	(	kIx(	(
<g/>
SDKÚ	SDKÚ	kA	SDKÚ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vzdal	vzdát	k5eAaPmAgMnS	vzdát
i	i	k8xC	i
funkce	funkce	k1gFnSc1	funkce
poslance	poslanec	k1gMnSc2	poslanec
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
Ivan	Ivan	k1gMnSc1	Ivan
Lexa	Lexa	k1gMnSc1	Lexa
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
HZDS	HZDS	kA	HZDS
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
uspěla	uspět	k5eAaPmAgFnS	uspět
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opakovala	opakovat	k5eAaImAgFnS	opakovat
se	se	k3xPyFc4	se
předešlá	předešlý	k2eAgFnSc1d1	předešlá
situace	situace	k1gFnSc1	situace
<g/>
;	;	kIx,	;
SDKÚ	SDKÚ	kA	SDKÚ
opět	opět	k6eAd1	opět
sestavila	sestavit	k5eAaPmAgFnS	sestavit
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
Mečiar	Mečiar	k1gMnSc1	Mečiar
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
HZDS	HZDS	kA	HZDS
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
ĽS-HZDS	ĽS-HZDS	k1gFnSc6	ĽS-HZDS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
Výboru	výbor	k1gInSc2	výbor
NR	NR	kA	NR
SR	SR	kA	SR
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
národnosti	národnost	k1gFnPc4	národnost
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
předsedu	předseda	k1gMnSc4	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006-2010	[number]	k4	2006-2010
byla	být	k5eAaImAgFnS	být
ĽS-HZDS	ĽS-HZDS	k1gFnSc1	ĽS-HZDS
součástí	součást	k1gFnSc7	součást
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgFnSc2d1	vedená
premiérem	premiér	k1gMnSc7	premiér
Robertem	Robert	k1gMnSc7	Robert
Ficem	Fic	k1gMnSc7	Fic
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ĽS-HZDS	ĽS-HZDS	k1gFnSc1	ĽS-HZDS
nezískala	získat	k5eNaPmAgFnS	získat
5	[number]	k4	5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
žádní	žádný	k3yNgMnPc1	žádný
její	její	k3xOp3gMnPc1	její
kandidáti	kandidát	k1gMnPc1	kandidát
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
strana	strana	k1gFnSc1	strana
spadla	spadnout	k5eAaPmAgFnS	spadnout
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgNnPc4	tři
procenta	procento	k1gNnPc4	procento
a	a	k8xC	a
nezískala	získat	k5eNaPmAgFnS	získat
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
výsledku	výsledek	k1gInSc2	výsledek
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
svůj	svůj	k3xOyFgInSc4	svůj
konec	konec	k1gInSc4	konec
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jemu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
celému	celý	k2eAgNnSc3d1	celé
vedení	vedení	k1gNnSc3	vedení
HZDS	HZDS	kA	HZDS
podle	podle	k7c2	podle
stanov	stanova	k1gFnPc2	stanova
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
funkce	funkce	k1gFnPc1	funkce
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
konaném	konaný	k2eAgInSc6d1	konaný
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
politik	politik	k1gMnSc1	politik
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
funkci	funkce	k1gFnSc6	funkce
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
nekandidoval	kandidovat	k5eNaImAgInS	kandidovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
Mečiarovi	Mečiar	k1gMnSc6	Mečiar
tak	tak	k9	tak
její	její	k3xOp3gFnSc1	její
řízení	řízení	k1gNnSc2	řízení
převzalo	převzít	k5eAaPmAgNnS	převzít
grémium	grémium	k1gNnSc1	grémium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
strana	strana	k1gFnSc1	strana
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
