<s>
Giv	Giv	k?	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Chacir	Chacir	k1gInSc1	Chacir
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ג	ג	k?	ג
ח	ח	k?	ח
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kopcovitý	kopcovitý	k2eAgInSc1d1	kopcovitý
pás	pás	k1gInSc1	pás
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
195	[number]	k4	195
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Galileji	Galilea	k1gFnSc6	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
cca	cca	kA	cca
8	[number]	k4	8
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Nazaret	Nazaret	k1gInSc1	Nazaret
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
nevýrazného	výrazný	k2eNgInSc2d1	nevýrazný
<g/>
,	,	kIx,	,
severojižně	severojižně	k6eAd1	severojižně
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
kopcovitého	kopcovitý	k2eAgInSc2d1	kopcovitý
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
3	[number]	k4	3
kilometrů	kilometr	k1gInPc2	kilometr
probíhá	probíhat	k5eAaImIp3nS	probíhat
mírně	mírně	k6eAd1	mírně
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
<g/>
,	,	kIx,	,
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívanou	využívaný	k2eAgFnSc7d1	využívaná
krajinou	krajina	k1gFnSc7	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Zarzir	Zarzira	k1gFnPc2	Zarzira
<g/>
,	,	kIx,	,
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
mírně	mírně	k6eAd1	mírně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	ten	k3xDgFnSc4	ten
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dílčích	dílčí	k2eAgInPc2d1	dílčí
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pahorku	pahorek	k1gInSc2	pahorek
Tel	tel	kA	tel
Chacir	Chacir	k1gMnSc1	Chacir
(	(	kIx(	(
<g/>
180	[number]	k4	180
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Manšija	Manšijus	k1gMnSc2	Manšijus
Zabda	Zabd	k1gMnSc2	Zabd
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Jizre	Jizr	k1gInSc5	Jizr
<g/>
'	'	kIx"	'
<g/>
elského	elský	k2eAgNnSc2d1	elský
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
podél	podél	k7c2	podél
východního	východní	k2eAgInSc2d1	východní
okraje	okraj	k1gInSc2	okraj
hřbetu	hřbet	k1gInSc2	hřbet
směřuje	směřovat	k5eAaImIp3nS	směřovat
vádí	vádí	k1gNnSc4	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Šimron	Šimron	k1gMnSc1	Šimron
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
úpatí	úpatí	k1gNnSc2	úpatí
pásma	pásmo	k1gNnSc2	pásmo
teče	téct	k5eAaImIp3nS	téct
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k8xS	jako
Nachal	Nachal	k1gMnSc1	Nachal
Šimron	Šimron	k1gMnSc1	Šimron
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Nahalal	Nahalal	k1gMnSc1	Nahalal
<g/>
.	.	kIx.	.
</s>
