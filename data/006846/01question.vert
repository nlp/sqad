<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
kopcovitý	kopcovitý	k2eAgInSc1d1	kopcovitý
pás	pás	k1gInSc1	pás
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
195	[number]	k4	195
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Galileji	Galilea	k1gFnSc6	Galilea
<g/>
?	?	kIx.	?
</s>
