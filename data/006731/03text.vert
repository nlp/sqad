<s>
Realismus	realismus	k1gInSc1	realismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
střídá	střídat	k5eAaImIp3nS	střídat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
romantismus	romantismus	k1gInSc1	romantismus
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
němu	on	k3xPp3gInSc3	on
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
rozumovou	rozumový	k2eAgFnSc4d1	rozumová
a	a	k8xC	a
racionální	racionální	k2eAgFnSc4d1	racionální
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předstupeň	předstupeň	k1gInSc4	předstupeň
realismu	realismus	k1gInSc2	realismus
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
renesance	renesance	k1gFnSc1	renesance
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
do	do	k7c2	do
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Realismus	realismus	k1gInSc1	realismus
byl	být	k5eAaImAgInS	být
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
popisovalo	popisovat	k5eAaImAgNnS	popisovat
život	život	k1gInSc4	život
hlavně	hlavně	k6eAd1	hlavně
bohatých	bohatý	k2eAgMnPc2d1	bohatý
a	a	k8xC	a
spokojených	spokojený	k2eAgMnPc2d1	spokojený
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
či	či	k8xC	či
historických	historický	k2eAgFnPc2d1	historická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Realismus	realismus	k1gInSc1	realismus
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
rozvoje	rozvoj	k1gInSc2	rozvoj
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
chápana	chápan	k1gMnSc2	chápan
jako	jako	k9	jako
analýza	analýza	k1gFnSc1	analýza
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
realismus	realismus	k1gInSc1	realismus
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
realis	realis	k1gFnSc3	realis
=	=	kIx~	=
věčný	věčný	k2eAgInSc4d1	věčný
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgInSc4d1	skutečný
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
žánry	žánr	k1gInPc7	žánr
realismu	realismus	k1gInSc2	realismus
jsou	být	k5eAaImIp3nP	být
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
a	a	k8xC	a
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelé	spisovatel	k1gMnPc1	spisovatel
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
komplexně	komplexně	k6eAd1	komplexně
<g/>
,	,	kIx,	,
pravdivě	pravdivě	k6eAd1	pravdivě
a	a	k8xC	a
věrné	věrný	k2eAgInPc1d1	věrný
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
všestranné	všestranný	k2eAgNnSc4d1	všestranné
studium	studium	k1gNnSc4	studium
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nitra	nitro	k1gNnSc2	nitro
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
vyzobrazení	vyzobrazení	k1gNnSc1	vyzobrazení
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uplatněna	uplatněn	k2eAgFnSc1d1	uplatněna
typizace	typizace	k1gFnSc1	typizace
–	–	k?	–
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
postihnout	postihnout	k5eAaPmF	postihnout
obecné	obecný	k2eAgNnSc1d1	obecné
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
příznačné	příznačný	k2eAgInPc1d1	příznačný
rysy	rys	k1gInPc1	rys
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
jedinečném	jedinečný	k2eAgMnSc6d1	jedinečný
hrdinovi	hrdina	k1gMnSc6	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
v	v	k7c6	v
realistickém	realistický	k2eAgNnSc6d1	realistické
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobově	dobově	k6eAd1	dobově
i	i	k9	i
společensky	společensky	k6eAd1	společensky
podmíněný	podmíněný	k2eAgInSc1d1	podmíněný
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
průměrného	průměrný	k2eAgInSc2d1	průměrný
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
velké	velký	k2eAgNnSc4d1	velké
společenské	společenský	k2eAgNnSc4d1	společenské
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
neužívá	užívat	k5eNaImIp3nS	užívat
autostylizace	autostylizace	k1gFnSc1	autostylizace
–	–	k?	–
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
přímo	přímo	k6eAd1	přímo
účasten	účasten	k2eAgMnSc1d1	účasten
<g/>
.	.	kIx.	.
</s>
<s>
Autorova	autorův	k2eAgFnSc1d1	autorova
objektivita	objektivita	k1gFnSc1	objektivita
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
podmíněna	podmíněn	k2eAgFnSc1d1	podmíněna
–	–	k?	–
vybírá	vybírat	k5eAaImIp3nS	vybírat
fakta	faktum	k1gNnPc4	faktum
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
prostředí	prostředí	k1gNnSc1	prostředí
→	→	k?	→
není	být	k5eNaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
objektivní	objektivní	k2eAgFnSc2d1	objektivní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vybírá	vybírat	k5eAaImIp3nS	vybírat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
objektivní	objektivní	k2eAgInSc4d1	objektivní
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Realističtí	realistický	k2eAgMnPc1d1	realistický
spisovatelé	spisovatel	k1gMnPc1	spisovatel
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
spíše	spíše	k9	spíše
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umí	umět	k5eAaImIp3nS	umět
zachytit	zachytit	k5eAaPmF	zachytit
jen	jen	k9	jen
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
Kritický	kritický	k2eAgInSc1d1	kritický
realismus	realismus	k1gInSc1	realismus
je	být	k5eAaImIp3nS	být
kvalitativně	kvalitativně	k6eAd1	kvalitativně
vyšší	vysoký	k2eAgInSc1d2	vyšší
stupeň	stupeň	k1gInSc1	stupeň
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
mnohem	mnohem	k6eAd1	mnohem
otevřenější	otevřený	k2eAgInSc4d2	otevřenější
<g/>
,	,	kIx,	,
nesmiřitelnější	smiřitelný	k2eNgInSc4d2	nesmiřitelnější
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
při	při	k7c6	při
zvětšování	zvětšování	k1gNnSc6	zvětšování
sociálních	sociální	k2eAgInPc2d1	sociální
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
důsledku	důsledek	k1gInSc6	důsledek
vzniká	vznikat	k5eAaImIp3nS	vznikat
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
sociálních	sociální	k2eAgInPc2d1	sociální
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
)	)	kIx)	)
Májovci	májovec	k1gMnPc1	májovec
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Hálek	hálka	k1gFnPc2	hálka
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
Karolina	Karolinum	k1gNnSc2	Karolinum
Světlá	světlat	k5eAaImIp3nS	světlat
Ruchovci	ruchovec	k1gMnSc3	ruchovec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
Lumírovci	lumírovec	k1gMnPc7	lumírovec
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
Drama	drama	k1gNnSc1	drama
Bratři	bratr	k1gMnPc1	bratr
Mrštíkové	Mrštíková	k1gFnSc2	Mrštíková
Ladislav	Ladislav	k1gMnSc1	Ladislav
Stroupežnický	Stroupežnický	k2eAgMnSc1d1	Stroupežnický
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
)	)	kIx)	)
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g />
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
Ivan	Ivan	k1gMnSc1	Ivan
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
Ivan	Ivan	k1gMnSc1	Ivan
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Gončarov	Gončarov	k1gInSc4	Gončarov
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Ostrovskij	Ostrovskij	k1gMnSc1	Ostrovskij
Maxim	Maxim	k1gMnSc1	Maxim
Gorkij	Gorkij	k1gMnSc1	Gorkij
-	-	kIx~	-
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
poddruhu	poddruh	k1gInSc2	poddruh
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
Božena	Božena	k1gFnSc1	Božena
Slančíková-Timrava	Slančíková-Timrava	k1gFnSc1	Slančíková-Timrava
Jozef	Jozef	k1gMnSc1	Jozef
Gregor	Gregor	k1gMnSc1	Gregor
Tajovský	Tajovský	k1gMnSc1	Tajovský
Janko	Janko	k1gMnSc1	Janko
Jesenský	Jesenský	k1gMnSc1	Jesenský
Henryk	Henryk	k1gMnSc1	Henryk
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
Bolesław	Bolesław	k1gMnSc1	Bolesław
Prus	Prus	k1gMnSc1	Prus
Gottfried	Gottfried	k1gMnSc1	Gottfried
Keller	Keller	k1gMnSc1	Keller
Theodor	Theodor	k1gMnSc1	Theodor
Fontane	Fontan	k1gInSc5	Fontan
Gerhart	Gerharta	k1gFnPc2	Gerharta
Hauptmann	Hauptmann	k1gMnSc1	Hauptmann
Stendhal	Stendhal	k1gMnSc1	Stendhal
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
<g />
.	.	kIx.	.
</s>
<s>
romatik	romatik	k1gMnSc1	romatik
<g/>
)	)	kIx)	)
Alexandre	Alexandr	k1gInSc5	Alexandr
Dumas	Dumas	k1gMnSc1	Dumas
mladší	mladý	k2eAgFnSc2d2	mladší
Honoré	Honorý	k2eAgFnSc2d1	Honorý
de	de	k?	de
Balzac	Balzac	k1gMnSc1	Balzac
Gustave	Gustav	k1gMnSc5	Gustav
Flaubert	Flaubert	k1gMnSc1	Flaubert
Edmond	Edmond	k1gMnSc1	Edmond
de	de	k?	de
Goncourt	Goncourt	k1gInSc1	Goncourt
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
Émile	Émile	k1gFnSc2	Émile
Zola	Zola	k1gMnSc1	Zola
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Guy	Guy	k1gFnSc7	Guy
de	de	k?	de
Maupassantem	Maupassant	k1gInSc7	Maupassant
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
poddruh	poddruh	k1gInSc1	poddruh
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
naturalismus	naturalismus	k1gInSc1	naturalismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
popisování	popisování	k1gNnSc6	popisování
reality	realita	k1gFnSc2	realita
zacházel	zacházet	k5eAaImAgInS	zacházet
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
větších	veliký	k2eAgInPc2d2	veliký
detailů	detail	k1gInPc2	detail
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
William	William	k1gInSc4	William
Makepeace	Makepeace	k1gFnPc1	Makepeace
Thackeray	Thackeraa	k1gFnSc2	Thackeraa
Thomas	Thomas	k1gMnSc1	Thomas
Hardy	Harda	k1gFnSc2	Harda
Jane	Jan	k1gMnSc5	Jan
Austenová	Austenový	k2eAgFnSc1d1	Austenová
-	-	kIx~	-
založila	založit	k5eAaPmAgFnS	založit
poddruh	poddruh	k1gInSc4	poddruh
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
rodinný	rodinný	k2eAgInSc4d1	rodinný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
představitelky	představitelka	k1gFnPc4	představitelka
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
George	George	k1gFnPc7	George
Eliot	Eliot	k1gMnSc1	Eliot
George	Georg	k1gFnSc2	Georg
Eliot	Eliot	k1gMnSc1	Eliot
Henrik	Henrik	k1gMnSc1	Henrik
Ibsen	Ibsen	k1gMnSc1	Ibsen
August	August	k1gMnSc1	August
Strindberg	Strindberg	k1gMnSc1	Strindberg
Bjø	Bjø	k1gMnSc1	Bjø
Bjø	Bjø	k1gMnSc1	Bjø
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
romantik	romantik	k1gMnSc1	romantik
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Twain	Twain	k1gMnSc1	Twain
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
Kálmán	Kálmán	k2eAgInSc4d1	Kálmán
Mikszáth	Mikszáth	k1gInSc4	Mikszáth
Imre	Imr	k1gFnSc2	Imr
Madách	Mady	k1gFnPc6	Mady
</s>
