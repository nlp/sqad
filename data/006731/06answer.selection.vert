<s>
Realismus	realismus	k1gInSc1	realismus
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
střídá	střídat	k5eAaImIp3nS	střídat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
romantismus	romantismus	k1gInSc1	romantismus
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
němu	on	k3xPp3gInSc3	on
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
rozumovou	rozumový	k2eAgFnSc4d1	rozumová
a	a	k8xC	a
racionální	racionální	k2eAgFnSc4d1	racionální
stránku	stránka	k1gFnSc4	stránka
<g/>
.	.	kIx.	.
</s>
