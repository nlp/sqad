<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
eucharistická	eucharistický	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
liturgii	liturgie	k1gFnSc6
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
liturgické	liturgický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
po	po	k7c6
druhém	druhý	k4xOgInSc6
vatikánském	vatikánský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
jako	jako	k9
alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
římský	římský	k2eAgInSc4d1
kánon	kánon	k1gInSc4
<g/>
.	.	kIx.
</s>