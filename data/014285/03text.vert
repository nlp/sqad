<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
eucharistická	eucharistický	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
eucharistická	eucharistický	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
liturgii	liturgie	k1gFnSc6
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
liturgické	liturgický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
po	po	k7c6
druhém	druhý	k4xOgInSc6
vatikánském	vatikánský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
jako	jako	k9
alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
římský	římský	k2eAgInSc4d1
kánon	kánon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
zkrácením	zkrácení	k1gNnSc7
a	a	k8xC
úpravou	úprava	k1gFnSc7
koptské	koptský	k2eAgFnSc2d1
anafory	anafora	k1gFnSc2
sv.	sv.	kA
Basila	Basil	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
z	z	k7c2
přelomu	přelom	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
zejména	zejména	k9
pro	pro	k7c4
neděle	neděle	k1gFnPc4
v	v	k7c6
liturgickém	liturgický	k2eAgNnSc6d1
mezidobí	mezidobí	k1gNnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
prefaci	preface	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
nelze	lze	k6eNd1
zaměnit	zaměnit	k5eAaPmF
za	za	k7c4
jinou	jiný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
eucharistická	eucharistický	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
