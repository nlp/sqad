<s>
Potkan	potkan	k1gMnSc1	potkan
nebo	nebo	k8xC	nebo
potkan	potkan	k1gMnSc1	potkan
obecný	obecný	k2eAgMnSc1d1	obecný
a	a	k8xC	a
potkan	potkan	k1gMnSc1	potkan
laboratorní	laboratorní	k2eAgMnSc1d1	laboratorní
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
norvegicus	norvegicus	k1gMnSc1	norvegicus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
myšovitý	myšovitý	k2eAgMnSc1d1	myšovitý
hlodavec	hlodavec	k1gMnSc1	hlodavec
rodu	rod	k1gInSc2	rod
Rattus	Rattus	k1gMnSc1	Rattus
často	často	k6eAd1	často
zaměňovaný	zaměňovaný	k2eAgMnSc1d1	zaměňovaný
s	s	k7c7	s
krysou	krysa	k1gFnSc7	krysa
obecnou	obecná	k1gFnSc7	obecná
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
rattus	rattus	k1gMnSc1	rattus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
synantropní	synantropní	k2eAgInSc1d1	synantropní
druh	druh	k1gInSc1	druh
žije	žít	k5eAaImIp3nS	žít
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
škodí	škodit	k5eAaImIp3nS	škodit
požíráním	požírání	k1gNnSc7	požírání
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
napadáním	napadání	k1gNnSc7	napadání
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k9	i
přenášením	přenášení	k1gNnSc7	přenášení
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
parazitů	parazit	k1gMnPc2	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Potkani	potkan	k1gMnPc1	potkan
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barevných	barevný	k2eAgFnPc6d1	barevná
mutacích	mutace	k1gFnPc6	mutace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
bezsrstí	bezsrstý	k2eAgMnPc1d1	bezsrstý
<g/>
,	,	kIx,	,
se	s	k7c7	s
změněnou	změněný	k2eAgFnSc7d1	změněná
strukturou	struktura	k1gFnSc7	struktura
srsti	srst	k1gFnSc2	srst
nebo	nebo	k8xC	nebo
se	s	k7c7	s
zakrněným	zakrněný	k2eAgInSc7d1	zakrněný
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
a	a	k8xC	a
složí	složit	k5eAaPmIp3nP	složit
i	i	k9	i
k	k	k7c3	k
laboratorním	laboratorní	k2eAgInPc3d1	laboratorní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
dožívá	dožívat	k5eAaImIp3nS	dožívat
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
až	až	k6eAd1	až
3-4	[number]	k4	3-4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
potkan	potkan	k1gMnSc1	potkan
se	se	k3xPyFc4	se
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
z	z	k7c2	z
maďarského	maďarský	k2eAgInSc2d1	maďarský
patkány	patkat	k5eAaPmNgInP	patkat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
ponticana	ponticana	k1gFnSc1	ponticana
"	"	kIx"	"
<g/>
pontská	pontský	k2eAgFnSc1d1	pontský
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
myš	myš	k1gFnSc1	myš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
potkanovi	potkan	k1gMnSc6	potkan
dříve	dříve	k6eAd2	dříve
říkalo	říkat	k5eAaImAgNnS	říkat
německá	německý	k2eAgFnSc1d1	německá
myš	myš	k1gFnSc1	myš
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
myšovitý	myšovitý	k2eAgMnSc1d1	myšovitý
hlodavec	hlodavec	k1gMnSc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
160	[number]	k4	160
<g/>
-	-	kIx~	-
<g/>
270	[number]	k4	270
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc4d2	kratší
než	než	k8xS	než
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
130	[number]	k4	130
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
zadních	zadní	k2eAgFnPc2d1	zadní
tlapek	tlapka	k1gFnPc2	tlapka
činí	činit	k5eAaImIp3nS	činit
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
mm	mm	kA	mm
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc4	ucho
mají	mít	k5eAaImIp3nP	mít
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
140-500	[number]	k4	140-500
g	g	kA	g
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
až	až	k6eAd1	až
do	do	k7c2	do
900	[number]	k4	900
g.	g.	k?	g.
Samci	samec	k1gInPc7	samec
jsou	být	k5eAaImIp3nP	být
robustnější	robustní	k2eAgInPc1d2	robustnější
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
až	až	k9	až
o	o	k7c4	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
menší	malý	k2eAgInPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
výskyt	výskyt	k1gInSc1	výskyt
abnormálně	abnormálně	k6eAd1	abnormálně
velkého	velký	k2eAgMnSc2d1	velký
potkana	potkan	k1gMnSc2	potkan
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
39	[number]	k4	39
cm	cm	kA	cm
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
mohutnější	mohutný	k2eAgFnSc1d2	mohutnější
než	než	k8xS	než
krysa	krysa	k1gFnSc1	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zbarvení	zbarvení	k1gNnSc1	zbarvení
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
krysy	krysa	k1gFnPc1	krysa
stejně	stejně	k6eAd1	stejně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
jako	jako	k8xC	jako
potkani	potkan	k1gMnPc1	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
následující	následující	k2eAgInPc4d1	následující
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
Ocas	ocas	k1gInSc4	ocas
<g/>
:	:	kIx,	:
potkan	potkan	k1gMnSc1	potkan
má	mít	k5eAaImIp3nS	mít
ocas	ocas	k1gInSc4	ocas
s	s	k7c7	s
šupinatým	šupinatý	k2eAgInSc7d1	šupinatý
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Shora	shora	k6eAd1	shora
bývá	bývat	k5eAaImIp3nS	bývat
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc1d2	kratší
než	než	k8xS	než
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
krysa	krysa	k1gFnSc1	krysa
má	mít	k5eAaImIp3nS	mít
ocas	ocas	k1gInSc1	ocas
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
tělo	tělo	k1gNnSc1	tělo
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
štíhlejší	štíhlý	k2eAgInSc1d2	štíhlejší
a	a	k8xC	a
jednobarevný	jednobarevný	k2eAgInSc1d1	jednobarevný
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
<g/>
:	:	kIx,	:
potkan	potkan	k1gMnSc1	potkan
má	mít	k5eAaImIp3nS	mít
zřetelně	zřetelně	k6eAd1	zřetelně
menší	malý	k2eAgNnPc1d2	menší
uši	ucho	k1gNnPc1	ucho
které	který	k3yQgFnPc4	který
přehnuté	přehnutý	k2eAgFnPc4d1	přehnutá
dopředu	dopředu	k6eAd1	dopředu
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
až	až	k9	až
k	k	k7c3	k
oku	oko	k1gNnSc3	oko
<g/>
;	;	kIx,	;
boltce	boltec	k1gInSc2	boltec
jsou	být	k5eAaImIp3nP	být
krátce	krátce	k6eAd1	krátce
osrstěné	osrstěný	k2eAgFnPc1d1	osrstěná
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
<g/>
:	:	kIx,	:
potkani	potkan	k1gMnPc1	potkan
mají	mít	k5eAaImIp3nP	mít
oči	oko	k1gNnPc4	oko
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
krysy	krysa	k1gFnSc2	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
<g/>
:	:	kIx,	:
potkan	potkan	k1gMnSc1	potkan
má	mít	k5eAaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
vpředu	vpředu	k6eAd1	vpředu
zaoblenou	zaoblený	k2eAgFnSc4d1	zaoblená
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
krysa	krysa	k1gFnSc1	krysa
špičatější	špičatý	k2eAgFnSc1d2	špičatější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
lze	lze	k6eAd1	lze
také	také	k9	také
najít	najít	k5eAaPmF	najít
výraznější	výrazný	k2eAgInSc4d2	výraznější
rozdíl	rozdíl	k1gInSc4	rozdíl
-	-	kIx~	-
mozkovna	mozkovna	k1gFnSc1	mozkovna
krysy	krysa	k1gFnSc2	krysa
má	mít	k5eAaImIp3nS	mít
hruškovitý	hruškovitý	k2eAgInSc4d1	hruškovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
potkana	potkan	k1gMnSc2	potkan
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
boční	boční	k2eAgFnPc1d1	boční
strany	strana	k1gFnPc1	strana
mozkovny	mozkovna	k1gFnSc2	mozkovna
rovné	rovný	k2eAgFnPc1d1	rovná
a	a	k8xC	a
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
</s>
<s>
Mléčné	mléčný	k2eAgFnPc1d1	mléčná
bradavky	bradavka	k1gFnPc1	bradavka
<g/>
:	:	kIx,	:
samice	samice	k1gFnPc1	samice
potkanů	potkan	k1gMnPc2	potkan
mají	mít	k5eAaImIp3nP	mít
6	[number]	k4	6
párů	pár	k1gInPc2	pár
mléčných	mléčný	k2eAgFnPc2d1	mléčná
bradavek	bradavka	k1gFnPc2	bradavka
oproti	oproti	k7c3	oproti
5	[number]	k4	5
párům	pár	k1gInPc3	pár
u	u	k7c2	u
krys	krysa	k1gFnPc2	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
potkan	potkan	k1gMnSc1	potkan
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
spíše	spíše	k9	spíše
vlhčí	vlhký	k2eAgFnPc4d2	vlhčí
polohy	poloha	k1gFnPc4	poloha
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
stok	stoka	k1gFnPc2	stoka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
krysa	krysa	k1gFnSc1	krysa
preferuje	preferovat	k5eAaImIp3nS	preferovat
sušší	suchý	k2eAgInPc4d2	sušší
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
chlévy	chlév	k1gInPc1	chlév
a	a	k8xC	a
stáje	stáj	k1gFnSc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
lépe	dobře	k6eAd2	dobře
plave	plavat	k5eAaImIp3nS	plavat
a	a	k8xC	a
potápí	potápět	k5eAaImIp3nS	potápět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
lehčí	lehčit	k5eAaImIp3nS	lehčit
a	a	k8xC	a
mrštnější	mrštný	k2eAgFnSc1d2	mrštnější
krysa	krysa	k1gFnSc1	krysa
naopak	naopak	k6eAd1	naopak
lépe	dobře	k6eAd2	dobře
šplhá	šplhat	k5eAaImIp3nS	šplhat
a	a	k8xC	a
skáče	skákat	k5eAaImIp3nS	skákat
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
je	být	k5eAaImIp3nS	být
kosmopolitní	kosmopolitní	k2eAgInSc4d1	kosmopolitní
a	a	k8xC	a
synantropní	synantropní	k2eAgInSc4d1	synantropní
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
se	se	k3xPyFc4	se
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
bažinatých	bažinatý	k2eAgFnPc2d1	bažinatá
oblastí	oblast	k1gFnPc2	oblast
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
končin	končina	k1gFnPc2	končina
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
krysa	krysa	k1gFnSc1	krysa
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc7d2	veliký
přizpůsobivostí	přizpůsobivost	k1gFnSc7	přizpůsobivost
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
nahradil	nahradit	k5eAaPmAgMnS	nahradit
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
hojnější	hojný	k2eAgInSc1d2	hojnější
výskyt	výskyt	k1gInSc1	výskyt
datuje	datovat	k5eAaImIp3nS	datovat
asi	asi	k9	asi
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
:	:	kIx,	:
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
i	i	k9	i
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
chatách	chata	k1gFnPc6	chata
na	na	k7c6	na
hřebenech	hřeben	k1gInPc6	hřeben
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1400	[number]	k4	1400
m.	m.	k?	m.
Potkani	potkan	k1gMnPc1	potkan
jsou	být	k5eAaImIp3nP	být
čilí	čilý	k2eAgMnPc1d1	čilý
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
a	a	k8xC	a
před	před	k7c7	před
rozedněním	rozednění	k1gNnSc7	rozednění
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
plavou	plavat	k5eAaImIp3nP	plavat
a	a	k8xC	a
šplhají	šplhat	k5eAaImIp3nP	šplhat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
krysy	krysa	k1gFnSc2	krysa
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
neběhají	běhat	k5eNaImIp3nP	běhat
po	po	k7c6	po
trámech	trám	k1gInPc6	trám
v	v	k7c6	v
podkroví	podkroví	k1gNnSc6	podkroví
a	a	k8xC	a
po	po	k7c6	po
střechách	střecha	k1gFnPc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
ostražití	ostražitý	k2eAgMnPc1d1	ostražitý
a	a	k8xC	a
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
dokážou	dokázat	k5eAaPmIp3nP	dokázat
být	být	k5eAaImF	být
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
.	.	kIx.	.
</s>
<s>
Doupě	doupě	k1gNnSc4	doupě
si	se	k3xPyFc3	se
budují	budovat	k5eAaImIp3nP	budovat
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
si	se	k3xPyFc3	se
však	však	k9	však
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
nory	nora	k1gFnPc1	nora
<g/>
.	.	kIx.	.
</s>
<s>
Potkani	potkan	k1gMnPc1	potkan
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
kolem	kolem	k7c2	kolem
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
hierarchickým	hierarchický	k2eAgNnSc7d1	hierarchické
uspořádáním	uspořádání	k1gNnSc7	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Alfa	alfa	k1gFnSc1	alfa
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
zpravidla	zpravidla	k6eAd1	zpravidla
zakladatelé	zakladatel	k1gMnPc1	zakladatel
skupiny	skupina	k1gFnSc2	skupina
-	-	kIx~	-
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
celou	celý	k2eAgFnSc4d1	celá
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
i	i	k9	i
zvířata	zvíře	k1gNnPc1	zvíře
velmi	velmi	k6eAd1	velmi
podřízená	podřízený	k2eAgNnPc1d1	podřízené
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
potomci	potomek	k1gMnPc1	potomek
jiného	jiný	k2eAgInSc2d1	jiný
páru	pár	k1gInSc2	pár
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
staří	starý	k2eAgMnPc1d1	starý
potkani	potkan	k1gMnPc1	potkan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
permanentně	permanentně	k6eAd1	permanentně
terorizováni	terorizovat	k5eAaImNgMnP	terorizovat
zbytkem	zbytek	k1gInSc7	zbytek
kolonie	kolonie	k1gFnSc2	kolonie
(	(	kIx(	(
<g/>
potkan	potkan	k1gMnSc1	potkan
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
silně	silně	k6eAd1	silně
podléhá	podléhat	k5eAaImIp3nS	podléhat
stresu	stres	k1gInSc2	stres
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
"	"	kIx"	"
<g/>
odreagování	odreagování	k1gNnSc1	odreagování
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Těmto	tento	k3xDgMnPc3	tento
jedincům	jedinec	k1gMnPc3	jedinec
je	být	k5eAaImIp3nS	být
také	také	k9	také
upírána	upírán	k2eAgFnSc1d1	upírána
potrava	potrava	k1gFnSc1	potrava
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
"	"	kIx"	"
<g/>
ochutnavače	ochutnavač	k1gMnPc4	ochutnavač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
objevu	objev	k1gInSc6	objev
neznámé	známý	k2eNgFnSc2d1	neznámá
potravy	potrava	k1gFnSc2	potrava
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
okusí	okusit	k5eAaPmIp3nS	okusit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
test	test	k1gInSc1	test
<g/>
"	"	kIx"	"
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
pak	pak	k6eAd1	pak
ochutná	ochutnat	k5eAaPmIp3nS	ochutnat
i	i	k9	i
zbytek	zbytek	k1gInSc4	zbytek
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
aspektů	aspekt	k1gInPc2	aspekt
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
potkan	potkan	k1gMnSc1	potkan
často	často	k6eAd1	často
odolává	odolávat	k5eAaImIp3nS	odolávat
jedům	jed	k1gInPc3	jed
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
návnadám	návnada	k1gFnPc3	návnada
"	"	kIx"	"
<g/>
na	na	k7c4	na
krysy	krysa	k1gFnPc4	krysa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
této	tento	k3xDgFnSc2	tento
kolonie	kolonie	k1gFnSc2	kolonie
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
cca	cca	kA	cca
6	[number]	k4	6
km2	km2	k4	km2
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
na	na	k7c4	na
které	který	k3yIgFnPc4	který
členové	člen	k1gMnPc1	člen
kolonie	kolonie	k1gFnSc2	kolonie
získávají	získávat	k5eAaImIp3nP	získávat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
svoje	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
nejinteligentnějších	inteligentní	k2eAgMnPc2d3	nejinteligentnější
hlodavců	hlodavec	k1gMnPc2	hlodavec
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přizpůsobivosti	přizpůsobivost	k1gFnSc3	přizpůsobivost
svého	své	k1gNnSc2	své
organizmu	organizmus	k1gInSc2	organizmus
a	a	k8xC	a
hierarchii	hierarchie	k1gFnSc4	hierarchie
potkaních	potkaní	k2eAgFnPc2d1	potkaní
kolonií	kolonie	k1gFnPc2	kolonie
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
působení	působení	k1gNnSc4	působení
důmyslných	důmyslný	k2eAgInPc2d1	důmyslný
hubících	hubící	k2eAgInPc2d1	hubící
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgNnPc2	všecek
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
a	a	k8xC	a
větších	veliký	k2eAgNnPc2d2	veliký
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mnohem	mnohem	k6eAd1	mnohem
snadněji	snadno	k6eAd2	snadno
najde	najít	k5eAaPmIp3nS	najít
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
mívá	mívat	k5eAaImIp3nS	mívat
pářicí	pářicí	k2eAgNnSc4d1	pářicí
období	období	k1gNnSc4	období
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
21	[number]	k4	21
až	až	k9	až
24	[number]	k4	24
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
mívá	mívat	k5eAaImIp3nS	mívat
obvykle	obvykle	k6eAd1	obvykle
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgInPc1d1	jiný
údaje	údaj	k1gInPc1	údaj
udávají	udávat	k5eAaImIp3nP	udávat
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
mláďat	mládě	k1gNnPc2	mládě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
vrh	vrh	k1gInSc1	vrh
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
15	[number]	k4	15
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
holá	holý	k2eAgFnSc1d1	holá
a	a	k8xC	a
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
osrstěná	osrstěný	k2eAgNnPc1d1	osrstěné
bývají	bývat	k5eAaImIp3nP	bývat
do	do	k7c2	do
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
otevírají	otevírat	k5eAaImIp3nP	otevírat
během	během	k7c2	během
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
jsou	být	k5eAaImIp3nP	být
kojena	kojen	k2eAgNnPc1d1	kojeno
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgNnPc1d1	samostatné
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
dospělost	dospělost	k1gFnSc4	dospělost
udávají	udávat	k5eAaImIp3nP	udávat
zdroje	zdroj	k1gInPc4	zdroj
různě	různě	k6eAd1	různě
od	od	k7c2	od
1,5	[number]	k4	1,5
do	do	k7c2	do
4	[number]	k4	4
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
dospívání	dospívání	k1gNnSc1	dospívání
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
odchovává	odchovávat	k5eAaImIp3nS	odchovávat
3	[number]	k4	3
vrhy	vrh	k1gInPc7	vrh
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výborných	výborný	k2eAgFnPc6d1	výborná
podmínkách	podmínka	k1gFnPc6	podmínka
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
5	[number]	k4	5
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vrhů	vrh	k1gInPc2	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Zabřeznout	zabřeznout	k5eAaPmF	zabřeznout
může	moct	k5eAaImIp3nS	moct
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
dalších	další	k2eAgMnPc2d1	další
myšovitých	myšovití	k1gMnPc2	myšovití
-	-	kIx~	-
už	už	k9	už
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
potkanů	potkan	k1gMnPc2	potkan
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
březosti	březost	k1gFnSc2	březost
ukončit	ukončit	k5eAaPmF	ukončit
tuto	tento	k3xDgFnSc4	tento
březost	březost	k1gFnSc4	březost
během	během	k7c2	během
nevhodných	vhodný	k2eNgFnPc2d1	nevhodná
podmínek	podmínka	k1gFnPc2	podmínka
nebo	nebo	k8xC	nebo
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
vhodnějším	vhodný	k2eAgInSc7d2	vhodnější
samcem	samec	k1gInSc7	samec
s	s	k7c7	s
lepším	dobrý	k2eAgInSc7d2	lepší
genetickým	genetický	k2eAgInSc7d1	genetický
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Potkan	potkan	k1gMnSc1	potkan
je	být	k5eAaImIp3nS	být
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jídelníčku	jídelníček	k1gInSc2	jídelníček
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
cca	cca	kA	cca
z	z	k7c2	z
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
semen	semeno	k1gNnPc2	semeno
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
obilovin	obilovina	k1gFnPc2	obilovina
či	či	k8xC	či
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
bílkoviny	bílkovina	k1gFnPc4	bílkovina
z	z	k7c2	z
ptačích	ptačí	k2eAgNnPc2d1	ptačí
vajec	vejce	k1gNnPc2	vejce
(	(	kIx(	(
<g/>
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
vejce	vejce	k1gNnPc4	vejce
domácích	domácí	k2eAgMnPc2d1	domácí
i	i	k8xC	i
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
údajů	údaj	k1gInPc2	údaj
však	však	k9	však
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
převažuje	převažovat	k5eAaImIp3nS	převažovat
živočišná	živočišný	k2eAgFnSc1d1	živočišná
potrava	potrava	k1gFnSc1	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
jiné	jiný	k2eAgFnSc2d1	jiná
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
napadnout	napadnout	k5eAaPmF	napadnout
větší	veliký	k2eAgNnPc1d2	veliký
zvířata	zvíře	k1gNnPc1	zvíře
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
králíka	králík	k1gMnSc2	králík
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
asi	asi	k9	asi
desetiny	desetina	k1gFnPc4	desetina
své	svůj	k3xOyFgFnPc4	svůj
tělesné	tělesný	k2eAgFnPc4d1	tělesná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
jako	jako	k8xS	jako
jakýkoli	jakýkoli	k3yIgMnSc1	jakýkoli
jiný	jiný	k2eAgMnSc1d1	jiný
hlodavec	hlodavec	k1gMnSc1	hlodavec
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
obrušovat	obrušovat	k5eAaImF	obrušovat
své	svůj	k3xOyFgInPc4	svůj
hlodavé	hlodavý	k2eAgInPc4d1	hlodavý
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
obrušuje	obrušovat	k5eAaImIp3nS	obrušovat
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
tvrdším	tvrdý	k2eAgInSc6d2	tvrdší
druhu	druh	k1gInSc6	druh
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
větve	větev	k1gFnPc1	větev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
myš	myš	k1gFnSc1	myš
prokousat	prokousat	k5eAaPmF	prokousat
skrz	skrz	k7c4	skrz
beton	beton	k1gInSc4	beton
nebo	nebo	k8xC	nebo
slabší	slabý	k2eAgInPc4d2	slabší
druhy	druh	k1gInPc4	druh
pletiva	pletivo	k1gNnSc2	pletivo
a	a	k8xC	a
kabelů	kabel	k1gInPc2	kabel
(	(	kIx(	(
<g/>
slitiny	slitina	k1gFnSc2	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
měkké	měkký	k2eAgInPc1d1	měkký
kovy	kov	k1gInPc1	kov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenými	přirozený	k2eAgMnPc7d1	přirozený
nepřáteli	nepřítel	k1gMnPc7	nepřítel
potkanů	potkan	k1gMnPc2	potkan
jsou	být	k5eAaImIp3nP	být
lasicovité	lasicovitý	k2eAgFnPc1d1	lasicovitá
šelmy	šelma	k1gFnPc1	šelma
(	(	kIx(	(
<g/>
tchoř	tchoř	k1gMnSc1	tchoř
a	a	k8xC	a
hranostaj	hranostaj	k1gMnSc1	hranostaj
<g/>
)	)	kIx)	)
a	a	k8xC	a
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
potkanů	potkan	k1gMnPc2	potkan
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
potravou	potrava	k1gFnSc7	potrava
menších	malý	k2eAgFnPc2d2	menší
sov	sova	k1gFnPc2	sova
-	-	kIx~	-
sovy	sova	k1gFnSc2	sova
pálené	pálená	k1gFnSc2	pálená
a	a	k8xC	a
kalouse	kalous	k1gMnSc5	kalous
ušatého	ušatý	k2eAgNnSc2d1	ušaté
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
účely	účel	k1gInPc4	účel
díky	díky	k7c3	díky
vhodným	vhodný	k2eAgInPc3d1	vhodný
rozměrům	rozměr	k1gInPc3	rozměr
<g/>
,	,	kIx,	,
snadnému	snadný	k2eAgInSc3d1	snadný
odchovu	odchov	k1gInSc3	odchov
a	a	k8xC	a
své	svůj	k3xOyFgFnSc3	svůj
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
varieta	varieta	k1gFnSc1	varieta
Rattus	Rattus	k1gMnSc1	Rattus
norvergicus	norvergicus	k1gMnSc1	norvergicus
var.	var.	k?	var.
alba	alba	k1gFnSc1	alba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sníženou	snížený	k2eAgFnSc4d1	snížená
schopnost	schopnost	k1gFnSc4	schopnost
přenášení	přenášení	k1gNnSc2	přenášení
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
potkan	potkan	k1gMnSc1	potkan
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
vznikaly	vznikat	k5eAaImAgFnP	vznikat
křížením	křížení	k1gNnSc7	křížení
odlišné	odlišný	k2eAgFnSc2d1	odlišná
barevné	barevný	k2eAgFnSc2d1	barevná
variety	varieta	k1gFnSc2	varieta
a	a	k8xC	a
potkan	potkan	k1gMnSc1	potkan
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
myši	myš	k1gFnSc2	myš
je	být	k5eAaImIp3nS	být
potkan	potkan	k1gMnSc1	potkan
odolnější	odolný	k2eAgMnSc1d2	odolnější
proti	proti	k7c3	proti
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Potkan	potkan	k1gMnSc1	potkan
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
norvegicus	norvegicus	k1gMnSc1	norvegicus
norvegicus	norvegicus	k1gMnSc1	norvegicus
<g/>
)	)	kIx)	)
Nejvíce	nejvíce	k6eAd1	nejvíce
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
poddruh	poddruh	k1gInSc1	poddruh
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
východní	východní	k2eAgMnSc1d1	východní
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
norvegicus	norvegicus	k1gMnSc1	norvegicus
caraco	caraco	k1gMnSc1	caraco
<g/>
)	)	kIx)	)
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poddruh	poddruh	k1gInSc1	poddruh
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
bližší	blízký	k2eAgFnPc4d2	bližší
informace	informace	k1gFnPc4	informace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potkan	potkan	k1gMnSc1	potkan
laboratorní	laboratorní	k2eAgMnSc1d1	laboratorní
(	(	kIx(	(
<g/>
Rattus	Rattus	k1gMnSc1	Rattus
norvergicus	norvergicus	k1gMnSc1	norvergicus
var.	var.	k?	var.
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
Šlechtěná	šlechtěný	k2eAgFnSc1d1	šlechtěná
bílá	bílý	k2eAgFnSc1d1	bílá
varieta	varieta	k1gFnSc1	varieta
potkana	potkan	k1gMnSc2	potkan
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
původně	původně	k6eAd1	původně
k	k	k7c3	k
pokusným	pokusný	k2eAgInPc3d1	pokusný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
varieta	varieta	k1gFnSc1	varieta
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
potkana	potkan	k1gMnSc2	potkan
obecného	obecný	k2eAgMnSc2d1	obecný
nemá	mít	k5eNaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
hostit	hostit	k5eAaImF	hostit
různé	různý	k2eAgFnPc4d1	různá
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
nemoci	nemoc	k1gFnPc4	nemoc
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
ochočit	ochočit	k5eAaPmF	ochočit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
jako	jako	k9	jako
ochočené	ochočený	k2eAgNnSc4d1	ochočené
domácí	domácí	k2eAgNnSc4d1	domácí
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
za	za	k7c7	za
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
var.	var.	k?	var.
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
barevné	barevný	k2eAgFnSc2d1	barevná
variety	varieta	k1gFnSc2	varieta
nebo	nebo	k8xC	nebo
variety	varieta	k1gFnSc2	varieta
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
nebo	nebo	k8xC	nebo
druhem	druh	k1gInSc7	druh
srsti	srst	k1gFnSc2	srst
<g/>
:	:	kIx,	:
Standard	standard	k1gInSc1	standard
-	-	kIx~	-
klasická	klasický	k2eAgFnSc1d1	klasická
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
typ	typ	k1gInSc1	typ
srsti	srst	k1gFnSc2	srst
Dumbo	Dumba	k1gFnSc5	Dumba
-	-	kIx~	-
potkani	potkan	k1gMnPc1	potkan
s	s	k7c7	s
velkýma	velký	k2eAgNnPc7d1	velké
ušima	ucho	k1gNnPc7	ucho
umístěnýma	umístěný	k2eAgFnPc7d1	umístěná
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
plošší	plochý	k2eAgInSc1d2	plošší
Manx	Manx	k1gInSc1	Manx
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Tailless	Tailless	k1gInSc1	Tailless
<g/>
)	)	kIx)	)
-	-	kIx~	-
bezocasí	bezocasý	k2eAgMnPc1d1	bezocasý
potkani	potkan	k1gMnPc1	potkan
Sphynx	Sphynx	k1gInSc4	Sphynx
(	(	kIx(	(
<g/>
Hairless	Hairless	k1gInSc4	Hairless
<g/>
/	/	kIx~	/
<g/>
nunu	nunu	k6eAd1	nunu
<g/>
)	)	kIx)	)
-	-	kIx~	-
bezsrstí	bezsrstý	k2eAgMnPc1d1	bezsrstý
potkani	potkan	k1gMnPc1	potkan
Patchwork	Patchwork	k1gInSc4	Patchwork
-	-	kIx~	-
potkani	potkan	k1gMnPc1	potkan
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
neustále	neustále	k6eAd1	neustále
narůstá	narůstat	k5eAaImIp3nS	narůstat
a	a	k8xC	a
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
srst	srst	k1gFnSc1	srst
drsná	drsný	k2eAgFnSc1d1	drsná
jako	jako	k8xS	jako
vousy	vous	k1gInPc4	vous
<g/>
;	;	kIx,	;
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
Rex	Rex	k1gFnSc1	Rex
-	-	kIx~	-
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
odstávající	odstávající	k2eAgFnSc1d1	odstávající
srst	srst	k1gFnSc1	srst
Fuzz	Fuzza	k1gFnPc2	Fuzza
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
jemná	jemný	k2eAgFnSc1d1	jemná
řídká	řídký	k2eAgFnSc1d1	řídká
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
vousky	vousek	k1gInPc1	vousek
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
a	a	k8xC	a
zakroucené	zakroucený	k2eAgInPc1d1	zakroucený
Dwarf	Dwarf	k1gInSc1	Dwarf
-	-	kIx~	-
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
též	též	k9	též
minipotkan	minipotkan	k1gMnSc1	minipotkan
Odd-eyed	Oddyed	k1gMnSc1	Odd-eyed
-	-	kIx~	-
každé	každý	k3xTgNnSc1	každý
oko	oko	k1gNnSc1	oko
má	mít	k5eAaImIp3nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
Satin	Satina	k1gFnPc2	Satina
-	-	kIx~	-
rovná	rovnat	k5eAaImIp3nS	rovnat
přiléhavá	přiléhavý	k2eAgFnSc1d1	přiléhavá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jemnější	jemný	k2eAgMnSc1d2	jemnější
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
saténový	saténový	k2eAgInSc4d1	saténový
lesk	lesk	k1gInSc4	lesk
Harley	harley	k1gInSc1	harley
-	-	kIx~	-
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
odstávající	odstávající	k2eAgFnSc1d1	odstávající
a	a	k8xC	a
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
množství	množství	k1gNnSc3	množství
podsady	podsada	k1gFnSc2	podsada
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
plyšová	plyšový	k2eAgFnSc1d1	plyšová
<g/>
"	"	kIx"	"
a	a	k8xC	a
načechraná	načechraný	k2eAgFnSc1d1	načechraná
Velveteen	Velveteen	k1gInSc1	Velveteen
-	-	kIx~	-
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
srst	srst	k1gFnSc1	srst
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k8xC	jako
dobrman	dobrman	k1gMnSc1	dobrman
nebo	nebo	k8xC	nebo
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
uhlazená	uhlazený	k2eAgFnSc1d1	uhlazená
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
věkem	věk	k1gInSc7	věk
zvířete	zvíře	k1gNnSc2	zvíře
se	se	k3xPyFc4	se
srst	srst	k1gFnSc1	srst
narovnává	narovnávat	k5eAaImIp3nS	narovnávat
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgFnPc1	čtyři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
barevné	barevný	k2eAgFnPc1d1	barevná
linie	linie	k1gFnPc1	linie
<g/>
:	:	kIx,	:
Americká	americký	k2eAgFnSc1d1	americká
modrá	modrý	k2eAgFnSc1d1	modrá
linie	linie	k1gFnSc1	linie
Ruská	ruský	k2eAgFnSc1d1	ruská
modrá	modrý	k2eAgFnSc1d1	modrá
linie	linie	k1gFnSc1	linie
Šampaň	šampaň	k1gFnSc1	šampaň
linie	linie	k1gFnSc2	linie
Béžová	béžový	k2eAgFnSc1d1	béžová
linie	linie	k1gFnPc1	linie
Při	při	k7c6	při
křížení	křížení	k1gNnSc6	křížení
potkanů	potkan	k1gMnPc2	potkan
s	s	k7c7	s
barvami	barva	k1gFnPc7	barva
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
linií	linie	k1gFnPc2	linie
se	se	k3xPyFc4	se
barvy	barva	k1gFnPc1	barva
potomků	potomek	k1gMnPc2	potomek
ředí	ředit	k5eAaImIp3nP	ředit
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
znehodnocují	znehodnocovat	k5eAaImIp3nP	znehodnocovat
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jejich	jejich	k3xOp3gFnSc4	jejich
barvu	barva	k1gFnSc4	barva
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
linie	linie	k1gFnPc1	linie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
častější	častý	k2eAgMnPc1d2	častější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
alba	alba	k1gFnSc1	alba
(	(	kIx(	(
<g/>
albín	albín	k1gInSc1	albín
<g/>
)	)	kIx)	)
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
srst	srst	k1gFnSc4	srst
a	a	k8xC	a
růžové	růžový	k2eAgNnSc4d1	růžové
oči	oko	k1gNnPc4	oko
aguti	aguť	k1gFnSc2	aguť
-	-	kIx~	-
přírodní	přírodní	k2eAgNnSc1d1	přírodní
divoké	divoký	k2eAgNnSc1d1	divoké
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
hnědošedá	hnědošedý	k2eAgFnSc1d1	hnědošedá
až	až	k6eAd1	až
hnědo	hnědo	k1gNnSc4	hnědo
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
s	s	k7c7	s
tickingem	ticking	k1gInSc7	ticking
<g />
.	.	kIx.	.
</s>
<s>
srsti	srst	k1gFnSc3	srst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
proužky	proužek	k1gInPc1	proužek
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
chlupu	chlup	k1gInSc6	chlup
<g/>
;	;	kIx,	;
břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
stříbřité	stříbřitý	k2eAgNnSc1d1	stříbřité
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
aguti	aguť	k1gFnSc2	aguť
blue	blu	k1gFnSc2	blu
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
agouti	agouť	k1gFnPc1	agouť
<g/>
)	)	kIx)	)
-	-	kIx~	-
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
s	s	k7c7	s
tickingem	ticking	k1gInSc7	ticking
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc4	břicho
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
amber	ambra	k1gFnPc2	ambra
-	-	kIx~	-
světle	světle	k6eAd1	světle
zlatavě	zlatavě	k6eAd1	zlatavě
oranžová	oranžový	k2eAgFnSc1d1	oranžová
se	se	k3xPyFc4	se
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
tickingem	ticking	k1gInSc7	ticking
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
podsada	podsada	k1gFnSc1	podsada
je	být	k5eAaImIp3nS	být
zbarvena	zbarvit	k5eAaPmNgFnS	zbarvit
do	do	k7c2	do
krémova	krémův	k2eAgNnSc2d1	krémovo
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
růžové	růžový	k2eAgFnPc1d1	růžová
beige	beig	k1gFnPc1	beig
-	-	kIx~	-
střední	střední	k2eAgFnSc1d1	střední
béžová	béžový	k2eAgFnSc1d1	béžová
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
od	od	k7c2	od
tmavě	tmavě	k6eAd1	tmavě
červené	červený	k2eAgFnSc2d1	červená
do	do	k7c2	do
černé	černá	k1gFnSc2	černá
black	black	k1gInSc1	black
-	-	kIx~	-
černá	černý	k2eAgFnSc1d1	černá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
oči	oko	k1gNnPc1	oko
black	black	k1gMnSc1	black
<g/>
,	,	kIx,	,
silvered	silvered	k1gMnSc1	silvered
-	-	kIx~	-
prostříbřená	prostříbřený	k2eAgFnSc1d1	prostříbřený
černá	černý	k2eAgFnSc1d1	černá
blue	blue	k1gFnSc1	blue
<g/>
,	,	kIx,	,
powder	powder	k1gInSc1	powder
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
břidlicově	břidlicově	k6eAd1	břidlicově
šedá	šedat	k5eAaImIp3nS	šedat
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
nebo	nebo	k8xC	nebo
ruby	rub	k1gInPc4	rub
<g/>
,	,	kIx,	,
tlapky	tlapka	k1gFnPc4	tlapka
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
pigmentem	pigment	k1gInSc7	pigment
blue	blu	k1gFnSc2	blu
<g/>
,	,	kIx,	,
american	american	k1gMnSc1	american
(	(	kIx(	(
<g/>
english	english	k1gMnSc1	english
<g/>
)	)	kIx)	)
-	-	kIx~	-
světlejší	světlý	k2eAgFnSc1d2	světlejší
lesklá	lesklý	k2eAgFnSc1d1	lesklá
modř	modř	k1gFnSc1	modř
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bříška	bříško	k1gNnSc2	bříško
<g/>
,	,	kIx,	,
černé	černý	k2eAgNnSc4d1	černé
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
tlapky	tlapka	k1gFnPc4	tlapka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
pigmentem	pigment	k1gInSc7	pigment
blue	blue	k1gFnSc1	blue
<g/>
,	,	kIx,	,
russian	russian	k1gInSc1	russian
-	-	kIx~	-
tmavá	tmavý	k2eAgFnSc1d1	tmavá
šedá	šedý	k2eAgFnSc1d1	šedá
barva	barva	k1gFnSc1	barva
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
černé	černá	k1gFnPc1	černá
buff	buffa	k1gFnPc2	buffa
=	=	kIx~	=
bůvolí	bůvolí	k1gNnSc1	bůvolí
-	-	kIx~	-
světlebéžová	světlebéžový	k2eAgFnSc1d1	světlebéžový
<g/>
,	,	kIx,	,
ruby	rub	k1gInPc1	rub
oči	oko	k1gNnPc4	oko
champagner	champagnra	k1gFnPc2	champagnra
(	(	kIx(	(
<g/>
šampaňská	šampaňský	k2eAgFnSc1d1	šampaňská
<g/>
)	)	kIx)	)
-	-	kIx~	-
světlebéžová	světlebéžový	k2eAgFnSc1d1	světlebéžový
s	s	k7c7	s
růžovým	růžový	k2eAgInSc7d1	růžový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
pink	pink	k2eAgNnPc4d1	pink
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tlapky	tlapka	k1gFnPc1	tlapka
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
a	a	k8xC	a
uši	ucho	k1gNnPc1	ucho
se	s	k7c7	s
světlým	světlý	k2eAgInSc7d1	světlý
pigmentem	pigment	k1gInSc7	pigment
chocolate	chocolat	k1gInSc5	chocolat
-	-	kIx~	-
čokoládové	čokoládový	k2eAgNnSc1d1	čokoládové
hnědá	hnědat	k5eAaImIp3nS	hnědat
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
a	a	k8xC	a
tlapky	tlapka	k1gFnPc1	tlapka
s	s	k7c7	s
tmavým	tmavý	k2eAgInSc7d1	tmavý
pigmentem	pigment	k1gInSc7	pigment
cinnamon	cinnamon	k1gInSc1	cinnamon
-	-	kIx~	-
skořicová	skořicová	k1gFnSc1	skořicová
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
nebo	nebo	k8xC	nebo
rubínové	rubínový	k2eAgNnSc1d1	rubínové
cinnamon	cinnamon	k1gNnSc1	cinnamon
pearl	pearla	k1gFnPc2	pearla
(	(	kIx(	(
<g/>
perlová	perlový	k2eAgFnSc1d1	Perlová
skořicová	skořicová	k1gFnSc1	skořicová
<g/>
)	)	kIx)	)
-	-	kIx~	-
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
chlupy	chlup	k1gInPc1	chlup
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarven	k2eAgInPc1d1	zbarven
různými	různý	k2eAgFnPc7d1	různá
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc4	břicho
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedé	šedá	k1gFnPc4	šedá
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
fawn	fawn	k1gNnSc4	fawn
-	-	kIx~	-
zlatě	zlatě	k6eAd1	zlatě
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
ruby	rub	k1gInPc4	rub
či	či	k8xC	či
dark	dark	k6eAd1	dark
ruby	rub	k1gInPc4	rub
oči	oko	k1gNnPc4	oko
himalay	himalaa	k1gFnSc2	himalaa
(	(	kIx(	(
<g/>
himálajské	himálajský	k2eAgNnSc1d1	himálajské
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
)	)	kIx)	)
-	-	kIx~	-
jako	jako	k8xC	jako
siamský	siamský	k2eAgMnSc1d1	siamský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bílý	bílý	k2eAgInSc1d1	bílý
základ	základ	k1gInSc1	základ
srsti	srst	k1gFnSc2	srst
<g/>
;	;	kIx,	;
oči	oko	k1gNnPc4	oko
červené	červená	k1gFnSc2	červená
siam	siam	k6eAd1	siam
(	(	kIx(	(
<g/>
siamské	siamský	k2eAgNnSc1d1	siamské
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
základ	základ	k1gInSc4	základ
smetanová	smetanový	k2eAgFnSc1d1	smetanová
až	až	k9	až
krémová	krémový	k2eAgFnSc1d1	krémová
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
i	i	k9	i
černé	černá	k1gFnSc2	černá
lila	lila	k2eAgFnSc1d1	lila
(	(	kIx(	(
<g/>
šeříková	šeříkový	k2eAgFnSc1d1	Šeříková
<g/>
)	)	kIx)	)
-	-	kIx~	-
teplá	teplý	k2eAgFnSc1d1	teplá
hnědostříbrná	hnědostříbrný	k2eAgFnSc1d1	hnědostříbrný
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
nebo	nebo	k8xC	nebo
dark	dark	k6eAd1	dark
ruby	rub	k1gInPc4	rub
oči	oko	k1gNnPc4	oko
topaz	topaz	k1gInSc1	topaz
(	(	kIx(	(
<g/>
topas	topas	k1gInSc1	topas
<g/>
)	)	kIx)	)
-	-	kIx~	-
zlato-oranžová	zlatoranžový	k2eAgFnSc1d1	zlato-oranžová
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
ruby	rub	k1gInPc1	rub
či	či	k8xC	či
dark	dark	k6eAd1	dark
ruby	rub	k1gInPc4	rub
oči	oko	k1gNnPc4	oko
pearl	pearnout	k5eAaPmAgMnS	pearnout
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
perlová	perlová	k1gFnSc1	perlová
<g/>
)	)	kIx)	)
-	-	kIx~	-
světle	světle	k6eAd1	světle
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
s	s	k7c7	s
modro-stříbrným	modrotříbrný	k2eAgInSc7d1	modro-stříbrný
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
černé	černá	k1gFnSc2	černá
<g/>
;	;	kIx,	;
potkani	potkan	k1gMnPc1	potkan
tohoto	tento	k3xDgNnSc2	tento
zbarvení	zbarvení	k1gNnSc2	zbarvení
nebo	nebo	k8xC	nebo
s	s	k7c7	s
genem	gen	k1gInSc7	gen
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
zbarvení	zbarvení	k1gNnSc4	zbarvení
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nesmí	smět	k5eNaImIp3nS	smět
křížit	křížit	k5eAaImF	křížit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
vrhu	vrh	k1gInSc2	vrh
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
dove	dovat	k5eAaPmIp3nS	dovat
-	-	kIx~	-
holubí	holubí	k2eAgFnSc4d1	holubí
šeď	šeď	k1gFnSc4	šeď
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
ruby	rub	k1gInPc7	rub
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
dark	dark	k6eAd1	dark
ruby	rub	k1gInPc4	rub
mink	mink	k1gMnSc1	mink
(	(	kIx(	(
<g/>
norek	norek	k1gMnSc1	norek
<g/>
)	)	kIx)	)
-	-	kIx~	-
tmavá	tmavý	k2eAgFnSc1d1	tmavá
hnědošedá	hnědošedý	k2eAgFnSc1d1	hnědošedá
srst	srst	k1gFnSc1	srst
s	s	k7c7	s
modrým	modrý	k2eAgNnSc7d1	modré
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
,	,	kIx,	,
hnědošedý	hnědošedý	k2eAgInSc4d1	hnědošedý
pigment	pigment	k1gInSc4	pigment
na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
<g/>
,	,	kIx,	,
ocase	ocas	k1gInSc6	ocas
a	a	k8xC	a
uších	ucho	k1gNnPc6	ucho
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
či	či	k8xC	či
dark	dark	k6eAd1	dark
ruby	rub	k1gInPc4	rub
mock	mock	k1gMnSc1	mock
mink	mink	k1gMnSc1	mink
(	(	kIx(	(
<g/>
havana	havana	k1gNnSc1	havana
<g/>
)	)	kIx)	)
-	-	kIx~	-
barva	barva	k1gFnSc1	barva
jako	jako	k8xC	jako
mink	mink	k1gMnSc1	mink
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
černé	černý	k2eAgFnSc2d1	černá
nebo	nebo	k8xC	nebo
ruby	rub	k1gInPc4	rub
platin	platina	k1gFnPc2	platina
-	-	kIx~	-
platinová	platinový	k2eAgFnSc1d1	platinová
(	(	kIx(	(
<g/>
stříbřitá	stříbřitý	k2eAgFnSc1d1	stříbřitá
<g/>
)	)	kIx)	)
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
do	do	k7c2	do
modra	modro	k1gNnSc2	modro
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
ruby	rub	k1gInPc1	rub
nebo	nebo	k8xC	nebo
černé	černý	k2eAgFnPc1d1	černá
black	black	k6eAd1	black
eyed	eyed	k6eAd1	eyed
white	white	k5eAaPmIp2nP	white
(	(	kIx(	(
<g/>
BEW	BEW	kA	BEW
<g/>
)	)	kIx)	)
-	-	kIx~	-
bílý	bílý	k1gMnSc1	bílý
potkan	potkan	k1gMnSc1	potkan
s	s	k7c7	s
černýma	černý	k2eAgNnPc7d1	černé
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
albína	albín	k1gMnSc4	albín
self	self	k1gInSc4	self
<g/>
,	,	kIx,	,
solid	solid	k1gInSc4	solid
-	-	kIx~	-
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
<g />
.	.	kIx.	.
</s>
<s>
berkshire	berkshir	k1gMnSc5	berkshir
-	-	kIx~	-
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
s	s	k7c7	s
co	co	k9	co
největším	veliký	k2eAgNnSc7d3	veliký
a	a	k8xC	a
nejostřeji	ostro	k6eAd3	ostro
ohraničeným	ohraničený	k2eAgInSc7d1	ohraničený
bílým	bílý	k2eAgInSc7d1	bílý
obdélníkem	obdélník	k1gInSc7	obdélník
na	na	k7c6	na
bříšku	bříško	k1gNnSc6	bříško
<g/>
,	,	kIx,	,
bílými	bílý	k2eAgFnPc7d1	bílá
"	"	kIx"	"
<g/>
ponožkami	ponožka	k1gFnPc7	ponožka
<g/>
"	"	kIx"	"
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
poslední	poslední	k2eAgFnSc7d1	poslední
třetinou	třetina	k1gFnSc7	třetina
ocasu	ocas	k1gInSc2	ocas
capped	capped	k1gInSc1	capped
-	-	kIx~	-
kapucín	kapucín	k1gMnSc1	kapucín
-	-	kIx~	-
hlava	hlava	k1gFnSc1	hlava
až	až	k9	až
po	po	k7c4	po
uši	ucho	k1gNnPc4	ucho
je	být	k5eAaImIp3nS	být
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
<g/>
,	,	kIx,	,
brada	brada	k1gFnSc1	brada
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zůstat	zůstat	k5eAaPmF	zůstat
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
také	také	k9	také
bílý	bílý	k2eAgInSc1d1	bílý
<g />
.	.	kIx.	.
</s>
<s>
hooded	hooded	k1gInSc1	hooded
-	-	kIx~	-
japonská	japonský	k2eAgFnSc1d1	japonská
kresba	kresba	k1gFnSc1	kresba
-	-	kIx~	-
jako	jako	k8xC	jako
kapucín	kapucín	k1gMnSc1	kapucín
<g/>
,	,	kIx,	,
na	na	k7c4	na
kapuci	kapuce	k1gFnSc4	kapuce
navazuje	navazovat	k5eAaImIp3nS	navazovat
pruh	pruh	k1gInSc1	pruh
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
<g/>
,	,	kIx,	,
nepřerušovaný	přerušovaný	k2eNgInSc4d1	nepřerušovaný
<g/>
,	,	kIx,	,
pigment	pigment	k1gInSc4	pigment
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
co	co	k9	co
nejdál	daleko	k6eAd3	daleko
husky	husky	k6eAd1	husky
-	-	kIx~	-
záda	záda	k1gNnPc1	záda
a	a	k8xC	a
boky	boka	k1gFnPc1	boka
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarvit	k5eAaPmNgFnP	zbarvit
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
uší	ucho	k1gNnPc2	ucho
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
které	který	k3yQgFnPc1	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
až	až	k9	až
mezi	mezi	k7c4	mezi
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
;	;	kIx,	;
husky	husky	k6eAd1	husky
potkani	potkan	k1gMnPc1	potkan
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
černobílí	černobílý	k2eAgMnPc1d1	černobílý
a	a	k8xC	a
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
světlají	světlat	k5eAaImIp3nP	světlat
irish	irish	k1gInSc4	irish
-	-	kIx~	-
irská	irský	k2eAgFnSc1d1	irská
kresba	kresba	k1gFnSc1	kresba
-	-	kIx~	-
tmavá	tmavý	k2eAgFnSc1d1	tmavá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
vrcholem	vrchol	k1gInSc7	vrchol
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
ocásku	ocásek	k1gInSc3	ocásek
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
co	co	k9	co
nejzřetelněji	zřetelně	k6eAd3	zřetelně
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
bareback	bareback	k1gInSc1	bareback
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
krk	krk	k1gInSc1	krk
a	a	k8xC	a
hruď	hruď	k1gFnSc1	hruď
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarven	k2eAgInPc1d1	zbarven
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
variegated	variegated	k1gInSc1	variegated
-	-	kIx~	-
pestrý	pestrý	k2eAgInSc1d1	pestrý
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
ramena	rameno	k1gNnPc1	rameno
bývají	bývat	k5eAaImIp3nP	bývat
zbarveny	zbarven	k2eAgFnPc1d1	zbarvena
jako	jako	k8xC	jako
bareback	bareback	k6eAd1	bareback
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
bocích	bok	k1gInPc6	bok
jsou	být	k5eAaImIp3nP	být
nejlépe	dobře	k6eAd3	dobře
pravidelně	pravidelně	k6eAd1	pravidelně
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
skvrny	skvrna	k1gFnSc2	skvrna
ve	v	k7c6	v
stejné	stejná	k1gFnSc6	stejná
nebo	nebo	k8xC	nebo
i	i	k9	i
jiné	jiný	k2eAgNnSc4d1	jiné
<g />
.	.	kIx.	.
</s>
<s>
barvě	barva	k1gFnSc3	barva
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
ramena	rameno	k1gNnSc2	rameno
varieberk	varieberk	k1gInSc4	varieberk
-	-	kIx~	-
potkan	potkan	k1gMnSc1	potkan
má	mít	k5eAaImIp3nS	mít
tmavé	tmavý	k2eAgNnSc4d1	tmavé
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
bílé	bílý	k2eAgNnSc4d1	bílé
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
do	do	k7c2	do
tmavé	tmavý	k2eAgFnSc2d1	tmavá
"	"	kIx"	"
<g/>
strakováním	strakování	k1gNnSc7	strakování
<g/>
"	"	kIx"	"
blazed	blazed	k1gMnSc1	blazed
-	-	kIx~	-
lysina	lysina	k1gFnSc1	lysina
-	-	kIx~	-
bílá	bílý	k2eAgFnSc1d1	bílá
lysina	lysina	k1gFnSc1	lysina
která	který	k3yRgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
až	až	k9	až
k	k	k7c3	k
nosu	nos	k1gInSc2	nos
headspotted	headspotted	k1gInSc1	headspotted
-	-	kIx~	-
bílá	bílý	k2eAgFnSc1d1	bílá
skvrna	skvrna	k1gFnSc1	skvrna
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
collared	collared	k1gInSc1	collared
-	-	kIx~	-
bílý	bílý	k2eAgInSc1d1	bílý
límeček	límeček	k1gInSc1	límeček
merle	merle	k1gFnSc2	merle
-	-	kIx~	-
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
zatím	zatím	k6eAd1	zatím
málo	málo	k6eAd1	málo
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
tmavší	tmavý	k2eAgInPc4d2	tmavší
znaky	znak	k1gInPc4	znak
na	na	k7c6	na
srsti	srst	k1gFnSc6	srst
american	americana	k1gFnPc2	americana
berkshire	berkshir	k1gInSc5	berkshir
-	-	kIx~	-
americký	americký	k2eAgMnSc5d1	americký
berkshire	berkshir	k1gMnSc5	berkshir
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stranu	strana	k1gFnSc4	strana
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	se	k3xPyFc4	se
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
lokte	loket	k1gInSc2	loket
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
od	od	k7c2	od
konce	konec	k1gInSc2	konec
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
downunder	downunder	k1gInSc1	downunder
-	-	kIx~	-
strakování	strakování	k1gNnSc1	strakování
<g/>
,	,	kIx,	,
fleky	flek	k1gInPc1	flek
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
i	i	k9	i
na	na	k7c6	na
jinak	jinak	k6eAd1	jinak
bílém	bílý	k2eAgNnSc6d1	bílé
bříšku	bříško	k1gNnSc6	bříško
masked	masked	k1gMnSc1	masked
-	-	kIx~	-
potkan	potkan	k1gMnSc1	potkan
s	s	k7c7	s
tmavou	tmavý	k2eAgFnSc7d1	tmavá
maskou	maska	k1gFnSc7	maska
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
škrabošky	škraboška	k1gFnSc2	škraboška
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
dalmation	dalmation	k1gInSc1	dalmation
-	-	kIx~	-
dalmatin	dalmatin	k1gMnSc1	dalmatin
<g/>
,	,	kIx,	,
potkan	potkan	k1gMnSc1	potkan
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
drobnější	drobný	k2eAgFnPc1d2	drobnější
než	než	k8xS	než
u	u	k7c2	u
variegated	variegated	k1gInSc4	variegated
</s>
