<p>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
z	z	k7c2	z
lat.	lat.	k?	lat.
philosophiæ	philosophiæ	k?	philosophiæ
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
vysokoškolským	vysokoškolský	k2eAgNnSc7d1	vysokoškolské
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
tedy	tedy	k9	tedy
označuje	označovat	k5eAaImIp3nS	označovat
nositele	nositel	k1gMnSc4	nositel
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
alternativu	alternativa	k1gFnSc4	alternativa
doctor	doctor	k1gInSc1	doctor
philosophiae	philosophiae	k1gNnSc2	philosophiae
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Philosophy	Philosopha	k1gFnSc2	Philosopha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
zkracováno	zkracovat	k5eAaImNgNnS	zkracovat
jako	jako	k8xC	jako
DPhil	DPhil	k1gInSc1	DPhil
<g/>
,	,	kIx,	,
či	či	k8xC	či
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
phil	phil	k1gMnSc1	phil
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
často	často	k6eAd1	často
také	také	k9	také
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
PhD	PhD	k1gFnSc2	PhD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
také	také	k9	také
často	často	k6eAd1	často
jako	jako	k8xC	jako
PhD	PhD	k1gFnSc1	PhD
<g/>
;	;	kIx,	;
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
obecně	obecně	k6eAd1	obecně
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
akademicko-vědecký	akademickoědecký	k2eAgInSc4d1	akademicko-vědecký
titul	titul	k1gInSc4	titul
získaný	získaný	k2eAgInSc4d1	získaný
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
cyklu	cyklus	k1gInSc6	cyklus
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
uváděný	uváděný	k2eAgInSc4d1	uváděný
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
oddělen	oddělit	k5eAaPmNgMnS	oddělit
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
hodnost	hodnost	k1gFnSc4	hodnost
doktora	doktor	k1gMnSc2	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
uvádí	uvádět	k5eAaImIp3nS	uvádět
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jako	jako	k9	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
uděluje	udělovat	k5eAaImIp3nS	udělovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
oblasti	oblast	k1gFnPc4	oblast
vyčleněný	vyčleněný	k2eAgInSc4d1	vyčleněný
jiný	jiný	k2eAgInSc4d1	jiný
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
–	–	k?	–
typicky	typicky	k6eAd1	typicky
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgInPc4d1	umělecký
obory	obor	k1gInPc4	obor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ArtD	ArtD	k1gFnSc6	ArtD
<g/>
.	.	kIx.	.
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
varianta	varianta	k1gFnSc1	varianta
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
obory	obora	k1gFnPc4	obora
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
–	–	k?	–
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
)	)	kIx)	)
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
samostatné	samostatný	k2eAgInPc4d1	samostatný
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bakalářské	bakalářský	k2eAgFnPc1d1	Bakalářská
(	(	kIx(	(
<g/>
bachelor	bachelor	k1gInSc1	bachelor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
–	–	k?	–
magisterské	magisterský	k2eAgFnPc1d1	magisterská
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
–	–	k?	–
doktorské	doktorský	k2eAgFnPc4d1	doktorská
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
,	,	kIx,	,
doctoral	doctorat	k5eAaPmAgMnS	doctorat
degree	degreat	k5eAaPmIp3nS	degreat
<g/>
,	,	kIx,	,
doctorate	doctorat	k1gMnSc5	doctorat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doktorát	doktorát	k1gInSc4	doktorát
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
představuje	představovat	k5eAaImIp3nS	představovat
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
možný	možný	k2eAgInSc1d1	možný
stupeň	stupeň	k1gInSc1	stupeň
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
dosažený	dosažený	k2eAgInSc4d1	dosažený
studiem	studio	k1gNnSc7	studio
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
požadavkem	požadavek	k1gInSc7	požadavek
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
či	či	k8xC	či
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
uvedené	uvedený	k2eAgNnSc4d1	uvedené
členění	členění	k1gNnSc4	členění
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
tzv.	tzv.	kA	tzv.
Boloňský	boloňský	k2eAgInSc4d1	boloňský
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
označovala	označovat	k5eAaImAgFnS	označovat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
harmonizace	harmonizace	k1gFnSc1	harmonizace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
–	–	k?	–
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
zavedení	zavedení	k1gNnSc2	zavedení
byla	být	k5eAaImAgFnS	být
porovnatelnost	porovnatelnost	k1gFnSc1	porovnatelnost
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
určitá	určitý	k2eAgFnSc1d1	určitá
standardizace	standardizace	k1gFnSc1	standardizace
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
také	také	k9	také
usnadnění	usnadnění	k1gNnSc4	usnadnění
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
–	–	k?	–
důsledkem	důsledek	k1gInSc7	důsledek
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
mj.	mj.	kA	mj.
zavedení	zavedení	k1gNnSc4	zavedení
těchto	tento	k3xDgInPc2	tento
stupňů	stupeň	k1gInPc2	stupeň
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
tří	tři	k4xCgNnPc2	tři
cyklů	cyklus	k1gInPc2	cyklus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
kreditový	kreditový	k2eAgInSc4d1	kreditový
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
European	European	k1gMnSc1	European
Credit	Credit	k1gFnSc2	Credit
Transfer	transfer	k1gInSc1	transfer
and	and	k?	and
Accumulation	Accumulation	k1gInSc1	Accumulation
System	Systo	k1gNnSc7	Systo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ECTS	ECTS	kA	ECTS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orientačně	orientačně	k6eAd1	orientačně
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
bakalářské	bakalářský	k2eAgNnSc1d1	bakalářské
studium	studium	k1gNnSc1	studium
trvá	trvat	k5eAaImIp3nS	trvat
většinou	většinou	k6eAd1	většinou
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případné	případný	k2eAgNnSc4d1	případné
navazující	navazující	k2eAgNnSc4d1	navazující
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
studium	studium	k1gNnSc1	studium
nezáleží	záležet	k5eNaImIp3nS	záležet
primárně	primárně	k6eAd1	primárně
na	na	k7c6	na
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
na	na	k7c6	na
získaných	získaný	k2eAgInPc6d1	získaný
kreditech	kredit	k1gInPc6	kredit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
většinou	většinou	k6eAd1	většinou
získat	získat	k5eAaPmF	získat
240	[number]	k4	240
kreditů	kredit	k1gInPc2	kredit
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
60	[number]	k4	60
ECTS	ECTS	kA	ECTS
za	za	k7c4	za
akademický	akademický	k2eAgInSc4d1	akademický
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
instituci	instituce	k1gFnSc6	instituce
a	a	k8xC	a
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
udělování	udělování	k1gNnSc2	udělování
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nabytí	nabytí	k1gNnSc3	nabytí
této	tento	k3xDgFnSc2	tento
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
takovýto	takovýto	k3xDgMnSc1	takovýto
student	student	k1gMnSc1	student
často	často	k6eAd1	často
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xC	jako
doktorand	doktorand	k1gMnSc1	doktorand
(	(	kIx(	(
<g/>
doctoral	doctorat	k5eAaImAgMnS	doctorat
student	student	k1gMnSc1	student
<g/>
)	)	kIx)	)
či	či	k8xC	či
PhD	PhD	k1gMnSc1	PhD
student	student	k1gMnSc1	student
(	(	kIx(	(
<g/>
event.	event.	k?	event.
též	též	k9	též
doctoral	doctorat	k5eAaImAgMnS	doctorat
candidate	candidat	k1gInSc5	candidat
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
candidate	candidat	k1gInSc5	candidat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uchazeč	uchazeč	k1gMnSc1	uchazeč
o	o	k7c6	o
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
musí	muset	k5eAaImIp3nS	muset
předložit	předložit	k5eAaPmF	předložit
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
–	–	k?	–
disertaci	disertace	k1gFnSc4	disertace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
obsahující	obsahující	k2eAgInPc1d1	obsahující
výsledky	výsledek	k1gInPc1	výsledek
originálního	originální	k2eAgInSc2d1	originální
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
natolik	natolik	k6eAd1	natolik
hodnotný	hodnotný	k2eAgInSc1d1	hodnotný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
uveřejněn	uveřejněn	k2eAgInSc1d1	uveřejněn
v	v	k7c6	v
recenzovaném	recenzovaný	k2eAgInSc6d1	recenzovaný
časopise	časopis	k1gInSc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
takovýto	takovýto	k3xDgInSc4	takovýto
kandidát	kandidát	k1gMnSc1	kandidát
musí	muset	k5eAaImIp3nS	muset
obhájit	obhájit	k5eAaPmF	obhájit
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
před	před	k7c7	před
komisí	komise	k1gFnSc7	komise
zkoušejících	zkoušející	k2eAgMnPc2d1	zkoušející
odborníků	odborník	k1gMnPc2	odborník
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
–	–	k?	–
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
philosophiae	philosophiae	k6eAd1	philosophiae
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
neodkazuje	odkazovat	k5eNaImIp3nS	odkazovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
akademické	akademický	k2eAgFnSc2d1	akademická
disciplíny	disciplína	k1gFnSc2	disciplína
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
řeckým	řecký	k2eAgInSc7d1	řecký
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
moudrosti	moudrost	k1gFnSc3	moudrost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
(	(	kIx(	(
<g/>
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
humanitní	humanitní	k2eAgFnPc4d1	humanitní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc1d1	přírodní
vědy	věda	k1gFnPc1	věda
<g/>
)	)	kIx)	)
jiné	jiný	k2eAgFnPc1d1	jiná
než	než	k8xS	než
teologie	teologie	k1gFnPc1	teologie
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
a	a	k8xC	a
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
profesní	profesní	k2eAgFnPc1d1	profesní
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgFnPc1d1	odborná
nebo	nebo	k8xC	nebo
technické	technický	k2eAgFnPc1d1	technická
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
tradičně	tradičně	k6eAd1	tradičně
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
základní	základní	k2eAgFnSc1d1	základní
fakulta	fakulta	k1gFnSc1	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
filosofická	filosofický	k2eAgFnSc1d1	filosofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
čtení	čtení	k1gNnSc1	čtení
===	===	k?	===
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
"	"	kIx"	"
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zkratku	zkratka	k1gFnSc4	zkratka
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
ph	ph	kA	ph
eɪ	eɪ	k?	eɪ
<g/>
͡	͡	k?	͡
<g/>
ʃ	ʃ	k?	ʃ
diː	diː	k?	diː
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
pí	pí	k1gNnSc1	pí
ejč	ejč	k?	ejč
dý	dý	k?	dý
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
česky	česky	k6eAd1	česky
[	[	kIx(	[
<g/>
pé	pé	k?	pé
há	há	k?	há
dé	dé	k?	dé
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
však	však	k9	však
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
zkratky	zkratka	k1gFnPc4	zkratka
nepoužívat	používat	k5eNaImF	používat
při	při	k7c6	při
představování	představování	k1gNnSc6	představování
konkrétních	konkrétní	k2eAgMnPc2d1	konkrétní
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
třeba	třeba	k6eAd1	třeba
vyslovit	vyslovit	k5eAaPmF	vyslovit
nezkrácené	zkrácený	k2eNgNnSc4d1	nezkrácené
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Uvádění	uvádění	k1gNnSc1	uvádění
<g/>
,	,	kIx,	,
zápis	zápis	k1gInSc1	zápis
===	===	k?	===
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
ty	ten	k3xDgMnPc4	ten
ostatní	ostatní	k2eAgMnPc4d1	ostatní
postgraduální	postgraduální	k2eAgMnPc4d1	postgraduální
této	tento	k3xDgFnSc2	tento
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
event.	event.	k?	event.
i	i	k8xC	i
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
DSc	DSc	k1gFnSc1	DSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ArtD	ArtD	k1gFnSc1	ArtD
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
uvádí	uvádět	k5eAaImIp3nS	uvádět
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
odděleno	oddělen	k2eAgNnSc1d1	odděleno
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
čárkou	čárka	k1gFnSc7	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
u	u	k7c2	u
jména	jméno	k1gNnSc2	jméno
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
až	až	k9	až
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
–	–	k?	–
doktorské	doktorský	k2eAgNnSc1d1	doktorské
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nižší	nízký	k2eAgInPc1d2	nižší
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
bakalářské	bakalářský	k2eAgFnPc1d1	Bakalářská
<g/>
,	,	kIx,	,
magisterské	magisterský	k2eAgFnPc1d1	magisterská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
běžně	běžně	k6eAd1	běžně
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
však	však	k9	však
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
některé	některý	k3yIgInPc4	některý
profesní	profesní	k2eAgInPc4d1	profesní
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tituly	titul	k1gInPc1	titul
spjaté	spjatý	k2eAgInPc1d1	spjatý
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
profesí	profes	k1gFnSc7	profes
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
apod.	apod.	kA	apod.
Pokud	pokud	k8xS	pokud
se	s	k7c7	s
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
užívá	užívat	k5eAaImIp3nS	užívat
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
běžně	běžně	k6eAd1	běžně
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xC	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Novák	Novák	k1gMnSc1	Novák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
komunikaci	komunikace	k1gFnSc6	komunikace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
emailové	emailový	k2eAgFnSc6d1	emailová
komunikaci	komunikace	k1gFnSc6	komunikace
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k9	tak
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
tento	tento	k3xDgInSc4	tento
vědecký	vědecký	k2eAgInSc4d1	vědecký
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
odděleno	oddělit	k5eAaPmNgNnS	oddělit
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pouze	pouze	k6eAd1	pouze
jiný	jiný	k2eAgInSc4d1	jiný
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
získaný	získaný	k2eAgInSc4d1	získaný
titul	titul	k1gInSc4	titul
<g/>
/	/	kIx~	/
<g/>
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
/	/	kIx~	/
<g/>
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
prof.	prof.	kA	prof.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obvyklé	obvyklý	k2eAgNnSc4d1	obvyklé
uvádění	uvádění	k1gNnSc4	uvádění
i	i	k9	i
nižších	nízký	k2eAgMnPc2d2	nižší
titulů	titul	k1gInPc2	titul
současně	současně	k6eAd1	současně
–	–	k?	–
typicky	typicky	k6eAd1	typicky
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
spolu	spolu	k6eAd1	spolu
mají	mít	k5eAaImIp3nP	mít
uzavřenu	uzavřen	k2eAgFnSc4d1	uzavřena
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
uznávání	uznávání	k1gNnSc6	uznávání
a	a	k8xC	a
rovnocennosti	rovnocennost	k1gFnSc6	rovnocennost
dosaženého	dosažený	k2eAgNnSc2d1	dosažené
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
často	často	k6eAd1	často
současně	současně	k6eAd1	současně
uvádí	uvádět	k5eAaImIp3nS	uvádět
navíc	navíc	k6eAd1	navíc
i	i	k9	i
nižší	nízký	k2eAgInSc4d2	nižší
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
technický	technický	k2eAgInSc4d1	technický
<g/>
,	,	kIx,	,
humanitní	humanitní	k2eAgInSc4d1	humanitní
<g/>
,	,	kIx,	,
lékařský	lékařský	k2eAgInSc4d1	lékařský
<g/>
,	,	kIx,	,
veterinární	veterinární	k2eAgInSc4d1	veterinární
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
Mgr.	Mgr.	kA	Mgr.
Jana	Jana	k1gFnSc1	Jana
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
MUDr.	MUDr.	kA	MUDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
MVDr.	MVDr.	kA	MVDr.
Jana	Jana	k1gFnSc1	Jana
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Jana	Jana	k1gFnSc1	Jana
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
doc.	doc.	kA	doc.
ThDr.	ThDr.	k1gMnSc1	ThDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
PhDr.	PhDr.	kA	PhDr.
Jana	Jana	k1gFnSc1	Jana
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
et	et	k?	et
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
;	;	kIx,	;
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
či	či	k8xC	či
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
samostatně	samostatně	k6eAd1	samostatně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
i	i	k9	i
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
nižší	nízký	k2eAgInPc4d2	nižší
tituly	titul	k1gInPc4	titul
(	(	kIx(	(
<g/>
magisterské	magisterský	k2eAgFnPc4d1	magisterská
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
bakalářské	bakalářský	k2eAgFnPc1d1	Bakalářská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
také	také	k9	také
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
<g/>
-li	i	k?	-li
jméno	jméno	k1gNnSc1	jméno
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přístavek	přístavek	k1gInSc4	přístavek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Host	host	k1gMnSc1	host
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
připomenul	připomenout	k5eAaPmAgMnS	připomenout
důležitost	důležitost	k1gFnSc4	důležitost
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
rozepsáno	rozepsán	k2eAgNnSc1d1	rozepsáno
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Host	host	k1gMnSc1	host
inženýr	inženýr	k1gMnSc1	inženýr
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
připomenul	připomenout	k5eAaPmAgMnS	připomenout
důležitost	důležitost	k1gFnSc4	důležitost
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
nejčastěji	často	k6eAd3	často
uvedl	uvést	k5eAaPmAgMnS	uvést
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Novak	Novak	k1gMnSc1	Novak
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
jako	jako	k8xC	jako
Jan	Jan	k1gMnSc1	Jan
Novak	Novak	k1gMnSc1	Novak
<g/>
,	,	kIx,	,
PhD	PhD	k1gMnSc1	PhD
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novak	Novak	k1gMnSc1	Novak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
méně	málo	k6eAd2	málo
oficiálních	oficiální	k2eAgInPc6d1	oficiální
<g/>
,	,	kIx,	,
neúředních	úřední	k2eNgInPc6d1	neúřední
dokumentech	dokument	k1gInPc6	dokument
též	též	k6eAd1	též
možné	možný	k2eAgNnSc1d1	možné
pro	pro	k7c4	pro
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc4	užití
obecné	obecný	k2eAgFnSc2d1	obecná
zkratky	zkratka	k1gFnSc2	zkratka
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prostého	prostý	k2eAgNnSc2d1	prosté
dr	dr	kA	dr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
oblasti	oblast	k1gFnSc6	oblast
byl	být	k5eAaImAgInS	být
doktorát	doktorát	k1gInSc4	doktorát
získán	získán	k2eAgMnSc1d1	získán
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
malý	malý	k2eAgInSc1d1	malý
doktorát	doktorát	k1gInSc1	doktorát
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
kodifikována	kodifikován	k2eAgFnSc1d1	kodifikována
podoba	podoba	k1gFnSc1	podoba
"	"	kIx"	"
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
"	"	kIx"	"
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
D.	D.	kA	D.
<g/>
"	"	kIx"	"
–	–	k?	–
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
PhD	PhD	k1gFnSc1	PhD
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
obecných	obecný	k2eAgFnPc2d1	obecná
typografických	typografický	k2eAgFnPc2d1	typografická
zásad	zásada	k1gFnPc2	zásada
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
správný	správný	k2eAgInSc4d1	správný
zápis	zápis	k1gInSc4	zápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Užívání	užívání	k1gNnSc1	užívání
<g/>
,	,	kIx,	,
formální	formální	k2eAgNnSc1d1	formální
oslovování	oslovování	k1gNnSc1	oslovování
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
formálním	formální	k2eAgNnSc6d1	formální
oslovování	oslovování	k1gNnSc6	oslovování
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vždy	vždy	k6eAd1	vždy
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
dosažený	dosažený	k2eAgInSc1d1	dosažený
titul	titul	k1gInSc1	titul
<g/>
/	/	kIx~	/
<g/>
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
bývá	bývat	k5eAaImIp3nS	bývat
upřednostnit	upřednostnit	k5eAaPmF	upřednostnit
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Nositele	nositel	k1gMnSc4	nositel
doktorátu	doktorát	k1gInSc2	doktorát
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
titulu	titul	k1gInSc2	titul
doktorské	doktorský	k2eAgFnSc2d1	doktorská
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
oslovujeme	oslovovat	k5eAaImIp1nP	oslovovat
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
doktore	doktor	k1gMnSc5	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
paní	paní	k1gFnSc3	paní
doktorko	doktorka	k1gFnSc5	doktorka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
<g />
.	.	kIx.	.
</s>
<s>
nižším	nízký	k2eAgInSc7d2	nižší
titulem	titul	k1gInSc7	titul
magisterské	magisterský	k2eAgFnSc2d1	magisterská
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
inženýre	inženýr	k1gMnSc5	inženýr
<g/>
/	/	kIx~	/
<g/>
magistře	magistra	k1gFnSc6	magistra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
nižším	nízký	k2eAgInSc7d2	nižší
titulem	titul	k1gInSc7	titul
bakalářské	bakalářský	k2eAgFnSc2d1	Bakalářská
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
6	[number]	k4	6
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
bakaláři	bakalář	k1gMnSc5	bakalář
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Některá	některý	k3yIgNnPc1	některý
dřívější	dřívější	k2eAgFnSc7d1	dřívější
<g />
.	.	kIx.	.
</s>
<s>
doporučení	doporučení	k1gNnSc4	doporučení
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
používat	používat	k5eAaImF	používat
k	k	k7c3	k
formálnímu	formální	k2eAgNnSc3d1	formální
oslovování	oslovování	k1gNnSc3	oslovování
jen	jen	k9	jen
titul	titul	k1gInSc4	titul
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
již	již	k9	již
nejsou	být	k5eNaImIp3nP	být
aktuální	aktuální	k2eAgInPc1d1	aktuální
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
do	do	k7c2	do
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
uděloval	udělovat	k5eAaImAgMnS	udělovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
bývalého	bývalý	k2eAgMnSc2d1	bývalý
tzv.	tzv.	kA	tzv.
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
)	)	kIx)	)
místo	místo	k7c2	místo
doktora	doktor	k1gMnSc2	doktor
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oslovení	oslovení	k1gNnSc1	oslovení
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
kandidáte	kandidát	k1gMnSc5	kandidát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
paní	paní	k1gFnSc1	paní
kandidátko	kandidátka	k1gFnSc5	kandidátka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
neužívalo	užívat	k5eNaImAgNnS	užívat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc1	starověk
<g/>
,	,	kIx,	,
středověk	středověk	k1gInSc1	středověk
a	a	k8xC	a
novověk	novověk	k1gInSc1	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
Latinsky	latinsky	k6eAd1	latinsky
Philosophiæ	Philosophiæ	k1gMnSc1	Philosophiæ
doctor	doctor	k1gMnSc1	doctor
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
starořeckého	starořecký	k2eAgInSc2d1	starořecký
Δ	Δ	k?	Δ
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
učitel	učitel	k1gMnSc1	učitel
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc4	doctor
lze	lze	k6eAd1	lze
také	také	k9	také
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
učený	učený	k2eAgMnSc1d1	učený
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
udělován	udělovat	k5eAaImNgInS	udělovat
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
dokončily	dokončit	k5eAaPmAgFnP	dokončit
dostatečně	dostatečně	k6eAd1	dostatečně
komplexní	komplexní	k2eAgFnSc4d1	komplexní
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
předmětu	předmět	k1gInSc6	předmět
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
případně	případně	k6eAd1	případně
završily	završit	k5eAaPmAgFnP	završit
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
podmínky	podmínka	k1gFnPc1	podmínka
přesněji	přesně	k6eAd2	přesně
definované	definovaný	k2eAgInPc1d1	definovaný
studijními	studijní	k2eAgFnPc7d1	studijní
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
a	a	k8xC	a
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
už	už	k9	už
za	za	k7c2	za
starého	starý	k2eAgInSc2d1	starý
Říma	Řím	k1gInSc2	Řím
však	však	k9	však
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
doktoři	doktor	k1gMnPc1	doktor
označováni	označován	k2eAgMnPc1d1	označován
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
konali	konat	k5eAaImAgMnP	konat
veřejné	veřejný	k2eAgFnPc4d1	veřejná
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
doktor	doktor	k1gMnSc1	doktor
stalo	stát	k5eAaPmAgNnS	stát
čestné	čestný	k2eAgNnSc4d1	čestné
označení	označení	k1gNnSc4	označení
učenců	učenec	k1gMnPc2	učenec
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
byl	být	k5eAaImAgInS	být
připojován	připojován	k2eAgInSc4d1	připojován
výstižný	výstižný	k2eAgInSc4d1	výstižný
přívlastek	přívlastek	k1gInSc4	přívlastek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
byl	být	k5eAaImAgMnS	být
doctor	doctor	k1gInSc4	doctor
angelicus	angelicus	k1gMnSc1	angelicus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Duns	Dunsa	k1gFnPc2	Dunsa
Scotus	Scotus	k1gMnSc1	Scotus
doctor	doctor	k1gMnSc1	doctor
subtilis	subtilis	k1gFnSc1	subtilis
nebo	nebo	k8xC	nebo
Roger	Roger	k1gMnSc1	Roger
Bacon	Bacon	k1gMnSc1	Bacon
doctor	doctor	k1gMnSc1	doctor
mirabilis	mirabilis	k1gFnSc1	mirabilis
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgMnPc1d1	církevní
otcové	otec	k1gMnPc1	otec
byli	být	k5eAaImAgMnP	být
nazývání	nazývání	k1gNnSc4	nazývání
doctores	doctoresa	k1gFnPc2	doctoresa
ecclesiae	ecclesia	k1gInSc2	ecclesia
a	a	k8xC	a
jako	jako	k8xC	jako
doctores	doctores	k1gMnSc1	doctores
thalmudiaci	thalmudiace	k1gFnSc4	thalmudiace
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
židovští	židovský	k2eAgMnPc1d1	židovský
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
znalci	znalec	k1gMnPc1	znalec
talmudu	talmud	k1gInSc2	talmud
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
mohl	moct	k5eAaImAgMnS	moct
nejprve	nejprve	k6eAd1	nejprve
student	student	k1gMnSc1	student
získat	získat	k5eAaPmF	získat
akademický	akademický	k2eAgInSc4d1	akademický
gradus	gradus	k1gInSc4	gradus
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
–	–	k?	–
Baccalaureatus	Baccalaureatus	k1gInSc1	Baccalaureatus
(	(	kIx(	(
<g/>
vavřínem	vavřín	k1gInSc7	vavřín
ověnčený	ověnčený	k2eAgMnSc1d1	ověnčený
<g/>
)	)	kIx)	)
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
absolvování	absolvování	k1gNnSc6	absolvování
trivia	trivium	k1gNnSc2	trivium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
dialektika	dialektika	k1gFnSc1	dialektika
a	a	k8xC	a
rétorika	rétorika	k1gFnSc1	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
absolvent	absolvent	k1gMnSc1	absolvent
vypomáhal	vypomáhat	k5eAaImAgMnS	vypomáhat
s	s	k7c7	s
učením	učení	k1gNnSc7	učení
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
kvadriviu	kvadrivium	k1gNnSc3	kvadrivium
(	(	kIx(	(
<g/>
aritmetika	aritmetika	k1gFnSc1	aritmetika
<g/>
,	,	kIx,	,
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
;	;	kIx,	;
dohromady	dohromady	k6eAd1	dohromady
tzv.	tzv.	kA	tzv.
sedm	sedm	k4xCc1	sedm
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
gradus	gradus	k1gInSc1	gradus
získával	získávat	k5eAaImAgInS	získávat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
artistické	artistický	k2eAgFnSc2d1	artistická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
ostatní	ostatní	k1gNnSc4	ostatní
fakulty	fakulta	k1gFnSc2	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Bakalář	bakalář	k1gMnSc1	bakalář
následně	následně	k6eAd1	následně
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
obhajobě	obhajoba	k1gFnSc3	obhajoba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
these	these	k1gFnPc1	these
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
získat	získat	k5eAaPmF	získat
gradus	gradus	k1gInSc1	gradus
Magister	magistra	k1gFnPc2	magistra
(	(	kIx(	(
<g/>
mistra	mistr	k1gMnSc2	mistr
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
učitel	učitel	k1gMnSc1	učitel
či	či	k8xC	či
představený	představený	k1gMnSc1	představený
<g/>
)	)	kIx)	)
na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
(	(	kIx(	(
<g/>
fakultě	fakulta	k1gFnSc6	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc4	jeho
celý	celý	k2eAgInSc4d1	celý
název	název	k1gInSc4	název
magistr	magistr	k1gMnSc1	magistr
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Magistr	magistr	k1gMnSc1	magistr
značil	značit	k5eAaImAgMnS	značit
na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
původně	původně	k6eAd1	původně
učitele	učitel	k1gMnPc4	učitel
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
akademickým	akademický	k2eAgInSc7d1	akademický
titulem	titul	k1gInSc7	titul
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nezbytným	zbytný	k2eNgInSc7d1	zbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
učitelského	učitelský	k2eAgNnSc2d1	učitelské
oprávnění	oprávnění	k1gNnSc2	oprávnění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
licentia	licentia	k1gFnSc1	licentia
docendi	docend	k1gMnPc1	docend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oprávněným	oprávněný	k2eAgInSc7d1	oprávněný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zpravidla	zpravidla	k6eAd1	zpravidla
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
učitelům	učitel	k1gMnPc3	učitel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říkalo	říkat	k5eAaImAgNnS	říkat
též	též	k9	též
magister	magister	k1gMnSc1	magister
regens	regens	k1gMnSc1	regens
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
boloňské	boloňský	k2eAgFnSc6d1	Boloňská
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
učitelů	učitel	k1gMnPc2	učitel
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
doctores	doctores	k1gInSc1	doctores
legum	legum	k1gNnSc1	legum
<g/>
)	)	kIx)	)
z	z	k7c2	z
čestného	čestný	k2eAgInSc2d1	čestný
titulu	titul	k1gInSc2	titul
stala	stát	k5eAaPmAgFnS	stát
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
akademická	akademický	k2eAgFnSc1d1	akademická
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
byli	být	k5eAaImAgMnP	být
zváni	zvát	k5eAaImNgMnP	zvát
doctores	doctores	k1gMnSc1	doctores
canonum	canonum	k1gNnSc1	canonum
et	et	k?	et
decretalium	decretalium	k1gNnSc1	decretalium
a	a	k8xC	a
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sloučilo	sloučit	k5eAaPmAgNnS	sloučit
studium	studium	k1gNnSc1	studium
světského	světský	k2eAgNnSc2d1	světské
a	a	k8xC	a
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dodnes	dodnes	k6eAd1	dodnes
užívaný	užívaný	k2eAgInSc1d1	užívaný
titul	titul	k1gInSc1	titul
iuris	iuris	k1gFnSc2	iuris
utriusque	utriusquat	k5eAaPmIp3nS	utriusquat
doctor	doctor	k1gInSc1	doctor
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
pak	pak	k6eAd1	pak
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
univerzitě	univerzita	k1gFnSc6	univerzita
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
theologiae	theologiae	k6eAd1	theologiae
doctor	doctor	k1gInSc1	doctor
a	a	k8xC	a
pak	pak	k6eAd1	pak
také	také	k9	také
doktorát	doktorát	k1gInSc4	doktorát
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
logiky	logika	k1gFnSc2	logika
apod.	apod.	kA	apod.
Povinným	povinný	k2eAgInSc7d1	povinný
předstupněm	předstupeň	k1gInSc7	předstupeň
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
doktorátu	doktorát	k1gInSc2	doktorát
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
nutně	nutně	k6eAd1	nutně
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgFnSc6d1	následovaná
ziskem	zisk	k1gInSc7	zisk
licenciátu	licenciát	k1gInSc2	licenciát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc4	doktor
bylo	být	k5eAaImAgNnS	být
též	též	k9	též
dříve	dříve	k6eAd2	dříve
používáno	používán	k2eAgNnSc4d1	používáno
označení	označení	k1gNnSc4	označení
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
univerzit	univerzita	k1gFnPc2	univerzita
získávali	získávat	k5eAaImAgMnP	získávat
doktorát	doktorát	k1gInSc4	doktorát
právníci	právník	k1gMnPc1	právník
a	a	k8xC	a
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
bohoslovecké	bohoslovecký	k2eAgFnSc6d1	bohoslovecká
a	a	k8xC	a
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
byl	být	k5eAaImAgInS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
titulem	titul	k1gInSc7	titul
Magister	magister	k1gMnSc1	magister
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
odborných	odborný	k2eAgFnPc6d1	odborná
fakultách	fakulta	k1gFnPc6	fakulta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
teologické	teologický	k2eAgFnSc2d1	teologická
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgFnSc2d1	právnická
a	a	k8xC	a
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uděloval	udělovat	k5eAaImAgMnS	udělovat
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
až	až	k6eAd1	až
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc4	magister
nahradil	nahradit	k5eAaPmAgInS	nahradit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
zůstal	zůstat	k5eAaPmAgMnS	zůstat
pouze	pouze	k6eAd1	pouze
titulem	titul	k1gInSc7	titul
farmaceutů	farmaceut	k1gMnPc2	farmaceut
(	(	kIx(	(
<g/>
PhMr	PhMr	k1gMnSc1	PhMr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tedy	tedy	k9	tedy
magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc1	titul
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
užívání	užívání	k1gNnSc2	užívání
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
již	již	k6eAd1	již
jen	jen	k6eAd1	jen
různé	různý	k2eAgFnPc4d1	různá
obdoby	obdoba	k1gFnPc4	obdoba
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Vídni	Vídeň	k1gFnSc6	Vídeň
tvořili	tvořit	k5eAaImAgMnP	tvořit
absolventi	absolvent	k1gMnPc1	absolvent
univerzit	univerzita	k1gFnPc2	univerzita
s	s	k7c7	s
doktorátem	doktorát	k1gInSc7	doktorát
tzv.	tzv.	kA	tzv.
doktorské	doktorský	k2eAgInPc1d1	doktorský
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
mj.	mj.	kA	mj.
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
příslušných	příslušný	k2eAgFnPc2d1	příslušná
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
na	na	k7c6	na
doktorských	doktorský	k2eAgFnPc6d1	doktorská
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
zaváděn	zavádět	k5eAaImNgInS	zavádět
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgInS	nahradit
tzv.	tzv.	kA	tzv.
malé	malý	k2eAgInPc1d1	malý
doktoráty	doktorát	k1gInPc1	doktorát
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
též	též	k9	též
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
systém	systém	k1gInSc4	systém
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
titulům	titul	k1gInPc3	titul
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reformy	reforma	k1gFnPc1	reforma
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
významné	významný	k2eAgFnPc1d1	významná
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
vysokoškolském	vysokoškolský	k2eAgNnSc6d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Artistická	artistický	k2eAgFnSc1d1	artistická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xC	jako
filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
finální	finální	k2eAgFnSc2d1	finální
<g/>
,	,	kIx,	,
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
požadovat	požadovat	k5eAaImF	požadovat
též	též	k9	též
určité	určitý	k2eAgInPc4d1	určitý
příspěvky	příspěvek	k1gInPc4	příspěvek
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
osvědčené	osvědčený	k2eAgNnSc1d1	osvědčené
disertací	disertace	k1gFnSc7	disertace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xS	jako
doktorát	doktorát	k1gInSc1	doktorát
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
veškeré	veškerý	k3xTgInPc1	veškerý
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spjaté	spjatý	k2eAgNnSc1d1	spjaté
financování	financování	k1gNnSc1	financování
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reformy	reforma	k1gFnPc1	reforma
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
začaly	začít	k5eAaPmAgFnP	začít
německé	německý	k2eAgFnPc4d1	německá
univerzity	univerzita	k1gFnPc4	univerzita
přitahovat	přitahovat	k5eAaImF	přitahovat
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
měla	mít	k5eAaImAgFnS	mít
natolik	natolik	k6eAd1	natolik
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
dovezena	dovézt	k5eAaPmNgFnS	dovézt
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
začala	začít	k5eAaPmAgFnS	začít
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
udělovat	udělovat	k5eAaImF	udělovat
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
těm	ten	k3xDgMnPc3	ten
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
měli	mít	k5eAaImAgMnP	mít
ukončený	ukončený	k2eAgInSc4d1	ukončený
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
program	program	k1gInSc4	program
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
ukončili	ukončit	k5eAaPmAgMnP	ukončit
předepsaný	předepsaný	k2eAgInSc4d1	předepsaný
kurz	kurz	k1gInSc4	kurz
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
obhájili	obhájit	k5eAaPmAgMnP	obhájit
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
disertaci	disertace	k1gFnSc4	disertace
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
původní	původní	k2eAgInSc4d1	původní
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
či	či	k8xC	či
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc6	první
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
začaly	začít	k5eAaPmAgFnP	začít
filozofické	filozofický	k2eAgFnPc1d1	filozofická
fakulty	fakulta	k1gFnPc1	fakulta
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
název	název	k1gInSc4	název
doktorátu	doktorát	k1gInSc2	doktorát
upravován	upravován	k2eAgInSc4d1	upravován
–	–	k?	–
např.	např.	kA	např.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
rer	rer	k?	rer
<g/>
.	.	kIx.	.
nat	nat	k?	nat
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
doktoráty	doktorát	k1gInPc4	doktorát
na	na	k7c6	na
fakultách	fakulta	k1gFnPc6	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
–	–	k?	–
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
doktorát	doktorát	k1gInSc1	doktorát
filosofie	filosofie	k1gFnSc2	filosofie
jako	jako	k8xC	jako
doktorát	doktorát	k1gInSc4	doktorát
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
===	===	k?	===
</s>
</p>
<p>
<s>
Vysokoškolské	vysokoškolský	k2eAgFnPc1d1	vysokoškolská
kvalifikace	kvalifikace	k1gFnPc1	kvalifikace
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
objevily	objevit	k5eAaPmAgInP	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
doktora	doktor	k1gMnSc2	doktor
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
DSc	DSc	k1gFnSc2	DSc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ScD	ScD	k1gFnSc1	ScD
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
vyšších	vysoký	k2eAgInPc2d2	vyšší
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
London	London	k1gMnSc1	London
představila	představit	k5eAaPmAgFnS	představit
DSc	DSc	k1gMnSc7	DSc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jako	jako	k9	jako
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
studijní	studijní	k2eAgInSc4d1	studijní
kurz	kurz	k1gInSc4	kurz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přímo	přímo	k6eAd1	přímo
následoval	následovat	k5eAaImAgInS	následovat
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
BSc	BSc	k1gFnSc1	BSc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
jako	jako	k9	jako
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
tzv.	tzv.	kA	tzv.
vyšší	vysoký	k2eAgInSc4d2	vyšší
doktorát	doktorát	k1gInSc4	doktorát
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
byl	být	k5eAaImAgInS	být
DSc	DSc	k1gMnSc4	DSc
z	z	k7c2	z
Durham	Durham	k1gInSc4	Durham
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
představen	představen	k2eAgInSc1d1	představen
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgNnSc1d1	uvedené
bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
následováno	následovat	k5eAaImNgNnS	následovat
dalšími	další	k2eAgFnPc7d1	další
univerzitami	univerzita	k1gFnPc7	univerzita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Cambridge	Cambridge	k1gFnSc2	Cambridge
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ScD	ScD	k1gFnSc4	ScD
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
a	a	k8xC	a
University	universita	k1gFnSc2	universita
of	of	k?	of
London	London	k1gMnSc1	London
transformuje	transformovat	k5eAaBmIp3nS	transformovat
svůj	svůj	k3xOyFgMnSc1	svůj
DSc	DSc	k1gMnSc1	DSc
do	do	k7c2	do
programu	program	k1gInSc2	program
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kvalifikace	kvalifikace	k1gFnPc4	kvalifikace
<g/>
,	,	kIx,	,
tak	tak	k9	tak
spíše	spíše	k9	spíše
než	než	k8xS	než
programy	program	k1gInPc1	program
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
PhD	PhD	k1gFnSc2	PhD
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
Harold	Harold	k1gMnSc1	Harold
Jeffreys	Jeffreys	k1gInSc1	Jeffreys
<g/>
,	,	kIx,	,
že	že	k8xS	že
získání	získání	k1gNnSc1	získání
doktorátu	doktorát	k1gInSc2	doktorát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
ScD	ScD	k1gFnSc1	ScD
<g/>
)	)	kIx)	)
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
Royal	Royal	k1gInSc4	Royal
Society	societa	k1gFnSc2	societa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
akademii	akademie	k1gFnSc3	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
představen	představit	k5eAaPmNgInS	představit
i	i	k9	i
program	program	k1gInSc1	program
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
současný	současný	k2eAgMnSc1d1	současný
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Philosophy	Philosopha	k1gFnSc2	Philosopha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
amerického	americký	k2eAgInSc2d1	americký
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
modelu	model	k1gInSc2	model
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rychle	rychle	k6eAd1	rychle
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
mezi	mezi	k7c7	mezi
britskými	britský	k2eAgMnPc7d1	britský
i	i	k8xC	i
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Mírně	mírně	k6eAd1	mírně
starší	starý	k2eAgFnSc1d2	starší
DSc	DSc	k1gFnSc1	DSc
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
univerzitách	univerzita	k1gFnPc6	univerzita
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
staršími	starší	k1gMnPc7	starší
tzv.	tzv.	kA	tzv.
vyššími	vysoký	k2eAgInPc7d2	vyšší
doktoráty	doktorát	k1gInPc7	doktorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebyly	být	k5eNaImAgInP	být
uvedené	uvedený	k2eAgInPc1d1	uvedený
pokročilé	pokročilý	k2eAgInPc1d1	pokročilý
programy	program	k1gInPc1	program
kritériem	kritérion	k1gNnSc7	kritérion
pro	pro	k7c4	pro
profesuru	profesura	k1gFnSc4	profesura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ambicióznější	ambiciózní	k2eAgMnPc1d2	ambicióznější
badatelé	badatel	k1gMnPc1	badatel
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
školách	škola	k1gFnPc6	škola
odjížděli	odjíždět	k5eAaImAgMnP	odjíždět
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
na	na	k7c4	na
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
roky	rok	k1gInPc4	rok
získat	získat	k5eAaPmF	získat
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
či	či	k8xC	či
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
školy	škola	k1gFnPc4	škola
nabízející	nabízející	k2eAgFnSc2d1	nabízející
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
udělovala	udělovat	k5eAaImAgFnS	udělovat
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
doktoráty	doktorát	k1gInPc4	doktorát
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
je	on	k3xPp3gFnPc4	on
Eugene	Eugen	k1gInSc5	Eugen
Schuyler	Schuyler	k1gMnSc1	Schuyler
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Williams	Williamsa	k1gFnPc2	Williamsa
Wright	Wright	k1gMnSc1	Wright
a	a	k8xC	a
James	James	k1gMnSc1	James
Morris	Morris	k1gFnSc2	Morris
Whiton	Whiton	k1gInSc1	Whiton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
též	též	k9	též
další	další	k2eAgFnPc1d1	další
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
USA	USA	kA	USA
začaly	začít	k5eAaPmAgFnP	začít
nabízet	nabízet	k5eAaImF	nabízet
doktorský	doktorský	k2eAgInSc4d1	doktorský
program	program	k1gInSc4	program
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnPc1	universita
či	či	k8xC	či
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
univerzity	univerzita	k1gFnPc1	univerzita
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
program	program	k1gInSc4	program
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Johns	Johns	k1gInSc1	Johns
Hopkins	Hopkins	k1gInSc1	Hopkins
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
udíleno	udílet	k5eAaImNgNnS	udílet
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
šesti	šest	k4xCc7	šest
univerzitami	univerzita	k1gFnPc7	univerzita
(	(	kIx(	(
<g/>
Yale	Yal	k1gFnPc1	Yal
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
Johns	Johns	k1gInSc1	Johns
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Princeton	Princeton	k1gInSc4	Princeton
University	universita	k1gFnSc2	universita
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
tak	tak	k9	tak
již	již	k6eAd1	již
nutné	nutný	k2eAgNnSc1d1	nutné
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
národní	národní	k2eAgFnSc1d1	národní
vláda	vláda	k1gFnSc1	vláda
financovala	financovat	k5eAaBmAgFnS	financovat
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
programy	program	k1gInPc4	program
předních	přední	k2eAgMnPc2d1	přední
profesorů	profesor	k1gMnPc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
schváleni	schválit	k5eAaPmNgMnP	schválit
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
,	,	kIx,	,
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
studenty	student	k1gMnPc7	student
vyšších	vysoký	k2eAgFnPc2d2	vyšší
kvalifikací	kvalifikace	k1gFnPc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
soukromé	soukromý	k2eAgFnSc2d1	soukromá
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prostředky	prostředek	k1gInPc1	prostředek
pro	pro	k7c4	pro
financování	financování	k1gNnSc4	financování
byly	být	k5eAaImAgFnP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Průlom	průlom	k1gInSc1	průlom
přišel	přijít	k5eAaPmAgInS	přijít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
soukromých	soukromý	k2eAgFnPc2d1	soukromá
nadací	nadace	k1gFnPc2	nadace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
pravidelně	pravidelně	k6eAd1	pravidelně
podporovat	podporovat	k5eAaImF	podporovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc4d1	velká
korporace	korporace	k1gFnPc4	korporace
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
podporovaly	podporovat	k5eAaImAgInP	podporovat
inženýrské	inženýrský	k2eAgInPc1d1	inženýrský
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
pak	pak	k6eAd1	pak
Rockefeller	Rockefeller	k1gInSc4	Rockefeller
Foundation	Foundation	k1gInSc4	Foundation
založila	založit	k5eAaPmAgFnS	založit
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
postdoktorandy	postdoktoranda	k1gFnSc2	postdoktoranda
postdoctoral	postdoctorat	k5eAaImAgInS	postdoctorat
fellowship	fellowship	k1gInSc1	fellowship
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
přední	přední	k2eAgFnPc4d1	přední
univerzity	univerzita	k1gFnPc4	univerzita
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
učenými	učený	k2eAgFnPc7d1	učená
společnostmi	společnost	k1gFnPc7	společnost
zřídily	zřídit	k5eAaPmAgFnP	zřídit
síť	síť	k1gFnSc4	síť
vědeckých	vědecký	k2eAgInPc2d1	vědecký
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pokrok	pokrok	k1gInSc4	pokrok
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
známá	známý	k2eAgFnSc1d1	známá
formule	formule	k1gFnSc1	formule
"	"	kIx"	"
<g/>
Publish	Publish	k1gInSc1	Publish
or	or	k?	or
perish	perish	k1gInSc1	perish
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
přibližně	přibližně	k6eAd1	přibližně
<g/>
:	:	kIx,	:
Publikuj	publikovat	k5eAaBmRp2nS	publikovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zahyň	zahynout	k5eAaPmRp2nS	zahynout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
značně	značně	k6eAd1	značně
expandovaly	expandovat	k5eAaImAgFnP	expandovat
ve	v	k7c6	v
vysokoškolském	vysokoškolský	k2eAgNnSc6d1	vysokoškolské
pregraduálním	pregraduální	k2eAgNnSc6d1	pregraduální
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
(	(	kIx(	(
<g/>
bakalářské	bakalářský	k2eAgInPc1d1	bakalářský
programy	program	k1gInPc1	program
<g/>
)	)	kIx)	)
a	a	k8xC	a
též	též	k9	též
značně	značně	k6eAd1	značně
přidaly	přidat	k5eAaPmAgInP	přidat
programy	program	k1gInPc1	program
magisterské	magisterský	k2eAgInPc1d1	magisterský
<g/>
,	,	kIx,	,
či	či	k8xC	či
doktorské	doktorský	k2eAgInPc4d1	doktorský
<g/>
,	,	kIx,	,
orientované	orientovaný	k2eAgInPc4d1	orientovaný
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
graduální	graduální	k2eAgFnSc1d1	graduální
<g/>
,	,	kIx,	,
či	či	k8xC	či
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
uvedených	uvedený	k2eAgFnPc2d1	uvedená
fakult	fakulta	k1gFnPc2	fakulta
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
vhodný	vhodný	k2eAgInSc4d1	vhodný
záznam	záznam	k1gInSc4	záznam
o	o	k7c6	o
publikační	publikační	k2eAgFnSc6d1	publikační
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
výzkumných	výzkumný	k2eAgInPc6d1	výzkumný
grantech	grant	k1gInPc6	grant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zásada	zásada	k1gFnSc1	zásada
"	"	kIx"	"
<g/>
Publish	Publish	k1gInSc1	Publish
or	or	k?	or
perish	perish	k1gInSc1	perish
<g/>
"	"	kIx"	"
stala	stát	k5eAaPmAgFnS	stát
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
důležitější	důležitý	k2eAgFnSc1d2	důležitější
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
menších	malý	k2eAgFnPc6d2	menší
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovšem	ovšem	k9	ovšem
platy	plat	k1gInPc1	plat
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
pracovníků	pracovník	k1gMnPc2	pracovník
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
oborech	obor	k1gInPc6	obor
menší	malý	k2eAgInSc1d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Požadavky	požadavek	k1gInPc1	požadavek
==	==	k?	==
</s>
</p>
<p>
<s>
Podrobné	podrobný	k2eAgInPc4d1	podrobný
požadavky	požadavek	k1gInPc4	požadavek
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
studenta	student	k1gMnSc4	student
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
absolvovat	absolvovat	k5eAaPmF	absolvovat
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
té	ten	k3xDgFnSc3	ten
magisterské	magisterský	k2eAgFnSc3d1	magisterská
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
úrovně	úroveň	k1gFnSc2	úroveň
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
(	(	kIx(	(
<g/>
magisterského	magisterský	k2eAgInSc2d1	magisterský
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
případně	případně	k6eAd1	případně
Honours	Honours	k1gInSc4	Honours
degree	degre	k1gFnSc2	degre
<g/>
.	.	kIx.	.
</s>
<s>
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Honours	Honours	k6eAd1	Honours
degree	degreat	k5eAaPmIp3nS	degreat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
honorovaný	honorovaný	k2eAgMnSc1d1	honorovaný
bakalář	bakalář	k1gMnSc1	bakalář
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
získat	získat	k5eAaPmF	získat
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
klasického	klasický	k2eAgInSc2d1	klasický
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
Bachelor	Bachelor	k1gInSc1	Bachelor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tříletého	tříletý	k2eAgNnSc2d1	tříleté
<g/>
,	,	kIx,	,
následovaného	následovaný	k2eAgNnSc2d1	následované
roční	roční	k2eAgInSc1d1	roční
nástavbou	nástavba	k1gFnSc7	nástavba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
ročního	roční	k2eAgInSc2d1	roční
"	"	kIx"	"
<g/>
honorovaného	honorovaný	k2eAgNnSc2d1	honorované
<g/>
"	"	kIx"	"
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
většinou	většinou	k6eAd1	většinou
požadován	požadován	k2eAgInSc1d1	požadován
větší	veliký	k2eAgInSc1d2	veliký
rozsah	rozsah	k1gInSc1	rozsah
než	než	k8xS	než
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
přijetí	přijetí	k1gNnSc3	přijetí
k	k	k7c3	k
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studiu	studio	k1gNnSc6	studio
může	moct	k5eAaImIp3nS	moct
často	často	k6eAd1	často
předcházet	předcházet	k5eAaImF	předcházet
předložení	předložení	k1gNnSc4	předložení
kopií	kopie	k1gFnPc2	kopie
důležitých	důležitý	k2eAgFnPc2d1	důležitá
akademických	akademický	k2eAgFnPc2d1	akademická
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
doporučujících	doporučující	k2eAgInPc2d1	doporučující
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
požadovány	požadován	k2eAgFnPc1d1	požadována
zformulovaný	zformulovaný	k2eAgInSc1d1	zformulovaný
záměr	záměr	k1gInSc1	záměr
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
osobní	osobní	k2eAgNnSc4d1	osobní
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
univerzit	univerzita	k1gFnPc2	univerzita
také	také	k9	také
před	před	k7c7	před
přijetím	přijetí	k1gNnSc7	přijetí
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
zve	zvát	k5eAaImIp3nS	zvát
uchazeče	uchazeč	k1gMnPc4	uchazeč
na	na	k7c4	na
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
pohovor	pohovor	k1gInSc4	pohovor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kandidát	kandidát	k1gMnSc1	kandidát
musí	muset	k5eAaImIp3nS	muset
předložit	předložit	k5eAaPmF	předložit
určitý	určitý	k2eAgInSc4d1	určitý
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
disertaci	disertace	k1gFnSc4	disertace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
skládající	skládající	k2eAgMnSc1d1	skládající
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
akademického	akademický	k2eAgInSc2d1	akademický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
hoden	hoden	k2eAgInSc1d1	hoden
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
recenzovaném	recenzovaný	k2eAgInSc6d1	recenzovaný
časopise	časopis	k1gInSc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
musí	muset	k5eAaImIp3nP	muset
kandidát	kandidát	k1gMnSc1	kandidát
obhájit	obhájit	k5eAaPmF	obhájit
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
před	před	k7c7	před
komisí	komise	k1gFnSc7	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
z	z	k7c2	z
odborníků	odborník	k1gMnPc2	odborník
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
disertace	disertace	k1gFnSc1	disertace
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
komisí	komise	k1gFnSc7	komise
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
disertace	disertace	k1gFnSc1	disertace
v	v	k7c6	v
principu	princip	k1gInSc6	princip
na	na	k7c6	na
odpovídající	odpovídající	k2eAgFnSc6d1	odpovídající
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
oznamují	oznamovat	k5eAaImIp3nP	oznamovat
případné	případný	k2eAgInPc4d1	případný
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyřešit	vyřešit	k5eAaPmF	vyřešit
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
disertaci	disertace	k1gFnSc4	disertace
nechat	nechat	k5eAaPmF	nechat
projít	projít	k5eAaPmF	projít
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ji	on	k3xPp3gFnSc4	on
schválit	schválit	k5eAaPmF	schválit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
nemluvících	mluvící	k2eNgFnPc6d1	nemluvící
zemích	zem	k1gFnPc6	zem
začaly	začít	k5eAaPmAgFnP	začít
přijímat	přijímat	k5eAaImF	přijímat
podobné	podobný	k2eAgInPc4d1	podobný
standardy	standard	k1gInPc4	standard
anglofonním	anglofonní	k2eAgInSc7d1	anglofonní
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
programům	program	k1gInPc3	program
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
doktoráty	doktorát	k1gInPc4	doktorát
orientované	orientovaný	k2eAgInPc4d1	orientovaný
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
student	student	k1gMnSc1	student
či	či	k8xC	či
kandidát	kandidát	k1gMnSc1	kandidát
obvykle	obvykle	k6eAd1	obvykle
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
akademické	akademický	k2eAgFnSc6d1	akademická
půdě	půda	k1gFnSc6	půda
pod	pod	k7c7	pod
úzkým	úzký	k2eAgInSc7d1	úzký
dohledem	dohled	k1gInSc7	dohled
<g/>
,	,	kIx,	,
supervizí	supervize	k1gFnSc7	supervize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
popularitou	popularita	k1gFnSc7	popularita
distančního	distanční	k2eAgNnSc2d1	distanční
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
technologií	technologie	k1gFnSc7	technologie
e-learningu	eearning	k1gInSc2	e-learning
některé	některý	k3yIgFnPc1	některý
univerzity	univerzita	k1gFnPc1	univerzita
nyní	nyní	k6eAd1	nyní
akceptují	akceptovat	k5eAaBmIp3nP	akceptovat
studenty	student	k1gMnPc4	student
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
do	do	k7c2	do
distančního	distanční	k2eAgNnSc2d1	distanční
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kombinovaného	kombinovaný	k2eAgNnSc2d1	kombinované
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vhodně	vhodně	k6eAd1	vhodně
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
částečně	částečně	k6eAd1	částečně
prezenční	prezenční	k2eAgNnSc4d1	prezenční
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
distanční	distanční	k2eAgNnSc4d1	distanční
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
částečný	částečný	k2eAgInSc4d1	částečný
úvazek	úvazek	k1gInSc4	úvazek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
programy	program	k1gInPc4	program
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
sendvičové	sendvičový	k2eAgNnSc1d1	sendvičové
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sandwich	sandwich	k1gInSc1	sandwich
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studenti	student	k1gMnPc1	student
netráví	trávit	k5eNaImIp3nP	trávit
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k6eAd1	místo
toto	tento	k3xDgNnSc1	tento
stráví	strávit	k5eAaPmIp3nS	strávit
první	první	k4xOgFnSc1	první
a	a	k8xC	a
poslední	poslední	k2eAgNnPc1d1	poslední
období	období	k1gNnPc1	období
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
domovských	domovský	k2eAgFnPc6d1	domovská
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
provádí	provádět	k5eAaImIp3nS	provádět
výzkum	výzkum	k1gInSc4	výzkum
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potvrzení	potvrzený	k2eAgMnPc1d1	potvrzený
PhD	PhD	k1gMnPc1	PhD
===	===	k?	===
</s>
</p>
<p>
<s>
Potvrzení	potvrzený	k2eAgMnPc1d1	potvrzený
PhD	PhD	k1gMnPc1	PhD
(	(	kIx(	(
<g/>
pojednání	pojednání	k1gNnSc1	pojednání
k	k	k7c3	k
disertační	disertační	k2eAgFnSc3d1	disertační
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
PhD	PhD	k1gFnSc1	PhD
confirmation	confirmation	k1gInSc1	confirmation
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
předběžná	předběžný	k2eAgFnSc1d1	předběžná
prezentace	prezentace	k1gFnSc1	prezentace
či	či	k8xC	či
přednáška	přednáška	k1gFnSc1	přednáška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
PhD	PhD	k1gFnSc6	PhD
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
fakultě	fakulta	k1gFnSc3	fakulta
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
dalším	další	k2eAgMnPc3d1	další
zainteresovaným	zainteresovaný	k2eAgMnPc3d1	zainteresovaný
členům	člen	k1gMnPc3	člen
<g/>
.	.	kIx.	.
</s>
<s>
Přednáška	přednáška	k1gFnSc1	přednáška
následuje	následovat	k5eAaImIp3nS	následovat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
vhodné	vhodný	k2eAgNnSc1d1	vhodné
téma	téma	k1gNnSc1	téma
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
takové	takový	k3xDgFnPc4	takový
náležitosti	náležitost	k1gFnPc4	náležitost
jako	jako	k8xC	jako
účel	účel	k1gInSc4	účel
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
použitou	použitý	k2eAgFnSc4d1	použitá
metodologii	metodologie	k1gFnSc4	metodologie
<g/>
,	,	kIx,	,
předběžné	předběžný	k2eAgInPc4d1	předběžný
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
očekávání	očekávání	k1gNnPc4	očekávání
<g/>
,	,	kIx,	,
plánované	plánovaný	k2eAgFnPc4d1	plánovaná
nebo	nebo	k8xC	nebo
hotové	hotový	k2eAgFnPc4d1	hotová
publikace	publikace	k1gFnPc4	publikace
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Potvrzení	potvrzení	k1gNnSc1	potvrzení
přednášky	přednáška	k1gFnSc2	přednáška
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
shledáno	shledat	k5eAaPmNgNnS	shledat
jako	jako	k8xC	jako
předběžná	předběžný	k2eAgFnSc1d1	předběžná
zkušební	zkušební	k2eAgFnSc1d1	zkušební
veřejná	veřejný	k2eAgFnSc1d1	veřejná
obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
členové	člen	k1gMnPc1	člen
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
směr	směr	k1gInSc4	směr
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
přednášky	přednáška	k1gFnSc2	přednáška
PhD	PhD	k1gMnSc1	PhD
kandidát	kandidát	k1gMnSc1	kandidát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
shledán	shledat	k5eAaPmNgMnS	shledat
jako	jako	k9	jako
potvrzený	potvrzený	k2eAgMnSc1d1	potvrzený
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
členové	člen	k1gMnPc1	člen
fakulty	fakulta	k1gFnSc2	fakulta
uvedené	uvedený	k2eAgFnSc2d1	uvedená
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
a	a	k8xC	a
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studie	studie	k1gFnSc1	studie
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
řízena	řízen	k2eAgFnSc1d1	řízena
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
končit	končit	k5eAaImF	končit
u	u	k7c2	u
kandidáta	kandidát	k1gMnSc2	kandidát
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnota	hodnota	k1gFnSc1	hodnota
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Práce	práce	k1gFnPc1	práce
v	v	k7c6	v
univerzitním	univerzitní	k2eAgNnSc6d1	univerzitní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c6	na
akademické	akademický	k2eAgFnSc6d1	akademická
půdě	půda	k1gFnSc6	půda
obvykle	obvykle	k6eAd1	obvykle
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
doktorát	doktorát	k1gInSc1	doktorát
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc1	PhD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pozici	pozice	k1gFnSc4	pozice
i	i	k9	i
bez	bez	k7c2	bez
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
lze	lze	k6eAd1	lze
obecně	obecně	k6eAd1	obecně
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesoři	profesor	k1gMnPc1	profesor
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc4	PhD
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
povinni	povinen	k2eAgMnPc1d1	povinen
jej	on	k3xPp3gMnSc4	on
mít	mít	k5eAaImF	mít
–	–	k?	–
uvedené	uvedený	k2eAgMnPc4d1	uvedený
je	být	k5eAaImIp3nS	být
užíváno	užíván	k2eAgNnSc4d1	užíváno
jakožto	jakožto	k8xS	jakožto
určité	určitý	k2eAgNnSc4d1	určité
měřítko	měřítko	k1gNnSc4	měřítko
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motivace	motivace	k1gFnSc1	motivace
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
PhD	PhD	k1gFnSc2	PhD
může	moct	k5eAaImIp3nS	moct
též	též	k9	též
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
budoucí	budoucí	k2eAgNnSc4d1	budoucí
zvýšení	zvýšení	k1gNnSc4	zvýšení
příjmu	příjem	k1gInSc2	příjem
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
toto	tento	k3xDgNnSc1	tento
není	být	k5eNaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
H.	H.	kA	H.
Casey	Casea	k1gFnPc1	Casea
z	z	k7c2	z
Univesity	Univesit	k1gInPc1	Univesit
of	of	k?	of
Warwick	Warwick	k1gInSc1	Warwick
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
výzkumem	výzkum	k1gInSc7	výzkum
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgInPc7	všecek
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
PhD	PhD	k1gFnPc7	PhD
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
navýšení	navýšení	k1gNnSc1	navýšení
příjmu	příjem	k1gInSc2	příjem
o	o	k7c4	o
26	[number]	k4	26
%	%	kIx~	%
(	(	kIx(	(
<g/>
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgInPc7	všecek
obory	obor	k1gInPc7	obor
<g/>
)	)	kIx)	)
–	–	k?	–
doktorské	doktorský	k2eAgNnSc1d1	doktorské
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc1	PhD
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
uvedené	uvedený	k2eAgFnPc4d1	uvedená
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
magisterské	magisterský	k2eAgFnSc2d1	magisterská
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
23	[number]	k4	23
%	%	kIx~	%
a	a	k8xC	a
bakalářské	bakalářský	k2eAgInPc1d1	bakalářský
14	[number]	k4	14
%	%	kIx~	%
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
též	též	k9	též
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
benefity	benefita	k1gFnPc1	benefita
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
dalšího	další	k2eAgNnSc2d1	další
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
či	či	k8xC	či
výcviku	výcvik	k1gInSc2	výcvik
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
z	z	k7c2	z
PhD	PhD	k1gFnSc2	PhD
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ačkoli	ačkoli	k8xS	ačkoli
některé	některý	k3yIgInPc1	některý
výzkumy	výzkum	k1gInPc1	výzkum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
překvalifikovaní	překvalifikovaný	k2eAgMnPc1d1	překvalifikovaný
pracovníci	pracovník	k1gMnPc1	pracovník
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
méně	málo	k6eAd2	málo
spokojení	spokojení	k1gNnSc2	spokojení
a	a	k8xC	a
méně	málo	k6eAd2	málo
produktivní	produktivní	k2eAgFnSc1d1	produktivní
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obtíže	obtíž	k1gFnPc1	obtíž
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
pociťovány	pociťován	k2eAgInPc1d1	pociťován
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
absolventy	absolvent	k1gMnPc4	absolvent
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
často	často	k6eAd1	často
musejí	muset	k5eAaImIp3nP	muset
zadlužit	zadlužit	k5eAaPmF	zadlužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
uvedené	uvedený	k2eAgInPc4d1	uvedený
zdárně	zdárně	k6eAd1	zdárně
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řádný	řádný	k2eAgInSc1d1	řádný
doktorát	doktorát	k1gInSc1	doktorát
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc1	PhD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyžadován	vyžadován	k2eAgInSc1d1	vyžadován
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
pozicích	pozice	k1gFnPc6	pozice
mimo	mimo	k7c4	mimo
akademickou	akademický	k2eAgFnSc4d1	akademická
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
farmacie	farmacie	k1gFnSc1	farmacie
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadnárodních	nadnárodní	k2eAgFnPc6d1	nadnárodní
korporacích	korporace	k1gFnPc6	korporace
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
v	v	k7c6	v
USA	USA	kA	USA
často	často	k6eAd1	často
nabízí	nabízet	k5eAaImIp3nS	nabízet
malou	malý	k2eAgFnSc4d1	malá
pobídku	pobídka	k1gFnSc4	pobídka
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
studentů	student	k1gMnPc2	student
v	v	k7c6	v
PhD	PhD	k1gFnPc6	PhD
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
ekvivaletního	ekvivaletní	k2eAgInSc2d1	ekvivaletní
Doctor	Doctor	k1gInSc4	Doctor
of	of	k?	of
Arts	Artsa	k1gFnPc2	Artsa
bylo	být	k5eAaImAgNnS	být
zkrátit	zkrátit	k5eAaPmF	zkrátit
čas	čas	k1gInSc4	čas
potřebný	potřebný	k2eAgInSc4d1	potřebný
ke	k	k7c3	k
zdárnému	zdárný	k2eAgNnSc3d1	zdárné
dokončení	dokončení	k1gNnSc3	dokončení
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zaměřením	zaměření	k1gNnSc7	zaměření
se	se	k3xPyFc4	se
na	na	k7c4	na
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Arts	Arts	k1gInSc4	Arts
programy	program	k1gInPc1	program
stále	stále	k6eAd1	stále
též	též	k9	též
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
určitou	určitý	k2eAgFnSc4d1	určitá
signifikantní	signifikantní	k2eAgFnSc4d1	signifikantní
výzkumovou	výzkumový	k2eAgFnSc4d1	výzkumový
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klást	klást	k5eAaImF	klást
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
titul	titul	k1gInSc4	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
srovnatelné	srovnatelný	k2eAgInPc4d1	srovnatelný
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdybyste	kdyby	kYmCp2nP	kdyby
se	se	k3xPyFc4	se
ptali	ptat	k5eAaImAgMnP	ptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
umění	umění	k1gNnPc2	umění
nebo	nebo	k8xC	nebo
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
znalosti	znalost	k1gFnPc1	znalost
se	se	k3xPyFc4	se
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
šíří	šířit	k5eAaImIp3nP	šířit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
stává	stávat	k5eAaImIp3nS	stávat
produktivnější	produktivní	k2eAgInSc1d2	produktivnější
a	a	k8xC	a
zdravější	zdravý	k2eAgInSc1d2	zdravější
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
a	a	k8xC	a
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
problém	problém	k1gInSc1	problém
též	též	k6eAd1	též
<g />
.	.	kIx.	.
</s>
<s>
tzv.	tzv.	kA	tzv.
inbreeding	inbreeding	k1gInSc1	inbreeding
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
například	například	k6eAd1	například
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
student	student	k1gMnSc1	student
něco	něco	k3yInSc4	něco
vystuduje	vystudovat	k5eAaPmIp3nS	vystudovat
<g/>
,	,	kIx,	,
udělá	udělat	k5eAaPmIp3nS	udělat
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
i	i	k9	i
doktorát	doktorát	k1gInSc1	doktorát
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
už	už	k6eAd1	už
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
samé	samý	k3xTgFnSc6	samý
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
pak	pak	k6eAd1	pak
kupříkladu	kupříkladu	k6eAd1	kupříkladu
i	i	k8xC	i
časem	časem	k6eAd1	časem
nahradí	nahradit	k5eAaPmIp3nS	nahradit
tohoto	tento	k3xDgMnSc2	tento
svého	svůj	k1gMnSc2	svůj
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
doktorát	doktorát	k1gInSc1	doktorát
dělal	dělat	k5eAaImAgInS	dělat
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
"	"	kIx"	"
<g/>
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
skupinky	skupinka	k1gFnPc1	skupinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
o	o	k7c4	o
sobě	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
špici	špice	k1gFnSc6	špice
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
regionální	regionální	k2eAgFnPc4d1	regionální
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
<g/>
:	:	kIx,	:
Akademický	akademický	k2eAgInSc1d1	akademický
inbreeding	inbreeding	k1gInSc1	inbreeding
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgFnSc2d1	národní
varianty	varianta	k1gFnSc2	varianta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
či	či	k8xC	či
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
nástupnických	nástupnický	k2eAgInPc6d1	nástupnický
státech	stát	k1gInPc6	stát
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
částí	část	k1gFnPc2	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
španělsky	španělsky	k6eAd1	španělsky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
stupeň	stupeň	k1gInSc1	stupeň
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Philosophy	Philosopha	k1gMnSc2	Philosopha
<g/>
)	)	kIx)	)
nazýván	nazývat	k5eAaImNgInS	nazývat
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
jen	jen	k6eAd1	jen
"	"	kIx"	"
<g/>
Doctor	Doctor	k1gInSc1	Doctor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
doktora	doktor	k1gMnSc4	doktor
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
užívá	užívat	k5eAaImIp3nS	užívat
de	de	k?	de
facto	facto	k1gNnSc1	facto
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obdobách	obdoba	k1gFnPc6	obdoba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
charakter	charakter	k1gInSc1	charakter
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
též	též	k9	též
díky	díky	k7c3	díky
Boloňskému	boloňský	k2eAgInSc3d1	boloňský
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
doktorátu	doktorát	k1gInSc3	doktorát
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
orientováno	orientovat	k5eAaBmNgNnS	orientovat
akademicky	akademicky	k6eAd1	akademicky
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
individuálně	individuálně	k6eAd1	individuálně
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
například	například	k6eAd1	například
od	od	k7c2	od
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
doktorát	doktorát	k1gInSc4	doktorát
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chodí	chodit	k5eAaImIp3nS	chodit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
za	za	k7c7	za
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
nadnárodní	nadnárodní	k2eAgFnSc4d1	nadnárodní
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Evropský	evropský	k2eAgInSc1d1	evropský
doktorát	doktorát	k1gInSc1	doktorát
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gMnSc1	Doctor
Europaeus	Europaeus	k1gMnSc1	Europaeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
nutností	nutnost	k1gFnSc7	nutnost
pro	pro	k7c4	pro
udělení	udělení	k1gNnSc4	udělení
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
také	také	k9	také
často	často	k6eAd1	často
PhD	PhD	k1gFnSc2	PhD
studium	studium	k1gNnSc1	studium
nebo	nebo	k8xC	nebo
doktorandské	doktorandský	k2eAgNnSc1d1	doktorandské
studium	studium	k1gNnSc1	studium
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
aspirantské	aspirantský	k2eAgNnSc1d1	aspirantský
studium	studium	k1gNnSc1	studium
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
(	(	kIx(	(
<g/>
umělecká	umělecký	k2eAgFnSc1d1	umělecká
<g/>
)	)	kIx)	)
aspirantura	aspirantura	k1gFnSc1	aspirantura
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
výchova	výchova	k1gFnSc1	výchova
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
postgraduál	postgraduál	k1gInSc1	postgraduál
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dnes	dnes	k6eAd1	dnes
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
<g/>
)	)	kIx)	)
v	v	k7c6	v
trvání	trvání	k1gNnSc6	trvání
3-4	[number]	k4	3-4
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
individuálního	individuální	k2eAgInSc2d1	individuální
studijního	studijní	k2eAgInSc2d1	studijní
plánu	plán	k1gInSc2	plán
(	(	kIx(	(
<g/>
ISP	ISP	kA	ISP
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
školitele	školitel	k1gMnSc2	školitel
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
vědecké	vědecký	k2eAgNnSc4d1	vědecké
bádání	bádání	k1gNnSc4	bádání
a	a	k8xC	a
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výzkumu	výzkum	k1gInSc2	výzkum
nebo	nebo	k8xC	nebo
vývoje	vývoj	k1gInSc2	vývoj
nebo	nebo	k8xC	nebo
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
studiu	studio	k1gNnSc3	studio
předchází	předcházet	k5eAaImIp3nS	předcházet
vždy	vždy	k6eAd1	vždy
studium	studium	k1gNnSc4	studium
magisterské	magisterský	k2eAgNnSc4d1	magisterské
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
pětileté	pětiletý	k2eAgNnSc1d1	pětileté
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
např.	např.	kA	např.
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
lékařské	lékařský	k2eAgInPc4d1	lékařský
obory	obor	k1gInPc4	obor
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
souvislé	souvislý	k2eAgNnSc4d1	souvislé
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
MUDr.	MUDr.	kA	MUDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
např.	např.	kA	např.
uměleckých	umělecký	k2eAgInPc6d1	umělecký
oborech	obor	k1gInPc6	obor
jde	jít	k5eAaImIp3nS	jít
zase	zase	k9	zase
o	o	k7c4	o
souvislé	souvislý	k2eAgNnSc4d1	souvislé
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
MgA.	MgA.	k1gMnSc1	MgA.
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
PhD	PhD	k1gMnSc1	PhD
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
i	i	k9	i
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
i	i	k9	i
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nostrifikace	nostrifikace	k1gFnSc1	nostrifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Absolvovaný	absolvovaný	k2eAgInSc1d1	absolvovaný
poslední	poslední	k2eAgInSc1d1	poslední
cyklus	cyklus	k1gInSc1	cyklus
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
požadován	požadovat	k5eAaImNgInS	požadovat
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
nadnárodních	nadnárodní	k2eAgFnPc6d1	nadnárodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
vědce	vědec	k1gMnSc4	vědec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
též	též	k9	též
o	o	k7c4	o
nezbytný	nezbytný	k2eAgInSc4d1	nezbytný
požadavek	požadavek	k1gInSc4	požadavek
pro	pro	k7c4	pro
případné	případný	k2eAgNnSc4d1	případné
pozdější	pozdní	k2eAgNnSc4d2	pozdější
získaní	získaný	k2eAgMnPc1d1	získaný
doktorátu	doktorát	k1gInSc2	doktorát
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
bývá	bývat	k5eAaImIp3nS	bývat
klíčovým	klíčový	k2eAgInSc7d1	klíčový
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
vysokoškolského	vysokoškolský	k2eAgMnSc4d1	vysokoškolský
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
působení	působení	k1gNnSc4	působení
jakožto	jakožto	k8xS	jakožto
odborný	odborný	k2eAgMnSc1d1	odborný
asistent	asistent	k1gMnSc1	asistent
<g/>
,	,	kIx,	,
či	či	k8xC	či
následně	následně	k6eAd1	následně
jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
habilitačního	habilitační	k2eAgNnSc2d1	habilitační
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pozice	pozice	k1gFnSc2	pozice
associate	associat	k1gInSc5	associat
professor	professora	k1gFnPc2	professora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
řízení	řízení	k1gNnSc2	řízení
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pozice	pozice	k1gFnSc2	pozice
professor	professora	k1gFnPc2	professora
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
lektora	lektor	k1gMnSc4	lektor
nebo	nebo	k8xC	nebo
asistenta	asistent	k1gMnSc4	asistent
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
odůvodněných	odůvodněný	k2eAgInPc6d1	odůvodněný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
jmenování	jmenování	k1gNnSc4	jmenování
docentem	docent	k1gMnSc7	docent
<g/>
,	,	kIx,	,
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
umělecké	umělecký	k2eAgFnPc4d1	umělecká
obory	obora	k1gFnPc4	obora
pak	pak	k6eAd1	pak
zákon	zákon	k1gInSc1	zákon
připouští	připouštět	k5eAaImIp3nS	připouštět
výjimku	výjimka	k1gFnSc4	výjimka
a	a	k8xC	a
pro	pro	k7c4	pro
jmenování	jmenování	k1gNnSc4	jmenování
docentem	docent	k1gMnSc7	docent
<g/>
,	,	kIx,	,
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vůbec	vůbec	k9	vůbec
absolvování	absolvování	k1gNnSc2	absolvování
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
nezbytně	nezbytně	k6eAd1	nezbytně
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Docent	docent	k1gMnSc1	docent
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vědecko-pedagogické	vědeckoedagogický	k2eAgFnSc2d1	vědecko-pedagogická
(	(	kIx(	(
<g/>
či	či	k8xC	či
umělecko-pedagogické	uměleckoedagogický	k2eAgFnPc4d1	umělecko-pedagogický
<g/>
)	)	kIx)	)
hodnosti	hodnost	k1gFnPc4	hodnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
splnit	splnit	k5eAaPmF	splnit
určité	určitý	k2eAgFnPc4d1	určitá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc4	činnost
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
atp.	atp.	kA	atp.
(	(	kIx(	(
<g/>
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
studium	studium	k1gNnSc4	studium
<g/>
/	/	kIx~	/
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Požadavky	požadavek	k1gInPc1	požadavek
====	====	k?	====
</s>
</p>
<p>
<s>
Náplní	náplň	k1gFnSc7	náplň
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
především	především	k9	především
výzkum	výzkum	k1gInSc1	výzkum
předem	předem	k6eAd1	předem
stanoveného	stanovený	k2eAgNnSc2d1	stanovené
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
uměleckého	umělecký	k2eAgInSc2d1	umělecký
<g/>
)	)	kIx)	)
úkolu	úkol	k1gInSc2	úkol
pod	pod	k7c7	pod
individuálním	individuální	k2eAgNnSc7d1	individuální
vedením	vedení	k1gNnSc7	vedení
školitele	školitel	k1gMnSc2	školitel
a	a	k8xC	a
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
oborové	oborový	k2eAgFnSc2d1	oborová
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Studovat	studovat	k5eAaImF	studovat
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
či	či	k8xC	či
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
formy	forma	k1gFnPc4	forma
studia	studio	k1gNnSc2	studio
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
rovnocenné	rovnocenný	k2eAgNnSc1d1	rovnocenné
(	(	kIx(	(
<g/>
dosažené	dosažený	k2eAgInPc1d1	dosažený
tituly	titul	k1gInPc1	titul
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
řádně	řádně	k6eAd1	řádně
zakončeno	zakončen	k2eAgNnSc1d1	zakončeno
složením	složení	k1gNnSc7	složení
státní	státní	k2eAgFnSc2d1	státní
doktorské	doktorský	k2eAgFnSc2d1	doktorská
zkoušky	zkouška	k1gFnSc2	zkouška
a	a	k8xC	a
obhájením	obhájení	k1gNnSc7	obhájení
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
student	student	k1gMnSc1	student
doktorského	doktorský	k2eAgInSc2d1	doktorský
programu	program	k1gInSc2	program
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
kvalitu	kvalita	k1gFnSc4	kvalita
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
nebo	nebo	k8xC	nebo
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
<g/>
;	;	kIx,	;
obhajobou	obhajoba	k1gFnSc7	obhajoba
se	se	k3xPyFc4	se
také	také	k9	také
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
schopnost	schopnost	k1gFnSc4	schopnost
a	a	k8xC	a
připravenost	připravenost	k1gFnSc4	připravenost
k	k	k7c3	k
samostatné	samostatný	k2eAgFnSc3d1	samostatná
činnosti	činnost	k1gFnSc3	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výzkumu	výzkum	k1gInSc2	výzkum
nebo	nebo	k8xC	nebo
vývoje	vývoj	k1gInSc2	vývoj
nebo	nebo	k8xC	nebo
k	k	k7c3	k
samostatné	samostatný	k2eAgFnSc3d1	samostatná
teoretické	teoretický	k2eAgFnPc4d1	teoretická
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
umělecké	umělecký	k2eAgFnPc4d1	umělecká
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Disertační	disertační	k2eAgFnPc1d1	disertační
práce	práce	k1gFnPc1	práce
musí	muset	k5eAaImIp3nP	muset
dle	dle	k7c2	dle
českého	český	k2eAgInSc2d1	český
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
obsahovat	obsahovat	k5eAaImF	obsahovat
"	"	kIx"	"
<g/>
původní	původní	k2eAgMnSc1d1	původní
a	a	k8xC	a
uveřejněné	uveřejněný	k2eAgInPc1d1	uveřejněný
výsledky	výsledek	k1gInPc1	výsledek
nebo	nebo	k8xC	nebo
výsledky	výsledek	k1gInPc1	výsledek
přijaté	přijatý	k2eAgInPc1d1	přijatý
k	k	k7c3	k
uveřejnění	uveřejnění	k1gNnSc3	uveřejnění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Školitel	školitel	k1gMnSc1	školitel
není	být	k5eNaImIp3nS	být
pouhým	pouhý	k2eAgMnSc7d1	pouhý
vedoucím	vedoucí	k1gMnSc7	vedoucí
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
kvalifikační	kvalifikační	k2eAgFnSc2d1	kvalifikační
práce	práce	k1gFnSc2	práce
doktoranda	doktorand	k1gMnSc2	doktorand
(	(	kIx(	(
<g/>
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dozoruje	dozorovat	k5eAaImIp3nS	dozorovat
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
celé	celý	k2eAgNnSc1d1	celé
jeho	jeho	k3xOp3gNnSc4	jeho
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
odbornou	odborný	k2eAgFnSc4d1	odborná
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
přípravu	příprava	k1gFnSc4	příprava
doktoranda	doktorand	k1gMnSc2	doktorand
<g/>
,	,	kIx,	,
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
se	s	k7c7	s
studentem	student	k1gMnSc7	student
jeho	on	k3xPp3gInSc2	on
ISP	ISP	kA	ISP
<g/>
,	,	kIx,	,
předkládá	předkládat	k5eAaImIp3nS	předkládat
předsedovi	předseda	k1gMnSc3	předseda
oborové	oborový	k2eAgFnSc2d1	oborová
rady	rada	k1gFnSc2	rada
roční	roční	k2eAgNnSc4d1	roční
hodnocení	hodnocení	k1gNnSc4	hodnocení
doktoranda	doktorand	k1gMnSc2	doktorand
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
děkanovi	děkan	k1gMnSc3	děkan
např.	např.	kA	např.
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
stáže	stáž	k1gFnSc2	stáž
doktoranda	doktorand	k1gMnSc2	doktorand
atd.	atd.	kA	atd.
Školitelem	školitel	k1gMnSc7	školitel
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
docent	docent	k1gMnSc1	docent
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
též	též	k9	též
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
odborník	odborník	k1gMnSc1	odborník
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
doktor	doktor	k1gMnSc1	doktor
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
vyučována	vyučován	k2eAgFnSc1d1	vyučována
metodologie	metodologie	k1gFnSc1	metodologie
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
zásady	zásada	k1gFnPc4	zásada
vědecké	vědecký	k2eAgFnSc2d1	vědecká
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
odborné	odborný	k2eAgInPc4d1	odborný
předměty	předmět	k1gInPc4	předmět
dle	dle	k7c2	dle
specializace	specializace	k1gFnSc2	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Kredity	kredit	k1gInPc1	kredit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
udílejí	udílet	k5eAaImIp3nP	udílet
za	za	k7c4	za
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
absolvované	absolvovaný	k2eAgInPc4d1	absolvovaný
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
výstupy	výstup	k1gInPc4	výstup
na	na	k7c6	na
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
konferencích	konference	k1gFnPc6	konference
<g/>
,	,	kIx,	,
absolvované	absolvovaný	k2eAgFnPc1d1	absolvovaná
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
stáže	stáž	k1gFnPc4	stáž
<g/>
,	,	kIx,	,
publikované	publikovaný	k2eAgInPc4d1	publikovaný
články	článek	k1gInPc4	článek
atd.	atd.	kA	atd.
Doktorand	doktorand	k1gMnSc1	doktorand
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
předepsán	předepsat	k5eAaPmNgInS	předepsat
určitý	určitý	k2eAgInSc1d1	určitý
počet	počet	k1gInSc1	počet
hodin	hodina	k1gFnPc2	hodina
pro	pro	k7c4	pro
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
přímou	přímý	k2eAgFnSc4d1	přímá
výuku	výuka	k1gFnSc4	výuka
<g/>
,	,	kIx,	,
semináře	seminář	k1gInPc1	seminář
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
cvičení	cvičení	k1gNnSc1	cvičení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
konzultace	konzultace	k1gFnPc4	konzultace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
programu	program	k1gInSc6	program
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
oborová	oborový	k2eAgFnSc1d1	oborová
rada	rada	k1gFnSc1	rada
ustavená	ustavený	k2eAgFnSc1d1	ustavená
podle	podle	k7c2	podle
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
předpisu	předpis	k1gInSc2	předpis
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc2	její
součásti	součást	k1gFnSc2	součást
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
akreditovaný	akreditovaný	k2eAgInSc4d1	akreditovaný
příslušný	příslušný	k2eAgInSc4d1	příslušný
doktorský	doktorský	k2eAgInSc4d1	doktorský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
oblasti	oblast	k1gFnSc2	oblast
studia	studio	k1gNnSc2	studio
mohou	moct	k5eAaImIp3nP	moct
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc1	jejich
součásti	součást	k1gFnPc1	součást
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
vytvořit	vytvořit	k5eAaPmF	vytvořit
společnou	společný	k2eAgFnSc4d1	společná
oborovou	oborový	k2eAgFnSc4d1	oborová
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Předsedu	předseda	k1gMnSc4	předseda
oborové	oborový	k2eAgFnSc2d1	oborová
rady	rada	k1gFnSc2	rada
volí	volit	k5eAaImIp3nS	volit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	středa	k1gFnSc4	středa
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
předsedou	předseda	k1gMnSc7	předseda
oborové	oborový	k2eAgFnSc2d1	oborová
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
garant	garant	k1gMnSc1	garant
doktorského	doktorský	k2eAgInSc2d1	doktorský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
<g/>
Doktorand	doktorand	k1gMnSc1	doktorand
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
svého	svůj	k3xOyFgMnSc2	svůj
školitele	školitel	k1gMnSc2	školitel
veřejně	veřejně	k6eAd1	veřejně
publikovat	publikovat	k5eAaBmF	publikovat
své	svůj	k3xOyFgInPc4	svůj
odborné	odborný	k2eAgInPc4d1	odborný
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
vědecké	vědecký	k2eAgFnPc4d1	vědecká
konference	konference	k1gFnPc4	konference
<g/>
,	,	kIx,	,
vést	vést	k5eAaImF	vést
či	či	k8xC	či
oponovat	oponovat	k5eAaImF	oponovat
bakalářské	bakalářský	k2eAgFnPc4d1	Bakalářská
práce	práce	k1gFnPc4	práce
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgInPc6d2	vyšší
ročnících	ročník	k1gInPc6	ročník
případně	případně	k6eAd1	případně
někdy	někdy	k6eAd1	někdy
též	též	k9	též
magisterské	magisterský	k2eAgFnSc2d1	magisterská
(	(	kIx(	(
<g/>
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc2	práce
apod.	apod.	kA	apod.
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
se	se	k3xPyFc4	se
v	v	k7c6	v
akademickém	akademický	k2eAgNnSc6d1	akademické
prostředí	prostředí	k1gNnSc6	prostředí
často	často	k6eAd1	často
hovorově	hovorově	k6eAd1	hovorově
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
postdoktorand	postdoktorand	k1gInSc1	postdoktorand
(	(	kIx(	(
<g/>
postdoc	postdoc	k1gFnSc1	postdoc
<g/>
)	)	kIx)	)
–	–	k?	–
dotyčný	dotyčný	k2eAgInSc1d1	dotyčný
je	být	k5eAaImIp3nS	být
způsobilý	způsobilý	k2eAgInSc1d1	způsobilý
pro	pro	k7c4	pro
vykonávání	vykonávání	k1gNnSc4	vykonávání
činnosti	činnost	k1gFnSc2	činnost
odborného	odborný	k2eAgMnSc2d1	odborný
asistenta	asistent	k1gMnSc2	asistent
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
pracovně	pracovně	k6eAd1	pracovně
někdy	někdy	k6eAd1	někdy
zkracováno	zkracován	k2eAgNnSc4d1	zkracováno
odb	odb	k?	odb
<g/>
.	.	kIx.	.
as	as	k1gInSc1	as
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pozice	pozice	k1gFnSc2	pozice
assistant	assistant	k1gMnSc1	assistant
professor	professor	k1gMnSc1	professor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
vědeckého	vědecký	k2eAgMnSc4d1	vědecký
pracovníka	pracovník	k1gMnSc4	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Předchozí	předchozí	k2eAgFnPc1d1	předchozí
obdoby	obdoba	k1gFnPc1	obdoba
vědeckých	vědecký	k2eAgInPc2d1	vědecký
titulů	titul	k1gInPc2	titul
====	====	k?	====
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
absolventy	absolvent	k1gMnPc4	absolvent
doktorských	doktorský	k2eAgInPc2d1	doktorský
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
v	v	k7c6	v
ČR	ČR	kA	ČR
zaveden	zaveden	k2eAgInSc1d1	zaveden
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
)	)	kIx)	)
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gMnSc4	jeho
obdobou	obdoba	k1gFnSc7	obdoba
stejný	stejný	k2eAgInSc1d1	stejný
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
standardně	standardně	k6eAd1	standardně
tříleté	tříletý	k2eAgNnSc1d1	tříleté
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
o	o	k7c4	o
standardně	standardně	k6eAd1	standardně
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letý	letý	k2eAgInSc1d1	letý
"	"	kIx"	"
<g/>
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předcházejícím	předcházející	k2eAgNnSc6d1	předcházející
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
SSSR	SSSR	kA	SSSR
předcházel	předcházet	k5eAaImAgInS	předcházet
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
de	de	k?	de
facto	facto	k1gNnSc1	facto
o	o	k7c4	o
ekvivalenty	ekvivalent	k1gInPc4	ekvivalent
<g/>
.	.	kIx.	.
</s>
<s>
CSc.	CSc.	kA	CSc.
byla	být	k5eAaImAgFnS	být
přesněji	přesně	k6eAd2	přesně
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
vědecká	vědecký	k2eAgFnSc1d1	vědecká
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
udělovala	udělovat	k5eAaImAgFnS	udělovat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
též	též	k9	též
"	"	kIx"	"
<g/>
vyšší	vysoký	k2eAgFnSc1d2	vyšší
vědecká	vědecký	k2eAgFnSc1d1	vědecká
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
"	"	kIx"	"
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
variantu	varianta	k1gFnSc4	varianta
dnes	dnes	k6eAd1	dnes
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
zákon	zákon	k1gInSc4	zákon
<g/>
)	)	kIx)	)
nezná	znát	k5eNaImIp3nS	znát
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
dnes	dnes	k6eAd1	dnes
uděluje	udělovat	k5eAaImIp3nS	udělovat
DSc	DSc	k1gFnSc1	DSc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gFnSc4	jeho
určitou	určitý	k2eAgFnSc4d1	určitá
obdobu	obdoba	k1gFnSc4	obdoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastních	vlastní	k2eAgFnPc2d1	vlastní
stanov	stanova	k1gFnPc2	stanova
<g/>
.	.	kIx.	.
</s>
<s>
Doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
akreditována	akreditovat	k5eAaBmNgFnS	akreditovat
pouze	pouze	k6eAd1	pouze
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
dnes	dnes	k6eAd1	dnes
mohou	moct	k5eAaImIp3nP	moct
udělovat	udělovat	k5eAaImF	udělovat
pouze	pouze	k6eAd1	pouze
tyto	tento	k3xDgFnPc4	tento
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
instituce	instituce	k1gFnPc1	instituce
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
typicky	typicky	k6eAd1	typicky
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
"	"	kIx"	"
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
"	"	kIx"	"
<g/>
doktorské	doktorský	k2eAgInPc4d1	doktorský
<g/>
"	"	kIx"	"
tituly	titul	k1gInPc4	titul
====	====	k?	====
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
ČR	ČR	kA	ČR
hovorově	hovorově	k6eAd1	hovorově
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
tzv.	tzv.	kA	tzv.
malého	malý	k2eAgInSc2d1	malý
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
absolvování	absolvování	k1gNnSc6	absolvování
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
zkoušky	zkouška	k1gFnSc2	zkouška
(	(	kIx(	(
<g/>
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
ThDr.	ThDr.	k1gFnSc1	ThDr.
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
není	být	k5eNaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
titulu	titul	k1gInSc2	titul
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc4	titul
magisterského	magisterský	k2eAgInSc2d1	magisterský
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
značí	značit	k5eAaImIp3nS	značit
de	de	k?	de
facto	facto	k1gNnSc1	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
doktoráty	doktorát	k1gInPc1	doktorát
(	(	kIx(	(
<g/>
rigorózum	rigorózum	k1gNnSc1	rigorózum
<g/>
)	)	kIx)	)
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
historická	historický	k2eAgFnSc1d1	historická
záležitost	záležitost	k1gFnSc1	záležitost
či	či	k8xC	či
specifikum	specifikum	k1gNnSc1	specifikum
Česka	Česko	k1gNnSc2	Česko
či	či	k8xC	či
Slovenska	Slovensko	k1gNnSc2	Slovensko
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
nemají	mít	k5eNaImIp3nP	mít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
obdobu	obdoba	k1gFnSc4	obdoba
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc2	jejich
udělování	udělování	k1gNnSc2	udělování
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
srov.	srov.	kA	srov.
J.D.	J.D.	k1gFnSc2	J.D.
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
žádný	žádný	k3yNgInSc4	žádný
další	další	k2eAgInSc4d1	další
stupeň	stupeň	k1gInSc4	stupeň
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
získaného	získaný	k2eAgNnSc2d1	získané
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
–	–	k?	–
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
chápe	chápat	k5eAaImIp3nS	chápat
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
klasické	klasický	k2eAgInPc4d1	klasický
stupně	stupeň	k1gInPc4	stupeň
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
:	:	kIx,	:
bakalářské	bakalářský	k2eAgFnPc1d1	Bakalářská
–	–	k?	–
magisterské	magisterský	k2eAgFnPc1d1	magisterská
–	–	k?	–
doktorské	doktorský	k2eAgFnPc1d1	doktorská
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
JUDr.	JUDr.	kA	JUDr.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
právníka	právník	k1gMnSc2	právník
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Mgr.	Mgr.	kA	Mgr.
de	de	k?	de
facto	facto	k1gNnSc4	facto
nijak	nijak	k6eAd1	nijak
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
magisterské	magisterský	k2eAgNnSc4d1	magisterské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
právník	právník	k1gMnSc1	právník
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
"	"	kIx"	"
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
hovorově	hovorově	k6eAd1	hovorově
označovanou	označovaný	k2eAgFnSc7d1	označovaná
jako	jako	k8xS	jako
státnice	státnice	k1gFnSc1	státnice
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
práv	práv	k2eAgMnSc1d1	práv
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
Mgr.	Mgr.	kA	Mgr.
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
následně	následně	k6eAd1	následně
složit	složit	k5eAaPmF	složit
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
zkoušku	zkouška	k1gFnSc4	zkouška
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
rigo	rigo	k6eAd1	rigo
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
které	který	k3yQgNnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
uznána	uznán	k2eAgFnSc1d1	uznána
i	i	k8xC	i
stejná	stejný	k2eAgFnSc1d1	stejná
práce	práce	k1gFnSc1	práce
jako	jako	k8xS	jako
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
připouští	připouštět	k5eAaImIp3nS	připouštět
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
předpis	předpis	k1gInSc4	předpis
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
stále	stále	k6eAd1	stále
zcela	zcela	k6eAd1	zcela
stejné	stejný	k2eAgFnPc4d1	stejná
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kompetence	kompetence	k1gFnPc1	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózum	rigorózum	k1gNnSc1	rigorózum
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgFnSc1d1	fakultativní
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
rigorózní	rigorózní	k2eAgFnSc1d1	rigorózní
zkouška	zkouška	k1gFnSc1	zkouška
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
ČR	ČR	kA	ČR
zpoplatněnou	zpoplatněný	k2eAgFnSc7d1	zpoplatněná
záležitostí	záležitost	k1gFnSc7	záležitost
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
–	–	k?	–
mimo	mimo	k6eAd1	mimo
lékařských	lékařský	k2eAgInPc2d1	lékařský
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
MDDr	MDDr	k1gMnSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
MVDr.	MVDr.	kA	MVDr.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
standardní	standardní	k2eAgNnSc4d1	standardní
zakončení	zakončení	k1gNnSc4	zakončení
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Financování	financování	k1gNnSc1	financování
====	====	k?	====
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
doktorand	doktorand	k1gMnSc1	doktorand
je	být	k5eAaImIp3nS	být
studentem	student	k1gMnSc7	student
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
dané	daný	k2eAgFnSc2d1	daná
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
studenta	student	k1gMnSc4	student
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dle	dle	k7c2	dle
formy	forma	k1gFnSc2	forma
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vnitřně	vnitřně	k6eAd1	vnitřně
(	(	kIx(	(
<g/>
pracovně	pracovně	k6eAd1	pracovně
<g/>
)	)	kIx)	)
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
interní	interní	k2eAgMnPc4d1	interní
a	a	k8xC	a
externí	externí	k2eAgMnPc4d1	externí
doktorandy	doktorand	k1gMnPc4	doktorand
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
doktorandů	doktorand	k1gMnPc2	doktorand
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
podobné	podobný	k2eAgFnSc2d1	podobná
asistentům	asistent	k1gMnPc3	asistent
(	(	kIx(	(
<g/>
pracovně	pracovně	k6eAd1	pracovně
někdy	někdy	k6eAd1	někdy
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
as	as	k1gNnSc1	as
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
a	a	k8xC	a
pobírají	pobírat	k5eAaImIp3nP	pobírat
plat	plat	k1gInSc4	plat
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
mzdu	mzda	k1gFnSc4	mzda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doktorand	doktorand	k1gMnSc1	doktorand
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
interní	interní	k2eAgMnSc1d1	interní
doktorand	doktorand	k1gMnSc1	doktorand
<g/>
)	)	kIx)	)
většinou	většinou	k6eAd1	většinou
pobírá	pobírat	k5eAaImIp3nS	pobírat
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
šest	šest	k4xCc1	šest
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doktorandi	doktorand	k1gMnPc1	doktorand
také	také	k9	také
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
pracovní	pracovní	k2eAgInSc4d1	pracovní
úvazek	úvazek	k1gInSc4	úvazek
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
asistenta	asistent	k1gMnSc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
financováni	financován	k2eAgMnPc1d1	financován
z	z	k7c2	z
příslušného	příslušný	k2eAgInSc2d1	příslušný
grantu	grant	k1gInSc2	grant
<g/>
.	.	kIx.	.
</s>
<s>
Interní	interní	k2eAgMnPc4d1	interní
doktorandy	doktorand	k1gMnPc4	doktorand
pak	pak	k6eAd1	pak
stát	stát	k5eAaImF	stát
také	také	k9	také
většinou	většina	k1gFnSc7	většina
různě	různě	k6eAd1	různě
zvýhodňuje	zvýhodňovat	k5eAaImIp3nS	zvýhodňovat
či	či	k8xC	či
podporuje	podporovat	k5eAaImIp3nS	podporovat
–	–	k?	–
v	v	k7c6	v
ČR	ČR	kA	ČR
stát	stát	k1gInSc1	stát
považuje	považovat	k5eAaImIp3nS	považovat
doktoranda	doktorand	k1gMnSc4	doktorand
za	za	k7c4	za
studenta	student	k1gMnSc4	student
nikoliv	nikoliv	k9	nikoliv
max	max	kA	max
<g/>
.	.	kIx.	.
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
26	[number]	k4	26
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
standardně	standardně	k6eAd1	standardně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
daňové	daňový	k2eAgNnSc4d1	daňové
<g/>
,	,	kIx,	,
účely	účel	k1gInPc4	účel
až	až	k9	až
do	do	k7c2	do
dovršení	dovršení	k1gNnSc2	dovršení
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
struktura	struktura	k1gFnSc1	struktura
studentů	student	k1gMnPc2	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
studentů	student	k1gMnPc2	student
studuje	studovat	k5eAaImIp3nS	studovat
první	první	k4xOgInSc4	první
cyklus	cyklus	k1gInSc4	cyklus
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
studentů	student	k1gMnPc2	student
studuje	studovat	k5eAaImIp3nS	studovat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
cyklu	cyklus	k1gInSc6	cyklus
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
magisterský	magisterský	k2eAgInSc1d1	magisterský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
a	a	k8xC	a
právě	právě	k9	právě
třetí	třetí	k4xOgInSc1	třetí
cyklus	cyklus	k1gInSc1	cyklus
(	(	kIx(	(
<g/>
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
studuje	studovat	k5eAaImIp3nS	studovat
stabilně	stabilně	k6eAd1	stabilně
kolem	kolem	k7c2	kolem
6-8	[number]	k4	6-8
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
studentů	student	k1gMnPc2	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc4	tento
cyklus	cyklus	k1gInSc4	cyklus
v	v	k7c6	v
ČR	ČR	kA	ČR
nedokončí	dokončit	k5eNaPmIp3nS	dokončit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
vyšší	vysoký	k2eAgFnSc4d2	vyšší
neúspěšnost	neúspěšnost	k1gFnSc4	neúspěšnost
než	než	k8xS	než
u	u	k7c2	u
studentů	student	k1gMnPc2	student
cyklu	cyklus	k1gInSc2	cyklus
prvního	první	k4xOgNnSc2	první
<g/>
;	;	kIx,	;
uvedené	uvedený	k2eAgFnPc4d1	uvedená
<g />
.	.	kIx.	.
</s>
<s>
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
souviset	souviset	k5eAaImF	souviset
též	též	k9	též
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ČR	ČR	kA	ČR
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
studenti	student	k1gMnPc1	student
třetího	třetí	k4xOgInSc2	třetí
cyklu	cyklus	k1gInSc2	cyklus
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
zápisu	zápis	k1gInSc2	zápis
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
měsíční	měsíční	k2eAgNnSc4d1	měsíční
doktorské	doktorský	k2eAgNnSc4d1	doktorské
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
výhodné	výhodný	k2eAgFnPc1d1	výhodná
se	se	k3xPyFc4	se
přihlásit	přihlásit	k5eAaPmF	přihlásit
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
doktorskému	doktorský	k2eAgNnSc3d1	doktorské
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ať	ať	k9	ať
už	už	k9	už
jej	on	k3xPp3gMnSc4	on
skutečně	skutečně	k6eAd1	skutečně
studovat	studovat	k5eAaImF	studovat
hodlají	hodlat	k5eAaImIp3nP	hodlat
<g/>
,	,	kIx,	,
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
těchto	tento	k3xDgMnPc2	tento
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stipendium	stipendium	k1gNnSc4	stipendium
do	do	k7c2	do
10	[number]	k4	10
000	[number]	k4	000
Kč	Kč	kA	Kč
nepokryje	pokrýt	k5eNaPmIp3nS	pokrýt
životními	životní	k2eAgInPc7d1	životní
náklady	náklad	k1gInPc7	náklad
již	již	k6eAd1	již
dospělých	dospělý	k2eAgMnPc2d1	dospělý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mívají	mívat	k5eAaImIp3nP	mívat
často	často	k6eAd1	často
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
či	či	k8xC	či
vlastní	vlastní	k2eAgNnSc4d1	vlastní
bydlení	bydlení	k1gNnSc4	bydlení
apod.	apod.	kA	apod.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Modely	model	k1gInPc1	model
supervize	supervize	k1gFnSc2	supervize
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
univerzitách	univerzita	k1gFnPc6	univerzita
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
školení	školení	k1gNnPc1	školení
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
akademiky	akademik	k1gMnPc4	akademik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toto	tento	k3xDgNnSc4	tento
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Dinham	Dinham	k6eAd1	Dinham
a	a	k8xC	a
Scott	Scott	k1gMnSc1	Scott
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
růst	růst	k1gInSc1	růst
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
studentů	student	k1gMnPc2	student
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
zvýšením	zvýšení	k1gNnSc7	zvýšení
řady	řada	k1gFnSc2	řada
návodů	návod	k1gInPc2	návod
<g/>
/	/	kIx~	/
<g/>
manuálů	manuál	k1gInPc2	manuál
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
školitele	školitel	k1gMnPc4	školitel
<g/>
,	,	kIx,	,
citují	citovat	k5eAaBmIp3nP	citovat
přitom	přitom	k6eAd1	přitom
příklady	příklad	k1gInPc4	příklad
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Pugh	Pugh	k1gInSc1	Pugh
a	a	k8xC	a
Phillips	Phillips	k1gInSc1	Phillips
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
autoři	autor	k1gMnPc1	autor
publikovali	publikovat	k5eAaBmAgMnP	publikovat
empirická	empirický	k2eAgNnPc4d1	empirické
data	datum	k1gNnPc4	datum
o	o	k7c6	o
přínosech	přínos	k1gInPc6	přínos
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
kandidát	kandidát	k1gMnSc1	kandidát
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
publikuje	publikovat	k5eAaBmIp3nS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
poznamenávají	poznamenávat	k5eAaImIp3nP	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
studenti	student	k1gMnPc1	student
jsou	být	k5eAaImIp3nP	být
tomu	ten	k3xDgNnSc3	ten
spíše	spíše	k9	spíše
nakloněni	naklonit	k5eAaPmNgMnP	naklonit
s	s	k7c7	s
adekvátní	adekvátní	k2eAgFnSc7d1	adekvátní
podporou	podpora	k1gFnSc7	podpora
svých	svůj	k3xOyFgMnPc2	svůj
školitelů	školitel	k1gMnPc2	školitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wisker	Wisker	k1gInSc1	Wisker
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
uvedených	uvedený	k2eAgFnPc6d1	uvedená
oblastech	oblast	k1gFnPc6	oblast
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
modely	model	k1gInPc7	model
dohledu	dohled	k1gInSc2	dohled
(	(	kIx(	(
<g/>
supervize	supervize	k1gFnSc1	supervize
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
supervize	supervize	k1gFnSc2	supervize
(	(	kIx(	(
<g/>
technical-rationality	technicalationalita	k1gFnSc2	technical-rationalita
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
model	model	k1gInSc4	model
(	(	kIx(	(
<g/>
negotiated	negotiated	k1gInSc1	negotiated
order	order	k1gMnSc1	order
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
mechanistický	mechanistický	k2eAgMnSc1d1	mechanistický
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
plynulou	plynulý	k2eAgFnSc4d1	plynulá
a	a	k8xC	a
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
literatury	literatura	k1gFnSc2	literatura
ohledně	ohledně	k7c2	ohledně
očekávání	očekávání	k1gNnSc2	očekávání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
školitelé	školitel	k1gMnPc1	školitel
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgMnPc4	svůj
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
doktorandů	doktorand	k1gMnPc2	doktorand
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgMnPc4	svůj
školitele	školitel	k1gMnPc4	školitel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nikoli	nikoli	k9	nikoli
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
školitele	školitel	k1gMnSc4	školitel
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
kurs	kurs	k1gInSc4	kurs
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc4	některý
zajímavosti	zajímavost	k1gFnPc4	zajímavost
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ČR	ČR	kA	ČR
uvádí	uvádět	k5eAaImIp3nS	uvádět
například	například	k6eAd1	například
pramen	pramen	k1gInSc4	pramen
Učitelské	učitelský	k2eAgFnSc2d1	učitelská
noviny	novina	k1gFnSc2	novina
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Stipendium	stipendium	k1gNnSc1	stipendium
(	(	kIx(	(
<g/>
doktorské	doktorský	k2eAgNnSc1d1	doktorské
stipendium	stipendium	k1gNnSc1	stipendium
<g/>
)	)	kIx)	)
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
sedm	sedm	k4xCc1	sedm
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
doktorandů	doktorand	k1gMnPc2	doktorand
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
429	[number]	k4	429
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
cca	cca	kA	cca
2	[number]	k4	2
200	[number]	k4	200
na	na	k7c4	na
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
6,4	[number]	k4	6,4
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
EU	EU	kA	EU
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
čísle	číslo	k1gNnSc6	číslo
3,1	[number]	k4	3,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polovina	polovina	k1gFnSc1	polovina
doktorandů	doktorand	k1gMnPc2	doktorand
studuje	studovat	k5eAaImIp3nS	studovat
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
99,3	[number]	k4	99,3
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
v	v	k7c6	v
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
79	[number]	k4	79
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
má	mít	k5eAaImIp3nS	mít
celkově	celkově	k6eAd1	celkově
podíl	podíl	k1gInSc1	podíl
30	[number]	k4	30
%	%	kIx~	%
na	na	k7c6	na
všech	všecek	k3xTgMnPc6	všecek
doktorandech	doktorand	k1gMnPc6	doktorand
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
13	[number]	k4	13
%	%	kIx~	%
<g/>
,	,	kIx,	,
ČVUT	ČVUT	kA	ČVUT
pak	pak	k6eAd1	pak
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
jenom	jenom	k9	jenom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
ČR	ČR	kA	ČR
získalo	získat	k5eAaPmAgNnS	získat
2	[number]	k4	2
629	[number]	k4	629
doktorandů	doktorand	k1gMnPc2	doktorand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
doktorů	doktor	k1gMnPc2	doktor
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
absolventů	absolvent	k1gMnPc2	absolvent
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
je	být	k5eAaImIp3nS	být
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
%	%	kIx~	%
doktorandů	doktorand	k1gMnPc2	doktorand
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
technické	technický	k2eAgFnPc4d1	technická
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
20	[number]	k4	20
%	%	kIx~	%
na	na	k7c4	na
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
14	[number]	k4	14
%	%	kIx~	%
na	na	k7c4	na
společenské	společenský	k2eAgFnPc4d1	společenská
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
na	na	k7c4	na
FF	ff	kA	ff
UK	UK	kA	UK
studovalo	studovat	k5eAaImAgNnS	studovat
v	v	k7c6	v
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
cyklu	cyklus	k1gInSc6	cyklus
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
133	[number]	k4	133
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
v	v	k7c6	v
magisterském	magisterský	k2eAgMnSc6d1	magisterský
1	[number]	k4	1
796	[number]	k4	796
a	a	k8xC	a
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
1	[number]	k4	1
158	[number]	k4	158
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
na	na	k7c4	na
FF	ff	kA	ff
MU	MU	kA	MU
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
4	[number]	k4	4
278	[number]	k4	278
–	–	k?	–
2	[number]	k4	2
271	[number]	k4	271
–	–	k?	–
740	[number]	k4	740
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
PřF	PřF	k1gFnSc6	PřF
UK	UK	kA	UK
bylo	být	k5eAaImAgNnS	být
složení	složení	k1gNnSc1	složení
studentů	student	k1gMnPc2	student
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
cyklech	cyklus	k1gInPc6	cyklus
2	[number]	k4	2
000	[number]	k4	000
–	–	k?	–
1	[number]	k4	1
136	[number]	k4	136
–	–	k?	–
1	[number]	k4	1
369	[number]	k4	369
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
na	na	k7c6	na
PřF	PřF	k1gFnSc6	PřF
MU	MU	kA	MU
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
873	[number]	k4	873
–	–	k?	–
895	[number]	k4	895
–	–	k?	–
911	[number]	k4	911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc4	některý
zajímavosti	zajímavost	k1gFnPc4	zajímavost
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
uvádí	uvádět	k5eAaImIp3nS	uvádět
například	například	k6eAd1	například
pramen	pramen	k1gInSc4	pramen
The	The	k1gFnSc2	The
Economist	Economist	k1gInSc1	Economist
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998-2006	[number]	k4	1998-2006
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
doktorátů	doktorát	k1gInPc2	doktorát
udělených	udělený	k2eAgInPc2d1	udělený
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
OECD	OECD	kA	OECD
o	o	k7c6	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nS	muset
přinést	přinést	k5eAaPmF	přinést
oproti	oproti	k7c3	oproti
magisterskému	magisterský	k2eAgNnSc3d1	magisterské
vzdělání	vzdělání	k1gNnSc3	vzdělání
žádné	žádný	k3yNgFnSc6	žádný
zvýšení	zvýšení	k1gNnSc4	zvýšení
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
i	i	k9	i
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
poklesu	pokles	k1gInSc3	pokles
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
univerzitě	univerzita	k1gFnSc6	univerzita
z	z	k7c2	z
USA	USA	kA	USA
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
studia	studio	k1gNnSc2	studio
dokončí	dokončit	k5eAaPmIp3nP	dokončit
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
chytřejší	chytrý	k2eAgMnSc1d2	chytřejší
než	než	k8xS	než
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
doktorát	doktorát	k1gInSc4	doktorát
nezískají	získat	k5eNaPmIp3nP	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
postdoktorandi	postdoktorand	k1gMnPc1	postdoktorand
neuplatní	uplatnit	k5eNaPmIp3nP	uplatnit
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
obtížně	obtížně	k6eAd1	obtížně
hledají	hledat	k5eAaImIp3nP	hledat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2005-2009	[number]	k4	2005-2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
uděleno	udělit	k5eAaPmNgNnS	udělit
odhadem	odhad	k1gInSc7	odhad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
titulů	titul	k1gInPc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
80	[number]	k4	80
%	%	kIx~	%
čerstvých	čerstvý	k2eAgMnPc2d1	čerstvý
absolventů	absolvent	k1gMnPc2	absolvent
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
ročně	ročně	k6eAd1	ročně
před	před	k7c7	před
zdaněním	zdanění	k1gNnSc7	zdanění
38	[number]	k4	38
600	[number]	k4	600
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
plat	plat	k1gInSc1	plat
stavebního	stavební	k2eAgMnSc2d1	stavební
dělníka	dělník	k1gMnSc2	dělník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
absolventů	absolvent	k1gMnPc2	absolvent
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nesouvisí	souviset	k5eNaImIp3nS	souviset
se	s	k7c7	s
získanou	získaný	k2eAgFnSc7d1	získaná
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
SRN	SRN	kA	SRN
skončí	skončit	k5eAaPmIp3nS	skončit
13	[number]	k4	13
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
absolventů	absolvent	k1gMnPc2	absolvent
doktorského	doktorský	k2eAgInSc2d1	doktorský
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
podřadném	podřadný	k2eAgNnSc6d1	podřadné
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
činí	činit	k5eAaImIp3nS	činit
21	[number]	k4	21
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
oborech	obor	k1gInPc6	obor
se	se	k3xPyFc4	se
výhoda	výhoda	k1gFnSc1	výhoda
držitelů	držitel	k1gMnPc2	držitel
titulů	titul	k1gInPc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
zcela	zcela	k6eAd1	zcela
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
–	–	k?	–
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
nebo	nebo	k8xC	nebo
jazyků	jazyk	k1gInPc2	jazyk
nevydělávají	vydělávat	k5eNaImIp3nP	vydělávat
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
magistři	magistr	k1gMnPc1	magistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
Economist	Economist	k1gInSc4	Economist
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
pouze	pouze	k6eAd1	pouze
lékařům	lékař	k1gMnPc3	lékař
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
vědních	vědní	k2eAgInPc6d1	vědní
oborech	obor	k1gInPc6	obor
a	a	k8xC	a
u	u	k7c2	u
obchodního	obchodní	k2eAgNnSc2d1	obchodní
a	a	k8xC	a
finančního	finanční	k2eAgNnSc2d1	finanční
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
vztaženém	vztažený	k2eAgInSc6d1	vztažený
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
obory	obor	k1gInPc4	obor
se	se	k3xPyFc4	se
držitelé	držitel	k1gMnPc1	držitel
titulu	titul	k1gInSc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
mohou	moct	k5eAaImIp3nP	moct
těšit	těšit	k5eAaImF	těšit
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
%	%	kIx~	%
rozdílu	rozdíl	k1gInSc2	rozdíl
ohodnocení	ohodnocení	k1gNnSc1	ohodnocení
oproti	oproti	k7c3	oproti
držitelům	držitel	k1gMnPc3	držitel
magisterské	magisterský	k2eAgFnSc2d1	magisterská
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
studie	studie	k1gFnSc1	studie
absolventů	absolvent	k1gMnPc2	absolvent
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
UK	UK	kA	UK
přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zapsali	zapsat	k5eAaPmAgMnP	zapsat
částečně	částečně	k6eAd1	částečně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
zůstat	zůstat	k5eAaPmF	zůstat
studenty	student	k1gMnPc4	student
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nP	aby
oddálili	oddálit	k5eAaPmAgMnP	oddálit
hledání	hledání	k1gNnSc3	hledání
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
připustila	připustit	k5eAaPmAgFnS	připustit
to	ten	k3xDgNnSc1	ten
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
studentů	student	k1gMnPc2	student
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
–	–	k?	–
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
získat	získat	k5eAaPmF	získat
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
táhne	táhnout	k5eAaImIp3nS	táhnout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
doktorskému	doktorský	k2eAgNnSc3d1	doktorské
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Doctor	Doctor	k1gMnSc1	Doctor
of	of	k?	of
Philosophy	Philosopha	k1gFnSc2	Philosopha
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hirschův	Hirschův	k2eAgInSc1d1	Hirschův
index	index	k1gInSc1	index
</s>
</p>
<p>
<s>
Impakt	Impakt	k1gInSc1	Impakt
faktor	faktor	k1gInSc1	faktor
</s>
</p>
<p>
<s>
RIV	RIV	kA	RIV
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
of	of	k?	of
Science	Science	k1gFnSc1	Science
</s>
</p>
<p>
<s>
Scopus	Scopus	k1gInSc1	Scopus
(	(	kIx(	(
<g/>
databáze	databáze	k1gFnSc1	databáze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grantová	grantový	k2eAgFnSc1d1	Grantová
agentura	agentura	k1gFnSc1	agentura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Technologická	technologický	k2eAgFnSc1d1	technologická
agentura	agentura	k1gFnSc1	agentura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Akreditované	akreditovaný	k2eAgInPc1d1	akreditovaný
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
–	–	k?	–
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Charakter	charakter	k1gInSc1	charakter
doktorátu	doktorát	k1gInSc2	doktorát
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
–	–	k?	–
Vysoké	vysoká	k1gFnSc2	vysoká
školství	školství	k1gNnSc2	školství
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
</s>
</p>
