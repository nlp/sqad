<s>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
z	z	k7c2	z
lat.	lat.	k?	lat.
philosophiæ	philosophiæ	k?	philosophiæ
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
vysokoškolským	vysokoškolský	k2eAgNnSc7d1	vysokoškolské
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
