<s>
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
val	val	k1gInSc1	val
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Limes	Limes	k1gMnSc1	Limes
Britannicus	Britannicus	k1gMnSc1	Britannicus
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Hadrians	Hadrians	k1gInSc1	Hadrians
Wall	Walla	k1gFnPc2	Walla
je	být	k5eAaImIp3nS	být
kamenné	kamenný	k2eAgNnSc1d1	kamenné
a	a	k8xC	a
hliněné	hliněný	k2eAgNnSc1d1	hliněné
hraniční	hraniční	k2eAgNnSc1d1	hraniční
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
17	[number]	k4	17
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgMnSc1d1	táhnoucí
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
severní	severní	k2eAgFnSc7d1	severní
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
skotských	skotský	k2eAgFnPc2d1	skotská
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
římským	římský	k2eAgNnSc7d1	římské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
začínal	začínat	k5eAaImAgMnS	začínat
blízko	blízko	k7c2	blízko
Newcastle	Newcastle	k1gFnSc2	Newcastle
upon	upon	k1gInSc1	upon
Tyne	Tyne	k1gFnSc4	Tyne
a	a	k8xC	a
táhl	táhnout	k5eAaImAgMnS	táhnout
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Carlisle	Carlisle	k1gFnSc4	Carlisle
až	až	k9	až
po	po	k7c4	po
Maryport	Maryport	k1gInSc4	Maryport
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
délce	délka	k1gFnSc6	délka
119	[number]	k4	119
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
val	val	k1gInSc1	val
tvořil	tvořit	k5eAaImAgInS	tvořit
po	po	k7c4	po
300	[number]	k4	300
let	léto	k1gNnPc2	léto
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
chránit	chránit	k5eAaImF	chránit
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
země	zem	k1gFnSc2	zem
před	před	k7c7	před
nájezdy	nájezd	k1gInPc7	nájezd
severních	severní	k2eAgInPc2d1	severní
kmenů	kmen	k1gInPc2	kmen
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
valu	val	k1gInSc2	val
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
122	[number]	k4	122
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Hadriana	Hadrian	k1gMnSc2	Hadrian
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
snad	snad	k9	snad
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
val	val	k1gInSc1	val
osobně	osobně	k6eAd1	osobně
navštívil	navštívit	k5eAaPmAgInS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
tři	tři	k4xCgFnPc4	tři
římské	římský	k2eAgFnPc4d1	římská
legie	legie	k1gFnPc4	legie
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hadriánův	Hadriánův	k2eAgMnSc1d1	Hadriánův
nástupce	nástupce	k1gMnSc1	nástupce
Antoninus	Antoninus	k1gMnSc1	Antoninus
Pius	Pius	k1gMnSc1	Pius
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
postavit	postavit	k5eAaPmF	postavit
nové	nový	k2eAgNnSc1d1	nové
opevnění	opevnění	k1gNnSc1	opevnění
(	(	kIx(	(
<g/>
Antoninův	Antoninův	k2eAgInSc1d1	Antoninův
val	val	k1gInSc1	val
<g/>
)	)	kIx)	)
asi	asi	k9	asi
160	[number]	k4	160
km	km	kA	km
severněji	severně	k6eAd2	severně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skotských	skotská	k1gFnPc6	skotská
Lowlands	Lowlandsa	k1gFnPc2	Lowlandsa
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
však	však	k9	však
krajinu	krajina	k1gFnSc4	krajina
pacifikovat	pacifikovat	k5eAaBmF	pacifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
164	[number]	k4	164
Antoninův	Antoninův	k2eAgInSc1d1	Antoninův
val	val	k1gInSc1	val
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
hranicí	hranice	k1gFnSc7	hranice
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgInS	stát
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
val	val	k1gInSc1	val
<g/>
.	.	kIx.	.
</s>
<s>
Nezdarem	nezdar	k1gInSc7	nezdar
skončil	skončit	k5eAaPmAgInS	skončit
i	i	k9	i
další	další	k2eAgInSc1d1	další
pokus	pokus	k1gInSc1	pokus
císaře	císař	k1gMnSc2	císař
Septimia	Septimius	k1gMnSc2	Septimius
Severa	Severa	k1gMnSc1	Severa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
211	[number]	k4	211
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
val	val	k1gInSc1	val
sloužil	sloužit	k5eAaImAgInS	sloužit
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
římské	římský	k2eAgFnSc2d1	římská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
410	[number]	k4	410
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
hradba	hradba	k1gFnSc1	hradba
chátrala	chátrat	k5eAaImAgFnS	chátrat
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
zdrojem	zdroj	k1gInSc7	zdroj
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc7	on
začali	začít	k5eAaPmAgMnP	začít
všímat	všímat	k5eAaImF	všímat
místní	místní	k2eAgMnPc1d1	místní
nadšenci	nadšenec	k1gMnPc1	nadšenec
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
archeologové	archeolog	k1gMnPc1	archeolog
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
pevnosti	pevnost	k1gFnSc2	pevnost
Vindolanda	Vindolando	k1gNnSc2	Vindolando
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Newcastle	Newcastle	k1gFnSc2	Newcastle
<g/>
,	,	kIx,	,
objeven	objeven	k2eAgInSc1d1	objeven
archiv	archiv	k1gInSc1	archiv
písemností	písemnost	k1gFnPc2	písemnost
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
destičkách	destička	k1gFnPc6	destička
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
osobní	osobní	k2eAgFnSc4d1	osobní
korespondenci	korespondence	k1gFnSc4	korespondence
římských	římský	k2eAgMnPc2d1	římský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
valu	val	k1gInSc2	val
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Val	val	k1gInSc1	val
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
severní	severní	k2eAgFnSc7d1	severní
hranicí	hranice	k1gFnSc7	hranice
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
opevněnou	opevněný	k2eAgFnSc7d1	opevněná
částí	část	k1gFnSc7	část
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
již	již	k6eAd1	již
římský	římský	k2eAgInSc4d1	římský
vliv	vliv	k1gInSc4	vliv
nesahal	sahat	k5eNaImAgMnS	sahat
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
jak	jak	k9	jak
k	k	k7c3	k
vojenským	vojenský	k2eAgInPc3d1	vojenský
účelům	účel	k1gInPc3	účel
jako	jako	k9	jako
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
průchozí	průchozí	k2eAgNnSc4d1	průchozí
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
obchodníky	obchodník	k1gMnPc4	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
museli	muset	k5eAaImAgMnP	muset
platit	platit	k5eAaImF	platit
daně	daň	k1gFnPc4	daň
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianův	Hadrianův	k2eAgInSc1d1	Hadrianův
val	val	k1gInSc1	val
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
80	[number]	k4	80
římských	římský	k2eAgFnPc2d1	římská
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
117	[number]	k4	117
kilometrům	kilometr	k1gInPc3	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Val	val	k1gInSc4	val
střežilo	střežit	k5eAaImAgNnS	střežit
80	[number]	k4	80
bran	brána	k1gFnPc2	brána
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
jedné	jeden	k4xCgFnSc2	jeden
římské	římský	k2eAgFnSc2d1	římská
míle	míle	k1gFnSc2	míle
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
branami	brána	k1gFnPc7	brána
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
jedna	jeden	k4xCgFnSc1	jeden
strážní	strážní	k2eAgFnSc1d1	strážní
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
a	a	k8xC	a
signalizaci	signalizace	k1gFnSc3	signalizace
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
valu	val	k1gInSc2	val
leželo	ležet	k5eAaImAgNnS	ležet
čtrnáct	čtrnáct	k4xCc1	čtrnáct
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
zdi	zeď	k1gFnSc2	zeď
není	být	k5eNaImIp3nS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
valu	val	k1gInSc2	val
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
použitém	použitý	k2eAgInSc6d1	použitý
materiálu	materiál	k1gInSc6	materiál
a	a	k8xC	a
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Irthing	Irthing	k1gInSc1	Irthing
je	být	k5eAaImIp3nS	být
val	val	k1gInSc4	val
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
z	z	k7c2	z
tesaných	tesaný	k2eAgInPc2d1	tesaný
kvádrů	kvádr	k1gInPc2	kvádr
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
3	[number]	k4	3
metry	metr	k1gInPc4	metr
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
byl	být	k5eAaImAgInS	být
val	val	k1gInSc1	val
původně	původně	k6eAd1	původně
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
a	a	k8xC	a
rašelinových	rašelinový	k2eAgInPc2d1	rašelinový
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc4d1	široký
okolo	okolo	k7c2	okolo
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vysoký	vysoký	k2eAgMnSc1d1	vysoký
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Val	val	k1gInSc1	val
byl	být	k5eAaImAgInS	být
částí	část	k1gFnSc7	část
obranného	obranný	k2eAgInSc2d1	obranný
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
(	(	kIx(	(
<g/>
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
následující	následující	k2eAgInPc4d1	následující
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
pásmo	pásmo	k1gNnSc1	pásmo
pevnůstek	pevnůstka	k1gFnPc2	pevnůstka
asi	asi	k9	asi
8-15	[number]	k4	8-15
km	km	kA	km
severněji	severně	k6eAd2	severně
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
monitorovaly	monitorovat	k5eAaImAgFnP	monitorovat
krajinu	krajina	k1gFnSc4	krajina
koliště	koliště	k1gNnSc2	koliště
či	či	k8xC	či
předprseň	předprseň	k1gFnSc1	předprseň
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
vyplněným	vyplněný	k2eAgInSc7d1	vyplněný
špičatými	špičatý	k2eAgInPc7d1	špičatý
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
kůly	kůl	k1gInPc1	kůl
hlavní	hlavní	k2eAgInSc1d1	hlavní
val	val	k1gInSc4	val
či	či	k8xC	či
hradbu	hradba	k1gFnSc4	hradba
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
silnici	silnice	k1gFnSc4	silnice
a	a	k8xC	a
vallum	vallum	k1gInSc1	vallum
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
náspy	násep	k1gInPc1	násep
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc1d1	oddělený
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořily	tvořit	k5eAaImAgFnP	tvořit
pomocné	pomocný	k2eAgFnPc1d1	pomocná
jednotky	jednotka	k1gFnPc1	jednotka
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
vojáci	voják	k1gMnPc1	voják
nebyli	být	k5eNaImAgMnP	být
římskými	římský	k2eAgMnPc7d1	římský
občany	občan	k1gMnPc7	občan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
kolísal	kolísat	k5eAaImAgInS	kolísat
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
pěší	pěší	k2eAgNnSc1d1	pěší
vojsko	vojsko	k1gNnSc1	vojsko
i	i	k8xC	i
kavalérie	kavalérie	k1gFnSc1	kavalérie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
např.	např.	kA	např.
Velké	velký	k2eAgFnSc2d1	velká
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
stavěné	stavěný	k2eAgFnSc2d1	stavěná
pro	pro	k7c4	pro
podobný	podobný	k2eAgInSc4d1	podobný
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převážně	převážně	k6eAd1	převážně
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
