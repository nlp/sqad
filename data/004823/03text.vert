<s>
Madhouse	Madhouse	k1gFnSc1	Madhouse
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
株	株	k?	株
<g/>
,	,	kIx,	,
Kabušiki	Kabušiki	k1gNnSc4	Kabušiki
gaiša	gaiš	k1gInSc2	gaiš
Maddohausu	Maddohaus	k1gInSc2	Maddohaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
anime	animat	k5eAaPmIp3nS	animat
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bývalými	bývalý	k2eAgMnPc7d1	bývalý
animátory	animátor	k1gMnPc7	animátor
z	z	k7c2	z
Muši	Muši	k1gNnSc2	Muši
Production	Production	k1gInSc4	Production
<g/>
:	:	kIx,	:
Masao	Masao	k6eAd1	Masao
Marujamou	Marujamý	k2eAgFnSc4d1	Marujamý
<g/>
,	,	kIx,	,
Osamu	Osama	k1gFnSc4	Osama
Dezakim	Dezaki	k1gNnSc7	Dezaki
<g/>
,	,	kIx,	,
Rintaróem	Rintaróum	k1gNnSc7	Rintaróum
a	a	k8xC	a
Jošiakim	Jošiaki	k1gNnSc7	Jošiaki
Kawadžirim	Kawadžirima	k1gFnPc2	Kawadžirima
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gNnSc7	jejich
nejznámější	známý	k2eAgInPc4d3	nejznámější
anime	animat	k5eAaPmIp3nS	animat
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Death	Death	k1gInSc4	Death
Note	Note	k1gFnPc2	Note
nebo	nebo	k8xC	nebo
Trigun	Triguna	k1gFnPc2	Triguna
<g/>
.	.	kIx.	.
</s>
