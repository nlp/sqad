<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1998	[number]	k4	1998
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
od	od	k7c2	od
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
sobotu	sobota	k1gFnSc4	sobota
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
200	[number]	k4	200
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
se	se	k3xPyFc4	se
ucházelo	ucházet	k5eAaImAgNnS	ucházet
18	[number]	k4	18
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
předčasné	předčasný	k2eAgFnPc4d1	předčasná
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předchozí	předchozí	k2eAgInSc4d1	předchozí
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
jen	jen	k9	jen
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
69	[number]	k4	69
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
74,03	[number]	k4	74,03
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
99,58	[number]	k4	99,58
%	%	kIx~	%
(	(	kIx(	(
<g/>
5	[number]	k4	5
969	[number]	k4	969
505	[number]	k4	505
<g/>
)	)	kIx)	)
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
platně	platně	k6eAd1	platně
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
frakcí	frakce	k1gFnSc7	frakce
se	se	k3xPyFc4	se
se	s	k7c7	s
74	[number]	k4	74
poslanci	poslanec	k1gMnPc7	poslanec
stala	stát	k5eAaPmAgFnS	stát
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
byla	být	k5eAaImAgFnS	být
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
odstupem	odstup	k1gInSc7	odstup
pak	pak	k6eAd1	pak
následovala	následovat	k5eAaImAgFnS	následovat
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
Unie	unie	k1gFnSc1	unie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
po	po	k7c6	po
delších	dlouhý	k2eAgNnPc6d2	delší
jednáních	jednání	k1gNnPc6	jednání
sestavila	sestavit	k5eAaPmAgFnS	sestavit
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
ODS	ODS	kA	ODS
garantovanou	garantovaný	k2eAgFnSc7d1	garantovaná
tzv.	tzv.	kA	tzv.
Opoziční	opoziční	k2eAgFnSc7d1	opoziční
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
stran	strana	k1gFnPc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Výsledky	výsledek	k1gInPc4	výsledek
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Rozdělení	rozdělení	k1gNnSc1	rozdělení
mandátů	mandát	k1gInPc2	mandát
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Mapy	mapa	k1gFnPc4	mapa
výsledků	výsledek	k1gInPc2	výsledek
====	====	k?	====
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
výsledků	výsledek	k1gInPc2	výsledek
pěti	pět	k4xCc2	pět
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
volbám	volba	k1gFnPc3	volba
do	do	k7c2	do
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
</s>
</p>
