<s>
Kouř	kouř	k1gInSc1
</s>
<s>
Kouř	kouř	k1gInSc1
z	z	k7c2
požáru	požár	k1gInSc2
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Kouř	kouř	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kouř	kouř	k1gInSc1
nebo	nebo	k8xC
též	též	k9
dým	dým	k1gInSc1
<g/>
,	,	kIx,
lidově	lidově	k6eAd1
též	též	k9
čoud	čoud	k1gInSc1
či	či	k8xC
čmoud	čmoud	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
produkt	produkt	k1gInSc1
nedokonalého	dokonalý	k2eNgNnSc2d1
spalování	spalování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
bezbarvé	bezbarvý	k2eAgInPc1d1
plyny	plyn	k1gInPc1
a	a	k8xC
drobné	drobný	k2eAgFnPc1d1
viditelné	viditelný	k2eAgFnPc1d1
části	část	k1gFnPc1
(	(	kIx(
<g/>
popel	popel	k1gInSc1
<g/>
,	,	kIx,
popílek	popílek	k1gInSc1
<g/>
,	,	kIx,
saze	saze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pevné	pevný	k2eAgFnPc1d1
části	část	k1gFnPc1
velikosti	velikost	k1gFnSc2
menší	malý	k2eAgFnPc1d2
než	než	k8xS
1	#num#	k4
μ	μ	k1gNnPc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
vznášet	vznášet	k5eAaImF
a	a	k8xC
působit	působit	k5eAaImF
jako	jako	k9
kondenzační	kondenzační	k2eAgNnPc4d1
jádra	jádro	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
oblasti	oblast	k1gFnSc3
vzniku	vznik	k1gInSc2
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
kouře	kouř	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
minulosti	minulost	k1gFnSc6
například	například	k6eAd1
velké	velký	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
i	i	k9
místy	místy	k6eAd1
výskytu	výskyt	k1gInSc2
mlh	mlha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částice	částice	k1gFnSc1
větší	veliký	k2eAgFnSc2d2
než	než	k8xS
10	#num#	k4
μ	μ	k6eAd1
podléhají	podléhat	k5eAaImIp3nP
gravitační	gravitační	k2eAgFnSc3d1
síle	síla	k1gFnSc3
a	a	k8xC
mají	mít	k5eAaImIp3nP
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
omezenou	omezený	k2eAgFnSc4d1
životnost	životnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
dýmem	dým	k1gInSc7
<g/>
,	,	kIx,
mlhou	mlha	k1gFnSc7
a	a	k8xC
kouřem	kouř	k1gInSc7
</s>
<s>
Dým	dým	k1gInSc1
je	být	k5eAaImIp3nS
rozptýlená	rozptýlený	k2eAgFnSc1d1
tuhá	tuhý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
v	v	k7c6
plynu	plyn	k1gInSc6
<g/>
,	,	kIx,
mlha	mlha	k1gFnSc1
je	být	k5eAaImIp3nS
kapalina	kapalina	k1gFnSc1
rozptýlená	rozptýlený	k2eAgFnSc1d1
v	v	k7c6
plynu	plyn	k1gInSc6
<g/>
,	,	kIx,
kouř	kouř	k1gInSc1
je	být	k5eAaImIp3nS
kapalina	kapalina	k1gFnSc1
a	a	k8xC
tuhá	tuhý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
rozptýlená	rozptýlený	k2eAgFnSc1d1
v	v	k7c6
plynu	plyn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BUŘIL	BUŘIL	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektronické	elektronický	k2eAgFnPc4d1
cigarety	cigareta	k1gFnPc4
-	-	kIx~
porovnání	porovnání	k1gNnSc1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
emitovaných	emitovaný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Smog	smog	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kouř	kouř	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kouř	kouř	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4115701-1	4115701-1	k4
</s>
