<s>
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Reichenberg	Reichenberg	k1gInSc1	Reichenberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
krajské	krajský	k2eAgNnSc1d1	krajské
město	město	k1gNnSc1	město
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
pátým	pátý	k4xOgInSc7	pátý
největším	veliký	k2eAgInSc7d3	veliký
městem	město	k1gNnSc7	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
třetím	třetí	k4xOgMnSc6	třetí
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Jabloncem	Jablonec	k1gInSc7	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
širší	široký	k2eAgFnSc4d2	širší
sídelní	sídelní	k2eAgFnSc4d1	sídelní
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
katastr	katastr	k1gInSc1	katastr
města	město	k1gNnSc2	město
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jako	jako	k8xC	jako
enklávu	enkláva	k1gFnSc4	enkláva
obec	obec	k1gFnSc1	obec
Stráž	stráž	k1gFnSc1	stráž
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Euroregionu	euroregion	k1gInSc2	euroregion
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
91	[number]	k4	91
km	km	kA	km
severo-severovýchodně	severoeverovýchodně	k6eAd1	severo-severovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
99	[number]	k4	99
km	km	kA	km
severo-severozápadně	severoeverozápadně	k6eAd1	severo-severozápadně
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Liberecké	liberecký	k2eAgFnSc6d1	liberecká
kotlině	kotlina	k1gFnSc6	kotlina
Žitavské	žitavský	k2eAgFnSc2d1	Žitavská
pánve	pánev	k1gFnSc2	pánev
mezi	mezi	k7c7	mezi
Ještědsko-kozákovským	Ještědskoozákovský	k2eAgInSc7d1	Ještědsko-kozákovský
hřbetem	hřbet	k1gInSc7	hřbet
jižně	jižně	k6eAd1	jižně
a	a	k8xC	a
Jizerskými	jizerský	k2eAgFnPc7d1	Jizerská
horami	hora	k1gFnPc7	hora
severovýchodně	severovýchodně	k6eAd1	severovýchodně
<g/>
.	.	kIx.	.
</s>
<s>
Pata	pata	k1gFnSc1	pata
radnice	radnice	k1gFnSc2	radnice
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
374	[number]	k4	374
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
katastru	katastr	k1gInSc2	katastr
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
Ještědu	Ještěd	k1gInSc2	Ještěd
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
Nisy	Nisa	k1gFnSc2	Nisa
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Machnín	Machnín	k1gMnSc1	Machnín
(	(	kIx(	(
<g/>
325	[number]	k4	325
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
Lužická	lužický	k2eAgFnSc1d1	Lužická
Nisa	Nisa	k1gFnSc1	Nisa
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Černá	černý	k2eAgFnSc1d1	černá
Nisa	Nisa	k1gFnSc1	Nisa
a	a	k8xC	a
Harcovský	Harcovský	k2eAgInSc1d1	Harcovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
Harcovská	Harcovský	k2eAgFnSc1d1	Harcovská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenými	přirozený	k2eAgFnPc7d1	přirozená
vodními	vodní	k2eAgFnPc7d1	vodní
plochami	plocha	k1gFnPc7	plocha
jsou	být	k5eAaImIp3nP	být
rybníky	rybník	k1gInPc4	rybník
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
Vesecký	Vesecký	k2eAgMnSc1d1	Vesecký
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Teich	Teich	k1gInSc1	Teich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kačák	Kačák	k1gMnSc1	Kačák
(	(	kIx(	(
<g/>
Žabák	žabák	k1gMnSc1	žabák
<g/>
)	)	kIx)	)
v	v	k7c6	v
Krásné	krásný	k2eAgFnSc6d1	krásná
Studánce	studánka	k1gFnSc6	studánka
či	či	k8xC	či
rybník	rybník	k1gInSc4	rybník
Seba	Seb	k1gInSc2	Seb
(	(	kIx(	(
<g/>
hráz	hráz	k1gFnSc1	hráz
<g/>
)	)	kIx)	)
v	v	k7c6	v
Janově	Janův	k2eAgInSc6d1	Janův
Dole	dol	k1gInSc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
rozprostíral	rozprostírat	k5eAaImAgMnS	rozprostírat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
6,2	[number]	k4	6,2
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dnes	dnes	k6eAd1	dnes
představuje	představovat	k5eAaImIp3nS	představovat
historický	historický	k2eAgInSc4d1	historický
střed	střed	k1gInSc4	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
11	[number]	k4	11
obcí	obec	k1gFnPc2	obec
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
o	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
23	[number]	k4	23
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
a	a	k8xC	a
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
čtyři	čtyři	k4xCgFnPc4	čtyři
obce	obec	k1gFnPc1	obec
od	od	k7c2	od
města	město	k1gNnSc2	město
oddělily	oddělit	k5eAaPmAgFnP	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
katastru	katastr	k1gInSc2	katastr
města	město	k1gNnSc2	město
tak	tak	k9	tak
činí	činit	k5eAaImIp3nS	činit
106,09	[number]	k4	106,09
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
35,2	[number]	k4	35,2
%	%	kIx~	%
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
jí	on	k3xPp3gFnSc3	on
jsou	být	k5eAaImIp3nP	být
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
nezemědělské	zemědělský	k2eNgFnSc2d1	nezemědělská
půdy	půda	k1gFnSc2	půda
tvoří	tvořit	k5eAaImIp3nS	tvořit
půda	půda	k1gFnSc1	půda
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
39,9	[number]	k4	39,9
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Liberce	Liberec	k1gInSc2	Liberec
určuje	určovat	k5eAaImIp3nS	určovat
jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
kotlině	kotlina	k1gFnSc6	kotlina
mezi	mezi	k7c7	mezi
dvojicí	dvojice	k1gFnSc7	dvojice
horských	horský	k2eAgInPc2d1	horský
masivů	masiv	k1gInPc2	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
horské	horský	k2eAgInPc1d1	horský
hřebeny	hřeben	k1gInPc1	hřeben
překážkou	překážka	k1gFnSc7	překážka
proudění	proudění	k1gNnSc4	proudění
vlhkého	vlhký	k2eAgInSc2d1	vlhký
atlantického	atlantický	k2eAgInSc2d1	atlantický
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
poměrně	poměrně	k6eAd1	poměrně
hojné	hojný	k2eAgFnSc2d1	hojná
srážky	srážka	k1gFnSc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
803,4	[number]	k4	803,4
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
-	-	kIx~	-
nejdeštivějším	deštivý	k2eAgInSc7d3	nejdeštivější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
srpen	srpen	k1gInSc1	srpen
s	s	k7c7	s
88,4	[number]	k4	88,4
mm	mm	kA	mm
<g/>
,	,	kIx,	,
nejsušší	suchý	k2eAgMnSc1d3	nejsušší
je	být	k5eAaImIp3nS	být
únor	únor	k1gInSc1	únor
s	s	k7c7	s
46,2	[number]	k4	46,2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
7,2	[number]	k4	7,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc1	červenec
s	s	k7c7	s
16,2	[number]	k4	16,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejchladnější	chladný	k2eAgInSc4d3	nejchladnější
leden	leden	k1gInSc4	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
býval	bývat	k5eAaImAgMnS	bývat
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dokonce	dokonce	k9	dokonce
národnostně	národnostně	k6eAd1	národnostně
podbarvených	podbarvený	k2eAgInPc2d1	podbarvený
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejstarším	starý	k2eAgInSc7d3	nejstarší
uváděným	uváděný	k2eAgInSc7d1	uváděný
názvem	název	k1gInSc7	název
je	být	k5eAaImIp3nS	být
Reychinberch	Reychinberch	k1gInSc1	Reychinberch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
a	a	k8xC	a
Raichmberg	Raichmberg	k1gMnSc1	Raichmberg
(	(	kIx(	(
<g/>
1369	[number]	k4	1369
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přípona	přípona	k1gFnSc1	přípona
-berg	erg	k1gInSc1	-berg
znamená	znamenat	k5eAaImIp3nS	znamenat
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
a	a	k8xC	a
reich	reich	k?	reich
"	"	kIx"	"
<g/>
bohatý	bohatý	k2eAgInSc4d1	bohatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
osada	osada	k1gFnSc1	osada
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
bohatstvím	bohatství	k1gNnSc7	bohatství
neoplývala	oplývat	k5eNaImAgFnS	oplývat
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
buď	buď	k8xC	buď
za	za	k7c4	za
přání	přání	k1gNnSc4	přání
osadníků	osadník	k1gMnPc2	osadník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
jméno	jméno	k1gNnSc4	jméno
přinesené	přinesený	k2eAgNnSc4d1	přinesené
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
jména	jméno	k1gNnSc2	jméno
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
<g/>
:	:	kIx,	:
Rychberk	Rychberk	k1gInSc1	Rychberk
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lychberk	Lychberk	k1gInSc1	Lychberk
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Libercum	Libercum	k1gInSc1	Libercum
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liberk	Liberk	k1gInSc1	Liberk
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následovala	následovat	k5eAaImAgFnS	následovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
slově	slovo	k1gNnSc6	slovo
dvě	dva	k4xCgNnPc1	dva
písmena	písmeno	k1gNnPc1	písmeno
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
mluvě	mluva	k1gFnSc6	mluva
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
na	na	k7c4	na
"	"	kIx"	"
<g/>
l	l	kA	l
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podobným	podobný	k2eAgInSc7d1	podobný
vývojem	vývoj	k1gInSc7	vývoj
prošel	projít	k5eAaPmAgInS	projít
také	také	k9	také
název	název	k1gInSc4	název
osady	osada	k1gFnSc2	osada
Liberk	Liberk	k1gInSc1	Liberk
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Rehberg	Rehberg	k1gMnSc1	Rehberg
či	či	k8xC	či
Richnberg	Richnberg	k1gMnSc1	Richnberg
<g/>
)	)	kIx)	)
u	u	k7c2	u
Rychnova	Rychnov	k1gInSc2	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Přípona	přípona	k1gFnSc1	přípona
-ec	c	k?	-ec
se	se	k3xPyFc4	se
do	do	k7c2	do
slova	slovo	k1gNnSc2	slovo
dostala	dostat	k5eAaPmAgFnS	dostat
přes	přes	k7c4	přes
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
liberecký	liberecký	k2eAgMnSc1d1	liberecký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původní	původní	k2eAgInSc1d1	původní
tvar	tvar	k1gInSc1	tvar
"	"	kIx"	"
<g/>
liberkský	liberkský	k1gMnSc1	liberkský
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
obtížně	obtížně	k6eAd1	obtížně
vyslovitelný	vyslovitelný	k2eAgInSc1d1	vyslovitelný
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
tvoří	tvořit	k5eAaImIp3nS	tvořit
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
hradební	hradební	k2eAgFnSc4d1	hradební
zeď	zeď	k1gFnSc4	zeď
rubínové	rubínový	k2eAgFnSc2d1	rubínová
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
cimbuřím	cimbuří	k1gNnSc7	cimbuří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stříbrných	stříbrný	k2eAgInPc6d1	stříbrný
závěsech	závěs	k1gInPc6	závěs
zavěšené	zavěšený	k2eAgFnSc2d1	zavěšená
zlaté	zlatý	k2eAgFnSc2d1	zlatá
veřeje	veřej	k1gFnSc2	veřej
brány	brána	k1gFnSc2	brána
jsou	být	k5eAaImIp3nP	být
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
polovinu	polovina	k1gFnSc4	polovina
brány	brána	k1gFnSc2	brána
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
zlatá	zlatý	k2eAgFnSc1d1	zlatá
padací	padací	k2eAgFnSc1d1	padací
mříž	mříž	k1gFnSc1	mříž
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
kovanými	kovaný	k2eAgFnPc7d1	kovaná
špičkami	špička	k1gFnPc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
otevřená	otevřený	k2eAgFnSc1d1	otevřená
branka	branka	k1gFnSc1	branka
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yRgFnSc7	který
je	být	k5eAaImIp3nS	být
střílna	střílna	k1gFnSc1	střílna
a	a	k8xC	a
podlouhlé	podlouhlý	k2eAgNnSc1d1	podlouhlé
okénko	okénko	k1gNnSc1	okénko
dělené	dělený	k2eAgFnSc2d1	dělená
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Věže	věž	k1gFnPc1	věž
mají	mít	k5eAaImIp3nP	mít
stříšku	stříška	k1gFnSc4	stříška
rubínové	rubínový	k2eAgFnSc2d1	rubínová
barvy	barva	k1gFnSc2	barva
zakončenou	zakončený	k2eAgFnSc7d1	zakončená
zlatou	zlatý	k2eAgFnSc7d1	zlatá
makovicí	makovice	k1gFnSc7	makovice
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
vlajícím	vlající	k2eAgInSc7d1	vlající
praporkem	praporek	k1gInSc7	praporek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
nad	nad	k7c7	nad
bránou	brána	k1gFnSc7	brána
visí	viset	k5eAaImIp3nS	viset
štít	štít	k1gInSc1	štít
lazurově	lazurově	k6eAd1	lazurově
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
se	s	k7c7	s
stříbrným	stříbrný	k2eAgNnSc7d1	stříbrné
kolem	kolo	k1gNnSc7	kolo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
znakem	znak	k1gInSc7	znak
Redernů	Redern	k1gInPc2	Redern
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cimbuří	cimbuří	k1gNnSc6	cimbuří
mezi	mezi	k7c7	mezi
věžemi	věž	k1gFnPc7	věž
stojí	stát	k5eAaImIp3nS	stát
rozkročen	rozkročen	k2eAgMnSc1d1	rozkročen
zlatý	zlatý	k2eAgMnSc1d1	zlatý
lev	lev	k1gMnSc1	lev
s	s	k7c7	s
korunkou	korunka	k1gFnSc7	korunka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
vztyčeným	vztyčený	k2eAgInSc7d1	vztyčený
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
městské	městský	k2eAgFnSc2d1	městská
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
šířky	šířka	k1gFnSc2	šířka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
žerďové	žerďové	k2eAgFnSc6d1	žerďové
polovině	polovina	k1gFnSc6	polovina
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
štítě	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
logo	logo	k1gNnSc1	logo
je	být	k5eAaImIp3nS	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
stylizovanými	stylizovaný	k2eAgFnPc7d1	stylizovaná
siluetami	silueta	k1gFnPc7	silueta
dvojice	dvojice	k1gFnSc2	dvojice
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
staveb	stavba	k1gFnPc2	stavba
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
hotelu	hotel	k1gInSc2	hotel
Ještěd	Ještěd	k1gInSc4	Ještěd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
levá	levý	k2eAgFnSc1d1	levá
část	část	k1gFnSc1	část
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
část	část	k1gFnSc1	část
hotelu	hotel	k1gInSc2	hotel
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
je	být	k5eAaImIp3nS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
nápisem	nápis	k1gInSc7	nápis
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
,	,	kIx,	,
na	na	k7c6	na
brodě	brod	k1gInSc6	brod
přes	přes	k7c4	přes
Harcovský	Harcovský	k2eAgInSc4d1	Harcovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
vznikat	vznikat	k5eAaImF	vznikat
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
obchodníci	obchodník	k1gMnPc1	obchodník
mohli	moct	k5eAaImAgMnP	moct
po	po	k7c6	po
náročném	náročný	k2eAgInSc6d1	náročný
přechodu	přechod	k1gInSc6	přechod
Ještědského	ještědský	k2eAgInSc2d1	ještědský
hřebene	hřeben	k1gInSc2	hřeben
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Reychinberch	Reychinbercha	k1gFnPc2	Reychinbercha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Liberec	Liberec	k1gInSc1	Liberec
proti	proti	k7c3	proti
sousednímu	sousední	k2eAgInSc3d1	sousední
Hrádku	Hrádok	k1gInSc3	Hrádok
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
nebo	nebo	k8xC	nebo
Frýdlantu	Frýdlant	k1gInSc3	Frýdlant
bezvýznamný	bezvýznamný	k2eAgMnSc1d1	bezvýznamný
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
významnými	významný	k2eAgMnPc7d1	významný
vlastníky	vlastník	k1gMnPc7	vlastník
okolního	okolní	k2eAgNnSc2d1	okolní
území	území	k1gNnSc2	území
byli	být	k5eAaImAgMnP	být
Bieberštejnové	Bieberštejn	k1gMnPc1	Bieberštejn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
však	však	k9	však
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
Redernové	Redern	k1gMnPc1	Redern
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
liberecký	liberecký	k2eAgInSc4d1	liberecký
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
špitál	špitál	k1gInSc1	špitál
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
panování	panování	k1gNnSc4	panování
císař	císař	k1gMnSc1	císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
povýšil	povýšit	k5eAaPmAgInS	povýšit
Liberec	Liberec	k1gInSc1	Liberec
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znaku	znak	k1gInSc6	znak
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
znak	znak	k1gInSc1	znak
Redernů	Redern	k1gMnPc2	Redern
-	-	kIx~	-
stříbrné	stříbrný	k2eAgNnSc4d1	stříbrné
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Redernu	Rederna	k1gFnSc4	Rederna
dala	dát	k5eAaPmAgFnS	dát
také	také	k9	také
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1599	[number]	k4	1599
<g/>
-	-	kIx~	-
<g/>
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
již	již	k9	již
určovaly	určovat	k5eAaImAgInP	určovat
ráz	ráz	k1gInSc4	ráz
města	město	k1gNnSc2	město
první	první	k4xOgFnSc2	první
kamenné	kamenný	k2eAgFnSc2d1	kamenná
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Redernové	Redern	k1gMnPc1	Redern
po	po	k7c6	po
Bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
opustili	opustit	k5eAaPmAgMnP	opustit
Frýdlantsko	Frýdlantsko	k1gNnSc4	Frýdlantsko
i	i	k8xC	i
Liberecko	Liberecko	k1gNnSc4	Liberecko
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
toto	tento	k3xDgNnSc4	tento
panství	panství	k1gNnSc4	panství
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
město	město	k1gNnSc4	město
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c4	v
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
sukno	sukno	k1gNnSc4	sukno
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgMnSc4	který
oblékal	oblékat	k5eAaImAgMnS	oblékat
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
také	také	k9	také
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Sokolovského	sokolovský	k2eAgNnSc2d1	Sokolovské
náměstí	náměstí	k1gNnSc2	náměstí
zastavěné	zastavěný	k2eAgInPc1d1	zastavěný
hrázděnými	hrázděný	k2eAgInPc7d1	hrázděný
domy	dům	k1gInPc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
tři	tři	k4xCgInPc1	tři
tyto	tento	k3xDgInPc4	tento
"	"	kIx"	"
<g/>
Valdštejnské	valdštejnský	k2eAgInPc4d1	valdštejnský
domky	domek	k1gInPc4	domek
<g/>
"	"	kIx"	"
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
Větrné	větrný	k2eAgFnSc6d1	větrná
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vévodově	vévodův	k2eAgFnSc6d1	vévodova
smrti	smrt	k1gFnSc6	smrt
připadl	připadnout	k5eAaPmAgInS	připadnout
Liberec	Liberec	k1gInSc1	Liberec
Matyáši	Matyáš	k1gMnSc6	Matyáš
z	z	k7c2	z
Gallasu	Gallas	k1gInSc2	Gallas
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
období	období	k1gNnSc1	období
nastalo	nastat	k5eAaPmAgNnS	nastat
pro	pro	k7c4	pro
Liberec	Liberec	k1gInSc4	Liberec
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
původní	původní	k2eAgFnSc2d1	původní
cechovní	cechovní	k2eAgFnSc2d1	cechovní
výroby	výroba	k1gFnSc2	výroba
přerodila	přerodit	k5eAaPmAgFnS	přerodit
v	v	k7c6	v
manufakturní	manufakturní	k2eAgFnSc6d1	manufakturní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c4	na
800	[number]	k4	800
soukenických	soukenický	k2eAgMnPc2d1	soukenický
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
480	[number]	k4	480
tovaryšů	tovaryš	k1gMnPc2	tovaryš
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
jejich	jejich	k3xOp3gMnPc2	jejich
pomocníků	pomocník	k1gMnPc2	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgNnSc7d3	veliký
manufakturním	manufakturní	k2eAgNnSc7d1	manufakturní
městem	město	k1gNnSc7	město
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
důležitost	důležitost	k1gFnSc4	důležitost
ještě	ještě	k6eAd1	ještě
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
přeměnou	přeměna	k1gFnSc7	přeměna
manufaktur	manufaktura	k1gFnPc2	manufaktura
na	na	k7c4	na
textilní	textilní	k2eAgFnPc4d1	textilní
továrny	továrna	k1gFnPc4	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
reprezentativních	reprezentativní	k2eAgFnPc2d1	reprezentativní
budov	budova	k1gFnPc2	budova
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
jeho	jeho	k3xOp3gInSc2	jeho
významu	význam	k1gInSc2	význam
jakožto	jakožto	k8xS	jakožto
druhého	druhý	k4xOgNnSc2	druhý
největšího	veliký	k2eAgNnSc2d3	veliký
města	město	k1gNnSc2	město
Čech	Čechy	k1gFnPc2	Čechy
<g/>
;	;	kIx,	;
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1850	[number]	k4	1850
Liberec	Liberec	k1gInSc1	Liberec
získal	získat	k5eAaPmAgInS	získat
postavení	postavení	k1gNnSc4	postavení
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
ze	z	k7c2	z
správního	správní	k2eAgNnSc2d1	správní
hlediska	hledisko	k1gNnSc2	hledisko
co	co	k9	co
do	do	k7c2	do
důležitosti	důležitost	k1gFnSc2	důležitost
zařadil	zařadit	k5eAaPmAgMnS	zařadit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
hned	hned	k6eAd1	hned
za	za	k7c4	za
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
zlatým	zlatý	k2eAgInSc7d1	zlatý
věkem	věk	k1gInSc7	věk
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
stavělo	stavět	k5eAaImAgNnS	stavět
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
honosných	honosný	k2eAgFnPc2d1	honosná
vil	vila	k1gFnPc2	vila
<g/>
,	,	kIx,	,
moderních	moderní	k2eAgFnPc2d1	moderní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
do	do	k7c2	do
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
zavedena	zaveden	k2eAgFnSc1d1	zavedena
železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
průmyslník	průmyslník	k1gMnSc1	průmyslník
Johann	Johann	k1gMnSc1	Johann
Liebieg	Liebieg	k1gMnSc1	Liebieg
založil	založit	k5eAaPmAgMnS	založit
továrnu	továrna	k1gFnSc4	továrna
Johann	Johann	k1gMnSc1	Johann
Liebieg	Liebieg	k1gMnSc1	Liebieg
&	&	k?	&
Comp	Comp	k1gMnSc1	Comp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
Textilana	textilana	k1gFnSc1	textilana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgInP	být
3	[number]	k4	3
konzuláty	konzulát	k1gInPc1	konzulát
<g/>
,	,	kIx,	,
50	[number]	k4	50
textilních	textilní	k2eAgFnPc2d1	textilní
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
60	[number]	k4	60
kovoprůmyslových	kovoprůmyslový	k2eAgInPc2d1	kovoprůmyslový
podniků	podnik	k1gInPc2	podnik
včetně	včetně	k7c2	včetně
automobilové	automobilový	k2eAgFnSc2d1	automobilová
továrny	továrna	k1gFnSc2	továrna
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
také	také	k9	také
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
liberecká	liberecký	k2eAgFnSc1d1	liberecká
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
byl	být	k5eAaImAgInS	být
převážně	převážně	k6eAd1	převážně
německým	německý	k2eAgNnSc7d1	německé
městem	město	k1gNnSc7	město
se	s	k7c7	s
7	[number]	k4	7
<g/>
%	%	kIx~	%
českou	český	k2eAgFnSc7d1	Česká
menšinou	menšina	k1gFnSc7	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Prosperitu	prosperita	k1gFnSc4	prosperita
ukončila	ukončit	k5eAaPmAgFnS	ukončit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
čtvrti	čtvrt	k1gFnSc2	čtvrt
Ostašov	Ostašov	k1gInSc1	Ostašov
tehdy	tehdy	k6eAd1	tehdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zajatecký	zajatecký	k2eAgInSc1d1	zajatecký
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
000	[number]	k4	000
ruských	ruský	k2eAgMnPc2d1	ruský
a	a	k8xC	a
italských	italský	k2eAgMnPc2d1	italský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1	národnostní
složení	složení	k1gNnSc1	složení
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
problémem	problém	k1gInSc7	problém
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
neocitli	ocitnout	k5eNaPmAgMnP	ocitnout
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gNnSc4	jejich
město	město	k1gNnSc4	město
budou	být	k5eAaImBp3nP	být
ovládat	ovládat	k5eAaImF	ovládat
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
pohraničí	pohraničí	k1gNnSc6	pohraničí
provincii	provincie	k1gFnSc4	provincie
Deutschböhmen	Deutschböhmen	k1gInSc1	Deutschböhmen
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
měnou	měna	k1gFnSc7	měna
a	a	k8xC	a
Libercem	Liberec	k1gInSc7	Liberec
jako	jako	k8xS	jako
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
přidružit	přidružit	k5eAaPmF	přidružit
se	se	k3xPyFc4	se
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
nebo	nebo	k8xC	nebo
Rakousku	Rakousko	k1gNnSc6	Rakousko
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
nová	nový	k2eAgFnSc1d1	nová
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
území	území	k1gNnSc2	území
obsadila	obsadit	k5eAaPmAgFnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1923	[number]	k4	1923
prezidenta	prezident	k1gMnSc2	prezident
české	český	k2eAgFnSc2d1	Česká
zemské	zemský	k2eAgFnSc2d1	zemská
správy	správa	k1gFnSc2	správa
politické	politický	k2eAgFnSc2d1	politická
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1923	[number]	k4	1923
libereckému	liberecký	k2eAgInSc3d1	liberecký
magistrátu	magistrát	k1gInSc3	magistrát
odňala	odnít	k5eAaPmAgFnS	odnít
kompetence	kompetence	k1gFnSc1	kompetence
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgNnSc2d1	tvořené
tehdy	tehdy	k6eAd1	tehdy
pouze	pouze	k6eAd1	pouze
pěti	pět	k4xCc2	pět
centrálními	centrální	k2eAgFnPc7d1	centrální
čtvrtěmi	čtvrt	k1gFnPc7	čtvrt
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Jeřáb	jeřáb	k1gInSc1	jeřáb
<g/>
,	,	kIx,	,
Perštýn	Perštýn	k1gInSc1	Perštýn
a	a	k8xC	a
Kristiánov	Kristiánov	k1gInSc1	Kristiánov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
politickým	politický	k2eAgInSc7d1	politický
okresem	okres	k1gInSc7	okres
Liberec-venkov	Liberecenkov	k1gInSc1	Liberec-venkov
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
okresu	okres	k1gInSc2	okres
Liberec	Liberec	k1gInSc4	Liberec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
prakticky	prakticky	k6eAd1	prakticky
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jím	jíst	k5eAaImIp1nS	jíst
formálně	formálně	k6eAd1	formálně
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
Sudetendeutsche	Sudetendeutschus	k1gMnSc5	Sudetendeutschus
Partei	Parte	k1gMnSc5	Parte
-	-	kIx~	-
SdP	SdP	k1gMnSc6	SdP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předákem	předák	k1gMnSc7	předák
byl	být	k5eAaImAgMnS	být
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Vratislavic	Vratislavice	k1gFnPc2	Vratislavice
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
Konrad	Konrad	k1gInSc4	Konrad
Henlein	Henlein	k2eAgInSc4d1	Henlein
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1938	[number]	k4	1938
připadly	připadnout	k5eAaPmAgFnP	připadnout
Sudety	Sudety	k1gFnPc1	Sudety
německé	německý	k2eAgFnPc1d1	německá
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc1	veškerý
české	český	k2eAgNnSc1d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
muselo	muset	k5eAaImAgNnS	muset
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nové	nový	k2eAgFnSc2d1	nová
sudetské	sudetský	k2eAgFnSc2d1	sudetská
župy	župa	k1gFnSc2	župa
(	(	kIx(	(
<g/>
Gauhauptstadt	Gauhauptstadt	k1gInSc1	Gauhauptstadt
<g/>
)	)	kIx)	)
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
místodržícího	místodržící	k1gMnSc2	místodržící
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
připojením	připojení	k1gNnSc7	připojení
předměstských	předměstský	k2eAgFnPc2d1	předměstská
obcí	obec	k1gFnPc2	obec
Rochlice	Rochlice	k1gFnSc2	Rochlice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Růžodol	růžodol	k1gInSc1	růžodol
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInSc1d1	dolní
Hanychov	Hanychov	k1gInSc1	Hanychov
<g/>
,	,	kIx,	,
Janův	Janův	k2eAgInSc1d1	Janův
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Františkov	Františkov	k1gInSc1	Františkov
<g/>
,	,	kIx,	,
Růžodol	růžodol	k1gInSc1	růžodol
I	I	kA	I
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnPc1d1	Staré
a	a	k8xC	a
Nové	Nové	k2eAgFnPc1d1	Nové
Pavlovice	Pavlovice	k1gFnPc1	Pavlovice
<g/>
,	,	kIx,	,
Ruprechtice	Ruprechtice	k1gFnPc1	Ruprechtice
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Harcov	Harcov	k1gInSc1	Harcov
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
Liberec	Liberec	k1gInSc1	Liberec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
žilo	žít	k5eAaImAgNnS	žít
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
město	město	k1gNnSc4	město
výrazně	výrazně	k6eAd1	výrazně
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
,	,	kIx,	,
vynutila	vynutit	k5eAaPmAgFnS	vynutit
si	se	k3xPyFc3	se
však	však	k9	však
změny	změna	k1gFnPc1	změna
struktury	struktura	k1gFnSc2	struktura
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
vydávány	vydávat	k5eAaImNgFnP	vydávat
polooficiální	polooficiální	k2eAgFnPc1d1	polooficiální
bankovky	bankovka	k1gFnPc1	bankovka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
20	[number]	k4	20
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
vysídleni	vysídlen	k2eAgMnPc1d1	vysídlen
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Liberec	Liberec	k1gInSc1	Liberec
stal	stát	k5eAaPmAgInS	stát
opět	opět	k6eAd1	opět
plnohodnotným	plnohodnotný	k2eAgNnSc7d1	plnohodnotné
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
po	po	k7c6	po
správní	správní	k2eAgFnSc6d1	správní
reformě	reforma	k1gFnSc6	reforma
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
však	však	k9	však
střediskem	středisko	k1gNnSc7	středisko
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
zrušen	zrušen	k2eAgInSc1d1	zrušen
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
a	a	k8xC	a
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
pouze	pouze	k6eAd1	pouze
městem	město	k1gNnSc7	město
okresním	okresní	k2eAgNnSc7d1	okresní
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Severočeského	severočeský	k2eAgInSc2d1	severočeský
kraje	kraj	k1gInSc2	kraj
spravovaného	spravovaný	k2eAgNnSc2d1	spravované
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
projížděla	projíždět	k5eAaImAgNnP	projíždět
Libercem	Liberec	k1gInSc7	Liberec
polská	polský	k2eAgNnPc1d1	polské
okupační	okupační	k2eAgNnPc1d1	okupační
vojska	vojsko	k1gNnPc1	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vpád	vpád	k1gInSc1	vpád
ve	v	k7c6	v
městě	město	k1gNnSc6	město
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
způsobil	způsobit	k5eAaPmAgInS	způsobit
smrt	smrt	k1gFnSc4	smrt
9	[number]	k4	9
nevinných	vinný	k2eNgFnPc2d1	nevinná
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
památník	památník	k1gInSc1	památník
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
zničení	zničení	k1gNnSc1	zničení
průčelí	průčelí	k1gNnSc2	průčelí
domů	dům	k1gInPc2	dům
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
také	také	k9	také
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
a	a	k8xC	a
Jana	Jan	k1gMnSc4	Jan
Třísku	Tříska	k1gMnSc4	Tříska
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
svobodného	svobodný	k2eAgNnSc2d1	svobodné
vysílání	vysílání	k1gNnSc2	vysílání
libereckého	liberecký	k2eAgInSc2d1	liberecký
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
Liberec	Liberec	k1gInSc1	Liberec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
stal	stát	k5eAaPmAgInS	stát
opět	opět	k6eAd1	opět
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krachu	krach	k1gInSc6	krach
textilních	textilní	k2eAgFnPc2d1	textilní
továren	továrna	k1gFnPc2	továrna
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
struktura	struktura	k1gFnSc1	struktura
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc1d1	obchodní
zóny	zóna	k1gFnPc1	zóna
a	a	k8xC	a
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
centrem	centrum	k1gNnSc7	centrum
nového	nový	k2eAgInSc2d1	nový
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
2	[number]	k4	2
514	[number]	k4	514
domech	dům	k1gInPc6	dům
34	[number]	k4	34
985	[number]	k4	985
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
18	[number]	k4	18
453	[number]	k4	453
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
4	[number]	k4	4
894	[number]	k4	894
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
27	[number]	k4	27
929	[number]	k4	929
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
130	[number]	k4	130
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
29	[number]	k4	29
139	[number]	k4	139
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
2	[number]	k4	2
725	[number]	k4	725
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
173	[number]	k4	173
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1	[number]	k4	1
312	[number]	k4	312
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
3	[number]	k4	3
072	[number]	k4	072
domech	dům	k1gInPc6	dům
38	[number]	k4	38
568	[number]	k4	568
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
6	[number]	k4	6
314	[number]	k4	314
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
30	[number]	k4	30
023	[number]	k4	023
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
30	[number]	k4	30
285	[number]	k4	285
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
3	[number]	k4	3
294	[number]	k4	294
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
889	[number]	k4	889
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1	[number]	k4	1
392	[number]	k4	392
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Liberce	Liberec	k1gInSc2	Liberec
stále	stále	k6eAd1	stále
stoupal	stoupat	k5eAaImAgInS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
stotisícovou	stotisícový	k2eAgFnSc4d1	stotisícová
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
104	[number]	k4	104
233	[number]	k4	233
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
počet	počet	k1gInSc1	počet
libereckých	liberecký	k2eAgMnPc2d1	liberecký
občanů	občan	k1gMnPc2	občan
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
až	až	k6eAd1	až
pod	pod	k7c4	pod
hranici	hranice	k1gFnSc4	hranice
statisíc	statisit	k5eAaImSgFnS	statisit
<g/>
,	,	kIx,	,
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obratu	obrat	k1gInSc3	obrat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
mezi	mezi	k7c4	mezi
stotisícová	stotisícový	k2eAgNnPc4d1	stotisícové
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
aglomerace	aglomerace	k1gFnSc1	aglomerace
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
kromě	kromě	k7c2	kromě
těchto	tento	k3xDgNnPc2	tento
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
spadá	spadat	k5eAaPmIp3nS	spadat
dalších	další	k2eAgFnPc2d1	další
22	[number]	k4	22
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Liberecká	liberecký	k2eAgFnSc1d1	liberecká
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
starou	starý	k2eAgFnSc4d1	stará
radnici	radnice	k1gFnSc4	radnice
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1599	[number]	k4	1599
<g/>
-	-	kIx~	-
<g/>
1603	[number]	k4	1603
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
štítem	štít	k1gInSc7	štít
stavitele	stavitel	k1gMnSc2	stavitel
Marcuse	Marcuse	k1gFnSc2	Marcuse
Antonia	Antonio	k1gMnSc2	Antonio
Spazia	Spazius	k1gMnSc2	Spazius
de	de	k?	de
Lanza	Lanz	k1gMnSc2	Lanz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
půdorys	půdorys	k1gInSc1	půdorys
je	být	k5eAaImIp3nS	být
naznačen	naznačit	k5eAaPmNgInS	naznačit
v	v	k7c6	v
dlažbě	dlažba	k1gFnSc6	dlažba
náměstí	náměstí	k1gNnSc2	náměstí
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Širším	široký	k2eAgInSc7d2	širší
symbolem	symbol	k1gInSc7	symbol
nejen	nejen	k6eAd1	nejen
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
vysílač	vysílač	k1gInSc1	vysílač
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
také	také	k9	také
liberecký	liberecký	k2eAgInSc1d1	liberecký
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
nejstaršími	starý	k2eAgFnPc7d3	nejstarší
dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
stavbami	stavba	k1gFnPc7	stavba
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
Valdštejnské	valdštejnský	k2eAgInPc4d1	valdštejnský
domky	domek	k1gInPc4	domek
ve	v	k7c6	v
Větrné	větrný	k2eAgFnSc6d1	větrná
ulici	ulice	k1gFnSc6	ulice
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1678	[number]	k4	1678
<g/>
-	-	kIx~	-
<g/>
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zámeckém	zámecký	k2eAgInSc6d1	zámecký
kopci	kopec	k1gInSc6	kopec
(	(	kIx(	(
<g/>
375	[number]	k4	375
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Hamrštejn	Hamrštejna	k1gFnPc2	Hamrštejna
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
Libercem	Liberec	k1gInSc7	Liberec
lze	lze	k6eAd1	lze
také	také	k9	také
nalézt	nalézt	k5eAaPmF	nalézt
Libereckou	liberecký	k2eAgFnSc4d1	liberecká
výšinu	výšina	k1gFnSc4	výšina
<g/>
,	,	kIx,	,
restauraci	restaurace	k1gFnSc4	restaurace
a	a	k8xC	a
rozhlednu	rozhledna	k1gFnSc4	rozhledna
postavenou	postavený	k2eAgFnSc4d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
postaven	postaven	k2eAgInSc1d1	postaven
tzv.	tzv.	kA	tzv.
Šolcův	Šolcův	k2eAgInSc1d1	Šolcův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
patrový	patrový	k2eAgInSc1d1	patrový
roubený	roubený	k2eAgInSc1d1	roubený
domek	domek	k1gInSc1	domek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Jizerské	jizerský	k2eAgFnSc2d1	Jizerská
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Harcovského	Harcovský	k2eAgInSc2d1	Harcovský
potoka	potok	k1gInSc2	potok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
novorenesanční	novorenesanční	k2eAgNnSc1d1	novorenesanční
bývalé	bývalý	k2eAgNnSc1d1	bývalé
sídlo	sídlo	k1gNnSc1	sídlo
rodiny	rodina	k1gFnSc2	rodina
textilních	textilní	k2eAgMnPc2d1	textilní
průmyslníků	průmyslník	k1gMnPc2	průmyslník
Liebiegů	Liebieg	k1gMnPc2	Liebieg
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Liebiegův	Liebiegův	k2eAgInSc1d1	Liebiegův
zámeček	zámeček	k1gInSc1	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
městského	městský	k2eAgInSc2d1	městský
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
Liebiegovu	Liebiegův	k2eAgFnSc4d1	Liebiegova
vilu	vila	k1gFnSc4	vila
<g/>
,	,	kIx,	,
postavenou	postavený	k2eAgFnSc4d1	postavená
též	též	k9	též
v	v	k7c6	v
novorenesačním	novorenesační	k2eAgInSc6d1	novorenesační
slohu	sloh	k1gInSc6	sloh
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1871	[number]	k4	1871
a	a	k8xC	a
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
sídlila	sídlit	k5eAaImAgFnS	sídlit
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
ji	on	k3xPp3gFnSc4	on
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
bývalých	bývalý	k2eAgFnPc6d1	bývalá
městských	městský	k2eAgFnPc6d1	městská
lázních	lázeň	k1gFnPc6	lázeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novorenesančních	novorenesanční	k2eAgFnPc2d1	novorenesanční
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Divadlo	divadlo	k1gNnSc1	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
vídeňskými	vídeňský	k2eAgMnPc7d1	vídeňský
architekty	architekt	k1gMnPc7	architekt
Fellner	Fellnero	k1gNnPc2	Fellnero
a	a	k8xC	a
Helmer	Helmra	k1gFnPc2	Helmra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
opona	opona	k1gFnSc1	opona
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Gustava	Gustav	k1gMnSc2	Gustav
Klimta	Klimt	k1gMnSc2	Klimt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
městské	městský	k2eAgFnPc1d1	městská
lázně	lázeň	k1gFnPc1	lázeň
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
nebo	nebo	k8xC	nebo
budova	budova	k1gFnSc1	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
<g/>
,	,	kIx,	,
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
dle	dle	k7c2	dle
projektu	projekt	k1gInSc2	projekt
Miksche	Miksch	k1gMnSc2	Miksch
a	a	k8xC	a
Niedzielskiho	Niedzielski	k1gMnSc2	Niedzielski
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
průčelí	průčelí	k1gNnSc1	průčelí
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
do	do	k7c2	do
Felberovy	Felberův	k2eAgFnSc2d1	Felberova
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc1d2	delší
postranní	postranní	k2eAgFnSc1d1	postranní
fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
do	do	k7c2	do
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Třípodlažní	třípodlažní	k2eAgInSc1d1	třípodlažní
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
o	o	k7c4	o
suterén	suterén	k1gInSc4	suterén
dostavěný	dostavěný	k2eAgInSc4d1	dostavěný
r.	r.	kA	r.
1891	[number]	k4	1891
A.	A.	kA	A.
Bürgerem	Bürger	k1gInSc7	Bürger
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
novorenesanční	novorenesanční	k2eAgInSc1d1	novorenesanční
<g/>
,	,	kIx,	,
vnějšek	vnějšek	k1gInSc1	vnějšek
je	být	k5eAaImIp3nS	být
členěn	členit	k5eAaImNgInS	členit
pilastry	pilastr	k1gInPc7	pilastr
a	a	k8xC	a
polosloupy	polosloup	k1gInPc7	polosloup
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
okrové	okrový	k2eAgNnSc1d1	okrové
zbarvení	zbarvení	k1gNnSc1	zbarvení
díky	díky	k7c3	díky
obkladu	obklad	k1gInSc3	obklad
z	z	k7c2	z
hořického	hořický	k2eAgInSc2d1	hořický
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
je	být	k5eAaImIp3nS	být
zdoben	zdobit	k5eAaImNgInS	zdobit
dvěma	dva	k4xCgInPc7	dva
toskánskými	toskánský	k2eAgInPc7d1	toskánský
sloupy	sloup	k1gInPc7	sloup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesou	nést	k5eAaImIp3nP	nést
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
štít	štít	k1gInSc4	štít
podepřený	podepřený	k2eAgInSc4d1	podepřený
triglyfy	triglyf	k1gInPc7	triglyf
a	a	k8xC	a
metopami	metopa	k1gFnPc7	metopa
(	(	kIx(	(
<g/>
deska	deska	k1gFnSc1	deska
dórského	dórský	k2eAgInSc2d1	dórský
vlysu	vlys	k1gInSc2	vlys
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
reliéfní	reliéfní	k2eAgFnSc7d1	reliéfní
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
plastik	plastika	k1gFnPc2	plastika
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
R.	R.	kA	R.
Weyr	Weyr	k1gInSc1	Weyr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nárožích	nároží	k1gNnPc6	nároží
atiky	atika	k1gFnSc2	atika
jsou	být	k5eAaImIp3nP	být
obelisky	obelisk	k1gInPc1	obelisk
sejmuté	sejmutý	k2eAgInPc1d1	sejmutý
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
jsou	být	k5eAaImIp3nP	být
sloupky	sloupek	k1gInPc1	sloupek
s	s	k7c7	s
dříky	dřík	k1gInPc7	dřík
z	z	k7c2	z
hlazené	hlazený	k2eAgFnSc2d1	hlazená
mauthausenské	mauthausenský	k2eAgFnSc2d1	mauthausenský
žuly	žula	k1gFnSc2	žula
a	a	k8xC	a
s	s	k7c7	s
hlavicemi	hlavice	k1gFnPc7	hlavice
z	z	k7c2	z
caraského	caraský	k2eAgInSc2d1	caraský
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
mramorové	mramorový	k2eAgNnSc1d1	mramorové
schodiště	schodiště	k1gNnSc1	schodiště
s	s	k7c7	s
kovovým	kovový	k2eAgNnSc7d1	kovové
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
zábradlím	zábradlí	k1gNnSc7	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Stropy	strop	k1gInPc1	strop
jsou	být	k5eAaImIp3nP	být
zdobeny	zdobit	k5eAaImNgFnP	zdobit
sádrovou	sádrový	k2eAgFnSc7d1	sádrová
a	a	k8xC	a
stěny	stěna	k1gFnSc2	stěna
mramorovou	mramorový	k2eAgFnSc7d1	mramorová
štukou	štuka	k1gFnSc7	štuka
<g/>
,	,	kIx,	,
na	na	k7c6	na
obojím	obojí	k4xRgMnSc6	obojí
jsou	být	k5eAaImIp3nP	být
medailóny	medailón	k1gInPc7	medailón
významných	významný	k2eAgFnPc2d1	významná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
spořitelny	spořitelna	k1gFnSc2	spořitelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
i	i	k8xC	i
doklady	doklad	k1gInPc1	doklad
baroka	baroko	k1gNnSc2	baroko
nebo	nebo	k8xC	nebo
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
domy	dům	k1gInPc4	dům
například	například	k6eAd1	například
na	na	k7c6	na
Sokolovském	sokolovský	k2eAgInSc6d1	sokolovský
(	(	kIx(	(
<g/>
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
<g/>
)	)	kIx)	)
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
264	[number]	k4	264
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgInSc4d1	klasicistní
se	se	k3xPyFc4	se
sloupovým	sloupový	k2eAgInSc7d1	sloupový
portálem	portál	k1gInSc7	portál
a	a	k8xC	a
balkónem	balkón	k1gInSc7	balkón
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Appeltův	Appeltův	k2eAgInSc1d1	Appeltův
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Barvířské	barvířský	k2eAgFnSc6d1	barvířská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
<g />
.	.	kIx.	.
</s>
<s>
122	[number]	k4	122
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
s	s	k7c7	s
lomenou	lomený	k2eAgFnSc7d1	lomená
atikou	atika	k1gFnSc7	atika
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
čtyř	čtyři	k4xCgNnPc2	čtyři
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
;	;	kIx,	;
čp.	čp.	k?	čp.
46	[number]	k4	46
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
klasicistně-empírový	klasicistněmpírový	k2eAgMnSc1d1	klasicistně-empírový
s	s	k7c7	s
portálem	portál	k1gInSc7	portál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Růžové	růžový	k2eAgFnSc3d1	růžová
ulici	ulice	k1gFnSc3	ulice
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
299	[number]	k4	299
klasicistní	klasicistní	k2eAgMnSc1d1	klasicistní
se	s	k7c7	s
štítem	štít	k1gInSc7	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc6	náměstí
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
<g />
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgMnSc1d1	klasicistní
se	s	k7c7	s
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Krausův	Krausův	k2eAgInSc1d1	Krausův
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kostelní	kostelní	k2eAgFnSc4d1	kostelní
ulici	ulice	k1gFnSc4	ulice
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
7	[number]	k4	7
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1784	[number]	k4	1784
<g/>
-	-	kIx~	-
<g/>
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
budova	budova	k1gFnSc1	budova
arciděkanství	arciděkanství	k1gNnSc2	arciděkanství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
10	[number]	k4	10
<g />
.	.	kIx.	.
</s>
<s>
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
s	s	k7c7	s
podloubím	podloubí	k1gNnSc7	podloubí
<g/>
;	;	kIx,	;
čp.	čp.	k?	čp.
14	[number]	k4	14
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
24	[number]	k4	24
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
26	[number]	k4	26
a	a	k8xC	a
35	[number]	k4	35
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
klasicistní	klasicistní	k2eAgInPc1d1	klasicistní
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
70	[number]	k4	70
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
se	se	k3xPyFc4	se
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
náměstí	náměstí	k1gNnSc6	náměstí
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc4	Beneš
nacházela	nacházet	k5eAaImAgFnS	nacházet
empírová	empírový	k2eAgFnSc1d1	empírová
Neptunova	Neptunův	k2eAgFnSc1d1	Neptunova
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
přenesena	přenést	k5eAaPmNgFnS	přenést
na	na	k7c4	na
Nerudovo	Nerudův	k2eAgNnSc4d1	Nerudovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
opět	opět	k6eAd1	opět
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gInSc1	originál
sochy	socha	k1gFnSc2	socha
Neptuna	Neptun	k1gMnSc2	Neptun
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
i	i	k9	i
Severočeské	severočeský	k2eAgNnSc1d1	Severočeské
muzeum	muzeum	k1gNnSc1	muzeum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
s	s	k7c7	s
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
věží	věž	k1gFnSc7	věž
(	(	kIx(	(
<g/>
kopií	kopie	k1gFnSc7	kopie
věže	věž	k1gFnSc2	věž
původní	původní	k2eAgFnSc2d1	původní
liberecké	liberecký	k2eAgFnSc2d1	liberecká
radnice	radnice	k1gFnSc2	radnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
před	před	k7c7	před
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
třídě	třída	k1gFnSc6	třída
nalézá	nalézat	k5eAaImIp3nS	nalézat
busta	busta	k1gFnSc1	busta
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
odhalená	odhalený	k2eAgFnSc1d1	odhalená
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Sobotkou	Sobotka	k1gMnSc7	Sobotka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
k	k	k7c3	k
prezidentovým	prezidentův	k2eAgMnPc3d1	prezidentův
160	[number]	k4	160
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
spodního	spodní	k2eAgNnSc2d1	spodní
centra	centrum	k1gNnSc2	centrum
Liberce	Liberec	k1gInSc2	Liberec
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
Krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výškou	výška	k1gFnSc7	výška
78	[number]	k4	78
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
desátou	desátá	k1gFnSc7	desátá
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
věže	věž	k1gFnPc4	věž
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
Hubáčkův	Hubáčkův	k2eAgInSc1d1	Hubáčkův
obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Ještěd	Ještěd	k1gInSc1	Ještěd
půdorysného	půdorysný	k2eAgInSc2d1	půdorysný
tvaru	tvar	k1gInSc2	tvar
"	"	kIx"	"
<g/>
včelích	včelí	k2eAgFnPc2d1	včelí
buněk	buňka	k1gFnPc2	buňka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
výrazný	výrazný	k2eAgInSc1d1	výrazný
netradičním	tradiční	k2eNgInSc7d1	netradiční
žlutým	žlutý	k2eAgInSc7d1	žlutý
keramickým	keramický	k2eAgInSc7d1	keramický
obkladem	obklad	k1gInSc7	obklad
a	a	k8xC	a
ocelovým	ocelový	k2eAgNnSc7d1	ocelové
obložením	obložení	k1gNnSc7	obložení
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zbourán	zbourán	k2eAgInSc1d1	zbourán
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
budovami	budova	k1gFnPc7	budova
nového	nový	k2eAgNnSc2d1	nové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
Forum	forum	k1gNnSc1	forum
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Velikého	veliký	k2eAgMnSc2d1	veliký
-	-	kIx~	-
původně	původně	k6eAd1	původně
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
zděný	zděný	k2eAgInSc4d1	zděný
trojlodní	trojlodní	k2eAgInSc4d1	trojlodní
kostel	kostel	k1gInSc4	kostel
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
-	-	kIx~	-
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejcennější	cenný	k2eAgInSc4d3	nejcennější
liberecký	liberecký	k2eAgInSc4d1	liberecký
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hřbitovní	hřbitovní	k2eAgInSc4d1	hřbitovní
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
postaven	postavit	k5eAaPmNgInS	postavit
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1695	[number]	k4	1695
<g/>
-	-	kIx~	-
<g/>
1698	[number]	k4	1698
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1753	[number]	k4	1753
<g/>
-	-	kIx~	-
<g/>
1756	[number]	k4	1756
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sálovou	sálový	k2eAgFnSc4d1	sálová
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
pravoúhlým	pravoúhlý	k2eAgInSc7d1	pravoúhlý
presbytářem	presbytář	k1gInSc7	presbytář
<g/>
,	,	kIx,	,
oratořemi	oratoř	k1gFnPc7	oratoř
<g/>
,	,	kIx,	,
sakristií	sakristie	k1gFnSc7	sakristie
a	a	k8xC	a
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
,	,	kIx,	,
průčelí	průčelí	k1gNnSc1	průčelí
je	být	k5eAaImIp3nS	být
zvlněné	zvlněný	k2eAgFnSc3d1	zvlněná
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hranolové	hranolový	k2eAgFnPc4d1	hranolová
věže	věž	k1gFnPc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bohaté	bohatý	k2eAgNnSc1d1	bohaté
rokokové	rokokový	k2eAgNnSc1d1	rokokové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
nástropní	nástropní	k2eAgFnPc1d1	nástropní
fresky	freska	k1gFnPc1	freska
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1761	[number]	k4	1761
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
svatém	svatý	k2eAgInSc6d1	svatý
Kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
přemalovány	přemalován	k2eAgFnPc1d1	přemalována
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sloup	sloup	k1gInSc1	sloup
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1719	[number]	k4	1719
<g/>
-	-	kIx~	-
<g/>
1720	[number]	k4	1720
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Matyáše	Matyáš	k1gMnSc2	Matyáš
Brauna	Braun	k1gMnSc2	Braun
a	a	k8xC	a
výklenkové	výklenkový	k2eAgFnSc2d1	výklenková
kaple	kaple	k1gFnSc2	kaple
křížové	křížový	k2eAgFnSc2d1	křížová
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1855	[number]	k4	1855
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
původního	původní	k2eAgInSc2d1	původní
chrámu	chrám	k1gInSc2	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
<g />
.	.	kIx.	.
</s>
<s>
Božského	božský	k2eAgNnSc2d1	božské
Srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgMnSc1d1	páně
a	a	k8xC	a
bývalý	bývalý	k2eAgInSc1d1	bývalý
klášter	klášter	k1gInSc1	klášter
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
Voršilek	voršilka	k1gFnPc2	voršilka
-	-	kIx~	-
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
novogotickém	novogotický	k2eAgInSc6d1	novogotický
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
poliklinika	poliklinika	k1gFnSc1	poliklinika
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgInSc2d1	paduánský
kostel	kostel	k1gInSc1	kostel
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
U	u	k7c2	u
Obrázku	obrázek	k1gInSc2	obrázek
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Bonifáce	Bonifác	k1gMnSc2	Bonifác
-	-	kIx~	-
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Hanychově	Hanychův	k2eAgInSc6d1	Hanychův
kostel	kostel	k1gInSc4	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
-	-	kIx~	-
novobarokní	novobarokní	k2eAgInSc1d1	novobarokní
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
<g />
.	.	kIx.	.
</s>
<s>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
sálová	sálový	k2eAgFnSc1d1	sálová
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
postranními	postranní	k2eAgFnPc7d1	postranní
kaplemi	kaple	k1gFnPc7	kaple
<g/>
,	,	kIx,	,
půlkruhově	půlkruhově	k6eAd1	půlkruhově
zakončeným	zakončený	k2eAgInSc7d1	zakončený
presbytářem	presbytář	k1gInSc7	presbytář
<g/>
,	,	kIx,	,
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
předsíní	předsíň	k1gFnSc7	předsíň
a	a	k8xC	a
obytnou	obytný	k2eAgFnSc7d1	obytná
přístavbou	přístavba	k1gFnSc7	přístavba
(	(	kIx(	(
<g/>
kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
rezidence	rezidence	k1gFnSc1	rezidence
<g/>
)	)	kIx)	)
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vincence	Vincenc	k1gMnSc4	Vincenc
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
-	-	kIx~	-
na	na	k7c6	na
Perštýně	Perštýn	k1gInSc6	Perštýn
<g/>
,	,	kIx,	,
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1868	[number]	k4	1868
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
novorománském	novorománský	k2eAgInSc6d1	novorománský
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
stejnolodní	stejnolodní	k2eAgFnSc1d1	stejnolodní
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
průčelím	průčelí	k1gNnSc7	průčelí
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
portálem	portál	k1gInSc7	portál
a	a	k8xC	a
kopulí	kopule	k1gFnSc7	kopule
kostel	kostel	k1gInSc1	kostel
Navštívení	navštívení	k1gNnSc6	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
-	-	kIx~	-
pozdně	pozdně	k6eAd1	pozdně
empírový	empírový	k2eAgInSc1d1	empírový
kostel	kostel	k1gInSc1	kostel
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Harcově	Harcův	k2eAgNnSc6d1	Harcův
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
-	-	kIx~	-
v	v	k7c6	v
Rochlicích	Rochlice	k1gFnPc6	Rochlice
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založen	založit	k5eAaPmNgMnS	založit
jako	jako	k8xS	jako
evangelický	evangelický	k2eAgInSc1d1	evangelický
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
středověký	středověký	k2eAgInSc4d1	středověký
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
barokní	barokní	k2eAgFnSc4d1	barokní
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
půlkruhově	půlkruhově	k6eAd1	půlkruhově
zakončeným	zakončený	k2eAgInSc7d1	zakončený
presbytářem	presbytář	k1gInSc7	presbytář
a	a	k8xC	a
věží	věž	k1gFnSc7	věž
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hrobka	hrobka	k1gFnSc1	hrobka
Appeltů	Appelt	k1gInPc2	Appelt
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgMnSc1d1	barokní
a	a	k8xC	a
novorenesanční	novorenesanční	k2eAgMnSc1d1	novorenesanční
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zaniklých	zaniklý	k2eAgFnPc2d1	zaniklá
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
stará	starý	k2eAgFnSc1d1	stará
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
vypálená	vypálený	k2eAgFnSc1d1	vypálená
za	za	k7c2	za
křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
noci	noc	k1gFnSc2	noc
nacisty	nacista	k1gMnSc2	nacista
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
místě	místo	k1gNnSc6	místo
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
Krajské	krajský	k2eAgFnSc2d1	krajská
vědecké	vědecký	k2eAgFnSc2d1	vědecká
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Stavby	stavba	k1gFnSc2	stavba
smíření	smíření	k1gNnSc2	smíření
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
přispěním	přispění	k1gNnSc7	přispění
synagoga	synagoga	k1gFnSc1	synagoga
nová	nový	k2eAgFnSc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
také	také	k9	také
pro	pro	k7c4	pro
celkovou	celkový	k2eAgFnSc4d1	celková
zchátralost	zchátralost	k1gFnSc4	zchátralost
a	a	k8xC	a
zanedbání	zanedbání	k1gNnSc4	zanedbání
základní	základní	k2eAgFnSc2d1	základní
údržby	údržba	k1gFnSc2	údržba
stržen	stržen	k2eAgInSc1d1	stržen
novogotický	novogotický	k2eAgInSc1d1	novogotický
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
donucena	donucen	k2eAgFnSc1d1	donucena
jej	on	k3xPp3gMnSc4	on
bezplatně	bezplatně	k6eAd1	bezplatně
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
postavený	postavený	k2eAgMnSc1d1	postavený
dle	dle	k7c2	dle
plánů	plán	k1gInPc2	plán
Gustava	Gustav	k1gMnSc2	Gustav
Sacherse	Sacherse	k1gFnSc2	Sacherse
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
krematorium	krematorium	k1gNnSc1	krematorium
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
hřbitova	hřbitov	k1gInSc2	hřbitov
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
park	park	k1gInSc1	park
Zahrada	zahrada	k1gFnSc1	zahrada
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgFnP	být
přemístěny	přemístěn	k2eAgInPc4d1	přemístěn
některé	některý	k3yIgInPc4	některý
památkově	památkově	k6eAd1	památkově
chráněné	chráněný	k2eAgInPc4d1	chráněný
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
3163	[number]	k4	3163
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zhruba	zhruba	k6eAd1	zhruba
440	[number]	k4	440
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
59	[number]	k4	59
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
liberecký	liberecký	k2eAgInSc1d1	liberecký
obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
z	z	k7c2	z
28	[number]	k4	28
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
justice	justice	k1gFnSc2	justice
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
i	i	k8xC	i
pobočka	pobočka	k1gFnSc1	pobočka
ústeckého	ústecký	k2eAgInSc2d1	ústecký
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
pobočky	pobočka	k1gFnSc2	pobočka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
okresní	okresní	k2eAgInSc1d1	okresní
a	a	k8xC	a
pobočka	pobočka	k1gFnSc1	pobočka
krajského	krajský	k2eAgNnSc2d1	krajské
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
samostatné	samostatný	k2eAgNnSc1d1	samostatné
krajské	krajský	k2eAgNnSc1d1	krajské
ředitelství	ředitelství	k1gNnSc1	ředitelství
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
má	mít	k5eAaImIp3nS	mít
26	[number]	k4	26
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
člení	členit	k5eAaImIp3nS	členit
se	se	k3xPyFc4	se
na	na	k7c4	na
33	[number]	k4	33
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
městské	městský	k2eAgFnPc4d1	městská
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
32	[number]	k4	32
je	být	k5eAaImIp3nS	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
přímo	přímo	k6eAd1	přímo
libereckým	liberecký	k2eAgInSc7d1	liberecký
magistrátem	magistrát	k1gInSc7	magistrát
a	a	k8xC	a
Vratislavice	Vratislavice	k1gFnSc1	Vratislavice
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
jsou	být	k5eAaImIp3nP	být
samosprávný	samosprávný	k2eAgInSc4d1	samosprávný
městský	městský	k2eAgInSc4d1	městský
obvod	obvod	k1gInSc4	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnSc3d1	městská
čtvrti	čtvrt	k1gFnSc3	čtvrt
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
číslují	číslovat	k5eAaImIp3nP	číslovat
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ale	ale	k9	ale
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
:	:	kIx,	:
Liberec	Liberec	k1gInSc1	Liberec
XXVI-Stráž	XXVI-Stráž	k1gFnSc1	XXVI-Stráž
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
XXVII-Svárov	XXVII-Svárov	k1gInSc1	XXVII-Svárov
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
XXXVI-Dlouhý	XXXVI-Dlouhý	k2eAgInSc1d1	XXXVI-Dlouhý
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
XXXVII-Jeřmanice	XXXVII-Jeřmanice	k1gFnSc1	XXXVII-Jeřmanice
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
XXXVIII-Minkovice	XXXVIII-Minkovice	k1gFnSc2	XXXVIII-Minkovice
a	a	k8xC	a
Liberec	Liberec	k1gInSc1	Liberec
XXXIX-Šimonovice	XXXIX-Šimonovice	k1gFnSc2	XXXIX-Šimonovice
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
Vratislavice	Vratislavice	k1gFnSc2	Vratislavice
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
XXX	XXX	kA	XXX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
průmyslem	průmysl	k1gInSc7	průmysl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
-	-	kIx~	-
zejména	zejména	k9	zejména
soukenictví	soukenictví	k1gNnSc2	soukenictví
a	a	k8xC	a
plátenictví	plátenictví	k1gNnSc2	plátenictví
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
soukeničtí	soukenický	k2eAgMnPc1d1	soukenický
mistři	mistr	k1gMnPc1	mistr
Urban	Urban	k1gMnSc1	Urban
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
ze	z	k7c2	z
Zawidówa	Zawidówum	k1gNnSc2	Zawidówum
a	a	k8xC	a
Christoph	Christopha	k1gFnPc2	Christopha
Krause	Kraus	k1gMnSc2	Kraus
vyučený	vyučený	k2eAgInSc4d1	vyučený
ve	v	k7c6	v
Frýdlantě	Frýdlant	k1gInSc6	Frýdlant
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
vlnu	vlna	k1gFnSc4	vlna
z	z	k7c2	z
ovčínů	ovčín	k1gInPc2	ovčín
na	na	k7c6	na
redernských	redernský	k2eAgNnPc6d1	redernský
panstvích	panství	k1gNnPc6	panství
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
dováželi	dovážet	k5eAaImAgMnP	dovážet
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
panství	panství	k1gNnPc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1579	[number]	k4	1579
<g/>
-	-	kIx~	-
<g/>
1580	[number]	k4	1580
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
společenstvo	společenstvo	k1gNnSc1	společenstvo
soukeníků	soukeník	k1gMnPc2	soukeník
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
barvírnu	barvírna	k1gFnSc4	barvírna
a	a	k8xC	a
vrchnost	vrchnost	k1gFnSc4	vrchnost
nechala	nechat	k5eAaPmAgFnS	nechat
postavit	postavit	k5eAaPmF	postavit
valchu	valcha	k1gFnSc4	valcha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
kvalita	kvalita	k1gFnSc1	kvalita
sukna	sukno	k1gNnSc2	sukno
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
obchodovat	obchodovat	k5eAaImF	obchodovat
na	na	k7c6	na
výročních	výroční	k2eAgInPc6d1	výroční
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
narůstajícímu	narůstající	k2eAgInSc3d1	narůstající
významu	význam	k1gInSc3	význam
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
suknem	sukno	k1gNnSc7	sukno
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1599	[number]	k4	1599
městu	město	k1gNnSc3	město
příslušné	příslušný	k2eAgNnSc4d1	příslušné
privilegium	privilegium	k1gNnSc4	privilegium
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
ustaven	ustavit	k5eAaPmNgInS	ustavit
liberecký	liberecký	k2eAgInSc1d1	liberecký
soukenický	soukenický	k2eAgInSc1d1	soukenický
cech	cech	k1gInSc1	cech
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
měl	mít	k5eAaImAgMnS	mít
10	[number]	k4	10
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
však	však	k9	však
rozrůstal	rozrůstat	k5eAaImAgInS	rozrůstat
-	-	kIx~	-
během	během	k7c2	během
dvaceti	dvacet	k4xCc3	dvacet
let	léto	k1gNnPc2	léto
přibylo	přibýt	k5eAaPmAgNnS	přibýt
dalších	další	k2eAgMnPc2d1	další
23	[number]	k4	23
mistrů	mistr	k1gMnPc2	mistr
soukenického	soukenický	k2eAgNnSc2d1	soukenické
řemesla	řemeslo	k1gNnSc2	řemeslo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejbohatšími	bohatý	k2eAgMnPc7d3	nejbohatší
a	a	k8xC	a
nejváženějšími	vážený	k2eAgMnPc7d3	nejváženější
občany	občan	k1gMnPc7	občan
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
rozvoj	rozvoj	k1gInSc1	rozvoj
soukenictví	soukenictví	k1gNnSc2	soukenictví
nastal	nastat	k5eAaPmAgInS	nastat
za	za	k7c4	za
Albrechta	Albrecht	k1gMnSc4	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
postavením	postavení	k1gNnSc7	postavení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
vévodových	vévodův	k2eAgFnPc2d1	vévodova
objednávek	objednávka	k1gFnPc2	objednávka
nových	nový	k2eAgFnPc2d1	nová
uniforem	uniforma	k1gFnPc2	uniforma
byl	být	k5eAaImAgInS	být
značný	značný	k2eAgInSc1d1	značný
<g/>
,	,	kIx,	,
platil	platit	k5eAaImAgInS	platit
však	však	k9	však
údajně	údajně	k6eAd1	údajně
málo	málo	k6eAd1	málo
a	a	k8xC	a
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
krátké	krátký	k2eAgInPc4d1	krátký
termíny	termín	k1gInPc4	termín
dodávek	dodávka	k1gFnPc2	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
lepším	dobrý	k2eAgInSc7d2	lepší
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
soukeníků	soukeník	k1gMnPc2	soukeník
objednávky	objednávka	k1gFnSc2	objednávka
sukna	sukno	k1gNnSc2	sukno
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
narůstajícímu	narůstající	k2eAgInSc3d1	narůstající
objemu	objem	k1gInSc3	objem
výroby	výroba	k1gFnSc2	výroba
přijal	přijmout	k5eAaPmAgInS	přijmout
cech	cech	k1gInSc1	cech
dalších	další	k2eAgMnPc2d1	další
75	[number]	k4	75
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
103	[number]	k4	103
tovaryšů	tovaryš	k1gMnPc2	tovaryš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1632	[number]	k4	1632
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
valcha	valcha	k1gFnSc1	valcha
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1633	[number]	k4	1633
také	také	k9	také
barvírna	barvírna	k1gFnSc1	barvírna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
výroba	výroba	k1gFnSc1	výroba
prudce	prudko	k6eAd1	prudko
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
kvůli	kvůli	k7c3	kvůli
početné	početný	k2eAgFnSc3d1	početná
emigraci	emigrace	k1gFnSc3	emigrace
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
protireformace	protireformace	k1gFnSc2	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
velké	velký	k2eAgFnPc1d1	velká
dávky	dávka	k1gFnPc1	dávka
odváděné	odváděný	k2eAgFnPc1d1	odváděná
Gallasům	Gallas	k1gMnPc3	Gallas
se	se	k3xPyFc4	se
Liberec	Liberec	k1gInSc1	Liberec
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
největší	veliký	k2eAgFnSc7d3	veliký
výrobnou	výrobna	k1gFnSc7	výrobna
sukna	sukno	k1gNnSc2	sukno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1728	[number]	k4	1728
měl	mít	k5eAaImAgInS	mít
soukenický	soukenický	k2eAgInSc1d1	soukenický
cech	cech	k1gInSc1	cech
419	[number]	k4	419
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
zaostával	zaostávat	k5eAaImAgInS	zaostávat
i	i	k9	i
cech	cech	k1gInSc1	cech
plátenický	plátenický	k2eAgInSc1d1	plátenický
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
pracovali	pracovat	k5eAaImAgMnP	pracovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
příměstských	příměstský	k2eAgFnPc6d1	příměstská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
samotném	samotný	k2eAgNnSc6d1	samotné
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
mistrů	mistr	k1gMnPc2	mistr
-	-	kIx~	-
asi	asi	k9	asi
desetina	desetina	k1gFnSc1	desetina
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
tkalců	tkadlec	k1gMnPc2	tkadlec
lnu	lnout	k5eAaImIp1nS	lnout
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Linci	Linec	k1gInSc6	Linec
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
manufaktura	manufaktura	k1gFnSc1	manufaktura
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
jemného	jemný	k2eAgNnSc2d1	jemné
sukna	sukno	k1gNnSc2	sukno
již	již	k9	již
roku	rok	k1gInSc2	rok
1672	[number]	k4	1672
<g/>
,	,	kIx,	,
liberecké	liberecký	k2eAgInPc1d1	liberecký
cechy	cech	k1gInPc1	cech
se	s	k7c7	s
"	"	kIx"	"
<g/>
novotám	novota	k1gFnPc3	novota
<g/>
"	"	kIx"	"
usilovně	usilovně	k6eAd1	usilovně
bránily	bránit	k5eAaImAgFnP	bránit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
požadovaly	požadovat	k5eAaImAgFnP	požadovat
zničení	zničení	k1gNnSc4	zničení
manufaktury	manufaktura	k1gFnSc2	manufaktura
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1710	[number]	k4	1710
v	v	k7c6	v
Plánici	plánice	k1gFnSc6	plánice
u	u	k7c2	u
Klatov	Klatovy	k1gInPc2	Klatovy
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
cechovního	cechovní	k2eAgNnSc2d1	cechovní
sukna	sukno	k1gNnSc2	sukno
se	se	k3xPyFc4	se
však	však	k9	však
postupně	postupně	k6eAd1	postupně
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
jako	jako	k9	jako
horší	zlý	k2eAgFnSc1d2	horší
než	než	k8xS	než
kvalita	kvalita	k1gFnSc1	kvalita
sukna	sukno	k1gNnSc2	sukno
z	z	k7c2	z
manufaktur	manufaktura	k1gFnPc2	manufaktura
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
modernizovat	modernizovat	k5eAaBmF	modernizovat
výrobu	výroba	k1gFnSc4	výroba
sukna	sukno	k1gNnSc2	sukno
v	v	k7c4	v
mocnářství	mocnářství	k1gNnSc4	mocnářství
postupným	postupný	k2eAgNnSc7d1	postupné
omezováním	omezování	k1gNnSc7	omezování
cechovních	cechovní	k2eAgNnPc2d1	cechovní
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
vyjímáním	vyjímání	k1gNnSc7	vyjímání
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
výrobců	výrobce	k1gMnPc2	výrobce
z	z	k7c2	z
cechovní	cechovní	k2eAgFnSc2d1	cechovní
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řad	řada	k1gFnPc2	řada
cechovních	cechovní	k2eAgMnPc2d1	cechovní
mistrů	mistr	k1gMnPc2	mistr
vyšli	vyjít	k5eAaPmAgMnP	vyjít
také	také	k9	také
první	první	k4xOgMnPc1	první
obchodníci	obchodník	k1gMnPc1	obchodník
se	se	k3xPyFc4	se
sukny	sukno	k1gNnPc7	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
byl	být	k5eAaImAgMnS	být
Gottfried	Gottfried	k1gInSc4	Gottfried
Möller	Möller	k1gMnSc1	Möller
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
závislí	závislý	k2eAgMnPc1d1	závislý
podomní	podomní	k2eAgMnPc1d1	podomní
dělníci	dělník	k1gMnPc1	dělník
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
první	první	k4xOgFnSc4	první
rozptýlenou	rozptýlený	k2eAgFnSc4d1	rozptýlená
manufakturu	manufaktura	k1gFnSc4	manufaktura
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
takovým	takový	k3xDgMnSc7	takový
výrobcem	výrobce	k1gMnSc7	výrobce
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Berger	Berger	k1gMnSc1	Berger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
textilních	textilní	k2eAgMnPc2d1	textilní
výrobců	výrobce	k1gMnPc2	výrobce
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Berger	Berger	k1gMnSc1	Berger
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
prvním	první	k4xOgInPc3	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
nové	nový	k2eAgNnSc4d1	nové
strojní	strojní	k2eAgNnSc4d1	strojní
spřádání	spřádání	k1gNnSc4	spřádání
vlny	vlna	k1gFnSc2	vlna
ve	v	k7c6	v
Stráži	stráž	k1gFnSc6	stráž
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
působily	působit	k5eAaImAgFnP	působit
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
manufaktury	manufaktura	k1gFnPc4	manufaktura
a	a	k8xC	a
osm	osm	k4xCc1	osm
velkoobchodníků	velkoobchodník	k1gMnPc2	velkoobchodník
se	se	k3xPyFc4	se
suknem	sukno	k1gNnSc7	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mechanizaci	mechanizace	k1gFnSc3	mechanizace
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
spřádací	spřádací	k2eAgInPc4d1	spřádací
stroje	stroj	k1gInPc4	stroj
provozovala	provozovat	k5eAaImAgFnS	provozovat
firma	firma	k1gFnSc1	firma
Anton	Anton	k1gMnSc1	Anton
Thum	Thum	k1gMnSc1	Thum
v	v	k7c6	v
Kateřinkách	Kateřinka	k1gFnPc6	Kateřinka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
200	[number]	k4	200
mechanických	mechanický	k2eAgNnPc2d1	mechanické
vřeten	vřeteno	k1gNnPc2	vřeteno
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výrobních	výrobní	k2eAgInPc2d1	výrobní
mechanismů	mechanismus	k1gInPc2	mechanismus
byla	být	k5eAaImAgFnS	být
poháněna	poháněn	k2eAgFnSc1d1	poháněna
vodními	vodní	k2eAgNnPc7d1	vodní
koly	kolo	k1gNnPc7	kolo
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
instalováno	instalován	k2eAgNnSc4d1	instalováno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
první	první	k4xOgMnPc1	první
podnikatelé	podnikatel	k1gMnPc1	podnikatel
-	-	kIx~	-
Möller	Möller	k1gMnSc1	Möller
<g/>
,	,	kIx,	,
Berger	Berger	k1gMnSc1	Berger
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
Ulbrich	Ulbrich	k1gMnSc1	Ulbrich
-	-	kIx~	-
sice	sice	k8xC	sice
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
koncentrované	koncentrovaný	k2eAgFnPc4d1	koncentrovaná
manufaktury	manufaktura	k1gFnPc4	manufaktura
<g/>
,	,	kIx,	,
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
je	on	k3xPp3gFnPc4	on
však	však	k8xC	však
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
továrny	továrna	k1gFnPc4	továrna
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
jejich	jejich	k3xOp3gFnPc4	jejich
společnosti	společnost	k1gFnPc4	společnost
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
silnějších	silný	k2eAgInPc2d2	silnější
obchodních	obchodní	k2eAgInPc2d1	obchodní
domů	dům	k1gInPc2	dům
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Textilní	textilní	k2eAgNnSc4d1	textilní
tovární	tovární	k2eAgNnSc4d1	tovární
impérium	impérium	k1gNnSc4	impérium
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
teprve	teprve	k6eAd1	teprve
Johann	Johann	k1gMnSc1	Johann
Liebieg	Liebieg	k1gMnSc1	Liebieg
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
podnik	podnik	k1gInSc1	podnik
Johann	Johann	k1gNnSc1	Johann
Liebieg	Liebieg	k1gInSc1	Liebieg
&	&	k?	&
Comp	Comp	k1gInSc1	Comp
<g/>
.	.	kIx.	.
zaměstnával	zaměstnávat	k5eAaImAgInS	zaměstnávat
až	až	k9	až
8000	[number]	k4	8000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
roční	roční	k2eAgInSc4d1	roční
obrat	obrat	k1gInSc4	obrat
2	[number]	k4	2
490	[number]	k4	490
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
cechy	cech	k1gInPc1	cech
již	již	k6eAd1	již
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
tzv.	tzv.	kA	tzv.
nucenými	nucený	k2eAgNnPc7d1	nucené
společenstvy	společenstvo	k1gNnPc7	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
vlny	vlna	k1gFnSc2	vlna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
otevření	otevření	k1gNnSc2	otevření
trhu	trh	k1gInSc2	trh
anglickému	anglický	k2eAgInSc3d1	anglický
textilu	textil	k1gInSc3	textil
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
po	po	k7c6	po
krachu	krach	k1gInSc6	krach
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
burze	burza	k1gFnSc6	burza
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
většina	většina	k1gFnSc1	většina
menších	malý	k2eAgInPc2d2	menší
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc4d1	velká
firmy	firma	k1gFnPc4	firma
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
naopak	naopak	k6eAd1	naopak
vyšly	vyjít	k5eAaPmAgInP	vyjít
posíleny	posílit	k5eAaPmNgInP	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
německých	německý	k2eAgInPc2d1	německý
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
majitelé	majitel	k1gMnPc1	majitel
měli	mít	k5eAaImAgMnP	mít
svá	svůj	k3xOyFgNnPc4	svůj
odbytiště	odbytiště	k1gNnPc4	odbytiště
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
faktory	faktor	k1gInPc7	faktor
byla	být	k5eAaImAgFnS	být
inflace	inflace	k1gFnSc1	inflace
německé	německý	k2eAgFnSc2d1	německá
marky	marka	k1gFnSc2	marka
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
ke	k	k7c3	k
zdrojům	zdroj	k1gInPc3	zdroj
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
poloha	poloha	k1gFnSc1	poloha
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgInPc4d1	hlavní
dopravní	dopravní	k2eAgInPc4d1	dopravní
tahy	tah	k1gInPc4	tah
<g/>
.	.	kIx.	.
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1	liberecký
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
tak	tak	k9	tak
musel	muset	k5eAaImAgInS	muset
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
levnou	levný	k2eAgFnSc4d1	levná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
za	za	k7c7	za
zbytkem	zbytek	k1gInSc7	zbytek
světa	svět	k1gInSc2	svět
zaostávat	zaostávat	k5eAaImF	zaostávat
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
tak	tak	k6eAd1	tak
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
firmy	firma	k1gFnPc1	firma
byly	být	k5eAaImAgFnP	být
nuceny	nucen	k2eAgFnPc1d1	nucena
zredukovat	zredukovat	k5eAaPmF	zredukovat
počet	počet	k1gInSc4	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
podniků	podnik	k1gInPc2	podnik
s	s	k7c7	s
2021	[number]	k4	2021
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
skončilo	skončit	k5eAaPmAgNnS	skončit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostavil	dostavit	k5eAaPmAgInS	dostavit
nedostatek	nedostatek	k1gInSc1	nedostatek
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
podniků	podnik	k1gInPc2	podnik
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
válečnou	válečný	k2eAgFnSc4d1	válečná
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
německé	německý	k2eAgFnPc1d1	německá
textilní	textilní	k2eAgFnPc1d1	textilní
firmy	firma	k1gFnPc1	firma
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
konfiskovány	konfiskován	k2eAgInPc1d1	konfiskován
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
znárodněny	znárodnit	k5eAaPmNgFnP	znárodnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
týkalo	týkat	k5eAaImAgNnS	týkat
společností	společnost	k1gFnSc7	společnost
Johann	Johann	k1gMnSc1	Johann
Liebieg	Liebieg	k1gMnSc1	Liebieg
<g/>
,	,	kIx,	,
Herminghaus	Herminghaus	k1gMnSc1	Herminghaus
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hoffman	Hoffman	k1gMnSc1	Hoffman
a	a	k8xC	a
Hettwer	Hettwer	k1gMnSc1	Hettwer
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Neumann	Neumann	k1gMnSc1	Neumann
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
barvírny	barvírna	k1gFnPc4	barvírna
a	a	k8xC	a
spol	spol	k1gInSc4	spol
<g/>
.	.	kIx.	.
a	a	k8xC	a
Textilana	textilana	k1gFnSc1	textilana
a.	a.	k?	a.
s.	s.	k?	s.
Protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
podniky	podnik	k1gInPc1	podnik
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
rukou	ruka	k1gFnPc6	ruka
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
členěny	členit	k5eAaImNgFnP	členit
oborově	oborově	k6eAd1	oborově
<g/>
,	,	kIx,	,
připadly	připadnout	k5eAaPmAgInP	připadnout
některé	některý	k3yIgInPc1	některý
mimolibereckým	mimoliberecký	k2eAgInPc3d1	mimoliberecký
národním	národní	k2eAgInPc3d1	národní
podnikům	podnik	k1gInPc3	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stal	stát	k5eAaPmAgInS	stát
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
sídlem	sídlo	k1gNnSc7	sídlo
Českých	český	k2eAgInPc2d1	český
vlnařských	vlnařský	k2eAgInPc2d1	vlnařský
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
konfiskacích	konfiskace	k1gFnPc6	konfiskace
tvořilo	tvořit	k5eAaImAgNnS	tvořit
tento	tento	k3xDgInSc4	tento
podnik	podnik	k1gInSc4	podnik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přijal	přijmout	k5eAaPmAgInS	přijmout
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1948	[number]	k4	1948
název	název	k1gInSc1	název
Textilana	textilana	k1gFnSc1	textilana
<g/>
,	,	kIx,	,
39	[number]	k4	39
závodů	závod	k1gInPc2	závod
s	s	k7c7	s
9000	[number]	k4	9000
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
působí	působit	k5eAaImIp3nP	působit
firmy	firma	k1gFnPc1	firma
Interlana	Interlana	k1gFnSc1	Interlana
a	a	k8xC	a
Elmarco	Elmarco	k6eAd1	Elmarco
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
nanovlákna	nanovlákna	k1gFnSc1	nanovlákna
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Textilany	textilana	k1gFnSc2	textilana
postupně	postupně	k6eAd1	postupně
přecházeli	přecházet	k5eAaImAgMnP	přecházet
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
podniků	podnik	k1gInPc2	podnik
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
až	až	k9	až
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
areál	areál	k1gInSc1	areál
Textilany	textilana	k1gFnSc2	textilana
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
byl	být	k5eAaImAgMnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
odstřelen	odstřelen	k2eAgInSc1d1	odstřelen
komín	komín	k1gInSc1	komín
<g/>
.	.	kIx.	.
</s>
<s>
Odstřel	odstřel	k1gInSc1	odstřel
komínu	komín	k1gInSc2	komín
Textilany	textilana	k1gFnSc2	textilana
Automobilový	automobilový	k2eAgInSc1d1	automobilový
průmysl	průmysl	k1gInSc1	průmysl
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
továrna	továrna	k1gFnSc1	továrna
Reichenberger	Reichenberger	k1gInSc4	Reichenberger
Automobil	automobil	k1gInSc1	automobil
Fabrik	fabrika	k1gFnPc2	fabrika
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
také	také	k9	také
Denso	Densa	k1gFnSc5	Densa
<g/>
,	,	kIx,	,
Webasto	Webasta	k1gFnSc5	Webasta
či	či	k8xC	či
Magna	Magn	k1gMnSc2	Magn
exteriors	exteriors	k1gInSc1	exteriors
&	&	k?	&
<g/>
interiors	interiors	k1gInSc1	interiors
Czech	Czech	k1gInSc1	Czech
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
také	také	k9	také
výroba	výroba	k1gFnSc1	výroba
textilních	textilní	k2eAgInPc2d1	textilní
strojů	stroj	k1gInPc2	stroj
Elitex	Elitex	k1gInSc1	Elitex
<g/>
,	,	kIx,	,
v	v	k7c6	v
polygrafici	polygrafice	k1gFnSc6	polygrafice
Tiskárna	tiskárna	k1gFnSc1	tiskárna
RUCH	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
výroba	výroba	k1gFnSc1	výroba
cukrovinek	cukrovinka	k1gFnPc2	cukrovinka
Lipo	Lipo	k1gMnSc1	Lipo
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
stejnojmenných	stejnojmenný	k2eAgInPc2d1	stejnojmenný
bonbónů	bonbón	k1gInPc2	bonbón
<g/>
,	,	kIx,	,
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
proslula	proslout	k5eAaPmAgFnS	proslout
napojením	napojení	k1gNnSc7	napojení
na	na	k7c4	na
orgány	orgány	k1gFnPc4	orgány
města	město	k1gNnSc2	město
a	a	k8xC	a
ODS	ODS	kA	ODS
stavební	stavební	k2eAgFnSc4d1	stavební
společnost	společnost	k1gFnSc4	společnost
Syner	Synra	k1gFnPc2	Synra
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
veřejně	veřejně	k6eAd1	veřejně
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
svým	svůj	k3xOyFgNnSc7	svůj
propojením	propojení	k1gNnSc7	propojení
s	s	k7c7	s
politickým	politický	k2eAgNnSc7d1	politické
vedením	vedení	k1gNnSc7	vedení
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
a	a	k8xC	a
netransparentním	transparentní	k2eNgInSc7d1	netransparentní
způsobem	způsob	k1gInSc7	způsob
získávání	získávání	k1gNnSc2	získávání
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
firmička	firmička	k1gFnSc1	firmička
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
politickým	politický	k2eAgInPc3d1	politický
kontaktům	kontakt	k1gInPc3	kontakt
stala	stát	k5eAaPmAgFnS	stát
dominantní	dominantní	k2eAgFnSc7d1	dominantní
stavební	stavební	k2eAgFnSc7d1	stavební
firmou	firma	k1gFnSc7	firma
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dokázala	dokázat	k5eAaPmAgFnS	dokázat
"	"	kIx"	"
<g/>
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
"	"	kIx"	"
většinu	většina	k1gFnSc4	většina
velkých	velký	k2eAgFnPc2d1	velká
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
nezřídka	nezřídka	k6eAd1	nezřídka
značně	značně	k6eAd1	značně
předražené	předražený	k2eAgNnSc1d1	předražené
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Liberecká	liberecký	k2eAgFnSc1d1	liberecká
multifunkční	multifunkční	k2eAgFnSc1d1	multifunkční
hala	hala	k1gFnSc1	hala
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Bílí	bílý	k2eAgMnPc1d1	bílý
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
společnost	společnost	k1gFnSc1	společnost
Syner	Syner	k1gInSc4	Syner
vlastní	vlastní	k2eAgInSc1d1	vlastní
většinový	většinový	k2eAgInSc1d1	většinový
podíl	podíl	k1gInSc1	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
získal	získat	k5eAaPmAgInS	získat
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
otevřeny	otevřen	k2eAgFnPc4d1	otevřena
tratě	trať	k1gFnPc4	trať
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vychází	vycházet	k5eAaImIp3nS	vycházet
pět	pět	k4xCc1	pět
jednokolejných	jednokolejný	k2eAgFnPc2d1	jednokolejná
a	a	k8xC	a
neelektrizovaných	elektrizovaný	k2eNgFnPc2d1	neelektrizovaná
železničních	železniční	k2eAgFnPc2d1	železniční
trati	trať	k1gFnSc6	trať
začleněných	začleněný	k2eAgFnPc2d1	začleněná
do	do	k7c2	do
celostátní	celostátní	k2eAgFnSc2d1	celostátní
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
tratě	trať	k1gFnSc2	trať
do	do	k7c2	do
Tanvaldu	Tanvald	k1gInSc2	Tanvald
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
regionální	regionální	k2eAgFnSc1d1	regionální
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc4d1	přímé
rychlíkové	rychlíkový	k2eAgNnSc4d1	rychlíkové
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
Děčína	Děčín	k1gInSc2	Děčín
a	a	k8xC	a
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
nebo	nebo	k8xC	nebo
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
;	;	kIx,	;
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
špatné	špatný	k2eAgNnSc1d1	špatné
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
silničnímu	silniční	k2eAgNnSc3d1	silniční
<g/>
.	.	kIx.	.
</s>
<s>
Spěšné	spěšný	k2eAgInPc1d1	spěšný
vlaky	vlak	k1gInPc1	vlak
spojují	spojovat	k5eAaImIp3nP	spojovat
Liberec	Liberec	k1gInSc4	Liberec
se	s	k7c7	s
saskými	saský	k2eAgInPc7d1	saský
Drážďany	Drážďany	k1gInPc7	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Liberce	Liberec	k1gInSc2	Liberec
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
velkou	velká	k1gFnSc4	velká
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
prošlo	projít	k5eAaPmAgNnS	projít
před	před	k7c7	před
otevřením	otevření	k1gNnSc7	otevření
dráhy	dráha	k1gFnSc2	dráha
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
nástupišť	nástupiště	k1gNnPc2	nástupiště
přístupných	přístupný	k2eAgNnPc2d1	přístupné
dvěma	dva	k4xCgFnPc7	dva
podchody	podchod	k1gInPc4	podchod
s	s	k7c7	s
výtahy	výtah	k1gInPc7	výtah
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
audiovizuálním	audiovizuální	k2eAgInSc7d1	audiovizuální
informačním	informační	k2eAgInSc7d1	informační
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
jsou	být	k5eAaImIp3nP	být
nádraží	nádraží	k1gNnPc4	nádraží
Liberec-Horní	Liberec-Horní	k2eAgInSc4d1	Liberec-Horní
Růžodol	růžodol	k1gInSc4	růžodol
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
086	[number]	k4	086
nebo	nebo	k8xC	nebo
frekventovaná	frekventovaný	k2eAgFnSc1d1	frekventovaná
zastávka	zastávka	k1gFnSc1	zastávka
nejblíže	blízce	k6eAd3	blízce
centru	centrum	k1gNnSc3	centrum
Liberec-Rochlice	Liberec-Rochlice	k1gFnSc2	Liberec-Rochlice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
vychází	vycházet	k5eAaImIp3nS	vycházet
pět	pět	k4xCc1	pět
jednokolejných	jednokolejný	k2eAgFnPc2d1	jednokolejná
neelektrifikovaných	elektrifikovaný	k2eNgFnPc2d1	neelektrifikovaná
tratí	trať	k1gFnPc2	trať
<g/>
:	:	kIx,	:
030	[number]	k4	030
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Turnov	Turnov	k1gInSc1	Turnov
-	-	kIx~	-
Stará	starý	k2eAgFnSc1d1	stará
Paka	Paka	k1gFnSc1	Paka
-	-	kIx~	-
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
s	s	k7c7	s
rychlíkovým	rychlíkový	k2eAgInSc7d1	rychlíkový
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Jaroměře	Jaroměř	k1gFnSc2	Jaroměř
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
jako	jako	k8xC	jako
trať	trať	k1gFnSc1	trať
0	[number]	k4	0
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
036	[number]	k4	036
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
-	-	kIx~	-
Tanvald	Tanvald	k1gInSc1	Tanvald
-	-	kIx~	-
Harrachov	Harrachov	k1gInSc1	Harrachov
st.	st.	kA	st.
hr	hr	k6eAd1	hr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc1d1	regionální
dráha	dráha	k1gFnSc1	dráha
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
regionálním	regionální	k2eAgInSc7d1	regionální
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Harrachova	Harrachov	k1gInSc2	Harrachov
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
polské	polský	k2eAgFnSc2d1	polská
Szklarské	Szklarský	k2eAgFnSc2d1	Szklarský
Poręby	Poręba	k1gFnSc2	Poręba
<g/>
.	.	kIx.	.
037	[number]	k4	037
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
-	-	kIx~	-
Černousy	Černous	k1gInPc1	Černous
st.	st.	kA	st.
hr	hr	k2eAgInPc1d1	hr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
regionálním	regionální	k2eAgInSc7d1	regionální
významem	význam	k1gInSc7	význam
a	a	k8xC	a
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
nákladním	nákladní	k2eAgInSc7d1	nákladní
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Z	Z	kA	Z
Černous	Černous	k1gMnSc1	Černous
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
polského	polský	k2eAgInSc2d1	polský
Zawidówa	Zawidów	k1gInSc2	Zawidów
<g/>
.	.	kIx.	.
086	[number]	k4	086
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
s	s	k7c7	s
rychlíkovým	rychlíkový	k2eAgInSc7d1	rychlíkový
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
089	[number]	k4	089
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Hrádek	hrádek	k1gInSc1	hrádek
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
-	-	kIx~	-
Žitava	Žitava	k1gFnSc1	Žitava
(	(	kIx(	(
<g/>
Zittau	Zittaus	k1gInSc2	Zittaus
<g/>
)	)	kIx)	)
-	-	kIx~	-
Varnsdorf	Varnsdorf	k1gInSc1	Varnsdorf
-	-	kIx~	-
Rybniště	Rybniště	k1gNnSc1	Rybniště
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc1d1	celostátní
dráha	dráha	k1gFnSc1	dráha
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
regionálním	regionální	k2eAgInSc7d1	regionální
i	i	k8xC	i
přeshraničním	přeshraniční	k2eAgInSc7d1	přeshraniční
významem	význam	k1gInSc7	význam
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
442	[number]	k4	442
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
Turnově	Turnov	k1gInSc6	Turnov
navazuje	navazovat	k5eAaImIp3nS	navazovat
dálnice	dálnice	k1gFnSc1	dálnice
D10	D10	k1gFnSc2	D10
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
silnice	silnice	k1gFnPc1	silnice
míří	mířit	k5eAaImIp3nP	mířit
do	do	k7c2	do
Jablonce	Jablonec	k1gInSc2	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Harrachova	Harrachov	k1gInSc2	Harrachov
a	a	k8xC	a
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frýdlantu	Frýdlant	k1gInSc6	Frýdlant
<g/>
,	,	kIx,	,
Hrádku	Hrádok	k1gInSc6	Hrádok
n.	n.	k?	n.
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Boru	bor	k1gInSc2	bor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
sídlí	sídlet	k5eAaImIp3nS	sídlet
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
a.s.	a.s.	k?	a.s.
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
transformací	transformace	k1gFnSc7	transformace
někdejšího	někdejší	k2eAgInSc2d1	někdejší
závodu	závod	k1gInSc2	závod
406	[number]	k4	406
krajského	krajský	k2eAgInSc2d1	krajský
podniku	podnik	k1gInSc2	podnik
ČSAD	ČSAD	kA	ČSAD
n.	n.	k?	n.
p.	p.	k?	p.
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
patřily	patřit	k5eAaImAgFnP	patřit
i	i	k9	i
provozovny	provozovna	k1gFnPc4	provozovna
Český	český	k2eAgInSc1d1	český
Dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
<g/>
,	,	kIx,	,
Hrádek	hrádek	k1gInSc1	hrádek
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
transformován	transformován	k2eAgInSc1d1	transformován
ve	v	k7c4	v
státní	státní	k2eAgInSc4d1	státní
podnik	podnik	k1gInSc4	podnik
Československá	československý	k2eAgFnSc1d1	Československá
automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
poté	poté	k6eAd1	poté
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
v	v	k7c6	v
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
a.	a.	k?	a.
s.	s.	k?	s.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
podnik	podnik	k1gInSc1	podnik
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
z	z	k7c2	z
70	[number]	k4	70
%	%	kIx~	%
společnost	společnost	k1gFnSc1	společnost
Liberecká	liberecký	k2eAgFnSc1d1	liberecká
automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
LIAD	LIAD	kA	LIAD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastní	vlastní	k2eAgMnSc1d1	vlastní
Petr	Petr	k1gMnSc1	Petr
Wasserbauer	Wasserbauer	k1gMnSc1	Wasserbauer
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
30	[number]	k4	30
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
města	město	k1gNnPc1	město
a	a	k8xC	a
obce	obec	k1gFnPc1	obec
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
(	(	kIx(	(
<g/>
7,13	[number]	k4	7,13
%	%	kIx~	%
město	město	k1gNnSc1	město
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
a.	a.	k?	a.
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
dominantním	dominantní	k2eAgMnSc7d1	dominantní
autobusovým	autobusový	k2eAgMnSc7d1	autobusový
dopravcem	dopravce	k1gMnSc7	dopravce
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
autobusové	autobusový	k2eAgFnSc2d1	autobusová
provozovny	provozovna	k1gFnSc2	provozovna
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Českém	český	k2eAgInSc6d1	český
Dubu	dub	k1gInSc6	dub
a	a	k8xC	a
Frýdlantu	Frýdlant	k1gInSc6	Frýdlant
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
dálkovou	dálkový	k2eAgFnSc7d1	dálková
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
nákladní	nákladní	k2eAgFnSc7d1	nákladní
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
činnostmi	činnost	k1gFnPc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
schválila	schválit	k5eAaPmAgFnS	schválit
záměr	záměr	k1gInSc4	záměr
oddělit	oddělit	k5eAaPmF	oddělit
do	do	k7c2	do
samostatných	samostatný	k2eAgFnPc2d1	samostatná
společností	společnost	k1gFnPc2	společnost
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
logistiku	logistika	k1gFnSc4	logistika
(	(	kIx(	(
<g/>
do	do	k7c2	do
Severotrans	Severotransa	k1gFnPc2	Severotransa
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autoopravárenství	autoopravárenství	k1gNnSc1	autoopravárenství
(	(	kIx(	(
<g/>
Autocentrum-Nord	Autocentrum-Nord	k1gInSc1	Autocentrum-Nord
<g/>
)	)	kIx)	)
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	(
<g/>
FinReal	FinReal	k1gInSc1	FinReal
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
rovněž	rovněž	k9	rovněž
společnost	společnost	k1gFnSc1	společnost
prodala	prodat	k5eAaPmAgFnS	prodat
divizi	divize	k1gFnSc4	divize
autobusové	autobusový	k2eAgFnSc2d1	autobusová
MHD	MHD	kA	MHD
Dopravnímu	dopravní	k2eAgInSc3d1	dopravní
podniku	podnik	k1gInSc3	podnik
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
a.	a.	k?	a.
s.	s.	k?	s.
asi	asi	k9	asi
320	[number]	k4	320
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
měla	mít	k5eAaImAgFnS	mít
391	[number]	k4	391
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tržby	tržba	k1gFnPc1	tržba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
činily	činit	k5eAaImAgInP	činit
308	[number]	k4	308
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
jen	jen	k9	jen
260	[number]	k4	260
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
se	se	k3xPyFc4	se
na	na	k7c6	na
tržbách	tržba	k1gFnPc6	tržba
podílely	podílet	k5eAaImAgFnP	podílet
zhruba	zhruba	k6eAd1	zhruba
stejnou	stejný	k2eAgFnSc7d1	stejná
měrou	míra	k1gFnSc7wR	míra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
a	a	k8xC	a
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Liberecká	liberecký	k2eAgFnSc1d1	liberecká
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
autobusových	autobusový	k2eAgFnPc2d1	autobusová
a	a	k8xC	a
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
měst	město	k1gNnPc2	město
Liberce	Liberec	k1gInSc2	Liberec
a	a	k8xC	a
Jablonce	Jablonec	k1gInSc2	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrálním	centrální	k2eAgNnSc7d1	centrální
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
terminál	terminál	k1gInSc1	terminál
MHD	MHD	kA	MHD
ve	v	k7c6	v
Fügnerové	Fügnerové	k2eAgFnSc6d1	Fügnerové
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
provozoval	provozovat	k5eAaImAgInS	provozovat
DPMLJ	DPMLJ	kA	DPMLJ
celkem	celkem	k6eAd1	celkem
51	[number]	k4	51
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
školních	školní	k2eAgInPc2d1	školní
<g/>
,	,	kIx,	,
nočních	noční	k2eAgInPc2d1	noční
a	a	k8xC	a
linek	linka	k1gFnPc2	linka
500	[number]	k4	500
a	a	k8xC	a
600	[number]	k4	600
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
z	z	k7c2	z
terminálu	terminál	k1gInSc2	terminál
MHD	MHD	kA	MHD
Fügnerova	Fügnerův	k2eAgNnPc4d1	Fügnerovo
k	k	k7c3	k
obchodním	obchodní	k2eAgInPc3d1	obchodní
centrům	centr	k1gInPc3	centr
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Liberce	Liberec	k1gInSc2	Liberec
(	(	kIx(	(
<g/>
OC	OC	kA	OC
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
Globus	globus	k1gInSc1	globus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
začal	začít	k5eAaPmAgInS	začít
provozovat	provozovat	k5eAaImF	provozovat
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
od	od	k7c2	od
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
dnešní	dnešní	k2eAgFnSc3d1	dnešní
ZOO	zoo	k1gFnSc3	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
trati	trať	k1gFnPc4	trať
<g/>
:	:	kIx,	:
trasu	trasa	k1gFnSc4	trasa
mezi	mezi	k7c7	mezi
Horním	horní	k2eAgInSc7d1	horní
Hanychovem	Hanychov	k1gInSc7	Hanychov
a	a	k8xC	a
Lidovými	lidový	k2eAgInPc7d1	lidový
sady	sad	k1gInPc7	sad
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
městskou	městský	k2eAgFnSc4d1	městská
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
stavěnou	stavěný	k2eAgFnSc4d1	stavěná
původně	původně	k6eAd1	původně
s	s	k7c7	s
rozchodem	rozchod	k1gInSc7	rozchod
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zrekonstruovanou	zrekonstruovaný	k2eAgFnSc4d1	zrekonstruovaná
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
rozchod	rozchod	k1gInSc4	rozchod
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
a	a	k8xC	a
meziměstskou	meziměstský	k2eAgFnSc4d1	meziměstská
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
trať	trať	k1gFnSc4	trať
do	do	k7c2	do
Jablonce	Jablonec	k1gInSc2	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Rochlice	Rochlice	k1gFnSc2	Rochlice
a	a	k8xC	a
Růžodolu	růžodol	k1gInSc2	růžodol
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
plánuje	plánovat	k5eAaImIp3nS	plánovat
její	její	k3xOp3gNnPc4	její
znovuvybudování	znovuvybudování	k1gNnPc4	znovuvybudování
kvůli	kvůli	k7c3	kvůli
obsluze	obsluha	k1gFnSc3	obsluha
rochlického	rochlický	k2eAgNnSc2d1	rochlický
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
budovala	budovat	k5eAaImAgFnS	budovat
nová	nový	k2eAgFnSc1d1	nová
trať	trať	k1gFnSc1	trať
běžného	běžný	k2eAgInSc2d1	běžný
rozchodu	rozchod	k1gInSc2	rozchod
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
ulice	ulice	k1gFnSc1	ulice
Jablonecká	jablonecký	k2eAgFnSc1d1	Jablonecká
<g/>
.	.	kIx.	.
</s>
<s>
Meziměstská	meziměstský	k2eAgFnSc1d1	meziměstská
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
bude	být	k5eAaImBp3nS	být
nejspíše	nejspíše	k9	nejspíše
přerozchodována	přerozchodovat	k5eAaBmNgFnS	přerozchodovat
jen	jen	k9	jen
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
po	po	k7c4	po
Vratislavice	Vratislavice	k1gFnPc4	Vratislavice
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
bude	být	k5eAaImBp3nS	být
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
souběžné	souběžný	k2eAgFnSc6d1	souběžná
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
hybridních	hybridní	k2eAgNnPc6d1	hybridní
vozidlech	vozidlo	k1gNnPc6	vozidlo
lehké	lehký	k2eAgFnSc2d1	lehká
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Regiotram	Regiotram	k1gInSc1	Regiotram
Nisa	Nisa	k1gFnSc1	Nisa
spojila	spojit	k5eAaPmAgNnP	spojit
i	i	k9	i
další	další	k2eAgNnPc1d1	další
města	město	k1gNnPc1	město
Euroregionu	euroregion	k1gInSc2	euroregion
Nisa	Nisa	k1gFnSc1	Nisa
jako	jako	k8xC	jako
např.	např.	kA	např.
Žitava	Žitava	k1gFnSc1	Žitava
<g/>
,	,	kIx,	,
Jelenia	Jelenium	k1gNnPc1	Jelenium
Góra	Góra	k1gFnSc1	Góra
<g/>
,	,	kIx,	,
Tanvald	Tanvald	k1gInSc1	Tanvald
<g/>
,	,	kIx,	,
Harrachov	Harrachov	k1gInSc1	Harrachov
či	či	k8xC	či
Železný	železný	k2eAgInSc1d1	železný
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
zatím	zatím	k6eAd1	zatím
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
finančně	finančně	k6eAd1	finančně
podpořen	podpořit	k5eAaPmNgInS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdný	k2eAgNnSc1d1	jízdné
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
nejdražší	drahý	k2eAgFnSc7d3	nejdražší
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
však	však	k9	však
zlevnilo	zlevnit	k5eAaPmAgNnS	zlevnit
<g/>
:	:	kIx,	:
dospělá	dospělý	k2eAgFnSc1d1	dospělá
dvacetiminutová	dvacetiminutový	k2eAgFnSc1d1	dvacetiminutová
jízdenka	jízdenka	k1gFnSc1	jízdenka
stála	stát	k5eAaImAgFnS	stát
10	[number]	k4	10
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
pětačtyřicetiminutová	pětačtyřicetiminutový	k2eAgFnSc1d1	pětačtyřicetiminutová
16	[number]	k4	16
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
platit	platit	k5eAaImF	platit
nové	nový	k2eAgFnPc4d1	nová
ceny	cena	k1gFnPc4	cena
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
,	,	kIx,	,
dospělá	dospělý	k2eAgFnSc1d1	dospělá
čtyřicetiminutová	čtyřicetiminutový	k2eAgFnSc1d1	čtyřicetiminutová
(	(	kIx(	(
<g/>
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
platí	platit	k5eAaImIp3nS	platit
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
za	za	k7c4	za
16	[number]	k4	16
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
dětská	dětský	k2eAgFnSc1d1	dětská
polovic	polovice	k1gFnPc2	polovice
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
se	se	k3xPyFc4	se
zvedla	zvednout	k5eAaPmAgFnS	zvednout
cena	cena	k1gFnSc1	cena
opět	opět	k6eAd1	opět
<g/>
:	:	kIx,	:
čtyřicetiminutová	čtyřicetiminutový	k2eAgFnSc1d1	čtyřicetiminutová
jízdenka	jízdenka	k1gFnSc1	jízdenka
stála	stát	k5eAaImAgFnS	stát
18	[number]	k4	18
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
celodenní	celodenní	k2eAgFnSc1d1	celodenní
70	[number]	k4	70
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
zdražení	zdražení	k1gNnSc1	zdražení
přišlo	přijít	k5eAaPmAgNnS	přijít
už	už	k6eAd1	už
za	za	k7c4	za
rok	rok	k1gInSc4	rok
-	-	kIx~	-
čtyřicetiminutová	čtyřicetiminutový	k2eAgFnSc1d1	čtyřicetiminutová
jízdenka	jízdenka	k1gFnSc1	jízdenka
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
stojí	stát	k5eAaImIp3nS	stát
20	[number]	k4	20
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
polovic	polovice	k1gFnPc2	polovice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
používány	používat	k5eAaImNgFnP	používat
jízdenky	jízdenka	k1gFnPc1	jízdenka
širokého	široký	k2eAgInSc2d1	široký
formátu	formát	k1gInSc2	formát
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
používá	používat	k5eAaImIp3nS	používat
SMS	SMS	kA	SMS
jízdenka	jízdenka	k1gFnSc1	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
kabinovou	kabinový	k2eAgFnSc4d1	kabinová
lanovou	lanový	k2eAgFnSc4d1	lanová
dráhu	dráha	k1gFnSc4	dráha
na	na	k7c4	na
Ještěd	Ještěd	k1gInSc4	Ještěd
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
1188	[number]	k4	1188
m	m	kA	m
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
402	[number]	k4	402
m	m	kA	m
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
dolní	dolní	k2eAgFnSc1d1	dolní
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
libereckém	liberecký	k2eAgNnSc6d1	liberecké
Horním	horní	k2eAgNnSc6d1	horní
Hanychově	Hanychův	k2eAgNnSc6d1	Hanychův
nedaleko	nedaleko	k7c2	nedaleko
konečné	konečný	k2eAgFnSc2d1	konečná
stanice	stanice	k1gFnSc2	stanice
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
čtyři	čtyři	k4xCgFnPc1	čtyři
lanovky	lanovka	k1gFnPc1	lanovka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Liberce	Liberec	k1gInSc2	Liberec
provozují	provozovat	k5eAaImIp3nP	provozovat
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
Skalka	skalka	k1gFnSc1	skalka
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
i	i	k8xC	i
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc4d1	ostatní
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
lyžařům	lyžař	k1gMnPc3	lyžař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostašově	Ostašův	k2eAgFnSc6d1	Ostašova
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Liberce	Liberec	k1gInSc2	Liberec
je	být	k5eAaImIp3nS	být
nevelké	velký	k2eNgNnSc4d1	nevelké
letiště	letiště	k1gNnSc4	letiště
s	s	k7c7	s
travnatou	travnatý	k2eAgFnSc7d1	travnatá
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
používá	používat	k5eAaImIp3nS	používat
místní	místní	k2eAgInSc1d1	místní
Aeroklub	aeroklub	k1gInSc1	aeroklub
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
Letecká	letecký	k2eAgFnSc1d1	letecká
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
Divadlo	divadlo	k1gNnSc1	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
je	být	k5eAaImIp3nS	být
třísouborové	třísouborový	k2eAgNnSc1d1	třísouborový
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Malé	Malé	k2eAgNnSc4d1	Malé
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
Krátké	Krátké	k2eAgNnSc4d1	Krátké
a	a	k8xC	a
úderné	úderný	k2eAgNnSc4d1	úderné
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
či	či	k8xC	či
loutkové	loutkový	k2eAgNnSc4d1	loutkové
Naivní	naivní	k2eAgNnSc4d1	naivní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
festival	festival	k1gInSc1	festival
loutkového	loutkový	k2eAgNnSc2d1	loutkové
divadla	divadlo	k1gNnSc2	divadlo
Mateřinka	mateřinka	k1gFnSc1	mateřinka
<g/>
,	,	kIx,	,
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
předškolního	předškolní	k2eAgInSc2d1	předškolní
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
fungovala	fungovat	k5eAaImAgFnS	fungovat
např.	např.	kA	např.
kina	kino	k1gNnSc2	kino
Varšava	Varšava	k1gFnSc1	Varšava
a	a	k8xC	a
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
je	být	k5eAaImIp3nS	být
však	však	k9	však
multikino	multikino	k1gNnSc1	multikino
Cinestar	Cinestara	k1gFnPc2	Cinestara
v	v	k7c6	v
OC	OC	kA	OC
NISA	Nisa	k1gFnSc1	Nisa
a	a	k8xC	a
multikino	multikino	k1gNnSc1	multikino
Cinema	Cinemum	k1gNnSc2	Cinemum
City	City	k1gFnSc2	City
v	v	k7c6	v
OC	OC	kA	OC
FORUM	forum	k1gNnSc1	forum
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
Krajská	krajský	k2eAgFnSc1d1	krajská
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Severočeské	severočeský	k2eAgNnSc1d1	Severočeské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnPc1d1	oblastní
galerie	galerie	k1gFnPc1	galerie
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
a	a	k8xC	a
IQLANDIA	IQLANDIA	kA	IQLANDIA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zábavní	zábavní	k2eAgFnPc1d1	zábavní
instituce	instituce	k1gFnPc1	instituce
jsou	být	k5eAaImIp3nP	být
Dům	dům	k1gInSc4	dům
kultury	kultura	k1gFnSc2	kultura
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc1d1	lidová
sady	sada	k1gFnPc1	sada
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgFnPc1d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
centrum	centrum	k1gNnSc1	centrum
postavené	postavený	k2eAgNnSc1d1	postavené
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
na	na	k7c6	na
konečné	konečná	k1gFnSc6	konečná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
Babylon	Babylon	k1gInSc1	Babylon
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
aquapark	aquapark	k1gInSc1	aquapark
a	a	k8xC	a
zábavní	zábavní	k2eAgInSc1d1	zábavní
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc1d1	společenský
a	a	k8xC	a
hotelový	hotelový	k2eAgInSc1d1	hotelový
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
Nisa	Nisa	k1gFnSc1	Nisa
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
obchodní	obchodní	k2eAgNnSc4d1	obchodní
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc4d1	společenské
centrum	centrum	k1gNnSc4	centrum
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
Forum	forum	k1gNnSc1	forum
Liberec	Liberec	k1gInSc1	Liberec
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
26	[number]	k4	26
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedna	jeden	k4xCgFnSc1	jeden
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
umělecká	umělecký	k2eAgFnSc1d1	umělecká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
6	[number]	k4	6
středních	střední	k2eAgNnPc2d1	střední
odborných	odborný	k2eAgNnPc2d1	odborné
učilišť	učiliště	k1gNnPc2	učiliště
a	a	k8xC	a
15	[number]	k4	15
středních	střední	k2eAgFnPc2d1	střední
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
škol	škola	k1gFnPc2	škola
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc1d1	stavební
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
strojní	strojní	k2eAgFnSc1d1	strojní
a	a	k8xC	a
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Liberec	Liberec	k1gInSc1	Liberec
nebo	nebo	k8xC	nebo
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
textilní	textilní	k2eAgFnSc1d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
vyšší	vysoký	k2eAgInPc1d2	vyšší
odborné	odborný	k2eAgInPc1d1	odborný
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
čtyři	čtyři	k4xCgNnPc4	čtyři
gymnázia	gymnázium	k1gNnPc4	gymnázium
<g/>
:	:	kIx,	:
Gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Gymnázium	gymnázium	k1gNnSc1	gymnázium
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
Podještědské	Podještědský	k2eAgNnSc1d1	Podještědské
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
soukromé	soukromý	k2eAgFnSc2d1	soukromá
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Na	na	k7c6	na
Bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
zejména	zejména	k9	zejména
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
pak	pak	k6eAd1	pak
pobočka	pobočka	k1gFnSc1	pobočka
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
pobočka	pobočka	k1gFnSc1	pobočka
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
univerzity	univerzita	k1gFnSc2	univerzita
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
provádí	provádět	k5eAaImIp3nS	provádět
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
textilních	textilní	k2eAgInPc2d1	textilní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Liberce	Liberec	k1gInSc2	Liberec
i	i	k8xC	i
město	město	k1gNnSc1	město
samo	sám	k3xTgNnSc1	sám
skýtá	skýtat	k5eAaImIp3nS	skýtat
množství	množství	k1gNnSc1	množství
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
nejrůznějším	různý	k2eAgInPc3d3	nejrůznější
sportům	sport	k1gInPc3	sport
provozovaným	provozovaný	k2eAgInPc3d1	provozovaný
jak	jak	k6eAd1	jak
na	na	k7c6	na
amatérské	amatérský	k2eAgFnSc6d1	amatérská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rekreačnímu	rekreační	k2eAgNnSc3d1	rekreační
sportování	sportování	k1gNnSc3	sportování
využívají	využívat	k5eAaPmIp3nP	využívat
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
okolních	okolní	k2eAgFnPc2d1	okolní
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
ideální	ideální	k2eAgFnPc4d1	ideální
podmínky	podmínka	k1gFnPc4	podmínka
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
<g/>
,	,	kIx,	,
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
nebo	nebo	k8xC	nebo
běh	běh	k1gInSc4	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
sjezdové	sjezdový	k2eAgNnSc4d1	sjezdové
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
,	,	kIx,	,
běh	běh	k1gInSc4	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
skoky	skok	k1gInPc7	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
běžecký	běžecký	k2eAgInSc1d1	běžecký
areál	areál	k1gInSc1	areál
ve	v	k7c6	v
Vesci	Vesce	k1gFnSc6	Vesce
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sportování	sportování	k1gNnSc3	sportování
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
slouží	sloužit	k5eAaImIp3nS	sloužit
moderní	moderní	k2eAgFnSc1d1	moderní
multifunkční	multifunkční	k2eAgFnSc1d1	multifunkční
Home	Home	k1gFnSc1	Home
Credit	Credit	k1gFnSc1	Credit
Arena	Arena	k1gFnSc1	Arena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
Bílí	bílit	k5eAaImIp3nP	bílit
Tygři	tygr	k1gMnPc1	tygr
Liberec	Liberec	k1gInSc4	Liberec
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
Stadion	stadion	k1gInSc4	stadion
U	u	k7c2	u
Nisy	Nisa	k1gFnSc2	Nisa
<g/>
,	,	kIx,	,
domovský	domovský	k2eAgInSc4d1	domovský
stadion	stadion	k1gInSc4	stadion
Slovanu	Slovan	k1gInSc2	Slovan
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plavecký	plavecký	k2eAgInSc1d1	plavecký
bazén	bazén	k1gInSc1	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Liberečtí	liberecký	k2eAgMnPc1d1	liberecký
sportovci	sportovec	k1gMnPc1	sportovec
mají	mít	k5eAaImIp3nP	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
republikových	republikový	k2eAgFnPc6d1	republiková
i	i	k8xC	i
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějšími	úspěšný	k2eAgInPc7d3	nejúspěšnější
sporty	sport	k1gInPc7	sport
jsou	být	k5eAaImIp3nP	být
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc1	florbal
<g/>
,	,	kIx,	,
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
orientační	orientační	k2eAgInSc1d1	orientační
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc1	karate
<g/>
,	,	kIx,	,
lyžování	lyžování	k1gNnSc1	lyžování
a	a	k8xC	a
judo	judo	k1gNnSc1	judo
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovan	Slovan	k1gInSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Bílí	bílit	k5eAaImIp3nP	bílit
Tygři	tygr	k1gMnPc1	tygr
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Patriots	Patriotsa	k1gFnPc2	Patriotsa
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
baseballový	baseballový	k2eAgInSc1d1	baseballový
klub	klub	k1gInSc1	klub
Vem	Vem	k?	Vem
Camará	Camarý	k2eAgFnSc1d1	Camará
Capoeira	Capoeira	k1gFnSc1	Capoeira
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
klub	klub	k1gInSc1	klub
capoeiry	capoeira	k1gMnSc2	capoeira
VTJ	VTJ	kA	VTJ
Rapid	rapid	k1gInSc1	rapid
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
tým	tým	k1gInSc1	tým
FC	FC	kA	FC
Slovanu	Slovan	k1gInSc2	Slovan
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
AC	AC	kA	AC
Slovan	Slovan	k1gInSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
atletický	atletický	k2eAgInSc1d1	atletický
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Harcov	Harcov	k1gInSc1	Harcov
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
oddíl	oddíl	k1gInSc1	oddíl
Všestrannosti	všestrannost	k1gFnSc2	všestrannost
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
<g/>
,	,	kIx,	,
florbal	florbal	k1gInSc1	florbal
<g/>
,	,	kIx,	,
vybíjená	vybíjená	k1gFnSc1	vybíjená
<g/>
.	.	kIx.	.
</s>
<s>
BK	BK	kA	BK
Kondoři	kondor	k1gMnPc1	kondor
Liberec	Liberec	k1gInSc4	Liberec
-	-	kIx~	-
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
klub	klub	k1gInSc4	klub
Dukla	Dukla	k1gFnSc1	Dukla
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
armádní	armádní	k2eAgNnSc1d1	armádní
sportovní	sportovní	k2eAgNnSc1d1	sportovní
středisko	středisko	k1gNnSc1	středisko
lyžování	lyžování	k1gNnSc2	lyžování
VK	VK	kA	VK
Dukla	Dukla	k1gFnSc1	Dukla
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
volejbalový	volejbalový	k2eAgInSc1d1	volejbalový
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Liberec	Liberec	k1gInSc1	Liberec
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc1	Sokol
Františkov	Františkov	k1gInSc1	Františkov
-	-	kIx~	-
tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
Liberec	Liberec	k1gInSc4	Liberec
3	[number]	k4	3
<g/>
-Františkov	-Františkov	k1gInSc1	-Františkov
FBC	FBC	kA	FBC
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
florbalový	florbalový	k2eAgInSc1d1	florbalový
klub	klub	k1gInSc1	klub
Liberec	Liberec	k1gInSc1	Liberec
Handball	Handball	k1gInSc1	Handball
-	-	kIx~	-
házenkářský	házenkářský	k2eAgInSc1d1	házenkářský
klub	klub	k1gInSc1	klub
SKST	SKST	kA	SKST
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
klub	klub	k1gInSc1	klub
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
Liberec	Liberec	k1gInSc1	Liberec
<g />
.	.	kIx.	.
</s>
<s>
Titans	Titans	k1gInSc1	Titans
-	-	kIx~	-
klub	klub	k1gInSc1	klub
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Sport	sport	k1gInSc1	sport
Aerobic	aerobic	k1gInSc1	aerobic
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
klub	klub	k1gInSc1	klub
gymnastického	gymnastický	k2eAgInSc2d1	gymnastický
aerobiku	aerobic	k1gInSc2	aerobic
Andy	Anda	k1gFnSc2	Anda
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
prvoligový	prvoligový	k2eAgInSc1d1	prvoligový
klub	klub	k1gInSc1	klub
futsalu	futsal	k1gInSc2	futsal
TJ	tj	kA	tj
Slovan	Slovan	k1gMnSc1	Slovan
Vesec	Vesec	k1gMnSc1	Vesec
-	-	kIx~	-
badmintonový	badmintonový	k2eAgInSc1d1	badmintonový
prvoligový	prvoligový	k2eAgInSc1d1	prvoligový
klub	klub	k1gInSc1	klub
VSK	VSK	kA	VSK
Slavia	Slavia	k1gFnSc1	Slavia
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
badmintonový	badmintonový	k2eAgInSc1d1	badmintonový
extraligový	extraligový	k2eAgInSc1d1	extraligový
klub	klub	k1gInSc1	klub
Judoclub	Judocluba	k1gFnPc2	Judocluba
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
judistický	judistický	k2eAgInSc1d1	judistický
klub	klub	k1gInSc1	klub
LTK	LTK	kA	LTK
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
tenis	tenis	k1gInSc1	tenis
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Marcela	Marcela	k1gFnSc1	Marcela
Augustová	Augustová	k1gFnSc1	Augustová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
moderátorka	moderátorka	k1gFnSc1	moderátorka
Yemi	Yem	k1gFnSc2	Yem
A.D.	A.D.	k1gFnSc1	A.D.
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
Guido	Guido	k1gNnSc1	Guido
Beck	Beck	k1gMnSc1	Beck
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Arthur	Arthur	k1gMnSc1	Arthur
Beer	Beer	k1gMnSc1	Beer
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bradáč	Bradáč	k1gMnSc1	Bradáč
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iluzionista	iluzionista	k1gMnSc1	iluzionista
<g/>
,	,	kIx,	,
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
žonglér	žonglér	k1gMnSc1	žonglér
<g/>
,	,	kIx,	,
rekordman	rekordman	k1gMnSc1	rekordman
a	a	k8xC	a
eskapolog	eskapolog	k1gMnSc1	eskapolog
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
král	král	k1gMnSc1	král
komiků	komik	k1gMnPc2	komik
<g/>
"	"	kIx"	"
Jonáš	Jonáš	k1gMnSc1	Jonáš
Červinka	Červinka	k1gMnSc1	Červinka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
rapper	rapper	k1gMnSc1	rapper
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
Lipo	Lipo	k1gMnSc1	Lipo
(	(	kIx(	(
<g/>
rapper	rapper	k1gMnSc1	rapper
<g/>
))	))	k?	))
Martin	Martin	k1gMnSc1	Martin
Damm	Damm	k1gMnSc1	Damm
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Christoph	Christoph	k1gMnSc1	Christoph
Demantius	Demantius	k1gMnSc1	Demantius
(	(	kIx(	(
<g/>
1567	[number]	k4	1567
<g/>
-	-	kIx~	-
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Lukáš	Lukáš	k1gMnSc1	Lukáš
Derner	Derner	k1gMnSc1	Derner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Enge	Enge	k1gFnSc1	Enge
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Herbert	Herbert	k1gMnSc1	Herbert
Feigl	Feigl	k1gMnSc1	Feigl
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakousko-americký	rakouskomerický	k2eAgMnSc1d1	rakousko-americký
filosof	filosof	k1gMnSc1	filosof
Zita	Zita	k1gMnSc1	Zita
Frydrychová	Frydrychová	k1gFnSc1	Frydrychová
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trampolinistka	trampolinistka	k1gFnSc1	trampolinistka
<g/>
,	,	kIx,	,
juniorská	juniorský	k2eAgFnSc1d1	juniorská
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
olympionička	olympionička	k1gFnSc1	olympionička
Franz	Franz	k1gMnSc1	Franz
Clam-Gallas	Clam-Gallas	k1gMnSc1	Clam-Gallas
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mecenáš	mecenáš	k1gMnSc1	mecenáš
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Gustav	Gustav	k1gMnSc1	Gustav
Ginzel	Ginzel	k1gMnSc1	Ginzel
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
horolezec	horolezec	k1gMnSc1	horolezec
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hampl	Hampl	k1gMnSc1	Hampl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Pavel	Pavel	k1gMnSc1	Pavel
Harant	Harant	k?	Harant
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
rapper	rapper	k1gMnSc1	rapper
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
Paulie	Paulie	k1gFnSc2	Paulie
Garand	Garanda	k1gFnPc2	Garanda
<g/>
)	)	kIx)	)
Jaromír	Jaromír	k1gMnSc1	Jaromír
Havlík	Havlík	k1gMnSc1	Havlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
muzikolog	muzikolog	k1gMnSc1	muzikolog
a	a	k8xC	a
proděkan	proděkan	k1gMnSc1	proděkan
HAMU	HAMU	kA	HAMU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Zuzana	Zuzana	k1gFnSc1	Zuzana
Hejnová	Hejnová	k1gFnSc1	Hejnová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
Konrad	Konrada	k1gFnPc2	Konrada
Henlein	Henleina	k1gFnPc2	Henleina
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sudetoněmecký	sudetoněmecký	k2eAgMnSc1d1	sudetoněmecký
separatista	separatista	k1gMnSc1	separatista
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Heinrich	Heinrich	k1gMnSc1	Heinrich
Herkner	Herkner	k1gMnSc1	Herkner
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hnělička	Hnělička	k1gFnSc1	Hnělička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
od	od	k7c2	od
Tobruku	Tobruk	k1gInSc2	Tobruk
a	a	k8xC	a
Dunkerque	Dunkerque	k1gFnPc2	Dunkerque
Kamillo	Kamillo	k1gNnSc1	Kamillo
Horn	Horn	k1gMnSc1	Horn
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Petr	Petr	k1gMnSc1	Petr
Jeništa	Jeništa	k1gMnSc1	Jeništa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g />
.	.	kIx.	.
</s>
<s>
Kaiser	Kaiser	k1gMnSc1	Kaiser
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
finanční	finanční	k2eAgFnSc2d1	finanční
skupiny	skupina	k1gFnSc2	skupina
PPF	PPF	kA	PPF
<g/>
,	,	kIx,	,
nejbohatší	bohatý	k2eAgMnSc1d3	nejbohatší
Čech	Čech	k1gMnSc1	Čech
Štěpán	Štěpán	k1gMnSc1	Štěpán
Klásek	klásek	k1gInSc1	klásek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
Jiří	Jiří	k1gMnSc1	Jiří
Kořínek	kořínek	k1gInSc1	kořínek
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esperantista	esperantista	k1gMnSc1	esperantista
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Harald	Harald	k1gMnSc1	Harald
Kreutzberg	Kreutzberg	k1gMnSc1	Kreutzberg
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
baletní	baletní	k2eAgMnSc1d1	baletní
teoretik	teoretik	k1gMnSc1	teoretik
Franz	Franz	k1gMnSc1	Franz
Liebieg	Liebieg	k1gMnSc1	Liebieg
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bankéř	bankéř	k1gMnSc1	bankéř
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
průmyslník	průmyslník	k1gMnSc1	průmyslník
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Olga	Olga	k1gFnSc1	Olga
Lounová	Lounová	k1gFnSc1	Lounová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Markus	Markus	k1gMnSc1	Markus
Lüpertz	Lüpertz	k1gMnSc1	Lüpertz
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Ludmila	Ludmila	k1gFnSc1	Ludmila
Macešková	maceškový	k2eAgFnSc1d1	Macešková
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
publikující	publikující	k2eAgFnSc1d1	publikující
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Jan	Jan	k1gMnSc1	Jan
Kameník	Kameník	k1gMnSc1	Kameník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Masopust	Masopust	k1gMnSc1	Masopust
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
klinický	klinický	k2eAgMnSc1d1	klinický
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Roderich	Roderich	k1gMnSc1	Roderich
Menzel	Menzel	k1gMnSc1	Menzel
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Petr	Petr	k1gMnSc1	Petr
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Marek	Marek	k1gMnSc1	Marek
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
Kenny	Kenna	k1gFnSc2	Kenna
Rough	Rougha	k1gFnPc2	Rougha
<g/>
)	)	kIx)	)
Miroslava	Miroslava	k1gFnSc1	Miroslava
Pleštilová	Pleštilová	k1gFnSc1	Pleštilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
na	na	k7c6	na
DAMU	DAMU	kA	DAMU
<g />
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Porsche	Porsche	k1gNnSc2	Porsche
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
automobilový	automobilový	k2eAgMnSc1d1	automobilový
konstruktér	konstruktér	k1gMnSc1	konstruktér
Otfried	Otfried	k1gMnSc1	Otfried
Preußler	Preußler	k1gMnSc1	Preußler
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Řídký	řídký	k2eAgMnSc1d1	řídký
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g />
.	.	kIx.	.
</s>
<s>
Augustin	Augustin	k1gMnSc1	Augustin
Schramm	Schramm	k1gMnSc1	Schramm
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komunistický	komunistický	k2eAgMnSc1d1	komunistický
funkcionář	funkcionář	k1gMnSc1	funkcionář
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
NKVD	NKVD	kA	NKVD
David	David	k1gMnSc1	David
Švehlík	Švehlík	k1gMnSc1	Švehlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Šalda	Šalda	k1gMnSc1	Šalda
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jiří	Jiří	k1gMnSc1	Jiří
Unger	Unger	k1gMnSc1	Unger
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
religionista	religionista	k1gMnSc1	religionista
Karel	Karel	k1gMnSc1	Karel
Vacek	Vacek	k1gMnSc1	Vacek
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Pavel	Pavel	k1gMnSc1	Pavel
Vandas	Vandas	k1gMnSc1	Vandas
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Benjamin	Benjamin	k1gMnSc1	Benjamin
Vomáčka	Vomáčka	k1gMnSc1	Vomáčka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Luděk	Luděk	k1gMnSc1	Luděk
<g />
.	.	kIx.	.
</s>
<s>
Zelenka	Zelenka	k1gMnSc1	Zelenka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
Pavel	Pavel	k1gMnSc1	Pavel
Pěnička	pěnička	k1gFnSc1	pěnička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
Martin	Martin	k1gMnSc1	Martin
Pěnička	pěnička	k1gFnSc1	pěnička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
Johann	Johann	k1gMnSc1	Johann
Liebieg	Liebieg	k1gMnSc1	Liebieg
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkopodnikatel	velkopodnikatel	k1gMnSc1	velkopodnikatel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zakladatel	zakladatel	k1gMnSc1	zakladatel
rodu	rod	k1gInSc2	rod
Liebiegů	Liebieg	k1gMnPc2	Liebieg
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
představitel	představitel	k1gMnSc1	představitel
libereckého	liberecký	k2eAgInSc2d1	liberecký
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
Václav	Václav	k1gMnSc1	Václav
Šamánek	Šamánek	k1gMnSc1	Šamánek
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
českojazyčného	českojazyčný	k2eAgNnSc2d1	českojazyčné
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Max	Max	k1gMnSc1	Max
Kühn	Kühn	k1gMnSc1	Kühn
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Miklós	Miklósa	k1gFnPc2	Miklósa
Radnóti	Radnóť	k1gFnSc2	Radnóť
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
studentem	student	k1gMnSc7	student
liberecké	liberecký	k2eAgFnSc2d1	liberecká
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
tkalcovské	tkalcovský	k2eAgFnSc2d1	tkalcovská
školy	škola	k1gFnSc2	škola
František	František	k1gMnSc1	František
Peterka	Peterka	k1gMnSc1	Peterka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Franz	Franz	k1gMnSc1	Franz
Peter	Peter	k1gMnSc1	Peter
Künzel	Künzel	k1gMnSc1	Künzel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
Karel	Karel	k1gMnSc1	Karel
Hubáček	Hubáček	k1gMnSc1	Hubáček
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Perretovy	Perretův	k2eAgFnSc2d1	Perretova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
vysílač	vysílač	k1gInSc4	vysílač
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Velinský	velinský	k2eAgMnSc1d1	velinský
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
;	;	kIx,	;
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
a	a	k8xC	a
zachytil	zachytit	k5eAaPmAgMnS	zachytit
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
atmosféru	atmosféra	k1gFnSc4	atmosféra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
svých	svůj	k3xOyFgInPc2	svůj
románů	román	k1gInPc2	román
Sebastian	Sebastian	k1gMnSc1	Sebastian
Navrátil	Navrátil	k1gMnSc1	Navrátil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
hlavně	hlavně	k9	hlavně
svým	svůj	k3xOyFgInSc7	svůj
singlem	singl	k1gInSc7	singl
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Přemysl	Přemysl	k1gMnSc1	Přemysl
Sobotka	Sobotka	k1gMnSc1	Sobotka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
a	a	k8xC	a
rentgenolog	rentgenolog	k1gMnSc1	rentgenolog
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
od	od	k7c2	od
r.	r.	kA	r.
1945	[number]	k4	1945
Eduard	Eduard	k1gMnSc1	Eduard
Clam-Gallas	Clam-Gallas	k1gMnSc1	Clam-Gallas
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Miloslav	Miloslav	k1gMnSc1	Miloslav
Nevrlý	nevrlý	k2eAgMnSc1d1	nevrlý
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
turista	turista	k1gMnSc1	turista
a	a	k8xC	a
skaut	skaut	k1gMnSc1	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
skautský	skautský	k2eAgMnSc1d1	skautský
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
činovník	činovník	k1gMnSc1	činovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pokračovatele	pokračovatel	k1gMnSc4	pokračovatel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
Amersfoort	Amersfoort	k1gInSc4	Amersfoort
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Amiens	Amiens	k1gInSc1	Amiens
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Augsburg	Augsburg	k1gInSc1	Augsburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Dunkerque	Dunkerque	k1gNnSc2	Dunkerque
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Naharija	Naharijum	k1gNnSc2	Naharijum
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
St.	st.	kA	st.
Gallen	Gallna	k1gFnPc2	Gallna
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Žitava	Žitava	k1gFnSc1	Žitava
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
