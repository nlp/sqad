<p>
<s>
Biblioteca	Bibliotec	k2eAgFnSc1d1	Biblioteca
Nacional	Nacional	k1gFnSc1	Nacional
de	de	k?	de
Españ	Españ	k1gFnPc2	Españ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Paseo	Paseo	k1gMnSc1	Paseo
de	de	k?	de
Recoletos	Recoletos	k1gMnSc1	Recoletos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Knihovnu	knihovna	k1gFnSc4	knihovna
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
V.	V.	kA	V.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
jako	jako	k8xC	jako
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
knihovnu	knihovna	k1gFnSc4	knihovna
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
Biblioteca	Biblioteca	k1gMnSc1	Biblioteca
Pública	Pública	k1gMnSc1	Pública
de	de	k?	de
Palacio	Palacio	k1gMnSc1	Palacio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
královského	královský	k2eAgInSc2d1	královský
patentu	patent	k1gInSc2	patent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Filip	Filip	k1gMnSc1	Filip
V.	V.	kA	V.
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
tiskař	tiskař	k1gMnSc1	tiskař
povinnost	povinnost	k1gFnSc4	povinnost
odevzdat	odevzdat	k5eAaPmF	odevzdat
jeden	jeden	k4xCgInSc4	jeden
výtisk	výtisk	k1gInSc4	výtisk
každé	každý	k3xTgFnSc2	každý
knihy	kniha	k1gFnSc2	kniha
této	tento	k3xDgFnSc6	tento
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
status	status	k1gInSc1	status
knihovny	knihovna	k1gFnSc2	knihovna
jakožto	jakožto	k8xS	jakožto
majetku	majetek	k1gInSc2	majetek
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
Ministerio	Ministerio	k1gNnSc1	Ministerio
de	de	k?	de
la	la	k1gNnPc2	la
Gobernación	Gobernación	k1gInSc1	Gobernación
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
knihovna	knihovna	k1gFnSc1	knihovna
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
knihovnu	knihovna	k1gFnSc4	knihovna
(	(	kIx(	(
<g/>
Biblioteca	Bibliotec	k2eAgFnSc1d1	Biblioteca
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
knihovna	knihovna	k1gFnSc1	knihovna
získala	získat	k5eAaPmAgFnS	získat
řadu	řada	k1gFnSc4	řada
starožitných	starožitný	k2eAgFnPc2d1	starožitná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1896	[number]	k4	1896
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
knihovna	knihovna	k1gFnSc1	knihovna
sídlí	sídlet	k5eAaImIp3nS	sídlet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
čítárna	čítárna	k1gFnSc1	čítárna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pojmula	pojmout	k5eAaPmAgFnS	pojmout
320	[number]	k4	320
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
zabavil	zabavit	k5eAaPmAgInS	zabavit
Konfiskační	konfiskační	k2eAgInSc1d1	konfiskační
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
Junta	junta	k1gFnSc1	junta
de	de	k?	de
Incautación	Incautación	k1gInSc1	Incautación
<g/>
)	)	kIx)	)
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
v	v	k7c6	v
náboženských	náboženský	k2eAgNnPc6d1	náboženské
zařízeních	zařízení	k1gNnPc6	zařízení
a	a	k8xC	a
soukromých	soukromý	k2eAgInPc6d1	soukromý
domech	dům	k1gInPc6	dům
a	a	k8xC	a
uložil	uložit	k5eAaPmAgInS	uložit
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
začaly	začít	k5eAaPmAgFnP	začít
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
ztrojnásobit	ztrojnásobit	k5eAaPmF	ztrojnásobit
kapacitu	kapacita	k1gFnSc4	kapacita
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
v	v	k7c4	v
Alcalá	Alcalý	k2eAgNnPc4d1	Alcalý
de	de	k?	de
Henares	Henaresa	k1gFnPc2	Henaresa
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
upravena	upraven	k2eAgFnSc1d1	upravena
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
budova	budova	k1gFnSc1	budova
na	na	k7c4	na
Paseo	Paseo	k1gNnSc4	Paseo
de	de	k?	de
Recoletos	Recoletos	k1gInSc1	Recoletos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
knihovnou	knihovna	k1gFnSc7	knihovna
sloučeny	sloučen	k2eAgInPc4d1	sloučen
některé	některý	k3yIgInPc4	některý
dosud	dosud	k6eAd1	dosud
samostatné	samostatný	k2eAgFnPc1d1	samostatná
instituce	instituce	k1gFnPc1	instituce
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
Hemeroteca	Hemerotec	k2eAgFnSc1d1	Hemerotec
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělský	španělský	k2eAgInSc1d1	španělský
bibliografický	bibliografický	k2eAgInSc1d1	bibliografický
institut	institut	k1gInSc1	institut
(	(	kIx(	(
<g/>
Instituto	Institut	k2eAgNnSc1d1	Instituto
Bibliográfico	Bibliográfico	k1gNnSc1	Bibliográfico
Hispánico	Hispánico	k1gNnSc1	Hispánico
<g/>
)	)	kIx)	)
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
a	a	k8xC	a
bibliografických	bibliografický	k2eAgInPc2d1	bibliografický
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
Centro	Centro	k1gNnSc1	Centro
del	del	k?	del
Tesoro	Tesora	k1gFnSc5	Tesora
Documental	Documental	k1gMnSc1	Documental
y	y	k?	y
Bibliográfico	Bibliográfico	k1gMnSc1	Bibliográfico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
stala	stát	k5eAaPmAgFnS	stát
autonomní	autonomní	k2eAgFnSc7d1	autonomní
entitou	entita	k1gFnSc7	entita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
Ministerio	Ministerio	k6eAd1	Ministerio
de	de	k?	de
Cultura	Cultura	k1gFnSc1	Cultura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Biblioteca	Bibliotecus	k1gMnSc2	Bibliotecus
Nacional	Nacional	k1gMnSc2	Nacional
de	de	k?	de
Españ	Españ	k1gMnSc2	Españ
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Španělska	Španělsko	k1gNnSc2	Španělsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
