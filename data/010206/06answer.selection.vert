<s>
Biblioteca	Bibliotec	k2eAgFnSc1d1	Biblioteca
Nacional	Nacional	k1gFnSc1	Nacional
de	de	k?	de
Españ	Españ	k1gFnPc2	Españ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
