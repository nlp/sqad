<s>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UN	UN	kA	UN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
světa	svět	k1gInSc2	svět
–	–	k?	–
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
193	[number]	k4	193
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
